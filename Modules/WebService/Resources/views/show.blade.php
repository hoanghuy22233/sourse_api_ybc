@extends(config('core.admin_theme').'.template')
@section('main')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
			<i class="la la-leaf"></i>
			</span>
                    <h3 class="kt-portlet__head-title">
                        Các gói dịch vụ
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="kt-pricing-1">
                    <div class="kt-pricing-1__items row">
                        <?php
                        $services = \Modules\WebService\Models\Service::where('display', 1)->orderBy('order_no', 'desc')->get();
                        ?>
                        @foreach($services as $service)
                            <?php
                            $colors = [
                                'btn-brand' => 0, 'btn-success' => 1, 'btn-danger' => 2, 'btn-warning' => 3
                            ];
                            $colors_rand = array_rand($colors);

                            $colors_icon = [
                                'kt-font-brand' => 0, 'kt-font-success' => 1, 'kt-font-danger' => 2, 'kt-font-warning' => 3
                            ];
                            $colors_icon_rand = array_rand($colors_icon);
                            $min_price = 0;
                            $price = json_decode($service->price);
                            if (is_array($price)) {
                                foreach ($price as $k => $val) {
                                    if ($k == 0) {
                                        $min_price = $val->price;
                                    } else {
                                        if ($val->price < $min_price) {
                                            $min_price = $val->price;
                                        }
                                    }
                                }
                            }
                            ?>
                            <div class="kt-pricing-1__item col-lg-3">
                                <div class="kt-pricing-1__visual">
                                    <div class="kt-pricing-1__hexagon1"></div>
                                    <div class="kt-pricing-1__hexagon2"></div>
                                    <span class="kt-pricing-1__icon {{$colors_icon_rand}}"><img
                                                src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($service->image, 111, null) }}"></span>
                                </div>
                                <span class="kt-pricing-1__price">{{number_format($min_price, 0, ',', '.')}}<sup>đ</sup></span>
                                <label style="font-weight: bold;">{{$service->name_vi}}</label>
                                <p class="kt-pricing-1__description" style="margin: 0;">
                                    {{ $service->account_max }} tài khoản
                                </p>
                                {{--<h2 class="kt-pricing-1__subtitle">{!! $service->intro !!}</h2>--}}
                                <div class="kt-pricing-1__btn">
                                    <a href="/admin/service_history/add?service_id={{$service->id}}">
                                        <button style="padding: 0.86rem 3.57rem 0.86rem 3.57rem;"
                                                class="btn {{$colors_rand}} btn-custom btn-pill btn-wide btn-uppercase btn-bolder btn-sm">
                                            Đăng ký
                                        </button>
                                    </a>
                                </div>

                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!--end::Portlet-->
    </div>
@endsection
