<!--begin::Portlet-->
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Thông tin thanh toán
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body">
            <div class="kt-section kt-section--first">

                @foreach($module['form']['general_tab2'] as $field)
                    @php
                        $field['value'] = @$result->{$field['name']};
                    @endphp
                    @include(config('core.admin_theme').".common.td.".$field['type'], ['field' => $field])
                @endforeach
            </div>
        </div>
    </div>
    <!--end::Form-->
</div>
<!--end::Portlet-->
<style>
    .fieldwrapper > div {
        display: inline-block;
    }
</style>
<script>
    $('.add-contact-info').click(function () {
        console.log('fd');
    });
</script>