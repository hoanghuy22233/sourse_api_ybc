@extends(config('core.admin_theme').'.template')
@section('main')
    <form class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid {{ @$module['code'] }}"
          action="" method="POST"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="return_direct" value="save_exit" type="hidden">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                     id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg" style="">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Đăng ký dịch vụ
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="/admin/service/show" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Quay lại</span>
                            </a>
                            <div class="btn-group">
                                @if(in_array($module['code'].'_add', $permissions))
                                    <button type="submit" class="btn btn-brand">
                                        <i class="la la-check"></i>
                                        <span class="kt-hidden-mobile">Đăng ký</span>
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-5" style="padding-right:0;padding-left:10px">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Thông tin gói dịch vụ
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body" style="    padding-top: 10px;">
                            <div class="kt-section kt-section--first">
                                {{--<select name="service_type" class="form-control">--}}
                                    {{--<option value="1">1</option>--}}
                                    {{--<option value="1">1</option>--}}
                                    {{--<option value="1">1</option>--}}
                                {{--</select>--}}
                                @foreach($module['form']['general_tab'] as $field)
                                    @if($field['type'] == 'custom')
                                        <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                             id="form-group-{{ $field['name'] }}">
                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                    <span class="color_btd">*</span>@endif</label>
                                            <div class="col-xs-12">
                                                @include($field['field'], ['field' => $field])
                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                            </div>
                                        </div>
                                    @else
                                        <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                             id="form-group-{{ $field['name'] }}">
                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                    <span class="color_btd">*</span>@endif</label>
                                            <div class="col-xs-12">
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                     id="form-group-{{ $field['name'] }}">
                                    <label>Giá tiền của gói đang chọn: <span id="service_current_price"></span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
            <div class="col-xs-12 col-md-7" style="padding-right:0;padding-left:10px">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Thông tin thanh toán
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body" style="    padding-top: 10px;">
                            <div class="kt-section kt-section--first">
                                <div class="form-group-div form-group">
                                    <label>Tổng tiền phải thanh toán: <span id="total_price"></span></label>
                                </div>
                                <div class="form-group-div form-group">
                                    <label>Giải thích: </label>
                                    <div class="col-xs-12">
                                        <span id="note"></span>
                                    </div>
                                </div>
                                @if (CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'service_history_publish'))
                                    <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                         id="form-group-{{ $field['name'] }}">
                                        <div class="col-xs-12">
                                            @include('webservice::form.fields.service_history_status', ['field' => ['name' => 'status', 'type' => 'custom', 'field' => '', 'label' => 'Kích hoạt gói', 'value' => 0]])
                                            <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </form>
@endsection
@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/form.css') }}">
@endsection
@section('custom_footer')
    <script src="{{ asset(config('core.admin_asset').'/js/pages/crud/metronic-datatable/advanced/vertical.js') }}"
            type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce_editor.js') }}"></script>
    <script type="text/javascript">
        editor_config.selector = ".editor";
        editor_config.path_absolute = "{{ (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" }}/";
        tinymce.init(editor_config);
    </script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/form.js') }}"></script>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            getTimeService('{{ @$_GET['use_date_max'] }}');

            $('body').on('change', 'select[name=service_id]', function () {
                getTimeService();
            });

            $('body').on('change', 'select[name=use_date_max]', function () {
                getTimeService();
            });
        });

        function getTimeService(use_date_max = false) {
            if (!use_date_max) {
                use_date_max = $('select[name=use_date_max]').val();
            }
            $.ajax({
                url: "/admin/service/get-price",
                data: {
                    service_id: $('select[name=service_id]').val(),
                    use_date_max: use_date_max
                },
                success: function (result) {
                    $('select[name=use_date_max]').html(result.date_option);
                    $('#service_current_price').html(result.service_price);
                    $('#total_price').html(result.payment);
                    $('#note').html(result.note);
                },
                error: function () {
                    toastr.error("Có lỗi xảy ra khi lấy giá dịch vụ! Vui lòng load lại trang và thử lại");
                }
            });
        }
    </script>
@endpush