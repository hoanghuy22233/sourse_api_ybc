@php
    $data = \Modules\WebService\Models\Service::where('display', 1)->orderBy('id', 'asc')->get();
@endphp
<?php

if (isset($field['multiple'])) {
    $value = old($field['name']) != null ? old($field['name']) : explode('|', @$field['value']);
} else {
    $value[] = old($field['name']) != null ? old($field['name']) : @$field['value'];
}
?>

<select class="form-control {{ $field['class'] or '' }}" id="{{ $field['name'] }}"
        {{ strpos($field['class'], 'require') !== false ? 'required' : '' }}
        name="{{ $field['name'] }}" {!! @$field['inner'] !!}>
    <option value="0">Chọn số ngày</option>
</select>
<span class="text-danger">{{ $errors->first($field['name']) }}</span>
