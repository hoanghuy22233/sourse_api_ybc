<?php

namespace Modules\WebService\Models;

use \Modules\EworkingCompany\Models\Company;
use Illuminate\Database\Eloquent\Model;

class ServiceHistory extends Model
{
    protected $table = 'service_history';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'company_id', 'service_id', 'use_date_max', 'payment', 'start_date', 'exp_date', 'status', 'service_type', 'account_max'
    ];
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }

}
