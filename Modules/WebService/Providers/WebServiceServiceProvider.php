<?php

namespace Modules\WebService\Providers;

use App\Models\Setting;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\JdesBill\Models\Bill;
use Modules\WebService\Console\ServiceRenewal;
use Mail;
use App\Mail\MailServer;

class WebServiceServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
//        $this->registerTranslations();
//        $this->registerFactories();
//        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            $this->registerConfig();
            $this->registerViews();

            //  Setting Custom
            $this->addSetting();
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();

            //  Xoa menu khách hàng
            \Eventy::addFilter('aside_menu.user', function () {
                return '';
            }, 2, 1);
        }
        //  Setting Custom
        $this->schedule();
        $this->commands($this->moreCommands);
    }

    protected $moreCommands = [
        ServiceRenewal::class
    ];

    public function addSetting()
    {
        \Eventy::addFilter('setting.custom_module', function ($module) {
            $module['tabs']['web_service'] = [
                'label' => 'Dịch vụ web',
                'icon' => '<i class="flaticon2-time"></i>',
                'td' => [
                    ['name' => 'min_day', 'type' => 'number', 'label' => 'Số ngày thông báo trước kì hạn'],
                    ['name' => 'max_day', 'type' => 'number', 'label' => 'Số ngày thông báo quá kì hạn'],
                    ['name' => 'inner', 'type' => 'inner', 'label' => '', 'html' => '<b>Cấu hình gia hạn tự động</b>'],
                    ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt gửi thông báo tự động'],
                    ['name' => 'minute_scan', 'type' => 'text', 'label' => 'Phút (0-59) tương ứng với số từ (0-59)', 'des' => 'Nhập vào số phút, có thể nhập vào 2 giá trị các nhau bởi dấu phảy :<br> Ví dụ ( phút 20 và tháng 50 ) : 20, 50'],
                    ['name' => 'hour_scan', 'type' => 'text', 'label' => 'Giờ (0-23) tương ứng với số từ (0-23)', 'des' => 'Nhập vào số tháng, có thể nhập vào 2 giá trị các nhau bởi dấu phảy'],
                    ['name' => 'day_in_month_scan', 'type' => 'text', 'label' => 'Ngày trong tháng (1-31) tương ứng với số từ (1-31)', 'des' => 'Nhập vào số ngày trong tháng, có thể nhập vào 2 giá trị các nhau bởi dấu phảy'],
                    ['name' => 'month_scan', 'type' => 'text', 'label' => 'Tháng (1-12) tương ứng với số từ (1-12)', 'des' => 'Nhập vào số tháng, có thể nhập vào 2 giá trị các nhau bởi dấu phảy'],
                    ['name' => 'day_in_week_scan', 'type' => 'text', 'label' => 'Thứ trong tuần ( thứ 2 -> Chủ nhật tương ứng với số từ 0 -> 7)', 'des' => 'Nhập vào số giờ, có thể nhập vào 2 giá trị các nhau bởi dấu phảy (chủ nhật = 0 or 7)'],
                ]
            ];
            return $module;
        }, 1, 1);
    }

    public function schedule()
    {
        \Eventy::addAction('schedule.run', function ($schedule) {
            $settings = Setting::where('type', 'web_service')->pluck('value', 'name')->toArray();
            if ($settings['status'] == 1) {
                $cron = @$settings['minute_scan'] . ' ' . @$settings['hour_scan'] . ' ' . @$settings['day_in_month_scan'] . ' ' . @$settings['month_scan'] . ' ' . @$settings['day_in_week_scan'];
                $schedule->command('services:run')->cron($cron);
            }
            return true;
        }, 1, 1);
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['service_view', 'service_add', 'service_edit', 'service_delete', 'service_history_publish',
                'service_history_view', 'service_history_add', 'service_history_edit', 'service_history_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function lockScreenWhenServiceExpires()
    {
        \Eventy::addFilter('block.main_before', function () {
            print view('webservice::partials.lock_screen_when_service_expires');
        }, 1, 1);
    }

    public function rendUserBarFooter()
    {
        \Eventy::addFilter('user_bar.footer', function () {
            print '<div class="kt-notification__custom kt-space-between">
    <a href="/admin/logout"
       class="btn btn-label btn-label-brand btn-sm btn-bold">Đăng xuất</a>

    <a href="/admin/service/show"
       class="btn btn-clean btn-sm btn-bold">Nâng cấp tài khoản</a>
</div>';
        }, 1, 1);
    }

    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('webservice::partials.aside_menu.aside_menu_dashboard_after');
        }, 2, 1);
    }

    public function addSettingService()
    {
        \Eventy::addFilter('setting.custom_module', function ($module) {
            $module['tabs']['service_tab'] = [
                'label' => 'Cấu hình dịch vụ',
                'icon' => '<i class="flaticon-mail"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'service_trial_id', 'type' => 'select2_model', 'label' => 'Gói dùng thử',
                        'model' => Service::class, 'display_field' => 'name_vi',],
                    ['name' => 'payment_before_date', 'type' => 'number', 'label' => 'Yêu cầu thanh toán trước số ngày',],
                ]
            ];
            return $module;
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('webservice.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'webservice'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/webservice');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/webservice';
        }, \Config::get('view.paths')), [$sourcePath]), 'webservice');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/webservice');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'webservice');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'webservice');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
