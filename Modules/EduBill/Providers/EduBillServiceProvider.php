<?php

namespace Modules\EduBill\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class EduBillServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Cấu hình Core
            $this->registerViews();
            //  Custom setting
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }
    }

    public function registerPermission()
    {
//        \Eventy::addFilter('permission.check', function ($per_check) {
//            $per_check = array_merge($per_check, ['course_view', 'course_add', 'course_edit', 'course_delete',
//                'quizzes_view', 'quizzes_add', 'quizzes_edit', 'quizzes_delete','misson_view', 'misson_add', 'misson_edit', 'misson_delete']);
//            return $per_check;
//        }, 1, 1);
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('edubill::partials.aside_menu.dashboard_after_course');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('EduBill', 'Config/config.php') => config_path('edubill.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('EduBill', 'Config/config.php'), 'edubill'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/edubill');

        $sourcePath = module_path('EduBill', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/edubill';
        }, \Config::get('view.paths')), [$sourcePath]), 'edubill');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/edubill');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'edubill');
        } else {
            $this->loadTranslationsFrom(module_path('EduBill', 'Resources/lang'), 'edubill');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('EduBill', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
