<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace Modules\EduBill\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\EduCourse\Models\Lesson;

class Course extends Model {

    protected $table = 'courses';

    protected $appends = ['category'];

//    Khóa học liên quan
    public function getCategoryAttribute()
    {
        $cat_ids = explode('|', @$this->attributes['multi_cat']);
        return Category::whereIn('id', $cat_ids)->first();
    }
    public function lesson()
    {
        return $this->hasMany(Lesson::class);
    }
    //Giảng viên
    public function lecturer()
    {
        return $this->belongsTo(Admin::class,'lecturer_id','id');
    }
    //Học viên
    public function contact()
    {
        return $this->hasMany(Contact::class,'course_id','id');
    }
    public function students()
    {
        return $this->hasMany(Student::class,'course_id','id');
    }

}