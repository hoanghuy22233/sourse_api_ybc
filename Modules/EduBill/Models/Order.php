<?php

namespace Modules\EduBill\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';

    protected $guarded = [];

    public function course() {
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }

}
