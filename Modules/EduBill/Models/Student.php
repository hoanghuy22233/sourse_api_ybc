<?php

namespace Modules\EduBill\Models;


use DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use Authenticatable;
    protected $guard = 'student';
    protected $guard_name = 'student';

    protected $table = 'students';
    protected $guarded = [];


}
