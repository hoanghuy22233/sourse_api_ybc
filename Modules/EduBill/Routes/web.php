<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {

    Route::group(['prefix' => 'bill'], function () {
        Route::get('', 'Admin\BillController@getIndex')->name('bill');
        Route::get('publish', 'Admin\BillController@getPublish')->name('bill.publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BillController@add');
        Route::get('delete/{id}', 'Admin\BillController@delete');
        Route::post('multi-delete', 'Admin\BillController@multiDelete');
        Route::get('search-for-select2', 'Admin\BillController@searchForSelect2')->name('bill.search_for_select2');
        Route::post('active-order', 'Admin\BillController@activeOrder')->name('bill.active-order');
        Route::post('active-order-all', 'Admin\BillController@activeOrderAll')->name('bill.active-order-all');
        Route::get('{id}', 'Admin\BillController@update');
        Route::post('{id}', 'Admin\BillController@update');

    });

});
