<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'label'], function () {
        Route::get('', 'Admin\LabelController@getIndex')->name('label')->middleware('permission:label_view');
        Route::get('publish', 'Admin\LabelController@getPublish')->name('label.publish')->middleware('permission:label_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\LabelController@add')->middleware('permission:label_add');
        Route::get('delete/{id}', 'Admin\LabelController@delete')->middleware('permission:label_delete');
        Route::post('multi-delete', 'Admin\LabelController@multiDelete')->middleware('permission:label_delete');
        Route::get('search-for-select2', 'Admin\LabelController@searchForSelect2')->name('label.search_for_select2')->middleware('permission:label_view');
        Route::get('{id}', 'Admin\LabelController@update')->middleware('permission:label_view');
        Route::post('{id}', 'Admin\LabelController@update')->middleware('permission:label_edit');
    });


    Route::group(['prefix' => 'group_label'], function () {
        Route::get('ajax-get-data', 'Admin\GroupLabelController@ajaxGetData');

        Route::get('', 'Admin\GroupLabelController@getIndex')->name('group_label')->middleware('permission:group_label_view');
        Route::get('publish', 'Admin\GroupLabelController@getPublish')->name('group_label.publish')->middleware('permission:group_label_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\GroupLabelController@add')->middleware('permission:group_label_add');
        Route::get('delete/{id}', 'Admin\GroupLabelController@delete')->middleware('permission:group_label_delete');
        Route::post('multi-delete', 'Admin\GroupLabelController@multiDelete')->middleware('permission:group_label_delete');
        Route::get('search-for-select2', 'Admin\GroupLabelController@searchForSelect2')->name('group_label.search_for_select2')->middleware('permission:group_label_view');
        Route::get('{id}', 'Admin\GroupLabelController@update')->middleware('permission:group_label_view');
        Route::post('{id}', 'Admin\GroupLabelController@update')->middleware('permission:group_label_edit');
    });



    Route::group(['prefix' => 'tag'], function () {
        Route::get('', 'Admin\TagController@getIndex')->name('tag')->middleware('permission:group_label_view');
        Route::get('publish', 'Admin\TagController@getPublish')->name('tag.publish')->middleware('permission:tag_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TagController@add')->middleware('permission:tag_add');
        Route::get('delete/{id}', 'Admin\TagController@delete')->middleware('permission:tag_delete');
        Route::post('multi-delete', 'Admin\TagController@multiDelete')->middleware('permission:tag_delete');
        Route::get('search-for-select2', 'Admin\TagController@searchForSelect2')->name('group_label.search_for_select2')->middleware('permission:tag_view');
        Route::get('{id}', 'Admin\TagController@update')->middleware('permission:tag_view');
        Route::post('{id}', 'Admin\TagController@update')->middleware('permission:tag_edit');
    });


});
