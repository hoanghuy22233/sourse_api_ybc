<?php

namespace Modules\JdesLabel\Models;

use Illuminate\Database\Eloquent\Model;


class GroupLabel extends Model
{

    protected $table = "group_label";

    public $timestamps = false;

    protected $fillable = [
        'name', 'type', 'code', 'unit', 'order_no', 'parent_id', 'status','note','id'
    ];

    public function childs()
    {
        return $this->hasMany($this, 'parent_id', 'id')->orderBy('order_no', 'asc');
    }
    public function lable()
    {
        return $this->hasMany(Label::class,'group_label_ids','id');
    }


}
