<?php
$trace_elements = \Modules\A4iLand\Models\TraceElement::pluck('name', 'id');
$land_parameters = (array)json_decode(@$result->land_parameters);
$water_parameters = (array)json_decode(@$result->water_parameters);
?>
<div class="col-xs-12 col-md-6">
    <!--begin::Portlet-->
    <div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_1">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Thông số đất
                </h3>
            </div>
            <div class="kt-portlet__head-group pt-3">
                <a title="Xem thêm" href="#" data-ktportlet-tool="toggle"
                   class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-angle-down"></i></a>
            </div>
        </div>
        <!--begin::Form-->
        <div class="kt-form">
            <div class="kt-portlet__body">
                <div class="kt-section kt-section--first">
                    <?php
                    $field = ['name' => 'land_parameters_image', 'type' => 'multiple_image_dropzone', 'class' => '', 'label' => 'Ảnh thông số đất', 'count' => 6];
                    ?>
                    <div class="form-group-div form-group {{ @$field['group_class'] }}"
                         id="form-group-{{ $field['name'] }}">
                        <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                <span class="color_btd">*</span>@endif</label>
                        <div class="col-xs-12">
                            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                        </div>
                    </div>

                    @foreach($trace_elements as $k => $v)
                        <div class="form-group-div form-group col-xs-6 col-md-6">
                            <div class="form-group">
                                <label class="col-xs-12">{!! $v !!}</label>
                                <div class="col-xs-12">
                                    <input type="text" class="col-xs-5 form-control fieldValue"
                                           name="land_parameters[{{ $k }}]" value="{{ @$land_parameters[$k] }}">
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->
</div>



<div class="col-xs-12 col-md-6">
    <!--begin::Portlet-->
    <div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_1">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Thông số nước
                </h3>

            </div>
            <div class="kt-portlet__head-group pt-3">
                <a title="Xem thêm" href="#" data-ktportlet-tool="toggle"
                   class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-angle-down"></i></a>
            </div>
        </div>
        <!--begin::Form-->
        <div class="kt-form">
            <div class="kt-portlet__body">
                <div class="kt-section kt-section--first">
                    <?php
                    $field = ['name' => 'water_parameters_image', 'type' => 'multiple_image_dropzone', 'class' => '', 'label' => 'Ảnh thông số nước', 'count' => 6];
                    ?>
                    <div class="form-group-div form-group {{ @$field['group_class'] }}"
                         id="form-group-{{ $field['name'] }}">
                        <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                <span class="color_btd">*</span>@endif</label>
                        <div class="col-xs-12">
                            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                        </div>
                    </div>

                    @foreach($trace_elements as $k => $v)
                        <div class="form-group-div form-group col-xs-6 col-md-6">
                            <div class="form-group">
                                <label class="col-xs-12">{!! $v !!}</label>
                                <div class="col-xs-12">
                                    <input type="text" class="col-xs-5 form-control fieldValue"
                                           name="water_parameters[{{ $k }}]" value="{{ @$water_parameters[$k] }}">
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!--end::Form-->
    </div>
    <!--end::Portlet-->
</div>
