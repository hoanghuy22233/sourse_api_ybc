<!--begin::Portlet-->
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Thông tin mùa vụ
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body">
            <div class="kt-section kt-section--first">
                <label>Đang diễn ra:</label>
                <?php
                $seasons = \Modules\A4iSeason\Models\Season::select(['id', 'tree_name', 'implementation_date'])->where('land_id', $result->id)->where('status', 1)->orderBy('id', 'desc')->get();
                ?>
                @if(count($seasons) > 0)
                    <ul>
                        @foreach($seasons as $v)
                            <li><a href="/admin/season/{{ $v->id }}">{{ $v->tree_name }}
                                    - {{ date('d/m/Y', strtotime($v->implementation_date)) }}</a></li>
                        @endforeach
                    </ul>
                @else
                    Chưa có mùa vụ nào<br>
                    @if(@$result->admin_id == \Auth::guard('admin')->user()->id)
                        <a href="/admin/season/add?land_id={{ $result->id }}" class="btn btn-outline-brand btn-sm ml-2"><i
                                    class="flaticon2-plus"></i>Tạo mùa vụ</a><br>
                    @endif
                @endif

                <label>Đã qua:</label>
                <?php
                $seasons = \Modules\A4iSeason\Models\Season::select(['id', 'tree_name', 'implementation_date'])->where('land_id', $result->id)->where('status', 0)->orderBy('id', 'desc')->get();
                ?>
                <ul>
                    @foreach($seasons as $v)
                        <li><a href="/admin/season/{{ $v->id }}">{{ $v->tree_name }}
                                - {{ date('d/m/Y', strtotime($v->implementation_date)) }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <!--end::Form-->
</div>
<!--end::Portlet-->