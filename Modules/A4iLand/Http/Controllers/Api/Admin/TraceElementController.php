<?php

namespace Modules\A4iLand\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\A4iOrganization\Models\Organization;
use Illuminate\Http\Request;
use Modules\A4iLand\Models\TraceElement;
use Validator;

class TraceElementController extends Controller
{

    protected $module = [
        'code' => 'trace_element',
        'table_name' => 'trace_elements',
        'label' => 'Thông số',
        'modal' => 'Modules\A4iLand\Models\TraceElement',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
    ];

    public function index(Request $request)
    {
        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = TraceElement::select('id', 'name', 'description')->whereRaw($where);

            //  Sort
            $listItem = $this->sort($request, $listItem);
            $listItem = $listItem->paginate(20)->appends($request->all());

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {
            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'land_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }

            $item = Organization::leftJoin('seasons', 'lands.id', '=', 'seasons.land_id')
                ->leftJoin('districts', 'districts.id', '=', 'lands.district_id')
                ->leftJoin('provinces', 'provinces.id', '=', 'lands.province_id')
                ->leftJoin('wards', 'wards.id', '=', 'lands.ward_id')
                ->leftJoin('admin', 'admin.id', '=', 'lands.admin_id')
                ->selectRaw('lands.*, seasons.id as season_id, seasons.tree_name as season_tree_name, 
                seasons.start_date as season_start_date, seasons.expected_date as season_expected_date, seasons.quantity_expected as seasons_quantity_expected,
                districts.name as district_name, provinces.name as province_name, wards.name as ward_name,
                admin.id as admin_id, admin.name as admin_name')->where('lands.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }
            $item->image = asset('public/filemanager/userfiles/' . $item->image);

            $item->season = [
                'id' => $item->season_id,
                'tree_name' => $item->season_tree_name,
                'start_date' => $item->season_start_date,
                'expected_date' => $item->season_expected_date,
                'quantity_expected' => $item->season_quantity_expected,
            ];
            unset($item->season_id);
            unset($item->season_tree_name);
            unset($item->season_start_date);
            unset($item->season_expected_date);
            unset($item->season_quantity_expected);

            $item->admin = [
                'id' => $item->admin_id,
                'name' => $item->admin_name,
            ];
            unset($item->admin_id);
            unset($item->admin_name);

            $item->type_owneds = @$this->type_owneds[$item->type_owneds];

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert

            $data['admin_id'] = \Auth::guard('api')->id();
            if ($request->has('image')) {
                $data['image'] = CommonHelper::saveFile($request->file('image'), 'land');
                if ($request->has('land_parameters')) {
                    $data['land_parameters'] = json_encode($request->land_parameters);
                }
                if ($request->has('water_parameters')) {
                    $data['water_parameters'] = json_encode($request->water_parameters);
                }
            }

            $item = Organization::create($data);

            return $this->show($item->id);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $item = Organization::find($id);
            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Validate errors',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert

            foreach ($data as $k => $v) {
                $item->{$k} = $v;
            }
            $item->save();

            return $this->show($item->id);
        }
    }

    public function delete($id) {
        Organization::find($id)->delete();
        return response()->json(null, 204);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
