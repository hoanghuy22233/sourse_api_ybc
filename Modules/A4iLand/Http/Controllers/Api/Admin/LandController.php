<?php

namespace Modules\A4iLand\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\A4iAdmin\Models\Admin;
use Modules\A4iLand\Models\Land;
use Illuminate\Http\Request;
use Validator;

class LandController extends Controller
{

    protected $module = [
        'code' => 'land',
        'table_name' => 'lands',
        'label' => 'Mảnh đất',
        'modal' => 'Modules\A4iLand\Models\Land',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'admin_name' => [
            'query_type' => 'custom'
        ],
        'province_id' => [
            'query_type' => '='
        ],
        'district_id' => [
            'query_type' => '='
        ],
        'ward_id' => [
            'query_type' => '='
        ],
    ];

    protected $type_owneds = [
        1 => 'Đất sử hữu',
        2 => 'Đất thuê',
        3 => 'Đất mượn',
        4 => 'Khác',
    ];

    public function index(Request $request)
    {
        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = land::leftJoin('seasons', 'lands.id', '=', 'seasons.land_id')
                ->leftJoin('admin', 'lands.admin_id', '=', 'admin.id')
                ->selectRaw('lands.id, lands.link_gg_map, lands.name, lands.image, lands.image, lands.lat, lands.long, lands.province_id, lands.district_id, lands.ward_id, seasons.id as season_id, seasons.tree_name as season_tree_name, 
                seasons.start_date as season_start_date, seasons.expected_date as season_expected_date, seasons.quantity_expected as season_quantity_expected, admin.id as admin_id,admin.name as admin_name')
                ->whereRaw($where)->groupBy('lands.id');

            //  Sort
            $listItem = $this->sort($request, $listItem);
            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());

            foreach ($listItem as $k => $item) {
                $item->season = [
                    'id' => $item->season_id,
                    'tree_name' => $item->season_tree_name,
                    'start_date' => $item->season_start_date,
                    'expected_date' => $item->season_expected_date,
                    'quantity_expected' => $item->season_quantity_expected,
                ];
                $item->image = asset('public/filemanager/userfiles/' . $item->image);
                $item->province = @$item->province->name;
                $item->district = @$item->district->name;
                $item->ward = @$item->ward->name;
                $item->admin = [
                    'id' => @$item->admin_id,
                    'name' => @$item->admin_name,
                ];
                if ($item->lat != null && $item->long != null) {
                    $item->link_gg_map = 'https://www.google.com/maps?q=' . $item->lat . ',' . $item->long;
                }
                unset($item->lat);
                unset($item->long);
                unset($item->province_id);
                unset($item->district_id);
                unset($item->ward_id);
                unset($item->season_id);
                unset($item->season_tree_name);
                unset($item->season_start_date);
                unset($item->season_expected_date);
                unset($item->season_quantity_expected);
                unset($item->admin_id);
                unset($item->admin_name);

            }
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {
            //  Check permission

            $item = land::leftJoin('seasons', 'lands.id', '=', 'seasons.land_id')
                ->leftJoin('districts', 'districts.id', '=', 'lands.district_id')
                ->leftJoin('provinces', 'provinces.id', '=', 'lands.province_id')
                ->leftJoin('wards', 'wards.id', '=', 'lands.ward_id')
                ->leftJoin('admin', 'admin.id', '=', 'lands.admin_id')
                ->selectRaw('lands.*, seasons.id as season_id, seasons.tree_name as season_tree_name, 
                seasons.start_date as season_start_date, seasons.expected_date as season_expected_date, seasons.quantity_expected as seasons_quantity_expected,
                districts.name as district_name, provinces.name as province_name, wards.name as ward_name,
                admin.id as admin_id, admin.name as admin_name')->where('lands.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $item->image = asset('public/filemanager/userfiles/' . $item->image);
            foreach (explode('|', $item->image_extra) as $img) {
                if ($img != '') {
                    $image_extra[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->image_extra = @$image_extra;

            foreach (explode('|', $item->land_parameters_image) as $img) {
                if ($img != '') {
                    $land_parameters_image[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->land_parameters_image = @$land_parameters_image;

            foreach (explode('|', $item->water_parameters_image) as $img) {
                if ($img != '') {
                    $water_parameters_image[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->water_parameters_image = @$water_parameters_image;

            $item->season = [
                'id' => $item->season_id,
                'tree_name' => $item->season_tree_name,
                'start_date' => $item->season_start_date,
                'expected_date' => $item->season_expected_date,
                'quantity_expected' => $item->season_quantity_expected,
            ];
            if ($item->lat != null && $item->long != null) {
                $item->link_gg_map = 'https://www.google.com/maps?q=' . $item->lat . ',' . $item->long;
            }
            unset($item->lat);
            unset($item->long);
            unset($item->season_id);
            unset($item->season_tree_name);
            unset($item->season_start_date);
            unset($item->season_expected_date);
            unset($item->seasons_quantity_expected);

            $item->admin = [
                'id' => $item->admin_id,
                'name' => $item->admin_name,
            ];
            unset($item->admin_id);
            unset($item->admin_name);

            $item->type_owneds = @$this->type_owneds[$item->type_owneds];
            $item->province = @$item->province->name;
            $item->district = @$item->district->name;
            $item->ward = @$item->ward->name;
            unset($item->province_id);
            unset($item->district_id);
            unset($item->ward_id);
            unset($item->district_name);
            unset($item->province_name);
            unset($item->ward_name);
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();

            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($image, 'land');
                    }
                } else {
                    $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($request->file('image'), 'land');
                }
                $data['image_extra'] = implode('|', $data['image_extra']);
            }

            if ($request->has('land_parameters_image')) {
                if (is_array($request->file('land_parameters_image'))) {
                    foreach ($request->file('land_parameters_image') as $land_parameters_image) {
                        $land_parameters_image_arr[] = CommonHelper::saveFile($land_parameters_image, 'land');
                    }
                    $data['land_parameters_image'] = @implode('|', $land_parameters_image_arr);
                } else {
                    $data['land_parameters_image'] = CommonHelper::saveFile($request->file('land_parameters_image'), 'land');
                }
            }

            if ($request->has('water_parameters_image')) {
                if (is_array($request->file('water_parameters_image'))) {
                    foreach ($request->file('water_parameters_image') as $water_parameters_image) {
                        $water_parameters_image_arr[] = CommonHelper::saveFile($water_parameters_image, 'land');
                    }
                    $data['water_parameters_image'] = @implode('|', $water_parameters_image_arr);
                } else {
                    $data['water_parameters_image'] = CommonHelper::saveFile($request->file('water_parameters_image'), 'land');
                }
            }

            if ($request->has('land_parameters')) {
                $x = str_replace('\"', '"', $request->land_parameters);
                $x = str_replace('"{', '{', $x);
                $x = str_replace('}"', '}', $x);
                $data['land_parameters'] = $x;
            }
            if ($request->has('water_parameters')) {
                $x = str_replace('\"', '"', $request->water_parameters);
                $x = str_replace('"{', '{', $x);
                $x = str_replace('}"', '}', $x);
                $data['water_parameters'] = $x;
            }

            $item = land::create($data);



            return $this->show($item->id);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $item = land::find($id);
            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Validate errors',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $data = $request->except('api_token');
            //  Tùy chỉnh dữ liệu insert
            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($image, 'land');
                    }
                } else {
                    $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($request->file('image'), 'land');
                }
                $data['image_extra'] = implode('|', $data['image_extra']);
            }

            if ($request->has('land_parameters_image')) {
                if (is_array($request->file('land_parameters_image'))) {
                    foreach ($request->file('land_parameters_image') as $land_parameters_image) {
                        $land_parameters_image_arr[] = CommonHelper::saveFile($land_parameters_image, 'land');
                    }
                    $data['land_parameters_image'] = @implode('|', $land_parameters_image_arr);
                } else {
                    $data['land_parameters_image'] = CommonHelper::saveFile($request->file('land_parameters_image'), 'land');
                }
            }

            if ($request->has('water_parameters_image')) {
                if (is_array($request->file('water_parameters_image'))) {
                    foreach ($request->file('water_parameters_image') as $water_parameters_image) {
                        $water_parameters_image_arr[] = CommonHelper::saveFile($water_parameters_image, 'land');
                    }
                    $data['water_parameters_image'] = @implode('|', $water_parameters_image_arr);
                } else {
                    $data['water_parameters_image'] = CommonHelper::saveFile($request->file('water_parameters_image'), 'land');
                }
            }

            if ($request->has('land_parameters')) {
                $x = str_replace('\"', '"', $request->land_parameters);
                $x = str_replace('"{', '{', $x);
                $x = str_replace('}"', '}', $x);
                $data['land_parameters'] = $x;
            }
            if ($request->has('water_parameters')) {
                $x = str_replace('\"', '"', $request->water_parameters);
                $x = str_replace('"{', '{', $x);
                $x = str_replace('}"', '}', $x);
                $data['water_parameters'] = $x;
            }

            foreach ($data as $k => $v) {
                $item->{$k} = $v;
            }
            $item->save();

            return $this->show($item->id);
        }
    }

    public function delete($id)
    {
        if (land::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . $this->module['table_name'] .  '.id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
                elseif ($filter_option['query_type'] == 'custom'){
                    $admin = Admin::where('name','like','%'.$request->get($filter_name).'%')->pluck('id')->toArray();
                    if (!empty($admin)) {
                        $where .= " AND lands.admin_id IN (".implode(',',$admin).")";
                    }
                }


            }
        }
//        dd($where);
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
