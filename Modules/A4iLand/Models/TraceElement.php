<?php

namespace Modules\A4iLand\Models;

use Illuminate\Database\Eloquent\Model;

class TraceElement extends Model
{
    protected $table = 'trace_elements';
    protected $fillable = [
        'name', 'description'
    ];
}
