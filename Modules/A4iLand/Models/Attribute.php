<?php



namespace Modules\A4iLand\Models;


use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $table = 'attributes';

    public $timestamps = false;

    protected $fillable = ['key', 'value', 'value2', 'table', 'type', 'item_id'];

    public function childs()
    {
        return $this->hasMany($this, 'parent_id', 'id')->orderBy('id', 'asc');
    }
}
