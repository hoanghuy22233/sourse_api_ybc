<?php

namespace Modules\OncodeCourse\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\ThemeEdu\Models\Student;

class Misson extends Model
{

    protected $table = 'misson';

    protected $guarded = [];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }

}
