<?php

namespace Modules\OncodeCourse\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\ThemeEdu\Models\QuizLog;

class Quizzes extends Model
{

    protected $table = 'quizzes';

    protected $guarded = [];
    public $timestamps = false;
//    protected $fillable = [
//        'name', 'image','content','time'
//    ];
    public function student() {
        return $this->belongsTo(QuizLog::class, 'quizlog_id');
    }
    public function lesson()
    {
        return $this->belongsTo(Lesson::class, 'lesson_id');
    }

}
