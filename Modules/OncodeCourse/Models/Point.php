<?php

namespace Modules\OncodeCourse\Models;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{

    protected $table = 'points';

    protected $guarded = [];
    public $timestamps = false;
//    protected $fillable = [
//        'name', 'lesson_id','content'
//    ];

    public function class()
    {
        return $this->belongsTo(Classs::class, 'class_id','id');
    }
    public function topic()
    {
        return $this->hasMany(Topic::class, 'lesson_item_id', 'id')->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }

}
