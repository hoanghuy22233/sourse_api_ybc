<?php

namespace Modules\OncodeCourse\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\OncodeCourse\Models\Classs;
use Modules\ThemeEdu\Models\Contact;
use Modules\ThemeEdu\Models\Student;

class MaketingMail extends Model
{
    protected $table = 'marketing_mail';
    protected $guarded = [];
//    protected $fillable = [
//        'admin_id','category_id',
//    ];

    public function classs()
    {
        return $this->belongsTo(Classs::class, 'class_id');
    }
    public function student()
    {
        return $this->hasMany(Student::class, 'student_ids','id');
    }

}
