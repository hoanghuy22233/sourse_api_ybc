<?php

namespace Modules\OncodeCourse\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{

    protected $table = 'topics';
    public $timestamps = false;
    protected $guarded = [];

    protected $fillable = [
        'name', 'lesson_item_id','content'
    ];

    public function lessonitem()
    {
        return $this->belongsTo(LessonItem::class, 'lesson_item_id');
    }
    public function recording()
    {
        return $this->hasMany(Recording::class, 'topic_id', 'id')->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }
//    public function recording()
//    {
//        return $this->belongsTo(Recording::class, 'id');
//    }

}
