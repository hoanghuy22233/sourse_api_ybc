<?php

namespace Modules\OncodeCourse\Models;

use Illuminate\Database\Eloquent\Model;

class Recording extends Model
{

    protected $table = 'recordings';
    public $timestamps = false;
    protected $guarded = [];

//    protected $fillable = [
//        'name', 'topic_id','content','file_audio','file_audio_name','file_audio_intro'
//    ];

    public function topic()
    {
        return $this->belongsTo(Topic::class, 'topic_id');
    }


}
