<?php

namespace Modules\OncodeCourse\Models;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{

    protected $table = 'lessons';

    protected $guarded = [];
    public $timestamps = false;
//    protected $fillable = [
//        'name', 'course_id'
//    ];

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
    public function lessonitem()
    {
        return $this->hasMany(LessonItem::class, 'lesson_id', 'id')->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }
    public function quiz()
    {
        return $this->hasMany(Quizzes::class, 'lesson_id', 'id')->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }
}
