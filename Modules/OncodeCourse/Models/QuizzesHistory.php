<?php

namespace Modules\OncodeCourse\Models;

use Illuminate\Database\Eloquent\Model;

class QuizzesHistory extends Model
{

    protected $table = 'quiz_log';

    protected $guarded = [];

    protected $fillable = [
        'scores', 'total_scores','answer_data','created_at	','updated_at','start_at','admin_id','quizzes_id'
    ];

    public function quizzes()
    {
        return $this->belongsTo(Lesson::class, 'quizzes_id');
    }
    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }


}
