@if (CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data'))
<label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
        <span class="color_btd">*</span>@endif</label>
<div class="col-xs-12">
    @include(config('core.admin_theme').".form.fields.select_model", ['field' => $field])
    <span class="form-text text-muted">{!! @$field['des'] !!}</span>
    <span class="text-danger">{{ $errors->first($field['name']) }}</span>
</div>
@endif