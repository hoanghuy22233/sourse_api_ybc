<?php

namespace Modules\OncodeCourse\Providers;

use App\Models\EmailTemplate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class OncodeCourseServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Cấu hình Core
//            $this->registerTranslations();
//            $this->registerConfig();
            $this->registerViews();
//            $this->registerFactories();
//            $this->loadMigrationsFrom(module_path('OncodeCourse', 'Database/Migrations'));


            //  Custom setting
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }

        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/setting') !== false) {
            $this->addSetting();
        }
    }

    public function addSetting()
    {
        \Eventy::addFilter('setting.custom_module', function ($module) {
            $module['tabs']['course_email'] = [
                'label' => 'Cấu hình email khóa học',
                'icon' => '<i class="flaticon-mail"></i>',
                'td' => [
                    ['name' => 'bill_add_send_admin', 'type' => 'select2_ajax_model', 'label' => 'Template email gửi admin khi có đơn hàng mới', 'object' => 'email_template', 'model' => EmailTemplate::class,
                        'display_field' => 'name'],
                    ['name' => 'bill_add_send_customer', 'type' => 'select2_ajax_model', 'label' => 'Template email hóa đơn gửi khách khi mua hàng', 'object' => 'email_template', 'model' => EmailTemplate::class,
                        'display_field' => 'name'],
                    ['name' => 'inner', 'type' => 'inner', 'label' => '', 'html' => '<a href="/admin/email_template" target="_blank">Xem danh sách template email</a>',],
                ]
            ];
            return $module;
        }, 1, 1);
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['course_view', 'course_add', 'course_edit', 'course_delete',
                'quizzes_view', 'quizzes_add', 'quizzes_edit', 'quizzes_delete','misson_view', 'misson_add', 'misson_edit', 'misson_delete',
                'point_view', 'point_add', 'point_edit', 'point_delete','tag']);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('oncodecourse::partials.aside_menu.dashboard_after_course');
        }, 0, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('oncodecourse::partials.aside_menu.dashboard_after_bill');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('OncodeCourse', 'Config/config.php') => config_path('oncodecourse.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('OncodeCourse', 'Config/config.php'), 'oncodecourse'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/oncodecourse');

        $sourcePath = module_path('OncodeCourse', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/oncodecourse';
        }, \Config::get('view.paths')), [$sourcePath]), 'oncodecourse');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/oncodecourse');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'oncodecourse');
        } else {
            $this->loadTranslationsFrom(module_path('OncodeCourse', 'Resources/lang'), 'oncodecourse');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('OncodeCourse', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
