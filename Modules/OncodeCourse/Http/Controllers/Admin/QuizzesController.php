<?php

namespace Modules\OncodeCourse\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\OncodeCourse\Models\Category;
use Modules\OncodeCourse\Models\Lesson;
use Modules\OncodeCourse\Models\QuizLog;
use Modules\OncodeCourse\Models\Quizzes;
use Modules\OncodeCourse\Models\Student;
use Modules\ThemeEduAdmin\Models\Menu;
use Validator;

class QuizzesController extends CURDBaseController
{
    protected $orderByRaw = 'order_no desc, id asc';
    protected $module = [
        'code' => 'quizzes',
        'table_name' => 'quizzes',
        'label' => 'Bài kiểm tra',
        'modal' => '\Modules\OncodeCourse\Models\Quizzes',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên '],
//            ['name' => 'time', 'type' => 'text', 'label' => 'Thời gian làm bài'],
            ['name' => 'accumulated_points', 'type' => 'number', 'label' => 'Điểm tích lũy tối đa'],
            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'oncodecourse::list.td.multi_cat', 'label' => 'Danh mục', 'object' => 'category'],
            ['name' => 'lesson_id', 'type' => 'relation', 'label' => 'Danh mục bài học', 'object' => 'lesson', 'display_field' => 'name'],
//            ['name' => 'topic_id', 'type' => 'text', 'label' => 'Danh mục nội dung tiết học'],
//            ['name' => 'code', 'type' => 'text', 'label' => 'Mã'],
//            ['name' => 'final_price', 'type' => 'price_vi', 'label' => 'Giá bán'],
//            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'oncodecourse::list.td.multi_cat', 'label' => 'Danh mục', 'object' => 'category_product'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên '],
                ['name' => 'multi_cat', 'type' => 'custom', 'field' => 'oncodecourse::form.fields.multi_cat', 'label' => 'Danh mục', 'model' => Category::class,
                    'object' => 'category', 'display_field' => 'name', 'multiple' => true,'where' => 'type=4','des' => 'Danh mục đầu tiên chọn là danh mục chính'],
//                ['name' => 'time', 'type' => 'text', 'class' => 'required', 'label' => 'Thời gian làm bài'],
                ['name' => 'lesson_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Danh mục bài học', 'model' => Lesson::class, 'display_field' => 'name',],
//                ['name' => 'code', 'type' => 'text', 'label' => 'Mã sản phẩm', 'group_class' => 'col-md-4'],
//                ['name' => 'base_price', 'type' => 'text', 'label' => 'Giá ban đầu', 'class' => 'number_price', 'group_class' => 'col-md-4'],
//                ['name' => 'final_price', 'type' => 'text', 'label' => 'Giá bán', 'class' => 'number_price', 'group_class' => 'col-md-4'],
//                ['name' => 'multi_cat', 'type' => 'custom', 'field' => 'oncodecourse::form.fields.multi_cat', 'label' => 'Danh mục sản phẩm', 'model' => \Modules\OncodeCourse\Models\Category::class,
//                    'object' => 'category_product', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=5', 'des' => 'Danh mục đầu tiên chọn là danh mục chính'],
//                ['name' => 'tags', 'type' => 'custom', 'field' => 'oncodecourse::form.fields.tags', 'label' => 'Từ khóa sản phẩm', 'model' => \Modules\OncodeCourse\Models\Category::class,
//                    'object' => 'tag_product', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=6'],
                ['name' => 'intro', 'type' => 'textarea', 'label' => 'Mô tả ngắn'],
                ['name' => 'accumulated_points', 'type' => 'number', 'label' => 'Điểm tích lũy tối đa'],
//                ['name' => 'content', 'type' => 'textarea_editor', 'label' => 'Nội dung'],
                ['name' => 'link_google_form', 'type' => 'text', 'label' => 'Link bài kiểm tra'],
                ['name' => 'link_google_form_answer', 'type' => 'text', 'label' => 'Link file excel danh sách phản hồi', 'des' => 'Link của file google excel chứa danh sách các bài kết quả của học viên'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'Thứ tự ưu tiên', 'value' => 0, 'group_class' => 'col-md-3', 'des' => 'Số to ưu tiên hiển thị trước'],
//                ['name' => 'file_audio_name', 'type' => 'text', 'label' => 'Tên bài nghe'],
//                ['name' => 'file_audio_intro', 'type' => 'text',  'label' => 'Thông tin bài nghe'],
//                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt', 'value' => 1, 'group_class' => 'col-md-6'],
//                ['name' => 'order_no', 'type' => 'number', 'label' => 'Thứ tự ưu tiên', 'value' => 0, 'group_class' => 'col-md-6', 'des' => 'Số to ưu tiên hiển thị trước'],
            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh'],
//                ['name' => 'image_extra', 'type' => 'multiple_image', 'count' => '6', 'label' => 'Ảnh khác'],
            ],

            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'Slug', 'des' => 'Đường dẫn sản phẩm trên thanh địa chỉ'],
                ['name' => 'meta_title', 'type' => 'text', 'label' => 'Meta title'],
                ['name' => 'meta_description', 'type' => 'text', 'label' => 'Meta description'],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên ',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'time' => [
            'label' => 'Thời gian làm bài',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'category_id' => [
            'label' => 'Danh mục',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'category',
            'model' => \Modules\OncodeCourse\Models\Category::class,
            'where' => 'type=4',
            'query_type' => 'custom'
        ],
//        'final_price' => [
//            'label' => 'Giá bán',
//            'type' => 'number',
//            'query_type' => 'like'
//        ],
//        'topic_id' => [
//            'label' => 'Danh mục nội dung tiết học',
//            'type' => 'select2_ajax_model',
//            'display_field' => 'name',
//            'object' => 'category_product',
//            'model' => \Modules\OncodeCourse\Models\Topic::class,
//            'query_type' => 'custom'
//        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('oncodecourse::quizzes.list')->with($data);
    }
    public function appendWhere($query, $request)
    {
        $query = $query->where('course_id', $request->course_id);
        return $query;
    }

//    public function appendWhere($query, $request)
//    {
//        //  Lấy các sản phẩm trong kho
//        $query = $query->where('company_id', null);
//
//        //  Lọc theo danh mục
//        if (!is_null($request->get('category_id'))) {
//            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
//        }
//
//        return $query;
//    }

    public function add(Request $request)
    {

        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('oncodecourse::quizzes.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    $stt1=trim(strstr($request->link_google_form,'d/'),'/edit');
                    $stt2=explode('/edit',strstr($request->link_google_form_answer,'d/'));

                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('link_google_form')) {
                        $data['google_form_id'] = $stt1;
                    }
                    if ($request->has('link_google_form_answer')) {
                        $data['google_form_answer_id'] = trim($stt2[0],'d/');
                    }
                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;


                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
//                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            //  Update điểm thi của các học viên
            $this->updateScores($item);

            $data = $this->getDataUpdate($request, $item);
            return view('oncodecourse::quizzes.edit')->with($data);
        } else if ($_POST) {
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ], [
                'name.required' => 'Bắt buộc phải nhập tên',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                $stt1=trim(strstr($request->link_google_form,'d/'),'/edit');
                $stt2=explode('/edit',strstr($request->link_google_form_answer,'d/'));
                //  Tùy chỉnh dữ liệu insert
                if ($request->has('multi_cat')) {
                    $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                    $data['category_id'] = $request->multi_cat[0];
                }
                if ($request->has('link_google_form')) {
                    $data['google_form_id'] = $stt1;
                }
                if ($request->has('link_google_form_answer')) {
                    $data['google_form_answer_id'] = trim($stt2[0],'d/');
                }
                #

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::flushCache($this->module['table_name']);
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function getToMyCompany(Request $request, $id)
    {
        $product_new = $this->duplicate($request, $id);
        $order = \Modules\JdesSetting\Models\Editor::where('product_ids', 'like', '%|'.$id.'|%')->first();
        $order->product_ids = $order->product_ids . $product_new->id . '|';
        $order->save();
        CommonHelper::one_time_message('success', 'Đã lấy sản phẩm về kho. Sản phẩm mới đã được tạo.');
        return redirect('/admin/product/' . $product_new->id);
    }

    public function duplicate(Request $request, $id)
    {
        $poduct = Lesson::find($id);
        $poduct_new = $poduct->replicate();
        $poduct_new->slug = $poduct->slug . $poduct_new->company_id;
        $poduct_new->company_id = \Auth::guard('admin')->user()->last_company_id;
        $poduct_new->admin_id = \Auth::guard('admin')->user()->id;
        $poduct_new->save();
        return $poduct_new;
    }

    /*
     * Update điểm số từ gg biểu mẫu về server & cộng điểm tích luy cho student
     * */
    public function updateScores($quizz)
    {
        try {
            if ($quizz->google_form_answer_id != null) {
                //  Gọi sang gg driver lấy data file bảng điểm
                $service = \Storage::cloud()->getAdapter()->getService();
                $mimeType = 'text/csv';

                $export = $service->files->export($quizz->google_form_answer_id, $mimeType);
                $response = response($export->getBody(), 200, $export->getHeaders());
                $data = preg_split("/\\r\\n|\\r|\\n/", $response->getContent());
                $title = explode(',', $data[0]);
                $key_mail_position = array_keys($title, 'Địa chỉ email')[0];
                $key_scores_position = array_keys($title, 'Điểm số')[0];

                foreach ($data as $k => $row) {
                    if ($k != 0) {
                        //  Lấy thông tin bài kiểm tra trên GG excel
                        $vals = explode(',', $row);
                        $email = $vals[$key_mail_position];
                        $student = Student::where('email', $email)->first();
                        if (is_object($student)) {
                            //  Tinh diem tich luy
                            $accumulated_points = (int)$quizz->accumulated_points;
                            $str = explode('/', $vals[$key_scores_position]);
                            if (isset($str[0]) && isset($str[1])) {
                                $ps = (int)$str[0] / (int)$str[1];
                                $accumulated_points = (int)($ps * $accumulated_points);
                            }

                            //  Cập nhật thông tin vào quizLog
                            QuizLog::updateOrCreate([
                                'student_id' => $student->id,
                                'quizz_id' => $quizz->id,
                                'email' => $email,
                            ], [
                                'scores' => $vals[$key_scores_position],
                                'data' => json_encode($vals),
                                'accumulated_points' => $accumulated_points,
                                'synch' => 1,
                            ]);
                        }
                    }
                }
            }
            CommonHelper::flushCache(['student', 'quizzes']);
            return $quizz;
        } catch (\Exception $ex) {
            dd($ex->getMessage());
            return false;
        }
    }
}
