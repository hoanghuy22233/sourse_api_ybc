<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
//
//    Route::group(['prefix' => 'dashboard'], function () {
//        Route::get('', 'Admin\DashboardController@dashboard');
////        Route::get('search', 'Admin\DashboardController@search');
//    });

    Route::group(['prefix' => 'course'], function () {
        Route::get('', 'Admin\CourseController@getIndex')->name('course');
        Route::get('publish', 'Admin\CourseController@getPublish')->name('course.publish')->middleware('permission:course_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\CourseController@add')->middleware('permission:course_add');
        Route::get('delete/{id}', 'Admin\CourseController@delete')->middleware('permission:course_delete');
        Route::post('multi-delete', 'Admin\CourseController@multiDelete')->middleware('permission:course_delete');
        Route::get('search-for-select2', 'Admin\CourseController@searchForSelect2')->name('course.search_for_select2')->middleware('permission:course_view');

        Route::get('{id}', 'Admin\CourseController@update')->middleware('permission:course_view');
        Route::post('{id}', 'Admin\CourseController@update')->middleware('permission:course_edit');
    });

    Route::group(['prefix' => 'lesson'], function () {
        Route::get('', 'Admin\LessonController@getIndex')->name('lesson')->middleware('permission:course_view');
        Route::get('publish', 'Admin\LessonController@getPublish')->name('lesson.publish')->middleware('permission:course_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\LessonController@add')->middleware('permission:course_add');
        Route::get('delete/{id}', 'Admin\LessonController@delete')->middleware('permission:course_delete');
        Route::post('multi-delete', 'Admin\LessonController@multiDelete')->middleware('permission:course_delete');
        Route::get('search-for-select2', 'Admin\LessonController@searchForSelect2')->name('lesson.search_for_select2')->middleware('permission:course_view');

        Route::get('{id}', 'Admin\LessonController@update')->middleware('permission:course_view');
        Route::post('{id}', 'Admin\LessonController@update')->middleware('permission:course_edit');
    });

    Route::group(['prefix' => 'category'], function () {
        Route::get('', 'Admin\CateCourseController@getIndex')->name('category')->middleware('permission:course_view');
        Route::get('publish', 'Admin\CateCourseController@getPublish')->name('category.publish')->middleware('permission:course_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\CateCourseController@add')->middleware('permission:course_add');
        Route::get('delete/{id}', 'Admin\CateCourseController@delete')->middleware('permission:course_delete');
        Route::post('multi-delete', 'Admin\CateCourseController@multiDelete')->middleware('permission:course_delete');
        Route::get('search-for-select2', 'Admin\CateCourseController@searchForSelect2')->name('category.search_for_select2')->middleware('permission:course_view');

        Route::get('{id}', 'Admin\CateCourseController@update')->middleware('permission:course_view');
        Route::post('{id}', 'Admin\CateCourseController@update')->middleware('permission:course_edit');
    });

    Route::group(['prefix' => 'lesson-item'], function () {
        Route::get('', 'Admin\LessonItemController@getIndex')->name('lesson.item')->middleware('permission:course_view');
        Route::get('publish', 'Admin\LessonItemController@getPublish')->name('lesson.item.publish')->middleware('permission:course_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\LessonItemController@add')->middleware('permission:course_add');
        Route::get('delete/{id}', 'Admin\LessonItemController@delete')->middleware('permission:course_delete');
        Route::post('multi-delete', 'Admin\LessonItemController@multiDelete')->middleware('permission:course_delete');
        Route::get('search-for-select2', 'Admin\LessonItemController@searchForSelect2')->name('lesson.item.search_for_select2')->middleware('permission:course_view');

        Route::get('{id}', 'Admin\LessonItemController@update')->middleware('permission:course_view');
        Route::post('{id}', 'Admin\LessonItemController@update')->middleware('permission:course_edit');
    });

    Route::group(['prefix' => 'topic'], function () {
        Route::get('', 'Admin\TopicController@getIndex')->name('topic')->middleware('permission:course_view');
        Route::get('publish', 'Admin\TopicController@getPublish')->name('topic.publish')->middleware('permission:course_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TopicController@add')->middleware('permission:course_add');
        Route::get('delete/{id}', 'Admin\TopicController@delete')->middleware('permission:course_delete');
        Route::post('multi-delete', 'Admin\TopicController@multiDelete')->middleware('permission:course_delete');
        Route::get('search-for-select2', 'Admin\TopicController@searchForSelect2')->name('topic.search_for_select2')->middleware('permission:course_view');

        Route::get('{id}', 'Admin\TopicController@update')->middleware('permission:course_view');
        Route::post('{id}', 'Admin\TopicController@update')->middleware('permission:course_edit');
    });

    Route::group(['prefix' => 'recording'], function () {
        Route::get('', 'Admin\RecordingController@getIndex')->name('recording')->middleware('permission:course_view');
        Route::get('publish', 'Admin\RecordingController@getPublish')->name('recording.publish')->middleware('permission:course_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\RecordingController@add')->middleware('permission:course_add');
        Route::get('delete/{id}', 'Admin\RecordingController@delete')->middleware('permission:course_delete');
        Route::post('multi-delete', 'Admin\RecordingController@multiDelete')->middleware('permission:course_delete');
        Route::get('search-for-select2', 'Admin\RecordingController@searchForSelect2')->name('recording.search_for_select2')->middleware('permission:course_view');

        Route::get('{id}', 'Admin\RecordingController@update')->middleware('permission:course_view');
        Route::post('{id}', 'Admin\RecordingController@update')->middleware('permission:course_edit');
    });

    Route::group(['prefix' => 'category_quizz'], function () {
        Route::get('', 'Admin\CateQuizzController@getIndex')->name('category-quizz')->middleware('permission:quizzes_view');
        Route::get('publish', 'Admin\CateQuizzController@getPublish')->name('category-quizz.publish')->middleware('permission:quizzes_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\CateQuizzController@add')->middleware('permission:quizzes_add');
        Route::get('delete/{id}', 'Admin\CateQuizzController@delete')->middleware('permission:quizzes_delete');
        Route::post('multi-delete', 'Admin\CateQuizzController@multiDelete')->middleware('permission:quizzes_delete');
        Route::get('search-for-select2', 'Admin\CateQuizzController@searchForSelect2')->name('category-quizz.search_for_select2')->middleware('permission:quizzes_view');

        Route::get('{id}', 'Admin\CateQuizzController@update')->middleware('permission:quizzes_view');
        Route::post('{id}', 'Admin\CateQuizzController@update')->middleware('permission:quizzes_edit');
    });

    Route::group(['prefix' => 'quizzes'], function () {
        Route::get('', 'Admin\QuizzesController@getIndex')->name('quizzes')->middleware('permission:quizzes_view');
        Route::get('publish', 'Admin\QuizzesController@getPublish')->name('quizzes.publish')->middleware('permission:quizzes_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\QuizzesController@add')->middleware('permission:quizzes_add');
        Route::get('delete/{id}', 'Admin\QuizzesController@delete')->middleware('permission:quizzes_delete');
        Route::post('multi-delete', 'Admin\QuizzesController@multiDelete')->middleware('permission:quizzes_delete');
        Route::get('search-for-select2', 'Admin\QuizzesController@searchForSelect2')->name('quizzes.search_for_select2')->middleware('permission:quizzes_view');

        Route::get('{id}', 'Admin\QuizzesController@update')->middleware('permission:quizzes_view');
        Route::post('{id}', 'Admin\QuizzesController@update')->middleware('permission:quizzes_edit');
    });

    Route::group(['prefix' => 'tag_course'], function () {
        Route::get('', 'Admin\TagCourseController@getIndex')->name('tag_course')->middleware('permission:tag');
        Route::get('publish', 'Admin\TagCourseController@getPublish')->name('tag_course.publish')->middleware('permission:tag');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TagCourseController@add')->middleware('permission:tag');
        Route::get('delete/{id}', 'Admin\TagCourseController@delete')->middleware('permission:tag');
        Route::post('multi-delete', 'Admin\TagCourseController@multiDelete')->middleware('permission:tag');
        Route::get('search-for-select2', 'Admin\TagCourseController@searchForSelect2')->name('tag_course.search_for_select2');
        Route::get('{id}', 'Admin\TagCourseController@update')->middleware('permission:tag');
        Route::post('{id}', 'Admin\TagCourseController@update')->middleware('permission:tag');
    });

    Route::group(['prefix' => 'point'], function () {
        Route::get('', 'Admin\PointController@getIndex')->name('quizzes')->middleware('permission:point_view');
        Route::get('publish', 'Admin\PointController@getPublish')->name('quizzes.publish')->middleware('permission:point_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\PointController@add')->middleware('permission:point_add');
        Route::get('delete/{id}', 'Admin\PointController@delete')->middleware('permission:point_delete');
        Route::post('multi-delete', 'Admin\PointController@multiDelete')->middleware('permission:point_delete');
        Route::get('search-for-select2', 'Admin\PointController@searchForSelect2')->name('quizzes.search_for_select2')->middleware('permission:point_view');

        Route::get('{id}', 'Admin\PointController@update')->middleware('permission:point_view');
        Route::post('{id}', 'Admin\PointController@update')->middleware('permission:point_edit');
    });


    Route::group(['prefix' => 'bill'], function () {
        Route::get('', 'Admin\BillController@getIndex')->name('bill');
        Route::get('publish', 'Admin\BillController@getPublish')->name('bill.publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BillController@add');
        Route::get('delete/{id}', 'Admin\BillController@delete');
        Route::post('multi-delete', 'Admin\BillController@multiDelete');
        Route::get('search-for-select2', 'Admin\BillController@searchForSelect2')->name('bill.search_for_select2');
        Route::post('active-order', 'Admin\BillController@activeOrder')->name('bill.active-order');
        Route::post('active-order-all', 'Admin\BillController@activeOrderAll')->name('bill.active-order-all');
        Route::get('{id}', 'Admin\BillController@update');
        Route::post('{id}', 'Admin\BillController@update');

    });


    Route::post('ajax-up-file', 'Admin\RecordingController@ajax_up_file')->name('ajax-up-file');

});
