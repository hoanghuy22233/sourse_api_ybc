<?php echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    <?php
    //    $page = str_replace('.xml', '', $page);
    //    $skip = ($page - 1) * 500;
    //    $take = 500;
    ?>
    {{--Sitemap danh muc--}}
    <?php
    $data = \Modules\STBDSitemap\Models\Post::select(['image', 'name', 'slug','multi_cat', 'category_id', 'updated_at'])->where('status', 1)->where('slug', '!=', '')->get();

    ?>
    @foreach($data as $item)
        <?php
        $slug = '';
        $catPost = \Modules\STBDSitemap\Models\Category::select(['slug', 'category_product_id'])->whereIn('id', explode('|', $item->multi_cat))->first();

        if (is_object($catPost)) {
            $catProduct = \Modules\STBDSitemap\Models\Category::select('slug')->where('id', $catPost->category_product_id)->first();
        }
        if (is_object($catPost)) {

            if (isset($catProduct) && is_object($catProduct)) {

//                $slug .= '/' . $catProduct->slug.'/tu-van';
                $slug .= '/' . $catProduct->slug;
            }else{
                $slug .= '/' . $catPost->slug;
            }
        }
        ?>
        <url>
            <loc>{{ URL::to($slug .'/'. $item->slug.'.html') }}</loc>
            <image>
                <loc>{{ URL::to(\Modules\STBDSitemap\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, 73, 107)) }}</loc>
                <caption>{{ $item->name }}</caption>
                <license>{{ URL::to('/') }}</license>
                <family_friendly>yes</family_friendly>
            </image>
            <lastmod>{{ date("Y-m-d", strtotime($item->updated_at))}}T{{ date("H:i:s", strtotime($item->updated_at))}}+07:00</lastmod>
            <changefreq>always</changefreq>
            <priority>0.4</priority>
        </url>
    @endforeach
</sitemapindex>