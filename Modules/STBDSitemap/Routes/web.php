<?php

Route::get('sitemap.xml', function () {
    return response()->view('stbdsitemap::sitemap')->header('Content-Type', 'text/xml');
});
Route::get('post-sitemap.xml', function () {
    return response()->view('stbdsitemap::post_sitemap')->header('Content-Type', 'text/xml');
});
Route::get('category-sitemap.xml', function () {
    return response()->view('stbdsitemap::category_sitemap')->header('Content-Type', 'text/xml');
});
Route::get('product-sitemap.xml', function () {
    return response()->view('stbdsitemap::product_sitemap')->header('Content-Type', 'text/xml');
});
//Route::get('post-sitemap.xml', function ($page) {
//    return response()->view('stbdsitemap::post_detail_sitemap', ['page' => $page])->header('Content-Type', 'text/xml');
//});
//Route::get('product-sitemap/{page}', function ($page) {
//    return response()->view('stbdsitemap::product_detail_sitemap', ['page' => $page])->header('Content-Type', 'text/xml');
//});