<?php

namespace Modules\ThemeRaoVat\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\EduSettings\Providers\RepositoryServiceProvider;
use App\Models\Setting;
use App\Http\Helpers\CommonHelper;
use View;

class ThemeRaoVatServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //  Cấu hình Core
//            $this->registerTranslations();
//            $this->registerConfig();
        $this->registerViews();
//            $this->registerFactories();
//            $this->loadMigrationsFrom(module_path('themeraovat', 'Database/Migrations'));
//            $this->app->register(RepositoryServiceProvider::class);

        //  Cầu hình frontend
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') === false) {
            $this->frontendSettings();
        }

        //  Cấu hình admin/setting
        /*if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/setting') !== false) {
            $this->customSetting();
        }*/
    }

    public function frontendSettings()
    {
        $settings = CommonHelper::getFromCache('frontend_settings', ['settings']);
        if (!$settings) {
            $settings = Setting::whereIn('type', ['general_tab', 'seo_tab', 'common_tab', 'homepage_tab', 'course_page_tab'])->pluck('value', 'name')->toArray();
            CommonHelper::putToCache('frontend_settings', $settings, ['settings']);
        }

        View::share('settings', $settings);
        return $settings;
    }

    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('ThemeRaoVat', 'Config/config.php') => config_path('themeraovat.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('ThemeRaoVat', 'Config/config.php'), 'themeraovat'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/themeraovat');

        $sourcePath = module_path('ThemeRaoVat', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/themeraovat';
        }, \Config::get('view.paths')), [$sourcePath]), 'themeraovat');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/themeraovat');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'themeraovat');
        } else {
            $this->loadTranslationsFrom(module_path('ThemeRaoVat', 'Resources/lang'), 'themeraovat');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('ThemeRaoVat', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
