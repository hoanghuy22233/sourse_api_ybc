@extends('themeraovat::layouts.default')
@section('main_content')
    <div id="body">
        <div id="main">
            <div class="container">
                @include('themeraovat::partials.top_search')
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                @include('themeraovat::partials.box_search')
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8">
                                <?php
                                $data = CommonHelper::getFromCache('posts_view_total_desc', ['posts']);
                                if (!$data) {
                                    $data = \Modules\ThemeRaoVat\Models\Post::select('multi_cat', 'id', 'name', 'category_id', 'intro', 'slug', 'image')
                                        ->where('status', 1)->orderBy('view_total', 'desc')->orderBy('order_no', 'desc')->orderBy('created_at', 'desc')->limit(6)->get();
                                    CommonHelper::putToCache('posts_view_total_desc', $data, ['posts']);
                                }
                                ?>
                                <div class="newsFocus">
                                    @foreach($data as $k => $item)
                                        @if($k == 0)
                                            <?php
                                            $cat = CommonHelper::getFromCache('categories_id' . $data[0]->id, ['categories', 'posts']);
                                            if (!$cat) {
                                                $cat = $data[0]->category;
                                                CommonHelper::putToCache('categories_id' . $data[0]->id, $cat, ['categories', 'posts']);
                                            }
                                            ?>
                                            <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $data[0]->slug : '/bai-viet/' . $data[0]->slug }}.html"
                                               title="{{ $data[0]->name }}"><img
                                                        src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($data[0]->image, 280, null) }}"
                                                        alt="{{ $data[0]->name }}"></a>
                                            <h2>
                                                <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $data[0]->slug : '/bai-viet/' . $data[0]->slug }}.html"
                                                   title="{{ $data[0]->name }}">{{ $data[0]->name }}</a></h2>
                                            <p>{{ $data[0]->intro }}</p>
                                            <?php
                                            unset($data[0]);
                                            ?>
                                        @endif
                                    @endforeach
                                </div>
                                <ul class="newsHot newsHot-list clearfix">
                                    @foreach($data as $k => $item)
                                        @if($k != 0)
                                            <?php
                                            $cat = CommonHelper::getFromCache('categories_id' . $item->id, ['categories', 'posts']);
                                            if (!$cat) {
                                                $cat = $item->category;
                                                CommonHelper::putToCache('categories_id' . $item->id, $cat, ['categories', 'posts']);
                                            }
                                            ?>
                                            <li>
                                                <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $item->slug : '/bai-viet/' . $item->slug }}.html"
                                                   title="{{ $item->name }}">{{ $item->name }}</a></li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="listItems">
                                            <h2 class="tl"><span><i class="fa fa-list-ul"></i> Tìm Đại lý phân phối, Nhà cung cấp nổi bật</span>
                                            </h2>

                                            <?php
                                            $posts = CommonHelper::getFromCache('posts_home_post_type1', ['posts']);
                                            if (!$posts) {
                                                $posts = \Modules\ThemeRaoVat\Models\Post::select('id', 'image', 'name', 'category_id', 'category_child_id', 'multi_cat', 'slug', 'yeu_cau_tai_chinh', 'address', 'province_id', 'created_at')
                                                    ->where('status', 1)->where('post_type', 1)->where('created_at', '>', date('Y-01-01 00:00:00'))->orderByRaw("RAND()")->orderBy('created_at', 'desc')->paginate(8);
                                                CommonHelper::putToCache('posts_home_post_type1', $posts, ['posts']);
                                            }
                                            ?>

                                            @foreach($posts as $item)
                                                <?php
                                                $cat = CommonHelper::getFromCache('categories_id' . $item->id, ['categories', 'posts']);
                                                if (!$cat) {
                                                    $cat = $item->category;
                                                    CommonHelper::putToCache('categories_id' . $item->id, $cat, ['categories', 'posts']);
                                                }
                                                ?>
                                                <div class="col-sm-6 block-item">
                                                    <div class="item">
                                                        <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $item->slug : '/bai-viet/' . $item->slug }}.html"
                                                           title="{{ $item->name }}"><img
                                                                    data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, 160, null) }}"
                                                                    class="lazy"
                                                                    alt="{{ $item->name }}"></a>
                                                        <div>
                                                            <h3>
                                                                <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $item->slug : '/bai-viet/' . $item->slug }}.html"
                                                                   title="{{ $item->name }}"
                                                                   class="rnvip5">
                                                                    {{ $item->name }}</a></h3>
                                                            <ul class="info">
                                                                <li><i class="fa fa-map-marker"></i>Khu vực ưu tiên
                                                                    : {{ $item->address }}
                                                                    , {{ @$item->province->name }}
                                                                </li>
                                                                <li><i class="fa fa-tag"></i>Yêu cầu tài chính : <span
                                                                            class="red">{{ $item->yeu_cau_tai_chinh }}</span>
                                                                </li>
                                                                <li><i class="fa fa-building-o"></i>Ngành hàng
                                                                    : {{ @$item->category_child->name }}<span
                                                                            class="pull-right">{{ date('d-m-Y', strtotime($item->created_at)) }}</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="rnajaxvipdata"></div>
                                                </div>
                                            </div>

                                            <nav aria-label="Page navigation" id="pagination">
                                                <span class="pull-left small">Xem thêm các tin tìm đại lý, nhà phân phối tương tự ?</span>

                                                {{--{{ @$posts->appends(Request::all())->links() }}--}}
                                                <a href="/tim-nha-phan-phoi"
                                                   class="small pull-right">Xem tất cả <i class="fa fa-angle-right"></i></a>
                                                <div class="clearfix"></div>
                                            </nav>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="listItems">
                                            <h2 class="tl"><span><i class="fa fa-list-ul"></i> Tìm Đại lý phân phối, Nhà cung cấp mới nhất</span>
                                            </h2>
                                            <?php
                                            $posts = CommonHelper::getFromCache('posts_home_post_new', ['posts']);
                                            if (!$posts) {
                                                $posts = \Modules\ThemeRaoVat\Models\Post::select('id', 'image', 'name', 'category_id', 'category_child_id', 'multi_cat', 'slug', 'yeu_cau_tai_chinh', 'address', 'province_id', 'created_at')
                                                    ->where('status', 1)->where('post_type', 1)->orderBy('created_at', 'desc')->paginate(8);
                                                CommonHelper::putToCache('posts_home_post_new', $posts, ['posts']);
                                            }
                                            ?>
                                            @foreach($posts as $item)
                                                <?php
                                                $cat = CommonHelper::getFromCache('categories_id' . $item->id, ['categories', 'posts']);
                                                if (!$cat) {
                                                    $cat = $item->category;
                                                    CommonHelper::putToCache('categories_id' . $item->id, $cat, ['categories', 'posts']);
                                                }
                                                ?>
                                                <div class="col-sm-6 block-item">
                                                    <div class="item">
                                                        <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $item->slug : '/bai-viet/' . $item->slug }}.html"
                                                           title="{{ $item->name }}"><img
                                                                    data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, 160, null) }}"
                                                                    class="lazy"
                                                                    alt="{{ $item->name }}"></a>
                                                        <div>
                                                            <h3>
                                                                <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $item->slug : '/bai-viet/' . $item->slug }}.html"
                                                                   title="{{ $item->name }}"
                                                                   class="rnvip5">
                                                                    {{ $item->name }}</a></h3>
                                                            <ul class="info">
                                                                <li><i class="fa fa-map-marker"></i>Khu vực ưu tiên
                                                                    : {{ $item->address }}
                                                                    , {{ @$item->province->name }}
                                                                </li>
                                                                <li><i class="fa fa-tag"></i>Yêu cầu tài chính : <span
                                                                            class="red">{{ $item->yeu_cau_tai_chinh }}</span>
                                                                </li>
                                                                <li><i class="fa fa-building-o"></i>Ngành hàng
                                                                    : {{ @$item->category_child->name }}<span
                                                                            class="pull-right">{{ date('d-m-Y', strtotime($item->created_at)) }}</span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                            <nav aria-label="Page navigation" id="pagination">
                                                <span class="pull-left small">Xem thêm các tin tìm đại lý, nhà phân phối tương tự ?</span>

                                                {{--{{ @$posts->appends(Request::all())->links() }}--}}
                                                <a href="/tim-nha-phan-phoi"
                                                   class="small pull-right">Xem tất cả <i class="fa fa-angle-right"></i></a>
                                                <div class="clearfix"></div>
                                            </nav>
                                        </div>

                                    </div>
                                </div>
                                <div class="adsMiddle text-center">

                                    <div class="banner mgb10" id="BannerLeft3">

                                    </div>

                                </div>

                                {{--                                @include('themeraovat::pages.home.partials.thi_truong_xi_hnay')--}}

                            </div>
                        </div>

                        {{--Sản phẩm bán buôn bán lẻ--}}

                        <div class="row">
                            <div class="col-md-12">
                                <div id="rnshophomedata">
                                    <div id="prodGrid" class="section">
                                        <div class="listItems">
                                            <h2 class="tl"><a href="/ban-si" style="color:#fff !important"><span><i
                                                                class="fa fa-list-ul"></i> Sản phẩm bán buôn, bán sỉ</span></a>
                                            </h2>
                                        </div>
                                        <div class="cleafix"></div>
                                        <div class="row mgt10 rnajaxshop">
                                            <?php
                                            $product_new = CommonHelper::getFromCache('products_news', ['products']);
                                            if (!$product_new) {
                                                $product_new = \Modules\ThemeRaoVat\Models\Product::select('id', 'image', 'name', 'price_intro', 'category_id', 'slug', 'multi_cat', 'category_child_id')
                                                    ->where('status', 1)->orderBy('created_at', 'desc')->limit(12)->get();
                                                CommonHelper::putToCache('products_news', $product_new, ['products']);
                                            }
                                            ?>
                                            @foreach( $product_new as $pn)
                                                <?php
                                                $cat = CommonHelper::getFromCache('categories_id' . $pn->id, ['categories', 'posts']);
                                                if (!$cat) {
                                                    $cat = $pn->category;
                                                    CommonHelper::putToCache('categories_id' . $pn->id, $cat, ['categories', 'posts']);
                                                }
                                                ?>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 rnsh">
                                                    <div class="item prodItem">
                                                        <div class="thumb">
                                                            <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $pn->slug : '/bai-viet/' . $pn->slug }}.html"
                                                               title="{{$pn->name}}">
                                                                <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($pn->image) }}"
                                                                     alt="{{$pn->name}}"></a>
                                                        </div>

                                                        <div class="info">
                                                            <h3>
                                                                <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $pn->slug : '/bai-viet/' . $pn->slug }}.html"
                                                                   title="{{$pn->name}}">
                                                                    {{$pn->name}}</a></h3>
                                                            <div class="price">
                                                                <div class="pull-left">
                                                                    <span>{!! !empty($pn->price_intro) ? @$pn->price_intro.'<sup>đ</sup>' : 'Giá liên hệ' !!}</span>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                            <div class="prodItem-bt">
                                                                <div class="pull-left"></i>
                                                                </div>
                                                                <div class="pull-right">


                                                                </div>

                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <a href="/ban-buon-ban-si"
                                               class="small pull-right">Xem tất cả <i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--end sản phẩm bán buôn bán lẻ--}}

                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $widget_home = CommonHelper::getFromCache('widgets_home_bottom', ['widgets']);
                                if (!$widget_home) {
                                    $widget_home = \Modules\ThemeRaoVat\Models\Widget::select(['name', 'content', 'config', 'type'])
                                        ->where('location', 'home_bottom')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')
                                        ->get();
                                    CommonHelper::putToCache('widgets_home_bottom', $widget_home, ['widgets']);
                                }
                                ?>
                                <div class="rnblock padb15">
                                    @foreach($widget_home as $w)
                                        {!! @$w->content !!}
                                    @endforeach
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <aside>

                            <?php
                            $categories = CommonHelper::getFromCache('categories_sidebar2' . @$settings['sidebar2_category_ids'], ['categories']);
                            if (!$categories) {
                                $categories = \Modules\ThemeRaoVat\Models\Category::select('name', 'id')->where('status', 1)->whereIn('id', explode('|', @$settings['sidebar2_category_ids']))->get();
                                CommonHelper::putToCache('categories_sidebar2' . @$settings['sidebar2_category_ids'], $categories, ['categories']);
                            }
                            ?>
                            @foreach($categories as $cat)
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="rndochomedata">
                                            <div class="widget">
                                                <div class="wg-hd">
                                                    <h3><i class="fa fa-list-ul"></i> {{ $cat->name }}</h3>
                                                </div>
                                                <div class="wg-ct p15">
                                                    <ul class="newsList">
                                                        <?php
                                                        $data = \Modules\ThemeRaoVat\Models\Post::where('status', 1)->where('multi_cat', 'like', '%|' . $cat->id . '|%')->orderBy('order_no', 'desc')->orderBy('created_at', 'desc')
                                                            ->limit(4)->get();
                                                        ?>
                                                        @foreach($data as $k => $v)
                                                            <?php
                                                            $cat = CommonHelper::getFromCache('categories_id' . $v->id, ['categories', 'posts']);
                                                            if (!$cat) {
                                                                $cat = $v->category;
                                                                CommonHelper::putToCache('categories_id' . $v->id, $cat, ['categories', 'posts']);
                                                            }
                                                            ?>
                                                            <li>
                                                                <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $v->slug : '/bai-viet/' . $v->slug }}.html"
                                                                   title="{{ $v->name }}">{{ $v->name }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                            <?php
                            $categories = CommonHelper::getFromCache('categories_sidebar1', ['categories']);
                            if (!$categories) {
                                $categories = \Modules\ThemeRaoVat\Models\Category::select('name', 'id')->where('status', 1)->whereIn('id', explode('|', @$settings['sidebar1_category_ids']))->get();
                                CommonHelper::putToCache('categories_sidebar1', $categories, ['categories']);
                            }
                            ?>
                            @foreach($categories as $cat)
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="rnnewsadvhomedata">
                                            <div class="widget">
                                                <div class="wg-hd">
                                                    <h3>
                                                        <i class="fa fa-list-ul"></i>
                                                        {{ $cat->name }}</h3>
                                                </div>
                                                <div class="wg-ct p15">
                                                    <ul class="newsList">
                                                        <?php
                                                        $data = CommonHelper::getFromCache('posts_multi_cat' . $cat->id, ['posts']);
                                                        if (!$data) {
                                                            $data = \Modules\ThemeRaoVat\Models\Post::where('status', 1)->where('multi_cat', 'like', '%|' . $cat->id . '|%')->orderBy('order_no', 'desc')->orderBy('created_at', 'desc')
                                                                ->limit(4)->get();
                                                            CommonHelper::putToCache('posts_multi_cat' . $cat->id, $data, ['posts']);
                                                        }
                                                        ?>
                                                        @foreach($data as $k => $v)
                                                            <?php
                                                            $cat = CommonHelper::getFromCache('categories_id' . $v->id, ['categories', 'posts']);
                                                            if (!$cat) {
                                                                $cat = $v->category;
                                                                CommonHelper::putToCache('categories_id' . $v->id, $cat, ['categories', 'posts']);
                                                            }
                                                            ?>
                                                            <li class="newsFocus"><a
                                                                        href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $v->slug : '/bai-viet/' . $v->slug }}.html"
                                                                        title="{{ $v->name }}">
                                                                    @if($k == 0)
                                                                        <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image, 230, null) }}">
                                                                    @endif
                                                                    {{ $v->name }}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            <div class="row">
                                <div class="col-md-12">
                                    <div id="rnumshophomedata">
                                        <div class="widget">
                                            <div class="wg-hd">
                                                <h3 style="background-color:#ff0000;border-bottom: 1px solid #ff0000">
                                                    <i class="fa fa-list-ul"></i> Shop bán sỉ mới nhất</h3>
                                            </div>
                                            <div class="wg-ct p15">
                                                <ul class="newsList">
                                                    <?php
                                                    $data = CommonHelper::getFromCache('admin_shop', ['admin']);
                                                    if (!$data) {
                                                        $admin_ids = \Modules\ThemeRaoVat\Models\Product::whereNotNull('admin_id')->groupBy('admin_id')->orderBy('admin_id', 'desc')->limit(12)->pluck('admin_id');
                                                        $data = \App\Models\Admin::select('id', 'name')->whereIn('id', $admin_ids)->orderBy('created_at', 'desc')->limit(12)->get();
                                                        CommonHelper::putToCache('admin_shop', $data, ['admin'], 86400);
                                                    }
                                                    ?>
                                                    @foreach($data as $v)
                                                        <li><a href="/tim-kiem?user_id={{ $v->id }}&post_type=3"
                                                               title="{{ $v->name }}">{{ $v->name }}</a></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{--                                @include('themeraovat::pages.home.partials.box_hoi_dap')--}}

                            <div class="advRight">
                                <div class="banner mgb10" id="BannerRight1">
                                    <div class="banner_item mgt5">
                                        <?php
                                        $data = CommonHelper::getFromCache('banners_banner_posts', ['banners']);
                                        if (!$data) {
                                            $data = \Modules\ThemeRaoVat\Models\Banner::where('location', 'banner_posts')->where('status', 1)->where('order_no', 2)->get();
                                            CommonHelper::putToCache('banners_banner_posts', $data, ['banners']);
                                        }
                                        ?>
                                        @foreach($data as $v)
                                            <a href="{{ $v->link }}"><img
                                                        data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$v->image, 262, null) }}"
                                                        alt="{{ @$v->name }}" class="lazy"></a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            {{--                            @include('themeraovat::pages.home.partials.box_tu_van_vien')--}}

                            {{--                            @include('themeraovat::pages.home.partials.tai_lieu_cho_ban')--}}


                        </aside>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
@section('footer_script')

@endsection
@section('head_script')
    <style>
        .listItems .block-item {
            padding-left: 5px;
            padding-right: 5px;
        }

        .listItems .item {
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
            border-radius: 3px;
        }

        @media (min-width: 768px) {
            .listItems .item {
                height: 235px;
                overflow: hidden;
            }

            .listItems ul.info {
                display: flow-root;
            }
        }
    </style>
@endsection
