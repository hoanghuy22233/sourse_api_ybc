<div class="row">
    <div class="col-md-12">
        <div id="rnshophomedata">
            <div id="prodGrid" class="section">
                <div class="listItems">
                    <h2 class="tl"><a href="/ban-si"
                                      style="color:#fff !important"><span><i
                                        class="fa fa-list-ul"></i> Thị trường sỉ hôm nay</span></a>
                    </h2>
                </div>
                <div class="cleafix"></div>
                <div class="row mgt10 rnajaxshop">

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 rnsh">
                        <div class="item prodItem">
                            <div class="thumb">
                                <a href="/khan-nen-tiet-trung-nikkori-nhat-ban-ds15256.html"
                                   title="Khăn nén tiệt trùng Nikkori Nhật Bản">
                                    <img src="/public/frontend/themes/raovat/Uploads/Shop/images/thumb/art_320_320_15256.jpg"
                                         alt="Khăn nén tiệt trùng Nikkori Nhật Bản"></a>
                            </div>

                            <div class="promotion">Giảm <strong>24.0%</strong></div>

                            <div class="info">
                                <h3>
                                    <a href="/khan-nen-tiet-trung-nikkori-nhat-ban-ds15256.html"
                                       title="Khăn nén tiệt trùng Nikkori Nhật Bản">Khăn
                                        nén tiệt trùng Nikkori Nhật Bản</a></h3>
                                <div class="price">
                                    <div class="pull-left"><span>18,000 - 19,000</span>
                                        đ
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="prodItem-bt">
                                    <div class="pull-left"><i class="fa fa-heart-o"></i>()
                                    </div>
                                    <div class="pull-right">
                                        <div class="star star-small"><span
                                                    class="star-05"></span></div>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>