<div class="widget">
    <div class="wg-hd"><h3><i class="fa fa-handshake-o"></i> TƯ VẤN VIÊN</h3></div>
    <div class="wg-ct p15">
        <ul class="reset newsThumb">
            <li class="mgb-5"><a href="tel:0977111896 "><i class="fa fa-headphones"
                                                           aria-hidden="true"></i> <strong
                            class="green">0977.111.896 </strong></a> -
                <span>Ms Bích Phượng</span>
            </li>
            <li class="mgb-5"><a href="tel:0963656512"><i class="fa fa-headphones"
                                                          aria-hidden="true"></i> <strong
                            class="green">0963.656.512</strong></a> - <span>Ms Phương</span>
            </li>

            <li class="mgb-5"><a href="tel:0966908766 "><i class="fa fa-headphones"
                                                           aria-hidden="true"></i> <strong
                            class="green">0966.908.766 </strong></a> - <span>Ms Lý</span>
            </li>
            <li class="mgb-5"><a href="tel:0962977716 "><i class="fa fa-headphones"
                                                           aria-hidden="true"></i> <strong
                            class="green">0962.977.716 </strong></a> - <span>Ms Mận</span>
            </li>
            <li class="mgb-5"><a href="tel:0918955158 "><i class="fa fa-headphones"
                                                           aria-hidden="true"></i> <strong
                            class="green">0918.955.158 </strong></a> - <span>Ms Thảo</span>
            </li>

        </ul>
    </div>
</div>