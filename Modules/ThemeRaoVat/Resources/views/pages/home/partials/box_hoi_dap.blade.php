<div class="widget">
    <div class="wg-hd">
        <h3><i class="fa fa-question-circle-o"></i> HỎI - ĐÁP</h3>
    </div>
    <div class="wg-ct p15">
        <ul class="newsList">

            <li><a title="Hỏi về chức năng tìm sản phẩm theo từ khóa"
                   href="/hoi-ve-chuc-nang-tim-san-pham-theo-tu-khoa-dq1985.html">Hỏi về
                    chức năng tìm sản phẩm theo từ khóa</a></li>

            <li><a title="Mở đại lý bán lẻ" href="/mo-dai-ly-ban-le-dq1921.html">Mở đại
                    lý bán lẻ</a></li>

            <li><a title="Tôi ở thành phố Thanh Hoá muốn làm đại lý được không ?"
                   href="/toi-o-thanh-pho-thanh-hoa-muon-lam-dai-ly-duoc-khong-dq1928.html">Tôi
                    ở thành phố Thanh Hoá muốn làm đại lý được không ?</a></li>

            <li><a title="Muốn làm đại lý" href="/muon-lam-dai-ly-dq1892.html">Muốn làm
                    đại lý</a></li>

            <li><a title="Muốn trở thành đại lý cấp 1 cần khoảng bao nhiêu vốn "
                   href="/muon-tro-thanh-dai-ly-cap-1-can-khoang-bao-nhieu-von-dq1885.html">Muốn
                    trở thành đại lý cấp 1 cần khoảng bao nhiêu vốn </a></li>

        </ul>
        <form class="question">
            <label>Gửi câu hỏi của bạn ở đây</label>
            <textarea placeholder="Câu hỏi"></textarea>
            <a href="/member/qpost" class="btn btn-primary btn-block">Gửi câu hỏi</a>
        </form>
    </div>
</div>