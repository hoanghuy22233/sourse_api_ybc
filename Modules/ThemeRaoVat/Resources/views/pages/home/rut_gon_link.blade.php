@extends('themeraovat::layouts.default')
@section('main_content')
    <div id="body">

        <div id="main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        <form action="" method="GET">
                            <input type="text" name="link" value="{{ @$_GET['link'] }}" style="width: 80%;"
                                   placeholder="Nhập link cần rút gọn">
                            <button type="submit" class="btn btn-primary">Rút gọn</button>
                            <p>Lưu ý: <a href="/admin/rut_gon_link" target="_blank">Đăng nhập</a> vào website để xem thống kê lượng truy cập vào
                                link rút gọn của bạn</p>

                            @if(isset($link))
                                <p style="margin-top: 50px;">
                                    <label>
                                        Link đã rút gọn: <a href="https://biit.link/{{ $link->result }}" id="link_result" target="_blank">https://biit.link/{{ $link->result }}</a>
                                    </label>
                                    <button id="button1" type="button" onclick="CopyToClipboard('link_result')">Sao chép</button>
                                </p>
                            @endif
                        </form>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        @include('themeraovat::pages.post.partials.sidebar_categories', ['post_type' => @$post_type])
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('footer_script')
    <script>
        $('input[name=link]').keyup(function (e) {
            if (e.keyCode == 13) {
                $(this).parents('form').submit();
            }
        });

        function CopyToClipboard(containerid) {
            if (document.selection) {
                var range = document.body.createTextRange();
                range.moveToElementText(document.getElementById(containerid));
                range.select().createTextRange();
                document.execCommand("copy");
            } else if (window.getSelection) {
                var range = document.createRange();
                range.selectNode(document.getElementById(containerid));
                window.getSelection().addRange(range);
                document.execCommand("copy");
            }
        }
    </script>
@endsection
