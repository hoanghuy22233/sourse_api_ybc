@extends('themeraovat::layouts.default')
@section('main_content')
    <div id="body">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title pull-left">
                                        <i class="fa fa-safari"></i> Gửi câu hỏi
                                    </h4>
                                    <div class="btn-group btn-group-sm pull-right">
                                        <a href="/" class="btn btn-success"><i class="fa fa-arrow-left"></i>Quay lại</a>
                                    </div>
                                </div>
                                <div class="panel-body">

                                    <form method="post" name="contact_id" id="adminForm" action=""
                                          enctype="multipart/form-data"
                                          novalidate="novalidate">
                                        @if(Session::has('message') && !Auth::check())
                                            <div class="alert text-center text-white " role="alert"
                                                 style=" margin: 0; font-size: 16px;">
                                                <a href="#" style="float:right;" class="alert-close"
                                                   data-dismiss="alert">&times;</a>
                                                <p style="color: red"><b>{{ Session::get('message') }}</b></p>
                                            </div>
                                        @endif
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Tiêu đề <span class="rnred">(*)</span></label>
                                                    <input class="required form-control input-sm" id="title"
                                                           maxlength="255" name="title"
                                                           placeholder="Nhập tiêu đề câu hỏi" type="text" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Nội dung câu hỏi <span class="rnred">(*)</span></label>
                                                    <textarea class="required form-control input-sm" cols="20"
                                                              id="description" name="description"
                                                              placeholder="Nhập nội dung câu hỏi" rows="2"
                                                              style="height:200px;"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input class="email form-control input-sm" id="email"
                                                           maxlength="255" name="email" type="text" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Điện thoại <span class="rnred">(*)</span></label>
                                                    <input class="required number form-control input-sm" id="tel"
                                                           maxlength="255" name="tel" type="text" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button class="btn btn-success" type="submit" id="btnSave"
                                                        value="Lưu dữ liệu">
                                                    Gửi câu hỏi
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    @include('themeraovat::pages.post.partials.sidebar_categories')
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer_script')

@endsection
