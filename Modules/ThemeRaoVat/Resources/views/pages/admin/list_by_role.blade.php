@extends('themeraovat::layouts.default')
@section('tailieu')
    active
@endsection
@section('main_content')
    <div id="mm-0" class="mm-page mm-slideout">
        <div class="se-pre-con"></div>
        <div class="theme-layout">
            @include('themeraovat::template.top_bar')
            <section>
                <div class="gap2 gray-bg">
                    <div class="container">
                        <div class="row">
                            <div class="user-profile">
                                @include('themeraovat::template.menu')
                            </div><!-- user profile banner  -->
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="central-meta">
                                        <div class="mobile">
                                            <span class="create-post">Thành viên {{ @$admins[0]->role_name }}</span>
                                            @foreach($admins as $admin)
                                                <div class="col-lg-3 col-md-4 col-sm-4 item">
                                                    <div class="pitdate-user">
                                                        <figure>
                                                            <a href="/profile/{{ $admin->id }}">
                                                                <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumbHasDomain($admin->image, 200, null) }}"
                                                                     alt="{{ $admin->name }}">
                                                            </a>
                                                        </figure>
                                                        <h4><a href="/profile/{{ $admin->id }}"
                                                               title="{{ $admin->name }}">{{ $admin->name }}</a></h4>
                                                        <span>{!! $admin->role_name !!}</span>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>


                                        <div id="SECTION615" class="ladi-section desktop">
                                            <div class="ladi-section-background"></div>
                                            <div class="ladi-container">
                                                <div id="HEADLINE653" class="ladi-element"><h2 class="ladi-headline">
                                                        Thành viên {{ @$admins[0]->role_name }}</h2></div>
                                                <div id="LINE654" class="ladi-element">
                                                    <div class="ladi-line">
                                                        <div class="ladi-line-container"></div>
                                                    </div>
                                                </div>
                                                <div id="GROUP941" class="ladi-element ladi-animation">
                                                    <div class="ladi-group">
                                                        @if(isset($admins[0]))
                                                            <?php $admin = $admins[0];?>
                                                            <div id="GROUP942" class="ladi-element">
                                                                <div class="ladi-group">
                                                                    <div id="GROUP943" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="IMAGE944" class="ladi-element">
                                                                                <div class="ladi-image">
                                                                                    <div class="ladi-image-background"
                                                                                         style="background-image: url({{ \App\Http\Helpers\CommonHelper::getUrlImageThumbHasDomain($admin->image, 270, null) }});"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="SHAPE945" class="ladi-element">
                                                                                <div class="ladi-shape">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                         width="100%" height="100%"
                                                                                         preserveAspectRatio="none"
                                                                                         viewBox="0 0 302.68 151.34"
                                                                                         fill="rgba(245, 65, 38, 0.5)">
                                                                                        <path d="M151.34,53.91a97.43,97.43,0,0,1,97.43,97.43h53.91A151.34,151.34,0,0,0,0,151.34H53.91A97.43,97.43,0,0,1,151.34,53.91Z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="GROUP946" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="GROUP947" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="HEADLINE948"
                                                                                         class="ladi-element"><h6
                                                                                                class="ladi-headline">{{ $admin->name }}</h6>
                                                                                    </div>
                                                                                    <div id="HEADLINE949"
                                                                                         class="ladi-element"><p
                                                                                                class="ladi-headline">{!! $admin->intro !!}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="GROUP950" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="SHAPE951"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M19,4V7H17A1,1 0 0,0 16,8V10H19V13H16V20H13V13H11V10H13V7.5C13,5.56 14.57,4 16.5,4M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE952"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M17.71,9.33C17.64,13.95 14.69,17.11 10.28,17.31C8.46,17.39 7.15,16.81 6,16.08C7.34,16.29 9,15.76 9.9,15C8.58,14.86 7.81,14.19 7.44,13.12C7.82,13.18 8.22,13.16 8.58,13.09C7.39,12.69 6.54,11.95 6.5,10.41C6.83,10.57 7.18,10.71 7.64,10.74C6.75,10.23 6.1,8.38 6.85,7.16C8.17,8.61 9.76,9.79 12.37,9.95C11.71,7.15 15.42,5.63 16.97,7.5C17.63,7.38 18.16,7.14 18.68,6.86C18.47,7.5 18.06,7.97 17.56,8.33C18.1,8.26 18.59,8.13 19,7.92C18.75,8.45 18.19,8.93 17.71,9.33M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE953"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M10,16.5V7.5L16,12M20,4.4C19.4,4.2 15.7,4 12,4C8.3,4 4.6,4.19 4,4.38C2.44,4.9 2,8.4 2,12C2,15.59 2.44,19.1 4,19.61C4.6,19.81 8.3,20 12,20C15.7,20 19.4,19.81 20,19.61C21.56,19.1 22,15.59 22,12C22,8.4 21.56,4.91 20,4.4Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        @if(isset($admins[1]))
                                                            <?php $admin = $admins[1];?>
                                                            <div id="GROUP954" class="ladi-element">
                                                                <div class="ladi-group">
                                                                    <div id="GROUP955" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="IMAGE956" class="ladi-element">
                                                                                <div class="ladi-image">
                                                                                    <div class="ladi-image-background"
                                                                                         style="background-image: url({{ \App\Http\Helpers\CommonHelper::getUrlImageThumbHasDomain($admin->image, 270, null) }});"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="SHAPE957" class="ladi-element">
                                                                                <div class="ladi-shape">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                         width="100%" height="100%"
                                                                                         preserveAspectRatio="none"
                                                                                         viewBox="0 0 302.68 151.34"
                                                                                         fill="rgba(245, 65, 38, 0.5)">
                                                                                        <path d="M151.34,53.91a97.43,97.43,0,0,1,97.43,97.43h53.91A151.34,151.34,0,0,0,0,151.34H53.91A97.43,97.43,0,0,1,151.34,53.91Z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="GROUP958" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="GROUP962" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="SHAPE963"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M19,4V7H17A1,1 0 0,0 16,8V10H19V13H16V20H13V13H11V10H13V7.5C13,5.56 14.57,4 16.5,4M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE964"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M17.71,9.33C17.64,13.95 14.69,17.11 10.28,17.31C8.46,17.39 7.15,16.81 6,16.08C7.34,16.29 9,15.76 9.9,15C8.58,14.86 7.81,14.19 7.44,13.12C7.82,13.18 8.22,13.16 8.58,13.09C7.39,12.69 6.54,11.95 6.5,10.41C6.83,10.57 7.18,10.71 7.64,10.74C6.75,10.23 6.1,8.38 6.85,7.16C8.17,8.61 9.76,9.79 12.37,9.95C11.71,7.15 15.42,5.63 16.97,7.5C17.63,7.38 18.16,7.14 18.68,6.86C18.47,7.5 18.06,7.97 17.56,8.33C18.1,8.26 18.59,8.13 19,7.92C18.75,8.45 18.19,8.93 17.71,9.33M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE965"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M10,16.5V7.5L16,12M20,4.4C19.4,4.2 15.7,4 12,4C8.3,4 4.6,4.19 4,4.38C2.44,4.9 2,8.4 2,12C2,15.59 2.44,19.1 4,19.61C4.6,19.81 8.3,20 12,20C15.7,20 19.4,19.81 20,19.61C21.56,19.1 22,15.59 22,12C22,8.4 21.56,4.91 20,4.4Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="HEADLINE960" class="ladi-element">
                                                                                <h6
                                                                                        class="ladi-headline">{{ $admin->name }}</h6>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        @if(isset($admins[2]))
                                                            <?php $admin = $admins[2];?>
                                                            <div id="GROUP966" class="ladi-element">
                                                                <div class="ladi-group">
                                                                    <div id="GROUP967" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="IMAGE968" class="ladi-element">
                                                                                <div class="ladi-image">
                                                                                    <div class="ladi-image-background"
                                                                                         style="background-image: url({{ \App\Http\Helpers\CommonHelper::getUrlImageThumbHasDomain($admin->image, 270, null) }});"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="SHAPE969" class="ladi-element">
                                                                                <div class="ladi-shape">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                         width="100%" height="100%"
                                                                                         preserveAspectRatio="none"
                                                                                         viewBox="0 0 302.68 151.34"
                                                                                         fill="rgba(245, 65, 38, 0.5)">
                                                                                        <path d="M151.34,53.91a97.43,97.43,0,0,1,97.43,97.43h53.91A151.34,151.34,0,0,0,0,151.34H53.91A97.43,97.43,0,0,1,151.34,53.91Z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="GROUP970" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="GROUP971" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="HEADLINE972"
                                                                                         class="ladi-element"><h6
                                                                                                class="ladi-headline">{{ $admin->name }}</h6>
                                                                                    </div>
                                                                                    <div id="HEADLINE973"
                                                                                         class="ladi-element"><p
                                                                                                class="ladi-headline">{{ $admin->intro }}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="GROUP974" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="SHAPE975"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M19,4V7H17A1,1 0 0,0 16,8V10H19V13H16V20H13V13H11V10H13V7.5C13,5.56 14.57,4 16.5,4M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE976"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M17.71,9.33C17.64,13.95 14.69,17.11 10.28,17.31C8.46,17.39 7.15,16.81 6,16.08C7.34,16.29 9,15.76 9.9,15C8.58,14.86 7.81,14.19 7.44,13.12C7.82,13.18 8.22,13.16 8.58,13.09C7.39,12.69 6.54,11.95 6.5,10.41C6.83,10.57 7.18,10.71 7.64,10.74C6.75,10.23 6.1,8.38 6.85,7.16C8.17,8.61 9.76,9.79 12.37,9.95C11.71,7.15 15.42,5.63 16.97,7.5C17.63,7.38 18.16,7.14 18.68,6.86C18.47,7.5 18.06,7.97 17.56,8.33C18.1,8.26 18.59,8.13 19,7.92C18.75,8.45 18.19,8.93 17.71,9.33M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE977"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M10,16.5V7.5L16,12M20,4.4C19.4,4.2 15.7,4 12,4C8.3,4 4.6,4.19 4,4.38C2.44,4.9 2,8.4 2,12C2,15.59 2.44,19.1 4,19.61C4.6,19.81 8.3,20 12,20C15.7,20 19.4,19.81 20,19.61C21.56,19.1 22,15.59 22,12C22,8.4 21.56,4.91 20,4.4Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div id="GROUP656" class="ladi-element">
                                                    <div class="ladi-group">
                                                        @if(isset($admins[3]))
                                                            <?php $admin = $admins[3];?>
                                                            <div id="GROUP657" class="ladi-element">
                                                                <div class="ladi-group">
                                                                    <div id="GROUP658" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="IMAGE659" class="ladi-element">
                                                                                <div class="ladi-image">
                                                                                    <div class="ladi-image-background"
                                                                                         style="background-image: url({{ \App\Http\Helpers\CommonHelper::getUrlImageThumbHasDomain($admin->image, 270, null) }});"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="SHAPE660" class="ladi-element">
                                                                                <div class="ladi-shape">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                         width="100%" height="100%"
                                                                                         preserveAspectRatio="none"
                                                                                         viewBox="0 0 302.68 151.34"
                                                                                         fill="rgba(245, 65, 38, 0.5)">
                                                                                        <path d="M151.34,53.91a97.43,97.43,0,0,1,97.43,97.43h53.91A151.34,151.34,0,0,0,0,151.34H53.91A97.43,97.43,0,0,1,151.34,53.91Z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="GROUP661" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="GROUP662" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="HEADLINE663"
                                                                                         class="ladi-element"><h6
                                                                                                class="ladi-headline">{{ $admin->name }}</h6>
                                                                                    </div>
                                                                                    <div id="HEADLINE664"
                                                                                         class="ladi-element"><p
                                                                                                class="ladi-headline">{{ $admin->intro }}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="GROUP665" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="SHAPE666"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M19,4V7H17A1,1 0 0,0 16,8V10H19V13H16V20H13V13H11V10H13V7.5C13,5.56 14.57,4 16.5,4M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE667"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M17.71,9.33C17.64,13.95 14.69,17.11 10.28,17.31C8.46,17.39 7.15,16.81 6,16.08C7.34,16.29 9,15.76 9.9,15C8.58,14.86 7.81,14.19 7.44,13.12C7.82,13.18 8.22,13.16 8.58,13.09C7.39,12.69 6.54,11.95 6.5,10.41C6.83,10.57 7.18,10.71 7.64,10.74C6.75,10.23 6.1,8.38 6.85,7.16C8.17,8.61 9.76,9.79 12.37,9.95C11.71,7.15 15.42,5.63 16.97,7.5C17.63,7.38 18.16,7.14 18.68,6.86C18.47,7.5 18.06,7.97 17.56,8.33C18.1,8.26 18.59,8.13 19,7.92C18.75,8.45 18.19,8.93 17.71,9.33M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE668"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M10,16.5V7.5L16,12M20,4.4C19.4,4.2 15.7,4 12,4C8.3,4 4.6,4.19 4,4.38C2.44,4.9 2,8.4 2,12C2,15.59 2.44,19.1 4,19.61C4.6,19.81 8.3,20 12,20C15.7,20 19.4,19.81 20,19.61C21.56,19.1 22,15.59 22,12C22,8.4 21.56,4.91 20,4.4Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        @if(isset($admins[4]))
                                                            <?php $admin = $admins[4];?>
                                                            <div id="GROUP669" class="ladi-element">
                                                                <div class="ladi-group">
                                                                    <div id="GROUP670" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="IMAGE671" class="ladi-element">
                                                                                <div class="ladi-image">
                                                                                    <div class="ladi-image-background"
                                                                                         style="background-image: url({{ \App\Http\Helpers\CommonHelper::getUrlImageThumbHasDomain($admin->image, 270, null) }});"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="SHAPE672" class="ladi-element">
                                                                                <div class="ladi-shape">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                         width="100%" height="100%"
                                                                                         preserveAspectRatio="none"
                                                                                         viewBox="0 0 302.68 151.34"
                                                                                         fill="rgba(245, 65, 38, 0.5)">
                                                                                        <path d="M151.34,53.91a97.43,97.43,0,0,1,97.43,97.43h53.91A151.34,151.34,0,0,0,0,151.34H53.91A97.43,97.43,0,0,1,151.34,53.91Z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="GROUP673" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="GROUP674" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="HEADLINE675"
                                                                                         class="ladi-element"><h6
                                                                                                class="ladi-headline">{{ $admin->name }}</h6>
                                                                                    </div>
                                                                                    <div id="HEADLINE676"
                                                                                         class="ladi-element"><p
                                                                                                class="ladi-headline">{{ $admin->intro }}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="GROUP677" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="SHAPE678"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M19,4V7H17A1,1 0 0,0 16,8V10H19V13H16V20H13V13H11V10H13V7.5C13,5.56 14.57,4 16.5,4M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE679"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M17.71,9.33C17.64,13.95 14.69,17.11 10.28,17.31C8.46,17.39 7.15,16.81 6,16.08C7.34,16.29 9,15.76 9.9,15C8.58,14.86 7.81,14.19 7.44,13.12C7.82,13.18 8.22,13.16 8.58,13.09C7.39,12.69 6.54,11.95 6.5,10.41C6.83,10.57 7.18,10.71 7.64,10.74C6.75,10.23 6.1,8.38 6.85,7.16C8.17,8.61 9.76,9.79 12.37,9.95C11.71,7.15 15.42,5.63 16.97,7.5C17.63,7.38 18.16,7.14 18.68,6.86C18.47,7.5 18.06,7.97 17.56,8.33C18.1,8.26 18.59,8.13 19,7.92C18.75,8.45 18.19,8.93 17.71,9.33M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE680"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M10,16.5V7.5L16,12M20,4.4C19.4,4.2 15.7,4 12,4C8.3,4 4.6,4.19 4,4.38C2.44,4.9 2,8.4 2,12C2,15.59 2.44,19.1 4,19.61C4.6,19.81 8.3,20 12,20C15.7,20 19.4,19.81 20,19.61C21.56,19.1 22,15.59 22,12C22,8.4 21.56,4.91 20,4.4Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        @if(isset($admins[5]))
                                                            <?php $admin = $admins[5];?>
                                                            <div id="GROUP681" class="ladi-element">
                                                                <div class="ladi-group">
                                                                    <div id="GROUP682" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="IMAGE683" class="ladi-element">
                                                                                <div class="ladi-image">
                                                                                    <div class="ladi-image-background"
                                                                                         style="background-image: url({{ \App\Http\Helpers\CommonHelper::getUrlImageThumbHasDomain($admin->image, 270, null) }});"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="SHAPE684" class="ladi-element">
                                                                                <div class="ladi-shape">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                         width="100%" height="100%"
                                                                                         preserveAspectRatio="none"
                                                                                         viewBox="0 0 302.68 151.34"
                                                                                         fill="rgba(245, 65, 38, 0.5)">
                                                                                        <path d="M151.34,53.91a97.43,97.43,0,0,1,97.43,97.43h53.91A151.34,151.34,0,0,0,0,151.34H53.91A97.43,97.43,0,0,1,151.34,53.91Z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="GROUP685" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="GROUP686" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="HEADLINE687"
                                                                                         class="ladi-element"><h6
                                                                                                class="ladi-headline">{{ $admin->name }}</h6>
                                                                                    </div>
                                                                                    <div id="HEADLINE688"
                                                                                         class="ladi-element"><p
                                                                                                class="ladi-headline">{{ $admin->intro }}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="GROUP689" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="SHAPE690"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M19,4V7H17A1,1 0 0,0 16,8V10H19V13H16V20H13V13H11V10H13V7.5C13,5.56 14.57,4 16.5,4M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE691"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M17.71,9.33C17.64,13.95 14.69,17.11 10.28,17.31C8.46,17.39 7.15,16.81 6,16.08C7.34,16.29 9,15.76 9.9,15C8.58,14.86 7.81,14.19 7.44,13.12C7.82,13.18 8.22,13.16 8.58,13.09C7.39,12.69 6.54,11.95 6.5,10.41C6.83,10.57 7.18,10.71 7.64,10.74C6.75,10.23 6.1,8.38 6.85,7.16C8.17,8.61 9.76,9.79 12.37,9.95C11.71,7.15 15.42,5.63 16.97,7.5C17.63,7.38 18.16,7.14 18.68,6.86C18.47,7.5 18.06,7.97 17.56,8.33C18.1,8.26 18.59,8.13 19,7.92C18.75,8.45 18.19,8.93 17.71,9.33M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE692"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M10,16.5V7.5L16,12M20,4.4C19.4,4.2 15.7,4 12,4C8.3,4 4.6,4.19 4,4.38C2.44,4.9 2,8.4 2,12C2,15.59 2.44,19.1 4,19.61C4.6,19.81 8.3,20 12,20C15.7,20 19.4,19.81 20,19.61C21.56,19.1 22,15.59 22,12C22,8.4 21.56,4.91 20,4.4Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div id="GROUP995" class="ladi-element">
                                                    <div class="ladi-group">
                                                        @if(isset($admins[6]))
                                                            <?php $admin = $admins[6];?>
                                                            <div id="GROUP996" class="ladi-element">
                                                                <div class="ladi-group">
                                                                    <div id="GROUP997" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="IMAGE998" class="ladi-element">
                                                                                <div class="ladi-image">
                                                                                    <div class="ladi-image-background"
                                                                                         style="background-image: url({{ \App\Http\Helpers\CommonHelper::getUrlImageThumbHasDomain($admin->image, 270, null) }});"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="SHAPE999" class="ladi-element">
                                                                                <div class="ladi-shape">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                         width="100%" height="100%"
                                                                                         preserveAspectRatio="none"
                                                                                         viewBox="0 0 302.68 151.34"
                                                                                         fill="rgba(245, 65, 38, 0.5)">
                                                                                        <path d="M151.34,53.91a97.43,97.43,0,0,1,97.43,97.43h53.91A151.34,151.34,0,0,0,0,151.34H53.91A97.43,97.43,0,0,1,151.34,53.91Z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="GROUP1000" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="GROUP1001" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="HEADLINE1002"
                                                                                         class="ladi-element"><h6
                                                                                                class="ladi-headline">{{ $admin->name }}</h6>
                                                                                    </div>
                                                                                    <div id="HEADLINE1003"
                                                                                         class="ladi-element"><p
                                                                                                class="ladi-headline">{{ $admin->intro }}
                                                                                            <br></p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="GROUP1004" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="SHAPE1005"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M19,4V7H17A1,1 0 0,0 16,8V10H19V13H16V20H13V13H11V10H13V7.5C13,5.56 14.57,4 16.5,4M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE1006"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M17.71,9.33C17.64,13.95 14.69,17.11 10.28,17.31C8.46,17.39 7.15,16.81 6,16.08C7.34,16.29 9,15.76 9.9,15C8.58,14.86 7.81,14.19 7.44,13.12C7.82,13.18 8.22,13.16 8.58,13.09C7.39,12.69 6.54,11.95 6.5,10.41C6.83,10.57 7.18,10.71 7.64,10.74C6.75,10.23 6.1,8.38 6.85,7.16C8.17,8.61 9.76,9.79 12.37,9.95C11.71,7.15 15.42,5.63 16.97,7.5C17.63,7.38 18.16,7.14 18.68,6.86C18.47,7.5 18.06,7.97 17.56,8.33C18.1,8.26 18.59,8.13 19,7.92C18.75,8.45 18.19,8.93 17.71,9.33M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE1007"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M10,16.5V7.5L16,12M20,4.4C19.4,4.2 15.7,4 12,4C8.3,4 4.6,4.19 4,4.38C2.44,4.9 2,8.4 2,12C2,15.59 2.44,19.1 4,19.61C4.6,19.81 8.3,20 12,20C15.7,20 19.4,19.81 20,19.61C21.56,19.1 22,15.59 22,12C22,8.4 21.56,4.91 20,4.4Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        @if(isset($admins[7]))
                                                            <?php $admin = $admins[7];?>
                                                            <div id="GROUP1008" class="ladi-element">
                                                                <div class="ladi-group">
                                                                    <div id="GROUP1009" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="IMAGE1010" class="ladi-element">
                                                                                <div class="ladi-image">
                                                                                    <div class="ladi-image-background"
                                                                                         style="background-image: url({{ \App\Http\Helpers\CommonHelper::getUrlImageThumbHasDomain($admin->image, 270, null) }});"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="SHAPE1011" class="ladi-element">
                                                                                <div class="ladi-shape">
                                                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                                                         width="100%" height="100%"
                                                                                         preserveAspectRatio="none"
                                                                                         viewBox="0 0 302.68 151.34"
                                                                                         fill="rgba(245, 65, 38, 0.5)">
                                                                                        <path d="M151.34,53.91a97.43,97.43,0,0,1,97.43,97.43h53.91A151.34,151.34,0,0,0,0,151.34H53.91A97.43,97.43,0,0,1,151.34,53.91Z"></path>
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="GROUP1012" class="ladi-element">
                                                                        <div class="ladi-group">
                                                                            <div id="GROUP1013" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="HEADLINE1014"
                                                                                         class="ladi-element"><h6
                                                                                                class="ladi-headline">{{ $admin->name }}</h6>
                                                                                    </div>
                                                                                    <div id="HEADLINE1015"
                                                                                         class="ladi-element"><p
                                                                                                class="ladi-headline">{{ $admin->intro }}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="GROUP1016" class="ladi-element">
                                                                                <div class="ladi-group">
                                                                                    <div id="SHAPE1017"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M19,4V7H17A1,1 0 0,0 16,8V10H19V13H16V20H13V13H11V10H13V7.5C13,5.56 14.57,4 16.5,4M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE1018"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M17.71,9.33C17.64,13.95 14.69,17.11 10.28,17.31C8.46,17.39 7.15,16.81 6,16.08C7.34,16.29 9,15.76 9.9,15C8.58,14.86 7.81,14.19 7.44,13.12C7.82,13.18 8.22,13.16 8.58,13.09C7.39,12.69 6.54,11.95 6.5,10.41C6.83,10.57 7.18,10.71 7.64,10.74C6.75,10.23 6.1,8.38 6.85,7.16C8.17,8.61 9.76,9.79 12.37,9.95C11.71,7.15 15.42,5.63 16.97,7.5C17.63,7.38 18.16,7.14 18.68,6.86C18.47,7.5 18.06,7.97 17.56,8.33C18.1,8.26 18.59,8.13 19,7.92C18.75,8.45 18.19,8.93 17.71,9.33M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="SHAPE1019"
                                                                                         class="ladi-element">
                                                                                        <div class="ladi-shape">
                                                                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                 width="100%"
                                                                                                 height="100%"
                                                                                                 viewBox="0 0 24 24"
                                                                                                 fill="rgba(5, 34, 74, 1.0)">
                                                                                                <path d="M10,16.5V7.5L16,12M20,4.4C19.4,4.2 15.7,4 12,4C8.3,4 4.6,4.19 4,4.38C2.44,4.9 2,8.4 2,12C2,15.59 2.44,19.1 4,19.61C4.6,19.81 8.3,20 12,20C15.7,20 19.4,19.81 20,19.61C21.56,19.1 22,15.59 22,12C22,8.4 21.56,4.91 20,4.4Z"></path>
                                                                                            </svg>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endif
                                                            @if(isset($admins[8]))
                                                                <?php $admin = $admins[8];?>
                                                                <div id="GROUP1020" class="ladi-element">
                                                                    <div class="ladi-group">
                                                                        <div id="GROUP1021" class="ladi-element">
                                                                            <div class="ladi-group">
                                                                                <div id="IMAGE1022"
                                                                                     class="ladi-element">
                                                                                    <div class="ladi-image">
                                                                                        <div class="ladi-image-background"
                                                                                             style="background-image: url({{ \App\Http\Helpers\CommonHelper::getUrlImageThumbHasDomain($admin->image, 270, null) }});"></div>
                                                                                    </div>
                                                                                </div>
                                                                                <div id="SHAPE1023"
                                                                                     class="ladi-element">
                                                                                    <div class="ladi-shape">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                                             width="100%" height="100%"
                                                                                             preserveAspectRatio="none"
                                                                                             viewBox="0 0 302.68 151.34"
                                                                                             fill="rgba(245, 65, 38, 0.5)">
                                                                                            <path d="M151.34,53.91a97.43,97.43,0,0,1,97.43,97.43h53.91A151.34,151.34,0,0,0,0,151.34H53.91A97.43,97.43,0,0,1,151.34,53.91Z"></path>
                                                                                        </svg>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div id="GROUP1024" class="ladi-element">
                                                                            <div class="ladi-group">
                                                                                <div id="GROUP1025"
                                                                                     class="ladi-element">
                                                                                    <div class="ladi-group">
                                                                                        <div id="HEADLINE1026"
                                                                                             class="ladi-element"><h6
                                                                                                    class="ladi-headline">{{ $admin->name }}</h6>
                                                                                        </div>
                                                                                        <div id="HEADLINE1027"
                                                                                             class="ladi-element"><p
                                                                                                    class="ladi-headline">{{ $admin->intro }}</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div id="GROUP1028"
                                                                                     class="ladi-element">
                                                                                    <div class="ladi-group">
                                                                                        <div id="SHAPE1029"
                                                                                             class="ladi-element">
                                                                                            <div class="ladi-shape">
                                                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                     width="100%"
                                                                                                     height="100%"
                                                                                                     viewBox="0 0 24 24"
                                                                                                     fill="rgba(5, 34, 74, 1.0)">
                                                                                                    <path d="M19,4V7H17A1,1 0 0,0 16,8V10H19V13H16V20H13V13H11V10H13V7.5C13,5.56 14.57,4 16.5,4M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                                </svg>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="SHAPE1030"
                                                                                             class="ladi-element">
                                                                                            <div class="ladi-shape">
                                                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                     width="100%"
                                                                                                     height="100%"
                                                                                                     viewBox="0 0 24 24"
                                                                                                     fill="rgba(5, 34, 74, 1.0)">
                                                                                                    <path d="M17.71,9.33C17.64,13.95 14.69,17.11 10.28,17.31C8.46,17.39 7.15,16.81 6,16.08C7.34,16.29 9,15.76 9.9,15C8.58,14.86 7.81,14.19 7.44,13.12C7.82,13.18 8.22,13.16 8.58,13.09C7.39,12.69 6.54,11.95 6.5,10.41C6.83,10.57 7.18,10.71 7.64,10.74C6.75,10.23 6.1,8.38 6.85,7.16C8.17,8.61 9.76,9.79 12.37,9.95C11.71,7.15 15.42,5.63 16.97,7.5C17.63,7.38 18.16,7.14 18.68,6.86C18.47,7.5 18.06,7.97 17.56,8.33C18.1,8.26 18.59,8.13 19,7.92C18.75,8.45 18.19,8.93 17.71,9.33M20,2H4A2,2 0 0,0 2,4V20A2,2 0 0,0 4,22H20A2,2 0 0,0 22,20V4C22,2.89 21.1,2 20,2Z"></path>
                                                                                                </svg>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div id="SHAPE1031"
                                                                                             class="ladi-element">
                                                                                            <div class="ladi-shape">
                                                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                                                     width="100%"
                                                                                                     height="100%"
                                                                                                     viewBox="0 0 24 24"
                                                                                                     fill="rgba(5, 34, 74, 1.0)">
                                                                                                    <path d="M10,16.5V7.5L16,12M20,4.4C19.4,4.2 15.7,4 12,4C8.3,4 4.6,4.19 4,4.38C2.44,4.9 2,8.4 2,12C2,15.59 2.44,19.1 4,19.61C4.6,19.81 8.3,20 12,20C15.7,20 19.4,19.81 20,19.61C21.56,19.1 22,15.59 22,12C22,8.4 21.56,4.91 20,4.4Z"></path>
                                                                                                </svg>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
@section('head_script')
    <style>
        .post-meta .description > p {
            width: unset;
            height: unset;
        }

        .item {
            display: inline-block;
        }

        .ladipage-message .ladipage-message-box {
            width: 400px;
            max-width: calc(100% - 50px);
            height: 160px;
            border: 1px solid rgba(0, 0, 0, .3);
            background-color: #fff;
            position: fixed;
            top: calc(50% - 155px);
            left: 0;
            right: 0;
            margin: auto;
            border-radius: 10px
        }

        .ladipage-message .ladipage-message-box h1 {
            background-color: rgba(6, 21, 40, .05);
            color: #000;
            padding: 12px 15px;
            font-weight: 600;
            font-size: 16px;
            border-top-left-radius: 10px;
            border-top-right-radius: 10px
        }

        .ladipage-message .ladipage-message-box .ladipage-message-text {
            font-size: 14px;
            padding: 0 20px;
            margin-top: 10px;
            line-height: 18px;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box
        }

        .ladipage-message .ladipage-message-box .ladipage-message-close {
            display: block;
            position: absolute;
            right: 15px;
            bottom: 10px;
            margin: 0 auto;
            padding: 10px 0;
            border: none;
            width: 80px;
            text-transform: uppercase;
            text-align: center;
            color: #000;
            background-color: #e6e6e6;
            border-radius: 5px;
            text-decoration: none;
            font-size: 14px;
            font-weight: 600;
            cursor: pointer
        }

        .ladi-wraper {
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        .ladi-section {
            margin: 0 auto;
            position: relative
        }

        .ladi-section .ladi-section-arrow-down {
            position: absolute;
            width: 36px;
            height: 30px;
            bottom: 0;
            right: 0;
            left: 0;
            margin: auto;
            background: url(https://w.ladicdn.com/v2/source/ladi-icons.svg) rgba(255, 255, 255, .2) no-repeat;
            background-position: 4px;
            cursor: pointer;
            z-index: 90000040
        }

        .ladi-section.ladi-section-readmore {
            transition: height 350ms linear 0s
        }

        .ladi-section .ladi-section-background {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            pointer-events: none;
            overflow: hidden
        }

        .ladi-container {
            position: relative;
            margin: 0 auto;
            height: 100%
        }

        .ladi-element {
            position: absolute
        }

        .ladi-overlay {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            pointer-events: none
        }

        .ladi-carousel {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        .ladi-carousel .ladi-carousel-content {
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            transition: left 350ms ease-in-out
        }

        .ladi-carousel .ladi-carousel-arrow {
            position: absolute;
            width: 30px;
            height: 36px;
            top: calc(50% - 18px);
            background: url(https://w.ladicdn.com/v2/source/ladi-icons.svg) rgba(255, 255, 255, .2) no-repeat;
            cursor: pointer;
            z-index: 90000040
        }

        .ladi-carousel .ladi-carousel-arrow-left {
            left: 5px;
            background-position: -28px
        }

        .ladi-carousel .ladi-carousel-arrow-right {
            right: 5px;
            background-position: -52px
        }

        .ladi-gallery {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-gallery .ladi-gallery-view {
            position: absolute;
            overflow: hidden
        }

        .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center center;
            width: 100%;
            height: 100%;
            position: relative;
            display: none;
            transition: transform 350ms ease-in-out;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            -webkit-perspective: 1000px;
            perspective: 1000px
        }

        .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.play-video {
            cursor: pointer
        }

        .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.play-video:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            width: 60px;
            height: 60px;
            background: url(https://w.ladicdn.com/source/ladipage-play.svg) no-repeat center center;
            background-size: contain;
            pointer-events: none;
            cursor: pointer
        }

        .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.next, .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.selected.right {
            left: 0;
            transform: translate3d(100%, 0, 0)
        }

        .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.prev, .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.selected.left {
            left: 0;
            transform: translate3d(-100%, 0, 0)
        }

        .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.next.left, .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.prev.right, .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item.selected {
            left: 0;
            transform: translate3d(0, 0, 0)
        }

        .ladi-gallery .ladi-gallery-view > .next, .ladi-gallery .ladi-gallery-view > .prev, .ladi-gallery .ladi-gallery-view > .selected {
            display: block
        }

        .ladi-gallery .ladi-gallery-view > .selected {
            left: 0
        }

        .ladi-gallery .ladi-gallery-view > .next, .ladi-gallery .ladi-gallery-view > .prev {
            position: absolute;
            top: 0;
            width: 100%
        }

        .ladi-gallery .ladi-gallery-view > .next {
            left: 100%
        }

        .ladi-gallery .ladi-gallery-view > .prev {
            left: -100%
        }

        .ladi-gallery .ladi-gallery-view > .next.left, .ladi-gallery .ladi-gallery-view > .prev.right {
            left: 0
        }

        .ladi-gallery .ladi-gallery-view > .selected.left {
            left: -100%
        }

        .ladi-gallery .ladi-gallery-view > .selected.right {
            left: 100%
        }

        .ladi-gallery .ladi-gallery-control {
            position: absolute;
            overflow: hidden
        }

        .ladi-gallery.ladi-gallery-top .ladi-gallery-view {
            width: 100%
        }

        .ladi-gallery.ladi-gallery-top .ladi-gallery-control {
            top: 0;
            width: 100%
        }

        .ladi-gallery.ladi-gallery-bottom .ladi-gallery-view {
            top: 0;
            width: 100%
        }

        .ladi-gallery.ladi-gallery-bottom .ladi-gallery-control {
            width: 100%;
            bottom: 0
        }

        .ladi-gallery.ladi-gallery-left .ladi-gallery-view {
            height: 100%
        }

        .ladi-gallery.ladi-gallery-left .ladi-gallery-control {
            height: 100%
        }

        .ladi-gallery.ladi-gallery-right .ladi-gallery-view {
            height: 100%
        }

        .ladi-gallery.ladi-gallery-right .ladi-gallery-control {
            height: 100%;
            right: 0
        }

        .ladi-gallery .ladi-gallery-view .ladi-gallery-view-arrow {
            position: absolute;
            width: 30px;
            height: 36px;
            top: calc(50% - 18px);
            background: url(https://w.ladicdn.com/v2/source/ladi-icons.svg) rgba(255, 255, 255, .2) no-repeat;
            cursor: pointer;
            z-index: 90000040
        }

        .ladi-gallery .ladi-gallery-view .ladi-gallery-view-arrow-left {
            left: 5px;
            background-position: -28px
        }

        .ladi-gallery .ladi-gallery-view .ladi-gallery-view-arrow-right {
            right: 5px;
            background-position: -52px
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-arrow {
            position: absolute;
            background: url(https://w.ladicdn.com/v2/source/ladi-icons.svg) rgba(255, 255, 255, .2) no-repeat;
            cursor: pointer;
            z-index: 90000040
        }

        .ladi-gallery.ladi-gallery-bottom .ladi-gallery-control .ladi-gallery-control-arrow, .ladi-gallery.ladi-gallery-top .ladi-gallery-control .ladi-gallery-control-arrow {
            top: calc(50% - 18px);
            width: 30px;
            height: 36px
        }

        .ladi-gallery.ladi-gallery-top .ladi-gallery-control .ladi-gallery-control-arrow-left {
            left: 0;
            background-position: -28px;
            transform: scale(.6)
        }

        .ladi-gallery.ladi-gallery-top .ladi-gallery-control .ladi-gallery-control-arrow-right {
            right: 0;
            background-position: -52px;
            transform: scale(.6)
        }

        .ladi-gallery.ladi-gallery-bottom .ladi-gallery-control .ladi-gallery-control-arrow-left {
            left: 0;
            background-position: -28px;
            transform: scale(.6)
        }

        .ladi-gallery.ladi-gallery-bottom .ladi-gallery-control .ladi-gallery-control-arrow-right {
            right: 0;
            background-position: -52px;
            transform: scale(.6)
        }

        .ladi-gallery.ladi-gallery-left .ladi-gallery-control .ladi-gallery-control-arrow, .ladi-gallery.ladi-gallery-right .ladi-gallery-control .ladi-gallery-control-arrow {
            left: calc(50% - 18px);
            width: 36px;
            height: 30px
        }

        .ladi-gallery.ladi-gallery-left .ladi-gallery-control .ladi-gallery-control-arrow-left {
            top: 0;
            background-position: -77px;
            transform: scale(.6)
        }

        .ladi-gallery.ladi-gallery-left .ladi-gallery-control .ladi-gallery-control-arrow-right {
            bottom: 0;
            background-position: 3px;
            transform: scale(.6)
        }

        .ladi-gallery.ladi-gallery-right .ladi-gallery-control .ladi-gallery-control-arrow-left {
            top: 0;
            background-position: -77px;
            transform: scale(.6)
        }

        .ladi-gallery.ladi-gallery-right .ladi-gallery-control .ladi-gallery-control-arrow-right {
            bottom: 0;
            background-position: 3px;
            transform: scale(.6)
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-box {
            position: relative
        }

        .ladi-gallery.ladi-gallery-top .ladi-gallery-control .ladi-gallery-control-box {
            display: inline-flex;
            left: 0;
            transition: left 150ms ease-in-out
        }

        .ladi-gallery.ladi-gallery-bottom .ladi-gallery-control .ladi-gallery-control-box {
            display: inline-flex;
            left: 0;
            transition: left 150ms ease-in-out
        }

        .ladi-gallery.ladi-gallery-left .ladi-gallery-control .ladi-gallery-control-box {
            display: inline-grid;
            top: 0;
            transition: top 150ms ease-in-out
        }

        .ladi-gallery.ladi-gallery-right .ladi-gallery-control .ladi-gallery-control-box {
            display: inline-grid;
            top: 0;
            transition: top 150ms ease-in-out
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-box .ladi-gallery-control-item {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center center;
            float: left;
            position: relative;
            cursor: pointer;
            filter: invert(15%)
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-box .ladi-gallery-control-item.play-video:after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            width: 30px;
            height: 30px;
            background: url(https://w.ladicdn.com/source/ladipage-play.svg) no-repeat center center;
            background-size: contain;
            pointer-events: none;
            cursor: pointer
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-box .ladi-gallery-control-item:hover {
            filter: none
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-box .ladi-gallery-control-item.selected {
            filter: none
        }

        .ladi-gallery .ladi-gallery-control .ladi-gallery-control-box .ladi-gallery-control-item:last-child {
            margin-right: 0 !important;
            margin-bottom: 0 !important
        }

        .ladi-box {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        .ladi-frame {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        .ladi-frame .ladi-frame-background {
            height: 100%;
            width: 100%;
            pointer-events: none
        }

        .ladi-banner {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        .ladi-banner .ladi-banner-background {
            height: 100%;
            width: 100%;
            pointer-events: none
        }

        #SECTION_POPUP .ladi-container {
            z-index: 90000070
        }

        #SECTION_POPUP .ladi-container > .ladi-element {
            z-index: 90000070;
            position: fixed;
            display: none
        }

        #SECTION_POPUP .ladi-container > .ladi-element.hide-visibility {
            display: block !important;
            visibility: hidden !important
        }

        #SECTION_POPUP .popup-close {
            width: 30px;
            height: 30px;
            position: absolute;
            right: 0;
            top: 0;
            transform: scale(.8);
            -webkit-transform: scale(.8);
            z-index: 9000000080;
            background: url(https://w.ladicdn.com/v2/source/ladi-icons.svg) rgba(255, 255, 255, .2) no-repeat;
            background-position: -108px;
            cursor: pointer;
            display: none
        }

        .ladi-popup {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-popup .ladi-popup-background {
            height: 100%;
            width: 100%;
            pointer-events: none
        }

        .ladi-countdown {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-countdown .ladi-countdown-background {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit;
            display: table;
            pointer-events: none
        }

        .ladi-countdown .ladi-countdown-text {
            position: absolute;
            width: 100%;
            height: 100%;
            text-decoration: inherit;
            display: table;
            pointer-events: none
        }

        .ladi-countdown .ladi-countdown-text span {
            display: table-cell;
            vertical-align: middle
        }

        .ladi-countdown > .ladi-element {
            text-decoration: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit;
            position: relative;
            display: inline-block
        }

        .ladi-countdown > .ladi-element:last-child {
            margin-right: 0 !important
        }

        .ladi-button {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden
        }

        .ladi-button:active {
            transform: translateY(2px)
        }

        .ladi-button .ladi-button-background {
            height: 100%;
            width: 100%;
            pointer-events: none
        }

        .ladi-button > .ladi-element {
            width: 100% !important;
            height: 100% !important;
            top: 0 !important;
            left: 0 !important;
            display: table;
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none
        }

        .ladi-button > .ladi-element .ladi-headline {
            display: table-cell;
            vertical-align: middle
        }

        .ladi-collection {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-collection.carousel {
            overflow: hidden
        }

        .ladi-collection .ladi-collection-content {
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            transition: left 350ms ease-in-out
        }

        .ladi-collection .ladi-collection-content .ladi-collection-item {
            display: block;
            position: relative;
            float: left;
            box-shadow: 0 0 0 1px #fff
        }

        .ladi-collection .ladi-collection-content .ladi-collection-page {
            float: left
        }

        .ladi-collection .ladi-collection-arrow {
            position: absolute;
            width: 30px;
            height: 36px;
            top: calc(50% - 18px);
            background: url(https://w.ladicdn.com/v2/source/ladi-icons.svg) rgba(255, 255, 255, .2) no-repeat;
            cursor: pointer;
            z-index: 90000040
        }

        .ladi-collection .ladi-collection-arrow-left {
            left: 5px;
            background-position: -28px
        }

        .ladi-collection .ladi-collection-arrow-right {
            right: 5px;
            background-position: -52px
        }

        .ladi-collection .ladi-collection-button-next {
            position: absolute;
            width: 36px;
            height: 30px;
            bottom: -40px;
            right: 0;
            left: 0;
            margin: auto;
            background: url(https://w.ladicdn.com/v2/source/ladi-icons.svg) rgba(255, 255, 255, .2) no-repeat;
            background-position: 4px;
            cursor: pointer;
            z-index: 90000040
        }

        .ladi-form {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-form > .ladi-element {
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form .ladi-element[id^=BUTTON_TEXT] {
            color: initial;
            font-size: initial;
            font-weight: initial;
            text-transform: initial;
            text-decoration: initial;
            font-style: initial;
            text-align: initial;
            letter-spacing: initial;
            line-height: initial
        }

        .ladi-form > .ladi-element .ladi-form-item-container {
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item {
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item-background {
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            background-size: 9px 6px !important;
            background-position: right .5rem center;
            background-repeat: no-repeat
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select-3 {
            width: calc(100% / 3 - 5px);
            max-width: calc(100% / 3 - 5px);
            min-width: calc(100% / 3 - 5px)
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select-3:nth-child(3) {
            margin-left: 7.5px
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select-3:nth-child(4) {
            margin-left: 7.5px
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select option {
            color: initial
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control:not(.ladi-form-control-select) {
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select:not([data-selected=""]) {
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select[data-selected=""] {
            text-transform: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-item {
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-item span {
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-item span[data-checked=true] {
            text-transform: inherit;
            text-decoration: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-item span[data-checked=false] {
            text-transform: inherit;
            text-align: inherit;
            letter-spacing: inherit;
            color: inherit;
            background-size: inherit;
            background-attachment: inherit;
            background-origin: inherit
        }

        .ladi-form .ladi-form-item-container {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-form .ladi-form-item {
            width: 100%;
            height: 100%;
            position: absolute
        }

        .ladi-form .ladi-form-item-background {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            pointer-events: none
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox {
            height: auto
        }

        .ladi-form .ladi-form-item .ladi-form-control {
            background-color: transparent;
            min-width: 100%;
            min-height: 100%;
            max-width: 100%;
            max-height: 100%;
            width: 100%;
            height: 100%;
            padding: 0 5px;
            color: inherit;
            font-size: inherit;
            border: none
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox {
            padding: 10px 5px
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox.ladi-form-checkbox-vertical .ladi-form-checkbox-item {
            margin-top: 0 !important;
            margin-left: 0 !important;
            margin-right: 0 !important;
            display: block;
            border: none
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox.ladi-form-checkbox-horizontal .ladi-form-checkbox-item {
            margin-top: 0 !important;
            margin-left: 0 !important;
            margin-bottom: 0 !important;
            display: inline-block;
            border: none
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item:last-child {
            margin-bottom: 0 !important
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item input {
            vertical-align: middle
        }

        .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span {
            display: inline-block;
            margin-left: 5px;
            cursor: default;
            max-width: calc(100% - 20px);
            vertical-align: top
        }

        .ladi-form .ladi-form-item textarea.ladi-form-control {
            resize: none;
            padding: 5px
        }

        .ladi-form .ladi-button {
            cursor: pointer
        }

        .ladi-form .ladi-button .ladi-headline {
            cursor: pointer;
            user-select: none
        }

        .ladi-cart {
            position: absolute;
            width: 100%;
            font-size: 12px
        }

        .ladi-cart .ladi-cart-row {
            position: relative;
            display: inline-table;
            width: 100%
        }

        .ladi-cart .ladi-cart-row:after {
            content: '';
            position: absolute;
            left: 0;
            bottom: 0;
            height: 1px;
            width: 100%;
            background: #dcdcdc
        }

        .ladi-cart .ladi-cart-no-product {
            text-align: center;
            font-size: 16px;
            vertical-align: middle
        }

        .ladi-cart .ladi-cart-image {
            width: 16%;
            vertical-align: middle;
            position: relative;
            text-align: center
        }

        .ladi-cart .ladi-cart-image img {
            max-width: 100%
        }

        .ladi-cart .ladi-cart-title {
            vertical-align: middle;
            padding: 0 5px;
            word-break: break-all
        }

        .ladi-cart .ladi-cart-title .ladi-cart-title-name {
            display: block;
            margin-bottom: 5px
        }

        .ladi-cart .ladi-cart-title .ladi-cart-title-variant {
            font-weight: 700;
            display: block
        }

        .ladi-cart .ladi-cart-image .ladi-cart-image-quantity {
            position: absolute;
            top: -3px;
            right: -5px;
            background: rgba(150, 149, 149, .9);
            width: 20px;
            height: 20px;
            border-radius: 50%;
            text-align: center;
            color: #fff;
            line-height: 20px
        }

        .ladi-cart .ladi-cart-quantity {
            width: 70px;
            vertical-align: middle;
            text-align: center
        }

        .ladi-cart .ladi-cart-quantity-content {
            display: inline-flex
        }

        .ladi-cart .ladi-cart-quantity input {
            width: 24px;
            text-align: center;
            height: 22px;
            -moz-appearance: textfield;
            border-top: 1px solid #dcdcdc;
            border-bottom: 1px solid #dcdcdc
        }

        .ladi-cart .ladi-cart-quantity input::-webkit-inner-spin-button, .ladi-cart .ladi-cart-quantity input::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0
        }

        .ladi-cart .ladi-cart-quantity button {
            border: 1px solid #dcdcdc;
            cursor: pointer;
            text-align: center;
            width: 21px;
            height: 22px;
            position: relative;
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none
        }

        .ladi-cart .ladi-cart-quantity button:active {
            transform: translateY(2px)
        }

        .ladi-cart .ladi-cart-quantity button span {
            font-size: 18px;
            position: relative;
            left: .5px
        }

        .ladi-cart .ladi-cart-quantity button:first-child span {
            top: -1.2px
        }

        .ladi-cart .ladi-cart-price {
            width: 100px;
            vertical-align: middle;
            text-align: right;
            padding: 0 5px
        }

        .ladi-cart .ladi-cart-action {
            width: 28px;
            vertical-align: middle;
            text-align: center
        }

        .ladi-cart .ladi-cart-action button {
            border: 1px solid #dcdcdc;
            cursor: pointer;
            text-align: center;
            width: 25px;
            height: 22px;
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none
        }

        .ladi-cart .ladi-cart-action button:active {
            transform: translateY(2px)
        }

        .ladi-cart .ladi-cart-action button span {
            font-size: 13px;
            position: relative;
            top: .5px
        }

        .ladi-video {
            position: absolute;
            width: 100%;
            height: 100%;
            cursor: pointer;
            overflow: hidden
        }

        .ladi-video .ladi-video-background {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            pointer-events: none
        }

        .ladi-group {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-checkout {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-shape {
            position: absolute;
            width: 100%;
            height: 100%;
            pointer-events: none
        }

        .ladi-html-code {
            position: absolute;
            width: 100%;
            height: 100%
        }

        .ladi-image {
            position: absolute;
            width: 100%;
            height: 100%;
            overflow: hidden;
            pointer-events: none
        }

        .ladi-image .ladi-image-background {
            background-repeat: no-repeat;
            background-position: left top;
            background-size: cover;
            background-attachment: scroll;
            background-origin: content-box;
            position: absolute;
            margin: 0 auto;
            width: 100%;
            height: 100%;
            pointer-events: none
        }

        .ladi-headline {
            width: 100%;
            display: inline-block;
            background-size: cover;
            background-position: center center
        }

        .ladi-headline a {
            text-decoration: underline
        }

        .ladi-paragraph {
            width: 100%;
            display: inline-block
        }

        .ladi-paragraph a {
            text-decoration: underline
        }

        .ladi-list-paragraph {
            width: 100%;
            display: inline-block
        }

        .ladi-list-paragraph a {
            text-decoration: underline
        }

        .ladi-list-paragraph ul li {
            position: relative;
            counter-increment: linum
        }

        .ladi-list-paragraph ul li:before {
            position: absolute;
            background-repeat: no-repeat;
            background-size: 100% 100%;
            left: 0
        }

        .ladi-list-paragraph ul li:last-child {
            padding-bottom: 0 !important
        }

        .ladi-line {
            position: relative
        }

        .ladi-line .ladi-line-container {
            border-bottom: 0 !important;
            border-right: 0 !important;
            width: 100%;
            height: 100%
        }

        a[data-action] {
            user-select: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            cursor: pointer
        }

        a:visited {
            color: inherit
        }

        a:link {
            color: inherit
        }

        [data-opacity="0"] {
            opacity: 0
        }

        [data-hidden=true] {
            display: none
        }

        [data-action=true] {
            cursor: pointer
        }

        .ladi-hidden {
            display: none
        }

        .backdrop-popup {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: 90000060
        }

        .lightbox-screen {
            display: none;
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            margin: auto;
            z-index: 9000000080;
            background: rgba(0, 0, 0, .5)
        }

        .lightbox-screen .lightbox-close {
            width: 30px;
            height: 30px;
            position: absolute;
            z-index: 9000000090;
            background: url(https://w.ladicdn.com/v2/source/ladi-icons.svg) rgba(255, 255, 255, .2) no-repeat;
            background-position: -108px;
            transform: scale(.7);
            -webkit-transform: scale(.7);
            cursor: pointer
        }

        .lightbox-screen .lightbox-hidden {
            display: none
        }

        .ladi-animation-hidden {
            visibility: hidden !important
        }

        .ladi-lazyload {
            background-image: none !important
        }

        .ladi-list-paragraph ul li.ladi-lazyload:before {
            background-image: none !important
        }

        @media (min-width: 768px) {
            .ladi-fullwidth {
                width: 100vw !important;
                left: calc(-50vw + 50%) !important;
                box-sizing: border-box !important;
                transform: none !important
            }
        }

        @media (max-width: 767px) {
            .ladi-element.ladi-auto-scroll {
                overflow-x: scroll;
                overflow-y: hidden;
                width: 100% !important;
                left: 0 !important;
                -webkit-overflow-scrolling: touch
            }

            .ladi-section.ladi-auto-scroll {
                overflow-x: scroll;
                overflow-y: hidden;
                -webkit-overflow-scrolling: touch
            }

            .ladi-carousel .ladi-carousel-content {
                transition: left .3s ease-in-out
            }

            .ladi-gallery .ladi-gallery-view > .ladi-gallery-view-item {
                transition: transform .3s ease-in-out
            }

            .ladi-form > .ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-item input {
                vertical-align: middle
            }
        }

        .ladi-notify-transition {
            transition: top .5s ease-in-out, bottom .5s ease-in-out, opacity .5s ease-in-out
        }

        .ladi-notify {
            padding: 5px;
            box-shadow: 0 0 1px rgba(64, 64, 64, .3), 0 8px 50px rgba(64, 64, 64, .05);
            border-radius: 40px;
            color: rgba(64, 64, 64, 1);
            background: rgba(250, 250, 250, .9);
            line-height: 1.6;
            width: 100%;
            height: 100%;
            font-size: 13px
        }

        .ladi-notify .ladi-notify-image img {
            float: left;
            margin-right: 13px;
            border-radius: 50%;
            width: 53px;
            height: 53px;
            pointer-events: none
        }

        .ladi-notify .ladi-notify-title {
            font-size: 100%;
            height: 17px;
            overflow: hidden;
            font-weight: 700;
            overflow-wrap: break-word;
            text-overflow: ellipsis;
            white-space: nowrap;
            line-height: 1
        }

        .ladi-notify .ladi-notify-content {
            font-size: 92.308%;
            height: 17px;
            overflow: hidden;
            overflow-wrap: break-word;
            text-overflow: ellipsis;
            white-space: nowrap;
            line-height: 1;
            padding-top: 2px
        }

        .ladi-notify .ladi-notify-time {
            line-height: 1.6;
            font-size: 84.615%;
            display: inline-block;
            overflow-wrap: break-word;
            text-overflow: ellipsis;
            white-space: nowrap;
            max-width: calc(100% - 155px);
            overflow: hidden
        }

        .ladi-notify .ladi-notify-copyright {
            font-size: 76.9231%;
            margin-left: 2px;
            position: relative;
            padding: 0 5px;
            cursor: pointer;
            opacity: .6;
            display: inline-block;
            top: -4px
        }

        .ladi-notify .ladi-notify-copyright svg {
            vertical-align: middle
        }

        .ladi-notify .ladi-notify-copyright svg:not(:root) {
            overflow: hidden
        }

        .ladi-notify .ladi-notify-copyright div {
            text-decoration: none;
            color: rgba(64, 64, 64, 1);
            display: inline
        }

        .ladi-notify .ladi-notify-copyright strong {
            font-weight: 700
        }

        .builder-container .ladi-notify {
            transition: unset
        }

        .ladi-spin-lucky {
            width: 100%;
            height: 100%;
            border-radius: 100%;
            box-shadow: 0 0 7px 0 rgba(64, 64, 64, .6), 0 8px 50px rgba(64, 64, 64, .3);
            background-repeat: no-repeat;
            background-size: cover
        }

        .ladi-spin-lucky .ladi-spin-lucky-start {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            margin: auto;
            width: 20%;
            height: 20%;
            cursor: pointer;
            background-size: contain;
            background-position: center center;
            background-repeat: no-repeat;
            transition: transform .3s ease-in-out;
            -webkit-transition: transform .3s ease-in-out
        }

        .ladi-spin-lucky .ladi-spin-lucky-start:hover {
            transform: scale(1.1)
        }

        .ladi-spin-lucky .ladi-spin-lucky-screen {
            width: 100%;
            height: 100%;
            background-size: cover;
            background-position: center center;
            background-repeat: no-repeat;
            border-radius: 100%;
            transition: transform 7s cubic-bezier(.25, .1, 0, 1);
            -webkit-transition: transform 7s cubic-bezier(.25, .1, 0, 1);
            text-decoration-line: inherit;
            text-transform: inherit;
            -webkit-text-decoration-line: inherit
        }

        .ladi-spin-lucky .ladi-spin-lucky-label {
            position: absolute;
            top: 50%;
            left: 50%;
            overflow: hidden;
            text-align: center;
            width: 42%;
            padding-left: 12%;
            transform-origin: 0 0;
            -webkit-transform-origin: 0 0;
            text-decoration-line: inherit;
            text-transform: inherit;
            -webkit-text-decoration-line: inherit;
            line-height: 1;
            text-shadow: rgba(0, 0, 0, .5) 1px 0 2px
        }</style>
    <style id="style_page" type="text/css">@media (min-width: 768px) {
            .ladi-section .ladi-container {
                width: 960px;
            }
        }

        @media (max-width: 767px) {
            .ladi-section .ladi-container {
                width: 420px;
            }
        }

        body {
            font-family: "Open Sans", sans-serif
        }</style>
    <style id="style_element" type="text/css">@media (min-width: 768px) {
            #SECTION_POPUP {
                height: 0px;
            }

            #SECTION436 {
                height: 579px;
            }

            #SECTION436 > .ladi-overlay {
                background-color: rgba(0, 0, 0, 0.3);
            }

            #SECTION436 > .ladi-section-background {
                background-size: auto 100%;
                background-attachment: scroll;
                background-origin: content-box;
                background-image: url("https://w.ladicdn.com/uploads/images/188953cd-179d-44b6-ad1f-a091a136f6d7.jpg");
                background-position: left top;
                background-repeat: no-repeat;
                opacity: 0.83;
            }

            #SECTION512 {
                height: 349.558px;
            }

            #SECTION512 > .ladi-section-background {
                background-color: rgb(255, 255, 255);
            }

            #LINE438 {
                width: 699px;
                top: 300px;
                left: 130px;
            }

            #LINE438 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(255, 235, 59);
                border-right: 2px solid rgb(255, 235, 59);
                border-bottom: 2px solid rgb(255, 235, 59);
                border-left: 0px !important;
            }

            #LINE438 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #HEADLINE439 {
                width: 803px;
                top: 253px;
                left: 79px;
            }

            #HEADLINE439 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 47px;
                font-weight: bold;
                text-align: center;
                letter-spacing: 5px;
                line-height: 1;
            }

            #HEADLINE440 {
                width: 215px;
                top: 446px;
                left: 193px;
            }

            #HEADLINE440 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE441 {
                width: 154px;
                top: 446px;
                left: 403px;
            }

            #HEADLINE441 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE442 {
                width: 154px;
                top: 446px;
                left: 579px;
            }

            #HEADLINE442 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.2;
            }

            #BUTTON_TEXT443 {
                width: 203px;
                top: 14.2px;
                left: 0px;
            }

            #BUTTON_TEXT443 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(33, 33, 33);
                font-size: 14px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #BUTTON443 {
                width: 218px;
                height: 48px;
                top: 489px;
                left: 378.5px;
            }

            #BUTTON443 > .ladi-button > .ladi-button-background {
                background-color: rgb(255, 235, 59);
            }

            #BUTTON443 > .ladi-button {
                border-radius: 0px;
            }

            #BUTTON443.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #HEADLINE513 {
                width: 198px;
                top: 69px;
                left: 381px;
            }

            #HEADLINE513 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(66, 66, 66);
                font-size: 24px;
                font-weight: bold;
                text-align: center;
                letter-spacing: 2px;
                line-height: 1;
            }

            #PARAGRAPH514 {
                width: 480px;
                top: 108px;
                left: 240px;
            }

            #PARAGRAPH514 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(10, 103, 233);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.4;
            }

            #GROUP516 {
                width: 282px;
                height: 126px;
                top: 207px;
                left: 133px;
            }

            #HEADLINE517 {
                width: 283px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE517 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(66, 66, 66);
                font-size: 18px;
                text-align: left;
                letter-spacing: 2px;
                line-height: 1.2;
            }

            #GROUP518 {
                width: 282px;
                height: 40px;
                top: 48px;
                left: 0px;
            }

            #SHAPE519 {
                width: 22.742px;
                height: 20px;
                top: 1px;
                left: 0px;
            }

            #SHAPE519 svg:last-child {
                fill: #000000;
            }

            #PARAGRAPH520 {
                width: 253px;
                top: 0px;
                left: 28.4274px;
            }

            #PARAGRAPH520 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(97, 97, 97);
                font-size: 13px;
                text-align: left;
                line-height: 1.6;
            }

            #GROUP521 {
                width: 193.427px;
                height: 21px;
                top: 77px;
                left: 0px;
            }

            #SHAPE522 {
                width: 22.7418px;
                height: 20px;
                top: 1px;
                left: 0px;
            }

            #SHAPE522 svg:last-child {
                fill: #000000;
            }

            #PARAGRAPH523 {
                width: 165px;
                top: 0px;
                left: 28.4274px;
            }

            #PARAGRAPH523 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(97, 97, 97);
                font-size: 13px;
                text-align: left;
                line-height: 1.6;
            }

            #GROUP524 {
                width: 281.427px;
                height: 21px;
                top: 105px;
                left: 0px;
            }

            #SHAPE525 {
                width: 22.742px;
                height: 20px;
                top: 1px;
                left: 0px;
            }

            #SHAPE525 svg:last-child {
                fill: #000000;
            }

            #PARAGRAPH526 {
                width: 253px;
                top: 0px;
                left: 28.4274px;
            }

            #PARAGRAPH526 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(97, 97, 97);
                font-size: 13px;
                text-align: left;
                line-height: 1.6;
            }

            #GROUP527 {
                width: 249px;
                height: 127px;
                top: 207px;
                left: 579px;
            }

            #GROUP528 {
                width: 249px;
                height: 21px;
                top: 106px;
                left: 0px;
            }

            #SHAPE529 {
                width: 20px;
                height: 20px;
                top: 0px;
                left: 0px;
            }

            #SHAPE529 svg:last-child {
                fill: #000000;
            }

            #PARAGRAPH530 {
                width: 223px;
                top: 1px;
                left: 26px;
            }

            #PARAGRAPH530 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(97, 97, 97);
                font-size: 13px;
                text-align: left;
                line-height: 1.6;
            }

            #GROUP531 {
                width: 170px;
                height: 21px;
                top: 77px;
                left: 0px;
            }

            #SHAPE532 {
                width: 20px;
                height: 20px;
                top: 1px;
                left: 0px;
            }

            #SHAPE532 svg:last-child {
                fill: #000000;
            }

            #PARAGRAPH533 {
                width: 145px;
                top: 0px;
                left: 25px;
            }

            #PARAGRAPH533 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(97, 97, 97);
                font-size: 13px;
                text-align: left;
                line-height: 1.6;
            }

            #GROUP534 {
                width: 248px;
                height: 21px;
                top: 48px;
                left: 0px;
            }

            #SHAPE535 {
                width: 20px;
                height: 20px;
                top: 1px;
                left: 0px;
            }

            #SHAPE535 svg:last-child {
                fill: #000000;
            }

            #PARAGRAPH536 {
                width: 223px;
                top: 0px;
                left: 25px;
            }

            #PARAGRAPH536 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(97, 97, 97);
                font-size: 13px;
                text-align: left;
                line-height: 1.6;
            }

            #HEADLINE537 {
                width: 248px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE537 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(66, 66, 66);
                font-size: 18px;
                text-align: left;
                letter-spacing: 2px;
                line-height: 1.2;
            }

            #BOX557 {
                width: 362px;
                height: 41px;
                top: 205px;
                left: 299px;
            }

            #BOX557 > .ladi-box {
                background-color: rgb(37, 23, 200);
                border-style: ridge;
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 22px;
            }

            #HEADLINE558 {
                width: 363px;
                top: 205px;
                left: 331px;
            }

            #HEADLINE558 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 24px;
                line-height: 1.6;
            }

            #HEADLINE562 {
                width: 715px;
                top: 318px;
                left: 130px;
            }

            #HEADLINE562 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 19px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #BOX563 {
                width: 408.64px;
                height: 39px;
                top: 388px;
                left: 292px;
            }

            #BOX563 > .ladi-box {
                background: #fdfbfb;
                background: -webkit-linear-gradient(202deg, #fdfbfb, #eaedee);
                background: linear-gradient(202deg, #fdfbfb, #eaedee);
                border-style: double;
                border-color: rgb(37, 23, 201);
                border-width: 7px;
                border-radius: 108px;
            }

            #HEADLINE564 {
                width: 419px;
                top: 392.5px;
                left: 292px;
            }

            #HEADLINE564 > .ladi-headline {
                color: rgb(2, 5, 43);
                font-size: 19px;
                text-align: center;
                line-height: 1.6;
            }

            #SECTION566 {
                height: 505.6px;
            }

            #SECTION566 > .ladi-section-background {
                background-color: rgba(235, 243, 254, 0.2);
            }

            #BOX567 {
                width: 1350px;
                height: 174px;
                top: 0px;
                left: -200px;
            }

            #BOX567 > .ladi-box {
                background-color: rgb(7, 58, 145);
            }

            #HEADLINE579 {
                width: 475px;
                top: 53.3px;
                left: -6px;
            }

            #HEADLINE579 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 54px;
                font-weight: bold;
                text-align: center;
                letter-spacing: 5px;
                line-height: 1;
            }

            #HEADLINE581 {
                width: 458px;
                top: 107.3px;
                left: 0px;
            }

            #HEADLINE581 > .ladi-headline {
                color: rgb(255, 235, 60);
                font-size: 16px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE582 {
                width: 146px;
                top: 53.3px;
                left: 595.573px;
            }

            #HEADLINE582 > .ladi-headline {
                color: rgb(245, 245, 245);
                font-size: 16px;
                line-height: 1.6;
            }

            #SHAPE578 {
                width: 20.4978px;
                height: 27.906px;
                top: 0px;
                left: 66.9046px;
            }

            #SHAPE578 svg:last-child {
                fill: rgba(200, 31, 23, 1.0);
            }

            #SHAPE577 {
                width: 23.5118px;
                height: 25.9307px;
                top: 0px;
                left: 31.5163px;
            }

            #SHAPE577 svg:last-child {
                fill: rgba(255, 235, 61, 1.0);
            }

            #SHAPE576 {
                width: 19.4396px;
                height: 26.0705px;
                top: 0.662416px;
                left: 0px;
            }

            #SHAPE576 svg:last-child {
                fill: rgba(37, 23, 201, 1.0);
            }

            #GROUP575 {
                width: 87.4024px;
                height: 27.906px;
                top: 254.094px;
                left: 710.176px;
            }

            #PARAGRAPH574 {
                width: 398px;
                top: 0px;
                left: 0px;
            }

            #PARAGRAPH574 > .ladi-paragraph {
                font-family: "Roboto", sans-serif;
                color: rgb(84, 84, 84);
                font-size: 15px;
                text-align: justify;
                line-height: 1.6;
            }

            #GROUP573 {
                width: 797.578px;
                height: 428px;
                top: 197.6px;
                left: 15.921px;
            }

            #SECTION583 {
                height: 582.9px;
            }

            #GROUP584 {
                width: 417px;
                height: 103px;
                top: 167.955px;
                left: 541px;
            }

            #GROUP585 {
                width: 78.9827px;
                height: 74.9769px;
                top: 0px;
                left: 0px;
            }

            #SHAPE586 {
                width: 56.023px;
                height: 56.023px;
                top: 0px;
                left: 0px;
            }

            #SHAPE586 > .ladi-shape {
                transform: rotate(180deg);
                -webkit-transform: rotate(180deg);
            }

            #SHAPE586 svg:last-child {
                fill: rgba(5, 31, 78, 0.2);
            }

            #SHAPE587 {
                width: 49.9827px;
                height: 49.9539px;
                top: 25.023px;
                left: 29px;
            }

            #SHAPE587 svg:last-child {
                fill: rgba(5, 31, 77, 1.0);
            }

            #HEADLINE588 {
                width: 33px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE588 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 18px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE589 {
                width: 191px;
                top: 0px;
                left: 96px;
            }

            #HEADLINE589 > .ladi-headline {
                color: rgb(7, 58, 145);
                font-size: 18px;
                line-height: 1.6;
            }

            #PARAGRAPH590 {
                width: 321px;
                top: 37px;
                left: 96px;
            }

            #PARAGRAPH590 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 14px;
                text-align: justify;
                line-height: 1.6;
            }

            #GROUP591 {
                width: 419px;
                height: 91.718px;
                top: 280.511px;
                left: 541px;
            }

            #GROUP592 {
                width: 74.5307px;
                height: 91.718px;
                top: 0px;
                left: 0px;
            }

            #SHAPE593 {
                width: 50.9425px;
                height: 63.4902px;
                top: 0px;
                left: 0px;
            }

            #SHAPE593 > .ladi-shape {
                transform: rotate(180deg);
                -webkit-transform: rotate(180deg);
            }

            #SHAPE593 svg:last-child {
                fill: rgba(5, 31, 78, 0.2);
            }

            #SHAPE594 {
                width: 51.7979px;
                height: 64.5191px;
                top: 27.1989px;
                left: 22.7328px;
            }

            #SHAPE594 svg:last-child {
                fill: rgba(5, 31, 77, 1.0);
            }

            #HEADLINE595 {
                width: 30px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE595 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 18px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE596 {
                width: 323px;
                top: 0px;
                left: 96px;
            }

            #HEADLINE596 > .ladi-headline {
                color: rgb(7, 58, 145);
                font-size: 18px;
                line-height: 1.6;
            }

            #PARAGRAPH597 {
                width: 321px;
                top: 40.4466px;
                left: 96px;
            }

            #PARAGRAPH597 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 14px;
                text-align: justify;
                line-height: 1.6;
            }

            #GROUP598 {
                width: 417px;
                height: 81px;
                top: 413.066px;
                left: 541px;
            }

            #GROUP599 {
                width: 78.9827px;
                height: 74.9769px;
                top: 0px;
                left: 0px;
            }

            #SHAPE600 {
                width: 56.023px;
                height: 56.023px;
                top: 0px;
                left: 0px;
            }

            #SHAPE600 > .ladi-shape {
                transform: rotate(180deg);
                -webkit-transform: rotate(180deg);
            }

            #SHAPE600 svg:last-child {
                fill: rgba(5, 31, 78, 0.2);
            }

            #SHAPE601 {
                width: 49.9827px;
                height: 49.9539px;
                top: 25.023px;
                left: 29px;
            }

            #SHAPE601 svg:last-child {
                fill: rgba(5, 31, 77, 1.0);
            }

            #HEADLINE602 {
                width: 33px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE602 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 18px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE603 {
                width: 191px;
                top: 0px;
                left: 96px;
            }

            #HEADLINE603 > .ladi-headline {
                color: rgb(7, 58, 145);
                font-size: 18px;
                line-height: 1.6;
            }

            #PARAGRAPH604 {
                width: 321px;
                top: 37px;
                left: 96px;
            }

            #PARAGRAPH604 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 14px;
                text-align: justify;
                line-height: 1.6;
            }

            #HEADLINE605 {
                width: 490px;
                top: 45.4px;
                left: 0px;
            }

            #HEADLINE605 > .ladi-headline {
                font-family: "Roboto", sans-serif;
                color: rgb(84, 84, 84);
                font-size: 30px;
                font-weight: bold;
                text-align: left;
                line-height: 1.2;
            }

            #IMAGE607 {
                width: 490px;
                height: 383.333px;
                top: 101.733px;
                left: 0px;
            }

            #IMAGE607 > .ladi-image > .ladi-image-background {
                width: 490px;
                height: 383.333px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s800x700/5e0337be8ee1512e0f853b97/85212408_1079152082444746_58076526301675520_n-20200507002220.jpg");
            }

            #GROUP608 {
                width: 417px;
                height: 103px;
                top: 45.4px;
                left: 541px;
            }

            #HEADLINE609 {
                width: 191px;
                top: 0px;
                left: 96px;
            }

            #HEADLINE609 > .ladi-headline {
                color: rgb(7, 58, 145);
                font-size: 18px;
                line-height: 1.6;
            }

            #PARAGRAPH610 {
                width: 321px;
                top: 37px;
                left: 96px;
            }

            #PARAGRAPH610 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 14px;
                text-align: justify;
                line-height: 1.6;
            }

            #SHAPE611 {
                width: 56.023px;
                height: 56.023px;
                top: 0px;
                left: 0px;
            }

            #SHAPE611 > .ladi-shape {
                transform: rotate(180deg);
                -webkit-transform: rotate(180deg);
            }

            #SHAPE611 svg:last-child {
                fill: rgba(13, 98, 242, 0.3);
            }

            #SHAPE612 {
                width: 49.9827px;
                height: 49.9539px;
                top: 25.023px;
                left: 29px;
            }

            #SHAPE612 svg:last-child {
                fill: rgba(7, 58, 145, 1.0);
            }

            #HEADLINE613 {
                width: 33px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE613 > .ladi-headline {
                color: rgb(18, 11, 92);
                font-size: 18px;
                font-weight: bold;
                line-height: 1.6;
            }

            #BUTTON614 {
                width: 203px;
                height: 40px;
                top: 522.9px;
                left: 402px;
            }

            #BUTTON614 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON614.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT614 {
                width: 160px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT614 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #SECTION615 {
                height: 1234.81px;
            }

            #HEADLINE653 {
                width: 461px;
                top: 44px;
                left: 249.5px;
            }

            #HEADLINE653 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1;
            }

            #LINE654 {
                width: 78px;
                top: 94px;
                left: 442.211px;
            }

            #LINE654 > .ladi-line > .ladi-line-container {
                border-top: 5px solid rgb(67 160 246 / 49%);
                border-right: 5px solid rgb(67 160 246 / 49%);
                border-bottom: 5px solid rgb(67 160 246 / 49%);
                border-left: 0px !important;
            }

            #LINE654 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #IMAGE659 {
                width: 219.615px;
                height: 218.83px;
                top: 0px;
                left: 24.192px;
            }

            #IMAGE659 > .ladi-image > .ladi-image-background {
                width: 219.615px;
                height: 220.434px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/xt-02-20200506114830.jpg");
            }

            #IMAGE659 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE660 {
                width: 268px;
                height: 134px;
                top: 112.83px;
                left: 0px;
            }

            #SHAPE660 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE660 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP658 {
                width: 268px;
                height: 246.83px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE663 {
                width: 178px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE663 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE664 {
                width: 135px;
                top: 24px;
                left: 21px;
            }

            #HEADLINE664 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP662 {
                width: 177px;
                height: 38px;
                top: 0px;
                left: 0px;
            }

            #SHAPE666 {
                width: 25.9528px;
                height: 22.5px;
                top: 0px;
                left: 0px;
            }

            #SHAPE666 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE667 {
                width: 25.9528px;
                height: 22.5px;
                top: 0px;
                left: 37.9771px;
            }

            #SHAPE667 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE668 {
                width: 28.3528px;
                height: 22.5px;
                top: 0px;
                left: 75.9543px;
            }

            #SHAPE668 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP665 {
                width: 104.307px;
                height: 22.5px;
                top: 67.9px;
                left: 36.3465px;
            }

            #GROUP661 {
                width: 177px;
                height: 90.4px;
                top: 267.215px;
                left: 45.5px;
            }

            #GROUP657 {
                width: 268px;
                height: 357.615px;
                top: 0px;
                left: 0px;
            }

            #IMAGE671 {
                width: 219.615px;
                height: 218.83px;
                top: 0px;
                left: 24.192px;
            }

            #IMAGE671 > .ladi-image > .ladi-image-background {
                width: 219.615px;
                height: 220.434px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x550/5e0337be8ee1512e0f853b97/hh-20200506033554.jpg");
            }

            #IMAGE671 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE672 {
                width: 268px;
                height: 134px;
                top: 112.83px;
                left: 0px;
            }

            #SHAPE672 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE672 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP670 {
                width: 268px;
                height: 246.83px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE675 {
                width: 178px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE675 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE676 {
                width: 135px;
                top: 24px;
                left: 21px;
            }

            #HEADLINE676 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP674 {
                width: 177px;
                height: 38px;
                top: 0px;
                left: 0px;
            }

            #SHAPE678 {
                width: 25.9528px;
                height: 22.5px;
                top: 0px;
                left: 0px;
            }

            #SHAPE678 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE679 {
                width: 25.9528px;
                height: 22.5px;
                top: 0px;
                left: 37.9771px;
            }

            #SHAPE679 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE680 {
                width: 28.3528px;
                height: 22.5px;
                top: 0px;
                left: 75.9543px;
            }

            #SHAPE680 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP677 {
                width: 104.307px;
                height: 22.5px;
                top: 67.9px;
                left: 36.3465px;
            }

            #GROUP673 {
                width: 177px;
                height: 90.4px;
                top: 267.215px;
                left: 45.5px;
            }

            #GROUP669 {
                width: 268px;
                height: 357.615px;
                top: 0px;
                left: 344.579px;
            }

            #IMAGE683 {
                width: 219.615px;
                height: 218.83px;
                top: 0px;
                left: 24.192px;
            }

            #IMAGE683 > .ladi-image > .ladi-image-background {
                width: 219.615px;
                height: 220.434px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x550/5e0337be8ee1512e0f853b97/hoang-tung-02-20200507004621.jpg");
            }

            #IMAGE683 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE684 {
                width: 268px;
                height: 134px;
                top: 112.83px;
                left: 0px;
            }

            #SHAPE684 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE684 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP682 {
                width: 268px;
                height: 246.83px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE687 {
                width: 178px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE687 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE688 {
                width: 135px;
                top: 24px;
                left: 21px;
            }

            #HEADLINE688 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP686 {
                width: 177px;
                height: 38px;
                top: 0px;
                left: 0px;
            }

            #SHAPE690 {
                width: 25.9528px;
                height: 22.5px;
                top: 0px;
                left: 0px;
            }

            #SHAPE690 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE691 {
                width: 25.9528px;
                height: 22.5px;
                top: 0px;
                left: 37.9771px;
            }

            #SHAPE691 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE692 {
                width: 28.3528px;
                height: 22.5px;
                top: 0px;
                left: 75.9543px;
            }

            #SHAPE692 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP689 {
                width: 104.307px;
                height: 22.5px;
                top: 67.9px;
                left: 36.3465px;
            }

            #GROUP685 {
                width: 177px;
                height: 90.4px;
                top: 267.215px;
                left: 45.5px;
            }

            #GROUP681 {
                width: 268px;
                height: 357.615px;
                top: 0px;
                left: 690.579px;
            }

            #GROUP656 {
                width: 958.579px;
                height: 357.615px;
                top: 466.193px;
                left: 0.710545px;
            }

            #GROUP656.ladi-animation > .ladi-group {
                animation-name: bounceInRight;
                -webkit-animation-name: bounceInRight;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 4s;
                -webkit-animation-duration: 4s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #SECTION718 {
                height: 706.808px;
            }

            #SECTION718 > .ladi-section-background {
                background-color: rgb(255, 255, 255);
            }

            #BOX719 {
                width: 2004px;
                height: 140.086px;
                top: 0px;
                left: 0px;
            }

            #BOX719 > .ladi-box {
                background-color: rgb(33, 33, 33);
            }

            #BOX734 {
                width: 541.166px;
                height: 72.6172px;
                top: 0px;
                left: 0px;
            }

            #BOX734 > .ladi-box {
                background-color: rgb(199, 14, 2);
            }

            #HEADLINE735 {
                width: 501px;
                top: 7.72524px;
                left: 17.7069px;
            }

            #HEADLINE735 > .ladi-headline {
                font-family: "Roboto Slab", serif;
                color: rgb(255, 255, 255);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1;
            }

            #GROUP742 {
                width: 2004px;
                height: 140.086px;
                top: 9.6px;
                left: -433px;
            }

            #GROUP743 {
                width: 541.166px;
                height: 72.6172px;
                top: 33.7344px;
                left: 666px;
            }

            #GROUP743.ladi-animation > .ladi-group {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 2s;
                -webkit-animation-duration: 2s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #HEADLINE788 {
                width: 280px;
                top: 340.75px;
                left: 32.8675px;
            }

            #HEADLINE788 > .ladi-headline {
                color: rgb(234, 242, 254);
                font-size: 12px;
                text-align: center;
                line-height: 1.4;
            }

            #FORM_ITEM785 {
                width: 279.657px;
                height: 40px;
                top: 96px;
                left: 0px;
            }

            #FORM_ITEM784 {
                width: 279.657px;
                height: 40px;
                top: 48px;
                left: 0px;
            }

            #FORM_ITEM783 {
                width: 279.657px;
                height: 40px;
                top: 0px;
                left: 0px;
            }

            #BUTTON_TEXT781 {
                width: 280px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT781 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON781 {
                width: 280.47px;
                height: 40px;
                top: 152px;
                left: 0px;
            }

            #BUTTON781 > .ladi-button > .ladi-button-background {
                background-color: rgb(0, 154, 244);
            }

            #BUTTON781 > .ladi-button {
                border-radius: 0px;
            }

            #FORM780 {
                width: 280.47px;
                height: 192px;
                top: 139.2px;
                left: 32.765px;
            }

            #FORM780 > .ladi-form {
                color: rgb(132, 132, 132);
                font-size: 14px;
                letter-spacing: 0px;
                line-height: 1.6;
            }

            #FORM780 .ladi-form-item .ladi-form-control::placeholder, #FORM780 .ladi-form .ladi-form-item .ladi-form-control-select[data-selected=""], #FORM780 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: rgba(132, 132, 132, 1.0);
            }

            #FORM780 .ladi-form-item {
                padding-left: 8px;
                padding-right: 8px;
            }

            #FORM780 .ladi-form-item.ladi-form-checkbox {
                padding-left: 13px;
                padding-right: 13px;
            }

            #FORM780 .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20rgba(132%2C%20132%2C%20132%2C%201.0)%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
            }

            #FORM780 .ladi-form-item-container {
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 1px;
                border-radius: 0px;
            }

            #FORM780 .ladi-form-item-background {
                background-color: rgb(255, 255, 255);
            }

            #BOX769 {
                width: 360px;
                height: 54.163px;
                top: 63.75px;
                left: 0px;
            }

            #BOX769 > .ladi-box {
                background-color: rgb(234, 242, 254);
            }

            #BOX768 {
                width: 360px;
                height: 470.4px;
                top: 0px;
                left: 0px;
            }

            #BOX768 > .ladi-box {
                background-color: rgb(10, 103, 233);
            }

            #GROUP767 {
                width: 360px;
                height: 470.4px;
                top: 169.85px;
                left: 581.297px;
            }

            #GROUP770 {
                width: 235.219px;
                height: 54.163px;
                top: 63.75px;
                left: 55.094px;
            }

            #COUNTDOWN771 {
                width: 234.525px;
                height: 46.3584px;
                top: 0px;
                left: 0px;
            }

            #COUNTDOWN771 > .ladi-countdown {
                color: rgb(5, 34, 74);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
            }

            #COUNTDOWN771 > .ladi-countdown > .ladi-element {
                width: calc((100% - 16px * 3) / 4);
                margin-right: 16px;
                height: 100%;
            }

            #COUNTDOWN771 > .ladi-countdown .ladi-countdown-background {
                background-color: rgba(6, 34, 75, 0);
                border-radius: 105px;
            }

            #HEADLINE776 {
                width: 48px;
                top: 35.163px;
                left: 0.719643px;
            }

            #HEADLINE776 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE777 {
                width: 48px;
                top: 35.163px;
                left: 62.8862px;
            }

            #HEADLINE777 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE778 {
                width: 48px;
                top: 35.163px;
                left: 125.052px;
            }

            #HEADLINE778 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE779 {
                width: 48px;
                top: 35.163px;
                left: 187.219px;
            }

            #HEADLINE779 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE789 {
                width: 179px;
                top: 196.692px;
                left: 668.149px;
            }

            #HEADLINE789 > .ladi-headline {
                color: rgb(245, 245, 245);
                font-size: 16px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE790 {
                width: 513px;
                top: 288.692px;
                left: 33.5px;
            }

            #HEADLINE790 > .ladi-headline {
                color: rgb(84, 84, 84);
                font-size: 16px;
                font-weight: bold;
                line-height: 1.6;
            }

            #IMAGE791 {
                width: 481.261px;
                height: 154.733px;
                top: 531.825px;
                left: 516.018px;
            }

            #IMAGE791 > .ladi-image > .ladi-image-background {
                width: 481.261px;
                height: 154.733px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s800x500/5e0337be8ee1512e0f853b97/dang-ky-ngay-20200507005830.jpg");
            }

            #IMAGE791.ladi-animation > .ladi-image {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #VIDEO792 {
                width: 437.265px;
                height: 245.961px;
                top: 197.6px;
                left: 522.735px;
            }

            #VIDEO792 > .ladi-video > .ladi-video-background {
                background-size: cover;
                background-attachment: scroll;
                background-origin: content-box;
                background-image: url("https://img.youtube.com/vi/b3czYDhk3ok/hqdefault.jpg");
                background-position: center center;
                background-repeat: no-repeat;
            }

            #SHAPE792 {
                width: 60px;
                height: 60px;
                top: 92.9805px;
                left: 188.632px;
            }

            #SHAPE792 svg:last-child {
                fill: rgba(0, 0, 0, 0.5);
            }

            #POPUP806 {
                width: 561px;
                height: 334px;
                top: 0px;
                left: 0px;
                bottom: 0px;
                right: 0px;
                margin: auto;
            }

            #POPUP806 > .ladi-popup > .ladi-popup-background {
                background-color: rgb(255, 255, 255);
            }

            #BOX807 {
                width: 230px;
                height: 334px;
                top: 0px;
                left: 0px;
            }

            #BOX807 > .ladi-box {
                background-color: rgb(27, 38, 51);
            }

            #HEADLINE814 {
                width: 264px;
                top: 54px;
                left: 253.5px;
            }

            #HEADLINE814 > .ladi-headline {
                font-family: "Montserrat", sans-serif;
                color: rgb(0, 0, 0);
                font-size: 24px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.2;
            }

            #PARAGRAPH815 {
                width: 257px;
                top: 99px;
                left: 267px;
            }

            #PARAGRAPH815 > .ladi-paragraph {
                font-family: "Roboto", sans-serif;
                color: rgb(54, 145, 117);
                font-size: 20px;
                text-align: center;
                letter-spacing: 0px;
                line-height: 1.4;
            }

            #FORM816 {
                width: 272px;
                height: 132px;
                top: 158px;
                left: 260px;
            }

            #FORM816 > .ladi-form {
                color: rgb(89, 89, 89);
                font-size: 16px;
                line-height: 1;
            }

            #FORM816 .ladi-form-item .ladi-form-control::placeholder, #FORM816 .ladi-form .ladi-form-item .ladi-form-control-select[data-selected=""], #FORM816 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: rgba(89, 89, 89, 1);
            }

            #FORM816 .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20rgba(89%2C89%2C89%2C1)%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
            }

            #FORM816 .ladi-form-item-container {
                border-style: solid;
                border-color: rgb(38, 38, 38);
                border-width: 2px;
            }

            #FORM816 .ladi-form-item-background {
                background-color: rgb(255, 255, 255);
            }

            #FORM_ITEM817 {
                width: 269px;
                height: 44px;
                top: 44px;
                left: 0px;
            }

            #BUTTON818 {
                width: 271px;
                height: 40px;
                top: 92px;
                left: 0.5px;
            }

            #BUTTON818 > .ladi-button > .ladi-button-background {
                background-color: rgb(27, 38, 51);
            }

            #BUTTON818 > .ladi-button {
                box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 1);
                -webkit-box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 1);
                border-radius: 0px;
            }

            #BUTTON_TEXT818 {
                width: 271px;
                top: 6px;
                left: 0px;
            }

            #BUTTON_TEXT818 > .ladi-headline {
                font-family: "Roboto", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                letter-spacing: 1px;
                line-height: 2;
            }

            #HEADLINE821 {
                width: 217px;
                top: 54px;
                left: 6.5px;
            }

            #HEADLINE821 > .ladi-headline {
                color: rgb(255, 235, 62);
                font-size: 27px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #FORM_ITEM822 {
                width: 272px;
                height: 35px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE823 {
                width: 150px;
                top: 475.5px;
                left: 720px;
            }

            #HEADLINE823 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 16px;
                font-weight: bold;
                font-style: italic;
                line-height: 1.6;
            }

            #IMAGE824 {
                width: 476.48px;
                height: 248.167px;
                top: 427.658px;
                left: 33.5px;
            }

            #IMAGE824 > .ladi-image > .ladi-image-background {
                width: 476.48px;
                height: 248.167px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s800x550/5e0337be8ee1512e0f853b97/55881731_657806031306841_2510107796930822144_n-20200507031307.jpg");
            }

            #HEADLINE882 {
                width: 150px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE882 > .ladi-headline {
                color: rgb(245, 245, 245);
                font-size: 16px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE885 {
                width: 150px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE885 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                line-height: 1.6;
            }

            #BOX924 {
                width: 282px;
                height: 58px;
                top: 196.692px;
                left: 83px;
            }

            #BOX924 > .ladi-box {
                background-color: rgb(33, 33, 33);
            }

            #HEADLINE927 {
                width: 241px;
                top: 213.192px;
                left: 103.5px;
            }

            #HEADLINE927 > .ladi-headline {
                color: rgb(245, 245, 245);
                font-size: 16px;
                font-weight: bold;
                line-height: 1.6;
            }

            #IMAGE944 {
                width: 219.615px;
                height: 218.83px;
                top: 0px;
                left: 24.192px;
            }

            #IMAGE944 > .ladi-image > .ladi-image-background {
                width: 219.615px;
                height: 220.434px;
                top: 0px;
                left: 0px;
            }

            #IMAGE944 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE945 {
                width: 268px;
                height: 134px;
                top: 112.83px;
                left: 0px;
            }

            #SHAPE945 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE945 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP943 {
                width: 268px;
                height: 246.83px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE948 {
                width: 178px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE948 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE949 {
                width: 135px;
                top: 24px;
                left: 21px;
            }

            #HEADLINE949 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP947 {
                width: 177px;
                height: 38px;
                top: 0px;
                left: 0px;
            }

            #SHAPE951 {
                width: 25.9528px;
                height: 22.5px;
                top: 0px;
                left: 0px;
            }

            #SHAPE951 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE952 {
                width: 25.9528px;
                height: 22.5px;
                top: 0px;
                left: 37.9771px;
            }

            #SHAPE952 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE953 {
                width: 28.3528px;
                height: 22.5px;
                top: 0px;
                left: 75.9543px;
            }

            #SHAPE953 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP950 {
                width: 104.307px;
                height: 22.5px;
                top: 67.9px;
                left: 36.3465px;
            }

            #GROUP946 {
                width: 177px;
                height: 90.4px;
                top: 267.215px;
                left: 45.5px;
            }

            #GROUP942 {
                width: 268px;
                height: 357.615px;
                top: 0px;
                left: 0px;
            }

            #IMAGE956 {
                width: 219.615px;
                height: 218.83px;
                top: 0px;
                left: 24.192px;
            }

            #IMAGE956 > .ladi-image > .ladi-image-background {
                width: 219.615px;
                height: 220.434px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x550/5e0337be8ee1512e0f853b97/93837421_2519688108347991_6243488516748083200_o-20200506115509.jpg");
            }

            #IMAGE956 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE957 {
                width: 268px;
                height: 134px;
                top: 112.83px;
                left: 0px;
            }

            #SHAPE957 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE957 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP955 {
                width: 268px;
                height: 246.83px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE960 {
                width: 178px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE960 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #SHAPE963 {
                width: 25.9528px;
                height: 22.5px;
                top: 0px;
                left: 0px;
            }

            #SHAPE963 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE964 {
                width: 25.9528px;
                height: 22.5px;
                top: 0px;
                left: 37.9771px;
            }

            #SHAPE964 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE965 {
                width: 28.3528px;
                height: 22.5px;
                top: 0px;
                left: 75.9543px;
            }

            #SHAPE965 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP962 {
                width: 104.307px;
                height: 22.5px;
                top: 67.9px;
                left: 36.3465px;
            }

            #GROUP958 {
                width: 177px;
                height: 90.4px;
                top: 267.215px;
                left: 45.5px;
            }

            #GROUP954 {
                width: 268px;
                height: 357.615px;
                top: 0px;
                left: 344.579px;
            }

            #IMAGE968 {
                width: 219.615px;
                height: 218.83px;
                top: 0px;
                left: 24.192px;
            }

            #IMAGE968 > .ladi-image > .ladi-image-background {
                width: 219.615px;
                height: 220.434px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x550/5e0337be8ee1512e0f853b97/54729144_2050227821680302_7298078153862283264_n-20200506115207.jpg");
            }

            #IMAGE968 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE969 {
                width: 268px;
                height: 134px;
                top: 112.83px;
                left: 0px;
            }

            #SHAPE969 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE969 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP967 {
                width: 268px;
                height: 246.83px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE972 {
                width: 198px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE972 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE973 {
                width: 135px;
                top: 24px;
                left: 21px;
            }

            #HEADLINE973 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP971 {
                width: 197px;
                height: 42px;
                top: 0px;
                left: 0px;
            }

            #SHAPE975 {
                width: 25.9528px;
                height: 22.5px;
                top: 0px;
                left: 0px;
            }

            #SHAPE975 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE976 {
                width: 25.9528px;
                height: 22.5px;
                top: 0px;
                left: 37.9771px;
            }

            #SHAPE976 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE977 {
                width: 28.3528px;
                height: 22.5px;
                top: 0px;
                left: 75.9543px;
            }

            #SHAPE977 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP974 {
                width: 104.307px;
                height: 22.5px;
                top: 67.9px;
                left: 36.3465px;
            }

            #GROUP970 {
                width: 197px;
                height: 90.4px;
                top: 267.215px;
                left: 45.5px;
            }

            #GROUP966 {
                width: 268px;
                height: 357.615px;
                top: 0px;
                left: 690.579px;
            }

            #GROUP941 {
                width: 958.579px;
                height: 357.615px;
                top: 115px;
                left: 1.42155px;
            }

            #GROUP941.ladi-animation > .ladi-group {
                animation-name: bounceInLeft;
                -webkit-animation-name: bounceInLeft;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #PARAGRAPH978 {
                width: 469px;
                top: 18.3214px;
                left: 52.5px;
            }

            #PARAGRAPH978 > .ladi-paragraph {
                color: rgb(255, 255, 255);
                font-size: 16px;
                line-height: 1.6;
            }

            #IMAGE979 {
                width: 118.965px;
                height: 35.2334px;
                top: 64.8833px;
                left: 679.608px;
            }

            #IMAGE979 > .ladi-image > .ladi-image-background {
                width: 118.965px;
                height: 35.2334px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x350/5e0337be8ee1512e0f853b97/vinalink-logo-20200507082310.png");
            }

            #IMAGE980 {
                width: 50.5px;
                height: 50.5px;
                top: 44.3214px;
                left: 808.5px;
            }

            #IMAGE980 > .ladi-image > .ladi-image-background {
                width: 50.5px;
                height: 50.5px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s400x400/5e0337be8ee1512e0f853b97/download-20200507082317.png");
            }

            #IMAGE982 {
                width: 50.5px;
                height: 50.5px;
                top: 44.3214px;
                left: 866px;
            }

            #IMAGE982 > .ladi-image > .ladi-image-background {
                width: 50.5px;
                height: 50.5px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s400x400/5e0337be8ee1512e0f853b97/vag-20200507082605.jpg");
            }

            #PARAGRAPH983 {
                width: 469px;
                top: 44.3214px;
                left: 687.5px;
            }

            #PARAGRAPH983 > .ladi-paragraph {
                color: rgb(255, 255, 255);
                font-size: 14px;
                line-height: 1.6;
            }

            #HEADLINE986 {
                width: 264px;
                top: 54px;
                left: 253.5px;
            }

            #HEADLINE986 > .ladi-headline {
                font-family: "Montserrat", sans-serif;
                color: rgb(0, 0, 0);
                font-size: 24px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.2;
            }

            #PARAGRAPH987 {
                width: 257px;
                top: 99px;
                left: 267px;
            }

            #PARAGRAPH987 > .ladi-paragraph {
                font-family: "Roboto", sans-serif;
                color: rgb(54, 145, 117);
                font-size: 20px;
                text-align: center;
                letter-spacing: 0px;
                line-height: 1.4;
            }

            #FORM_ITEM989 {
                width: 269px;
                height: 44px;
                top: 44px;
                left: 0px;
            }

            #BUTTON_TEXT990 {
                width: 271px;
                top: 6px;
                left: 0px;
            }

            #BUTTON_TEXT990 > .ladi-headline {
                font-family: "Roboto", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                letter-spacing: 1px;
                line-height: 2;
            }

            #BUTTON990 {
                width: 271px;
                height: 40px;
                top: 92px;
                left: 0.5px;
            }

            #BUTTON990 > .ladi-button > .ladi-button-background {
                background-color: rgb(27, 38, 51);
            }

            #BUTTON990 > .ladi-button {
                box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 1);
                -webkit-box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 1);
                border-radius: 0px;
            }

            #FORM_ITEM992 {
                width: 272px;
                height: 35px;
                top: 0px;
                left: 0px;
            }

            #FORM988 {
                width: 272px;
                height: 132px;
                top: 158px;
                left: 260px;
            }

            #FORM988 > .ladi-form {
                color: rgb(89, 89, 89);
                font-size: 16px;
                line-height: 1;
            }

            #FORM988 .ladi-form-item .ladi-form-control::placeholder, #FORM988 .ladi-form .ladi-form-item .ladi-form-control-select[data-selected=""], #FORM988 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: rgba(89, 89, 89, 1);
            }

            #FORM988 .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20rgba(89%2C89%2C89%2C1)%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
            }

            #FORM988 .ladi-form-item-container {
                border-style: solid;
                border-color: rgb(38, 38, 38);
                border-width: 2px;
            }

            #FORM988 .ladi-form-item-background {
                background-color: rgb(255, 255, 255);
            }

            #BOX993 {
                width: 230px;
                height: 334px;
                top: 0px;
                left: 0px;
            }

            #BOX993 > .ladi-box {
                background-color: rgb(27, 38, 51);
            }

            #HEADLINE994 {
                width: 217px;
                top: 54px;
                left: 6.5px;
            }

            #HEADLINE994 > .ladi-headline {
                color: rgb(255, 235, 62);
                font-size: 27px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #POPUP985 {
                width: 561px;
                height: 334px;
                top: 0px;
                left: 0px;
                bottom: 0px;
                right: 0px;
                margin: auto;
            }

            #POPUP985 > .ladi-popup > .ladi-popup-background {
                background-color: rgb(255, 255, 255);
            }

            #IMAGE998 {
                width: 219.615px;
                height: 229.47px;
                top: 0px;
                left: 24.192px;
            }

            #IMAGE998 > .ladi-image > .ladi-image-background {
                width: 219.615px;
                height: 229.47px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x550/5e0337be8ee1512e0f853b97/minh-tam-20200514085203.jpg");
            }

            #IMAGE998 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE999 {
                width: 268px;
                height: 140.515px;
                top: 118.316px;
                left: 0px;
            }

            #SHAPE999 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE999 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP997 {
                width: 268px;
                height: 258.831px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1002 {
                width: 178px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1002 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE1003 {
                width: 135px;
                top: 25.1669px;
                left: 21px;
            }

            #HEADLINE1003 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP1001 {
                width: 177px;
                height: 54.5282px;
                top: 0px;
                left: 0px;
            }

            #SHAPE1005 {
                width: 25.9528px;
                height: 23.5939px;
                top: 0px;
                left: 0px;
            }

            #SHAPE1005 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE1006 {
                width: 25.9528px;
                height: 23.5939px;
                top: 0px;
                left: 37.9771px;
            }

            #SHAPE1006 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE1007 {
                width: 28.3528px;
                height: 23.5939px;
                top: 0px;
                left: 75.9543px;
            }

            #SHAPE1007 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP1004 {
                width: 104.307px;
                height: 23.5939px;
                top: 71.2013px;
                left: 36.3465px;
            }

            #GROUP1000 {
                width: 177px;
                height: 94.7952px;
                top: 280.207px;
                left: 45.5px;
            }

            #GROUP996 {
                width: 268px;
                height: 375.002px;
                top: 0px;
                left: 0px;
            }

            #IMAGE1010 {
                width: 219.615px;
                height: 229.47px;
                top: 0px;
                left: 24.192px;
            }

            #IMAGE1010 > .ladi-image > .ladi-image-background {
                width: 219.615px;
                height: 229.47px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x550/5e0337be8ee1512e0f853b97/74479509_2551697195047712_19365436307013632_n-20200514085323.jpg");
            }

            #IMAGE1010 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE1011 {
                width: 268px;
                height: 140.515px;
                top: 118.316px;
                left: 0px;
            }

            #SHAPE1011 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE1011 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP1009 {
                width: 268px;
                height: 258.831px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1014 {
                width: 178px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1014 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE1015 {
                width: 135px;
                top: 50.3337px;
                left: 21px;
            }

            #HEADLINE1015 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP1013 {
                width: 177px;
                height: 78.3337px;
                top: 0px;
                left: 0px;
            }

            #SHAPE1017 {
                width: 25.9528px;
                height: 23.5939px;
                top: 0px;
                left: 0px;
            }

            #SHAPE1017 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE1018 {
                width: 25.9528px;
                height: 23.5939px;
                top: 0px;
                left: 37.9771px;
            }

            #SHAPE1018 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE1019 {
                width: 28.3528px;
                height: 23.5939px;
                top: 0px;
                left: 75.9543px;
            }

            #SHAPE1019 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP1016 {
                width: 104.307px;
                height: 23.5939px;
                top: 79.6951px;
                left: 36.3465px;
            }

            #GROUP1012 {
                width: 177px;
                height: 103.289px;
                top: 280.207px;
                left: 45.5px;
            }

            #GROUP1008 {
                width: 268px;
                height: 383.496px;
                top: 0px;
                left: 344.579px;
            }

            #IMAGE1022 {
                width: 219.615px;
                height: 229.47px;
                top: 0px;
                left: 24.1925px;
            }

            #IMAGE1022 > .ladi-image > .ladi-image-background {
                width: 219.615px;
                height: 229.47px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x550/5e0337be8ee1512e0f853b97/95860013_266312071077934_5498025049069715456_o-20200514085530.jpg");
            }

            #IMAGE1022 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE1023 {
                width: 268px;
                height: 140.515px;
                top: 118.316px;
                left: 0px;
            }

            #SHAPE1023 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE1023 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP1021 {
                width: 268px;
                height: 258.831px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1026 {
                width: 178px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1026 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE1027 {
                width: 135px;
                top: 25.1668px;
                left: 21px;
            }

            #HEADLINE1027 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP1025 {
                width: 177px;
                height: 53.1668px;
                top: 0px;
                left: 0px;
            }

            #SHAPE1029 {
                width: 25.9528px;
                height: 23.5939px;
                top: 0px;
                left: 0px;
            }

            #SHAPE1029 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE1030 {
                width: 25.9528px;
                height: 23.5939px;
                top: 0px;
                left: 37.9771px;
            }

            #SHAPE1030 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE1031 {
                width: 28.3528px;
                height: 23.5939px;
                top: 0px;
                left: 75.9543px;
            }

            #SHAPE1031 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP1028 {
                width: 104.307px;
                height: 23.5939px;
                top: 71.2013px;
                left: 36.3465px;
            }

            #GROUP1024 {
                width: 177px;
                height: 94.7952px;
                top: 280.207px;
                left: 45.5px;
            }

            #GROUP1020 {
                width: 268px;
                height: 375.002px;
                top: 0px;
                left: 690.579px;
            }

            #GROUP995 {
                width: 958.579px;
                height: 383.496px;
                top: 823.808px;
                left: 0.710545px;
            }

            #GROUP995.ladi-animation > .ladi-group {
                animation-name: bounceIn;
                -webkit-animation-name: bounceIn;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 4s;
                -webkit-animation-duration: 4s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #SECTION1032 {
                height: 745.3px;
            }

            #SECTION1032 > .ladi-section-background {
                background-color: rgb(255, 255, 255);
            }

            #HEADLINE1033 {
                width: 945px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1033 > .ladi-headline {
                color: rgb(11, 103, 230);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1;
            }

            #GROUP1036 {
                width: 444.228px;
                height: 208px;
                top: 54px;
                left: 0px;
            }

            #BOX1037 {
                width: 370.112px;
                height: 208px;
                top: 0px;
                left: 74.1162px;
            }

            #BOX1037 > .ladi-box {
                background-color: rgb(255, 255, 255);
                border-style: solid;
                border-color: rgb(11, 104, 230);
                border-width: 2px;
                border-radius: 30px;
            }

            #GROUP1038 {
                width: 150.343px;
                height: 160.25px;
                top: 23.875px;
                left: 0px;
            }

            #BOX1039 {
                width: 150.343px;
                height: 160.25px;
                top: 0px;
                left: 0px;
            }

            #BOX1039 > .ladi-box {
                background: rgba(11, 106, 230, 1.0);
                background: -webkit-linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                background: linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                border-radius: 1000px;
            }

            #IMAGE1040 {
                width: 137.688px;
                height: 146.236px;
                top: 7.00707px;
                left: 6.32777px;
            }

            #IMAGE1040 > .ladi-image > .ladi-image-background {
                width: 281.48px;
                height: 147.308px;
                top: -1.072px;
                left: -132px;
                background-image: url("https://w.ladicdn.com/s600x450/5e0337be8ee1512e0f853b97/talkshow01-20200610043345.png");
            }

            #IMAGE1040 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1041 {
                width: 293.68px;
                height: 51.9969px;
                top: 7px;
                left: 150.343px;
            }

            #HEADLINE1043 {
                width: 293px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1043 > .ladi-headline {
                color: rgb(37, 23, 201);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #PARAGRAPH1045 {
                width: 293px;
                top: 39.9969px;
                left: 0.679843px;
            }

            #PARAGRAPH1045 > .ladi-paragraph {
                color: rgb(5, 34, 74);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #GROUP1046 {
                width: 444.228px;
                height: 208px;
                top: 54px;
                left: 515.772px;
            }

            #BOX1047 {
                width: 370.112px;
                height: 208px;
                top: 0px;
                left: 74.1162px;
            }

            #BOX1047 > .ladi-box {
                background-color: rgb(255, 255, 255);
                border-style: solid;
                border-color: rgb(11, 104, 230);
                border-width: 2px;
                border-radius: 30px;
            }

            #GROUP1048 {
                width: 150.343px;
                height: 160.25px;
                top: 23.875px;
                left: 0px;
            }

            #BOX1049 {
                width: 150.343px;
                height: 160.25px;
                top: 0px;
                left: 0px;
            }

            #BOX1049 > .ladi-box {
                background: rgba(11, 106, 230, 1.0);
                background: -webkit-linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                background: linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                border-radius: 1000px;
            }

            #IMAGE1050 {
                width: 137.688px;
                height: 146.236px;
                top: 7.00707px;
                left: 6.32777px;
            }

            #IMAGE1050 > .ladi-image > .ladi-image-background {
                width: 281.48px;
                height: 147.308px;
                top: 0px;
                left: -126px;
                background-image: url("https://w.ladicdn.com/s600x450/5e0337be8ee1512e0f853b97/ban-sao-cua-talkshow01-2-20200610043411.png");
            }

            #IMAGE1050 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
            }

            #HEADLINE1053 {
                width: 239px;
                top: 0px;
                left: 150.343px;
            }

            #HEADLINE1053 > .ladi-headline {
                color: rgb(37, 23, 201);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #BUTTON1093 {
                width: 167px;
                height: 40px;
                top: 681.69px;
                left: 450px;
            }

            #BUTTON1093 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1093.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT1093 {
                width: 121px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1093 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE1094 {
                width: 221px;
                top: 603.69px;
                left: 423px;
            }

            #HEADLINE1094 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 25px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE1096 {
                width: 296px;
                top: 643.69px;
                left: 385.5px;
            }

            #HEADLINE1096 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                font-weight: bold;
                line-height: 1.6;
            }

            #POPUP1097 {
                width: 388px;
                height: 358px;
                top: 0px;
                left: 0px;
                bottom: 0px;
                right: 0px;
                margin: auto;
            }

            #GROUP1098 {
                width: 387.375px;
                height: 231px;
                top: 0px;
                left: 0px;
            }

            #IMAGE1099 {
                width: 387.375px;
                height: 231px;
                top: 0px;
                left: 0px;
            }

            #IMAGE1099 > .ladi-image > .ladi-image-background {
                width: 387.375px;
                height: 358px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s700x700/57b167c9ca57d39c18a1c57c/nen.png");
            }

            #HEADLINE1100 {
                width: 325px;
                top: 86.415px;
                left: 32px;
            }

            #HEADLINE1100 > .ladi-headline {
                color: rgb(37, 23, 201);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #PARAGRAPH1101 {
                width: 286px;
                top: 165.214px;
                left: 51.5px;
            }

            #PARAGRAPH1101 > .ladi-paragraph {
                color: rgb(33, 33, 33);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.4;
            }

            #LINE1102 {
                width: 48px;
                top: 203.214px;
                left: 170.5px;
            }

            #LINE1102 > .ladi-line > .ladi-line-container {
                border-top: 3px solid rgb(10, 103, 233);
                border-right: 3px solid rgb(10, 103, 233);
                border-bottom: 3px solid rgb(10, 103, 233);
                border-left: 0px !important;
            }

            #LINE1102 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #GROUP1126 {
                width: 412px;
                height: 97px;
                top: 10.1px;
                left: 296px;
            }

            #LINE1127 {
                width: 85px;
                top: 79px;
                left: 163.5px;
            }

            #LINE1127 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(242, 67, 13);
                border-right: 2px solid rgb(242, 67, 13);
                border-bottom: 2px solid rgb(242, 67, 13);
                border-left: 0px !important;
            }

            #LINE1127 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #HEADLINE1129 {
                width: 413px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1129 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(5, 31, 77);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #SECTION1130 {
                height: 162.876px;
            }

            #BUTTON1131 {
                width: 195px;
                height: 40px;
                top: 107.1px;
                left: 288px;
            }

            #BUTTON1131 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1131 > .ladi-button {
                border-style: solid;
                border-color: rgb(0, 0, 0);
                border-width: 1px;
                border-radius: 33px;
            }

            #BUTTON1131.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT1131 {
                width: 193px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1131 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON1132 {
                width: 193px;
                height: 40px;
                top: 107.1px;
                left: 500px;
            }

            #BUTTON1132 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1132 > .ladi-button {
                border-style: solid;
                border-color: rgb(0, 0, 0);
                border-width: 1px;
                border-radius: 33px;
            }

            #BUTTON1132.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT1132 {
                width: 191px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1132 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #PARAGRAPH1176 {
                width: 288px;
                top: 98px;
                left: 168.939px;
            }

            #PARAGRAPH1176 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(0, 154, 244);
                font-size: 13px;
                font-weight: bold;
                line-height: 1.6;
            }

            #PARAGRAPH1177 {
                width: 288px;
                top: 98px;
                left: 672px;
            }

            #PARAGRAPH1177 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(0, 154, 244);
                font-size: 13px;
                font-weight: bold;
                line-height: 1.6;
            }

            #PARAGRAPH1178 {
                width: 288px;
                top: 198px;
                left: 168.939px;
            }

            #PARAGRAPH1178 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(33, 33, 33);
                font-size: 13px;
                font-weight: bold;
                line-height: 1.6;
            }

            #PARAGRAPH1181 {
                width: 288px;
                top: 202px;
                left: 672px;
            }

            #PARAGRAPH1181 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(33, 33, 33);
                font-size: 13px;
                font-weight: bold;
                line-height: 1.6;
            }

            #BOX1185 {
                width: 370.112px;
                height: 208px;
                top: 0px;
                left: 74.1162px;
            }

            #BOX1185 > .ladi-box {
                background-color: rgb(255, 255, 255);
                border-style: solid;
                border-color: rgb(11, 104, 230);
                border-width: 2px;
                border-radius: 30px;
            }

            #BOX1187 {
                width: 150.343px;
                height: 160.25px;
                top: 0px;
                left: 0px;
            }

            #BOX1187 > .ladi-box {
                background: rgba(11, 106, 230, 1.0);
                background: -webkit-linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                background: linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                border-radius: 1000px;
            }

            #IMAGE1188 {
                width: 137.688px;
                height: 146.236px;
                top: 7.00707px;
                left: 6.32777px;
            }

            #IMAGE1188 > .ladi-image > .ladi-image-background {
                width: 281.48px;
                height: 147.308px;
                top: -1.072px;
                left: -124px;
                background-image: url("https://w.ladicdn.com/s600x450/5e0337be8ee1512e0f853b97/ban-sao-cua-talkshow01-3-20200610043454.png");
            }

            #IMAGE1188 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1186 {
                width: 150.343px;
                height: 160.25px;
                top: 23.875px;
                left: 0px;
            }

            #GROUP1184 {
                width: 444.228px;
                height: 208px;
                top: 379.19px;
                left: 496.711px;
            }

            #GROUP1189 {
                width: 293.68px;
                height: 51.9969px;
                top: 7px;
                left: 150.343px;
            }

            #HEADLINE1191 {
                width: 293px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1191 > .ladi-headline {
                color: rgb(37, 23, 201);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #PARAGRAPH1190 {
                width: 293px;
                top: 39.9969px;
                left: 0.679843px;
            }

            #PARAGRAPH1190 > .ladi-paragraph {
                color: rgb(5, 34, 74);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #PARAGRAPH1193 {
                width: 288px;
                top: 527.19px;
                left: 168.939px;
            }

            #PARAGRAPH1193 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(33, 33, 33);
                font-size: 13px;
                font-weight: bold;
                line-height: 1.6;
            }

            #BOX1197 {
                width: 370.112px;
                height: 208px;
                top: 0px;
                left: 74.1162px;
            }

            #BOX1197 > .ladi-box {
                background-color: rgb(255, 255, 255);
                border-style: solid;
                border-color: rgb(11, 104, 230);
                border-width: 2px;
                border-radius: 30px;
            }

            #BOX1199 {
                width: 150.343px;
                height: 160.25px;
                top: 0px;
                left: 0px;
            }

            #BOX1199 > .ladi-box {
                background: rgba(11, 106, 230, 1.0);
                background: -webkit-linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                background: linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                border-radius: 1000px;
            }

            #IMAGE1200 {
                width: 137.688px;
                height: 146.236px;
                top: 7.00707px;
                left: 6.32777px;
            }

            #IMAGE1200 > .ladi-image > .ladi-image-background {
                width: 281.48px;
                height: 147.308px;
                top: -1.072px;
                left: -126px;
                background-image: url("https://w.ladicdn.com/s600x450/5e0337be8ee1512e0f853b97/ban-sao-cua-ban-sao-cua-talkshow01-2-20200610043436.png");
            }

            #IMAGE1200 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1198 {
                width: 150.343px;
                height: 160.25px;
                top: 23.875px;
                left: 0px;
            }

            #PARAGRAPH1202 {
                width: 293px;
                top: 39.9969px;
                left: 0.679843px;
            }

            #PARAGRAPH1202 > .ladi-paragraph {
                color: rgb(5, 34, 74);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #HEADLINE1203 {
                width: 293px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1203 > .ladi-headline {
                color: rgb(37, 23, 201);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP1201 {
                width: 293.68px;
                height: 51.9969px;
                top: 7px;
                left: 150.343px;
            }

            #GROUP1196 {
                width: 444.228px;
                height: 208px;
                top: 379.19px;
                left: 0.710545px;
            }

            #PARAGRAPH1204 {
                width: 288px;
                top: 429px;
                left: 662.5px;
            }

            #PARAGRAPH1204 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(0, 154, 244);
                font-size: 13px;
                font-weight: bold;
                line-height: 1.6;
            }

            #PARAGRAPH1205 {
                width: 288px;
                top: 525px;
                left: 672px;
            }

            #PARAGRAPH1205 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(33, 33, 33);
                font-size: 13px;
                font-weight: bold;
                line-height: 1.6;
            }

            #SECTION1262 {
                height: 393.9px;
                display: none !important;
            }

            #SECTION1262 > .ladi-section-background {
                background-color: rgb(234, 242, 255);
            }

            #FRAME1264 {
                width: 278px;
                height: 353px;
                top: 23.376px;
                left: 26px;
            }

            #FRAME1264 > .ladi-frame > .ladi-frame-background {
                background-color: rgb(255, 255, 255);
            }

            #FRAME1264 > .ladi-frame {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
            }

            #SHAPE1265 {
                width: 319.249px;
                height: 189.132px;
                top: 248.392px;
                left: -25.6245px;
            }

            #SHAPE1265 > .ladi-shape {
                transform: rotate(-165deg);
                -webkit-transform: rotate(-165deg);
            }

            #SHAPE1265 svg:last-child {
                fill: url("#SHAPE1265_desktop_gradient");
            }

            #SHAPE1266 {
                width: 28.3984px;
                height: 23.6646px;
                top: 28.896px;
                left: 24.001px;
            }

            #SHAPE1266 svg:last-child {
                fill: rgba(242, 67, 13, 1.0);
            }

            #PARAGRAPH1267 {
                width: 240px;
                top: 63.938px;
                left: 19px;
            }

            #PARAGRAPH1267 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(5, 31, 77);
                font-size: 14px;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE1269 {
                width: 226px;
                top: 304.896px;
                left: 27px;
            }

            #HEADLINE1269 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #FRAME1271 {
                width: 278px;
                height: 353px;
                top: 23.376px;
                left: 347px;
            }

            #FRAME1271 > .ladi-frame > .ladi-frame-background {
                background-color: rgb(255, 255, 255);
            }

            #FRAME1271 > .ladi-frame {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
            }

            #SHAPE1272 {
                width: 319.249px;
                height: 189.132px;
                top: 248.392px;
                left: -25.6245px;
            }

            #SHAPE1272 > .ladi-shape {
                transform: rotate(-165deg);
                -webkit-transform: rotate(-165deg);
            }

            #SHAPE1272 svg:last-child {
                fill: url("#SHAPE1272_desktop_gradient");
            }

            #SHAPE1273 {
                width: 28.3984px;
                height: 23.6646px;
                top: 28.896px;
                left: 24.001px;
            }

            #SHAPE1273 svg:last-child {
                fill: rgba(242, 67, 13, 1.0);
            }

            #PARAGRAPH1274 {
                width: 240px;
                top: 63.938px;
                left: 19px;
            }

            #PARAGRAPH1274 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(5, 31, 77);
                font-size: 14px;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE1276 {
                width: 226px;
                top: 304.896px;
                left: 27px;
            }

            #HEADLINE1276 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #FRAME1278 {
                width: 278px;
                height: 353px;
                top: 23.376px;
                left: 668px;
            }

            #FRAME1278 > .ladi-frame > .ladi-frame-background {
                background-color: rgb(255, 255, 255);
            }

            #FRAME1278 > .ladi-frame {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
            }

            #SHAPE1279 {
                width: 319.249px;
                height: 189.132px;
                top: 248.392px;
                left: -25.6245px;
            }

            #SHAPE1279 > .ladi-shape {
                transform: rotate(-165deg);
                -webkit-transform: rotate(-165deg);
            }

            #SHAPE1279 svg:last-child {
                fill: url("#SHAPE1279_desktop_gradient");
            }

            #SHAPE1280 {
                width: 28.3984px;
                height: 23.6646px;
                top: 28.896px;
                left: 24.001px;
            }

            #SHAPE1280 svg:last-child {
                fill: rgba(242, 67, 13, 1.0);
            }

            #PARAGRAPH1281 {
                width: 240px;
                top: 63.938px;
                left: 19px;
            }

            #PARAGRAPH1281 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(5, 31, 77);
                font-size: 14px;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE1283 {
                width: 226px;
                top: 304.896px;
                left: 27px;
            }

            #HEADLINE1283 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #PARAGRAPH1289 {
                width: 288px;
                top: 429px;
                left: 161.939px;
            }

            #PARAGRAPH1289 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(0, 154, 244);
                font-size: 13px;
                font-weight: bold;
                line-height: 1.6;
            }

            #SECTION1294 {
                height: 397.9px;
                display: none !important;
            }

            #SECTION1294 > .ladi-section-background {
                background-color: rgb(234, 242, 255);
            }

            #FRAME1296 {
                width: 278px;
                height: 353px;
                top: 23.376px;
                left: 361px;
            }

            #FRAME1296 > .ladi-frame > .ladi-frame-background {
                background-color: rgb(255, 255, 255);
            }

            #FRAME1296 > .ladi-frame {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
            }

            #SHAPE1297 {
                width: 319.249px;
                height: 189.132px;
                top: 248.392px;
                left: -25.6245px;
            }

            #SHAPE1297 > .ladi-shape {
                transform: rotate(-165deg);
                -webkit-transform: rotate(-165deg);
            }

            #SHAPE1297 svg:last-child {
                fill: url("#SHAPE1297_desktop_gradient");
            }

            #SHAPE1298 {
                width: 28.3984px;
                height: 23.6646px;
                top: 28.896px;
                left: 24.001px;
            }

            #SHAPE1298 svg:last-child {
                fill: rgba(242, 67, 13, 1.0);
            }

            #PARAGRAPH1299 {
                width: 240px;
                top: 63.938px;
                left: 19px;
            }

            #PARAGRAPH1299 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(5, 31, 77);
                font-size: 14px;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE1301 {
                width: 226px;
                top: 304.896px;
                left: 27px;
            }

            #HEADLINE1301 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE1302 {
                width: 204px;
                top: 325.389px;
                left: 39px;
            }

            #HEADLINE1302 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 12px;
                font-style: italic;
                text-align: center;
                line-height: 1.2;
            }

            #BUTTON1322 {
                width: 160px;
                height: 40px;
                top: 218px;
                left: 180.939px;
            }

            #BUTTON1322 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1322.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT1322 {
                width: 127px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1322 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON_TEXT1323 {
                width: 135px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1323 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON1323 {
                width: 160px;
                height: 40px;
                top: 547.19px;
                left: 180.939px;
            }

            #BUTTON1323 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1323.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT1325 {
                width: 135px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1325 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON1325 {
                width: 160px;
                height: 40px;
                top: 222px;
                left: 681.5px;
            }

            #BUTTON1325 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1325.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT1327 {
                width: 127px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1327 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON1327 {
                width: 160px;
                height: 40px;
                top: 545px;
                left: 690px;
            }

            #BUTTON1327 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1327.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #SECTION1329 {
                height: 306.9px;
            }

            #HEADLINE1330 {
                width: 617px;
                top: 0px;
                left: 209.5px;
            }

            #HEADLINE1330 > .ladi-headline {
                color: rgb(13, 98, 242);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #IMAGE1340 {
                width: 135.847px;
                height: 40.2334px;
                top: 113.776px;
                left: 133px;
            }

            #IMAGE1340 > .ladi-image > .ladi-image-background {
                width: 135.847px;
                height: 40.2334px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x350/5e0337be8ee1512e0f853b97/vinalink-logo-20200507082310.png");
            }

            #IMAGE1341 {
                width: 85.3833px;
                height: 85.3833px;
                top: 192.63px;
                left: 0.82135px;
            }

            #IMAGE1341 > .ladi-image > .ladi-image-background {
                width: 85.3833px;
                height: 85.3833px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s400x400/5e0337be8ee1512e0f853b97/download-20200507082317.png");
            }

            #IMAGE1342 {
                width: 87.026px;
                height: 87.026px;
                top: 82.3797px;
                left: 0px;
            }

            #IMAGE1342 > .ladi-image > .ladi-image-background {
                width: 87.026px;
                height: 87.026px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s400x400/5e0337be8ee1512e0f853b97/vag-20200507082605.jpg");
            }

            #IMAGE1343 {
                width: 98px;
                height: 98px;
                top: 200.63px;
                left: 330.83px;
            }

            #IMAGE1343 > .ladi-image > .ladi-image-background {
                width: 98px;
                height: 98px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s400x400/5e0337be8ee1512e0f853b97/04f4d6de12efe8b1b1fe-20200520040203.jpg");
            }

            #IMAGE1344 {
                width: 161px;
                height: 48.7431px;
                top: 217.258px;
                left: 133px;
            }

            #IMAGE1344 > .ladi-image > .ladi-image-background {
                width: 161px;
                height: 48.7431px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s500x350/5e0337be8ee1512e0f853b97/99296459_259634028741024_1548725250856845312_n-20200527024314.png");
            }

            #IMAGE1345 {
                width: 200px;
                height: 66.3571px;
                top: 265.224px;
                left: -249px;
            }

            #IMAGE1345 > .ladi-image > .ladi-image-background {
                width: 200px;
                height: 66.3571px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x400/5e0337be8ee1512e0f853b97/logo-i-mentor-1-20200506234942.png");
            }

            #IMAGE1346 {
                width: 105px;
                height: 105px;
                top: 82.3797px;
                left: 330.83px;
            }

            #IMAGE1346 > .ladi-image > .ladi-image-background {
                width: 105px;
                height: 105px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/logo--20200527024738.jpg");
            }

            #IMAGE1347 {
                width: 107.275px;
                height: 107.275px;
                top: 82.3797px;
                left: 518px;
            }

            #IMAGE1347 > .ladi-image > .ladi-image-background {
                width: 107.275px;
                height: 107.275px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/ea485681d93623687a27-20200527030229.jpg");
            }

            #SECTION1348 {
                height: 203.2px;
            }

            #SECTION1348 > .ladi-section-background {
                background-color: rgb(255, 255, 255);
            }

            #HEADLINE1355 {
                width: 617px;
                top: 26.2px;
                left: 191.5px;
            }

            #HEADLINE1355 > .ladi-headline {
                color: rgb(13, 98, 242);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #IMAGE1358 {
                width: 124.669px;
                height: 115.318px;
                top: 82.441px;
                left: 607.331px;
            }

            #IMAGE1358 > .ladi-image > .ladi-image-background {
                width: 124.669px;
                height: 124.668px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/b45645a788e275bc2cf3-20200603024122.jpg");
            }

            #IMAGE556 {
                width: 230.14px;
                height: 76.3571px;
                top: 44.3214px;
                left: 0px;
            }

            #IMAGE556 > .ladi-image > .ladi-image-background {
                width: 230.14px;
                height: 76.3571px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x400/5e0337be8ee1512e0f853b97/logo-i-mentor-1-1-20200506235002.png");
            }

            #IMAGE1359 {
                width: 222.659px;
                height: 70.3571px;
                top: 96.227px;
                left: 272px;
            }

            #IMAGE1359 > .ladi-image > .ladi-image-background {
                width: 222.659px;
                height: 70.3571px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x400/5e0337be8ee1512e0f853b97/logo-i-mentor-1-20200506234942.png");
            }

            #SECTION1360 {
                height: 509.9px;
            }

            #GROUP1361 {
                width: 369px;
                height: 369px;
                top: 84.973px;
                left: 295.5px;
            }

            #BOX1362 {
                width: 369px;
                height: 369px;
                top: 0px;
                left: 0px;
            }

            #BOX1362 > .ladi-box {
                background-color: rgb(234, 242, 254);
                border-radius: 1000px;
            }

            #HEADLINE1363 {
                width: 332px;
                top: 142.5px;
                left: 18.5px;
            }

            #HEADLINE1363 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1.4;
            }

            #GROUP1364 {
                width: 384.881px;
                height: 180.655px;
                top: 84.973px;
                left: 0.3335px;
            }

            #IMAGE1365 {
                width: 181.302px;
                height: 180.655px;
                top: 0px;
                left: 203.579px;
            }

            #IMAGE1365 > .ladi-image > .ladi-image-background {
                width: 181.302px;
                height: 181.977px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s500x500/5e0337be8ee1512e0f853b97/24261005-start-up-business-concept-stock-photo-1-20200603031942.jpg");
            }

            #IMAGE1365 > .ladi-image {
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1366 {
                width: 209px;
                height: 69px;
                top: 40.327px;
                left: 0px;
            }

            #LINE1367 {
                width: 178px;
                top: 40px;
                left: 29px;
            }

            #LINE1367 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(10, 103, 233);
                border-right: 2px solid rgb(10, 103, 233);
                border-bottom: 2px solid rgb(10, 103, 233);
                border-left: 0px !important;
            }

            #LINE1367 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #BOX1368 {
                width: 33px;
                height: 33px;
                top: 32.5px;
                left: 0px;
            }

            #BOX1368 > .ladi-box {
                background: #fdfbfb;
                background: -webkit-linear-gradient(180deg, #fdfbfb, #eaedee);
                background: linear-gradient(180deg, #fdfbfb, #eaedee);
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 2px;
                border-radius: 1000px;
            }

            #HEADLINE1369 {
                width: 176px;
                top: 0px;
                left: 33px;
            }

            #HEADLINE1369 > .ladi-headline {
                color: rgb(248, 5, 5);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #PARAGRAPH1370 {
                width: 142px;
                top: 57px;
                left: 44px;
            }

            #PARAGRAPH1370 > .ladi-paragraph {
                color: rgb(5, 34, 74);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #HEADLINE1371 {
                width: 25px;
                top: 36.5px;
                left: 4px;
            }

            #HEADLINE1371 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP1372 {
                width: 384.881px;
                height: 180.655px;
                top: 294.973px;
                left: 0.3335px;
            }

            #IMAGE1373 {
                width: 181.302px;
                height: 180.655px;
                top: 0px;
                left: 203.579px;
            }

            #IMAGE1373 > .ladi-image > .ladi-image-background {
                width: 482.969px;
                height: 286.232px;
                top: -50px;
                left: -124px;
                background-image: url("https://w.ladicdn.com/s800x600/5e0337be8ee1512e0f853b97/meeting-business-people-avatar-character_24877-57276-20200603033232.jpg");
            }

            #IMAGE1373 > .ladi-image {
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1374 {
                width: 207px;
                height: 66.5px;
                top: 39.327px;
                left: 0px;
            }

            #LINE1375 {
                width: 178px;
                top: 41px;
                left: 29px;
            }

            #LINE1375 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(10, 103, 233);
                border-right: 2px solid rgb(10, 103, 233);
                border-bottom: 2px solid rgb(10, 103, 233);
                border-left: 0px !important;
            }

            #LINE1375 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #BOX1376 {
                width: 33px;
                height: 33px;
                top: 33.5px;
                left: 0px;
            }

            #BOX1376 > .ladi-box {
                background: #fdfbfb;
                background: -webkit-linear-gradient(180deg, #fdfbfb, #eaedee);
                background: linear-gradient(180deg, #fdfbfb, #eaedee);
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 2px;
                border-radius: 1000px;
            }

            #HEADLINE1377 {
                width: 176px;
                top: 0px;
                left: 30px;
            }

            #HEADLINE1377 > .ladi-headline {
                color: rgb(240, 8, 8);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE1379 {
                width: 25px;
                top: 37.5px;
                left: 4px;
            }

            #HEADLINE1379 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP1380 {
                width: 392.302px;
                height: 180.655px;
                top: 84.973px;
                left: 560.912px;
            }

            #IMAGE1381 {
                width: 181.302px;
                height: 180.655px;
                top: 0px;
                left: 0px;
            }

            #IMAGE1381 > .ladi-image > .ladi-image-background {
                width: 370.909px;
                height: 370.909px;
                top: -24.322px;
                left: -71px;
                background-image: url("https://w.ladicdn.com/s700x700/5e0337be8ee1512e0f853b97/flat-leadership-design_23-2147939846-20200603033413.jpg");
            }

            #IMAGE1381 > .ladi-image {
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1382 {
                width: 211px;
                height: 67.5px;
                top: 33.327px;
                left: 181.302px;
            }

            #LINE1383 {
                width: 178px;
                top: 42px;
                left: 0px;
            }

            #LINE1383 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(10, 103, 233);
                border-right: 2px solid rgb(10, 103, 233);
                border-bottom: 2px solid rgb(10, 103, 233);
                border-left: 0px !important;
            }

            #LINE1383 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #HEADLINE1384 {
                width: 176px;
                top: 0px;
                left: 15px;
            }

            #HEADLINE1384 > .ladi-headline {
                color: rgb(250, 9, 9);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP1386 {
                width: 33px;
                height: 33px;
                top: 34.5px;
                left: 178px;
            }

            #BOX1387 {
                width: 33px;
                height: 33px;
                top: 0px;
                left: 0px;
            }

            #BOX1387 > .ladi-box {
                background: #fdfbfb;
                background: -webkit-linear-gradient(180deg, #fdfbfb, #eaedee);
                background: linear-gradient(180deg, #fdfbfb, #eaedee);
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 2px;
                border-radius: 1000px;
            }

            #HEADLINE1388 {
                width: 25px;
                top: 4px;
                left: 4px;
            }

            #HEADLINE1388 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP1389 {
                width: 392.302px;
                height: 180.655px;
                top: 294.973px;
                left: 567.912px;
            }

            #IMAGE1390 {
                width: 181.302px;
                height: 180.655px;
                top: 0px;
                left: 0px;
            }

            #IMAGE1390 > .ladi-image > .ladi-image-background {
                width: 181.302px;
                height: 181.977px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s500x500/5e0337be8ee1512e0f853b97/man-woman-working-their-job_52683-24245-20200603033623.jpg");
            }

            #IMAGE1390 > .ladi-image {
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1391 {
                width: 211px;
                height: 89.5px;
                top: 11.327px;
                left: 181.302px;
            }

            #LINE1392 {
                width: 178px;
                top: 64px;
                left: 0px;
            }

            #LINE1392 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(10, 103, 233);
                border-right: 2px solid rgb(10, 103, 233);
                border-bottom: 2px solid rgb(10, 103, 233);
                border-left: 0px !important;
            }

            #LINE1392 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #HEADLINE1393 {
                width: 176px;
                top: 0px;
                left: 15px;
            }

            #HEADLINE1393 > .ladi-headline {
                color: rgb(238, 10, 10);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP1395 {
                width: 33px;
                height: 33px;
                top: 56.5px;
                left: 178px;
            }

            #BOX1396 {
                width: 33px;
                height: 33px;
                top: 0px;
                left: 0px;
            }

            #BOX1396 > .ladi-box {
                background: #fdfbfb;
                background: -webkit-linear-gradient(180deg, #fdfbfb, #eaedee);
                background: linear-gradient(180deg, #fdfbfb, #eaedee);
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 2px;
                border-radius: 1000px;
            }

            #HEADLINE1397 {
                width: 25px;
                top: 4px;
                left: 4px;
            }

            #HEADLINE1397 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #SECTION1427 {
                height: 199.9px;
            }

            #SECTION1427 > .ladi-section-background {
                background-color: rgba(235, 243, 255, 0.5);
            }

            #HEADLINE1429 {
                width: 438px;
                top: 10px;
                left: 261px;
            }

            #HEADLINE1429 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP1431 {
                width: 245.213px;
                height: 115px;
                top: 59.45px;
                left: 91.876px;
            }

            #BOX1432 {
                width: 245.213px;
                height: 115px;
                top: 0px;
                left: 0px;
            }

            #BOX1432 > .ladi-box {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                background-color: rgb(255, 255, 255);
            }

            #HEADLINE1433 {
                width: 105px;
                top: 21.9668px;
                left: 61.189px;
            }

            #HEADLINE1433 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 42px;
                text-align: left;
                line-height: 1.2;
            }

            #HEADLINE1433.ladi-animation > .ladi-headline {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #HEADLINE1434 {
                width: 162px;
                top: 66.8886px;
                left: 61.189px;
            }

            #HEADLINE1434 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 14px;
                font-weight: bold;
                text-align: left;
                line-height: 1.6;
            }

            #GROUP1435 {
                width: 54.3515px;
                height: 50.4427px;
                top: 32.2786px;
                left: 6.8375px;
            }

            #BOX1436 {
                width: 54.3515px;
                height: 50.4427px;
                top: 0px;
                left: 0px;
            }

            #BOX1436 > .ladi-box {
                background-color: rgba(255, 255, 255, 0);
                border-style: solid;
                border-color: rgba(10, 103, 233, 0.5);
                border-width: 1px;
                border-radius: 1000px;
            }

            #SHAPE1437 {
                width: 35.4308px;
                height: 32.8827px;
                top: 8.55761px;
                left: 9.53051px;
            }

            #SHAPE1437 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP1438 {
                width: 262.649px;
                height: 115px;
                top: 59.45px;
                left: 342.99px;
            }

            #BOX1439 {
                width: 229.326px;
                height: 115px;
                top: 0px;
                left: 0px;
            }

            #BOX1439 > .ladi-box {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                background-color: rgb(255, 255, 255);
            }

            #HEADLINE1440 {
                width: 214px;
                top: 20.6455px;
                left: 49.0219px;
            }

            #HEADLINE1440 > .ladi-headline {
                color: rgb(242, 67, 13);
                font-size: 42px;
                text-align: left;
                line-height: 1.2;
            }

            #HEADLINE1440.ladi-animation > .ladi-headline {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #HEADLINE1441 {
                width: 136px;
                top: 66.889px;
                left: 87.8174px;
            }

            #HEADLINE1441 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 14px;
                font-weight: bold;
                text-align: left;
                line-height: 1.6;
            }

            #BOX1442 {
                width: 50.83px;
                height: 50.4427px;
                top: 32.2787px;
                left: 0px;
            }

            #BOX1442 > .ladi-box {
                background-color: rgba(255, 255, 255, 0);
                border-style: solid;
                border-color: rgba(10, 103, 233, 0.5);
                border-width: 1px;
                border-radius: 1000px;
            }

            #SHAPE1443 {
                width: 26.4928px;
                height: 26.291px;
                top: 44.3545px;
                left: 12.1686px;
            }

            #SHAPE1443 svg:last-child {
                fill: rgba(5, 31, 77, 1.0);
            }

            #GROUP1444 {
                width: 243.443px;
                height: 115px;
                top: 59.45px;
                left: 627.103px;
            }

            #BOX1445 {
                width: 234.317px;
                height: 115px;
                top: 0px;
                left: 0px;
            }

            #BOX1445 > .ladi-box {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                background-color: rgb(255, 255, 255);
            }

            #HEADLINE1446 {
                width: 192px;
                top: 21.9668px;
                left: 51.9362px;
            }

            #HEADLINE1446 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 42px;
                text-align: left;
                line-height: 1.2;
            }

            #HEADLINE1446.ladi-animation > .ladi-headline {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #HEADLINE1447 {
                width: 139px;
                top: 66.8886px;
                left: 89.7286px;
            }

            #HEADLINE1447 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 14px;
                font-weight: bold;
                text-align: left;
                line-height: 1.6;
            }

            #BOX1448 {
                width: 51.9362px;
                height: 50.4427px;
                top: 32.2787px;
                left: 0px;
            }

            #BOX1448 > .ladi-box {
                background-color: rgba(255, 255, 255, 0);
                border-style: solid;
                border-color: rgba(10, 103, 233, 0.5);
                border-width: 1px;
                border-radius: 1000px;
            }

            #SHAPE1449 {
                width: 30.7849px;
                height: 29.8997px;
                top: 42.0671px;
                left: 10.5756px;
            }

            #SHAPE1449 svg:last-child {
                fill: rgba(5, 31, 77, 1.0);
            }

            #IMAGE1457 {
                width: 200px;
                height: 68.6734px;
                top: 229.957px;
                left: 477px;
            }

            #IMAGE1457 > .ladi-image > .ladi-image-background {
                width: 200px;
                height: 68.6734px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x400/5e0337be8ee1512e0f853b97/105288713_190068589094741_4828959623268428556_n-20200627030256.png");
            }

            #IMAGE1458 {
                width: 200px;
                height: 51.8607px;
                top: 99.9623px;
                left: 699px;
            }

            #IMAGE1458 > .ladi-image > .ladi-image-background {
                width: 200px;
                height: 51.8607px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x400/5e0337be8ee1512e0f853b97/104493260_2639813402930081_8900506494812502845_n-1-20200627030256.png");
            }

            #IMAGE1459 {
                width: 200px;
                height: 52.8px;
                top: 169.406px;
                left: 699px;
            }

            #IMAGE1459 > .ladi-image > .ladi-image-background {
                width: 200px;
                height: 52.8px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x400/5e0337be8ee1512e0f853b97/logo-ngang-2-20200627030324.png");
            }

            #IMAGE1460 {
                width: 244.323px;
                height: 44.099px;
                top: 246.244px;
                left: 699px;
            }

            #IMAGE1460 > .ladi-image > .ladi-image-background {
                width: 244.323px;
                height: 44.099px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x350/5e0337be8ee1512e0f853b97/logo-20200627030324.png");
            }
        }

        @media (max-width: 767px) {
            #SECTION_POPUP {
                height: 0px;
            }

            #SECTION436 {
                height: 631.09px;
            }

            #SECTION436 > .ladi-overlay {
                background-color: rgba(0, 0, 0, 0.3);
            }

            #SECTION436 > .ladi-section-background {
                background-size: auto 100%;
                background-attachment: scroll;
                background-origin: content-box;
                background-image: url("https://w.ladicdn.com/uploads/images/188953cd-179d-44b6-ad1f-a091a136f6d7.jpg");
                background-position: left top;
                background-repeat: no-repeat;
                opacity: 0.83;
            }

            #SECTION512 {
                height: 417.4px;
            }

            #SECTION512 > .ladi-section-background {
                background-color: rgb(255, 255, 255);
            }

            #LINE438 {
                width: 300px;
                top: 293.59px;
                left: 63.75px;
            }

            #LINE438 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(255, 235, 59);
                border-right: 2px solid rgb(255, 235, 59);
                border-bottom: 2px solid rgb(255, 235, 59);
                border-left: 0px !important;
            }

            #LINE438 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #HEADLINE439 {
                width: 331px;
                top: 256.59px;
                left: 40.189px;
            }

            #HEADLINE439 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 33px;
                font-weight: bold;
                text-align: center;
                letter-spacing: 5px;
                line-height: 1;
            }

            #HEADLINE440 {
                width: 215px;
                top: 457.59px;
                left: 5px;
            }

            #HEADLINE440 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE441 {
                width: 154px;
                top: 457.59px;
                left: 237.5px;
            }

            #HEADLINE441 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE442 {
                width: 154px;
                top: 482.59px;
                left: 133.5px;
            }

            #HEADLINE442 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.2;
            }

            #BUTTON_TEXT443 {
                width: 218px;
                top: 14.2px;
                left: 0px;
            }

            #BUTTON_TEXT443 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(33, 33, 33);
                font-size: 14px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                line-height: 1.4;
            }

            #BUTTON443 {
                width: 203.689px;
                height: 48px;
                top: 521.59px;
                left: 111.905px;
            }

            #BUTTON443 > .ladi-button > .ladi-button-background {
                background-color: rgb(255, 235, 59);
            }

            #BUTTON443 > .ladi-button {
                border-radius: 0px;
            }

            #BUTTON443.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #HEADLINE513 {
                width: 198px;
                top: 49px;
                left: 111.5px;
            }

            #HEADLINE513 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(66, 66, 66);
                font-size: 24px;
                font-weight: bold;
                text-align: center;
                letter-spacing: 2px;
                line-height: 1;
            }

            #PARAGRAPH514 {
                width: 355px;
                top: 80px;
                left: 33px;
            }

            #PARAGRAPH514 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(10, 103, 233);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.4;
            }

            #GROUP516 {
                width: 248px;
                height: 126px;
                top: 252px;
                left: 86px;
            }

            #HEADLINE517 {
                width: 248px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE517 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(66, 66, 66);
                font-size: 18px;
                text-align: left;
                letter-spacing: 2px;
                line-height: 1.2;
            }

            #GROUP518 {
                width: 248px;
                height: 21px;
                top: 48px;
                left: 0px;
            }

            #SHAPE519 {
                width: 20px;
                height: 20px;
                top: 1px;
                left: 0px;
            }

            #SHAPE519 svg:last-child {
                fill: #000000;
            }

            #PARAGRAPH520 {
                width: 223px;
                top: 0px;
                left: 25px;
            }

            #PARAGRAPH520 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(97, 97, 97);
                font-size: 13px;
                text-align: left;
                line-height: 1.6;
            }

            #GROUP521 {
                width: 170px;
                height: 21px;
                top: 77px;
                left: 0px;
            }

            #SHAPE522 {
                width: 20px;
                height: 20px;
                top: 1px;
                left: 0px;
            }

            #SHAPE522 svg:last-child {
                fill: #000000;
            }

            #PARAGRAPH523 {
                width: 145px;
                top: 0px;
                left: 25px;
            }

            #PARAGRAPH523 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(97, 97, 97);
                font-size: 13px;
                text-align: left;
                line-height: 1.6;
            }

            #GROUP524 {
                width: 248px;
                height: 21px;
                top: 105px;
                left: 0px;
            }

            #SHAPE525 {
                width: 20px;
                height: 20px;
                top: 1px;
                left: 0px;
            }

            #SHAPE525 svg:last-child {
                fill: #000000;
            }

            #PARAGRAPH526 {
                width: 223px;
                top: 0px;
                left: 25px;
            }

            #PARAGRAPH526 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(97, 97, 97);
                font-size: 13px;
                text-align: left;
                line-height: 1.6;
            }

            #GROUP527 {
                width: 249px;
                height: 127px;
                top: 113px;
                left: 86px;
            }

            #GROUP528 {
                width: 249px;
                height: 21px;
                top: 106px;
                left: 0px;
            }

            #SHAPE529 {
                width: 20px;
                height: 20px;
                top: 0px;
                left: 0px;
            }

            #SHAPE529 svg:last-child {
                fill: #000000;
            }

            #PARAGRAPH530 {
                width: 223px;
                top: 1px;
                left: 26px;
            }

            #PARAGRAPH530 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(97, 97, 97);
                font-size: 13px;
                text-align: left;
                line-height: 1.6;
            }

            #GROUP531 {
                width: 170px;
                height: 21px;
                top: 77px;
                left: 0px;
            }

            #SHAPE532 {
                width: 20px;
                height: 20px;
                top: 1px;
                left: 0px;
            }

            #SHAPE532 svg:last-child {
                fill: #000000;
            }

            #PARAGRAPH533 {
                width: 145px;
                top: 0px;
                left: 25px;
            }

            #PARAGRAPH533 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(97, 97, 97);
                font-size: 13px;
                text-align: left;
                line-height: 1.6;
            }

            #GROUP534 {
                width: 248px;
                height: 21px;
                top: 48px;
                left: 0px;
            }

            #SHAPE535 {
                width: 20px;
                height: 20px;
                top: 1px;
                left: 0px;
            }

            #SHAPE535 svg:last-child {
                fill: #000000;
            }

            #PARAGRAPH536 {
                width: 223px;
                top: 0px;
                left: 25px;
            }

            #PARAGRAPH536 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(97, 97, 97);
                font-size: 13px;
                text-align: left;
                line-height: 1.6;
            }

            #HEADLINE537 {
                width: 248px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE537 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(66, 66, 66);
                font-size: 18px;
                text-align: left;
                letter-spacing: 2px;
                line-height: 1.2;
            }

            #BOX557 {
                width: 253.5px;
                height: 38px;
                top: 194.591px;
                left: 78.939px;
            }

            #BOX557 > .ladi-box {
                background-color: rgb(37, 23, 200);
                border-style: ridge;
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 22px;
            }

            #HEADLINE558 {
                width: 230px;
                top: 200.591px;
                left: 98.75px;
            }

            #HEADLINE558 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 16px;
                line-height: 1.6;
            }

            #HEADLINE562 {
                width: 281px;
                top: 311.59px;
                left: 78.939px;
            }

            #HEADLINE562 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #BOX563 {
                width: 286px;
                height: 40px;
                top: 402.59px;
                left: 70.75px;
            }

            #BOX563 > .ladi-box {
                background: #fdfbfb;
                background: -webkit-linear-gradient(202deg, #fdfbfb, #eaedee);
                background: linear-gradient(202deg, #fdfbfb, #eaedee);
                border-style: double;
                border-color: rgb(37, 23, 201);
                border-width: 7px;
                border-radius: 108px;
            }

            #HEADLINE564 {
                width: 278px;
                top: 412.09px;
                left: 88.5px;
            }

            #HEADLINE564 > .ladi-headline {
                color: rgb(2, 5, 43);
                font-size: 13px;
                line-height: 1.6;
            }

            #SECTION566 {
                height: 535.337px;
            }

            #SECTION566 > .ladi-section-background {
                background-color: rgba(235, 243, 254, 0.2);
            }

            #BOX567 {
                width: 420px;
                height: 60.556px;
                top: -42px;
                left: 0.5px;
            }

            #BOX567 > .ladi-box {
                background-color: rgb(7, 58, 145);
            }

            #HEADLINE579 {
                width: 300px;
                top: 340.556px;
                left: 60px;
            }

            #HEADLINE579 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 45px;
                font-weight: bold;
                text-align: center;
                letter-spacing: 5px;
                line-height: 1;
            }

            #HEADLINE581 {
                width: 300px;
                top: 0px;
                left: 78.939px;
            }

            #HEADLINE581 > .ladi-headline {
                color: rgb(255, 235, 60);
                font-size: 10px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE582 {
                width: 150px;
                top: 440.556px;
                left: 135px;
            }

            #HEADLINE582 > .ladi-headline {
                color: rgb(245, 245, 245);
                font-size: 16px;
                line-height: 1.6;
            }

            #SHAPE578 {
                width: 22.5048px;
                height: 27.7806px;
                top: 0px;
                left: 66.0243px;
            }

            #SHAPE578 svg:last-child {
                fill: rgba(200, 31, 23, 1.0);
            }

            #SHAPE577 {
                width: 25.814px;
                height: 25.8141px;
                top: 0px;
                left: 30.8865px;
            }

            #SHAPE577 svg:last-child {
                fill: rgba(255, 235, 61, 1.0);
            }

            #SHAPE576 {
                width: 21.3431px;
                height: 25.9533px;
                top: 0.659439px;
                left: 0px;
            }

            #SHAPE576 svg:last-child {
                fill: rgba(37, 23, 201, 1.0);
            }

            #GROUP575 {
                width: 88.5291px;
                height: 27.7806px;
                top: 422px;
                left: 153.235px;
            }

            #PARAGRAPH574 {
                width: 355px;
                top: 0px;
                left: 0px;
            }

            #PARAGRAPH574 > .ladi-paragraph {
                font-family: "Roboto", sans-serif;
                color: rgb(84, 84, 84);
                font-size: 12px;
                line-height: 1.6;
            }

            #GROUP573 {
                width: 355px;
                height: 449.781px;
                top: 36.556px;
                left: 33px;
            }

            #SECTION583 {
                height: 835.46px;
            }

            #GROUP584 {
                width: 417px;
                height: 103px;
                top: 471.82px;
                left: 1.5px;
            }

            #GROUP585 {
                width: 78.9827px;
                height: 74.9769px;
                top: 0px;
                left: 0px;
            }

            #SHAPE586 {
                width: 56.023px;
                height: 56.023px;
                top: 0px;
                left: 0px;
            }

            #SHAPE586 > .ladi-shape {
                transform: rotate(180deg);
                -webkit-transform: rotate(180deg);
            }

            #SHAPE586 svg:last-child {
                fill: rgba(5, 31, 78, 0.2);
            }

            #SHAPE587 {
                width: 49.9827px;
                height: 49.9539px;
                top: 25.023px;
                left: 29px;
            }

            #SHAPE587 svg:last-child {
                fill: rgba(5, 31, 77, 1.0);
            }

            #HEADLINE588 {
                width: 33px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE588 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 18px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE589 {
                width: 191px;
                top: 8px;
                left: 96px;
            }

            #HEADLINE589 > .ladi-headline {
                color: rgb(7, 58, 145);
                font-size: 18px;
                line-height: 1.6;
            }

            #PARAGRAPH590 {
                width: 321px;
                top: 37px;
                left: 96px;
            }

            #PARAGRAPH590 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 14px;
                line-height: 1.6;
            }

            #GROUP591 {
                width: 417px;
                height: 109.906px;
                top: 580.639px;
                left: 3px;
            }

            #GROUP592 {
                width: 81.9637px;
                height: 109.906px;
                top: 0px;
                left: 0px;
            }

            #SHAPE593 {
                width: 56.023px;
                height: 76.0805px;
                top: 0px;
                left: 0px;
            }

            #SHAPE593 > .ladi-shape {
                transform: rotate(180deg);
                -webkit-transform: rotate(180deg);
            }

            #SHAPE593 svg:last-child {
                fill: rgba(5, 31, 78, 0.2);
            }

            #SHAPE594 {
                width: 56.9637px;
                height: 77.3135px;
                top: 32.5925px;
                left: 25px;
            }

            #SHAPE594 svg:last-child {
                fill: rgba(5, 31, 77, 1.0);
            }

            #HEADLINE595 {
                width: 33px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE595 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 18px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE596 {
                width: 299px;
                top: 14px;
                left: 96px;
            }

            #HEADLINE596 > .ladi-headline {
                color: rgb(7, 58, 145);
                font-size: 18px;
                line-height: 1.6;
            }

            #PARAGRAPH597 {
                width: 321px;
                top: 50.2469px;
                left: 96px;
            }

            #PARAGRAPH597 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 14px;
                line-height: 1.6;
            }

            #GROUP598 {
                width: 417px;
                height: 81px;
                top: 682.459px;
                left: 1.5px;
            }

            #GROUP599 {
                width: 78.9827px;
                height: 74.9769px;
                top: 0px;
                left: 0px;
            }

            #SHAPE600 {
                width: 56.023px;
                height: 56.023px;
                top: 0px;
                left: 0px;
            }

            #SHAPE600 > .ladi-shape {
                transform: rotate(180deg);
                -webkit-transform: rotate(180deg);
            }

            #SHAPE600 svg:last-child {
                fill: rgba(5, 31, 78, 0.2);
            }

            #SHAPE601 {
                width: 49.9827px;
                height: 49.9539px;
                top: 25.023px;
                left: 29px;
            }

            #SHAPE601 svg:last-child {
                fill: rgba(5, 31, 77, 1.0);
            }

            #HEADLINE602 {
                width: 33px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE602 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 18px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE603 {
                width: 191px;
                top: 0px;
                left: 96px;
            }

            #HEADLINE603 > .ladi-headline {
                color: rgb(7, 58, 145);
                font-size: 18px;
                line-height: 1.6;
            }

            #PARAGRAPH604 {
                width: 321px;
                top: 37px;
                left: 96px;
            }

            #PARAGRAPH604 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 14px;
                line-height: 1.6;
            }

            #HEADLINE605 {
                width: 353px;
                top: 9px;
                left: 40px;
            }

            #HEADLINE605 > .ladi-headline {
                font-family: "Roboto", sans-serif;
                color: rgb(84, 84, 84);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #IMAGE607 {
                width: 420px;
                height: 283.688px;
                top: 51px;
                left: 1.5px;
            }

            #IMAGE607 > .ladi-image > .ladi-image-background {
                width: 420px;
                height: 283.688px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s750x600/5e0337be8ee1512e0f853b97/85212408_1079152082444746_58076526301675520_n-20200507002220.jpg");
            }

            #GROUP608 {
                width: 420px;
                height: 95px;
                top: 363px;
                left: 0px;
            }

            #HEADLINE609 {
                width: 193px;
                top: 0px;
                left: 96.6906px;
            }

            #HEADLINE609 > .ladi-headline {
                color: rgb(7, 58, 145);
                font-size: 18px;
                line-height: 1.6;
            }

            #PARAGRAPH610 {
                width: 324px;
                top: 29px;
                left: 96px;
            }

            #PARAGRAPH610 > .ladi-paragraph {
                color: rgb(0, 0, 0);
                font-size: 14px;
                line-height: 1.6;
            }

            #SHAPE611 {
                width: 56.426px;
                height: 56.023px;
                top: 0px;
                left: 0px;
            }

            #SHAPE611 > .ladi-shape {
                transform: rotate(180deg);
                -webkit-transform: rotate(180deg);
            }

            #SHAPE611 svg:last-child {
                fill: rgba(13, 98, 242, 0.3);
            }

            #SHAPE612 {
                width: 50.3422px;
                height: 49.9539px;
                top: 25.023px;
                left: 29.2087px;
            }

            #SHAPE612 svg:last-child {
                fill: rgba(7, 58, 145, 1.0);
            }

            #HEADLINE613 {
                width: 33px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE613 > .ladi-headline {
                color: rgb(18, 11, 92);
                font-size: 18px;
                font-weight: bold;
                line-height: 1.6;
            }

            #BUTTON614 {
                width: 160px;
                height: 40px;
                top: 778.459px;
                left: 130px;
            }

            #BUTTON614 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON614.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT614 {
                width: 203px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT614 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 16px;
                text-align: center;
                line-height: 1.6;
            }

            #SECTION615 {
                height: 883.224px;
            }

            #HEADLINE653 {
                width: 360px;
                top: 8px;
                left: 27.5px;
            }

            #HEADLINE653 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1;
            }

            #LINE654 {
                width: 155px;
                top: 75px;
                left: 130px;
            }

            #LINE654 > .ladi-line > .ladi-line-container {
                border-top: 5px solid rgb(67 160 246 / 49%);
                border-right: 5px solid rgb(67 160 246 / 49%);
                border-bottom: 5px solid rgb(67 160 246 / 49%);
                border-left: 0px !important;
            }

            #LINE654 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #IMAGE659 {
                width: 100.77px;
                height: 101.955px;
                top: 0px;
                left: 11.1005px;
            }

            #IMAGE659 > .ladi-image > .ladi-image-background {
                width: 100.77px;
                height: 101.955px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/xt-02-20200506114830.jpg");
            }

            #IMAGE659 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE660 {
                width: 122.973px;
                height: 62.4316px;
                top: 52.5684px;
                left: 0px;
            }

            #SHAPE660 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE660 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP658 {
                width: 122.973px;
                height: 115px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE663 {
                width: 81px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE663 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE664 {
                width: 61px;
                top: 63px;
                left: 10px;
            }

            #HEADLINE664 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP662 {
                width: 81px;
                height: 91px;
                top: 0px;
                left: 0px;
            }

            #SHAPE666 {
                width: 11.9085px;
                height: 10.4828px;
                top: 0px;
                left: 0px;
            }

            #SHAPE666 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE667 {
                width: 11.9085px;
                height: 10.4828px;
                top: 0px;
                left: 17.4259px;
            }

            #SHAPE667 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE668 {
                width: 13.0097px;
                height: 10.4828px;
                top: 0px;
                left: 34.8521px;
            }

            #SHAPE668 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP665 {
                width: 47.8618px;
                height: 10.4828px;
                top: 105.635px;
                left: 16.5691px;
            }

            #GROUP661 {
                width: 81px;
                height: 116.118px;
                top: 117.497px;
                left: 15.8779px;
            }

            #GROUP657 {
                width: 122.973px;
                height: 233.615px;
                top: 0px;
                left: 0px;
            }

            #IMAGE671 {
                width: 100.77px;
                height: 101.955px;
                top: 0px;
                left: 11.1005px;
            }

            #IMAGE671 > .ladi-image > .ladi-image-background {
                width: 100.77px;
                height: 101.955px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/hh-20200506033554.jpg");
            }

            #IMAGE671 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE672 {
                width: 122.973px;
                height: 62.4316px;
                top: 52.5684px;
                left: 0px;
            }

            #SHAPE672 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE672 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP670 {
                width: 122.973px;
                height: 115px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE675 {
                width: 81px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE675 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE676 {
                width: 61px;
                top: 63.179px;
                left: 10px;
            }

            #HEADLINE676 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP674 {
                width: 81px;
                height: 105.179px;
                top: 0px;
                left: 0px;
            }

            #SHAPE678 {
                width: 11.9085px;
                height: 10.4828px;
                top: 0px;
                left: 0px;
            }

            #SHAPE678 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE679 {
                width: 11.9085px;
                height: 10.4828px;
                top: 0px;
                left: 17.4259px;
            }

            #SHAPE679 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE680 {
                width: 13.0097px;
                height: 10.4828px;
                top: 0px;
                left: 34.8521px;
            }

            #SHAPE680 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP677 {
                width: 47.8618px;
                height: 10.4828px;
                top: 112.425px;
                left: 16.5691px;
            }

            #GROUP673 {
                width: 81px;
                height: 122.908px;
                top: 118.5px;
                left: 19.2103px;
            }

            #GROUP669 {
                width: 122.973px;
                height: 241.408px;
                top: 0px;
                left: 146.737px;
            }

            #IMAGE683 {
                width: 100.77px;
                height: 101.955px;
                top: 0px;
                left: 11.1005px;
            }

            #IMAGE683 > .ladi-image > .ladi-image-background {
                width: 100.77px;
                height: 101.955px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/hoang-tung-02-20200507004621.jpg");
            }

            #IMAGE683 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE684 {
                width: 122.973px;
                height: 62.4316px;
                top: 52.5684px;
                left: 0px;
            }

            #SHAPE684 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE684 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP682 {
                width: 122.973px;
                height: 115px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE687 {
                width: 81px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE687 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE688 {
                width: 61px;
                top: 76.182px;
                left: 10px;
            }

            #HEADLINE688 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP686 {
                width: 81px;
                height: 104.182px;
                top: 0px;
                left: 0px;
            }

            #SHAPE690 {
                width: 11.9085px;
                height: 10.4828px;
                top: 0px;
                left: 0px;
            }

            #SHAPE690 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE691 {
                width: 11.9085px;
                height: 10.4828px;
                top: 0px;
                left: 17.4259px;
            }

            #SHAPE691 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE692 {
                width: 13.0097px;
                height: 10.4828px;
                top: 0px;
                left: 34.8521px;
            }

            #SHAPE692 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP689 {
                width: 47.8618px;
                height: 10.4828px;
                top: 116.635px;
                left: 12.6777px;
            }

            #GROUP685 {
                width: 81px;
                height: 127.118px;
                top: 111.497px;
                left: 18.8779px;
            }

            #GROUP681 {
                width: 122.973px;
                height: 238.615px;
                top: 0px;
                left: 293.474px;
            }

            #GROUP656 {
                width: 416.447px;
                height: 241.408px;
                top: 357.408px;
                left: 3.55271px;
            }

            #GROUP656.ladi-animation > .ladi-group {
                animation-name: bounceInRight;
                -webkit-animation-name: bounceInRight;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 4s;
                -webkit-animation-duration: 4s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #SECTION718 {
                height: 1100.47px;
            }

            #SECTION718 > .ladi-section-background {
                background-color: rgb(255, 255, 255);
            }

            #BOX719 {
                width: 419.251px;
                height: 385.615px;
                top: 0px;
                left: 0px;
            }

            #BOX719 > .ladi-box {
                background-color: rgb(33, 33, 33);
            }

            #BOX734 {
                width: 419.251px;
                height: 57.432px;
                top: 0px;
                left: 0px;
            }

            #BOX734 > .ladi-box {
                background-color: rgb(199, 14, 2);
            }

            #HEADLINE735 {
                width: 415px;
                top: 4.10229px;
                left: 4.72396px;
            }

            #HEADLINE735 > .ladi-headline {
                font-family: "Roboto Slab", serif;
                color: rgb(255, 255, 255);
                font-size: 24px;
                font-weight: bold;
                text-align: center;
                line-height: 1;
            }

            #GROUP742 {
                width: 419.724px;
                height: 385.615px;
                top: 14px;
                left: 0px;
            }

            #GROUP743 {
                width: 419.724px;
                height: 57.432px;
                top: 20.5115px;
                left: 0px;
            }

            #HEADLINE788 {
                width: 280px;
                top: 408px;
                left: 44.6645px;
            }

            #HEADLINE788 > .ladi-headline {
                color: rgb(234, 242, 254);
                font-size: 12px;
                text-align: center;
                line-height: 1.4;
            }

            #FORM_ITEM785 {
                width: 279.657px;
                height: 40px;
                top: 96px;
                left: 0px;
            }

            #FORM_ITEM784 {
                width: 279.657px;
                height: 40px;
                top: 48px;
                left: 0px;
            }

            #FORM_ITEM783 {
                width: 279.657px;
                height: 40px;
                top: 0px;
                left: 0px;
            }

            #BUTTON_TEXT781 {
                width: 280px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT781 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON781 {
                width: 280.47px;
                height: 40px;
                top: 152px;
                left: 0px;
            }

            #BUTTON781 > .ladi-button > .ladi-button-background {
                background-color: rgb(0, 154, 244);
            }

            #BUTTON781 > .ladi-button {
                border-radius: 0px;
            }

            #FORM780 {
                width: 280.47px;
                height: 192px;
                top: 206.45px;
                left: 44.562px;
            }

            #FORM780 > .ladi-form {
                color: rgb(132, 132, 132);
                font-size: 14px;
                letter-spacing: 0px;
                line-height: 1.6;
            }

            #FORM780 .ladi-form-item .ladi-form-control::placeholder, #FORM780 .ladi-form .ladi-form-item .ladi-form-control-select[data-selected=""], #FORM780 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: rgba(132, 132, 132, 1.0);
            }

            #FORM780 .ladi-form-item {
                padding-left: 8px;
                padding-right: 8px;
            }

            #FORM780 .ladi-form-item.ladi-form-checkbox {
                padding-left: 13px;
                padding-right: 13px;
            }

            #FORM780 .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20rgba(132%2C%20132%2C%20132%2C%201.0)%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
            }

            #FORM780 .ladi-form-item-container {
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 1px;
                border-radius: 0px;
            }

            #FORM780 .ladi-form-item-background {
                background-color: rgb(255, 255, 255);
            }

            #BOX769 {
                width: 360px;
                height: 66.163px;
                top: 121px;
                left: 4.5px;
            }

            #BOX769 > .ladi-box {
                background-color: rgb(234, 242, 254);
            }

            #BOX768 {
                width: 383px;
                height: 470.4px;
                top: 0px;
                left: 0px;
            }

            #BOX768 > .ladi-box {
                background-color: rgb(10, 103, 233);
            }

            #GROUP767 {
                width: 383px;
                height: 470.4px;
                top: 92px;
                left: 20.2762px;
            }

            #GROUP770 {
                width: 235.219px;
                height: 54.163px;
                top: 131px;
                left: 66.891px;
            }

            #COUNTDOWN771 {
                width: 234.525px;
                height: 46.3584px;
                top: 0px;
                left: 0px;
            }

            #COUNTDOWN771 > .ladi-countdown {
                color: rgb(5, 34, 74);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
            }

            #COUNTDOWN771 > .ladi-countdown > .ladi-element {
                width: calc((100% - 16px * 3) / 4);
                margin-right: 16px;
                height: 100%;
            }

            #COUNTDOWN771 > .ladi-countdown .ladi-countdown-background {
                background-color: rgba(6, 34, 75, 0);
                border-radius: 105px;
            }

            #HEADLINE776 {
                width: 48px;
                top: 35.163px;
                left: 0.719643px;
            }

            #HEADLINE776 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE777 {
                width: 48px;
                top: 35.163px;
                left: 62.8862px;
            }

            #HEADLINE777 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE778 {
                width: 48px;
                top: 35.163px;
                left: 125.052px;
            }

            #HEADLINE778 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE779 {
                width: 48px;
                top: 35.163px;
                left: 187.219px;
            }

            #HEADLINE779 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE789 {
                width: 150px;
                top: 936.4px;
                left: 135px;
            }

            #HEADLINE789 > .ladi-headline {
                color: rgb(245, 245, 245);
                font-size: 16px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE790 {
                width: 420px;
                top: 686.4px;
                left: 0px;
            }

            #HEADLINE790 > .ladi-headline {
                color: rgb(84, 84, 84);
                font-size: 16px;
                font-weight: bold;
                line-height: 1.6;
            }

            #IMAGE791 {
                width: 420px;
                height: 158.733px;
                top: 522.4px;
                left: 0px;
            }

            #IMAGE791 > .ladi-image > .ladi-image-background {
                width: 420px;
                height: 158.733px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s750x500/5e0337be8ee1512e0f853b97/dang-ky-ngay-20200507005830.jpg");
            }

            #IMAGE791.ladi-animation > .ladi-image {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #VIDEO792 {
                width: 419px;
                height: 235.687px;
                top: 250.65px;
                left: 0px;
            }

            #VIDEO792 > .ladi-video > .ladi-video-background {
                background-size: cover;
                background-attachment: scroll;
                background-origin: content-box;
                background-image: url("https://img.youtube.com/vi/b3czYDhk3ok/hqdefault.jpg");
                background-position: center center;
                background-repeat: no-repeat;
            }

            #SHAPE792 {
                width: 60px;
                height: 60px;
                top: 87.8435px;
                left: 179.5px;
            }

            #SHAPE792 svg:last-child {
                fill: rgba(0, 0, 0, 0.5);
            }

            #POPUP806 {
                width: 420px;
                height: 322px;
                top: 0px;
                left: 0px;
                bottom: 0px;
                right: 0px;
                margin: auto;
            }

            #POPUP806 > .ladi-popup > .ladi-popup-background {
                background-color: rgb(255, 255, 255);
            }

            #BOX807 {
                width: 418.5px;
                height: 91px;
                top: 0px;
                left: 0px;
            }

            #BOX807 > .ladi-box {
                background-color: rgb(27, 38, 51);
            }

            #HEADLINE814 {
                width: 264px;
                top: 99px;
                left: 73.5px;
            }

            #HEADLINE814 > .ladi-headline {
                font-family: "Montserrat", sans-serif;
                color: rgb(0, 0, 0);
                font-size: 24px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.2;
            }

            #PARAGRAPH815 {
                width: 242px;
                top: 293px;
                left: 89.5px;
            }

            #PARAGRAPH815 > .ladi-paragraph {
                font-family: "Roboto", sans-serif;
                color: rgb(54, 145, 117);
                font-size: 14px;
                text-align: center;
                letter-spacing: 0px;
                line-height: 1.2;
            }

            #FORM816 {
                width: 267.39px;
                height: 149px;
                top: 131px;
                left: 79.1096px;
            }

            #FORM816 > .ladi-form {
                color: rgb(89, 89, 89);
                font-size: 16px;
                line-height: 1;
            }

            #FORM816 .ladi-form-item .ladi-form-control::placeholder, #FORM816 .ladi-form .ladi-form-item .ladi-form-control-select[data-selected=""], #FORM816 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: rgba(89, 89, 89, 1);
            }

            #FORM816 .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20rgba(89%2C89%2C89%2C1)%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
            }

            #FORM816 .ladi-form-item-container {
                border-style: solid;
                border-color: rgb(38, 38, 38);
                border-width: 2px;
            }

            #FORM816 .ladi-form-item-background {
                background-color: rgb(255, 255, 255);
            }

            #FORM_ITEM817 {
                width: 264.441px;
                height: 101.034px;
                top: 7.14383px;
                left: 1.47457px;
            }

            #BUTTON818 {
                width: 266.407px;
                height: 56.8218px;
                top: 92.1781px;
                left: 0px;
            }

            #BUTTON818 > .ladi-button > .ladi-button-background {
                background-color: rgb(27, 38, 51);
            }

            #BUTTON818 > .ladi-button {
                box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 1);
                -webkit-box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 1);
                border-radius: 0px;
            }

            #BUTTON_TEXT818 {
                width: 271px;
                top: 6px;
                left: 0px;
            }

            #BUTTON_TEXT818 > .ladi-headline {
                font-family: "Roboto", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                letter-spacing: 1px;
                line-height: 2;
            }

            #HEADLINE821 {
                width: 419px;
                top: 8px;
                left: 0px;
            }

            #HEADLINE821 > .ladi-headline {
                color: rgb(255, 235, 62);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #FORM_ITEM822 {
                width: 267.39px;
                height: 47.9657px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE823 {
                width: 150px;
                top: 490.337px;
                left: 167px;
            }

            #HEADLINE823 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 16px;
                font-weight: bold;
                font-style: italic;
                line-height: 1.6;
            }

            #IMAGE824 {
                width: 420px;
                height: 230.07px;
                top: 870.4px;
                left: 0.5px;
            }

            #IMAGE824 > .ladi-image > .ladi-image-background {
                width: 420px;
                height: 230.07px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s750x550/5e0337be8ee1512e0f853b97/55881731_657806031306841_2510107796930822144_n-20200507031307.jpg");
            }

            #HEADLINE882 {
                width: 298px;
                top: 596.09px;
                left: 128px;
            }

            #HEADLINE882 > .ladi-headline {
                color: rgb(245, 245, 245);
                font-size: 22px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE885 {
                width: 150px;
                top: 623.047px;
                left: 135px;
            }

            #HEADLINE885 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 16px;
                line-height: 1.6;
            }

            #BOX924 {
                width: 282px;
                height: 58px;
                top: 118.823px;
                left: 70.7762px;
            }

            #BOX924 > .ladi-box {
                background-color: rgb(33, 33, 33);
            }

            #HEADLINE927 {
                width: 241px;
                top: 135.323px;
                left: 106px;
            }

            #HEADLINE927 > .ladi-headline {
                color: rgb(245, 245, 245);
                font-size: 16px;
                font-weight: bold;
                line-height: 1.6;
            }

            #IMAGE944 {
                width: 100.77px;
                height: 101.955px;
                top: 0px;
                left: 11.1005px;
            }

            #IMAGE944 > .ladi-image > .ladi-image-background {
                width: 100.77px;
                height: 101.955px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/tuan-ha-02-20200506115044.jpg");
            }

            #IMAGE944 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE945 {
                width: 122.973px;
                height: 62.4316px;
                top: 52.5684px;
                left: 0px;
            }

            #SHAPE945 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE945 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP943 {
                width: 122.973px;
                height: 115px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE948 {
                width: 81px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE948 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE949 {
                width: 61px;
                top: 63px;
                left: 10px;
            }

            #HEADLINE949 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP947 {
                width: 81px;
                height: 91px;
                top: 0px;
                left: 0px;
            }

            #SHAPE951 {
                width: 11.9085px;
                height: 10.4828px;
                top: 0px;
                left: 0px;
            }

            #SHAPE951 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE952 {
                width: 11.9085px;
                height: 10.4828px;
                top: 0px;
                left: 17.4259px;
            }

            #SHAPE952 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE953 {
                width: 13.0097px;
                height: 10.4828px;
                top: 0px;
                left: 34.8521px;
            }

            #SHAPE953 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP950 {
                width: 47.8618px;
                height: 10.4828px;
                top: 105.635px;
                left: 16.5691px;
            }

            #GROUP946 {
                width: 81px;
                height: 116.118px;
                top: 117.497px;
                left: 15.8779px;
            }

            #GROUP942 {
                width: 122.973px;
                height: 233.615px;
                top: 0px;
                left: 0px;
            }

            #IMAGE956 {
                width: 100.77px;
                height: 101.955px;
                top: 0px;
                left: 11.1005px;
            }

            #IMAGE956 > .ladi-image > .ladi-image-background {
                width: 100.77px;
                height: 101.955px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/93837421_2519688108347991_6243488516748083200_o-20200506115509.jpg");
            }

            #IMAGE956 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE957 {
                width: 122.973px;
                height: 62.4316px;
                top: 52.5684px;
                left: 0px;
            }

            #SHAPE957 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE957 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP955 {
                width: 122.973px;
                height: 115px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE960 {
                width: 81px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE960 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #SHAPE963 {
                width: 11.9085px;
                height: 10.4828px;
                top: 0px;
                left: 0px;
            }

            #SHAPE963 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE964 {
                width: 11.9085px;
                height: 10.4828px;
                top: 0px;
                left: 17.4259px;
            }

            #SHAPE964 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE965 {
                width: 13.0097px;
                height: 10.4828px;
                top: 0px;
                left: 34.8521px;
            }

            #SHAPE965 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP962 {
                width: 47.8618px;
                height: 10.4828px;
                top: 112.425px;
                left: 16.5691px;
            }

            #GROUP958 {
                width: 81px;
                height: 122.908px;
                top: 118.5px;
                left: 19.2103px;
            }

            #GROUP954 {
                width: 122.973px;
                height: 241.408px;
                top: 0px;
                left: 146.737px;
            }

            #IMAGE968 {
                width: 100.77px;
                height: 101.955px;
                top: 0px;
                left: 11.1005px;
            }

            #IMAGE968 > .ladi-image > .ladi-image-background {
                width: 100.77px;
                height: 101.955px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/54729144_2050227821680302_7298078153862283264_n-20200506115207.jpg");
            }

            #IMAGE968 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE969 {
                width: 122.973px;
                height: 62.4316px;
                top: 52.5684px;
                left: 0px;
            }

            #SHAPE969 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE969 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP967 {
                width: 122.973px;
                height: 115px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE972 {
                width: 81px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE972 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE973 {
                width: 61px;
                top: 76.182px;
                left: 10px;
            }

            #HEADLINE973 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP971 {
                width: 81px;
                height: 104.182px;
                top: 0px;
                left: 0px;
            }

            #SHAPE975 {
                width: 11.9085px;
                height: 10.4828px;
                top: 0px;
                left: 0px;
            }

            #SHAPE975 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE976 {
                width: 11.9085px;
                height: 10.4828px;
                top: 0px;
                left: 17.4259px;
            }

            #SHAPE976 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE977 {
                width: 13.0097px;
                height: 10.4828px;
                top: 0px;
                left: 34.8521px;
            }

            #SHAPE977 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP974 {
                width: 47.8618px;
                height: 10.4828px;
                top: 116.635px;
                left: 12.6777px;
            }

            #GROUP970 {
                width: 81px;
                height: 127.118px;
                top: 111.497px;
                left: 18.8779px;
            }

            #GROUP966 {
                width: 122.973px;
                height: 238.615px;
                top: 0px;
                left: 293.474px;
            }

            #GROUP941 {
                width: 416.447px;
                height: 241.408px;
                top: 106px;
                left: 1.7765px;
            }

            #GROUP941.ladi-animation > .ladi-group {
                animation-name: bounceInRight;
                -webkit-animation-name: bounceInRight;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #PARAGRAPH978 {
                width: 400px;
                top: 3.8763px;
                left: 0px;
            }

            #PARAGRAPH978 > .ladi-paragraph {
                color: rgb(255, 255, 255);
                font-size: 16px;
                line-height: 1.6;
            }

            #IMAGE979 {
                width: 118.965px;
                height: 35.2334px;
                top: 0px;
                left: 287.5px;
            }

            #IMAGE979 > .ladi-image > .ladi-image-background {
                width: 118.965px;
                height: 35.2334px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x350/5e0337be8ee1512e0f853b97/vinalink-logo-20200507082310.png");
            }

            #IMAGE980 {
                width: 50.5px;
                height: 50.5px;
                top: 51.091px;
                left: 363.75px;
            }

            #IMAGE980 > .ladi-image > .ladi-image-background {
                width: 50.5px;
                height: 50.5px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s400x400/5e0337be8ee1512e0f853b97/download-20200507082317.png");
            }

            #IMAGE982 {
                width: 50.5px;
                height: 50.5px;
                top: 51.091px;
                left: 293.75px;
            }

            #IMAGE982 > .ladi-image > .ladi-image-background {
                width: 50.5px;
                height: 50.5px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s400x400/5e0337be8ee1512e0f853b97/vag-20200507082605.jpg");
            }

            #PARAGRAPH983 {
                width: 400px;
                top: 5.3763px;
                left: 214.5px;
            }

            #PARAGRAPH983 > .ladi-paragraph {
                color: rgb(255, 255, 255);
                font-size: 16px;
                line-height: 1.6;
            }

            #HEADLINE986 {
                width: 264px;
                top: 99px;
                left: 73.5px;
            }

            #HEADLINE986 > .ladi-headline {
                font-family: "Montserrat", sans-serif;
                color: rgb(0, 0, 0);
                font-size: 24px;
                font-weight: bold;
                text-transform: uppercase;
                text-align: center;
                letter-spacing: 1px;
                line-height: 1.2;
            }

            #PARAGRAPH987 {
                width: 242px;
                top: 293px;
                left: 89.5px;
            }

            #PARAGRAPH987 > .ladi-paragraph {
                font-family: "Roboto", sans-serif;
                color: rgb(54, 145, 117);
                font-size: 14px;
                text-align: center;
                letter-spacing: 0px;
                line-height: 1.2;
            }

            #FORM_ITEM989 {
                width: 264.441px;
                height: 101.034px;
                top: 7.14383px;
                left: 1.47457px;
            }

            #BUTTON_TEXT990 {
                width: 271px;
                top: 6px;
                left: 0px;
            }

            #BUTTON_TEXT990 > .ladi-headline {
                font-family: "Roboto", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                letter-spacing: 1px;
                line-height: 2;
            }

            #BUTTON990 {
                width: 266.407px;
                height: 56.8218px;
                top: 92.1781px;
                left: 0px;
            }

            #BUTTON990 > .ladi-button > .ladi-button-background {
                background-color: rgb(27, 38, 51);
            }

            #BUTTON990 > .ladi-button {
                box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 1);
                -webkit-box-shadow: 0px 0px 0px 0px rgba(255, 255, 255, 1);
                border-radius: 0px;
            }

            #FORM_ITEM992 {
                width: 267.39px;
                height: 47.9657px;
                top: 0px;
                left: 0px;
            }

            #FORM988 {
                width: 267.39px;
                height: 149px;
                top: 131px;
                left: 79.1096px;
            }

            #FORM988 > .ladi-form {
                color: rgb(89, 89, 89);
                font-size: 16px;
                line-height: 1;
            }

            #FORM988 .ladi-form-item .ladi-form-control::placeholder, #FORM988 .ladi-form .ladi-form-item .ladi-form-control-select[data-selected=""], #FORM988 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"] {
                color: rgba(89, 89, 89, 1);
            }

            #FORM988 .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
                background-image: url("data:image/svg+xml;utf8, %3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20version%3D%221.1%22%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20rgba(89%2C89%2C89%2C1)%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
            }

            #FORM988 .ladi-form-item-container {
                border-style: solid;
                border-color: rgb(38, 38, 38);
                border-width: 2px;
            }

            #FORM988 .ladi-form-item-background {
                background-color: rgb(255, 255, 255);
            }

            #BOX993 {
                width: 418.5px;
                height: 91px;
                top: 0px;
                left: 0px;
            }

            #BOX993 > .ladi-box {
                background-color: rgb(27, 38, 51);
            }

            #HEADLINE994 {
                width: 419px;
                top: 8px;
                left: 0px;
            }

            #HEADLINE994 > .ladi-headline {
                color: rgb(255, 235, 62);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #POPUP985 {
                width: 420px;
                height: 322px;
                top: 0px;
                left: 0px;
                bottom: 0px;
                right: 0px;
                margin: auto;
            }

            #POPUP985 > .ladi-popup > .ladi-popup-background {
                background-color: rgb(255, 255, 255);
            }

            #IMAGE998 {
                width: 100.77px;
                height: 104.49px;
                top: 0px;
                left: 11.1005px;
            }

            #IMAGE998 > .ladi-image > .ladi-image-background {
                width: 100.77px;
                height: 104.49px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/minh-tam-20200514085203.jpg");
            }

            #IMAGE998 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE999 {
                width: 122.973px;
                height: 63.9837px;
                top: 53.8753px;
                left: 0px;
            }

            #SHAPE999 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE999 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP997 {
                width: 122.973px;
                height: 117.859px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1002 {
                width: 81px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1002 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE1003 {
                width: 61px;
                top: 64.5658px;
                left: 10px;
            }

            #HEADLINE1003 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP1001 {
                width: 81px;
                height: 93.2617px;
                top: 0px;
                left: 0px;
            }

            #SHAPE1005 {
                width: 11.9085px;
                height: 10.7433px;
                top: 0px;
                left: 0px;
            }

            #SHAPE1005 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE1006 {
                width: 11.9085px;
                height: 10.7433px;
                top: 0px;
                left: 17.4259px;
            }

            #SHAPE1006 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE1007 {
                width: 13.0097px;
                height: 10.7433px;
                top: 0px;
                left: 34.8521px;
            }

            #SHAPE1007 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP1004 {
                width: 47.8618px;
                height: 10.7433px;
                top: 113.26px;
                left: 16.5691px;
            }

            #GROUP1000 {
                width: 81px;
                height: 124.003px;
                top: 120.418px;
                left: 15.8779px;
            }

            #GROUP996 {
                width: 122.973px;
                height: 244.421px;
                top: 0px;
                left: 0px;
            }

            #IMAGE1010 {
                width: 100.77px;
                height: 104.489px;
                top: 0px;
                left: 11.1005px;
            }

            #IMAGE1010 > .ladi-image > .ladi-image-background {
                width: 100.77px;
                height: 104.489px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/74479509_2551697195047712_19365436307013632_n-20200514085323.jpg");
            }

            #IMAGE1010 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE1011 {
                width: 122.973px;
                height: 63.9832px;
                top: 53.8748px;
                left: 0px;
            }

            #SHAPE1011 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE1011 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP1009 {
                width: 122.973px;
                height: 117.858px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1014 {
                width: 81px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1014 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE1015 {
                width: 61px;
                top: 64.7498px;
                left: 10px;
            }

            #HEADLINE1015 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP1013 {
                width: 81px;
                height: 107.794px;
                top: 0px;
                left: 0px;
            }

            #SHAPE1017 {
                width: 11.9085px;
                height: 10.7434px;
                top: 0px;
                left: 0px;
            }

            #SHAPE1017 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE1018 {
                width: 11.9085px;
                height: 10.7434px;
                top: 0px;
                left: 17.4259px;
            }

            #SHAPE1018 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE1019 {
                width: 13.0097px;
                height: 10.7434px;
                top: 0px;
                left: 34.8521px;
            }

            #SHAPE1019 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP1016 {
                width: 47.8618px;
                height: 10.7434px;
                top: 115.22px;
                left: 16.5691px;
            }

            #GROUP1012 {
                width: 81px;
                height: 125.963px;
                top: 121.445px;
                left: 19.2103px;
            }

            #GROUP1008 {
                width: 122.973px;
                height: 247.408px;
                top: 0px;
                left: 146.737px;
            }

            #IMAGE1022 {
                width: 100.77px;
                height: 104.49px;
                top: 0px;
                left: 11.1005px;
            }

            #IMAGE1022 > .ladi-image > .ladi-image-background {
                width: 100.77px;
                height: 104.49px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/95860013_266312071077934_5498025049069715456_o-20200514085530.jpg");
            }

            #IMAGE1022 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
                filter: grayscale(100%);
            }

            #SHAPE1023 {
                width: 122.973px;
                height: 63.9837px;
                top: 53.8753px;
                left: 0px;
            }

            #SHAPE1023 > .ladi-shape {
                transform: rotate(-180deg);
                -webkit-transform: rotate(-180deg);
            }

            #SHAPE1023 svg:last-child {
                fill: rgb(67 160 246 / 49%);
            }

            #GROUP1021 {
                width: 122.973px;
                height: 117.859px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1026 {
                width: 81px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1026 > .ladi-headline {
                color: rgb(5, 34, 74);
                font-size: 18px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE1027 {
                width: 61px;
                top: 78.0759px;
                left: 10px;
            }

            #HEADLINE1027 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP1025 {
                width: 81px;
                height: 106.772px;
                top: 0px;
                left: 0px;
            }

            #SHAPE1029 {
                width: 11.9085px;
                height: 10.7434px;
                top: 0px;
                left: 0px;
            }

            #SHAPE1029 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE1030 {
                width: 11.9085px;
                height: 10.7434px;
                top: 0px;
                left: 17.4259px;
            }

            #SHAPE1030 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #SHAPE1031 {
                width: 13.0097px;
                height: 10.7434px;
                top: 0px;
                left: 34.8521px;
            }

            #SHAPE1031 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP1028 {
                width: 47.8618px;
                height: 10.7434px;
                top: 119.535px;
                left: 12.6777px;
            }

            #GROUP1024 {
                width: 81px;
                height: 130.278px;
                top: 114.268px;
                left: 18.8779px;
            }

            #GROUP1020 {
                width: 122.973px;
                height: 244.546px;
                top: 0px;
                left: 293.474px;
            }

            #GROUP995 {
                width: 416.447px;
                height: 247.408px;
                top: 608.816px;
                left: 1.7765px;
            }

            #GROUP995.ladi-animation > .ladi-group {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 4s;
                -webkit-animation-duration: 4s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #SECTION1032 {
                height: 485.272px;
            }

            #SECTION1032 > .ladi-section-background {
                background-color: rgb(255, 255, 255);
            }

            #HEADLINE1033 {
                width: 370px;
                top: 10px;
                left: 30.4855px;
            }

            #HEADLINE1033 > .ladi-headline {
                color: rgb(11, 103, 230);
                font-size: 22px;
                font-weight: bold;
                text-align: center;
                line-height: 1;
            }

            #GROUP1036 {
                width: 212.664px;
                height: 95.4252px;
                top: 61.88px;
                left: 1.7765px;
            }

            #BOX1037 {
                width: 171.093px;
                height: 95.4252px;
                top: 0px;
                left: 36.0432px;
            }

            #BOX1037 > .ladi-box {
                background-color: rgb(255, 255, 255);
                border-style: solid;
                border-color: rgb(11, 104, 230);
                border-width: 2px;
                border-radius: 30px;
            }

            #GROUP1038 {
                width: 50.7082px;
                height: 53.6405px;
                top: 23.3728px;
                left: 0px;
            }

            #BOX1039 {
                width: 50.7082px;
                height: 53.6405px;
                top: 0px;
                left: 0px;
            }

            #BOX1039 > .ladi-box {
                background: rgba(11, 106, 230, 1.0);
                background: -webkit-linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                background: linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                border-radius: 1000px;
            }

            #IMAGE1040 {
                width: 46.4398px;
                height: 48.9495px;
                top: 2.34548px;
                left: 2.13424px;
            }

            #IMAGE1040 > .ladi-image > .ladi-image-background {
                width: 94.9622px;
                height: 49.6969px;
                top: -0.7474px;
                left: -48.5224px;
                background-image: url("https://w.ladicdn.com/s400x350/5e0337be8ee1512e0f853b97/talkshow01-20200610043345.png");
            }

            #IMAGE1040 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1041 {
                width: 165.452px;
                height: 44.6429px;
                top: 6.34119px;
                left: 47.2124px;
            }

            #HEADLINE1043 {
                width: 167px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1043 > .ladi-headline {
                color: rgb(37, 23, 201);
                font-size: 12px;
                font-weight: bold;
                text-align: left;
                line-height: 1.2;
            }

            #PARAGRAPH1045 {
                width: 132px;
                top: 32.0946px;
                left: 0.307202px;
            }

            #PARAGRAPH1045 > .ladi-paragraph {
                color: rgb(5, 34, 74);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #GROUP1046 {
                width: 204.106px;
                height: 95.2496px;
                top: 61.88px;
                left: 217.325px;
            }

            #BOX1047 {
                width: 161.46px;
                height: 95.2496px;
                top: 0px;
                left: 40.9053px;
            }

            #BOX1047 > .ladi-box {
                background-color: rgb(255, 255, 255);
                border-style: solid;
                border-color: rgb(11, 104, 230);
                border-width: 2px;
                border-radius: 30px;
            }

            #GROUP1048 {
                width: 54.1056px;
                height: 53.5396px;
                top: 30.7769px;
                left: 0px;
            }

            #BOX1049 {
                width: 54.1056px;
                height: 53.5396px;
                top: 0px;
                left: 0px;
            }

            #BOX1049 > .ladi-box {
                background: rgba(11, 106, 230, 1.0);
                background: -webkit-linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                background: linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                border-radius: 1000px;
            }

            #IMAGE1050 {
                width: 49.5513px;
                height: 48.8575px;
                top: 2.34106px;
                left: 2.27724px;
            }

            #IMAGE1050 > .ladi-image > .ladi-image-background {
                width: 93.3584px;
                height: 48.8575px;
                top: 0px;
                left: -43.8071px;
                background-image: url("https://w.ladicdn.com/s400x350/5e0337be8ee1512e0f853b97/ban-sao-cua-talkshow01-2-20200610043411.png");
            }

            #IMAGE1050 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
            }

            #HEADLINE1053 {
                width: 150px;
                top: 7.05968px;
                left: 54.1056px;
            }

            #HEADLINE1053 > .ladi-headline {
                color: rgb(37, 23, 201);
                font-size: 12px;
                font-weight: bold;
                text-align: left;
                line-height: 1.2;
            }

            #BUTTON1093 {
                width: 121.075px;
                height: 29px;
                top: 442.402px;
                left: 168.451px;
            }

            #BUTTON1093 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1093.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT1093 {
                width: 167px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1093 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE1094 {
                width: 221px;
                top: 386.402px;
                left: 116px;
            }

            #HEADLINE1094 > .ladi-headline {
                color: rgb(0, 154, 244);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE1096 {
                width: 296px;
                top: 414.402px;
                left: 106px;
            }

            #HEADLINE1096 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 14px;
                font-weight: bold;
                line-height: 1.6;
            }

            #POPUP1097 {
                width: 360px;
                height: 337px;
                top: 0px;
                left: 0px;
                bottom: 0px;
                right: 0px;
                margin: auto;
            }

            #GROUP1098 {
                width: 360px;
                height: 335px;
                top: 0px;
                left: 0px;
            }

            #IMAGE1099 {
                width: 360px;
                height: 335px;
                top: 0px;
                left: 0px;
            }

            #IMAGE1099 > .ladi-image > .ladi-image-background {
                width: 360px;
                height: 335px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s700x650/57b167c9ca57d39c18a1c57c/nen.png");
            }

            #HEADLINE1100 {
                width: 322px;
                top: 125.625px;
                left: 19px;
            }

            #HEADLINE1100 > .ladi-headline {
                color: rgb(37, 23, 201);
                font-size: 24px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #PARAGRAPH1101 {
                width: 284px;
                top: 179.311px;
                left: 38px;
            }

            #PARAGRAPH1101 > .ladi-paragraph {
                color: rgb(33, 33, 33);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.4;
            }

            #LINE1102 {
                width: 48px;
                top: 281.314px;
                left: 156px;
            }

            #LINE1102 > .ladi-line > .ladi-line-container {
                border-top: 3px solid rgb(10, 103, 233);
                border-right: 3px solid rgb(10, 103, 233);
                border-bottom: 3px solid rgb(10, 103, 233);
                border-left: 0px !important;
            }

            #LINE1102 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #GROUP1126 {
                width: 412px;
                height: 97px;
                top: 10px;
                left: 4px;
            }

            #LINE1127 {
                width: 85px;
                top: 79px;
                left: 163.5px;
            }

            #LINE1127 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(242, 67, 13);
                border-right: 2px solid rgb(242, 67, 13);
                border-bottom: 2px solid rgb(242, 67, 13);
                border-left: 0px !important;
            }

            #LINE1127 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #HEADLINE1129 {
                width: 413px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1129 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(5, 31, 77);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #SECTION1130 {
                height: 217px;
            }

            #BUTTON1131 {
                width: 195px;
                height: 40px;
                top: 117px;
                left: 112.5px;
            }

            #BUTTON1131 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1131 > .ladi-button {
                border-style: solid;
                border-color: rgb(0, 0, 0);
                border-width: 1px;
                border-radius: 33px;
            }

            #BUTTON1131.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT1131 {
                width: 193px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1131 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON1132 {
                width: 193px;
                height: 40px;
                top: 167px;
                left: 113.5px;
            }

            #BUTTON1132 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1132 > .ladi-button {
                border-style: solid;
                border-color: rgb(0, 0, 0);
                border-width: 1px;
                border-radius: 33px;
            }

            #BUTTON1132.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT1132 {
                width: 191px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1132 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #PARAGRAPH1176 {
                width: 146px;
                top: 96.977px;
                left: 58px;
            }

            #PARAGRAPH1176 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(0, 154, 244);
                font-size: 7px;
                font-weight: bold;
                line-height: 1.6;
            }

            #PARAGRAPH1177 {
                width: 126px;
                top: 96.977px;
                left: 274.486px;
            }

            #PARAGRAPH1177 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(0, 154, 244);
                font-size: 7px;
                font-weight: bold;
                line-height: 1.6;
            }

            #PARAGRAPH1178 {
                width: 154px;
                top: 157.305px;
                left: 54px;
            }

            #PARAGRAPH1178 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(33, 33, 33);
                font-size: 8px;
                font-weight: bold;
                line-height: 1.6;
            }

            #PARAGRAPH1181 {
                width: 154px;
                top: 157.13px;
                left: 274.486px;
            }

            #PARAGRAPH1181 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(33, 33, 33);
                font-size: 8px;
                font-weight: bold;
                line-height: 1.6;
            }

            #BOX1185 {
                width: 161.61px;
                height: 95.4252px;
                top: 0px;
                left: 34.0455px;
            }

            #BOX1185 > .ladi-box {
                background-color: rgb(255, 255, 255);
                border-style: solid;
                border-color: rgb(11, 104, 230);
                border-width: 2px;
                border-radius: 30px;
            }

            #BOX1187 {
                width: 47.8977px;
                height: 53.6405px;
                top: 0px;
                left: 0px;
            }

            #BOX1187 > .ladi-box {
                background: rgba(11, 106, 230, 1.0);
                background: -webkit-linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                background: linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                border-radius: 1000px;
            }

            #IMAGE1188 {
                width: 43.8659px;
                height: 48.9495px;
                top: 2.34548px;
                left: 2.01595px;
            }

            #IMAGE1188 > .ladi-image > .ladi-image-background {
                width: 94.9622px;
                height: 49.6969px;
                top: 0px;
                left: -45px;
                background-image: url("https://w.ladicdn.com/s400x350/5e0337be8ee1512e0f853b97/ban-sao-cua-talkshow01-3-20200610043454.png");
            }

            #IMAGE1188 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1186 {
                width: 47.8977px;
                height: 53.6405px;
                top: 23.3728px;
                left: 0px;
            }

            #GROUP1184 {
                width: 209.898px;
                height: 95.4252px;
                top: 223.842px;
                left: 222.102px;
            }

            #GROUP1189 {
                width: 158px;
                height: 44.0946px;
                top: 4.34119px;
                left: 51.8977px;
            }

            #HEADLINE1191 {
                width: 158px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1191 > .ladi-headline {
                color: rgb(37, 23, 201);
                font-size: 12px;
                font-weight: bold;
                text-align: left;
                line-height: 1.2;
            }

            #PARAGRAPH1190 {
                width: 125px;
                top: 32.0946px;
                left: 0.290175px;
            }

            #PARAGRAPH1190 > .ladi-paragraph {
                color: rgb(5, 34, 74);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #PARAGRAPH1193 {
                width: 154px;
                top: 334.477px;
                left: 58px;
            }

            #PARAGRAPH1193 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(33, 33, 33);
                font-size: 8px;
                font-weight: bold;
                line-height: 1.6;
            }

            #BOX1197 {
                width: 167.875px;
                height: 95.4252px;
                top: 0px;
                left: 35.3654px;
            }

            #BOX1197 > .ladi-box {
                background-color: rgb(255, 255, 255);
                border-style: solid;
                border-color: rgb(11, 104, 230);
                border-width: 2px;
                border-radius: 30px;
            }

            #BOX1199 {
                width: 49.7546px;
                height: 53.6405px;
                top: 0px;
                left: 0px;
            }

            #BOX1199 > .ladi-box {
                background: rgba(11, 106, 230, 1.0);
                background: -webkit-linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                background: linear-gradient(9deg, rgba(11, 106, 230, 1.0), rgba(234, 242, 254, 1.0));
                border-radius: 1000px;
            }

            #IMAGE1200 {
                width: 45.5665px;
                height: 48.9495px;
                top: 2.34548px;
                left: 2.0941px;
            }

            #IMAGE1200 > .ladi-image > .ladi-image-background {
                width: 94.9622px;
                height: 49.6969px;
                top: 0px;
                left: -42px;
                background-image: url("https://w.ladicdn.com/s400x350/5e0337be8ee1512e0f853b97/ban-sao-cua-ban-sao-cua-talkshow01-2-20200610043436.png");
            }

            #IMAGE1200 > .ladi-image {
                border-color: rgb(255, 255, 255);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1198 {
                width: 49.7546px;
                height: 53.6405px;
                top: 23.3728px;
                left: 0px;
            }

            #PARAGRAPH1202 {
                width: 130px;
                top: 32.0946px;
                left: 0.301425px;
            }

            #PARAGRAPH1202 > .ladi-paragraph {
                color: rgb(5, 34, 74);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #HEADLINE1203 {
                width: 164px;
                top: 0px;
                left: 0px;
            }

            #HEADLINE1203 > .ladi-headline {
                color: rgb(37, 23, 201);
                font-size: 12px;
                font-weight: bold;
                text-align: left;
                line-height: 1.2;
            }

            #GROUP1201 {
                width: 163.231px;
                height: 44.0008px;
                top: 6.34119px;
                left: 46.3245px;
            }

            #GROUP1196 {
                width: 209.555px;
                height: 95.4252px;
                top: 223.842px;
                left: 2.84217px;
            }

            #PARAGRAPH1204 {
                width: 146px;
                top: 264.555px;
                left: 274.486px;
            }

            #PARAGRAPH1204 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(0, 154, 244);
                font-size: 7px;
                font-weight: bold;
                line-height: 1.6;
            }

            #PARAGRAPH1205 {
                width: 154px;
                top: 324.555px;
                left: 278px;
            }

            #PARAGRAPH1205 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(33, 33, 33);
                font-size: 8px;
                font-weight: bold;
                line-height: 1.6;
            }

            #SECTION1262 {
                height: 1131.33px;
                display: none !important;
            }

            #SECTION1262 > .ladi-section-background {
                background-color: rgb(234, 242, 255);
            }

            #FRAME1264 {
                width: 278px;
                height: 353px;
                top: 382px;
                left: 71px;
            }

            #FRAME1264 > .ladi-frame > .ladi-frame-background {
                background-color: rgb(255, 255, 255);
            }

            #FRAME1264 > .ladi-frame {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
            }

            #SHAPE1265 {
                width: 319.249px;
                height: 189.132px;
                top: 248.392px;
                left: -25.6245px;
            }

            #SHAPE1265 > .ladi-shape {
                transform: rotate(-165deg);
                -webkit-transform: rotate(-165deg);
            }

            #SHAPE1265 svg:last-child {
                fill: url("#SHAPE1265_desktop_gradient");
            }

            #SHAPE1266 {
                width: 28.3984px;
                height: 23.6646px;
                top: 28.896px;
                left: 24.001px;
            }

            #SHAPE1266 svg:last-child {
                fill: rgba(242, 67, 13, 1.0);
            }

            #PARAGRAPH1267 {
                width: 240px;
                top: 63.938px;
                left: 19px;
            }

            #PARAGRAPH1267 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(5, 31, 77);
                font-size: 14px;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE1269 {
                width: 227px;
                top: 304.896px;
                left: 27px;
            }

            #HEADLINE1269 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #FRAME1271 {
                width: 278px;
                height: 353px;
                top: 10px;
                left: 71px;
            }

            #FRAME1271 > .ladi-frame > .ladi-frame-background {
                background-color: rgb(255, 255, 255);
            }

            #FRAME1271 > .ladi-frame {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
            }

            #SHAPE1272 {
                width: 319.249px;
                height: 189.132px;
                top: 248.392px;
                left: -25.6245px;
            }

            #SHAPE1272 > .ladi-shape {
                transform: rotate(-165deg);
                -webkit-transform: rotate(-165deg);
            }

            #SHAPE1272 svg:last-child {
                fill: url("#SHAPE1272_desktop_gradient");
            }

            #SHAPE1273 {
                width: 28.3984px;
                height: 23.6646px;
                top: 28.896px;
                left: 24.001px;
            }

            #SHAPE1273 svg:last-child {
                fill: rgba(242, 67, 13, 1.0);
            }

            #PARAGRAPH1274 {
                width: 240px;
                top: 63.938px;
                left: 19px;
            }

            #PARAGRAPH1274 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(5, 31, 77);
                font-size: 14px;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE1276 {
                width: 227px;
                top: 304.896px;
                left: 27px;
            }

            #HEADLINE1276 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #FRAME1278 {
                width: 278px;
                height: 353px;
                top: 763px;
                left: 71px;
            }

            #FRAME1278 > .ladi-frame > .ladi-frame-background {
                background-color: rgb(255, 255, 255);
            }

            #FRAME1278 > .ladi-frame {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
            }

            #SHAPE1279 {
                width: 319.249px;
                height: 189.132px;
                top: 248.392px;
                left: -25.6245px;
            }

            #SHAPE1279 > .ladi-shape {
                transform: rotate(-165deg);
                -webkit-transform: rotate(-165deg);
            }

            #SHAPE1279 svg:last-child {
                fill: url("#SHAPE1279_desktop_gradient");
            }

            #SHAPE1280 {
                width: 28.3984px;
                height: 23.6646px;
                top: 28.896px;
                left: 24.001px;
            }

            #SHAPE1280 svg:last-child {
                fill: rgba(242, 67, 13, 1.0);
            }

            #PARAGRAPH1281 {
                width: 240px;
                top: 63.938px;
                left: 19px;
            }

            #PARAGRAPH1281 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(5, 31, 77);
                font-size: 14px;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE1283 {
                width: 227px;
                top: 304.896px;
                left: 27px;
            }

            #HEADLINE1283 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #PARAGRAPH1289 {
                width: 146px;
                top: 259.055px;
                left: 54px;
            }

            #PARAGRAPH1289 > .ladi-paragraph {
                font-family: "Roboto Slab", serif;
                color: rgb(0, 154, 244);
                font-size: 7px;
                font-weight: bold;
                line-height: 1.6;
            }

            #SECTION1294 {
                height: 442.357px;
                display: none !important;
            }

            #SECTION1294 > .ladi-section-background {
                background-color: rgb(234, 242, 255);
            }

            #FRAME1296 {
                width: 278px;
                height: 353px;
                top: 9px;
                left: 71px;
            }

            #FRAME1296 > .ladi-frame > .ladi-frame-background {
                background-color: rgb(255, 255, 255);
            }

            #FRAME1296 > .ladi-frame {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
            }

            #SHAPE1297 {
                width: 319.249px;
                height: 189.132px;
                top: 248.392px;
                left: -25.6245px;
            }

            #SHAPE1297 > .ladi-shape {
                transform: rotate(-165deg);
                -webkit-transform: rotate(-165deg);
            }

            #SHAPE1297 svg:last-child {
                fill: url("#SHAPE1297_desktop_gradient");
            }

            #SHAPE1298 {
                width: 28.3984px;
                height: 23.6646px;
                top: 28.896px;
                left: 24.001px;
            }

            #SHAPE1298 svg:last-child {
                fill: rgba(242, 67, 13, 1.0);
            }

            #PARAGRAPH1299 {
                width: 240px;
                top: 63.938px;
                left: 19px;
            }

            #PARAGRAPH1299 > .ladi-paragraph {
                font-family: "Open Sans", sans-serif;
                color: rgb(5, 31, 77);
                font-size: 14px;
                text-align: center;
                line-height: 1.6;
            }

            #HEADLINE1301 {
                width: 226px;
                top: 304.896px;
                left: 27px;
            }

            #HEADLINE1301 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 14px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #HEADLINE1302 {
                width: 204px;
                top: 325.389px;
                left: 39px;
            }

            #HEADLINE1302 > .ladi-headline {
                font-family: "Open Sans", sans-serif;
                color: rgb(255, 255, 255);
                font-size: 12px;
                font-style: italic;
                text-align: center;
                line-height: 1.2;
            }

            #BUTTON1322 {
                width: 127.639px;
                height: 26px;
                top: 170.305px;
                left: 54px;
            }

            #BUTTON1322 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1322.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT1322 {
                width: 160px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1322 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 9px;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON_TEXT1323 {
                width: 160px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1323 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 9px;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON1323 {
                width: 135.361px;
                height: 27px;
                top: 347.477px;
                left: 274.486px;
            }

            #BUTTON1323 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1323.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT1325 {
                width: 160px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1325 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 9px;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON1325 {
                width: 135.361px;
                height: 23.875px;
                top: 170.13px;
                left: 274.486px;
            }

            #BUTTON1325 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1325.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: infinite;
                -webkit-animation-iteration-count: infinite;
            }

            #BUTTON_TEXT1327 {
                width: 160px;
                top: 9px;
                left: 0px;
            }

            #BUTTON_TEXT1327 > .ladi-headline {
                color: rgb(255, 255, 255);
                font-size: 9px;
                text-align: center;
                line-height: 1.6;
            }

            #BUTTON1327 {
                width: 127.639px;
                height: 23.875px;
                top: 349.039px;
                left: 54px;
            }

            #BUTTON1327 > .ladi-button > .ladi-button-background {
                background: #ff6a00;
                background: -webkit-linear-gradient(180deg, #ff6a00, #ee0979);
                background: linear-gradient(180deg, #ff6a00, #ee0979);
            }

            #BUTTON1327.ladi-animation > .ladi-button {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 0s;
                -webkit-animation-delay: 0s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #SECTION1329 {
                height: 583.425px;
            }

            #HEADLINE1330 {
                width: 400px;
                top: 0px;
                left: 5.0005px;
            }

            #HEADLINE1330 > .ladi-headline {
                color: rgb(13, 98, 242);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #IMAGE1340 {
                width: 135.847px;
                height: 40.2334px;
                top: 127.109px;
                left: 31.461px;
            }

            #IMAGE1340 > .ladi-image > .ladi-image-background {
                width: 135.847px;
                height: 40.2334px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x350/5e0337be8ee1512e0f853b97/vinalink-logo-20200507082310.png");
            }

            #IMAGE1341 {
                width: 85.3833px;
                height: 85.3833px;
                top: 265.995px;
                left: 147.666px;
            }

            #IMAGE1341 > .ladi-image > .ladi-image-background {
                width: 85.3833px;
                height: 85.3833px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s400x400/5e0337be8ee1512e0f853b97/download-20200507082317.png");
            }

            #IMAGE1342 {
                width: 87.026px;
                height: 87.026px;
                top: 48px;
                left: 184.488px;
            }

            #IMAGE1342 > .ladi-image > .ladi-image-background {
                width: 87.026px;
                height: 87.026px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s400x400/5e0337be8ee1512e0f853b97/vag-20200507082605.jpg");
            }

            #IMAGE1343 {
                width: 98px;
                height: 98px;
                top: 259.687px;
                left: 18.671px;
            }

            #IMAGE1343 > .ladi-image > .ladi-image-background {
                width: 98px;
                height: 98px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s400x400/5e0337be8ee1512e0f853b97/04f4d6de12efe8b1b1fe-20200520040203.jpg");
            }

            #IMAGE1344 {
                width: 161px;
                height: 48.7431px;
                top: 299.462px;
                left: 244.001px;
            }

            #IMAGE1344 > .ladi-image > .ladi-image-background {
                width: 161px;
                height: 48.7431px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s500x350/5e0337be8ee1512e0f853b97/99296459_259634028741024_1548725250856845312_n-20200527024314.png");
            }

            #IMAGE1345 {
                width: 200px;
                height: 66.3571px;
                top: 372px;
                left: 110px;
            }

            #IMAGE1345 > .ladi-image > .ladi-image-background {
                width: 200px;
                height: 66.3571px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x400/5e0337be8ee1512e0f853b97/logo-i-mentor-1-20200506234942.png");
            }

            #IMAGE1346 {
                width: 105px;
                height: 105px;
                top: 94.7257px;
                left: 293.5px;
            }

            #IMAGE1346 > .ladi-image > .ladi-image-background {
                width: 105px;
                height: 105px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/logo--20200527024738.jpg");
            }

            #IMAGE1347 {
                width: 107.275px;
                height: 107.275px;
                top: 428.472px;
                left: 244.001px;
            }

            #IMAGE1347 > .ladi-image > .ladi-image-background {
                width: 107.275px;
                height: 107.275px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/ea485681d93623687a27-20200527030229.jpg");
            }

            #SECTION1348 {
                height: 308.644px;
            }

            #SECTION1348 > .ladi-section-background {
                background-color: rgb(255, 255, 255);
            }

            #HEADLINE1355 {
                width: 400px;
                top: 4px;
                left: 10.0005px;
            }

            #HEADLINE1355 > .ladi-headline {
                color: rgb(13, 98, 242);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #IMAGE1358 {
                width: 124.669px;
                height: 115.318px;
                top: 176px;
                left: 147.666px;
            }

            #IMAGE1358 > .ladi-image > .ladi-image-background {
                width: 124.669px;
                height: 124.668px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x450/5e0337be8ee1512e0f853b97/b45645a788e275bc2cf3-20200603024122.jpg");
            }

            #IMAGE556 {
                width: 118.622px;
                height: 39.3571px;
                top: 28.8763px;
                left: 0.5px;
            }

            #IMAGE556 > .ladi-image > .ladi-image-background {
                width: 118.622px;
                height: 39.3571px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s450x350/5e0337be8ee1512e0f853b97/logo-i-mentor-1-1-20200506235002.png");
            }

            #IMAGE1359 {
                width: 222.659px;
                height: 70.3571px;
                top: 100px;
                left: 116.671px;
            }

            #IMAGE1359 > .ladi-image > .ladi-image-background {
                width: 222.659px;
                height: 70.3571px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x400/5e0337be8ee1512e0f853b97/logo-i-mentor-1-20200506234942.png");
            }

            #SECTION1360 {
                height: 970.62px;
            }

            #GROUP1361 {
                width: 197.188px;
                height: 197.188px;
                top: 0px;
                left: 111.406px;
            }

            #BOX1362 {
                width: 197.188px;
                height: 197.188px;
                top: 0px;
                left: 0px;
            }

            #BOX1362 > .ladi-box {
                background-color: rgb(234, 242, 254);
                border-radius: 1000px;
            }

            #HEADLINE1363 {
                width: 187px;
                top: 85.088px;
                left: 4.28313px;
            }

            #HEADLINE1363 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1;
            }

            #GROUP1364 {
                width: 384.88px;
                height: 180.655px;
                top: 214px;
                left: 21.2706px;
            }

            #IMAGE1365 {
                width: 181.302px;
                height: 180.655px;
                top: 0px;
                left: 203.578px;
            }

            #IMAGE1365 > .ladi-image > .ladi-image-background {
                width: 181.302px;
                height: 181.977px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s500x500/5e0337be8ee1512e0f853b97/24261005-start-up-business-concept-stock-photo-1-20200603031942.jpg");
            }

            #IMAGE1365 > .ladi-image {
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1366 {
                width: 220px;
                height: 69px;
                top: 43.327px;
                left: 0px;
            }

            #LINE1367 {
                width: 178px;
                top: 40px;
                left: 29px;
            }

            #LINE1367 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(10, 103, 233);
                border-right: 2px solid rgb(10, 103, 233);
                border-bottom: 2px solid rgb(10, 103, 233);
                border-left: 0px !important;
            }

            #LINE1367 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #BOX1368 {
                width: 33px;
                height: 33px;
                top: 32.5px;
                left: 0px;
            }

            #BOX1368 > .ladi-box {
                background: #fdfbfb;
                background: -webkit-linear-gradient(180deg, #fdfbfb, #eaedee);
                background: linear-gradient(180deg, #fdfbfb, #eaedee);
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 2px;
                border-radius: 1000px;
            }

            #HEADLINE1369 {
                width: 176px;
                top: 0px;
                left: 44px;
            }

            #HEADLINE1369 > .ladi-headline {
                color: rgb(248, 5, 5);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #PARAGRAPH1370 {
                width: 142px;
                top: 57px;
                left: 44px;
            }

            #PARAGRAPH1370 > .ladi-paragraph {
                color: rgb(5, 34, 74);
                font-size: 14px;
                text-align: left;
                line-height: 1.4;
            }

            #HEADLINE1371 {
                width: 25px;
                top: 36.5px;
                left: 4px;
            }

            #HEADLINE1371 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP1372 {
                width: 384.881px;
                height: 180.655px;
                top: 399.322px;
                left: 17.5598px;
            }

            #IMAGE1373 {
                width: 181.302px;
                height: 180.655px;
                top: 0px;
                left: 203.579px;
            }

            #IMAGE1373 > .ladi-image > .ladi-image-background {
                width: 181.302px;
                height: 271.67px;
                top: -22px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s500x600/5e0337be8ee1512e0f853b97/meeting-business-people-avatar-character_24877-57276-20200603033232.jpg");
            }

            #IMAGE1373 > .ladi-image {
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1374 {
                width: 223px;
                height: 63.5px;
                top: 42.327px;
                left: 0px;
            }

            #LINE1375 {
                width: 178px;
                top: 38px;
                left: 29px;
            }

            #LINE1375 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(10, 103, 233);
                border-right: 2px solid rgb(10, 103, 233);
                border-bottom: 2px solid rgb(10, 103, 233);
                border-left: 0px !important;
            }

            #LINE1375 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #BOX1376 {
                width: 33px;
                height: 33px;
                top: 30.5px;
                left: 0px;
            }

            #BOX1376 > .ladi-box {
                background: #fdfbfb;
                background: -webkit-linear-gradient(180deg, #fdfbfb, #eaedee);
                background: linear-gradient(180deg, #fdfbfb, #eaedee);
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 2px;
                border-radius: 1000px;
            }

            #HEADLINE1377 {
                width: 176px;
                top: 0px;
                left: 47px;
            }

            #HEADLINE1377 > .ladi-headline {
                color: rgb(240, 8, 8);
                font-size: 16px;
                font-weight: bold;
                line-height: 1.6;
            }

            #HEADLINE1379 {
                width: 25px;
                top: 34.5px;
                left: 4px;
            }

            #HEADLINE1379 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP1380 {
                width: 392.302px;
                height: 180.655px;
                top: 774.965px;
                left: 21.2706px;
            }

            #IMAGE1381 {
                width: 181.302px;
                height: 180.655px;
                top: 0px;
                left: 0px;
            }

            #IMAGE1381 > .ladi-image > .ladi-image-background {
                width: 181.302px;
                height: 181.977px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s500x500/5e0337be8ee1512e0f853b97/flat-leadership-design_23-2147939846-20200603033413.jpg");
            }

            #IMAGE1381 > .ladi-image {
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1382 {
                width: 211px;
                height: 66.5px;
                top: 34.327px;
                left: 181.302px;
            }

            #LINE1383 {
                width: 178px;
                top: 41px;
                left: 0px;
            }

            #LINE1383 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(10, 103, 233);
                border-right: 2px solid rgb(10, 103, 233);
                border-bottom: 2px solid rgb(10, 103, 233);
                border-left: 0px !important;
            }

            #LINE1383 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #HEADLINE1384 {
                width: 176px;
                top: 0px;
                left: 17.5px;
            }

            #HEADLINE1384 > .ladi-headline {
                color: rgb(250, 9, 9);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP1386 {
                width: 33px;
                height: 33px;
                top: 33.5px;
                left: 178px;
            }

            #BOX1387 {
                width: 33px;
                height: 33px;
                top: 0px;
                left: 0px;
            }

            #BOX1387 > .ladi-box {
                background: #fdfbfb;
                background: -webkit-linear-gradient(180deg, #fdfbfb, #eaedee);
                background: linear-gradient(180deg, #fdfbfb, #eaedee);
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 2px;
                border-radius: 1000px;
            }

            #HEADLINE1388 {
                width: 25px;
                top: 4px;
                left: 4px;
            }

            #HEADLINE1388 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP1389 {
                width: 392.302px;
                height: 180.655px;
                top: 586.643px;
                left: 17.5598px;
            }

            #IMAGE1390 {
                width: 181.302px;
                height: 180.655px;
                top: 0px;
                left: 0px;
            }

            #IMAGE1390 > .ladi-image > .ladi-image-background {
                width: 181.302px;
                height: 181.977px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s500x500/5e0337be8ee1512e0f853b97/man-woman-working-their-job_52683-24245-20200603033623.jpg");
            }

            #IMAGE1390 > .ladi-image {
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 5px;
                border-radius: 300px;
            }

            #GROUP1391 {
                width: 211px;
                height: 88.5px;
                top: 22.327px;
                left: 181.302px;
            }

            #LINE1392 {
                width: 178px;
                top: 63px;
                left: 0px;
            }

            #LINE1392 > .ladi-line > .ladi-line-container {
                border-top: 2px solid rgb(10, 103, 233);
                border-right: 2px solid rgb(10, 103, 233);
                border-bottom: 2px solid rgb(10, 103, 233);
                border-left: 0px !important;
            }

            #LINE1392 > .ladi-line {
                width: 100%;
                padding: 8px 0px;
            }

            #HEADLINE1393 {
                width: 176px;
                top: 0px;
                left: 17.5px;
            }

            #HEADLINE1393 > .ladi-headline {
                color: rgb(238, 10, 10);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #GROUP1395 {
                width: 33px;
                height: 33px;
                top: 55.5px;
                left: 178px;
            }

            #BOX1396 {
                width: 33px;
                height: 33px;
                top: 0px;
                left: 0px;
            }

            #BOX1396 > .ladi-box {
                background: #fdfbfb;
                background: -webkit-linear-gradient(180deg, #fdfbfb, #eaedee);
                background: linear-gradient(180deg, #fdfbfb, #eaedee);
                border-style: solid;
                border-color: rgb(10, 103, 233);
                border-width: 2px;
                border-radius: 1000px;
            }

            #HEADLINE1397 {
                width: 25px;
                top: 4px;
                left: 4px;
            }

            #HEADLINE1397 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 16px;
                font-weight: bold;
                text-align: center;
                line-height: 1.6;
            }

            #SECTION1427 {
                height: 152px;
            }

            #SECTION1427 > .ladi-section-background {
                background-color: rgba(235, 243, 255, 0.5);
            }

            #HEADLINE1429 {
                width: 338px;
                top: 14px;
                left: 48.4216px;
            }

            #HEADLINE1429 > .ladi-headline {
                color: rgb(5, 31, 77);
                font-size: 30px;
                font-weight: bold;
                text-align: center;
                line-height: 1.2;
            }

            #GROUP1431 {
                width: 160.112px;
                height: 60.5678px;
                top: 56px;
                left: 3.22985px;
            }

            #BOX1432 {
                width: 147.458px;
                height: 60.5592px;
                top: 0px;
                left: 0px;
            }

            #BOX1432 > .ladi-box {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                background-color: rgb(255, 255, 255);
            }

            #HEADLINE1433 {
                width: 75px;
                top: 11.5678px;
                left: 36.7959px;
            }

            #HEADLINE1433 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 25px;
                text-align: left;
                line-height: 1.2;
            }

            #HEADLINE1433.ladi-animation > .ladi-headline {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #HEADLINE1434 {
                width: 156px;
                top: 41.5678px;
                left: 4.11171px;
            }

            #HEADLINE1434 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 12px;
                font-weight: bold;
                text-align: left;
                line-height: 1.6;
            }

            #GROUP1435 {
                width: 32.6839px;
                height: 26.5632px;
                top: 16.998px;
                left: 4.11171px;
            }

            #BOX1436 {
                width: 32.6839px;
                height: 26.5632px;
                top: 0px;
                left: 0px;
            }

            #BOX1436 > .ladi-box {
                background-color: rgba(255, 255, 255, 0);
                border-style: solid;
                border-color: rgba(10, 103, 233, 0.5);
                border-width: 1px;
                border-radius: 1000px;
            }

            #SHAPE1437 {
                width: 21.3061px;
                height: 17.316px;
                top: 4.50645px;
                left: 5.73111px;
            }

            #SHAPE1437 svg:last-child {
                fill: rgba(5, 34, 74, 1.0);
            }

            #GROUP1438 {
                width: 162.119px;
                height: 65.5612px;
                top: 56px;
                left: 149.354px;
            }

            #BOX1439 {
                width: 140.898px;
                height: 60.5592px;
                top: 0px;
                left: 0px;
            }

            #BOX1439 > .ladi-box {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                background-color: rgb(255, 255, 255);
            }

            #HEADLINE1440 {
                width: 132px;
                top: 10.872px;
                left: 30.119px;
            }

            #HEADLINE1440 > .ladi-headline {
                color: rgb(242, 67, 13);
                font-size: 25px;
                text-align: left;
                line-height: 1.2;
            }

            #HEADLINE1440.ladi-animation > .ladi-headline {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #HEADLINE1441 {
                width: 102px;
                top: 43.5612px;
                left: 35.6012px;
            }

            #HEADLINE1441 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 14px;
                font-weight: bold;
                text-align: left;
                line-height: 1.6;
            }

            #BOX1442 {
                width: 31.23px;
                height: 26.5632px;
                top: 16.998px;
                left: 0px;
            }

            #BOX1442 > .ladi-box {
                background-color: rgba(255, 255, 255, 0);
                border-style: solid;
                border-color: rgba(10, 103, 233, 0.5);
                border-width: 1px;
                border-radius: 1000px;
            }

            #SHAPE1443 {
                width: 16.2772px;
                height: 13.8449px;
                top: 23.3572px;
                left: 7.47642px;
            }

            #SHAPE1443 svg:last-child {
                fill: rgba(5, 31, 77, 1.0);
            }

            #GROUP1444 {
                width: 131.125px;
                height: 63.5612px;
                top: 56px;
                left: 293.067px;
            }

            #BOX1445 {
                width: 126.889px;
                height: 60.5592px;
                top: 0px;
                left: 0px;
            }

            #BOX1445 > .ladi-box {
                box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                -webkit-box-shadow: 0px 0px 30px -15px rgba(0, 0, 0, 0.2);
                background-color: rgb(255, 255, 255);
            }

            #HEADLINE1446 {
                width: 103px;
                top: 11.5678px;
                left: 28.1249px;
            }

            #HEADLINE1446 > .ladi-headline {
                color: rgb(10, 103, 233);
                font-size: 23px;
                text-align: left;
                line-height: 1.2;
            }

            #HEADLINE1446.ladi-animation > .ladi-headline {
                animation-name: pulse;
                -webkit-animation-name: pulse;
                animation-delay: 1s;
                -webkit-animation-delay: 1s;
                animation-duration: 3s;
                -webkit-animation-duration: 3s;
                animation-iteration-count: 1;
                -webkit-animation-iteration-count: 1;
            }

            #HEADLINE1447 {
                width: 75px;
                top: 43.5612px;
                left: 42.1249px;
            }

            #HEADLINE1447 > .ladi-headline {
                color: rgb(0, 0, 0);
                font-size: 13px;
                font-weight: bold;
                text-align: left;
                line-height: 1.6;
            }

            #BOX1448 {
                width: 28.1249px;
                height: 26.5632px;
                top: 16.998px;
                left: 0px;
            }

            #BOX1448 > .ladi-box {
                background-color: rgba(255, 255, 255, 0);
                border-style: solid;
                border-color: rgba(10, 103, 233, 0.5);
                border-width: 1px;
                border-radius: 1000px;
            }

            #SHAPE1449 {
                width: 16.6707px;
                height: 15.7452px;
                top: 22.1526px;
                left: 5.72696px;
            }

            #SHAPE1449 svg:last-child {
                fill: rgba(5, 31, 77, 1.0);
            }

            #IMAGE1457 {
                width: 200px;
                height: 68.6734px;
                top: 368.205px;
                left: 215.5px;
            }

            #IMAGE1457 > .ladi-image > .ladi-image-background {
                width: 200px;
                height: 68.6734px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x400/5e0337be8ee1512e0f853b97/105288713_190068589094741_4828959623268428556_n-20200627030256.png");
            }

            #IMAGE1458 {
                width: 200px;
                height: 51.8607px;
                top: 376.611px;
                left: 15.5px;
            }

            #IMAGE1458 > .ladi-image > .ladi-image-background {
                width: 200px;
                height: 51.8607px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x400/5e0337be8ee1512e0f853b97/104493260_2639813402930081_8900506494812502845_n-1-20200627030256.png");
            }

            #IMAGE1459 {
                width: 200px;
                height: 52.8px;
                top: 203.342px;
                left: 105px;
            }

            #IMAGE1459 > .ladi-image > .ladi-image-background {
                width: 200px;
                height: 52.8px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x400/5e0337be8ee1512e0f853b97/logo-ngang-2-20200627030324.png");
            }

            #IMAGE1460 {
                width: 244.323px;
                height: 44.099px;
                top: 453.878px;
                left: 15.5px;
            }

            #IMAGE1460 > .ladi-image > .ladi-image-background {
                width: 244.323px;
                height: 44.099px;
                top: 0px;
                left: 0px;
                background-image: url("https://w.ladicdn.com/s550x350/5e0337be8ee1512e0f853b97/logo-20200627030324.png");
            }
        }

        @media(max-width: 768px) {
            .mobile {
                display: block;
            }
            .desktop {
                display: none;
            }
        }

        @media(min-width: 768px) {
            .mobile {
                display: none;
            }
            .desktop {
                display: block;
            }
        }
    </style>


    <script id="script_event_data" type="text/javascript">(function () {
            var run = function () {
                if (typeof window.LadiPageScript == "undefined" || window.LadiPageScript == undefined || typeof window.ladi == "undefined" || window.ladi == undefined) {
                    setTimeout(run, 100);
                    return;
                }
                window.LadiPageApp = window.LadiPageApp || new window.LadiPageAppV2();
                window.LadiPageScript.runtime.ladipage_id = '5eb34ce26b12637b2bd2952b';
                window.LadiPageScript.runtime.isMobileOnly = false;
                window.LadiPageScript.runtime.DOMAIN_SET_COOKIE = ["imentor.vn"];
                window.LadiPageScript.runtime.DOMAIN_FREE = [];
                window.LadiPageScript.runtime.bodyFontSize = 12;
                window.LadiPageScript.runtime.time_zone = 7;
                window.LadiPageScript.runtime.currency = "VND";
                window.LadiPageScript.runtime.eventData = "%7B%22BUTTON443%22%3A%7B%22type%22%3A%22button%22%2C%22option.data_action%22%3A%7B%22type%22%3A%22section%22%2C%22action%22%3A%22SECTION718%22%7D%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22desktop.style.animation-delay%22%3A%221s%22%2C%22mobile.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-delay%22%3A%221s%22%7D%2C%22BUTTON614%22%3A%7B%22type%22%3A%22button%22%2C%22option.data_action%22%3A%7B%22type%22%3A%22popup%22%2C%22action%22%3A%22POPUP806%22%7D%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22desktop.style.animation-delay%22%3A%220s%22%2C%22mobile.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-delay%22%3A%220s%22%7D%2C%22GROUP656%22%3A%7B%22type%22%3A%22group%22%2C%22desktop.style.animation-name%22%3A%22bounceInRight%22%2C%22desktop.style.animation-delay%22%3A%221s%22%2C%22mobile.option.auto_scroll%22%3Atrue%2C%22mobile.style.animation-name%22%3A%22bounceInRight%22%2C%22mobile.style.animation-delay%22%3A%220s%22%7D%2C%22GROUP743%22%3A%7B%22type%22%3A%22group%22%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22desktop.style.animation-delay%22%3A%221s%22%7D%2C%22FORM_ITEM785%22%3A%7B%22type%22%3A%22form_item%22%2C%22option.input_type%22%3A%22tel%22%2C%22option.input_tabindex%22%3A3%7D%2C%22FORM_ITEM784%22%3A%7B%22type%22%3A%22form_item%22%2C%22option.input_type%22%3A%22email%22%2C%22option.input_tabindex%22%3A2%7D%2C%22FORM_ITEM783%22%3A%7B%22type%22%3A%22form_item%22%2C%22option.input_type%22%3A%22text%22%2C%22option.input_tabindex%22%3A1%7D%2C%22FORM780%22%3A%7B%22type%22%3A%22form%22%2C%22option.form_config_id%22%3A%225eb3800081e7d47b21ee8a72%22%2C%22option.form_send_ladipage%22%3Atrue%2C%22option.thankyou_type%22%3A%22default%22%2C%22option.thankyou_value%22%3A%22C%E1%BA%A3m%20%C6%A1n%20b%E1%BA%A1n%20%C4%91%C3%A3%20quan%20t%C3%A2m!%22%2C%22option.form_auto_funnel%22%3Atrue%2C%22option.form_auto_complete%22%3Atrue%7D%2C%22COUNTDOWN771%22%3A%7B%22type%22%3A%22countdown%22%2C%22option.countdown_type%22%3A%22countdown%22%2C%22option.countdown_minute%22%3A720%7D%2C%22COUNTDOWN_ITEM772%22%3A%7B%22type%22%3A%22countdown_item%22%2C%22option.countdown_item_type%22%3A%22day%22%7D%2C%22COUNTDOWN_ITEM773%22%3A%7B%22type%22%3A%22countdown_item%22%2C%22option.countdown_item_type%22%3A%22hour%22%7D%2C%22COUNTDOWN_ITEM774%22%3A%7B%22type%22%3A%22countdown_item%22%2C%22option.countdown_item_type%22%3A%22minute%22%7D%2C%22COUNTDOWN_ITEM775%22%3A%7B%22type%22%3A%22countdown_item%22%2C%22option.countdown_item_type%22%3A%22seconds%22%7D%2C%22IMAGE791%22%3A%7B%22type%22%3A%22image%22%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22desktop.style.animation-delay%22%3A%220s%22%2C%22mobile.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-delay%22%3A%220s%22%7D%2C%22VIDEO792%22%3A%7B%22type%22%3A%22video%22%2C%22option.video_value%22%3A%22https%3A%2F%2Fyoutu.be%2Fb3czYDhk3ok%22%2C%22option.video_type%22%3A%22youtube%22%2C%22option.video_control%22%3Atrue%7D%2C%22POPUP806%22%3A%7B%22type%22%3A%22popup%22%2C%22option.show_popup_welcome_page%22%3Atrue%2C%22option.delay_popup_welcome_page%22%3A7%2C%22desktop.option.popup_position%22%3A%22default%22%2C%22desktop.option.popup_backdrop%22%3A%22background-color%3A%20rgba(0%2C%200%2C%200%2C%200.5)%3B%22%2C%22mobile.option.popup_position%22%3A%22default%22%2C%22mobile.option.popup_backdrop%22%3A%22background-color%3A%20rgba(0%2C%200%2C%200%2C%200.5)%3B%22%7D%2C%22FORM816%22%3A%7B%22type%22%3A%22form%22%2C%22option.form_config_id%22%3A%225eb3800081e7d47b21ee8a72%22%2C%22option.form_send_ladipage%22%3Atrue%2C%22option.thankyou_type%22%3A%22default%22%2C%22option.thankyou_value%22%3A%22C%C3%A1m%20%C6%A1n%20b%E1%BA%A1n%20%C4%91%C3%A3%20quan%20t%C3%A2m%22%2C%22option.form_auto_funnel%22%3Atrue%2C%22option.form_auto_complete%22%3Atrue%7D%2C%22FORM_ITEM817%22%3A%7B%22type%22%3A%22form_item%22%2C%22option.input_type%22%3A%22email%22%2C%22option.input_tabindex%22%3A1%7D%2C%22FORM_ITEM822%22%3A%7B%22type%22%3A%22form_item%22%2C%22option.input_type%22%3A%22text%22%2C%22option.input_tabindex%22%3A2%7D%2C%22HEADLINE823%22%3A%7B%22type%22%3A%22headline%22%2C%22option.data_action%22%3A%7B%22action%22%3A%22https%3A%2F%2Fimentor.vn%2F%22%2C%22target%22%3A%22_blank%22%2C%22type%22%3A%22link%22%7D%7D%2C%22GROUP941%22%3A%7B%22type%22%3A%22group%22%2C%22desktop.style.animation-name%22%3A%22bounceInLeft%22%2C%22desktop.style.animation-delay%22%3A%220s%22%2C%22mobile.option.auto_scroll%22%3Atrue%2C%22mobile.style.animation-name%22%3A%22bounceInRight%22%2C%22mobile.style.animation-delay%22%3A%220s%22%7D%2C%22FORM_ITEM989%22%3A%7B%22type%22%3A%22form_item%22%2C%22option.input_type%22%3A%22email%22%2C%22option.input_tabindex%22%3A1%7D%2C%22FORM_ITEM992%22%3A%7B%22type%22%3A%22form_item%22%2C%22option.input_type%22%3A%22text%22%2C%22option.input_tabindex%22%3A2%7D%2C%22FORM988%22%3A%7B%22type%22%3A%22form%22%2C%22option.form_config_id%22%3A%225eb3800081e7d47b21ee8a72%22%2C%22option.form_send_ladipage%22%3Atrue%2C%22option.thankyou_type%22%3A%22default%22%2C%22option.thankyou_value%22%3A%22C%C3%A1m%20%C6%A1n%20b%E1%BA%A1n%20%C4%91%C3%A3%20quan%20t%C3%A2m%22%2C%22option.form_auto_funnel%22%3Atrue%2C%22option.form_auto_complete%22%3Atrue%7D%2C%22POPUP985%22%3A%7B%22type%22%3A%22popup%22%2C%22option.show_popup_welcome_page%22%3Atrue%2C%22option.delay_popup_welcome_page%22%3A7%2C%22desktop.option.popup_position%22%3A%22default%22%2C%22desktop.option.popup_backdrop%22%3A%22background-color%3A%20rgba(0%2C%200%2C%200%2C%200.5)%3B%22%2C%22mobile.option.popup_position%22%3A%22default%22%2C%22mobile.option.popup_backdrop%22%3A%22background-color%3A%20rgba(0%2C%200%2C%200%2C%200.5)%3B%22%7D%2C%22GROUP995%22%3A%7B%22type%22%3A%22group%22%2C%22desktop.style.animation-name%22%3A%22bounceIn%22%2C%22desktop.style.animation-delay%22%3A%220s%22%2C%22mobile.option.auto_scroll%22%3Atrue%2C%22mobile.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-delay%22%3A%220s%22%7D%2C%22BUTTON1093%22%3A%7B%22type%22%3A%22button%22%2C%22option.data_action%22%3A%7B%22type%22%3A%22link%22%2C%22action%22%3A%22http%3A%2F%2Fladi.demopage.me%2F5ecdd328b86f6f246400f6ac%22%7D%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22desktop.style.animation-delay%22%3A%220s%22%2C%22mobile.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-delay%22%3A%220s%22%7D%2C%22POPUP1097%22%3A%7B%22type%22%3A%22popup%22%2C%22desktop.option.popup_position%22%3A%22default%22%2C%22desktop.option.popup_backdrop%22%3A%22background-color%3A%20rgba(0%2C%200%2C%200%2C%200.5)%3B%22%2C%22mobile.option.popup_position%22%3A%22default%22%2C%22mobile.option.popup_backdrop%22%3A%22background-color%3A%20rgba(0%2C%200%2C%200%2C%200.5)%3B%22%7D%2C%22BUTTON1131%22%3A%7B%22type%22%3A%22button%22%2C%22option.data_action%22%3A%7B%22type%22%3A%22hidden_show%22%2C%22hidden_ids%22%3A%5B%22SECTION1294%22%5D%2C%22show_ids%22%3A%5B%22SECTION1262%22%5D%7D%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22desktop.style.animation-delay%22%3A%220s%22%2C%22mobile.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-delay%22%3A%220s%22%7D%2C%22BUTTON1132%22%3A%7B%22type%22%3A%22button%22%2C%22option.data_action%22%3A%7B%22type%22%3A%22hidden_show%22%2C%22hidden_ids%22%3A%5B%22SECTION1262%22%5D%2C%22show_ids%22%3A%5B%22SECTION1294%22%5D%7D%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22desktop.style.animation-delay%22%3A%220s%22%2C%22mobile.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-delay%22%3A%220s%22%7D%2C%22BUTTON1322%22%3A%7B%22type%22%3A%22button%22%2C%22option.data_action%22%3A%7B%22type%22%3A%22link%22%2C%22action%22%3A%22https%3A%2F%2Fimentor.vn%2Fban-ve-thuong-hieu-thuc-chien-xay-dung-thuong-hieu-thanh-cong%22%7D%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22desktop.style.animation-delay%22%3A%220s%22%2C%22mobile.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-delay%22%3A%220s%22%7D%2C%22BUTTON1323%22%3A%7B%22type%22%3A%22button%22%2C%22option.data_action%22%3A%7B%22type%22%3A%22link%22%2C%22action%22%3A%22https%3A%2F%2Fimentor.vn%2Ftalkshow-04%22%7D%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22desktop.style.animation-delay%22%3A%221s%22%2C%22mobile.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-delay%22%3A%221s%22%7D%2C%22BUTTON1325%22%3A%7B%22type%22%3A%22button%22%2C%22option.data_action%22%3A%7B%22type%22%3A%22link%22%2C%22action%22%3A%22https%3A%2F%2Fimentor.vn%2Ftung-buoc-xay-dung-he-thong-spa-tham-my-vien-but-pha%22%7D%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-name%22%3A%22pulse%22%7D%2C%22BUTTON1327%22%3A%7B%22type%22%3A%22button%22%2C%22option.data_action%22%3A%7B%22type%22%3A%22link%22%2C%22action%22%3A%22https%3A%2F%2Fimentor.vn%2Ftalkshow-04%22%2C%22target%22%3A%22_blank%22%7D%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22desktop.style.animation-delay%22%3A%220s%22%2C%22mobile.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-delay%22%3A%220s%22%7D%2C%22HEADLINE1433%22%3A%7B%22type%22%3A%22headline%22%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22desktop.style.animation-delay%22%3A%221s%22%2C%22mobile.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-delay%22%3A%221s%22%7D%2C%22HEADLINE1440%22%3A%7B%22type%22%3A%22headline%22%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22desktop.style.animation-delay%22%3A%221s%22%2C%22mobile.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-delay%22%3A%221s%22%7D%2C%22HEADLINE1446%22%3A%7B%22type%22%3A%22headline%22%2C%22desktop.style.animation-name%22%3A%22pulse%22%2C%22desktop.style.animation-delay%22%3A%221s%22%2C%22mobile.style.animation-name%22%3A%22pulse%22%2C%22mobile.style.animation-delay%22%3A%221s%22%7D%7D";
                window.LadiPageScript.run(true);
                window.LadiPageScript.runEventScroll();
            };
            run();
        })();</script>
@endsection
