@extends('themeraovat::layouts.default')
@section('main_content')
    <div id="body" class="shopWrap">
        <div style="background: #fff; width: 100%; padding-top: 15px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        @include('themeraovat::partials.top_search', ['post_type' => 3])
                    </div>
                </div>
            </div>
        </div>
        <div class="container" id="prodDetail">
            <div class="row" id="prodDetail-top">
                <div class="col-lg-4 col-md-4 col-sm-4">

                    <div id="prodDetail-img">
                        <div class="lSSlideOuter ">
                            <div class="lSSlideWrapper usingCss"
                                 style="transition-duration: 500ms; transition-timing-function: ease;">
                                <ul id="image-gallery" class="gallery list-unstyled lightSlider lsGrab lSSlide"
                                    style="width: 2238px; transform: translate3d(-746px, 0px, 0px); height: 373px; padding-bottom: 0%;">
                                    @if($product->image != '')
                                        <li data-thumb="{{ asset('public/filemanager/userfiles/' . $product->image) }}"
                                            class="lslide active"
                                            style="width: 848px; margin-right: 0px;">
                                            <img data-src="{{ asset('public/filemanager/userfiles/' . $product->image) }}"
                                                 class="lazy"
                                                 alt="{{ $product->name }}">
                                        </li>
                                    @endif
                                    <?php
                                    $img_extra = explode('|', $product->image_extra);
                                    $k = 0;
                                    ?>
                                    @foreach($img_extra as $img)
                                        @if($img != '')
                                            <li data-thumb="{{ asset('public/filemanager/userfiles/' . $img) }}"
                                                class="{{ ($product->image == '' && $k == 0) ? 'active' : '' }}"
                                                style="width: 373px; margin-right: 0px;">
                                                <img data-src="{{ asset('public/filemanager/userfiles/' . $img) }}"
                                                     class="lazy"
                                                     alt="{{$product->name}} ">
                                            </li>
                                            <?php $k++;?>
                                        @endif
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div id="prodDetail-info">
                        <h1>{{$product->name}}</h1>
                        <div class="price">

                            <div class="priceNew pull-left">
                                <span>{!! !empty($product->price_intro) ? $product->price_intro.'<sup>đ</sup>' : 'Giá liên hệ' !!}</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="rate clearfix">
                            <div class="star star-medium"><span class="star-05"></span></div>

                        </div>
                        <hr>
                        <div class="shipping">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <ul class="info">

                                        <li><i class="fa fa-user-circle-o"></i> Được bán bởi: <a
                                                    href="/tim-kiem?user_id={{ $product->admin_id }}&post_type=3" title="{{ @$product->admin->name }}"><span
                                                        class="rngreen">{{ $product->contact_name == null ? @$product->admin->name : $product->contact_name }}</span></a></li>
                                        <li><i class="fa fa-bullhorn"></i> Tình trạng: <span
                                                    class="rngreen">{{ $product->instock == 1 ? 'Còn hàng' : 'Hết hàng' }}</span></li>
                                        <li><i class="fa fa-phone-square"></i> Điện thoại: <a
                                                    href="tel:{{ str_replace(' ', '', str_replace('.', '', ($product->contact_tel == null ? @$product->admin->tel : $product->contact_tel))) }}"><span
                                                        class="rngreen">{{ $product->contact_tel == null ? @$product->admin->tel : $product->contact_tel }}</span></a>
                                        </li>

                                        <li><i class="fa fa-map-marker"></i> Địa chỉ: <span
                                                    class="rngreen">{{ $product->contact_address == null ? @$product->admin->address : $product->contact_address }}</span></li>
                                        <li><i class="fa fa-calendar"></i> Ngày đăng: <span
                                                    class="rngreen">{{ date('d-m-Y', strtotime($product->created_at)) }}</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <ul class="info">
                                        <li><i class="fa fa-qrcode"></i> Mã tin: <span
                                                    class="rngreen">{{$product->id}}</span></li>
                                        <li><i class="fa fa-shopping-cart"></i> Mua tối thiểu <span
                                                    class="rngreen">{{$product->min_buy}}</span>
                                        </li>
                                        <li><i class="fa fa-shopping-basket"></i> Ngành hàng: <a href="/tim-kiem?category_child_id={{ str_replace('|', '', $product->category_child_id) }}&post_type=3" class="rngreen">{{ @$product->category_child->name }}</a>
                                        </li>
                                        <li><i class="fa fa-map-marker"></i> Nơi sản xuất: <span
                                                    class="rngreen">{{$product->production}}</span>
                                        </li>
                                        <li><i class="fa fa-calendar"></i> Ngày hết hạn: <span
                                                    class="rngreen">@if($product->deadline != null && date('Y', strtotime($product->deadline)) != '1970'){{ date('d-m-Y', strtotime($product->deadline)) }}@endif</span>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="table-bordered">
                                {!! $product->price_content !!}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-8 col-xs-12">
                                <div class="clearfix btn-box">
                                    {{--<button class="btn btn-buynow" data-toggle="modal" data-target="#dialogBuy">Đặt--}}
                                    {{--mua--}}
                                    {{--</button>--}}


                                    <a class="btn btn-chat"
                                       href="/tim-kiem?post_type=3&category_child_id={{ str_replace('|', '', $product->category_child_id) }}"
                                       title="Công ty alifarm VN">Xem
                                        sản phẩm khác</a>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="zalo-share-button"
                                             data-href="{{ "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"
                                             data-oaid="255893260035326317" data-layout="1" data-color="blue"
                                             data-customize="false"
                                             style="overflow: hidden; display: inline-block; width: 70px; height: 20px;">
                                            <iframe frameborder="0" allowfullscreen="" scrolling="no" width="70px"
                                                    height="20px"
                                                    src="https://sp.zalo.me/plugins/share?dev=null&amp;color=blue&amp;oaid=255893260035326317&amp;href={{ "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}&amp;layout=1&amp;customize=false&amp;callback=null&amp;id=983858fd-00c8-4492-afab-86e30e4ef196&amp;domain={{ "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}&amp;android=false&amp;ios=false"></iframe>
                                        </div>
                                    </div>
                                    <div class="col-md-6" style="overflow: hidden;">
                                        <div id="fb-root"></div>
                                        <script async defer crossorigin="anonymous"
                                                src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v8.0&appId=561583071141201&autoLogAppEvents=1"
                                                nonce="WCE4t1BV"></script>
                                        <div class="fb-like"
                                             data-href="{{ "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"
                                             data-width=""
                                             data-layout="standard" data-action="like" data-size="small"
                                             data-share="true"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="bt-block">
                            <span><i class="fa fa-info-circle" aria-hidden="true"></i> Chú ý: {{$_SERVER['HTTP_HOST']}} không bán hàng trực tiếp, quý khách mua hàng xin vui lòng liên lạc với người bán.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="prodDetail-desc">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#prodDetail-desc1">Mô tả sản phẩm</a></li>
                </ul>
                <div class="tab-content">
                    <div id="prodDetail-desc1" class="tab-pane fade in active text-justify">
                        <p>{!! $product->intro !!}</p>
                        <p>{!! $product->content !!}</p>
                    </div>
                </div>
            </div>
            <div class="mgt10">
                <div id="tags">
                    <label><strong>Từ khóa:</strong></label>
                    <?php
                    $tags = explode(',', $product->tags);
                    ?>
                    @foreach($tags as $tag)
                        <a class="font_4" title="{{ $tag }}"
                           href="/tim-kiem?tag={{ $tag }}&post_type=3">{{ $tag }}</a>;&nbsp;
                    @endforeach
                </div>
            </div>
            @if($product->source != null)<p style="font-size: 11px;">Nguồn: {{ $product->source }}</p>@endif
        </div>
        <div id="popular" class="section">

            <div class="container">
                <div class="shopHd-tl"><h3>Sản phẩm khác của người bán</h3><a
                            href="/tim-kiem?post_type=3&admin_id={{ str_replace('|', '', $product->admin_id) }}"
                            class="small pull-right">Xem tất
                        cả <i class="fa fa-angle-right"></i></a></div>
                <div class="owl-carousel owl-carousel-popular owl-loaded owl-drag">


                    <div class="owl-stage-outer">
                        <div class="owl-stage"
                             style="transform: translate3d(-1150px, 0px, 0px); transition: all 0s ease 0s; width: 3834px;">

                            <?php
                            $data = CommonHelper::getFromCache('products_admin_id' . $product->admin_id, ['products']);
                            if (!$data) {
                                $data = \Modules\ThemeRaoVat\Models\Product::select('id', 'image', 'name', 'category_id', 'multi_cat', 'slug', 'created_at')
                                    ->where('id', '!=', $product->id)->where('admin_id', $product->admin_id)->where('status', 1)->orderBy('created_at', 'desc')->limit(10)->get();
                                CommonHelper::putToCache('products_admin_id' . $product->admin_id, $data, ['products']);
                            }
                            ?>
                            @foreach($data as $v)
                                <?php
                                $cat = CommonHelper::getFromCache('categories_id' . $v->id, ['categories', 'posts']);
                                if (!$cat) {
                                    $cat = $v->category;
                                    CommonHelper::putToCache('categories_id' . $v->id, $cat, ['categories', 'posts']);
                                }
                                ?>
                                <div class="owl-item cloned" style="width: 181.667px; margin-right: 10px;">
                                    <div class="item prodItem">
                                        <div class="thumb"><a
                                                    href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $v->slug : '/bai-viet/' . $v->slug }}.html"
                                                    title="{{ $v->name }}"><img
                                                        src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image) }}"
                                                        alt="{{ $v->name }}" class="lazy"></a></div>

                                        <div class="info">
                                            <h3>
                                                <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $v->slug : '/bai-viet/' . $v->slug }}.html"
                                                   title="{{ $v->name }}">{{ $v->name }}</a></h3>
                                            <div class="price">

                                                <div class="pull-left">
                                                    <span>{!! !empty($v->price_intro) ? @$v->price_intro.'<sup>đ</sup>' : 'Giá liên hệ' !!}</span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="prodItem-bt">
                                                <div class="pull-left"></div>
                                                <div class="pull-right">


                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                    <div class="owl-nav">
                        <div class="owl-prev">prev</div>
                        <div class="owl-next">next</div>
                    </div>
                    <div class="owl-dots">
                        <div class="owl-dot active"><span></span></div>
                        <div class="owl-dot"><span></span></div>
                    </div>
                </div>
            </div>

        </div>
        <div id="favorite" class="section">

            <div class="container">
                <div class="shopHd-tl"><h3>Có thể bạn quan tâm</h3><a
                            href="/tim-kiem?post_type=3&category_child_id={{ str_replace('|', '', $product->category_child_id) }}"
                            class="small pull-right">Xem tất cả <i
                                class="fa fa-angle-right"></i></a></div>
                <div class="owl-carousel owl-carousel-popular owl-loaded owl-drag">


                    <div class="owl-stage-outer">
                        <div class="owl-stage"
                             style="transform: translate3d(-1150px, 0px, 0px); transition: all 0s ease 0s; width: 4601px;">
                            <?php
                            $product_relate = CommonHelper::getFromCache('products_products_admin_id', ['products']);
                            if (!$product_relate) {
                                $product_relate = \Modules\ThemeRaoVat\Models\Product::select('id', 'image', 'name', 'category_id', 'multi_cat', 'slug', 'created_at')
                                    ->where('id', '!=', $product->id)->where('category_child_id', 'like', '%|' . str_replace('|', '', $product->category_child_id) . '|%')->where('status', 1)->orderBy('created_at', 'desc')->limit(10)->get();
                                CommonHelper::putToCache('products_products_admin_id', $product_relate, ['products']);
                            }
                            ?>
                            @foreach($product_relate as $p)
                                <?php
                                $cat = CommonHelper::getFromCache('categories_id' . $p->id, ['categories', 'posts']);
                                if (!$cat) {
                                    $cat = $p->category;
                                    CommonHelper::putToCache('categories_id' . $p->id, $cat, ['categories', 'posts']);
                                }
                                ?>
                                <div class="owl-item cloned" style="width: 181.667px; margin-right: 10px;">
                                    <div class="item prodItem">
                                        <div class="thumb"><a
                                                    href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $p->slug : '/bai-viet/' . $p->slug }}.html"
                                                    title="Cá SaBa đông lạnh ( Pacific Mackerel) nguyên con Nhật Bản"><img
                                                        src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($p->image) }}"
                                                        alt="{{$p->name}}" class="lazy"></a>
                                        </div>

                                        <div class="info">
                                            <h3>
                                                <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $p->slug : '/bai-viet/' . $p->slug }}.html"
                                                   title="Cá SaBa đông lạnh ( Pacific Mackerel) nguyên con Nhật Bản">{{$p->name}}</a>
                                            </h3>
                                            <div class="price">

                                                <div class="pull-left">
                                                    <span>{!! !empty($v->price_intro) ? @$v->price_intro .'<sup>đ</sup>' : 'Giá liên hệ' !!}</span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="prodItem-bt">
                                                <div class="pull-left"></div>
                                                <div class="pull-right">

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                    <div class="owl-nav">
                        <div class="owl-prev">prev</div>
                        <div class="owl-next">next</div>
                    </div>
                    <div class="owl-dots">
                        <div class="owl-dot active"><span></span></div>
                        <div class="owl-dot"><span></span></div>
                    </div>
                </div>
            </div>

        </div>
        <script src="{{ asset('public/frontend/themes/raovat/js/lightslider.min.js') }}"
                type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#image-gallery').lightSlider({
                    gallery: true,
                    item: 1,
                    thumbItem: 5,
                    slideMargin: 0,
                    speed: 500,
                    auto: true,
                    loop: true,
                    onSliderLoad: function () {
                        $('#image-gallery').removeClass('cS-hidden');
                    }
                });
            });
        </script>
    </div>
@endsection
@section('footer_script')

@endsection
@section('head_script')
    <style>
        #searchTop {
            padding-top: 15px;
        }
        .table-bordered table {
            border: 1px solid #ddd;
            width: 100%;
            max-width: 100%;
            margin-bottom: 20px;
            background-color: transparent;
            border-collapse: collapse;
            border-spacing: 0;
        }
        .table-bordered table thead tr,
        .table-bordered table tbody td {
            text-align: center;
        }

        .table-bordered table th {
            color: #05496b !important;
            font-weight: normal !important;
            vertical-align: middle;
            text-align: center;
            border-top: 0;
            border-bottom-width: 2px;
            border: 1px solid #ddd;
            padding: 8px;
            line-height: 1.42857143;
            font-weight: bolder;
        }

        .table-bordered table td {
            border: 1px solid #ddd;
            padding: 8px;
            line-height: 1.42857143;
            vertical-align: top;
        }
    </style>
@endsection
