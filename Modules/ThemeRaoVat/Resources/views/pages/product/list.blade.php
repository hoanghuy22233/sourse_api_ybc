@extends('themeraovat::layouts.default')
@section('main_content')
    <div id="body" class="shopWrap">
        <div style="background: #fff; width: 100%; padding-top: 15px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        @include('themeraovat::partials.top_search')
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="rnshophd mgt10">
                        <h1>@if(isset($_GET['keyword'])) Từ khóa tìm kiếm: {{ $_GET['keyword'] }}@else {{ @$category->name }} @endif</h1>
                    </div>
                </div>
            </div>
        </div>
        <div id="prodGrid" class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8">
                        {{--<ul class="nav nav-tabs">--}}

                            {{--<li class="active">--}}
                                {{--<a href="#">Phổ biến</a>--}}
                            {{--</li>--}}
                            {{--<li class=""><a href="#">Mới nhất</a></li>--}}
                            {{--<li class=""><a href="#">Mua nhiều</a></li>--}}
                            {{--<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Giá <i--}}
                                            {{--class="fa fa-angle-down"></i></a>--}}
                                {{--<ul class="dropdown-menu">--}}
                                    {{--<li><a href="#">Xem nhiều nhất</a></li>--}}
                                    {{--<li><a href="#">Giá từ cao -&gt; thấp</a></li>--}}
                                    {{--<li><a href="#">Giá từ thấp -&gt; cao</a></li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                        {{--</ul>--}}

                        <div class="rnshop-index">
                            <div class="row">
                                @foreach($products as $item)
                                    <?php
                                    $cat = CommonHelper::getFromCache('categories_id' . $item->id, ['categories', 'posts']);
                                    if (!$cat) {
                                        $cat = $item->category;
                                        CommonHelper::putToCache('categories_id' . $item->id, $cat, ['categories', 'posts']);
                                    }
                                    ?>
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 product-item">
                                        <div class="item prodItem">
                                            <div class="thumb">
                                                <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $item->slug : '/san-pham/' . $item->slug }}.html" title="Bánh tráng dừa">
                                                    <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->image) }} "
                                                         alt="Bánh tráng dừa" class="lazy"></a>
                                            </div>

                                            <div class="info">
                                                <h3><a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $item->slug : '/bai-viet/' . $item->slug }}.html" title="Bánh tráng dừa">Bánh
                                                       {{$item->name}}</a></h3>
                                                <div class="price">

                                                    <div class="pull-left"><span>{!! !empty($item->price_intro) ? @$item->price_intro.'<sup>đ</sup>' : 'Giá liên hệ' !!}</span></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="prodItem-bt">
                                                    {{--<div class="pull-left"><i class="fa fa-heart-o"></i>(130)</div>--}}
                                                    <div class="pull-right">
                                                        {{--<div class="star star-small"><span class="star-05"></span></div>--}}

                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        {{ @$products->appends(Request::all())->links() }}

                    </div>
                    <div class="col-md-3 col-sm-4">
                        @include('themeraovat::pages.post.partials.sidebar_categories', ['post_type' => @$post_type])
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
@section('head_script')
    <style>
        #boxSearch .nav-tabs li {
            width: 50%;
        }
    </style>
@endsection
@section('footer_script')

@endsection
