@extends('themeraovat::layouts.default')
@section('main_content')
    <div id="body">

        <div id="main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        <div class="hdCare">
                            <h1><i class="fa fa-newspaper-o"></i> @if(isset($_GET['keyword'])) Từ khóa tìm
                                kiếm: {{ $_GET['keyword'] }} - @else {{ @$category->name }} - @endif @include('themeraovat::pages.post.partials.text_filter')</h1>
                        </div>
                        <div class="newsCare" id="newshot">
                            <div class="newsList">
                                @foreach($posts as $item)
                                    <?php
                                    $cat = CommonHelper::getFromCache('categories_id' . $item->id, ['categories', 'posts']);
                                    if (!$cat) {
                                        $cat = $item->category;
                                        CommonHelper::putToCache('categories_id' . $item->id, $cat, ['categories', 'posts']);
                                    }
                                    ?>
                                    <div class="pwrap">
                                        <div class="post-entry">
                                            <div class="post-thumb">
                                                <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $item->slug : '/bai-viet/' . $item->slug }}.html"
                                                   title="{{ $item->name }}"
                                                   class="thumbnail">
                                                    <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, 258, 172) }}" class="lazy"
                                                         alt="{{ $item->name }}"></a>
                                            </div>

                                            <h2>
                                                <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $item->slug : '/bai-viet/' . $item->slug }}.html"
                                                   title="{{ $item->name }}">{{ $item->name }}</a>
                                            </h2>
                                            <div class="post-meta"><span>{{ date('H:i', strtotime($item->created_at)) }}</span> - {{ date('d/m/Y', strtotime($item->created_at)) }}</div>
                                            <p>{!! $item->intro !!}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            {{ @$posts->appends(Request::all())->links() }}
                        </div>

                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        @include('themeraovat::pages.post.partials.sidebar_categories', ['post_type' => @$post_type])
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('footer_script')

@endsection
