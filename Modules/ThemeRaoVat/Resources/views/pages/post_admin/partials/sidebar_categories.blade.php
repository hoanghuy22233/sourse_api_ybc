<aside>

    {{--<div class="widget p15 radius" id="newsletter">
        <label><i class="fa fa-envelope-open-o"></i><strong> Nhận bản tin từ chúng tôi</strong></label>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Nhập email nhận thông tin">
            <span class="input-group-btn">
<button class="btn btn-primary" type="button" data-toggle="modal"
data-target="#dialogREmail">Đăng ký</button>
</span>
        </div>
    </div>--}}

    @include('themeraovat::partials.box_search')

    @if(isset($_GET['category_id']) || (isset($category) && $category->parent_id == null))
        <?php
        $category_id = isset($category) ? $category->id : @$_GET['category_id']
        ?>
        <div class="widget" id="categories">
            <div class="wg-hd">
                <h3>NGÀNH HÀNG</h3>
            </div>
            <div class="wg-ct p15">
                <ul class="categoriesList cat-list">
                    <?php
                    $k = 0;
                    $cats = CommonHelper::getFromCache('categories_parent_id', ['categories']);
                    if (!$cats) {
                        $cats = \Modules\ThemeRaoVat\Models\Category::select('name', 'id')->where('status', 1)->where('parent_id', $category_id)->get();
                        CommonHelper::putToCache('categories_parent_id', $cats, ['categories']);
                    }
                    ?>
                    @foreach($cats as $cat)
                        <?php $k++;?>
                        <li class="@if($k > 10) sub hide @endif"><h3><a
                                        href="/tim-kiem?category_child_id={{ $cat->id }}&post_type={{ @$post_type }}"
                                        title="{{ $cat->name }}"
                                        class="rnbold">{{ $cat->name }}</a>

                            </h3>
                        </li>
                    @endforeach
                    <li class="sub" id="show-all"><a title="Xem tất cả"
                                                     href="javascript:RNShowLink(1, 'cat-list');" class="rnred">
                            Xem tất cả <i class="fa fa-plus-square-o"></i></a></li>
                    <li class="sub hide" id="hide-all"><a title="thu gọn"
                                                          href="javascript:RNShowLink(0, 'cat-list');"
                                                          class="rnred"> Thu gọn <i
                                    class="fa fa-minus-square-o"></i></a></li>
                </ul>
            </div>
        </div>
    @endif

    <div class="widget" id="categories">
        <div class="wg-hd"><h3>TÌM THEO TỈNH THÀNH</h3></div>
        <div class="wg-ct p15">
            <ul class="categoriesList province-list">
                <?php
                $k = 0;
                $provinces = CommonHelper::getFromCache('provinces_asc', ['provinces']);
                if (!$provinces) {
                    $provinces = \App\Models\Province::orderBy('name', 'asc')->pluck('name', 'id');
                    CommonHelper::putToCache('provinces_asc', $provinces, ['provinces']);
                }
                ?>
                @foreach($provinces as $id => $name)
                    <?php $k++;?>
                    <li class="@if($k > 10) sub hide @endif"><h3><a
                                    href="/tim-kiem?province_id={{ $id }}&post_type={{ @$post_type }}"
                                    title="{{ $name }}"
                                    class="">{{ $name }}</a></h3></li>
                @endforeach
                <li class="sub" id="show-all"><a title="Xem tất cả"
                                                 href="javascript:RNShowLink(1, 'province-list');" class="rnred">
                        Xem tất cả <i class="fa fa-plus-square-o"></i></a></li>
                <li class="sub hide" id="hide-all"><a title="thu gọn"
                                                      href="javascript:RNShowLink(0, 'province-list');"
                                                      class="rnred"> Thu gọn <i
                                class="fa fa-minus-square-o"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="clear"></div>


    <div class="advRight">
        <?php
        $data = CommonHelper::getFromCache('banner_posts_order_no2', ['banners']);
        if (!$data) {
            $data = \Modules\ThemeRaoVat\Models\Banner::where('location', 'banner_posts')->where('status', 1)->where('order_no', 2)->get();
            CommonHelper::putToCache('banner_posts_order_no2', $data, ['banners']);
        }
        ?>
        <div class="banner mgb10" id="BannerRight1">
            @foreach($data as $v)
                <div class="banner_item mgt5"><a
                            href="{{ $v->link }}"
                            target="_blank"><img
                                data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$v->image, 262, null) }}"
                                class="lazy"></a></div>
            @endforeach
        </div>
    </div>
</aside>