@extends('themeraovat::layouts.default')
@section('main_content')
    <div id="body">

        <div id="main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9">

                        @include('themeraovat::partials.top_search')

                        <div id="itemBrowse-hd">
                            <h1>@if(isset($_GET['keyword']) && $_GET['keyword'] != '') Từ khóa tìm
                                kiếm: {{ $_GET['keyword'] }} - @else {{ @$category->name }} - @endif @include('themeraovat::pages.post.partials.text_filter')</h1>
                        </div>
                        <div id="itemsTabs">
                            <div id="itemsTabs1" class="tab-pane fade in active">
                                <div class="listItems">
                                    <div class="listItems-hd">
                                        <h2 class="pull-left">@include('themeraovat::pages.post.partials.text_filter')</h2>
                                        <div class="pull-right">
                                            <label>Sắp xếp theo:</label>
                                            <select>
                                                <option>Thông thường</option>
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    @foreach($posts as $item)
                                        <?php
                                        $cat = CommonHelper::getFromCache('categories_id' . $item->id, ['categories', 'posts']);
                                        if (!$cat) {
                                            $cat = $item->category;
                                            CommonHelper::putToCache('categories_id' . $item->id, $cat, ['categories', 'posts']);
                                        }
                                        ?>
                                        <div class="item">
                                            <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $item->slug : '/bai-viet/' . $item->slug }}.html"
                                               title="{{ $item->name }}">
                                                <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, 160, null) }}"
                                                     class="lazy"
                                                     alt="{{ $item->name }}"></a>
                                            <h3>
                                                <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $item->slug : '/bai-viet/' . $item->slug }}.html"
                                                   title="{{ $item->name }}"
                                                   class="rnvip5">{{ $item->name }}</a></h3>
                                            <div class="desc">
                                                {!! $item->intro !!}
                                            </div>
                                            <ul class="info nav-pills">
                                                <li><i class="fa fa-map-marker"></i>Khu vực ưu tiên
                                                    : {{ @$item->province->name }}
                                                </li>
                                                <li><i class="fa fa-tag"></i>Y/c tài chính : <span
                                                            class="red">{{ $item->yeu_cau_tai_chinh }}</span>
                                                </li>
                                                <li><i class="fa fa-building-o"></i>Y/c mặt bằng :</li>
                                                <li class="pull-right">
                                                    <span>{{ date('d/m/Y', strtotime($item->created_at)) }}</span></li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    @endforeach

                                    {{ @$posts->appends(Request::all())->links() }}
                                </div>
                            </div>
                        </div>

                        <div class="p15 text-center" id="boxBottom">
                            Quý khách muốn nhanh chóng tìm được đại lý phân phối, nhà cung cấp phù hợp ?
                            <a class="btn btn-success" href="/admin/post/add" rel="nofollow">Click vào đây để đăng
                                tin
                                muốn làm đại lý phân phối</a>
                        </div>

                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        @include('themeraovat::pages.post.partials.sidebar_categories', ['post_type' => @$post_type])
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('footer_script')

@endsection
