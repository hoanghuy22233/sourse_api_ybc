<?php
if (!isset($post_type)) {
    $post_type = @$_GET['post_type'];
}
if (!isset($category_child_id)) {
    $category_child_id = @$_GET['category_child_id'];
}
if (!isset($province_id)) {
    $province_id = @$_GET['province_id'];
}

if ($post_type === 0) {
    echo 'Tin tức';
} elseif ($post_type == 1) {
    echo 'Tìm nhà phân phối';
} elseif ($post_type == 2) {
    echo 'Nhận mở đại lý';
} elseif ($post_type == 3) {
    echo 'Bán buôn bán sỉ';
}

if ($category_child_id != null) {
    $cat = CommonHelper::getFromCache('categories_id_' . $category_child_id, ['categories']);
    if (!$cat) {
        $cat = \Modules\ThemeRaoVat\Models\Category::select('name', 'id')->where('status', 1)->where('id', $category_child_id)->first();
        CommonHelper::putToCache('categories_id_' . $category_child_id, $cat, ['categories']);
    }
    if (is_object($cat)) {
        echo ' nghành ' . $cat->name;
    }
}

if ($province_id != null) {
    $province = CommonHelper::getFromCache('provinces_id_' . $province_id, ['provinces']);
    if (!$province) {
        $province = \App\Models\Province::select('name', 'id')->where('id', $province_id)->first();
        CommonHelper::putToCache('provinces_id_' . $province_id, $province, ['provinces']);
    }
    if (is_object($province)) {
        echo ' tại ' . $province->name;
    }
}
?>

