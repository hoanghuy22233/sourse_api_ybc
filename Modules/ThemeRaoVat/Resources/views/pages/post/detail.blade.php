@extends('themeraovat::layouts.default')
@section('main_content')
    <div id="body">

        <div id="main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-9">

                        @include('themeraovat::partials.top_search', ['post_type' => $post->post_type])

                        {{--                        @include('themeraovat::partials.breadcrumb')--}}


                        <div id="detail">
                            <h1 class="h1">{{ $post->name }}</h1>

                            @if($post->post_type != 0)
                                <ul class="info">
                                    <li><i class="fa fa-map-marker"></i><strong>Khu vực ưu tiên:</strong> <span
                                                class="green">{{ @$post->province->name }}</span></li>
                                    @if($post->chinh_sach_ho_tro != '')
                                        <li><i class="fa fa-tag"></i><strong>Chính sách hỗ trợ:</strong> <span
                                                    class="green">{{ $post->chinh_sach_ho_tro }}</span></li>
                                    @endif
                                    <li><i class="fa fa-tag"></i><strong>Yêu cầu tài chính:</strong> <span
                                                class="green">{{ $post->yeu_cau_tai_chinh }}</span></li>

                                </ul>
                                <div class="mgt5">
                                    <div id="fb-root"></div>
                                    <script async defer crossorigin="anonymous"
                                            src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v8.0&appId=561583071141201&autoLogAppEvents=1"
                                            nonce="WCE4t1BV"></script>
                                    <div class="fb-like"
                                         data-href="{{ "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"
                                         data-width=""
                                         data-layout="standard" data-action="like" data-size="small"
                                         data-share="true"></div>
                                </div>
                            @endif

                            <div class="clearfix"></div>
                            <div class="clearfix desc mgt5">
                                <div class="text-justify">
                                    {!! $post->content !!}
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            @if($post->post_type != 0)
                                <div id="detailTabs">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#detailTabs1"><strong>Xem Hình
                                                    ảnh</strong></a></li>
                                        <li class=""><a data-toggle="tab" href="#detailTabs2"><strong>Xem Video</strong></a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="detailTabs1" class="text-center tab-pane fade active in">
                                            <div id="rnimginfo_28608">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">

                                                        <div id="prodDetail-img">
                                                            <div class="lSSlideOuter ">
                                                                <div class="lSSlideWrapper usingCss"
                                                                     style="transition-duration: 500ms; transition-timing-function: ease;">
                                                                    <ul id="image-gallery"
                                                                        class="gallery list-unstyled lightSlider lsGrab lSSlide"
                                                                        style="width: 26288px; transform: translate3d(-21200px, 0px, 0px); height: 499px; padding-bottom: 0%;">
                                                                        @if($post->image != '')
                                                                            <li data-thumb="{{ asset('public/filemanager/userfiles/' . $post->image) }}"
                                                                                class="lslide active"
                                                                                style="width: 848px; margin-right: 0px;">
                                                                                <img src="{{ asset('public/filemanager/userfiles/' . $post->image) }}"
                                                                                     class="lazy"
                                                                                     alt="{{ $post->name }}">
                                                                            </li>
                                                                        @endif

                                                                        <?php
                                                                        $img_extra = explode('|', $post->image_extra);
                                                                        $k = 0;
                                                                        ?>
                                                                        @foreach($img_extra as $img)
                                                                            @if($img != '')
                                                                                <li data-thumb="{{ asset('public/filemanager/userfiles/' . $img) }}"
                                                                                    class="{{ ($post->image == '' && $k == 0) ? 'active' : '' }}"
                                                                                    style="width: 848px; margin-right: 0px;">
                                                                                    <img src="{{ asset('public/filemanager/userfiles/' . $img) }}"
                                                                                         class="lazy"
                                                                                         alt="{{ $post->name }}">
                                                                                </li>
                                                                                <?php $k++;?>
                                                                            @endif
                                                                        @endforeach
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <script src="{{ asset('public/frontend/themes/raovat/js/lightslider.min.js') }}"
                                                        type="text/javascript"></script>
                                                <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $('#image-gallery').lightSlider({
                                                            gallery: true,
                                                            item: 1,
                                                            thumbItem: 5,
                                                            slideMargin: 0,
                                                            speed: 500,
                                                            auto: true,
                                                            loop: true,
                                                            onSliderLoad: function () {
                                                                $('#image-gallery').removeClass('cS-hidden');
                                                            }
                                                        });
                                                    });
                                                </script>
                                            </div>
                                            <script type="text/javascript">
                                                $(document).ready(function () {
                                                    RNShowProductImg($("#hdProductId").val(), $("#hdProductName").val());
                                                });
                                            </script>
                                        </div>
                                        <div id="detailTabs2" class="tab-pane fade">
                                            @if($post->link_youtube != null && $post->link_youtube != '')
                                                <div id="video">
                                                    <?php
                                                    $code = $post->link_youtube;
                                                    if (strpos($post->link_youtube, 'v=') !== false) {
                                                        $code = @explode('v=', $post->link_youtube)[1];
                                                        $code = @explode('&', $code)[0];
                                                    } elseif (strpos($post->link_youtube, 'https://youtu.be/') !== false) {
                                                        $code = @explode('/', $post->link_youtube)[count(explode('/', $post->link_youtube)) - 1];
                                                    }
                                                    ?>
                                                    <div class="text-center">
                                                        <iframe width="100%" height="400"
                                                                src="https://www.youtube.com/embed/{{ $code }}"
                                                                frameborder="0"
                                                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                                allowfullscreen></iframe>
                                                        {!! $post->link_youtube !!}
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row" id="detailAddress">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
                                            <thead>
                                            <tr>
                                                <th colspan="2">Chi tiết</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th>Mã tin đăng</th>
                                                <td>{{ $post->id }}</td>
                                            </tr>
                                            <tr>
                                                <th>Ngành hàng</th>
                                                <td>
                                                    <a href="/tim-kiem?category_child_id={{ str_replace('|', '', $post->category_child_id) }}">{{ @$post->category_child->name }}</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>KV ưu tiên</th>
                                                <td>
                                                    <a href="/tim-kiem?province_id={{ $post->province_id }}">{{ @$post->province->name }}</a>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>Ngày đăng</th>
                                                <td>{{ date('d-m-Y', strtotime($post->created_at)) }}</td>
                                            </tr>

                                            <tr>
                                                <th>Tên liên lạc</th>
                                                <td>
                                                    <a href="{{ $post->admin_id != null ? '/tim-kiem?user_id=' . $post->admin_id : '#' }}">{{ $post->contact_name == null ? @$post->admin->name : $post->contact_name }}</a>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table">
                                            <thead>
                                            <tr>
                                                <th colspan="2">Liên hệ</th>
                                            </tr>
                                            </thead>
                                            <tbody>


                                            <tr>
                                                <th>Điện thoại</th>
                                                <td>
                                                    @if($post->post_type == 1)
                                                        <a href="tel:{{ str_replace(' ', '', str_replace('.', '', ($post->contact_tel == null ? @$post->admin->tel : $post->contact_tel))) }}"
                                                           class="hidden-lg hidden-md"><span><i
                                                                        class="fa fa-phone red rnbold"></i> Gọi điện</span></a>
                                                        <div id="profile-phone"
                                                             phone="{{ ($post->contact_tel == null ? @$post->admin->tel : $post->contact_tel) }}"
                                                             class="hidden-sm hidden-xs">
                                                            <a type="button" class="btn-phone-display btn btn-success"
                                                               href="tel:{{ str_replace(' ', '', str_replace('.', '', ($post->contact_tel == null ? @$post->admin->tel : $post->contact_tel))) }}">
                                                                Nhấn để hiện số
                                                            </a>
                                                        </div>
                                                    @else
                                                        Vui lòng gọi hotline để xem đầy đủ thông tin!
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                @if($post->contact_zalo != '')
                                                    <th>Gửi tin nhắn qua zalo</th>
                                                    <td>
                                                        <div class=""><a href="{{ $post->contact_zalo }}"
                                                                         target="_blank"
                                                                         class="btn btn-primary" rel="nofollow">Zalo</a>
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>

                                            <tr>
                                                <th>Hết hạn</th>
                                                <td>
                                                    @if($post->deadline != null && date('Y', strtotime($post->deadline)) != '1970')
                                                        {{ date('d-m-Y', strtotime($post->deadline)) }}
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th colspan="2">
                                                </th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            @endif
                        </div>

                        <div class="clearfix"></div>

                        @if($post->post_type != 0)

                            <?php
                            $sp = [
                                1 => 'Được phân phối độc quyền',
                                2 => 'Được hưởng chính sách ưu đãi khuyến mãi',
                                3 => 'Được ưu đãi chính sách về giá',
                                4 => 'Hỗ trợ dư nợ',
                                5 => 'Hỗ trợ marketing, hình ảnh, đào tạo',
                                6 => 'Hỗ trợ catalogue, sản phẩm mẫu',
                                7 => 'Khuyến mãi các dịp lễ, tết, sinh nhật',
                                8 => 'Chiết khấu cao trên mỗi mặt hàng',
                            ];
                            $supports = explode('|', $post->supporting);
                            ?>
                            @if(!empty($supports))
                                <div class="mgt10">
                                    <label><strong>Chính sách hỗ trợ:</strong></label>
                                    <ul>
                                        @foreach($supports as $v)
                                            @if(isset($sp[$v]))
                                                <li>{{ $sp[$v] }}</li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="mgt10">
                                <div id="tags">
                                    <label><strong>Từ khóa:</strong></label>
                                    <?php
                                    $tags = explode(',', $post->tags);
                                    ?>
                                    @foreach($tags as $tag)
                                        <a class="font_4" title="{{ $tag }}"
                                           href="/tim-kiem?tag={{ urlencode($tag) }}">{{ $tag }}</a>;&nbsp;
                                    @endforeach
                                </div>
                                @if($post->source != null)<p style="font-size: 11px;">
                                    Nguồn: {{ $post->source }}</p>@endif
                            </div>
                            <div class="clearfix"></div>

                            <div class="p15 text-center" id="boxBottom">
                                Quý khách muốn nhanh chóng tìm được nhà cung cấp phù hợp ?
                                <a class="btn btn-success" href="/admin/post" rel="nofollow">Click vào đây để đăng tin
                                    muốn mở đại lý</a>
                            </div>

                            <div class="clearfix"></div>
                        @endif

                        @if($post->post_type != 0)
                            <div id="itemOther">
                                <p><strong>Xem thêm các nhu cầu khác</strong></p>
                                <div class="listItems">
                                    <h2 class="tl"><span><i class="fa fa-list-ul"></i> CÁC TIN LIÊN QUAN</span>
                                    </h2>
                                    <?php
                                    $data = CommonHelper::getFromCache('posts_category_desc', ['posts']);
                                    if (!$data) {
                                        $data = \Modules\ThemeRaoVat\Models\Post::select('id', 'image', 'name', 'category_id', 'multi_cat', 'slug', 'created_at', 'address', 'province_id', 'yeu_cau_tai_chinh', 'intro', 'acreage', 'mat_bang')
                                            ->where('id', '!=', $post->id)->where('category_child_id', 'like', '%|' . str_replace('|', '', $post->category_child_id) . '|%')->where('status', 1)
                                            ->where('post_type', $post->post_type)->orderBy('created_at', 'desc')->limit(10)->get();
                                        CommonHelper::putToCache('posts_category_desc', $data, ['posts']);
                                    }
                                    ?>
                                    @foreach($data as $v)
                                        <?php
                                        $cat = CommonHelper::getFromCache('categories_id' . $v->id, ['categories', 'posts']);
                                        if (!$cat) {
                                            $cat = $v->category;
                                            CommonHelper::putToCache('categories_id' . $v->id, $cat, ['categories', 'posts']);
                                        }
                                        ?>
                                        <div class="item cust-item">
                                            <h3>
                                                <a href="{{ is_object($cat) ? '/' . $cat->slug . '/' . $v->slug : '/bai-viet/' . $v->slug }}.html"
                                                   title="{{ $v->name }}"
                                                   class="rnvip1">{{ $v->name }}</a></h3>

                                            <div class="desc">{{ $v->intro }}
                                            </div>
                                            <ul class="info nav-pills">
                                                <li><i class="fa fa-tag"></i>Tài chính : <span
                                                            class="red">{{ $v->yeu_cau_tai_chinh }}</span>
                                                </li>
                                                <li><i class="fa fa-building-o"></i>Mặt bằng : {{ $v->mat_bang }}</li>
                                                <li><i class="fa fa-map-marker"></i>Địa chỉ : {{ $v->address }}
                                                    , {{ @$v->province->name }}</li>
                                                {{--<li><span class="rnbadge2">Cần tìm</span></li>--}}
                                                <li class="pull-right">
                                                    <span>{{ date('d-m-Y', strtotime($v->created_at)) }}</span></li>
                                            </ul>
                                        </div>
                                    @endforeach


                                    <nav aria-label="Page navigation" id="pagination">
                                    <span class="pull-right small"><a
                                                href="/tim-kiem?category_child_id={{ str_replace('|', '', $post->category_child_id) }}"
                                                title="Xem thêm các tin {{ @$post->category_child->name }} khác">Xem thêm các tin {{ @$post->category_child->name }} khác ?</a></span>
                                        <div class="clearfix"></div>
                                    </nav>
                                </div>
                            </div>
                        @endif

                        <div class="clearfix"></div>

                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        @include('themeraovat::pages.post.partials.sidebar_categories', [
                        'category_child_id' => str_replace('|', '', $post->category_child_id),
                        'post_type' => $post->post_type])
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('head_script')
    <style>
        #image-gallery {
            height: unset !important;
            max-height: 636px;
        }

        #image-gallery img {
            width: unset;
            max-height: 636px;
        }

        .lSSlideOuter .lSPager.lSGallery {
            /*display: inline;*/
        }
    </style>
@endsection
@section('footer_script')

@endsection
