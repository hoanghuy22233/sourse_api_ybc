<html xmlns="http://www.w3.org/1999/xhtml">
<head id="ctl00_Head1">
    @include('themeraovat::partials.head_meta')
    @include('themeraovat::partials.head_script')
    {!! @$settings['frontend_head_code'] !!}
    @yield('head_script')
<script>

    function RNShowLink(showid, object) {
        if (showid == 1) {
            $('.' + object).find(".sub").removeClass("hide");
            $('.' + object).find("#show-all").addClass("hide");
        } else {
            $('.' + object).find(".sub").addClass("hide");
            $('.' + object).find("#show-all").removeClass("hide");
        }
    }
</script>
</head>
<body>

<div id="container">

@include('themeraovat::partials.header')
@include('themeraovat::partials.menu')

@yield('main_content')

@include('themeraovat::partials.footer')

    <!--Start of Tawk.to Script-->

    <!--End of Tawk.to Script-->
</div>


</body>

@include('themeraovat::partials.footer_script')
@yield('footer_script')
{!! @$settings['frontend_body_code'] !!}

</html>