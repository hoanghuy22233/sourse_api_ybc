{{--<script src="{{ URL::asset('public/frontend/themes/raovat/js/jquery.validate.min.js')}}" type="text/javascript"></script>--}}
<script src="/public/frontend/themes/raovat/js/bootstrap.min.js" type="text/javascript"></script>
{{--<script src="/public/frontend/themes/raovat/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="/public/frontend/themes/raovat/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="/public/frontend/themes/raovat/js/chosen.jquery.js" type="text/javascript"></script>--}}
<script src="/public/frontend/themes/raovat/js/custom.js" type="text/javascript"></script>

<a href="javascript:;" title="Lên đầu trang" onclick="jQuery('html,body').animate({scrollTop: 0},500);"
   class="go_top"></a>

{{--Ladyload--}}
<script>
    setTimeout(function(){
        !function (e) {
            document.createElement("style").innerHTML = "img:not([src]) {visibility: hidden;}";

            function t(e, t) {
                var n = new Image, r = e.getAttribute("data-src");
                n.onload = function () {
                    e.parent ? e.parent.replaceChild(n, e) : e.src = r, e.style.opacity = "1", t && t()
                }, n.src = r
            }

            for (var n = new Array, r = function (e, t) {
                if (document.querySelectorAll) t = document.querySelectorAll(e); else {
                    var n = document, r = n.styleSheets[0] || n.createStyleSheet();
                    r.addRule(e, "f:b");
                    for (var i = n.all, l = 0, c = [], o = i.length; l < o; l++) i[l].currentStyle.f && c.push(i[l]);
                    r.removeRule(0), t = c
                }
                return t
            }("img.lazy"), i = function () {
                for (var r = 0; r < n.length; r++) i = n[r], l = void 0, (l = i.getBoundingClientRect()).top >= 0 && l.left >= 0 && l.top <= (e.innerHeight || document.documentElement.clientHeight) && t(n[r], function () {
                    n.splice(r, r)
                });
                var i, l
            }, l = 0; l < r.length; l++) n.push(r[l]);
            i(), function (t, n) {
                e.addEventListener ? this.addEventListener(t, n, !1) : e.attachEvent ? this.attachEvent("on" + t, n) : this["on" + t] = n
            }("scroll", i)
        }(this);
    }, 20);

</script>
