<div id="ftTop">
    <div class="container">
        <div class="row">
            <?php
            $categories = \Modules\ThemeRaoVat\Models\Category::select('id', 'name', 'slug')->whereNull('parent_id')->where('status', 1)->where('type', 10)->orderBy('name', 'asc')->limit(15)->get();
            ?>
            @foreach($categories as $category)
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <h3><a href="/{{$category->slug}}" title="{{ $category->name}}">{{ $category->name}}</a>
                    </h3>
                    <?php
                    $childs = $category->childs;
                    ?>
                    @foreach($childs as $category)
                        <ul class="reset">
                            <li><a href="/{{$category->slug}}"
                                   title="{{ $category->name}}">{{ $category->name}}</a>
                            </li>
                        </ul>
                    @endforeach
                </div>
            @endforeach

        </div>
    </div>
</div>