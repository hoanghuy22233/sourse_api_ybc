<div id="searchTop" class="hidden-sm hidden-xs">
    <div class="inner">
        <form action="/tim-kiem" method="GET" id="searchhome" name="searchhome">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 text-center">
                    <label>Tìm kiếm</label></div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <input type="text"  name="keyword" value="{{ @$_GET['keyword'] }}"
                           placeholder="Nhập từ khóa để tìm kiếm..."></div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <select class="rntype"  name="post_type">
                        <option value="">Tất cả</option>
                        <option value="1" {{ @$_GET['post_type'] == 1 || (!isset($_GET['post_type']) && @$post_type == 1) ? 'selected' : '' }}>Tìm nhà phân phối</option>
                        <option value="2" {{ @$_GET['post_type'] == 2 || (!isset($_GET['post_type']) && @$post_type == 2) ? 'selected' : '' }}>Mở đại lý phân phối</option>
                        <option value="3" {{ @$_GET['post_type'] == 3 || (!isset($_GET['post_type']) && @$post_type == 3) ? 'selected' : '' }}>Bán buôn, Bán sỉ</option>
                        <option value="4" {{ @$_GET['post_type'] == 4 || (!isset($_GET['post_type']) && @$post_type == 4) ? 'selected' : '' }}>Tài liệu hệ thống phân phối</option>
                    </select>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <button class="btn btn-primary" type="submit" onclick="RNCheckSearch();">Tìm kiếm
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>