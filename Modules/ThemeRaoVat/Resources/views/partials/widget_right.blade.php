<div class="widget">
    <?php
    $data = CommonHelper::getFromCache('widgets_location_home_sidebar_right', ['widgets']);
    if (!$data) {
        $data = \Modules\ThemeRaoVat\Models\Widget::select(['name', 'content', 'config', 'type'])
            ->where('location', 'home_sidebar_right')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')
            ->get();
        CommonHelper::putToCache('widgets_location_home_sidebar_right', $data, ['widgets']);
    }
    //dd($data);
    ?>
    @foreach($data as $widget)
        {{--{{ dd(json_decode($widget->config)->view) }}--}}
        <aside class="sidebar static right">
            <div class="row">
                @if($widget->type == 'html')
                    <div class="widget">
                        <h4 class="widget-title">{{$widget->name}}</h4>
                        {!! $widget->content !!}
                    </div><!-- twitter feed-->
                @else
                    <div class="widget">
                        <h4 class="widget-title">{{$widget->name}}</h4>
                        @include(@json_decode($widget->config)->view, ['config' => (array) json_decode($widget->config)])
                    </div>
                @endif
            </div>
        </aside>
    @endforeach
</div>