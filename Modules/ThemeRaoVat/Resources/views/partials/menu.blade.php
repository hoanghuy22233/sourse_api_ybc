<nav class="collapse navbar-collapse clearfix" role="navigation" >
    <div class="container">
        <ul class="nav navbar-nav">
            <li><a href="/"><i class="fa fa-home"></i> <span class="hidden-lg hidden-md">Trang chủ</span></a></li>
            <?php
            $menus = CommonHelper::getFromCache('menus_by_location_main_menu', ['menus']);
            if (!$menus) {
                $menus = \Modules\ThemeRaoVat\Helpers\ThemeRaoVatHelper::getMenusByLocation('main_menu', 10, false);
                CommonHelper::putToCache('menus_by_location_main_menu', $menus, ['menus']);
            }
            ?>
            @foreach($menus as $menu1)
                <li class="{{ strpos($_SERVER['REQUEST_URI'], $menu1['url']) !== false ? 'active' : '' }}">
                    <a title="{{ $menu1['name'] }}" href="{{ $menu1['url'] }}" class="">{{ $menu1['name'] }}</a>
                    @if(!empty($menu1['childs']))
                        <ul class="menuSub">
                            @foreach($menu1['childs'] as $c => $menu2)
                                <li class="{{ strpos($_SERVER['REQUEST_URI'], $menu2['url']) !== false ? 'active' : '' }}"><a href="{{ $menu2['url'] }}" title="{{ $menu2['name'] }}">{{ $menu2['name'] }}</a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
</nav>
<style>
    .fixed {
        top: 0;
        position: fixed;
    }

    nav.navbar-collapse.fixed {
        width: 100%;
        z-index: 999;
    }
</style>
<script>
    $(document).ready(function () {
        var obj = $('nav.navbar-collapse');
        var top = obj.offset().top - parseFloat(obj.css('marginTop').replace(/auto/, 0));

        $(window).scroll(function (event) {
            // what the y position of the scroll is
            var y = $(this).scrollTop();

            // whether that's below the form
            if (y >= top) {
                // if so, ad the fixed class
                obj.addClass('fixed');
            } else {
                // otherwise remove it
                obj.removeClass('fixed');
            }
        });
    });

</script>