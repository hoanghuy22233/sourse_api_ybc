<div id="boxSearch" class="radius">
    <h3>
        <img src="/public/frontend/themes/raovat/img/icon-searchbox.png"
             alt="Công cụ tìm kiếm"><strong>Công cụ
            tìm kiếm</strong></h3>
    <ul class="nav nav-tabs">
        <li class="{{ (isset($_GET['boxSearch1']) || !isset($_GET['boxSearch2'])) ? 'active' : '' }}"><a data-toggle="tab" href="#boxSearch1"
                              ><strong>Nhà phân
                    phối</strong></a></li>
        <li class="{{ isset($_GET['boxSearch2']) ? 'active' : '' }}"><a data-toggle="tab" href="#boxSearch2"><strong>Doanh nghiệp</strong></a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="boxSearch1" class="rnbox-search tab-pane {{ (isset($_GET['boxSearch1']) || !isset($_GET['boxSearch2'])) ? 'fade in active' : '' }}">
            <form action="/tim-kiem" method="GET" id="searchproduct"
                  name="searchproduct">
                <div><input  name="keyword"
                            placeholder="Nhập từ khóa cần tìm..." type="text"
                            value="{{ @$_GET['keyword'] }}">
                </div>
                <div>
                    <select class="category_id"  name="category_id">
                        <option value="">- Danh mục -</option>
                        <?php
                        $categories = CommonHelper::getFromCache('categories_type10', ['categories']);
                        if (!$categories) {
                        $categories = \Modules\ThemeRaoVat\Models\Category::where('type', 10)->whereNull('parent_id')->orderBy('name', 'asc')->pluck('name', 'id');
                            CommonHelper::putToCache('categories_type10', $categories, ['categories']);
                        }
                        ?>
                        @foreach($categories as $id => $name)
                            <option value="{{ $id }}" {{ $id == @$_GET['category_id'] ? 'selected' : '' }}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <select class="category_child_id"  name="category_child_id">
                        <option value="">- Ngành hàng -</option>
                    </select>
                </div>

                <div>
                    <?php
                    $provinces = CommonHelper::getFromCache('province_id_asc', ['provinces']);
                    if (!$provinces) {
                    $provinces = \App\Models\Province::orderBy('name', 'asc')->pluck('name', 'id');
                        CommonHelper::putToCache('province_id_asc', $provinces, ['provinces']);
                    }
                    ?>
                    <select class=""  name="province_id">
                        <option value="">- Tỉnh/ TP ưu tiên -</option>
                        @foreach($provinces as $id => $name)
                            <option value="{{ $id }}" {{ @$_GET['province_id'] == $id ? 'selected' : '' }}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>

                <div>
                    <select class="" id="price_range" name="price_range">
                        <option value="">- Yêu cầu tài chính -</option>
                        <option value="1" {{ @$_GET['price_range'] == 1 ? 'selected' : '' }}>
                            Không yêu cầu
                        </option>
                        <option value="2" {{ @$_GET['price_range'] == 2 ? 'selected' : '' }}>
                            Dưới 100 Triệu
                        </option>
                        <option value="3" {{ @$_GET['price_range'] == 3 ? 'selected' : '' }}>
                            100 triệu - 200 triệu
                        </option>
                        <option value="4" {{ @$_GET['price_range'] == 4 ? 'selected' : '' }}>
                            200 triệu - 300 triệu
                        </option>
                        <option value="5" {{ @$_GET['price_range'] == 5 ? 'selected' : '' }}>
                            300 triệu - 500 triệu
                        </option>
                        <option value="6" {{ @$_GET['price_range'] == 6 ? 'selected' : '' }}>
                            500 triệu - 800 triệu
                        </option>
                        <option value="7" {{ @$_GET['price_range'] == 7 ? 'selected' : '' }}>
                            800 triệu - 1 Tỷ
                        </option>
                        <option value="8" {{ @$_GET['price_range'] == 8 ? 'selected' : '' }}>
                            1 Tỷ - 2 Tỷ
                        </option>
                        <option value="9" {{ @$_GET['price_range'] == 9 ? 'selected' : '' }}>
                            2 tỷ - 3 tỷ
                        </option>
                        <option value="10" {{ @$_GET['price_range'] == 10 ? 'selected' : '' }}>
                            3 tỷ - 5 tỷ
                        </option>
                        <option value="11" {{ @$_GET['price_range'] == 11 ? 'selected' : '' }}>
                            5 tỷ - 7 tỷ
                        </option>
                        <option value="12" {{ @$_GET['price_range'] == 12 ? 'selected' : '' }}>
                            7 tỷ - 10 tỷ
                        </option>
                        <option value="13" {{ @$_GET['price_range'] == 13 ? 'selected' : '' }}>
                            10 tỷ - 15 tỷ
                        </option>
                        <option value="14" {{ @$_GET['price_range'] == 14 ? 'selected' : '' }}>
                            15 tỷ - 20 tỷ
                        </option>
                        <option value="15" {{ @$_GET['price_range'] == 15 ? 'selected' : '' }}>
                            20 tỷ - 30 tỷ
                        </option>
                        <option value="16" {{ @$_GET['price_range'] == 16 ? 'selected' : '' }}>
                            30 tỷ - 50 tỷ
                        </option>
                        <option value="17" {{ @$_GET['price_range'] == 17 ? 'selected' : '' }}>
                            Trên 50 tỷ
                        </option>
                    </select>
                </div>
                <div>
                    <select class="" id="acreage" name="acreage">
                        <option value="">- Yêu cầu mặt bằng -</option>
                        <option value="1" {{ @$_GET['acreage'] == 1 ? 'selected' : '' }}>
                            Chưa xác định
                        </option>
                        <option value="2" {{ @$_GET['acreage'] == 2 ? 'selected' : '' }}>
                            Dưới 30 m2
                        </option>
                        <option value="3" {{ @$_GET['acreage'] == 3 ? 'selected' : '' }}>
                            30 m2 - 50 m2
                        </option>
                        <option value="4" {{ @$_GET['acreage'] == 4 ? 'selected' : '' }}>
                            50 m2 - 80 m2
                        </option>
                        <option value="5" {{ @$_GET['acreage'] == 5 ? 'selected' : '' }}>
                            80 m2 - 120 m2
                        </option>
                        <option value="6" {{ @$_GET['acreage'] == 6 ? 'selected' : '' }}>
                            120 m2 - 200 m2
                        </option>
                        <option value="7" {{ @$_GET['acreage'] == 7 ? 'selected' : '' }}>
                            200 m2 - 250 m2
                        </option>
                        <option value="8" {{ @$_GET['acreage'] == 8 ? 'selected' : '' }}>
                            250 m2 - 300 m2
                        </option>
                        <option value="9" {{ @$_GET['acreage'] == 9 ? 'selected' : '' }}>
                            300 m2 - 500 m2
                        </option>
                        <option value="10" {{ @$_GET['acreage'] == 10 ? 'selected' : '' }}>
                            Trên 500 m2
                        </option>
                    </select>
                </div>
                <div>
                    <input class="btn btn-primary btn-block text-uppercase btn-lg" name="boxSearch1" value="Tìm kiếm" type="submit" />
                </div>
            </form>
        </div>
        <div id="boxSearch2" class="rnbox-search tab-pane {{ isset($_GET['boxSearch2']) ? 'fade in active' : '' }}">
            <form action="/tim-kiem" method="GET" id="searchagent"
                  name="searchagent">
                <div><input  name="keyword"
                            placeholder="Nhập từ khóa cần tìm..." type="text" value="{{ @$_GET['keyword'] }}">
                </div>
                <div>
                    <select class=""  name="post_type">
                        <option value="">- Loại hình -</option>
                        <option value="1" {{ @$_GET['post_type'] == 1 ? 'selected' : '' }}>Nhà sản xuất (Manufacturer)
                        </option>
                        <option value="2" {{ @$_GET['post_type'] == 2 ? 'selected' : '' }}>Nhà phân phối (Distributor)
                        </option>
                        <option value="3" {{ @$_GET['post_type'] == 3 ? 'selected' : '' }}>Nhà bán buôn (Wholesaler)
                        </option>
                        <option value="4" {{ @$_GET['post_type'] == 4 ? 'selected' : '' }}>Nhà bán lẻ (Retailer)
                        </option>
                    </select>
                </div>
                <div>
                    <select class="category_id"  name="category_id">
                        <option value="">- Danh mục -</option>
                        @foreach($categories as $id => $name)
                            <option value="{{ $id }}" {{ $id == @$_GET['category_id'] ? 'selected' : '' }}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <select class="category_child_id"  name="category_child_id">
                        <option value="">- Ngành hàng -</option>
                    </select>
                </div>
                <div>
                    <select class="province_id"  name="province_id">
                        <option value="">- Tỉnh/ TP -</option>
                        @foreach($provinces as $id => $name)
                            <option value="{{ $id }}" {{ @$_GET['province_id'] == $id ? 'selected' : '' }}>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <select class="district_id" id="district_id" name="district_id">
                        <option value="">- Quận/ Huyện -</option>
                    </select>
                </div>
                <div>
                    <input class="btn btn-primary btn-block text-uppercase btn-lg" name="boxSearch2" value="Tìm kiếm" type="submit" />
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function fillCategoryChild(category_id = false) {
        if (category_id == false) {
            var category_id = $('#boxSearch1 .category_id').val();
            if (category_id == '') {
                category_id = $('#boxSearch2 .category_id').val();
            }
        }

        if (category_id != '') {
            $.ajax({
                url : '/admin/category_post/get-childs-html',
                type: 'GET',
                data: {
                    category_id : category_id,
                },
                success: function (resp) {
                    var data = resp.data;
                    var html = '<option value="">Chọn Ngành hàng</option>';
                    Object.keys(data).map(function(key) {
                        html += '<option value="'+key+'" '+ (key.toString() == '{{ @$_GET['category_child_id'] }}' ? 'selected' : '') +'>'+data[key]+'</option>';
                    });
                    $('#boxSearch .category_child_id').html(html);
                },
                error: function () {
                    alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại');
                }
            });
        }
    }

    function fillDistricts(province_id = false) {
        if (province_id == false) {
            var province_id = $('#boxSearch .province_id').val();
        }

        if (province_id != '') {
            $.ajax({
                url : '/admin/location/districts/get-data',
                type: 'GET',
                data: {
                    province_id : province_id,
                },
                success: function (resp) {
                    var data = resp.data;
                    var html = '<option value="">Chọn quận / huyện</option>';
                    Object.keys(data).map(function(key) {
                        html += '<option value="'+key+'" '+ (key.toString() == '{{ @$_GET['district_id'] }}' ? 'selected' : '') +'>'+data[key]+'</option>';
                    });
                    $('#boxSearch .district_id').html(html);
                },
                error: function () {
                    alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại');
                }
            });
        }
    }

    $(document).ready(function () {
        fillCategoryChild();
        fillDistricts();

        $('body').on('change', '.category_id', function () {
            fillCategoryChild($(this).val());
        });

        $('body').on('change', '.province_id', function () {
            fillDistricts($(this).val());
        });
    });
</script>