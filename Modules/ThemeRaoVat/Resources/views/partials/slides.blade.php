<div class="smartowl_header_slider">
<?php
$data = CommonHelper::getFromCache('banners_banner_slides', ['banners']);
if (!$data) {
    $data = @\Modules\ThemeRaoVat\Models\Banner::where('location', 'banner_slides')->where('status', 1)->get();
    CommonHelper::putToCache('banners_banner_slides', $data, ['banners']);
}
?>
{{--<img data-src="{{ asset('public/filemanager/userfiles/' . @\Modules\ThemeRaoVat\Models\Banner::where('location', 'banner_slides')->where('status', 1)->first()->image) }}"
     style="width: 100%;" class="lazy">--}}
<!-- START Inspiration Header REVOLUTION SLIDER 6.2.12 -->
    <p class="rs-p-wp-fix"></p>
    <rs-module-wrap id="rev_slider_32_1_wrapper" data-source="gallery" style="background:transparent;padding:0;">
        <rs-module id="rev_slider_32_1" style="" data-version="6.2.12">
            <rs-slides>
                @foreach($data as $k => $v)
                    <rs-slide data-key="rs-{{ $k }}" data-title="Slide" data-anim="ei:d;eo:d;s:800;r:0;t:fade;sl:d;">
                        <img src="{{ asset('public/filemanager/userfiles/' . $v->image) }}"
                             alt="Slide" title="Homepage 1"
                             data-bg="c:#eeeeee;"
                             data-parallax="off" class="rev-slidebg " data-no-retina>
                        <!--
                            -->
                        <rs-layer id="slider-32-slide-{{ $k }}-layer-1" class="rs-pxl-2" data-type="text"
                                  data-color="rgba(0,0,0,1)" data-rsp_ch="on"
                                  data-xy="x:c;xo:0,-1px,0,0;y:m;yo:-70px,-70px,-70px,-78px;"
                                  data-text="w:nowrap,nowrap,nowrap,normal;s:90,70,70,40;l:80,80,80,60;a:center;"
                                  data-dim="w:auto,auto,auto,411px;h:auto,auto,auto,121px;"
                                  data-frame_0="y:-50px;sX:2;sY:2;rX:-45deg;"
                                  data-frame_1="e:power4.out;st:509.999694824;sp:1500;sR:509.999694824;"
                                  data-frame_999="y:30px;sX:0.8;sY:0.8;o:0;rX:45deg;e:power2.inOut;st:3799.99969482;sp:600;sR:1790;"
                                  style="z-index:15;font-family:Pacifico;">{{ $v->name }}
                        </rs-layer>
                        <!--

                            -->
                        <rs-layer id="slider-32-slide-{{ $k }}-layer-2" class="rs-pxl-2" data-type="text"
                                  data-color="rgba(68,68,68,1)" data-rsp_ch="on"
                                  data-xy="x:c;xo:-8px;y:m;yo:36px,36px,36px,-47px;"
                                  data-text="w:nowrap,nowrap,nowrap,normal;s:20,20,20,15;l:20,20,20,30;a:center;"
                                  data-dim="w:auto,auto,auto,360px;" data-frame_0="y:50px;sX:2;sY:2;rX:45deg;"
                                  data-frame_1="e:power4.out;st:599.999694824;sp:1500;sR:599.999694824;"
                                  data-frame_999="o:0;st:7799.99969482;sp:1500;sR:5700;"
                                  style="z-index:17;font-family:Roboto;">{!! $v->intro !!}
                        </rs-layer>
                        @if($v->link != '')
                            <a id="slider-32-slide-{{ $k }}-layer-3" class="rs-layer rev-btn rs-pxl-3"
                               href="{{ $v->link }}"
                               target="_blank" rel="noopener" data-type="button" data-color="rgba(255,255,255,1)"
                               data-xy="x:c;y:m;yo:112px,112px,112px,92px;" data-text="s:15;l:50;ls:2px;fw:700;"
                               data-rsp_bd="off" data-padding="r:30;l:30;" data-border="bor:3px,3px,3px,3px;"
                               data-frame_0="y:100px;rX:90deg;"
                               data-frame_1="e:power4.out;st:699.999694824;sp:1500;sR:699.999694824;"
                               data-frame_999="st:7799.99969482;sp:1500;auto:true;"
                               data-frame_hover="bgc:#ea940c;boc:#000;bor:3px,3px,3px,3px;bos:solid;oX:50;oY:50;sp:150;e:power1.inOut;"
                               style="z-index:18;background-color:rgba(243,156,18,1);font-family:Roboto;cursor:pointer;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">Xem
                                thêm
                            </a>
                        @endif
                    </rs-slide>
                @endforeach
            </rs-slides>
        </rs-module>
        <script type="text/javascript">
            setREVStartSize({
                c: 'rev_slider_32_1',
                rl: [1240, 1024, 778, 480],
                el: [868, 768, 960, 720],
                gw: [1240, 1024, 778, 480],
                gh: [868, 768, 960, 720],
                type: 'standard',
                justify: '',
                layout: 'fullscreen',
                offsetContainer: '',
                offset: '60px',
                mh: "0"
            });
            var revapi32,
                tpj;
            jQuery(function () {
                tpj = jQuery;
                revapi32 = tpj("#rev_slider_32_1")
                if (revapi32 == undefined || revapi32.revolution == undefined) {
                    revslider_showDoubleJqueryError("rev_slider_32_1");
                } else {
                    revapi32.revolution({
                        sliderLayout: "fullscreen",
                        visibilityLevels: "1240,1024,778,480",
                        gridwidth: "1240,1024,778,480",
                        gridheight: "868,768,960,720",
                        lazyType: "all",
                        perspective: 600,
                        perspectiveType: "local",
                        editorheight: "868,768,960,720",
                        responsiveLevels: "1240,1024,778,480",
                        fullScreenOffset: "60px",
                        progressBar: {
                            disableProgressBar: true
                        },
                        navigation: {
                            mouseScrollNavigation: false,
                            onHoverStop: false,
                            touch: {
                                touchenabled: true
                            },
                            arrows: {
                                enable: true,
                                style: "metis",
                                hide_onmobile: true,
                                hide_under: 768,
                                left: {},
                                right: {}
                            }
                        },
                        parallax: {
                            levels: [10, 15, 20, 25, 30, 35, 40, -10, -15, -20, -25, -30, -35, -40, -45, 55],
                            type: "scroll",
                            origo: "slidercenter"
                        },
                        fallbacks: {
                            disableFocusListener: true,
                            allowHTML5AutoPlayOnAndroid: true
                        },
                    });
                }

            });
        </script>
        <script>
            var htmlDivCss = unescape("%23rev_slider_32_1_wrapper%20.metis.tparrows%20%7B%0A%20%20background%3A%23ffffff%3B%0A%20%20padding%3A10px%3B%0A%20%20transition%3Aall%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20width%3A60px%3B%0A%20%20height%3A60px%3B%0A%20%20box-sizing%3Aborder-box%3B%0A%20%7D%0A%20%0A%20%23rev_slider_32_1_wrapper%20.metis.tparrows%3Ahover%20%7B%0A%20%20%20background%3Argba%28255%2C255%2C255%2C0.75%29%3B%0A%20%7D%0A%20%0A%20%23rev_slider_32_1_wrapper%20.metis.tparrows%3Abefore%20%7B%0A%20%20color%3A%23000000%3B%20%20%0A%20%20%20transition%3Aall%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%7D%0A%20%0A%20%23rev_slider_32_1_wrapper%20.metis.tparrows%3Ahover%3Abefore%20%7B%0A%20%20%20transform%3Ascale%281.5%29%3B%0A%20%20%7D%0A%20%0A");
            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
            if (htmlDiv) {
                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
            } else {
                var htmlDiv = document.createElement('div');
                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
            }
        </script>
        <script>
            var htmlDivCss = unescape("%0A%0A");
            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
            if (htmlDiv) {
                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
            } else {
                var htmlDiv = document.createElement('div');
                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
            }
        </script>
    </rs-module-wrap>
    <!-- END REVOLUTION SLIDER -->
</div>