<style>
    .alert.alert-default {
        border: 1px solid #ccc;
    }

    .wpb_wrapper.bxh-time {
        display: inline-block;
        width: 100%;
        text-align: center;
        margin-bottom: 50px;
    }

    .block-bxh .vc_row.wpb_row.vc_row-fluid {
        margin-bottom: 10px;
    }

    .alert-success {
        color: #FFFFFF !important;
    }

    .nut-khac .alert {
        display: inline-block;
        font-weight: bold;
        padding: 15px 30px;
        margin-right: 20px;
        width: 156px;
    }

    .nut-khac {
        display: inline-block;
        width: 100%;
        margin-top: 30px;
        text-align: center;
    }

    .wpb_row {
        background-size: cover;
    }

    @media (max-width: 768px) {
        .block-bxh .wpb_column {
            width: 100% !important;
        }
    }
</style>
<div class="wpb_row high-padding content-area block-bxh desktop"
     style="background-image: url(/public/frontend/themes/sach100/images/bxh.png) !important;">
    <div class="container">
        <div class="row">
            <main id="main" class="vc_col-md-12 site-main main-content">
                <article id="post-1825" class="post-1825 page type-page status-publish hentry">
                    <div class="entry-content">
                        <div class="vc_row wpb_row vc_row-fluid ">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="title-subtile-holder"><h1
                                                    class="section-title ">Bảng Xếp Hạng</h1>
                                            <div class="section-border "></div>
                                            <div class="section-subtitle "></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="vc_row wpb_row vc_row-fluid bxh-block" >
                            <div class="wpb_wrapper bxh-time">
                                <a class="alert  alert-success " data-box="bxh-box-1" data-animate="fadeInLeft"
                                   href="javascript:;"
                                   style="border-top-right-radius: 0;border-bottom-right-radius: 0;">Ngày
                                </a>
                                <a class="alert  alert-default " data-box="bxh-box-2" data-animate="fadeInLeft"
                                   href="javascript:;" style="border-radius: 0;
    border: 1px solid #ccc;
    border-left: 0;
    border-right: 0;">Tuần
                                </a>
                                <a class="alert  alert-default " data-box="bxh-box-3" data-animate="fadeInLeft"
                                   href="javascript:;" style="border-top-left-radius: 0;
    border-bottom-left-radius: 0">Tháng
                                </a>
                                <?php
                                $subjects = \Modules\Sach100Exam\Models\Category::select('name', 'id')->where('type', 10)->where('status', 1)->orderBy('id', 'asc')->get();

                                $ngay = date('Y-m-d 00:00:00');
                                $tuan = date('Y-m-1 00:00:00');
                                $thang = date('Y-m-0 00:00:00');

                                $bxh_ngay = [];
                                $bxh_tuan = [];
                                $bxh_thang = [];
                                foreach ($subjects as $subject) {
                                    $bxh_ngay[$subject->id] = \Modules\ThemeRaoVat\Models\Exam::where('subject_id', $subject->id)->where('created_at', '>', $ngay)
                                        ->whereNotNull('total_point')->where('total_point', '!=', 0)->groupBy('student_id')
                                        ->orderBy('total_point', 'asc')->limit(5)->get();

                                    $bxh_tuan[$subject->id] = \Modules\ThemeRaoVat\Models\Exam::where('subject_id', $subject->id)->where('created_at', '>', $tuan)->groupBy('student_id')
                                        ->whereNotNull('total_point')->where('total_point', '!=', 0)->orderBy('total_point', 'asc')->limit(5)->get();

                                    $bxh_thang[$subject->id] = \Modules\ThemeRaoVat\Models\Exam::where('subject_id', $subject->id)->where('created_at', '>', $thang)->groupBy('student_id')
                                        ->whereNotNull('total_point')->where('total_point', '!=', 0)->orderBy('total_point', 'asc')->limit(5)->get();
                                }
                                ?>
                            </div>

                            <div>
                                <div class="bxh-box-1">
                                    @foreach ($subjects as $subject)
                                        <div id="bxh-1" style="width: 20%;"
                                             class="wpb_column vc_column_container vc_col-sm-2">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="pricing-table animateIn recommended"
                                                         data-animate="fadeInLeft">
                                                        <div class="table-content"><h4
                                                                    class="text-center title-pricing">
                                                                {{ $subject->name }}</h4>
                                                            <div class="circle-container">
                                                                <div class="block-circle">
                                                                    <div class="circle-content"><p
                                                                                class="text-center price_circle">{{ $subject->name }}
                                                                        </p></div>
                                                                </div>
                                                            </div>
                                                            <ul class="text-center" style="padding: 0;">
                                                                @foreach($bxh_ngay[$subject->id] as $k => $exam)
                                                                    <li>{{ $k + 1 }}. {{ @$exam->student->name }}
                                                                        - {{ $exam->total_point }}
                                                                        /{{ $exam->max_point }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="bxh-box-2" style="display: none;">
                                    @foreach ($subjects as $subject)
                                        <div id="bxh-1" style="width: 20%;"
                                             class="wpb_column vc_column_container vc_col-sm-2">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="pricing-table animateIn recommended"
                                                         data-animate="fadeInLeft">
                                                        <div class="table-content"><h4
                                                                    class="text-center title-pricing">
                                                                {{ $subject->name }}</h4>
                                                            <div class="circle-container">
                                                                <div class="block-circle">
                                                                    <div class="circle-content"><p
                                                                                class="text-center price_circle">{{ $subject->name }}
                                                                        </p></div>
                                                                </div>
                                                            </div>
                                                            <ul class="text-center" style="padding: 0;">
                                                                @foreach($bxh_tuan[$subject->id] as $k => $exam)
                                                                    <li>{{ $k + 1 }}. {{ @$exam->student->name }}
                                                                        - {{ $exam->total_point }}
                                                                        /{{ $exam->max_point }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="bxh-box-3" style="display: none;">
                                    @foreach ($subjects as $subject)
                                        <div id="bxh-1" style="width: 20%;"
                                             class="wpb_column vc_column_container vc_col-sm-2">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="pricing-table animateIn recommended"
                                                         data-animate="fadeInLeft">
                                                        <div class="table-content"><h4
                                                                    class="text-center title-pricing">
                                                                {{ $subject->name }}</h4>
                                                            <div class="circle-container">
                                                                <div class="block-circle">
                                                                    <div class="circle-content"><p
                                                                                class="text-center price_circle">{{ $subject->name }}
                                                                        </p></div>
                                                                </div>
                                                            </div>
                                                            <ul class="text-center" style="padding: 0;">
                                                                @foreach($bxh_thang[$subject->id] as $k => $exam)
                                                                    <li>{{ $k + 1 }}. {{ @$exam->student->name }}
                                                                        - {{ $exam->total_point }}
                                                                        /{{ $exam->max_point }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        @if($_SERVER['REQUEST_URI'] != '/')
                            <div class="wpb_wrapper nut-khac" style="
    ">
                                <a class="alert  alert-warning " data-animate="fadeInLeft" href="/tao-bai-thi">Dạng đề
                                    khác
                                </a>
                                <a class="alert  alert-info " data-animate="fadeInLeft"
                                   href="/dap-an/{{ @$_GET['exam_id'] }}">Xem đáp án
                                </a>
                                <a class="alert  alert-success " data-animate="fadeInLeft" href="/tin-tuc">Tin tức
                                </a>
                            </div>
                        @endif
                    </div><!-- .entry-content -->
                </article><!-- #post-## -->
            </main>
        </div>
    </div>
</div>


<div class="vc_row wpb_row vc_row-fluid vc_custom_1461047794091 block-bxh mobile" style="background-image: url(/public/frontend/themes/sach100/images/danh_gia_cua_sv.png) !important; margin: 0 !important;
    padding-top: 43px;
    padding-bottom: 68px;">
    <div class="container">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="title-subtile-holder"><h1
                                class="section-title ">Bảng Xếp Hạng</h1>
                        <div class="section-border "></div>
                        <div class="section-subtitle "></div>
                    </div>
                    <div class="vc_row bxh-block"  >
                        <div class="wpb_wrapper bxh-time">
                            <a class="alert  alert-success " data-box="bxh-box-1" data-animate="fadeInLeft"
                               href="javascript:;"
                               style="border-top-right-radius: 0;border-bottom-right-radius: 0;">Ngày
                            </a>
                            <a class="alert  alert-default " data-box="bxh-box-2" data-animate="fadeInLeft"
                               href="javascript:;" style="border-radius: 0;
    border: 1px solid #ccc;
    border-left: 0;
    border-right: 0;">Tuần
                            </a>
                            <a class="alert  alert-default " data-box="bxh-box-3" data-animate="fadeInLeft"
                               href="javascript:;" style="border-top-left-radius: 0;
    border-bottom-left-radius: 0">Tháng
                            </a>
                        </div>

                        <div>
                            <div class="bxh-box-1">
                                <div data-animate="fadeIn"
                                     class="testimonials-container-3 owl-carousel owl-theme animateIn">
                                    @foreach ($subjects as $subject)
                                        <div class="item vc_col-md-12 relative text-white" id="bxh-1">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="pricing-table animateIn recommended"
                                                         data-animate="fadeInLeft">
                                                        <div class="table-content"><h4
                                                                    class="text-center title-pricing">
                                                                {{ $subject->name }}</h4>
                                                            <div class="circle-container">
                                                                <div class="block-circle">
                                                                    <div class="circle-content"><p
                                                                                class="text-center price_circle">{{ $subject->name }}
                                                                        </p></div>
                                                                </div>
                                                            </div>
                                                            <ul class="text-center" style="padding: 0;">
                                                                @foreach($bxh_ngay[$subject->id] as $k => $exam)
                                                                    <li>{{ $k + 1 }}. {{ @$exam->student->name }}
                                                                        - {{ $exam->total_point }}
                                                                        /{{ $exam->max_point }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="bxh-box-2" style="display: none;">
                                <div data-animate="fadeIn"
                                     class="testimonials-container-3 owl-carousel owl-theme animateIn">
                                    @foreach ($subjects as $subject)
                                        <div class="item vc_col-md-12 relative text-white" id="bxh-1">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="pricing-table animateIn recommended"
                                                         data-animate="fadeInLeft">
                                                        <div class="table-content"><h4
                                                                    class="text-center title-pricing">
                                                                {{ $subject->name }}</h4>
                                                            <div class="circle-container">
                                                                <div class="block-circle">
                                                                    <div class="circle-content"><p
                                                                                class="text-center price_circle">{{ $subject->name }}
                                                                        </p></div>
                                                                </div>
                                                            </div>
                                                            <ul class="text-center" style="padding: 0;">
                                                                @foreach($bxh_tuan[$subject->id] as $k => $exam)
                                                                    <li>{{ $k + 1 }}. {{ @$exam->student->name }}
                                                                        - {{ $exam->total_point }}
                                                                        /{{ $exam->max_point }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="bxh-box-3" style="display: none;">
                                <div data-animate="fadeIn"
                                     class="testimonials-container-3 owl-carousel owl-theme animateIn">
                                    @foreach ($subjects as $subject)
                                        <div class="item vc_col-md-12 relative text-white" id="bxh-1">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="pricing-table animateIn recommended"
                                                         data-animate="fadeInLeft">
                                                        <div class="table-content"><h4
                                                                    class="text-center title-pricing">
                                                                {{ $subject->name }}</h4>
                                                            <div class="circle-container">
                                                                <div class="block-circle">
                                                                    <div class="circle-content"><p
                                                                                class="text-center price_circle">{{ $subject->name }}
                                                                        </p></div>
                                                                </div>
                                                            </div>
                                                            <ul class="text-center" style="padding: 0;">
                                                                @foreach($bxh_thang[$subject->id] as $k => $exam)
                                                                    <li>{{ $k + 1 }}. {{ @$exam->student->name }}
                                                                        - {{ $exam->total_point }}
                                                                        /{{ $exam->max_point }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($_SERVER['REQUEST_URI'] != '/')
                        <div class="wpb_wrapper nut-khac" style="
    ">
                            <a class="alert  alert-warning " data-animate="fadeInLeft" href="/tao-bai-thi">Dạng đề
                                khác
                            </a>
                            <a class="alert  alert-info " data-animate="fadeInLeft"
                               href="/dap-an/{{ @$_GET['exam_id'] }}">Xem đáp án
                            </a>
                            <a class="alert  alert-success " data-animate="fadeInLeft" href="/tin-tuc">Tin tức
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery('.bxh-block a').click(function () {
        var block = jQuery(this).data('box');
        console.log(block);
        jQuery(this).addClass('alert-success').removeClass('alert-default');
        jQuery(this).siblings().removeClass('alert-success').addClass('alert-default');
        jQuery('.' + block).show();
        jQuery('.' + block).siblings().hide();
    });
</script>