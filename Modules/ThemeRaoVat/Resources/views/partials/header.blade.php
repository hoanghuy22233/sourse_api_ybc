<header id="header" class="bg-w navbar" role="">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><i
                            class="fa fa-bars"></i></button>
                <a class="navbar-brand" href="/"
                   title="{{ @$settings['name'] }}"><img
                            alt="{{ @$settings['name'] }}"
                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo'], null, 90) }}"></a>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9" id="hdRight">
                <div class="text-right">
                    <div class="banner" id="BannerTop">
                        <?php
                        $banner = CommonHelper::getFromCache('banners_home_header_top', ['banners']);
                        if (!$banner) {
                        $banner = \Modules\ThemeRaoVat\Models\Banner::where('location', 'home_header_top')->where('status', 1)->get();
                            CommonHelper::putToCache('banners_home_header_top', $banner, ['banners']);
                        }
                        ?>
                        @foreach($banner as $b)
                            <div class="bannerShow"><a
                                        href="{{ $b->link }}"
                                        target="_blank"><img
                                            data-src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb($b->image)}}" class="lazy"
                                            width="728" height="90"></a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12" id="hdrightb">
                <div class="navTop">
                    <ul class="nav nav-pills">
                        <li><a href="/admin/post/add" class="btn btn-danger"><i class="fa fa-plus"></i> Đăng tin tìm
                                đại lý</a></li>
                        <li><a href="/admin/post/add" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>
                                Đăng tin bán sỉ</a></li>
                        <li><a href="/lien-he"><i class="fa fa-question-circle-o"></i> Gửi câu hỏi</a></li>
                        @if(\Auth::guard('admin')->check())
                            <li><a rel="nofollow" href="/admin/profile" class="rnred"><i class="fa fa-tasks"></i> Trang cá nhân</a></li>
                            <li><a rel="nofollow" href="/admin/logout"><i class="fa fa-sign-out"></i> Thoát</a></li>
                        @else
                            <li><a rel="nofollow" href="/admin/login"><i class="fa fa-sign-in"></i> Đăng
                                    nhập</a></li>
                            <li><a rel="nofollow" href="/admin/register"><i class="fa fa-user-plus"></i> Đăng
                                    ký</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>