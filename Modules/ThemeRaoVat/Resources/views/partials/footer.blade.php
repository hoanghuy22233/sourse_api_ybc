<footer id="footer">

    {{--<div class="bg-info footer-hotline" id="footer-hotline">
        <div class="container p15">
            <div class="row">
                <div class="col-md-12">
                    <div class="title text-primary">
                        <i class="fa fa-headphones" aria-hidden="true"></i> Hotline hỗ trợ đăng tin, quảng cáo, hợp
                        tác tìm đại lý
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-12"><a href="tel:0963656512"><strong class="green">0963.656.512</strong></a>
                    - <span>Ms Phương</span></div>

                <div class="col-md-3 col-xs-12"><a href="tel:0977111896 "><strong
                                class="green">0977.111.896</strong></a> - <span>Ms Bích Phượng</span></div>
                <div class="col-md-3 col-xs-12"><a href="tel:0966908766"><strong
                                class="green">0966.908.766 </strong></a> - <span>Ms Lý</span></div>
                <div class="col-md-3 col-xs-12"><a href="tel:0962977716"><strong
                                class="green">0962.977.716 </strong></a> - <span>Ms Mận</span></div>
                <div class="col-md-3 col-xs-12"><a href="tel:0918955158"><strong class="green">0918.955.158</strong></a>
                    - <span>Ms Thảo</span></div>


            </div>
        </div>
    </div>--}}

    {{--    @include('themeraovat::partials.footer_categories')--}}

    <div id="ftBottom" class="text-center">
        <div class="container">

            <ul class="nav nav-footer">
                <?php
                $menus = CommonHelper::getFromCache('menus_menu_footer', ['menus']);
                if (!$menus) {
                $menus = \Modules\ThemeRaoVat\Models\Menu::select('id', 'name', 'url')->where('location', 'menu_footer')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->limit(15)->get();
                    CommonHelper::putToCache('menus_menu_footer', $menus, ['menus']);
                }
                ?>
                @foreach($menus as $menu1)

                    <li><a title="{{ $menu1['name'] }}" href="{{ $menu1['url'] }}" class="">{{ $menu1['name'] }}</a></li>
                @endforeach


            </ul>
            <?php
            $footers = CommonHelper::getFromCache('widgets_footer1', ['widgets']);
            if (!$footers) {
            $footers = \Modules\ThemeRaoVat\Models\Widget::select('name', 'content', 'location')->where('status', 1)->whereIn('location', ['footer1'])
                ->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->get();
                CommonHelper::putToCache('widgets_footer1', $footers, ['widgets']);
            }
            $footer_icon = CommonHelper::getFromCache('widgets_footer_icon', ['widgets']);
            if (!$footer_icon) {
            $footer_icon = \Modules\ThemeRaoVat\Models\Widget::select('name', 'content', 'location')->where('status', 1)->whereIn('location', ['footer_icon'])
                ->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->get();
                CommonHelper::putToCache('widgets_footer_icon', $footer_icon, ['widgets']);
            }
            ?>
            <address>
                @foreach($footers as $v)
                    {!! @$v->content !!}
                @endforeach
            </address>
            <p>Cộng đồng Tìmkiếmđốitác.com là diễn đàn phi lợi nhuận, phi tôn giáo, phi chính trị & được lập ra với mục đích giao lưu, học hỏi, chia sẻ và thảo luận mọi vấn đề liên quan đến giao lưu hợp tác kinh tế. Ngôi nhà chung cho tất cả mọi người yêu thích kinh doanh.
                Website đang hoạt động thử nghiệm, chờ giấy phép MXH của Bộ TT & TT.</p>
            <div class="social">
                @foreach($footer_icon as $fi)
                    {!! @$fi->content !!}

                @endforeach
                {{--<a href="https://www.facebook.com/timkiemdoitaccom/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a>--}}
                {{--<a href="" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a>--}}
                {{--<a href="" target="_blank" title="GPlus"><i class="fa fa-google-plus"></i></a>--}}
                {{--<a href="#"><i class="fa fa-pinterest"></i></a>--}}
                {{--<a href="#"><i class="fa fa-linkedin"></i></a>--}}
            </div>
        </div>
    </div>
</footer>