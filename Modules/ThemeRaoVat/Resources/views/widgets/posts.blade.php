<?php
$where = @$config['where'];
$limit = @$config['limit'];
$category_id = @$config['category_id'];
$data = CommonHelper::getFromCache('posts_' . json_encode($config), ['posts']);
if (!$data) {
    $data = \Modules\ThemeEdu\Models\Post::where('status', 1);
    if ($where != '') {
        $data = $data->whereRaw($where);
    }
    if ($limit != '') {
        $data = $data->limit($limit);
    }
    if ($category_id != '') {
        $data = $data->where('multi_cat', 'like', '%|'.$category_id.'|%');
    }
    $data = $data->orderBy('order_no', 'desc')->orderBy('created_at', 'desc');
    $data = $data->get();
    CommonHelper::putToCache('posts_' . json_encode($config), $data, ['posts']);
}
?>
@foreach($data as $post)
    <div class="blog-grid">
        <figure><a href="/blog/{{$post->slug}}.html">
                <img alt="{{@$post->name}}"
                     class="lazy"
                     data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image,361,200) }}"></a>
        </figure>
        <div class="blog-meta">
            <div class="blog-head">
                <a href="/blog/{{$post->slug}}.html" title=""
                   class="date">{{date('d',strtotime($post->created_at))}} T{{ (int)date('m',strtotime($post->created_at)) }}</a>
                <h4 class="blog-title"><a href="/blog/{{$post->slug}}.html"
                                          title=""> {!!$post->name!!}</a>
                </h4>
            </div>
        </div>
    </div>
@endforeach