<style>
    .c-form button {
        border: medium none;
        border-radius: 30px;
        color: #fff;
        float: right;
        font-size: 13px;
        font-weight: 500;
        padding: 10px 30px;
        transition: all 0.2s linear 0s;
        background: #fa6342;
    }
    .contact-add {
        margin-bottom: 50px;
    }
</style>
<h3>Form đăng ký</h3>
<div class="c-form contact-add">
    <div>
        <label>Họ & tên</label>
        <input type="text" placeholder="" id="contact-name">
    </div>
    <div>
        <label>Số điện thoại</label>
        <input type="text" placeholder="" id="contact-tel">
    </div>
    <div>
        <label>Email</label>
        <input type="text" placeholder="" id="contact-email">
    </div>
    <div>
        <label>Địa chỉ</label>
        <input type="text" placeholder="" id="contact-address">
    </div>
    <div>
        <label>Lời nhắn</label>
        <textarea rows="3" placeholder="" id="contact-content"></textarea>
    </div>
    <div>
        <button class="main-btn contact-add-submit" type="submit" data-ripple="">Gửi đi</button>
    </div>
</div>
<script>
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    $(document).ready(function () {
        $('.contact-add-submit').click(function () {
            if ($('#contact-name').val().length == 0) {
                alert ('Bạn chưa nhập vào tên!');
            } else if ($('#contact-tel').val().length == 0) {
                alert ('Bạn chưa nhập vào số điện thoại!');
            } else {
                if ($('#contact-email').val().length != 0 && !isEmail($('#contact-email').val())) {
                    alert ('Email nhập sai!');
                } else {
                    $('.contact-add-submit').attr('disabled', 'disabled');
                    $('.contact-add-submit').css('opacity', '0.4');
                    $.ajax({
                        url : '/admin/ajax/contact/add',
                        type: 'POST',
                        data: {
                            name: $('#contact-name').val(),
                            email: $('#contact-email').val(),
                            tel: $('#contact-tel').val(),
                            address: $('#contact-address').val(),
                            content: $('#contact-content').val(),
                            link: '<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>',
                        },
                        success: function (resp) {
                            if (resp.status == true) {
                                alert(resp.msg);
                                $('.contact-add input, .contact-add textarea').val('');
                                $('.contact-add-submit').removeAttr('disabled');
                                $('.contact-add-submit').css('opacity', '1');
                            } else {
                                alert(resp.msg);
                                // location.reload();
                            }
                        },
                        error: function () {
                            alert('Có lỗi xảy ra! Vui lòng load lại website & thử lại.');
                        }
                    });
                }
            }
        });

        $('.contact-add input, .contact-add textarea').keypress(function (e) {
            if(e.which == 13) {
                $('.contact-add-submit').click();
            }
        });
    });
</script>