@if(\Session::get('show_popup1') == null)
    <style>
        #popup1 img {
            max-height: 400px;
        }
    </style>

    <div class="modal fade " id="popup1" style=" padding-right: 15px;" aria-modal="true">
        <div class="modal-dialog">
            <div class="modal-content">
                @if($widget->name != '')
                    <div class="modal-header">
                        <h4 class="modal-title">{!! $widget->name !!}</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        {!! $widget->content !!}
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger close" data-dismiss="modal">Đóng</button>
                    </div>
                @else
                    <button type="button" class="close" data-dismiss="modal" style="color: #fff;font-size: 35px;position: absolute;right: -21px;top: -24px;">×</button>
                    {!! $widget->content !!}
                @endif
            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function () {
            @if (isset($config['time']))
                myVar = setInterval(function () {
                jQuery('#popup1').removeClass('fade').show();
                clearTimeout(myVar);
            }, {{ $config['time'] }});
            @else
                myVar = setInterval(function () {
                jQuery('#popup1').removeClass('fade').show();
                clearTimeout(myVar);
            }, 5000);
            @endif

            jQuery('#popup1 .close').click(function () {
                jQuery('#popup1').addClass('fade').hide();
            });
        });
    </script>
    <?php
    \Session::put('show_popup1', true);
    ?>
@endif