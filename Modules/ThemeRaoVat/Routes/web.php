<?php
Route::get('rut-gon-linkk/direct', 'Frontend\HomeController@rutGonLinkDirect');
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions', 'locale']], function () {
    Route::group(['prefix' => 'dashboard', 'middleware' => ['guest:admin', 'get_permissions', 'locale', \Modules\ThemeRaoVat\Http\Middleware\CheckPermissionViewDashboard::class]], function () {
        Route::get('', '\App\Http\Controllers\Admin\DashboardController@getIndex');
    });

    Route::group(['prefix' => 'theme'], function () {
        Route::match(['get', 'post'], 'setting', 'Admin\ThemeRaoVatController@setting')->name('themeraovat')->middleware('permission:theme');
    });
});
//Route::get('register-course-when-login', 'Frontend\ContactController@registerCourseWhenLogin')->name('register-course-when-login');
Route::get('route', 'Frontend\ContactController@route')->name('route');
Route::group(['prefix' => '', 'middleware' => 'no_auth:student'], function () {
    Route::get('dang-nhap', 'Frontend\AuthController@getLogin')->name('dang-nhap');
    Route::post('dang-nhap', 'Frontend\AuthController@authenticate');
    Route::get('dang-ky', 'Frontend\AuthController@getRegister')->name('dang-ky');
    Route::post('dang-ky', 'Frontend\AuthController@postRegister');
});

Route::match(array('GET', 'POST'), 'quen-mat-khau', 'Frontend\AuthController@getEmailForgotPassword');
Route::match(array('GET', 'POST'), 'forgot-password/{change_password}', 'Frontend\AuthController@getForgotPassword');
Route::get('dang-xuat', function () {
    \Auth::guard('student')->logout();
    return redirect('/');
});

//Route::match(array('GET', 'POST'), 'profile/{id}', 'Frontend\AuthController@profileAdmin')->name('profile');

Route::group(['prefix' => '', 'middleware' => ['guest:student']], function () {
    Route::get('profile', 'Frontend\StudentController@myProfile');
    Route::get('profile/edit', 'Frontend\StudentController@getEditProfile');
    Route::post('profile/edit', 'Frontend\StudentController@postEditProfile');

    Route::group(['prefix' => 'student'], function () {
        Route::get('doi-mat-khau', 'Frontend\StudentController@getChangePass');
        Route::post('doi-mat-khau', 'Frontend\StudentController@postChangePass');
    });
});
Route::get('lien-he', 'Frontend\ContactController@getContact');
Route::post('lien-he', 'Frontend\ContactController@postContact');

Route::get('tim-kiem', 'Frontend\CourseController@getSearch');

Route::get('lich-hoc', 'Frontend\CourseController@lichHoc');

Route::group(['prefix' => '', 'middleware' => ['guest:student']], function () {
    Route::get('profile', 'Frontend\StudentController@myProfile');
    Route::get('profile/edit', 'Frontend\StudentController@getEditProfile');
    Route::post('profile/edit', 'Frontend\StudentController@postEditProfile');

    Route::group(['prefix' => 'student'], function () {
        Route::get('doi-mat-khau', 'Frontend\StudentController@getChangePass');
        Route::post('doi-mat-khau', 'Frontend\StudentController@postChangePass');
    });

});

Route::get('rut-gon-link', 'Frontend\HomeController@rutGonLink');

Route::group(['prefix' => 'gio-hang'],function () {
    Route::get('', 'Frontend\CartController@getCart');
    Route::post('', 'Frontend\CartController@postCart');
    Route::get('add/{id}', 'Frontend\CartController@addCart');
    Route::get('del_item/{rowId}','Frontend\CartController@delItemCart');
    Route::get('del_all_item','Frontend\CartController@delAllItemCart');
    Route::get('update_item/{rowId}/{qty}','Frontend\CartController@updateItemCart');
    Route::get('thank-you', 'Frontend\CartController@getComplete')->name('cart.complete');
    Route::post('ajax-add-cart', 'Frontend\CartController@ajaxAddCart')->name('cart.ajaxAddCart');
});

Route::group(['prefix' => 'thanh-vien'], function () {
    Route::get('', 'Frontend\HomeController@listallMember');
    Route::get('{role_name}', 'Frontend\HomeController@listMember');
});

Route::get('{slug1}', 'Frontend\CourseController@oneParam');
Route::get('{slug1}/{slug2}', 'Frontend\CourseController@twoParam');
Route::get('{slug1}/{slug2}/{slug3}', 'Frontend\CourseController@threeParam');
Route::get('{slug1}/{slug2}/{slug3}/{slug4}', 'Frontend\CourseController@fourParam');

Route::get('', 'Frontend\HomeController@getHome');

//  Dang nhap fb - gg
Route::get('/login/{param}/redirect/', 'Frontend\StudentController@redirect')->name('auth_redirect');//dang nhap
Route::get('/login/{param}/callback/', 'Frontend\StudentController@callback');