<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace Modules\ThemeRaoVat\Models ;

use App\Models\District;
use App\Models\Province;
use App\Models\Ward;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model {

    protected $table = 'posts';

    use SoftDeletes;


    protected $fillable = [
        'name_vi', 'slug', 'user_id', 'intro', 'content', 'status', 'image', 'category_id', 'video', 'order_no',
        'important', 'data','type','day','show_home','image_present'
    ];

    protected $appends = ['category', 'category_child'];

    public function admin()
    {
        return $this->belongsTo(Admin::class,'admin_id');
    }

    //    bài viết liên quan
    public function getCategoryPost()
    {
        $cat_ids = explode('|', @$this->attributes['multi_cat']);
        return Category::whereIn('id', $cat_ids)->first();
    }

    public function getCategoryChildAttribute() {
        $cat_ids = explode('|', @$this->attributes['category_child_id']);
        return Category::whereIn('id', $cat_ids)->first();
    }

    public function getCategoryAttribute() {
        $cat_ids = explode('|', @$this->attributes['multi_cat']);
        return Category::whereIn('id', $cat_ids)->first();
    }

    public function province() {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function district() {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function ward() {
        return $this->belongsTo(Ward::class, 'ward_id');
    }
}
