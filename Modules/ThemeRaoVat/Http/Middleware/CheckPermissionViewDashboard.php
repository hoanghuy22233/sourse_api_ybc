<?php

namespace Modules\ThemeRaoVat\Http\Middleware;

use App\Http\Helpers\CommonHelper;
use Closure;

class CheckPermissionViewDashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'dashboard_view')) {
                return redirect('/admin/post');
            }

            return $next($request);

        } catch (\Exception $ex) {
            return response()->json([
                'result'  => false,
                'message' => $ex->getMessage()
            ]);
        }
    }
}
