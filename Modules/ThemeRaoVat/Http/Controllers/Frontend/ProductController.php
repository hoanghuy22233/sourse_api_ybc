<?php

namespace Modules\ThemeRaoVat\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Modules\ThemeRaoVat\Models\Product;

class ProductController extends Controller
{
//      Trang chi tiết bài viết
    public function detail($slug1, $slug)
    {
        $slug = str_replace('.html', '', $slug);

//        $data['product'] = CommonHelper::getFromCache('product_slug');
//        if (!$data['product']) {
            $data['product'] = Product::where('slug', $slug)->first();
//            CommonHelper::putToCache('product_slug', $data['product']);
//        }


        if (!is_object($data['product'])) {
            abort(404);
        }
        
        $data['product']->view_total = $data['product']->view_total == null ? 1 : $data['product']->view_total++;
        $data['product']->save();
        
        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['product']->meta_title != '' ? $data['product']->meta_title : $data['product']->name,
            'meta_description' => $data['product']->meta_description != '' ? $data['product']->meta_description : $data['product']->name,
            'meta_keywords' => $data['product']->meta_keywords != '' ? $data['product']->meta_keywords : $data['product']->name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themeraovat::pages.product.detail', $data);
    }
}
