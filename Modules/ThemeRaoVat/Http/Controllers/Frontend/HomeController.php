<?php

namespace Modules\ThemeRaoVat\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Modules\ThemeRaoVat\Models\RutGonLink;
use Modules\ThemeRaoVat\Models\RutGonLinkLog;
use Session;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    function getHome()
    {

        $data['user'] = \Auth::guard('student')->user();
        return view('themeraovat::pages.home.home', $data);
    }

    public function listMember($role_name)
    {
        $admins = Admin::leftJoin('role_admin', 'role_admin.admin_id', '=', 'admin.id')
            ->leftJoin('roles', 'roles.id', '=', 'role_admin.role_id')
            ->where('roles.name', $role_name)
            ->selectRaw('admin.id, admin.email, admin.name, admin.image, admin.tel, admin.intro, roles.display_name as role_name')->paginate(30);

        $data['admins'] = $admins;
//        dd($admins);

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Danh sách thành viên ' . @$admins[0]->role_name,
            'meta_description' => 'Danh sách thành viên' . @$admins[0]->role_name,
            'meta_keywords' => 'Danh sách thành viên' . @$admins[0]->role_name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themeraovat::pages.admin.list_by_role')->with($data);
    }

    public function listallMember()
    {
        $admins = Admin::leftJoin('role_admin', 'role_admin.admin_id', '=', 'admin.id')
            ->leftJoin('roles', 'roles.id', '=', 'role_admin.role_id')
            ->selectRaw('admin.id, admin.email, admin.name, admin.image, admin.tel, admin.intro, roles.display_name as role_name')->paginate(30);
        $data['admins'] = $admins;
//        dd($admins);

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Danh sách thành viên ' . @$admins[0]->role_name,
            'meta_description' => 'Danh sách thành viên' . @$admins[0]->role_name,
            'meta_keywords' => 'Danh sách thành viên' . @$admins[0]->role_name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themeraovat::pages.admin.list_by_role')->with($data);
    }

    public function rutGonLink(request $r) {
        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Rút gọn link',
            'meta_description' => 'Rút gọn link website, thống kê truy cập link',
            'meta_keywords' => 'Rút gọn link',
        ];
        view()->share('pageOption', $pageOption);

        $data = [];
        if ($r->has('link') && $r->link != null) {
            $link = RutGonLink::where('link', $r->link)->first();
            if (!is_object($link)) {
                $link = new RutGonLink();
                $link->admin_id = @\Auth::guard('admin')->user()->id;
                $link->link = $r->link;
                $link->save();

                $link->result = base64_encode($link->id);
                $link->save();
            }
            $data['link'] = $link;
        }

        return view('themeraovat::pages.home.rut_gon_link')->with($data);
    }

    public function rutGonLinkDirect(request $r) {
        if ($r->has('result')) {
            $link = RutGonLink::where('result', $r->result)->first();
            if (is_object($link)) {
                //  Tạo log
                $rut_gon_link_log = new RutGonLinkLog();
                $rut_gon_link_log->ip = $r->get('ip', null);
                $rut_gon_link_log->from = $r->get('from', null);
                $rut_gon_link_log->rut_gon_link_id = $link->id;
                $rut_gon_link_log->save();

                return redirect($link->link);
            }
        }
        return redirect('/');
    }
}
