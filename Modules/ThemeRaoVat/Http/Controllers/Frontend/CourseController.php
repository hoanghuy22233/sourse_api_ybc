<?php

namespace Modules\ThemeRaoVat\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Error;
use Illuminate\Http\Request;
use Mail;
use Modules\EduCourse\Models\LessonItem;
use Modules\EduCourse\Models\Quizzes;
use Modules\ThemeRaoVat\Models\Category;
use Modules\ThemeRaoVat\Models\Course;
use Modules\ThemeRaoVat\Models\Note;
use Modules\ThemeRaoVat\Models\Post;
use Modules\ThemeRaoVat\Models\Product;
use Modules\ThemeRaoVat\Models\RutGonLink;
use Modules\ThemeRaoVat\Models\RutGonLinkLog;
use Session;
use Validator;
use \Modules\ThemeRaoVat\Models\Order;

class CourseController extends Controller
{
    public function list($slug1, $slug, $r = false)
    {
        //  Danh mục cha
        if ($slug == null) {
            return $this->viewParentCategory($slug1, $r);
        }

        $category = Category::where('slug', $slug)->first();
        if (!is_object($category)) {
            abort(404);
        }
        $data['category'] = $category;
        $data['slug1'] = $slug;

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $category->meta_title != '' ? $category->meta_title : $category->name,
            'meta_description' => $category->meta_description != '' ? $category->meta_description : $category->name,
            'meta_keywords' => $category->meta_keywords != '' ? $category->meta_keywords : $category->name,
        ];
        view()->share('pageOption', $pageOption);

        if ($slug1 == 'ban-buon-ban-si') {
            //  DS Sản phẩm
            $data['products'] = CommonHelper::getFromCache('products_multi_category_id' . $slug1 . $category->id . ($r ? base64_encode(json_encode($r->all())) : ''), ['products']);
            if (!$data['products']) {
                $data['products'] = @Product::where(function ($query) use ($category) {
                    $query->orWhere('multi_cat', 'like', '%|' . @$category->id . '|%');
                    $query->orWhere('category_child_id', 'like', '%|' . @$category->id . '|%');
                });

                $data['products'] = $data['products']->orderBy('order_no', 'desc')->orderBy('created_at', 'desc')->paginate(20);

                CommonHelper::putToCache('products_multi_category_id' . $slug1 . $category->id . ($r ? base64_encode(json_encode($r->all())) : ''), $data['products'], ['products']);
            }
            $data['post_type'] = 3;
            return view('themeraovat::pages.product.list')->with($data);
        } else {
            //  DS Tin tức
            $data['posts'] = CommonHelper::getFromCache('posts_multi_category_id' . $slug1 . $category->id . ($r ? base64_encode(json_encode($r->all())) : ''), ['posts']);
            if (!$data['posts']) {
                $data['posts'] = @Post::where(function ($query) use ($category) {
                    $query->orWhere('multi_cat', 'like', '%|' . @$category->id . '|%');
                    $query->orWhere('category_child_id', 'like', '%|' . @$category->id . '|%');
                });

                if ($slug1 == 'tim-nha-phan-phoi') {
                    $data['posts'] = $data['posts']->where('post_type', 1);
                } elseif ($slug1 == 'nhan-mo-dai-ly') {
                    $data['posts'] = $data['posts']->where('post_type', 2);
                }

                $data['posts'] = $data['posts']->orderBy('order_no', 'desc')->orderBy('created_at', 'desc')->paginate(20);

                CommonHelper::putToCache('posts_multi_category_id' . $slug1 . $category->id . ($r ? base64_encode(json_encode($r->all())) : ''), $data['posts'], ['posts']);
            }
            if ($slug1 == 'tim-nha-phan-phoi') {
                $data['post_type'] = 1;
            } elseif ($slug1 == 'nhan-mo-dai-ly') {
                $data['post_type'] = 2;
            }
            return view('themeraovat::pages.post.list')->with($data);
        }
    }

    public function viewParentCategory($slug, $r) {
        if ($slug == 'tim-nha-phan-phoi') {
            $data['posts'] = CommonHelper::getFromCache('posts_multi_category_slug' . $slug . ($r ? base64_encode(json_encode($r->all())) : ''), ['posts']);
            if (!$data['posts']) {
                $data['posts'] = @Post::whereRaw('1=1');

                $data['posts'] = $data['posts']->where('post_type', 1);

                $data['posts'] = $data['posts']->orderBy('order_no', 'desc')->orderBy('created_at', 'desc')->paginate(20);

                CommonHelper::putToCache('posts_multi_category_slug' . $slug . ($r ? base64_encode(json_encode($r->all())) : ''), $data['posts'], ['posts']);
            }
            $data['post_type'] = 1;

            //  Thẻ meta cho seo
            $pageOption = [
                'meta_title' => 'Tìm đại lý, Tìm nhà phân phối',
                'meta_description' => 'Tìm đại lý, Tìm nhà phân phối',
                'meta_keywords' => 'Tìm đại lý, Tìm nhà phân phối',
            ];
            view()->share('pageOption', $pageOption);

            return view('themeraovat::pages.post.list')->with($data);

        } elseif ($slug == 'nhan-mo-dai-ly') {
            $data['posts'] = CommonHelper::getFromCache('posts_multi_category_slug' . $slug . ($r ? base64_encode(json_encode($r->all())) : ''), ['posts']);
            if (!$data['posts']) {
                $data['posts'] = @Post::whereRaw('1=1');

                $data['posts'] = $data['posts']->where('post_type', 2);

                $data['posts'] = $data['posts']->orderBy('order_no', 'desc')->orderBy('created_at', 'desc')->paginate(20);

                CommonHelper::putToCache('posts_multi_category_slug' . $slug . ($r ? base64_encode(json_encode($r->all())) : ''), $data['posts'], ['posts']);
            }
            $data['post_type'] = 2;

            //  Thẻ meta cho seo
            $pageOption = [
                'meta_title' => 'Cần tìm nguồn hàng, Nhận mở đại lý phân phối',
                'meta_description' => 'Cần tìm nguồn hàng, Nhận mở đại lý phân phối',
                'meta_keywords' => 'Cần tìm nguồn hàng, Nhận mở đại lý phân phối',
            ];
            view()->share('pageOption', $pageOption);

            return view('themeraovat::pages.post.list')->with($data);
        } elseif ($slug == 'ban-buon-ban-si') {
            $data['products'] = CommonHelper::getFromCache('products_multi_category_slug' . $slug . ($r ? base64_encode(json_encode($r->all())) : ''), ['products']);
            if (!$data['products']) {
                $data['products'] = @Product::whereRaw('1=1');

                $data['products'] = $data['products']->orderBy('order_no', 'desc')->orderBy('created_at', 'desc')->paginate(20);

                CommonHelper::putToCache('products_multi_category_slug' . $slug . ($r ? base64_encode(json_encode($r->all())) : ''), $data['products'], ['products']);
            }
            $data['post_type'] = 3;

            //  Thẻ meta cho seo
            $pageOption = [
                'meta_title' => 'Bán buôn THỰC PHẨM, Bán sỉ giá rẻ',
                'meta_description' => 'Bán buôn THỰC PHẨM, Bán sỉ giá rẻ',
                'meta_keywords' => 'Bán buôn THỰC PHẨM, Bán sỉ giá rẻ',
            ];
            view()->share('pageOption', $pageOption);

            return view('themeraovat::pages.product.list')->with($data);
        } else {
            $category = Category::where('slug', $slug)->first();
            if (!is_object($category)) {
                abort(404);
//                return $this->rutGonLink($slug);
            }
            $data['category'] = $category;

            //  Thẻ meta cho seo
            $pageOption = [
                'meta_title' => $category->meta_title != '' ? $category->meta_title : $category->name,
                'meta_description' => $category->meta_description != '' ? $category->meta_description : $category->name,
                'meta_keywords' => $category->meta_keywords != '' ? $category->meta_keywords : $category->name,
            ];
            view()->share('pageOption', $pageOption);

            //  DS Tin tức
            $data['posts'] = CommonHelper::getFromCache('posts_admin_multi_category_id' . $category->id . ($r ? base64_encode(json_encode($r->all())) : ''), ['posts']);
            if (!$data['posts']) {
                $data['posts'] = @Post::where(function ($query) use ($category) {
                    $query->orWhere('multi_cat', 'like', '%|' . @$category->id . '|%');
                    foreach ($category->childs as $child) {
                        $query->orWhere('multi_cat', 'like', '%|' . @$child->id . '|%');
                    }
                });

                $data['posts'] = $data['posts']->orderBy('order_no', 'desc')->orderBy('id', 'desc')->paginate(20);

                CommonHelper::putToCache('posts_admin_multi_category_id' . $category->id . ($r ? base64_encode(json_encode($r->all())) : ''), $data['posts'], ['posts']);
            }
            return view('themeraovat::pages.post_admin.list')->with($data);
        }
    }

    public function rutGonLink($slug) {
        $link = RutGonLink::where('result', $slug)->first();
        if (is_object($link)) {
            //  Tạo log
            $rut_gon_link_log = new RutGonLinkLog();
            $rut_gon_link_log->ip = @$_SERVER['REMOTE_ADDR'];
            $rut_gon_link_log->from = @$_SERVER['HTTP_REFERER'];
            $rut_gon_link_log->rut_gon_link_id = $link->id;
            $rut_gon_link_log->save();

            return redirect($link->link);
        }
        abort(404);
    }

    public function oneParam(request $r, $slug1)
    {
        //  Chuyen den trang danh sanh tin tuc | Danh sach san pham
        return $this->list($slug1, null, $r);
    }

    //  VD: hobasoft/card-visit  | danh-muc-cha/danh-muc-con | danh-muc/bai-viet.html | danh-muc/san-pham.html
    public function twoParam(request $r, $slug1, $slug2)
    {

        if (strpos($slug2, '.html')) {  //  Nếu là trang chi tiết
            $slug2 = str_replace('.html', '', $slug2);
            if (Product::where('slug', $slug2)->where('status', 1)->count() > 0) {        //  Chi tiet khóa học
                $productController = new ProductController();
                return $productController->detail($slug1, $slug2);
            } else {
                $postController = new PostController();
                return $postController->detail($slug1, $slug2);
            }
        }
        return $this->list($slug1, $slug2, $r);
    }

    //  VD: hobasoft/card-visit/card-truyen-thong
    public function threeParam($slug1, $slug2, $slug3)
    {
        if (strpos($slug3, '.html')) {  //  Nếu là trang chi tiết
            $slug3 = str_replace('.html', '', $slug3);
            if (Course::where('slug', $slug3)->where('status', 1)->count() > 0) {        //  Chi tiet san pham
                return $this->detail($slug3);
            } elseif (\Modules\ThemeRaoVat\Models\LessonItem::where('slug', $slug3)->count() > 0) {//Chi tiet video
                return $this->lesson_item_detail($slug3);
            } else {        //  Chi tiet tin tuc
                $postController = new PostController();
                return $postController->detail($slug3);
            }
        }
        return $this->list($slug3);
    }

    public function fourParam($slug1, $slug2, $slug3, $slug4)
    {
        if (strpos($slug4, '.html')) {  //  Nếu là trang chi tiết
            $slug4 = str_replace('.html', '', $slug4);
            if (Course::where('slug', $slug4)->where('status', 1)->count() > 0) {        //  Chi tiet san pham
                return $this->detail($slug4);
            } else {        //  Chi tiet tin tuc
                $postController = new PostController();
                return $postController->detail($slug4);
            }
        }
        abort(404);
    }

    //  Trang chi tiết khóa học
    public function detail($slug1, $slug)
    {
        $data['user'] = \Auth::guard('student')->user();
        $slug = str_replace('.html', '', $slug);
        $data['course'] = CommonHelper::getFromCache('course_slug' . $slug, ['courses']);
        if (!$data['course']) {
            $data['course'] = Course::where('slug', $slug)->where('status', 1)->first();
            CommonHelper::putToCache('course_slug' . $slug, $data['course'], ['courses']);
        }

        if (!is_object($data['course'])) {
            abort(404);
        }
        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['course']->meta_title != '' ? $data['course']->meta_title : $data['course']->name,
            'meta_description' => $data['course']->meta_description != '' ? $data['course']->meta_description : $data['course']->name,
            'meta_keywords' => $data['course']->meta_keywords != '' ? $data['course']->meta_keywords : $data['course']->name,
        ];
        view()->share('pageOption', $pageOption);

        if ($slug1 == 'mau-kinh-doanh') {
            if ($data['course']->final_price == 0) {
                return view('themeraovat::pages.business_template.free.detail', $data);
            } else {
                return view('themeraovat::pages.business_template.pay.detail', $data);
            }
        }

        return view('themeraovat::pages.course.list_lesson', $data);
    }

    //  Trang chi tiet video bai hoc
    public function lesson_item_detail($slug)
    {
        $data['lesson_item'] = CommonHelper::getFromCache('lesson_items_slug' . $slug, ['lesson_items']);
        if (!$data['lesson_item']) {
            $data['lesson_item'] = LessonItem::where('slug', $slug)->first();
            CommonHelper::putToCache('lesson_items_slug' . $slug, $data['lesson_item'], ['lesson_items']);
        }
        if (!is_object($data['lesson_item'])) {
            abort(404);
        }

        //  Kiểm tra nếu user chưa mua khóa này hoặc khóa này ko cho học thử sẽ bắn quay lại trang chủ
        $order = Order::where('student_id', @\Auth::guard('student')->user()->id)->where('course_id', $data['lesson_item']->course_id)->first();
        if ($data['lesson_item']->publish != 1 && @$order->status != 1) {
            CommonHelper::one_time_message('error', 'Bạn chưa mua bài học này!');
            return back();
        }

        $data['courses'] = Course::findOrFail($data['lesson_item']->course_id);
        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['lesson_item']->meta_title != '' ? $data['lesson_item']->meta_title : $data['lesson_item']->name,
            'meta_description' => $data['lesson_item']->meta_description != '' ? $data['lesson_item']->meta_description : $data['lesson_item']->name,
            'meta_keywords' => $data['lesson_item']->meta_keywords != '' ? $data['lesson_item']->meta_keywords : $data['lesson_item']->name,
        ];
        view()->share('pageOption', $pageOption);

        if ($data['courses']->type == 'Video') {
            return view('themeraovat::pages.course.video.detail', $data);
        }
        return view('themeraovat::pages.course.text.course_detail', $data);
    }

    public function lesson_detail($slug1, $slug)
    {
        $data['user'] = \Auth::guard('student')->user();
        $slug = str_replace('.html', '', $slug);
        $data['lessonitem'] = CommonHelper::getFromCache('lesson_item_slug' . $slug, ['lesson_items']);
        if (!$data['lessonitem']) {
            $data['lessonitem'] = LessonItem::where('slug', $slug)->first();

            CommonHelper::putToCache('lesson_item_slug' . $slug, $data['lessonitem'], ['lesson_items']);
        }

        $data['slug1'] = $slug1;
        if (!is_object($data['lessonitem'])) {
            abort(404);
        }

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['lessonitem']->meta_title != '' ? $data['lessonitem']->meta_title : $data['lessonitem']->name,
            'meta_description' => $data['lessonitem']->meta_description != '' ? $data['lessonitem']->meta_description : $data['lessonitem']->name,
            'meta_keywords' => $data['lessonitem']->meta_keywords != '' ? $data['lessonitem']->meta_keywords : $data['lessonitem']->name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themeraovat::pages.course.text.course_detail', $data);
    }

    public function getSearch(request $r)
    {
        $data['user'] = \Auth::guard('student')->user();

//        if (!$data['courses']) {
//            $data['courses'] = Course::where('name', 'like', '%' . $r->q . '%')->where('status', 1)->orderBy('created_at', 'desc')->paginate(8);
//            CommonHelper::putToCache('courses_name_like' . $r->q, $data['courses']);
//        }

        $pageOption = [
            'meta_title' => 'Tìm kiếm từ khóa: ' . @$r->keyword,
            'meta_description' => 'Tìm kiếm từ khóa: ' . @$r->keyword,
            'meta_keywords' => 'Tìm kiếm từ khóa: ' . @$r->keyword,
        ];
        view()->share('pageOption', $pageOption);
        $data['keyword'] = $r->keyword;

        if (@$r->post_type == '3') {
            //  Tìm sản phẩm
            $items = Product::where('status', 1);

        } else {
            //  Tìm bài viết
            $items = Post::where('status', 1);

            if ($r->has('post_type') && $r->post_type != null) {
                $items = $items->where('post_type', $r->post_type);
            }
        }

        if ($r->has('user_id') && $r->user_id != null) {
            $items = $items->where('admin_id', $r->user_id);
        }

        if ($r->has('tag') && $r->tag != null) {
            $items = $items->where('tags', 'like', '%' . $r->tag . '%');
        }

        if ($r->has('keyword') && $r->keyword != null) {
            $items = $items->where('name', 'like', '%' . $r->keyword . '%');
        }

        if ($r->has('category_id') && $r->category_id != null) {
            $items = $items->where('multi_cat', 'like', '%|' . $r->category_id . '|%');
        }

        if ($r->has('category_child_id') && $r->category_child_id != null) {
            $items = $items->where('category_child_id', 'like', '%|' . $r->category_child_id . '|%');
        }

        if ($r->has('province_id') && $r->province_id != null) {
            $items = $items->where('province_id', $r->province_id);
        }

        /*if ($r->has('price_range') && $r->price_range != null) {
            $items = $items->where('price_range', $r->price_range);
        }

        if ($r->has('acreage') && $r->acreage != null) {
            $items = $items->where('acreage', $r->acreage);
        }*/

        $items = $items->orderBy('created_at', 'desc')->paginate(16);
        $data['post_type'] = @$r->post_type;

        if (@$r->post_type == '3') {
            //  Tìm sản phẩm
            $data['products'] = $items;
            return view('themeraovat::pages.product.list', $data);
        } else {
            //  Tìm bài viết
            $data['posts'] = $items;
            return view('themeraovat::pages.post.list', $data);
        }
    }

    function getDetail(request $r)
    {
        return view('themeraovat::pages.course.detail');
    }

    public function lichHoc()
    {
        $data['user'] = \Auth::guard('student')->user();
        $pageOption = [
            'meta_title' => 'Lịch học',
            'meta_description' => 'Lịch học, lịch dạy của trung tâm',
            'meta_keywords' => 'Lịch học, lịch dạy',
        ];
        view()->share('pageOption', $pageOption);
        return view('themeraovat::pages.calendar.google_iframe', $data);
    }

//    function Error(request $r)
//    {
//        $error = new Error();
//        $error->module = 'ThemeRaoVat';
//        $error->message ='Tiết học:'. $r->lesson_item_id.'Học viên:'. @\Auth::guard('student')->user()->id;
//        $error->code = 'mã';
//        $error->file = 'thư mục';
//        $error->save();
//        \App\Http\Helpers\CommonHelper::flushCache($this->module['table_name']);
//        return response()->json([
//            'status' => true,
//            'msg' => 'Thông báo lỗi thành công'
//        ]);
//    }

    function Note(request $r)
    {
        $note = new Note();
        $note->student_id = @\Auth::guard('student')->user()->id;
        $note->lesson_item_id = @$r->lesson_item_id;
        $note->note = @$r->message;
        $note->save();
        \App\Http\Helpers\CommonHelper::flushCache($this->module['table_name']);
        return response()->json([
            'status' => true,
            'msg' => 'Lưu thành công'
        ]);
    }
}
