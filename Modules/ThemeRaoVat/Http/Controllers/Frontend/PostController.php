<?php

namespace Modules\ThemeRaoVat\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeRaoVat\Models\Menus;
use Modules\ThemeRaoVat\Models\Post;

class PostController extends Controller
{
//      Trang chi tiết bài viết
    public function detail($slug1, $slug)
    {
        $data['user'] = \Auth::guard('student')->user();
        $slug = str_replace('.html', '', $slug);

//        $data['post'] = CommonHelper::getFromCache('post_slug');
//        if (!$data['post']) {
            $data['post'] = Post::where('slug', $slug)->first();
//            CommonHelper::putToCache('post_slug', $data['post']);
//        }

        if (!is_object($data['post'])) {
            abort(404);
        }

        $data['post']->view_total = $data['post']->view_total == null ? 1 : $data['post']->view_total++;
        $data['post']->save();

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
            'meta_description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->name,
            'meta_keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themeraovat::pages.post.detail', $data);
    }
}
