@if($item->status == 0)
    <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" data-id="{{ $item->id }}"
          data-column="{{ $field['name'] }}">Admin khóa</span>
@elseif(@\App\Models\RoleAdmin::where('admin_id', $item->id)
 //->where('company_id', \Auth::guard('admin')->user()->last_company_id)
->first()->status == 1)
    <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill" data-id="{{ $item->id }}"
          data-column="{{ $field['name'] }}">Kích hoạt</span>
@else
    <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" data-id="{{ $item->id }}"
          data-column="{{ $field['name'] }}">Khóa</span>
@endif