<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'admins', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\AdminController@index')->middleware('api_permission:admin_company_view');
    });
    Route::group(['prefix' => 'admin/profile', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::match(array('GET', 'POST'), '{id}', 'Admin\AdminController@profileAdmin')->name('admin.profile_admin');
//        Route::match(array('GET', 'POST'), '{id}', function (){
//            dd(1);
//        })->name('admin.profile_admin');
    });
});
