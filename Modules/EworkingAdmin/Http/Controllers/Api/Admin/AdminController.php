<?php

namespace Modules\EworkingAdmin\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\RoleAdmin;
use Illuminate\Http\Request;
use Modules\EworkingAdmin\Models\Admin;

class AdminController extends Controller
{

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'email' => [
            'query_type' => 'like'
        ],
    ];

    public function getProfile()
    {
        try {
            return response()->json([
                'status' => true,
                'msg' => 'Thành công',
                'errors' => (object)[],
                'data' => [
                    'id' => Auth::guard('api')->user()->id,
                    'email' => Auth::guard('api')->user()->email,
                    'name' => Auth::guard('api')->user()->name,
                    'tel' => Auth::guard('api')->user()->tel,
                    'address' => Auth::guard('api')->user()->address,
                    'image' => asset('public/filemanager/userfiles/' . Auth::guard('api')->user()->image),
                    'gender' => Auth::guard('api')->user()->gender,
                    'birthday' => Auth::guard('api')->user()->birthday,
                    'api_token' => Auth::guard('api')->user()->api_token,
                ],
                'code' => 201
            ]);

        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lấy dữ liệu thất bại',
                'errors' => [
                    'exception' => [
                        'Người dùng không tồn tại'
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }

    }

    public function index(Request $request)
    {
        try {
            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'admin_company_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Admin::whereRaw($where)
                ->where('status', 1);

            if ($request->has('company_id')) {
                $admin_ids = RoleAdmin::where('company_id', $request->company_id)->where('status', 1)->pluck('admin_id')->toArray();
                $listItem = $listItem->whereIn('id', $admin_ids);
            }

            //  Sort
            $listItem = $this->sort($request, $listItem);
            $listItem = $listItem->paginate(20)->appends($request->all());
//
            foreach ($listItem as $k => $item) {
//                dd();
                $item->role_name = @RoleAdmin::join('roles', 'roles.id', '=', 'role_admin.role_id')
                    ->select(['roles.name', 'roles.display_name'])->where('role_admin.admin_id', $item->id)
                    ->where('roles.company_id', $request->company_id)->first()->display_name;
//                $item->role_name = @$item->roles[0]->display_name ? '';
                $item->admin_id = $item->id;
                $item->image = asset('public/filemanager/userfiles/' . $item->image);

                unset($item->password_md5);
                unset($item->change_password);
                unset($item->api_token);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }
    public function ProfileAdmin($id)
    {
        try {
            $admin = Admin::where('id',(int)$id)->first();
            if (!is_object($admin)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            return response()->json([
                'status' => true,
                'msg' => 'Thành công',
                'errors' => (object)[],
                'data' => [
                    'id' => $admin->id,
                    'admin_id' => $admin->id,
                    'email' => $admin->email,
                    'name' => $admin->name,
                    'tel' => $admin->tel,
                    'address' => $admin->address,
                    'image' => asset('public/filemanager/userfiles/' . $admin->image),
                    'gender' => $admin->gender,
                    'birthday' => $admin->birthday,
                ],
                'code' => 201
            ]);

        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lấy dữ liệu thất bại',
                'errors' => [
                    'exception' => [
                        'Người dùng không tồn tại'
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }
//    public function Detail($id) {
//
//        try {
//            //  Check permission
//            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'company_view')) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Lỗi',
//                    'errors' => [
//                        'exception' => [
//                            'Không đủ quyền'
//                        ]
//                    ],
//                    'data' => null,
//                    'code' => 403
//                ]);
//            }
//
//            $item = Job::join('companies', 'companies.id', '=', 'jobs.company_id')
//                ->join('internal_notifications', 'internal_notifications.company_id', '=', 'companies.id')
//                ->selectRaw('companies.id as company_id, companies.name as company_name,
//                             jobs.name as jobs_name, jobs.id as jobs_id,
//                             internal_notifications.title as internal_notifications_title,  internal_notifications.content as internal_notifications_content,  internal_notifications.id as internal_notifications_id')
//                ->where('jobs.company_id',$id)
//                ->where('jobs.status',1)
//                ->whereNotNull('jobs.name')
//                ->whereDate('internal_notifications.deadline','<=',date('Y-m-d'))
//                ->get();
//
//
//            foreach ($item as $k => $v) {
//                $job_end_date = @Task::whereNotNull('job_id')->where('job_id',$v->jobs_id)->whereNotNull('end_date')->orderBy('end_date','desc')->first()->end_date;
//                $days = (strtotime($job_end_date) - strtotime(date('Y-m-d'))) / (60 * 60 * 24);
//                $bg_deadline='';
//                if ($days < 0) {
//                    $bg_deadline = '#fff';
//                } elseif ($days == 0) {
//                    $bg_deadline = 'Tomato';
//                } elseif ($days > 0 && $days <= 2) {
//                    $bg_deadline = 'LightSalmon';
//                } elseif ($days > 2 && $days <= 7) {
//                    $bg_deadline = 'MistyRose';
//                } elseif ($days > 0 && $days > 7) {
//                    $bg_deadline = 'SeaShell';
//                }
//                $v->company = [
//                    'id' => $v->company_id,
//                    'name' => $v->company_name,
//                ];
//                $v->jobs = [
//                    'id' => $v->jobs_id,
//                    'name' => $v->jobs_name,
//                    'job_color' => $bg_deadline,
//                    'end_date' => $job_end_date,
//                ];
//
////                Tin tuc
//                $v->internal_notifications = [
//                    'id' => $v->internal_notifications_id,
//                    'title' => $v->internal_notifications_title,
//                    'content' => $v->internal_notifications_content,
//                ];
//                unset($v->company_id);
//                unset($v->company_name);
//
//                unset($v->jobs_id);
//                unset($v->jobs_name);
//
//                unset($v->internal_notifications_title);
//                unset($v->internal_notifications_content);
//            }
//            if (!is_object($item)) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Lỗi',
//                    'errors' => [
//                        'exception' => [
//                            'Không tìm thấy bản ghi'
//                        ]
//                    ],
//                    'data' => null,
//                    'code' => 404
//                ]);
//            }
//
//            return response()->json([
//                'status' => true,
//                'msg' => '',
//                'errors' => (object)[],
//                'data' => $item,
//                'code' => 201
//            ]);
//        } catch (\Exception $ex) {
//            return response()->json([
//                'status' => false,
//                'msg' => 'Lỗi',
//                'errors' => [
//                    'exception' => [
//                        $ex->getMessage()
//                    ]
//                ],
//                'data' => null,
//                'code' => 401
//            ]);
//        }
//    }
    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
