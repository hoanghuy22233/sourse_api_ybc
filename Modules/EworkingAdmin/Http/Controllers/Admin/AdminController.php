<?php

namespace Modules\EworkingAdmin\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use App\Models\RoleAdmin;
use App\Models\Roles;
use Auth;
use Eventy;
use Illuminate\Http\Request;
use Modules\EworkingAdmin\Models\Admin;
use Modules\EworkingCompany\Http\Controllers\Admin\CompanyController;
use Modules\EworkingCompany\Models\Company;
use Modules\EworkingInviteCompany\Models\InviteHistory;
use Modules\EworkingJob\Http\Controllers\Admin\JobTypeController;
use Modules\EworkingRole\Http\Controllers\Admin\RoleController;
use Validator;

class AdminController extends CURDBaseController
{

    protected $_role;
    protected $orderByRaw = 'status desc, id desc';

    public function __construct()
    {
        parent::__construct();
        $this->_role = new RoleController();
    }

    protected $module = [
        'code' => 'admin',
        'table_name' => 'admin',
        'label' => 'eworkingadmin::admin.admin',
        'modal' => '\Modules\EworkingAdmin\Models\Admin',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'eworkingadmin::admin.image', 'sort' => true],
            ['name' => 'name', 'type' => 'custom', 'td' => 'eworkingadmin::td.name', 'label' => 'eworkingadmin::admin.name', 'sort' => true],
            ['name' => 'role_id', 'type' => 'role_name', 'label' => 'eworkingadmin::admin.role'],
            ['name' => 'tel', 'type' => 'text', 'label' => 'eworkingadmin::admin.phone', 'sort' => true],
            ['name' => 'email', 'type' => 'text', 'label' => 'eworkingadmin::admin.email', 'sort' => true],
            ['name' => 'status', 'type' => 'custom', 'td' => 'eworkingadmin::td.status_company', 'label' => 'eworkingadmin::admin.status', 'sort' => true],

        ],
        'list_all' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'eworkingadmin::admin.image'],
            ['name' => 'name', 'type' => 'custom', 'td' => 'eworkingadmin::td.name', 'label' => 'eworkingadmin::admin.name'],
            ['name' => 'tel', 'type' => 'text', 'label' => 'eworkingadmin::admin.phone'],
            ['name' => 'email', 'type' => 'text', 'label' => 'eworkingadmin::admin.email'],

            ['name' => 'status', 'type' => 'status', 'label' => 'eworkingadmin::admin.status'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Họ & tên'],
                ['name' => 'email', 'type' => 'text', 'class' => 'required', 'label' => 'Email'],
                ['name' => 'tel', 'type' => 'text', 'label' => 'Số điện thoại'],
                ['name' => 'password', 'type' => 'password', 'class' => 'required', 'label' => 'Mật khẩu'],
                ['name' => 'password_confimation', 'type' => 'password', 'class' => 'required', 'label' => 'Nhập lại mật khẩu'],
            ],
        ]
    ];

    protected $filter = [
        'name' => [
            'label' => 'eworkingadmin::admin.name',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'tel' => [
            'label' => 'eworkingadmin::admin.phone',
            'type' => 'number',
            'query_type' => '='
        ],
        'email' => [
            'label' => 'eworkingadmin::admin.email',
            'type' => 'number',
            'query_type' => '='
        ],
        'status' => [
            'label' => 'eworkingadmin::admin.status',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'eworkingadmin::admin.status',
                1 => 'eworkingadmin::admin.active',
                0 => 'eworkingadmin::admin.no_active',
            ]
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('eworkingadmin::list')->with($data);
    }

    public function all(Request $request)
    {
        $data = $this->getDataListAll($request);

        return view('eworkingadmin::all')->with($data);
    }

    public function getDataListAll(Request $request)
    {
        //  Filter
        $where = $this->filterSimple($request);
        $listItem = $this->model->whereRaw($where);
        if ($this->whereRaw) {
            $listItem = $listItem->whereRaw($this->whereRaw);
        }

        //  Export
        if ($request->has('export')) {
            $this->exportExcel($request, $listItem->get());
        }

        //  Sort
        $listItem = $this->sort($request, $listItem);
        if ($request->has('limit')) {
            $data['listItem'] = $listItem->paginate($request->limit);
            $data['limit'] = $request->limit;
        } else {
            $data['listItem'] = $listItem->paginate($this->limit_default);
            $data['limit'] = $this->limit_default;
        }
        $data['page'] = $request->get('page', 1);

        $data['param_url'] = $request->all();

        //  Get data default (param_url, filter, module) for return view
        $data['module'] = $this->module;
        $data['filter'] = $this->filter;
        if ($this->whereRaw) {
            $data['record_total'] = $this->model->whereRaw($this->whereRaw);
        } else {
            $data['record_total'] = $this->model;
        }

        $data['record_total'] = $data['record_total']->whereRaw($where)->count();

        //  Set data for seo
        $data['page_title'] = $this->module['label'];
        $data['page_type'] = 'list';
        return $data;
    }

    public function add(Request $request)
    {
        try {

            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('eworkingadmin::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    $data['status'] = $request->has('status') ? 1 : 0;
                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    $data['admin_id'] = \Auth::guard('admin')->user()->id;

                    if ($request->has('contact_info_key')) {
                        foreach ($request->contact_info_key as $k => $key) {
                            if ($key != null && $request->contact_info_value[$k] != null) {
                                $data['contact_info'][] = [
                                    'key' => $key,
                                    'value' => $request->contact_info_value[$k]
                                ];
                            }
                        }
                    }
                    $data['contact_info'] = json_encode($data['contact_info']);
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }
                    return $this->addReturn($request);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
                return back();
            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('eworkingadmin::edit')->with($data);
            } else if ($_POST) {


                if ($item->id == \Auth::guard('admin')->user()->id) {
                    $validator = Validator::make($request->all(), [
                        'name' => 'required'
                    ], [
                        'name.required' => 'Bắt buộc phải nhập tên',
                    ]);

                    if ($validator->fails()) {
                        return back()->withErrors($validator)->withInput();
                    }
                }
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                //  Tùy chỉnh dữ liệu insert
                $data['status'] = $request->has('status') ? 1 : 0;

                #
                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                return $this->updateReturn($request, $item);
            }
        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {
            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function getCompanyPublish(Request $request)
    {
        try {

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos($item->company_ids, '|' . \Auth::guard('admin')->user()->last_company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Thành viên không thuộc công ty!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            $roleAdmin = RoleAdmin::where('admin_id', $item->id)->where('company_id', \Auth::guard('admin')->user()->last_company_id)->first();
            if (is_object($roleAdmin)) {
                if ($roleAdmin->status == 1) {
                    $this->deActiveAdminFromCompany($item->id, \Auth::guard('admin')->user()->last_company_id);
                } else {
                    $this->activeAdminFromCompany($item, $roleAdmin->role_id);
                }
                return response()->json([
                    'status' => true,
                    'published' => $item->{$request->column} == 1 ? true : false
                ]);
            }
            return response()->json([
                'status' => false,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    //  vd: Giám đốc hủy kích hoạt nhân viên khỏi công ty. 1 => 0
    public function deActiveAdminFromCompany($admin_id, $company_id)
    {
        //  Hủy kích hoạt
        RoleAdmin::where('admin_id', $admin_id)->where('company_id', $company_id)->update([
            'status' => 0
        ]);

        $admin = Admin::find($admin_id);
        $company_ids = str_replace('|' . $company_id . '|', '', $admin->company_ids);
        if ($company_ids == '') {
            $admin->last_company_id = null;
        } else {
            $admin->last_company_id = explode('|', $company_ids)[0];
        }
        $admin->save();

        //  Gửi mail cho thành viên này rằng bạn đã bị hủy kích hoạt khỏi công ty
        try {
            Eventy::action('admin.deactiveFromCompany', [
                'email' => @\Modules\eworkingadmin\Models\Admin::find($admin_id)->email,
                'company_name' => @Company::find($company_id)->short_name,
                'name' => @\Modules\eworkingadmin\Models\Admin::find($admin_id)->name,
            ]);
        } catch (\Exception $ex) {

        }

        return true;
    }

    //  vd: giám đốc kích hoạt tài khoản nhân viên từ 0 => 1
    public function activeAdminFromCompany($admin, $role_id)
    {

        //  Tạo mới lời mời
        $inviteHistory = InviteHistory::create([
            'email' => $admin->email,
            'company_id' => \Auth::guard('admin')->user()->last_company_id,
            'role_id' => $role_id,
            'status' => 0,
            'intro' => 'Công ty ' . Company::find(\Auth::guard('admin')->user()->last_company_id)->short_name . ' mời bạn quay trở lại với vị trí ' . Roles::find($role_id)->display_name,
            'admin_id' => \Auth::guard('admin')->user()->id
        ]);
        $data['link'] = \URL::to('admin/invite_history');
//        $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
//        $data['role_id'] = $role_id;
//        $data['intro'] = $inviteHistory->intro;

        try {
            Eventy::action('inviteCompany.renew', [
                'email' => @$admin->email,
                'name' => @$admin->name,
                'company_name' => @Company::find($inviteHistory->company_id)->short_name,
                'role_name' => @Roles::find($role_id)->name,
                'link' => $data['link']
            ]);
        } catch (\Exception $ex) {

        }

        return true;
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            $this->deleteAdminFromCompany([$item->id]);

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code'] . '/all');
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->deleteAdminFromCompany($ids);
            }
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công!'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

//    Giám đốc xóa nhân viên khỏi công ty
    public function deleteAdminFromCompany($admin_ids)
    {
        //  Xóa company_id khỏi cột companies_id trong bảng admin
        $last_company_ids = Admin::find(\Auth::guard('admin')->user()->id)->last_company_ids;
        foreach ($admin_ids as $id) {
            $user = Admin::find($id);
            $user->company_ids = str_replace($user->company_ids, '|' . $last_company_ids . '|', '|');
            if ($user->company_ids == '|') {
                $user->company_ids = Null;
            }
            $user->save();
        }
        RoleAdmin::whereIn('admin_id', $admin_ids)->where('company_id', \Auth::guard('admin')->user()->last_company_id)->delete();
        return true;
    }

    public function validator(array $data)
    {
        $rules = array(
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|min:6|max:50',
        );

        $fieldNames = array(
            'name' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
        );

        $validator = Validator::make($data, $rules);
        $validator->setAttributeNames($fieldNames);
        return $validator;
    }

    /*
     * Gán người dùng vào 1 công ty
     * Truyền vào: object admin(bản ghi admin đang login), company_id, role_id
     * Trả về: object admin
     * */
    public function assignMembersToCompany($admin, $role_id = false, $company = false, $company_id = false)
    {
        if (!$company) {
            $company = Company::where('id', $company_id)->first();
        }
        $company->admin_id = $admin->id;
        $company->save();


        //  Gán người dùng vừa tạo vào cty mới
        //  Update company_ids cho bang admin
        $companyController = new CompanyController();
        $companyController->updateFieldCompanyIds($admin, $company->id);

        $admin->last_company_id = $company->id;
        $admin->save();

//  Tạo quyền master cho cty mới và gán cho người dùng
        if ($role_id) {
            $role = Roles::find($role_id);
        } else {
            $role = Roles::where('name', 'super_admin_company')->whereNull('company_id')->first();
        }
        $data = [
            'company_id' => $company->id,
            'name' => $role->name . '_' . $company->id,
        ];
        $role_new = $this->_role->duplicateRole(false, $company->id, $role, $data, $admin->id);
//        thêm job type
        $jobTypeController = new JobTypeController();
        $jobTypeController->CreateDefaultJobTypeForCompany($company, $role_new);
        return $admin;
    }

    public function profile(Request $request)
    {
        if (!$_POST) {
            $data['result'] = \Auth::guard('admin')->user();
            $data['module'] = $this->module;
            $data['page_title'] = 'Chỉnh sửa ' . trans($this->module['label']);
            $data['page_type'] = 'update';
            return view('eworkingadmin::profile', $data);
        } else if ($_POST) {
//            dd($request->all());
            try {
                $item = \Auth::guard('admin')->user();

                if ($item->id == \Auth::guard('admin')->user()->id) {
                    $validator = Validator::make($request->all(), [
                        'name' => 'required',

                    ], [
                        'name.required' => 'Bắt buộc phải nhập tên',
                    ]);

                    if ($validator->fails()) {
                        return back()->withErrors($validator)->withInput();
                    }
                }

                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert
                if (Admin::where('id', '!=', $item->id)->where('email', $data['email'])->count() > 0) {
                    CommonHelper::one_time_message('error', 'Email này đã được sử dụng!');
                    return back();
                }

                if ($request->has('status') && CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'admin_company_edit')) {
                    if ($request->status == 1 && $item->status == 0) {
                        $this->activeAdminFromCompany($item, $request->role_id);
                    } elseif ($request->status == 0 && $item->status == 1) {
                        $this->deActiveAdminFromCompany($item->id, \Auth::guard('admin')->user()->last_company_id);
                    }
                }
                unset($data['email']);
                #
                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }

                return back();
            } catch (\Exception $ex) {
                CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//                CommonHelper::one_time_message('error', $ex->getMessage());
                return redirect()->back()->withInput();
            }
        }
    }

    public function profileAdmin(Request $request, $id)
    {
        $admin = Admin::findOrFail($id);
        if (!$_POST) {
            if ($id == Auth::guard('admin')->user()->id) {
                return redirect('/admin/profile');
            }
            $data['result'] = $admin;
            $data['module'] = $this->module;
            $data['page_title'] = 'Chỉnh sửa ' . $this->module['label'];
            $data['page_type'] = 'update';
            return view('eworkingadmin::profile', $data);
        } else {
            $item = $admin;
            $msg = 'Cập nhật thành công!';
            if (CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'admin_company_edit')) {
                $roleAdmin = RoleAdmin::where('admin_id', $id)->where('company_id', \Auth::guard('admin')->user()->last_company_id)->first();
                if ($request->has('status')) {
                    if (@$roleAdmin->status == 0) {
                        $this->activeAdminFromCompany($item, $request->role_id);
                        $msg .= ' Đã gửi lời mời lại cho thành viên này.';
                    }
                } else {
                    if (@$roleAdmin->status == 1) {
                        $this->deActiveAdminFromCompany($item->id, \Auth::guard('admin')->user()->last_company_id);
                        $msg .= ' Đã kích thành viên ra khỏi công ty.';
                    }
                }
                RoleAdmin::where('admin_id', $id)
                    ->where('company_id', Auth::guard('admin')->user()->last_company_id)
                    ->update([
                        'role_id' => $request->role_id
                    ]);
            }
            CommonHelper::one_time_message('success', $msg);
            return back();
        }
    }

    public function searchForSelect2(Request $request)
    {
        $data = $this->model->select([$request->col, 'id'])->where($request->col, 'like', '%' . $request->keyword . '%');
        if ($request->where != '') {
            $data = $data->whereRaw(urldecode(str_replace('&#039;', "'", $request->where)));
        }
        if (@$request->company_id != null) {
            $data = $data->where('company_ids', 'like', '%|' . $request->company_id . '|%');
        }
        $data = $data->limit(5)->get();
        return response()->json([
            'status' => true,
            'items' => $data
        ]);
    }

    public function searchForSelect2All(Request $request)
    {
        $data = $this->model->select([$request->col, 'id'])->where($request->col, 'like', '%' . $request->keyword . '%');
        if ($request->where != '') {
            $data = $data->whereRaw(urldecode(str_replace('&#039;', "'", $request->where)));
        }
        if (@$request->company_id != null) {
            $data = $data->where('company_id', $request->company_id);
        }
        $data = $data->limit(5)->get();
        return response()->json([
            'status' => true,
            'items' => $data
        ]);
    }

    public function loginOtherAccount(Request $request)
    {
        if (isset($request->id)){
            $admin = Admin::find($request->id);
            \Auth::guard('admin')->login($admin);
            CommonHelper::one_time_message('success', 'Đăng nhập tài khoản ' . $admin->email . ' thành công!');
            return redirect('/admin/dashboard');
        }else{
            return redirect()->back;
        }
    }
    public function appendWhere($query, $request)
    {
        $query = $query->where('company_ids', 'like', '%|' . \Auth::guard('admin')->user()->last_company_id . '|%');
        return $query;
    }
}



