@if(in_array('contact_view', $permissions))
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/contact"
                                                      class="kt-menu__link "><span
                    class="kt-menu__link-icon"><i class="flaticon-book"></i>
            </span><span class="kt-menu__link-text">{{ trans('themetienganhadmin::admin.contact') }}</span></a></li>
@endif