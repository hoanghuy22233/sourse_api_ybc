<?php

return [
    'theme' => 'Giao diện',
    'post' => 'Bài viết',
    'all_post' => 'Tất cả bài viết',
    'create_post' => 'Tạo bài mới',
    'category' => 'Chuyên mục',
    'tag' => 'Thẻ',
    'contact' => 'Liên hệ',
    'banner' => 'Banner',
    'widget' => 'Widget',
    'menu' => 'Menu',
];
