<?php

namespace Modules\ThemeTiengAnhAdmin\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\RoleAdmin;
use Auth;
use DB;
use Illuminate\Http\Request;
use Mail;
use Modules\JdesBill\Models\Bill;
use Modules\Theme\Models\Post;
use Modules\JdesProduct\Models\Product;
use Modules\ThemeSemicolonwebJdes\Models\Company;

class DashboardController extends Controller
{
    protected
        $module = [
        'code' => 'post',
        'table_name' => 'posts',
        'label' => 'Bài viết',
    ];

    public function dashboardSoftware()
    {


        $data['page_title'] = 'Thống kê';
        $data['page_type'] = 'list';
        $data['module'] = $this->module;

        $role_name = CommonHelper::getRoleName(\Auth::guard('admin')->user()->id, 'name');
        if ($role_name == 'kenh') {
            return view('themetienganhadmin::dashboard_user')->with($data);
        } elseif ($role_name == 'nguoi-hoc') {
            return redirect('/');
        } else {
            return view('themetienganhadmin::dashboard')->with($data);
        }
    }


    public function tooltipInfo(Request $request)
    {
        $modal = new $request->modal;
        $data['item'] = $modal->find($request->id);
        $data['tooltip_info'] = $request->tooltip_info;

        return view('admin.common.modal.tooltip_info', $data);
    }

    public function ajax_up_file(Request $request)
    {
        if ($request->has('file')) {
            $file = CommonHelper::saveFile($request->file('file'));
        }
        return $file;
    }
}
