<?php

namespace Modules\ThemeTiengAnhAdmin\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Mail\MailServer;
use App\Models\Setting;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeTiengAnhAdmin\Models\Contact;
use Modules\ThemeTiengAnhAdmin\Models\Course;
use Validator;

class ContactController extends CURDBaseController
{

    protected $orderByRaw = 'status asc, id desc';

    protected $module = [
        'code' => 'contact',
        'table_name' => 'contacts',
        'label' => 'Liên hệ',
        'modal' => '\Modules\ThemeTiengAnhAdmin\Models\Contact',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'tel', 'type' => 'text', 'label' => 'Điện thoại'],
            ['name' => 'email', 'type' => 'text', 'label' => 'Email'],
            ['name' => 'link', 'type' => 'a', 'label' => 'Đăng ký tại link'],
            ['name' => 'course_id', 'type' => 'relation', 'object' => 'course', 'display_field' => 'name', 'label' => 'Khóa học'],
            ['name' => 'view', 'type' => 'status', 'label' => 'Đánh dấu'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Kích hoạt'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên'],
                ['name' => 'tel', 'type' => 'number', 'label' => 'Điện thoại'],
                ['name' => 'email', 'type' => 'text', 'label' => 'Email'],
                ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ'],
                ['name' => 'link', 'type' => 'text', 'label' => 'Đăng ký tại link'],
//                ['name' => 'gender', 'type' => 'select', 'options' => [
//                    '0' => 'Nam',
//                    '1' => 'Nữ',
//                    '2' => 'Khác',
//                ], 'label' => 'Giới tính'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Đã kích hoạt', 'value' => 1, 'group_class' => 'col-md-6'],
                ['name' => 'view', 'type' => 'checkbox', 'label' => 'Đã xem', 'value' => 1, 'group_class' => 'col-md-6'],
            ],

            'info_tab' => [
                ['name' => 'content', 'type' => 'textarea', 'label' => 'Nội dung'],

            ],

        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'tel' => [
            'label' => 'Điện thoại',
            'type' => 'number',
            'query_type' => 'like'
        ],
        'email' => [
            'label' => 'Email',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'course_id' => [
            'label' => 'Khóa học',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'course',
            'model' => Course::class,
            'query_type' => '='
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Khóa',
                1 => 'Đã kích hoạt',
            ]
        ],
    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);

        return view('themetienganhadmin::contact.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
//        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
//            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
//        }

        return $query;
    }

    public function add(Request $request)
    {
        try {

            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('themetienganhadmin::contact.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
//                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('themetienganhadmin::contact.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false,
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function ajaxAdd(Request $r)
    {
        try {
            foreach ($r->all() as $k => $v) {
                $this->model->$k = $v;
            }
            $this->model->save();

            $this->_mailSetting = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();

            $admin_emails = explode(',', trim($this->_mailSetting['admin_emails'], ','));
            $admin_email_first = '';
            foreach ($admin_emails as $k => $admin_email) {
                if ($k == 0) {
                    $admin_email_first = trim($admin_email);
                    unset($admin_emails[0]);
                } else {
                    $admin_emails[$k] = trim($admin_email);
                }
            }
            $data = [
                'subject' => 'Học viên liên hệ trên website',
                'user' => (object)[
                    'email' => $admin_email_first,
                    'name' => 'Quản trị viên'
                ],
                'customer' => (object)[
                    'email' => $r->email
                ],
                'name' => $this->_mailSetting['mail_name'],
                'cc' => $admin_emails,
                'view' => 'themetienganhadmin::emails.new_contact',
            ];

            \Mail::to($data['user'])->send(new MailServer($data));

            return response()->json([
                'status' => true,
                'msg' => 'Đăng ký thành công'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Đăng ký thất bại',
                'error' => $ex->getMessage()
            ]);
        }
    }
}
