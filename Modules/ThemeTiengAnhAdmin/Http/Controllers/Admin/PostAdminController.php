<?php

namespace Modules\ThemeTiengAnhAdmin\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeTiengAnh\Models\Admin;
use Modules\ThemeTiengAnhAdmin\Models\Category;
use Modules\ThemeTiengAnhAdmin\Models\Menu;
use App\Models\{RoleAdmin };

use Validator;

class PostAdminController extends CURDBaseController
{
    protected $module = [
        'code' => 'post_admin',
        'table_name' => 'post_admins',
        'label' => 'Bài viết admin',
        'modal' => '\Modules\ThemeTiengAnhAdmin\Models\PostAdmin',
        'list' => [
//            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên bài viết'],
//            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'themetienganhadmin::list.td.multi_cat', 'label' => 'Danh mục ', 'object' => 'category'],
//            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'themetienganhadmin::list.td.multi_cat', 'label' => 'Danh mục lịch học', 'object' => 'category_post'],
//            ['name' => 'tags', 'type' => 'custom', 'td' => 'themetienganhadmin::list.td.multi_cat', 'label' => 'Từ khóa', 'object' => 'tag_post'],
//            ['name' => 'slug', 'type' => 'text', 'label' => 'Đường dẫn tĩnh'],
//            ['name' => 'tin_hot', 'type' => 'status', 'label' => 'Tin hot'],
//            ['name' => 'order_no', 'type' => 'number', 'label' => 'Ưu tiên'],
            ['name' => 'admin_id', 'type' => 'relation', 'object' => 'admin', 'display_field' => 'name', 'label' => 'Kênh đăng'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trang thái'],
            ['name' => 'view_total', 'type' => 'number', 'label' => 'Lượt xem'],
            ['name' => 'view_frontend', 'type' => 'custom', 'td' => 'themetienganhadmin::list.td.view_frontend', 'label' => 'Xem'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tiêu đề bài viết'],
                ['name' => 'slug', 'type' => 'slug', 'class' => '', 'label' => 'Slug', 'des' => 'Đường dẫn bài viết trên thanh địa chỉ'],
//                ['name' => 'multi_cat', 'type' => 'custom', 'field' => 'themetienganhadmin::form.fields.multi_cat', 'label' => 'Danh mục', 'model' => Category::class,
//                    'object' => 'category_post', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=1', 'des' => 'Danh mục đầu tiên chọn là danh mục chính'],
                ['name' => 'content', 'type' => 'textarea_editor', 'label' => 'Nội dung'],
                ['name' => 'do_kho', 'type' => 'select', 'label' => 'Độ khó', 'options' => [
                    '' => '',
                    'dễ' => 'Dễ',
                    'trung bình' => 'Trung bình',
                    'khó' => 'Khó',
                ], 'group_class' => 'col-md-3'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt','value' => 1, 'group_class' => 'col-md-3'],
            ],

            'image_tab' => [
//                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh đại diện'],
                ['name' => 'link_youtube', 'type' => 'text', 'label' => 'Link youtube'],
            ],

            'question_tab' => [
                ['name' => 'question', 'type' => 'textarea_editor', 'label' => 'Tên câu hỏi'],
                ['name' => 'answer_a', 'type' => 'text', 'label' => 'Đáp án a', 'group_class' => 'col-md-6'],
                ['name' => 'answer_b', 'type' => 'text', 'label' => 'Đáp án b', 'group_class' => 'col-md-6'],
                ['name' => 'answer_c', 'type' => 'text', 'label' => 'Đáp án c', 'group_class' => 'col-md-6'],
                ['name' => 'answer_d', 'type' => 'text', 'label' => 'Đáp án d', 'group_class' => 'col-md-6'],
                ['name' => 'answer_exactly', 'type' => 'select', 'options' => [
                    'a' => 'A',
                    'b' => 'B',
                    'c' => 'C',
                    'd' => 'D',

                ], 'label' => 'Đáp án chính xác', 'group_class' => ''],
                ['name' => 'detailed_answer', 'type' => 'textarea', 'label' => 'Lời giải chi tiết'],
            ],

            'hoc_them_tab' => [
                ['name' => 'content_extra', 'type' => 'textarea_editor', 'label' => 'Học thêm'],
            ],

            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'Slug', 'des' => 'Đường dẫn bài viết trên thanh địa chỉ'],
                ['name' => 'meta_title', 'type' => 'custom', 'field' => 'themetienganhadmin::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta title', 'max_char' => 1000],
                ['name' => 'meta_description', 'type' => 'custom', 'field' => 'themetienganhadmin::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta description', 'max_char' => 1000],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, tên, đường dẫn',
        'fields' => 'id, name, slug'
    ];

    protected $filter = [
        'admin_id' => [
            'label' => 'Kênh',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'admin',
            'model' => Admin::class,
            'where' => '',
            'query_type' => 'custom'
        ],
        /*'tags' => [
            'label' => 'Từ khóa bài viết',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'tag_post',
            'model' => \Modules\ThemeTiengAnhAdmin\Models\Category::class,
            'query_type' => 'custom'
        ],*/

        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
        ],

    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('themetienganhadmin::post_admin.list')->with($data);
    }
    public function appendWhere($query, $request)
    {

        //  Lọc theo danh mục
        if (!is_null($request->get('category_id'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }
        if (!is_null($request->get('tags'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }

        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu mình tạo
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('admin_id', \Auth::guard('admin')->user()->id);
        }
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {

                $data = $this->getDataAdd($request);

                return view('themetienganhadmin::post_admin.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('tags')) {
                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
                    }
                    $data['admin_id'] = \Auth::guard('admin')->id();

                    #

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
//        try {
            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('themetienganhadmin::post_admin.edit')->with($data);
            } else if ($_POST) {

                

                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());


                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('tags')) {
                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
                    }
//                    $data['admin_id'] = \Auth::guard('admin')->id();

                    #

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
//        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
////            dd($ex->getMessage());
////            CommonHelper::one_time_message('error', $ex->getMessage());
//            return redirect()->back()->withInput();
//        }
    }

    public function getPublish(Request $request)
    {
        try {

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
