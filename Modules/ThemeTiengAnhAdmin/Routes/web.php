<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions','locale']], function () {
    Route::group(['prefix' => 'theme'], function () {
        Route::get('', 'Admin\ThemeController@getIndex')->name('theme')->middleware('permission:theme');
        Route::get('active', 'Admin\ThemeController@active')->name('theme.active')->middleware('permission:theme');
    });

    Route::group(['prefix' => 'menu'], function () {
        Route::get('', 'Admin\MenuController@getIndex')->name('menu')->middleware('permission:theme');
        Route::get('publish', 'Admin\MenuController@getPublish')->name('menu.publish')->middleware('permission:theme');
        Route::match(array('GET', 'POST'), 'add', 'Admin\MenuController@add')->middleware('permission:theme');
        Route::get('{id}/duplicate', 'Admin\MenuController@duplicate')->middleware('permission:theme');
        Route::get('delete/{id}', 'Admin\MenuController@delete')->middleware('permission:theme');
        Route::post('multi-delete', 'Admin\MenuController@multiDelete')->middleware('permission:theme');
        Route::get('search-for-select2', 'Admin\MenuController@searchForSelect2')->name('menu.search_for_select2')->middleware('permission:theme');
        Route::get('{id}', 'Admin\MenuController@update')->middleware('permission:theme');
        Route::post('{id}', 'Admin\MenuController@update')->middleware('permission:theme');
    });

    Route::group(['prefix' => 'banner'], function () {
        Route::get('', 'Admin\BannerController@getIndex')->name('banner')->middleware('permission:theme');
        Route::get('publish', 'Admin\BannerController@getPublish')->name('banner.publish')->middleware('permission:theme');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BannerController@add')->middleware('permission:theme');
        Route::get('{id}/duplicate', 'Admin\BannerController@duplicate')->middleware('permission:theme');
        Route::get('delete/{id}', 'Admin\BannerController@delete')->middleware('permission:theme');
        Route::post('multi-delete', 'Admin\BannerController@multiDelete')->middleware('permission:theme');
        Route::get('search-for-select2', 'Admin\BannerController@searchForSelect2')->name('banner.search_for_select2')->middleware('permission:theme');
        Route::get('{id}', 'Admin\BannerController@update')->middleware('permission:theme');
        Route::post('{id}', 'Admin\BannerController@update')->middleware('permission:theme');
    });

    Route::group(['prefix' => 'contact'], function () {
        Route::get('', 'Admin\ContactController@getIndex')->name('contact')->middleware('permission:contact_view');
        Route::get('publish', 'Admin\ContactController@getPublish')->name('contact.publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ContactController@add')->middleware('permission:contact_add');
        Route::get('{id}/duplicate', 'Admin\ContactController@duplicate')->middleware('permission:contact_add');
        Route::get('delete/{id}', 'Admin\ContactController@delete')->middleware('permission:contact_delete');
        Route::post('multi-delete', 'Admin\ContactController@multiDelete')->middleware('permission:contact_delete');
        Route::get('search-for-select2', 'Admin\ContactController@searchForSelect2')->name('contact.search_for_select2')->middleware('permission:contact_view');
        Route::get('{id}', 'Admin\ContactController@update')->middleware('permission:contact_view');
        Route::post('{id}', 'Admin\ContactController@update')->middleware('permission:contact_edit');
    });


    Route::group(['prefix' => 'post'], function () {
        Route::get('', 'Admin\PostController@getIndex')->name('post')->middleware('permission:post_view');
        Route::get('publish', 'Admin\PostController@getPublish')->name('post.publish')->middleware('permission:post_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\PostController@add')->middleware('permission:post_add');
        Route::get('{id}/duplicate', 'Admin\PostController@duplicate')->middleware('permission:theme');
        Route::get('delete/{id}', 'Admin\PostController@delete')->middleware('permission:post_delete');
        Route::post('multi-delete', 'Admin\PostController@multiDelete')->middleware('permission:post_delete');
        Route::get('search-for-select2', 'Admin\PostController@searchForSelect2')->name('post.search_for_select2')->middleware('permission:post_view');
        Route::get('{id}', 'Admin\PostController@update')->middleware('permission:post_view');
        Route::post('{id}', 'Admin\PostController@update')->middleware('permission:post_edit');
    });
 Route::group(['prefix' => 'post_admin'], function () {
        Route::get('', 'Admin\PostAdminController@getIndex')->name('post_admin')->middleware('permission:post_admin_view');
        Route::get('publish', 'Admin\PostAdminController@getPublish')->name('post_admin.publish')->middleware('permission:post_admin_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\PostAdminController@add')->middleware('permission:post_admin_add');
        Route::get('{id}/duplicate', 'Admin\PostAdminController@duplicate')->middleware('permission:theme');
        Route::get('delete/{id}', 'Admin\PostAdminController@delete')->middleware('permission:post_admin_delete');
        Route::post('multi-delete', 'Admin\PostAdminController@multiDelete')->middleware('permission:post_admin_delete');
        Route::get('search-for-select2', 'Admin\PostAdminController@searchForSelect2')->name('post_admin.search_for_select2')->middleware('permission:post_admin_view');
        Route::get('{id}', 'Admin\PostAdminController@update')->middleware('permission:post_admin_view');
        Route::post('{id}', 'Admin\PostAdminController@update')->middleware('permission:post_admin_edit');
    });


    Route::group(['prefix' => 'category_post'], function () {
        Route::get('', 'Admin\CategoryPostController@getIndex')->name('category_post')->middleware('permission:post_view');
        Route::get('publish', 'Admin\CategoryPostController@getPublish')->name('category_post.publish')->middleware('permission:post_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\CategoryPostController@add')->middleware('permission:post_add');
        Route::get('{id}/duplicate', 'Admin\CategoryPostController@duplicate')->middleware('permission:post_add');
        Route::get('delete/{id}', 'Admin\CategoryPostController@delete')->middleware('permission:post_delete');
        Route::post('multi-delete', 'Admin\CategoryPostController@multiDelete')->middleware('permission:post_delete');
        Route::get('search-for-select2', 'Admin\CategoryPostController@searchForSelect2')->name('category_post.search_for_select2')->middleware('permission:post_view');
        Route::get('{id}', 'Admin\CategoryPostController@update')->middleware('permission:post_view');
        Route::post('{id}', 'Admin\CategoryPostController@update')->middleware('permission:post_edit');
    });


    Route::group(['prefix' => 'tag_post'], function () {
        Route::get('', 'Admin\TagPostController@getIndex')->name('tag_post')->middleware('permission:post_view');
        Route::get('publish', 'Admin\TagPostController@getPublish')->name('tag_post.publish')->middleware('permission:post_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TagPostController@add')->middleware('permission:post_add');
        Route::get('{id}/duplicate', 'Admin\TagPostController@duplicate')->middleware('permission:post_add');
        Route::get('delete/{id}', 'Admin\TagPostController@delete')->middleware('permission:post_delete');
        Route::post('multi-delete', 'Admin\TagPostController@multiDelete')->middleware('permission:post_delete');
        Route::get('search-for-select2', 'Admin\TagPostController@searchForSelect2')->name('tag_post.search_for_select2');
        Route::get('{id}', 'Admin\TagPostController@update')->middleware('permission:post_view');
        Route::post('{id}', 'Admin\TagPostController@update')->middleware('permission:post_view');
    });

    Route::group(['prefix' => 'widget'], function () {
        Route::get('', 'Admin\WidgetController@getIndex')->name('widget')->middleware('permission:theme');
        Route::get('publish', 'Admin\WidgetController@getPublish')->name('widget.publish')->middleware('permission:theme');
        Route::match(array('GET', 'POST'), 'add', 'Admin\WidgetController@add')->middleware('permission:theme');
        Route::get('{id}/duplicate', 'Admin\WidgetController@duplicate')->middleware('permission:theme');
        Route::get('delete/{id}', 'Admin\WidgetController@delete')->middleware('permission:theme');
        Route::post('multi-delete', 'Admin\WidgetController@multiDelete')->middleware('permission:theme');
        Route::get('search-for-select2', 'Admin\WidgetController@searchForSelect2')->name('widget.search_for_select2')->middleware('permission:theme');
        Route::get('{id}', 'Admin\WidgetController@update')->middleware('permission:theme');
        Route::post('{id}', 'Admin\WidgetController@update')->middleware('permission:theme');

    });
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'Admin\DashboardController@dashboardSoftware');
    });
});

Route::group(['prefix' => 'admin'], function () {
    Route::group(['prefix' => 'ajax'], function () {
        Route::post('contact/add', 'Admin\ContactController@ajaxAdd');
    });
});
