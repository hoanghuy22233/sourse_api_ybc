<?php
namespace Modules\ThemeTiengAnhAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Widget extends Model
{

    protected $table = 'widgets';
    public $timestamps = false;

    protected $fillable = [
        'name', 'status', 'intro','location','content','order_no'
    ];



}
