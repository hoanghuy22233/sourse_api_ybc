<?php

namespace Modules\ThemeHandymanServices\Providers;

use App\Http\Helpers\CommonHelper;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\Translate\Http\Controllers\TranslateController;

class ThemeHandymanServicesServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('ThemeHandymanServices', 'Database/Migrations'));
        $this->rendMultiLanguager();
    }

    public function rendMultiLanguager()
    {
//        dd(Cookie::get('language'));
        $languageController = new TranslateController();
        $render_list_langs = $languageController->getHtmlLanguage();
        \Eventy::addFilter('user_bar.multi-language-frontend', function () use ($render_list_langs) {
            print '<div class="kt-header__topbar-item kt-header__topbar-item--langs frontend">
	    <div class="kt-header__topbar-wrapper" title="' . $render_list_langs['present']['name'] . '">
	        <span class="kt-header__topbar-icon">
				<img class="" src="' . CommonHelper::getUrlImageThumb($render_list_langs['present']['image'], 'false', 'false') . '" alt="' . $render_list_langs['present']['name'] . '">
			</span>
	    </div>
	    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround" style="height: fit-content; display: none">
	        <ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
                ' . $render_list_langs['list'] . '
            </ul>	    
        </div>
	</div>';
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('ThemeHandymanServices', 'Config/config.php') => config_path('themehandymanservices.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('ThemeHandymanServices', 'Config/config.php'), 'themehandymanservices'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/themehandymanservices');

        $sourcePath = module_path('ThemeHandymanServices', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/themehandymanservices';
        }, \Config::get('view.paths')), [$sourcePath]), 'themehandymanservices');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/themehandymanservices');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'themehandymanservices');
        } else {
            $this->loadTranslationsFrom(module_path('ThemeHandymanServices', 'Resources/lang'), 'themehandymanservices');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('ThemeHandymanServices', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
