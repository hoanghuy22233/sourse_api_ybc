<?php
namespace Modules\ThemeHandymanServices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    protected $table = 'services';
    public $timestamps = false;
    protected $fillable = [
        'name', 'image', 'intro','content','order_no','slug','status',
    ];
    public function fileds()
    {
        return $this->hasMany(ServiceFields::class);
    }
}
