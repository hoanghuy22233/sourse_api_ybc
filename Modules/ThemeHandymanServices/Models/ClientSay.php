<?php
namespace Modules\ThemeHandymanServices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClientSay extends Model
{

    protected $table = 'clients_say';
    public $timestamps = false;



    protected $fillable = [
        'name', 'image', 'intro','content'
    ];



}
