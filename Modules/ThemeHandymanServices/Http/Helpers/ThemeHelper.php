<?php

namespace Modules\ThemeHandymanServices\Http\Helpers;

use App\Http\Helpers\CommonHelper;
use Auth;
use Mail;
use Session;
use View;

class ThemeHelper
{

    public static function getUrlProduct($product)
    {
        $url = '';

        //  Lấy đường dẫn cty
        /*$company = Company::find($product->company_id);
        if (is_object($company)) {
            return '/';
        }
        $url .= '/' . $company->slug;*/

        //  Lấy đường dẫn danh mục
        $cat1 = CommonHelper::getFromCache('category_id_' . $product->category_id);
        if (!$cat1) {
            $cat1 = \Modules\ThemeSemicolonwebJdes\Models\Category::where('id', $product->category_id)->first();
            CommonHelper::putToCache('category_id_' . $product->category_id, $cat1);
        }
        if (is_object($cat1)) {
            $url .= '/' . $cat1->slug;
            if ($cat1->parent_id != null) {
                $cat2 = CommonHelper::getFromCache('category_id_' . $cat1->parent_id);
                if (!$cat2) {
                    $cat2 = \Modules\ThemeSemicolonwebJdes\Models\Category::where('id', $cat1->parent_id)->first();
                    CommonHelper::putToCache('category_id_' . $cat1->parent_id, $cat2);
                }
                if (is_object($cat2)) {
                    $url .= '/' . $cat2->slug;
                    if ($cat2->parent_id != null) {

                        $cat3 = CommonHelper::getFromCache('category_id_' . $cat2->parent_id);
                        if (!$cat3) {
                            $cat3 = \Modules\ThemeSemicolonwebJdes\Models\Category::where('id', $cat2->parent_id)->first();
                            CommonHelper::putToCache('category_id_' . $cat2->parent_id, $cat3);
                        }
                        if (is_object($cat3)) {
                            $url .= '/' . $cat3->slug;
                        }
                    }
                }
            }
        }

        //  Lấy đường dẫn sản phẩm
        $url .= '/' . $product->slug . '.html';

        return $url;
    }

    public static function getUrlCategory($category)
    {
        $url = '';

        //  Lấy đường dẫn cty
        /*$company = Company::find($category->company_id);
        if (is_object($company)) {
            return '/';
        }
        $url .= '/' . $company->slug;*/

        //  Lấy đường dẫn danh mục
        $cat1 = CommonHelper::getFromCache('category_id_' . $category->parent_id);
        if (!$cat1) {
            $cat1 = \Modules\ThemeSemicolonwebJdes\Models\Category::where('id', $category->parent_id)->first();
            CommonHelper::putToCache('category_id_' . $category->parent_id, $cat1);
        }
        if (is_object($cat1)) {
            $url .= '/' . $cat1->slug;
            if ($cat1->parent_id != null) {
                $cat2 = CommonHelper::getFromCache('category_id_' . $cat1->parent_id);
                if (!$cat2) {
                    $cat2 = \Modules\ThemeSemicolonwebJdes\Models\Category::where('id', $cat1->parent_id)->first();
                    CommonHelper::putToCache('category_id_' . $cat1->parent_id, $cat2);
                }
                if (is_object($cat2)) {
                    $url .= '/' . $cat2->slug;
                }
            }
        }

        //  Lấy đường dẫn sản phẩm
        $url .= '/' . $category->slug;

        return $url;
    }
}
