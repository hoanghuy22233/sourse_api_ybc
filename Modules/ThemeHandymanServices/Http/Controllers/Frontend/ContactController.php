<?php

namespace Modules\ThemeHandymanServices\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use Modules\ThemeHandymanServices\Http\Helpers\CommonHelper;

use Modules\ThemeHandymanServices\Models\Contact;
use Modules\ThemeHandymanServices\Models\Menus;


class ContactController extends Controller
{
    function getContact()
    {

        return view('themehandymanservices::pages.contact.contact');
    }
    function PostContact(request $r)
    {
            $contact = new contact;
            $contact->name = @$r->name;
            $contact->email = @$r->email;
            $contact->tel = @$r->phone;
            $contact->address = @$r->address;
            $contact->content = @$r->message;
            $contact->save();
            \App\Http\Helpers\CommonHelper::one_time_message('success', 'Gửi thành công');
            return redirect()->back();
    }

}
