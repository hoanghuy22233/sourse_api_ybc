<?php

namespace Modules\ThemeHandymanServices\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\HandymanServicesTheme\Models\Category;
use Modules\ThemeHandymanServices\Models\Booking;
use Modules\ThemeHandymanServices\Models\Post;
use Session;


class HomeController extends Controller
{
    function getHome()
    {
        return view('themehandymanservices::pages.home.home');
    }

    function getAbout(request $r)
    {
        $data['relates'] = Post::orderBy('created_at','desc')->where('faq','<>',1)->take(9)->get();
        $data['menu_page'] = Category::where('slug',$r->path())->first();
//        dd($data['menu_page']);
        return view('themehandymanservices::pages.home.introduce',$data);
    }

    public function bookingSuccess()
    {
        $booking_id = Session::get('booking_id');
        $data['booking'] = Booking::where('id', $booking_id)->first();
        if (!is_object($data['booking'])){
            Session::forget('booking_id');
        }
        return view('themehandymanservices::pages.service.thankyou')->with($data);

    }

}
