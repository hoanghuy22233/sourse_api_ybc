@extends('themehandymanservices::layouts.default')
@section('main_content')
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>.gm-style-pbc {
            transition: opacity ease-in-out;
            background-color: rgba(0, 0, 0, 0.45);
            text-align: center
        }

        .gm-style-pbt {
            font-size: 22px;
            color: white;
            font-family: Roboto, Arial, sans-serif;
            position: relative;
            margin: 0;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%)
        }
    </style>
    <style>.gm-style img {
            max-width: none;
        }

        .gm-style {
            font: 400 11px Roboto, Arial, sans-serif;
            text-decoration: none;
        }</style>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="pingback" href="https://handyman-services.cmsmasters.net/xmlrpc.php">
    {{--<title>Contacts – Handyman Services</title>--}}
    <link rel="dns-prefetch" href="https://maps.googleapis.com/">
    <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Handyman Services » Feed"
          href="https://handyman-services.cmsmasters.net/feed/">
    <link rel="alternate" type="application/rss+xml" title="Handyman Services » Comments Feed"
          href="https://handyman-services.cmsmasters.net/comments/feed/">
    <script type="text/javascript" async=""
            src="{{ URL::asset('public/frontend/themes/handyman-services/js/recaptcha__en.js') }}"></script>
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "https:\/\/handyman-services.cmsmasters.net\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.5"}
        };
        !function (a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case"flag":
                        return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                    case"emoji":
                        return b = d([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }

            var g, h, i, j, k = b.createElement("canvas"), l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.everything || (h = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
{{--    @include('themehandymanservices::partials.header_script')--}}
    <style id="rs-plugin-settings-inline-css" type="text/css">
        #rs-demo-id {
        }
    </style>
    <style id="woocommerce-inline-inline-css" type="text/css">
        .woocommerce form .form-row .required {
            visibility: visible;
        }
    </style>
    <style id="handyman-services-style-inline-css" type="text/css">

        html body {
            background-color: #f0f0f0;
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2017/06/pattern-bg.jpg);
            background-position: top center;
            background-repeat: repeat;
            background-attachment: scroll;
            background-size: auto;

        }

        .header_mid .header_mid_inner .logo_wrap {
            width: 185px;
        }

        .header_mid_inner .logo img.logo_retina {
            width: 185px;
        }

        @media (min-width: 540px) {
            .headline_aligner,
            .cmsmasters_breadcrumbs_aligner {
                min-height: 201px;
            }
        }

        .header_top {
            height: 38px;
        }

        .header_mid {
            height: 100px;
        }

        .header_bot {
            height: 50px;
        }

        #page.cmsmasters_heading_after_header #middle,
        #page.cmsmasters_heading_under_header #middle .headline .headline_outer {
            padding-top: 100px;
        }

        #page.cmsmasters_heading_after_header.enable_header_top #middle,
        #page.cmsmasters_heading_under_header.enable_header_top #middle .headline .headline_outer {
            padding-top: 138px;
        }

        #page.cmsmasters_heading_after_header.enable_header_bottom #middle,
        #page.cmsmasters_heading_under_header.enable_header_bottom #middle .headline .headline_outer {
            padding-top: 150px;
        }

        #page.cmsmasters_heading_after_header.enable_header_top.enable_header_bottom #middle,
        #page.cmsmasters_heading_under_header.enable_header_top.enable_header_bottom #middle .headline .headline_outer {
            padding-top: 188px;
        }

        @media only screen and (max-width: 1024px) {
            .header_top,
            .header_mid,
            .header_bot {
                height: auto;
            }

            .header_mid .header_mid_inner > div {
                height: 100px;
            }

            .header_bot .header_bot_inner > div {
                height: 50px;
            }

            #page.cmsmasters_heading_after_header #middle,
            #page.cmsmasters_heading_under_header #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_top #middle,
            #page.cmsmasters_heading_under_header.enable_header_top #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_bottom #middle,
            #page.cmsmasters_heading_under_header.enable_header_bottom #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_top.enable_header_bottom #middle,
            #page.cmsmasters_heading_under_header.enable_header_top.enable_header_bottom #middle .headline .headline_outer {
                padding-top: 0 !important;
            }
        }

        @media only screen and (max-width: 768px) {
            .header_mid .header_mid_inner > div,
            .header_bot .header_bot_inner > div {
                height: auto;
            }
        }

    </style>

    <style id="handyman-services-retina-inline-css" type="text/css">
        #cmsmasters_row_98d1c18a04 .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_98d1c18a04 .cmsmasters_row_outer_parent {
            padding-bottom: 50px;
        }

        #cmsmasters_row_98d1c18a04 .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-left: 0%;
        }

        #cmsmasters_row_98d1c18a04 .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-right: 0%;
        }

        #google_map_dd6c255e59 {
            height: 445px;
        }

        #cmsmasters_row_0d2b9d2cfc .cmsmasters_row_outer_parent {
            padding-top: 35px;
        }

        #cmsmasters_heading_4cd71645eb {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 10px;
        }

        #cmsmasters_heading_4cd71645eb .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_4cd71645eb .cmsmasters_heading, #cmsmasters_heading_4cd71645eb .cmsmasters_heading a {
            font-weight: 300;
        }

        #cmsmasters_heading_4cd71645eb .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_4cd71645eb .cmsmasters_heading_divider {
        }

        #cmsmasters_heading_d2e6dbfd12 {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 10px;
        }

        #cmsmasters_heading_d2e6dbfd12 .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_d2e6dbfd12 .cmsmasters_heading, #cmsmasters_heading_d2e6dbfd12 .cmsmasters_heading a {
            font-size: 32px;
            line-height: 36px;
        }

        #cmsmasters_heading_d2e6dbfd12 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_d2e6dbfd12 .cmsmasters_heading_divider {
        }

        #cmsmasters_divider_45c6304304 {
            border-bottom-width: 4px;
            border-bottom-style: solid;
            padding-top: 10px;
            margin-bottom: 5px;
            border-bottom-color: #f2a61f;
        }

        #cmsmasters_row_f2a53b981a .cmsmasters_row_outer_parent {
            padding-top: 50px;
        }

        #cmsmasters_row_f2a53b981a .cmsmasters_row_outer_parent {
            padding-bottom: 60px;
        }

        #cmsmasters_row_c6b170148c {
            background-color: #fbfbfb;
        }

        #cmsmasters_row_c6b170148c .cmsmasters_row_outer_parent {
            padding-top: 50px;
        }

        #cmsmasters_row_c6b170148c .cmsmasters_row_outer_parent {
            padding-bottom: 0px;
        }

        #cmsmasters_heading_debcf86c55 {
            text-align: left;
            margin-top: 0px;
            margin-bottom: 10px;
        }

        #cmsmasters_heading_debcf86c55 .cmsmasters_heading {
            text-align: left;
        }

        #cmsmasters_heading_debcf86c55 .cmsmasters_heading, #cmsmasters_heading_debcf86c55 .cmsmasters_heading a {
            font-weight: 300;
        }

        #cmsmasters_heading_debcf86c55 .cmsmasters_heading a:hover {
        }

        #cmsmasters_heading_debcf86c55 .cmsmasters_heading_divider {
        }

        #cmsmasters_row_022474f888 {
            background-color: #fbfbfb;
        }

        #cmsmasters_row_022474f888 .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_022474f888 .cmsmasters_row_outer_parent {
            padding-bottom: 35px;
        }

        #cmsmasters_row_022474f888 .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-left: 5%;
        }

        #cmsmasters_row_022474f888 .cmsmasters_row_inner.cmsmasters_row_fullwidth {
            padding-right: 5%;
        }

        #cmsmasters_clients_0622dbd888 .cmsmasters_clients_item {
            height: 90px;
            line-height: 90px;
        }

        #cmsmasters_clients_0622dbd888 .cmsmasters_clients_item a {
            line-height: 90px;
        }
        #cmsmasters_row_321e89fb7f {
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2015/04/header.jpg);
            background-position: top center;
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: cover;
        }#cmsmasters_row_321e89fb7f .cmsmasters_row_outer_parent {
             padding-bottom: 130px;
         }
        #cmsmasters_row_321e89fb7f .cmsmasters_row_outer_parent {
            padding-top: 120px;
        }
        .cmsmasters_row .cmsmasters_row_outer_parent {
            padding: 0.1px 0;
            z-index: 2;
        }
        #cmsmasters_heading_551d562beb .cmsmasters_heading, #cmsmasters_heading_551d562beb .cmsmasters_heading a {
            font-size: 44px;
            line-height: 54px;
            color: #ffffff;
            font-family: 'Titillium Web', Arial, Helvetica, 'Nimbus Sans L', sans-serif;
            font-weight: 700;
            font-style: normal;
            text-transform: none;
            text-decoration: none;
        }
        #cmsmasters_heading_551d562beb .cmsmasters_heading {
            text-align: center;
        }
        .cmsmasters_heading_wrap .cmsmasters_heading {
            display: inline-block;
            margin: 0;
        }
        #cmsmasters_heading_551d562beb {
            text-align: center;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .cmsmasters_heading_wrap {
            position: relative;
            overflow: hidden;
        }
        .cmsmasters_column_inner {
            width: 100%;
            position: relative;
            min-height: 1px;
        }
        .one_first {
            width: 96%;
            float: none;
            position: relative;
            clear: both;
        }
    </style>

    {{--<script>if (document.location.protocol != "https:") {--}}
            {{--document.location = document.URL.replace(/^http:/i, "https:");--}}
        {{--}</script>--}}
    <script type="text/javascript">
        /* <![CDATA[ */
        var LS_Meta = {"v": "6.7.6"};
        /* ]]> */
    </script>

    <meta name="generator"
          content="Powered by LayerSlider 6.7.6 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress.">
    <!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->
    <link rel="https://api.w.org/" href="https://handyman-services.cmsmasters.net/wp-json/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD"
          href="https://handyman-services.cmsmasters.net/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="https://handyman-services.cmsmasters.net/wp-includes/wlwmanifest.xml">

    <link rel="canonical" href="https://handyman-services.cmsmasters.net/contacts/">
    <link rel="shortlink" href="https://handyman-services.cmsmasters.net/?p=11850">
    <link rel="alternate" type="application/json+oembed"
          href="https://handyman-services.cmsmasters.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhandyman-services.cmsmasters.net%2Fcontacts%2F">
    <link rel="alternate" type="text/xml+oembed"
          href="https://handyman-services.cmsmasters.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhandyman-services.cmsmasters.net%2Fcontacts%2F&amp;format=xml">
    <noscript>
        <style>.woocommerce-product-gallery {
                opacity: 1 !important;
            }</style>
    </noscript>
    <script type="text/javascript">
        var cli_flush_cache = 2;
    </script>
    <meta name="generator"
          content="Powered by Slider Revolution 5.4.8.3 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.">
    <link rel="icon"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-32x32.png"
          sizes="32x32">
    <link rel="icon"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-192x192.png"
          sizes="192x192">
    <link rel="apple-touch-icon-precomposed"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-180x180.png">
    <meta name="msapplication-TileImage"
          content="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-270x270.png">
    <script type="text/javascript">function setREVStartSize(e) {
            try {
                e.c = jQuery(e.c);
                var i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                    f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function (e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({height: f})
            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };</script>
    <script type="text/javascript" charset="UTF-8"
            src="{{ URL::asset('public/frontend/themes/handyman-services/js/common.js') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('public/frontend/themes/handyman-services/js/util.js') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('public/frontend/themes/handyman-services/js/map.js') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('public/frontend/themes/handyman-services/js/geocoder.js') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('public/frontend/themes/handyman-services/js/marker.js') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('public/frontend/themes/handyman-services/js/onion.js') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('public/frontend/themes/handyman-services/js/AuthenticationService.Authenticate') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('public/frontend/themes/handyman-services/js/GeocodeService.Search') }}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{ URL::asset('public/frontend/themes/handyman-services/js/GeocodeService.Search') }}"></script>
</head>
<div id="screen-shader" style="
            transition: opacity 0.1s ease 0s;
            z-index: 2147483647;
            margin: 0;
            border-radius: 0;
            padding: 0;
            background: #111111;
            pointer-events: none;
            position: fixed;
            top: -10%;
            right: -10%;
            width: 120%;
            height: 120%;
            opacity: 0.6000;
            mix-blend-mode: multiply;
            display: none;
        "></div>
<body data-rsssl="1" class="page-template-default page page-id-11850 woocommerce-js">

<div class="cmsmasters_header_search_form">
    <span class="cmsmasters_header_search_form_close"></span>
    <form method="get">
        <div class="cmsmasters_header_search_form_field">
            <input type="search" name="s" placeholder="Enter Keywords" value="">
            <button type="submit" class="cmsmasters_theme_icon_search"></button>
        </div>
    </form>
</div>
<!--  Start Page  -->
<div id="page"
     class="csstransition chrome_only cmsmasters_boxed fixed_header enable_header_top cmsmasters_heading_after_header hfeed site">

    <!--  Start Main  -->
    <div id="main">

        <!--  Start Header  -->
    @include('themehandymanservices::template.menu')
    <!--  Finish Header  -->


        <!--  Start Middle  -->
        <div id="middle">
{{--            <div class="headline cmsmasters_color_scheme_default">--}}
{{--                <div class="headline_outer">--}}
{{--                    <div class="headline_color"></div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="middle_inner">
                <div class="content_wrap fullwidth">

                    <!-- Start Content  -->
                    <div class="middle_content entry">
                    </div>
                </div>
                <div id="cmsmasters_row_321e89fb7f" class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                    <div class="cmsmasters_row_outer_parent">
                        <div class="cmsmasters_row_outer">
                            <div class="cmsmasters_row_inner">
                                <div class="cmsmasters_row_margin cmsmasters_11">
                                    <div id="cmsmasters_column_267a7afc95" class="cmsmasters_column one_first">
                                        <div class="cmsmasters_column_inner">
                                            <div id="cmsmasters_heading_551d562beb" class="cmsmasters_heading_wrap cmsmasters_heading_align_center">
                                                <?php
                                                $widget = \Modules\ThemeHandymanServices\Models\Widget::where('location', 'title5')->where('status', 1)->first();
                                                ?>

                                                <h2 class="cmsmasters_heading">{!! $widget->{'name_' . $language}  !!}</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="cmsmasters_row_0d2b9d2cfc"
                     class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                    <div class="cmsmasters_row_outer_parent">
                        <div class="cmsmasters_row_outer">
                            <div class="cmsmasters_row_inner">
                                <div class="cmsmasters_row_margin cmsmasters_11">
                                    <div id="cmsmasters_column_6e7281658c" class="cmsmasters_column one_first">
                                        <div class="cmsmasters_column_inner">
                                            <div id="cmsmasters_heading_4cd71645eb"
                                                 class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                            </div>
                                            <div id="cmsmasters_heading_d2e6dbfd12"
                                                 class="cmsmasters_heading_wrap cmsmasters_heading_align_left">
                                                <h2 class="cmsmasters_heading">
                                                    {!! @$widget->{'content_' . $language} !!} </h2>
                                            </div>
                                            <div id="cmsmasters_divider_45c6304304"
                                                 class="cmsmasters_divider cmsmasters_divider_width_short cmsmasters_divider_extrashort"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="cmsmasters_row_f2a53b981a"
                     class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                    <div class="cmsmasters_row_outer_parent">
                        <div class="cmsmasters_row_outer">
                            <div class="cmsmasters_row_inner">
                                <div class="cmsmasters_row_margin cmsmasters_3414">
                                    <div id="cmsmasters_column_097bda8bbc" class="cmsmasters_column three_fourth">
                                        <div class="cmsmasters_column_inner">
                                            <div class="cmsmasters_contact_form">
                                                <div class="cmsmasters-form-builder">
                                                    <div class="cmsmasters_notice cmsmasters_notice_success cmsmasters_theme_icon_check success_box"
                                                         style="display:none;">
                                                        <div class="notice_icon"></div>
                                                        <div class="notice_content">
                                                            <p>Thank You! <br>Your message has been sent successfully.
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <script type="text/javascript">
                                                        var captchaWidgetRender = [];
                                                        var validateCaptcha = function () {
                                                            captchaWidgetRender.forEach(function (item_fn) {
                                                                if (typeof item_fn === "function") {
                                                                    item_fn();
                                                                }
                                                            });
                                                        };
                                                        jQuery(document).ready(function () {
                                                            jQuery('#form_704').validationEngine('attach', {
                                                                promptPosition: 'topRight',
                                                                scroll: false,
                                                                autoPositionUpdate: true,
                                                                showArrow: false
                                                            });
                                                            jQuery('#form_704 a#cmsmasters_704_formsend').click(function () {
                                                                jQuery('#form_704 .loading').animate({opacity: 1}, 250);
                                                                if (jQuery('#form_704').validationEngine('validate')) {
                                                                    jQuery.post('https://handyman-services.cmsmasters.net/wp-content/plugins/cmsmasters-contact-form-builder/inc/form-builder-sendmail.php', {
                                                                        cmsmasters_name_2: jQuery('#cmsmasters_name_2').val(),
                                                                        cmsmasters_email_2: jQuery('#cmsmasters_email_2').val(),
                                                                        cmsmasters_phone: jQuery('#cmsmasters_phone').val(),
                                                                        cmsmasters_date: jQuery('#cmsmasters_date').val(),
                                                                        cmsmasters_message: jQuery('#cmsmasters_message').val(),
                                                                        contactemail: 'NzA0fGV4YW1wbGVAZXhhbXBsZS5jb218NzA0',
                                                                        formname: '704',
                                                                        fromname: 'wordpress'
                                                                    }, function (data) {
                                                                        jQuery('#form_704 .loading').animate({opacity: 0}, 250);
                                                                        jQuery('#form_704').fadeOut('slow');
                                                                        document.getElementById('form_704').reset();
                                                                        jQuery('#form_704').parent().find('.box').hide();
                                                                        jQuery('#form_704').parent().find('.success_box').fadeIn('fast');
                                                                        jQuery('html, body').animate({scrollTop: jQuery('#form_704').offset().top - 140}, 'slow');
                                                                        jQuery('#form_704').parent().find('.success_box').delay(5000).fadeOut(1000, function () {
                                                                            jQuery('#form_704').fadeIn('slow');
                                                                        });
                                                                    });
                                                                    return false;
                                                                } else {
                                                                    jQuery('#form_704 .loading').animate({opacity: 0}, 250);
                                                                    return false;
                                                                }
                                                            });
                                                        });
                                                    </script>
                                                    <form action="{{route('contact.post')}}"
                                                          method="post" id="form_704">

                                                        <div class="form_info cmsmasters_input one_half">
                                                            <label for="cmsmasters_name_2">{{trans('themehandymanservices::site.name')}} <span
                                                                        class="color_2">*</span></label>
                                                            <div class="form_field_wrap">
                                                                <input type="text" name="name"
                                                                       id="cmsmasters_name_2" value="" size="35"
                                                                       class="validate[required]">
                                                            </div>
                                                        </div>
                                                        <div class="form_info cmsmasters_input one_half">
                                                            <label for="cmsmasters_email_2">{{trans('themehandymanservices::site.email')}} <span class="color_2">*</span></label>
                                                            <div class="form_field_wrap">
                                                                <input type="email" name="email"
                                                                       id="cmsmasters_email_2" value="" size="35"
                                                                       class="validate[required,custom[email]]">
                                                            </div>
                                                        </div>
                                                        <div class="form_info cmsmasters_input one_half">
                                                            <label for="cmsmasters_phone">{{trans('themehandymanservices::site.phone')}} <span
                                                                        class="color_2">*</span></label>
                                                            <div class="form_field_wrap">
                                                                <input type="text" name="phone"
                                                                       id="cmsmasters_phone" value="" size="35"
                                                                       class="validate[required,custom[number]]">
                                                            </div>
                                                        </div>
                                                        <div class="form_info cmsmasters_input one_half">
                                                            <label for="cmsmasters_date">{{trans('themehandymanservices::site.address')}} <span
                                                                        class="color_2">*</span></label>
                                                            <div class="form_field_wrap">
                                                                <input type="text" name="address"
                                                                       id="cmsmasters_date" value="" size="35"
                                                                       class="validate[required]">
                                                            </div>
                                                        </div>
                                                        <div class="form_info cmsmasters_textarea one_first">
                                                            <label for="cmsmasters_message">{{trans('themehandymanservices::site.message')}}</label>
                                                            <div class="form_field_wrap">
                                                                <textarea name="message"
                                                                          id="cmsmasters_message" cols="60"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="cl"></div>
                                                        <div class="loading"></div>
                                                        <div class="form_info submit_wrap">
                                                            <button name="submit" type="submit"
                                                                    id="cmsmasters_704_formsend"
                                                                    class="cmsmasters_button">{{trans('themehandymanservices::site.send_contact')}}
                                                            </button>
                                                        </div>
                                                        <div class="cl"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $widgets = \Modules\ThemeSemicolonwebJdes\Models\Widget::where('location','contact')->where('status', 1)->first();
                                    ?>
                                    <div id="cmsmasters_column_a892a7c775" class="cmsmasters_column one_fourth">
                                        <div class="cmsmasters_column_inner">
                                            <div class="cmsmasters_sidebar sidebar_layout_11">
                                                <aside id="text-5" class="widget widget_text"><h3 class="widgettitle">
                                                        {!! @$widgets->{'name_' . $language}  !!}</h3>
                                                    <div class="textwidget">

                                                        {!! @$widgets->{'content_' . $language}  !!}

                                                    </div>
                                                </aside>
                                                <div class="cl"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('themehandymanservices::pages.partner')

                <div class="cl"></div>
                <div class="content_wrap fullwidth">

                    <div class="middle_content entry"></div>
                    <!--  Finish Content  -->


                </div>
            </div>
        </div>
        <!--  Finish Middle  -->
        <!--  Start Bottom  -->
    {{--@include('themehandymanservices::partials.footer')--}}
    <!--  Finish Footer  -->

    </div>
    <span class="cmsmasters_responsive_width"></span>
    <!--  Finish Page  -->

</div>
    <div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
    <div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
    <script type="text/javascript">
        /* <![CDATA[ */
        cli_cookiebar_settings = '{"animate_speed_hide":"500","animate_speed_show":"500","background":"#FFF","border":"#b1a6a6c2","border_on":false,"button_1_button_colour":"#000","button_1_button_hover":"#000000","button_1_link_colour":"#fff","button_1_as_button":true,"button_1_new_win":false,"button_2_button_colour":"#333","button_2_button_hover":"#292929","button_2_link_colour":"#444","button_2_as_button":false,"button_2_hidebar":false,"button_3_button_colour":"#000","button_3_button_hover":"#000000","button_3_link_colour":"#fff","button_3_as_button":true,"button_3_new_win":false,"button_4_button_colour":"#000","button_4_button_hover":"#000000","button_4_link_colour":"#fff","button_4_as_button":true,"font_family":"inherit","header_fix":false,"notify_animate_hide":true,"notify_animate_show":false,"notify_div_id":"#cookie-law-info-bar","notify_position_horizontal":"right","notify_position_vertical":"bottom","scroll_close":false,"scroll_close_reload":false,"accept_close_reload":false,"reject_close_reload":false,"showagain_tab":true,"showagain_background":"#fff","showagain_border":"#000","showagain_div_id":"#cookie-law-info-again","showagain_x_position":"100px","text":"#000","show_once_yn":false,"show_once":"10000","logging_on":false,"as_popup":false,"popup_overlay":true,"bar_heading_text":"","cookie_bar_as":"banner","popup_showagain_position":"bottom-right","widget_position":"left"}';
        /* ]]> */
    </script>
    <script type="text/javascript">
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    </script>
    <link rel="stylesheet" id="cmsmasters_contact_form_style-css"
          href="{{ URL::asset('public/frontend/themes/handyman-services/css/contact-form-style.css') }}" type="text/css"
          media="screen">
    <script type="text/javascript">
        /* <![CDATA[ */
        var wpcf7 = {
            "apiSettings": {
                "root": "https:\/\/handyman-services.cmsmasters.net\/wp-json\/contact-form-7\/v1",
                "namespace": "contact-form-7\/v1"
            }
        };
        /* ]]> */
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var wc_add_to_cart_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "View cart",
            "cart_url": "https:\/\/handyman-services.cmsmasters.net\/cart\/",
            "is_cart": "",
            "cart_redirect_after_add": "no"
        };
        /* ]]> */
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var woocommerce_params = {"ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"};
        /* ]]> */
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var wc_cart_fragments_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "cart_hash_key": "wc_cart_hash_111d670d6faaadc1c57d42767a063ed0",
            "fragment_name": "wc_fragments_111d670d6faaadc1c57d42767a063ed0",
            "request_timeout": "5000"
        };
        /* ]]> */
    </script>

    <script type="text/javascript">
        /* <![CDATA[ */
        var cmsmasters_script = {
            "theme_url": "https:\/\/handyman-services.cmsmasters.net\/wp-content\/themes\/handyman-services",
            "site_url": "https:\/\/handyman-services.cmsmasters.net\/",
            "ajaxurl": "https:\/\/handyman-services.cmsmasters.net\/wp-admin\/admin-ajax.php",
            "nonce_ajax_like": "74e7bc310a",
            "nonce_ajax_view": "28fc9979c1",
            "project_puzzle_proportion": "0.7272",
            "gmap_api_key": "AIzaSyBz2LZYZ7NgCSc7JlVIDUADZ-aSw1mdDsY",
            "gmap_api_key_notice": "Please add your Google Maps API key",
            "gmap_api_key_notice_link": "read more how",
            "primary_color": "#005c8c",
            "ilightbox_skin": "dark",
            "ilightbox_path": "vertical",
            "ilightbox_infinite": "0",
            "ilightbox_aspect_ratio": "1",
            "ilightbox_mobile_optimizer": "1",
            "ilightbox_max_scale": "1",
            "ilightbox_min_scale": "0.2",
            "ilightbox_inner_toolbar": "0",
            "ilightbox_smart_recognition": "0",
            "ilightbox_fullscreen_one_slide": "0",
            "ilightbox_fullscreen_viewport": "center",
            "ilightbox_controls_toolbar": "1",
            "ilightbox_controls_arrows": "0",
            "ilightbox_controls_fullscreen": "1",
            "ilightbox_controls_thumbnail": "1",
            "ilightbox_controls_keyboard": "1",
            "ilightbox_controls_mousewheel": "1",
            "ilightbox_controls_swipe": "1",
            "ilightbox_controls_slideshow": "0",
            "ilightbox_close_text": "Close",
            "ilightbox_enter_fullscreen_text": "Enter Fullscreen (Shift+Enter)",
            "ilightbox_exit_fullscreen_text": "Exit Fullscreen (Shift+Enter)",
            "ilightbox_slideshow_text": "Slideshow",
            "ilightbox_next_text": "Next",
            "ilightbox_previous_text": "Previous",
            "ilightbox_load_image_error": "An error occurred when trying to load photo.",
            "ilightbox_load_contents_error": "An error occurred when trying to load contents.",
            "ilightbox_missing_plugin_error": "The content your are attempting to view requires the <a href='{pluginspage}' target='_blank'>{type} plugin<\\\/a>."
        };
        /* ]]> */
    </script>

    <script type="text/javascript" src="{{ URL::asset('public/frontend/themes/handyman-services/js/js') }}"></script>
    <script type="text/javascript"
            src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.gMap.min.js') }}"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var cmsmasters_woo_script = {
            "currency_symbol": "\u00a3",
            "thumbnail_image_width": "50",
            "thumbnail_image_height": "50"
        };
        /* ]]> */
    </script>

    <script type="text/javascript"
            src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.validationEngine.min.js') }}"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var cmsmasters_ve_lang = {
            "required": "* This field is required",
            "select_option": "* Please select an option",
            "required_checkbox": "* This checkbox is required",
            "min": "* Minimum",
            "allowed": " characters allowed",
            "max": "* Maximum",
            "invalid_email": "* Invalid email address",
            "invalid_number": "* Invalid number",
            "invalid_url": "* Invalid URL",
            "numbers_spaces": "* Numbers and spaces only",
            "letters_spaces": "* Letters and spaces only"
        };
        /* ]]> */
    </script>
    <script type="text/javascript"
            src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.validationEngine-lang.js') }}"></script>
    <script type="text/javascript"
            src="{{ URL::asset('public/frontend/themes/handyman-services/js/api.js') }}"></script>


    <div style="position: absolute; left: 0px; top: -2px; height: 1px; overflow: hidden; visibility: hidden; width: 1px;">
        <span style="position: absolute; font-size: 300px; width: auto; height: auto; margin: 0px; padding: 0px; font-family: Roboto, Arial, sans-serif;">BESbswy</span>
    </div>
    @endsection