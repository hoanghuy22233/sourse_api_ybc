
@extends('themehandymanservices::layouts.default')
@section('main_content')
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="pingback" href="https://handyman-services.cmsmasters.net/xmlrpc.php">
    {{--<title>Standard Blog – Handyman Services</title>--}}
    <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Handyman Services » Feed"
          href="https://handyman-services.cmsmasters.net/feed/">
    <link rel="alternate" type="application/rss+xml" title="Handyman Services » Comments Feed"
          href="https://handyman-services.cmsmasters.net/comments/feed/">
    <link rel="alternate" type="application/rss+xml" title="Handyman Services » Standard Blog Comments Feed"
          href="https://handyman-services.cmsmasters.net/standard-blog/feed/">
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "https:\/\/handyman-services.cmsmasters.net\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.5"}
        };
        !function (a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case"flag":
                        return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                    case"emoji":
                        return b = d([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }

            var g, h, i, j, k = b.createElement("canvas"), l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.everything || (h = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        .paginate>ul.nav-pagination li:before {
            content: unset;
        }
        .paginate>ul.nav-pagination li {
            padding: 5px 10px;
            border: 1px solid #ccc;
            margin: 0 5px;
            cursor: pointer;
        }
        .paginate>ul.nav-pagination li:hover {
            background: #ffba13;
            color: #fff;
        }
        .header_top_center div.kt-header__topbar-item--langs div.dropdown-menu>ul.kt-nav>li{
            z-index: 999;
            background: #fff;
        }

        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
{{--    @include('themehandymanservices::partials.header_script')--}}
    <style id="rs-plugin-settings-inline-css" type="text/css">
        #rs-demo-id {
        }
    </style>
    <style id="woocommerce-inline-inline-css" type="text/css">
        .woocommerce form .form-row .required {
            visibility: visible;
        }
    </style>
    <style id="handyman-services-style-inline-css" type="text/css">

        html body {
            background-color: #f0f0f0;
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2017/06/pattern-bg.jpg);
            background-position: top center;
            background-repeat: repeat;
            background-attachment: scroll;
            background-size: auto;

        }

        .header_mid .header_mid_inner .logo_wrap {
            width: 185px;
        }

        .header_mid_inner .logo img.logo_retina {
            width: 185px;
        }

        .headline_outer {
            background-image: url(https://handyman-services.cmsmasters.net/wp-content/uploads/2017/06/heading.jpg);
            background-repeat: no-repeat;
            background-attachment: scroll;
            background-size: cover;
        }

        @media (min-width: 540px) {
            .headline_aligner,
            .cmsmasters_breadcrumbs_aligner {
                min-height: 200px;
            }
        }

        .header_top {
            height: 38px;
        }

        .header_mid {
            height: 100px;
        }

        .header_bot {
            height: 50px;
        }
        .header_top_center div.kt-header__topbar-item--langs div.dropdown-menu{
            box-shadow: 0 0 10px 0 #ccc;
            cursor: pointer;
        }

        #page.cmsmasters_heading_after_header #middle,
        #page.cmsmasters_heading_under_header #middle .headline .headline_outer {
            padding-top: 100px;
        }

        #page.cmsmasters_heading_after_header.enable_header_top #middle,
        #page.cmsmasters_heading_under_header.enable_header_top #middle .headline .headline_outer {
            padding-top: 138px;
        }

        #page.cmsmasters_heading_after_header.enable_header_bottom #middle,
        #page.cmsmasters_heading_under_header.enable_header_bottom #middle .headline .headline_outer {
            padding-top: 150px;
        }

        #page.cmsmasters_heading_after_header.enable_header_top.enable_header_bottom #middle,
        #page.cmsmasters_heading_under_header.enable_header_top.enable_header_bottom #middle .headline .headline_outer {
            padding-top: 188px;
        }

        @media only screen and (max-width: 1024px) {
            .header_top,
            .header_mid,
            .header_bot {
                height: auto;
            }

            .header_mid .header_mid_inner > div {
                height: 100px;
            }

            .header_bot .header_bot_inner > div {
                height: 50px;
            }

            #page.cmsmasters_heading_after_header #middle,
            #page.cmsmasters_heading_under_header #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_top #middle,
            #page.cmsmasters_heading_under_header.enable_header_top #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_bottom #middle,
            #page.cmsmasters_heading_under_header.enable_header_bottom #middle .headline .headline_outer,
            #page.cmsmasters_heading_after_header.enable_header_top.enable_header_bottom #middle,
            #page.cmsmasters_heading_under_header.enable_header_top.enable_header_bottom #middle .headline .headline_outer {
                padding-top: 0 !important;
            }
        }

        @media only screen and (max-width: 768px) {
            .header_mid .header_mid_inner > div,
            .header_bot .header_bot_inner > div {
                height: auto;
            }
        }

    </style>
    <style id="handyman-services-retina-inline-css" type="text/css">
        #cmsmasters_row_41d8ea08e9 .cmsmasters_row_outer_parent {
            padding-top: 0px;
        }

        #cmsmasters_row_41d8ea08e9 .cmsmasters_row_outer_parent {
            padding-bottom: 50px;
        }


    </style>

    {{--<script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script>--}}
    <script type="text/javascript">
        /* <![CDATA[ */
        var LS_Meta = {"v": "6.7.6"};
        /* ]]> */
    </script>

    <meta name="generator"
          content="Powered by LayerSlider 6.7.6 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress.">
    <!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->
    <link rel="https://api.w.org/" href="https://handyman-services.cmsmasters.net/wp-json/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD"
          href="https://handyman-services.cmsmasters.net/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="https://handyman-services.cmsmasters.net/wp-includes/wlwmanifest.xml">
    <meta name="generator" content="WordPress 5.2.5">
    <meta name="generator" content="WooCommerce 3.6.4">
    <link rel="canonical" href="https://handyman-services.cmsmasters.net/standard-blog/">
    <link rel="shortlink" href="https://handyman-services.cmsmasters.net/?p=882">
    <link rel="alternate" type="application/json+oembed"
          href="https://handyman-services.cmsmasters.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhandyman-services.cmsmasters.net%2Fstandard-blog%2F">
    <link rel="alternate" type="text/xml+oembed"
          href="https://handyman-services.cmsmasters.net/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhandyman-services.cmsmasters.net%2Fstandard-blog%2F&amp;format=xml">
    <noscript>
        <style>.woocommerce-product-gallery {
                opacity: 1 !important;
            }</style>
    </noscript>
    <script type="text/javascript">
        var cli_flush_cache = 2;
    </script>
    <meta name="generator"
          content="Powered by Slider Revolution 5.4.8.3 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.">
    <link rel="icon"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-32x32.png"
          sizes="32x32">
    <link rel="icon"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-192x192.png"
          sizes="192x192">
    <link rel="apple-touch-icon-precomposed"
          href="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-180x180.png">
    <meta name="msapplication-TileImage"
          content="https://handyman-services.cmsmasters.net/wp-content/uploads/2017/07/cropped-favicon-270x270.png">
    <script type="text/javascript">function setREVStartSize(e) {
            try {
                e.c = jQuery(e.c);
                var i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                    f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function (e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({height: f})
            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        }
    </script>
{{--</head>--}}
<div id="screen-shader" style="
            transition: opacity 0.1s ease 0s;
            z-index: 2147483647;
            margin: 0;
            border-radius: 0;
            padding: 0;
            background: #111111;
            pointer-events: none;
            position: fixed;
            top: -10%;
            right: -10%;
            width: 120%;
            height: 120%;
            opacity: 0.6000;
            mix-blend-mode: multiply;
            display: none;
        "></div>
{{--<body data-rsssl="1" class="page-template-default page page-id-882 woocommerce-js">--}}

<div class="cmsmasters_header_search_form">
    <span class="cmsmasters_header_search_form_close"></span>
    <form method="get" action="https://handyman-services.cmsmasters.net/">
        <div class="cmsmasters_header_search_form_field">
            <input type="search" name="s" placeholder="Enter Keywords" value="">
            <button type="submit" class="cmsmasters_theme_icon_search"></button>
        </div>
    </form>
</div>
<!--  Start Page  -->
<div id="page"
     class="csstransition chrome_only cmsmasters_boxed fixed_header enable_header_top cmsmasters_heading_after_header hfeed site">

    <!--  Start Main  -->
    <div id="main">

        <!--  Start Header  -->
    @include('themehandymanservices::template.menu')
    <!--  Finish Header  -->


        <!--  Start Middle  -->
        <div id="middle">
            <div class="headline cmsmasters_color_scheme_default">
                <div class="headline_outer">
                    <div class="headline_color"></div>
                    <div class="headline_inner align_left">
                        <div class="headline_aligner"></div>
                        <?php
                        $widget = \Modules\ThemeHandymanServices\Models\Widget::where('location', 'title6')->where('status', 1)->first();
                        ?>
                        <div class="headline_text"><h1 class="entry-title">{!! $widget->{'name_' . $language}  !!}</h1></div>
                        <div class="cmsmasters_breadcrumbs">
                            <div class="cmsmasters_breadcrumbs_aligner"></div>
                            <div class="cmsmasters_breadcrumbs_inner"><a
                                        href="/" class="cms_home">{{trans('themehandymanservices::site.home')}}</a>
                                <span class="breadcrumbs_sep"> / </span>
                                <span>{!! $widget->{'name_' . $language}  !!}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="middle_inner">
                <div class="content_wrap r_sidebar">

                    <!-- Start Content  -->
                    <div class="content entry">
                        <div id="cmsmasters_row_41d8ea08e9"
                             class="cmsmasters_row cmsmasters_color_scheme_default cmsmasters_row_top_default cmsmasters_row_bot_default cmsmasters_row_boxed">
                            <div class="cmsmasters_row_outer_parent">
                                <div class="cmsmasters_row_outer">
                                    <div class="cmsmasters_row_inner">
                                        <div class="cmsmasters_row_margin cmsmasters_11">
                                            <div id="cmsmasters_column_b72ce6a7da" class="cmsmasters_column one_first">
                                                <div class="cmsmasters_column_inner">
                                                    <div id="blog_bcba34b7e9" class="cmsmasters_wrap_blog entry-summary"
                                                         data-layout="standard" data-layout-mode=""
                                                         data-url="https://handyman-services.cmsmasters.net/wp-content/plugins/cmsmasters-content-composer/"
                                                         data-orderby="name" data-order="ASC" data-count="5"
                                                         data-categories="advice,diy,home-improvement,home-smarter,repairs,solution,your-home"
                                                         data-metadata="date,categories,author,comments,likes,more"
                                                         data-pagination="more">
                                                        <div class="blog standard isotope"
                                                             style="position: relative; height: 3383px;">
                                                            <!-- Start Post Default Article  -->

                                                            @foreach($posts as $post)
                                                            <article id="post-103"
                                                                     class="cmsmasters_post_default post-103 post type-post status-publish format-image has-post-thumbnail hentry category-advice post_format-post-format-image"
                                                                     style="position: absolute; left: 0px; top: 0px;">
                                                                <div class="cmsmasters_post_cont">
                                                                    <figure class="cmsmasters_img_wrap"><a
                                                                                href="/{{ Modules\ThemeHandymanServices\Http\Helpers\ThemeHelper::getUrlProduct($post) }}"
                                                                                title="How Long Will the Tiny Homes Fad Last?"
                                                                                rel="ilightbox[img_103_5e153f13527f7]"
                                                                                class="cmsmasters_img_link"><img
                                                                                    width="860" height="514"
                                                                                    src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image,695,415)}}"
                                                                                    class=" wp-post-image"
                                                                                    alt="{{@$post->{'name_' . $language} }}"
                                                                                    title="{{@$post->{'name_' . $language} }}">
                                                                                    </a>
                                                                    </figure>
                                                                    <span class="cmsmasters_post_date"><abbr
                                                                                class="published" title="May 13, 2017">{{date('d-m-Y',strtotime($post->created_at))}}</abbr></span>
                                                                    <header class="cmsmasters_post_header entry-header">
                                                                        <h2 class="cmsmasters_post_title entry-title"><a
                                                                                    href="{{ Modules\ThemeHandymanServices\Http\Helpers\ThemeHelper::getUrlProduct($post) }}">{{@$post->{'name_' . $language} }}</a></h2>
                                                                    </header>
                                                                    <div class="cmsmasters_post_content entry-content">
                                                                        <p>{{@$post->{'intro_' . $language} }}</p>
                                                                    </div>
                                                                    <footer class="cmsmasters_post_footer entry-meta"><a
                                                                                class="cmsmasters_post_read_more"
                                                                                href="{{ Modules\ThemeHandymanServices\Http\Helpers\ThemeHelper::getUrlProduct($post) }}">Xem chi tiết</a>
                                                                    </footer>

                                                                </div>
                                                            </article>
                                                            @endforeach
                                                            <!-- Finish Post Default Article  -->
                                                        </div>
                                                            <div class="row mb-3">
                                                                <div class="col-md-12 paginate">
                                                                    {{ $posts->appends(Request::all())->links() }}
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cl"></div>
                    </div>
                    <!--  Finish Content  -->


                    <!--  Start Sidebar  -->
                    <div class="sidebar">
                        <aside id="search-2" class="widget widget_search">
                            <div class="search_bar_wrap">
                                <form method="get" action="/search">
                                    <p class="search_field">
                                        <input name="search" placeholder="Nhập từ khóa" value="" type="search">
                                    </p>
                                    <p class="search_button">
                                        <button type="submit" class="cmsmasters_theme_icon_search"></button>
                                    </p>
                                </form>
                            </div>
                        </aside>
                        <?php
                        $categories = @\Modules\HandymanServicesTheme\Models\Category::where('type', 1)->where('parent_id', 0)->orderBy('id', 'asc')->where('status', 1)->get();
                        ?>
                        <style>
                            #categories-2 > ul.cate_list > li.cate_li{
                                content: '\f111'!important;
                                font-family: 'FontAwesome' !important;
                            }

                        </style>
                        <aside id="categories-2" class="widget widget_categories">
                            <h3 class="widgettitle">{{trans('themehandymanservices::site.category')}}</h3>
                            <ul class="cate_list">
                                @foreach($categories as $cat)
                                <li class="cat-item cat-item-31 cate_li"><a href="/{{ $cat->slug }}">{{ @$cat->{'name_' . $language}  }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </aside>
                    </div>
                    <!--  Finish Sidebar  -->


                </div>
            </div>
        </div>
        <!--  Finish Middle  -->
        <!--  Start Bottom  -->
    {{--@include('themehandymanservices::partials.footer')--}}
    <!--  Finish Footer  -->

    </div>
    <span class="cmsmasters_responsive_width"></span>
    <!--  Finish Page  -->

    <div class="cli-modal-backdrop cli-fade cli-settings-overlay"></div>
    <div class="cli-modal-backdrop cli-fade cli-popupbar-overlay"></div>
</div>
<script type="text/javascript">
    /* <![CDATA[ */
    cli_cookiebar_settings = '{"animate_speed_hide":"500","animate_speed_show":"500","background":"#FFF","border":"#b1a6a6c2","border_on":false,"button_1_button_colour":"#000","button_1_button_hover":"#000000","button_1_link_colour":"#fff","button_1_as_button":true,"button_1_new_win":false,"button_2_button_colour":"#333","button_2_button_hover":"#292929","button_2_link_colour":"#444","button_2_as_button":false,"button_2_hidebar":false,"button_3_button_colour":"#000","button_3_button_hover":"#000000","button_3_link_colour":"#fff","button_3_as_button":true,"button_3_new_win":false,"button_4_button_colour":"#000","button_4_button_hover":"#000000","button_4_link_colour":"#fff","button_4_as_button":true,"font_family":"inherit","header_fix":false,"notify_animate_hide":true,"notify_animate_show":false,"notify_div_id":"#cookie-law-info-bar","notify_position_horizontal":"right","notify_position_vertical":"bottom","scroll_close":false,"scroll_close_reload":false,"accept_close_reload":false,"reject_close_reload":false,"showagain_tab":true,"showagain_background":"#fff","showagain_border":"#000","showagain_div_id":"#cookie-law-info-again","showagain_x_position":"100px","text":"#000","show_once_yn":false,"show_once":"10000","logging_on":false,"as_popup":false,"popup_overlay":true,"bar_heading_text":"","cookie_bar_as":"banner","popup_showagain_position":"bottom-right","widget_position":"left"}';
    /* ]]> */
</script>
<script type="text/javascript">
    var c = document.body.className;
    c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
    document.body.className = c;
</script>
<link rel="stylesheet" id="mediaelement-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/mediaelementplayer-legacy.min.css') }}"
      type="text/css" media="all">
<link rel="stylesheet" id="wp-mediaelement-css"
      href="{{ URL::asset('public/frontend/themes/handyman-services/css/mediaelementplayer-legacy.min.css') }}wp-mediaelement.min.css"
      type="text/css" media="all">
<script type="text/javascript">
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/handyman-services.cmsmasters.net\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    /* ]]> */
</script>

<script type="text/javascript">
    /* <![CDATA[ */
    var wc_add_to_cart_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
        "i18n_view_cart": "View cart",
        "cart_url": "https:\/\/handyman-services.cmsmasters.net\/cart\/",
        "is_cart": "",
        "cart_redirect_after_add": "no"
    };
    /* ]]> */
</script>

<script type="text/javascript">
    /* <![CDATA[ */
    var woocommerce_params = {"ajax_url": "\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"};
    /* ]]> */
</script>

<script type="text/javascript">
    /* <![CDATA[ */
    var wc_cart_fragments_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
        "cart_hash_key": "wc_cart_hash_111d670d6faaadc1c57d42767a063ed0",
        "fragment_name": "wc_fragments_111d670d6faaadc1c57d42767a063ed0",
        "request_timeout": "5000"
    };
    /* ]]> */
</script>

<script type="text/javascript">
    /* <![CDATA[ */
    var cmsmasters_script = {
        "theme_url": "https:\/\/handyman-services.cmsmasters.net\/wp-content\/themes\/handyman-services",
        "site_url": "https:\/\/handyman-services.cmsmasters.net\/",
        "ajaxurl": "https:\/\/handyman-services.cmsmasters.net\/wp-admin\/admin-ajax.php",
        "nonce_ajax_like": "74e7bc310a",
        "nonce_ajax_view": "28fc9979c1",
        "project_puzzle_proportion": "0.7272",
        "gmap_api_key": "AIzaSyBz2LZYZ7NgCSc7JlVIDUADZ-aSw1mdDsY",
        "gmap_api_key_notice": "Please add your Google Maps API key",
        "gmap_api_key_notice_link": "read more how",
        "primary_color": "#005c8c",
        "ilightbox_skin": "dark",
        "ilightbox_path": "vertical",
        "ilightbox_infinite": "0",
        "ilightbox_aspect_ratio": "1",
        "ilightbox_mobile_optimizer": "1",
        "ilightbox_max_scale": "1",
        "ilightbox_min_scale": "0.2",
        "ilightbox_inner_toolbar": "0",
        "ilightbox_smart_recognition": "0",
        "ilightbox_fullscreen_one_slide": "0",
        "ilightbox_fullscreen_viewport": "center",
        "ilightbox_controls_toolbar": "1",
        "ilightbox_controls_arrows": "0",
        "ilightbox_controls_fullscreen": "1",
        "ilightbox_controls_thumbnail": "1",
        "ilightbox_controls_keyboard": "1",
        "ilightbox_controls_mousewheel": "1",
        "ilightbox_controls_swipe": "1",
        "ilightbox_controls_slideshow": "0",
        "ilightbox_close_text": "Close",
        "ilightbox_enter_fullscreen_text": "Enter Fullscreen (Shift+Enter)",
        "ilightbox_exit_fullscreen_text": "Exit Fullscreen (Shift+Enter)",
        "ilightbox_slideshow_text": "Slideshow",
        "ilightbox_next_text": "Next",
        "ilightbox_previous_text": "Previous",
        "ilightbox_load_image_error": "An error occurred when trying to load photo.",
        "ilightbox_load_contents_error": "An error occurred when trying to load contents.",
        "ilightbox_missing_plugin_error": "The content your are attempting to view requires the <a href='{pluginspage}' target='_blank'>{type} plugin<\\\/a>."
    };
    /* ]]> */
</script>


<script type="text/javascript">
    /* <![CDATA[ */
    var cmsmasters_woo_script = {
        "currency_symbol": "\u00a3",
        "thumbnail_image_width": "50",
        "thumbnail_image_height": "50"
    };
    /* ]]> */
</script>

<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.isotope.min.js') }}"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var cmsmasters_isotope_mode = {
        "theme_url": "https:\/\/handyman-services.cmsmasters.net\/wp-content\/themes\/handyman-services",
        "site_url": "https:\/\/handyman-services.cmsmasters.net\/",
        "ajaxurl": "https:\/\/handyman-services.cmsmasters.net\/wp-admin\/admin-ajax.php",
        "nonce_ajax_like": "74e7bc310a",
        "nonce_ajax_view": "28fc9979c1",
        "project_puzzle_proportion": "0.7272",
        "gmap_api_key": "AIzaSyBz2LZYZ7NgCSc7JlVIDUADZ-aSw1mdDsY",
        "gmap_api_key_notice": "Please add your Google Maps API key",
        "gmap_api_key_notice_link": "read more how",
        "primary_color": "#005c8c",
        "ilightbox_skin": "dark",
        "ilightbox_path": "vertical",
        "ilightbox_infinite": "0",
        "ilightbox_aspect_ratio": "1",
        "ilightbox_mobile_optimizer": "1",
        "ilightbox_max_scale": "1",
        "ilightbox_min_scale": "0.2",
        "ilightbox_inner_toolbar": "0",
        "ilightbox_smart_recognition": "0",
        "ilightbox_fullscreen_one_slide": "0",
        "ilightbox_fullscreen_viewport": "center",
        "ilightbox_controls_toolbar": "1",
        "ilightbox_controls_arrows": "0",
        "ilightbox_controls_fullscreen": "1",
        "ilightbox_controls_thumbnail": "1",
        "ilightbox_controls_keyboard": "1",
        "ilightbox_controls_mousewheel": "1",
        "ilightbox_controls_swipe": "1",
        "ilightbox_controls_slideshow": "0",
        "ilightbox_close_text": "Close",
        "ilightbox_enter_fullscreen_text": "Enter Fullscreen (Shift+Enter)",
        "ilightbox_exit_fullscreen_text": "Exit Fullscreen (Shift+Enter)",
        "ilightbox_slideshow_text": "Slideshow",
        "ilightbox_next_text": "Next",
        "ilightbox_previous_text": "Previous",
        "ilightbox_load_image_error": "An error occurred when trying to load photo.",
        "ilightbox_load_contents_error": "An error occurred when trying to load contents.",
        "ilightbox_missing_plugin_error": "The content your are attempting to view requires the <a href='{pluginspage}' target='_blank'>{type} plugin<\\\/a>."
    };
    /* ]]> */
</script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/jquery.isotope.mode.js') }}"></script>
<script type="text/javascript">
    var mejsL10n = {
        "language": "en",
        "strings": {
            "mejs.install-flash": "You are using a browser that does not have Flash player enabled or installed. Please turn on your Flash player plugin or download the latest version from https:\/\/get.adobe.com\/flashplayer\/",
            "mejs.fullscreen-off": "Turn off Fullscreen",
            "mejs.fullscreen-on": "Go Fullscreen",
            "mejs-video": "Download Video",
            "mejs.fullscreen": "Fullscreen",
            "mejs.time-jump-forward": ["Jump forward 1 second", "Jump forward %1 seconds"],
            "mejs.loop": "Toggle Loop",
            "mejs.play": "Play",
            "mejs.pause": "Pause",
            "mejs.close": "Close",
            "mejs.time-slider": "Time Slider",
            "mejs.time-help-text": "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.",
            "mejs.time-skip-back": ["Skip back 1 second", "Skip back %1 seconds"],
            "mejs.captions-subtitles": "Captions\/Subtitles",
            "mejs.captions-chapters": "Chapters",
            "mejs.none": "None",
            "mejs.mute-toggle": "Mute Toggle",
            "mejs.volume-help-text": "Use Up\/Down Arrow keys to increase or decrease volume.",
            "mejs.unmute": "Unmute",
            "mejs.mute": "Mute",
            "mejs.volume-slider": "Volume Slider",
            "mejs.video-player": "Video Player",
            "mejs.audio-player": "Audio Player",
            "mejs.ad-skip": "Skip ad",
            "mejs.ad-skip-info": ["Skip in 1 second", "Skip in %1 seconds"],
            "mejs.source-chooser": "Source Chooser",
            "mejs.stop": "Stop",
            "mejs.speed-rate": "Speed Rate",
            "mejs.live-broadcast": "Live Broadcast",
            "mejs.afrikaans": "Afrikaans",
            "mejs.albanian": "Albanian",
            "mejs.arabic": "Arabic",
            "mejs.belarusian": "Belarusian",
            "mejs.bulgarian": "Bulgarian",
            "mejs.catalan": "Catalan",
            "mejs.chinese": "Chinese",
            "mejs.chinese-simplified": "Chinese (Simplified)",
            "mejs.chinese-traditional": "Chinese (Traditional)",
            "mejs.croatian": "Croatian",
            "mejs.czech": "Czech",
            "mejs.danish": "Danish",
            "mejs.dutch": "Dutch",
            "mejs.english": "English",
            "mejs.estonian": "Estonian",
            "mejs.filipino": "Filipino",
            "mejs.finnish": "Finnish",
            "mejs.french": "French",
            "mejs.galician": "Galician",
            "mejs.german": "German",
            "mejs.greek": "Greek",
            "mejs.haitian-creole": "Haitian Creole",
            "mejs.hebrew": "Hebrew",
            "mejs.hindi": "Hindi",
            "mejs.hungarian": "Hungarian",
            "mejs.icelandic": "Icelandic",
            "mejs.indonesian": "Indonesian",
            "mejs.irish": "Irish",
            "mejs.italian": "Italian",
            "mejs.japanese": "Japanese",
            "mejs.korean": "Korean",
            "mejs.latvian": "Latvian",
            "mejs.lithuanian": "Lithuanian",
            "mejs.macedonian": "Macedonian",
            "mejs.malay": "Malay",
            "mejs.maltese": "Maltese",
            "mejs.norwegian": "Norwegian",
            "mejs.persian": "Persian",
            "mejs.polish": "Polish",
            "mejs.portuguese": "Portuguese",
            "mejs.romanian": "Romanian",
            "mejs.russian": "Russian",
            "mejs.serbian": "Serbian",
            "mejs.slovak": "Slovak",
            "mejs.slovenian": "Slovenian",
            "mejs.spanish": "Spanish",
            "mejs.swahili": "Swahili",
            "mejs.swedish": "Swedish",
            "mejs.tagalog": "Tagalog",
            "mejs.thai": "Thai",
            "mejs.turkish": "Turkish",
            "mejs.ukrainian": "Ukrainian",
            "mejs.vietnamese": "Vietnamese",
            "mejs.welsh": "Welsh",
            "mejs.yiddish": "Yiddish"
        }
    };
</script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/mediaelement-and-player.min.js') }}"></script>
<script type="text/javascript"
        src="{{ URL::asset('public/frontend/themes/handyman-services/js/mediaelement-migrate.min.js') }}"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var _wpmejsSettings = {
        "pluginPath": "\/wp-includes\/js\/mediaelement\/",
        "classPrefix": "mejs-",
        "stretching": "responsive"
    };
    /* ]]> */
</script>
<script type="text/javascript"
        src="{{ URL::asset('wp-mediaelement.min.js') }}"></script>

@endsection