<?php
//$menu_mains = \Modules\HandymanServicesTheme\Models\Category::where('location','main_menu')->where('status',1)->get();
$menu_mains = \Modules\HandymanServicesTheme\Models\Category::where('location','main_menu')->where('status',1)->get();
$menu_top= \Modules\HandymanServicesTheme\Models\Category::where('location','menu_top')->where('status',1)->get();
$menu_services = \Modules\ThemeHandymanServices\Models\Service::where('status',1)->get();

?>
<style>

    /*.header_top_center div.kt-header__topbar-item--langs {*/
    /*    position: relative;*/

    /*}*/
    /*.header_top_center div.kt-header__topbar-item--langs div.dropdown-menu {*/
    /*     display: none;*/
    /* }*/
    /*.header_top_center div.kt-header__topbar-item--langs:hover .header_top_center div.kt-header__topbar-item--langs div.dropdown-menu {*/
    /*    position: absolute;*/
    /*    top: 100%;*/
    /*    left: 0;*/
    /*    right: 0;*/
    /*    display: block;*/

    /*}*/





    .navigation .menu-item-mega-container > ul > li > a .nav_title{
        font-weight: normal;
        text-transform: capitalize;
    }
    nav > div > ul div.menu-item-mega-container > ul > li{

        display: inline-block;
        width: 25%;
    }
    .tel-top:hover{
        color: unset!important;
    }
    .cmsmasters_theme_icon_search:before {
        content: '\f002' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters_theme_icon_user_phone:before {
        content: '\f095' !important;
        font-family: 'FontAwesome' !important;
    }

    .tparrows.tp-leftarrow:before {
        content: '\f053' !important;
        font-family: 'FontAwesome' !important;
    }

    .tparrows.tp-rightarrow:before {
        content: '\f054' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters-icon-custom-trophy:before {
        content: '\f091' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters-icon-custom-time-reverse:before {
        content: '\f1da' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters-icon-custom-wallet:before {
        content: '\f555' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters-icon-custom-stopwatch:before {
        content: '\f2f2' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters-icon-custom-preview:before {
        content: '\f06e' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters_theme_icon_user_mail:before {
        content: '\f0e0' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters_theme_icon_user_address:before {
        content: '\f3c5' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters-icon-linkedin:before {
        content: '\f08c' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters-icon-facebook:before {
        content: '\f39e' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters-icon-gplus:before {
        content: '\f0d5' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters-icon-twitter:before {
        content: '\f099' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters-icon-skype:before {
        content: '\f17e' !important;
        font-family: 'FontAwesome' !important;
    }

    ul > li:before {
        content: '\f111';
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters_img_rollover_wrap .cmsmasters_img_rollover .cmsmasters_open_post_link:before {
        content: '\f0ad' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters_theme_icon_resp_nav_slide_up:before {
        content: '\f106' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters_theme_icon_pagination_prev:before, .cmsmasters_theme_icon_slide_prev:before, .cmsmasters_theme_icon_comments_nav_prev:before, .cmsmasters_prev_arrow span:before {
        content: '\f104' !important;
        font-family: 'FontAwesome' !important;
    }

    .cmsmasters_theme_icon_pagination_next:before, .cmsmasters_theme_icon_slide_next:before, .cmsmasters_theme_icon_comments_nav_next:before, .cmsmasters_next_arrow span:before {
        content: '\f105' !important;
        font-family: 'FontAwesome' !important;
    }
    .header_top .header_top_right {
        height: 100%;
        padding: 0 0 0 20px;
        overflow: hidden;
        text-align: right;
        display: inline-block;
        float: right;
    }
    .header_top .header_top_center {
        height: 100%;
        padding: 0 0 0 20px;
        /*overflow: hidden;*/
        text-align: right;
        background-color:#fff;
        display: inline-block;
        float: right;
    }
    .header_top_center div.kt-header__topbar-item--langs{
        height: 100%;
    }
    .header_top_center div.kt-header__topbar-item--langs div.dropdown-menu{
        /*height: 38px;*/
        padding: 0;
        min-width: 38px!important;
        right: auto;
        border: none;
    }
    .header_top_center div.kt-header__topbar-item--langs div.dropdown-menu>ul.kt-nav{
        height: 100%;
        margin-bottom: 0;
    }
    .header_top_center div.kt-header__topbar-item--langs div.dropdown-menu>ul.kt-nav>li{
        height: 38px!important;
        padding: 5px;
        border-bottom: 1px solid #ccc;
    }
    .header_top_center div.kt-header__topbar-item--langs div.dropdown-menu>ul.kt-nav>li>a>span.kt-nav__link-text{
        display: none;
    }
    .header_top_center div.kt-header__topbar-item--langs div.dropdown-menu>ul.kt-nav>li:before{
       content: unset!important;
    }
    .header_top_center div.kt-header__topbar-item--langs div.dropdown-menu>ul.kt-nav>li>a>span>img{
        height: 100%;
    }
    .header_top_center div.kt-header__topbar-item--langs div.kt-header__topbar-wrapper{
        height: 100%;
        padding: 5px;
    }
    .header_top_center div.kt-header__topbar-item--langs>div.kt-header__topbar-wrapper>span.kt-header__topbar-icon>img{
        height: 100%;
    }
</style>
<header id="header">
    <div class="header_top" data-height="38" style="overflow: inherit; height: 38px;">
        <div class="header_top_outer">
            <div class="header_top_inner">
                <div class="header_top_left">
                    <div class="top_nav_wrap">
                        <a class="responsive_top_nav" href="javascript:void(0)"></a>
                        <nav>
                            <div class="menu-top-line-navigation-container">
                                <ul id="top_line_nav" class="top_line_nav">
                                    @foreach($menu_top as $menu_top)

                                        <li id="menu-item-14042"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14042">
                                            <a href="/{{$menu_top->slug}}">
                                                <span class="nav_item_wrap">{{@$menu_top->{'name_' . $language} }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                        @if(\Auth::guard('admin')->check())
                                            <li id="menu-item-14044"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14044">
                                                <a href="/admin/dashboard">
                                                    <span class="nav_item_wrap">{{trans('themehandymanservices::site.open_admin')}}</span>
                                                </a>
                                            </li>

                                    @else
                                            <li id="menu-item-14044"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14044">
                                                <a href="/admin/login">
                                                    <span class="nav_item_wrap">{{trans('themehandymanservices::site.login')}}</span>
                                                </a>
                                            </li>
                                    @endif
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="header_top_center">
                {!! Eventy::filter('user_bar.multi-language-frontend', '') !!}
                </div>
                <div class="header_top_right">


                    <div class="meta_wrap">
                        <span class="cmsmasters_theme_icon_user_phone">

                            <span class="tel" style="cursor: pointer">Call Us:  <a class="tel-top" href="tel:{{$settings['hotline']}}" style=" font-size: inherit;">{{$settings['hotline']}}</a></span>
                        </span>
                    </div>
                </div>

            </div>
        </div>
        <div class="header_top_but closed"><span class="cmsmasters_theme_icon_resp_nav_slide_down"></span></div>
    </div>
    <div class="header_mid" data-height="100" style="height: 100px;">
        <div class="header_mid_outer">
            <div class="header_mid_inner">
                <div class="logo_wrap"><a href="/"
                                          title="Handyman Services" class="logo">
                        <img src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo'],null) }}"
                             alt="{{@$settings['name']}}">
                        <img class="logo_retina"
                             src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo'],null) }}"
                             alt="{{@$settings['name']}}" width="185" height="64">
                    </a>
                </div>

                <!--  Start Navigation  -->
                <div class="mid_nav_wrap pos_unset">
                    <nav class="pos_unset">
                        <div class="menu-primary-navigation-containe pos_unset">
                            <ul id="navigation" class="mid_nav navigation pos_unset">

                                @foreach($menu_mains as $menu)

                                    @if($menu->parent_id == 0)
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item page-item-7366  menu-item-13034 menu-item-depth-0 pos_unset">
                                            <a href="/{{ $menu->slug }}"
                                               title="{{ $menu->{'name_' . $language}  }}">{{ $menu->{'name_' . $language}  }}</a>

                                            @if($menu->type_show==2)
                                                <div class="menu-item-mega-container"
                                                     style="width: 940px;right: 0;left: auto; border: none; padding: 0;">
                                                    <ul class="sub-menu" style="margin: 0;">

                                                        @foreach( $menu->childs as $menu_item )
                                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-12978 menu-item-depth-1">
                                                                <a href="{{ $menu_item->slug }}">
                                                                        <span class="nav_item_wrap">
                                                                            <span class="nav_title">{{ @$menu_item->{'name_' . $language}  }}</span>
                                                                        </span>
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @else
                                                @foreach( $menu->childs as $k=>$menu_item1 )
                                                    @if($k==0)
                                                        <ul class="sub-menu">
                                                    @endif
                                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13027 menu-item-depth-1">
                                                            <a href="/{{ $menu_item1->slug }}">
                                                                    <span class="nav_item_wrap">
                                                                        <span class="nav_title">{{ @$menu_item1->{'name_' . $language}  }}</span>
                                                                    </span>
                                                            </a>
                                                        </li>
                                                    @if($k==$menu->childs->count()-1)
                                                        </ul>
                                                    @endif
                                                @endforeach

                                            @endif
                                        </li>
                                    @endif
                                @endforeach

                            </ul>


                        </div>
                    </nav>
                </div>
                <!--  Finish Navigation  -->
            </div>
        </div>
    </div>
</header>