
<div id="bottom" class="cmsmasters_color_scheme_footer">
    <div class="bottom_bg">
        <div class="bottom_outer">
            <div class="bottom_inner sidebar_layout_14141414">
                <?php
                $widgets = \Modules\HandymanServicesTheme\Models\Widget::whereIn('location', ['footer_0', 'footer_1', 'footer_2', 'footer_3'])->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->get();
                ?>
                @foreach($widgets as $widget)

                <aside id="text-2" class="widget widget_text"><h3 class="widgettitle">{!! @$widget->{'name_' . $language}  !!}</h3>
                    <div class="textwidget">
                            {!! @$widget->{'content_' . $language}  !!}
                    </div>
                </aside>
                    @endforeach

            </div>
        </div>
    </div>
</div>
<!--  Finish Bottom  -->
<a href="javascript:void(0)" id="slide_top" class="cmsmasters_theme_icon_resp_nav_slide_up"
   style="display: none;"><span></span></a>
</div>
<!--  Finish Main  -->

<!--  Start Footer  -->
<footer id="footer" class="cmsmasters_color_scheme_footer cmsmasters_footer_default">
    <div class="footer_border">
        <div class="footer_inner">
            <div class="footer_default_left">
                <div class="footer_logo_wrap"><a href="/" class="footer_logo">
                        <img src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo-small'],null) }}"
                             alt="{{@$settings['name']}}">
                        <img class="footer_logo_retina"
                             src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo-small'],null) }}"
                             alt="{{@$settings['name']}}" width="185" height="64">
                    </a>
                </div>
                <span class="footer_copyright copyright">
                    <?php
                    $widget_bot_1 = \Modules\ThemeSemicolonwebJdes\Models\Widget::where('location','footer_bot_1')->where('status', 1)->first();
                    $widget_bot_2= \Modules\ThemeSemicolonwebJdes\Models\Widget::where('location','footer_bot_2')->where('status', 1)->first();
                    ?>

					<a class="privacy-policy-link" href="https://handyman-services.cmsmasters.net/privacy-policy/">{!! @$widget_bot_1->{'name_' . $language}   !!}</a> / {!! @$widget_bot_2->{'name_' . $language}   !!} </span>
            </div>
            <div class="footer_default_right">
                <div class="footer_custom_html_wrap">
                    <div class="footer_custom_html"><span class="cmsmasters_theme_icon_user_phone"><span
                                    class="tel"><a class="tel-top" href="tel:{{$settings['hotline']}}" style=" font-size: inherit;">{{$settings['hotline']}}</a></span></span></div>
                </div>
                {{--<div class="social_wrap">--}}
                    {{--<div class="social_wrap_inner">--}}
                        {{--<ul>--}}
                            {{--<li>--}}
                                {{--<a href="{!! @$settings['facebook'] !!}"--}}
                                   {{--class="cmsmasters_social_icon cmsmasters_social_icon_2 cmsmasters-icon-facebook"--}}
                                   {{--title="Facebook" target="_blank"></a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{!! @$settings['google_plus'] !!}"--}}
                                   {{--class="cmsmasters_social_icon cmsmasters_social_icon_3 cmsmasters-icon-gplus"--}}
                                   {{--title="Google" target="_blank"></a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{!! @$settings['twitter'] !!}"--}}
                                   {{--class="cmsmasters_social_icon cmsmasters_social_icon_4 cmsmasters-icon-twitter"--}}
                                   {{--title="Twitter" target="_blank"></a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="{!! @$settings['skype'] !!}"--}}
                                   {{--class="cmsmasters_social_icon cmsmasters_social_icon_5 cmsmasters-icon-skype"--}}
                                   {{--title="Skype" target="_blank"></a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</footer>
<!--  Finish Footer  -->