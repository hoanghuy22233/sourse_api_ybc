<?php
Route::get('load-content-cart', 'CartController@loadContentCart');
Route::get('search--product', 'ProductController@searchProduct');
Route::get('remove-product-in-cart', 'CartController@removeProductInCart');
Route::get('add-to-cart', 'CartController@addToCart');
Route::get('update-count-cart', 'CartController@updateCountCart');
Route::get('update-change-product-in-cart', 'CartController@updateChangeProductInCart');

Route::match(array('GET', 'POST'), 'quen-mat-khau', 'AppCoffeeThemeController@getEmailForgotPassword');
Route::match(array('GET', 'POST'), 'forgot-password/{change_password}', 'AppCoffeeThemeController@getForgotPassword');
Route::get('dang-xuat', function () {
    \Auth::guard('user')->logout();
    return redirect('/');
});
Route::get('add-bill', 'BillController@addBill');

Route::group(['prefix' => ''], function () {
    Route::get('', 'AppCoffeeThemeController@index');
    Route::get('dang-ky', 'AppCoffeeThemeController@getRegister');
    Route::post('dang-ky', 'AppCoffeeThemeController@postRegister');
    Route::get('dang-nhap', 'AppCoffeeThemeController@getLogin');
    Route::post('dang-nhap', 'AppCoffeeThemeController@authenticate');
    Route::get('{slug1}/{slug2}', 'AppCoffeeThemeController@twoParam');
});



Route::get('{slug1}', 'AppCoffeeThemeController@oneParam');