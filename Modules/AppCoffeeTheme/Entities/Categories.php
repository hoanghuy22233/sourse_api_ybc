<?php

namespace Modules\AppCoffeeTheme\Entities;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $guarded = [];
    protected $table = 'categories';

    public function childs()
    {
        return $this->hasMany($this, 'parent_id', 'id')->orderBy('order_no', 'asc');
    }
}
