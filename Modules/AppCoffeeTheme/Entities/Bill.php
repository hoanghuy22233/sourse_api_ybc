<?php

namespace Modules\AppCoffeeTheme\Entities;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $guarded = [];
    protected $table = 'bills';
    public function choose_table()
    {
        return $this->belongsTo(ChooseTable::class, 'choose_table_id', 'id');
    }
    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id', 'id');
    }
    public function order()
    {
        return $this->hasMany(Order::class, 'bill_id', 'id');
    }

}
