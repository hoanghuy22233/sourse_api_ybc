@extends('appcoffeetheme::layouts.master.master')

@section('content')
<style>
    .app-container {
        margin-top: 0;
    }

    .stardust_overflow::-webkit-scrollbar {
        height: 3px;
    }

    .stardust_overflow::-webkit-scrollbar-thumb {
        background: #ccc;
    }

    .filter-bar::-webkit-scrollbar {
        height: 3px;
    }

    .filter-bar::-webkit-scrollbar-thumb {
        background: #ccc;
    }

    .stardust_overflow {
        max-width: 100%;
        overflow-x: auto;
    }

    .stardust_overflow > li {
        width: 20%;
        overflow-x: hidden;
    }

    .filter-bar > .filter-bar-item {
        min-width: calc(20% + 1px - .5rem) !important;
    }

    .filter-bar {
        overflow-x: auto;
    }

    .stardust_dot {
        position: relative;
    }

    .menu_login {
        display: none;
        background: #fff;
        border: 1px solid #ddd;
        padding: 5px;
        margin: 0;
        border-radius: 3px;
    }

    .stardust_dot:hover .menu_login {
        display: block;
        position: absolute;
        top: 100%;
        width: 100px;
        right: 0;

    }

    .menu_login > li:hover {
        background: #f2f2f2;
    }

    .menu_login > li > a {
        color: #000;
        cursor: pointer;
    }

    .menu_login > li {
        list-style: none;
        padding: 5px 0;
        border-bottom: 1px solid #ddd;
    }

    .icon_menu {
        padding-left: 20px;
    }

    .icon_menu > li {
        list-style: none;
        height: 3px;
        border: 1px solid #ccc;
        margin-bottom: 3px;
        width: 20px;
        background: #ccc;
        cursor: pointer;
    }


    .new_title > h2 {
        text-transform: uppercase;
        color: #aaa;
        font-size: 16px;
        font-weight: unset;
        background: #fff;
        padding: 20px;
        margin-bottom: 5px;
    }

    .new_title > .item-news-list {
        background: #fff;
        padding: 10px;
        margin-bottom: 1px;
    }

    .new_title > .item-news-list > h3 {
        margin: 0;
        font-weight: unset;
    }

    .new_title > .item-news-list > h3 > a {
        text-transform: capitalize;
        color: #000;
        font-size: 14px;
        font-weight: unset;
    }

    .item-news-list-last {
        margin-bottom: 10px;
    }

    .load_more_block {
        text-align: center;
    }

    .load_more {
        padding: 7px 10px;
        background: red;
        color: #fff;
        cursor: pointer;
    }

    .icon_mail {
        float: left;
        font-size: 30px;
        height: 100%;
        line-height: 100%;
        margin-right: 10px;
    }

    .icon_phone {
        float: left;
        font-size: 35px;
        height: 100%;
        line-height: 100%;
        margin-right: 10px;
    }

    .email_info {
        height: 40px;
        margin-bottom: 20px;
    }

    .email_content_right > p {
        margin: 0;
    }

    .email_content_right > .title-top {
        margin-bottom: 5px;
        font-size: 16px;
    }

    .email_content_right > .content-bot {
        /*margin-bottom: 5px;*/
        font-size: 13px;
    }

    .new_footer {
        background: #fff;
        padding: 15px;
    }

    .new_footer-bot > p {
        color: #484646;
    }

    .new_footer-bot {
        background: #ccc;
        text-align: center;
        margin: -15px;
        padding: 10px;
    }
</style>
<div role="main">
    <div class="search-items-container">
        <div class="search-items-container__tab-panel" style="margin-top: 45px;">

            <!--                        danh sách tin tức-->

            <div class="new_title">

                <div class="item-news-list">
                    <h1 style="text-align: center">{{$post->name}}</h1>
                    <p>{!! $post->content !!}</p>
                </div>
            </div>
            <!--                        danh mục-->
            <div class="new_title">
                <h2>Danh mục</h2>
                <div class="item-news-list">
                    <h3 class="item-card-list__item-card-wrapper">
                        <a class="" href="#">Khuyến mại</a>
                    </h3>
                </div>
                <div class="item-news-list">
                    <h3 class="item-card-list__item-card-wrapper">
                        <a class="" href="#">Sự kiện</a>
                    </h3>
                </div>
                <div class="item-news-list">
                    <h3 class="item-card-list__item-card-wrapper">
                        <a class="" href="#">Tin tức</a>
                    </h3>
                </div>
                <div class="item-news-list">
                    <h3 class="item-card-list__item-card-wrapper">
                        <a class="" href="#">Hướng dẫn</a>
                    </h3>
                </div>
            </div>

            <div class="new_footer">
                <div class="item-news-list ">
                    <div class="email_info ">
                        <div class="icon_mail">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        </div>
                        <div class="email_content_right">
                            <p class="title-top">Email</p>
                            <p class="content-bot">Gửi câu hỏi của bạn!</p>
                        </div>
                    </div>
                </div>
                <div class="item-news-list ">
                    <div class="email_info ">
                        <div class="icon_phone">
                            <i class="fa fa-phone-square" aria-hidden="true"></i>
                        </div>
                        <div class="email_content_right">
                            <p class="title-top">Điện thoại</p>
                            <p class="content-bot">Gọi 19001500 ngay!</p>
                        </div>
                    </div>
                </div>

                <div class="new_footer-bot">
                    <p>Copyright © - Công ty TNHH Hobasoft., All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection