<div class="bottombar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    $widget=\Modules\AppCoffeeTheme\Models\Widget::where('location','footer')->where('status',1)->first();
                ?>
                <span class="copyright">{{@$widget->name}}</span>
            </div>
        </div>
    </div>
</div><!-- bottom bar -->