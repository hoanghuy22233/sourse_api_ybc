@if(count($products) == 0)
    <span class="alert alert-danger">Không có sản phẩm nào</span>
@endif
@foreach($products as $k=>$product)
    <div class="item-card-list__item-card-wrapper" data-id="{{ $product->id }}" data-price="{{ $product->final_price }}">
        <a class="_38_74r" href="#">
            <div class="VqaMtr">
                <div class="_3ZDC1p _1tDEiO">
                    <img width="invalid-value" height="invalid-value"
                         class="_1T9dHf _3XaILN"
                         style="object-fit: contain"
                         src="{{asset('public/filemanager/userfiles/'.$product->image)}}">
                </div>
                @if($product->final_price !=0 && $product->final_price < $product->base_price)
                    <div class="FsMOUG">
                        <div class="badge-text badge-text--promotion">
                            <div class="badge-text__text typo-r12">
                                <div class="badge__promotion">{{CEIL(($product->base_price - $product->final_price)*100 / $product->base_price)}}%
                                    <div class="badge__promotion-off">GIẢM</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="_26_GQ0">
                <div class="typo-r14 _2-Dw3_ i5LZ4j">{{$product->name}}</div>
                <div class="item-promotion-labels _1xDT8N _2ApczS"></div>
                <div class="_34q0o4 price">
                    @if($product->final_price >0 && $product->final_price < $product->base_price)
                        <div class="_1w9jLI typo-l12 _3EWU1m price_base">{{number_format($product->base_price, 0, '', '.')}}<span>₫</span></div>
                        <div class="kD6vRc"></div>
                        <div class="_1w9jLI typo-r16 _1k557v" style="max-width: calc(100% - 24px);">
                            <span class="price_down">{{number_format($product->final_price, 0, '', '.')}}</span>
                            <span class="_1q8LLE">₫</span>
                        </div>
                    @elseif($product->final_price ==0 && $product->base_price>0 )
                        <div class="_1w9jLI typo-r16 _1k557v" style="max-width: calc(100% - 24px);">
                            <span class="price_down">{{number_format($product->base_price, 0, '', '.')}}</span>
                            <span class="_1q8LLE">₫</span>
                        </div>
                    @elseif($product->base_price ==0 && $product->final_price>0 )
                        <div class="_1w9jLI typo-r16 _1k557v" style="max-width: calc(100% - 24px);">
                            <span class="price_down">{{number_format($product->final_price, 0, '', '.')}}</span>
                            <span class="_1q8LLE">₫</span>
                        </div>
                    @endif
                </div>

            </div>
        </a></div>
@endforeach