@extends('appcoffeetheme::layouts.master.default')
@section('main_content')
    <section>
        <div class="gap no-gap signin whitish medium-opacity">
            <div class="bg-image" style="background-image:url(/public/frontend/themes/edu/images/resources/theme-bg.jpg)"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="big-ad">
                            <figure><img src="{{ URL::asset('public/filemanager/userfiles/' . @$settings['logo']) }}" alt="{{ @$settings['name'] }}"></figure>
                            <h1>Chào mừng bạn đến với {{ @$settings['name'] }}</h1>
                            <p>
                                {!! @$settings['web_description'] !!}
                            </p>

                            <div class="fun-fact">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-4">
                                        <div class="fun-box">
                                            <i class="ti-check-box"></i>
                                            <h6>Registered People</h6>
                                            <span>1,01,242</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4">
                                        <div class="fun-box">
                                            <i class="ti-layout-media-overlay-alt-2"></i>
                                            <h6>Posts Published</h6>
                                            <span>21,03,245</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4">
                                        <div class="fun-box">
                                            <i class="ti-user"></i>
                                            <h6>Online Users</h6>
                                            <span>40,145</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="barcode">
                                <figure><img src="{{ URL::asset('public/frontend/themes/edu/images/resources/Barcode.jpg') }}" alt=""></figure>
                                <div class="app-download">
                                    <span>Download Mobile App and Scan QR Code to login</span>
                                    <ul class="colla-apps">
                                        <li><a title="" href="https://play.google.com/store?hl=en"><img
                                                        src="{{ URL::asset('public/frontend/themes/edu/images/android.png') }}" alt="">android</a></li>
                                        <li><a title="" href="https://www.apple.com/lae/ios/app-store/"><img
                                                        src="{{ URL::asset('public/frontend/themes/edu/images/apple.png') }}" alt="">iPhone</a></li>
                                        <li><a title="" href="https://www.microsoft.com/store/apps"><img
                                                        src="{{ URL::asset('public/frontend/themes/edu/images/windows.png') }}" alt="">Windows</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="we-login-register">
                            <div class="form-title">
                                @if (session('success'))
                                    <div class="alert bg-success" role="alert">
                                        {!!session('success')!!}
                                    </div>
                                @endif
                                <i class="fa fa-key"></i>Quên mật khẩu
                                <span>Nhập email của bạn vào, chúng tôi sẽ gửi vào mail cho bạn link để thay đổi mật khẩu</span>
                            </div>
                            <form class="we-form" method="post">
                                <input type="email" name="email" placeholder="Email" required>
                                <button type="submit" data-ripple="">Gửi yêu cầu</button>
                            </form>
                            {{--<a class="with-smedia facebook" href="#" title="" data-ripple="">Register with facebook</a>
                            <a class="with-smedia twitter" href="#" title="" data-ripple="">Register with twitter</a>--}}
                            <span>Bạn đã có tài khoản? <a class="we-account underline" href="/dang-nhap" title="">Đăng nhập</a></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection