<?php $k = time() . rand(1,1000);?>
<li style="margin-left: 20px; margin-bottom: 39px; border-bottom: 1px solid #ccc;">
    <a class="kt-avatar__cancel center-content attr-delete" data-toggle="kt-tooltip" data-original-title="Xóa hàng này" style="position: absolute;
    right: 0;
    top: -22px;
    background: #fff;
    padding: 3px;
    color: red;
    cursor: pointer;
    border: 1px solid red;
    border-radius: 47%;">
        <i class="fa fa-trash"></i>
    </a>
    <div style="position: absolute;
    right: 20px;
    top: -22px;
    background: #fff;
    padding: 3px;
    color: red;
    cursor: pointer;">
        <?php $field = ['name' => 'status' . $k, 'type' => 'checkbox', 'class' => '', 'label' => 'Kich hoạt',
            'value' => @$cat->status];?>
        @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
    </div>
    <div class="col-md-4 item-field">
        <?php $field = ['name' => 'category_id' . $k, 'type' => 'select2_ajax_model', 'object' => 'category_post', 'class' => '', 'label' => 'Danh mục',
            'model' => \Modules\RaoVatCrawler\Entities\Category::class, 'display_field' => 'name', 'display_field2' => 'id', 'value' => @$cat->category_id];?>
        @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
    </div>
    <div class="col-md-6 item-field">
        <?php $field = ['name' => 'link_crawl' . $k, 'type' => 'text', 'class' => 'required', 'label' => 'Lấy từ Link',
            'value' => @$cat->link_crawl];?>
        @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
    </div>
    <div class="col-md-2 item-field">
        <?php $field = ['name' => 'post_type' . $k, 'type' => 'select', 'class' => 'required', 'label' => 'Loại tin',
            'value' => @$cat->post_type, 'options' => [
                1 => 'Tìm nhà phân phối',
                2 => 'Mở đại lý phân phối',
                3 => 'Bán buôn, Bán sỉ',
                4 => 'Tài liệu hệ thống phân phối',
            ]];?>
        @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
    </div>
</li>