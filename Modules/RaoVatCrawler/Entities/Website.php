<?php

namespace Modules\RaoVatCrawler\Entities;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    protected $table = 'website';
    protected $guarded = [];
    public $timestamps = false;

    public function categories() {
        return $this->hasMany(DoomCategory::class, 'website_id', 'id')->where('status', 1);
    }
}
