<?php

namespace Modules\RaoVatCrawler\Entities;

use Illuminate\Database\Eloquent\Model;

class DoomCategory extends Model
{
    protected $table = 'doom_categories';
    protected $guarded = [];
    protected $fillable = [
        'category_id', 'link_crawl', 'website_id', 'post_type'
    ];
    public $timestamps = false;
}
