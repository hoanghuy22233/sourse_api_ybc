<?php

namespace Modules\RaoVatCrawler\Entities;

use Illuminate\Database\Eloquent\Model;

class LogCrawlProduct extends Model
{
    protected $table = 'log_crawl_product';
    protected $guarded = [];

}
