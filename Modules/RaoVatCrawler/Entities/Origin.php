<?php

namespace Modules\RaoVatCrawler\Entities;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
   protected $table = 'origins';
   public $timestamps = false;

   protected $fillable = [
       'name_origin', 'status', 'crawl_from'
   ];
}
