<?php

namespace Modules\RaoVatCrawler\Entities;

use Illuminate\Database\Eloquent\Model;

class PropertieName extends Model
{
    protected $table = 'properties_name';
    protected $guarded = [];
    public $timestamps = false;

}
