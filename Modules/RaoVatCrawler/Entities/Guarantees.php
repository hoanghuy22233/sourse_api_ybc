<?php

namespace Modules\RaoVatCrawler\Entities;

use Illuminate\Database\Eloquent\Model;

class Guarantees extends Model
{

    protected $table = 'guarantees';

    protected $fillable = [
        'name'
    ];
    public $timestamps =false;
}
