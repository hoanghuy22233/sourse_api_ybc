<?php

namespace Modules\RaoVatCrawler\Entities;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    protected $guarded = [];

    public function website()
    {
        return $this->belongsTo(Website::class, 'website_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'multi_cat', 'id');
    }


}
