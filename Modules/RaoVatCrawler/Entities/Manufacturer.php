<?php

namespace Modules\RaoVatCrawler\Entities;


use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    protected $table = 'manufactureres';
    protected $guarded = [];

    protected $fillable = [
        'name', 'slug', 'status', 'crawl_from'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'manufacture_id', 'id');
    }

}