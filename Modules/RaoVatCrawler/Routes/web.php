<?php

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'website'], function () {
        Route::get('', 'WebsiteDoomController@getIndex')->name('website')->middleware('permission:super_admin');
        Route::get('publish', 'WebsiteDoomController@getPublish')->name('website.publish')->middleware('permission:super_admin');
        Route::match(array('GET', 'POST'), 'add', 'WebsiteDoomController@add')->middleware('permission:super_admin');
        Route::get('delete/{id}', 'WebsiteDoomController@delete')->middleware('permission:super_admin');
        Route::post('multi-delete', 'WebsiteDoomController@multiDelete')->middleware('permission:super_admin');
        Route::get('crawl', 'WebsiteDoomController@crawl')->middleware('permission:super_admin');
        Route::match(array('GET', 'POST'),'doom/{website_id}', 'DoomController@update')->name('website.doom')->middleware('permission:super_admin');
        Route::get('{website_id}/test', 'DoomController@test')->middleware('permission:super_admin');
        Route::get('search-for-select2', 'WebsiteDoomController@searchForSelect2')->name('website.search_for_select2')->middleware('permission:super_admin');
        Route::get('ajax_html_select_category', 'DoomController@ajaxGetHtmlSelectCategory')->middleware('permission:super_admin');
        Route::get('{id}', 'WebsiteDoomController@update')->middleware('permission:super_admin');
        Route::post('{id}', 'WebsiteDoomController@update')->middleware('permission:super_admin');
    });

    /*Route::group(['prefix' => 'doom-product'], function () {
        Route::get('', 'ProductController@getIndex')->name('doom-product');
        Route::get('publish', 'ProductController@getPublish')->name('doom-product.publish');
        Route::match(array('GET', 'POST'), 'add', 'ProductController@add');
        Route::get('delete/{id}', 'ProductController@delete');
        Route::post('multi-delete', 'ProductController@multiDelete');
        Route::get('delete-all', 'ProductController@allDelete');
        Route::get('get-data-export', 'ProductController@getDataExport');
        Route::get('get-data-image-export', 'ProductController@getDataExportImage');
        Route::get('search-for-select2', 'ProductController@searchForSelect2')->name('doom-product.search_for_select2');
        Route::get('{id}', 'ProductController@update');
        Route::post('{id}', 'ProductController@update');
    });*/
});
