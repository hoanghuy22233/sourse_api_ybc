<?php

namespace Modules\RaoVatCrawler\Http\Controllers;

use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Setting;
use Modules\RaoVatCrawler\Console\CrawlProduct;
use Modules\RaoVatCrawler\Entities\Category;
use Modules\RaoVatCrawler\Entities\Post;
use Modules\ThemeRaoVat\Models\Menu;
use Validator;
use Excel;
use Storage;

class WebsiteDoomController extends CURDBaseController
{
    protected $module = [
        'code' => 'website',
        'table_name' => 'website',
        'label' => 'Website',
        'modal' => '\Modules\RaoVatCrawler\Entities\Website',
        'list' => [
            ['name' => 'domain', 'type' => 'text_edit', 'label' => 'Tên website'],
            ['name' => 'doom', 'type' => 'custom', 'td' => 'raovatcrawler::list.td.doom', 'label' => 'Doom'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],
            ['name' => 'status', 'type' => 'inner', 'label' => 'Số SP đã lấy'],
            ['name' => 'status', 'type' => 'inner', 'label' => 'Lần quét gần nhất'],
            ['name' => 'status', 'type' => 'inner', 'label' => 'Lịch quét tiếp theo'],
            ['name' => 'type', 'type' => 'select', 'options' => [
                'product' => 'Sản phẩm',
                'post' => 'Tin tức'
            ], 'label' => 'Lấy dạng'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'domain', 'type' => 'text', 'class' => 'required', 'label' => 'Tên website'],
                ['name' => 'doom', 'type' => 'textarea', 'class' => '', 'label' => 'Doom'],
                ['name' => 'type', 'type' => 'select', 'class' => '', 'label' => 'Loại tin lấy', 'options' => [
                    'product' => 'Sản phẩm',
                    'post' => 'Tin tức'
                ]],
                ['name' => 'status', 'type' => 'checkbox', 'class' => '', 'label' => 'Trạng thái', 'value' => 1],
            ],
        ],
    ];

    protected $filter = [
        'domain' => [
            'label' => 'Tên website',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);
        return view('raovatcrawler::list')->with($data);
    }

    //  Seed dữ liệu
    public function setCategoryParent() {
        $cats = Category::whereNull('parent_id')->get();
        foreach ($cats as $cat) {
            $post = Post::select('multi_cat')->where('category_child_id', '|'.$cat->id.'|')->first();
            if (is_object($post)) {
                $cat->parent_id = str_replace('|', '', $post->multi_cat);
                $cat->save();
            }
        }
        die('ok');
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('raovatcrawler::add')->with($data);
            } else if ($_POST) {

                $validator = Validator::make($request->all(), [
                    'domain' => 'required'
                ], [
                    'domain.required' => 'Bắt buộc phải nhập domain',
                ]);
                if ($validator->fails()) {
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => false,
                            'error' => $validator->errors()->all(),
                        ]);
                    }
                    return back()->withErrors($validator)->withInput();

                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
//                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $item = $this->model->find($id);
            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('raovatcrawler::edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'domain' => 'required'
                ], [
                    'domain.required' => 'Bắt buộc phải nhập domain',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
//                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_test') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id . '/test');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }
    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);
            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);
            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', $ex->getMessage());
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function crawl(){
        try {
            $crawlProduct = new CrawlProduct();
            $crawlProduct->handle();

            CommonHelper::one_time_message('success', 'Crawl thành công!');
            return back();
        } catch (\Exception $ex) {
            dd($ex->getMessage());
            CommonHelper::one_time_message('success', 'Crawl thất bại!');
            return back();
        }
    }
    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
