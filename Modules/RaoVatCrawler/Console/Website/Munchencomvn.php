<?php

namespace Modules\RaoVatCrawler\Console\Website;

use App\Http\Helpers\CommonHelper;
use Mail;
use Modules\RaoVatCrawler\Entities\Manufacturer;
use Modules\RaoVatCrawler\Entities\Origin;
use Modules\RaoVatCrawler\Entities\PropertieName;
use Modules\RaoVatCrawler\Entities\PropertieValue;
use Modules\RaoVatCrawler\Entities\Guarantees;
use Session;

class Munchencomvn extends CrawlProductBase
{

    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Bài viết',
        'modal' => '\Modules\RaoVatCrawler\Entities\Product',
    ];

    /**
     * Lấy link danh sách Bài viết
     */
    public function getPageListLink($cat_doom, $doom_setting, $i)
    {
        $cat_doom->link_crawl = str_replace('.html', '', $cat_doom->link_crawl);
        return $cat_doom->link_crawl .'-'.  $i . '.html';
    }

    public function appendData($data)
    {
        $data['code'] = str_replace('Mã SP : ', '', $data['code']);

        $data['highlight'] = @$data['content'];
        unset($data['content']);

        return $data;
    }

    public function getName($data, $html)
    {
        $v = $html->find('table.table.tbnober tr td', 0);
        $data['name'] = trim(strip_tags($v->innertext));

        $v = $html->find('table.table.tbnober tr td', 2);
        $data['final_price'] = trim(strip_tags($v->innertext));
        $data['final_price'] = $this->cleanPrice($data['final_price']);

        return $data;
    }

    public function updateProduct($product, $data)
    {
        print "        => Updated product " . $product->id . ':' . $product->name . "\n";
        return true;
    }
}
