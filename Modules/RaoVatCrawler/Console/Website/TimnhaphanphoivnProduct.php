<?php

namespace Modules\RaoVatCrawler\Console\Website;

use App\Http\Helpers\CommonHelper;
use App\Models\Province;
use Mail;
use Modules\RaoVatCrawler\Console\CrawlProduct;
use Modules\RaoVatCrawler\Entities\Category;
use Modules\RaoVatCrawler\Entities\PropertieName;
use Modules\RaoVatCrawler\Entities\PropertieValue;
use Session;

class TimnhaphanphoivnProduct extends CrawlProductBase
{

    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Sản phẩm',
        'modal' => '\Modules\RaoVatCrawler\Entities\Product',
    ];

    function __construct($website)
    {
        parent::__construct($website);

        if (Session::get('login_timnhaphanphoi') == null) {
            $this->loginSystem();
            Session::put('login_timnhaphanphoi', true);
        }
    }

    private function loginSystem()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://timnhaphanphoi.vn/account/logon");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        $rs = str_get_html(curl_exec($ch));
        $reqFields = array(
//            "__VIEWSTATE" => $rs->find("input[id=__VIEWSTATE]", 0)->value,
//            "__VIEWSTATEGENERATOR" => $rs->find("input[id=__VIEWSTATEGENERATOR]", 0)->value,
//            "__EVENTVALIDATION" => $rs->find("input[id=__EVENTVALIDATION]", 0)->value,
            'UserName' => 'kisyrua',
            'Password' => 'ruatien',
//            'ctl00$ucLogin1$btLogin' => 'Đăng nhập',
//            'ctl00$txtKey' => ''
        );

        curl_setopt($ch, CURLOPT_URL, "http://khosachnoi.com");
        curl_setopt($ch, CURLOPT_POST, count($reqFields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($reqFields));
        curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie_khosachnoicom.txt');
        $rs = str_get_html(curl_exec($ch));
    }

    public function appendData($data, $cat_doom = false)
    {
//        dd($data);
        //  Thuộc tính
        if (isset($data['proprerties_id'])) {
            foreach ($data['proprerties_id'] as $name => $value) {
                if ($value != '') {
                    if (strpos($name, 'Được bán bởi') !== false) {
                        $data['contact_name'] = $value;
                    } elseif (strpos($name, 'Tình trạng') !== false) {
                        $data['instock'] = $value == 'Còn hàng' ? 1 : 0;
                    } elseif (strpos($name, 'Điện thoại') !== false) {
                        $data['contact_tel'] = $value;
                    } elseif (strpos($name, 'Địa chỉ') !== false) {
                        $data['contact_address'] = $value;
                    } elseif (strpos($name, 'Mua tối thiểu') !== false) {
                        $data['min_buy'] = $value;
                    } elseif (strpos($name, 'Mã tin') !== false) {
                        $data['crawl_code'] = $value;
                    } elseif (strpos($name, 'Ngành hàng') !== false) {
                        $db_item = Category::where('name', $value)->first();
                        if (!is_object($db_item)) {
                            $db_item = new Category();
                            $db_item->name = $value;
                            $db_item->type = 10;
                            $db_item->slug = str_slug($value, '-');
                            $db_item->parent_id = @$cat_doom->category_id;
                            $db_item->save();
                        }
                        $data['category_child_id'] = '|' . $db_item->id . '|';
                        $data['multi_cat'] = '|' . $db_item->parent_id . '|';
                    } elseif (strpos($name, 'Nơi sản xuất') !== false) {
                        $data['production'] = $value;
                    } elseif (strpos($name, 'Ngày hết hạn') !== false) {
                        $data['deadline'] = date('Y-m-d', strtotime($value));
                    } elseif (strpos($name, 'Ngày đăng') !== false) {
                        $data['created_at'] = date('Y-m-d', strtotime($value));
                    }
                }
            }
            unset($data['proprerties_id']);
        }
        $data['source'] = 'Timnhaphanphoi.vn';
        return $data;
    }

    public function updateProduct($product, $cat_doom, $product_data)
    {
        if ($product->contact_address != $product_data['contact_address']) {
            $product->contact_address = $product_data['contact_address'];
        }
        if (date('Y-m-d', strtotime($product->created_at)) != $product_data['created_at']) {
            $product->created_at = $product_data['created_at'];
        }
        if ($product->price_intro != $product_data['price_intro']) {
            $product->price_intro = $product_data['price_intro'];
        }
        if ($product->price_content != $product_data['price_content']) {
            $product->price_content = $product_data['price_content'];
        }
        $product->save();
        print "        => Updated product " . $product->id . ':' . $product->name . "\n";
        return true;
    }

    /**
     * Lấy thông tin sản phẩm từ link sản phẩm
     */
    public function getDataProduct($product_link)
    {
        $html = file_get_html($product_link);

        $data = [];

        $data = $this->getAttribute($data, $html);

        $data = $this->getName($data, $html);

        $data = $this->getImage($data, $html);

        $data = $this->getImageExtra($data, $html);

        $data = $this->getContent($data, $html);

        $data = $this->getprice($data, $html);

        return $data;
    }

    public function getprice($data, $html) {
        $v = $html->find(@$this->_doom_setting->price_intro, 0);
        if ($v != null) {
            $data['price_intro'] = trim(strip_tags($v->innertext));
        }

        $v = $html->find(@$this->_doom_setting->price_content, 0);
        if ($v != null) {
            $data['price_content'] = '<table>'.trim($v->innertext).'</table>';
        }
        return $data;
    }

    public function getAttribute($data, $html) {
        $v = $html->find(@$this->_doom_setting->attributes);
//        dd($v);
        if ($v != null) {
            $data['proprerties_id'] = [];
            foreach ($v as $attribute) {
                $name = strip_tags($attribute->innertext);
                $val = $attribute->find('span', 0);
                if ($val != null) {
                    $val = strip_tags($val->innertext);
                }
                $data['proprerties_id'][trim($name)] = trim($val);
            }
        }
        return $data;
    }

}
