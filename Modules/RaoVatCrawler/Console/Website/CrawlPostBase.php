<?php

namespace Modules\RaoVatCrawler\Console\Website;

use App\Models\Error;
use Mail;
use App\Http\Helpers\CommonHelper;
use Modules\RaoVatCrawler\Entities\Manufacturer;
use Modules\RaoVatCrawler\Entities\Origin;
use Modules\RaoVatCrawler\Entities\Post;
use Modules\RaoVatCrawler\Entities\PropertieName;
use Modules\RaoVatCrawler\Entities\Guarantees;
use Modules\RaoVatCrawler\Entities\PropertieValue;
use Session;

class CrawlPostBase extends Base
{
    protected $_website;
    protected $_domain;
    protected $_doom_setting;
    protected $module = [
        'code' => 'post',
        'table_name' => 'posts',
        'label' => 'Bài viết',
        'modal' => '\Modules\RaoVatCrawler\Entities\Post',
    ];

    function __construct($website)
    {
        parent::__construct();

        $this->_website = $website;

        //  Lấy tên miền website
        $this->_domain = @explode('//', $website->domain)[1];
        $this->_domain = preg_replace('/\//', '', $this->_domain);

        $this->_doom_setting = json_decode($website->doom);

        /*if (Session::get('login_KhosachnoiCom') == null) {
            $this->loginSystem();
            Session::put('login_KhosachnoiCom', true);
        }*/
    }

    private function loginSystem()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://khosachnoi.com");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        $rs = str_get_html(curl_exec($ch));
        $reqFields = array(
            "__VIEWSTATE" => $rs->find("input[id=__VIEWSTATE]", 0)->value,
            "__VIEWSTATEGENERATOR" => $rs->find("input[id=__VIEWSTATEGENERATOR]", 0)->value,
            "__EVENTVALIDATION" => $rs->find("input[id=__EVENTVALIDATION]", 0)->value,
            'ctl00$ucLogin1$txtUserName' => 'kisyrua',
            'ctl00$ucLogin1$txtPassword' => 'ruatien',
            'ctl00$ucLogin1$btLogin' => 'Đăng nhập',
            'ctl00$txtKey' => ''
        );

        curl_setopt($ch, CURLOPT_URL, "http://khosachnoi.com");
        curl_setopt($ch, CURLOPT_POST, count($reqFields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($reqFields));
        curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie_khosachnoicom.txt');
        $rs = str_get_html(curl_exec($ch));
    }

    public function crawlPageList($test = false)
    {
        $result = [
            'total_created' => 0,
            'total_updated' => 0
        ];

        //  Lấy cấu hình doom
        $doom_setting = json_decode($this->_website->doom);

        //  Thực hiện quét các danh mục đã cấu hình
        foreach ($this->_website->categories as $cat_doom) {
            $i = 0;
            $stop = false;
            while (!$stop) {
                $i++;

                //  Chỉ chạy 1 trang đầu
//                if ($i == 100) {
//                    $stop = true;
//                    break;
//                }

                $page_list_link = $this->getPageListLink($cat_doom, $doom_setting, $i);
                $html = file_get_html($page_list_link);

                $posts_find = $html->find($doom_setting->target);
                $link_old = '';

                //  Nếu không tìm thấy Bài viết nào thì dừng lại
                if ($posts_find == null || empty($posts_find)) {
                    $stop = true;
                    break;
                }

                foreach ($posts_find as $k => $post) {

                    try {
                        //  Lấy link Bài viết
                        $post_link = $post->find($doom_setting->link, 0);
                        if ($post_link == null) {
                            return false;
                        }
                        $post_link = $post_link->getAttribute('href');
                        $post_link = $this->attachDomainToLink($post_link);

                        //  Nếu chưa tồn tại lưu nhớ link Bài viết đầu thì tạo lưu nhớ cho link Bài viết đầu tiên lấy được
                        if (!isset($post_first_link)) {
                            $post_first_link = $post_link;
                        } else {
                            //  Nếu link Bài viết này trùng với link Bài viết đầu tiên lấy được tức là đang bị chạy vòng tròn lặp lại sẽ dừng chạy
                            if ($post_link == $post_first_link) {
                                $stop = true;
                                break;
                            }
                        }

//                        $post_link = 'https://timnhaphanphoi.vn/cong-ty-tnhh-tm-dv-sx-xnk-green-food-tim-dai-ly-nha-phan-phoi-thuc-pham-cac-tinh-thanh-tren-toan-toan-quoc-dr20392.html';
//                        $post_link = 'https://timnhaphanphoi.vn/xuc-xich-ga-tuoi-can-tim-nha-phan-phoi-nha-ban-buon-ban-si-toan-quoc-dr43332.html';
//                        $post_link = 'https://timnhaphanphoi.vn/can-nguon-hang-mo-dai-ly-mat-ong-mut-phet-do-hop-khac-tai-tp-sam-son-thanh-hoa-dc43503.html';
                        if ($post_link != $link_old) {
                            $link_old = $post_link;

                            //  Kiểm tra trong db xem đã crawl Bài viết này chưa
                            $post_exist = Post::where('crawl_link', $post_link)->first();

                            if (is_object($post_exist)) {
                                //  Đã có thì  cập nhật

                                //  Dừng không cập nhật
                                $stop = true;
                                break;


                                $post_data = $this->getDataPost($post_link);
                                $post_data['crawl_link'] = $post_link;
                                $post_data = $this->cleanData($post_data);
                                $post_data = $this->appendData($post_data, $cat_doom);
                                if ($test) {
                                    return $this->printDemo($post_data);
                                }
                                $this->updatePost($post_exist, $post_data);
                                $result['total_updated'] ++ ;
                            } else {
                                //  chưa thì tạo mới
                                $post_data = $this->getDataPost($post_link);
                                $post_data['crawl_link'] = $post_link;

                                $post_data = $this->cleanData($post_data);
                                $post_data = $this->appendData($post_data, $cat_doom);
                                if ($test) {
                                    return $this->printDemo($post_data);
                                }
                                $prd = $this->createPost($cat_doom, $post_data);
                                $result['total_created'] ++ ;
                            }
                        }
                    } catch (\Exception $ex) {
                        Error::create([
                            'module' => 'raovatcrawler',
                            'message' => $ex->getLine() . ' : ' . $ex->getMessage(),
                            'file' => $ex->getFile(),
                            'code' => $this->_domain
                        ]);
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Lấy link danh sách Bài viết
     */
    public function getPageListLink($cat_doom, $doom_setting, $i)
    {
        return $cat_doom->link_crawl . str_replace('{i}', $i, $doom_setting->category_pagination);
    }

    /**
     * Hiển thị ra màn hình dữ liệu demo Bài viết
     */
    public function printDemo($data)
    {
        $key_name = [
            'crawl_link' => '<strong>Link Bài viết:</strong> ',
            'name' => '<strong>Tên:</strong> ',
            'code' => '<strong>Mã:</strong> ',
            'base_price' => '<strong>Giá cũ:</strong> ',
            'final_price' => '<strong>Giá bán:</strong> ',
            'image' => '<strong>Ảnh đại diện:</strong> ',
            'image_extra' => '<strong>Ảnh khác:</strong> ',
            'intro' => '<strong>Mô tả:</strong> ',
            'content' => '<strong>Nội dung:</strong> ',
            'highlight' => '<strong>Nội dung:</strong> ',
            'proprerties_id' => '<strong>Thuộc tính: </strong>',
            'manufacture_id' => '<strong>Hãng: </strong>',
            'origin_id' => '<strong>Nơi sản xuất: </strong>',
            'guarantee' => '<strong>Bảo hành: </strong>'
        ];
        foreach ($data as $key => $v) {
            echo @$key_name[$key] . '<br>';

            switch ($key) {
                case "crawl_link":
                    echo '<a href="' . $v . '" target="_blank">' . $v . '</a><br>';
                    break;
                case "image":
                    echo '<img src="/public/filemanager/userfiles/' . $v . '" style="width: 150px; height: 150px;"></a><br>';
                    break;
                case "image_extra":
                    foreach (explode('|', $v) as $val) {
                        if ($val != '') {
                            echo '<img src="/public/filemanager/userfiles/' . $val . '" style="width: 150px; height: 150px;"></a>';
                        }
                    }
                    echo '<br>';
                    break;
                case "base_price":
                    print number_format($v, 0, '.', '.') . 'đ<br>';
                    break;
                case "final_price":
                    print number_format($v, 0, '.', '.') . 'đ<br>';
                    break;
                case "intro":
                    print $v . '<br>';
                    break;
                case "content":
                    print $v . '<br>';
                    break;
                case "highlight":
                    print $v . '<br>';
                    break;
                case "proprerties_id":
                    if (is_string($v)) {
                        $v = explode('|', $v);
                        $data = PropertieValue::whereIn('id', $v)->get();
                        foreach ($data as $val) {
                            echo @$val->property_name->name . ': ' . @$val->value . '<br> ';
                        }
                        echo '<br>';
                    }
                    break;
                case "manufacture_id":
                    echo @Manufacturer::find($v)->name . '<br>';
                    break;
                case "guarantee":
                    echo @Guarantees::find($v)->name . '<br>';
                    break;
                case "origin_id":
                    echo @Origin::find($v)->name_origin . '<br>';
                    break;
                default:
                    echo $v . '<br>';
            }
        }
        return true;
    }

    public function createPost($cat_doom, $post_data)
    {
        $post = new Post();
        foreach ($post_data as $k => $v) {
            $post->{$k} = $v;
        }
        $post->multi_cat = '|' . $cat_doom->category_id . '|';
        $post->category_id = $cat_doom->category_id;
        $post->slug = $this->renderSlug(false, $post_data['name']);
        $post->status = 1;
        $post->crawl_updated_at = date('Y-m-d H:i:s');
        $post->save();
        print "        => Create post " . $post->id . ':' . $post->name . "\n";
        return $post;
    }

    public function updatePost($post, $data)
    {
        $post->crawl_updated_at = date('Y-m-d H:i:s');
        $post->save();
        print "        => Updated post " . $post->id . ':' . $post->name . "\n";
        return true;
    }

    /**
     * Lấy thông tin Bài viết từ link Bài viết
     */
    public function getDataPost($post_link)
    {
        $html = file_get_html($post_link);

        $data = [];

        $data = $this->getName($data, $html);

        $data = $this->getImage($data, $html);

        $data = $this->getImageExtra($data, $html);

        $data = $this->getIntro($data, $html);

        $data = $this->getContent($data, $html);

        return $data;
    }

    /**
     * Lấy nội dung
     */
    public function getContent($data, $html)
    {
        $v = $html->find(@$this->_doom_setting->content, 0);
        if ($v != null) {
            $data['content'] = trim($v->innertext);
            $data['content'] = $this->saveImgInContent($data['content'], $v, 'post/' . date('Y/m/d/') . str_slug($data['name']) . '/content');
            $data['content'] = $this->cleanContent($data['content'], $html);
        }
        return $data;
    }

    public function getIntro($data, $html)
    {
        $v = $html->find(@$this->_doom_setting->intro, 0);
        if ($v != null) {
            $data['intro'] = trim($v->innertext);
        }
        return $data;
    }

    /**
     * Lấy ảnh của Bài viết
     */
    public function getImage($data, $html)
    {
        $v = $html->find(@$this->_doom_setting->image, 0);
        if ($v != null) {
            $data['image'] = '';
            if ($v->getAttribute('data-src') !== false) {
                $data['image'] = @$v->getAttribute('data-src');
            } elseif ($v->getAttribute('src') !== false) {
                $data['image'] = trim(@$v->getAttribute('src'));
            } elseif ($v->getAttribute('style') !== false) {    //  Ảnh trong thuộc tính background-image
                $data['image'] = trim(@$v->getAttribute('style'));
                $data['image'] = @explode('url(', $data['image'])[1];
                $data['image'] = @explode(');', $data['image'])[0];
                $data['image'] = str_replace('"', '', $data['image']);
            }

            if ($data['image'] != '') {
                $data['image'] = explode('?', $data['image'])[0];

                $data['image'] = $this->attachDomainToLink($data['image']);

                try {
                    $data['image'] = CommonHelper::saveFile($data['image'], 'post/' . date('Y/m/d/') . str_slug($data['name']));
                } catch (\Exception $ex) {

                }
            }
        }
        return $data;
    }

    public function getName($data, $html)
    {

        $v = $html->find(@$this->_doom_setting->name, 0);
        if ($v != null) {
            $data['name'] = trim(strip_tags($v->innertext));
        }
        return $data;
    }

    /**
     * Lấy ảnh thêm của Bài viết
     */
    public function getImageExtra($data, $html)
    {
        $v = $html->find(@$this->_doom_setting->image_extra);
        if ($v != null) {
            $image_extra_arr = [];
            foreach ($v as $image_extra) {
                $image_extra_src = '';
                if ($image_extra->getAttribute('data-src') !== false) {
                    $image_extra_src = @$image_extra->getAttribute('data-src');
                } elseif ($image_extra->getAttribute('src') !== false) {
                    $image_extra_src = trim(@$image_extra->getAttribute('src'));
                } elseif ($image_extra->getAttribute('style') !== false) {    //  Ảnh trong thuộc tính background-image
                    $image_extra_src = trim(@$image_extra->getAttribute('style'));
                    $image_extra_src = @explode('url(', $image_extra_src)[1];
                    $image_extra_src = @explode(');', $image_extra_src)[0];
                    $image_extra_src = str_replace('"', '', $image_extra_src);
                }

                $image_extra_src = explode('?', $image_extra_src)[0];
                if ($image_extra_src != '') {

                    $image_extra_src = $this->attachDomainToLink($image_extra_src);
                    try {
                        $image_extra_arr[] = CommonHelper::saveFile($image_extra_src, 'post/' . date('Y/m/d/') . str_slug($data['name']));
                    } catch (\Exception $ex) {

                    }
                }
            }
            $data['image_extra'] = '|' . implode('|', $image_extra_arr) . '|';
        }
        return $data;
    }

    /**
     * Lưu ảnh trong phần nội dung về server
     */
    public function saveImgInContent($content, $content_doom, $path = 'uploads/')
    {
        //  Tìm và lấy các thẻ <img
        $img_doom_arr = $content_doom->find('img');
        foreach ($img_doom_arr as $img_doom) {
            $img_doom_src2 = false;

            // lấy link ảnh trong data-src hay trong src
            if ($img_doom->getAttribute('data-src') !== false) {
                $img_doom_src = @$img_doom->getAttribute('data-src');
                $img_doom_src2 = @$img_doom->getAttribute('src');
            } else {
                $img_doom_src = trim(@$img_doom->getAttribute('src'));
            }

            //  Xóa các ký tự thừa trong link ảnh
            $img_doom_src = explode('?', $img_doom_src)[0];

            if ($img_doom_src != '') {
                //  Gắn tên miền vào link ảnh
                $img_src = $this->attachDomainToLink($img_doom_src);

                //  Lưu link ảnh
                $img_src = CommonHelper::saveFile($img_src, $path);

                //  Thay link ảnh mới ở server mình vào link ảnh cũ ở server web nguồn
                $content = str_replace($img_doom_src, '/public/filemanager/userfiles/' . $img_src, $content);
                if ($img_doom_src2) {
                    $content = str_replace($img_doom_src2, '/public/filemanager/userfiles/' . $img_src, $content);
                }
            }
        }
        return $content;
    }

    /**
     * Xóa các ký tự thừa trong nội dung
     */
    public function cleanContent($content, $html)
    {

        return $content;
    }

    /**
     * Gắn tên miền vào link
     */
    public function attachDomainToLink($link)
    {
        if (strpos($link, 'http') !== false) {
            return $link;
        }

        if (substr($link, 0, 2) == '//') {
            return 'http:' . $link;
        }

        //  Nếu link ko gắn domain thì gắn vào
        if (strpos($link, $this->_domain) === false) {
            //  Xóa 2 dấu // liên tiếp
            $link = preg_replace('/\/\//', '', $link);

            if (substr($link, 0, 1) == '/') {
                $link = substr($link, 1);
            }

            $link = $this->_website->domain . $link;
        }
        return $link;
    }

    public function cleanData($post_data)
    {
        return $post_data;
    }

    public function appendData($data, $cat_doom = false)
    {
        return $data;
    }
}
