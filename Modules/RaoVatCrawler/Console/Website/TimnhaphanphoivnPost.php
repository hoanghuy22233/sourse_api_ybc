<?php

namespace Modules\RaoVatCrawler\Console\Website;

use App\Http\Helpers\CommonHelper;
use App\Models\Province;
use Mail;
use Modules\RaoVatCrawler\Entities\Category;
use Modules\RaoVatCrawler\Entities\PropertieName;
use Modules\RaoVatCrawler\Entities\PropertieValue;
use Session;

class TimnhaphanphoivnPost extends CrawlPostBase
{

    protected $module = [
        'code' => 'post',
        'table_name' => 'posts',
        'label' => 'Bài viết',
        'modal' => '\Modules\RaoVatCrawler\Entities\Post',
    ];

    function __construct($website)
    {
        parent::__construct($website);

        if (Session::get('login_timnhaphanphoi') == null) {
            $this->loginSystem();
            Session::put('login_timnhaphanphoi', true);
        }
    }

    private function loginSystem()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://timnhaphanphoi.vn/account/logon");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        $rs = str_get_html(curl_exec($ch));
        $reqFields = array(
//            "__VIEWSTATE" => $rs->find("input[id=__VIEWSTATE]", 0)->value,
//            "__VIEWSTATEGENERATOR" => $rs->find("input[id=__VIEWSTATEGENERATOR]", 0)->value,
//            "__EVENTVALIDATION" => $rs->find("input[id=__EVENTVALIDATION]", 0)->value,
            'UserName' => 'kisyrua',
            'Password' => 'ruatien',
//            'ctl00$ucLogin1$btLogin' => 'Đăng nhập',
//            'ctl00$txtKey' => ''
        );

        curl_setopt($ch, CURLOPT_URL, "http://khosachnoi.com");
        curl_setopt($ch, CURLOPT_POST, count($reqFields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($reqFields));
        curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie_khosachnoicom.txt');
        $rs = str_get_html(curl_exec($ch));
    }

    public function appendData($data, $cat_doom = false)
    {
        //  Thuộc tính
        if (isset($data['proprerties_id'])) {
            foreach ($data['proprerties_id'] as $name => $value) {
                if ($value != '') {
                    if (strpos($name, 'Mã tin đăng') !== false) {
                        $data['crawl_code'] = $value;
                    } elseif (strpos($name, 'Ngành hàng') !== false) {
                        $db_item = Category::where('name', $value)->first();
                        if (!is_object($db_item)) {
                            $db_item = new Category();
                            $db_item->name = $value;
                            $db_item->type = 10;
                            $db_item->slug = str_slug($value, '-');
                            $db_item->parent_id = $cat_doom->category_id;
                            $db_item->save();
                        }
                        $data['category_child_id'] = '|' . $db_item->id . '|';
                        $data['multi_cat'] = '|' . $db_item->parent_id . '|';
                    } elseif (strpos($name, 'KV ưu tiên') !== false) {
                        $db_item = Province::where('name', $value)->first();
                        if (!is_object($db_item)) {
                            $db_item = new Province();
                            $db_item->name = $value;
                            $db_item->save();
                        }
                        $data['province_id'] = $db_item->id;
                    } elseif (strpos($name, 'Tên liên lạc') !== false) {
                        $data['contact_name'] = $value;
                    } elseif (strpos($name, 'Điện thoại') !== false) {
                        $data['contact_tel'] = str_replace('tel:', '', $value);
                    } elseif (strpos($name, 'Gửi tin nhắn qua zalo') !== false) {
                        $data['contact_zalo'] = $value;
                    } elseif (strpos($name, 'Hết hạn') !== false) {
                        $data['deadline'] = date('Y-m-d', strtotime($value));
                    } elseif (strpos($name, 'Ngày đăng') !== false) {
                        $data['created_at'] = date('Y-m-d', strtotime($value));
                    }
                }
            }
            unset($data['proprerties_id']);
        }

        $data['post_type'] = $cat_doom->post_type;
        $data['source'] = 'Timnhaphanphoi.vn';
//        dd($data);
        return $data;
    }

    /**
     * Lấy thông tin Bài viết từ link Bài viết
     */
    public function getDataPost($post_link)
    {
        $html = file_get_html($post_link);

        $data = [];

        $data = $this->getTags($data, $html);

        $data = $this->getYeuCauTaiChinh($data, $html);

        $data = $this->getVideo($data, $html);

        $data = $this->getName($data, $html);

        $data = $this->getImage($data, $html);

        $data = $this->getIntro($data, $html);

        $data = $this->getContent($data, $html);

        $data = $this->getAttribute($data, $html);

        return $data;
    }

    public function updatePost($post, $data)
    {
        print "        => Updated post " . $post->id . ':' . $post->name . "\n";
        return true;
    }

    public function getTags($data, $html) {
        $doom = $html->find('#tags a');
        if ($doom != null) {
            $data['tags'] = '';
            foreach ($doom as $v) {
                $data['tags'] .= $v->innertext . ',';
            }
            $data['tags'] = substr($data['tags'], 0, -1);
        }
        return $data;
    }

    public function getYeuCauTaiChinh($data, $html) {
        try {
            $doom = $html->find('#detail ul.info li');
            foreach ($doom as $d) {
                if (strpos($d->find('strong', 0)->innertext, 'Chính sách hỗ trợ') !== false) {
                    $data['chinh_sach_ho_tro'] = $d->find('span', 0)->innertext;
                } elseif(strpos($d->find('strong', 0)->innertext, 'Yêu cầu tài chính') !== false) {
                    $data['yeu_cau_tai_chinh'] = $d->find('span', 0)->innertext;
                } elseif(strpos($d->find('strong', 0)->innertext, 'Tài chính') !== false) {
                    $data['yeu_cau_tai_chinh'] = $d->find('span', 0)->innertext;
                } elseif(strpos($d->find('strong', 0)->innertext, 'Mặt bằng') !== false) {
                    $data['mat_bang'] = $d->find('span', 0)->innertext;
                }
            }
        } catch (\Exception $ex) {

        }
        return $data;
    }

    /**
     * Lấy các thuộc tính của Bài viết
     */
    public function getAttribute($data, $html)
    {
        $v = $html->find(@$this->_doom_setting->attributes);
//        dd($v);
        if ($v != null) {
            $data['proprerties_id'] = [];
            foreach ($v as $attribute) {
                if ($val = $attribute->find('th', 0) != null && $val = $attribute->find('td', 0) != null) {
                    $name = $attribute->find('th', 0)->innertext;
                    if ($name == 'Điện thoại' || $name == 'Gửi tin nhắn qua zalo') {
                        $val = $attribute->find('td a', 0)->getAttribute('href');
                    } else {
                        $val = $attribute->find('td', 0)->innertext;
                    }

                    $data['proprerties_id'][trim($name)] = trim($val);
                }
            }
        }
        return $data;
    }

    public function getVideo($data, $html)
    {
        $v = $html->find('#detailTabs2 #video .text-center', 0);
        if ($v != null) {
            $data['link_youtube'] = trim($v->innertext);
        }
        return $data;
    }


    /**
     * Lấy ảnh của Bài viết
     */
    public function getImage($data, $html)
    {
        $doom = $html->find('#hdProductId', 0);
        if ($doom != null) {
            $id = $doom->getAttribute('value');
            $subpath = $html->find('#hdSubpath' . $id, 0);
            if ($subpath != null) {
                $subpath = $subpath->getAttribute('value');
                $html2 = file_get_html('https://timnhaphanphoi.vn/AjaxData/GetImgs/' . $id . '?vn=_img&ym=' . $subpath . '&tit=');

                $images = $html2->find(@$this->_doom_setting->image);
                if ($images != null) {
                    $image_extra_arr = [];
                    foreach ($images as $k => $image) {
                        if ($k == 0) {
                            $data['image'] = '';
                            $data['image'] = trim(@$image->getAttribute('src'));

                            if ($data['image'] != '') {
                                $data['image'] = explode('?', $data['image'])[0];

                                $data['image'] = $this->attachDomainToLink($data['image']);

                                try {
                                    $data['image'] = CommonHelper::saveFile($data['image'], 'post/' . date('Y/m/d/') . str_slug($data['name']));
                                } catch (\Exception $ex) {

                                }
                            }
                        }

                        $image_extra_src = trim(@$image->getAttribute('src'));
                        $image_extra_src = explode('?', $image_extra_src)[0];
                        if ($image_extra_src != '') {

                            $image_extra_src = $this->attachDomainToLink($image_extra_src);
                            try {
                                $image_extra_arr[] = CommonHelper::saveFile($image_extra_src, 'post/' . date('Y/m/d/') . str_slug($data['name']));
                            } catch (\Exception $ex) {

                            }
                        }
                    }
                    $data['image_extra'] = '|' . implode('|', $image_extra_arr) . '|';
                }
            }
        }
        return $data;
    }
}
