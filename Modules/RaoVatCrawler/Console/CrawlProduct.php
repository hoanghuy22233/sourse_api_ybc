<?php

namespace Modules\RaoVatCrawler\Console;

use Illuminate\Console\Command;
use Modules\RaoVatCrawler\Console\Website\CrawlProductBase;
use Modules\RaoVatCrawler\Entities\LogCrawlProduct;
use Modules\RaoVatCrawler\Entities\Website;

class CrawlProduct extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'crawl:product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl Bài viết.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle($website_id = false, $test = false)
    {
        $websites = Website::with(['categories'])->where('type', 'product');
        if (!$test) {
            $websites = $websites->where('status', 1);
        }

        if ($website_id) {
            $websites = $websites->where('id', $website_id);
        };
        $websites = $websites->orderBy('updated_at', 'desc')->get();
        foreach ($websites as $website) {
            $log_crawl = new LogCrawlProduct();
            $log_crawl->website_id = $website->id;

            print "Crawl website " . $website->domain . "\n";
            $domain = str_replace('http://', '', $website->domain);
            $domain = str_replace('https://', '', $domain);
            $domain = str_replace('https://', '', $domain);
            $domain = str_replace('www.', '', $domain);
            $domain = preg_replace('/\//', '', $domain);
            $domain = preg_replace('/\./', '', $domain);
//            dd($domain);
            $domain = ucfirst($domain);
            $domain = $domain . 'Product';
//            dd('Modules\RaoVatCrawler\Console\\Website\\' . $domain);/

            if (class_exists('Modules\RaoVatCrawler\Console\Website\\' . $domain)) {
                $namespace = 'Modules\RaoVatCrawler\Console\Website\\' . $domain;
                $crawler = new $namespace($website);
                $result = $crawler->crawlPageList($test);
            } else {
                $crawler = new CrawlProductBase($website);
                $result = $crawler->crawlPageList($test);
            }

            $log_crawl->total_updated = $result['total_updated'];
            $log_crawl->total_created = $result['total_created'];
            $log_crawl->save();
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
//            ['check', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
//            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
