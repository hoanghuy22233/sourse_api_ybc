<?php

namespace Modules\RaoVatCrawler\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\RaoVatCrawler\Console\CrawlPost;
use Modules\RaoVatCrawler\Console\CrawlProduct;
use Modules\RaoVatCrawler\Console\UpdateProduct;

class RaoVatCrawlerServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Cấu hình Core
//            $this->registerTranslations();
//            $this->registerConfig();
            $this->registerViews();
//            $this->registerFactories();
//            $this->loadMigrationsFrom(module_path('raovatcrawler', 'Database/Migrations'));

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }

        $this->commands($this->moreCommands);
    }

    protected $moreCommands = [
        CrawlProduct::class,
        UpdateProduct::class,
        CrawlPost::class,
    ];

    public function rendAsideMenu()
    {

        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('raovatcrawler::partials.aside_menu.dashboard_after_course');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('raovatcrawler.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'raovatcrawler'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/raovatcrawler');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/raovatcrawler';
        }, \Config::get('view.paths')), [$sourcePath]), 'raovatcrawler');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/raovatcrawler');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'raovatcrawler');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'raovatcrawler');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
