<?php

namespace Modules\RaoVatCrawler\Helpers;


class RaoVatCrawlerHelper
{


//$i = Tên ngắn gọn thay thế cho tên dài quá
    //$data = Tên sp. mục đích để phân biệt với tên các sp khác
    public static function saveFileSTBD($file, $path, $i, $product_name)
    {
        ini_set('max_execution_time', NULL);
        if (is_string($file)) {

            $name = explode('.', $file);
            $file_name_insert = $i . '_' . str_slug($product_name, '_') . '.' . end($name);
            $v = file_get_contents($file);
            file_put_contents(base_path() . '/public/filemanager/userfiles/' . $path . '/' . $file_name_insert , $v);

            return $path . '/' . $file_name_insert;
        } else {
            $file_name = $file->getClientOriginalName();
            $name = explode('.', $file_name);
            $file_name_insert = str_slug(str_replace(end($name), '', $file_name), '-') . '.' . end($name);
            $file->move(base_path() . '/public/filemanager/userfiles/' . $path, $file_name_insert);
            return $path . '/' . $file_name_insert;
        }
    }
}
