<?php

namespace Modules\ChinhSachBaoHanh\Http\Controllers;

use App\Http\Helpers\CommonHelper;
use Modules\ChinhSachBaoHanh\Models;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\ChinhSachBaoHanh\Rules\ValidRecaptcha;
use Modules\ChinhSachBaoHanh\Models\RegisGuarantee;

class ChinhSachBaoHanhController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('chinhsachbaohanh::index');
    }
    public function getSearchGuarantee(Request $request)
    {
//        $validatedData = $request->validate([
//            'g-recaptcha-response' => ['required', new ValidRecaptcha()]
//        ]);
        $data['guarantee'] = CommonHelper::getFromCache('search_guarantee_by_code_' . base64_encode(json_encode($request->all())));
        if (!$data['guarantee']) {
            $guarantee = new RegisGuarantee();
            if ($request->has('seri_regis') && $request->seri_regis != '') {
                $guarantee = $guarantee->where('seri',$request->seri_regis);
            }
//                if ($request->has('category_id') && $request->category_id != '') {
//                    $guarantee = $guarantee->where('category_id', $request->category_id);
//                }
            if ($request->has('email_regis') && $request->email_regis != '') {
                $guarantee = $guarantee->where('email', $request->email_regis)->orWhere('phone',$request->phone_regis);
            }
//                if ($request->has('phone_regis') && $request->phone_regis != '') {
//                    $guarantee = $guarantee->;
//                }
            $data['guarantee'] = $guarantee->first();
            CommonHelper::putToCache('search_guarantee_by_code_' . base64_encode(json_encode($request->all())), $data['guarantee']);


            return view('chinhsachbaohanh::regis_guarantee')->with($data);
        }
    }


    public function postRegisGuarantee(Request $request)
    {

//        $validatedData = $request->validate([
//            'g-recaptcha-response' => ['required', new ValidRecaptcha()]
//        ]);
        $regis = new RegisGuarantee();
        $regis->manufacturer_id = $request->manufacturer_id;
        $regis->date_buy = $request->date_buy;
        $regis->name = $request->name;
        $regis->province_id = $request->province;
        $regis->distric_id = $request->district;
        $regis->address = $request->address;
        $regis->phone = $request->phone;
        $regis->email = $request->email;
        $regis->seri = $request->seri;
        $regis->showroom_id = $request->showroom_id;

        $regis->save();
        return redirect()->back()->with('flash_message', 'Đăng ký thành công');

    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('chinhsachbaohanh::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('chinhsachbaohanh::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('chinhsachbaohanh::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
