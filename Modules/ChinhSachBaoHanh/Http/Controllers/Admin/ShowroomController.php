<?php

namespace Modules\ChinhSachBaoHanh\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;

class ShowroomController extends CURDBaseController
{
    protected $module = [
        'code' => 'showroom',
        'table_name' => 'showrooms',
        'label' => 'showrooms',
        'modal' => '\Modules\ChinhSachBaoHanh\Models\Showroom',
    ];
}
