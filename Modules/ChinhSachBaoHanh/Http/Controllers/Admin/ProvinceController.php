<?php

namespace Modules\ChinhSachBaoHanh\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;

class ProvinceController extends CURDBaseController
{
    protected $module = [
        'code' => 'province',
        'table_name' => 'province',
        'label' => 'Tỉnh/Thành',
        'modal' => '\Modules\ChinhSachBaoHanh\Models\Province',
    ];
}
