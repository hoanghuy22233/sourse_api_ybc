<?php

namespace Modules\ChinhSachBaoHanh\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\ChinhSachBaoHanh\Models\District;
use Modules\ChinhSachBaoHanh\Models\Province;
use Modules\ChinhSachBaoHanh\Models\Showroom;
use Validator;

class ChinhSachBaoHanhController extends CURDBaseController
{


    protected $module = [
        'code' => 'chinh-sach-bao-hanh',
        'table_name' => 'regis_guarantee',
        'label' => 'Chính sách bảo hành',
        'modal' => '\Modules\ChinhSachBaoHanh\Models\RegisGuarantee',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên khách hàng'],
            ['name' => 'date_buy', 'type' => 'date_vi', 'label' => 'Ngày mua hàng'],
            ['name' => 'province_id', 'type' => 'custom','td' => 'chinhsachbaohanh::list.td.relation', 'object'=>'province', 'display_field'=>'name', 'label' => 'Tỉnh/Thành'],
            ['name' => 'distric_id', 'type' => 'custom','td' => 'chinhsachbaohanh::list.td.relation', 'object'=>'district', 'display_field'=>'name', 'label' => 'Quận/Huyện'],
            ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ'],
            ['name' => 'phone', 'type' => 'text', 'label' => 'SĐT'],
            ['name' => 'email', 'type' => 'text', 'label' => 'Email'],
            ['name' => 'seri', 'type' => 'text', 'label' => 'Số Seri'],
            ['name' => 'showroom_id', 'type' => 'custom','td' => 'chinhsachbaohanh::list.td.relation', 'object'=>'showroom', 'display_field'=>'name', 'label' => 'Nơi mua hàng'],
//            ['name' => 'manufacturer_id', 'type' => 'text', 'label' => 'Domain'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên', ],
                ['name' => 'date_buy', 'type' => 'date', 'class' => 'required', 'label' => 'Ngày mua hàng'],
                ['name' => 'province_id', 'type' => 'select2_ajax_model', 'class' => 'required', 'object' => 'province',
                    'label' => 'Tỉnh/Thành', 'model' => Province::class, 'display_field' => 'name', 'display_field2' => 'id'],
                ['name' => 'distric_id', 'type' => 'select2_ajax_model', 'object' => 'district', 'class' => 'required',
                    'label' => 'Quận/Huyện', 'model' => District::class, 'display_field' => 'name', 'display_field2' => 'id'],
                ['name' => 'address', 'type' => 'text', 'class' => 'required', 'label' => 'Địa chỉ'],
                ['name' => 'phone', 'type' => 'text', 'class' => 'required', 'label' => 'SĐT'],
                ['name' => 'email', 'type' => 'text', 'class' => 'required', 'label' => 'Email'],
                ['name' => 'seri', 'type' => 'text', 'class' => 'required', 'label' => 'Số Seri'],
                ['name' => 'showroom_id', 'type' => 'select2_ajax_model', 'class' => 'required', 'object' => 'showroom',
                    'label' => 'Nơi mua hàng', 'model' => Showroom::class, 'display_field' => 'name', 'display_field2' => 'id'],
            ],

        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên khách hàng',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'date_buy' => [
            'label' => 'Ngày mua',
            'type' => 'date',
            'query_type' => '='
        ],
//
        'province_id' => [
            'label' => 'Tỉnh/Thành',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'province',
            'model' => Province::class,
            'query_type' => '='
        ],
        'distric_id' => [
            'label' => 'Quận/Huyện',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'district',
            'model' => District::class,
            'query_type' => '='
        ],
        'address' => [
            'label' => 'Địa chỉ',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'phone' => [
            'label' => 'SĐT',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'email' => [
            'label' => 'Email',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'seri' => [
            'label' => 'Số Seri',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'showroom_id' => [
            'label' => 'Nơi mua hàng',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'showroom',
            'model' => Showroom::class,
            'query_type' => '='
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);
        return view('chinhsachbaohanh::list')->with($data);
    }
//
    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('chinhsachbaohanh::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {

                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);
            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('chinhsachbaohanh::edit')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }



    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
