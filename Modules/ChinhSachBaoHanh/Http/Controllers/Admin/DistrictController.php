<?php

namespace Modules\ChinhSachBaoHanh\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;

class DistrictController extends CURDBaseController
{
    protected $module = [
        'code' => 'district',
        'table_name' => 'district',
        'label' => 'Quận/Huyện',
        'modal' => '\Modules\ChinhSachBaoHanh\Models\District',
    ];
}
