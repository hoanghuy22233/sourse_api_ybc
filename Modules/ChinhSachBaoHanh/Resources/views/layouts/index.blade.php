@extends('frontend.layouts.layout_homepage')
@section('main_content')
    <div class="f" style="background:#fbfbfb;padding-bottom:30px;">
        @if(!empty($posts))
            @foreach($posts as $post)
                @php
                    $cate_get_by_post = CommonHelper::getFromCache('get_category_by_post');
                    if(!$cate_get_by_post){
                        $cate_get_by_post = App\Models\Category::whereIn('id', explode('|', $post->multi_cat))->first();
                        CommonHelper::putToCache('get_category_by_post', $cate_get_by_post);
                    }
                @endphp
                <div class="topic">
                    <div class="ddimg flexL" style="order:{{$post->oder}};" id="nb0">
                        <img class="lazy0" src="{{CommonHelper::getUrlImageThumb($post->image, 675, null) }}" alt="{{$post->name}}"/>
                    </div>
                    <div class="ddinfo padd1" style="order:@if($post->oder == 1) 2 @else 1 @endif;">
                        <h2>{{$post->name}}</h2>
                        <p>
                        <p>
                            <strong>{{$post->intro}}</strong><br/>
                            {!!  $post->content !!}
                        </p>
                        <a class="hxn"
                           href=" {{route('cate.list', ['slug' => @$cate_get_by_post->slug])}}">Xem
                            ngay</a>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    <div class="f" style="background:#fff;">
        <div class="b">
            <div class="f flexJus topic2">
                @if(!empty($category))
                    @foreach($category as $cate)
                        <a href="{{route('cate.list', ['slug' => $cate->slug])}}" title="{{$cate->name}}">
                            <img src="{{CommonHelper::getUrlImageThumb($cate->banner_sidebar, 215, null) }}"  alt="{{$cate->name}}"/>
                            <h2>{{$cate->name}}</h2>
                        </a>
                    @endforeach
                @endif
            </div>
        </div>
    </div>

@endsection