{{--<!DOCTYPE html>--}}
{{--<html lang="vi-vn" xml:lang="vi-vn">--}}
{{--<head>--}}
{{--    @include('frontend.partials.head_meta')--}}
{{--    @include('frontend.partials.header_script')--}}
{{--    <link rel="stylesheet" href="{{asset('public/frontend/css/custom2.css')}}?v={{ date('s') }}">--}}

{{--    @include('frontend.partials.menu_home')--}}
{{--    @include('frontend.partials.menu_cate_home')--}}
{{--</head>--}}
<style>
    .table-responsive {
        display: block;
        width: 100%;
        overflow-x: auto;
        -webkit-overflow-scrolling: touch;
    }
    .table {
        width: 100%;
        margin-bottom: 1rem;
        color: #212529;
    }
    table {
        border-collapse: collapse;
    }
    table thead th {
        vertical-align: bottom;
        border-bottom: 2px solid #dee2e6;
    }

    .table td, .table th {
        padding: .75rem;
        vertical-align: top;
        border-top: 1px solid #dee2e6;
    }
</style>
<div class="container">
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
{{--                <th>Danh mục</th>--}}
                <th>Số seri</th>
                <th>Ngày mua</th>
                <th>Cửa hàng</th>
                <th>Tỉnh/Thành phố</th>
                <th>Quận/huyện</th>
                <th>Tên khách hàng</th>
                <th>Địa chỉ</th>
                <th>SĐT</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                @php
                /*
                    $cats = CommonHelper::getFromCache('get_category_guarantee');
                      if (!$cats) {
                          $cats = \Modules\ChinhSachBaoHanh\Models\Category::where('status', 1)->where('id', @$guarantee->category_id)->first();
                          CommonHelper::putToCache('get_category_guarantee',$cats);
                      } */
                       $show_room = CommonHelper::getFromCache('get_showroom_guarantee');
                      if (!$show_room) {
                          $show_room = \Modules\ChinhSachBaoHanh\Models\Showroom::where('id', @$guarantee->showroom_id)->first();
                          CommonHelper::putToCache('get_showroom_guarantee',$show_room);
                      }
                       $provin = CommonHelper::getFromCache('get_province_guarantee');
                      if (!$provin) {
                          $provin = \Modules\ChinhSachBaoHanh\Models\Province::where('id', @$guarantee->province_id)->first();
                          CommonHelper::putToCache('get_province_guarantee',$provin);
                      }
                      $district = CommonHelper::getFromCache('get_district_guarantee');
                      if (!$district) {
                          $district = \Modules\ChinhSachBaoHanh\Models\District::where('id', @$guarantee->district_id)->first();
                          CommonHelper::putToCache('get_district_guarantee',$district);
                      }

                @endphp
{{--                <td>{{ $cats['name'] }}</td>--}}
                <td>{{ @$guarantee->seri }}</td>
                <td>{{ @$guarantee->date_buy }}</td>
                <td>{{ @$show_room->name }}</td>
                <td>{{ @$provin->name }}</td>
                <td>{{ @$district->name }}</td>
                <td>{{ @$guarantee->name }}</td>
                <td>{{ @$guarantee->address }}</td>
                <td>{{ @$guarantee->phone }}</td>
                <td>{{ @$guarantee->email }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

{{--@include('frontend.partials.footer')--}}
{{--</body>--}}
{{--</html>--}}