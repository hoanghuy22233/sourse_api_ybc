<!DOCTYPE html>
<html lang="vi-vn" xml:lang="vi-vn">
<head>
    @include('frontend.partials.head_meta')
    @include('frontend.partials.header_script')
    <link rel="stylesheet" href="{{asset('public/frontend/css/custom2.css')}}?v={{ date('s') }}">
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script>
        function onSubmit(token) {
            document.getElementById("demo-form").submit();
        }
    </script>
    @include('frontend.partials.menu_home')
    @include('frontend.partials.menu_cate_home')
</head>

<div class="w3-container">
    <div class="w3-bar w3-sand">
        <button class="w3-bar-item w3-button tablink w3-green" onclick="openCity(event,'search-guarantee')">TRA CỨU
            BẢO HÀNH
        </button>
        <button class="w3-bar-item w3-button tablink" onclick="openCity(event,'regis-guarantee')">ĐĂNG KÝ BẢO HÀNH
        </button>
    </div>
    <section>
        <div class="col-lg-12">
            @if(Session('flash_message'))
                <div class="alert alert-success">
                    {!! Session('flash_message') !!}
                </div>
            @endif
        </div>
    </section>
    <div class="w3-container w3-border city" id="search-guarantee">
        <div id="mainContent">
            <h2 class="page-title sm-block">
                <span>Tra cứu bảo hành</span>
            </h2>
                <fieldset>
                    <form class="form-horizontal" role="form" method="get" action="{{ route('searchGuarantee') }}">
                    <div class="row line-form">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="s2id_autogen1" class="col-md-4 control-label">
                                    Thương hiệu <span class="asterisk">*</span>
                                </label>
                                <div class="col-md-3 select-item">
                                    <select id="productType" class="select2-offscreen" name="manufacturer_id" required>
                                        <option value="">Chọn thương hiệu</option>
                                        @php
                                            $manus = CommonHelper::getFromCache('get_manufacturer');
                                              if (!$manus) {
                                                  $manus = \App\Models\Manufacturer::where('status', 1)->get();
                                                  CommonHelper::putToCache('get_manufacturer',$manus);
                                              }
                                        @endphp
                                        @foreach($manus as $manu)
                                            <option value="{{ $manu->id }}" required>{{ $manu->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="s2id_autogen1" class="col-md-4 control-label">
                                    Số seri <span class="asterisk">*</span>
                                </label>
                                <div class="col-md-3 select-item">
                                    <input type="text" name="seri_regis" placeholder="Nhập số seri sản phẩm"  value="{{ @$_GET['seri_regis'] }}" class="seri_product" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="s2id_autogen1" class="col-md-4 control-label">
                                    Email đăng ký: <span class="asterisk">*</span>
                                </label>
                                <div class="col-md-3 select-item">
                                    <input type="email" name="email_regis" placeholder="Nhập email đăng ký" value="{{ @$_GET['email_regis'] }}" class="seri_product" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="s2id_autogen1" class="col-md-4 control-label">
                                    SĐT đăng ký: <span class="asterisk">*</span>
                                </label>
                                <div class="col-md-3 select-item">
                                    <input type="text" placeholder="Nhập SĐT đăng ký" name="phone_regis" value="{{ @$_GET['phone_regis'] }}" class="seri_product" required>
                                </div>
                            </div>
                            <div class="g-recaptcha" id="feedback-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY')  }}"></div>
                            <div class="form-group mobile-button-wrapper">
                                <div class="col-md-8">
                                    <button class="btn btn-success" type="submit">Tra cứu</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <span class="asterisk">*</span>
                        <span class="asterisk-desc">: bắt buộc</span>
                    </div>
                    </form>
                </fieldset>
        </div>
    </div>
    <div id="regis-guarantee" class="w3-container w3-border city" style="display:none">
        <form action="{{ route('RegisGuarantee') }}" method="post">
            <div class="row line-form">
                <div class="col-sm-6">
                    <div class="row line-form">
                        <div class="col-sm-6">
                            <label>
                                Thương hiệu <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <select id="productType" class="select2-offscreen" name="manufacturer_id" required>
                                <option value="">Chọn thương hiệu</option>
                                @php
                                    $manus = CommonHelper::getFromCache('get_manafacturer');
                                      if (!$manus) {
                                          $manus = \App\Models\Manufacturer::where('status', 1)->get();
                                          CommonHelper::putToCache('get_manafacturer',$manus);
                                      }
                                @endphp
                                @foreach($manus as $manu)
                                    <option value="{{ $manu->id }}">{{ $manu->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row line-form">
                        <div class="col-sm-6">
                            <label>
                                Chọn tỉnh thành <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            @php
                                $provinces = CommonHelper::getFromCache('provinces');
                                if (!$provinces) {
                                    $provinces = \App\Models\Province::all();
                                    CommonHelper::putToCache('provinces', $provinces);
                                }
                            @endphp
                            <select name="province" id="province" class="styled select2-offscreen" title=""
                                    tabindex="-1">
                                <option value=""> Chọn tỉnh thành</option>
                                @foreach($provinces as $province)
                                    <option value="{{ $province->id }}">{{ $province->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row line-form">
                <div class="col-sm-6">
                    <div class="row line-form">
                        <div class="col-sm-6">
                            <label>
                                Số seri <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="seri" placeholder="Nhập số seri" class="seri_product" required required>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row line-form">
                        <div class="col-sm-6">
                            <label>
                                Chọn quận / huyện <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            @php
                                $districts = CommonHelper::getFromCache('district');
                            if (!$districts) {
                                $districts = \App\Models\District::all();
                                CommonHelper::putToCache('district', $districts);
                            }
                            @endphp
                            <select name="district" id="district" class="styled select2-offscreen" title=""
                                    tabindex="-1">
                                <option value=""> Chọn quận / huyện</option>
                                @foreach($districts as $district)
                                    <option value="{{ $district->id }}">{{ $district->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row line-form">
                <div class="col-sm-6">
                    <div class="row line-form">
                        <div class="col-sm-6">
                            <label>
                                Ngày mua hàng <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="date" name="date_buy" class="seri_product" required>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row line-form">
                        <div class="col-sm-6">
                            <label>
                                Địa chỉ <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="address" placeholder="Nhập số nhà, tên đường" class="seri_product" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row line-form">
                <div class="col-sm-6">
                    <div class="row line-form">
                        <div class="col-sm-6">
                            <label>
                                Nơi mua hàng <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            @php
                                $show_rooms = CommonHelper::getFromCache('get_showroom');
                                  if (!$show_rooms) {
                                      $show_rooms = \App\Models\Showroom::select(['id','location','address'])->groupBy('location')->get();
                                      CommonHelper::putToCache('get_showroom',$show_rooms);
                                  }
                            @endphp
                            <select id="address-buy" class="select2-offscreen" style="margin-top: 10px" name="showroom_id" required>
                                <option value="">Chọn đại lý</option>
                                @foreach($show_rooms as $show_room)
                                    <option value="{{ $show_room->id }}">{!! $show_room->address !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row line-form">
                        <div class="col-sm-6">
                            <label>
                                SĐT <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="phone" placeholder="Nhập SĐT" class="seri_product" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row line-form">
                <div class="col-sm-6">
                    <div class="row line-form">
                        <div class="col-sm-6">
                            <label>
                                Họ và tên <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="name" placeholder="Nhập họ tên" class="seri_product" required>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row line-form">
                        <div class="col-sm-6">
                            <label>
                                Email <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6">
                            <input type="email" name="email" placeholder="Nhập email" class="seri_product" required>
                        </div>
                    </div>
                </div>
                <div class="g-recaptcha" id="feedback-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY')  }}"></div>
                <div class="form-group mobile-button-wrapper">
                    <div class="col-md-8">
                        <button class="btn btn-success" type="submit" data-bind="click: onSearch">Đăng ký</button>
                    </div>
                </div>
                <div>
                    <span class="asterisk">*</span>
                    <span class="asterisk-desc">: bắt buộc</span>
                </div>
            </div>
        </form>
    </div>

</div>

</div>
</div>

<script>
    function openCity(evt, cityName) {
        var i, x, tablinks;
        x = document.getElementsByClassName("city");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" w3-green", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " w3-green";
    }
</script>
<script>
    $(document).ready(function () {
        $('#province').on('change', function () {
            var url = 'province/' + $(this).val() + '/district/';
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $('#district').html(data);
                }
            })
        });

    });
    $(document).ready(function () {
        $('#address-province').on('change', function () {
            var url = 'province/' + $(this).val() + '/district/';
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $('#district').html(data);
                }
            })
        });

    });
</script>
<style>
    .templatingSelect2 {
        width: 300px;
    }

    h4 {
        margin-left: 10px;
    }
</style>

@include('frontend.partials.footer')
</body>
</html>