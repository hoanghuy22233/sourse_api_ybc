
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
    .w3-sand, .w3-hover-sand:hover {
        color: #000 !important;
        background-color: #fdf5e6 !important;
    }

    .w3-bar {
        width: 100%;
        overflow: hidden;
    }

    .w3-bar:before, .w3-bar:after {
        content: "";
        display: table;
        clear: both;
    }

    .w3-green, .w3-hover-green:hover {
        color: #fff !important;
        background-color: #4CAF50 !important;
    }

    .w3-bar .w3-button {
        white-space: normal;
    }

    .w3-bar .w3-bar-item {
        padding: 8px 16px;
        float: left;
        width: auto;
        border: none;
        display: block;
        outline: 0;
    }

    .w3-bar:after {
        content: "";
        display: table;
        clear: both;
    }

    section {
        margin-top: 10px;
        margin-bottom: 10px;
    }


    .form-group {
        display: flex;
        margin: 20px 0;
    }

    label.control-label {
        width: 35%;
        text-align: right;
        padding: 8px 0;
        padding-right: 50px;
    }

    .select-item {
        width: 65%;
        text-align: left;
    }

    .select-item>input,.select-item>select {
        padding: 8px;
        width: 25%;
    }

    span.asterisk {
        font-size: 12px;
        color: #D80600;
    }

    div.g-recaptcha,.tra_cuu {
        width: 65%;
        text-align: left;
    }
    .tra_cuu>button, .line-form-title>button {
        background-color: #28a745;
        border: none;
        padding: 10px;
        border-radius: 5px;
        color: #fff;
        font-weight: bold;
        cursor: pointer;
        text-transform: uppercase;
    }
    .w3-border {
        border: 1px solid #ccc!important;
    }
    fieldset{
        min-width: 0;
        border: none;
    }
    .form-content-1 {
        display: flex;
        margin: 30px 20px;
    }
    .line-form-title>label {
        padding: 10px;
    }
    .line-form-title>button {
        margin: auto;
    }
    .line-form-title, .line-form-input {
        width: 25%;
    }

    .line-form-input>input, .line-form-input>select {
        padding: 10px;
        width: 90%;
    }

    @media (max-width: 768px) {

        .form-group {
            flex-wrap: wrap;
        }
        .col-md-3.select-item>input, .col-md-3.select-item>select, .select-item{
            width: 100%;
        }
        label.control-label{
            width: 100%;
            text-align: left;
            padding-right: 0;
        }
        div.g-recaptcha, .tra_cuu{
            width: 100%;
            text-align: center;
        }
        .line-form-input>input, .line-form-input>select, .line-form-title, .line-form-input{
            width: 100%;
        }
        .line-form-title>label {
            margin: 10px 0;
            padding: 0;
            display: inline-block;
        }
        .form-content-1 {
            margin: 5px 10px;
            flex-wrap: wrap;
        }
        .line-form-input>input, .line-form-input>select {
            border: 1px solid #ccc;
        }
    }
</style>
<div class="w3-container">
    <div class="w3-bar w3-sand">
        <button class="w3-bar-item w3-button tablink w3-green" onclick="openCity(event,'search-guarantee')">TRA CỨU
            BẢO HÀNH
        </button>
        <button class="w3-bar-item w3-button tablink" onclick="openCity(event,'regis-guarantee')">ĐĂNG KÝ BẢO HÀNH
        </button>
    </div>
    <section>
        <div class="col-lg-12">
            @if(Session('flash_message'))
                <div class="alert alert-success">
                    {!! Session('flash_message') !!}
                </div>
            @endif
        </div>
    </section>
    <div class="w3-container w3-border city" id="search-guarantee">
        <div id="mainContent">
{{--            <h2 class="page-title sm-block">--}}
{{--                <span>Tra cứu bảo hành</span>--}}
{{--            </h2>--}}
            <fieldset>
                <form class="form-horizontal" role="form" method="get" action="{{ route('searchGuarantee') }}">
                    <div class="row line-form">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="s2id_autogen1" class="col-md-4 control-label">
                                    Thương hiệu <span class="asterisk">*</span>
                                </label>
                                <div class="col-md-3 select-item">
                                    <select id="productType" class="select2-offscreen" name="manufacturer_id" required>
                                        <option value="">Chọn thương hiệu</option>
                                        @php
                                            $manus = CommonHelper::getFromCache('get_manufacturer');
                                              if (!$manus) {
                                                  $manus = \Modules\ChinhSachBaoHanh\Models\Manufacturer::where('status', 1)->get();
                                                  CommonHelper::putToCache('get_manufacturer',$manus);
                                              }
                                        @endphp
                                        @foreach($manus as $manu)
                                            <option value="{{ $manu->id }}" required>{{ $manu->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="s2id_autogen1" class="col-md-4 control-label">
                                    Số seri <span class="asterisk">*</span>
                                </label>
                                <div class="col-md-3 select-item">
                                    <input type="text" name="seri_regis" placeholder="Nhập số seri sản phẩm"
                                           value="{{ @$_GET['seri_regis'] }}" class="seri_product" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="s2id_autogen1" class="col-md-4 control-label">
                                    Email đăng ký: <span class="asterisk">*</span>
                                </label>
                                <div class="col-md-3 select-item">
                                    <input type="email" name="email_regis" placeholder="Nhập email đăng ký"
                                           value="{{ @$_GET['email_regis'] }}" class="seri_product" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="s2id_autogen1" class="col-md-4 control-label">
                                    SĐT đăng ký: <span class="asterisk">*</span>
                                </label>
                                <div class="col-md-3 select-item">
                                    <input type="text" placeholder="Nhập SĐT đăng ký" name="phone_regis"
                                           value="{{ @$_GET['phone_regis'] }}" class="seri_product" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">

                                </label>
                            <div class="g-recaptcha" id="feedback-recaptcha"
                                 data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY')  }}"></div>
                            </div>
                            <div class="form-group mobile-button-wrapper">
                                <label class="control-label">

                                </label>
                                <div class="tra_cuu">
                                    <button class="btn btn-success" type="submit">Tra cứu</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <span class="asterisk">*</span>
                        <span class="asterisk-desc">: bắt buộc</span>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
    <div id="regis-guarantee" class="w3-container w3-border city" style="display:none">
        <form action="{{ route('RegisGuarantee') }}" method="post">
            <div class="row line-form form-content-1">
                        <div class="col-sm-6 line-form-title">
                            <label>
                                Thương hiệu <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6 line-form-input">
                            <select id="productType" class="select2-offscreen" name="manufacturer_id" required>
                                <option value="">Chọn thương hiệu</option>
                                @php
                                    $manus = CommonHelper::getFromCache('get_manafacturer');
                                      if (!$manus) {
                                          $manus = \Modules\ChinhSachBaoHanh\Models\Manufacturer::where('status', 1)->get();
                                          CommonHelper::putToCache('get_manafacturer',$manus);
                                      }
                                @endphp
                                @foreach($manus as $manu)
                                    <option value="{{ $manu->id }}">{{ $manu->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 line-form-title">
                            <label>
                                Chọn tỉnh thành <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6 line-form-input">
                            @php
                                $provinces = CommonHelper::getFromCache('provinces');
                                if (!$provinces) {
                                    $provinces = \Modules\ChinhSachBaoHanh\Models\Province::all();
                                    CommonHelper::putToCache('provinces', $provinces);
                                }
                            @endphp
                            <select name="province" id="province" class="styled select2-offscreen" title=""
                                    tabindex="-1">
                                <option value=""> Chọn tỉnh thành</option>
                                @foreach($provinces as $province)
                                    <option value="{{ $province->id }}">{{ $province->name }}</option>
                                @endforeach
                            </select>
                        </div>
            </div>
            <div class="row line-form form-content-1">
                        <div class="col-sm-6 line-form-title">
                            <label>
                                Số seri <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6 line-form-input">
                            <input type="text" name="seri" placeholder="Nhập số seri" class="seri_product" required
                                   required>
                        </div>
                        <div class="col-sm-6 line-form-title">
                            <label>
                                Chọn quận / huyện <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6 line-form-input">
                            @php
                                $districts = CommonHelper::getFromCache('district');
                            if (!$districts) {
                                $districts = \Modules\ChinhSachBaoHanh\Models\District::all();
                                CommonHelper::putToCache('district', $districts);
                            }
                            @endphp
                            <select name="district" id="district" class="styled select2-offscreen" title=""
                                    tabindex="-1">
                                <option value=""> Chọn quận / huyện</option>
                                @foreach($districts as $district)
                                    <option value="{{ $district->id }}">{{ $district->name }}</option>
                                @endforeach
                            </select>
                        </div>
            </div>
            <div class="row line-form form-content-1">
                        <div class="col-sm-6 line-form-title">
                            <label>
                                Ngày mua hàng <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6 line-form-input">
                            <input type="date" name="date_buy" class="seri_product" required>
                        </div>
                        <div class="col-sm-6 line-form-title">
                            <label>
                                Địa chỉ <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6 line-form-input">
                            <input type="text" name="address" placeholder="Nhập số nhà, tên đường" class="seri_product"
                                   required>
                        </div>
            </div>
            <div class="row line-form form-content-1">
                        <div class="col-sm-6 line-form-title">
                            <label>
                                Nơi mua hàng <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6 line-form-input">
                            @php
                                $show_rooms = CommonHelper::getFromCache('get_showroom');
                                  if (!$show_rooms) {
                                      $show_rooms = Modules\ChinhSachBaoHanh\Models\Showroom::select(['id','location','address'])->groupBy('location')->get();
                                      CommonHelper::putToCache('get_showroom',$show_rooms);
                                  }
                            @endphp
                            <select id="address-buy" class="select2-offscreen" style="margin-top: 10px"
                                    name="showroom_id" required>
                                <option value="">Chọn đại lý</option>
                                @foreach($show_rooms as $show_room)
                                    <option value="{{ $show_room->id }}">{!! $show_room->address !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 line-form-title">
                            <label>
                                SĐT <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6 line-form-input">
                            <input type="text" name="phone" placeholder="Nhập SĐT" class="seri_product" required>
                        </div>
            </div>
            <div class="row line-form form-content-1">
                        <div class="col-sm-6 line-form-title">
                            <label>
                                Họ và tên <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6 line-form-input">
                            <input type="text" name="name" placeholder="Nhập họ tên" class="seri_product" required>
                        </div>
                        <div class="col-sm-6 line-form-title">
                            <label>
                                Email <span class="asterisk">*</span>
                            </label>
                        </div>
                        <div class="col-sm-6 line-form-input">
                            <input type="email" name="email" placeholder="Nhập email" class="seri_product" required>
                        </div>
                </div>
            <div class="row line-form form-content-1">
                <div class="line-form-title line-form-title-mobie">

                </div>
                <script type="text/javascript" async=""
                        src="https://www.gstatic.com/recaptcha/releases/qpy2aGtSgsYPZzCoYWjcaBCo/recaptcha__vi.js"></script>
                <script src="https://www.google.com/recaptcha/api.js"></script>
                <div class="g-recaptcha" id="feedback-recaptcha"
                     data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY')  }}"></div>
            </div>
            <div class="row line-form form-content-1">
                <div class="line-form-title line-form-title-mobie">

                </div>
                <div class=" line-form-title form-group mobile-button-wrapper">
                        <button class="btn btn-success" type="submit" data-bind="click: onSearch">Đăng ký</button>

                </div>
            </div>

                <div>
                    <span class="asterisk">*</span>
                    <span class="asterisk-desc">: bắt buộc</span>
                </div>
            </div>
        </form>
    </div>

</div>

<script>
    function openCity(evt, cityName) {
        var i, x, tablinks;
        x = document.getElementsByClassName("city");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" w3-green", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " w3-green";
    }
</script>
<script>
    $(document).ready(function () {
        $('#province').on('change', function () {
            var url = 'province/' + $(this).val() + '/district/';
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $('#district').html(data);
                }
            })
        });

    });
    $(document).ready(function () {
        $('#address-province').on('change', function () {
            var url = 'province/' + $(this).val() + '/district/';
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    $('#district').html(data);
                }
            })
        });

    });
</script>

<script src="https://www.google.com/recaptcha/api.js"></script>
<script type="text/javascript" async="" src="https://www.gstatic.com/recaptcha/releases/qpy2aGtSgsYPZzCoYWjcaBCo/recaptcha__vi.js"></script>
