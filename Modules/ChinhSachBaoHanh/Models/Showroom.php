<?php

namespace Modules\ChinhSachBaoHanh\Models;

use Illuminate\Database\Eloquent\Model;

class Showroom extends Model
{
    protected $table = 'showrooms';
}
