<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace Modules\ChinhSachBaoHanh\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegisGuarantee extends Model {

    protected $table = 'regis_guarantee';

    use SoftDeletes;


    protected $dates = ['deleted_at'];

    protected $softDelete = true;

    public function manufacturer() {
        return $this->belongsTo(Manufacturer::class, 'manufacturer_id');
    }
    public function province() {
        return $this->belongsTo(Province::class, 'province_id');
    }
    public function district() {
        return $this->belongsTo(District::class, 'distric_id');
    }

    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }
    public function showroom() {
        return $this->belongsTo(Showroom::class, 'showroom_id');
    }
}