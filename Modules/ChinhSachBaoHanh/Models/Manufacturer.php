<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 7/31/2018
 * Time: 12:47 PM
 */

namespace Modules\ChinhSachBaoHanh\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Manufacturer extends Model
{
    protected $table = 'manufactureres';

    public function category() {
        return $this->belongsTo(Category::class, 'category_id');
    }

}