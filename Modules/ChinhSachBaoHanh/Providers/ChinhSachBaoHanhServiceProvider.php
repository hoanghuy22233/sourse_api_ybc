<?php

namespace Modules\ChinhSachBaoHanh\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ChinhSachBaoHanhServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
//        $this->registerTranslations();
//        $this->registerConfig();
        $this->registerViews();
//        $this->registerFactories();
//        $this->loadMigrationsFrom(module_path('Landingpage', 'Database/Migrations'));
        //  Nếu là trang admin thì gọi các cấu hình
//        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
//            //  Custom setting
//            $this->registerPermission();
//
//        }

        //  Cấu hình menu trái
        $this->rendAsideMenu();
    }

    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('chinhsachbaohanh::partials.aside_menu.dashboard_after_chinh_sach_bao_hanh');
        }, 1, 1);
    }
//    public function registerPermission()
//    {
//        \Eventy::addFilter('permission.check', function ($per_check) {
//            $per_check = array_merge($per_check, ['landingpage_view', 'landingpage_add', 'landingpage_edit', 'landingpage_delete', 'landingpage_publish',]);
//            return $per_check;
//        }, 1, 1);
//    }
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('Landingpage', 'Config/config.php') => config_path('landingpage.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('Landingpage', 'Config/config.php'), 'landingpage'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/chinhsachbaohanh');

        $sourcePath = module_path('chinhsachbaohanh', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/chinhsachbaohanh';
        }, \Config::get('view.paths')), [$sourcePath]), 'chinhsachbaohanh');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/landingpage');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'landingpage');
        } else {
//            $this->loadTranslationsFrom(module_path('Landingpage', 'Resources/lang'), 'landingpage');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
//            app(Factory::class)->load(module_path('Landingpage', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
