<?php
//Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
//    Route::group(['prefix' => 'landingpage'], function () {
//        Route::get('', 'Admin\LandingpageController@getIndex')->name('landingpage')->middleware('permission:landingpage_view');
//        Route::get('publish', 'Admin\LandingpageController@getPublish')->name('landingpage.publish')->middleware('permission:landingpage_publish');
//        Route::match(array('GET', 'POST'), 'add', 'Admin\LandingpageController@add')->middleware('permission:landingpage_add');
//        Route::get('delete/{id}', 'Admin\LandingpageController@delete')->middleware('permission:landingpage_delete');
//        Route::post('multi-delete', 'Admin\LandingpageController@multiDelete')->middleware('permission:landingpage_delete');
//        Route::get('search-for-select2', 'Admin\LandingpageController@searchForSelect2')->name('landingpage.search_for_select2')->middleware('permission:landingpage_view');
//    });
//    });
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'chinh-sach-bao-hanh'], function () {
        Route::get('', 'Admin\ChinhSachBaoHanhController@getIndex');
        Route::get('publish', 'Admin\ChinhSachBaoHanhController@getPublish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ChinhSachBaoHanhController@add');
        Route::get('delete/{id}', 'Admin\ChinhSachBaoHanhController@delete');
        Route::post('multi-delete', 'Admin\ChinhSachBaoHanhController@multiDelete');
        Route::get('search-for-select2', 'Admin\ChinhSachBaoHanhController@searchForSelect2');
        Route::get('{id}', 'Admin\ChinhSachBaoHanhController@update');
        Route::post('{id}', 'Admin\ChinhSachBaoHanhController@update');
    });
    Route::group(['prefix' => 'province'], function () {
        Route::get('search-for-select2', 'Admin\ProvinceController@searchForSelect2');
    });
    Route::group(['prefix' => 'district'], function () {
        Route::get('search-for-select2', 'Admin\DistrictController@searchForSelect2');
    });
    Route::group(['prefix' => 'showroom'], function () {
        Route::get('search-for-select2', 'Admin\ShowroomController@searchForSelect2');
    });
});
Route::get('chinh-sach-bao-hanh', 'ChinhSachBaoHanhController@index');
Route::post('regis-guarantee','ChinhSachBaoHanhController@postRegisGuarantee')->name('RegisGuarantee');
Route::get('tra-cuu-bao-hanh', 'ChinhSachBaoHanhController@getSearchGuarantee')->name('searchGuarantee');
