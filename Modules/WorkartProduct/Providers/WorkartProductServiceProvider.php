<?php

namespace Modules\WorkartProduct\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class WorkartProductServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Cấu hình core
//            $this->registerTranslations();
//            $this->registerConfig();
            $this->registerViews();
//            $this->registerFactories();
//            $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');


            //  Custom setting
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['product_view', 'product_add', 'product_edit', 'product_delete', 'product_publish',
                'promotional_code_view', 'promotional_code_add', 'promotional_code_edit', 'promotional_code_delete', 'promotional_code_publish',
                'gift_card_view', 'gift_card_add', 'gift_card_edit', 'gift_card_delete', 'gift_card_publish',
                'sending_marketing_view', 'sending_marketing_add', 'sending_marketing_edit', 'sending_marketing_delete', 'sending_marketing_publish',
                'clipart_view', 'clipart_add', 'clipart_edit', 'clipart_delete', 'clipart_publish',
                'clipart_category_view', 'clipart_category_add', 'clipart_category_edit', 'clipart_category_delete', 'clipart_category_publish',
                'campaign_view', 'campaign_add', 'campaign_edit', 'campaign_delete', 'campaign_publish',
                'product_printarea_view', 'product_printarea_add', 'product_printarea_edit', 'product_printarea_delete', 'product_printarea_publish',
                'product_variant_view', 'product_variant_add', 'product_variant_edit', 'product_variant_delete', 'product_variant_publish',
                ]);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('workartproduct::partials.aside_menu.dashboard_after_product');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('workartproduct.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'workartproduct'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/workartproduct');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/workartproduct';
        }, \Config::get('view.paths')), [$sourcePath]), 'workartproduct');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/workartproduct');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'workartproduct');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'workartproduct');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
