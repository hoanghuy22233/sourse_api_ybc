@extends(config('core.admin_theme').'.template')
@section('main')
    <form class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid {{ @$module['code'] }}"
          action="" method="POST"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="return_direct" value="save_continue" type="hidden">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                     id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg" style="">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Chỉnh sửa {{ $module['label'] }}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="/admin/{{ $module['code'] }}" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">
Come back</span>
                            </a>
                            <div class="btn-group">
                                @if(in_array('product_edit', $permissions))
                                    <button type="submit" class="btn btn-brand">
                                        <i class="la la-check"></i>
                                        <span class="kt-hidden-mobile">Save</span>
                                    </button>
                                    <button type="button"
                                            class="btn btn-brand dropdown-toggle dropdown-toggle-split"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_continue">
                                                    <i class="kt-nav__link-icon flaticon2-reload"></i>
                                                    Lưu và tiếp tục
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_exit">
                                                    <i class="kt-nav__link-icon flaticon2-power"></i>
                                                    Lưu & Thoát
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_create">
                                                    <i class="kt-nav__link-icon flaticon2-add-1"></i>
                                                    Save and create new
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-6">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Basic information
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                @foreach($module['form']['general_tab'] as $field)
                                    @php
                                        $field['value'] = @$result->{$field['name']};
                                    @endphp
                                    <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                         id="form-group-{{ $field['name'] }}">
                                        @if($field['type'] == 'custom')
                                            @include($field['field'], ['field' => $field])
                                        @else
                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                    <span class="color_btd">*</span>@endif</label>
                                            <div class="col-xs-12">
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                <span class="form-text text-muted">{!! @$field['des'] !!}</span>
                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                                    <div class="form-group-div form-group " id="form-group-value">
                                        <label for="value">Order </label>
                                        <div class="col-xs-6">
                                            <input type="text" name="value" class="form-control " id="value" value="" placeholder="">
                                            <span class="form-text text-muted"></span>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                                    <div class="form-group-div form-group " id="form-group-value">
                                        <label for="value">Gift card product </label>
                                        <div class="col-xs-6">
                                            <input type="text" name="value" class="form-control " id="value" value="" placeholder="">
                                            <span class="form-text text-muted"></span>
                                            <span class="text-danger"></span>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>

            <div class="col-xs-12 col-md-6">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Time
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                @foreach($module['form']['time_tab'] as $field)
                                    @php
                                        $field['value'] = @$result->{$field['name']};
                                    @endphp
                                    <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                         id="form-group-{{ $field['name'] }}">
                                        @if($field['type'] == 'custom')
                                            @include($field['field'], ['field' => $field])
                                        @else
                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                    <span class="color_btd">*</span>@endif</label>
                                            <div class="col-xs-12">
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                <span class="form-text text-muted">{!! @$field['des'] !!}</span>
                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                            </div>
                                        @endif


                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>


            <div class="col-xs-12 col-md-12">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Time
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="card-body pt-4">
                                <!--begin::Timeline-->
                                <div class="d-flex mt-4 mt-sm-0">
                                    <span class="font-weight-bold mr-4">Số dư</span>
                                    <div class="progress progress-xs mt-2 mb-2 flex-shrink-0 w-150px w-xl-250px">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 63%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="font-weight-bolder text-dark ml-4">78%</span>
                                </div>
                                <div class="timeline timeline-6 mt-3">
                                    <div class="timeline-item align-items-start">
                                        <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">Today
                                        </div>
                                        <div class="timeline-badge">
                                            <i class="fa fa-genderless text-warning icon-xl"></i>
                                        </div>
                                        <div class="font-weight-mormal font-size-lg timeline-content text-muted pl-3">
                                            Nguyễn Văn C trả $65 bằng gift card với hóa đơn #fasbass
                                        </div>
                                    </div>
                                    <div class="timeline-item align-items-start">
                                        <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">
                                            2 phút trước
                                        </div>
                                        <div class="timeline-badge">
                                            <i class="fa fa-genderless text-success icon-xl"></i>
                                        </div>
                                        <div class="timeline-content d-flex">
                                            <span class="font-weight-bolder text-dark-75 pl-3 font-size-lg">Nguyễn Văn A trả $69 bằng gift card</span>
                                        </div>
                                    </div>
                                    <div class="timeline-item align-items-start">
                                        <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">8 phút
                                            trước
                                        </div>
                                        <div class="timeline-badge">
                                            <i class="fa fa-genderless text-danger icon-xl"></i>
                                        </div>
                                        <div class="timeline-content font-weight-bolder font-size-lg text-dark-75 pl-3">
                                            Make deposit
                                            <a href="#" class="text-primary">USD 700</a>. to ESL
                                        </div>
                                    </div>
                                    <div class="timeline-item align-items-start">
                                        <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">1 ngày
                                            trước
                                        </div>
                                        <div class="timeline-badge">
                                            <i class="fa fa-genderless text-primary icon-xl"></i>
                                        </div>
                                        <div class="timeline-content font-weight-mormal font-size-lg text-muted pl-3">
                                            Indulging in poorly driving and keep structure keep great
                                        </div>
                                    </div>
                                    <div class="timeline-item align-items-start">
                                        <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">2 ngày
                                            trước
                                        </div>
                                        <div class="timeline-badge">
                                            <i class="fa fa-genderless text-danger icon-xl"></i>
                                        </div>
                                        <div class="timeline-content font-weight-bolder text-dark-75 pl-3 font-size-lg">
                                            New order placed
                                            <a href="#" class="text-primary">#XF-2356</a>.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </form>
@endsection
@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/form.css') }}">
    <style>
        .bg-success {
            background-color: #3699ff!important;
        }
        .progress.progress-xs {
            height: .5rem;
        }
        .w-xl-250px {
            width: 250px!important;
        }
        .w-150px {
            width: 150px!important;
        }
        .progress {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            height: 1rem;
            overflow: hidden;
            line-height: 0;
            font-size: .75rem;
            background-color: #ebedf3;
            border-radius: .42rem;
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .timeline.timeline-6:before {
            content: '';
            position: absolute;
            left: 51px;
            width: 3px;
            top: 0;
            bottom: 0;
            background-color: #ebedf3;
        }


        .timeline.timeline-6 {
            position: relative;
        }

        .card.card-custom > .card-body {
            padding: 2rem 2.25rem;
        }

        .timeline.timeline-6 .timeline-item {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            position: relative;
            margin-bottom: 1.7rem;
        }

        .align-items-start {
            -webkit-box-align: start !important;
            -ms-flex-align: start !important;
            align-items: flex-start !important;
        }

        .timeline.timeline-6 .timeline-item .timeline-label {
            width: 50px;
            -ms-flex-negative: 0;
            flex-shrink: 0;
            font-size: 1rem;
            font-weight: 500;
            position: relative;
            color: #3f4254;
        }

        .font-size-lg {
            font-size: 1.08rem;
        }

        .text-dark-75 {
            color: #3f4254 !important;
        }

        .font-weight-bolder {
            font-weight: 600 !important;
        }
    </style>
@endsection
@section('custom_footer')
    <script src="{{ asset(config('core.admin_asset').'/js/pages/crud/metronic-datatable/advanced/vertical.js') }}"
            type="text/javascript"></script>

    <script src="{{asset('public/ckeditor/ckeditor.js') }}"></script>
    <script src="{{asset('public/ckfinder/ckfinder.js') }}"></script>
    <script src="{{asset('public/libs/file-manager.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce_editor.js') }}"></script>
    <script type="text/javascript">
        editor_config.selector = ".editor";
        editor_config.path_absolute = "{{ (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" }}/";
        tinymce.init(editor_config);
    </script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/form.js') }}"></script>
@endsection
