<?php

namespace Modules\WorkartProduct\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPrintareas extends Model
{

    protected $table = 'product_printareas';

    protected $fillable = [
        'name','id'
    ];
    public $timestamps =false;

    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
