<?php
namespace Modules\WorkartProduct\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\WorkartUser\Models\User;

class PromotionalCode extends Model
{

    protected $table = 'promotional_codes';



    protected $fillable = [
        'code', 'type','connection', 'user_id','value', 'quantity','created_at', 'updated_at', 'promotion_limit'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
