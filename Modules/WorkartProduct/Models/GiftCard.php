<?php
namespace Modules\WorkartProduct\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class GiftCard extends Model
{

    protected $table = 'gift_cards';
    public $timestamps = false;



    protected $fillable = [
        'gift_card_code', 'user_id','expiration_date', 'check_expiration_date','value', 'note'
    ];


    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }


}
