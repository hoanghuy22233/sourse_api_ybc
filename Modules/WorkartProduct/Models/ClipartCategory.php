<?php

namespace Modules\WorkartProduct\Models;

use Illuminate\Database\Eloquent\Model;

class ClipartCategory extends Model
{

    protected $table = 'clipart_categories';

    protected $fillable = [
        'name','id'
    ];
    public $timestamps =false;
}
