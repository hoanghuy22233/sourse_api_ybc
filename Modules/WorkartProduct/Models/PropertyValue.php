<?php

namespace Modules\WorkartProduct\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyValue extends Model
{
    protected $table = 'properties_value';
    protected $guarded = [];
}
