<?php

namespace Modules\WorkartProduct\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{

    protected $table = 'product_variants';

    protected $fillable = [
        'id'
    ];
    public $timestamps =false;
    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
