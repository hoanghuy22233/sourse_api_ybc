<?php

namespace Modules\WorkartProduct\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{

    protected $table = 'campaigns';

    protected $fillable = [
        'title','id'
    ];
    public $timestamps =false;
}
