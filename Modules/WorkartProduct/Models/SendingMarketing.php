<?php
namespace Modules\WorkartProduct\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class SendingMarketing extends Model
{

    protected $table = 'sending_marketings';



    protected $fillable = [
        'email_customer_ids', 'email_type','email_source',
        'email_status','sms_customer_ids', 'sms_source', 'sms_type', 'sms_status', 'created_at'
    ];


    public function user() {
        return $this->belongsTo(\Modules\ThemeWorkart\Models\User::class, 'email_customer_ids','id');



}


}
