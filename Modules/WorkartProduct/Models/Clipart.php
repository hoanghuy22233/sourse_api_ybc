<?php

namespace Modules\WorkartProduct\Models;

use Illuminate\Database\Eloquent\Model;

class Clipart extends Model
{

    protected $table = 'cliparts';

    protected $fillable = [
        'name','id'
    ];
    public $timestamps =false;
}
