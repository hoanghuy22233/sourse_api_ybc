<?php

namespace Modules\KitCareBooking\Http\Controllers\Api\Admin;

use App\Mail\MailServer;
use App\Models\Admin;
use App\Models\Setting;
use Illuminate\Support\Facades\Mail;
use \Modules\A4iSeason\Models\Disease;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\KitCareBooking\Models\Booking;
use Modules\KitCareBooking\Models\ErrorCode;
use Modules\KitCareBooking\Models\Job;
use Modules\Theme\Models\Post;
use Validator;
use URL;

class BookingController extends Controller
{
    protected $_mailSetting;

    public function __construct()
    {
        $this->_mailSetting = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();
    }

    protected $module = [
        'code' => 'booking',
        'table_name' => 'bookings',
        'label' => 'Yêu cầu',
        'modal' => 'Modules\KitCareBooking\Models\Booking',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'product_id' => [
            'query_type' => '='
        ],
        'province_id' => [
            'query_type' => '='
        ],
        'district_id' => [
            'query_type' => '='
        ],
        'producer_id' => [
            'query_type' => '='
        ],
        'error_code_id' => [
            'query_type' => '='
        ],
        'time' => [
            'query_type' => '='
        ],
        'from_date' => [
            'query_type' => 'from_to_date'
        ],
        'to_date' => [
            'query_type' => 'from_to_date'
        ],
        'status' => [
            'query_type' => '='
        ],
        'bonus_services' => [
            'query_type' => '='
        ],


    ];

    protected $type_booking = [
        1 => 'Chưa tiếp nhận',
        2 => 'Đang yêu cầu',
        3 => 'Đã tiếp nhận',
        4 => 'Đã sửa xong',
        5 => 'Đã hoàn thành',
        6 => 'Bảo hành sau khi sửa',
        7 => 'Hủy',
        8 => 'Tư vấn bán mới ',
    ];
    protected $type_care = [
        0 => 'Không bảo hành',
        1 => '1 tháng',
        2 => '3 tháng',
        3 => '6 tháng',

    ];
    protected $type_payment_status = [
        0 => 'Chưa thanh toán',
        1 => 'Đã thanh toán',

    ];

    protected $type_bonus_services = [
        1 => 'Sửa DV',
        2 => 'Bảo hành MEEG',

    ];

    public function index(Request $request)
    {

        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Booking::leftJoin('admin', 'admin.id', '=', 'bookings.admin_id')
                ->selectRaw('bookings.id, bookings.product_id, bookings.producer_id, bookings.product_code, bookings.error_code_id, bookings.tel
                , bookings.province_id, bookings.district_id, bookings.image, bookings.note, bookings.time, bookings.admin_id, bookings.payment_status, bookings.cost, bookings.bonus_services
                , bookings.note_user, bookings.care, bookings.address, bookings.status, bookings.created_at, bookings.ktv_ids,admin.id as admin_id, admin.name as admin_name, admin.image as admin_image')->whereRaw($where);

            if (!is_null($request->get('tag_id'))) {
                $listItem = $listItem->where('tag_id', 'LIKE', '%|' . $request->tag_id . '|%');
            }

            //  Chỉ truy vấn ra các đơn mình được nhìn thấy
            $role_code = CommonHelper::getRoleName(\Auth::guard('api')->id(), 'name');
            if ($role_code == 'custommer') {
                $listItem = $listItem->where('bookings.admin_id', \Auth::guard('api')->id());
            } elseif ($role_code == 'technicians') {
                $listItem = $listItem->where('bookings.ktv_ids', 'like', "%|" . \Auth::guard('api')->id() . "|%");
            }

            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());

            foreach ($listItem as $k => $v) {
                $v->bonus_services = array_key_exists($v->bonus_services, $this->type_bonus_services) ? $this->type_bonus_services[$v->bonus_services] : '';

                $v->image = asset('public/filemanager/userfiles/' . $v->image);
                $v->user = [
                    'id' => $v->admin_id,
                    'name' => $v->admin_name,
                    'tel' => $v->tel,
                    'image' => asset('public/filemanager/userfiles/' . $v->admin_image),
                    'role_name' => CommonHelper::getRoleName($v->admin_id, 'name'),
                    'role_display_name' => CommonHelper::getRoleName($v->admin_id, 'display_name'),

                ];

                $v->province_name = @$v->province->name;
                $v->district_name = @$v->district->name;

                $v->producer_name = @$v->producer->name;

                $product_name = @$v->product->name;
                unset($v->product);
                $v->product = [
                    'id' => @$v->product_id,
                    'name' => $product_name,
                    'code' => $v->product_code
                ];

                unset($v->product_code);
                $a = Admin::select('id', 'name', 'image')
                    ->whereIn('id', explode('|', $v->ktv_ids))
                    ->get()->toArray();
                foreach ($a as $k1 => $v1) {
                    $a[$k1]['image'] = $a[$k1]['image'] != null ? asset('public/filemanager/userfiles/' . $a[$k1]['image']) : null;
                }
                $v->ktv = $a;




                $error = ErrorCode::select('id', 'name')
                    ->whereIn('id', explode('|', $v->error_code_id))
                    ->get()->toArray();
                $v->error_code = $error;

                $v->status_data = $v->status;
                $v->status = array_key_exists($v->status, $this->type_booking) ? $this->type_booking[$v->status] : '';

                $v->payment_status = array_key_exists($v->payment_status, $this->type_payment_status) ? $this->type_payment_status[$v->payment_status] : '';

                $v->care = array_key_exists($v->care, $this->type_care) ? $this->type_care[$v->care] : '';

                unset($v->province_id);
                unset($v->district_id);
                unset($v->province);
                unset($v->district);
                unset($v->admin_id);
                unset($v->admin_name);
                unset($v->producer);
                unset($v->product_id);
                unset($v->producer_id);
                unset($v->tel);
                unset($v->ktv_ids);
                unset($v->admin_image);
                unset($v->error_code_id);
            }
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = Booking::leftJoin('admin', 'admin.id', '=', 'bookings.admin_id')
                ->selectRaw('bookings.id, bookings.product_id, bookings.producer_id, bookings.product_code, bookings.error_code_id, bookings.tel, bookings.cost
                , bookings.province_id, bookings.district_id, bookings.image, bookings.image_present, bookings.note, bookings.time, bookings.admin_id, bookings.payment_status, bookings.bonus_services, bookings.note_admin, bookings.job_id
                , bookings.note_user, bookings.care, bookings.address, bookings.status, bookings.created_at, bookings.ktv_ids, bookings.image_extra, bookings.image_present2,admin.id as admin_id, admin.name as admin_name, admin.image as admin_image')
                ->where('bookings.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            if ($item->image != null) {
                $item->image = asset('public/filemanager/userfiles/' . $item->image);
            }
            foreach (explode('|', $item->image_present) as $img) {
                if ($img != '') {
                    $image_present[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->image_present = @$image_present;

            if ($item->image_extra != null) {
                $item->image_extra = asset('public/filemanager/userfiles/' . $item->image_extra);
            }
            foreach (explode('|', $item->image_present2) as $img) {
                if ($img != '') {
                    $image_present2[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->image_present2 = @$image_present2;


            $item->user = [
                'id' => $item->admin_id,
                'name' => $item->admin_name,
                'tel' => $item->tel,
                'image' => asset('public/filemanager/userfiles/' . $item->admin_image),
                'role_name' => CommonHelper::getRoleName($item->admin_id, 'name'),
                'role_display_name' => CommonHelper::getRoleName($item->admin_id, 'display_name'),
            ];


            $a = Admin::select('id', 'name', 'image')
                ->whereIn('id', explode('|', $item->ktv_ids))
                ->get()->toArray();
            foreach ($a as $k1 => $v1) {
                $a[$k1]['image'] = $a[$k1]['image'] != null ? asset('public/filemanager/userfiles/' . $a[$k1]['image']) : null;
            }
            $item->ktv = $a;


            $b = Job::select('id', 'name')
                ->whereIn('id', explode('|', $item->job_id))
                ->get()->toArray();
            $item->job = $b;


            $error = ErrorCode::select('id', 'name')
                ->whereIn('id', explode('|', $item->error_code_id))
                ->get()->toArray();
            $item->error_code = $error;


            $item->province_name = @$item->province->name;
            $item->district_name = @$item->district->name;
            $item->producer_name = @$item->producer->name;
            $product_name = @$item->product->name;
            unset($item->product);
            $item->product = [
                'id' => @$item->product_id,
                'name' => $product_name,
                'code' => $item->product_code
            ];
            $item->status_data = $item->status;

            $item->status = array_key_exists($item->status, $this->type_booking) ? $this->type_booking[$item->status] : '';
            $item->payment_status = array_key_exists($item->payment_status, $this->type_payment_status) ? $this->type_payment_status[$item->payment_status] : '';
            $item->bonus_services = array_key_exists($item->bonus_services, $this->type_bonus_services) ? $this->type_bonus_services[$item->bonus_services] : '';

            $item->care = array_key_exists($item->care, $this->type_care) ? $this->type_care[$item->care] : '';

            unset($item->province_id);
            unset($item->district_id);
            unset($item->province);
            unset($item->district);
            unset($item->admin_id);
            unset($item->admin_name);
            unset($item->producer);
            unset($item->product_id);
            unset($item->producer_id);
            unset($item->error_code_id);
            unset($item->job_id);
            unset($item->ktv_ids);
            unset($item->product_code);
            unset($item->admin_image);
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'product_code' => 'required',
//            'image' => 'required'
        ], [
            'product_code.required' => 'Required to enter mã sản phẩm',
//            'image.required' => 'Required to enter ảnh',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();
            $data['admin_name'] = str_slug(\Auth::guard('api')->user()->name, '-');


            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = $data['image_present'][] = CommonHelper::saveFile($image, 'booking/' . $data['admin_name'] . '/' . date('d-m-y'));
                    }
                } else {
                    $data['image'] = $data['image_present'][] = CommonHelper::saveFile($request->file('image'), 'booking/' . $data['admin_name'] . '/' . date('d-m-y'));
                }
                $data['image_present'] = implode('|', $data['image_present']);
            }

            $item = Booking::create($data);
            $email = explode(',', $this->_mailSetting['admin_emails']);
            $info = [];
            foreach ($email as $v) {
                $info['email'] = $v;
                $info['name'] = @Admin::where('email', $v)->first()->name;
                $info['booking_id'] = $item->id;
                $this->SendMailFullSlot($info);
            }
            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {

        $item = Booking::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = $data['image_present'][] = CommonHelper::saveFile($image, 'booking/' . $data['admin_name'] . '/' . date('d-m-y'));
                }
            } else {
                $data['image'] = $data['image_present'][] = CommonHelper::saveFile($request->file('image'), 'booking/' . $data['admin_name'] . '/' . date('d-m-y'));
            }
            $data['image_present'] = implode('|', $data['image_present']);
        }

        if ($request->has('image_extra')) {
            if (is_array($request->file('image_extra'))) {
                foreach ($request->file('image_extra') as $image) {
                    $data['image_extra'] = $data['image_present2'][] = CommonHelper::saveFile($image, 'booking');
                }
            } else {
                $data['image_extra'] = $data['image_present2'][] = CommonHelper::saveFile($request->file('image'), 'booking');
            }
            $data['image_present2'] = implode('|', $data['image_present2']);
        }


        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (Booking::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {

                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . ".time >= '" . date('Y-m-d', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . ".time <= '" . date('Y-m-d', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }

    public function SendMailFullSlot($info)
    {

        $user = (object)[
            'email' => $info['email'],
            'name' => $info['name'],
            'booking_id' => $info['booking_id'],
        ];
        $data = [
            'view' => 'kitcarebooking::mail.email',
            'user' => $user,
            'name' => $this->_mailSetting['mail_name'],
            'subject' => 'Đơn hàng mới!'
        ];
        Mail::to($user)->send(new MailServer($data));
        return true;
    }
}
