<?php

namespace Modules\WorkartProduct\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\CardBill\Models\User;
use Modules\WorkartProduct\Models\Product;
use Validator;

class SendingMarketingController extends CURDBaseController
{


    protected $module = [
        'code' => 'sending_marketing',
        'table_name' => 'sending_marketings',
        'label' => 'Sending marketings',
        'modal' => '\Modules\WorkartProduct\Models\SendingMarketing',
        'list' => [

            ['name' => 'created_at', 'type' => 'date_vi', 'label' => 'created at'],


        ],
        'form' => [
            'general_tab' => [
                ['name' => 'email_customer_ids', 'type' => 'select2_ajax_model', 'label' => 'Email', 'model' => \Modules\WorkartUser\Models\User::class,
                    'object' => 'user', 'display_field' => 'name','display_field2' => 'email', 'multiple' => true, 'des' => ''],
                ['name' => 'email_type', 'type' => 'select', 'options' =>
                    [
                        0 => 'Gửi báo cáo công việc',
                        1 => 'Gửi chúc mừng sinh nhật',
                        2 => 'Gửi thư nghỉ lễ tết',
                    ], 'class' => '', 'label' => 'Type of announcement'],
                ['name' => 'email_source', 'type' => 'select', 'options' =>
                    [
                        0 => 'Gửi báo cáo công việc',
                        1 => 'Gửi chúc mừng sinh nhật',
                        2 => 'Gửi thư nghỉ lễ tết',
                    ], 'class' => '', 'label' => 'email source'],
                ['name' => 'email_status', 'type' => 'select', 'options' =>
                    [
                        0 => 'Không kích hoạt',
                        1 => 'Kích hoạt',
                    ], 'class' => '', 'label' => 'email status'],

            ],

            'time_tab' => [
                ['name' => 'sms_customer_ids', 'type' => 'select2_ajax_model', 'label' => 'SMS', 'model' => \Modules\ThemeWorkart\Models\User::class,
                    'object' => 'user', 'display_field' => 'name','display_field2' => 'email', 'multiple' => true, 'des' => ''],
                ['name' => 'sms_type', 'type' => 'select', 'options' =>
                    [
                        0 => 'Gửi báo cáo công việc',
                        1 => 'Gửi chúc mừng sinh nhật',
                        2 => 'Gửi thư nghỉ lễ tết',
                    ], 'class' => '', 'label' => 'Type of announcement'],
                ['name' => 'sms_source', 'type' => 'select', 'options' =>
                    [
                        0 => 'Gửi báo cáo công việc',
                        1 => 'Gửi chúc mừng sinh nhật',
                        2 => 'Gửi thư nghỉ lễ tết',
                    ], 'class' => '', 'label' => 'sms source'],
                ['name' => 'sms_status', 'type' => 'select', 'options' =>
                    [
                        0 => 'Không kích hoạt',
                        1 => 'Kích hoạt',
                    ], 'class' => '', 'label' => 'sms status'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, mã, giá trị',
        'fields' => 'id, gift_card_code, value'
    ];

    protected $filter = [
        'gift_card_code' => [
            'label' => 'Mã',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'value' => [
            'label' => 'Giá trị',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'user_id' => [
            'label' => 'Khách hàng',
            'type' => 'select2_model',
            'display_field' => 'name',
            'model' => \Modules\WorkartUser\Models\User::class,
            'object' => 'user',
            'query_type' => '='
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('workartproduct::sending_marketing.list')->with($data);
    }
    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
//        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
//            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
//        }

        return $query;
    }
    public function add(Request $request)
    {
        try {

            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('workartproduct::sending_marketing.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'gift_card_code' => 'required'
                ], [
                    'gift_card_code.required' => 'Required to enter mã',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    unset($data['iframe']);
                    #
                    if ($request->has('email_customer_ids')) {
                        $data['email_customer_ids'] = '|' . implode('|', $request->email_customer_ids) . '|';
                    }
                    if ($request->has('sms_customer_ids')) {
                        $data['email_customer_ids'] = '|' . implode('|', $request->email_customer_ids) . '|';
                    }
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
//                        $this->adminLog($request,$this->model,'add');
                        $this->afterAddLog($request, $this->model);
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'New successfully created!');
                    } else {
                        CommonHelper::one_time_message('error', 'Im new. Please reload the page and try again!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }


                    if ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }
                    return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return redirect()->back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('workartproduct::sending_marketing.edit')->with($data);
            } else if ($_POST) {
                {
                    $validator = Validator::make($request->all(), [
                        'gift_card_code' => 'required'
                    ], [
                        'gift_card_code.required' => 'Required to enter mã',
                    ]);

                    if ($validator->fails()) {
                        return back()->withErrors($validator)->withInput();
                    } else {
                        $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                        //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;


                        foreach ($data as $k => $v) {
                            $item->$k = $v;
                        }
                        if ($item->save()) {
//                            $this->adminLog($request,$item,'edit');
                            CommonHelper::flushCache($this->module['table_name']);
                            CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                        } else {
                            CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                        }
                        if ($request->ajax()) {
                            return response()->json([
                                'status' => true,
                                'msg' => '',
                                'data' => $item
                            ]);
                        }

                        if ($request->return_direct == 'save_continue') {
                            return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                        } elseif ($request->return_direct == 'save_create') {
                            return redirect('admin/' . $this->module['code'] . '/add');
                        }

                        return redirect('admin/' . $this->module['code']);
                    }
                }
            }
        } catch
        (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {

            

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
//            $this->adminLog($request,$item,'publish');
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return redirect()->back();
//            }

            $item->delete();
//            $this->adminLog($request,$item,'delete');
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return redirect()->back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {

            

            $ids = $request->ids;
//            $this->adminLog($request,$ids,'multi_delete');
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
