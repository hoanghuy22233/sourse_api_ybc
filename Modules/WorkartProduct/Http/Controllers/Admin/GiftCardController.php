<?php

namespace Modules\WorkartProduct\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\CardBill\Models\User;
use Modules\WorkartProduct\Models\Product;
use Validator;

class GiftCardController extends CURDBaseController
{


    protected $module = [
        'code' => 'gift_card',
        'table_name' => 'gift_cards',
        'label' => 'Gift cards',
        'modal' => '\Modules\WorkartProduct\Models\GiftCard',
        'list' => [
            ['name' => 'gift_card_code', 'type' => 'text_edit', 'label' => 'Code'],
            ['name' => 'user_id', 'type' => 'relation', 'object' => 'user', 'display_field' => 'name', 'label' => 'User'],
            ['name' => 'value', 'type' => 'text', 'label' => 'Value', ],
            ['name' => 'expiration_date', 'type' => 'date_vi', 'label' => 'Expiration date'],


        ],
        'form' => [
            'general_tab' => [
                ['name' => 'gift_card_code', 'type' => 'text', 'class' => 'required', 'label' => 'Code', 'group_class' => 'col-md-6'],
                ['name' => 'value', 'type' => 'text', 'class' => '', 'label' => 'Value', 'group_class' => 'col-md-6'],
                ['name' => 'note', 'type' => 'textarea', 'class' => '', 'label' => 'Note',],

            ],

            'time_tab' => [
                ['name' => 'user_id', 'type' => 'select2_model', 'class'=>'', 'label' => 'User', 'model' => \Modules\WorkartUser\Models\User::class,
                    'display_field' => 'name', 'display_field2' => 'id',],
                ['name' => 'expiration_date',  'type' => 'date', 'label' => 'Expiration date'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, code, value',
        'fields' => 'id, gift_card_code, value'
    ];

    protected $filter = [
        'gift_card_code' => [
            'label' => 'Code',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'value' => [
            'label' => 'Value',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'user_id' => [
            'label' => 'User',
            'type' => 'select2_model',
            'display_field' => 'name',
            'model' => \Modules\WorkartUser\Models\User::class,
            'object' => 'user',
            'query_type' => '='
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('workartproduct::gift_card.list')->with($data);
    }
    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
//        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
//            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
//        }

        return $query;
    }
    public function add(Request $request)
    {
        try {

            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('workartproduct::gift_card.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'gift_card_code' => 'required'
                ], [
                    'gift_card_code.required' => 'Required to enter mã',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    unset($data['iframe']);
                    #

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
//                        $this->adminLog($request,$this->model,'add');
                        $this->afterAddLog($request, $this->model);
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'New successfully created!');
                    } else {
                        CommonHelper::one_time_message('error', 'Im new. Please reload the page and try again!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }


                    if ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }
                    return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return redirect()->back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('workartproduct::gift_card.edit')->with($data);
            } else if ($_POST) {
                {
                    $validator = Validator::make($request->all(), [
                        'gift_card_code' => 'required'
                    ], [
                        'gift_card_code.required' => 'Required to enter mã',
                    ]);

                    if ($validator->fails()) {
                        return back()->withErrors($validator)->withInput();
                    } else {
                        $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                        //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;


                        foreach ($data as $k => $v) {
                            $item->$k = $v;
                        }
                        if ($item->save()) {
//                            $this->adminLog($request,$item,'edit');
                            CommonHelper::flushCache($this->module['table_name']);
                            CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                        } else {
                            CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                        }
                        if ($request->ajax()) {
                            return response()->json([
                                'status' => true,
                                'msg' => '',
                                'data' => $item
                            ]);
                        }

                        if ($request->return_direct == 'save_continue') {
                            return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                        } elseif ($request->return_direct == 'save_create') {
                            return redirect('admin/' . $this->module['code'] . '/add');
                        }

                        return redirect('admin/' . $this->module['code']);
                    }
                }
            }
        } catch
        (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {

            

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
//            $this->adminLog($request,$item,'publish');
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return redirect()->back();
//            }

            $item->delete();
//            $this->adminLog($request,$item,'delete');
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return redirect()->back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {

            

            $ids = $request->ids;
//            $this->adminLog($request,$ids,'multi_delete');
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
