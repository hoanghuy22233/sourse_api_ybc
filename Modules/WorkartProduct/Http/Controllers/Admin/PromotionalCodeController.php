<?php

namespace Modules\WorkartProduct\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\WorkartProduct\Models\Product;
use Modules\WorkartUser\Models\User;
use Validator;

class PromotionalCodeController extends CURDBaseController
{


    protected $module = [
        'code' => 'promotional_code',
        'table_name' => 'promotional_codes',
        'label' => 'Promotional code',
        'modal' => '\Modules\WorkartProduct\Models\PromotionalCode',
        'list' => [
            ['name' => 'code', 'type' => 'text_edit', 'label' => 'Promotional code'],
            ['name' => 'type', 'type' => 'select', 'options' =>
                [
                    0 => 'Giá trị xác định (theo VNĐ)',
                    1 => 'Theo phần trăm',
                    2 => 'Miễn phí vận chuyển',
                ], 'class' => '', 'label' => 'Promotion type'],
            ['name' => 'value', 'type' => 'text', 'label' => 'Promotional value', ],

            ['name' => 'quantity', 'type' => 'text', 'label' => 'Quantity', ],
            ['name' => 'promotion_limit', 'type' => 'text', 'label' => 'promotion limit', ],
            ['name' => 'status', 'type' => 'select', 'options' => [
                0 => 'Ngừng khuyến mãi',
                1 => 'Đang khuyến mãi',
                2 => 'Đã kích hoạt',
                3 => 'Chưa kích hoạt',
            ], 'class' => 'required', 'label' => 'Status',],
            ['name' => 'time_start', 'type' => 'datetime_vi', 'label' => 'Time start'],


        ],
        'form' => [
            'general_tab' => [
                ['name' => 'code', 'type' => 'text', 'class' => 'required', 'label' => 'Promotional code',],
                ['name' => 'type', 'type' => 'select', 'options' =>
                    [
                        0 => 'Giá trị xác định (theo VNĐ)',
                        1 => 'Theo phần trăm',
                        2 => 'Miễn phí vận chuyển',
                    ], 'class' => '', 'label' => 'Promotion type', 'group_class' => 'col-md-6'],
                ['name' => 'connection', 'type' => 'select', 'options' =>
                    [
                        0 => 'Tất cả sản phẩm',
                        1 => 'Sản phẩm A',
                        2 => 'Sản phẩm B',
                    ], 'class' => '', 'label' => 'Select the product', 'group_class' => 'col-md-6'],

                ['name' => 'user_id', 'type' => 'select2_model', 'class'=>'', 'label' => 'Customer', 'model' => User::class,
                    'display_field' => 'name', 'display_field2' => 'id', 'group_class' => 'col-md-6'],
                ['name' => 'value', 'type' => 'text', 'class' => '', 'label' => 'Promotional value', 'group_class' => 'col-md-6'],
                ['name' => 'quantity', 'type' => 'text', 'class' => '', 'label' => 'Quantity promotional', 'group_class' => 'col-md-6'],
                ['name' => 'status', 'type' => 'select', 'options' => [
                    0 => 'Ngừng khuyến mãi',
                    1 => 'Đang khuyến mãi',
                    2 => 'Đã kích hoạt',
                    3 => 'Chưa kích hoạt',
                ], 'class' => 'required', 'label' => 'Status', 'value' => '1', 'group_class' => 'col-md-6'],
                ['name' => 'promotion_limit', 'type' => 'text', 'class' => '', 'label' => 'promotion limit',],


            ],

            'time_tab' => [
                ['name' => 'time_start', 'type' => 'custom','field'=>'workartproduct::form.fields.input_datetime-local', 'label' => 'time start'],
                ['name' => 'time_end',  'type' => 'custom','field'=>'workartproduct::form.fields.input_datetime-local', 'label' => 'time end'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, mã, Quantity',
        'fields' => 'id, code, quantity, promotion_limit'
    ];

    protected $filter = [
        'code' => [
            'label' => 'Mã sản phẩm',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'type' => [
            'label' => 'Promotion type',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Status',
                0 => 'Giá trị xác định (theo VNĐ)',
                1 => 'Theo phần trăm',
                2 => 'Miễn phí vận chuyển',
            ],
        ],
        'status' => [
            'label' => 'Status',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Status',
                0 => 'Ngừng khuyến mãi',
                1 => 'Đang khuyến mãi',
                2 => 'Đã kích hoạt',
                3 => 'Chưa kích hoạt',
            ],
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('workartproduct::promotional_code.list')->with($data);
    }
    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
//        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
//            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
//        }

        return $query;
    }
    public function add(Request $request)
    {
        try {

            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('workartproduct::promotional_code.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'code' => 'required'
                ], [
                    'code.required' => 'Required to enter mã',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    unset($data['iframe']);
                    #

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
//                        $this->adminLog($request,$this->model,'add');
                        $this->afterAddLog($request, $this->model);
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'New successfully created!');
                    } else {
                        CommonHelper::one_time_message('error', 'Im new. Please reload the page and try again!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }


                    if ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }
                    return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
//        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return redirect()->back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('workartproduct::promotional_code.edit')->with($data);
            } else if ($_POST) {
                {
                    $validator = Validator::make($request->all(), [
                        'code' => 'required'
                    ], [
                        'code.required' => 'Required to enter mã',
                    ]);

                    if ($validator->fails()) {
                        return back()->withErrors($validator)->withInput();
                    } else {
                        $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                        //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;


                        foreach ($data as $k => $v) {
                            $item->$k = $v;
                        }
                        if ($item->save()) {
//                            $this->adminLog($request,$item,'edit');
                            CommonHelper::flushCache($this->module['table_name']);
                            CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                        } else {
                            CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                        }
                        if ($request->ajax()) {
                            return response()->json([
                                'status' => true,
                                'msg' => '',
                                'data' => $item
                            ]);
                        }

                        if ($request->return_direct == 'save_continue') {
                            return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                        } elseif ($request->return_direct == 'save_create') {
                            return redirect('admin/' . $this->module['code'] . '/add');
                        }

                        return redirect('admin/' . $this->module['code']);
                    }
                }
            }
//        } catch
//        (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
////            CommonHelper::one_time_message('error', $ex->getMessage());
//            return redirect()->back()->withInput();
//        }
    }

    public function getPublish(Request $request)
    {
        try {

            

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
//            $this->adminLog($request,$item,'publish');
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return redirect()->back();
//            }

            $item->delete();
//            $this->adminLog($request,$item,'delete');
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return redirect()->back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {

            

            $ids = $request->ids;
//            $this->adminLog($request,$ids,'multi_delete');
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
