<?php

namespace Modules\ThemeOncode\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';

    protected $guarded = [];

    public function admin()
    {
        return $this->belongsTo(Admin::class,'admin_id');
    }
    public function student()
    {
        return $this->belongsTo(Student::class,'student_id');
    }
    public function course()
    {
        return $this->belongsTo(Course::class,'course_id');
    }

}
