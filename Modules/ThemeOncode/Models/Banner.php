<?php

/**
 * Banners Model
 *
 * Banners Model manages Banners operation. 
 *
 * @category   Banners
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license    
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\ThemeOncode\Models ;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
	protected $table = 'banners';

    public function getImageUrlAttribute()
    {
        return url('/').'/public/filemanager/userfiles/slides/'.@$this->attributes['image'];
    }
}
