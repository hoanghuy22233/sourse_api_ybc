<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace Modules\ThemeOncode\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model {

    protected $table = 'posts';

    use SoftDeletes;


    protected $fillable = [
        'name_vi', 'slug', 'user_id', 'intro', 'content', 'status', 'image', 'category_id', 'video', 'order_no',
        'important', 'data','type','day','show_home'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class,'admin_id');
    }
    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
}