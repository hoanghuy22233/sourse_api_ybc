<?php

namespace Modules\ThemeOncode\Models;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{

    protected $table = 'quizs';

    protected $guarded = [];

    protected $fillable = [
        'name', 'quizzes_id','content','file_audio','a','b','c','d','answer'
    ];

    public function quizzes()
    {
        return $this->belongsTo(Lesson::class, 'quizzes_id');
    }


}
