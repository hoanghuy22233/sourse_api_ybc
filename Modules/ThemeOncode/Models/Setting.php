<?php

namespace Modules\ThemeOncode\Models ;
use Illuminate\Database\Eloquent\Model;
class Setting extends Model
{
	protected $table = 'settings';
    public $timestamps = false;
}
