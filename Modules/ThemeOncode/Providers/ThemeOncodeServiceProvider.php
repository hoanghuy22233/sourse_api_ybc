<?php

namespace Modules\ThemeOncode\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\EduSettings\Providers\RepositoryServiceProvider;
use Modules\ThemeOncode\Models\Setting;
use App\Http\Helpers\CommonHelper;
use View;

class ThemeOncodeServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //  Cấu hình Core
//            $this->registerTranslations();
//            $this->registerConfig();
        $this->registerViews();
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Custom setting
            $this->registerPermission();
//            $this->registerFactories();
//            $this->loadMigrationsFrom(module_path('ThemeOncode', 'Database/Migrations'));
//            $this->app->register(RepositoryServiceProvider::class);

            //  Cầu hình frontend
            if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') === false) {
                $this->frontendSettings();
            }
            //  Cấu hình menu trái
            $this->rendAsideMenu();


            //  Cấu hình admin/setting
            /*if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/setting') !== false) {
                $this->customSetting();
            }*/
        }
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, [
                'order_view', 'order_add', 'order_edit', 'order_delete', 'order_publish',

            ]);
            return $per_check;
        }, 1, 1);
    }


    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('themeoncode::partials.aside_menu.dashboard_after_bill');
        }, 1, 1);
    }

    public function frontendSettings()
    {
        $settings = CommonHelper::getFromCache('frontend_settings', ['settings']);
        if (!$settings) {
            $settings = Setting::whereIn('type', ['general_tab', 'seo_tab', 'common_tab', 'homepage_tab', 'course_page_tab'])->pluck('value', 'name')->toArray();
            CommonHelper::putToCache('frontend_settings', $settings, ['settings']);
        }

        View::share('settings', $settings);
        return $settings;
    }

    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('ThemeOncode', 'Config/config.php') => config_path('themeoncode.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('ThemeOncode', 'Config/config.php'), 'themeoncode'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/themeedu');

        $sourcePath = module_path('ThemeOncode', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/themeedu';
        }, \Config::get('view.paths')), [$sourcePath]), 'themeoncode');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/themeedu');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'themeoncode');
        } else {
            $this->loadTranslationsFrom(module_path('ThemeOncode', 'Resources/lang'), 'themeoncode');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('ThemeOncode', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
