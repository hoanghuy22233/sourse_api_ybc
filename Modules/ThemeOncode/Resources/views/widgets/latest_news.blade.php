<div class="col-lg-12">
    <div class="sec-heading style9 text-center">
        <h2>{!! $widget->name !!}</h2>
    </div>
</div>
<?php
$where = @$config['where'];
$limit = @$config['limit'];
$data = CommonHelper::getFromCache('posts_' . json_encode($config), ['posts']);
if (!$data) {
    $data = \Modules\ThemeEdu\Models\Post::where('status', 1);
    if ($where != '') {
        $data = $data->whereRaw($where);
    }
    $data = $data->orderBy('order_no', 'desc')->orderBy('id', 'desc');
    if ($limit != '') {
        $data = $data->limit($limit);
    }
    $data = $data->get();
    CommonHelper::putToCache('posts_' . json_encode($config), $data, ['posts']);
}
?>
@foreach($data as $post)
    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="blog-grid">
            <figure><a href="/blog/{{$post->slug}}.html">
                    <img alt="{{@$post->name}}"
                         class="lazy"
                         data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image,361,200) }}"></a>
            </figure>
            <div class="blog-meta">
                <div class="blog-head">
                    <a href="/blog/{{$post->slug}}.html" title=""
                       class="date">{{date('d',strtotime($post->created_at))}} T{{ (int)date('m',strtotime($post->created_at)) }}</a>
                    <h4 class="blog-title"><a href="/blog/{{$post->slug}}.html"
                                              title=""> {!!$post->name!!}</a>
                    </h4>
                </div>
            </div>
        </div>
    </div>
@endforeach