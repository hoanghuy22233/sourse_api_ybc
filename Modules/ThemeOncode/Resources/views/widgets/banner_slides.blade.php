<style>
    .owl-stage {
        max-height: 500px;
        overflow: hidden;
    }

    .text-caro-meta {
        left: 27%;
        top: 35%;
    }
    @media(max-width: 768px) {
        .owl-carousel .owl-item img {
            max-width: 320px;
        }
        .text-caro-meta h1 a {
            font-size: 12px;
        }
        .text-caro-meta {
            left: 50%;
        }
    }
</style>
<?php
$data = CommonHelper::getFromCache('banners_' . json_encode($config), ['banners']);
if (!$data) {
    $data = \Modules\ThemeOncode\Models\Banner::where('location', @$config['location'])->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
    CommonHelper::putToCache('banners_' . json_encode($config), $data, ['banners']);
}
?>
@if(count($data) > 0)
    <div class="gap no-gap overlap22">
        <div class="text-caro owl-carousel owl-theme owl-loaded">
            <div class="owl-stage-outer">
                <div class="owl-stage"
                     style="transition: all 0s ease 0s; width: 11418px;">
                    @foreach($data as $k => $v)
                        <div class="owl-item {{ $k == 0 ? 'active' : '' }}" style="margin-right: 0px;">
                            <div class="unit-text-caro">
                                <img class="lazy" data-src="{{ asset('public/filemanager/userfiles/' . $v->image) }}"
                                     alt="{{ $v->name }}">
                                <div class="text-caro-meta">
                                    <span>{{ $v->name }}</span>
                                    <img class="lazy" data-src="{{ asset('public/frontend/themes/edu-ldp/images/icon-21.png') }}"
                                         alt="{{ $v->name }}">
                                    <h1><a href="{{ $v->link }}"
                                           title="{{ $v->name }}">{!! $v->intro !!}</a></h1>
                                    <p>
                                        {!! $v->content !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="owl-controls">
                <div class="owl-nav">
                    <div class="owl-prev" style="display: none;">prev</div>
                    <div class="owl-next" style="display: none;">next</div>
                </div>
                <div class="owl-dots" style="display: none;"></div>
            </div>
        </div>
    </div>
@endif