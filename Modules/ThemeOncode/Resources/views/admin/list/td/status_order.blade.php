<?php
//  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các đơn hàng của mình
if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
    $field['type'] = 'select';
} else {
    $field['type'] = 'status';
}
?>
@include(config('core.admin_theme').'.list.td.'.$field['type'])
