@extends(config('core.admin_theme').'.template')

@section('main')
    <?php
    $courses = \Modules\ThemeOncode\Models\Course::where('status', 1)->get();
    $leatures = \Modules\ThemeOncode\Models\Admin::where('status', 1);
    $role = \App\Models\RoleAdmin::where('role_id', 2)->pluck('admin_id')->toArray();
    $leatures = $leatures->whereIn('id', $role)->get();


    $admin_id = \Auth::guard('admin')->user()->id;

    if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
        //  Nếu ko có quyền xem hết => giảng viên => chỉ xem được dữ liệu của mình
        $lecturerLike = "lecturer_id like '%|" . $admin_id . "|%'";
        $lecturer_eq = "lecturer_id = " . $admin_id;
    } else {
        //  nếu ko phải giảng viên thì xem được dữ liệu lọc theo giảng viên
        if (@$_GET['admin_id'] != '' && @$_GET['admin_id'] != null) {
            $lecturerLike = "lecturer_id like '%|" . $_GET['admin_id'] . "|%'";
            $lecturer_eq = "lecturer_id = " . $_GET['admin_id'];
        } else {
            $lecturerLike = "1=1";
            $lecturer_eq = "1=1";
        }
    }

    if (@$_GET['course_id'] != null && @$_GET['course_id'] != '') {
        $course_eq = "course_id = " . $_GET['course_id'];
    } else {
        $course_eq = "1 = 1";
    }

    $whereRaw = '1=1';
    //  Mặc định lấy ngày đầu tháng
    $start_date = isset($_GET['start_date']) ? $_GET['start_date'] : date('Y-m-01');

    //  Mặc định lấy ngày hôm nay
    $end_date = isset($_GET['end_date']) ? $_GET['end_date'] : date('Y-m-d');
    $whereRaw .= " AND created_at >= '" . $start_date . " 00:00:00' AND created_at <= '" . $end_date . " 23:59:59'";
    ?>

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title bold uppercase">
                            Bộ lọc
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <form method="GET" action="" class="row align-items-center">
                        <div class="col-sm-3 col-md-3">
                            <label>Chọn giảng viên</label>
                            <?php
                            //  Truy vấn giảng viên
                            $leatures = \Modules\ThemeOncode\Models\Admin::where('status', 1);
                            $role = \App\Models\RoleAdmin::where('role_id', 2)->pluck('admin_id')->toArray();
                            $leatures = $leatures->whereIn('id', $role)->get();
                            ?>
                            <select class="form-control" name="admin_id">
                                <option value="">Chọn giảng viên</option>
                                @foreach($leatures as $l)
                                    <option value="{{$l->id}}" {{ @$_GET['admin_id'] == $l->id ? 'selected' : '' }}>{{$l->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-9 col-md-3 text-right filter-date">
                            <label>Khóa học:</label>
                            <?php
                            $courses = \Modules\ThemeOncode\Models\Course::whereRaw($lecturer_eq)->get();
                            ?>
                            <select class="form-control" name="course_id">
                                <option value="">Chọn khóa học</option>
                                @foreach($courses as $course)
                                    <option value="{{ $course->id }}" {{ @$_GET['course_id'] == $course->id ? 'selected' : '' }}>{{ $course->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-9 col-md-5 text-right filter-date">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Từ ngày:</label>
                                    <input type="date" class="form-control" placeholder="Từ ngày..." name="start_date"
                                           value="{{ $start_date }}">
                                </div>
                                <div class="col-lg-6">
                                    <label>Đến ngày:</label>
                                    <input type="date" class="form-control" style="" placeholder="Đến ngày..."
                                           name="end_date" value="{{ $end_date }}">
                                </div>
                            </div>
                        </div>
                        <div class="d-flex align-items-center col-12 mt-4">
                            <input class="loc" type="submit" value="Lọc"
                                   style="padding:7px 0px 7px 0px;width:70px;border:1px solid #ccc;border-radius: 4px;">
                            <a href="/admin/dashboard" style="display: block;margin-left: 10px;" class="loc">Xóa bộ
                                lọc</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        @include('themeoncode::dashboard.dashboard_content')
    </div>

@endsection

@section('custom_head')

    <link href="{{ asset('public/frontend/themes/oncode/admin/css/style.bundle.css') }}" rel="stylesheet"/>
    {{--          type="text/css">--}}
    <style type="text/css">
        .kt-datatable__cell > span > a.cate {
            color: #5867dd;
            margin-bottom: 3px;
            background: rgba(88, 103, 221, 0.1);
            height: auto;
            display: inline-block;
            width: auto;
            padding: 0.15rem 0.75rem;
            border-radius: 2px;
        }

        .paginate > ul.pagination > li {
            padding: 5px 10px;
            border: 1px solid #ccc;
            margin: 0 5px;
            cursor: pointer;
        }

        .paginate > ul.pagination span {
            color: #000;
        }

        .paginate > ul.pagination > li.active {
            background: #0b57d5;
            color: #fff !important;
        }

        .paginate > ul.pagination > li.active span {
            color: #fff !important;
        }

        .kt-widget12__desc, .kt-widget12__value {
            text-align: center;
        }

        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99 list_user
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <style type="text/css">
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }


        @media (max-width: 768px) {
            .kt-widget12 .kt-widget12__content .thong_ke_so {
                display: inline-block;
            }

            .thong_ke_so .col-sm-3 {
                display: inline-block;
                width: 50%;
                float: left;
                padding: 0;
                margin-bottom: 20px;
            }
        }

        .filter-date label {
            float: left;
        }

        /*.filter-date label {
            float: left;
            padding-top: 9px;
        }

        .filter-date input {
            width: 70%;
            float: left;
        }*/
    </style>

@endsection

@push('scripts')
    <script src="{{ url('public/libs/chartjs/js/Chart.bundle.js') }}"></script>
    <script src="{{ url('public/libs/chartjs/js/utils.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>


    @include('themeoncode::dashboard.dashboard_chart')

@endpush
