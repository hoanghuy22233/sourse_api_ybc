<div class="profile__section">
    <div class="row">
        <div class="col-lg-2">
            <div class="profile__author">
                <a class="profile__author-thumb"  href="/profile/{{ @$user->id }}">
                    <img alt="{{@@$user->name}}"
                         class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($user->image,155) }}">
                </a>
                <div class="profile__author-content"><a class="profile__author-name" href="#">Lưu Thế Vinh</a>
                    <div class="profile__author-country">Học viên </div>
                </div>
            </div>
        </div>
        <div class="col-lg-10">
            <ul class="profile__menu">
                <li><a @yield('khoahoc') href="/student/{{ @$user->id }}/khoa-hoc">Khóa học</a></li>
                <li><a href="#">Bài kiểm tra</a></li>
                <li><a href="#">Nhiệm vụ</a></li>
            </ul>
        </div>
    </div>
</div>


