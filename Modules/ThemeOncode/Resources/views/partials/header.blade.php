<div class="topbar">
    <div class="container">
        <div class="topbar__wrap">
            <div class="topbar__slogan">
                <p>ONCODE - Học viện đào tạo lập trình viên quốc tế</p>
            </div>
            <ul class="topbar__info">
                <li class="topbar__info-item"><a class="topbar__info-link" href="#"><i class="fal fa-map-marker-alt"></i><span>Hà Nội, Việt Nam</span></a></li>
                <li class="topbar__info-item"><a class="topbar__info-link" href="#"><i class="fal fa-phone-alt"></i><span>085 724 1019</span></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="header">
    <div class="container">
        <div class="header__box">
            <div class="header__top">
                <div class="header__top-left">
                    <div class="header__logo"><a class="header__logo-link" href="/" title="title"><img class="img-fluid lazy" data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}" alt="logo"></a></div>
                    <div class="header__search">
                        <form action="/tim-kiem"  method="get" id="search-form">
                            <div class="input-group">
                                <label for="live-search-bar">Tìm kiếm</label>
                                <input class="form-control live-search-bar" type="search" name="q" value="{{@$_GET['q']}}" placeholder="Nhập tên khóa học/giảng viên" id="live-search-bar">
                                <button class="search-btn" type="submit" aria-label><i class="fal fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="header__top-direction">
                    @if(@\Auth::guard('student')->check())

                        <div class="header__item-login"><a href="/dang-xuat">Đăng xuất</a></div>
                    @else
                        <div class="header__item"><a   class="header__item-link" href="/dang-ky"><i class="fal fa-unlock-alt"></i>Học miễn phí</a></div>
                    @endif

                    <div class="header__item">
                        <div class="header__item-cart"><a class="dropdown-toggle cart_anchor" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="far fa-shopping-cart"></i><span class="count-number">{{Cart::count()}} </span></a>
                            <ul class="dropdown-menu">
                                <div class="hidden" id="items-id-in-current-cart" style="display: none" data-all-cart-items=""></div>
                                <li class="cart-rgister">
                                    <ul class="list">
                                        @foreach(Cart::content() as $k=>$cart_item)
                                        <li class="list__item">
                                            <div class="col-12"><a class="list__item-link" href="#">
                                                    {{$cart_item->name}}<br><span class="price"> {{ number_format($cart_item->price,0,'','.') }} ₫</span></a></div>
                                        </li>
                                        @endforeach

                                        <li class="list__item">
                                            <div class="col-12">
                                                <div class="view"><a class="view__link" href="/gio-hang"><i class="fas fa-angle-double-right"></i>Xem Giỏ Hàng</a>
                                                    <p class="view__sum">Tổng cộng: {{ Cart::total(0, '', ',') }} ₫   </p>
                                                </div>
                                                <div class="btn__click">
                                                    <button class="btn__click-pay"><a href="/gio-hang" style="color:white;" title="title"> Thanh Toán </a></button>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="header__item d-flex">
                        @if(@\Auth::guard('student')->check())
                            <div class="header__item-login"><a href="/profile">{{ @\Auth::guard('student')->user()->name }}</a></div>

                            @else
                            <div class="header__item-login" ><a   href="/dang-nhap">Đăng nhập</a></div>
                        @endif

                    </div>
                </div>
            </div>
            <div class="header__menu">
                <?php
                $menus = CommonHelper::getFromCache('menu_main_menu', ['menus']);
                if (!$menus) {
                    $menus = \Modules\ThemeEduLDP\Models\Menu::where('status', 1)->where('location', 'main_menu')->whereNull('parent_id')->orderBy('order_no', 'desc')->orderBy('name', 'asc')->get();
                    CommonHelper::putToCache('menu_main_menu', $menus, ['menus']);
                }
                ?>
                <div class="header__menu-slogan"><span>Học xong đi làm ngay</span></div>
                    <ul class="header__menu-list">
                        @foreach($menus as $key => $menu)
                            <li class="header__menu-item {{$key == 0 ? 'active' : 0}}"><a class="header__menu-link" href="{{@$menu->url}}">{{@$menu->name}}</a></li>
                        @endforeach
                    </ul>
            </div>
        </div>
    </div>
</div>
<div class="header__mobile">
    <div class="header__mobile-wrap">
        <div class="header__mobile-btn"><i class="fal fa-bars"></i></div>
        <div class="header__mobile-logo"><a href="index.html"><img class="img-fluid" src="{{ asset('public/frontend/themes/oncode/images/layout/kyna_logo.png') }}" alt="logo"></a></div>
        <div class="header__mobile-cart">
            <div class="header__mobile-cartSearch"><i class="fal fa-search"></i></div>
            <div class="header__item-cart"><a href="#"><i class="far fa-shopping-cart"></i><span class="count-number">{{Cart::count()}} </span></a>
                <ul class="dropdown-menu">
                    <div class="hidden" id="items-id-in-current-cart" style="display: none" data-all-cart-items=""></div>
                    <li class="cart-rgister">
                        <ul class="list">
                            <li class="list__item">
                                <div class="col-12"><a class="list__item-link" href="#">
                                        Khóa học HTMl, CSS, JS cơ bản<br><span class="price"> 490.000 ₫</span></a></div>
                            </li>
                            <li class="list__item">
                                <div class="col-12">
                                    <div class="view"><a class="view__link"><i class="fas fa-angle-double-right"></i>Xem Giỏ Hàng</a>
                                        <p class="view__sum">Tổng cộng: 198000 ₫   </p>
                                    </div>
                                    <div class="btn__click">
                                        <button class="btn__click-pay">Thanh Toán  </button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header__mobile-bottom"><a class="header__item-link" href="#"><i class="fal fa-unlock-alt"></i>Học miễn phí</a><a class="header__item-link" href="#">Đăng nhập</a></div>
    <div class="header__mobile-search">
        <form class="search-form" action="" method="get">
            <div class="input-group">
                <input type="text" placeholder="Nhập tên khóa học/giảng viên">
            </div>
        </form>
    </div>
    <div class="header__mobile-menu">
        <div class="btn-close"><i class="fal fa-times"></i></div>
        <div class="header__mobile-inner"><a class="login" href="#">Đăng nhập</a>
            <ul class="header__mobile-list">
                <li class="header__list-item"><a class="header__list-link" href="index.html"><img src="{{ asset('public/frontend/themes/oncode/images/home/menu-icon-1.png') }}" alt=""><span class="name">Giới thiệu</span></a></li>
                <li class="header__list-item"><a class="header__list-link" href="courses.html"><img src="{{ asset('public/frontend/themes/oncode/images/home/menu-icon-1.png') }}" alt=""><span class="name">Khóa học</span>
                        <div class="child-icon"><i class="fas fa-caret-right"></i></div></a>
                    <div class="header__mobile-category">
                        <div class="go-back"><i class="fal fa-chevron-left"></i>Trở về</div>
                        <ul class="header__mobile-list">
                            <li class="header__list-item"><a class="header__list-link" href="index.html"><span>HTML, CSS</span></a></li>
                            <li class="header__list-item"><a class="header__list-link" href="index.html"><span>JS</span></a></li>
                            <li class="header__list-item"><a class="header__list-link" href="index.html"><span>ReactJS</span></a></li>
                        </ul>
                    </div>
                </li>
                <li class="header__list-item"><a class="header__list-link" href="jobs.html"><img src="{{ asset('public/frontend/themes/oncode/images/home/menu-icon-1.png') }}" alt=""><span class="name">Việc làm</span>
                        <div class="child-icon"><i class="fas fa-caret-right"></i></div></a>
                    <div class="header__mobile-category">
                        <div class="go-back"><i class="fal fa-chevron-left"></i>Trở về</div>
                        <ul class="header__mobile-list">
                            <li class="header__list-item"><a class="header__list-link" href="index.html"><span>HTML, CSS</span></a></li>
                            <li class="header__list-item"><a class="header__list-link" href="index.html"><span>JS</span></a></li>
                            <li class="header__list-item"><a class="header__list-link" href="index.html"><span>ReactJS</span></a></li>
                        </ul>
                    </div>
                </li>
                <li class="header__list-item"><a class="header__list-link" href="post.html"><img src="{{ asset('public/frontend/themes/oncode/images/home/menu-icon-1.png') }}" alt=""><span class="name">Bí quyết</span>
                        <div class="child-icon"><i class="fas fa-caret-right"></i></div></a>
                    <div class="header__mobile-category">
                        <div class="go-back"><i class="fal fa-chevron-left"></i>Trở về</div>
                        <ul class="header__mobile-list">
                            <li class="header__list-item"><a class="header__list-link" href="index.html"><span>HTML, CSS</span></a></li>
                            <li class="header__list-item"><a class="header__list-link" href="index.html"><span>JS</span></a></li>
                            <li class="header__list-item"><a class="header__list-link" href="index.html"><span>ReactJS</span></a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>