<?php
$banners = \Modules\ThemeOncode\Models\Banner::where('status', 1)->where('location', 'banner_slides')->get();
?>

<div class="slider">
    <div class="container-fluid">
        <div class="row">
            <div class="slider__wrap owl-carousel owl-theme">
                @foreach($banners as $banner)
                    <img
                            src="{{ asset('public/filemanager/userfiles/' . $banner->image) }}" alt="slider-img"
                            title="{{ $banner->name }}">
                @endforeach
            </div>
        </div>
    </div>
</div>