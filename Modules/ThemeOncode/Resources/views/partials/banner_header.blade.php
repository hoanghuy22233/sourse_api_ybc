<section>
    <div class="gray-bg">
        <div class="row">
            <div class="col-lg-12">
                <div class="featured-baner mate-black low-opacity" style="max-height: 150px; overflow: hidden;">
                    <img class="lazy" data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['banner_top'], null, null) }}"
                         alt="{{ @$settings['name'] }}">
                    <h3>{!! @$pageOption['meta_title'] !!}</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="banner__detail">
                    <h2 class="banner__detail-title"> <span class="title__text">{{ $course->name }}</span></h2>
                    <div class="banner__detail-rate"><span class="rate__items"><i class="fas fa-user"> </i><span>{{@$course->contact->count()}} học viên đăng ký</span></span>
                        <span class="rate__items"><i class="fas fa-play-circle"> </i><span> {{@$course->lesson->count()}} bài</span></span>
                        <span class="rate__items"><i class="fas fa-clock"> </i><span>8 giờ</span></span>

                    </div>
                    <div class="banner__detail-lever">
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <p class="lever__items"><i class="fas fa-layer-group"></i><span>Trình độ: {{$course->level}}</span></p>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <p class="lever__items"><i class="fas fa-infinity"></i><span>Thời hạn: {{$course->date_of_possession==0 ? 'Mãi mãi':$course->date_of_possession}}</span></p>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <p class="lever__items"><i class="fas fa-laptop-house"></i><span>Xem được trên {{$course->utilities}}</span></p>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <p class="lever__items"><i class="fas fa-graduation-cap"></i><span>Chứng chỉ: {{$course->certificate}}</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>