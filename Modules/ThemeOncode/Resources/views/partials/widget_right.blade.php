<?php
$data = CommonHelper::getFromCache('widgets_location_home_sidebar_right', ['widgets']);
if (!$data) {
    $data = \Modules\ThemeOncode\Models\Widget::select(['name', 'content'])
        ->where('location', 'home_sidebar_right')->where('status', 1)
        ->get();
    CommonHelper::putToCache('widgets_location_home_sidebar_right', $data, ['widgets']);
}
?>
@foreach($data as $v)
    <div class="widget">
        <h4 class="widget-title">{{$v->name}}</h4>
        {!! $v->content !!}
    </div><!-- twitter feed-->
@endforeach

