<link rel="shortcut icon" href="{{ asset('public/frontend/themes/oncode/images/layout/favicon.ico') }}">
<!-- Bootstrap-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<!-- Font Awesome CSS-->
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous">
<!-- Google Fonts-->
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&amp;display=swap" rel="stylesheet">
<!-- Owl Carousel CSS-->
<link href="{{ asset('public/frontend/themes/oncode/css/owl.carousel.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/frontend/themes/oncode/css/owl.theme.default.min.css') }}" rel="stylesheet">
<!-- Fancybox-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css">
<!-- Main CSS-->
<link href="{{ asset('public/frontend/themes/oncode/css/main.css') }}" rel="stylesheet">

{{--Custom--}}
<script src="{{ URL::asset('public/libs/jquery-3.4.0.min.js') }}"></script>