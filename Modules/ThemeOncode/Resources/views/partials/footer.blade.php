<?php
$data = CommonHelper::getFromCache('widget_footer', ['widgets']);
if (!$data) {
    $data = \Modules\ThemeEduLDP\Models\Widget::select('name', 'content', 'location')->where('status', 1)->whereIn('location', ['copyright', 'footer1', 'footer2', 'footer3', 'footer4'])
        ->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->get();
    CommonHelper::putToCache('widget_footer', $data, ['widgets']);
}

$footers = [];
foreach ($data as $v) {
    $footers[$v->location][] = $v;
}
//dd($footers);
?>
<div class="footer bgcLightGrey">
    <div class="footer__top">
        <div class="container">
            <div class="row">
                <div class="mb-3 col-sm-6 col-lg-3">
                    <div class="footer__top-hotline">
                        <h4 class="footer__title">Kết nối với Oncode</h4>
                        <ul class="footer__hotline-contact">
                            <li><span class="first"><i class="fal fa-phone"></i>Hotline</span><span class="second">0965863079</span></li>
                            <li><span class="first"><i class="fal fa-envelope"></i>Email</span><span class="second">hotro@oncode.edu.vn</span></li>
                        </ul>
                        <div class="footer__hotline-social"><a href="#" target="_blank"><img src="{{ asset('public/frontend/themes/oncode/images/layout/fb_icon_footer.png') }}" alt="fanpage"></a><a href="#" target="_blank"><img src="{{ asset('public/frontend/themes/oncode/images/layout/youtube_icon_footer.png') }}" alt="fanpage"></a><a href="#" target="_blank"><img src="{{ asset('public/frontend/themes/oncode/images/layout/zalo_icon_footer.png') }}" alt="fanpage"></a></div>
                    </div>
                </div>
                <div class="mb-3 col-sm-6 col-lg-3">
                    <div class="footer__top-info">
                        <h4 class="footer__title">Thông tin Oncode</h4>
                        <ul>
                            <li><a href="/khoa-hoc">Danh sách khóa học</a></li>
                            <li><a href="#">Câu hỏi thường gặp</a></li>
                            <li><a href="#">Cách thanh toán học phí</a></li>
                            <li><a href="#">Thông tin hữu ích</a></li>
                            <li><a href="#">VietnamWorks</a></li>
                            <li><a href="#">PRIMUS</a></li>
                        </ul>
                    </div>
                </div>
                <div class="mb-3 col-sm-6 col-lg-3">
                    <div class="footer__top-about">
                        <h4 class="footer__title">Về Oncode</h4>
                        <ul>
                            <li class="iconfeature"><a href="#">Giảng dạy tại Oncode</a></li>
                            <li><a href="#">Hợp tác với Oncode</a></li>
                            <li><a href="#">Đối tác phân phối</a></li>
                            <li><a href="#">Quy chế hoạt động sàn GDTMĐT</a></li>
                        </ul>
                    </div>
                </div>
                <div class="mb-3 col-sm-6 col-lg-3">
                    <div class="footer__top-fanpage">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Foncodeacademy&amp;tabs=272&amp;width=255&amp;height=220&amp;small_header=false&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=true&amp;appId=168015681053039" width="255" height="150" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="footer__address">
                        <div class="footer__address-text">
                            <p class="footer__address-copyright">&copy; 2020 - Bản quyền của Công ty TNHH Bigvio Media</p>
                            <p><span>Trụ sở chính: Số nhà 19 ngõ 77 phố Lãng Yên, phường Thanh Lương, quận Hai Bà Trưng, Tp Hà Nội</span>
                            <p>Giấy phép ĐKKD số 123456789 do Sở Kế hoạch và Đầu tư TP Hà Nội cấp</p>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 footer__info"><a class="col-lg-6" href="#" target="_blank"><img class="img-fluid" src="{{ asset('public/frontend/themes/oncode/images/layout/dathongbao.png') }}" alt="da thong bao"></a><a class="col-lg-6" href="#" target="_blank"><img class="img-fluid" src="{{ asset('public/frontend/themes/oncode/images/layout/logoCCDV.png') }}" alt="da dang ky"></a>
                    <ul>
                        <li><a href="#" title="title">Điều khoản dịch vụ </a></li>
                        <li><a href="#" title="title">Chính sách bảo mật</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Start Back to Top-->
<div class="back-to-top" id="back-to-top"><a href="#"><i class="fas fa-arrow-up"></i></a></div>
<!-- End Back to Top-->