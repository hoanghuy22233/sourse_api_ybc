<!DOCTYPE html>
<html lang="en">
<head>
    @include('themeoncode::partials.head_meta')
    @include('themeoncode::partials.head_script')

    {!! @$settings['frontend_head_code'] !!}
    @yield('head_script')
</head>
<body>
<div class="wrapper">
    @include('themeoncode::partials.header')
    @yield('main_content')
    @include('themeoncode::partials.footer')
</div>
@include('themeoncode::partials.footer_script')

{!! @$settings['frontend_body_code'] !!}
<style>
    .green-button{
        color: #FFF;
        background: #fb6a00;
        -webkit-box-shadow: 0 3px 1px #64950d;
        -moz-box-shadow: 0 3px 1px #64950d;
        box-shadow: 0 3px 1px #fb6a00;	border: 0;
        width: 100%;
        padding: 10px;-webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        display: block;
        text-decoration: none;
        text-align: center;
        font-size: 1.2em;

    }
</style>
</body>
</html>