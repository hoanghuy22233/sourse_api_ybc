<script>
    <?php
    $char = \Modules\ThemeOncode\Models\Order::selectRaw('Sum(price) as total_price, COUNT(id) as total_bill, MONTH(created_at) as month')
        ->where('status', 1)
        ->whereRaw($lecturer_eq)->whereRaw($course_eq);
    if (isset($_GET['start_date'])) {
        $char = $char->whereRaw($whereRaw);
    } else {
        $char = $char->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('-11 month')))->where('created_at', '<=', date('Y-m-d 23:59:00'));
    }

    $char = $char->groupBy(\DB::raw('MONTH(created_at)'))->orderBy('created_at', 'asc')->get();


    $char_user = \App\Models\Student::selectRaw('COUNT(id) as total, MONTH(created_at) as month')
        ->whereRaw($lecturerLike);
    if (isset($_GET['start_date'])) {
        $char_user = $char_user->whereRaw($whereRaw);
    } else {
        $char_user = $char_user->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('-11 month')))->where('created_at', '<=', date('Y-m-d 23:59:00'));
    }
    if (@$_GET['course_id'] != null && @$_GET['course_id'] != '') {
        $student_ids = \Modules\ThemeOncode\Models\Order::where('status', 1)->where('course_id', $_GET['course_id'])->pluck('student_id')->toArray();
        $char_user = $char_user->whereIn('id', $student_ids);
    }
    $char_user = $char_user->groupBy(\DB::raw('MONTH(created_at)'))->orderBy('created_at', 'asc')->get()->toArray();

    if (count($char_user) < 11) {
        $arr = [];
        for ($i = 11; $i > count($char_user); $i--) {
            $arr[] = 0;
        }
        $char_user = array_merge($arr, $char_user);
    }

    ?>
    var lineChartData = {
        labels: [
            @foreach($char as $item)
                'T{{ $item->month }}',
            @endforeach
        ],
        datasets: [{
            label: 'Đơn hàng',
            borderColor: window.chartColors.red,
            backgroundColor: window.chartColors.red,
            fill: false,
            data: [
                //    dữ liệu của số lượng Hợp đồng
                @foreach($char as $item)
                    '{{ $item->total_bill }}',
                @endforeach
            ],
            yAxisID: 'y-axis-1',
        }, {
            label: 'Số tiền (đv: triệu)',
            borderColor: window.chartColors.blue,
            backgroundColor: window.chartColors.blue,
            fill: false,
            data: [
                //    dữ liệu số tiên thu được theo tháng
                @foreach($char as $item)
                    '{{ round($item->total_price)/1000000 }}',
                @endforeach
            ],
            yAxisID: 'y-axis-2'
        },]
    };

    window.onload = function () {
        var ctx = document.getElementById('myChart').getContext('2d');
        window.myLine = Chart.Line(ctx, {
            data: lineChartData,
            options: {
                responsive: true,
                hoverMode: 'index',
                stacked: false,
                title: {
                    display: true,
                    // text: 'Chart.js Line Chart - Multi Axis'
                },
                scales: {
                    yAxes: [{
                        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: 'left',
                        id: 'y-axis-1',
                        ticks: {
                            beginAtZero: true,
                        }

                    }, {
                        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: 'right',
                        id: 'y-axis-2',
                        ticks: {
                            beginAtZero: true,
                        },
                        // grid line settings
                        gridLines: {
                            drawOnChartArea: false, // only want the grid lines for one axis to show up
                        },
                    }, {
                        type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                        display: true,
                        position: 'left',
                        id: 'y-axis-3',
                        ticks: {
                            beginAtZero: true,
                        },
                        // grid line settings
                        gridLines: {
                            drawOnChartArea: false, // only want the grid lines for one axis to show up
                        },
                    }],
                }
            }
        });
    };
</script>