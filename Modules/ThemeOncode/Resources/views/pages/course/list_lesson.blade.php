<?php

$carts = Cart::content();
$cart_id = [];
foreach ($carts as $v) {
    $cart_id[] = $v->id;
}
if (isset(\Auth::guard('student')->user()->id)) {
    $order = \Modules\ThemeOncode\Models\Order::where('student_id', @\Auth::guard('student')->user()->id)->where('course_id', $course->id)->first();
}
?>
@extends('themeoncode::layouts.default')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0&appId=1442327099412363&autoLogAppEvents=1"></script>
@section('khoahoc')
    class="active"
@endsection
@section('main_content')

    @include('themeoncode::partials.breadcrumb')
    @include('themeoncode::partials.banner_header')
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    @include('themeoncode::pages.course.components.detail_left')
                </div>
                <div class="col-md-4 col-xs-12">
                    @include('themeoncode::pages.course.components.detail_right')
                </div>
            </div>
        </div>
    </div>
@endsection
