<div class="breadcrumb">
    <div class="container">
        <ul class="breadcrumb__list">
            <li class="breadcrumb__list-item"><a class="item__link" href="index.html"><i class="fas fa-home"></i> Trang Chủ </a></li>
            <li class="breadcrumb__list-item"><a class="item__link" href="courses.html"><i class="fas fa-angle-double-right"></i> Danh sách khóa học</a></li>
            <li class="breadcrumb__list-item"><a class="item__link" href="#"><i class="fas fa-angle-double-right"></i> React native cơ bản</a></li>
        </ul>
    </div>
</div>