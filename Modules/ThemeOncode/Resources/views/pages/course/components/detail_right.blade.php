<div class="content__cart">
    <div class="content__cart-header">
        <div class="header__card">
            <div class="header__card-img"><img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($course->image,'auto','auto') }}"
                                               alt="{{ $course->name }}" class="lazy">
                <button class="img__bnt"><i class="fab fa-youtube"></i></button>
            </div>
            <div class="header__card-video">
                <iframe width="100%" height="215.42" src="https://www.youtube.com/embed/DbVCQQ5WZGM?autoplay=1&amp;mute=1&amp;enablejsapi=1" allow="autoplay" frameborder="0" allowfullscreen=""></iframe>
            </div>
        </div>
    </div>
    <div class="content__cart-body">
        <p class="body__price">@if($course->base_price != 0)<strike>{{number_format(@$course->base_price,0,'','.')}}<sup>đ</sup></strike>@endif  <span
                    style="color: red">{!! !empty($course->final_price) ? number_format(@$course->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span>
        </p>
    </div>
    <div class="content__cart-footer">

        @if(!\Auth::guard('student')->check())
            @if (in_array($course->id, $cart_id))
                <a class="main-btn"
                   href="/gio-hang"
                   style="color: green;font-weight: 700;"
                   title="">Khóa học đã được thêm vào giỏ hàng, xem ngay</a>
            @else
                <a class="footer__linkOgrin" href="/gio-hang/add/{{$course->id}}">
                    <b>MUA NGAY</b></a>
                <a class="footer__linkGrey add_to_cart" data-toggle="modal" data-target="#cart" style="cursor: pointer;">
                    <b>
                        Thêm vào giỏ hàng
                    </b>
                </a>

            @endif
        @elseif(is_object(@$order))
            @if(@$order->status == 1)
                @if($course->lesson!=null)

                    <a class="footer__linkOgrin"
                       title=""
                       href="#list_lesson">Học
                        ngay</a>
                @endif
            @else
                <a class="main-btn" rel="nofollow"
                   href="#"
                   style="color: orange;font-weight: 700;"
                   title="">Khóa học đang chờ admin kich hoạt</a>
            @endif

        @else
            @if (in_array($course->id, $cart_id))
                <a class="main-btn"
                   href="/gio-hang"
                   style="color: green;font-weight: 700;"
                   title="">Khóa học đã được thêm vào giỏ hàng,xem ngay</a>
            @else
                <a class="footer__linkOgrin" href="/gio-hang/add/{{$course->id}}">
                    <b>MUA NGAY</b></a>
                <a class="footer__linkGrey add_to_cart" style="cursor: pointer;">
                    <b>
                        Thêm vào giỏ hàng
                    </b>
                </a>
            @endif
        @endif
        <div class="footer__icon">
            <div class="footer__icon-items"><a class="items__link" href="#"><span class="items__link-face"><i class="fab fa-facebook-f color"></i></span></a><a class="items__link" href="#"><span class="items__link-linkedin"><i class="fab fa-linkedin-in"></i></span></a></div>
        </div>
    </div>
</div>
<!-- content -->
{{--Popup thêm vào giỏ hàng--}}
<div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm thành công vào giỏ hàng!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-primary"><a style="color:white;cursor: pointer;" href="/gio-hang">Xem giỏ hàng</a></button>
                <button type="button"  class="btn btn-secondary"><a class="close-pop main-btn3" href="/khoa-hoc" style="color:white;cursor: pointer;">Tiếp tục mua hàng</a></button>
            </div>
            <p class="strp-error"></p>
        </div>
    </div>
</div>


{{--<div class="content__advertise"><a class="content__advertise-link" href=""><img src="{{asset('public/frontend/themes/oncode/images/courseDetail/girl.jpg')}}" alt=""></a></div>--}}
<script>
    $(document).ready(function () {
        $('.add_to_cart').click(function () {

            $.ajax({
                url: '{{route('cart.ajaxAddCart')}}',
                type: 'post',
                data: {
                    id: '{{$course->id}}'
                },
                success: function (result) {
                    if (result.status == true) {
                        console.log('Thêm vào giỏ hàng thành công')
                    } else {
                        $('.strp-error').html('<span>' + result.msg + '</span>');
                    }
                },
                error: function (e) {
                    console.log(e.message);
                }
            })
        });
        $('.close-pop').click(function () {
            $('.popup-add-cart').hide();
            $('.full_k_che').hide();
            location.reload();
        });
    });
</script>