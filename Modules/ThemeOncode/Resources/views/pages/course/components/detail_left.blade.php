<div class="content__detail">
    <div class="content__detail-general mt-4">
        <h5 class="general__heading">Bạn sẽ học được gì?</h5>
        <div class="general__hr"> </div>
        <div class="general__list">
            <ul class="general__list-content">
                <li class="content__items">{{$course->intro}}</li>
            </ul>
            <p>&nbsp;</p>
        </div>
    </div>
    <div class="content__detail-general mt-4">
        <h5 class="general__heading">Nôi dung khóa học</h5>
        <div class="general__hr"> </div>
        <div class="general__syllabus">
            <div class="mt-2"> <a class="general__syllabus-link" href="#" data-toggle="collapse" data-target="#chapter0" aria-expanded="false" aria-controls="collapseExample">
                    <?php
                    $lessons = $course->lesson;
                    ?>

                    <div class="link__content">
                        @if(@$lessons != null)
                            @foreach(@$lessons as $k => $lesson)
                                <h6 class="link__content-detail">{{$lesson->name}}</h6>
                            @endforeach
                        @endif


                    </div>
                    <div class="link__example">
                        <div class="link__example-items">
                            <p class="items__text"><i class="far fa-play-circle"></i>  21 video</p>
                        </div>
                        <div class="link__example-items">
                            <p class="items__text"><i class="far fa-pen"></i>  0 bài tập</p>
                        </div>
                    </div><span class="link__icon"><i class="fas fa-chevron-down"></i></span></a>
                <div class="collapse" id="chapter0" style="height: 100px;">
                    <div class="syllabus__section">
                        @if(@$lessons != null)
                            @foreach(@$lessons as $k => $lesson)
                                <?php
                                $lessonItem = $lesson->lessonItem;
                                ?>
                                    @if(@$lessonItem->count()>0)
                                        @foreach(@$lessonItem as $lessonItem)
                                            <div class="syllabus__section-items">
                                                    @if(@$order->status == 1)
                                                    <span class="items__tittleChapter">
                                                        {{$lessonItem->name}}
                                                    </span>

                                                    @else
                                                        @if($lessonItem->publish == 1 && (!is_object(@$order) || @$order->status == 0))
                                                        <span class="items__tittleChapter">
                                                        {{$lessonItem->name}}
                                                        </span>
                                                            <a href="{{asset('khoa-hoc/'.$course->slug.'/'.$lessonItem->slug.'.html')}}"
                                                               class="btn-sm btn-success">Học thử</a>
                                                        @elseif(is_object(@$order) && @$order->status == 1)
                                                            <img class="lazy" data-src="https://media-kyna.cdn.vccloud.vn/img/icon-arrow-circle-right-line.png"
                                                                 alt="">
                                                        <span class="items__tittleChapter">
                                                        {{$lessonItem->name}}
                                                    </span>
                                                        @else
                                                            <img class="lazy" data-src="https://media-kyna.cdn.vccloud.vn/img/icon-arrow-circle-right-line.png"
                                                                 alt=""> <span class="items__tittleChapter">
                                                        {{$lessonItem->name}}
                                                    </span>
                                                        @endif
                                                    @endif
                                               </div>
                                        @endforeach
                                    @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="content__detail-general mt-4">
        <h5 class="general__heading">Giới thiệu khóa học</h5>
        <div class="general__hr"></div>
        <div class="general__wrapper" id="wrapper">
            {!! $course->content !!}
        </div><a class="general__moreRead" href="#collapseRead" data-toggle="collapse" data-target="#wrapper" aria-expanded="false" aria-controls="collapseExample">Xem thêm <i class="fas fa-chevron-down"></i></a><a class="general__lessRead" href="#collapseRead" data-toggle="collapse" data-target="#wrapper" aria-expanded="false" aria-controls="collapseExample">Thu gọn<i class="fas fa-chevron-up"></i></a>
    </div>
    <div class="content__detail-general mt-4">
        <h5 class="general__heading">Thông tin giảng viên</h5>
        <div class="general__hr"></div>
        @if(is_object($course->lecturer))
        <div class="general__lecturer">
            <div class="row">
                <div class="col-md-3 col-xs-12"><a class="general__lecturer-link" href="#">
                        <div class="link__avr">
                         <img  src="{{asset('public/filemanager/userfiles/'.@$course->lecturer->image)}}"
                                        alt="Giảng viên: {{ $course->lecturer->name }}">
                        </div>
                        <div class="link__info">
                            <p class="link__info-name">{{@$course->lecturer->name}}</p>
                            <p class="link__info-major"><i class="fas fa-briefcase"></i>Trung tâm</p>
                        </div></a></div>
                <div class="col-md-9 col-xs-12">
                    <p><span style="color: #333333; font-family: 'Open Sans', sans-serif; font-size:14px">{!! @$course->lecturer->note !!}</span></p>
                </div>
            </div><a class="general__lecturer-info" href="/giang-vien/Nordic-coder">Chi tiết <i class="fas fa-chevron-right"></i></a>
        </div>
        @endif
    </div>
    <div class="content__detail-general mt-4">
        @if(is_object($course->lecturer))
        <h5 class="general__heading">Khóa học của giảng viên <a class="general__heading-link" href="#">{{@$course->lecturer->name}}</a></h5>
        <div class="row">
            <?php
            $relates_lecturer = \Modules\ThemeEdu\Models\Course::where('lecturer_id', $course->lecturer_id)->where('id', '<>', $course->id)->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->take(8)->get();
            ?>
                @foreach($relates_lecturer as $v)
            <div class="col-xs-12 col-md-6 col-xl-4">
                <div class="course__box">
                    <a class="course__box-image " style="height:auto" href="/khoa-hoc/{{@$v->slug}}.html">
                        <img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image, 259, null) }}"
                             alt="{{@$v->name}}">
                        <div class="image__rating">
                            <div class="image__rating-text"><b><i class="fas fa-play-circle"></i><span>32</span></b></div>

                        </div></a>
                    <div class="course__box-content">
                        <a class="content__title" href="/khoa-hoc/{{@$v->slug}}.html">{{@$v->name}}</a>

                        <p><i class="fas fa-user"></i> Giảng viên: {{@$v->lecturer->name}} </p>
                        <p><i class="fas fa-tags"></i>Thể loại: {{ $v->type }} </p>
                        <p><i class="fas fa-play-circle"></i> Bài học: {{@$v->lesson->count()}} bài</p>
                        <p><i class="far fa-clock"></i> Thời hạn: {{$v->date_of_possession==0 ? 'Mãi mãi':$v->date_of_possession}}</p>
                        <div class="content__price">
                            <div class="content__price-text"><a class="video__button d-none" href="#"><i class="fas fa-play"></i></a><span></span></div>
                            <div class="content__price-number"><strike>{{number_format(@$v->base_price,0,'','.')}}<sup>đ</sup></strike>  <span
                                        style="color: red">{!! !empty($v->final_price) ? number_format(@$v->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span></div>
                        </div>
                    </div>
                </div>
            </div>
                @endforeach
        </div><a class="general__link" href="#">Xem Thêm</a>
        @endif
    </div>
    <div class="content__detail-general mt-4">
        <h5 class="general__heading">Khóa học cùng danh mục </h5>
        <div class="row">
            <?php
            $relates = \Modules\ThemeEdu\Models\Course::where('multi_cat', 'like', '%' . $course->multi_cat . '%')->where('id', '<>', $course->id)->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->take(8)->get();
            ?>
            @foreach($relates as $v)
            <div class="col-xs-12 col-md-6 col-xl-4">
                <div class="course__box">
                    <a class="course__box-image " style="height:auto" href="/khoa-hoc/{{@$v->slug}}.html">
                        <img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image, 259, null) }}"
                             alt="{{@$v->name}}">
                        <div class="image__rating">
                            <div class="image__rating-text"><b><i class="fas fa-play-circle"></i><span>32</span></b></div>

                        </div></a>
                    <div class="course__box-content"><a class="content__title" href="/khoa-hoc/{{@$v->slug}}.html">{{@$v->name}}</a>

                        <span><i class="fas fa-user"></i> Giảng viên: {{@$v->lecturer->name}} </span>
                        <span><i class="fas fa-tags"></i>Thể loại: {{ $v->type }} </span>
                        <span><i class="fas fa-play-circle"></i> Bài học: {{@$v->lesson->count()}} bài</span>
                        <span><i class="far fa-clock"></i> Thời hạn: {{$v->date_of_possession==0 ? 'Mãi mãi':$v->date_of_possession}}</span>
                        <div class="content__price">
                            <div class="content__price-text"><a class="video__button d-none" href="#"><i class="fas fa-play"></i></a><span></span></div>
                            <div class="content__price-number"> <strike>{{number_format(@$v->base_price,0,'','.')}}<sup>đ</sup></strike>  <span
                                        style="color: red">{!! !empty($v->final_price) ? number_format(@$v->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span></div>
                        </div>
                    </div>
                </div>
            </div>
                @endforeach
        </div><a class="general__link" href="#">Xem Thêm</a>
    </div>
</div>