@extends('themeoncode::layouts.default')
@section('main_content')

    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>
        <section>
            <div class="gap2 bg-white">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="col-lg-12">
                                    <div id="middle">
                                        <div class="child-middle" style="padding: 100px 0; text-align: center; border-top: 1px solid #ddd;color: red;">

                                                <h1 style="font-family: 'Titillium Web', Arial, Helvetica, 'Nimbus Sans L', sans-serif;font-weight: 700; font-style: normal;">
                                                    Chúc mừng bạn đặt hàng thành công </h1>
                                                <h3 style="font-family: 'Titillium Web', Arial, Helvetica, 'Nimbus Sans L', sans-serif; font-style: normal;">
                                                    Chúng tôi sẽ sớm duyệt đơn hàng của bạn , khóa học của bạn sẽ được kick hoạt và gửi về mail cho bạn</h3>



                                        </div>
                                    </div>
                                </div><!-- nave list -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>

@endsection