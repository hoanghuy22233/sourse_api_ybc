@extends('themeoncode::layouts.default')
@section('main_content')

    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 offset-lg-3">
                            @if(Cart::Count()>0)
                                <aside class="sidebar static right">
                                    <div class="widget mt-5">
                                        <h4 class="widget-title">Thông tin khách hàng</h4>
                                        @if(!\Auth::guard('student')->check())
                                            <div class="pl-3 pr-3 text-center">
                                                <a class="with-smedia facebook text-center mb-2"
                                                   href="/dang-nhap" title="" data-ripple="">Đăng
                                                    nhập</a><br><a href="/dang-ky"
                                                                   class="text-primary text-center">Hoặc tạo tài
                                                    khoản mới</a>
                                            </div>
                                        @endif
                                        <div>
                                            <form action="" method="post" enctype="multipart/form-data">
                                                <div>
                                                    @if(\Auth::guard('student')->check())
                                                        <div class="pl-3 pr-3 pt-2 pb-2">
                                                            <strong>Tên:</strong> {{\Auth::guard('student')->user()->name}}
                                                        </div>
                                                        <div class="pl-3 pr-3 pt-2 pb-2">
                                                            <strong>Email:</strong> {{\Auth::guard('student')->user()->email}}
                                                        </div>
                                                        <div class="pl-3 pr-3 pt-2 pb-2"><strong>Số điện
                                                                thoại:</strong> {{\Auth::guard('student')->user()->phone}}
                                                        </div>
                                                        @if(Cart::total()>0)
                                                            <div class="pl-3 pr-3 pt-2 pb-2">

                                                                    <span><input type="file" name="invoice_image"
                                                                                 value="" size="40"></span><br>
                                                                <i class="color-red">Ảnh chụp màn hình thanh
                                                                    toán
                                                                    chuyển khoản!<br>Nếu bạn thanh toán tiền
                                                                    trước
                                                                    cho đơn hàng này, chúng tôi sẽ duyệt và kick
                                                                    hoạt khoá học cho bạn nhanh hơn!</i>
                                                            </div>
                                                        @endif
                                                    @else
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Họ Tên</label>
                                                                <input type="text" name="name" value="{{old('name')}}"
                                                                       class="form-control" placeholder="Tên">
                                                                @if ($errors->has('name'))
                                                                    <i class="color-red">{{$errors->first('name')}}</i>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Email</label>
                                                                <input type="email" name="email"
                                                                       value="{{old('email')}}" size="40"
                                                                       class=" form-control "
                                                                       placeholder="Email">
                                                                @if ($errors->has('email'))
                                                                    <i class="color-red">{{$errors->first('email')}}</i>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Mật khẩu</label>
                                                                <input type="password" name="password"
                                                                       value="{{old('password')}}"
                                                                       class=" form-control "
                                                                       placeholder="Nhập mật khẩu">
                                                                @if ($errors->has('password'))
                                                                    <i class="color-red">{{$errors->first('password')}}</i>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Nhập lại mật khẩu</label>
                                                                <input type="password" name="re_password" value=""
                                                                       class=" form-control "
                                                                       placeholder="Nhập lại mật khẩu">
                                                                @if ($errors->has('re_password'))
                                                                    <i class="color-red">{{$errors->first('re_password')}}</i>
                                                                @endif
                                                            </div>
                                                            @if(Cart::total()>0)
                                                                <div class="pl-3 pr-3 pt-2 pb-2">
                                                                    <span>
                                                                        <input type="file" name="invoice_image" value=""
                                                                               size="40">
                                                                    </span><br>
                                                                    <i class="color-red">Ảnh chụp màn hình thanh
                                                                        toán
                                                                        chuyển khoản!<br>Nếu bạn thanh toán tiền
                                                                        trước
                                                                        cho đơn hàng này, chúng tôi sẽ duyệt và kick
                                                                        hoạt khoá học cho bạn nhanh hơn!</i>
                                                                </div>
                                                            @endif
                                                            @endif
                                                            <button type="submit" class="green-button ">Hoàn tất đơn hàng</button>
                                                        </div>
                                                    </div>

                                            </div>
                                                </div>
                                            </form>
                                        </div>


                                    </div><!-- recent post-->
                                </aside>
                            @endif
                        </div>
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="col-lg-12">
                                    @if(Cart::Count()>0)
                                        <div class="cart">
                                            <div class="container">
                                                <div class="cart__wrap">
                                                    <div class="cart__header">
                                                        <div class="cart__header-info"><img src="images/home/icon-cart-checkout.png" alt="">
                                                            <div class="cart__info">
                                                                <div class="cart__info-title">Thông tin giỏ hàng</div>
{{--                                                                <div class="cart__info-des"><span>1</span><span class="pc">khóa học,</span><span class="mob"> <b>khóa học</b>đã chọn</span><span>398.000 ₫</span></div>--}}
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="cart__body">
                                                        <form id="cart-form"  method="post">
                                                            <input type="hidden" name="" value="">
                                                            <input type="hidden" name="">
                                                            <ol class="k-shopping-list-items list-unstyled">
                                                                @foreach(Cart::content() as $k=>$cart_item)
                                                                <li class="items">
                                                                    <div class="k-shopping-list-items-title" data-id="" data-price="398000" data-brand="">
                                                                        <div class="items-img"><a href="" title=""><img class="img-fluid" src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($cart_item->options->image, 100, null) }}" alt="" title=""></a></div>
                                                                        <div class="items-text">
                                                                            <h4><a href="#" title=""><b>{{$cart_item->name}}</b></a></h4>
                                                                            <div class="k-shopping-list-items-group-price -mob"><span class="orange">{{ number_format($cart_item->price,0,'','.') }}đ</span></div>
                                                                            <a class="items-remove cart-item-remove" href="/gio-hang/del_item/{{$cart_item->rowId}}" data-id="1828">
                                                                                <i class="ti-close"></i>
                                                                                    Xóa khóa học</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="k-shopping-list-items-group-price">
                                                                        <div class="k-shopping-list-items-price-old"> <span style="color: red">{{ number_format($cart_item->price,0,'','.') }}<sup>đ</sup></span></div>

                                                                    </div>
                                                                </li>
                                                                @endforeach
                                                            </ol>
                                                        </form>
                                                    </div>
                                                    <div class="cart__footer">
                                                        <div class="k-shopping-checkout-total-price clearfix">
                                                            <div class="k-shopping-checkout-total-price-text"><span>Tổng thành tiền</span>
{{--                                                                <label for="">Học phí </label>--}}
                                                                <label for="">Tổng cộng</label>
                                                            </div>
                                                            <div class="k-shopping-list-items-group-price">
{{--                                                                <div class="k-shopping-checkout-total-price-old"><span>--}}
{{--                                                                <s>398.000&nbsp;&#x20AB;</s></span></div>--}}

                                                                <div class="k-shopping-checkout-total-price-new"><span>{{ Cart::total(0, '', ',') }}</span></div>
                                                            </div>
                                                        </div>
                                                        <div class="k-shopping-checkout-choose-another"><a href="/khoa-hoc"><img src="images/cart/icon-arrow-left.png" alt=""> Chọn thêm khóa học khác</a></div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    @else
                                        <h3 class="mt-5 mb-5">Bạn chưa chọn mua khóa học nào!</h3>
                                    @endif
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section><!-- content -->

    </div>

@endsection