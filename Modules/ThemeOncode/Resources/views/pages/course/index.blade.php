@extends('themeoncode::layouts.default')
@section('khoahoc')
    active
@endsection
@section('main_content')
<?php
 $courses = \Modules\ThemeOncode\Models\Course::where('status',1)->latest()->paginate(9);
?>
    <!-- Start Breadcrumb-->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <div class="container">
                <li class="breadcrumb-item"><i class="fas fa-home"></i><a href="index.html">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="#">Danh sách khóa học</a></li>

            </div>
        </ol>
    </nav>
    <!-- End Breadcrumb-->

    <!-- Start Filter-->
    <div class="filter" id="form-filter">
        {{--    --}}
        <form method="get" action="">
            {{ csrf_field() }}
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 mb-4 mb-lg-0">
                        <a class="btn btn-green filter__button" href="#">
                            <img src="{{ asset('public/frontend/themes/oncode/images/course/filter-icon.png') }}" alt="">Lọc kết quả</a>
                        <div class="filter__checkbox" id="filter__checkbox">
                            <input id="form-filter-checkbox" type="checkbox" value="{{Request::url()}}?learn_by=video">
                            <label for="form-filter-checkbox">Học qua Video</label>
                            <input id="form-filter-checkbox" type="checkbox" value="{{Request::url()}}?learn_by=free">
                            <label for="form-filter-checkbox">Miễn phí</label>
                        </div>
                        <select class="form-control">
                            <option class="selected">Theo thời lượng</option>
                            <option>Dưới 3 tiếng</option>
                            <option>3 - 24 tiếng</option>
                        </select>
                        <select class="form-control">
                            <option class="selected">Theo trình độ</option>
                            <option>Cơ bản</option>
                            <option>Nâng cao</option>
                            <option>Mọi cấp độ</option>
                        </select>
                    </div>
                    <div class="col-lg-3 my-auto">
                        <div class="field-sort">
                            <label>Sắp xếp:</label>
                            <select id="sort" name="sort" class="form-control">
                                <option  value="{{Request::url()}}?sort_by=none">-- Lọc --</option>
                                <option value="{{Request::url()}}?sort_by=giam_dan">Gía giảm dần</option>
                                <option value="{{Request::url()}}?sort_by=tang_dan">Gía tăng dần</option>
                                <option value="{{Request::url()}}?sort_by=a_z">A - Z</option>
                                <option value="{{Request::url()}}?sort_by=z_a">Z - A</option>
                            </select>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- Start Course-->
    <div class="course">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-3">
                    <?php
                    $data = CommonHelper::getFromCache('categories_type_5');
                    if (!$data) {
                        $data = \Modules\ThemeEdu\Models\Category::where('type', 5)->orderBy('order_no', 'desc')->orderBy('name', 'asc')->where('status', 1)->get();
                        CommonHelper::putToCache('categories_type_5', $data);
                    }
                    ?>
                    <div class="course__category">
                        <div class="course__category-title">IT và lập trình</div>
                        <ul class="course__category-list">
                            @foreach($data as $v)
                                @if($v->parent_id == 0)
                                    <li class="list__item"><a class="list__item-link" href="/{{ @$v->slug }}"><img src="{{asset('public/frontend/themes/oncode/images/course/course-icon-3.png')}}" alt="">{{ @$v->name }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-lg-9">
                    <div class="row">
                        <div class="col-12 col-lg-9">
                            <div class="course__title">{{$courses->count()}} Khóa học
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($courses as $v)
                            <div class="col-xs-12 col-md-6 col-xl-4">
                                <div class="course__item">
                                    <a class="course__box-image " style="height:auto" href="/khoa-hoc/{{@$v->slug}}.html">
                                        <img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image, 259, null) }}"
                                             alt="{{@$v->name}}">
                                        <div class="image__rating">
                                            <div class="image__rating-text"><b><i class="fas fa-play-circle"></i><span>{{@$v->lesson->count()}}</span></b></div>

                                        </div></a>
                                    <div class="course__box-content">
                                        <a class="content__title" href="/khoa-hoc/{{@$v->slug}}.html">{{@$v->name}}</a>

                                        <p><i class="fas fa-user"></i> Giảng viên: {{@$v->lecturer->name}} </p>
                                        <p><i class="fas fa-tags"></i>Thể loại: {{ $v->type }} </p>
                                        <p><i class="fas fa-play-circle"></i> Bài học: {{@$v->lesson->count()}} bài</p>
                                        <p><i class="far fa-clock"></i> Thời hạn: {{$v->date_of_possession==0 ? 'Mãi mãi':$v->date_of_possession}}</p>
                                        <div class="content__price">
                                            <div class="content__price-text"><a class="video__button d-none" href="#"><i class="fas fa-play"></i></a><span></span></div>
                                            <div class="content__price-number"><strike>{{number_format(@$v->base_price,0,'','.')}}<sup>đ</sup></strike>  <span
                                                        style="color: red">{!! !empty($v->final_price) ? number_format(@$v->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="newsletter">
                        <div class="newsletter__background"><img src="images/newsletter.png" alt=""></div>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="newsletter__box">
                                    <div class="newsletter__box-title">Đăng ký nhận thông báo</div>
                                    <div class="newsletter__box-text">Đăng ký ngay để nhận tin tức mới nhất, độc quyền và nhiều ưu đãi khác.</div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="newsletter__box ml-lg-auto">
                                    <div class="newsletter__box-form">
                                        <input class="form__email" id="email" type="email" placeholder="Email của bạn...">
                                    </div><a class="btn newsletter__box-button" href="#">Đăng ký ngay<i class="fal fa-paper-plane"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Course-->
    <script>
        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        $(document).ready(function () {
            $('.subscribe_to_newsletter-btn').click(function () {
                var email = $('input[name=subscribe_to_newsletter_email]').val();
                if (email.length == 0) {
                    alert ('Bạn chưa nhập vào email!');
                } else {
                    if (!isEmail(email)) {
                        alert ('Email nhập sai!');
                    } else {
                        $('.subscribe_to_newsletter-btn').attr('disabled', 'disabled');
                        $('.subscribe_to_newsletter-btn').css('opacity', '0.4');
                        $.ajax({
                            url : '/admin/ajax/contact/add',
                            type: 'POST',
                            data: {
                                email: email,
                                link: '<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>',
                            },
                            success: function (resp) {
                                if (resp.status == true) {
                                    alert(resp.msg);
                                    $('input[name=subscribe_to_newsletter_email]').val('');
                                    $('.subscribe_to_newsletter-btn').removeAttr('disabled');
                                    $('.subscribe_to_newsletter-btn').css('opacity', '1');
                                } else {
                                    alert(resp.msg);
                                    location.reload();
                                }
                            },
                            error: function () {
                                alert('Có lỗi xảy ra! Vui lòng load lại website & thử lại.');
                            }
                        });
                    }
                }
            });

            $('.news-letter-bx input, .news-letter-bx textarea').keypress(function (e) {
                if(e.which == 13) {
                    $('.subscribe_to_newsletter-btn').click();
                }
            });
            $('#sort').on('change',function (){
                var url = $(this).val();

                window.location.replace(url);
                $('#sort option').attr('selected',selected);
                return false;

            })
            $('#filter__checkbox input[type="checkbox"]').on('change',function (){
                var url = $(this).val();
                if(url){
                    $(this).attr("checked", true)
                    window.location = url;
                }

                return false;
            })

        });
    </script>
@endsection
