@extends('themeoncode::layouts.default')
@section('main_content')
    
    @include('themeoncode::partials.slides')
    <?php
    $categoriesCourse = \Modules\ThemeOncode\Models\Category::where('type',5)->where('status',1)->limit(6)->get();
    $bannerNews = \Modules\ThemeOncode\Models\Banner::where('status', 1)->where('location', 'banner_slides_news')->get();
    ?>
    <div class="skillCourse bgcLightGrey px-60">
        <div class="container">
            <h2 class="skillCourse__heading title"> Khóa học phát triển sự nghiệp cùng Oncode</h2>
            <h5 class="skillCourse__paragraph">Thăng tiến vượt bậc với các khóa học phát triển kỹ năng</h5>
            <div class="skillCourse__tab">
                <div class="row">
                    <div class="col-12">
                        <div class="tab__menu mb-4 text-center">
                            <ul class="tab__menu-list nav nav-tabs" role="tablist">
                                @foreach($categoriesCourse as $k => $categoryItem)
                                    <li class="tab__menu-item nav-item">
                                        <a class="nav-link tab__menu-link {{$k == 0 ? 'active' : ''}}" href="#category_tab_{{$categoryItem->id}}" data-toggle="tab" role="tab">{{$categoryItem->name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="skillCourse__tab-content">
                    @foreach($categoriesCourse as $keyindex => $cateItem)
                    <div class="skillCourse__tab-pane tab-pane {{$keyindex == 0 ? 'active' : ''}}" id="category_tab_{{$cateItem->id}}" role="tabpanel">
                        <?php
                        $courseCate = \Modules\ThemeEdu\Models\Course::where('multi_cat', 'like', '%' . $cateItem->id . '%')->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->get();
                        ?>
                        <div class="skillCourse__pane-box">

                                <div class="row">
                                    @foreach($courseCate as $key => $v)
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="course__box">
                                            <a class="course__box-image" href="/khoa-hoc/{{@$v->slug}}.html">
                                                <img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image, 259, null) }}"
                                                     alt="{{@$v->name}}">
                                                <div class="image__rating">
                                                    <div class="image__rating-text"><b><i class="fas fa-play-circle"></i><span>{{@$v->lesson->count()}}</span></b></div>

                                                </div></a>
                                            <div class="course__box-content">
                                                <a class="content__title" href="/khoa-hoc/{{@$v->slug}}.html">{{@$v->name}}</a>

                                                <p><i class="fas fa-user"></i> Giảng viên: {{@$v->lecturer->name}} </p>
                                                <p><i class="fas fa-tags"></i>Thể loại: {{ $v->type }} </p>
                                                <p><i class="fas fa-play-circle"></i> Bài học: {{@$v->lesson->count()}} bài</p>
                                                <p><i class="far fa-clock"></i> Thời hạn: {{$v->date_of_possession==0 ? 'Mãi mãi':$v->date_of_possession}}</p>
                                                <div class="content__price">
                                                    <div class="content__price-text"><a class="video__button d-none" href="#"><i class="fas fa-play"></i></a><span></span></div>
                                                    <div class="content__price-number"><strike>{{number_format(@$v->base_price,0,'','.')}}<sup>đ</sup></strike>  <span
                                                                style="color: red">{!! !empty($v->final_price) ? number_format(@$v->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @endforeach
                                </div>


                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="jobs px-60 d-flex">
        <div class="container">
            <div class="row">
                @foreach($bannerNews as $b)
                <div class="col-lg-4 col-md-6">
                    <div class="item">
                        <img class="img-fluid" src="{{ asset('public/filemanager/userfiles/' . $b->image) }}" alt="">
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
    <?php
    $courseFree = \Modules\ThemeOncode\Models\Course::where('status', 1)->where('final_price',0)->limit(4)->get();
    ?>
    <div class="freeCourse bgcLightGrey px-60">
        <div class="container">
            <h2 class="skillCourse__heading title">Khóa học 0 đồng</h2>
            <div class="freeCourse__box owl-carousel owl-theme">
                @foreach($courseFree as $courseFreeItem)
                    <div class="course__box">
                        <a class="course__box-image" href="/khoa-hoc/{{@$courseFreeItem->slug}}.html">
                            <img src="{{ asset('public/filemanager/userfiles/' . $courseFreeItem->image) }}">
                            <div class="image__rating">
                                <div class="image__rating-text" style="color:white"><b><i class="fas fa-play-circle"></i><span>{{@$courseFreeItem->lesson->count()}}</span></b></div>
                                <div ><span> <span></span>

                            </div></a>
                        <div class="course__box-content">
                            <a class="content__title" href="/khoa-hoc/{{@$courseFreeItem->slug}}.html">{{@$courseFreeItem->name}}</a>

                            <p><i class="fas fa-user"></i> Giảng viên: {{@$courseFreeItem->lecturer->name}} </p>
                            <p><i class="fas fa-tags"></i>Thể loại: {{ $courseFreeItem->type }} </p>
                            <p><i class="fas fa-play-circle"></i> Bài học: {{@$courseFreeItem->lesson->count()}} bài</p>
                            <p><i class="far fa-clock"></i> Thời hạn: {{$courseFreeItem->date_of_possession==0 ? 'Mãi mãi':$v->date_of_possession}}</p>
                            <div class="content__price">
                                <div class="content__price-text"><a class="video__button d-none" href="#"><i class="fas fa-play"></i></a><span></span></div>
                                <div class="content__price-number"><strike>{{number_format(@$v->base_price,0,'','.')}}<sup>đ</sup></strike>  <span
                                            style="color: red">{!! !empty($v->final_price) ? number_format(@$v->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span></div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="teachers px-60">
        <div class="container">
            <?php
            $leacture = \App\Models\Admin::join('role_admin', 'role_admin.admin_id', '=', 'admin.id')
                ->join('roles', 'roles.id', '=', 'role_admin.role_id')
                ->where('roles.name', 'lecturers')
                ->limit(3)->get();
            ?>
            <h2 class="title">Đội ngũ giảng viên</h2>
            <div class="teachers__list owl-carousel owl-theme">
                @foreach($leacture as $leactureItem)

                    @if($leactureItem->status == 1)
                        <div class="teacher__item">
                            <div class="teacher__item-header">
                                <div class="teacher__avatar">
                                    <div class="teacher__avatar-img"><img src="{{ asset('public/filemanager/userfiles/' . $leactureItem->image) }}" alt="avatar teacher"></div>
                                </div>
                            </div>
                            <div class="teacher__item-body">
                                <h3 class="teacher__name">{{$leactureItem->short_name}}</h3>
                                <p class="teacher__major"><i class="fal fa-briefcase"></i>{{$leactureItem->note}}
                                </p>
                                <div class="teacher__info">
                                    <div class="teacher__info-item">
                                        <p>Số khóa học</p><span>{{$leactureItem->facebook}}</span>
                                    </div>
                                    <div class="teacher__info-item">
                                        <p>Tổng giờ giảng</p><span>{{$leactureItem->skype}}</span>
                                    </div>
                                    <div class="teacher__info-item">
                                        <p>Số khóa học</p><span>{{$leactureItem->zalo}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="teacher__item-footer"><a class="teacher__item-btn" href="#">Xem thêm</a></div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection

