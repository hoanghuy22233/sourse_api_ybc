@extends('themeoncode::layouts.default')
@section('main_content')
    <div class="container mt-5 mb-4">

        <div class="row">
            <div class="col-12 col-lg-6 offset-lg-2 text-center">
                <h4>Nhập email của bạn vào, chúng tôi sẽ gửi vào mail cho bạn link để thay đổi mật khẩu</h4>
            </div>
            <div class="col-12 col-lg-6 offset-lg-2">
                @if (session('success'))
                    <div class="alert bg-success" role="alert">
                        <p style="color: red"><b>{!!session('success')!!}</b></p>
                    </div>
                @endif
                    <form class="we-form" method="post">
                        <input type="email" class="form-control" name="email" placeholder="Email" required>
                        <button type="submit" class="green-button  mt-3 mb-4">Gửi yêu cầu</button>
                    </form>
                    <span>Bạn đã có tài khoản? <a class="we-account underline" href="/dang-nhap" title="">Đăng nhập</a></span>
            </div>
        </div>
    </div>
@endsection