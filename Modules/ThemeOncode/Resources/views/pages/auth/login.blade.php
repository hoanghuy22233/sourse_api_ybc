@extends('themeoncode::layouts.default')
@section('main_content')
    <div class="container mt-5 mb-4">

        <div class="row">
            <div class="col-12 col-lg-6 offset-lg-2 text-center">
                <h4>Đăng nhập</h4>
            </div>
            <div class="col-12 col-lg-6 offset-lg-2">
                @if (Session('success'))
                    <div class="alert bg-success" role="alert">
                        <p style="color: red"><b>{!!session('success')!!}</b></p>
                    </div>
                @endif
                @if(Session::has('message') && !Auth::check())
                    <div class="alert text-center text-white " role="alert"
                         style=" margin: 0; font-size: 16px;">
                        <a href="#" style="float:right;" class="alert-close"
                           data-dismiss="alert">&times;</a>
                        <p style="color: red"><b>{{ Session::get('message') }}</b></p>
                    </div>
                @endif
                <form class="form-controls" method="post" action="/dang-nhap">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Họ Tên</label>
                        <input type="text" name="email" class="form-control" placeholder="Email hoặc điện thoại">
                        @if($errors->has('email'))
                            <p style="color: red"><b>{{ $errors->first('email') }}</b></p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="text" name="email" class="form-control" placeholder="Email hoặc điện thoại">
                        @if($errors->has('email'))
                            <p style="color: red"><b>{{ $errors->first('email') }}</b></p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mật khẩu</label>
                        <input type="password" name="password" class="form-control" placeholder="Nhập mật khẩu">
                        @if($errors->has('password'))
                            <p style="color: red"><b>{{ $errors->first('password') }}</b></p>
                        @endif
                    </div>

                    <button type="submit" class="green-button ">Đăng nhập</button>
                </form>
                <a class="forgot underline mt-3 d-block" href="/quen-mat-khau" title="">Quên mật khẩu?</a>
                    <span>Bạn chưa có tài khoản ? <a class="we-account underline" href="/dang-ky"
                                                     title="">Đăng ký</a></span>
            </div>
        </div>
    </div>
@endsection