@extends('themeoncode::layouts.default')
@section('main_content')

    <div class="container mt-5 mb-4">

        <div class="row">
            <div class="col-12 col-lg-6 offset-lg-2 text-center">
                <h4>Đăng ký ngay</h4>
            </div>
            <div class="col-12 col-lg-6 offset-lg-2">
                <form class="form-controls" method="post" action="/dang-ky">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Họ Tên</label>
                        <input type="text" name="name" class="form-control"  placeholder="Nhập Họ Tên" value="{{old('name')}}">
                        @if($errors->has('name'))
                            <p style="color: red"><b>{{ $errors->first('name') }}</b></p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="text" name="email" class="form-control"  placeholder="Nhập email" value="{{old('email')}}">
                        @if($errors->has('email'))
                            <p style="color: red"><b>{{ $errors->first('email') }}</b></p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Số điện thoại</label>
                        <input type="text" name="phone" class="form-control"  placeholder="SĐT" value="{{old('phone')}}">
                        @if($errors->has('phone'))
                            <p style="color: red"><b>{{ $errors->first('phone') }}</b></p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mật khẩu</label>
                        <input type="password" name="password" class="form-control"  placeholder="Nhập mật khẩu" value="{{old('password')}}">
                        @if($errors->has('password'))
                            <p style="color: red"><b>{{ $errors->first('password') }}</b></p>
                        @endif
                    </div>
                    <button type="submit" class="green-button ">Đăng ký ngay</button>
                </form>
                <a class="forgot underline mt-3 d-block" href="/quen-mat-khau" title="">Quên mật khẩu?</a>

            </div>
        </div>
    </div>

@endsection