@extends('themeoncode::layouts.default')
@section('main_content')
    <div class="profile">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20">
                        <div class="profile__user">
                            <figure><img src="{{ asset('public/frontend/themes/oncode/images/profile-user/banner.jpg') }}" alt="banner"></figure>
                            @include('themeoncode::partials.student_menu', ['user' => $user])
                        </div>
                        <div class="col-md-8">
                            <div class="forum-form">
                                @if(Session::has('success'))
                                    <p style="top: -15px;" class="alert alert-success">{!! Session::get('success') !!}</p>
                                @endif
                                <div class="central-meta"><span class="create-post">Thông tin cơ bản</span>
                                    <form class="form" method="post" action="/profile/edit"
                                          enctype="multipart/form-data">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="student_id"
                                               value="{{ Auth::guard('student')->user()->id }}">
                                        <div>
                                            <label>Họ & tên</label>
                                            <input type="text" name="name" placeholder="Name"
                                                   value="{{ Auth::guard('student')->user()->name }}">
                                        </div>
                                        <div>
                                            <label>Số điện thoại</label>
                                            <input type="text" name="phone" placeholder="Phone"
                                                   value="{{ Auth::guard('student')->user()->phone }}">
                                        </div>
                                        <div>
                                            <label>Email</label>
                                            <input type="text" name="email" placeholder="Email"
                                                   value="{{ Auth::guard('student')->user()->email }}">
                                        </div>
                                        <div>
                                            <label>Địa chỉ</label>
                                            <input type="text" name="address" placeholder="Address"
                                                   value="{{ Auth::guard('student')->user()->address }}">
                                        </div>
                                        <div>
                                            <label>Ảnh đại diện</label>
                                            <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@\Auth::guard('student')->user()->image, 150) }}" class="lazy">
                                            <input type="file" name="image" placeholder="Image"
                                                   value="{{ Auth::guard('student')->user()->image }}">
                                        </div>
                                        <div>
                                            <label>Ảnh banner</label>
                                            <img data-src="{{ asset('public/filemanager/userfiles/' . @Auth::guard('student')->user()->banner, 100, null) }}" class="lazy">
                                            <input type="file" name="banner" placeholder="Banner"
                                                   value="{{ Auth::guard('student')->user()->banner }}">
                                        </div>
                                        <div>

                                            <?php
                                            $data = CommonHelper::getFromCache('center_all', ['centers']);
                                            if (!$data) {
                                                $data = \Modules\ThemeEdu\Models\Center::orderBy('order_no', 'desc')->orderBy('name', 'asc')->get();
                                                CommonHelper::putToCache('center_all', $data, ['centers']);
                                            }
                                            ?>
                                                <label>Chi nhánh</label>
                                            <select name="center_id" class="form-control">
                                                <option>Chọn chi nhánh bạn học</option>
                                                @foreach($data as $v)
                                                    <option value="{{ $v->id }}" {{ Auth::guard('student')->user()->center_id == $v->id ? 'selected' : '' }}>{{ $v->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div>
                                            <label>Ngày sinh</label>
                                            <input type="date" name="birthday" placeholder="Birthday"
                                                   value="{{ Auth::guard('student')->user()->birthday }}">
                                        </div>
                                        <div>
                                            <label>Giới tính</label>
                                            <div class="col-md-6" style="display: inline-block;">
                                                <label>
                                                    <input type="radio" name="gender" placeholder="Gender"
                                                           style="width: 50px;"
                                                           value="1" {{ Auth::guard('student')->user()->gender === 1 ? 'checked' : '' }}>Nam
                                                </label>
                                            </div>
                                            <div class="col-md-6" style="display: inline-block;">
                                                <label>
                                                    <input type="radio" name="gender" placeholder="Gender"
                                                           style="width: 50px;"
                                                           value="0" {{ Auth::guard('student')->user()->gender === 0 ? 'checked' : '' }}>Nữ
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <a class="main-btn3 mr-4" href="/profile">Quay lại
                                                </a>
                                            </div>
                                            <div class="col-md-6">
                                                <a class="pt-2" style="padding-left: 73%;" href="/student/doi-mat-khau">Đổi mật khẩu
                                                </a>
                                            </div>
                                            <div class="col-md-3">
                                                <button class="btn btn-primary ml-4" type="submit" data-ripple="">Cập nhật
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="central-meta stick-widget"><span class="create-post">Cấu hình khác</span>
                                <div class="personal-head"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection