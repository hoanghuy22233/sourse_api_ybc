@extends('themeoncode::layouts.default')
@section('main_content')
    <div class="menu">
        <div class="container">
            <div class="menu__image"><img src="{{ asset('public/frontend/themes/oncode/images/layout/menu.png') }}" alt="menu"></div>
            <div class="menu__content">
                <div class="menu__content-title">Học Viện Oncode</div>
                <div class="menu__content-button">
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                </div>
                <ul class="menu__content-list">
                    <li class="list__item"><a class="list__item-link" @yield('khoahoc') href="/student/{{ @$user->id }}/khoa-hoc">Khóa học</a></li>
                    <li class="list__item"><a class="list__item-link" href="#">Tài liệu</a></li>
                    <li class="list__item"><a class="list__item-link" href="#">Lịch học</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="userCourse">
        <div class="container">
            <div class="row">

                <div class="col-lg-12">
                    <div class="row">
                        <?php
                        $data = CommonHelper::getFromCache('course_of_student_8_' . $user->id, ['courses']);
                        if (!$data) {
                            $data = \Modules\EduCourse\Models\Course::join('orders', 'orders.course_id', '=', 'courses.id')
                                ->selectRaw('courses.*')
                                ->where('orders.status', 1)->where('orders.student_id', $user->id)->orderBy('orders.created_at', 'desc')->paginate(8);
                            CommonHelper::putToCache('course_of_student_8_' . $user->id, $data, ['courses']);
                        }

                        ?>
                            @foreach($data as $v)
                        <div class="col-md-6">
                            <div class="userCourse__item">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="userCourse__item-image"><img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$v->image,244,122) }}"
                                                                                 alt="{{@$course->name}}"></div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="userCourse__item-content">
                                            <h6><a href="/khoa-hoc/{{(@$v->slug)}}.html"
                                                   title="{{@$v->name}}">{{@$v->name}}</a></h6>
                                            <ul>
                                                <li><i class="fas fa-user"></i>Bài học: <span>{{@$v->lesson->count()}} bài</span></li>
                                                <li><i class="fas fa-clock"></i>Thời hạn: <span>{{$v->date_of_possession==0 ? 'Mãi mãi':$v->date_of_possession}}</span></li>
                                                <li><i class="fas fa-money-bill"></i>
                                                    <strike>{{number_format(@$v->base_price,0,'','.')}} <sup>đ </sup></strike><span style="color: red">{!! !empty($v->final_price) ? number_format(@$v->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span>
                                                </li>
                                                <li><i class="fas fa-user"></i>Giảng viên: <span>{{@$v->lecturer->name}}</span></li>
                                                <li><i class="fas fa-tags"></i>Thể loại: <span>{{ $v->type }}</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="userCourse__item-button"><a class="btn"  href="/khoa-hoc/{{(@$v->slug)}}.html">Học ngay</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            @endforeach
{{--                        <div class="col-md-12">--}}
{{--                            <div class="userCourse__button"><a class="btn btn-purple mx-auto" href="#">Xem Thêm</a></div>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection