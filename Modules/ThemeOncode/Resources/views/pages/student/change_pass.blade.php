@extends('themeoncode::layouts.default')
@section('main_content')
    <div class="profile">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20">
                        <div class="profile__user">
                            <figure><img src="{{ asset('public/frontend/themes/oncode/images/profile-user/banner.jpg') }}" alt="banner"></figure>
                            @include('themeoncode::partials.student_menu', ['user' => $user])
                        </div>
                        <div class="col-md-8">
                            <div class="forum-form">
                                @if(Session::has('success'))
                                    <p style="top: -15px;" class="alert alert-success">{!! Session::get('success') !!}</p>
                                @endif
                                    @if(Session::has('password-error'))
                                        <p style="top: -15px;"
                                           class="alert alert-danger">{!! Session::get('password-error') !!}</p>
                                    @endif
                                <div class="central-meta"><span class="create-post">Đổi mật khẩu</span>
                                    <form class="form" method="post" action="/student/doi-mat-khau"
                                          enctype="multipart/form-data">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="student_id"
                                               value="{{ Auth::guard('student')->user()->id }}">
                                        @if(\Auth::guard('student')->user()->password != '')
                                            <div>
                                                <label>Mật khẩu hiện tại</label>
                                                <input type="password" name="password"
                                                       value="" required>
                                            </div>
                                        @endif
                                        <div>
                                            <label>Mật khẩu mới</label>
                                            <input type="password" name="new_password"
                                                   value="" required>
                                        </div>
                                        <div>
                                            <label>Nhập lại mật khẩu mới</label>
                                            <input type="password" name="re_new_password"
                                                   value="" required>
                                            @if ($errors->has('re_new_password'))
                                                <p class="text-danger"
                                                   style=" padding-left: 10px;">{{$errors->first('re_new_password')}}</p>
                                            @endif
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <a class="main-btn3 mr-4" href="/profile">Quay lại
                                                </a>
                                            </div>

                                            <div class="col-md-3">
                                                <button class="btn btn-primary ml-4" type="submit" data-ripple="">Cập nhật
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="central-meta stick-widget"><span class="create-post">Cấu hình khác</span>
                                <div class="personal-head"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection