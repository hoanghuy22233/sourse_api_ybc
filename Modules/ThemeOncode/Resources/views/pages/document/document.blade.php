@extends('themeoncode::layouts.default')
@section('tailieu')
    active
@endsection
@section('main_content')
    <!-- Start Breadcrumb-->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <div class="container">
            <li class="breadcrumb-item"><i class="fas fa-home"></i><a href="index.html">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="#">{{$category->name}}</a></li>
        </div>
    </ol>
</nav>
<!-- End Breadcrumb-->

    <div class="block">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        @foreach($posts as $post)

                        <div class="col-lg-6 px-0">
                            <div class="block__module">
                                <div class="block__module-img">
                                    <a href="#">
                                        <img class="lazy" alt="{{@$post->name}}"
                                                                                data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image,361,200) }}"></a></div>
                                <h3 class="block__module-title"> <a href="/{{$slug1}}/{{@$post->slug}}.html">{{@$post->name}}</a></h3>
                                <div class="block__module-info"><span class="td__name"> <a href="#">{!!$post->intro!!}</a><span>-</span></span><span class="td__date">
                              <time class="td__date-updat" datetime="2020-10-06T20:55:44+00:00">{{date("d-m-Y", strtotime(@$post->updated_at))}}</time></span></div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12 paginatee">
                            {{ @$posts->appends(Request::all())->links() }}
                        </div>
                    </div>

{{--                    <div class="pagination__title">--}}
{{--                        <h4>Bài mới nhất</h4>--}}
{{--                    </div>--}}

{{--                    <div class="pagination__inner">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-md-4 col-sm-4 col-4 px-1">--}}
{{--                                <div class="pagination__inner-thumb"><a href="post-detail.html"> <img src="./images/post/marketing.jpg"></a></div>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-8 col-sm-8 col-8">--}}
{{--                                <div class="pagination__inner-detail">--}}
{{--                                    <h3 class="block__module-title"><a href="post-detail.html">Sale marketing là gì? Cơ hội việc làm ngành sale and marketing</a></h3>--}}
{{--                                    <div class="block__module-info">--}}
{{--                                        <div class="td__name"><a href="#">Ronaldo<span>-</span></a></div>--}}
{{--                                        <div class="td__date">--}}
{{--                                            <time class="td__date-updat" datetime="2020-10-06T20:55:44+00:00">November 2, 2020</time>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="pagination-excerpt">Khi nói đến lĩnh vực kinh doanh, nhất là trong thời đại kinh tế phát triển hẳn bạn cũng đang thắc mắc định nghĩa về Sale marketing...</div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="btn__box text-center mb-3"><a class="btn btn-green mx-auto" href="#">Xem thêm</a></div>--}}
                </div>
                <div class="col-lg-4">
                    <div class="image__scroll">
                        <div class="image__scroll-item"><img src="https://blog.topcv.vn/wp-content/uploads/2020/05/Banner-blog-topcv-Sidebar-Right.png"><img src="https://blog.topcv.vn/wp-content/uploads/2020/05/Banner-blog-topcv-Sidebar-Right.png"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection