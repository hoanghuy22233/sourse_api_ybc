@extends('themeoncode::layouts.default')
@section('tailieu')
    active
@endsection
@section('main_content')
    <!-- Start Breadcrumb-->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <div class="container">
                <li class="breadcrumb-item"><i class="fas fa-home"></i><a href="index.html">Trang chủ</a></li>
                <li class="breadcrumb-item"><a href="#">{{@$post->category->name}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{@$post->name}}</li>
            </div>
        </ol>
    </nav>
    <!-- End Breadcrumb-->
    <!-- Start Key Detail-->
    <div class="postDetail">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="postDetail__box">
                        <div class="postDetail__box-title">
                            <div class="title__item">{{@$post->name}}</div>
                        </div>
                        <div class="postDetail__box-subtitle">
                            <div class="subtitle__item"> {{date('d-m-Y',strtotime($post->created_at))}}</div>
                            <div class="subtitle__item"><span>{{@$post->category->name}}</span></div>
                        </div>
                        <div class="postDetail__box-content">
                            {!! @$post->content !!}
                        </div>
                    </div>
                    <div class="newsletter">
                        <div class="newsletter__background"><img src="images/newsletter.png" alt=""></div>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="newsletter__box">
                                    <div class="newsletter__box-title">Đăng ký nhận thông báo</div>
                                    <div class="newsletter__box-text">Đăng ký ngay để nhận tin tức mới nhất, độc quyền và nhiều ưu đãi khác.</div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="newsletter__box ml-lg-auto">
                                    <div class="newsletter__box-form">
                                        <input class="form__email" id="email" type="email" placeholder="Email của bạn...">
                                    </div><a class="btn newsletter__box-button" href="#">Đăng ký ngay<i class="fal fa-paper-plane"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="image__scroll">
                        <div class="image__scroll-item"><img src="https://blog.topcv.vn/wp-content/uploads/2020/05/Banner-blog-topcv-Sidebar-Right.png"><img src="https://blog.topcv.vn/wp-content/uploads/2020/05/Banner-blog-topcv-Sidebar-Right.png"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Key Detail-->



@endsection
