<?php

namespace Modules\ThemeOncode\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeOncode\Models\Category;
use Modules\ThemeOncode\Models\Menus;
use Modules\ThemeOncode\Models\Post;

class PostController extends Controller
{
//      Trang chi tiết bài viết
    public function detail($slug1, $slug)
    {
        $data['user'] = \Auth::guard('student')->user();

        $slug = str_replace('.html', '', $slug);

//        $data['post'] = CommonHelper::getFromCache('post_slug');
//        if (!$data['post']) {
            $data['post'] = Post::where('slug', $slug)->first();
//            CommonHelper::putToCache('post_slug', $data['post']);
//        }


        if (!is_object($data['post'])) {
            abort(404);
        }

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
            'meta_description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->name,
            'meta_keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themeoncode::pages.document.document_detail', $data);
    }
    //danh sách bài viết
    public function list($slug, $r = false){

        $user = \Auth::guard('student')->user();
        $category = Category::where('slug', $slug)->first();
        if (!is_object($category)) {
            abort(404);
        }
        $data['category'] = $category;
        $data['slug1'] = $slug;

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $category->meta_title != '' ? $category->meta_title : $category->name,
            'meta_description' => $category->meta_description != '' ? $category->meta_description : $category->name,
            'meta_keywords' => $category->meta_keywords != '' ? $category->meta_keywords : $category->name,
        ];
        view()->share('pageOption', $pageOption);

        if (in_array($category->type, [1])) {         //  Tài liệu

            $data['posts'] = CommonHelper::getFromCache('posts_multi_category_id' . $category->id . ($r ? base64_encode(json_encode($r->all())) : ''), ['posts']);
            if (!$data['posts']) {
                $data['posts'] = @Post::where('multi_cat', 'like', '%|' . @$category->id . '|%')
                    ->orderBy('order_no', 'desc')->orderBy('id', 'asc')->paginate(8);
                CommonHelper::putToCache('posts_multi_category_id' . $category->id . ($r ? base64_encode(json_encode($r->all())) : ''), $data['posts'], ['posts']);
            }
            return view('themeoncode::pages.document.document')->with($data)->with('user', $user);
        }

    }
    public function about() {
        $data['post'] = Post::where('slug', 'gioi-thieu-gamali')->where('status', 1)->first();
        if (!is_object($data['post'])) {
            abort(404);
        }
        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
            'meta_description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->name,
            'meta_keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->name,
        ];
        view()->share('pageOption', $pageOption);
        return view('themeoncode::pages.about.about', $data);
    }
}
