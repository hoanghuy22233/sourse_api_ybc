<?php

namespace Modules\ThemeOncode\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\RoleAdmin;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;


use Modules\ThemeOncode\Models\Admin;
use Modules\ThemeOncode\Models\Course;
use Modules\ThemeOncode\Models\Setting;
use Validator;

class OrderController extends CURDBaseController
{
    protected $module = [
        'code' => 'order',
        'table_name' => 'orders',
        'label' => 'Đơn hàng',
        'modal' => '\Modules\ThemeOncode\Models\Order',
        'list' => [
            ['name' => 'name', 'type' => 'relation', 'object' => 'student', 'display_field' => 'name', 'label' => 'Tên khách'],
            ['name' => 'email', 'type' => 'relation', 'object' => 'student', 'display_field' => 'email', 'label' => 'Email'],
            ['name' => 'tel', 'type' => 'relation', 'object' => 'student', 'display_field' => 'phone', 'label' => 'SĐT'],
            ['name' => 'course_name', 'type' => 'relation', 'object' => 'course', 'display_field' => 'name', 'label' => 'Khóa học'],
            ['name' => 'course_price', 'type' => 'relation_price_vi', 'object' => 'course', 'display_field' => 'final_price', 'label' => 'Giá'],
            ['name' => 'status', 'type' => 'custom', 'td' => 'themeoncode::admin.list.td.status_order', 'options' => [
                0 => 'Chời kích hoạt',
                1 => 'Kích hoạt',
            ], 'label' => 'Trạng thái'],
            ],
        'form' => [
            'general_tab' => [
                ['name' => 'price', 'type' => 'text', 'class' => 'number_price', 'label' => 'Giá tiền'],
                ['name' => 'quantity', 'type' => 'text', 'class' => 'number_price', 'label' => 'Số lượng'],
                ['name' => 'course_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Khóa học', 'model' => Course::class, 'display_field' => 'name',],
                ['name' => 'student_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Sinh viên', 'model' => \Modules\ThemeOncode\Models\Student::class, 'display_field' => 'name',],

                ['name' => 'status', 'type' => 'select', 'options' => [
                    0 => 'Chời kích hoạt',
                    1 => 'Kích hoạt',
                ], 'label' => 'Trạng thái'],
                ['name' => 'content', 'type' => 'text', 'class' => 'textarea', 'label' => 'Nội dung'],

            ],
            'info_tab' => [
                ['name' => 'name', 'type' => 'text', 'label' => 'Họ tên'],
                ['name' => 'email', 'type' => 'text', 'label' => 'Email'],
                ['name' => 'tel', 'type' => 'text', 'label' => 'Số điện thoại'],
                ['name' => 'gender', 'type' => 'text', 'label' => 'Giới tính'],
                ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ'],
            ],
        ],
    ];

    protected $filter = [
//        'student_id' => [
//            'label' => 'Tên khách hàng',
//            'type' => 'select2_model',
//            'display_field' => 'name',
//            'model' => Student::class,
//            'object' => 'student',
//            'query_type' => '='
//        ],
//        'total_price' => [
//            'label' => 'Tổng tiền',
//            'type' => 'number',
//            'query_type' => 'like'
//        ],
        'created_at' => [
            'label' => 'Ngày đặt',
            'type' => 'date',
            'query_type' => 'custom'
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
        ],

    ];

    public function getIndex(Request $request)
    {

        $data = $this->getDataList($request);
        return view('themeoncode::order.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các đơn hàng của mình
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('lecturer_id', \Auth::guard('admin')->user()->id);
        }

        return $query;
    }
    public function add(Request $request)
    {
        try {

            if (!$_POST) {
                $data1['user_id'] = $request->user_id;
                $data = $this->getDataAdd($request);
                return view('themeoncode::order.add', $data1)->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
//                    'name' => 'required'
                ], [
//                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    $data['admin_id'] = \Auth::guard('admin')->id();

                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                        if ($this->model->save()) {
                            CommonHelper::flushCache($this->module['table_name']);
                            $this->afterAddLog($request, $this->model);

                            CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                        } else {
                            CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                        }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('themeoncode::order.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
//                    'name' => 'required'
                ], [
//                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    $data['admin_id'] = \Auth::guard('admin')->id();

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }



    public function getPublish(Request $request)
    {
        try {
            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0) {
                $item->{$request->column} = 1;
//                $this->updateStatusOrder($item->id, 1);
            } else {
                $item->{$request->column} = 0;
//                $this->updateStatusOrder($item->id, 0);
            }

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false,
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
//                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.',
                'msg' => $ex->getMessage(),
            ]);
        }
    }



    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }






}
