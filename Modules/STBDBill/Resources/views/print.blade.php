<!doctype html>
<html lang="en">
<head>
    @include('admin.themes.metronic1.partials.head_meta')
    @include('admin.themes.metronic1.partials.head_script')
    <style>
        .edit {
            cursor: pointer;
        }
    </style>
</head>
<body>
<script>
    $(document).ready(function () {
        $('.edit').click(function () {
            $(this).parent().find('.content').hide();
            let text = $(this).parent().find('.content').text().trim();
            $(this).parent().append('<textarea class="confirm" style="width: 100%;">' + text + '</textarea><span class="btn btn-success btn-sm confirm" style="cursor: pointer">Xác nhận</span>');
            $(this).hide();
        });
        $('body').on('click', '.btn.confirm', function () {
            $(this).parent().find('.content').show();
            let text = $(this).parent().find('textarea.confirm').val();
            $(this).parent().find('.content').text(text);
            $(this).parent().find('span.edit').show();
            $(this).parent().find('.confirm').remove();

        });
    });
</script>
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--tabs" style="width: fit-content">
                    <div class="kt-portlet__body" style="margin-left: 2%; width: fit-content">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12 p-3 header-print">
                            {!! $header_print !!}
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12 p-3 content-print">
                            <div class="container clearfix">
                                <h1 style="text-align: center">BÁO GIÁ</h1>
                                <p>
                                    <span class="edit no-print" title="Click vào để sửa">
                                        <i class="flaticon-edit"></i>
                                    </span>
                                    <span class="content"> ..., ngày ... tháng ... năm ...</span>
                                </p>
                                <p>Kính gửi: Quý khách hàng!</p>
                                <p>Chúng tôi xin gửi tới Quý khách hàng bảng báo giá in các hạng mục theo yêu cầu và
                                    mong muốn nhận được sự hợp tác của Quý Khách hàng.</p>
                                <div class="table-responsive">
                                    <table class="table cart">
                                        <thead>
                                        <tr>
                                            <th class="cart-product-thumbnail">Hình ảnh</th>
                                            <th class="cart-product-name">Sản phẩm</th>
                                            <th class="cart-product-shop">Shop</th>
                                            <th class="cart-product-price">Đơn giá</th>
                                            <th class="cart-product-quantity">Số lượng</th>
                                            <th class="cart-product-subtotal">Thành tiền</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($result->orders as $item)
                                            <tr class="cart_item">
                                                <td class="cart-product-thumbnail">
                                                    <img width="64"
                                                         height="64"
                                                         src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($item->product_image, null, null)}}"
                                                         alt="{{$item->product_name}}">
                                                </td>
                                                <td class="cart-product-name">
                                                    <span>{{$item->product_name}}</span>
                                                </td>
                                                <td class="cart-product-shop">
                                                    <a style="color: #333">{{$result->company->short_name}}</a>
                                                </td>
                                                <td class="cart-product-price">
                                                    <span class="amount"> {{number_format($item->product_price)}} đ</span>
                                                </td>
                                                <td class="cart-product-quantity">
                                                    <span>{{$item->quantity}}</span>
                                                </td>
                                                <td class="cart-product-subtotal">
                                                    <span class="amount">{{number_format($item->quantity * $item->product_price)}} đ</span>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-12 p-3 footer_print">
                            {!! $footer_print !!}
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>
</body>
</html>