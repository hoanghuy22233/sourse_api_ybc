<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions','locale']], function () {
    Route::group(['prefix' => 'menu'], function () {
        Route::get('', 'Admin\MenuController@getIndex')->middleware('permission:menu_view');
        Route::get('publish', 'Admin\MenuController@getPublish')->middleware('permission:menu_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\MenuController@add')->middleware('permission:menu_add');
        Route::get('delete/{id}', 'Admin\MenuController@delete')->middleware('permission:menu_delete');
        Route::post('multi-delete', 'Admin\MenuController@multiDelete')->middleware('permission:menu_delete');
        Route::get('search-for-select2', 'Admin\MenuController@searchForSelect2')->middleware('permission:menu_view');
        Route::get('{id}', 'Admin\MenuController@update')->middleware('permission:menu_view');
        Route::post('{id}', 'Admin\MenuController@update')->middleware('permission:menu_edit');
    });

    Route::group(['prefix' => 'banner'], function () {
        Route::get('', 'Admin\BannerController@getIndex')->name('banner')->middleware('permission:banner_view');
        Route::get('publish', 'Admin\BannerController@getPublish')->name('banner.publish')->middleware('permission:banner_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BannerController@add')->middleware('permission:banner_add');
        Route::get('delete/{id}', 'Admin\BannerController@delete')->middleware('permission:banner_delete');
        Route::post('multi-delete', 'Admin\BannerController@multiDelete')->middleware('permission:banner_delete');
        Route::get('search-for-select2', 'Admin\BannerController@searchForSelect2')->name('banner.search_for_select2')->middleware('permission:banner_view');

        Route::get('{id}', 'Admin\BannerController@update')->middleware('permission:banner_view');
        Route::post('{id}', 'Admin\BannerController@update')->middleware('permission:banner_edit');
    });

    Route::group(['prefix' => 'contact'], function () {
        Route::get('', 'Admin\ContactController@getIndex')->name('contact')->middleware('permission:contact_view');
        Route::get('publish', 'Admin\ContactController@getPublish')->name('contact.publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ContactController@add')->middleware('permission:contact_add');
        Route::get('delete/{id}', 'Admin\ContactController@delete')->middleware('permission:contact_delete');
        Route::post('multi-delete', 'Admin\ContactController@multiDelete')->middleware('permission:contact_delete');
        Route::get('search-for-select2', 'Admin\ContactController@searchForSelect2')->name('contact.search_for_select2')->middleware('permission:contact_view');
        Route::get('{id}', 'Admin\ContactController@update')->middleware('permission:contact_view');
        Route::post('{id}', 'Admin\ContactController@update')->middleware('permission:contact_edit');
    });


    Route::group(['prefix' => 'post'], function () {
        Route::get('', 'Admin\PostController@getIndex')->name('post')->middleware('permission:post_view');
        Route::get('publish', 'Admin\PostController@getPublish')->name('post.publish')->middleware('permission:post_edit');
        Route::post('multi-publish', 'Admin\PostController@enabledStatus')->middleware('permission:post_view');
        Route::post('multi-dispublish', 'Admin\PostController@disabledStatus')->middleware('permission:post_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\PostController@add')->middleware('permission:post_add');
        Route::get('delete/{id}', 'Admin\PostController@delete')->middleware('permission:post_delete');
        Route::post('multi-delete', 'Admin\PostController@multiDelete')->middleware('permission:post_delete');
        Route::get('search-for-select2', 'Admin\PostController@searchForSelect2')->name('post.search_for_select2')->middleware('permission:post_view');
        Route::get('{id}', 'Admin\PostController@update')->middleware('permission:post_view');
        Route::post('{id}', 'Admin\PostController@update')->middleware('permission:post_edit');
    });


    Route::group(['prefix' => 'category_post'], function () {
        Route::get('', 'Admin\CategoryPostController@getIndex')->name('category_post')->middleware('permission:category_post_view');
        Route::get('publish', 'Admin\CategoryPostController@getPublish')->name('category_post.publish')->middleware('permission:category_post_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\CategoryPostController@add')->middleware('permission:category_post_add');
        Route::get('delete/{id}', 'Admin\CategoryPostController@delete')->middleware('permission:category_post_delete');
        Route::post('multi-delete', 'Admin\CategoryPostController@multiDelete')->middleware('permission:category_post_delete');
        Route::get('search-for-select2', 'Admin\CategoryPostController@searchForSelect2')->name('category_post.search_for_select2')->middleware('permission:category_post_view');
        Route::get('{id}', 'Admin\CategoryPostController@update')->middleware('permission:category_post_view');
        Route::post('{id}', 'Admin\CategoryPostController@update')->middleware('permission:category_post_edit');
    });


    Route::group(['prefix' => 'related_products'], function () {
        Route::get('', 'Admin\ProductController@getIndex')->middleware('permission:post_view');
        Route::get('publish', 'Admin\ProductController@getPublish')->middleware('permission:post_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ProductController@add')->middleware('permission:post_view');
        Route::get('delete/{id}', 'Admin\ProductController@delete')->middleware('permission:post_view');
        Route::post('multi-delete', 'Admin\ProductController@multiDelete')->middleware('permission:post_view');
        Route::get('search-for-select2', 'Admin\ProductController@searchForSelect2')->middleware('permission:post_view');

        Route::get('{id}', 'Admin\ProductController@update')->middleware('permission:post_view');
        Route::post('{id}', 'Admin\ProductController@update')->middleware('permission:post_view');
    });

    Route::group(['prefix' => 'tag_post'], function () {
        Route::get('', 'Admin\TagPostController@getIndex')->name('tag_post')->middleware('permission:tag_post_view');
        Route::get('publish', 'Admin\TagPostController@getPublish')->name('tag_post.publish')->middleware('permission:tag_post_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TagPostController@add')->middleware('permission:tag_post_add');
        Route::get('delete/{id}', 'Admin\TagPostController@delete')->middleware('permission:tag_post_delete');
        Route::post('multi-delete', 'Admin\TagPostController@multiDelete')->middleware('permission:tag_post_delete');
        Route::get('search-for-select2', 'Admin\TagPostController@searchForSelect2')->name('tag_post.search_for_select2');
        Route::get('{id}', 'Admin\TagPostController@update')->middleware('permission:tag_post_view');
        Route::post('{id}', 'Admin\TagPostController@update')->middleware('permission:tag_post_edit');
    });

    Route::group(['prefix' => 'widget'], function () {
        Route::get('', 'Admin\WidgetController@getIndex')->name('widget')->middleware('permission:widget_view');
        Route::get('publish', 'Admin\WidgetController@getPublish')->name('widget.publish')->middleware('permission:widget_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\WidgetController@add')->middleware('permission:widget_add');
        Route::get('delete/{id}', 'Admin\WidgetController@delete')->middleware('permission:widget_delete');
        Route::post('multi-delete', 'Admin\WidgetController@multiDelete')->middleware('permission:widget_delete');
        Route::get('search-for-select2', 'Admin\WidgetController@searchForSelect2')->name('widget.search_for_select2')->middleware('permission:widget_view');
        Route::get('{id}', 'Admin\WidgetController@update')->middleware('permission:widget_view');
        Route::post('{id}', 'Admin\WidgetController@update')->middleware('permission:widget_edit');

    });
});



Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions','locale']], function () {

    Route::group(['prefix' => 'transport_types'], function () {
        Route::get('', 'Admin\TransportTypesController@getIndex')->name('transport_types')->middleware('permission:super_admin');
        Route::get('publish', 'Admin\TransportTypesController@getPublish')->name('transport_types.publish')->middleware('permission:super_admin');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TransportTypesController@add')->middleware('permission:super_admin');
        Route::get('delete/{id}', 'Admin\TransportTypesController@delete')->middleware('permission:super_admin');
        Route::post('multi-delete', 'Admin\TransportTypesController@multiDelete')->middleware('permission:super_admin');
        Route::get('search-for-select2', 'Admin\TransportTypesController@searchForSelect2')->name('transport_types.search_for_select2')->middleware('permission:super_admin');

        Route::get('{id}', 'Admin\TransportTypesController@update')->middleware('permission:super_admin');
        Route::post('{id}', 'Admin\TransportTypesController@update')->middleware('permission:super_admin');
    });

    Route::group(['prefix' => 'transport_product_types'], function () {
        Route::get('', 'Admin\TransportProductTypesController@getIndex')->name('transport_product_types')->middleware('permission:super_admin');
        Route::get('publish', 'Admin\TransportProductTypesController@getPublish')->name('transport_product_types.publish')->middleware('permission:super_admin');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TransportProductTypesController@add')->middleware('permission:super_admin');
        Route::get('delete/{id}', 'Admin\TransportProductTypesController@delete')->middleware('permission:super_admin');
        Route::post('multi-delete', 'Admin\TransportProductTypesController@multiDelete')->middleware('permission:super_admin');
        Route::get('search-for-select2', 'Admin\TransportProductTypesController@searchForSelect2')->name('transport_product_types.search_for_select2')->middleware('permission:super_admin');

        Route::get('{id}', 'Admin\TransportProductTypesController@update')->middleware('permission:super_admin');
        Route::post('{id}', 'Admin\TransportProductTypesController@update')->middleware('permission:super_admin');
    });

    Route::group(['prefix' => 'transport_shipping_methods'], function () {
        Route::get('', 'Admin\TransportShippingMethodsController@getIndex')->name('transport_shipping_methods')->middleware('permission:super_admin');
        Route::get('publish', 'Admin\TransportShippingMethodsController@getPublish')->name('transport_shipping_methods.publish')->middleware('permission:super_admin');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TransportShippingMethodsController@add')->middleware('permission:super_admin');
        Route::get('delete/{id}', 'Admin\TransportShippingMethodsController@delete')->middleware('permission:super_admin');
        Route::post('multi-delete', 'Admin\TransportShippingMethodsController@multiDelete')->middleware('permission:super_admin');
        Route::get('search-for-select2', 'Admin\TransportShippingMethodsController@searchForSelect2')->name('transport_shipping_methods.search_for_select2')->middleware('permission:super_admin');

        Route::get('{id}', 'Admin\TransportShippingMethodsController@update')->middleware('permission:super_admin');
        Route::post('{id}', 'Admin\TransportShippingMethodsController@update')->middleware('permission:super_admin');
    });
});
