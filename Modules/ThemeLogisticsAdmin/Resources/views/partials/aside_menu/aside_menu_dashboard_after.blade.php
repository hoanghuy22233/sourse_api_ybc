@if(in_array('theme', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon-imac"></i>
                    </span><span class="kt-menu__link-text">{{ trans('themelogisticsadmin::admin.theme') }}</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/theme?status=1" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">{{ trans('themelogisticsadmin::admin.theme') }}</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/banner" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">{{ trans('themelogisticsadmin::admin.banner') }}</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/widget" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">{{ trans('themelogisticsadmin::admin.widget') }}</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/menu" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">{{ trans('themelogisticsadmin::admin.menu') }}</span></a></li>
                {!! Eventy::filter('aside_menu.theme_menu_childs', '') !!}

                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/transport_types" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Loại vận chuyển</span></a></li>

                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/transport_product_types" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Đặc tính hàng hóa</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/transport_shipping_methods" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Phương thức vận chuyển</span></a></li>
            </ul>
        </div>
    </li>
@endif