<?php

namespace Modules\ThemeLogisticsAdmin\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;

class ProductController extends CURDBaseController
{
    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Sản phẩm',
        'modal' => '\Modules\LogisticsProduct\Models\Product',
    ];
}
