<?php

namespace Modules\ThemeLogisticsAdmin\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\LogisticsProduct\Models\Product;
use Modules\ThemeLogisticsAdmin\Models\Category;
use Modules\ThemeLogisticsAdmin\Models\Post;
use Validator;

class PostController extends CURDBaseController
{
    protected $module = [
        'code' => 'post',
        'table_name' => 'posts',
        'label' => 'Bài viết',
        'modal' => '\Modules\ThemeLogisticsAdmin\Models\Post',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên bài viết'],
            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'theme::list.td.multi_cat', 'label' => 'Danh mục', 'object' => 'category_post'],
            ['name' => 'tags', 'type' => 'custom', 'td' => 'theme::list.td.multi_cat', 'label' => 'Từ khóa', 'object' => 'tag_post'],
            ['name' => 'show_home', 'type' => 'status', 'label' => 'H.thị trang chủ', 'data' => [
                0 => 'Ẩn',
                1 => 'Hiện'
            ]],
            ['name' => 'slug', 'type' => 'text', 'label' => 'Đường dẫn tĩnh'],
            ['name' => 'order_no', 'type' => 'number', 'label' => 'Ưu tiên'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trang thái'],
            ['name' => 'view_total', 'type' => 'number', 'label' => 'Luợt xem'],
            ['name' => 'admin_id', 'type' => 'relation', 'object' => 'admin', 'display_field' => 'name', 'label' => 'Người tạo'],
            ['name' => 'view', 'type' => 'custom', 'td' => 'theme::list.td.view_post_fontend', 'label' => 'Xem'],
            ['name' => 'updated_at', 'type' => 'text', 'label' => 'Cập nhật']
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên'],
                ['name' => 'multi_cat', 'class' => 'required', 'type' => 'custom', 'field' => 'theme::form.fields.multi_cat', 'label' => 'Danh mục bài viết', 'model' => \Modules\ThemeLogisticsAdmin\Models\Category::class,
                    'object' => 'category_post', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=1', 'des' => 'Danh mục đầu tiên chọn là danh mục chính'],
                ['name' => 'intro', 'type' => 'custom', 'field' => 'theme::form.fields.count_char_intro', 'class' => '', 'label' => 'Mô tả ngắn (Tối đa 120 ký tự)'],
                ['name' => 'content', 'type' => 'textarea_editor', 'label' => 'Nội dung'],
                /*['name' => 'product_sidebar', 'type' => 'select2_ajax_model', 'class' => '', 'label' => 'Sản phẩm hiển thị sidebar', 'model' =>Product::class,
                    'object' => 'related_products', 'display_field' => 'name', 'display_field2' => 'id', 'multiple' => true],*/
                ['name' => 'type_page', 'type' => 'select', 'value' => 'post_type', 'options' =>
                    [
                        'page_static' => 'Trang tĩnh',
                        'post_type' => 'Tin tức',
                    ], 'class' => 'required', 'label' => 'Kiểu trang', 'group_class' => 'col-md-3'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'Thứ tự ưu tiên', 'value' => 0, 'group_class' => 'col-md-3', 'des' => 'Số to ưu tiên hiển thị trước'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt', 'value' => 1, 'group_class' => 'col-md-3'],
            ],
            'related_tab' => [

            ],
            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh bài viết'],
            ],

            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'Slug', 'des' => 'Đường dẫn bài viết trên thanh địa chỉ'],
                ['name' => 'meta_title', 'type' => 'custom', 'field' => 'theme::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta title', 'max_char' => 1000],
                ['name' => 'meta_description', 'type' => 'custom', 'field' => 'theme::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta description', 'max_char' => 1000],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, tên',
        'fields' => 'id, name'
    ];

    protected $filter = [
        'multi_cat' => [
            'label' => 'Danh mục',
            'type' => 'select2_model',
            'model' => Category::class,
            'where' => 'type = 1',
            'query_type' => 'like'
        ],
        'tags' => [
            'label' => 'Tags',
            'type' => 'select2_model',
            'model' => Category::class,
            'where' => 'type = 2',
            'query_type' => 'like'
        ],
        'show_home' => [
            'label' => 'Hiển thị trang chủ',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Hiển thị trang chủ',
                0 => 'Không',
                1 => 'Có'
            ]
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Ẩn',
                1 => 'Duyệt'
            ]
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('theme::post.list')->with($data);
    }

    public function appendWhere($query, $request)
    {

        //  Lọc theo danh mục
        if (!is_null($request->get('category_id'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }
        if (!is_null($request->get('tags'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {

                $data = $this->getDataAdd($request);

                return view('theme::post.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('related_products')) {
                        $data['related_products'] = '|' . implode('|', $request->related_products) . '|';
                    }
                    if ($request->has('related_posts')) {
                        $data['related_posts'] = '|' . implode('|', $request->related_posts) . '|';

                    }
                    $data['admin_id']= \Auth::guard('admin')->user()->id;
                    /*if ($request->has('product_sidebar')) {
                        $data['product_sidebar'] = '|' . implode('|', $request->product_sidebar) . '|';

                    }*/
                    if ($request->has('tags')) {
                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
                    }
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        if (!empty($request->related_posts)) {
                            foreach ($request->related_posts as $related_post) {
                                $pos = Post::find($related_post);
                                if (empty($pos->related_posts)) {

                                    $pos->related_posts = '|' . $this->model->id . '|';
                                } else {
                                    $pos->related_posts .= $this->model->id . '|';
                                }
                                $pos->save();
                            }
                        }
                        $this->afterAddLog($request, $this->model);
                        $this->adminLog($request,$this->model,'add');
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tạo mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('theme::post.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());


                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }

                    /*if ($request->has('product_sidebar')) {
                        $data['product_sidebar'] = '|' . implode('|', $request->product_sidebar) . '|';

                    }*/
                    if ($request->has('related_products')) {
                        $data['related_products'] = '|' . implode('|', $request->related_products) . '|';
                    }
                    if ($request->has('related_posts')) {
                        $data['related_posts'] = '|' . implode('|', $request->related_posts) . '|';
                        foreach ($request->related_posts as $related_post) {

                            $pos = Post::find($related_post);
                            if (empty($pos->related_posts)) {
                                $pos->related_posts = '|' . $item->id . '|';
                            } else {
                                $pos->related_posts .= $item->id . '|';
                            }
                            $pos->save();
                        }
                    }
                    if ($request->has('tags')) {
                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
                    }
                    #

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        $this->adminLog($request,$item,'edit');
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            $this->adminLog($request,$item,'publish');
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            $this->adminLog($request,$item,'delete');
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            $this->adminLog($request,$ids,'multi_delete');
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }

            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
    public function enabledStatus(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {

                foreach ($ids as $post){

                    $post = $this->model->find($post);
                    $post->status = 1;
                    $post->save();
                }
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Đổi trang thái sang kích hoạt thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
    public function disabledStatus(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                foreach ($ids as $post){
                    $post = $this->model->find($post);
                    $post->status = 0;
                    $post->save();
                }
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Đổi trạng thái sang hủy kích hoạt thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
