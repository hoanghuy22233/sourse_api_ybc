<?php

namespace Modules\ThemeLogisticsAdmin\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;

class TransportShippingMethodsController extends CURDBaseController
{

    protected $module = [
        'code' => 'transport_shipping_methods',
        'table_name' => 'transport_shipping_methods',
        'label' => 'Phương thức vận chuyển',
        'modal' => '\Modules\ThemeLogisticsAdmin\Models\TransportShippingMethods',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'name', 'type' => 'relation', 'object' => 'transport_type', 'display_field' => 'name', 'label' => 'Loại vận chuyển'],
            ['name' => 'name', 'type' => 'relation', 'object' => 'transport_product_type', 'display_field' => 'name', 'label' => 'Loại hàng hóa'],
            ['name' => 'base_price', 'type' => 'price_vi', 'label' => 'Giá ban đầu'],
            ['name' => 'final_price', 'type' => 'price_vi', 'label' => 'Giá giảm'],
            ['name' => 'order_no', 'type' => 'text', 'label' => 'Ưu tiên'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'transport_type_id', 'class' => 'required', 'type' => 'select_model', 'label' => 'Loại vận chuyển', 'model' => \Modules\ThemeLogisticsAdmin\Models\TransportTypes::class,
                    'object' => 'transport_type', 'display_field' => 'name',],
                ['name' => 'transport_product_type_id', 'class' => 'required', 'type' => 'select_model', 'label' => 'Đặc tính hàng hóa', 'model' => \Modules\ThemeLogisticsAdmin\Models\TransportProductTypes::class,
                    'object' => 'transport_product_type', 'display_field' => 'name',],
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên'],
                ['name' => 'type_calculation', 'type' => 'select', 'options' => [
                    0 => 'Trọng lượng',
                    1 => 'Số kiện'
                ], 'class' => 'required', 'label' => 'Tên'],
                ['name' => 'amount_from', 'type' => 'number', 'class' => '', 'label' => 'Số lượng/trọng lượng từ', 'group_class' => 'col-md-6'],
                ['name' => 'amount_to', 'type' => 'number', 'class' => '', 'label' => 'Số lượng/trọng lượng đến', 'group_class' => 'col-md-6'],
                ['name' => 'range_from', 'type' => 'number', 'class' => '', 'label' => 'khoảng cách từ', 'group_class' => 'col-md-6'],
                ['name' => 'range_to', 'type' => 'number', 'class' => '', 'label' => 'Khoảng cách đến', 'group_class' => 'col-md-6'],
                ['name' => 'base_price', 'type' => 'price_vi', 'class' => '', 'label' => 'Giá ban đầu', 'group_class' => 'col-md-6'],
                ['name' => 'final_price', 'type' => 'price_vi', 'class' => '', 'label' => 'Giá giảm', 'group_class' => 'col-md-6'],
                ['name' => 'final_price_date_from', 'type' => 'date', 'class' => '', 'label' => 'Ngày bắt  giảm giá', 'group_class' => 'col-md-6'],
             ['name' => 'final_price_date_to', 'type' => 'date', 'class' => '', 'label' => 'Ngày kết thúc giảm giá', 'group_class' => 'col-md-6'],
                ['name' => 'order_no', 'type' => 'number', 'class' => '', 'label' => 'Ưu tiên'],
            ],
            'info_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh đại diện'],
            ],

        ],
    ];

    protected $quick_search = [
        'label' => 'ID, tên',
        'fields' => 'id, name'
    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);

        return view('theme::transport_types.list')->with($data);
    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('theme::transport_types.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->adminLog($request,$this->model,'add');
                        $this->afterAddLog($request, $this->model);
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('theme::transport_types.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        $this->adminLog($request,$item,'edit');
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            $this->adminLog($request,$item,'publish');
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            $this->adminLog($request,$item,'delete');
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            $this->adminLog($request,$ids,'multi_delete');
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
