<?php
namespace Modules\ThemeLogisticsAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\ThemeLogistics\Models\Product;

class Contact extends Model
{

    protected $table = 'contacts';
    public $timestamps = false;



    protected $fillable = [
        'name', 'gender','email', 'tel','address', 'content','user_ip', 'product_id'
    ];

    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }

}
