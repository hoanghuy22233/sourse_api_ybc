<?php
namespace Modules\ThemeLogisticsAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransportShippingMethods extends Model
{

    protected $table = 'transport_shipping_methods';


    protected $fillable = [
        'name', 'intro', 'link', 'image', 'created_at', 'updated_at', 'location', 'status','order_no'
    ];

    public function transport_type() {
        return $this->belongsTo(TransportTypes::class, 'transport_type_id');
    }

    public function transport_product_type() {
        return $this->belongsTo(TransportProductTypes::class, 'transport_product_type_id');
    }
}
