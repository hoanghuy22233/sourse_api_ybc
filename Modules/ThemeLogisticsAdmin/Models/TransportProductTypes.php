<?php
namespace Modules\ThemeLogisticsAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransportProductTypes extends Model
{

    protected $table = 'transport_product_types';


    protected $fillable = [
        'name', 'transport_type_id', 'image', 'created_at', 'updated_at', 'order_no'
    ];

    public function transport_type() {
        return $this->belongsTo(TransportTypes::class, 'transport_type_id');
    }

}
