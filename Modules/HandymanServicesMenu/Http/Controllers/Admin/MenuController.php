<?php

namespace Modules\HandymanServicesMenu\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\A4iOrganization\Models\Organization;
use Modules\HandymanServicesMenu\Models\Menu;
use Validator;

class MenuController extends CURDBaseController
{
    protected $module = [
        'code' => 'menus',
        'table_name' => 'menus',
        'label' => 'handymanservicesmenu::admin.menu',
        'modal' => '\Modules\HandymanServicesMenu\Models\Menu',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit_trans', 'label' => 'handymanservicesmenu::admin.name'],
            ['name' => 'url', 'type' => 'text', 'class' => '', 'label' => 'handymanservicesmenu::admin.url'],
            ['name' => 'status', 'type' => 'status', 'label' => 'handymanservicesmenu::admin.status'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name_vi', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicesmenu::admin.name_vi'],
                ['name' => 'name_en', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicesmenu::admin.name_en'],
                ['name' => 'name_hu', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicesmenu::admin.name_hu'],
                ['name' => 'url', 'type' => 'text', 'class' => '', 'label' => 'handymanservicesmenu::admin.url'],
//                ['name' => 'location', 'type' => 'select', 'label' => 'handymanservicesmenu::admin.location', 'options' => [
//                    '' => 'Chọn vị trí',
//                    'main_menu' => 'handymanservicesmenu::admin.main_menu',
//                    'menu-top' => 'handymanservicesmenu::admin.menu_top',
////                    'post-menu' => 'handymanservicesmenu::admin.post_menu',
//                ],],
//                ['name' => 'parent_id', 'type' => 'custom', 'field' => 'handymanservicesmenu::form.fields.select_parent_id', 'class' => '', 'label' => 'handymanservicesmenu::admin.parent_cate'],
//                ['name' => 'type', 'type' => 'select', 'label' => 'handymanservicesmenu::admin.type', 'options' => [
//                    '' => 'Chọn thể loại',
//                    1 => 'handymanservicesmenu::admin.cate',
//                    3 => 'handymanservicesmenu::admin.page',
//                ],],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'handymanservicesmenu::admin.status'],

            ],

            'info_tab' => [
                ['name' => 'content_vi', 'type' => 'textarea_editor2', 'label' => 'handymanservicesmenu::admin.content_vi'],
                ['name' => 'content_en', 'type' => 'textarea_editor2', 'label' => 'handymanservicesmenu::admin.content_en'],
                ['name' => 'content_hu', 'type' => 'textarea_editor2', 'label' => 'handymanservicesmenu::admin.content_hu'],

            ],

        ],
    ];

    protected $filter = [
        'name_vi' => [
            'label' => 'handymanservicesmenu::admin.name_vi',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'name_en' => [
            'label' => 'handymanservicesmenu::admin.name_en',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'name_hu' => [
            'label' => 'handymanservicesmenu::admin.name_hu',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'type' => [
            'label' => 'handymanservicesmenu::admin.type',
            'type' => 'select',
            'options' => [
               '' => 'handymanservicesmenu::admin.type',
                1 => 'handymanservicesmenu::admin.cate',
                3 => 'handymanservicesmenu::admin.page',
            ],
            'query_type' => '='
        ],
        'location' => [
            'label' => 'handymanservicesmenu::admin.location',
            'type' => 'select',
            'options' => [
                '' => 'Chọn vị trí',
                'main_menu' => 'handymanservicesmenu::admin.main_menu',
                'menu-top' => 'handymanservicesmenu::admin.menu_top',
//                'post-menu' => 'handymanservicesmenu::admin.post_menu',
            ],
            'query_type' => '='
        ],
        'status' => [
            'label' => 'handymanservicesmenu::admin.status',
            'type' => 'select',
            'options' => [
                '' => 'handymanservicesmenu::admin.status',
                0 => 'handymanservicesmenu::admin.no_active',
                1 => 'handymanservicesmenu::admin.active',
            ],
            'query_type' => '='
        ],

    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);

        return view('handymanservicesmenu::list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        }

        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('handymanservicesmenu::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => trans('handymanservicesmenu::admin.name_required_form'),
                    'name_en.required' => trans('handymanservicesmenu::admin.name_required_form'),
                    'name_hu.required' => trans('handymanservicesmenu::admin.name_required_form')
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert

//                    if ($request->has('contact_info_name')) {
//                        $data['contact_info'] = json_encode($this->getContactInfo($request));
//                    }
////                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
//                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
//                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', trans('handymanservicesmenu::admin.insert_sucess'));
                    } else {
                        CommonHelper::one_time_message('error', trans('handymanservicesmenu::admin.insert_error'));
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('handymanservicesmenu::edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => trans('handymanservicesmenu::admin.name_required_form'),
                    'name_en.required' => trans('handymanservicesmenu::admin.name_required_form'),
                    'name_hu.required' => trans('handymanservicesmenu::admin.name_required_form')
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

//                    if ($request->has('contact_info_name')) {
//                        $data['contact_info'] = json_encode($this->getContactInfo($request));
//                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', trans('handymanservicesmenu::admin.update_success'));
                    } else {
                        CommonHelper::one_time_message('error', trans('handymanservicesmenu::admin.update_error'));
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', trans('handymanservicesmenu::admin.system_error'));
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'handymanservicesmenu::admin.no_record'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'handymanservicesmenu::admin.system_error'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', trans('handymanservicesmenu::admin.delete_success'));
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', trans('handymanservicesmenu::admin.system_error'));
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', trans('handymanservicesmenu::admin.delete_success'));
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'handymanservicesmenu::admin.system_error'
            ]);
        }
    }
    public function locationMenu(Request $request){

        $menu_parent = Menu::where('location',$request->location_menu)->where('status',1)->where('type','<>',2)->get();
        $html=' <option value="0">Chọn danh mục cha</option>';
        foreach ($menu_parent as $value){
            $html.='<option value='.$value->id.'>'.$value->name_vi.'</option>';
        }
        return response()->json([
            'status' => true,
            'html' => $html
        ]);
    }
    public function typeMenu(Request $request)
    {
        if ($request->type_menu == 3){
            return response()->json([
                'status' => true,
                'type_menu' => $request->type_menu
            ]);
        }else{
            return response()->json([
                'status' => false,
                'type_menu' => 'handymanservicesmenu::admin.constant'
            ]);
        }

    }
}
