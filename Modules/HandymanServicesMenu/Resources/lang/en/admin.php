<?php
return [
    'choose' => 'choose',
    'menu' => 'Menu',
    'add' => 'add',
    'edit' => 'edit',
    'back' => 'back',
    'save' => 'save',
    'save_and_continue' => 'save and continue',
    'save_and_quit' => 'save and quit',
    'save_and_create' => 'save and create',
    'basic_information' => 'basic information',
    'search' => 'search',
    'action' => 'action',
    'choose_action' => 'choose action',
    'export_excel' => 'Export Excel',
    'multi_delete' => 'Delete multiple',
    'create_new' => 'create new',
    'filter' => 'filter',
    'reset' => 'Reset',
    'display' => 'display',
    'id' => 'ID',
    'image' => 'image',
    'info_menu' => 'Information menu',
    'content' => 'content',
    'name' => 'name',
    'type' => 'type',
    'url' => 'url',
    'cate' => 'Categories',
    'page' => 'Page',
    'parent' => 'parent',
    'location' => 'location',
    'status' => 'status',
    'name_vi' => 'name(vi)',
    'name_en' => 'name(en)',
    'name_hu' => 'name(hu)',
    'parent_cate' => 'parent categories',
    'content_vi' => 'content(vi)',
    'content_en' => 'content(en)',
    'content_hu' => 'content(hu)',
    'main_menu' => 'Main menu',
    'menu-top' => 'Menu top',
    'post-menu' => 'Post menu item',
    'active' => 'Active',
    'no_active' => 'No active',
    'update_success' => 'Update successful!',
    'update_error' => 'Error updating. Please reload the page and try again!',
    'delete_success' => 'Delete successful!',
    'name_required_form' => 'Do not leave blank!',
    'insert_sucess' => 'Create new success!',
    'insert_error' => 'Error creating new. Please reload the page and try again!',
    'system_error' => 'System error! Please contact a technician!',
    'no_record' => 'No records found!',
    'constant' => 'Constant',
];