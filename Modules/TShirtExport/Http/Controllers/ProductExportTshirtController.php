<?php

namespace Modules\TShirtExport\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\TShirtExport\Entities\Product;
use Validator;
use Excel;
use Storage;

//use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class ProductExportTshirtController extends CURDBaseController
{
    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Sản phẩm',
        'modal' => '\Modules\TShirtExport\Entities\Product',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Product Name'],
            ['name' => 'size_regular_price', 'type' => 'custom', 'td' => 'exporttshirt::list.td.size_regular_price', 'label' => 'Size - regular price'],
            ['name' => 'tags', 'type' => 'text', 'label' => 'Tags'],
            ['name' => 'category', 'type' => 'text', 'label' => 'Category'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'label' => 'Product Name'],
                ['name' => 'description', 'type' => 'textarea', 'inner' => 'rows=15', 'label' => 'Description'],
                ['name' => 'tags', 'type' => 'text', 'label' => 'Tags'],
                ['name' => 'category', 'type' => 'text', 'label' => 'Category'],
                ['name' => 'link', 'type' => 'text', 'label' => 'Link', 'class' => 'required'],
            ],
            'size_regular_price' => [
                ['name' => 'size_regular_price', 'type' => 'custom', 'field' => 'exporttshirt::form.fields.dynamic4', 'object' => 'tours', 'class' => 'required', 'label' => 'Địa chỉ'],

            ],
        ],
    ];

    protected $filter = [

    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('exporttshirt::list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('exporttshirt::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
//                    'color' => 'required',
//                    'image' => 'required',

                ], [
//                    'color.required' => 'Bắt buộc phải nhập color',
//                    'image.required' => 'Bắt buộc phải chọn ảnh',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert


                    if ($request->has('size')) {
                        $size_regular_price = [];
                        foreach ($request->size as $k => $v) {
                            $size_regular_price[] = [
                                'size' => $v,
                                'regular_price' => $request->regular_price[$k],
                            ];
                        }
                        $data['size_regular_price'] = json_encode($size_regular_price);
                    }
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }
                    if ($this->model->save()) {
//                        $this->afterAddLog($request, $this->model);
                        CommonHelper::flushCache();
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }


    public function update(Request $request)
    {
        $item = $this->model->find($request->id);
        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('exporttshirt::edit')->with($data);
        } else if ($_POST) {
            $validator = Validator::make($request->all(), [
//                'color' => 'required',
//                'image' => 'required',
            ], [
//                'color.required' => 'Bắt buộc phải nhập color',
//                'image.required' => 'Bắt buộc phải chọn ảnh',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert
                if ($request->has('size')) {
                    $size_regular_price = [];
                    foreach ($request->size as $k => $v) {
                        $size_regular_price[] = [
                            'size' => $v,
                            'regular_price' => $request->regular_price[$k],
                        ];
                    }
                    $data['size_regular_price'] = json_encode($size_regular_price);
                }
                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::flushCache();
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function returnError($data, $request)
    {
        CommonHelper::one_time_message('error', $data['msg']);
        return redirect()->back();
    }

    public function getPublish(Request $request)
    {
        try {

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            $this->adminLog($request, $item, 'publish');
            CommonHelper::flushCache();
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
//            $this->adminLog($request, $item, 'delete');
            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
//            $this->adminLog($request, $ids, 'multi_delete');
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }

            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function duplicate(Request $request, $id)
    {

        $poduct = Product::find($id);
        $poduct_new = $poduct->replicate();
//        $poduct_new->company_id = \Auth::guard('admin')->user()->last_company_id;
        $poduct_new->admin_id = \Auth::guard('admin')->user()->id;
        $poduct_new->save();

        return $poduct_new;
    }

    public function enabledStatus(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                foreach ($ids as $product) {
                    $product = $this->model->find($product);
                    $product->status = 1;
                    $product->save();
                }
            }
            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Đổi trang thái sang kích hoạt thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


    public function getDataExport(Request $request)
    {

        $ids = explode(',', trim($request->ids, ','));
        $datas = Product::whereIn('id', $ids)->get();
        $this->export($request, $datas);
    }

    public function export(Request $request, $datas)
    {
        $config = [
            'excel.csv' => [
                'use_bom' => true,
                'delimiter' => ',',
            ],
        ];
        config($config);

        \Excel::create(str_slug($this->module['label'], '_') . '_' . date('d_m_Y'), function ($excel) use ($datas) {
            // Set the title
            $excel->setTitle(str_slug($this->module['label'], '_') . ' ' . date('d m Y'));
            $excel->sheet($this->module['label'] . '_' . date('d_m_Y'), function ($sheet) use ($datas) {

                $field_name[] = 'Product Name';
                $field_name[] = 'Description';
                $field_name[] = 'Regular Price';
                $field_name[] = 'Tags';
                $field_name[] = 'Category';
                $field_name[] = 'Color';
                $field_name[] = 'Size';
                $field_name[] = 'Image';
                $sheet->row(1, $field_name);
                $k = 2;

                $color = Setting::where('name', 'color_setting')->where('type', 'export_tab')->value('value');
                $colors = explode(',', trim($color));

                foreach ($datas as $data) {
                    $size_regular_prices = json_decode($data->size_regular_price);

                    $link_driver = explode('folders/', $data->link);
                    if (isset($link_driver[1])) {
                        $config = [
                            'filesystems.disks.google.folderId' => $link_driver[1],
                        ];
                        config($config);
                        $dir = '/';
                        $recursive = true; // Get subdirectories also?
//                      Tìm tới các folder sp bên trong đầu vào
                        $contents = collect(Storage::cloud()->listContents($dir, $recursive));
                        $products = [];
                        foreach ($contents as $item) {
                            if ($item['type'] == 'dir') {
                                $products[$item['path']]['name'] = $item['name'];
                            } elseif ($item['type'] == 'file') {
                                if (!isset($products[$item['dirname']]['images'])) {
                                    $products[$item['dirname']]['images'] = [];
                                }
                                $products[$item['dirname']]['images'][] = $item['filename'];
                                $products[$item['dirname']]['extension'][] = $item['extension'];
                            }
                        }
                        foreach ($size_regular_prices as $size_regular_price) {
                            foreach ($products as $product_name) {
                                foreach ($product_name['images'] as $img => $image) {
                                    $colorr='';
                                    foreach ($colors as $val) {
                                        if (strpos($image, $val) !== false) {
                                            $colorr = explode($val, $image)[1];
                                        }
                                    }

                                    $data_export = [];
                                    $data_export[] = $product_name['name'];
                                    $data_export[] = $this->getRanDomString($data->description, '{', '}', '|');
                                    $data_export[] = '$' . $size_regular_price->regular_price;
                                    $data_export[] = $data->tags;
                                    $data_export[] = $data->category;
                                    $data_export[] = $colorr;
                                    $data_export[] = $size_regular_price->size;
                                    $data_export[] = $image . '-scaled.' . $product_name['extension'][$img];
                                    $sheet->row($k, $data_export);
                                    $k++;

                                }
                            }
                        }
                    } else {
                        $sheet->row($k, []);
                    }
                }
            });
        })->download('csv');

    }

//$string: chuỗi truyền vào
//$delimiter: ký tự ngăn cách các chuỗi cần random
//$wrap: Ký tự bao bọc chuỗi muốn random.
//VD: {a|b|c} => $wrap1 = { , $wrap2 = } , $delimiter= |
    public function getRanDomString($string, $wrap1, $wrap2, $delimiter)
    {
        $descriptions = explode($wrap1, $string);

        $str = $descriptions[0];
        unset($descriptions[0]);
        foreach ($descriptions as $description) {
            $arr = explode($wrap2, $description);
            $a = explode($delimiter, $arr[0]);

            $b = array_rand($a, 1);

            $str .= $a[$b] . $arr[1];

        }
        return $str;
    }

    public function getAllImageFolder($folder)
    {
        //Get a list of file paths using the glob function.
        $fileList = glob(base_path() . '/public/filemanager/userfiles/' . $folder . '/*');
        return $fileList;
    }

    public function disabledStatus(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                foreach ($ids as $product) {
                    $product = $this->model->find($product);
                    $product->status = 0;
                    $product->save();
                }
            }
            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Đổi trạng thái sang hủy kích hoạt thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
