<?php

namespace Modules\TShirtExport\Http\Controllers;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Illuminate\Http\Request;

class ColorSettingTshirtController extends CURDBaseController
{
    protected $module = [
        'code' => 'setting',
        'label' => 'Cấu Hình Màu',
        'modal' => '\App\Models\Setting',
        'tabs' => [
            'export_tab' => [
                'label' => 'Ký tự phân tách',
                'icon' => '<i class="kt-menu__link-icon flaticon2-layers-2"></i>',
                'intro' => '',
                'td' => [
                    ['type' => 'text', 'class' => '', 'label' => 'Nhập ký tự phân tách', 'name' => 'color_setting'],
                ],
            ],
        ]
    ];

    public function setting(Request $request)
    {

        $data['page_type'] = 'list';

        $module = \Eventy::filter('theme.custom_module', $this->module);
        if (!$_POST) {
            $listItem = $this->model->get();
            $tabs = [];
            foreach ($listItem as $item) {
                $tabs[$item->type][$item->name] = $item->value;
            }
            #
            $data['tabs'] = $tabs;
            $data['page_title'] = $module['label'];
            $data['module'] = \Eventy::filter('theme.custom_module', $module);
            return view(config('core.admin_theme') . '.setting.view')->with($data);
        } else {
            foreach ($module['tabs'] as $type => $tab) {
                $data = $this->processingValueInFields($request, $tab['td'], $type . '_');

                //  Tùy chỉnh dữ liệu insert
                #
                foreach ($data as $key => $value) {
                    $item = Setting::where('name', $key)->where('type', $type)->first();
                    if (!is_object($item)) {
                        $item = new Setting();
                        $item->name = $key;
                        $item->type = $type;
                    }
                    $item->value = $value;
                    $item->save();
                }
            }

            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Cập nhật thành công!');

            if ($request->return_direct == 'save_exit') {
                return redirect('admin/dashboard');
            }
            return back();
        }
    }
}
