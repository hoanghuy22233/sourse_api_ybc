<li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
            href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                    <i
                            class="kt-menu__link-icon flaticon-imac"></i>
                </span><span class="kt-menu__link-text">Sản phẩm</span><i
                class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <li class="kt-menu__item " aria-haspopup="true"><a
                        href="/admin/product" class="kt-menu__link "><i
                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                            class="kt-menu__link-text">Danh sách sản phẩm</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a
                        href="/admin/product/add" class="kt-menu__link "><i
                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                            class="kt-menu__link-text">Tạo mới sản phẩm</span></a></li>
            <li class="kt-menu__item " aria-haspopup="true"><a
                        href="/admin/color-setting" class="kt-menu__link "><i
                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                            class="kt-menu__link-text">Cấu hình màu</span></a></li>
        </ul>
    </div>
</li>