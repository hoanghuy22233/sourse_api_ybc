<a class="kt-nav__link export-excel" title="Xuất các bản ghi đang lọc ra file excel" href="/admin/product/export-excel/{{$item->id}}">
    <i class="kt-nav__link-icon la la-file-excel-o"></i>
    <span class="kt-nav__link-text">Export</span>
</a>