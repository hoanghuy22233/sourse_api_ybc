<style>
    .form-group-dynamic .fieldwrapper > div:nth-child(1) {
        padding-left: 0;
    }

    .fieldwrapper {
        padding: 5px;
        border: 1px solid #ccc;
        margin-bottom: 5px;
    }
</style>
<fieldset id="buildyourform-{{ $field['name'] }}" class="{{ @$field['class'] }}">

    @if(isset($result))
        <?php
        $sizes = json_decode($result->size_regular_price);
        ?>
        @foreach ($sizes as $v)
            <div class="fieldwrapper row" id="field">
                <div class="col-xs-10 col-md-10">
                    <div class="row">
                        <div class="col-xs-6 col-md-6">
                            <input type="text" class="form-control fieldname"
                                   name="size[]" value="{{ $v->size }}"
                                   placeholder="Size" required>
                        </div>
                        <div class="col-xs-6 col-md-6">
                            <input type="text" class="form-control fieldname"
                                   name="regular_price[]" value="{{ $v->regular_price }}"
                                   placeholder="Regular_price" required>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 col-md-2 " style="text-align: right;">
                    <i type="xóa hàng" style="cursor: pointer;"
                       class="btn remove btn btn-danger btn-icon la la-remove"></i>
                </div>
            </div>
        @endforeach
    @else
        <div class="fieldwrapper row" id="field">
            <div class="col-xs-10 col-md-10">
                <div class="row">
                    <div class="col-xs-6 col-md-6">
                        <input type="text" class="form-control fieldname"
                               name="size[]" value=""
                               placeholder="Size" required>
                    </div>
                    <div class="col-xs-6 col-md-6">
                        <input type="text" class="form-control fieldname"
                               name="regular_price[]" value=""
                               placeholder="Regular_price" required>
                    </div>
                </div>
            </div>
            <div class="col-xs-2 col-md-2 " style="text-align: right;">
                <i type="xóa hàng" style="cursor: pointer;"
                   class="btn remove btn btn-danger btn-icon la la-remove"></i>
            </div>
        </div>
    @endif
</fieldset>
<a class="btn btn btn-primary btn-add-dynamic" style="color: white; margin-top: 20px; cursor: pointer;">
    <span>
        <i class="la la-plus"></i>
        <span>Thêm thuộc tính</span>
    </span>
</a>
<script>
    $(document).ready(function () {
        $(".btn-add-dynamic").click(function () {
            var lastField = $("#buildyourform-{{ $field['name'] }} div:last");
            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $('<div class="fieldwrapper row" style="margin-bottom: 5px;" id="field' + intId + '"/>');
            fieldWrapper.data("idx", intId);
            var fields = $('<div class="col-xs-10 col-md-10">\n' +
                '                            <div class="row">\n' +
                '                                <div class="col-xs-6 col-md-6">\n' +
                '                                    <input type="text" class="form-control fieldname"\n' +
                '                                                                           name="size[]" value=""\n' +
                '                                                                           placeholder="Size" required>\n' +
                '                                </div>\n' +
                '                                 <div class="col-xs-6 col-md-6">\n' +
                '                                    <input type="text" class="form-control fieldname"\n' +
                '                                           name="regular_price[]" value=""\n' +
                '                                           placeholder="Regular_price" required>\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>');
            var removeButton = $('<div class="col-xs-2 col-md-2" style="text-align: right;"><i type="xóa hàng" style="cursor: pointer;" class="btn remove btn btn-danger btn-icon la la-remove" ></i></div>');

            fieldWrapper.append(fields);
            fieldWrapper.append(removeButton);
            $("#buildyourform-{{ $field['name'] }}").append(fieldWrapper);
        });
        $('body').on('click', '.remove', function () {
            $(this).parents('.fieldwrapper').remove();
        });
    });
</script>