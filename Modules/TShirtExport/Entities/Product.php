<?php

namespace Modules\TShirtExport\Entities;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];
}
