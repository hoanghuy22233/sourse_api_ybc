<?php

namespace Modules\HandymanServicesBooking\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\EworkingCompany\Models\Company;
use Modules\HandymanServicesBooking\Models\Service;
use Validator;

class ServiceController extends CURDBaseController
{

    protected $module = [
        'code' => 'service',
        'table_name' => 'services',
        'label' => 'handymanservicesbooking::admin.service',
        'modal' => '\Modules\HandymanServicesBooking\Models\Service',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'handymanservicesbooking::admin.image', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit_trans', 'label' => 'handymanservicesbooking::admin.name', 'sort' => true],
            ['name' => 'intro', 'type' => 'text', 'label' => 'handymanservicesbooking::admin.describe', 'sort' => true],
            ['name' => 'status', 'type' => 'status', 'label' => 'handymanservicesbooking::admin.status'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name_vi', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicesbooking::admin.name_vi'],
                ['name' => 'name_en', 'type' => 'text','class' => 'required','label' => 'handymanservicesbooking::admin.name_en'],
                ['name' => 'name_hu', 'type' => 'text','class' => 'required','label' => 'handymanservicesbooking::admin.name_hu'],
                ['name' => 'slug', 'type' => 'text', 'label' => 'handymanservicesbooking::admin.url'],
                ['name' => 'intro_vi', 'type' => 'text', 'label' => 'handymanservicesbooking::admin.intro_vi'],
                ['name' => 'intro_en', 'type' => 'text', 'label' => 'handymanservicesbooking::admin.intro_en'],
                ['name' => 'intro_hu', 'type' => 'text', 'label' => 'handymanservicesbooking::admin.intro_hu'],
                ['name' => 'content_vi', 'type' => 'textarea_editor2', 'label' => 'handymanservicesbooking::admin.content_vi'],
                ['name' => 'content_en', 'type' => 'textarea_editor2', 'label' => 'handymanservicesbooking::admin.content_en'],
                ['name' => 'content_hu', 'type' => 'textarea_editor2', 'label' => 'handymanservicesbooking::admin.content_hu'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'handymanservicesbooking::admin.active','value' => 1, 'group_class' => 'col-md-4'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'handymanservicesbooking::admin.priority', 'value' => 0, 'group_class' => 'col-md-4'],
            ],
            'image_tab' => [
                ['name' => 'image', 'type' => 'file_image', 'label' => 'Ảnh'],

          ],
        ]
    ];

    protected $filter = [
        'name_vi' => [
            'label' => 'handymanservicesbooking::admin.name_vi',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'name_en' => [
            'label' => 'handymanservicesbooking::admin.name_en',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'name_hu' => [
            'label' => 'handymanservicesbooking::admin.name_hu',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'intro' => [
            'label' => 'handymanservicesbooking::admin.describe',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'status' => [
            'label' => 'handymanservicesbooking::admin.status',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'handymanservicesbooking::admin.status',
                0 => 'handymanservicesbooking::admin.no_active',
                1 => 'handymanservicesbooking::admin.active',
            ],
        ],

    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);

        return view('handymanservicesbooking::service.list')->with($data);
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('handymanservicesbooking::service.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => trans('handymanservicesbooking::admin.name_required_form'),
                    'name_en.required' => trans('handymanservicesbooking::admin.name_required_form'),
                    'name_hu.required' => trans('handymanservicesbooking::admin.name_required_form')
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', trans('handymanservicesbooking::admin.insert_sucess'));
                    } else {
                        CommonHelper::one_time_message('error', trans('handymanservicesbooking::admin.insert_error'));
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

//    public function getTimePrice($request)
//    {
//        $time_price = [];
//        if ($request->has('time_price_use_date_max')) {
//            foreach ($request->time_price_use_date_max as $k => $key) {
//                if ($key != null) {
//                    $time_price[] = [
//                        'use_date_max' => $key,
//                        'price' => $request->time_price_price[$k],
//                    ];
//                }
//            }
//        }
//        return $time_price;
//    }

    public function update(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            if (!is_object($item)){
                abort(404);
            }
            if (!$_POST) {

                $data = $this->getDataUpdate($request, $item);
                return view('handymanservicesbooking::service.edit')->with($data);
            } else if ($_POST) {

                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => trans('handymanservicesbooking::admin.name_required_form'),
                    'name_en.required' => trans('handymanservicesbooking::admin.name_required_form'),
                    'name_hu.required' => trans('handymanservicesbooking::admin.name_required_form')
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', trans('handymanservicesbooking::admin.update_success'));
                    } else {
                        CommonHelper::one_time_message('error', trans('handymanservicesbooking::admin.update_error'));
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', trans('handymanservicesbooking::admin.system_error'));
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'handymanservicesbooking::admin.no_record'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'handymanservicesbooking::admin.system_error'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            $item->delete();

            CommonHelper::one_time_message('success', trans('handymanservicesbooking::admin.delete_success'));
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', trans('handymanservicesbooking::admin.system_error'));
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {

            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', trans('handymanservicesbooking::admin.delete_success'));
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'handymanservicesbooking::admin.system_error'
            ]);
        }
    }

    public function getPriceInfo($request)
    {
        $price = [];
        if ($request->has('price_day')) {
            foreach ($request->price_day as $k => $key) {
                if ($key != null) {
                    $price[] = [
                        'day' => $key,
                        'price' => str_replace(',', '', str_replace('.', '', $request->price_price[$k])),
                    ];
                }
            }
        }
        return $price;
    }

    public function show()
    {
        $data['page_title'] = 'Các gói dịch vụ';
        $data['page_type'] = 'list';
        return view('handymanservicesbooking::service.show')->with($data);
    }

//    public function ajaxResolePrice(Request $request)
//    {
//
////      Gói cpi đăng kí mới
//        $service = Service::find(@$request->service_id);
////Gói đang dùng
//        $service_current = Service::find(@Company::find(\Auth::guard('admin')->user()->last_company_id)->service_id);
//
//        if (@$service->account_max <= @$service_current->account_max) {
////            Gia hạn
//            $service_type = 1;
//        } else {
////            Nâng cấp
//            $service_type = 2;
//        }
//
//        $data = $this->resolePrice($service, $request->service_id, $request->use_date_max, $service_type);
//
//        if ($service_type == 1) {
//            $data['note'] = 'Tiền thanh toán = Tiền của gói mới';
//        }
//        if ($service_type == 2) {
//            $data['note'] = 'Tiền thanh toán = Tiền gói mới - Số ngày còn lại của gói cũ x Giá lớn nhất của gói đang dùng / số ngày sử dụng lớn nhất của gói đang dùng';
//        }
//
//        return response()->json($data);
//    }


    public function resolePrice($service = false, $service_id, $use_date_max, $service_type)
    {
        $price = 0;
        //lấy money của gói dịch vụ đang hoạt động của công ty đang muốn nâng cấp trong bảng Service_history
        $price_history = ServiceHistory::select('service_id', 'payment', 'use_date_max')->where('company_id', \Auth::guard('admin')->user()->last_company_id)->where('status', 1)->first();
//        dd($price_history);
        //Lấy giá tiền của gói dịch vụ cpi đăng ký mới.
        if (!$service) {
            $service = Service::find($service_id);
        }
        $service_price = json_decode($service->price);

        $money = 0;
        if (is_array($service_price)) {
            foreach ($service_price as $v) {
                if ($v->day == $use_date_max) {
                    $money = $v->price;
                }
            }

            if ($money == 0) {
                $money = $service_price[0]->price;
            }
        }

        $date_option = '';
        if (is_array($service_price)) {

            foreach ($service_price as $v) {
                $selected = $v->day == $use_date_max ? 'selected' : '';
                $date_option .= '<option value="' . $v->day . '" '.$selected.'>' . $v->day . ' ngày</option>';
            }
        }

        //lấy ngày hết hạn gói dịch vụ trong bảng companies
        $day = \Modules\EworkingCompany\Models\Company::select('exp_date')->where('id', \Auth::guard('admin')->user()->last_company_id)->first();

        //Tính số ngày chênh lệch từ ngày muốn gia hạn đến ngày hết hạn gói dịch vụ đang sử dụng
        $exp_date = strtotime(@$day->exp_date);
        $today = strtotime(date('Y-m-d'));
        $date_diff = abs($exp_date - $today);
        $day_abs = floor($date_diff / (60 * 60 * 24));

        if ($service_type == 1) {
            $price += $money;
        }
        if ($service_type == 2) {
            $max_day = 0;
            $max_price = 0;
//            Gói dịch vụ đang dùng
            $service_cur = json_decode(Service::find($price_history->service_id)->price);;
            if (is_array($service_cur)){
                foreach ($service_cur as $val) {
                    if ($val->day > $max_day) {
                        $max_day = $val->day;
                    }
                    if ($val->price > $max_price) {
                        $max_price = $val->price;
                    }
                }
            }

            $price += $money - $day_abs * $max_price / $max_day;
        }

        return [
            'payment' => number_format($price, 0, '.', '.') . '<sup>EUR</sup>',
            'price' => $price,
            'service_price' => number_format($money, 0, '.', '.') . '<sup>EUR</sup>',
            'date_option' => $date_option
        ];
    }
}
