<?php

namespace Modules\HandymanServicesBooking\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\HandymanServicesBooking\Models\Service;
use Validator;

class ServiceFieldsController extends CURDBaseController
{
    protected $module = [
        'code' => 'service_field',
        'table_name' => 'service_fields',
        'label' => 'handymanservicesbooking::admin.input_service',
        'modal' => '\Modules\HandymanServicesBooking\Models\ServiceFields',
        'list' => [
            ['name' => 'service_id', 'type' => 'relation_trans_edit', 'label' => 'handymanservicesbooking::admin.service', 'object' => 'service', 'display_field' => 'name'],
            ['name' => 'name', 'type' => 'text_edit_trans', 'label' => 'handymanservicesbooking::admin.name'],
            ['name' => 'intro', 'type' => 'text_edit_trans', 'label' => 'handymanservicesbooking::admin.describe'],
            ['name' => 'hours', 'type' => 'number', 'label' => 'handymanservicesbooking::admin.hours'],
            ['name' => 'status', 'type' => 'status', 'label' => 'handymanservicesbooking::admin.status'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'service_id', 'type' => 'select2_model_trans', 'class' => '', 'label' => 'handymanservicesbooking::admin.service', 'model' => Service::class, 'display_field' => 'name',],
                ['name' => 'name_vi', 'type' => 'text', 'label' => 'handymanservicesbooking::admin.name_vi'],
                ['name' => 'name_en', 'type' => 'text', 'label' => 'handymanservicesbooking::admin.name_en'],
                ['name' => 'name_hu', 'type' => 'text', 'label' => 'handymanservicesbooking::admin.name_hu'],
                ['name' => 'intro_vi', 'type' => 'textarea', 'label' => 'handymanservicesbooking::admin.intro_vi'],
                ['name' => 'intro_en', 'type' => 'textarea', 'label' => 'handymanservicesbooking::admin.intro_en'],
                ['name' => 'intro_hu', 'type' => 'textarea', 'label' => 'handymanservicesbooking::admin.intro_hu'],
                ['name' => 'hours', 'type' => 'number', 'label' => 'handymanservicesbooking::admin.hours'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'handymanservicesbooking::admin.active', 'value' => 1, 'group_class' => 'col-md-6'],

            ],

//            'info_tab' => [
//                ['name' => 'image', 'type' => 'file_image', 'label' => 'Ảnh'],
//
//            ],

        ],
    ];

    protected $filter = [
        'name_vi' => [
            'label' => 'handymanservicesbooking::admin.service',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'name' => [
            'label' => 'handymanservicesbooking::admin.name',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'intro' => [
            'label' => 'handymanservicesbooking::admin.describe',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'hours' => [
            'label' => 'handymanservicesbooking::admin.hours',
            'type' => 'number',
            'query_type' => 'like'
        ],
        'status' => [
            'label' => 'handymanservicesbooking::admin.status',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'handymanservicesbooking::admin.status',
                0 => 'handymanservicesbooking::admin.no_active',
                1 => 'handymanservicesbooking::admin.active',
            ],
        ],

    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);

        return view('handymanservicesbooking::servicefield.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        }

        return $query;
    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('handymanservicesbooking::servicefield.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => trans('handymanservicesbooking::admin.name_required_form'),
                    'name_en.required' => trans('handymanservicesbooking::admin.name_required_form'),
                    'name_hu.required' => trans('handymanservicesbooking::admin.name_required_form')
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

//                    if ($request->has('contact_info_name')) {
//                        $data['contact_info'] = json_encode($this->getContactInfo($request));
//                    }
//                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', trans('handymanservicesbooking::admin.insert_sucess'));
                    } else {
                        CommonHelper::one_time_message('error', trans('handymanservicesbooking::admin.insert_error'));
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('handymanservicesbooking::servicefield.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => trans('handymanservicesbooking::admin.name_required_form'),
                    'name_en.required' => trans('handymanservicesbooking::admin.name_required_form'),
                    'name_hu.required' => trans('handymanservicesbooking::admin.name_required_form')
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

//                    if ($request->has('contact_info_name')) {
//                        $data['contact_info'] = json_encode($this->getContactInfo($request));
//                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', trans('handymanservicesbooking::admin.update_success'));
                    } else {
                        CommonHelper::one_time_message('error', trans('handymanservicesbooking::admin.update_error'));
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', trans('handymanservicesbooking::admin.system_error'));
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'handymanservicesbooking::admin.no_record'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'handymanservicesbooking::admin.system_error'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', trans('handymanservicesbooking::admin.delete_success'));
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', trans('handymanservicesbooking::admin.system_error'));
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', trans('handymanservicesbooking::admin.delete_success'));
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'handymanservicesbooking::admin.system_error'
            ]);
        }
    }

}
