<?php

namespace Modules\HandymanServicesBooking\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Admin;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\HandymanServicesBooking\Models\Booking;
use Validator;

class BookingController extends CURDBaseController
{

    protected $orderByRaw = 'status ASC';

    protected $module = [
        'code' => 'booking',
        'table_name' => 'bookings',
        'label' => 'handymanservicesbooking::admin.booking',
        'modal' => '\Modules\HandymanServicesBooking\Models\Booking',
        'list' => [
            ['name' => 'service_id', 'type' => 'relation_trans', 'object' => 'service', 'display_field' => 'name', 'label' => 'handymanservicesbooking::admin.request'],
            ['name' => 'user_id', 'type' => 'custom', 'td' => 'handymanservicesbooking::list.td.user_name', 'object' => 'user', 'display_field' => 'name', 'label' => 'handymanservicesbooking::admin.customer',],
            ['name' => 'date_start', 'type' => 'custom', 'td' => 'handymanservicesbooking::list.td.date_service', 'label' => 'handymanservicesbooking::admin.workday'],
            ['name' => 'price', 'type' => 'price_eur', 'label' => 'handymanservicesbooking::admin.price'],
            ['name' => 'note', 'type' => 'text', 'label' => 'handymanservicesbooking::admin.note'],
//            ['name' => 'payed', 'type' => 'select', 'label' => 'handymanservicesbooking::admin.pay', 'options' => [
//                0 => 'handymanservicesbooking::admin.unpaid',
//                1 => 'handymanservicesbooking::admin.paid',
//            ],
//            ],
            ['name' => 'payed', 'type' => 'custom', 'td' => 'handymanservicesbooking::list.td.status_pay', 'label' => 'handymanservicesbooking::admin.pay'],
            ['name' => 'status', 'type' => 'custom', 'td' => 'handymanservicesbooking::list.td.select_status', 'label' => 'handymanservicesbooking::admin.status',
                'options' => [
                    'Pending' => 'handymanservicesbooking::admin.pending',
                    'Received' => 'handymanservicesbooking::admin.received',
                    'Complete' => 'handymanservicesbooking::admin.complete',
                    'No Complete' => 'handymanservicesbooking::admin.no_complete',
                    'Cancel' => 'handymanservicesbooking::admin.cancel',
                ],
            ],
            ['name' => 'admin_id', 'type' => 'custom', 'td' => 'handymanservicesbooking::list.td.admin_id', 'label' => 'handymanservicesbooking::admin.employees'],
            ['name' => 'action', 'type' => 'custom', 'td' => 'handymanservicesbooking::list.td.btn_booking', 'label' => 'handymanservicesbooking::admin.action']
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'service_id', 'type' => 'select2_ajax_model_trans', 'class' => '',
                    'label' => 'handymanservicesbooking::admin.request', 'model' => \Modules\HandymanServicesBooking\Models\Service::class,
                    'object' => 'service', 'display_field' => 'name'],
                ['name' => 'date_start', 'type' => 'date_vi', 'class' => '', 'label' => 'handymanservicesbooking::admin.workday'],
                ['name' => 'time_start', 'type' => 'custom', 'field' => 'handymanservicesbooking::form.fields.time', 'label' => 'handymanservicesbooking::admin.time_start'],
                ['name' => 'time_end', 'type' => 'custom', 'field' => 'handymanservicesbooking::form.fields.time', 'label' => 'handymanservicesbooking::admin.time_end'],
                ['name' => 'note', 'type' => 'textarea', 'label' => 'handymanservicesbooking::admin.note'],
                ['name' => 'status', 'type' => 'custom', 'field' => 'handymanservicesbooking::form.fields.select_status', 'label' => 'handymanservicesbooking::admin.status',
                    'options' => [
                        'Pending' => 'handymanservicesbooking::admin.pending',
                        'Received' => 'handymanservicesbooking::admin.received',
                        'Complete' => 'handymanservicesbooking::admin.complete',
                        'No Complete' => 'handymanservicesbooking::admin.no_complete',
                        'Cancel' => 'handymanservicesbooking::admin.cancel',
                    ],
                ],
//                ['name' => 'price', 'type' => 'price', 'label' => 'Giá'],
            ],

//            'info_tab' => [
//                ['name' => 'user_name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên Khách hàng'],
//                ['name' => 'user_tel', 'type' => 'text', 'class' => '', 'label' => 'Số điện thoại'],
//                ['name' => 'user_email', 'type' => 'text', 'class' => '', 'label' => 'Email'],
//                ['name' => 'user_gender', 'type' => 'text', 'class' => '', 'label' => 'Giới tính'],
//                ['name' => 'user_address', 'type' => 'text', 'class' => '', 'label' => 'Địa chỉ nhận hàng'],
//                ['name' => 'note', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Chú ý'],
//            ],
        ],
    ];

    protected $filter = [
        'service_id' => [
            'label' => 'handymanservicesbooking::admin.request',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'date_start' => [
            'label' => 'handymanservicesbooking::admin.workday',
            'type' => 'text',
            'query_type' => 'custom'
        ],
        'price' => [
            'label' => 'handymanservicesbooking::admin.price',
            'type' => 'number',
            'query_type' => 'like'
        ],
        'payed' => [
            'label' => 'handymanservicesbooking::admin.pay',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'handymanservicesbooking::admin.status',
                0 => 'handymanservicesbooking::admin.unpaid',
                1 => 'handymanservicesbooking::admin.paid',
            ],
        ],
        'status' => [
            'label' => 'handymanservicesbooking::admin.status',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'handymanservicesbooking::admin.status',
                0 => 'handymanservicesbooking::admin.pending',
                1 => 'handymanservicesbooking::admin.received',
                2 => 'handymanservicesbooking::admin.complete',
            ],
        ],

    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('handymanservicesbooking::list')->with($data);
    }

    public function changeStatus(Request $request)
    {
//     dd($request);
        $booking = Booking::find($request->booking_id);
        $booking->status = $request->status;
        $booking->save();

        CommonHelper::one_time_message('success', trans("handymanservicesbooking::admin.update_success"));
        return response()->json([
            'status' => true,
            'msg' => '{{trans("handymanservicesbooking::admin.add_booking"))}}'
        ]);
    }

    public function addAdminInBooking(Request $request)
    {
        $booking = Booking::find($request->booking_id);
        if ($booking->admin_id == null) {
            $booking->admin_id = $request->admin_id;
            $booking->status = 'Received';
            $booking->save();
            CommonHelper::one_time_message('success', trans('handymanservicesbooking::admin.add_booking'));
            return response()->json([
                'status' => true,
                'msg' => 'handymanservicesbooking::admin.add_booking'
            ]);
        }
        CommonHelper::one_time_message('error', trans('handymanservicesbooking::admin.out_booking'));
        return response()->json([
            'status' => false,
            'msg' => 'handymanservicesbooking::admin.out_booking'
        ]);
    }

    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của  mình
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('admin_id', \Auth::guard('admin')->user()->id)->orWhereNull('admin_id')
                ->orderByRaw("CASE WHEN status = 'Pending' THEN 1
              WHEN status = 'Received' THEN 2
              WHEN status = 'Processing' THEN 3
              WHEN status = 'Complete' THEN 4
              WHEN status = 'No Complete' THEN 5
              ELSE status END ASC")->orderBy('id','ASC');
//
//            //        Chỉ hiển thị đơn hàng phù hợp với khung giờ của  admin
////            $query = $query->where(function ($query){
////                $query->where('time_start', '>=', \Auth::guard('admin')->user()->area1_start)->whereTime('time_start', '<=', \Auth::guard('admin')->user()->area1_end)->whereNotNull('time_start');
////                $query->orWhere('time_start', '>=', \Auth::guard('admin')->user()->area2_start)->whereTime('time_start', '<=', \Auth::guard('admin')->user()->area2_end)->whereNotNull('time_start');
////                $query->orWhere('time_start', '>=', \Auth::guard('admin')->user()->area3_start)->whereTime('time_start', '<=', \Auth::guard('admin')->user()->area3_end)->whereNotNull('time_start');
////            });
        }
        $query = $query->orderByRaw("CASE WHEN status = 'Pending' THEN 1
              WHEN status = 'Received' THEN 2
              WHEN status = 'Processing' THEN 3
              WHEN status = 'Complete' THEN 4
              WHEN status = 'No Complete' THEN 5
              ELSE status END ASC")->orderBy('id','ASC');
//        $query = $query->orderByRaw("CASE WHEN status = 'Pendding' THEN 1
//              WHEN status = 'Received' THEN 2
//              WHEN status = 'Processing' THEN 3
//              WHEN status = 'Complete' THEN 4
//              WHEN status = 'No Complete' THEN 5
//              ELSE status END ASC");


//dd($query->get()->toArray());

//        dd($query->get()->toArray());
        return $query;
    }

//    public function add(Request $request)
//    {
//        try {
//
//
//            if (!$_POST) {
//                $data = $this->getDataAdd($request);
//                return view('handymanservicesbooking::add')->with($data);
//            } else if ($_POST) {
//                $validator = Validator::make($request->all(), [
//                    'user_name' => 'required'
//                ], [
//                    'user_name.required' => 'Bắt buộc phải nhập tên',
//                ]);
//                if ($validator->fails()) {
//                    return back()->withErrors($validator)->withInput();
//                } else {
//                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    //  Tùy chỉnh dữ liệu insert
////                    $data['status'] = $request->has('status') ? 1 : 0;
//                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
//                    #
//                    foreach ($data as $k => $v) {
//                        $this->model->$k = $v;
//                    }
//
//                    if ($this->model->save()) {
//                        $this->afterAddLog($request, $this->model);
//
//                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
//                    } else {
//                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
//                    }
//
//                    if ($request->ajax()) {
//                        return response()->json([
//                            'status' => true,
//                            'msg' => '',
//                            'data' => $this->model
//                        ]);
//                    }
//
//                    if ($request->return_direct == 'save_continue') {
//                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
//                    } elseif ($request->return_direct == 'save_create') {
//                        return redirect('admin/' . $this->module['code'] . '/add');
//                    }
//
//                    return redirect('admin/' . $this->module['code']);
//                }
//            }
//        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', $ex->getMessage());
//            return redirect()->back()->withInput();
//        }
//    }
//
    public function update(Request $request)
    {

        try {
            $item = $this->model->find($request->id);

//            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)){
                abort(404);
            }
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('handymanservicesbooking::edit')->with($data);
            } else if ($_POST) {
//                dd(1);

                    if ($item->id == \Auth::guard('admin')->user()->id) {
                        $validator = Validator::make($request->all(), [
                            //                        'name' => 'required'
                        ], [
                            //                        'name.required' => 'Bắt buộc phải nhập tên',
                        ]);

                        if ($validator->fails()) {
                            return back()->withErrors($validator)->withInput();
                        }
                    }
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //                    dd($data);
                    //  Tùy chỉnh dữ liệu edit
                    //                $data['status'] = $request->has('status') ? 1 : 0;

                if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
                    unset($data['service_id']);
                    unset($data['date_start']);
                    unset($data['time_start']);
                    unset($data['time_end']);
                    unset($data['note']);
                }
//                dd($data);
                    #
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }

                    if ($item->save()) {
                        CommonHelper::one_time_message('success', trans("handymanservicesbooking::admin.update_success"));
                    } else {
                        CommonHelper::one_time_message('error', trans("handymanservicesbooking::admin.update_error"));
                    }


                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }
                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
            }
        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }








//
//
//        try {
//            $item = $this->model->find($request->id);
//
//            if (!is_object($item)) {
//                abort(404);
//            }
//            if (!$_POST) {
//                dd(1);
//                $data = $this->getDataUpdate($request, $item);
//                return view('handymanservicesbooking::edit')->with($data);
//            } else {
//                if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
//                    dd($request);
//                    $item->service_id = $request->service_id;
//                    $item->date_start = $request->date_start;
//                    $item->time_start = $request->time_start;
//                    $item->time_end = $request->time_end;
//                    $item->note = $request->note;
//                    $item->status = $request->status;
//                } else {
//                    $item->status = $request->status;
//                }
//
//                $item->save();
//                CommonHelper::one_time_message('success', 'Sửa thành công.');
//                return redirect()->back();
//            }
//        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            return redirect()->back()->withInput();
//        }
    }


    public function getPublish(Request $request)
    {
        try {

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'handymanservicesbooking::admin.no_record'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => trans("handymanservicesbooking::admin.update_error")
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

//            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', trans("handymanservicesbooking::admin.delete_success"));
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', trans("handymanservicesbooking::admin.update_error"));
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', trans("handymanservicesbooking::admin.update_error"));
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'handymanservicesbooking::admin.update_error'
            ]);
        }
    }

}
