<?php

namespace Modules\HandymanServicesBooking\Providers;

use App\Models\RoleAdmin;
use App\Models\Roles;
use App\Models\Setting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class HandymanServicesBookingServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('HandymanServicesBooking', 'Database/Migrations'));

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Custom setting
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();

            //  Cấu hình quyền mặc định
            $this->addSettingRole();
        }

        $this->registerDefaultRole();
    }

    public function registerDefaultRole() {
        \Eventy::addAction('admin.register', function ($data) {
            $default_role_id = @Setting::where('name', 'register_default_role_id')->first()->value;

            $role_admin = new RoleAdmin();
            $role_admin->admin_id = $data['id'];
            $role_admin->role_id = $default_role_id;
            $role_admin->save();
            return true;
        }, 1, 1);
    }

    public function addSettingRole()
    {
        \Eventy::addFilter('setting.custom_module', function ($module) {
            $module['tabs']['role_default'] = [
                'label' => 'handymanservicesbooking::admin.configuration_role',
                'icon' => '<i class="flaticon2-user-1"></i>',
//                'intro' => '',
                'td' => [
                    ['name' => 'register_default_role_id', 'type' => 'select2_model', 'label' => trans('handymanservicesbooking::admin.role_default'),
                        'model' => Roles::class, 'display_field' => 'display_name',],
                ]
            ];

            $module['tabs']['price_per_hours'] = [
                'label' => 'handymanservicesbooking::admin.setting_1hour',
                'icon' => '<i class="flaticon2-user-1"></i>',
                'td' => [
                    ['name' =>'price_per_hours','type' =>'price','label' =>'admin.setting_1hour','class' =>''],
                ]
            ];
            return $module;
        }, 1, 1);
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['booking_view', 'booking_add', 'booking_edit', 'booking_delete', 'booking_publish',]);
            return $per_check;
        }, 1, 1);
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['service_view', 'service_add', 'service_edit', 'service_delete', 'service_publish',]);
            return $per_check;
        }, 1, 1);
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['service_field_view', 'service_field_add', 'service_field_edit', 'service_field_delete', 'service_field_publish',]);
            return $per_check;
        }, 1, 1);
    }


    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('handymanservicesbooking::partials.aside_menu.dashboard_after_booking');
        }, 1, 1);

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('HandymanServicesBooking', 'Config/config.php') => config_path('handymanservicesbooking.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('HandymanServicesBooking', 'Config/config.php'), 'handymanservicesbooking'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/handymanservicesbooking');

        $sourcePath = module_path('HandymanServicesBooking', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/handymanservicesbooking';
        }, \Config::get('view.paths')), [$sourcePath]), 'handymanservicesbooking');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/handymanservicesbooking');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'handymanservicesbooking');
        } else {
            $this->loadTranslationsFrom(module_path('HandymanServicesBooking', 'Resources/lang'), 'handymanservicesbooking');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('HandymanServicesBooking', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
