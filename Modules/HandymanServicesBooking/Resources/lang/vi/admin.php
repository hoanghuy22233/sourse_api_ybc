<?php
return [
    'booking' => 'Đơn đặt',
    'service' => 'Dịch vụ',
    'input_service' => 'Trường dịch vụ',
    'choose' => 'Chọn',
    'menu' => 'Menu',
    'add' => 'Thêm',
    'edit' => 'Chỉnh sửa',
    'back' => 'Quay lại',
    'save' => 'Lưu',
    'save_and_continue' => 'Lưu và tiếp tục',
    'save_and_quit' => 'Lưu và thoát',
    'save_and_create' => 'Lưu và tạo mới',
    'basic_information' => 'Thông tin cơ bản',
    'search' => 'Tìm kiếm',
    'action' => 'Hành động',
    'choose_action' => 'Chọn hành động',
    'export_excel' => 'Xuất Excel',
    'multi_delete' => 'Xóa nhiều',
    'create_new' => 'Tạo mới',
    'filter' => 'Lọc',
    'reset' => 'Reset',
    'display' => 'Hiển thị',
    'id' => 'ID',
    'image' => 'Ảnh',
    'content' => 'Nội dung',
    'see' => 'Xem',
    'take_bill' => 'Nhận đơn',
    'history_service' => 'Lịch sử đăng ký dịch vụ',
    'company' => 'Công ty',
    'of_the' => 'của',
    'the_service_pack' => 'Các gói dịch vụ',
    'account' => 'Tài khoản',
    'register' => 'Đăng ký',
    'info_order' => 'Thông tin đơn hàng',
    'info_customer' => 'Thông tin khách hàng',
    'request' => 'Yêu cầu',
    'info' => 'Thông tin',
    'job' => 'Công việc',
    'workday' => 'Ngày làm',
    'employees' => 'Người làm',
    'work_time' => 'Giờ làm',
    'parameter' => 'Các thông số công việc',
    'note' => 'Ghi chú',
    'total_price' => 'Thành tiền',
    'customer_name' => 'Tên khách hàng',
    'phone' => 'Số điện thoại',
    'email' => 'Email',
    'address' => 'Địa chỉ',
    'gender' => 'Giới tính',
    'edit_bill' => 'Sửa đơn',
    'status_order' => 'Trạng thái nhận đơn.',
    'status' => 'Trạng thái ',
    'đ' => 'EUR',
    'customer' => 'Khách hàng ',
    'price' => 'Giá ',
    'pay' => 'Thanh toán ',
    'unpaid' => 'Chưa thanh toán ',
    'paid' => 'Đã thanh toán ',
    'pending' => 'Chờ xử lý',
    'received' => 'Đã nhận',
    'complete' => 'Hoàn thành',
    'no_complete' => 'Không hoàn thành',
    'cancel' => 'Hủy',
    'name' => 'Tên',
    'describe' => 'Mô tả',
    'time_start' => 'Giờ bắt đầu',
    'time_end' => 'Giờ kết thúc',
    'content_vi' => 'Nội dung(vi)',
    'content_en' => 'Nội dung(en)',
    'content_hu' => 'Nội dung(hu)',
    'intro_vi' => 'Mô tả ngắn(vi)',
    'intro_en' => 'Mô tả ngắn(en)',
    'intro_hu' => 'Mô tả ngắn(hu)',
    'name_vi' => 'Tên(vi)',
    'name_en' => 'Tên(en)',
    'name_hu' => 'Tên(hu)',
    'url' => 'Đường dẫn',
    'active' => 'Kích hoạt',
    'priority' => 'Thứ tự ưu tiên',
    'hours' => 'Giờ làm',
    'configuration_role' => 'Cấu hình quyền',
    'role_default' => 'Quyền mặc định khi đăng ký tài khoản',
    'add_booking' => 'Nhận đơn thành công!',
    'out_booking' => 'Đơn này đã có người nhận!',
    'update_success' => 'Cập nhật thành công!',
    'update_error' => 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!',
    'delete_success' => 'Xóa thành công!',
    'name_required_form' => 'Không được để trống!',
    'insert_sucess' => 'Tạo mới thành công!',
    'insert_error' => 'Lỗi tạo mới. Vui lòng tải lại trang và thử lại!',
    'system_error' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên!',
    'no_record' => 'Không tìm thấy bản ghi!',
    'setting_1hour' => 'Cấu hình đơn giá cho 1 giờ làm',
];