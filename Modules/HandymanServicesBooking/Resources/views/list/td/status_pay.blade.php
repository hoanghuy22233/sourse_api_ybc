
    @if($item->{$field['name']}==1)
        @if(in_array('view_all_data', $permissions))
            <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill publish" data-url="{{@$field['url']}}" data-id="{{ $item->id }}"
                  style="cursor:pointer;" data-column="{{ $field['name'] }}">{{trans('handymanservicesbooking::admin.paid')}}</span>
        @else
            {{trans('handymanservicesbooking::admin.paid')}}
        @endif

    @else
        @if(in_array('view_all_data', $permissions))
            <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill publish" data-url="{{@$field['url']}}" data-id="{{ $item->id }}"
                  style="cursor:pointer;" data-column="{{ $field['name'] }}">{{trans('handymanservicesbooking::admin.unpaid')}}</span>
        @else
            {{trans('handymanservicesbooking::admin.unpaid')}}
        @endif

    @endif
