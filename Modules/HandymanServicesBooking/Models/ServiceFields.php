<?php
namespace Modules\HandymanServicesBooking\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceFields extends Model
{

    protected $table = 'service_fields';
    public $timestamps = false;



    protected $fillable = [
        'name', 'hours', 'intro','service_id','status',
    ];


    public function service() {
        return $this->belongsTo(Service::class, 'service_id');
    }
}
