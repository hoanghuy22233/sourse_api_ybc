<?php
namespace Modules\A4iAdmin\Models;

use App\Http\Helpers\CommonHelper;
use App\Models\Roles;
use DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Modules\A4iDashboard\Models\Bill;
use Modules\A4iLocation\Models\District;
use Modules\A4iLocation\Models\Province;
use Modules\A4iLocation\Models\Ward;
use Modules\A4iOrganization\Models\Organization;

class Admin extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword;

    protected $table = 'admin';

    protected $fillable = [
        'name', 'email', 'password', 'address', 'tel', 'image', 'gender', 'birthday', 'role_id', 'province_id','district_id', 'ward_id', 'supply_chains'
    ];

    protected $hidden = ['password', 'remember_token'];

    public function roles()
    {
        return $this->belongsToMany(Roles::class, 'role_admin', 'admin_id', 'role_id');
    }

    public function getProfileSrcAttribute()
    {
        if (@$this->attributes['image'] == '')
            $src = url('public/images_core/avatar_user_default.png');
        else
            $src = CommonHelper::getUrlImageThumb(@$this->attributes['image'], 25, 25);
        return $src;
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }

    public function admin_log()
    {
        return $this->hasMany(Website::class, 'admin_id', 'id');
    }

    public function season()
    {
        return $this->hasOne(Season::class, 'season_id');
    }
}
