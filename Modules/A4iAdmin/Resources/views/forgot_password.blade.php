<html lang="en">
<head>
    <link rel="stylesheet" href="{{asset('public/backend/bootstrap/css/bootstrap.min.css')}}">

</head>
<body>
<!--Preloader-->
<style>
    .sp-header {
        display: none !important;

    }
</style>
<!--/Preloader-->
<html lang="en">
<head>
    <link rel="stylesheet" href="{{asset('public/backend/bootstrap/css/bootstrap.min.css')}}">

</head>
<body>
<!--Preloader-->
<style>
    .sp-header {
        display: none !important;

    }
</style>
<!--/Preloader-->
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4" style="padding-top: 70px">
            <div class="wrapper pa-0" style="border: 1px solid #ddd; line-height: 25px; padding: 20px">
                <div class="notification hidden">
                </div>
                <header class="sp-header">
                    <div class="sp-logo-wrap pull-left">
                        <a href="/">
                            <img class="brand-img mr-10" style="width: 50px"
                                 src="" alt="brand">
                            <span class="brand-text"></span>
                        </a>
                    </div>
                </header>

                <!-- Main Content -->
                <div class="page-wrapper pa-0 ma-0 auth-page">
                    <div class="container-fluid">
                        <!-- Row -->
                        <div class="table-struct full-width full-height">
                            <div class="table-cell vertical-align-middle auth-form-wrap">
                                <div class="auth-form  ml-auto mr-auto no-float">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <div class="sp-logo-wrap text-center pa-0 mb-30">
                                                <a href="/">
                                                    <img class="brand-img mr-10" style="width: 50px"
                                                         src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                                                         alt="brand">
                                                    <span class="brand-text">{{ @$settings['name'] }}</span>
                                                </a>
                                            </div>
                                            <div class="mb-30">

                                                @if ($errors->any())
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                                <li class="text-danger">{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-wrap">
                                                <form method="post" action="?{{$change_password}}">
                                                    {{csrf_field()}}
                                                    <input name="change_password" value="{{$change_password}}"
                                                           style="display: none;">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="email">Mật khẩu</label>
                                                        <input type="password" class="form-control" name="password"
                                                               id="password" placeholder="Nhập mật khẩu mới">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="email">Nhập lại mật
                                                            khẩu</label>
                                                        <input type="password" class="form-control" name="re_password"

                                                               id="re_password" placeholder="Nhập lại mật khẩu mới">
                                                    </div>
                                                    <div class="form-group text-center" style="float: right">
                                                        <a href="/dashboard" style="text-decoration: underline; padding: 10px; font-size: 13px">
                                                            Trở về trang chủ
                                                        </a>

                                                        <button type="submit" class="btn btn-primary btn-rounded">Lấy lại
                                                            mật
                                                            khẩu
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Row -->
                    </div>

                </div>
                <!-- /Main Content -->
            </div>
        </div>
    </div>
</div>
<!-- JavaScript -->
</body>
</html>

