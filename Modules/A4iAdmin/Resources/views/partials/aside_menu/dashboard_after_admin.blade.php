@if(in_array('admin_view', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                            <i
                                    class="kt-menu__link-icon flaticon-avatar"></i>
                        </span><span class="kt-menu__link-text">Thành viên</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                @if(in_array('admin_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/admin" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Thành viên</span></a></li>
                @endif

                @if(in_array('role_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/role" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Phân quyền</span></a></li>
                @endif
            </ul>
        </div>
    </li>
@endif