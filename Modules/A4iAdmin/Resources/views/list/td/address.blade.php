@if(isset($item->ward->name))<a
        href="/admin/admin?search=true&ward_id={{ @$item->ward->id }}">{{ @$item->ward->name }}</a>, @endif
@if(isset($item->district->name))<a
        href="/admin/admin?search=true&district_id={{ @$item->district->id }}">{{ @$item->district->name }}</a>, @endif
@if(isset($item->province->name))<a
        href="/admin/admin?search=true&province_id={{ @$item->province->id }}">{{ @$item->province->name }}</a>@endif