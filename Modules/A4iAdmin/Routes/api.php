<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'admins'], function () {
        Route::get('get-type', 'Admin\AdminController@getType');
        Route::post('restore-password-by-code', 'Admin\AdminController@restorePasswordByCode');
        Route::get('', 'Admin\AdminController@index')->middleware('api_permission:admin_view');
    });
});