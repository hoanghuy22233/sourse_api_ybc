<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::match(array('GET', 'POST'), 'profile', 'Admin\AdminController@profile');

    Route::group(['prefix' => 'admin'], function () {
        Route::get('', 'Admin\AdminController@getIndex')->name('admin')->middleware('permission:admin_view');
        /*Route::get('publish', 'Admin\AdminController@publish')->name('admin.publish')->middleware('permission:admin_edit');*/
        Route::match(array('GET', 'POST'), 'add', 'Admin\AdminController@add')->middleware('permission:admin_add');
        Route::get('delete/{id}', 'Admin\AdminController@delete')->middleware('permission:admin_delete');
        Route::post('multi-delete', 'Admin\AdminController@multiDelete')->middleware('permission:admin_delete');
        Route::get('search-for-select2', 'Admin\AdminController@searchForSelect2')->name('admin.search_for_select2')->middleware('permission:admin_view');
        Route::get('{id}', 'Admin\AdminController@update')->middleware('permission:admin_view');
        Route::post('{id}', 'Admin\AdminController@update')->middleware('permission:admin_add');
    });

    Route::match(array('GET', 'POST'), 'profile/{id}', 'Admin\AdminController@profileAdmin')->name('admin.profile');

});
