<?php

namespace Modules\A4iAdmin\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\A4iAdmin\Models\Admin;
use App\Models\RoleAdmin;
use Modules\EworkingRole\Http\Controllers\Admin\RoleController;
use App\Models\Roles;
use Eventy;
use Auth;
use Validator;

class AdminController extends CURDBaseController
{

    protected $orderByRaw = 'status desc, id desc';

    protected $module = [
        'code' => 'admin',
        'table_name' => 'admin',
        'label' => 'Thành viên',
        'modal' => '\Modules\A4iAdmin\Models\Admin',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'sort' => true],
            ['name' => 'name', 'type' => 'custom', 'td' => 'a4iadmin::list.td.name', 'label' => 'Tên', 'sort' => true],
            ['name' => 'role_id', 'type' => 'role_name', 'label' => 'Quyền'],
            ['name' => 'tel', 'type' => 'text', 'label' => 'SĐT', 'sort' => true],
            ['name' => 'email', 'type' => 'text', 'label' => 'Email', 'sort' => true],
            ['name' => 'address', 'type' => 'custom', 'td' => 'a4iadmin::list.td.address', 'label' => 'Địa chỉ',],
            ['name' => 'count_land', 'type' => 'custom', 'td' => 'a4iadmin::list.td.count_land', 'label' => 'Mảnh đất',],
            ['name' => 'count_season', 'type' => 'custom', 'td' => 'a4iadmin::list.td.count_season', 'label' => 'Muà vụ',],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái', 'sort' => true],
        ],
        'list_all' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh'],
            ['name' => 'name', 'type' => 'custom', 'td' => 'a4iadmin::list.td.name', 'label' => 'Tên'],
            ['name' => 'tel', 'type' => 'number', 'label' => 'SĐT'],
            ['name' => 'email', 'type' => 'text', 'label' => 'Email'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'image', 'type' => 'file_image', 'label' => 'Ảnh đại diện'],
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Họ & tên'],
                ['name' => 'tel', 'type' => 'text', 'label' => 'SĐT'],
                ['name' => 'email', 'type' => 'text', 'label' => 'email'],
            ],
            'add_columns_to_excel' => [
                ['name' => 'province_id', 'type' => 'relation', 'label' => 'Tỉnh/Thành', 'object' => 'province', 'display_field' => 'name'],
                ['name' => 'district_id', 'type' => 'relation', 'label' => 'Quận/Huyện', 'object' => 'district', 'display_field' => 'name'],
                ['name' => 'ward_id', 'type' => 'relation', 'label' => 'Phường/Xã', 'object' => 'ward', 'display_field' => 'name'],
                ['name' => 'organization_id', 'type' => 'relation', 'label' => 'Tổ chức', 'object' => 'organization', 'display_field' => 'name'],
                ['name' => 'gender', 'type' => 'text', 'label' => 'Giới tính'],
                ['name' => 'birthday', 'type' => 'text', 'label' => 'Ngày sinh'],
            ]
        ]
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'tel' => [
            'label' => 'SĐT',
            'type' => 'number',
            'query_type' => '='
        ],
//        'role_id' => [
//            'label' => 'Quyền',
//            'type' => 'select2_ajax_model',
//            'query_type' => '=',
//            'object' => 'roles',
//            'display_field' => 'display_name',
//            'model' => Roles::class,
//        ],
        'email' => [
            'label' => 'Email',
            'type' => 'number',
            'query_type' => '='
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                1 => 'Kich hoạt',
                0 => 'Khóa',
            ]
        ],
        'province_id' => [
            'label' => 'Tỉnh Thành',
            'type' => 'select2_ajax_model',
            'object' => 'province',
            'display_field' => 'name',
            'model' => \Modules\A4iLocation\Models\Province::class,
            'query_type' => '='
        ],
        'district_id' => [
            'label' => 'Quận Huyện',
            'type' => 'select2_ajax_model',
            'object' => 'district',
            'display_field' => 'name',
            'model' => \Modules\A4iLocation\Models\District::class,
            'query_type' => '='
        ],
        'ward_id' => [
            'label' => 'Phường Xã',
            'type' => 'select2_ajax_model',
            'object' => 'ward',
            'display_field' => 'name',
            'model' => \Modules\A4iLocation\Models\Ward::class,
            'query_type' => '='
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('a4iadmin::list')->with($data);
    }

    public function profile(Request $request)
    {
        if (!$_POST) {

            $data['result'] = \Auth::guard('admin')->user();

            $data['module'] = $this->module;
            $data['page_title'] = 'Chỉnh sửa ' . $this->module['label'];
            $data['page_type'] = 'update';
            return view('a4iadmin::profile', $data);
        } else if ($_POST) {
            try {
                $item = \Auth::guard('admin')->user();

                if ($item->id == \Auth::guard('admin')->user()->id) {
                    $validator = Validator::make($request->all(), [
                        'name' => 'required',
                        'email' => 'required|email',
                    ], [
                        'name.required' => 'Bắt buộc phải nhập tên',
                        'email.required' => 'Bắt buộc phải nhập email',
                        'email.email' => 'Vui lòng nhập email',
                    ]);

                    if ($validator->fails()) {
                        return back()->withErrors($validator)->withInput();
                    }
                }

                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert
                $data['address']=$request->address;
                $data['birthday']=$request->birthday;
                $data['province_id']=$request->province;
                $data['district_id']=$request->district_id;
                $data['ward_id']=$request->ward;
                $data['organization_id']=$request->organization;

                if (Admin::where('id', '!=', $item->id)->where('email', $data['email'])->count() > 0) {
                    CommonHelper::one_time_message('error', 'Email này đã được sử dụng!');
                    return back();
                }

                #
                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }

                return back();
            } catch (\Exception $ex) {
                CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//                CommonHelper::one_time_message('error', $ex->getMessage());
                return redirect()->back()->withInput();
            }
        }
    }

    public function profileAdmin(Request $request, $id)
    {

        $admin = Admin::findOrFail($id);
        if (!$_POST) {
            $data['result'] = $admin;
            $data['module'] = $this->module;
            $data['page_title'] = 'Chỉnh sửa ' . $this->module['label'];
            $data['page_type'] = 'update';
            return view('a4iadmin::profile', $data);
        } else {
            $msg = 'Cập nhật thành công!';
            if (CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'admin_edit')) {
                $roleAdmin = RoleAdmin::where('admin_id', $id)->first();
                RoleAdmin::where('admin_id', $id)
                    ->update([
                        'role_id' => $request->role_id
                    ]);
            }
            CommonHelper::one_time_message('success', $msg);
            return back();
        }
    }

}



