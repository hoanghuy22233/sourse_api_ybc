<?php

namespace Modules\A4iAdmin\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Http\Helpers\SmsController;
use Illuminate\Http\Request;
use Modules\A4iAdmin\Models\Admin;
use Validator;

class AdminController extends Controller
{

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'email' => [
            'query_type' => 'like'
        ],
        'tel' => [
            'query_type' => 'like'
        ],
        'status' => [
            'query_type' => '='
        ],
        'province_id' => [
            'query_type' => '='
        ],
        'district_id' => [
            'query_type' => '='
        ],
        'ward_id' => [
            'query_type' => '='
        ],
    ];
    protected $status = [
        1 => 'Kich hoạt',
        0 => 'Khóa',
    ];

    public function getType(Request $request)
    {
        return response()->json([
            'status' => true,
            'msg' => '',
            'errors' => (object)[],
            'data' => [
                1 => 'Nông dân/Xã viên',
                2 => 'HTX Nông nghiệp',
                3 => 'Quỹ tín dụng',
                4 => 'Ngân hàng',
                5 => 'Bảo hiểm',
                6 => 'Thương lái/Thu mua',
                7 => 'Cty sản xuất',
                8 => 'Cty xuất khẩu',
                9 => 'Tổ chức,viện NC, Trường học',
                10 => 'UBND xã, phòng nống nghiệp, Sở',
                11 => 'Khác',
            ],
            'code' => 201
        ]);
    }

    public function index(Request $request)
    {
        try {
            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Admin::select(['id', 'email', 'name', 'tel', 'address', 'image', 'gender', 'birthday', 'province_id', 'district_id', 'ward_id', 'status'])
                ->whereRaw($where);

            //  Sort
            $listItem = $this->sort($request, $listItem);
            $limit = $request->get('limit', 20);
            $listItem = $listItem->paginate($limit)->appends($request->all());

            foreach ($listItem as $k => $item) {
                $item->image = asset('public/filemanager/userfiles/' . $item->image);
            }
            foreach ($listItem as $k => $v) {
                $v->province = @$v->province->name;
                $v->district = @$v->district->name;
                $v->ward = @$v->ward->name;
                $v->status = @$this->status[$v->status];
                unset($v->province_id);
                unset($v->district_id);
                unset($v->ward_id);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }

    public function restorePasswordByCode(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'code_change_password' => 'required',
            ], [
                'code_change_password.required' => 'Bắt buộc phải nhập mã',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'msg' => 'lỗi',
                    'errors' => $validator->errors(),
                    'data' => null,
                    'code' => 401
                ]);
            }


            $admin = Admin::where(function ($query) use ($request) {
                $query->orWhere('tel', @$request->get('tel'));
                $query->orWhereNull('email', @$request->get('email'));
            })->first();
            if (!is_object($admin)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn nhập sai email',
                    'errors' => [
                    ],
                    'data' => null,
                    'code' => 401
                ]);
            }
            if ($admin->status == 0) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Tài khoản của bạn chưa được kich hoạt',
                    'errors' => [
                    ],
                    'data' => null,
                    'code' => 401
                ]);
            } elseif ($admin->status == -1) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Tài khoản của bạn đã bị khóa',
                    'errors' => [
                    ],
                    'data' => null,
                    'code' => 401
                ]);
            }

            $smsController = new SmsController();
            $respon = $smsController->verifyUserPhoneFirebase($admin->tel, $request->get('tel_token', ''));
            if ($respon['status'] !== true) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Số điện thoại chưa được xác nhận!',
                    'errors' => [
                    ],
                    'data' => null,
                    'code' => 401
                ]);
            }

            /*$code_confirm = ((int) date('d') + (int) date('m') + (int) date('Y') + (int)substr(@$request->get('tel'), 6, 9)) % 10000;
            if ($code_confirm < 10) {
                $code_confirm = '000' . $code_confirm;
            } elseif ($code_confirm < 100) {
                $code_confirm = '00' . $code_confirm;
            } elseif ($code_confirm < 1000) {
                $code_confirm = '0' . $code_confirm;
            }

            if ($request->code_change_password != $code_confirm) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Sai mã kich hoạt',
                    'errors' => [
                    ],
                    'data' => null,
                    'code' => 401
                ]);
            }*/

            $admin->code_change_password = null;
            $admin->save();

            return response()->json([
                'status' => true,
                'msg' => 'Thành công',
                'errors' => (object)[],
                'data' => [
                    'id' => $admin->id,
                    'email' => $admin->email,
                    'name' => $admin->name,
                    'tel' => $admin->tel,
                    'address' => $admin->address,
                    'image' => asset('public/filemanager/userfiles/' . $admin->image),
                    'gender' => $admin->gender,
                    'birthday' => $admin->birthday,
                    'api_token' => $admin->api_token,
                    'role_display_name' => CommonHelper::getRoleName($admin->id),
                    'role_name' => CommonHelper::getRoleName($admin->id, 'name')
                ],
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lấy dữ liệu không thành công',
                'errors' => [
                    'exception' => $ex->getMessage()
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }
}
