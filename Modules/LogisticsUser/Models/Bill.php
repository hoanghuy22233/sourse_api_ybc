<?php
namespace Modules\LogisticsUser\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{

    protected $table = 'bills';

    protected $fillable = [
        'receipt_method' , 'user_gender', 'date' , 'coupon_code' , 'note' , 'status' , 'total_price' , 'user_id', 'user_tel', 'user_name', 'user_email', 'user_address', 'user_wards', 'user_city_id'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orders() {
        return $this->hasMany(Order::class, 'bill_id', 'id');
    }

    public function company() {
        return $this->belongsTo(\Modules\EworkingCompany\Models\Company::class, 'company_id');
    }
}
