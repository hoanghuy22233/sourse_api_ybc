<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace Modules\ThemeNVD\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model {

    protected $table = 'courses';
    protected $guarded = ['name','intro','content','image','slug','multi_cat','base_price',
        'final_price','lecturer_id','status','created_at','time','category_id','order_no',
        'type','level','utilities','date_of_possession','certificate','tags','document'];
    protected $appends = ['category'];

//    Khóa học liên quan
    public function getCategoryAttribute()
    {
        $cat_ids = explode('|', @$this->attributes['multi_cat']);
        return Category::whereIn('id', $cat_ids)->first();
    }
    public function lesson()
    {
        return $this->hasMany(Lesson::class,'course_id','id')->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }
    //Giảng viên
    public function lecturer()
    {
        return $this->belongsTo(Admin::class,'lecturer_id','id');
    }
    //Học viên
    public function order()
    {
        return $this->hasMany(\Modules\EduBill\Models\Order::class,'course_id','id');
    }
    public function students()
    {
        return $this->hasMany(Student::class,'course_id','id');
    }
    public function admin(){
        return $this->belongsTo(Admin::class, 'lecturer_id','id');
    }

}