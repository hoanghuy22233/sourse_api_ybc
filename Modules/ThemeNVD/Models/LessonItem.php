<?php

namespace Modules\ThemeNVD\Models;

use Illuminate\Database\Eloquent\Model;

class LessonItem extends Model
{

    protected $table = 'lesson_items';

    protected $guarded = [];
    public $timestamps = false;
//    protected $fillable = [
//        'name', 'lesson_id','content'
//    ];

    public function lesson()
    {
        return $this->belongsTo(Lesson::class, 'lesson_id');
    }
    public function topic()
    {
        return $this->hasMany(Topic::class, 'lesson_item_id', 'id')->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }

}
