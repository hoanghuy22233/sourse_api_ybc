<?php

namespace Modules\ThemeNVD\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = 'categories';

    protected $guarded = [];

    public function parent()
    {
        return $this->hasOne($this, 'id', 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany($this, 'parent_id', 'id')->orderBy('order_no', 'asc');
    }

    public function childsMenu()
    {
        return $this->hasMany($this, 'parent_id', 'id')->select(['id', 'name', 'slug'])->orderBy('order_no', 'asc');
    }

    public function posts() {
        return $this->hasMany(Post::class, 'category_id', 'id');
    }
}
