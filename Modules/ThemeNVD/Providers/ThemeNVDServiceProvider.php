<?php

namespace Modules\ThemeNVD\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\EduSettings\Providers\RepositoryServiceProvider;
use App\Models\Setting;
use App\Http\Helpers\CommonHelper;
use View;

class ThemeNVDServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //  Cấu hình Core
//            $this->registerTranslations();
//            $this->registerConfig();
        $this->registerViews();
//            $this->registerFactories();
//            $this->loadMigrationsFrom(module_path('ThemeNVD', 'Database/Migrations'));
//            $this->app->register(RepositoryServiceProvider::class);

        //  Cầu hình frontend
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') === false) {
            $this->frontendSettings();
        }

        //  Cấu hình admin/setting
        /*if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/setting') !== false) {
            $this->customSetting();
        }*/

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            // Cấu hình Core
//            $this->registerTranslations();
//            $this->registerConfig();
//            $this->registerFactories();
//            $this->loadMigrationsFrom(module_path('ThemeEduAdmin', 'Database/Migrations'));


            //  Custom setting

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }
    }

    public function frontendSettings()
    {
        $settings = CommonHelper::getFromCache('frontend_settings', ['settings']);
        if (!$settings) {
            $settings = Setting::whereIn('type', ['general_tab', 'seo_tab', 'common_tab', 'homepage_tab', 'course_page_tab'])->pluck('value', 'name')->toArray();
            CommonHelper::putToCache('frontend_settings', $settings, ['settings']);
        }

        View::share('settings', $settings);
        return $settings;
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('themenvd::partials.aside_menu.aside_menu');
        }, 0, 1);
    }

    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('ThemeNVD', 'Config/config.php') => config_path('themenvd.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('ThemeNVD', 'Config/config.php'), 'themenvd'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/themenvd');

        $sourcePath = module_path('ThemeNVD', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/themenvd';
        }, \Config::get('view.paths')), [$sourcePath]), 'themenvd');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/themenvd');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'themenvd');
        } else {
            $this->loadTranslationsFrom(module_path('ThemeNVD', 'Resources/lang'), 'themenvd');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('ThemeNVD', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
