<?php
//Route::get('test', function () {
//    dd(json_encode([
//        1 => 'Abc bạn nhé',
//        84 => '<p>- T&ecirc;n Cửa H&agrave;ng: &ldquo;Hoa&rsquo;s Beauty Shop&rdquo;<br />
//- Thời gian hoạt động dự kiến: Ng&agrave;y 01/09/2014<br />
//- Ng&agrave;nh nghề hoạt động:<br />
//- Chuy&ecirc;n cung cấp sĩ v&agrave; lẻ tất cả c&aacute;c loại hoa tươi v&agrave; hoa kh&ocirc; trong nước<br />
//- Cung cấp c&aacute;c phụ kiện trang tr&iacute; li&ecirc;n quan<br />
//- Dịch vụ tư vấn, huớng dẫn cắm hoa từ đơn giản đến n&acirc;ng cao<br />
//- Quy m&ocirc; c&ocirc;ng ty : dưới 10 người<br />
//- Vốn chủ sở hữu: VNĐ 300,000,000<br />
//- Địa chỉ cửa h&agrave;ng: 25 Nguyễn Thị Minh Khai, Phường Đakao, Quận 1, Tp.HCM</p>
//',
//        2 => '- Diện tích cửa hàng là 40m2, với không gian thông thoáng, có thể bày trí bắt mắt các kệ
//trưng bày hoa và hàng dự trữ, khách hàng có thể thoải mái di chuyển để lựa chọn Hoa
//mình thích. Diện tích rộng nên ngoài không gian trưng bày, shop sẽ bố trí thêm không
//gian để tư vấn và hướng dẫn khách cắm hoa.
//- Hình thức trang trí sang trọng, lãng mạn với gam màu tươi sáng làm màu sắc chủ đạo.
//- Sỡ dĩ nhóm chọn địa điểm tại Nguyễn Thị Minh khai vì khu vực này có khá nhiều
//Công ty,Văn phòng, trình độ dân trí và thu nhập cao, giao thông thuận lợi...',
//        3 => '- Liên kết các shop hoa tươi trên cả nước trong việc cung cấp các dịch vụ hoa tươi.
//- Xây dựng mạng lưới điện hoa chuyên nghiệp trải rộng khắp các tỉnh, thành phố trên
//toàn quốc, ngoài nước
//- Hoàn thiện các quy trình kiểm tra chất lượng và giao nhận.
//- Tạo độ tin cậy và chuyên nghiệp để giử khách hàng trung thành',
//        13 => '<table border="1" cellpadding="1" cellspacing="1" style="width:500px">
//	<tbody>
//		<tr>
//			<td>Danh mục</td>
//			<td>Gi&aacute; b&aacute;n trung b&igrave;nh</td>
//			<td>SL</td>
//			<td>Doanh thu h&agrave;ng ng&agrave;y</td>
//			<td style="text-align:right">Doanh thu h&agrave;ng th&aacute;ng</td>
//		</tr>
//		<tr>
//			<td>B&aacute;n lẻ / ng&agrave;y</td>
//			<td>300.000</td>
//			<td>10</td>
//			<td>3.000.000</td>
//			<td style="text-align:right">90.000.000</td>
//		</tr>
//		<tr>
//			<td>Sự kiện / th&aacute;ng</td>
//			<td>10.000.000/lần</td>
//			<td>02</td>
//			<td>&nbsp;</td>
//			<td style="text-align:right">20.000.000</td>
//		</tr>
//		<tr>
//			<td>Doanh thu kh&aacute;c</td>
//			<td>&nbsp;</td>
//			<td>&nbsp;</td>
//			<td>&nbsp;</td>
//			<td style="text-align:right">15.000.000</td>
//		</tr>
//		<tr>
//			<td>Tổng doanh thu</td>
//			<td>&nbsp;</td>
//			<td>&nbsp;</td>
//			<td>&nbsp;</td>
//			<td style="text-align:right">125.000.000</td>
//		</tr>
//	</tbody>
//</table>
//
//<p>&nbsp;</p>
//'
//    ]));
//});

Route::get('dang-ky-thanh-vien', 'Frontend\HomeController@getSigUpMember');
Route::post('dang-ky-thanh-vien', 'Frontend\HomeController@postSigUpMember')->name('register-member');
Route::get('lien-he', 'Frontend\ContactController@getContact')->name('get-contact');
Route::post('lien-he', 'Frontend\ContactController@postContact')->name('post-contact');

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions', 'locale']], function () {
    Route::group(['prefix' => 'theme'], function () {
        Route::match(['get', 'post'], 'setting', 'Admin\EduThemeController@setting')->name('themenvd')->middleware('permission:theme');
    });

    Route::group(['prefix' => 'khkd_form_fields'], function () {
        Route::get('', 'Admin\KhkdFormFieldsController@getIndex')->name('khkd_form_fields')->middleware('permission:theme');
        Route::get('publish', 'Admin\KhkdFormFieldsController@getPublish')->name('khkd_form_fields.publish')->middleware('permission:theme');
        Route::match(array('GET', 'POST'), 'add', 'Admin\KhkdFormFieldsController@add')->middleware('permission:theme');
        Route::get('{id}/duplicate', 'Admin\KhkdFormFieldsController@duplicate')->middleware('permission:theme');
        Route::get('delete/{id}', 'Admin\KhkdFormFieldsController@delete')->middleware('permission:theme');
        Route::post('multi-delete', 'Admin\KhkdFormFieldsController@multiDelete')->middleware('permission:theme');
        Route::get('search-for-select2', 'Admin\KhkdFormFieldsController@searchForSelect2')->name('khkd_form_fields.search_for_select2')->middleware('permission:theme');
        Route::get('{id}', 'Admin\KhkdFormFieldsController@update')->middleware('permission:theme');
        Route::post('{id}', 'Admin\KhkdFormFieldsController@update')->middleware('permission:theme');
    });

    //  Admin
    Route::group(['prefix' => 'admin'], function () {
        Route::match(array('GET', 'POST'), 'add', 'Admin\AdminController@add')->middleware('permission:admin_add');
        Route::get( '{id}', 'Admin\AdminController@update')->middleware('permission:admin_view');
    });
});
//Route::get('register-course-when-login', 'Frontend\ContactController@registerCourseWhenLogin')->name('register-course-when-login');
Route::get('route', 'Frontend\ContactController@route')->name('route');
Route::get('topic', 'Frontend\ContactController@topic')->name('topic');
Route::get('misson', 'Frontend\ContactController@misson')->name('misson');
Route::get('no-misson', 'Frontend\ContactController@noMisson')->name('no-misson');
Route::group(['prefix' => '', 'middleware' => 'no_auth:student'], function () {
    Route::get('dang-nhap', 'Frontend\AuthController@getLogin');
    Route::post('dang-nhap', 'Frontend\AuthController@authenticate');
    Route::get('dang-ky', 'Frontend\AuthController@getRegister');
    Route::post('dang-ky', 'Frontend\AuthController@postRegister');
});

Route::match(array('GET', 'POST'), 'quen-mat-khau', 'Frontend\AuthController@getEmailForgotPassword');
Route::match(array('GET', 'POST'), 'forgot-password/{change_password}', 'Frontend\AuthController@getForgotPassword');
Route::get('dang-xuat', function () {
    \Auth::guard('student')->logout();
    return redirect('/');
});

//Route::match(array('GET', 'POST'), 'profile/{id}', 'Frontend\AuthController@profileAdmin')->name('profile');

Route::group(['prefix' => '', 'middleware' => ['guest:student']], function () {
    Route::get('profile', 'Frontend\StudentController@myProfile');
    Route::get('profile/edit', 'Frontend\StudentController@getEditProfile');
    Route::post('profile/edit', 'Frontend\StudentController@postEditProfile');

    Route::post('tu-tao-mau-kinh-doanh', 'Frontend\CourseController@tuTaoMauKinhDoanh');

    Route::group(['prefix' => 'student'], function () {
        Route::get('doi-mat-khau', 'Frontend\StudentController@getChangePass');
        Route::post('doi-mat-khau', 'Frontend\StudentController@postChangePass');

        Route::get('{id}/khoa-hoc', 'Frontend\StudentController@course');
        Route::get('{id}/bai-kiem-tra', 'Frontend\StudentController@quiz');
        Route::get('{id}/diem-thi', 'Frontend\StudentController@getPoint');
        Route::get('{id}/{slug}', 'Frontend\StudentController@misson');
    });
});

Route::get('detail', 'Frontend\CourseController@getDetail');

Route::get('mau-kinh-doanh-tu-tao/{id}', 'Frontend\CourseController@mauKinhDoanh');
Route::get('mau-tra-phi/{id}', 'Frontend\CourseController@mauTraPhi');

Route::get('profile/{id}', 'Frontend\StudentController@getProfile');
//Route::get('error', 'Frontend\CourseController@Error')->name('error');
Route::get('note', 'Frontend\CourseController@Note')->name('note');
//Route::post('dang-ky-khoa-hoc', 'Frontend\ContactController@postContact')->name('contact.post');
Route::get('tim-kiem', 'Frontend\CourseController@getSearch');
//Route::get('tim-kiem-post', 'Frontend\CourseController@getSearchPost');
//Route::get('tim-kiem-quizz', 'Frontend\CourseController@getSearchQuizz');

Route::get('lich-hoc', 'Frontend\CourseController@lichHoc');

Route::group(['prefix' => 'gio-hang'],function () {
    Route::get('', 'Frontend\CartController@getCart');
    Route::post('', 'Frontend\CartController@postCart');
    Route::get('add/{id}', 'Frontend\CartController@addCart');
    Route::get('del_item/{rowId}','Frontend\CartController@delItemCart');
    Route::get('del_all_item','Frontend\CartController@delAllItemCart');
    Route::get('update_item/{rowId}/{qty}','Frontend\CartController@updateItemCart');
    Route::get('thank-you', 'Frontend\CartController@getComplete')->name('cart.complete');
    Route::post('ajax-add-cart', 'Frontend\CartController@ajaxAddCart')->name('cart.ajaxAddCart');
});

Route::group(['prefix' => 'thanh-vien'], function () {
    Route::get('', 'Frontend\HomeController@listallMember');
    Route::get('{role_name}', 'Frontend\HomeController@listMember');
});

Route::get('{slug1}', 'Frontend\CourseController@oneParam');
Route::get('{slug1}/{slug2}', 'Frontend\CourseController@twoParam');
Route::get('{slug1}/{slug2}/{slug3}', 'Frontend\CourseController@threeParam');
Route::get('{slug1}/{slug2}/{slug3}/{slug4}', 'Frontend\CourseController@fourParam');

//Route::get('', 'Frontend\HomeController@getHome');

//  Dang nhap fb - gg
Route::get('/login/{param}/redirect/', 'Frontend\StudentController@redirect')->name('auth_redirect');//dang nhap
Route::get('/login/{param}/callback/', 'Frontend\StudentController@callback');

Route::get('/', 'Frontend\HomeController@getIndex');