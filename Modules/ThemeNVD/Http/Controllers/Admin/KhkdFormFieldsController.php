<?php

namespace Modules\ThemeNVD\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;

class KhkdFormFieldsController extends CURDBaseController
{
    protected $whereRaw = 'parent_id is null';
    protected $orderByRaw = 'order_no desc, id asc';

    protected $module = [
        'code' => 'khkd_form_fields',
        'table_name' => 'khkd_form_fields',
        'label' => 'Form kế hoạch kinh doanh',
        'modal' => '\Modules\ThemeNVD\Models\KhkdFormFields',
        'list' => [
            ['name' => 'label', 'type' => 'text_edit', 'label' => 'Tên'],
//            ['name' => 'order_no', 'type' => 'text', 'label' => 'Thứ tự'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'course_id', 'type' => 'number', 'label' => 'course_id'],
                ['name' => 'parent_id', 'type' => 'number', 'label' => 'parent_id'],
                ['name' => 'label', 'type' => 'textarea_editor', 'label' => 'Tên mục'],
                ['name' => 'section', 'type' => 'select', 'options' => [
                    '' => 'Không hiển thị',
                    'dau_muc' => 'Đầu mục',
                    'trang_trang' => 'Trang trắng',
                    'gach_dau_dong' => 'Gạch đầu dòng',
                ], 'label' => 'Khối hiển thị', 'group_class' => 'col-md-4'],
                ['name' => 'type', 'type' => 'select', 'options' => [
                    '' => 'Không nhập',
                    'input_text' => 'Input text',
                    'input_number' => 'Input number',
                    'textarea' => 'Textarea',
                    'textarea_editor' => 'Textarea Editor',
                ], 'label' => 'Kiểu nhập', 'group_class' => 'col-md-4'],
                ['name' => 'level', 'type' => 'select', 'options' => [
                    1 => 1,
                    2 => 2,
                    3 => 3,
                    4 => 4,
                    5 => 5,
                ], 'label' => 'Cấp độ', 'group_class' => 'col-md-4'],
                ['name' => 'des', 'type' => 'text', 'label' => 'Mô tả'],
                ['name' => 'tutorial', 'type' => 'textarea_editor', 'label' => 'Hướng dẫn nhập liệu'],
                ['name' => 'default_value', 'type' => 'textarea_editor', 'label' => 'Nội dung nhập sẵn'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'Thứ tự', 'group_class' => 'col-md-4'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Trạng thái', 'value' => 1, 'group_class' => 'col-md-4'],
            ],
            'info_tab' => [

            ],
        ],
    ];

    protected $filter = [
        'order_no' => [
            'label' => 'Thứ tự',
            'type' => 'number',
            'query_type' => '='
        ],
        'type' => [
            'label' => 'Kiểu nhập',
            'type' => 'select',
            'options' => [
                '' => 'Không nhập',
                'input_text' => 'Input text',
                'input_number' => 'Input number',
                'textarea' => 'Textarea',
                'textarea_editor' => 'Textarea Editor',
            ],
            'query_type' => '='
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, tiêu đều, content',
        'fields' => 'id, course_id, label, des, tutorial, default_value'
    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);

        return view('themenvd::admin.khkd_form_fields.list')->with($data);
    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('themenvd::admin.khkd_form_fields.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
//                    'image' => 'required'
                ], [
//                    'image.required' => 'Bắt buộc phải nhập ảnh',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('themenvd::admin.khkd_form_fields.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
//                    'image' => 'required'
                ], [
//                    'image.required' => 'Bắt buộc phải nhập ảnh',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
