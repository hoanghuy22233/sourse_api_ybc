<?php

namespace Modules\ThemeNVD\Http\Controllers\Api\Admin;

use App\Mail\MailServer;
use App\Models\Admin;
use App\Models\AdminLog;
use App\Models\Error;
use App\Models\Setting;
use Illuminate\Support\Facades\Mail;
use \Modules\A4iSeason\Models\Disease;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\EduCourse\Models\MaketingMail;
use Modules\EduMarketing\Models\MarketingMail;
use Modules\KitCareBooking\Helpers\KitCareBookingHelper;
use Modules\KitCareBooking\Models\Booking;
use Modules\KitCareBooking\Models\ErrorCode;
use Modules\KitCareBooking\Models\Job;
use Modules\Theme\Models\Post;
use Modules\ThemeNVD\Models\Category;
use Validator;
use URL;

class HomeController extends Controller
{

    public function index(Request $request)
    {

        try {

            $category_ids = @Setting::where('name', 'homepage_categories')->where('type', 'app')->first()->value;
            $category_ids = explode('|', $category_ids);
            $data['categories'] = Category::select('id', 'name')->whereIn('id', $category_ids)->where('status', 1)->get();
            foreach ($data['categories'] as $cat) {
                $cat->posts = \Modules\ThemeNVD\Models\Post::select('id', 'name', 'image')
                    ->where('status', 1)->where('multi_cat', 'like', '%|' . @$cat->id . '|%')->limit(8)->get();
                foreach ($cat->posts as $post) {
                    $post->image = asset('public/filemanager/userfiles/' . $post->image);
                }
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $data,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function menu(Request $request)
    {

        try {

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => [
                    [
                        'image' => asset('public/frontend/themes/clb/images/ic_news.png'),
                        'name' => 'Tin tức',
                        'category_id' => 221
                    ],
                    [
                        'image' => asset('public/frontend/themes/clb/images/ic_events.png'),
                        'name' => 'Sự kiện',
                        'category_id' => 240
                    ],
                    [
                        'image' => asset('public/frontend/themes/clb/images/ic_media.png'),
                        'name' => 'YBC Media',
                        'category_id' => 239
                    ],
                    [
                        'image' => asset('public/frontend/themes/clb/images/ic_contract.png'),
                        'name' => 'Lập KHKD',
                        'category_id' => null
                    ],
                    [
                        'image' => asset('public/frontend/themes/clb/images/ic_course.png'),
                        'name' => 'Khóa học',
                        'category_id' => null
                    ],

                ],
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
//        $id = 1465;
        try {

            $item = Booking::leftJoin('admin', 'admin.id', '=', 'bookings.admin_id')
                ->selectRaw('bookings.id, bookings.product_id, bookings.producer_id, bookings.product_code, bookings.error_code_id, bookings.tel, bookings.cost
                , bookings.province_id, bookings.district_id, bookings.image, bookings.image_present, bookings.note, bookings.time, bookings.admin_id, bookings.payment_status, bookings.bonus_services, bookings.note_admin, bookings.job_id
                , bookings.note_user, bookings.care, admin.address, bookings.status, bookings.created_at, bookings.ktv_ids, bookings.image_extra, bookings.image_present2,admin.id as admin_id, admin.name as admin_name, admin.image as admin_image')
                ->where('bookings.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            if ($item->image != null) {
                $item->image = asset('public/filemanager/userfiles/' . $item->image);
            }
            foreach (explode('|', $item->image_present) as $img) {
                if ($img != '') {
                    $image_present[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->image_present = @$image_present;

            foreach (explode('|', $item->image_extra) as $img) {
                if ($img != '') {
                    $image_extra[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->image_extra = @$image_extra;

            foreach (explode('|', $item->image_present2) as $img) {
                if ($img != '') {
                    $image_present2[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->image_present2 = @$image_present2;

            if ($item->address == null) {
                $item->address = '';
            }

            $item->user = [
                'id' => $item->admin_id,
                'name' => $item->admin_name,
                'tel' => $item->tel,
                'image' => asset('public/filemanager/userfiles/' . $item->admin_image),
                'role_name' => CommonHelper::getRoleName($item->admin_id, 'name'),
                'role_display_name' => CommonHelper::getRoleName($item->admin_id, 'display_name'),
            ];

            $a = Admin::select('id', 'name', 'image')
                ->whereIn('id', explode('|', $item->ktv_ids))
                ->get()->toArray();
            foreach ($a as $k1 => $v1) {
                $a[$k1]['image'] = $a[$k1]['image'] != null ? asset('public/filemanager/userfiles/' . $a[$k1]['image']) : null;
            }
            $item->ktv = $a;


            $b = Job::select('id', 'name')
                ->whereIn('id', explode('|', $item->job_id))
                ->get()->toArray();
            $item->job = $b;


            $error = ErrorCode::select('id', 'name')
                ->whereIn('id', explode('|', $item->error_code_id))
                ->get()->toArray();
            $item->error_code = $error;


            $item->province_name = @$item->admin->province->name == null ? '' : @$item->province->name;
            $item->district_name = @$item->admin->district->name == null ? '' : @$item->district->name;
            $item->producer_name = @$item->producer->name == null ? '' : @$item->producer->name;
            $product_name = @$item->product->name;
            unset($item->product);
            $item->product = [
                'id' => @$item->product_id,
                'name' => $product_name,
                'code' => $item->product_code
            ];
            $item->status_data = $item->status;

            $item->status = array_key_exists($item->status, $this->type_booking) ? $this->type_booking[$item->status] : '';
            $item->payment_status_key = (int) $item->payment_status;
            $item->payment_status = array_key_exists($item->payment_status, $this->type_payment_status) ? $this->type_payment_status[$item->payment_status] : '';
            $item->bonus_services = array_key_exists($item->bonus_services, $this->type_bonus_services) ? $this->type_bonus_services[$item->bonus_services] : '';

            $item->care_key = (int) $item->care;
            $item->care = array_key_exists($item->care, $this->type_care) ? $this->type_care[$item->care] : '';

            unset($item->province_id);
            unset($item->district_id);
            unset($item->province);
            unset($item->district);
            unset($item->admin_id);
            unset($item->admin_name);
            unset($item->producer);
            unset($item->product_id);
            unset($item->producer_id);
            unset($item->error_code_id);
            unset($item->job_id);
            unset($item->ktv_ids);
            unset($item->product_code);
            unset($item->admin_image);
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {
        try {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('admin_api')->id();
            $data['create_by'] = \Auth::guard('admin_api')->id();
            $data['admin_name'] = str_slug(\Auth::guard('admin_api')->user()->name, '-');


            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = $data['image_present'][] = CommonHelper::saveFile($image, 'booking/' . $data['admin_name'] . '/' . date('d-m-y'));
                    }
                } else {
                    $data['image'] = $data['image_present'][] = CommonHelper::saveFile($request->file('image'), 'booking/' . $data['admin_name'] . '/' . date('d-m-y'));
                }
                $data['image_present'] = implode('|', $data['image_present']);
            }

            $item = Booking::create($data);

            KitCareBookingHelper::sendMailToAdmin($item, 16);

            $mailSetting = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();
            $admin_emails = $mailSetting['admin_emails'];
            $admin_emails = explode(',', $admin_emails);
            $admin_ids = Admin::whereIn('email', $admin_emails)->pluck('id')->toArray();
            KitCareBookingHelper::pushNotiFication($item, 'mới được tạo', [], $admin_ids);

            return $this->show($item->id);
        } catch (\Exception $ex) {
            Error::create([
                'module' => 'kitcareBooking - booking',
                'message' => 'Dòng ' . $ex->getLine() . ': ' . $ex->getMessage(),
                'file' => $ex->getFile(),
                'code' => $ex->getLine(),

            ]);
            return response()->json([
                'status' => false,
                'msg' => $ex->getMessage(),
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }


    public function update(Request $request, $id)
    {

        $item = Booking::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            $image_present = [];
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = $image_present[] = CommonHelper::saveFile($image, 'booking/' . $data['admin_name'] . '/' . date('d-m-y'));
                }
            } else {
                $data['image'] = $image_present[] = CommonHelper::saveFile($request->file('image'), 'booking/' . $data['admin_name'] . '/' . date('d-m-y'));
            }
            $data['image_present'] = implode('|', $image_present);
        }

        if ($request->has('image_extra')) {
            $image_extra = [];
            if (is_array($request->file('image_extra'))) {
                foreach ($request->file('image_extra') as $image) {
                    $image_extra[] = CommonHelper::saveFile($image, 'booking');
                }
            } else {
                $image_extra[] = CommonHelper::saveFile($request->file('image_extra'), 'booking');
            }
            $data['image_extra'] = implode('|', $image_extra);
        }


        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (Booking::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {

                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . ".time >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . ".time <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }

    public function SendMailFullSlot($info)
    {

        $user = (object)[
            'email' => $info['email'],
            'name' => $info['name'],
            'booking_id' => $info['booking_id'],
        ];
        $data = [
            'view' => 'kitcarebooking::emails.booking_admin',
            'user' => $user,
            'name' => $this->_mailSetting['mail_name'],
            'subject' => 'Đơn hàng mới!'
        ];
        Mail::to($user)->send(new MailServer($data));
        return true;
    }


}
