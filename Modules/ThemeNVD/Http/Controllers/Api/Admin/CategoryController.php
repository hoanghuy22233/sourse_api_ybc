<?php

namespace Modules\ThemeNVD\Http\Controllers\Api\Admin;

use \Modules\A4iSeason\Models\Disease;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\ThemeNVD\Models\Category;
use Modules\ThemeNVD\Models\Post;
use Validator;

class CategoryController extends Controller
{

    protected $module = [
        'code' => 'category',
        'table_name' => 'categories',
        'label' => 'Bài viết',
        'modal' => 'Modules\ThemeNVD\Models\Category',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'parent_id' => [
            'query_type' => 'custom'
        ]
    ];

    public function index(Request $request)
    {

        try {
            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Category::selectRaw('categories.id, categories.name, categories.image, categories.created_at, categories.intro')
                ->whereRaw($where)->where($this->module['table_name'] . '.status', 1);

            if ($request->has('category_id')) {
                $listItem = $listItem->where('multi_cat', 'like', '%|'.$request->category_id.'|%');
            }

            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            foreach ($listItem as $k => $v) {
                $v->image = asset('public/filemanager/userfiles/' . $v->image);

            }
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id, Request $r)
    {
        try {

            $item = Category::selectRaw('categories.id, categories.name, categories.image, categories.content, categories.created_at, categories.intro')->where($this->module['table_name'] . '.id', $id)->where($this->module['table_name'] . '.status', 1)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $relate_item = Category::selectRaw('categories.id, categories.name, categories.image, categories.created_at, categories.intro')
                ->where($this->module['table_name'] . '.id', '!=', $id)
                ->where($this->module['table_name'] . '.status', 1);

            $cat_ids = explode('|', $item->multi_cat);
            $relate_item = $relate_item->where(function ($query) use ($cat_ids) {
                foreach ($cat_ids as $cat_id) {
                    if ($cat_id != '') {
                        $query->orwhere('multi_cat', 'like', '%|' . $cat_id . '|%');
                    }
                }
            });

            $limit = $r->has('limit') ? $r->limit : 4;
            $relate_item = $relate_item->paginate($limit);
            foreach ($relate_item as $k => $v) {
                $v->image = asset('public/filemanager/userfiles/' . $v->image);
            };
            $item->image = asset('public/filemanager/userfiles/' . $item->image);


            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => [
                    'item' => $item,
                    'relate' => $relate_item,
                ],
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();
            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = CommonHelper::saveFile($image, 'disease');
                    }
                } else {
                    $data['image'] = CommonHelper::saveFile($request->file('image'), 'disease');
                }
            }

            if ($request->has('post_image')) {
                if (is_array($request->file('post_image'))) {
                    foreach ($request->file('post_image') as $image) {
                        $data['post_image'] = CommonHelper::saveFile($image, 'disease');
                    }
                } else {
                    $data['post_image'] = CommonHelper::saveFile($request->file('post_image'), 'disease');
                }
            }

            $item = Category::create($data);

            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {

        $item = Category::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = CommonHelper::saveFile($image, 'disease');
                }
            } else {
                $data['image'] = CommonHelper::saveFile($request->file('image'), 'disease');
            }
        }
        if ($request->has('post_image')) {
            if (is_array($request->file('post_image'))) {
                foreach ($request->file('post_image') as $image) {
                    $data['post_image'] = CommonHelper::saveFile($image, 'disease');
                }
            } else {
                $data['post_image'] = CommonHelper::saveFile($request->file('post_image'), 'disease');
            }
        }

        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (Category::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $col => $mode) {
                $model = $model->orderBy($this->module['table_name'] .'.'. $col, $mode);
            }
        } else {
            $model = $model->orderBy('order_no', 'desc')->orderBy('id', 'desc');
        }
        return $model;
    }
}
