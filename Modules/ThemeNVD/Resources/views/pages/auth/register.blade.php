@extends('themenvd::layouts.default')
@section('main_content')
    <section>
        <div class="gap no-gap signin whitish medium-opacity">
            <div class="bg-image"
                 style="background-image:url(/public/frontend/themes/edu/images/resources/theme-bg.jpg)"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="big-ad">
                            <figure><a href="/"><img class="lazy"
                                            data-src="{{ URL::asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                                            alt="{{ @$settings['name'] }}" style="max-width: 150px;"></a></figure>
                            <h1>Chào mừng bạn đến với {{ @$settings['name'] }}</h1>
                            <p>
                                {!! @$settings['web_description'] !!}
                            </p>

                            @include('themenvd::pages.auth.count_student')
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="we-login-register" style="background-color: #003300;">
                            <div class="form-title">
                                <i class="fa fa-key"></i>Đăng ký
                                <span>Đăng nhập ngay bây giờ và gặp gỡ những người bạn tuyệt vời trên khắp thế giới.</span>
                            </div>
                            <form class="we-form" method="post" action="/dang-ky">
                                <div class="we-form">
                                    <input type="name" name="name" placeholder="Họ tên" value="{{old('name')}}">
                                    @if($errors->has('name'))
                                        <p style="color: red"><b>{{ $errors->first('name') }}</b></p>
                                    @endif
                                </div>
                                <div class="we-form">
                                    <input type="email" name="email" placeholder="Email" value="{{old('email')}}">
                                    @if($errors->has('email'))
                                        <p style="color: red"><b>{{ $errors->first('email') }}</b></p>
                                    @endif
                                </div>
                                <div class="we-form">
                                    <input type="password" name="password" placeholder="Mật khẩu">
                                    @if($errors->has('password'))
                                        <p style="color: red"><b>{{ $errors->first('password') }}</b></p>
                                    @endif
                                </div>

                                <input type="text" name="phone" placeholder="SĐT">
                                @if($errors->has('phone'))
                                    <p style="color: red"><b>{{ $errors->first('phone') }}</b></p>
                                @endif
                                {!! csrf_field() !!}
                                <button type="submit" data-ripple="">Đăng ký</button>
                            </form>
                            <a class="forgot underline" href="/quen-mat-khau" title="">Quên mật khẩu?</a>
                            <a class="with-smedia facebook" href="/login/facebook/redirect/" title="" data-ripple="">Đăng
                                nhập với facebook</a>
                            <a class="with-smedia google" href="/login/google/redirect/" title="" data-ripple="">Đăng
                                nhập với google</a>
                            <span>Bạn chưa có tài khoản? <a class="we-account underline" href="/dang-nhap" title="">Đăng nhập</a></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection