<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta charset="utf-8"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <link href="https://fonts.googleapis.com/css?family=Pacifico:400%7CRoboto:400%2C700%2C900" rel="stylesheet"
          property="stylesheet" media="all" type="text/css">

    <link rel="stylesheet" href="{{ asset('/public/frontend/themes/clb/interface_repository/temp1/css/base.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/public/frontend/themes/clb/interface_repository/temp1/css/fancy.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/public/frontend/themes/clb/interface_repository/temp1/css/main.css') }}"/>
    <script src="{{ asset('/public/frontend/themes/clb/interface_repository/temp1/js/compatibility.min.js') }}"></script>
    <script src="{{ asset('/public/frontend/themes/clb/interface_repository/temp1/js/theViewer.min.js') }}"></script>
    <script>
        try {
            theViewer.defaultViewer = new theViewer.Viewer({});
        } catch (e) {
        }
    </script>
    <style>
        *{
            font-family: Roboto !important;
        }
    </style>
</head>
<body>
<div id="sidebar">
    <div id="outline">
    </div>
</div>
<div id="page-container">
    @include('themenvd::pages.business_template.interface_repository.temp1.partials.mo_dau',
        ['kh_image' => $khkd_log->logo, 'kh_name' => $khkd_log->name])

    <?php
    $data = (array)json_decode($khkd_log->data);
    $fields = \Modules\ThemeNVD\Models\KhkdFormFields::whereIn('id', array_keys($data))->orderBy('order_no', 'asc')->orderBy('id', 'asc')->get();
    ?>
    @foreach($fields as $field)
        @if(isset($data[$field->id]) && @$data[$field->id] != '' && $field->section != null)
            {{--{!! $field->label !!}<br>
            {!! @$data[$field->id] !!}<br>--}}
            @include('themenvd::pages.business_template.interface_repository.temp1.partials.' . $field->section , [
                    'kh_image' => $khkd_log->logo, 'kh_name' => $khkd_log->name,
                    'name' => $field->label,
                    'content' => @$data[$field->id]
                ])
        @endif
    @endforeach

    @include('themenvd::pages.business_template.interface_repository.temp1.partials.trang_cuoi',
        ['kh_image' => $khkd_log->logo, 'kh_name' => $khkd_log->name])

</div>
<div class="loading-indicator">

</div>
</body>
</html>
