<style>
    .trang_trang table td {
        padding: 28px;
    }
</style>
<div id="pf27" class="pf w0 h0 trang_trang" data-page-no="27">
    <div class="pc pc27 w0 h0"><img class="bi x0 y0 w0 h0" alt=""
                                    src="{{ asset('/public/frontend/themes/clb/interface_repository/temp1/images/bg27-1.png') }}"/>
        <div class="c x0 y1 w0 h23">
            <div class="t m11 x56 h4b y217 ff4 fs38 fc5 sc0 ls0 ws0">{{ strip_tags($name) }}<span class="ff1 fc0">X<span
                            class="fc5"> </span></span></div>
            <div class="t m11 x67 h4c y218 ff4 fs39 fc0 sc0 ls0 ws0" style="width: 2501px;
    white-space: unset;
    height: unset;
    display: inline-block;
    top: 0;
    color: #000;">{!! $content !!}</div>
            <div class="t m11 x51 h4f y22a ff3 fs3c fc3 sc0 ls0 ws0">{{ $kh_name }}</div>
        </div>
    </div>
    <div class="pi" data-data='{"ctm":[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}'></div>
</div>