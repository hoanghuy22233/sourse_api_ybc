@extends('themenvd::layouts.wp')
@section('head_script')
<style>
    #content-bottom-widgets .widget .custom-info i {
        margin-left: 0;
    }
    .vc_parallax-inner.skrollable.skrollable-between {
        top: 0 !important;
    }
    #hoat-dong-chinh {
        width: 100%;
        display: inline-block;
    }
    #hoat-dong-chinh .iconbox h4 {
        font-size: 16px;
        padding: 0 !important;
        text-align: center;
        line-height: 27px;
        margin-top: 23% !important;
        vertical-align: middle;
    }
    #hoat-dong-chinh .iconbox {
        display: inline-block;
        width: calc(20% - 20px);
        float: left;
        margin: 10px;
        background: #ffffff;
        padding: 11px;
        position: relative;
        height: 148px;
        border: 1px solid #106bb4;
    }
    .mc4wp-form-fields input {
        padding: 5px 10px;
        height: 30px;
    }
    form#mc4wp-form-1 p {
        margin-bottom: 7px;
    }
    form#mc4wp-form-1 label {
        display: inline-block;
        width: 100%;
    }

    @media only screen and (max-width: 600px) {
        .video_info iframe{
            width: 350px !important;
        }
    }
</style>
@endsection
@section('main_content')

    <div class="wpb_column vc_column_container vc_col-sm-12">
        <img src="/public/frontend/themes/clb/images/gioi_thieu.png" alt="">
    </div>

    <div data-vc-full-width="true" data-vc-full-width-init="false"
         class="vc_row wpb_row vc_row-fluid vc_custom_1460567613213 vc_row-has-fill">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                            <?php

                            $widgets = \Modules\ThemeEdu\Models\Widget::where('location', 'home_sidebar_left')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
                            $widgets2 = \Modules\ThemeEdu\Models\Widget::where('location', 'home_sidebar_right')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();

                            ?>
                            <div class="vc_column-inner">
                                @foreach($widgets as $widget)

                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <h3 class="style2 bg-dark"><span
                                                            style="color: #ffffff;">{{$widget->name}}</span>
                                                </h3>

                                            </div>
                                        </div>
                                        <div class="vc_empty_space"
                                             style="height: 15px"><span
                                                    class="vc_empty_space_inner"></span>
                                        </div>

                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                @if($widget->type == 'html')
                                                    {!!$widget->content !!}
                                                @endif

                                            </div>
                                        </div>

                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner">
                                @foreach($widgets2 as $wg)
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">

                                            <div class="wpb_wrapper">
                                                <h3 class="style2 bg-dark"><span
                                                            style="color: #ffffff;">{{$wg->name}}</span>
                                                </h3>

                                            </div>
                                        </div>
                                        <div class="vc_empty_space"
                                             style="height: 50px"><span
                                                    class="vc_empty_space_inner"></span>
                                        </div>
                                        <div class="wpb_text_column wpb_content_element ">

                                            <div class="wpb_wrapper video_info">
                                                @if($wg->type == 'html')
                                                    {!!$wg->content !!}
                                                @endif


                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                    <div class="vc_empty_space" style="height: 50px"><span
                                class="vc_empty_space_inner"></span></div>
                    <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text">
                                                    <span class="vc_sep_holder vc_sep_holder_l"><span
                                                                style="border-color:rgb(241,242,248);border-color:rgba(241,242,248,0.1);"
                                                                class="vc_sep_line"></span></span><span
                                class="vc_sep_holder vc_sep_holder_r"><span
                                    style="border-color:rgb(241,242,248);border-color:rgba(241,242,248,0.1);"
                                    class="vc_sep_line"></span></span>
                    </div>
                    <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1460568709204">
                        <?php
                        $banner = \Modules\ThemeNVD\Models\Banner::where('status', 1)->where('location', 'home_top')->orderBy('order_no', 'ASC')->get();

                        ?>
                        @foreach($banner as $b)
                            <div class="wpb_column vc_column_container vc_col-sm-2">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_single_image wpb_content_element vc_align_center">

                                            <figure class="wpb_wrapper vc_figure">
                                                <a href="#" target="_self"
                                                   class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                            width="110" height="100"
                                                            src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb($b->image)}}"
                                                            class="vc_single_image-img attachment-full"
                                                            alt=""/></a>
                                                <figcaption
                                                        class="vc_figure-caption">{{@$b->name}}
                                                </figcaption>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="vc_row-full-width vc_clearfix"></div>

    <div class="vc_row wpb_row vc_row-fluid vc_custom_1460519094442">


        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <h3 class="no-margin-top" style="font-size: 30px;">SỰ KIỆN SẮP
                                TỚI </h3>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="vc_row wpb_row vc_row-fluid vc_custom_1460521049287" id="su-kien">
        <?php

        $post = \Modules\ThemeNVD\Models\Post::select(['name', 'intro', 'address', 'image', 'date_place', 'slug', 'multi_cat'])->where('multi_cat', 'LIKE', '%|' . 240 . '|%')
            ->where('status', 1)->orderBy('date_place', 'desc')->limit(6)->get();

        ?>
        @foreach($post as $item)
            <div class="wpb_column vc_column_container vc_col-sm-4">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <!-- BEGIN .imagebox -->
                        <div class="imagebox  ">
                            <div class="box-wrapper">

                                <div class="box-image">
                                    <img width="700" height="500"
                                         src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$item->image ) }}"
                                         class="attachment-full" alt=""
                                         sizes="(max-width: 700px) 100vw, 700px"/></div>


                                <div class="box-header">
                                    <h3 class="box-title">
                                        <a href="{{ \Modules\ThemeNVD\Helpers\ThemeNVDHelper::getPostSlug($item) }}"
                                           target="_self">
                                            {{$item->name}}</a>
                                    </h3>

                                </div>

                                <div class="box-content">
                                    <div class="box-desc">
                                        {{$item->intro}}
                                    </div>


                                    <div class="box-button">
                                        <a href="{{ \Modules\ThemeNVD\Helpers\ThemeNVDHelper::getPostSlug($item) }}"
                                           target="_self">
                                            Xem thêm </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- End .imagebox -->
                        <div class="vc_empty_space" style="height: 30px"><span
                                    class="vc_empty_space_inner"></span></div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div data-vc-full-width="true" data-vc-full-width-init="false"
         class="vc_row wpb_row vc_row-fluid vc_custom_1460520975088 vc_row-has-fill">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <div class="title-button">
                                <h3 class="style1"><i class="fa fa-2x fa-line-chart"></i>Hoạt
                                    động chính</h3>
                            </div>

                        </div>
                    </div>
                    <div class="vc_empty_space" style="height: 30px"><span
                                class="vc_empty_space_inner"></span></div>
                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-4" id="hoat-dong-chinh">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="iconbox ">
                                        <div class="box-header">
                                            <h4 class="box-title">TÂM SỰ ĐỜI<br>DOANH NHÂN</h4>
                                        </div>
                                    </div>
                                    <div class="iconbox ">
                                        <div class="box-header">
                                            <h4 class="box-title">YBC TALKSHOW</h4>
                                        </div>
                                    </div>
                                    <div class="iconbox ">
                                        <div class="box-header">

                                            <h4 class="box-title">YBC ĐÀO TẠO</h4>
                                        </div>
                                    </div>
                                    <div class="iconbox ">
                                        <div class="box-header">

                                            <h4 class="box-title">YBC - KẾT NỐI<br>GIAO
                                                THƯƠNG </h4>
                                        </div>
                                    </div>
                                    <div class="iconbox ">
                                        <div class="box-header">

                                            <h4 class="box-title">YBC PITCHING<br>GỌI
                                                VỐN</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="vc_row-full-width vc_clearfix"></div>
    <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="1.5"
         data-vc-parallax-o-fade="on"
         data-vc-parallax-image="/public/frontend/themes/clb/images/footer_banner.png"
         class="vc_row wpb_row vc_row-fluid bg-scheme2 vc_custom_1460535090775 vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving-fade js-vc_parallax-o-fade">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            {{--<h3 class="no-margin-top"
                                style="text-align: center; line-height: 1.3;"><span
                                        style="color: #ffffff;">Hãy tham gia ngay YBC START-UP</span>
                            </h3>--}}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="vc_row-full-width vc_clearfix"></div>

    <div class="vc_row wpb_row vc_row-fluid vc_custom_1460572124805">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">

                        <div class="wpb_wrapper">
                            {{--<i class="fa fa-2x fa-user"></i>--}}
                            <h3 class="style1">
                                 <a href="">Thành viên</a>
                                | <a href="">Chuyên gia</a>
                                | <a href="">Thành viên chính thức</a>

                            </h3>

                        </div>

                    </div>
                    <div class="vc_empty_space" style="height: 50px"><span
                                class="vc_empty_space_inner"></span></div>
                    <!-- BEGIN: .elements-carousel -->
                    <div class="elements-carousel  "
                         data-config="{&quot;items&quot;:2,&quot;itemsTablet&quot;:[768,2],&quot;itemsMobile&quot;:[479,1],&quot;autoPlay&quot;:false,&quot;stopOnHover&quot;:true,&quot;mouseDrag&quot;:true,&quot;touchDrag&quot;:true,&quot;responsive&quot;:true,&quot;scrollPerPage&quot;:true,&quot;slideSpeed&quot;:200,&quot;paginationSpeed&quot;:200,&quot;rewindSpeed&quot;:200,&quot;navigation&quot;:true,&quot;rewindNav&quot;:true,&quot;pagination&quot;:false,&quot;paginationNumbers&quot;:false,&quot;dragBeforeAnimFinish&quot;:true,&quot;addClassActive&quot;:true,&quot;autoHeight&quot;:true,&quot;navigationText&quot;:[&quot;Previous&quot;,&quot;Next&quot;],&quot;itemsScaleUp&quot;:true}">
                        <div class="elements-carousel-wrap">
                            <?php
                            $not_user1 = \App\Models\RoleAdmin::where('role_id', '=', 178)->pluck('admin_id')->toArray();
                            $admins = \Modules\ThemeNVD\Models\Admin::select('id', 'name', 'image', 'job')
                                ->whereIn('id', $not_user1)->orderBy('id', 'asc')->limit(10)->get();

                            //        dd($admins)
                            ?>
                            @foreach($admins as $ad1)

                                <div class="testimonial  has-image">

                                    <div class="testimonial-image">
                                        <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$ad1->image , null) }}"
                                             alt="Jeff Kane"/>
                                    </div>

                                    <div class="testimonial-content">
                                        <div class="testimonial-meta">
                                            <div class="testimonial-author">
                                                <strong class="author-name">{{$ad1->name}}</strong>
                                                <br>
                                                <div class="author-info"><span
                                                            class="subtitle">{{$ad1->job}}</span>

                                                </div>
                                            </div>
                                        </div>
                                        <blockquote>
                                            {{$ad1->intro}}
                                        </blockquote>
                                    </div>
                                </div>
                            @endforeach
                            <?php
                            $not_user2 = \App\Models\RoleAdmin::where('role_id', '=', 180)->pluck('admin_id')->toArray();
                            $admins2 = \Modules\ThemeNVD\Models\Admin::select('id', 'name', 'image')
                                ->whereIn('id', $not_user2)->orderBy('id', 'asc')->limit(10)->get();

                            //        dd($admins)
                            ?>
                            @foreach($admins2 as $ad2)

                                <div class="testimonial  has-image">

                                    <div class="testimonial-image">
                                        <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$ad2->image , null) }}"
                                             alt="Jeff Kane"/>
                                    </div>

                                    <div class="testimonial-content">
                                        <div class="testimonial-meta">
                                            <div class="testimonial-author">
                                                <strong class="author-name">{{$ad2->name}}</strong>
                                                <span class="divider">-</span>
                                                <div class="author-info"><span
                                                            class="subtitle">{{$ad2->job}}</span>

                                                </div>
                                            </div>
                                        </div>
                                        <blockquote>
                                            {{$ad2->intro}}
                                        </blockquote>
                                    </div>
                                </div>
                            @endforeach

                            <?php
                            $not_user3 = \App\Models\RoleAdmin::where('role_id', '=', 179)->pluck('admin_id')->toArray();
                            $admins3 = \Modules\ThemeNVD\Models\Admin::select('id', 'name', 'image')
                                ->whereIn('id', $not_user3)->orderBy('id', 'asc')->limit(10)->get();

                            ?>
                            @foreach($admins3 as $ad3)

                                <div class="testimonial  has-image">

                                    <div class="testimonial-image">
                                        <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$ad3->image , null) }}"
                                             alt="Jeff Kane"/>
                                    </div>

                                    <div class="testimonial-content">
                                        <div class="testimonial-meta">
                                            <div class="testimonial-author">
                                                <strong class="author-name">{{$ad3->name}}</strong>
                                                <span class="divider">-</span>
                                                <div class="author-info"><span
                                                            class="subtitle">{{$ad3->job}}</span>

                                                </div>
                                            </div>
                                        </div>
                                        <blockquote>
                                            {{$ad3->intro}}
                                        </blockquote>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- END: .elements-carousel -->
                    <div class="vc_empty_space" style="height: 50px"><span
                                class="vc_empty_space_inner"></span></div>
                    <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text">
                                                    <span class="vc_sep_holder vc_sep_holder_l"><span
                                                                style="border-color:rgb(54,70,115);border-color:rgba(54,70,115,0.08);"
                                                                class="vc_sep_line"></span></span><span
                                class="vc_sep_holder vc_sep_holder_r"><span
                                    style="border-color:rgb(54,70,115);border-color:rgba(54,70,115,0.08);"
                                    class="vc_sep_line"></span></span>
                    </div>
                    <div class="vc_empty_space" style="height: 35px"><span
                                class="vc_empty_space_inner"></span></div>
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <p style="text-align: center;"><a class="link2"
                                                              href="/thanh-vien/thanh-vien-chinh-thuc">Xem
                                    thêm thành viên<i class="fa fa-chevron-right"></i></a>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="vc_row-full-width vc_clearfix"></div>
    <div class="vc_row wpb_row vc_row-fluid vc_custom_1460559155148">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <div class="title-button">
                                <h3 class="style1"><i class="fa fa-2x fa-newspaper-o"></i>TIN
                                    MỚI</h3>
                                <p><a class="button sm border"
                                      href="/tin-tuc-su-kien">Xem thêm</a></p>
                            </div>

                        </div>
                    </div>
                    <div class="vc_empty_space" style="height: 30px"><span
                                class="vc_empty_space_inner"></span></div>
                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-8">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="blog-shortcode  blog-grid blog-two-columns post-thumbnail-cover">

                                        <div class="blog-entries">

                                            <?php

                                            $postNew = \Modules\ThemeNVD\Models\Post::select(['id', 'name', 'image', 'intro', 'date_place', 'admin_id', 'multi_cat', 'slug'])
                                                ->where('status', 1)->where('show_homepage', 1)->orderBy('id', 'desc')->limit(2)->get();

                                            $id_post_new = $postNew->pluck('id');
                                            $postNew2 = \Modules\ThemeNVD\Models\Post::select(['name', 'image', 'intro', 'date_place', 'admin_id', 'multi_cat', 'slug'])
                                                ->where('status', 1)->where('show_homepage', 1)->orderBy('id', 'desc')->whereNotIn('id', $id_post_new)->limit(4)->get();


                                            ?>
                                            <div class="entries-wrapper">
                                                @foreach($postNew as $pN)

                                                    <article
                                                            class="post-102 post type-post status-publish format-standard has-post-thumbnail hentry category-sales-enablement tag-business tag-sales tag-training">
                                                        <div class="entry-wrapper">


                                                            <div class="entry-cover">
                                                                <h4 class="entry-time">
                                                                                                <span class="entry-day">

											{{date('d',strtotime($pN->date_place))}}										</span>
                                                                    <span class="entry-month">
											{{date('m',strtotime($pN->date_place))}}										</span>
                                                                </h4>
                                                                <a href="{{ \Modules\ThemeNVD\Helpers\ThemeNVDHelper::getPostSlug($pN) }}">
                                                                    <img width="600"
                                                                         height="413"
                                                                         src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$pN->image, null, null) }}"
                                                                         class="attachment-portfolio-medium-crop size-portfolio-medium-crop wp-post-image"
                                                                         alt=""/> </a>
                                                            </div>


                                                            <div class="entry-content-wrap">
                                                                <div class="entry-header">
                                                                    <h2 class="entry-title">
                                                                        <a href="{{ \Modules\ThemeNVD\Helpers\ThemeNVDHelper::getPostSlug($pN) }}">
                                                                            {{$pN->name}}</a>
                                                                    </h2>
                                                                </div>


                                                                <div class="entry-footer">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                @endforeach


                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <div class="blog-shortcode  blog-grid blog-one-column post-thumbnail-cover">

                                        <div class="blog-entries">
                                            <div class="entries-wrapper">
                                                @foreach($postNew2 as $pN2)
                                                    <article
                                                            class="post-94 post type-post status-publish format-standard has-post-thumbnail hentry category-sales-coaching tag-coaching tag-sales tag-training">
                                                        <div class="entry-wrapper">


                                                            <div class="entry-cover">
                                                                <h4 class="entry-time">
                                                                                                <span class="entry-day">

											{{date('d',strtotime($pN2->date_place))}}										</span>
                                                                    <span class="entry-month">
											{{date('m',strtotime($pN2->date_place))}}										</span>
                                                                </h4>
                                                                <a href="{{ \Modules\ThemeNVD\Helpers\ThemeNVDHelper::getPostSlug($pN2) }}">
                                                                    <img width="600"
                                                                         height="413"
                                                                         src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$pN2->image, null, null) }}"
                                                                         class="attachment-portfolio-medium-crop size-portfolio-medium-crop wp-post-image"
                                                                         alt=""/> </a>
                                                            </div>


                                                            <div class="entry-content-wrap">
                                                                <div class="entry-header">
                                                                    <h2 class="entry-title">
                                                                        <a href="{{ \Modules\ThemeNVD\Helpers\ThemeNVDHelper::getPostSlug($pN2) }}">{{$pN2->name}}</a>
                                                                    </h2>
                                                                </div>


                                                                <div class="entry-footer">

                                                                    <div class="entry-meta">

                                                                        <i class="fa fa-folder-open"></i>
                                                                        <span class="entry-categories">

                                                                                                        <a href="{{ \Modules\ThemeNVD\Helpers\ThemeNVDHelper::getPostSlug($pN2) }}">{{@$pN2->category->name}}</a>
                                                                                                    </span>


                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
