@extends('themenvd::layouts.default')
@section('main_content')
    @include('themenvd::partials.banner_header')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>

        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">


                                <div class="col-lg-8">
                                    @if(Cart::Count()>0)
                                        <div class="central-meta">
                                            <h4 class="create-post">Đơn hàng của bạn</h4>
                                            <div class="cart-sec">
                                                <table class="table table-responsive">
                                                    <tbody>
                                                    <tr>
                                                        <th>Sản phẩm trong đơn</th>
                                                        <th>Giá</th>
                                                    </tr>
                                                    @foreach(Cart::content() as $k=>$cart_item)
                                                        <tr>
                                                            <td>
                                                                <a href="/gio-hang/del_item/{{$cart_item->rowId}}"
                                                                   title=""
                                                                   class="delete-cart"><i class="ti-close"></i></a>
                                                                <div class="cart-avatar">

                                                                    <img class="lazy" data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($cart_item->options->image, 100, null) }}"
                                                                         alt="{{$cart_item->name}}">
                                                                </div>
                                                                <div class="cart-meta">
                                                                    <a href="/khoa-hoc/{{\Modules\ThemeNVD\Models\Course::find($cart_item->id)->slug}}.html"><span>{{$cart_item->name}}</span></a>
                                                                </div>
                                                            </td>
                                                            <td>
                                                            <span class="cart-prices">
                                                                    <span style="color: red">{{ number_format($cart_item->price,0,'','.') }}<sup>đ</sup></span>
                                                            </span>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="amount-area">

                                                        <div class="total-area">
                                                            <ul>
                                                                <li class="order-total"
                                                                    style="padding: 10px 20px!important">
                                                                    <span><a href="/gio-hang/del_all_item"
                                                                             class="text-primary"
                                                                             title="Xóa tất cả sản phẩm">Xóa toàn bộ giỏ hàng</a></span>

                                                                    <span class="float-none">Tổng tiền:</span>

                                                                    <i style="font-weight: 900">{{ Cart::total(0, '', ',') }}
                                                                        <sup>đ</sup></i>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <h3 class="alert alert-danger">Bạn chưa chọn mua khóa học nào!</h3>
                                    @endif
                                </div>
                                <div class="col-lg-4">
                                    @if(Cart::Count()>0)
                                        <aside class="sidebar static right">
                                            <div class="widget">
                                                <h4 class="widget-title">Thông tin khách hàng</h4>
                                                @if(!\Auth::guard('student')->check())
                                                    <div class="pl-3 pr-3 text-center">
                                                        <a class="with-smedia facebook text-center mb-2"
                                                           href="/dang-nhap" title="" data-ripple="">Đăng
                                                            nhập</a><br><a href="/dang-ky"
                                                                           class="text-primary text-center">Hoặc tạo tài
                                                            khoản mới</a>
                                                    </div>
                                                @endif
                                                <div>
                                                    <form action="" method="post" enctype="multipart/form-data">
                                                        <div>
                                                            @if(\Auth::guard('student')->check())
                                                                <div class="pl-3 pr-3 pt-2 pb-2">
                                                                    <strong>Tên:</strong> {{\Auth::guard('student')->user()->name}}
                                                                </div>
                                                                <div class="pl-3 pr-3 pt-2 pb-2">
                                                                    <strong>Email:</strong> {{\Auth::guard('student')->user()->email}}
                                                                </div>
                                                                <div class="pl-3 pr-3 pt-2 pb-2"><strong>Số điện
                                                                        thoại:</strong> {{\Auth::guard('student')->user()->phone}}
                                                                </div>
                                                                @if(Cart::total()>0)
                                                                    <div class="pl-3 pr-3 pt-2 pb-2">

                                                                    <span><input type="file" name="invoice_image"
                                                                                 value="" size="40"></span><br>
                                                                        <i class="color-red">Ảnh chụp màn hình thanh
                                                                            toán
                                                                            chuyển khoản!<br>Nếu bạn thanh toán tiền
                                                                            trước
                                                                            cho đơn hàng này, chúng tôi sẽ duyệt và kick
                                                                            hoạt khoá học cho bạn nhanh hơn!</i>
                                                                    </div>
                                                                @endif
                                                            @else
                                                                <div class="pl-3 pr-3 pt-2 pb-2">
                                                            <span>
                                                                <input type="text" name="name" value="{{old('name')}}"
                                                                       class="form-control" placeholder="Tên">
                                                                @if ($errors->has('name'))
                                                                    <i class="color-red">{{$errors->first('name')}}</i>
                                                                @endif
                                                            </span>
                                                                </div>
                                                                <div class="pl-3 pr-3 pt-2 pb-2">
                                                            <span>
                                                                <input type="email" name="email"
                                                                       value="{{old('email')}}" size="40"
                                                                       class=" form-control "
                                                                       placeholder="Email">
                                                                @if ($errors->has('email'))
                                                                    <i class="color-red">{{$errors->first('email')}}</i>
                                                                @endif
                                                            </span>
                                                                </div>
                                                                <div class="pl-3 pr-3 pt-2 pb-2">
                                                            <span>
                                                                <input type="tel" name="phone" value="{{old('phone')}}"
                                                                       size="40"
                                                                       class=" form-control "
                                                                       placeholder="Số điện thoại">
                                                                @if ($errors->has('phone'))
                                                                    <i class="color-red">{{$errors->first('phone')}}</i>
                                                                @endif
                                                            </span>
                                                                </div>

                                                                <div class="pl-3 pr-3 pt-2 pb-2">
                                                                <span>
                                                                    <input type="password" name="password"
                                                                           value="{{old('password')}}"
                                                                           class=" form-control "
                                                                           placeholder="Nhập mật khẩu">
                                                                    @if ($errors->has('password'))
                                                                        <i class="color-red">{{$errors->first('password')}}</i>
                                                                    @endif
                                                                </span>
                                                                </div>

                                                                <div class="pl-3 pr-3 pt-2 pb-2">
                                                                <span>
                                                                    <input type="password" name="re_password" value=""
                                                                           class=" form-control "
                                                                           placeholder="Nhập lại mật khẩu">
                                                                    @if ($errors->has('re_password'))
                                                                        <i class="color-red">{{$errors->first('re_password')}}</i>
                                                                    @endif
                                                                </span>
                                                                </div>
                                                                @if(Cart::total()>0)
                                                                    <div class="pl-3 pr-3 pt-2 pb-2">
                                                                    <span>
                                                                        <input type="file" name="invoice_image" value=""
                                                                               size="40">
                                                                    </span><br>
                                                                        <i class="color-red">Ảnh chụp màn hình thanh
                                                                            toán
                                                                            chuyển khoản!<br>Nếu bạn thanh toán tiền
                                                                            trước
                                                                            cho đơn hàng này, chúng tôi sẽ duyệt và kick
                                                                            hoạt khoá học cho bạn nhanh hơn!</i>
                                                                    </div>
                                                                @endif
                                                            @endif
                                                            <div class="modal-footer">
                                                                <button type="submit">Hoàn tất đơn hàng</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>


                                            </div><!-- recent post-->
                                        </aside>
                                    @endif
                                </div><!-- sidebar -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->

    </div>

@endsection