<div class="col-lg-3">
    <aside class="sidebar static left">
        <div class="widget stick-widget">
            <?php
            $data = CommonHelper::getFromCache('categories_type_5');
            if (!$data) {
                $data = \Modules\ThemeNVD\Models\Category::where('type', 5)->orderBy('order_no', 'desc')->orderBy('name', 'asc')->where('status', 1)->get();
                CommonHelper::putToCache('categories_type_5', $data);
            }
            ?>
            <h4 class="widget-title">Chuyên mục</h4>
            <ul class="naves">
                @foreach($data as $v)
                    @if($v->parent_id == 0)
                        <li class="nav-item dropdown">
                            <i class="ti-clipboard"></i>
                            <a id="navbarDropdown" class="nav-link"
                               href="/{{ @$v->slug }}"
                               title="{{ @$v->name }}">{{ @$v->name }}</a>
                            <ul>
                                @foreach($data as $sub_cat)
                                    @if($sub_cat->parent_id == $v->id)
                                        <li class="dropdown-content">
                                            <a class="dropdown-item"
                                               href="/{{ @$sub_cat->slug }}"
                                               title="{{ @$sub_cat->name }}">{{ @$sub_cat->name }}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div><!-- Shortcuts -->
    </aside>
</div><!-- sidebar -->