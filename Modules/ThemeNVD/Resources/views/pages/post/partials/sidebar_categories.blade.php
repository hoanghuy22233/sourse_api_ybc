<div class="widget">
    <?php
    $categories = CommonHelper::getFromCache('categorys_type_1_location_menu_cate', ['categories']);
    if (!$categories) {
        $categories = \Modules\ThemeNVD\Models\Category::where('type', 1)->orderBy('order_no', 'desc')->orderBy('name', 'asc')->where('status', 1)->get();
        CommonHelper::putToCache('categorys_type_1_location_menu_cate', $categories, ['categories']);
    }
    ?>
    <h4 class="widget-title">Chuyên mục</h4>
    <ul class="naves">
        @foreach($categories as $cat)
            @if($cat->parent_id==0)
                <li class="nav-item dropdown">
                    <i class="ti-clipboard"></i>
                    <a id="navbarDropdown" class="nav-link" href="/{{ @$cat->slug }}"
                       title="{{ @$cat->name }}">{{ @$cat->name }}</a>

                    @foreach($categories as $sub_menu)
                        @if($sub_menu->parent_id==$cat->id)
                            <div class="dropdown-content">
                                <a class="dropdown-item" href="/{{ @$sub_menu->slug }}"
                                   title="{{ @$sub_menu->name }}">{{ @$sub_menu->name }}</a>
                                <div class="dropdown-divider"></div>
                            </div>
                        @endif
                    @endforeach

                </li>
            @endif
        @endforeach
    </ul>
</div><!-- Shortcuts -->

