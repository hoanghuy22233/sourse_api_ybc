@extends('themenvd::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">


    <!-- topbar -->
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themenvd::partials.student_menu')
                                </div><!-- user profile banner  -->

                                <div class="col-lg-8 col-md-8">
                                    <div class="forum-form">
                                        @if(Session::has('success'))
                                            <p style="top: -15px;"
                                               class="alert alert-success">{!! Session::get('success') !!}</p>
                                        @endif
                                        @if(Session::has('password-error'))
                                            <p style="top: -15px;"
                                               class="alert alert-danger">{!! Session::get('password-error') !!}</p>
                                        @endif
                                        <div class="central-meta">
                                            <span class="create-post">Đổi mật khẩu</span>
                                            <form method="post" class="c-form" action="/student/doi-mat-khau"
                                                  enctype="multipart/form-data">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="student_id"
                                                       value="{{ Auth::guard('student')->user()->id }}">
                                                @if(\Auth::guard('student')->user()->password != '')
                                                    <div>
                                                        <label>Mật khẩu hiện tại</label>
                                                        <input type="password" name="password"
                                                               value="" required>
                                                    </div>
                                                @endif
                                                <div>
                                                    <label>Mật khẩu mới</label>
                                                    <input type="password" name="new_password"
                                                           value="" required>
                                                </div>
                                                <div>
                                                    <label>Nhập lại mật khẩu mới</label>
                                                    <input type="password" name="re_new_password"
                                                           value="" required>
                                                    @if ($errors->has('re_new_password'))
                                                        <p class="text-danger"
                                                           style=" padding-left: 10px;">{{$errors->first('re_new_password')}}</p>
                                                    @endif
                                                </div>
                                                <div class="">
                                                    <button class="main-btn" type="submit" data-ripple="">Cập nhật
                                                    </button>
                                                    <a class="main-btn3" href="/profile/edit">Quay lại
                                                    </a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="central-meta stick-widget">
                                        <span class="create-post">Cấu hình khác</span>
                                        <div class="personal-head">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection
@section('head_script')
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/profile.css') }}?v={{ time() }}">
@endsection