<div class="show_dangky modal fade" id="exampleModalLong" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Đăng ký khóa học</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="cmsmasters_column_inner">
                <div class="featured_block_inner">
                    <div class="central-meta">
                        <div role="form" class="page-createbox">
                            <form method="post" class="c-form dang-ky-course">
                                <input type="hidden" name="course_id" value="{{ @$course->id }}">
                                <div class="row">

                                    <div class="col-lg-12">
                                        <input type="text" name="name" placeholder="Họ & Tên" required>
                                    </div>
                                    <div class="col-lg-12">
                                        <input type="email" name="email" placeholder="Email">
                                    </div>
                                    <div class="col-lg-12">
                                        <input type="text" name="phone" placeholder="Số điện thoại" required>
                                    </div>
                                    <div class="col-lg-12">
                                        <textarea name="message" cols="40" rows="5" placeholder="Lời nhắn"></textarea>
                                    </div>

                                    <div class="col-lg-12">
                                        <button class="main-btn dky_course cmsmasters_button" type="button"
                                                data-ripple="">Gửi đăng ký
                                        </button>
                                        <button class="main-btn3" type="button" data-ripple="" onclick="$(this).parents('.modal').modal('hide');">Quay lại</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.dky_course').click(function () {
        // e.preventDefault();
        $.ajax({
            url: '/dang-ky-khoa-hoc',
            data: $('form.dang-ky-course').serialize(),
            type: 'POST',
            success: function (result) {
                $('form.dang-ky-course input, form.dang-ky-course textarea').val('');
                $('.show_dangky').modal('hide');
                alert(result.msg);
            },
            error: function () {
                alert('Có lỗi xảy ra. Vui lòng load lại website và thử lại!');
            }
        });
    });

    $(document).ready(function () {
        $(".dang_ky").click(function () {
            $('.show_dangky').modal('show');
            var course_id = $(this).data('course_id');
            console.log(course_id);
            $('form.dang-ky-course input[name=course_id]').val(course_id);
        });
    });
</script>