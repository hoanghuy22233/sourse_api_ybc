
<script id="shopping-cart-items-updater" type="text/javascript"></script>
<script>
    (function () {
        function addEventListener(element, event, handler) {
            if (element.addEventListener) {
                element.addEventListener(event, handler, false);
            } else if (element.attachEvent) {
                element.attachEvent('on' + event, handler);
            }
        }

        function maybePrefixUrlField() {
            if (this.value.trim() !== '' && this.value.indexOf('http') !== 0) {
                this.value = "http://" + this.value;
            }
        }

        var urlFields = document.querySelectorAll('.mc4wp-form input[type="url"]');
        if (urlFields && urlFields.length > 0) {
            for (var j = 0; j < urlFields.length; j++) {
                addEventListener(urlFields[j], 'blur', maybePrefixUrlField);
            }
        } /* test if browser supports date fields */
        var testInput = document.createElement('input');
        testInput.setAttribute('type', 'date');
        if (testInput.type !== 'date') {

            /* add placeholder & pattern to all date fields */
            var dateFields = document.querySelectorAll('.mc4wp-form input[type="date"]');
            for (var i = 0; i < dateFields.length; i++) {
                if (!dateFields[i].placeholder) {
                    dateFields[i].placeholder = 'YYYY-MM-DD';
                }
                if (!dateFields[i].pattern) {
                    dateFields[i].pattern = '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])';
                }
            }
        }

    })();
</script>
<link rel="stylesheet" property="stylesheet" id="rs-icon-set-fa-icon-css"
      href="{{ URL::asset('/public/frontend/themes/YBC/wp-content/plugins/revslider/public/assets/fonts/font-awesome/css/font-awesome.css')}}"
      type="text/css" media="all"/>
<link href="https://fonts.googleapis.com/css?family=Hind+Vadodara:700%7CRoboto:400" rel="stylesheet"
      property="stylesheet" media="all" type="text/css">

<script type="text/javascript">
    if (typeof revslider_showDoubleJqueryError === "undefined") {
        function revslider_showDoubleJqueryError(sliderID) {
            var err = "<div class='rs_error_message_box'>";
            err += "<div class='rs_error_message_oops'>Oops...</div>";
            err += "<div class='rs_error_message_content'>";
            err += "You have some jquery.js library include that comes after the Slider Revolution files js inclusion.<br>";
            err += "To fix this, you can:<br>&nbsp;&nbsp;&nbsp; 1. Set 'Module General Options' -> 'Advanced' -> 'jQuery & OutPut Filters' -> 'Put JS to Body' to on";
            err += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jQuery.js inclusion and remove it";
            err += "</div>";
            err += "</div>";
            jQuery(sliderID).show().html(err);
        }
    }
</script>
<link rel='stylesheet' id='vc_font_awesome_5_shims-css'
      href='{{ URL::asset('/public/frontend/themes/YBC/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/v4-shims.min.css')}}'
      type='text/css' media='all'/>
<link rel='stylesheet' id='vc_font_awesome_5-css'
      href='{{ URL::asset('/public/frontend/themes/YBC/wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/all.min.css')}}'
      type='text/css' media='all'/>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/live.linethemes.com\/cosine\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-content/plugins/contact-form-7/includes/js/scripts.js')}}'></script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js')}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var woocommerce_params = {
        "ajax_url": "\/cosine\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/cosine\/?wc-ajax=%%endpoint%%"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js')}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {
        "ajax_url": "\/cosine\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/cosine\/?wc-ajax=%%endpoint%%",
        "cart_hash_key": "wc_cart_hash_2a534ab35dcd7059e87417a1b56fdb43",
        "fragment_name": "wc_fragments_2a534ab35dcd7059e87417a1b56fdb43",
        "request_timeout": "5000"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js')}}'></script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-content/themes/cosine/assets/js/components.js')}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _themeConfig = {
        "stickyHeader": "1",
        "responsiveMenu": "1",
        "blogLayout": null,
        "pagingStyle": "numeric",
        "pagingContainer": "#main-content > .main-content-wrap > .content-inner",
        "pagingNavigator": ".navigation.paging-navigation.loadmore"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-content/themes/cosine/assets/js/theme.js')}}'></script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-includes/js/wp-embed.min.js')}}'></script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js')}}'></script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-content/plugins/line-shortcodes/js/shortcodes-3rd.js')}}'></script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-content/plugins/line-shortcodes/js/shortcodes.js')}}'></script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-content/plugins/js_composer/assets/lib/bower/skrollr/dist/skrollr.min.js')}}'></script>
<script type='text/javascript'
        src='//maps.google.com/maps/api/js?v=3&#038;key=AIzaSyDRpvtm5MXtjh8mCoPvxbZFN2qt3J1qPbk&#038;ver=5.2.9'></script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-content/plugins/line-shortcodes/js/maps.js')}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var mc4wp_forms_config = [];
    /* ]]> */
</script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-content/plugins/mailchimp-for-wp/assets/js/forms-api.min.js')}}'></script>
<!--[if lte IE 9]>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC//wp-content/plugins/mailchimp-for-wp/assets/js/third-party/placeholders.min.js')}}'></script>
<![endif]-->
<script async src="/demo-switcher/main.php"></script>