<style>
    .menu-child {
        display: none;
    }

    nav .nav-list > li:hover > .menu-child {
        display: block;
    }
</style>
<div class="topbar transperent stick">
    <div class="logo">
        <a title="{{ @$settings['name'] }}" href="/"><img class="lazy"
                                                          data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                                                          alt=""></a>
    </div>
    <nav>
        <ul class="nav-list">
            <?php
            $menus = CommonHelper::getFromCache('menu_main_menu', ['menus']);
            if (!$menus) {
                $menus = \Modules\ThemeNVD\Models\Menu::where('status', 1)->where('location', 'main_menu')->whereNull('parent_id')->orderBy('order_no', 'desc')->orderBy('name', 'asc')->get();
                CommonHelper::putToCache('menu_main_menu', $menus, ['menus']);
            }
            ?>
            @foreach($menus as $menu)
                <?php
                    $menu_childs = CommonHelper::getFromCache('menus_childs_' . $menu->id, ['menus']);
                    if (!$menu_childs) {
                        $menu_childs = $menu->childs;
                        CommonHelper::putToCache('menus_childs_' . $menu->id, $menu_childs, ['menus']);
                    }
                    ?>
                <li>
                    <a class="" href="{{@$menu->url}}" title="">{{@$menu->name}} @if(count($menu_childs) > 0)
                            <svg class="bi bi-chevron-down" width="1em" height="1em" viewBox="0 0 16 16" style="display: inline-block;"
                                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"></path>
                            </svg> @endif</a>
                    @if(count($menu_childs) > 0)
                        <div class="menu-child">
                            <ul>
                                @foreach($menu_childs as $child)
                                    <li><a href="{{@$child->url}}" title="">{!! $child->name !!}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </li>
            @endforeach
        </ul>
        @if(@\Auth::guard('student')->check())
            @include('themenvd::partials.user_image')
        @else
            <a class="main-btn" title="" href="/dang-nhap" data-ripple="">Đăng nhập</a>
        @endif
        <div class="shopping_cart">
            <a href="/gio-hang">
                <i class="fas fa-shopping-cart"></i>
                <sup class="cart_total_item">{{Cart::count()}}</sup>
            </a>

        </div>
    </nav><!-- nav menu -->
</div>
<!-- topbar nav -->