<!DOCTYPE html>
<html lang="vi">

<head>
    @include('themenvd::partials.head_meta')

    @include('themenvd::partials.head_wp_script')

    {!! @$settings['frontend_head_code'] !!}
    @yield('head_script')
</head>

<body class="home page-template-default page page-id-2 theme-cosine woocommerce-no-js page-loading layout-wide no-sidebar has-topbar header-v1  pageheader-hidden wpb-js-composer js-comp-ver-6.2.0 vc_responsive"
      itemscope="itemscope" itemtype="http://schema.org/WebPage">
{{--<div class="loading-overlay"></div>--}}


<div id="site-wrapper">

    <div id="site-header">
        <div id="headerbar">
            <div class="wrapper">

                <div class="social-links">
                    <a href="https://www.facebook.com/ybcstartup" target="_blank">
                        <i class="fa fa-facebook-official footer-bottom__link-icon"></i>
                    </a>
                    <a href="https://www.facebook.com/groups/clbdoanhnhantrekhoinghiep.ybcstartup"
                       class="footer-bottom__link">
                        <i class="fas fa-users footer-bottom__link-icon"></i>
                    </a>
                    <a href="https://www.youtube.com/channel/UCJJVCVUbqSh52oiwU4UDfiQ" class="footer-bottom__link ytb">
                        <i class="fab fa-youtube footer-bottom__link-icon"></i>
                    </a>
                    <a href="https://www.tiktok.com/@ybcstartup?lang=vi-VN" class="footer-bottom__link">
                        <img src="/public/frontend/themes/YBC/img/tiktok.png" width="26" alt=""
                             class="footer-bottom__link-icon">
                    </a>
                </div>
                <!-- /.social-links -->

                <div class="custom-info">
                    {{--<i class="fa fa-reply"></i>HN: ybcstartup@gmail.com--}}
                    {{--<i class="fa fa-reply"></i>HCM: clbdoanhnhantrehcm@gmail.com--}}
                    <i class="fa fa-phone"></i>0913 915 358

                </div>
<div class="custom-info" style="margin-left: 540px">
    <div class="widget widget_search"><form role="search" method="get" class="search-form" action="https://live.linethemes.com/cosine/">
            <label>
                <span class="screen-reader-text">Search for:</span>
                <input type="search" class="search-field" placeholder="Tìm kiếm …" value="" name="s" style="height: 35px; border-radius: 20px;">
            </label>
            <input type="submit" class="search-submit" value="Search" style="height: 14px">
        </form>
    </div>

                </div>

                <!-- /.custom-info -->
            </div>
            <!-- /.wrapper -->
        </div>
        <!-- /#headerbar -->
        @include('themenvd::partials.menu_wp')
        <div id="masthead-placeholder"></div>
    </div>
    <!-- /#site-header -->


    <div id="site-content">
        <!-- /#page-header -->


        <div id="page-body">
            <div class="wrapper">

                <div class="content-wrap">

                    <main id="main-content" class="content" itemprop="mainContentOfPage">
                        <div class="main-content-wrap">

                            <div class="content-inner">
                                @include('themenvd::partials.banner_slides_wp')
                                <div class="vc_row-full-width vc_clearfix"></div>

                                @yield('main_content')

                                <div class="vc_row-full-width vc_clearfix"></div>

                            </div>
                            <!-- /.content-inner -->

                        </div>
                    </main>
                    <!-- /#main-content -->

                </div>
                <!-- /.content-wrap -->

            </div>
            <!-- /.wrapper -->
        </div>
        <!-- /#page-body -->

    </div>
    <!-- /#site-content -->

    @include('themenvd::partials.footer_wp')
    <!-- /#site-footer -->
</div>
<!-- /#site-wrapper -->
@include('themenvd::partials.footer_script_wp')

@yield('footer_script')
@include('themenvd::partials.chat_facebook')

{!! @$settings['frontend_body_code'] !!}
</body>

</html>