<style>
    * {
        margin: 0;
        padding: 0;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    /*sub-menu*/
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f5f5f5;
        z-index: 1;
        list-style: none;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .dropdown-item {
        display: block;
        width: 100%;
        padding: -0.25rem 0.5rem -1.25rem 0.75rem;
        clear: both;
        font-weight: 400;
        color: #212529;
        text-align: inherit;
        white-space: nowrap;
        background-color: transparent;
        border: 0;
    }

    .user-profile figure {
        max-height: 300px;
        overflow: hidden;
    }

    /*Menu*/
    .profile-menu > li > .menu-child {
        display: none;
    }
    .profile-menu > li:hover > .menu-child {
        display: block;
    }

    .profile-menu > li ul {
        background: #2a3c53 none repeat scroll 0 0;
        border-radius: 0 0 4px 4px;
        border-top: 1px solid #fa6342;
        list-style: outside none none;
        margin: 0;
        padding-left: 0;
        position: absolute;
        text-align: left;
        top: 100%;
        left: 0;
        transition: all 0.1s linear 0s;
        width: 170px;
        z-index: 9;
        opacity: 1;
        visibility: visible;
    }
    .profile-menu > li ul > li {
        display: inline-block;
        width: 100%;
        position: relative;
    }
    .profile-menu > li ul > li > a {
        display: inline-block;
        font-size: 13px;
        padding: 0px 10px;
        transition: all 0.2s linear 0s;
        width: 100%;
        color: #fff;
    }
    .profile-menu > li ul > li > a:hover {
        background: #1a2c43 none repeat scroll 0 0;
        padding-left: 12px;
    }
    .banner_slides .owl-carousel {
        max-height: 300px;
    }
    .banner_slides .owl-prev, .owl-next {
        left: 20px;
    }
    .banner_slides .owl-next {
        right: 20px;
    }
</style>
<figure>
    @include('themenvd::widgets.banner_slides', ['config' => [
        'location' => 'banner_slides'
    ]])
</figure>
<div class="profile-section">
    <div class="row">
        <div class="col-lg-2 display_none">
            <div class="profile-author">
                <a class="profile-author-thumb" href="/">
                    <img alt="author" data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                         class="lazy">
                </a>
                <div class="author-content">
                    <a class="h4 author-name" href="/">{{ @$settings['name'] }}</a>
                </div>
            </div>
        </div>
        <div class="col-lg-10 display_none">
            <ul class="profile-menu">
                <?php
                $menus = \Modules\ThemeNVD\Helpers\ThemeNVDHelper::getMenusByLocation('main_menu');
                ?>
                @foreach($menus as $menu1)
                    <li class="nav-item dropdown">
                        <a class="nav-link"
                           href="{{ $menu1['url'] }}">{{ $menu1['name'] }}</a>
                        @if(!empty($menu1['childs']))
                            <div class="menu-child">
                                <ul>
                                    @foreach($menu1['childs'] as $c => $menu2)
                                        <li><a href="{{ $menu2['url'] }}" title="{{ $menu2['name'] }}">{{ $menu2['name'] }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>