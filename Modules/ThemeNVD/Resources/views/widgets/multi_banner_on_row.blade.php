{{--Hiển thị nhiều ảnh banner trên 1 dòng--}}

<?php
$data = CommonHelper::getFromCache('banners_' . json_encode($config), ['banners']);
if (!$data) {
    $data = \Modules\ThemeNVD\Models\Banner::where('location', @$config['location'])->where('status', 1);
    if (@$config['where'] != '') {
        $data = $data->whereRaw(@$config['where']);
    }
    if (@$config['limit'] != '') {
        $data = $data->limit(@$config['limit']);
    }
    $data = $data->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
    CommonHelper::putToCache('banners_' . json_encode($config), $data, ['banners']);
}
?>
<style>
    .mt-banner-item {
        padding: 0 7.5px;
        float: left;
        width: {{ 100/count($data) }}%;
    }
</style>
<div class="col-xs-12">
    @foreach($data as $v)
        <div class="mt-banner-item">
            <a href="{{ $v->link }}" title="">
                <figure class="pitrest-post">
                    <img src="{{ asset('public/filemanager/userfiles/'.$v->image) }}" alt="{{ $v->name }}">
                    <h5 class="pitrest-pst-hding">{{ $v->name }}</h5>
                </figure>
            </a>
        </div>
    @endforeach
</div>