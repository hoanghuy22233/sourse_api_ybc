<style>
    .subscribe_to_newsletter-btn {
        border: medium none;
        border-radius: 30px;
        color: #fff;
        float: right;
        font-size: 13px;
        font-weight: 500;
        transition: all 0.2s linear 0s;
        display: inline-block;
        margin-top: 10px;
        padding: 13px 20px;
        width: 100%;
        -webkit-appearance: button;
        background: #fa6342;
    }

</style>
<div class="news-letter-bx">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="leter-meta">
                    <h2>{!! $widget->name !!}</h2>
                    <p>
                        {!! $widget->content !!}
                    </p>
                    <i><img src="{{ asset('/public/frontend/themes/edu-ldp/images/envelop.png') }}" alt=""></i>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="leter-input">
                    <div class="subscribe_to_newsletter-form">
                        <input type="email" name="subscribe_to_newsletter_email" placeholder="Email của bạn...">
                        <button type="submit" class="main-btn subscribe_to_newsletter-btn" data-ripple="" >
                            Đăng ký ngay
                            <i class="fa fa-paper-plane-o"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    $(document).ready(function () {
        $('.subscribe_to_newsletter-btn').click(function () {
            var email = $('input[name=subscribe_to_newsletter_email]').val();
            if (email.length == 0) {
                alert ('Bạn chưa nhập vào email!');
            } else {
                if (!isEmail(email)) {
                    alert ('Email nhập sai!');
                } else {
                    $('.subscribe_to_newsletter-btn').attr('disabled', 'disabled');
                    $('.subscribe_to_newsletter-btn').css('opacity', '0.4');
                    $.ajax({
                        url : '/admin/ajax/contact/add',
                        type: 'POST',
                        data: {
                            email: email,
                            link: '<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>',
                        },
                        success: function (resp) {
                            if (resp.status == true) {
                                alert(resp.msg);
                                $('input[name=subscribe_to_newsletter_email]').val('');
                                $('.subscribe_to_newsletter-btn').removeAttr('disabled');
                                $('.subscribe_to_newsletter-btn').css('opacity', '1');
                            } else {
                                alert(resp.msg);
                                location.reload();
                            }
                        },
                        error: function () {
                            alert('Có lỗi xảy ra! Vui lòng load lại website & thử lại.');
                        }
                    });
                }
            }
        });

        $('.news-letter-bx input, .news-letter-bx textarea').keypress(function (e) {
            if(e.which == 13) {
                $('.subscribe_to_newsletter-btn').click();
            }
        });
    });
</script>