<?php

namespace Modules\CardUser\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\CardHistoryCard\Models\HistoryCard;
use Modules\CardUser\Models\Card;
use Modules\CardUser\Models\User;
use Modules\CardUser\Models\UserAddress;
use Validator;

//use Modules\JdesUser\Models\BillPayment;

class UserController extends CURDBaseController
{

    protected $module = [
        'code' => 'user',
        'table_name' => 'users',
        'label' => 'Khách hàng',
        'modal' => '\Modules\CardUser\Models\User',
        'list' => [
//            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'sort' => true],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên', 'sort' => true],
            ['name' => 'card_id', 'type' => 'relation', 'label' => 'Mã thẻ', 'object' => 'card', 'display_field' => 'code'],
            ['name' => 'tel', 'type' => 'text', 'label' => 'SĐT', 'sort' => true],
//            ['name' => 'email', 'type' => 'text', 'label' => 'Email', 'sort' => true],
//            ['name' => 'gender', 'type' => 'select', 'label' => 'Giới tính', 'options' => [
//                '0' => 'Nam',
//                '1' => 'Nữ',
//            ],],
//            ['name' => 'birthday', 'type' => 'date_vi', 'label' => 'Ngày sinh', 'sort' => true],
//            ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ', 'sort' => true],
            ['name' => 'total_money', 'type' => 'custom', 'td' => 'carduser::list.td.price_eur', 'label' => 'Tổng tiền', 'sort' => true],

            ['name' => 'action', 'type' => 'custom', 'td' => 'carduser::list.td.action', 'class' => '', 'label' => 'Hành động'],
            ['name' => '', 'type' => 'custom', 'td' => 'carduser::list.td.view_info', 'class' => '', 'label' => 'Xem'],
//            ['name' => 'bill', 'type' => 'custom', 'td' => 'carduser::list.td.bill', 'class' => '', 'label' => 'Tạo hóa đơn'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên'],
                ['name' => 'tel', 'type' => 'text', 'class' => 'required', 'label' => 'SĐT', 'group_class' => 'col-md-6'],
                ['name' => 'email', 'type' => 'text', 'class' => '', 'label' => 'Email', 'group_class' => 'col-md-6'],
//                ['name' => 'gender', 'type' => 'select', 'options' =>
//                    [
//                        '0' => 'Nam',
//                        '1' => 'Nữ',
//                    ], 'class' => '', 'value' => '', 'label' => 'Giới tính', 'group_class' => 'col-md-4'],
//                ['name' => 'birthday', 'type' => 'datetimepicker', 'label' => 'Ngày sinh', 'group_class' => 'col-md-4'],
                ['name' => 'country_code', 'type' => 'text', 'class' => 'required', 'label' => 'Mã nước', 'group_class' => 'col-md-4', 'value' => '44'],
//                ['name' => 'address', 'type' => 'custom', 'class' => 'required', 'fields' => 'carduser::form.fields.dynamic4', 'object' => 'tours', 'label' => 'Địa chỉ'],
                ['name' => 'address', 'type' => 'custom', 'class' => 'required', 'fields' => 'carduser::form.fields.dynamic4', 'label' => 'Địa chỉ'],

            ],
//            'image_tab' => [
//                ['name' => 'image', 'type' => 'file_image', 'label' => 'Ảnh'],
//            ],
            'card_info_tab' => [
                ['name' => 'card_id', 'type' => 'select2_ajax_model', 'class' => '', 'label' => 'Mã thẻ', 'model' => Card::class, 'object' => 'card', 'display_field' => 'code','where'=>'status=1'],
                ['name' => 'password', 'type' => 'custom','class' => '', 'fields' => 'carduser::form.fields.pass', 'label' => 'Mật khẩu thẻ'],
            ],

        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Khách hàng',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'email' => [
            'label' => 'Email',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'tel' => [
            'label' => 'SĐT',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'gender' => [
            'label' => 'Giới tính',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Giới tính',
                0 => 'Nam',
                1 => 'Nữ',
            ],
        ],
        'card_id' => [
            'label' => 'Mã thẻ',
            'type' => 'select2_ajax_model',
            'display_field' => 'code',
            'object' => 'card',
            'model' => Card::class,
            'query_type' => '='
        ],
    ];

    public function napTien(Request $request)
    {
        $user = User::find($request->user_id);
        $user->total_money += $request->money_number;
        $user->save();
        $history_bill = HistoryCard::create([

            'card_code' => @$user->card->code,
            'user_name' => @$user->name,
            'user_id' => @$user->id,
            'admin_id' => \Auth::guard('admin')->id(),
            'money' => $request->money_number,
        ]);
        return $history_bill;
    }

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);


        return view('carduser::list')->with($data);
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('carduser::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'tel' => 'unique:users,tel|numeric',
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                    'tel.unique' => 'Số điện thoại ' . $request->tel . ' đã tồn tại',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    $data['admin_id'] = \Auth::guard('admin')->id();
                    //  Tùy chỉnh dữ liệu insert
                    $data['password'] = bcrypt($request->password);

//                    if ($request->has('card_id')) {
//                        $user_used = User::select('name', 'id')->where('card_id', $request->card_id)->first();
//                        if (is_object($user_used)) {
//                            CommonHelper::one_time_message('error', 'Thẻ này đã được sử dụng cho <a href="/admin/user/' . $user_used->id . '" target="_blank">' . $user_used->name . '</a>');
//                            return back()->withInput();
//                        }
//                    }
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {

                        if ($request->has('ten')) {
                            foreach ($request->ten as $kt=>$tt){
                                UserAddress::create([
                                    'name' => $tt,
                                    'address' => $request->diachi[$kt],
//                                    'tel' => $request->sdt[$kt],
//                                    'default' => 0,
                                    'user_id' => $this->model->id
                                ]);
                            }

                        }
                        if ($request->has('card_id') && !empty($request->card_id)) {
                            $card = @Card::find($this->model->card_id);
                            $card->status = 0;
                            $card->save();
                        }

                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('carduser::edit')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'tel' => 'unique:users,tel,' . $request->id . '|numeric',
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                    'tel.unique' => 'Số điện thoại ' . $request->tel . ' đã tồn tại',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());


                    //  Tùy chỉnh dữ liệu insert
                    if ($request->password != null) {
                        $data['password'] = bcrypt($request->password);
                    } else {
                        unset($data['password']);
                    }

//                    if ($request->has('card_id')) {
//
//                        $user_used = User::select('name', 'id')->where('id', '!=', $item->id)->where('card_id', $request->card_id)->first();
//                        if (is_object($user_used)) {
//                            CommonHelper::one_time_message('error', 'Thẻ này đã được sử dụng cho <a href="/admin/user/' . $user_used->id . '" target="_blank">' . $user_used->name . '</a>');
//                            return back()->withInput();
//                        }
//
//                    }
//                    if(isset($request->deffault)){
////                        $request->deffault=1;
////                    }else{
////                        $request->deffault=0;
//                        dd($request->get('deffault'));
//                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {

                        //  Cập nhật địa chỉ user
                        $user_address_ids = [];
                        if ($request->has('ten')) {
                            foreach ($request->ten as $k => $ten) {
                                $user_address = UserAddress::updateOrCreate(
                                    [
                                        'name' => $ten,
                                        'address' => $request->diachi[$k],
                                        'user_id' => $item->id
                                    ],
                                    [
                                    ]);
                                $user_address_ids[] = $user_address->id;
                            }
                        }
                        UserAddress::whereNotIn('id', $user_address_ids)->delete();

                        //  Cập nhật thẻ cho user
                        if ($request->has('card_id') && !empty($request->card_id)) {
                            $card = @Card::find($request->card_id);
                            $card->status = 0;
                            $card->save();
                        }
//                        if ($request->has('ten')) {
//                            $user_address = UserAddress::where('user_id', $item->id)->get();
//                            foreach ($request->ten as $kt => $tt) {
//                                foreach ($user_address as $kt1 => $tt1) {
//                                    UserAddress::updateOrCreate(
//                                        ['id' => $request->idd[$kt]],
//                                        ['name' => $request->ten[$kt],
//                                            'address' => $request->diachi[$kt],
//                                            'user_id' => $item->id
//                                        ]);
//                                    UserAddress::whereNotIn('id', $request->idd)->delete();
//                                }
//                            }
//                        }
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            $items = $this->model->whereIn('id', $ids)->get();
            foreach ($items as $item) {

                $item->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function checkTelDuplicate(Request $request)
    {
        $tel = BillPayment::where('tel', $request->tel)->where('tel', '<>', '')->whereNotNull('tel')->get()->count();

        if (isset($request->id)) {
            $tel = BillPayment::where('tel', $request->tel)->where('tel', '<>', '')->where('id', '<>', $request->id)->whereNotNull('tel')->get()->count();
        }
        $html_tel = '';
        if ($tel > 0) {
            $html_tel = 'Số điện thoại này đã tồn tại';
        }
        return $html_tel;
    }

    public function checkMailDuplicate(Request $request)
    {
        $email = BillPayment::where('email', $request->email)->whereNotNull('email')->where('email', '<>', '')->get()->count();
        if (isset($request->id)) {
            $email = BillPayment::where('email', $request->email)->where('email', '<>', '')->where('id', '<>', $request->id)->whereNotNull('email')->get()->count();
        }
        $html_email = '';
        if ($email > 0) {
            $html_email = 'Email này đã tồn tại';
        }
        return $html_email;
    }
}
