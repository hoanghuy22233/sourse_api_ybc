<?php

namespace Modules\CardUser\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class CardUserServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('CardUser', 'Database/Migrations'));

        $this->registerPermission();

    }

public function registerPermission()
{
    \Eventy::addFilter('permission.check', function ($per_check) {
        $per_check = array_merge($per_check, ['user_view', 'user_add', 'user_edit', 'user_delete']);
        return $per_check;
    }, 1, 1);
}

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('CardUser', 'Config/config.php') => config_path('carduser.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('CardUser', 'Config/config.php'), 'carduser'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/carduser');

        $sourcePath = module_path('CardUser', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/carduser';
        }, \Config::get('view.paths')), [$sourcePath]), 'carduser');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/carduser');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'carduser');
        } else {
            $this->loadTranslationsFrom(module_path('CardUser', 'Resources/lang'), 'carduser');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('CardUser', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
