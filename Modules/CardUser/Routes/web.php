<?php

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'user'], function () {
        Route::get('', 'Admin\UserController@getIndex')->name('user')->middleware('permission:user_view');
        Route::get('publish', 'Admin\UserController@getPublish')->name('user.publish')->middleware('permission:user_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\UserController@add')->middleware('permission:user_add');
        Route::get('delete/{id}', 'Admin\UserController@delete')->middleware('permission:user_delete');
        Route::post('multi-delete', 'Admin\UserController@multiDelete')->middleware('permission:user_delete');
        Route::get('search-for-select2', 'Admin\UserController@searchForSelect2')->name('user.search_for_select2')->middleware('permission:user_view');
        Route::get('{id}', 'Admin\UserController@update');
        Route::post('{id}', 'Admin\UserController@update')->middleware('permission:user_edit');
    });
});

Route::get('nap-tien', 'Admin\UserController@napTien');
Route::post('check-tel-duplicate', 'Admin\UserController@checkTelDuplicate');
Route::post('check-mail-duplicate', 'Admin\UserController@checkMailDuplicate');
