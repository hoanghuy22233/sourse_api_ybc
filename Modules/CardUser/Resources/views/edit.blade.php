@extends(config('core.admin_theme').'.template')
@section('main')
    <?php
    $status_text = [
        0 => 'Mới tạo',
        1 => 'Chờ xưởng duyệt',
        2 => 'Đang làm',
        3 => 'Hoàn thành',
        4 => 'Hủy',
    ];
    ?>
    <form class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid {{ @$module['code'] }}"
          action="" method="POST"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="return_direct" value="save_continue" type="hidden">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                     id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg" style="">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Chỉnh sửa {{ $module['label'] }}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="/admin/{{ $module['code'] }}" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Quay lại</span>
                            </a>
                            <div class="btn-group">
                                @if(in_array($module['code'].'_edit', $permissions))
                                    <button type="submit" class="btn btn-brand">
                                        <i class="la la-check"></i>
                                        <span class="kt-hidden-mobile">Lưu</span>
                                    </button>
                                    <button type="button"
                                            class="btn btn-brand dropdown-toggle dropdown-toggle-split"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_continue">
                                                    <i class="kt-nav__link-icon flaticon2-reload"></i>
                                                    Lưu và tiếp tục
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_exit">
                                                    <i class="kt-nav__link-icon flaticon2-power"></i>
                                                    Lưu & Thoát
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_create">
                                                    <i class="kt-nav__link-icon flaticon2-add-1"></i>
                                                    Lưu và tạo mới
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-9">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Thông tin khách hàng
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                @foreach($module['form']['general_tab'] as $field)
                                    @php
                                        $field['value'] = @$result->{$field['name']};
                                    @endphp
                                    <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                         id="form-group-{{ $field['name'] }}">
                                        @if($field['type'] == 'custom')
                                            @include($field['fields'], ['fields' => $field])
                                        @else
                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                    <span class="color_btd">*</span>@endif</label>
                                            <div class="col-xs-12">
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                <span class="form-text text-muted">{!! @$field['des'] !!}</span>
                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->

            </div>
            <div class="col-xs-12 col-md-3">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Ví tiền
                            </h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                @foreach($module['form']['card_info_tab'] as $field)
                                    @php
                                        $field['value'] = @$result->{$field['name']};
                                    @endphp
                                    <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                         id="form-group-{{ $field['name'] }}">
                                        @if($field['type'] == 'custom')
                                            @include($field['fields'], ['fields' => $field])
                                        @else
                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                    <span class="color_btd">*</span>@endif</label>
                                            <div class="col-xs-12">
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                <span class="form-text text-muted">{!! @$field['des'] !!}</span>
                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                            <div class="kt-section kt-section--first">
                                <label>Số dư: £{{ number_format($result->total_money, 2, '.', '.') }}</label><br>
                                <label><a href="/admin/history-card?user_id={{ $result->id }}">Lịch sử nạp tiền</a></label><br>
                                <label><a href="/admin/bill?user_id={{ $result->id }}">Lịch sử thanh toán</a></label><br>
                                <label><a href="/admin/bill/add?user_id={{ $result->id }}">Tạo hóa đơn</a></label><br>
                                <div class="pop_{{$result->id}}" style="cursor: pointer">
                                    <span class="btn btn-primary">Nạp tiền</span>

                                    <div class="form-group hidden_{{$result->id}} add_{{$result->id}}" style="display: none;">

                                        <input style="margin-top: 20px;" type="number" class="form-control "
                                               name="add-money" placeholder="Nhập số tiền">
                                        <span  class="nap_{{$result->id}} btn btn-success">Nạp tiền</span>
                                    </div>
                                </div>
                                <script>
                                    $(document).ready(function () {

                                        $(".nap_{{$result->id}}").click(function () {
                                            var money_number = $(this).parent('.add_{{$result->id}}').find('input').val();
                                            var user_id = '{{$result->id}}';
                                            if (confirm('Bạn có chắc chắn muốn nạp không')) {
                                                $.ajax({
                                                    url: '/nap-tien',
                                                    type: 'get',
                                                    data: {
                                                        'money_number': money_number,
                                                        'user_id': user_id,
                                                    },
                                                    success: function () {
                                                        location.reload();
                                                    }
                                                });
                                            }
                                            location.reload();
                                        });

                                        $(".pop_{{$result->id}}").click(function () {
                                            $(this).children(".add_{{$result->id}}").show();
                                            $('.che_man').show();
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->

                <!--begin::Portlet-->
                {{--<div class="kt-portlet">--}}
                    {{--<div class="kt-portlet__head">--}}
                        {{--<div class="kt-portlet__head-label">--}}
                            {{--<h3 class="kt-portlet__head-title">--}}
                                {{--Ảnh--}}
                            {{--</h3>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!--begin::Form-->--}}
                    {{--<div class="kt-form">--}}
                        {{--<div class="kt-portlet__body">--}}
                            {{--<div class="kt-section kt-section--first">--}}
                                {{--@foreach($module['form']['image_tab'] as $field)--}}

                                    {{--@php--}}
                                        {{--$field['value'] = @$result->{$field['name']};--}}
                                    {{--@endphp--}}
                                    {{--<div class="form-group-div form-group {{ @$field['group_class'] }}"--}}
                                         {{--id="form-group-{{ $field['name'] }}">--}}
                                        {{--<label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)--}}
                                                {{--<span class="color_btd">*</span>@endif</label>--}}
                                        {{--<div class="col-xs-12">--}}
                                            {{--@include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])--}}
                                            {{--<span class="text-danger">{{ $errors->first($field['name']) }}</span>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!--end::Form-->--}}
                {{--</div>--}}
            </div>


        </div>

    </form>
@endsection
@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/form.css') }}">
@endsection
@section('custom_footer')
    <script src="{{ asset(config('core.admin_asset').'/js/pages/crud/metronic-datatable/advanced/vertical.js') }}"
            type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce_editor.js') }}"></script>
    <script type="text/javascript">
        editor_config.selector = ".editor";
        editor_config.path_absolute = "{{ (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" }}/";
        tinymce.init(editor_config);
    </script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/form.js') }}"></script>
@endsection
