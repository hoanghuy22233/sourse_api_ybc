<input type="text" name="{{ @$field['name'] }}" class="form-control {{ @$field['class'] }}"
       id="{{ $field['name'] }}" {!! @$field['inner'] !!}
       value=""
       {{ strpos(@$field['class'], 'require') !== false ? 'required' : '' }}
       placeholder="{{ trans(@$field['label']) }}"
>
