<?php
///*
//|--------------------------------------------------------------------------
//| API Routes
//|--------------------------------------------------------------------------
//|
//| Here is where you can register API routes for your application. These
//| routes are loaded by the RouteServiceProvider within a group which
//| is assigned the "api" middleware group. Enjoy building your API!
//|
//*/
//
//Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
//    Route::group(['prefix' => 'job', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
//        Route::get('/{id}', 'Admin\JobController@getDetail');
//    });
//});


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'jobs', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\JobController@index')->middleware('api_permission:job_view');
        Route::get('deadline', 'Admin\JobController@deadline')->middleware('api_permission:job_view');
        Route::get('{id}', 'Admin\JobController@show')->middleware('api_permission:job_view');
    });

    Route::group(['prefix' => 'tasks', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\TaskController@index')->middleware('api_permission:job_view');
        Route::get('finish/{id}', 'Admin\TaskController@finish')->middleware('api_permission:job_view');
    });
    Route::get('comment/{id}', 'Admin\CommentController@showComment')->middleware('api_permission:job_view');
});
