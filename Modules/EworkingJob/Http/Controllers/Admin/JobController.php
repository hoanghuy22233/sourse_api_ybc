<?php

/**
 * Widget Controller
 *
 * Widget Controller manages Widget by admin.
 *
 * @category   Widget
 * @package    hobasoft
 * @author     hobasoft.com
 * @copyright  2018 hobasoft.com
 * @license
 * @version    1.3
 * @link       http://hobasoft.com
 * @email      webhobasoft@gmail.com
 * @since      Version 1.0
 * @deprecated None
 */

namespace Modules\EworkingJob\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Helpers\CommonHelper;
use Auth;
use DB;
use Illuminate\Http\Request;
use Modules\EworkingJob\Models\Job;
use Modules\EworkingJob\Models\JobType;
use Modules\EworkingJob\Models\Project;
use Modules\EworkingJob\Models\RoleAdmin;
use Modules\EworkingJob\Models\Subject;
use Modules\EworkingJob\Models\Task;
use Validator;

class JobController extends CURDBaseController
{
    protected $orderByRaw = 'id desc';
    protected $_base;

    public function __construct()
    {
        parent::__construct();
        $this->_base = new BaseController();
    }

    protected $module = [
        'code' => 'job',
        'label' => 'Công việc',
        'modal' => '\Modules\EworkingJob\Models\Job',
        'list' => [
            ['name' => 'project_id', 'type' => 'custom', 'td' => 'eworkingjob::job.td.project_id', 'label' => 'Dự án', 'sort' => true],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tiêu đề', 'sort' => true],
            ['name' => 'end_date', 'type' => 'custom', 'td' => 'eworkingjob::job.td.deadline', 'label' => 'Deadline', 'sort' => true],
            ['name' => 'employees_id', 'type' => 'custom', 'td' => 'eworkingjob::job.td.admin_ids_job', 'label' => 'Người làm'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái', 'sort' => true],
        ],
        'form' => [
            'header_job' => [
                'class' => '',
                'td' => [
                    ['name' => 'header_job', 'view' => 'custom', 'type' => 'eworkingjob::form.header_job'],
                ],
            ],
            'general' => [
                'label' => 'Thông tin chung <a class="btn-add edit-general" title="Sửa thông tin chung"><i class="flaticon-edit"></i></a>',
                'class' => '',
                'td' => [
                    ['name' => 'name', 'view' => 'custom', 'type' => 'eworkingjob::form.general', 'class' => 'required', 'label' => 'Tên công việc'],
                ]
            ],
            'sub_manager' => [
                'label' => 'Phụ trách chung',
                'class' => '',
                'td' => [
                    ['name' => 'name', 'view' => 'custom', 'type' => 'eworkingjob::form.sub_manager', 'class' => 'required', 'label' => 'Tên công việc'],
                ],
            ],
            'task_single' => [
                'label' => 'Nhiệm vụ không thuộc bộ môn',
                'class' => '',
                'td' => [
                    ['name' => 'subject_id', 'view' => 'custom', 'type' => 'eworkingjob::form.task_content'],
                ],
            ],
            'subject' => [
                'label' => 'Bộ môn',
                'class' => '',
                'td' => [
                    ['name' => 'subject_id', 'view' => 'custom', 'type' => 'eworkingjob::form.subjects_content'],
                ],
            ],

            'comment_box' => [
                'label' => 'Bình luận',
                'class' => '',
                'td' => [
                    ['name' => 'comment_box', 'view' => 'custom', 'type' => 'eworkingjob::form.comment_box', 'class' => 'comment_box'],
                ],
            ],
        ],
        'col' => [
            'col1' => [
                'class' => 'col-xs-12 col-md-4',
                'tab_name' => ['general', 'comment_box'],
            ],
            'col2' => [
                'class' => 'col-xs-12 col-md-8',
                'tab_name' => ['sub_manager', 'task_single', 'subject'],
            ]
        ],
        'footer' => [
            'eworkingjob::popup.subject.popup_add_subject',
            'eworkingjob::popup.task.popup_add_task',
            'eworkingjob::popup.job.edit_general',
            'eworkingjob::popup.job.edit_submanager',
            'eworkingjob::job.footer',
        ]
    ];

    protected $filter = [
        'project_id' => [
            'label' => 'Dự án',
            'type' => 'custom',
            'field' => 'admin.themes.metronic1.list.filter.select2_ajax_model',
            'model' => \Modules\EworkingJob\Models\Project::class,
            'object' => 'project',
            'where_this_company' => true,
            'display_field' => 'name',
            'query_type' => '='
        ],
        'name' => [
            'label' => 'Tiêu đề',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'end_date' => [
            'filter' => 'custom',
            'label' => 'Deadline',
            'type' => 'date',
            'query_type' => '='
        ],
    ];
    protected $listStatus = [
        "1" => 'Chờ duyệt',
        "2" => 'Đang làm',
        "3" => 'Làm lại',
        "4" => 'Chưa làm',
        "5" => 'Hoàn thành',
        "6" => 'Hủy'
    ];
    protected $stringAdd = ' đã thêm bạn vào một nhiệm vụ trong công việc ';
    protected $stringRemove = ' đã xóa bạn khỏi một nhiệm vụ trong công việc ';

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    if (@$filter_option['filter'] == 'custom') {
                        $listJob = Job::whereRaw($where)->pluck('id')->toArray();
                        $listJobTrue = [];
                        foreach ($listJob as $idJob) {
                            $query = \Modules\EworkingJob\Models\Task::select('end_date')->whereIn('task_type', [1, 2, 3])->where('job_id', $idJob)->orderBy('end_date', 'desc');
                            if ($query->exists()) {
                                if ($query->first()->end_date == $request{$filter_name}) {
                                    $listJobTrue[] = $idJob;
                                }
                            }
                        }
                        $list = implode(',', $listJobTrue);
                        if ($list != '') {
                            $where .= " AND id IN (" . $list . ")";
                        } else {
                            $where .= " AND id is null";
                        }
                    } else {
                        $where .= " AND " . $filter_name . " = '" . $request->get($filter_name) . "'";
                    }
                }
            }
        }
        return $where;
    }

    public function add(Request $request)
    {
        if ($request->ajax()) {
            $item = new Job();
            if ($request->has('project_id')) {
                $item->project_id = $request->project_id;
            }
            $item->admin_id = Auth::guard('admin')->user()->id;
            $item->company_id = Auth::guard('admin')->user()->last_company_id;
            $item->name = $request->name;
            $item->intro = $request->intro;
            $item->job_type_id = $request->job_type_id;
            $item->status = 1;
            if ($item->save()) {
                CommonHelper::one_time_message('success', 'Tạo thành công!');
//            if ($request->ajax()) {
                return response()->json([
                    'status' => true,
                    'link' => route('job.edit', $item->id)
                ]);
//            }
            } else {
                CommonHelper::one_time_message('success', 'Tạo công việc thất bại!');
//            if ($request->ajax()) {
                return response()->json([
                    'status' => false
                ]);
//            }
            }
        }
//        return redirect('/admin/' . $this->module['code'] . '/' . $item->id);
    }

    public function JobTypeId()
    {
        // Truy vấn các công việc thuộc phòng ban mà thành viên được phêp xem
        $find_role_id = RoleAdmin::where('admin_id', \Auth::guard('admin')->user()->id)
            ->where('company_id', \Auth::guard('admin')->user()->last_company_id)
            ->first();
        $role_id = '';
        if (!is_null($find_role_id)) {
            $role_id = $find_role_id->role_id;
        }

        $jobTypeIds = JobType::where('role_ids', 'like', '%|' . $role_id . '|%')
            ->pluck('id')
            ->toArray();
        return $jobTypeIds;
    }


    public function appendWhere($query, $request)
    {

        $jobTypeIds = $this->JobTypeId();
        $query = $query->whereIn('job_type_id', $jobTypeIds);
        $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id)
            ->where('name', '!=', '');
        return $query;
    }

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);
        return view('eworkingjob::list')->with($data);
    }

    public function convertValueStringToArray($string)
    {
        $data = [];
        foreach ((array)json_decode($string) as $key => $value) {
            $get_key = array_keys((array)$value);
            $data[(string)end($get_key)] = end($value);
        }
        return $data;
    }


    public function update(Request $request)
    {
        $jobTypeIds = $this->JobTypeId();

        $item = $this->model->find($request->id);
        if (!is_object($item)) abort(404);
        //  Chỉ sửa được liệu công ty mình đang vào
        if (!in_array($item->job_type_id, $jobTypeIds) || strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
            CommonHelper::one_time_message('error', 'Bạn không có quyền!');
            return redirect()->route('job');
        }

        $dir_name = config('eworkingjob.job_data_draf') . $request->id;
        $filename = $dir_name . '/draft.txt';

        if (!$_POST) {


            if ($request->has('reset') && $request->reset == 1) {
//            reset data job
                if (file_exists($filename)) {
                    unlink($filename);
                    CommonHelper::one_time_message('success', 'Reset dữ liệu thành công!');
                } else {
                    CommonHelper::one_time_message('success', 'Dữ liệu của bạn đang là mới nhất!');
                }

                return redirect('/admin/job/' . $request->id);
            }
            $data = $this->getDataUpdate($request, $item);
            $data['message_data'] = [];
            if (file_exists($filename)) { //kiem tra xem file ton tai thi lay ra content
                $data['message_data'] = (array)json_decode(file_get_contents($filename));
            }
            return view('eworkingjob::job.edit')->with($data);
        } else if ($_POST) {

            if ($request->ajax()) {
                //LƯU DỰ LIỆU NHÁP VÀO DỮ LIỆU
                $this->saveSubject($dir_name, $filename, $request);

                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }
//
//                return $this->updateReturn($request, $item);
            }
        }
//        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
////            CommonHelper::one_time_message('error', $ex->getMessage());
//            return redirect()->back()->withInput();
//        }
    }

    function addNotificationsForAddTask($admins, $job_id, $task)
    {
//        $content = is_numeric($task->project_id) ? 'thuộc dự án ' . $task->project->name : $task->name;
        $content = $task->job->project_id != null ? 'thuộc dự án ' . $task->job->project->name : $task->job->name;
        $arrAdmins = explode('|', $admins);
        foreach ($arrAdmins as $key => $admin) {
            if (Auth::guard('admin')->user()->id != $admin) {
                $dataNotification1 = [
                    'name' => 'Thêm vào nhiệm vụ',
                    'content' => Auth::guard('admin')->user()->name . $this->stringAdd . $content,
                    'link' => route('job.edit', ['id' => $job_id]),
                    'readed' => 0,
                    'type' => 1, //hành động thêm vào nhiệm vụ
                    'from_admin_id' => Auth::guard('admin')->user()->id,
                    'to_admin_id' => (int)$admin,
                    'job_id' => $job_id,
                    'project_id' => @$task->job->project_id,
                    'company_id' => Auth::guard('admin')->user()->last_company_id,
                ];
                $this->createNotification($dataNotification1);
            }
        }
    }


    function addNotificationsForTask($admins, $job_id, $task)
    {
//        $content = is_numeric($task->project_id) ? 'thuộc dự án ' . $task->project->name : $task->name;
        $content = $task->job->project_id != null ? 'thuộc dự án ' . $task->job->project->name : $task->job->name;
        if ($admins['currents'] != null) {

            foreach ($admins['currents'] as $admin_currents) {
                if (Auth::guard('admin')->user()->id != $admin_currents) {
                    $dataNotification2 = [
                        'name' => 'Xóa khỏi nhiệm vụ',
                        'content' => Auth::guard('admin')->user()->name . $this->stringRemove . $content,
                        'link' => route('job.edit', ['id' => $job_id]),
                        'readed' => 0,
                        'type' => 2, //hành động xóa khỏi nhiệm vụ
                        'from_admin_id' => Auth::guard('admin')->user()->id,
                        'to_admin_id' => (int)$admin_currents,
                        'job_id' => $job_id,
                        'project_id' => @$task->job->project_id,
                        'company_id' => Auth::guard('admin')->user()->last_company_id,
                    ];
                    $this->createNotification($dataNotification2);
                }
            }
        }

        if ($admins['news'] != null) {
            foreach ($admins['news'] as $admin_news) {
                if (Auth::guard('admin')->user()->id != $admin_news) {
                    $dataNotification1 = [
                        'name' => 'Thêm vào nhiệm vụ',
                        'content' => Auth::guard('admin')->user()->name . $this->stringAdd . $content,
                        'link' => route('job.edit', ['id' => $job_id]),
                        'readed' => 0,
                        'type' => 1, //hành động thêm vào nhiệm vụ
                        'from_admin_id' => Auth::guard('admin')->user()->id,
                        'to_admin_id' => (int)$admin_news,
                        'job_id' => $job_id,
                        'project_id' => @$task->job->project_id,
                        'company_id' => Auth::guard('admin')->user()->last_company_id,
                    ];
                    $this->createNotification($dataNotification1);
                }
            }
        }
    }

    public
    function saveSubject($dir_name, $filename, $request)
    {
//        try {
//        DB::beginTransaction();
        $message_data = [];
        if (file_exists($filename)) { //kiem tra xem file ton tai thi lay ra content
            $message_data = (array)json_decode(file_get_contents($filename));
        }
//        dd($message_data);
        foreach ($message_data as $data_draft) {

//            update thong tin chung
            if (isset($data_draft->general)) {
                unset($data_draft->general->job_type_name);
                unset($data_draft->general->project_name);
                unset($data_draft->general->job_id);
                $job = Job::find($request->id);
                foreach ((array)$data_draft->general as $name => $value) {
                    $job->{$name} = $value;
                }
                $job->save();
            } else if (isset($data_draft->sub_manage)) {

                $query = Task::where('job_id', $request->id)->where('task_type', 1);
                if ($query->exists()) {
                    $submanager = $query->first();
                    $add = false;
                } else {
                    $submanager = new Task();
                    $add = true;
                }
                $admins = $this->getAdminAddTask($submanager->admin_ids, $data_draft->sub_manage->admin_ids);
                $old_state = $submanager->status;
                $old_admin = $submanager->admin_ids;
                foreach ($data_draft->sub_manage as $name => $value) {
                    $submanager->{$name} = $value;
                }

                $submanager->name = 'Phụ trách chung';
                $submanager->job_id = $request->id;
                $submanager->task_type = 1;
                $submanager->company_id = Auth::guard('admin')->user()->last_company_id;
                if ($add) {
                    $submanager->admin_id = Auth::guard('admin')->user()->id;
                }
                if ($submanager->save()) {
                    if ($old_state != $submanager->status) {

                        $content = $submanager->job->project_id != null ? 'thuộc dự án ' . $submanager->job->project->name : $submanager->job->name;
//
                        $success_text = $submanager->status == 0 ? 'hoàn thành' : 'thực hiện lại';
                        foreach (explode('|', trim($submanager->admin_ids, '|')) as $admin_sub) {
                            if (Auth::guard('admin')->user()->id != $admin_sub) {
                                $dataNotification = [
                                    'name' => ucwords($success_text) . ' công việc',
                                    'content' => Auth::guard('admin')->user()->name . ' đã đánh dấu ' . $success_text . ' công việc ' . $content,
                                    'link' => route('job.edit', ['id' => $submanager->job_id]),
                                    'readed' => 0,
                                    'type' => 6, //hành động thêm vào nhiệm vụ
                                    'from_admin_id' => Auth::guard('admin')->user()->id,
                                    'to_admin_id' => $admin_sub,
                                    'job_id' => $submanager->job_id,
                                    'project_id' => @$submanager->job->project_id,
                                    'company_id' => Auth::guard('admin')->user()->last_company_id,
                                ];
                                $this->createNotification($dataNotification);
                            }
                        }

                    }
                    if ($admins['currents'] != null) {

                        foreach ($admins['currents'] as $admin_currents) {
                            if (Auth::guard('admin')->user()->id != $admin_currents) {
                                $dataNotification2 = [
                                    'name' => 'Phụ trách chung',
                                    'content' => Auth::guard('admin')->user()->name . $this->stringRemove . $content,
                                    'link' => route('job.edit', ['id' => $submanager->job_id]),
                                    'readed' => 0,
                                    'type' => 2, //hành động thêm vào nhiệm vụ
                                    'from_admin_id' => Auth::guard('admin')->user()->id,
                                    'to_admin_id' => (int)$admin_currents,
                                    'job_id' => $submanager->job_id,
                                    'project_id' => @$submanager->job->project_id,
                                    'company_id' => Auth::guard('admin')->user()->last_company_id,
                                ];
                                $this->createNotification($dataNotification2);
                            }
                        }
                    }

                    if ($admins['news'] != null) {
                        foreach ($admins['news'] as $admin_news) {
                            if (Auth::guard('admin')->user()->id != $admin_news) {
                                $dataNotification1 = [
                                    'name' => 'Phụ trách chung',
                                    'content' => Auth::guard('admin')->user()->name . $this->stringAdd . $content,
                                    'link' => route('job.edit', ['id' => $submanager->job_id]),
                                    'readed' => 0,
                                    'type' => 1, //hành động thêm vào nhiệm vụ
                                    'from_admin_id' => Auth::guard('admin')->user()->id,
                                    'to_admin_id' => (int)$admin_news,
                                    'job_id' => $submanager->job_id,
                                    'project_id' => @$submanager->job->project_id,
                                    'company_id' => Auth::guard('admin')->user()->last_company_id,
                                ];
                                $this->createNotification($dataNotification1);
                            }
                        }
                    }
                }
            } else if (isset($data_draft->add_tasks)) {

                foreach ($data_draft->add_tasks as $key => $task_draft) {

                    $task = new Task;
                    $task->name = $task_draft->name;
                    $task->start_date = $task_draft->start_date;
                    $task->end_date = $task_draft->end_date;
                    $task->admin_ids = $task_draft->admin_ids;
                    $task->job_id = $request->id;
                    $task->status = $task_draft->status;
                    $task->task_type = 2;
                    $task->company_id = Auth::guard('admin')->user()->last_company_id;
                    $task->admin_id = Auth::guard('admin')->user()->id;
                    $task->save();
                    $admins = trim($task->admin_ids, '|');
                    if (!is_null($admins)) {
                        $this->addNotificationsForAddTask($admins, $request->id, $task);
                    }
                }
            } else if (isset($data_draft->add_subject)) { //neu co obj roi thi update

                foreach ($data_draft->add_subject as $key => $subject_draft) {
                    $subject = new Subject;
                    $subject->name = $subject_draft->name;
                    $subject->job_id = $request->id;
                    $subject->company_id = Auth::guard('admin')->user()->last_company_id;
                    $subject->type = 1;
                    $subject->admin_id = Auth::guard('admin')->user()->id;
                    $subject->save();
                    foreach ($subject_draft->data as $task_child) {
                        $task = new Task;
                        $task->name = $task_child->name;
                        $task->start_date = $task_child->start_date;
                        $task->end_date = $task_child->end_date;
                        $task->admin_ids = $task_child->admin_ids;
                        $task->job_id = $request->id;
                        $task->status = $task_child->status;
                        $task->subject_id = $subject->id;
                        $task->task_type = 3;
                        $task->company_id = Auth::guard('admin')->user()->last_company_id;
                        $task->admin_id = Auth::guard('admin')->user()->id;
                        if ($task->save()) {
                            $admins = trim($task->admin_ids, '|');
                            if (!is_null($admins)) {
                                $this->addNotificationsForAddTask($admins, $request->id, $task);
                            }
                        }
                    }
                }
            } else if (isset($data_draft->main_adit_task)) { //neu co obj roi thi update
                foreach ($data_draft->main_adit_task as $key => $task_single) {
                    $task = Task::where('id', $task_single->id)->first();
                    if (is_object($task)) {
                        if ($task_single->delete_status) {
                            Task::destroy($task_single->id);
                        } else {
                            $admins = $this->getAdminAddTask($task->admin_ids, $task_single->admin_ids);
                            $task->name = $task_single->name;
                            $task->start_date = $task_single->start_date;
                            $task->end_date = $task_single->end_date;
                            $task->admin_ids = $task_single->admin_ids;
                            $old_state = $task->status;
                            $task->status = $task_single->status;
                            $task->company_id = Auth::guard('admin')->user()->last_company_id;
                            $task->task_type = 2;

                            if ($task->save()) {
                                if ($old_state != $task_single->status && Auth::guard('admin')->user()->id != $task->admin_id) {
                                    $content = $task->job->project_id != null ? 'thuộc dự án ' . $task->job->project->name : $task->job->name;
                                    $dataNotification = [
                                        'name' => 'Thay đổi trạng thái nhiệm vụ',
                                        'content' => Auth::guard('admin')->user()->name . ' đã thay đổi trạng thái nhiệm vụ trong công việc ' . $content,
                                        'link' => route('job.edit', ['id' => $task->job_id]),
                                        'readed' => 0,
                                        'type' => 6, //hành động thay đổi trạng thái nhiệm vụ
                                        'from_admin_id' => Auth::guard('admin')->user()->id,
                                        'to_admin_id' => $task->admin_id,
                                        'job_id' => $task->job_id,
                                        'project_id' => @$task->job->project_id,
                                        'company_id' => Auth::guard('admin')->user()->last_company_id,
                                    ];
                                    $this->createNotification($dataNotification);
                                }
                                $this->addNotificationsForTask($admins, $request->id, $task);
                            }
                        }
                    }
                }
            } else if (isset($data_draft->main_adit_subject)) { //neu co obj roi thi update
                foreach ($data_draft->main_adit_subject as $key => $task_child) {
//                    $task = Task::findorFail($task_child->id);
                    $task = Task::where('id', $task_child->id)->first();

                    if (is_object($task)) {
                        if ($task_child->delete_status) {
                            Task::destroy($task_child->id);
                        } else {
                            $admins = $this->getAdminAddTask($task->admin_ids, $task_child->admin_ids);
                            $task->name = $task_child->name;
                            $task->start_date = $task_child->start_date;
                            $task->end_date = $task_child->end_date;
                            $task->admin_ids = $task_child->admin_ids;
                            $old_state = $task->status;
                            $task->status = $task_child->status;
                            $task->company_id = Auth::guard('admin')->user()->last_company_id;
                            $task->task_type = 3;
                            if ($task->save()) {
                                if ($old_state != $task_child->status && Auth::guard('admin')->user()->id != $task->admin_id) {
//                                    $content = $task->project_id != null ? 'thuộc dự án ' . $task->project->name : $task->job->name;
                                    $content = $task->job->project_id != null ? 'thuộc dự án ' . $task->job->project->name : $task->job->name;
                                    $dataNotification = [
                                        'name' => 'Thay đổi trạng thái nhiệm vụ',
                                        'content' => Auth::guard('admin')->user()->name . ' đã thay đổi trạng thái nhiệm vụ trong công việc ' . $content,
                                        'link' => route('job.edit', ['id' => $task->job_id]),
                                        'readed' => 0,
                                        'type' => 6, //hành động thay đổi trạng thái nhiệm vụ
                                        'from_admin_id' => Auth::guard('admin')->user()->id,
                                        'to_admin_id' => $task->admin_id,
                                        'job_id' => $task->job_id,
                                        'project_id' => @$task->job->project_id,
                                        'company_id' => Auth::guard('admin')->user()->last_company_id,
                                    ];
                                    $this->createNotification($dataNotification);
                                }
                                $this->addNotificationsForTask($admins, $request->id, $task);
                            }

                        }
                    }
                }
            } else if (isset($data_draft->add_task_in_subject)) { //neu co obj roi thi update
                foreach ($data_draft->add_task_in_subject as $key => $task_child) {
                    $task = new Task;
                    $task->name = $task_child->name;
                    $task->start_date = $task_child->start_date;
                    $task->end_date = $task_child->end_date;
                    $task->admin_ids = $task_child->admin_ids;
                    $task->job_id = $request->id;
                    $task->status = $task_child->status;
                    $task->subject_id = $task_child->subject_id;
                    $task->company_id = Auth::guard('admin')->user()->last_company_id;
                    $task->admin_id = Auth::guard('admin')->user()->id;
                    $task->task_type = 3;
                    if ($task->save()) {
                        $admins = trim($task->admin_ids, '|');
                        if (!is_null($admins)) {
                            $this->addNotificationsForAddTask($admins, $request->id, $task);
                        }
                    }
                }
            } else if (isset($data_draft->main_delete_subject)) {
                Subject::whereIn('id', $data_draft->main_delete_subject)->delete();
            }
        }

        if (file_exists($filename)) {
            unlink($filename);
            CommonHelper::one_time_message('success', 'Cập nhật dữ liệu thành công');
        } else {
            CommonHelper::one_time_message('success', 'Dữ liệu không có thay đổi !');
        }
//            DB::commit();
//        } catch (\Exception $e) {

//        DB::rollback();
//        }

    }

    function createNotification($data)
    {
        if (is_array($data)) {
            try {
                \Eventy::action('notification.add', $data);
            } catch (\Exception $ex) {

            }
        }
    }

    public
    function draftGeneral(Request $request)
    {
        if (!$request->ajax()) abort(404);
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ], [
            'name.required' => 'Bắt buộc phải nhập tiêu đề',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'error' => $validator->errors()->all(),
            ]);
        } else {
            $data = $request->all();

            $data['job_type_name'] = $data['job_type_id'] == null ? null : JobType::find($data['job_type_id'])->name;
            $data['project_name'] = $data['project_id'] == null ? null : Project::find($data['project_id'])->name;
            $dir_name = config('eworkingjob.job_data_draf') . $data['job_id'];
            $filename = $dir_name . '/draft.txt';

            $message_data = [];
            if (file_exists($filename)) { //kiem tra xem file ton tai thi lay ra content
                $message_data = (array)json_decode(file_get_contents($filename));
            }

            $exists_general = 0;
            foreach ($message_data as $data_draft) {
                if (isset($data_draft->general)) { //neu co obj general roi thi luu thay doi
                    $data_draft->general = (object)$data;
                    $exists_general = 1;
                }
            }

            if ($exists_general == 0) {
                $draft['general'] = $data;
//            them moi data
                $this->_base->logsFileText($dir_name, $filename, $data = $draft);
            } else {
//            luu thay doi
                file_put_contents($filename, json_encode($message_data));
            }

            return response()->json([
                'status' => true,
                'data' => $data
            ]);
        }
    }

    public
    function getAdminAddTask($task_current, $task_new)
    {
        $task_current = trim($task_current, '|');
        $task_new = trim($task_new, '|');
        $new = null;
        $currents = null;
        if (!is_null($task_current)) {
            $task_news = explode('|', $task_new);
            foreach ($task_news as $v) {

                if ($v != '' && !in_array($v, explode('|', @$task_current))) {
                    $new[] = $v;
                }
            }
        } else {
            $new = explode('|', @$task_new);
        }

        if (!is_null($task_new)) {
            $task_currents = explode('|', @$task_current);

            foreach ($task_currents as $v) {

                if ($v != '' && !in_array($v, explode('|', @$task_new))) {
                    $currents[] = $v;
                }
            }
        } else {
            $currents = explode('|', @$task_current);
        }
        return ['currents' => $currents, 'news' => $new];
    }


    public
    function getPublish(Request $request)
    {
        try {
            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public
    function delete(Request $request)
    {
//        try {
        $item = $this->model->find($request->id);
        //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
        if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
            CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
            return back();
        }

        $dir_name = config('eworkingjob.job_data_draf') . $item->id;
        $file_name = base_path() . '/' . $dir_name;
        if (is_dir($file_name)) {
            $this->rmdir_files($file_name);
        }
        $item->delete();

        CommonHelper::one_time_message('success', 'Xóa thành công!');
        return back();
//        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            return back();
//        }
    }

    function rmdir_files($dir)
    {
        foreach (glob($dir . '/*') as $file) {
            if (is_dir($file)) delete_files($file); else unlink($file);
        }
        rmdir($dir);
    }

    public
    function multiDelete(Request $request)
    {
//        try {
        $ids = $request->ids;
        if (is_array($ids)) {
            $jobs = $this->model->whereIn('id', $ids)->get();
            if (count($jobs) > 0) {
                foreach ($jobs as $item) {
                    $dir_name = config('eworkingjob.job_data_draf') . $item->id;
                    $file_name = base_path() . '/' . $dir_name;
                    if (is_dir($file_name)) {
                        $this->rmdir_files($file_name);
                    }
                    $item->delete();
                }
            }
        }

        return response()->json([
            'status' => true,
            'msg' => ''
        ]);
//        } catch (\Exception $ex) {
//            return response()->json([
//                'status' => false,
//                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
//            ]);
//        }
    }

//    public function getByIdProject($projet_id)
//    {
//        $data['jobsInProject'] = \Modules\EworkingJob\Models\Job::where('project_id', $projet_id)->paginate(5);
//        $data['project'] = \App\Models\Project::find($projet_id);
//        return view('eworkingproject::form.cong_viec')->with($data);
//    }

}
