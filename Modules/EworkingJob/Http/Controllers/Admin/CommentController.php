<?php
/**
 * Customer Controller
 *
 * Customer Controller manages Customer by admin.
 *
 * @category   Customer
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license
 * @version    1.3
 * @link       http://techvill.net
 * @email      support@techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\EworkingJob\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\{Bill, Group_customer};
use Auth;
use DataTables;
use Illuminate\Http\Request;
use Modules\EworkingAdmin\Models\Admin;
use Modules\EworkingJob\Models\Job;
use Validator;

class CommentController extends Controller
{


    protected $_base;

    public function __construct()
    {
        $this->_base = new BaseController();
    }

    public function test_chat(Request $request)
    {

    }

    public function seenChat(Request $request)
    {
        $type = $request->type . '_has_notifi';
        $bill = Bill::find($request->bill_id);
        if ($bill->{$type} != 0) {
            $bill->{$type} = 0;
            $save = $bill->save();
            if ($save) {
                return response()->json([
                    'status' => true
                ]);
            } else {
                return response()->json([
                    'status' => false
                ]);
            }
        }
    }

    public function listAdminAjax()
    {
        $admins = Admin::select('id', 'name', 'image')->where('company_ids', 'like', '%|' . Auth::guard('admin')->user()->last_company_id . '|%')->get();
        $listAdmins = null;
        foreach ($admins as $ad) {
            $listAdmins[] = json_decode(json_encode([
                'id' => $ad->id,
                'name' => $ad->name,
                'avatar' => CommonHelper::getUrlImageThumb($ad->image, false, false),
                'type' => 'contact'
            ]));
        }
        if ($listAdmins != null && count($listAdmins) > 0) {
            return response()->json([
                'json_admin' => $listAdmins
            ]);

//            file_put_contents('public/backend/admins.json', json_encode($listAdmins));
//            $admin = file_get_contents('public/backend/admins.json');
        }
        return false;
    }

    function logChat($channel, $data)
    {
        $dir_name = config('eworkingjob.job_data_draf') . $channel;
        $filename = $dir_name . '/comment.txt';
        $this->_base->logsFileText($dir_name, $filename, $data);
        return response()->json([
            'status' => true,
            'msg' => ''
        ]);
    }

    function tagAdmin($listAdmins, $channel)
    {
        if (count($listAdmins) > 0) {
            $job = Job::find($channel);
            //     When prompted, the name will
            $content = is_numeric($job->project_id) ? 'thuộc dự án ' . $job->project->name : $job->name;
            foreach ($listAdmins as $admin) {
                $dataNotification = [
                    'name' => 'Bình luận',
                    'content' => Auth::guard('admin')->user()->name . ' nhắc đến bạn trong một bình luận trong công việc ' . $content,
                    'link' => route('job.edit', ['id' => $channel]),
                    'readed' => 0,
                    'type' => 3,
                    'from_admin_id' => Auth::guard('admin')->user()->id,
                    'to_admin_id' => $admin,
                    'job_id' => $channel,
                    'company_id' => Auth::guard('admin')->user()->last_company_id,
                ];
                try {
                    \Eventy::action('notification.add', $dataNotification);
                } catch (\Exception $ex) {

                }
            }
        }
    }

    public function commentJob(Request $request)
    {
//        dd($request->all());
        $channel = $request->channel;
        $dir_name = config('eworkingjob.job_data_draf') . $channel;
        $filename = $dir_name . '/comment.txt';
        $message_data = [];
        if (file_exists($filename)) {
            $message_data = (array)json_decode(file_get_contents($filename));
        }
        $data['id'] = count($message_data);

        $data['time'] = date("H:i:s d-m-Y");
        $data['name'] = Auth::guard('admin')->user()->name;
        $data['avt'] = Auth::guard('admin')->user()->image;
        $data['avt_for_realtime'] = CommonHelper::getUrlImageThumb(Auth::guard('admin')->user()->image, 100, 100);
        $data['user_id'] = Auth::guard('admin')->user()->id;
        $admins = Admin::pluck('name', 'id')->toArray();
        $newString = '[content]:' . $request->message; //chat content for comment
        $replaceTrue = 0;
        $listIdTrue = '';
        $contentChat = '[content]:' . $request->message; //  chat content for notifications
        foreach ($admins as $id_ad => $Admin) {
            if (strpos($newString, '@|' . $id_ad . '|') != false) {
                $replaceTrue++;
                $listIdTrue .= ($listIdTrue == '') ? $id_ad : '|' . $id_ad; //list id exist in string
                $newString = str_replace('@|' . $id_ad . '|',
                    '<a class="name-tag" title="' . $Admin . '" target="_blank" href="' . route('admin.profile_admin', ['id' => $id_ad]) . '">' . $Admin . '</a>', $newString);
                $contentChat = str_replace('@|' . $id_ad . '|', $Admin, $contentChat);
            }
        }
//        loại bo '[content]:'
        $newString = str_replace('[content]:', '', $newString);
        $contentChat = str_replace('[content]:', '', $contentChat);
        $listAdmins = explode('|', $listIdTrue); //lay ra danh sach admin can gui tin nhan

        $this->tagAdmin($listAdmins, $channel);
        $data['message'] = $newString;
        $data['content'] = $contentChat;
        $data['image'] = $request->image;
        $data['tag'] = $listIdTrue;
        //message
        if ($request->has('image') && $data['image'] == null && $request->has('message') && $data['message'] == null) {
            return response()->json([
                'status' => false,
                'msg' => 'fail'
            ]);
        } else {

            $this->chatLive($data, $channel);
            $this->logChat($channel, $data);
            return response()->json([
                'status' => true,
            ]);
        }
    }


    public function saveComment($id_job, $message_data)
    {
        $dir_name = config('eworkingjob.job_data_draf') . $id_job;
        $filename = $dir_name . '/comment.txt';
        file_put_contents($filename, json_encode($message_data));
    }

    public function getEditComment(Request $request)
    {
//        type = 1:  xóa ảnh ở sủa comment
//        type = 0:  Sửa
//        dd($request->all());
        $data = $this->getKeyComment($request);
        if (is_null($data['master_key'])) {
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại data'
            ]);
        }
        $replaceTrue = 0;
        $listIdTrue = '';
        $newString = '[content]:' . $request->msg_new; //chat content for comment
        $contentChat = '[content]:' . $request->message; //  chat content for notifications
        $admins = Admin::select('name', 'id')->get();
        $id_replace[] = null;
        foreach ($admins as $Admin) {
            $id_ad = $Admin->id;
            $Admin = $Admin->name;
            if (strpos($newString, '@|' . $id_ad . '|') != false) {
                $replaceTrue++;
                $listIdTrue .= ($listIdTrue == '') ? $id_ad : '|' . $id_ad; //list id exist in string
                $newString = str_replace('@|' . $id_ad . '|',
                    '<a class="name-tag" title="' . $Admin . '" target="_blank" href="' . route('admin.profile_admin', ['id' => $id_ad]) . '">' . $Admin . '</a>', $newString);
                $contentChat = str_replace('@|' . $id_ad . '|', $Admin, $contentChat);
            }

            if (!in_array($id_ad, $id_replace) && strpos($newString, $Admin) && !in_array($id_ad, explode('|', $listIdTrue))) {
                $newString = str_replace($Admin,
                    '<a class="name-tag" title="' . $Admin . '" target="_blank" href="' . route('admin.profile_admin', ['id' => $id_ad]) . '">' . $Admin . '</a>', $newString);
                $id_replace[] = $id_ad;
            }
        }
        $listAdmins = explode('|', $listIdTrue); //lay ra danh sach admin can gui tin nhan
        $this->tagAdmin($listAdmins, $request->id);
//        loại bo '[content]:'
        $newString = str_replace('[content]:', '', $newString);
//            sua comment
        $data['message_data'][$data['master_key']]->message = $newString;
//        $data['message_data'][$data['master_key']]->message = 1123;
        ksort($data['message_data']);
        $this->saveComment($request->id, $data['message_data']);
        CommonHelper::one_time_message('success', 'Sửa bình luận thành công');
        return response()->json([
            'status' => true
        ]);
    }

    function getKeyComment($request)
    {

        $dir_name = config('eworkingjob.job_data_draf') . $request->id;
        $filename = $dir_name . '/comment.txt';
        $message_data = [];

        if (file_exists($filename)) {
            $message_data = (array)json_decode(file_get_contents($filename));
        }

//        dd($request->all());
        $master_key = null;
        foreach ($message_data as $key => $data) {
            if ($data->id == $request->id_comment) {
                $master_key = $key;
            }
        }
        return $data = [
            'master_key' => $master_key,
            'message_data' => $message_data,
        ];
    }

    public function getDeleteComment(Request $request)
    {
        $data = $this->getKeyComment($request);
        if (is_null($data['master_key'])) {
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại data'
            ]);
        }
        unset($data['message_data'][$data['master_key']]);
        ksort($data['message_data']);
        $this->saveComment($request->id, $data['message_data']);
        return response()->json([
            'status' => true
        ]);
    }


    public function chatLive($data, $channel)
    {
        \Pusher::trigger($channel, 'chat', $data);
    }
}
