<?php

namespace Modules\EworkingJob\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\EworkingCompany\Models\Company;
use Modules\EworkingJob\Models\Job;
use Modules\EworkingJob\Models\Task;
use Modules\EworkingProject\Models\Project;

class TaskController extends Controller
{

    protected $module = [
        'code' => 'task',
        'table_name' => 'tasks',
        'label' => 'Nhiệm vụ',
        'modal' => 'Modules\EworkingJob\Models\Task',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'job_id' => [
            'query_type' => '='
        ],
        'subject_id' => [
            'query_type' => '='
        ],
        'company_id' => [
            'query_type' => '='
        ],
    ];

    public function index(Request $request)
    {
        try {
            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'job_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }
            if ($request->has('project_id')) {
                $jobs = Job::where('project_id',$request->project_id)->get();
                foreach ($jobs as $item){
                    $item->company;
                    $query = Task::where('job_id', $item->id)->whereIn('task_type', [1, 2, 3]);
                    $item->end_date = $query->max('end_date');
                    $item->start_date = $query->min('start_date');
                    $item->project = [
                        'id' => $item->project->id,
                        'name' => $item->project->name
                    ];
                }
                return response()->json([
                    'status' => true,
                    'msg' => '',
                    'errors' => (object)[],
                    'data' => [
                        'afterDeadlines' => [],
                        'beforeDeadlines' => $jobs
                    ],
                    'code' => 201
                ]);
            }
            //  Nếu truy vấn lấy các nhiệm vụ deadline
            if ($request->has('deadline') && !$request->has('admin_id')) {
                if ($request->has('company')) {
                    $company = [\Auth::guard('api')->user()->last_company_id];
                } else {
                    $company_ids = trim(\Auth::guard('api')->user()->company_ids, '|');
                    $company_id_arr = explode('|', $company_ids);
                    $company = array_unique($company_id_arr);
                }

                $data_deadline = \Modules\EworkingJob\Http\Helpers\EworkingJobHelper::deadline($company,\Auth::guard('api')->user()->id);
                return response()->json([
                    'status' => true,
                    'msg' => '',
                    'errors' => (object)[],
                    'data' => [
                        'afterDeadlines' => $data_deadline['data']['afterDeadlines'],
                        'beforeDeadlines' => $data_deadline['data']['beforeDeadlines']
                    ],
                    'code' => 201
                ]);
            }

            //  Filter
            $where = $this->filterSimple($request);

            $listItem = Task::leftJoin('subjects', 'subjects.id', '=', 'tasks.subject_id')
                ->leftJoin('jobs', 'jobs.id', '=', 'tasks.job_id')
                ->selectRaw('tasks.*, 
                    subjects.id as subject_id, subjects.name as subject_name,
                    jobs.id as job_id, jobs.name as job_name')
                ->whereRaw($where);

            if ($request->has('admin_id')) {
                $listItem = $listItem->where('admin_ids', 'like', '%|' . $request->admin_id . '|%');
            }

            //  Sort
            $listItem = $this->sort($request, $listItem);
            $listItem = $listItem->paginate(20)->appends($request->all());
            foreach ($listItem as $item) {
                //  Lấy ra bộ môn
                $item->subject = [
                    'id' => $item->subject_id,
                    'name' => $item->subject_name
                ];
                unset($item->subject_id);
                unset($item->subject_name);

                //  Lấy ra công việc
                $item->job = [
                    'id' => $item->job_id,
                    'name' => $item->job_name
                ];
                $item->job_id;
                unset($item->job_name);
                $item->status = $item->status ? 0 : 1;

                //  Lấy ra người làm
                $admin_ids = explode('|', $item->admin_ids);
                $item->admins = \App\Models\Admin::select('id', 'name')->whereIn('id', $admin_ids)->get();
                unset($item->admin_ids);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {
            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'job_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }

            $item = Task::leftJoin('companies', 'companies.id', '=', 'tasks.company_id')->leftJoin('jobs', 'jobs.id', '=', 'tasks.job_id')
                ->selectRaw('tasks.id, tasks.name, tasks.end_date, companies.short_name as companies_short_name, jobs.name as jobs_name')
                ->where('tasks.job_id', $id)->orderBy('end_date', 'desc')->get();
            foreach ($item as $k => $v) {
                $v->company = [
                    'name' => $v->companies_short_name
                ];
                $v->job = [
                    'name' => $v->jobs_name,
//                    'end_date' => $item->first()->end_date
                ];
                unset($v->companies_short_name);
                unset($v->jobs_name);
            }
            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . $this->module['table_name'] . '.id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }


    public function finish(Request $request, $id)
    {
        $item = Task::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }
        $item->status = $item->status ? 0 : 1;
        if ($item->save()) {
            return response()->json([
                'status' => true,
                'msg' => $item->status ? 'Đang làm' : 'Hoàn thành',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        }
        return response()->json([
            'status' => false,
            'msg' => '',
            'errors' => (object)[],
            'data' => $item,
            'code' => 401
        ]);

    }
}
