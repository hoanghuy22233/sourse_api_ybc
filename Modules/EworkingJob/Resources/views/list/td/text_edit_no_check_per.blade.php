<a href="/admin/{{ $module['code'] }}/{{ $item->id }}{{ isset($_GET['subject_id']) ? '?subject_id=' . $_GET['subject_id'] : '' }}"
   style="    font-size: 14px!important;"
   class="{{ isset($field['tooltip_info']) ? 'a-tooltip_info' : '' }}">{!! $item->{$field['name']} !!}</a>
<div class="row-actions" style="    font-size: 13px;">
    <span class="edit" title="ID của bản ghi">ID: {{ @$item->id }} | </span>
    <span class="edit"><a
                href="{{ url('/admin/'.$module['code'].'/' . $item->id) }}{{ isset($_GET['subject_id']) ? '?subject_id=' . $_GET['subject_id'] : '' }}"
                title="Sửa bản ghi này">Sửa</a> | </span>
    <span class="trash"><a class="delete-warning"
                           href="{{ url('/admin/'.$module['code'].'/delete/' . $item->id) }}{{ isset($_GET['subject_id']) ? '?subject_id=' . $_GET['subject_id'] : '' }}"
                           title="Xóa bản ghi">Xóa</a> | </span>
</div>
