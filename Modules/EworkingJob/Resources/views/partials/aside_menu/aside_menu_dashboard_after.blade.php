<li class="kt-menu__item kt-menu__item--submenu kt-menu__item--open" aria-haspopup="true"
    data-ktmenu-submenu-toggle="hover"><a
            href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i class="kt-menu__link-icon flaticon-folder-1"></i>
                    </span><span class="kt-menu__link-text">Công việc</span><i
                class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu " kt-hidden-height="80"><span
                class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            @if(in_array('super_admin', $permissions))
                @if(in_array('subject_warehouse_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/subject_warehouse" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Kho bộ môn</span></a></li>
                @endif
            @else
                @if(in_array('project_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/project" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Dự án</span></a></li>
                @endif
                @if(in_array('job_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/job" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Công việc</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/job_type" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Loại công việc</span></a></li>
                @endif
                @if(in_array('subject_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/subject" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Bộ môn</span></a></li>
                @endif
            @endif
        </ul>
    </div>
</li>