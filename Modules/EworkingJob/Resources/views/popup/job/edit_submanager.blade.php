<?php
//$arr = \Modules\EworkingAdmin\Models\Admin::where('company_ids', 'like', '%|' . Auth::guard('admin')->user()->last_company_id . '|%')->where('status', 1)->pluck('name', 'id')->toArray();
$arr = \App\Models\RoleAdmin::where('company_id', Auth::guard('admin')->user()->last_company_id)->where('status', 1)->get();
?>
<div class="modal fade" id="popup-edit-submanager" role="dialog">
    <form id="form-edit-submanager" action="post" autocomplete="off">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content " style="height:auto">
                <div class="modal-header">
                    <h4 style="display:inline-block;">Thông tin phụ trách chung</h4>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-content">
                    <div class="row" style="margin: 15px">
                        <div class="col-md-12">
                            <input type="number" name="status" class="hidden">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="usr">Ngày bắt đầu</label>
                                        <div class='input-group date' id='start_date'>
                                            <input type='text' name="start_date" class="form-control datetimepicker"/>
                                            <span class="input-group-addon select-datetimepicker">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="usr">Ngày hết hạn</label>
                                        <div class='input-group date' id='end_date'>
                                            <input type='text' name="end_date" class="form-control datetimepicker"/>
                                            <span class="input-group-addon select-datetimepicker">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="usr">Người phụ trách</label>
                                        <select style="width: 100%;" class="form-control admin_ids select2-admin_ids"
                                                id="admin_ids"
                                                name="admin_ids" multiple>
                                            @foreach ($arr as $value)
                                                <option value='{{ @$value->admin_id }}'>{{ @$value->admin->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit">
                        <i class="icon-header fa fa-check-circle"></i>
                        Hoàn thành
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {

        $('body').on('click', '.success-submanager', function () {
            let value = $('.value-submanager');
            // alert(value.data('success'))
            loading();
            event.preventDefault();
            $.ajax({
                url: value.data('success'),
                type: 'POST',
                data: {},
                success: function (res) {
                    if (res.status) {
                        window.location.reload();
                    }
                }
            });
            stopLoading();
            modal.modal();
        });

        $('body').on('click', '.edit-submanager', function () {
            var modal = $('#popup-edit-submanager');
            loading();
            let value = $('.value-submanager');
            modal.find("input[name='start_date']").val(value.data('start_date'));
            modal.find("input[name='end_date']").val(value.data('end_date'));
            modal.find("input[name='status']").val(value.data('status'));
            modal.find("input[name='start_date']").datepicker({format: "dd-mm-yyyy",});
            modal.find("input[name='end_date']").datepicker({format: "dd-mm-yyyy",});
            modal.find(".admin_ids option").each(function () {
                console.log(value.data('admin_ids') + '|' + $(this).attr('value'));
                if (value.data('admin_ids').indexOf($(this).attr('value')) != -1) {
                    $(this).attr('selected', '');
                }
            });
            $('.admin_ids').select2();
            stopLoading();
            modal.modal();
        });

        $('#form-edit-submanager').validate({
            onfocusout: false,
            onkeyup: false,
            onclick: false,
            rules: {
                start_date: {
                    required: true,
                },
                end_date: {
                    required: true,
                },
                admin_ids: {
                    required: true,
                }
            },
            messages: {
                end_date: {
                    required: "<p style='color: red'>Ngày bắt đầu không được để trống.</p>",
                },
                start_date: {
                    required: "<p style='color: red'>Ngày kết thúc không được để trống.</p>",
                },
                admin_ids: {
                    required: "<p style='color: red'>Người phụ trách không được để trống</p>",
                },
            },
            submitHandler: function (form) {
                //code in her
                var modal = $('#popup-edit-submanager');
                // alert(form.find("input[name='start_date']").val() + '|' + form.find("input[name='end_date']").val() + '|' + form.find(".admin_ids").val());
                // console.log(form.find("input[name='start_date']").val() + '|' + form.find("input[name='end_date']").val() + '|' + form.find(".admin_ids").val());
                event.preventDefault();
                $.ajax({
                    url: '/admin/job/edit-single',
                    type: 'POST',
                    data: {
                        job_id: '{{$result->id}}',
                        start_date: modal.find("input[name='start_date']").val(),
                        end_date: modal.find("input[name='end_date']").val(),
                        admin_ids: modal.find(".admin_ids").val(),
                        status: modal.find("input[name='status']").val(),
                    },
                    success: function (res) {
                        if (res.status) {
                            window.location.reload();
                        }
                    }
                });
                return false;
            }
        });
    });
</script>
