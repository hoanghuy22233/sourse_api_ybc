<?php
$admin_ids = explode('|', \Modules\EworkingJob\Http\Helpers\EworkingJobHelper::getAdminInJob($item->id));
$admins = \App\Models\Admin::select(['id', 'name', 'image'])->whereIn('id', $admin_ids)->get();
?>
@if(count($admins) > 0)
    <div class="kt-widget__details">
        <div class="kt-section__content" style="padding: 0 0 0 25px;">
            <div class="kt-media-group">
                <?php
                $countArray = (count($admins) >= 5) ? 5 : count($admins);
                ?>
                @for($i = 0; $i < $countArray; $i ++)
                    <?php
                    $admin = $admins[$i];
                    ?>
                    <a href="{{URL::to('/admin/profile/'.$admin->id)}}"
                       class="kt-media kt-media--sm kt-media--circle"
                       data-toggle="kt-tooltip" data-skin="brand"
                       data-placement="top"
{{--                       title=""--}}
                    >
{{--                       data-original-title="{{ $admin['name'] }}">--}}
                        <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($admin['image'], 30, 30) }}"
                             alt="{{ $admin['name'] }}" title="{{ $admin['name'] }}">
                    </a>
                @endfor
                @if(count($admins) > 5)
                    <a href="#"
                       class="kt-media kt-media--sm kt-media--circle"
                       data-toggle="kt-tooltip" data-skin="brand"
                       data-placement="top" title=""
                       data-original-title="Micheal York">
                        <span>{{count($admins) - 5}}</span>
                    </a>
                @endif
            </div>
        </div>
    </div>
@endif
<style>
    .tooltip-arrow p, .admin-task {
        color: #00a0d2;
    }

    .p-other {
        cursor: pointer;
    }

    .tooltip-arrow {
        width: 180px;
        /*max-width: 180px;*/
        height: auto;
        padding: 5px;
        border-radius: 10px 0px 10px 10px;
        border: 1px solid green;
        background: #ffffff;
        display: none;
    }
</style>
