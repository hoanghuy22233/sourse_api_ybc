<?php
$query = \Modules\EworkingJob\Models\Task::select('end_date')->whereIn('task_type', [1, 2, 3])->where('job_id', $item->id)->orderBy('end_date', 'desc');
?>
@if($query->exists())
    {{$query->first()->end_date != null ? date('d/m/Y',strtotime($query->first()->end_date)) : ''}}
@endif
