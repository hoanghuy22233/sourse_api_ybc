<?php
$id = $result->id;
$admins = \App\Models\Admin::select('name', 'id')->get();
?>
<style>
    .kt-todo__username.name-comment, .name-tag {
        font-weight: bold !important;
        cursor: pointer;
    }

    .kt-todo__username.name-comment {
        color: #2c77f4 !important;
    }

    .box-save {
        height: 50px;
    }

    .comment-controls .btn {
        cursor: pointer;
    }

    .comment-controls {
        position: absolute;
        bottom: 55px;
        left: 12px;
        transform: translateY(48px);
        transition-duration: 85ms;
        transition-timing-function: ease;
        transition-property: opacity, transform;
    }

    .comment-box-input:focus {
        outline: none;
    }

    .comment-box-input {
        height: 70px;
        border: none;
        background: #fff !important;
        box-sizing: content-box;
        box-shadow: none;
        margin: 0;
        min-height: 20px;
        padding: 0;
        width: 100%;
        resize: none;
        overflow-wrap: break-word;
        overflow: hidden;
    }

    .comment-box {
        padding: 8px 12px;
        position: relative;
        transition-duration: 85ms;
        transition-timing-function: ease;
        transition-property: padding-bottom;
    }

    .comment-frame {
        width: 100%;
        box-shadow: 0 4px 8px -2px rgba(9, 30, 66, .25), 0 0 0 1px rgba(9, 30, 66, .08);
        background-color: #fff;
        border-radius: 3px;
        margin: 4px 4px 12px 0;
        overflow: hidden;
        position: relative;
        transition: box-shadow 85ms ease;
    }

    .comment-job {
        padding: 15px;
        width: 100%;
        border-bottom: 1px solid #ddd;
    }

    .comment-job .reply span {
        text-decoration: underline;
        cursor: pointer;
    }

    .file_image_cmt .img-item .action {
        padding: 5px;
    }
</style>
<style>
    .mentions-input-box {
        position: relative;
        background: #fff;
    }

    .mentions-input-box textarea {
        width: 100%;
        display: block;
        height: 18px;
        padding: 9px;
        border: 1px solid #dcdcdc;
        border-radius: 3px;
        overflow: hidden;
        background: transparent;
        position: relative;
        outline: 0;
        resize: none;

        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .mentions-input-box .mentions-autocomplete-list {
        display: none;
        background: #fff;
        border: 1px solid #b2b2b2;
        position: absolute;
        left: 0;
        right: 0;
        z-index: 10000;
        margin-top: -2px;

        border-radius: 5px;
        border-top-right-radius: 0;
        border-top-left-radius: 0;

        -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.148438);
        -moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.148438);
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.148438);
    }

    .mentions-input-box .mentions-autocomplete-list ul {
        margin: 0;
        padding: 0;
    }

    .mentions-input-box .mentions-autocomplete-list li {
        background-color: #fff;
        padding: 0 5px;
        margin: 0;
        width: auto;
        border-bottom: 1px solid #eee;
        height: 26px;
        line-height: 26px;
        overflow: hidden;
        cursor: pointer;
        list-style: none;
        white-space: nowrap;
    }

    .mentions-input-box .mentions-autocomplete-list li:last-child {
        border-radius: 5px;
    }

    .mentions-input-box .mentions-autocomplete-list li > img,
    .mentions-input-box .mentions-autocomplete-list li > div.icon {
        width: 16px;
        height: 16px;
        float: left;
        margin-top: 5px;
        margin-right: 5px;
        -moz-background-origin: 3px;

        border-radius: 3px;
    }

    .mentions-input-box .mentions-autocomplete-list li em {
        font-weight: bold;
        font-style: none;
    }

    .mentions-input-box .mentions-autocomplete-list li:hover,
    .mentions-input-box .mentions-autocomplete-list li.active {
        background-color: #f2f2f2;
    }

    .mentions-input-box .mentions-autocomplete-list li b {
        background: #ffff99;
        font-weight: normal;
    }

    .mentions-input-box .mentions {
        position: absolute;
        left: 1px;
        right: 0;
        top: 1px;
        bottom: 0;
        padding: 9px;
        color: #fff;
        overflow: hidden;

        white-space: pre-wrap;
        word-wrap: break-word;
    }

    .mentions-input-box .mentions > div {
        color: #fff;
        white-space: pre-wrap;
        width: 100%;
    }

    .mentions-input-box .mentions > div > strong {
        font-weight: normal;
        background: #d8dfea;
    }

    .mentions-input-box .mentions > div > strong > span {
        filter: progid:DXImageTransform.Microsoft.Alpha(opacity=0);
    }

    .comment-job {
        padding: 15px;
        width: 100%;
        border-bottom: 1px solid #ddd;
    }

    .kt-todo__text.comment-content {
        color: #000000 !important;
    }

    .header-content-chat {
        position: relative;
        display: flex;
    }

    .right-cmt {
        display: flex;
        flex-direction: column;
        padding-left: 5px;
    }

    .header-content-chat .kt-widget2__actions {
        position: absolute;
        top: 0;
        right: 0;
    }
</style>

<div class="row">
    <div class="kt-grid kt-grid--desktop kt-grid--ver-desktop  kt-todo col-md-12" id="kt_todo">

        <!--Begin:: Tasks Content-->
        <div class="kt-grid__item kt-grid__item--fluid kt-todo__content" id="kt_todo_content">


            <div class="kt-todo__tasks-bottom">
                <div class="row">
                    {{--comment--}}
                    <div class="col-md-12">
                        <!--Begin:: Inbox View-->
                        <div class="kt-grid__item kt-grid__item--fluid  kt-portlet kt-portlet--height-fluid kt-todo__view"
                             style="box-shadow:none" id="kt_todo_view">
                            <!--Begin:: Portlet Body-->
                            <div class="kt-portlet__body kt-portlet__body--fit-y">
                                <!--Begin:: Wrapper-->
                                <div style="padding-bottom: 0;" class="kt-todo__wrapper">
                                    <!--Begin:: Body-->
                                    <div style="padding-bottom: 0;" class="kt-todo__body">
                                        <div class="kt-todo__comments list-comment-job">
                                            <?php
                                            $dir_name = config('eworkingjob.job_data_draf') . $id;
                                            $filename = $dir_name . '/comment.txt';
                                            $message_data = [];
                                            if (file_exists($filename)) {
                                                $message_data = (array)json_decode(file_get_contents($filename));
                                            }
                                            //                                            dd($message_data);
                                            ?>
                                            @if(count($message_data) > 0 && $message_data != null)
                                                @foreach($message_data as $key=>$value)
                                                    <?php
                                                    $user_comment = \App\Models\Admin::find($value->user_id);
                                                    $image = '';
                                                    $content = '';
                                                    if ($value->message != null && $value->message != '') {
                                                        $content = $value->message;
                                                    }
                                                    ?>
                                                    <div style="padding-top: 5px;"
                                                         class="kt-todo__comment comment-job cmt-{{$value->id}}">
                                                        <div class="kt-todo__box header-content-chat">
                                                            <span title="{{$user_comment->name}}"
                                                                  class="kt-media kt-media--sm" data-toggle="expand"
                                                                  style="background-image: url('{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($user_comment->image,100,100)}}');background-size: contain;">
                                                                <span></span>
                                                            </span>
                                                            <div class="right-cmt">
                                                                <a href="{{route('admin.profile',['id'=>$user_comment->id])}}"
                                                                   title="{{$user_comment->name}}"
                                                                   class="kt-todo__username name-comment">
                                                                    {{$user_comment->name}}
                                                                </a>
                                                                <span class="kt-todo__date" style="font-style: italic">
                                                            {{$value->time}}
                                                            </span>
                                                            </div>
                                                            {{--                                                            {{Auth::guard('admin')->user()->id == $value->user_id}}--}}
                                                            @if(Auth::guard('admin')->user()->id == $value->user_id)
                                                                <div class="kt-widget2__actions">
                                                                    <a href="#"
                                                                       class="btn btn-clean btn-sm btn-icon btn-icon-md"
                                                                       data-toggle="dropdown" aria-expanded="false">
                                                                        <i class="flaticon-more-1"></i>
                                                                    </a>
                                                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right"
                                                                         x-placement="bottom-end"
                                                                         style="position: absolute; transform: translate3d(538px, 34px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                        <ul class="kt-nav">


                                                                            <li class="kt-nav__item">
                                                                                <a data-id="{{$value->id}}"
                                                                                   data-tag="{{@$value->tag}}"
                                                                                   data-content="{{$content}}"
                                                                                   class="kt-nav__link edit-comment">
                                                                                    <i class="kt-nav__link-icon flaticon2-edit"></i>
                                                                                    <span class="kt-nav__link-text">Sửa bình luận</span>
                                                                                </a>
                                                                            </li>
                                                                            <li class="kt-nav__item">
                                                                                <a data-id="{{$value->id}}"

                                                                                   class="kt-nav__link delete-comment">
                                                                                    <i class="kt-nav__link-icon la la-close"></i>
                                                                                    <span class="kt-nav__link-text">Xóa bình luận</span>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <span class="kt-todo__text comment-content">
                                                                {!! $content !!}
                                                         </span>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="text-center">Chưa có bình luận nào về công việc</div>
                                            @endif
                                        </div>
                                    </div>
                                    <!--End:: Body-->
                                </div>
                                <!--End:: Wrapper-->
                            </div>
                            <!--End:: Portlet Body-->
                        </div>
                        <!--End:: Inbox View-->
                    </div>
                    {{--endcomment--}}
                </div>
            </div>
        </div>
        <!--End:: Tasks Content-->
    </div>
    <div class="col-md-12 col-xs-12">
        <div style="display: flex">
            {{--<img style="margin-right: 14px;" height="35px" width="35px"--}}
            {{--src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(Auth::guard('admin')->user()->image,null, null) }}"--}}
            {{--class="img-circle" alt="">--}}
            <form id="form-chat">
                <div class="comment-frame">
                    <div class="comment-box mentions">
                                <textarea cols="30" rows="5" class="comment-box-input mention js-new-comment-input"
                                          placeholder="Nhập bình luận"></textarea>
                        <textarea id="comment-box-input" class="hidden"></textarea>
                        <input id="file-comment" multiple type="file" name="file" onchange="changeImg(this)"
                               class="hidden file_upload">
                        <p id="msg"></p>
                        <div class="box-save">
                            <div class="comment-controls u-clearfix">
                                <span class="btn btn-brand btn-sm">Bình luận</span>
                            </div>
                            {{--<div class="comment-box-options">--}}
                            {{--<p class="text-primary" id="name-file"></p>--}}
                            {{--<a class="comment-box-options-item js-comment-add-attachment" style="cursor: pointer"--}}
                            {{--title="Add an attachment…">--}}
                            {{--<i class="fa fa-paperclip" aria-hidden="true"></i>--}}
                            {{--</a>--}}
                            {{--<a class="comment-box-options-item btn btn-default btn-sm js-comment-mention-member"--}}
                            {{--title="Mention a member…">--}}
                            {{--@--}}
                            {{--</a>--}}

                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="dialog-edit-comment" role="dialog" style="z-index:10060;">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content " style="height:auto">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-content">
                <div class="row" style="margin: 15px">
                    <h5 class="text-bold text-name"></h5>
                </div>
                <div class="row" style="margin: 15px">
                    <label for="">Bình luận</label>
                    <div class="col-md-12 mentions-input-box">
                        {{--<div class="mentions">--}}
                        <textarea class="form-control text-msg" name="" id="" cols="30" rows="4"></textarea>
                        <textarea style="display:none;" class="content-edit-comment" name="" id="" cols="30" rows="4"></textarea>
                        <input  style="display:none;"type="text" name="tag-comment">
                        {{--</div>--}}
                    </div>
                    {{--<div class="col-md-12">--}}
                    {{--<div class="form-group-div form-group {{ @$field['group_class'] }}"--}}
                    {{--id="form-group-image-comment">--}}
                    {{--<label class="control-label">Ảnh</label>--}}
                    {{--<div class="col-sm-12 file_image_cmt">--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-12" style="margin-top: 15px;">--}}
                    {{--<span class="btn btn-success">Thêm ảnh</span>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success" id="save-comment">Lưu bình luận</a>
                <button class="btn btn-danger" data-dismiss="modal">Hủy</button>
            </div>
        </div>
    </div>
</div>
@include('eworkingjob::form.comment_script',['id_job'=>$id])
