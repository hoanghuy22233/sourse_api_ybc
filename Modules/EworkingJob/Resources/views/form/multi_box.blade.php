@section('main')
    <?php
    //dd()
    ?>
    <form class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid {{ @$module['code'] }}"
          action="{{ @$action }}{{(isset($_GET['subject_id'])) ? '?subject_id='.$_GET['subject_id'] : ''}}" method="POST"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                     id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg" style="">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Thêm {{ $module['label'] }}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="/admin/{{ $module['code'] }}" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Quay lại</span>
                            </a>
                            <div class="btn-group">
                                @if(in_array($module['code'].'_add', $permissions))
                                    <button type="submit" class="btn btn-brand">
                                        <i class="la la-check"></i>
                                        <span class="kt-hidden-mobile">Lưu</span>
                                    </button>
                                    <button type="button"
                                            class="btn btn-brand dropdown-toggle dropdown-toggle-split"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-reload"></i>
                                                    <input class="kt-nav__link-text" value="Lưu và tiếp tục"
                                                           type="submit"
                                                           style="border: 0;background: none;padding: 0;text-align: left;">
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-power"></i>
                                                    <input class="kt-nav__link-text" value="Lưu & Thoát" type="submit"
                                                           style="border: 0;background: none;padding: 0;text-align: left;">
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-add-1"></i>
                                                    <input class="kt-nav__link-text" value="Lưu và tạo mới"
                                                           type="submit"
                                                           style="border: 0;background: none;padding: 0;text-align: left;">
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

        <div class="row">
            @foreach($module['form'] as $key => $tab)
                <div class="{{ @$tab['class'] }}">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    {!!  str_replace('{id}',@$result->id,@$tab['label'] ) !!}
                                </h3>
                            </div>
                        </div>
                        <!--begin::Form-->
                        <div class="kt-form">
                            <div class="kt-portlet__body">
                                <div class="kt-section kt-section--first">
                                    @foreach($tab['td'] as $field)
                                        @include("admin.common.td.".$field['type'], ['field' => $field])
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            @endforeach
        </div>
    </form>
    @if(isset($module['form']['footer']))
        @foreach($module['form']['footer'] as $include)
            @include($include)
        @endforeach
    @endif
@endsection
