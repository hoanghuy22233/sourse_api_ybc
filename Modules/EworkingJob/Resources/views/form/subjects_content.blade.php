@if (isset($result))
    <script>
        changeValue = function (obj) {
            let count_change = 0;
            obj.parents('tr').find('.change-update').each(function () {
                if ($(this).val() != $(this).data('value')) {
                    count_change++;
                }
            });
            let btn_update = obj.parents('tr').find('.btn-update-product');
            alert(count_change);
            // if (count_change > 0) {
            //     btn_update.addClass('button-flicker');
            // } else {
            //     btn_update.removeClass('button-flicker');
            // }
        }
        $(document).ready(function () {
            let rs_subject = $('#result-subject').parents('.kt-portlet__body');
            KTApp.block(rs_subject, {
                overlayColor: "#ff19600",
                type: "v2",
                state: "success",
                size: "lg"
            });

            //hien popup sua nhiem vu
            $.ajax({
                url: '/admin/subject/get-by-id/' + '{{$result->id}}',
                type: 'GET',
                success: function (res) {
                    rs_subject.html(res);
                }, error: function () {
                    alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                }
            });
            $('body').on('click', '.pagination-subjects ul li', function (event) {
                event.preventDefault();
                let url = $(this).children('a').attr('href');
                if (url != undefined) {
                    $.ajax({
                        url: url,
                        type: 'GET',
                        beforeSend: function () {
                            // Handle the beforeSend event
                            KTApp.block(rs_subject, {
                                overlayColor: "#000",
                                type: "v1",
                                state: "success",
                                size: "lg"
                            });
                        },
                        success: function (res) {
                            rs_subject.html(res);
                        }, error: function () {
                            alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                        }
                    });
                }
            });
        });
    </script>
    <div id="result-subject" style="height: 150px"></div>
@else
    <style>
        .kt-widget11 .table tbody > tr > td {
            padding: 0.75rem;
        }
    </style>

    @foreach($subjects as $subject)
        @if($delete_subject == null || !in_array($subject->id,$delete_subject))
            @include('eworkingjob::subject.collapse_subject',['type_edit'=>'-subject'])
        @endif
    @endforeach

    <?php
    $dir_name = config('eworkingjob.job_data_draf'). $job_id;
    $filename = $dir_name . '/draft.txt';
    $message_data = [];
    if (file_exists($filename)) { //kiem tra xem file ton tai thi lay ra content
        $message_data = (array)json_decode(file_get_contents($filename));
    }

    $status_draft = false;
    foreach ($message_data as $data_draft) {
        if (isset($data_draft->add_subject)) {
            $subjects_draft = $data_draft->add_subject;
            $status_draft = true;
        }
    }
    ?>

    @if($status_draft)
        @foreach($subjects_draft as $subject)
            @include('eworkingjob::subject.collapse_subject_draft',['main'=>'main-','type_edit'=>'-subject','subject_id'=>$subject->id])
        @endforeach
    @endif

    {{$subjects->render(config('core.admin_theme').'.partials.paginate',['class'=>'subjects'])}}
@endif
