@if (isset($result))
    <script>
        //hien popup sua nhiem vu
        $(document).ready(function () {
            //hien popup sua nhiem vu
            getTask = function (obj) {
                let id = obj.data('id');
                let modal = $('#popup-add-task');
                modal.find('form').resetForm();
                // $('.error.invalid-feedback').remove();
                // $('input.is-invalid').removeClass('is-invalid');
                let form = $('#form-task');
                form.validate().resetForm();
                form[0].reset();
                modal.data('type', 'edit');
                loading();
                modal.find('.title-popup').text(obj.attr('title'));
                modal.data('sub-id', obj.data('sub-id'));
                modal.data('type-task', obj.data('type-task'));
                modal.find('#id-task').val(id);
                modal.find('#name').val(obj.data('name'));
                modal.find('#status').val(obj.data('status'));
                modal.find('#start_date input').val(obj.data('start_date'));
                modal.find('#end_date input').val(obj.data('end_date'));

                modal.find("input[name='start_date']").datepicker({format: "dd-mm-yyyy"});
                modal.find("input[name='end_date']").datepicker({format: "dd-mm-yyyy"});
                modal.find('#admin_ids option').each(function () {
                    if (obj.data('admin_ids').indexOf('|' + $(this).attr('value') + '|') != '-1') {
                        $(this).attr('selected', '');
                    } else {
                        $(this).prop("selected", false);
                    }
                });
                modal.find('#admin_ids').select2();
                stopLoading();
                modal.modal();
                $('#admin-single').select2();
            }


            $('body').on('click', '.btn-edit-admin', function () {
                $('.edit-admin-single').show();
                $('.subject-items.admin-single').hide();
            });


            $('body').on('change', '#list_subject', function () {
                $('#add-subject').attr('data-href', '{{URL::to('admin/job/insert-subject')}}?id=' + $(this).val() + '&job_id=' + $('input[name=id]').val());
            });
        });
    </script>
    <style>
        .edit-admin-single {
            border: 1px solid;
            padding: 10px;
            background: aliceblue;
        }

        .tooltip-arrow {
            width: 200px;
            /*max-width: 200px;*/
            height: auto;
            padding: 5px;
            border-radius: 10px 0px 10px 10px;
            border: 1px solid green;
            background: #ffffff;
            display: none;
        }

        .p-other {
            cursor: pointer;
        }

        .tooltip-arrow p, .admin-task {

        }

        .edit-task {
            cursor: pointer;
        }

        .box-delete {
            width: 50px;
            display: flex;
            align-content: center;
            background: #ff4b3c;
            cursor: pointer;
            border-bottom: 1px solid #ddd;
        }

        .box-delete i {
            color: #fff;
            margin: auto;
            text-align: center;
        }

        .accordion-header {
            display: flex;
        }

        .accordion-header span {
            margin-right: 2px;
        }

        /*.accordion-heading {*/
        /*border: 1px solid #e5e5e5;*/
        /*width: 100%;*/
        /*}*/

        /*.accordion-heading .accordion-toggle {*/
        /*font-weight: bold;*/
        /*}*/

        .subject-items tr td:nth-child(2n+1) {
            text-align: right;
        }

        .subject-items {
            margin-top: 10px;
            border: 1px solid #ccc;
            padding: 5px;
        }

        .box.box-info img {
            max-width: inherit;
        }

        #comment_tab .box-body {
            background: #f4f5f7;
        }

        .accordion-inner {
            background: #e5e5e5;
        }
    </style>
    <script>
        $(document).ready(function () {
            let rs_task = $('#result-tasks').parents('.kt-portlet__body');
            KTApp.block(rs_task, {
                overlayColor: "#000",
                type: "v1",
                state: "success",
                size: "lg"
            });
            $.ajax({
                url: '/admin/task/get-by-id/' + '{{$result->id}}',
                type: 'GET',
                success: function (res) {
                    rs_task.html(res);
                }, error: function () {
                    alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                }
            });

            $('body').on('click', '.pagination-tasks ul li', function (event) {
                event.preventDefault();
                let url = $(this).children('a').attr('href');
                if (url != undefined) {
                    $.ajax({
                        url: url,
                        type: 'GET',
                        beforeSend: function () {
                            // Handle the beforeSend event
                            KTApp.block(rs_task, {
                                overlayColor: "#000",
                                type: "v1",
                                state: "success",
                                size: "lg"
                            });
                        },
                        success: function (res) {
                            rs_task.html(res);
                        }, error: function () {
                            alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                        }
                    });
                }
            });
        });
    </script>
    <script>
        $(document).ready(function () {


            $('body').on('click', '.delete-task', function (e) {
                e.preventDefault();
                let url = $(this).attr('href');
                if (confirm('Bạn có chắc chắn xóa nhiệm vụ này !!')) {
                    Location(url);
                }
            });

            $('body').on('click', '.finish-task', function (e) {
                e.preventDefault();
                let url = $(this).attr('href');
                let type = $(this).data('type-finish');
                type = type == '1' ? ' hủy' : '';
                if (confirm('Bạn có chắc chắn' + type + ' hoàn thành nhiệm vụ này !!')) {
                    Location(url);
                }
            });

            Location = function (url) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {},
                    success: function (data) {
                        if (data.status) {
                            window.location.reload();
                        } else {
                            toastr.error("Thất bại!");
                        }
                    }
                });
            }
        });
    </script>

    <div id="result-tasks" style="height: 150px"></div>
@else


    <?php
    $dir_name = config('eworkingjob.job_data_draf'). $job_id;
    $filename = $dir_name . '/draft.txt';
    $message_data = [];
    if (file_exists($filename)) { //kiem tra xem file ton tai thi lay ra content
        $message_data = (array)json_decode(file_get_contents($filename));
    }

    $status_draft = false;
    $tasks_draft = null;
    foreach ($message_data as $data_draft) {
        if (isset($data_draft->add_tasks)) { //neu co obj general roi thi luu thay doi
            $tasks_draft = $data_draft->add_tasks;
            $status_draft = true;
        }
    }
    ?>
    @include('eworkingjob::task.task',['main'=>'single-','tasks'=>$task_single,'status_draft'=>$status_draft,'tasks_draft'=>$tasks_draft])
    {{$task_single->render(config('core.admin_theme').'.partials.paginate',['class'=>'tasks'])}}
@endif


