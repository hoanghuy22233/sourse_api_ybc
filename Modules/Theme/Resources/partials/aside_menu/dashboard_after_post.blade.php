@if(in_array('post_view', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon-file-2"></i>
                    </span><span class="kt-menu__link-text">{{ trans('theme::admin.post') }}</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                @if(in_array('post_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/post" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">{{ trans('theme::admin.all_post') }}</span></a></li>
                @endif
                @if(in_array('post_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/post/add" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">{{ trans('theme::admin.create_post') }}</span></a></li>
                @endif
                @if(in_array('category_post_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true">
                        <a href="/admin/category_post" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">{{ trans('theme::admin.category') }}</span></a></li>
                @endif
                @if(in_array('tag_post_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/tag_post" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">{{ trans('theme::admin.tag') }}</span></a></li>
                @endif
            </ul>
        </div>
    </li>
@endif