<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {

    //  Post
    Route::group(['prefix' => 'posts'], function () {
        Route::get('', 'Admin\PostController@index');
        Route::get('{id}', 'Admin\PostController@show');
    });

    //  Category
    Route::group(['prefix' => 'categories'], function () {
        Route::get('', 'Admin\CategoryController@index');
        Route::get('{id}', 'Admin\CategoryController@show');
    });


    //  Banner
    Route::group(['prefix' => 'banners'], function () {
        Route::get('', 'Admin\BannerController@index');

    });

    //  liên hệ
    Route::group(['prefix' => 'contacts', 'middleware'], function () {
        Route::get('', 'Admin\ContactController@index');
        Route::post('', 'Admin\ContactController@store');
        Route::get('{id}', 'Admin\ContactController@show');
        Route::post('{id}', 'Admin\ContactController@update');
        Route::delete('{id}', 'Admin\ContactController@delete');

    });
});
