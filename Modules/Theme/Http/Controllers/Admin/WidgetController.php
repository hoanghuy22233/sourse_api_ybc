<?php

namespace Modules\Theme\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;

class WidgetController extends CURDBaseController
{
    protected $orderByRaw = 'status desc, id desc';

    protected $module = [
        'code' => 'widget',
        'table_name' => 'widgets',
        'label' => 'Widget',
        'modal' => '\Modules\Theme\Models\Widget',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'location', 'type' => 'select', 'label' => 'Vị trí hiển thị', 'options' => [
                'home0' => 'Widget trang chủ 1',
                'home1' => 'Widget trang chủ 2',
                'footer_0' => 'Widget footer 1',
                'footer_1' => 'Widget footer 2',
                'footer_2' => 'Widget footer 3',
                'footer_3' => 'Widget footer 4',
                'footer_bot_1' => 'Widget footer bot 1',
                'footer_bot_2' => 'Widget footer bot 2',
                'user0' => 'Widget khách hàng',
                'facebook0' => 'Widget facebook',
                'faq0' => 'Widget faq 1',
                'faq1' => 'Widget faq 2',
                'faq2' => 'Widget faq 3',
                'product0' => 'Widget chi tiết sản phẩm 1',
                'product1' => 'Widget chi tiết sản phẩm 2',
                'product2' => 'Widget chi tiết sản phẩm 3',
                'title1' => 'Widget header sản phẩm',
                'title2' => 'Widget header giỏ hàng',
                'title3' => 'Widget header thanh toán',
                'title4' => 'Widget header hỏi đáp',
                'title5' => 'Widget header liên hệ',
                'title6' => 'Widget header tin tức',
                'title7' => 'Widget header công ty',
                'background_image' => 'Background image'
            ],],
            ['name' => 'order_no', 'type' => 'text', 'label' => 'Thứ tự'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên'],
                ['name' => 'content', 'type' => 'textarea_editor', 'label' => 'Nội dung'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'Thứ tự'],


            ],

            'info_tab' => [
                ['name' => 'location', 'type' => 'select', 'options' => [
                    'home0' => 'Widget trang chủ 1',
                    'home1' => 'Widget trang chủ 2',
                    'footer_0' => 'Widget footer 1',
                    'footer_1' => 'Widget footer 2',
                    'footer_2' => 'Widget footer 3',
                    'footer_3' => 'Widget footer 4',
                    'footer_bot_1' => 'Widget footer bot 1',
                    'footer_bot_2' => 'Widget footer bot 2',
                    'user0' => 'Widget khách hàng',
                    'facebook0' => 'Widget facebook',
                    'faq0' => 'Widget faq 1',
                    'faq1' => 'Widget faq 2',
                    'faq2' => 'Widget faq 3',
                    'product0' => 'Widget chi tiết sản phẩm 1',
                    'product1' => 'Widget chi tiết sản phẩm 2',
                    'product2' => 'Widget chi tiết sản phẩm 3',
                    'title1' => 'Widget header sản phẩm',
                    'title2' => 'Widget header giỏ hàng',
                    'title3' => 'Widget header thanh toán',
                    'title4' => 'Widget header hỏi đáp',
                    'title5' => 'Widget header liên hệ',
                    'title6' => 'Widget header tin tức',
                    'title7' => 'Widget header công ty',
                    'background_image' => 'Background image'
                ], 'label' => 'Vị trí hiển thị'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt', 'value' => 0],


            ],

        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'location' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'options' => [
                '' => 'Trạng thái',
                'home0' => 'Widget trang chủ 1',
                'home1' => 'Widget trang chủ 2',
                'footer_0' => 'Widget footer 1',
                'footer_1' => 'Widget footer 2',
                'footer_2' => 'Widget footer 3',
                'footer_3' => 'Widget footer 4',
                'footer_bot_1' => 'Widget footer bot 1',
                'footer_bot_2' => 'Widget footer bot 2',
                'user0' => 'Widget khách hàng',
                'facebook0' => 'Widget facebook',
                'faq0' => 'Widget faq 1',
                'faq1' => 'Widget faq 2',
                'faq2' => 'Widget faq 3',
                'product0' => 'Widget chi tiết sản phẩm 1',
                'product1' => 'Widget chi tiết sản phẩm 2',
                'product2' => 'Widget chi tiết sản phẩm 3',
                'title1' => 'Widget header sản phẩm',
                'title2' => 'Widget header giỏ hàng',
                'title3' => 'Widget header thanh toán',
                'title4' => 'Widget header hỏi đáp',
                'title5' => 'Widget header liên hệ',
                'title6' => 'Widget header tin tức',
                'title7' => 'Widget header công ty',
                'background_image' => 'Background image'
            ],
            'query_type' => 'like'
        ],

    ];

    public function getIndex(Request $request)
    {
        

        $data = $this->getDataList($request);

        return view('theme::widget.list')->with($data);
    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('theme::widget.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {
                

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('theme::widget.edit')->with($data);
            } else if ($_POST) {

                    

                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {

                

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {

                

            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {

                

            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
