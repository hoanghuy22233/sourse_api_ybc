<?php

namespace Modules\Theme\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;

class PostController extends CURDBaseController
{
    protected $module = [
        'code' => 'post',
        'table_name' => 'posts',
        'label' => 'Bài viết',
        'modal' => '\Modules\Theme\Models\Post',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên bài viết'],
            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'theme::list.td.multi_cat', 'label' => 'Danh mục', 'object' => 'category_post'],
            ['name' => 'tags', 'type' => 'custom', 'td' => 'theme::list.td.multi_cat', 'label' => 'Từ khóa', 'object' => 'tag_post'],
            ['name' => 'slug', 'type' => 'text', 'label' => 'Đường dẫn tĩnh'],
//            ['name' => 'tin_hot', 'type' => 'status', 'label' => 'Tin hot'],
            ['name' => 'order_no', 'type' => 'number', 'label' => 'Ưu tiên'],
//            ['name' => 'popular', 'type' => 'status', 'label' => 'Nổi bật'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trang thái'],
//            ['name' => 'faq', 'type' => 'status', 'label' => 'FAQ?'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên'],
                ['name' => 'multi_cat', 'type' => 'custom', 'field' => 'theme::form.fields.multi_cat', 'label' => 'Danh mục bài viết', 'model' => \Modules\Theme\Models\Category::class,
                    'object' => 'category_post', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=1', 'des' => 'Danh mục đầu tiên chọn là danh mục chính'],
                ['name' => 'tags', 'type' => 'custom', 'field' => 'theme::form.fields.tags', 'label' => 'Từ khóa bài viết', 'model' => \Modules\Theme\Models\Category::class,
                    'object' => 'tag_post', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=2'],
                ['name' => 'intro', 'type' => 'textarea', 'label' => 'Mô tả ngắn'],
                ['name' => 'content', 'type' => 'textarea_editor', 'label' => 'Nội dung'],
//                ['name' => 'faq', 'type' => 'checkbox', 'label' => 'FAQ','value' => 1, 'group_class' => 'col-md-2'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt','value' => 1, 'group_class' => 'col-md-3'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'Thứ tự ưu tiên', 'value' => 0, 'group_class' => 'col-md-3', 'des' => 'Số to ưu tiên hiển thị trước'],
//                ['name' => 'tin_hot', 'type' => 'checkbox', 'label' => 'Tin hot', 'group_class' => 'col-md-3'],
//                ['name' => 'popular', 'type' => 'checkbox', 'label' => 'Nổi bật', 'group_class' => 'col-md-3'],

            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh bài viết'],
            ],

            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'Slug', 'des' => 'Đường dẫn bài viết trên thanh địa chỉ'],
                ['name' => 'meta_title', 'type' => 'text', 'label' => 'Meta title'],
                ['name' => 'meta_description', 'type' => 'text', 'label' => 'Meta description'],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên bài viết',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'slug' => [
            'label' => 'Đường dẫn tĩnh',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'category_id' => [
            'label' => 'Danh mục',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'category_post',
            'model' => \Modules\Theme\Models\Category::class,
            'query_type' => 'custom'
        ],
        'tags' => [
            'label' => 'Từ khóa bài viết',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'tag_post',
            'model' => \Modules\Theme\Models\Category::class,
            'query_type' => 'custom'
        ],
        'tin_hot' => [
            'label' => 'Tin hot',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
        ],
        'popular' => [
            'label' => 'Nổi bật',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
        ],

    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('theme::post.list')->with($data);
    }
    public function appendWhere($query, $request)
    {

        //  Lọc theo danh mục
        if (!is_null($request->get('category_id'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }
        if (!is_null($request->get('tags'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {

                $data = $this->getDataAdd($request);

                return view('theme::post.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('tags')) {
                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
                    }
                    #

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }
                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('theme::post.edit')->with($data);
            } else if ($_POST) {

                

                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());


                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('tags')) {
                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
                    }
                    #

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
