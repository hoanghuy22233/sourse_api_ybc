<?php

namespace Modules\Theme\Http\Controllers\Api\Admin;

use \Modules\A4iSeason\Models\Disease;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\GapFertilizer\Models\Product;
use Modules\Theme\Models\Post;
use Validator;

class PostController extends Controller
{

    protected $module = [
        'code' => 'post',
        'table_name' => 'posts',
        'label' => 'Bài viết',
        'modal' => 'Modules\Theme\Models\Post',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'category_id' => [
            'query_type' => 'custom'
        ]
    ];

    public function index(Request $request)
    {

        try {
            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Post::selectRaw('posts.id, posts.name, posts.image, posts.created_at, posts.intro')
                ->whereRaw($where)->where($this->module['table_name'] . '.status', 1);

            if ($request->has('category_id')) {
                $listItem = $listItem->where('multi_cat', 'like', '%|'.$request->category_id.'|%');
            }

            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            foreach ($listItem as $k => $v) {
                $v->image = asset('public/filemanager/userfiles/' . $v->image);

            }
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id, Request $r)
    {
        try {

            $item = Post::selectRaw('posts.id, posts.name, posts.image, posts.multi_cat, posts.content, posts.created_at, posts.intro, posts.product_sidebar')->where($this->module['table_name'] . '.id', $id)->where($this->module['table_name'] . '.status', 1)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }
            $relate_item = Post::selectRaw('posts.id, posts.name, posts.image, posts.created_at, posts.intro')
                ->where($this->module['table_name'] . '.id', '!=', $id)
                ->where($this->module['table_name'] . '.status', 1);


            foreach ($relate_item as $k => $v) {
                $v->image = asset('public/filemanager/userfiles/' . $v->image);

            }
            if ($item->product_sidebar != '') {
                $product_sidebar = explode('|', $item->product_sidebar);
                $product_sidebar = Product::select('id', 'name', 'image', 'final_price', 'intro')->whereIn('id', $product_sidebar)->where('status', 1)->get();
                foreach ($product_sidebar as $p) {
                    $p->image = asset('public/filemanager/userfiles/' . $p->image);
                }

            }

            $cat_ids = explode('|', $item->multi_cat);
            $relate_item = $relate_item->where(function ($query) use ($cat_ids) {
                foreach ($cat_ids as $cat_id) {
                    if ($cat_id != '') {
                        $query->orwhere('multi_cat', 'like', '%|' . $cat_id . '|%');
                    }
                }
            });

            $limit = $r->has('limit') ? $r->limit : 4;
            $relate_item = $relate_item->paginate($limit);

            $item->image = asset('public/filemanager/userfiles/' . $item->image);

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => [
                    'item' => $item,
                    'relate' => $relate_item,
                    'product_relate' => @$product_sidebar,
                ],
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();
            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = CommonHelper::saveFile($image, 'disease');
                    }
                } else {
                    $data['image'] = CommonHelper::saveFile($request->file('image'), 'disease');
                }
            }

            if ($request->has('disease_image')) {
                if (is_array($request->file('disease_image'))) {
                    foreach ($request->file('disease_image') as $image) {
                        $data['disease_image'] = CommonHelper::saveFile($image, 'disease');
                    }
                } else {
                    $data['disease_image'] = CommonHelper::saveFile($request->file('disease_image'), 'disease');
                }
            }

            $item = Disease::create($data);

            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {

        $item = Disease::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = CommonHelper::saveFile($image, 'disease');
                }
            } else {
                $data['image'] = CommonHelper::saveFile($request->file('image'), 'disease');
            }
        }
        if ($request->has('disease_image')) {
            if (is_array($request->file('disease_image'))) {
                foreach ($request->file('disease_image') as $image) {
                    $data['disease_image'] = CommonHelper::saveFile($image, 'disease');
                }
            } else {
                $data['disease_image'] = CommonHelper::saveFile($request->file('disease_image'), 'disease');
            }
        }

        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (Disease::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('order_no', 'desc')->orderBy('id', 'desc');
        }
        return $model;
    }
}
