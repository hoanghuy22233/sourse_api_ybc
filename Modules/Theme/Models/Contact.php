<?php
namespace Modules\Theme\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{

    protected $table = 'contacts';
    public $timestamps = false;



    protected $fillable = [
        'name', 'gender','email', 'tel','address', 'content','user_ip', 'product_id', 'image', 'admin_id'
    ];



}
