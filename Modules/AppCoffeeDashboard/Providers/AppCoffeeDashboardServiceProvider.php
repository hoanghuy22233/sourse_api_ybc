<?php

namespace Modules\AppCoffeeDashboard\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class AppCoffeeDashboardServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('AppCoffeeDashboard', 'Database/Migrations'));
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Custom setting
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {

            $per_check = array_merge($per_check, ['dashboard','view_all_data']);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('appcoffeedashboard::partials.aside_menu.aside_menu_dashboard');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('AppCoffeeDashboard', 'Config/config.php') => config_path('appcoffeedashboard.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('AppCoffeeDashboard', 'Config/config.php'), 'appcoffeedashboard'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/appcoffeedashboard');

        $sourcePath = module_path('AppCoffeeDashboard', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/appcoffeedashboard';
        }, \Config::get('view.paths')), [$sourcePath]), 'appcoffeedashboard');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/appcoffeedashboard');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'appcoffeedashboard');
        } else {
            $this->loadTranslationsFrom(module_path('AppCoffeeDashboard', 'Resources/lang'), 'appcoffeedashboard');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('AppCoffeeDashboard', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
