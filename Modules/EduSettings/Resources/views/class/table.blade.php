<style>
    .form-group-dynamic .fieldwrapper > div:nth-child(1) {
        padding-left: 0;
    }
</style>
<div class="form-group form-group-dynamic" id="form-group-{{ $field['name'] }}">
    <label for="{{ $field['name'] }}"
           class="col-xs-12 control-label">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
            <span class="color_btd">*</span>@endif</label>
    <div class="col-xs-12">
        <div class="row">
            <fieldset id="buildyourform-{{ $field['name'] }}" class="{{ @$field['class'] }}">
                <div class="fieldwrapper" id="field1" >
                    <div class="col-xs-5">{{ @$field['cols'][0] }}</div>
                    <div class="col-xs-5">{{ @$field['cols'][1] }}</div>
                    <div class="col-xs-2">
                    </div>
                </div>

                    <div class="fieldwrapper" id="field1" >
                        <div class="row">
                            <div class="col-xs-5">
                                <select class="form-control fieldname" name="{{ $field['name'] }}_key[]">
                                    @foreach($options as $op)
                                        <option value="{{ $op->id }}" {{ $op->id == $v ? 'selected' : '' }}>{{ $op->{$field['display_field']} }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-5"><input type="text" class="col-xs-5 form-control fieldValue" value="{{ $v }}"
                                                         name="{{ $field['name'] }}_value[]" placeholder="Giá trị"></div>
                            <div class="col-xs-2"><input type="button" class="col-xs-2 btn remove btn btn-danger btn-icon la la-remove" value="-" title="Xóa hàng">
                            </div>
                        </div>
                    </div>
                @endforeach
            </fieldset>
        </div>
        <input type="button" value="Thêm hàng" class="btn btn-primary add-{{ $field['name'] }}" id="add-{{ $field['name'] }}"/>
        <span class="text-danger">{{ $errors->first($field['name']) }}</span>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#add-{{ $field['name'] }}").click(function () {
            var lastField = $("#buildyourform-{{ $field['name'] }} div:last");
            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $('<div style="display:flex; padding: 10px" class="fieldwrapper" id="field' + intId + '"/>');
            fieldWrapper.data("idx", intId);
            var fKey = $('<div class="col-xs-5"><select class="form-control fieldname" name="{{ $field['name'] }}_key[]"> @foreach($options as $op)<option value="{{ $op->id }}">{{ $op->{$field['display_field']} }}</option>@endforeach</select></div>');
            var fValue = $('<div class="col-xs-5"><input type="text" class="col-xs-5 form-control fieldValue" name="{{ $field["name"] }}_value[]" placeholder="Giá trị"/></div>');
            var image = $('<div class="col-xs-5"><input type="file" class="col-xs-5 form-control fieldValue" name="{{ $field["name"] }}_image[]" placeholder="Ảnh"/></div>');
            var removeButton = $('<div class="col-xs-2"><input type="button" class="col-xs-2 btn remove btn btn-danger btn-icon la la-remove" value="-" title="Xóa hàng"/></div>');
            $('body').on('click', '.remove', function () {
                $(this).parents('.fieldwrapper').remove();
            });
            fieldWrapper.append(fKey);
            fieldWrapper.append(fValue);
            fieldWrapper.append(image);
            fieldWrapper.append(removeButton);
            $("#buildyourform-{{ $field['name'] }}").append(fieldWrapper);
        });
    });
</script>
