<label for="password">{{ @$field['label'] }} </label>
<input type="text" name="{{ @$field['name'] }}" class="form-control {{ @$field['class'] }}"
       id="{{ $field['name'] }}" {!! @$field['inner'] !!} min="8"
       value=""
       {{ strpos(@$field['class'], 'require') !== false ? 'required' : '' }}
       placeholder="{{ trans(@$field['label']) }}"
>
<span class="form-text text-muted"></span>
<span class="text-success">{{ @$field['des'] }}</span>
