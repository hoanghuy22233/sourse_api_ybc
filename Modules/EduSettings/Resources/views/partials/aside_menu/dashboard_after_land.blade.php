@if(in_array('student_view', $permissions))
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/class"
                                                      class="kt-menu__link "><span
                    class="kt-menu__link-icon">
<i class="flaticon-calendar-1"></i></span><span class="kt-menu__link-text">Lớp học</span></a></li>
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/student"
                                                      class="kt-menu__link "><span
                    class="kt-menu__link-icon">
<i class="flaticon-user-add"></i></span><span class="kt-menu__link-text">Học viên</span></a></li>
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/schedule"
                                                      class="kt-menu__link "><span
                    class="kt-menu__link-icon">
<i class="flaticon-calendar-1"></i></span><span class="kt-menu__link-text">Lịch học</span></a></li>
@endif
