<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">CRM</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
@if(in_array('class_view', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon2-group"></i>
                    </span><span class="kt-menu__link-text">Lớp học</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/class" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Lớp học</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/schedule" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Lịch học</span></a></li>

            </ul>
        </div>
    </li>

@endif
@if(in_array('student_view', $permissions))

<li class="kt-menu__item" aria-haspopup="true"><a href="/admin/student"
                                                  class="kt-menu__link "><span
                class="kt-menu__link-icon">
<i class="flaticon-user-add"></i></span><span class="kt-menu__link-text">Học viên</span></a></li>
@endif
