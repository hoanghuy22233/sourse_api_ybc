<?php

namespace Modules\EduSettings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStdentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'name' => 'required|min:5',
             'email' => 'email|max:255|unique:students,email,'.$this->id,
             'phone' => 'min:10|max:11|unique:students,phone,'.$this->id,
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
