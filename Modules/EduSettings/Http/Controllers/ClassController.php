<?php

namespace Modules\EduSettings\Http\Controllers;

use App\Http\Controllers\Admin\CURDBaseController;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\EduSettings\Http\Requests\CreateClassRequest;
use Modules\EduSettings\Repositories\Classs\ClassRepositoryEloquent;
use App\Http\Helpers\CommonHelper;

class ClassController extends CURDBaseController
{
    protected $orderByRaw = 'status desc, id desc';

    protected $module = [
        'code' => 'class',
        'table_name' => 'classs',
        'label' => 'Lớp học',
        'modal' => '\Modules\EduSettings\Entities\Classs',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'number_time', 'type' => 'text', 'label' => 'Thời lượng'],
            ['name' => 'teacher_id', 'type' => 'relation', 'label' => 'Giảng viên', 'object' => 'teacher','display_field' => 'name'],
            ['name' => 'tutors_id', 'type' => 'relation', 'label' => 'Trợ giảng', 'object' => 'tutors','display_field' => 'name'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],
            ['name' => 'updated_at', 'type' => 'text', 'label' => 'Cập nhật'],
        ],
        'list_all' => [
            ['name' => 'avatar', 'type' => 'image', 'label' => 'Ảnh'],
            ['name' => 'phone', 'type' => 'number', 'label' => 'SĐT'],
            ['name' => 'email', 'type' => 'text', 'label' => 'Email'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên'],
                ['name' => 'number_time', 'type' => 'number', 'class' => 'required', 'label' => 'Thời lượng'],
                ['name' => 'teacher_id', 'type' => 'select2_ajax_model', 'object' => 'admin', 'class' => 'required', 'label' => 'Giảng viên', 'model' => Admin::class, 'display_field' => 'name'],
                ['name' => 'tutors_id', 'type' => 'select2_ajax_model', 'object' => 'admin', 'class' => '', 'label' => 'Trợ giảng', 'model' => Admin::class, 'display_field' => 'name'],
                ['name' => 'start', 'type' => 'date', 'class' => 'required', 'label' => 'Ngày bắt đầu'],
                ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ', 'class' => 'required'],
                ['name' => 'room', 'type' => 'text', 'label' => 'Phòng học', 'class' => 'required'],
                ['name' => 'status', 'type' => 'select', 'options' =>[
                    1 => 'Lập kết hoạch',
                    2 => 'Trong tiến trình',
                    3 => 'Đóng cửa',
                    4 => 'Đã kết thúc'
                ], 'label' => 'Trạng thái'],
            ],
            'info_tab' => [
                ['name' => 'monday', 'type' => 'checkbox', 'label' => 'Thứ 2', 'value' => '0'],
                ['name' => 'tuesday', 'type' => 'checkbox', 'label' => 'Thứ 3', 'value' => '0'],
                ['name' => 'wednesday', 'type' => 'checkbox', 'label' => 'Thứ 4', 'value' => '0'],
                ['name' => 'thurday', 'type' => 'checkbox', 'label' => 'Thứ 5', 'value' => '0'],
                ['name' => 'friday', 'type' => 'checkbox', 'label' => 'Thứ 6', 'value' => '0'],
                ['name' => 'saturday', 'type' => 'checkbox', 'label' => 'Thứ 7', 'value' => '0'],
                ['name' => 'sunday', 'type' => 'checkbox', 'label' => 'CN', 'value' => '0'],
            ],
        ]
    ];

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $data = $this->getDataList($request);
        return view('edusettings::class.list')->with($data);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $data = $this->getDataList($request);
        return view('edusettings::class.add')->with($data);
    }

    /**
     * @param CreateClassRequest $request
     * @param ClassRepositoryEloquent $repositoryEloquent
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateClassRequest $request, ClassRepositoryEloquent $repositoryEloquent)
    {
        try {
            $data = $request->only('');
            $result = $repositoryEloquent->create();
        } catch (\Exception $ex){
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('edusettings::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('edusettings::edit');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function add(Request $request)
    {

        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('edusettings::rank.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    $stt1=trim(strstr($request->link_google_form,'d/'),'/edit');
                    $stt2=explode('/edit',strstr($request->link_google_form_answer,'d/'));

                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('link_google_form')) {
                        $data['google_form_id'] = $stt1;
                    }
                    if ($request->has('link_google_form_answer')) {
                        $data['google_form_answer_id'] = trim($stt2[0],'d/');
                    }
//                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;


                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
//                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

//        $items = @explode('/d/', trim($item->link_google_form,'/edit') );
//        dd($items);
        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('edusettings::rank.edit')->with($data);
        } else if ($_POST) {
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ], [
                'name.required' => 'Bắt buộc phải nhập tên',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                $stt1=trim(strstr($request->link_google_form,'d/'),'/edit');
                $stt2=explode('/edit',strstr($request->link_google_form_answer,'d/'));
                //  Tùy chỉnh dữ liệu insert
                if ($request->has('multi_cat')) {
                    $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                    $data['category_id'] = $request->multi_cat[0];
                }
                if ($request->has('link_google_form')) {
                    $data['google_form_id'] = $stt1;
                }
                if ($request->has('link_google_form_answer')) {
                    $data['google_form_answer_id'] = trim($stt2[0],'d/');
                }
                #

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::flushCache($this->module['table_name']);
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function duplicate(Request $request, $id)
    {
        $poduct = Lesson::find($id);
        $poduct_new = $poduct->replicate();
        $poduct_new->slug = $poduct->slug . $poduct_new->company_id;
        $poduct_new->company_id = \Auth::guard('admin')->user()->last_company_id;
        $poduct_new->admin_id = \Auth::guard('admin')->user()->id;
        $poduct_new->save();
        return $poduct_new;
    }
}
