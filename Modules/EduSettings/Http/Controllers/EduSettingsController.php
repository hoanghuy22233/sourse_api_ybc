<?php

namespace Modules\EduSettings\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\CURDBaseController;
use App\Http\Helpers\CommonHelper;
use Modules\EduSettings\Http\Requests\CreateStdentRequest;
use Modules\EduSettings\Http\Requests\UpdateStdentRequest;
use Modules\EduSettings\Repositories\Student\StudentRepository;
use Modules\EduSettings\Repositories\Student\StudentRepositoryEloquent;
use Validator;

class EduSettingsController extends CURDBaseController
{
    protected $orderByRaw = 'status desc, id desc';

    protected $module = [
        'code' => 'student',
        'table_name' => 'students',
        'label' => 'Học viên',
        'modal' => '\Modules\EduSettings\Entities\Student',
        'list' => [
            ['name' => 'avatar', 'type' => 'image', 'label' => 'Ảnh', 'sort' => true],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Họ & Tên', 'sort' => true],
            ['name' => 'phone', 'type' => 'text', 'label' => 'SĐT', 'sort' => true],
            ['name' => 'email', 'type' => 'text', 'label' => 'Email', 'sort' => true],
//            ['name' => 'source', 'type' => 'text', 'label' => 'Source', 'sort' => true],
            ['name' => 'type', 'type' => 'select', 'label' => 'Loại', 'options' => [
                0 => 'Khách',
                1 => 'Học viên'
            ],],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái', 'sort' => true],
            ['name' => 'updated_at', 'type' => 'date_vi', 'label' => 'Cập nhật', 'sort' => true],
        ],
        'list_all' => [
            ['name' => 'avatar', 'type' => 'image', 'label' => 'Ảnh'],
            ['name' => 'phone', 'type' => 'number', 'label' => 'SĐT'],
            ['name' => 'email', 'type' => 'text', 'label' => 'Email'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'type', 'type' => 'select', 'options' => [
                    0 => 'Khách',
                    1 => 'Học viên'
                ], 'class' => 'required', 'label' => 'Chọn thể loại'],
                ['name' => 'avatar', 'type' => 'file_image', 'label' => 'Ảnh đại diện'],
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Họ & tên'],
                ['name' => 'phone', 'type' => 'number', 'class' => 'required', 'label' => 'SĐT'],
                ['name' => 'email', 'type' => 'email', 'class' => 'required', 'label' => 'Email'],
//                ['name' => 'password', 'type' => 'password', 'label' => 'Mật khẩu'],
                ['name' => 'password', 'type' => 'custom','class' => '', 'fields' => 'edusettings::form.fields.password', 'label' => 'Mật khẩu', 'des'=>'Chú ý: Mật khẩu bắt buộc từ 8 kí tự trở lên'],
                ['name' => 'review', 'type' => 'text', 'class' => '', 'label' => 'Đánh giá'],
            ],
            'info_tab' => [
                ['name' => 'facebook', 'type' => 'text', 'label' => 'Facebook'],
                ['name' => 'zalo', 'type' => 'text', 'label' => 'Zalo'],
                ['name' => 'gender', 'type' => 'select', 'label' => 'Giới tính', 'options' => [
                    1 => 'Nam',
                    2 => 'Nữ'
                ]],
                ['name' => 'birthday','type' => 'date', 'label' => 'Ngày sinh'],
                ['name' => 'source', 'type' => 'select', 'options' =>[
                    1 => 'Facebook',
                    2 => 'Bạn bè',
                    3 => 'Homepage',
                    4 => 'Khác'
                ], 'label' => 'Nguồn'],
                ['name' => 'center', 'type' => 'text', 'label' => 'Địa chỉ'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Trạng thái', 'value' => '1'],
            ]
        ]
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'email' => [
            'label' => 'Email',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'phone' => [
            'label' => 'SDT',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'source' => [
            'label' => 'Source',
            'type' => 'text',
            'query_type' => 'like'
        ],
    ];

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);
        return view('edusettings::student.list')->with($data);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAdd(Request $request)
    {
        $data = $this->getDataAdd($request);
        return view('edusettings::student.add')->with($data);
    }


    /**
     * @param CreateStdentRequest $request
     * @param StudentRepositoryEloquent $repository
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function add(CreateStdentRequest $request, StudentRepositoryEloquent $repository)
    {
        try {
            $data = $this->processingValueInFields($request, $this->getAllFormFiled());
            $data['user_id'] = \Auth::guard('admin')->user()->id;

            $data['password'] = bcrypt($request->password);
            $result = $repository->create($data,'success','Tạo mới thành công');

            if(is_object($result) == null){
                CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
            }

            if ($request->return_direct == 'save_continue') {
                return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
            } elseif ($request->return_direct == 'save_create') {
                return redirect('admin/' . $this->module['code'] . '/add');
            }

            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUpdate(Request $request){
        $item = $this->model->find($request->id);
        $data = $this->getDataUpdate($request, $item);
        return view('edusettings::student.edit')->with($data);
    }

    /**
     * @param UpdateStdentRequest $request
     * @param StudentRepository $repository
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UpdateStdentRequest $request, StudentRepository $repository){
        try {

            $data = $this->processingValueInFields($request, $this->getAllFormFiled());
            if ($request->password == null) {
                unset($data['password']);
            }
            $result =  $repository->update($request->id, $data,'success', 'Cập nhật thành công!');

            if(is_object($result) == null){
                CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
            }

            if ($request->return_direct == 'save_continue') {
                return redirect('admin/' . $this->module['code'] . '/' . $request->id);
            } elseif ($request->return_direct == 'save_create') {
                return redirect('admin/' . $this->module['code'] . '/add');
            }

            return redirect('admin/' . $this->module['code']);
        }
        catch (\Exception $ex){
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }
    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }
    public function delete(Request $request, StudentRepository $repository)
    {
        try {
            $repository->delete($request->id,'success','Xóa thàng công');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function multiDelete(Request $request)
    {
        try {
            $items = $this->model->whereIn('id', $request->ids)->get();
            foreach ($items as $item) {
                $item->delete();
            }

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
