<?php

namespace Modules\EduSettings\Repositories\Student;

use Illuminate\Support\Facades\Session;
use App\Http\Helpers\CommonHelper;

abstract class StudentRepository
{

    protected $_model;

    public function __construct()
    {
        $this->setModel();
    }

    abstract public function getModel();

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function setModel()
    {
        $this->_model = app()->make(
            $this->getModel()
        );
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->_model->orderBy('created_at', 'desc');
    }


    public function find($id)
    {
        $result = $this->_model->find($id);
        return $result;
    }
    /**
     * @param array $attributes
     * @param $type
     * @param $text
     * @return mixed
     */
    public function create(array $attributes,$type,$text)
    {
        $attributes['password'] = bcrypt($attributes['password']);
        CommonHelper::one_time_message($type, $text);
        return $this->_model->create($attributes);
    }

    /**
     * @param $id
     * @param array $attributes
     * @param $type
     * @param $text
     * @return bool
     */
    public function update($id, array $attributes, $type, $text)
    {
        if(isset($attributes['password'])){
            $attributes['password'] = bcrypt($attributes['password']);
        }
        $result = $this->find($id);

        if ($result) {
            dd($attributes);
            $result->update($attributes);
            CommonHelper::one_time_message($type, $text);
            return $result;
        }
        return false;
    }

    public function delete($id,$type,$text)
    {
        $result = $this->find($id);
        if ($result) {
            $result->delete();
            CommonHelper::one_time_message($type, $text);
            return true;

        }
        return false;
    }
}

