<?php

namespace Modules\EduSettings\Entities;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedules';
    protected $guarded=[];
    public $timestamps = false;
}
