<?php

namespace Modules\EduSettings\Entities;

use App\Models\Admin;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Modules\ThemeEdu\Models\Contact;

class Student extends Authenticatable
{

    protected $guard = 'student';
    protected $guard_name = 'student';

    protected $table = 'students';
    protected $fillable = ['code', 'name', 'phone', 'email', 'password', 'source', 'channel', 'user_id', 'center', 'avatar', 'status', 'facebook', 'zalo', 'gender', 'birthday','type'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(Admin::class, 'user_id');
    }
    public function contact(){
        return $this->hasMany(Contact::class, 'student_id');
    }

    protected $hidden = [
        'password'
    ];

}
