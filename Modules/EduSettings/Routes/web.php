<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'student'], function () {
        Route::get('/', 'StudentController@getIndex')->middleware('permission:student_view');
        Route::get('publish', 'StudentController@getPublish')->name('student.publish')->middleware('permission:student_edit');
        Route::get('add', 'StudentController@getAdd')->middleware('permission:student_add');
        Route::post('add', 'StudentController@add')->middleware('permission:student_add');
        Route::get('delete/{id}', 'StudentController@delete')->middleware('permission:student_delete');
        Route::post('multi-delete', 'StudentController@multiDelete')->middleware('permission:student_delete');
        Route::get('search-for-select2', 'StudentController@searchForSelect2')->name('user.search_for_select2')->middleware('permission:student_view');
        Route::get('{id}', 'StudentController@getUpdate');
        Route::post('{id}', 'StudentController@update')->middleware('permission:student_edit');
    });

    Route::group(['prefix' => 'class'], function () {
        Route::get('/', 'ClassController@index')->middleware('permission:class_view');
        Route::get('add', 'ClassController@create')->middleware('permission:class_add');
        Route::post('add', 'ClassController@store')->middleware('permission:class_add');
        Route::get('delete/{id}', 'ClassController@delete')->middleware('permission:class_delete');
        Route::post('multi-delete', 'ClassController@multiDelete')->middleware('permission:class_delete');
        Route::get('search-for-select2', 'ClassController@searchForSelect2')->name('user.search_for_select2')->middleware('permission:class_view');

        Route::get('{id}', 'ClassController@update')->middleware('permission:class_view');
        Route::post('{id}', 'ClassController@update')->middleware('permission:class_edit');
    });

    Route::group(['prefix' => 'misson'], function () {
        Route::get('', 'MissonController@getIndex')->name('misson')->middleware('permission:misson_view');
        Route::get('publish', 'MissonController@getPublish')->name('misson.publish')->middleware('permission:misson_publish');
        Route::match(array('GET', 'POST'), 'add', 'MissonController@add')->middleware('permission:misson_add');
        Route::get('delete/{id}', 'MissonController@delete')->middleware('permission:misson_delete');
        Route::post('multi-delete', 'MissonController@multiDelete')->middleware('permission:misson_delete');
        Route::get('search-for-select2', 'MissonController@searchForSelect2')->name('misson.search_for_select2')->middleware('permission:misson_view');

        Route::get('{id}', 'MissonController@update')->middleware('permission:misson_view');
        Route::post('{id}', 'MissonController@update')->middleware('permission:misson_edit');
    });

    Route::group(['prefix' => 'history-misson'], function () {
        Route::get('', 'HistoryMissonController@getIndex')->name('history-misson')->middleware('permission:misson_view');
        Route::get('publish', 'HistoryMissonController@getPublish')->name('history-misson.publish')->middleware('permission:misson_publish');
        Route::match(array('GET', 'POST'), 'add', 'HistoryMissonController@add')->middleware('permission:misson_add');
        Route::get('delete/{id}', 'HistoryMissonController@delete')->middleware('permission:misson_delete');
        Route::post('multi-delete', 'HistoryMissonController@multiDelete')->middleware('permission:misson_delete');
        Route::get('search-for-select2', 'HistoryMissonController@searchForSelect2')->name('history-misson.search_for_select2')->middleware('permission:misson_view');

        Route::get('{id}', 'HistoryMissonController@update')->middleware('permission:misson_view');
        Route::post('{id}', 'HistoryMissonController@update')->middleware('permission:misson_edit');
    });

    Route::group(['prefix' => 'rank'], function () {
        Route::get('', 'RankController@getIndex')->name('student')->middleware('permission:student_view');
        Route::get('publish', 'RankController@getPublish')->name('student.publish')->middleware('permission:student_publish');
        Route::match(array('GET', 'POST'), 'add', 'RankController@add')->middleware('permission:student_add');
        Route::get('delete/{id}', 'RankController@delete')->middleware('permission:student_delete');
        Route::post('multi-delete', 'RankController@multiDelete')->middleware('permission:student_delete');
        Route::get('search-for-select2', 'RankController@searchForSelect2')->name('student.search_for_select2')->middleware('permission:student_view');

        Route::get('{id}', 'RankController@update')->middleware('permission:student_view');
        Route::post('{id}', 'RankController@update')->middleware('permission:student_edit');
    });
    Route::group(['prefix' => 'schedule'], function () {
        Route::get('', 'ScheduleController@getIndex')->name('schedule');
        Route::get('publish', 'ScheduleController@getPublish')->name('schedule.publish');
        Route::match(array('GET', 'POST'), 'add', 'ScheduleController@add');
        Route::get('delete/{id}', 'ScheduleController@delete');
        Route::post('multi-delete', 'ScheduleController@multiDelete');
        Route::get('search-for-select2', 'ScheduleController@searchForSelect2')->name('schedule.search_for_select2');
        Route::get('{id}', 'ScheduleController@update');
        Route::post('{id}', 'ScheduleController@update');
    });
});
