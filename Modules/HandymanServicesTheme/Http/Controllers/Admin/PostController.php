<?php

namespace Modules\HandymanServicesTheme\Http\Controllers\Admin;

use App\Models\Setting;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;

class PostController extends CURDBaseController
{
    protected $module = [
        'code' => 'post',
        'table_name' => 'posts',
        'label' => 'handymanservicestheme::admin.post',
        'modal' => '\Modules\HandymanServicesTheme\Models\Post',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'handymanservicestheme::admin.image', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit_trans', 'label' => 'handymanservicestheme::admin.post_name'],
            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'handymanservicestheme::list.td.multi_cat', 'label' => 'handymanservicestheme::admin.cate', 'object' => 'category_post'],
            ['name' => 'tags', 'type' => 'custom', 'td' => 'handymanservicestheme::list.td.multi_cat', 'label' => 'handymanservicestheme::admin.key', 'object' => 'tag_post'],
            ['name' => 'slug', 'type' => 'text', 'label' => 'handymanservicestheme::admin.url'],
            ['name' => 'faq', 'type' => 'status', 'label' => 'Faq'],
            ['name' => 'order_no', 'type' => 'number', 'label' => 'handymanservicestheme::admin.prioritize'],
            ['name' => 'popular', 'type' => 'status', 'label' => 'handymanservicestheme::admin.popular'],
            ['name' => 'status', 'type' => 'status', 'label' => 'handymanservicestheme::admin.status'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name_vi', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicestheme::admin.name_vi'],
                ['name' => 'name_en', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicestheme::admin.name_en'],
                ['name' => 'name_hu', 'type' => 'text', 'class' => 'required', 'label' => 'handymanservicestheme::admin.name_hu'],
                ['name' => 'multi_cat', 'type' => 'custom', 'field' => 'handymanservicestheme::form.fields.multi_cat', 'label' => 'handymanservicestheme::admin.cate_name', 'model' => \Modules\HandymanServicesTheme\Models\Category::class,
                    'object' => 'category_post', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=1', 'des' => 'handymanservicestheme::admin.first_cate'],
                ['name' => 'tags', 'type' => 'custom', 'field' => 'handymanservicestheme::form.fields.tags', 'label' => 'handymanservicestheme::admin.key_post', 'model' => \Modules\HandymanServicesTheme\Models\Category::class,
                    'object' => 'tag_post', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=2'],
                ['name' => 'intro_vi', 'type' => 'textarea', 'label' => 'handymanservicestheme::admin.intro_vi'],
                ['name' => 'intro_en', 'type' => 'textarea', 'label' => 'handymanservicestheme::admin.intro_en'],
                ['name' => 'intro_hu', 'type' => 'textarea', 'label' => 'handymanservicestheme::admin.intro_hu'],
                ['name' => 'content_vi', 'type' => 'textarea_editor', 'label' => 'handymanservicestheme::admin.content_vi'],
                ['name' => 'content_en', 'type' => 'textarea_editor', 'label' => 'handymanservicestheme::admin.content_en'],
                ['name' => 'content_hu', 'type' => 'textarea_editor', 'label' => 'handymanservicestheme::admin.content_hu'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'handymanservicestheme::admin.active','value' => 1, 'group_class' => 'col-md-3'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'handymanservicestheme::admin.priority', 'value' => 0, 'group_class' => 'col-md-3', 'des' => 'handymanservicestheme::admin.big_number'],
                ['name' => 'faq', 'type' => 'checkbox', 'label' => 'Faq', 'group_class' => 'col-md-3'],
                ['name' => 'popular', 'type' => 'checkbox', 'label' => 'handymanservicestheme::admin.popular', 'group_class' => 'col-md-3'],
//
            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'handymanservicestheme::admin.image'],
            ],

            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'handymanservicestheme::admin.slug', 'des' => 'handymanservicestheme::admin.url_link'],
                ['name' => 'meta_title', 'type' => 'text', 'label' => 'handymanservicestheme::admin.meta_title'],
                ['name' => 'meta_description', 'type' => 'text', 'label' => 'handymanservicestheme::admin.meta_description'],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'handymanservicestheme::admin.meta_keywords'],
            ],
        ],
    ];

    protected $filter = [
        'name_vi' => [
            'label' => 'handymanservicestheme::admin.name_vi',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'name_en' => [
            'label' => 'handymanservicestheme::admin.name_en',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'name_hu' => [
            'label' => 'handymanservicestheme::admin.name_hu',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'slug' => [
            'label' => 'handymanservicestheme::admin.url',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'category_id' => [
            'label' => 'handymanservicestheme::admin.cate',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'category_post',
            'model' => \Modules\HandymanServicesTheme\Models\Category::class,
            'query_type' => 'custom'
        ],
        'tags' => [
            'label' => 'handymanservicestheme::admin.key_post',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'tag_post',
            'model' => \Modules\HandymanServicesTheme\Models\Category::class,
            'query_type' => 'custom'
        ],
        'popular' => [
            'label' => 'handymanservicestheme::admin.popular',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'handymanservicestheme::admin.status',
                0 => 'handymanservicestheme::admin.no_active',
                1 => 'handymanservicestheme::admin.active',
            ],
        ],
        'status' => [
            'label' => 'handymanservicestheme::admin.status',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'handymanservicestheme::admin.status',
                0 => 'handymanservicestheme::admin.no_active',
                1 => 'handymanservicestheme::admin.active',
            ],
        ],

    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('handymanservicestheme::post.list')->with($data);
    }
    public function appendWhere($query, $request)
    {

        //  Lọc theo danh mục
        if (!is_null($request->get('category_id'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }
        if (!is_null($request->get('tags'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {

                $data = $this->getDataAdd($request);

                return view('handymanservicestheme::post.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => 'Bắt buộc phải nhập',
                    'name_en.required' => 'Bắt buộc phải nhập',
                    'name_hu.required' => 'Bắt buộc phải nhập',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('tags')) {
                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
                    }
                    #

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('handymanservicestheme::post.edit')->with($data);
            } else if ($_POST) {



                $validator = Validator::make($request->all(), [
                    'name_vi' => 'required',
                    'name_en' => 'required',
                    'name_hu' => 'required'
                ], [
                    'name_vi.required' => 'Bắt buộc phải nhập',
                    'name_en.required' => 'Bắt buộc phải nhập',
                    'name_hu.required' => 'Bắt buộc phải nhập',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());


                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('tags')) {
                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
                    }
                    #

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
