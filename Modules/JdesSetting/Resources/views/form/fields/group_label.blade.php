<select name="{{ $name }}[]" class="form-control select2-{{ $name }}" multiple>
    @php
        $html = '';
        $group_labels_selected = $group_labels = [];
        if(isset($_GET['group_label']) && !empty($_GET['group_label'])) {
            if (!empty($_GET['group_label'])) {
                $group_labels_selected = \Modules\JdesLabel\Models\GroupLabel::whereIn('id', $_GET['group_label'])->pluck('name', 'id')->toArray();

                $group_labels = \Modules\JdesLabel\Models\GroupLabel::where('parent_id', $_GET['group_label'][count($_GET['group_label']) - 1])->orderBy('name', 'asc')->pluck('name', 'id')->toArray();
            }
        } else {
            $group_labels = \Modules\JdesLabel\Models\GroupLabel::select(['id', 'name'])->whereNull('parent_id')->orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        }
    @endphp
    <option value="">Chọn Nhóm nhãn</option>
    @foreach ($group_labels_selected as $k => $v)
        <option value="{{ $k }}" selected>{{ $v }}</option>
    @endforeach
    @foreach ($group_labels as $k => $v)
        <option value="{{ $k }}">{{ $v }}</option>
    @endforeach
</select>
<script>
    $(document).ready(function() {
        $('.select2-{{ $name }}').select2();
        $('.select2-{{ $name }}').change(function () {
            loading();
            var parent_id = $(this).val();
            $.ajax({
                url: '/admin/group_label/ajax-get-data',
                data: {
                    parent_id: parent_id
                },
                type: 'GET',
                success: function (resp) {
                    stopLoading()
                    $('.select2-{{ $name }}').html(resp);
                },
                error: function () {
                    stopLoading();
                }
            });
        });
    });
</script>