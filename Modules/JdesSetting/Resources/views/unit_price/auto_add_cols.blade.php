@if($type == 'xuong_in')
    <div class="col-xs-12 item-col xuong_in_items_data item-col{{ $order_no }}" data-order_no="{{ $order_no }}"
         data-col-id="">
        <div class="col-sm-4" style="padding: 0;">
            <label>Địa chỉ</label>
            <input type="hidden" name="label_name1[{{ $order_no }}]" value="Địa chỉ">
            <input type="hidden" name="label_id1[{{ $order_no }}]"
                   value="{{ @\Modules\JdesLabel\Models\Label::where('code', 'xuong_in_address')->first()->id }}">
            <input type="hidden" name="label_type1[{{ $order_no }}]" value="0">
        </div>
        <div class="col-sm-8 tham-so no-padding">
            <div class="form-group" id="form-group-value1[{{ $order_no }}]">
                <input type="text" name="value1[{{ $order_no }}]" class="form-control " id="value1[{{ $order_no }}]"
                       value="{{ $item->address }}"
                       placeholder="Tham số">
            </div>
        </div>
        <span class="don_vi">
        </span>
        <button title="Xóa" class="btn btn-danger delete-col" type="button"><i class="fa fa-trash"
                                                                   style="margin-left: 4px;"></i></button>
    </div>

    <?php $order_no++;?>
    <div class="col-xs-12 item-col xuong_in_items_data item-col{{ $order_no }}" data-order_no="{{ $order_no }}"
         data-col-id="">
        <div class="col-sm-4" style="padding: 0;">
            <label>Mô tả</label>
            <input type="hidden" name="label_name1[{{ $order_no }}]" value="Mô tả">
            <input type="hidden" name="label_id1[{{ $order_no }}]"
                   value="{{ @\Modules\JdesLabel\Models\Label::where('code', 'xuong_in_note')->first()->id }}">
            <input type="hidden" name="label_type1[{{ $order_no }}]" value="0">
        </div>
        <div class="col-sm-8 tham-so no-padding">
            <div class="form-group" id="form-group-value1[{{ $order_no }}]">
                <input type="text" name="value1[{{ $order_no }}]" class="form-control " id="value1[{{ $order_no }}]"
                       value="{{ $item->note }}"
                       placeholder="Tham số">
            </div>
        </div>
        <span class="don_vi">
        </span>
        <button class="btn btn-danger delete-col" type="button"><i class="fa fa-trash"
                                                                   style="margin-left: 4px;"></i></button>
    </div>

    <?php $order_no++;?>
    <div class="col-xs-12 item-col xuong_in_items_data item-col{{ $order_no }}" data-order_no="{{ $order_no + 2 }}"
         data-col-id="">
        <div class="col-sm-4" style="padding: 0;">
            <label>Liên lạc</label>
            <input type="hidden" name="label_name1[{{ $order_no }}]" value="Liên lạc">
            <input type="hidden" name="label_id1[{{ $order_no }}]"
                   value="{{ @\Modules\JdesLabel\Models\Label::where('code', 'xuong_in_hotline')->first()->id }}">
            <input type="hidden" name="label_type1[{{ $order_no }}]" value="0">
        </div>
        <div class="col-sm-8 tham-so no-padding">
            <div class="form-group" id="form-group-value1[{{ $order_no }}]">
                <input type="text" name="value1[{{ $order_no }}]" class="form-control " id="value1[{{ $order_no }}]"
                       value="{{ $item->hotline }}"
                       placeholder="Tham số">
            </div>
        </div>
        <span class="don_vi">
        </span>
        <button class="btn btn-danger delete-col" type="button"><i class="fa fa-trash"
                                                                   style="margin-left: 4px;"></i></button>
    </div>
@endif