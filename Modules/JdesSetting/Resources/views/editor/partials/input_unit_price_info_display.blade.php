<?php $idRand = 'id' . $col->id . rand(1, 1000); ?>
<?php
if (@$col->label->input_text == 'select') {
    $param_display = @\Modules\JdesSetting\Models\Attribute::where('table', 'label')->where('item_id', @$col->label->id)
        ->where('key', $col->value)->where('type', 'select')->first()->value;
} else {
    $param_display = $col->value;
}
?>
<div class="item-checkbox-label-info">
    <div class="label-info-div">
        <label id="{{ $idRand }}">{{ $col->label_name }} <input type="checkbox" name="label_info[]"
                                                                class="label_info" data-checkbox_id="{{ $idRand }}"
                                                                data-label_id="{{ $col->label_id }}"
                                                                data-value="{{ $col->value }}"
                                                                data-order_id="{{ isset($order->id) ? $order->id : $col->order_id }}"
                                                                data-label_name="{{ $col->label_name }}"
                                                                data-label_parameter="{{ $col->label_parameter }}"
                                                                data-unit="{{ $col->unit }}"
                                                                data-from_type="{{ @$col->label->code == 'name' ? $col_from : $col_from . '_item' }}"
                                                                data-from_id="{{ @$col->order->id }}"
                                                                value="name"> {{ $param_display }} {{ @$col->label->unit }}
        </label>
    </div>
</div>