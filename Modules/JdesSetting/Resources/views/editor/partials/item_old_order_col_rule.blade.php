<input id="count_set_order_col_rule" value="{{ count($rules) }}" type="hidden">
<div class="col-xs-10 col-md-10 col-lg-10 col-sm-10 reset-sel">
    <label>Cột bị ảnh hưởng</label>
    <?php $rows = \Modules\JdesSetting\Models\OrderRows::where('order_id', $col->order_id)->orderBy('id', 'asc')->pluck('name', 'id');?>
    <select name="col_affect" class="form-control set_price_item">
        @foreach($rows as $id => $name)
            <option value="{{ $id }}" data-type="row" disabled>Dòng: {{ $name }}</option>
            <?php $orderCols = \Modules\JdesSetting\Models\OrderCols::where('order_id', $col->order_id)->where('order_rows_id', $id)->orderBy('id', 'asc')->get();?>
            @foreach($orderCols as $orderCol)
                <option value="{{ $orderCol->id }}" data-type="col"
                        @if(@$rules[0]->col_affect == $orderCol->id) selected @endif>
                    — {{ @$orderCol->label_name }}
                    : {{ $orderCol->label_parameter }}{{ @$orderCol->label->unit }}</option>
            @endforeach
        @endforeach
    </select>
</div>
@foreach($rules as $k => $rule)
    <input type="hidden" name="values[{{$rule->id}}][]">
    <div class="set_order_col_rule">
        <div class="calculation-container">
            <div class="col-xs-10 col-md-10 col-lg-10 col-sm-10" style="padding-right: 0">
                <?php
                $rule_value_arr = explode('|', $rule->value);
                $col_value_arr = explode('|', $col->multi_val);
                ?>
                    @foreach($col_value_arr as $col_value)
                        @if($col_value != '')
                            <div class="form-group" style="cursor: pointer">
                                <input id="col_value_{{$rule->id}}_{{$col_value}}"  name="values[{{$rule->id}}][]" value="{{ $col_value }}" type="checkbox" @if(in_array($col_value, $rule_value_arr)) checked @endif>
                                <label style="cursor: pointer" for="col_value_{{$rule->id}}_{{$col_value}}">  {{ $col_value }}</label>
                            </div>
                        @endif
                    @endforeach
                <?php
                $field = ['name' => 'unit_price_id[' . $rule->id . ']', 'type' => 'select2_model', 'class' => '', 'label' => 'Đơn giá', 'value' => $rule->unit_price_id,
                    'model' => \Modules\JdesSetting\Models\Editor::class, 'display_field' => 'name', 'where' => "type = 'unit_price' AND group_id is null"];
                ?>
                @if($field['type'] == 'custom')
                                                            @include($field['field'], ['field' => $field])
                                                        @else
                                                            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                        @endif
            </div>
            <div class="col-xs-2 col-md-2 col-lg-2 col-sm-2">
                <button data-value="{{$rule->id}}" class="btn btn-danger delete-set_price" title="Xóa" type="button"><i
                            class="fa fa-trash"></i></button>
            </div>
        </div>
    </div>
@endforeach