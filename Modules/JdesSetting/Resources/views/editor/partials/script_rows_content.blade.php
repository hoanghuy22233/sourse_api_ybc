<script>
    $(document).ready(function () {
        let getSession = sessionStorage.getItem('checkshow');
        if (getSession == 'true') {
            $('#data_ready').addClass('in');
            $('#data_ready').css('display', 'block');
            $('body').css('padding-right', '10px');
            getSession = false;
        }
        $('#rows-content table select').change(function () {
            loading();
            var col_id = $(this).data('col_id');
            var val = $(this).val();
            if ($('#data_ready').hasClass('in')) {
                getSession = true;
            }
            sessionStorage.setItem('checkshow', getSession);
            $.ajax({
                url: '/admin/editor/change-value-col',
                type: 'GET',
                data: {
                    order_id: '{{ $order->id }}',
                    col_id: col_id,
                    val: val,
                },
                success: function (resp) {
                    stopLoading();
                    if (resp.reload) {
                        location.reload();
                    }
                },
                error: function () {
                    stopLoading();
                    alert('Có lỗi xảy ra. Vui lòng load lại trang và thử lại!');
                }
            });
        });
    });

    $('.show-order_set_price').hover(function () {
        $('#setPriceCol input[name=object_id]').val($(this).data('object_id'));
        $('#setPriceCol input[name=object_type]').val($(this).data('object_type'));
        var object = $(this);
        $.ajax({
            url: '/admin/editor/html-item-old-set-price',
            type: 'GET',
            data: {
                object_id: $(this).data('object_id'),
                object_type: $(this).data('object_type')
            },
            success: function (resp) {
                object.find('.pop-title p').html(resp);
            },
            error: function () {

            }
        });
    });
</script>