<div class="navbar navbar-fix">
    <div class="navbar-inner bg-fix">
        @if($order->parent_id == null)
            <div class="pop-tool">
                <a class="brand active"
                   href="/admin/product/editor/{{ $order->id }}">{{ $order->plan }} @if($order->plan_active == 1) <img src="{{ asset('public/images_core/icons/published.png') }}"> @endif</a>
                <div class="pop-show ">
                    <div class="pop-title" style="margin-top: 3px;"></div>
                    <div class="option_more">
                        <input type="checkbox" name="plan_active_{{ $order->id }}" data-id="{{ $order->id }}" class="plan_active"
                               title="Chọn để xác nhận phương án" {{ $order->plan_active == 1 ? 'checked' : '' }}>
                        <button href="#planGroupPopup" role="button" title="Sửa tên option"
                                class="btn edit-orderGroup no-mb" data-toggle="modal"
                                type="button" data-group_id="{{ $order->id }}" data-group_name="{{ $order->plan }}"><i
                                    class="flaticon-edit"
                                    style="margin-left: 4px;"></i></button>
                        <a title="Xóa" href="/admin/editor/delete-plan/{{$order->id}}/{{$order->id}}"
                           class="icon-delete delete-warning"><i class="fa fa-trash"
                                                                 style="transform: translateY(4px);"></i></a>
                    </div>
                </div>
            </div>

            <?php $data = $order->plan_childs;?>
        @else
            <div class="pop-tool">
                <a class="brand"
                   href="/admin/product/editor/{{ $order->plan_parent->id }}">{{ $order->plan_parent->plan }} @if($order->plan_parent->plan_active == 1) <img src="{{ asset('public/images_core/icons/published.png') }}"> @endif</a>
                <div class="pop-show ">
                    <div class="pop-title" style="margin-top: 3px;"></div>
                    <div class="option_more">
                        <input type="checkbox" name="plan_active_{{ $order->plan_parent->id }}" data-id="{{ $order->plan_parent->id }}" class="plan_active"
                               title="Chọn để xác nhận phương án" {{ $order->plan_parent->plan_active == 1 ? 'checked' : '' }}>
                        <button href="#planGroupPopup" role="button" title="Sửa tên option"
                                class="btn edit-orderGroup no-mb" data-toggle="modal"
                                type="button" data-group_id="{{ $order->plan_parent->id }}"
                                data-group_name="{{ $order->plan_parent->plan }}"><i
                                    class="flaticon-edit"
                                    style="margin-left: 4px;"></i></button>
                        <a title="Xóa" href="/admin/editor/delete-plan/{{$order->plan_parent->id}}/{{$order->id}}"
                           class="icon-delete delete-warning"><i class="fa fa-trash"
                                                                 style="transform: translateY(4px);"></i></a>
                    </div>

                </div>
            </div>
            <?php $data = \Modules\JdesSetting\Models\Editor::select(['id', 'plan', 'plan_active'])->where('parent_id', $order->parent_id)->orderBy('id', 'asc')->get();?>
        @endif
        @foreach($data as $child)
            <div class="pop-tool">
                <a class="brand {{ $child->id == $order->id ? 'active' : '' }}"
                   href="/admin/product/editor/{{ $child->id }}">{{ $child->plan }} @if($child->plan_active == 1) <img src="{{ asset('public/images_core/icons/published.png') }}"> @endif</a>
                <div class="pop-show ">
                    <div class="pop-title" style="margin-top: 3px;"></div>
                    <div class="option_more">
                        <input type="checkbox" name="plan_active_{{ $child->id }}" data-id="{{ $child->id }}" class="plan_active"
                                title="Chọn để xác nhận phương án" {{ $child->plan_active == 1 ? 'checked' : '' }}>
                        <button href="#planGroupPopup" role="button" title="Sửa tên option"
                                class="btn edit-orderGroup no-mb" data-toggle="modal"
                                type="button" data-group_id="{{ $child->id }}" data-group_name="{{ $child->plan }}"><i
                                    class="flaticon-edit"
                                    style="margin-left: 4px;"></i></button>
                        <a title="Xóa" href="/admin/editor/delete-plan/{{$child->id}}/{{$order->id}}"
                           class="icon-delete delete-warning"><i class="fa fa-trash"
                                                                 style="transform: translateY(4px);"></i></a>
                    </div>

                </div>
            </div>
        @endforeach
        @include('jdessetting::editor.popup.add_plan_name')
        <a class="brand" title="Thêm phương án"
           href="/admin/editor/add-plan/{{ $order->parent_id == null ? $order->id : $order->plan_parent->id }}?order_id={{ $order->id }}">Thêm
            phương án...</a>
    </div>
</div>
