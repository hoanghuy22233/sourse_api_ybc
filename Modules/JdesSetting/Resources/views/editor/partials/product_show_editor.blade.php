<?php
if (!isset($order)) {
    $order = \Modules\JdesSetting\Models\Editor::where('product_ids', 'like', '%|' . $product_id . '|%')->first();
}
?>
@if(is_object($order))
    <div class="editor-row-content">
        <?php
        $rows = $order->rows;
        $count_cols_max = @\Modules\JdesSetting\Models\OrderCols::selectRaw('count(*) as total')->where('order_id', $order->id)->groupBy('order_rows_id')->orderBy('total', 'desc')->first()->total;
        $cols_db = $order->cols;
        $cols = [];
        if ($cols_db != null) {
            foreach ($cols_db as $col) {
                $cols[$col->order_rows_id][] = $col;
            }
        }
        $orderRowController = new \Modules\JdesSetting\Http\Controllers\Admin\OrderRowController();

        $filename = base_path() . '/' . config('jdessetting.editor_log_path') . 'editor_id_' . $order->id . $key_editor_log . '.txt';
        $editor_log = (array)json_decode(@file_get_contents($filename));
        ?>
        @foreach($rows as $row)
            <?php $count_col_in_row = 0;?>
            @if ($row->publish == 1)
                @if(isset($cols[$row->id]))
                    @foreach($cols[$row->id] as $col)
                        <a class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-primary"
                           style="margin-right: 5px;font-weight: 500;">{{ @$col->label_name }}:
                            <?php
                            $count_col_in_row++;
                            $col_label_parameter = isset($editor_log[$col->id]) ? @$editor_log[$col->id]->label_parameter : $col->label_parameter;
                            $col_value = isset($editor_log[$col->id]) ? @$editor_log[$col->id]->value : $col->value;
                            $col_multi_val = isset($editor_log[$col->id]) ? @$editor_log[$col->id]->multi_val : $col->multi_val;
                            $col_from_id = isset($editor_log[$col->id]) ? @$editor_log[$col->id]->from_id : $col->from_id;
                            ?>
                            @if ($col_value != null && $col_multi_val != null)
                                @foreach(explode('|', $col_multi_val) as $k => $v)
                                    @if($k == 0)
                                        {{ $v }}{{ @$col->label->unit }}
                                    @endif
                                @endforeach
                            @else
                                {{ $col_label_parameter }}{{ isset($col->label) ? @$col->label->unit : $col->unit }}
                            @endif
                        </a>
                    @endforeach
                @endif
            @endif
        @endforeach
        @if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false)
            <a href="/admin/product/editor/{{ $order->id }}?key_editor_log={{ $key_editor_log }}" target="_blank">Xem
                thêm</a>
        @endif
    </div>
@endif