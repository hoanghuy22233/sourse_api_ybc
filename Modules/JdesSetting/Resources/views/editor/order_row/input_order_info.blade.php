<div class="tab-pane fade in active" id="{{ $tab_code }}" style="padding: 5px 15px">
    <?php
    $rows = \Modules\JdesSetting\Models\OrderRows::where('order_id', $order->id)->orderBy('id', 'asc')->get();
    ?>
    @foreach($rows as $row)
        <div class="row" style="border-bottom: 1px solid #ccc;padding-left: 15px">
            <label class="text-left" style="float: left;margin-right: 10px;min-width: 80px;">{{ $row->name }}</label>
            <?php $cols = \Modules\JdesSetting\Models\OrderCols::where('order_id', $order->id)->where('order_rows_id', $row->id)->orderBy('order_no', 'asc')->get();?>
            @foreach($cols as $col)
                @include('jdessetting::editor.partials.input_unit_price_info_display', ['col' => $col, 'col_from' => 'order'])
            @endforeach
        </div>
    @endforeach
</div>
