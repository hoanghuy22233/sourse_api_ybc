<div id="setPriceCol" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="width:100%;height:100%">
            <form method="POST" action="/admin/editor/post-item-set_price">
                {!! csrf_field() !!}
                <input type="hidden" name="order_id" value="{{ $order->id }}">
                <input type="hidden" name="row_id" value="">
                <input type="hidden" name="object_id" value="">
                <input type="hidden" name="object_type" value="">
                <div class="modal-header">
                    <h4 class="modal-title" id="setPriceColLabel">Set giá trị</h4>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <div id="set_price_content">
                        <div class="set_price_old">
                        </div>
                    </div>
                    <button class="btn btn-success add-calcu" title="Thêm giá trị" id="add-item-set_price"
                            type="button"><i class="fa fa-plus"></i></button>
                    <div class="form-group">
                        <label class="col-xs-3">Đơn vị</label>
                        <div class="col-xs-7">
                            <input type="text" class="form-control" name="unit" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" type="button"
                            aria-hidden="true">Quay lại
                    </button>
                    <button class="btn btn-primary" type="submit" id="submit-set_price">Áp dụng
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('.setPriceCol_btn').click(function () {
        loading();
        $('#setPriceCol .set_price_item_new').remove(); //  Xoa cac khoi set_price cu
        $('#setPriceCol input[name=object_id]').val($(this).data('object_id'));
        $('#setPriceCol input[name=object_type]').val($(this).data('object_type'));
        $('#setPriceCol input[name=row_id]').val($(this).data('row_id'));
        $('#setPriceCol input[name=unit]').val($(this).data('unit'));
        $.ajax({
            url: '/admin/editor/item-old-set-price',
            type: 'GET',
            data: {
                order_id: '{{ $order->id }}',
                object_id: $(this).data('object_id'),
                object_type: $(this).data('object_type')
            },
            success: function (resp) {
                $('#setPriceCol .set_price_old').html(resp);
                stopLoading();
            },
            error: function () {
                stopLoading();
            }
        });
    });

    function addItemSetPrice() {
        $.ajax({
            url: '/admin/editor/add-item-set_price',
            type: 'GET',
            data: {
                order_id: '{{ $order->id }}',
            },
            success: function (resp) {
                $('#set_price_content').append(resp);
            }
        });
    }

    /*$(document).ready(function () {
        addItemSetPrice();
    });*/

    $('#setPriceCol #add-item-set_price').click(function () {
        addItemSetPrice();
    });

    $('body').on('click', '#setPriceCol .delete-set_price', function () {
        $(this).parents('.set_price_item').remove();
    });

    $('body').on('change', '#setPriceCol .set_price_item', function () {
        var type = $(this).find(":selected").data('type');
        console.log(type);
        $(this).parents('.set_price_item').find('.set_price_type').val(type);
    });
</script>