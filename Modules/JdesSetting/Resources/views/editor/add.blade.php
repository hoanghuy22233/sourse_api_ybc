@extends('admin.template')
@section('main')
    <div class="content-wrapper form-primary">
        <section class="content-header">
            <h1>
                Đơn giá
                <small>Đơn giá</small>
            </h1>
            @include('admin.common.breadcrumb')
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <form id="" method="post" action=""
                              class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="col-xs-12">
                                    <?php $field = ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên đơn giá'];?>
                                    @if($field['type'] == 'custom')
                                                            @include($field['field'], ['field' => $field])
                                                        @else
                                                            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                        @endif
                                </div>
                                <div class="col-xs-12">
                                    fdfd
                                </div>
                            </div>
                            <div class="box-footer">
                                <a type="reset" class="btn btn-s-md btn-default"
                                   href="{{ URL::to('/admin/' . $module['code']) }}">Quay lại</a>
                                <button type="reset" class="btn btn-s-md btn-primary">Reset dữ liệu</button>
                                <button type="submit" class="btn btn-info pull-right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('custom_header')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset('public/libs/select2/css/select2.min.css') }}">
@endsection
@push('scripts')
    <script type="text/javascript" src="{{ asset('public/backend/js/form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/libs/select2/js/select2.min.js') }}"></script>
@endpush