<div class="row-actions" style="    font-size: 13px;">
    <span class="edit" title="ID của bản ghi">ID: {{ @$item->id }} | </span>
    <span class="edit"><a
                href="{{ url('admin/' . $module['code'] . '/' . $item->id) }}"
                title="Sửa bản ghi này">Sửa</a> | </span>
    @if($permissions['delete'])
        <span class="trash"><a class="delete-warning"
                               href="{{ url('admin/' . $module['code'] . '/delete' . '/' . $item->id) }}"
                               title="Xóa bản ghi">Xóa</a> | </span>
    @endif
    @if(\Request::route()->getName() == 'order')
        <span class="view"><a href="/admin/{{ $module['code'] }}/duplicate/{{ $item->id }}"
                              title="Kế thừa báo giá này">Kế thừa</a></span>
    @else
        @if($permissions['add'])
            <span class="view"><a href="/admin/{{ $module['code'] }}/duplicate/{{ $item->id }}"
                                  title="Nhân đôi bản ghi này và chuyển đến trang bản ghi mới được nhân đôi">Nhân bản</a></span>
        @endif
    @endif
</div>