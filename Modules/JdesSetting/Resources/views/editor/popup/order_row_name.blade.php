<div id="orderRowName" class="modal fade" role="dialog">
    <div class="modal-dialog" style="max-width: unset;width: 80%;">
        <div class="modal-content" style="width:100%;height:100%">
            <div class="modal-header">
                <h4 class="modal-title" id="orderRowNameLabel">Sửa tên hàng</h4>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>

            <div class="modal-body no-pr">
                <div id="form_orderRowName" action="/admin/editor/{{ $order->id }}/order_row/change-name">
                    <input type="hidden" name="row_id" value="">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tên hàng</label>
                        <div class="col-sm-9">
                            <input type="text" name="row_name" class="form-control" value=""
                                   placeholder="Tên hàng">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Quay lại</button>
                <button class="btn btn-primary" id="submit_orderRowName" type="button">Lưu lại</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#submit_orderRowName').click(function () {
        loading();
        $.ajax({
            url: $('#form_orderRowName').attr('action'),
            type: 'POST',
            data: {
                row_id: $('#form_orderRowName input[name=row_id]').val(),
                row_name: $('#form_orderRowName input[name=row_name]').val(),
            },
            success: function (resp) {
                stopLoading();
                location.reload();
            },
            error: function () {
                stopLoading();
                alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
            }
        });
    });

    $('.edit-orderRowName').click(function () {
        $('div#form_orderRowName input[name=row_id]').val($(this).data('row_id'));
        $('div#form_orderRowName input[name=row_name]').val($(this).data('row_name'));
    });
</script>
