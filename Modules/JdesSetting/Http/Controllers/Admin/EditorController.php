<?php

namespace Modules\JdesSetting\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Admin;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\JdesBill\Models\Bill;
use Modules\JdesBill\Models\Order;
use Modules\JdesLabel\Models\GroupLabel;
use Modules\JdesLabel\Models\Label;
use Modules\JdesProduct\Models\Product;
use Modules\JdesSetting\Models\Editor;
use Modules\JdesSetting\Models\OrderCloneDraft;
use Modules\JdesSetting\Models\OrderColRule;
use Modules\JdesSetting\Models\OrderCols;
use Modules\JdesSetting\Models\OrderGroup;
use Modules\JdesSetting\Models\OrderRows;
use Modules\JdesSetting\Models\OrderSetPrice;
use Modules\JdesSetting\Models\UnitPriceColumn;
use Validator;

class EditorController extends CURDBaseController
{
    protected $module = [
        'code' => 'editor',
        'table_name' => 'products',
        'label' => 'Editor product',
        'modal' => '\Modules\JdesProduct\Models\Product',
        'form' => [
            'view' => 'order.edit',
            'tabs' => [
                'general' => [
                    'td' => [
                        ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên'],
                        ['name' => 'apply_user_ids', 'type' => 'select2_model', 'class' => '', 'label' => 'Client',
                            'model' => \App\Models\User::class, 'display_field' => 'name', 'multiple' => true],
                        ['name' => 'admin_id', 'type' => 'custom', 'field' => 'jdessetting::form.fields.admin_name_field', 'class' => '', 'label' => 'BillPayment',
                            'model' => \App\Models\Admin::class, 'display_field' => 'name', 'inner' => 'disabled'],
                        ['name' => 'admin_id', 'type' => 'custom', 'field' => 'jdessetting::form.fields.admin_name_field', 'class' => '', 'label' => 'Group',
                            'model' => \App\Models\Admin::class, 'display_field' => 'name', 'inner' => 'disabled'],
                        /*['name' => 'note', 'type' => 'textarea', 'class' => '', 'label' => 'Ghi chú'],
                        ['name' => 'image', 'type' => 'file_image', 'class' => '', 'label' => 'Ảnh công thức'],
                        ['name' => 'save_on', 'type' => 'select', 'options' =>
                            [
                                'Doanh nghiệp' => 'Doanh nghiệp',
                                'Cá nhân' => 'Cá nhân',
                            ], 'class' => '', 'label' => 'Lưu vào'],
                        ['name' => 'tag', 'type' => 'select', 'options' =>
                            [
                                'Giới thiệu' => 'Giới thiệu',
                                'Môi giới' => 'Môi giới',
                            ], 'class' => '', 'label' => 'Thẻ'],*/
                    ]
                ],
                'time' => [
                    'label' => 'Thời gian',
                    'td' => [
                        ['name' => 'time_date_from', 'type' => 'date', 'class' => '', 'label' => 'Từ'],
                        ['name' => 'time_date_to', 'type' => 'date', 'class' => '', 'label' => 'Đến'],
                        ['name' => 'time_description', 'type' => 'number', 'class' => '', 'label' => 'Thẻ'],
                        ['name' => 'time_description', 'type' => 'text', 'class' => '', 'label' => 'Lý do'],
                    ]
                ],
                'sale' => [
                    'label' => 'Sale',
                    'td' => [
                        ['name' => 'sale_sale', 'type' => 'number', 'class' => '', 'label' => 'Sale (%)'],
                        ['name' => 'sale_marketing', 'type' => 'select', 'class' => '', 'label' => 'Slid', 'options' => [
                            'Có' => 'Có',
                            'Không' => 'Không'
                        ]],
                        ['name' => 'sale_exc', 'type' => 'number', 'class' => '', 'label' => 'Exc (%)'],
                        ['name' => 'sale_status', 'type' => 'select', 'class' => '', 'label' => '', 'options' => [
                            1 => 'Có',
                            0 => 'Không'
                        ]],
                    ]
                ],
                'info' => [
                    'label' => 'Thông tin',
                    'td' => [
                        ['name' => 'ico', 'type' => 'file', 'class' => '', 'label' => 'Ico'],
                        ['name' => 'slid', 'type' => 'file', 'class' => '', 'label' => 'Slid'],
                        ['name' => 'description', 'type' => 'text', 'class' => '', 'label' => 'Mô tả']
                    ]
                ],
                'seo' => [
                    'label' => 'SEO',
                    'td' => [
                        ['name' => 'seo_title', 'type' => 'text', 'class' => '', 'label' => 'Title'],
                        ['name' => 'seo_met', 'type' => 'text', 'class' => '', 'label' => 'Met'],
                        ['name' => 'seo_description', 'type' => 'text', 'class' => '', 'label' => 'Mô tả'],
                    ]
                ],
                'footer' => [
                    'label' => '',
                    'td' => [
                        ['name' => 'group_product_id', 'type' => 'select_model_tree', 'class' => 'nhom-sp', 'label' => 'Nhóm SP',
                            'model' => \Modules\JdesLabel\Models\GroupLabel::class, 'where' => 'type=2', 'display_field' => 'name'],
                        ['name' => 'type', 'type' => 'select', 'class' => '', 'model' => \Modules\JdesSetting\Models\Editor::class, 'display_field' => 'type', 'label' => 'Kiểu',
                            'options' => [
                                'order_template' => 'Báo giá mẫu',
                                'order' => 'Báo giá chờ duyệt',
                                'job' => 'Công việc'
                            ]],
                        ['name' => 'factory_id', 'type' => 'select2_model', 'class' => 'select-label item-select2 xuong_in', 'model' => \App\Models\Admin::class, /*'where' => 'type="factory"',*/
                            'display_field' => 'name', 'label' => 'xưởng in',]
                    ]
                ]
            ],
        ]
    ];

    public function getEditor(Request $request, $id) {
        $product = Product::find($id);
        if (!is_object($product)) abort(404);

        $item = Editor::where('product_ids', 'like', '%|'.$product->id.'|%')->first();
        if (!is_object($item)) {
            $order_group = OrderGroup::create([
                'name' => 'Nhóm 1',
            ]);
            $item = Editor::create([
                'admin_id' => \Auth::guard('admin')->user()->id,
                'status' => 'draft',
                'is_root' => 1,
                'type' => 'order_template',
                'group_id' => $order_group->id,
                'product_ids' => '|' . $product->id . '|'
            ]);
        }
        return redirect('/admin/product/editor/' . $item->id);
    }

    public function detail(Request $request, $id)
    {
        $item = Editor::find($id);
        $data['order_id'] = $request->id;
        if (!$_POST) {
            $key_editor_log = $request->get('key_editor_log', '_admin_id_' .\Auth::guard('admin')->id());
            $filename = base_path() . '/' . config('jdessetting.editor_log_path') . 'editor_id_' . $item->id . $key_editor_log . '.txt';
            $data['editor_log'] = (array) json_decode(@file_get_contents($filename));

            $data['module'] = $this->module;
            $data['order'] = $item;
            $data['page_title'] = 'Editor của sản phẩm ' . $item->name;
            $data['page_type'] = 'update';
            return view('jdessetting::editor.view')->with($data);
        }
        
        if ($request->has('add_order_row')) {
            $order_row = OrderRows::create([
                'order_id' => $item->id
            ]);
            CommonHelper::one_time_message('success', 'Đã tạo dòng mới!');
            return redirect()->back();
        }

        return redirect()->back();
    }

    public function addPlan($id, Request $request)
    {
        $order = Editor::find($id);
        $count = Editor::where('parent_id', $id)->count();
        $order_new = $order->replicate();
        $order_new->parent_id = $order->id;
        $order_new->type = 'order';
        $order_new->plan = 'Phương án ' . ((int)$count + 2);
        $order_new->is_root = 0;
        $order_new->save();

        return redirect('/admin/product/editor/' . $order_new->id);
    }

    public function editPlan(Request $request)
    {
        $order = Editor::find($request->id);
        $order->plan = $request->name;
        $order->save();
        return redirect('/admin/product/editor/' . $order->id);
    }

    public function deletePlan($id, $order_id, Request $request)
    {
        $order = Editor::find($id);
        if ($order->parent_id == null) {
            return redirect()->back();
        }
        $parent_id = $order->parent_id;
        $parent = Editor::find($order->parent_id);
        OrderRows::where('order_id', $order->id)->delete();
        OrderCols::where('order_id', $order->id)->delete();
        OrderSetPrice::where('order_id', $order->id)->delete();
        OrderColRule::where('order_id', $order->id)->delete();
        $order->delete();
        $order_Id = Editor::find($order_id);
        if ($order_Id != NUll) {
            return redirect('/admin/product/editor/' . $order_Id->id);
        }
        return redirect('/admin/product/editor/' . $parent->id);
    }

    // getId Childs
    function getChildParent($data, $dataArray)
    {
        $pushId = [];
        foreach ($data as $key => $val) {
            unset($data[$key]);
            array_push($dataArray, $val->id);
            array_push($pushId, $val->id);
        }
//        $child = GroupLabel::whereIn('parent_id', $pushId)->get();
        return array_unique($dataArray);
    }

    // get all label
    function getAllDataLabel($dataArray, $model)
    {
        $getId = [];
        if ($model == 'Label') {
            $label = Label::where('status', 1)->where('group_label_ids', '<>', null)->pluck('group_label_ids', 'id');
        } elseif ($model == 'Order') {
            $label = Editor::where('type', 'unit_price')->where('group_label_ids', '<>', null)->pluck('group_label_ids', 'id');
        }
        foreach ($dataArray as $val) {
            foreach ($label as $key => $valIn) {
                if (in_array($val, explode('|', $valIn))) {
                    array_push($getId, $key);
                }
            }
        }
        return array_unique($getId);
    }

    // change group label
    public function changeGroupLabel(Request $request)
    {
        $dataArray = [];
        if ($request->data) {
            $name = 'Chọn nhãn';
            $notname = 'Không có nhãn';
            if ($request->data == null) {
                $labels = Label::where('status', 1)->get();
                return view('jdessetting::editor.order_row.label_ajax_byGroup', compact('labels', 'name', 'notname'));
            }
            $dataGroupLabel = GroupLabel::whereIn('id', explode(',', $request->data))->get();
            $dataArray = $this->getChildParent($dataGroupLabel, $dataArray);
            $getIdLabel = $this->getAllDataLabel($dataArray, $model = 'Label');
            $labels = Label::whereIn('id', $getIdLabel)->get();
            if (!is_object($labels)) return '';
            return view('jdessetting::editor.order_row.label_ajax_byGroup', compact('labels', 'name', 'notname'));
        }
        if ($request->data_price) {
            $name = 'Chọn đơn giá';
            $notname = 'Không có đơn giá';
            if ($request->data_price == null) {
                $labels = Editor::where('type', 'unit_price')->get();
                return view('jdessetting::editor.order_row.price_ajax_byGroup', compact('labels', 'name', 'notname'));
            }
            $dataGroupLabel = GroupLabel::whereIn('id', explode(',', $request->data_price))->get();
            $dataArray = $this->getChildParent($dataGroupLabel, $dataArray);
            $getIdLabel = $this->getAllDataLabel($dataArray, $model = 'Order');
            $labels = Editor::whereIn('id', $getIdLabel)->get();
            if (!is_object($labels)) return '';
            return view('jdessetting::editor.order_row.price_ajax_byGroup', compact('labels', 'name', 'notname'));

        }
    }


    //add unit col
    public function addUniPriceColumn(Request $request)
    {
        if ($request->ajax()) {
            if (!$request->name_col_price) {
                $name_col_price = '';
            } else {
                $name_col_price = $request->name_col_price;
            }
            if (!$request->newlabel_ids) {
                $newlabel_ids = '';
            } else {
                $newlabel_ids = '|' . implode('|', $request->newlabel_ids) . '|';
            }

            if (!$request->last_id) {
                $lastId = UnitPriceColumn::all()->last()->id + 1;
            } else {
                $lastId = $request->last_id + 1;
            }
            $getData = new stdClass();
            $getData->id = $lastId;
            $getData->name = $name_col_price;
            $getData->label_ids = $newlabel_ids;
            $getLabel = Label::all();

            return view('admin.common.td.data_unit_price_colums', compact('getData', 'getLabel'));
        }
        return redirect()->back();
    }

    public function getAjaxIdUserByBill(Request $request)
    {
        $data['admins'] = Admin::whereIn('id', explode('|', $request->getAdmin))->get();
        $data['factory'] = Admin::find($request->factory);
        return view('jdessetting::editor.popup.modal_donhang_value')->with($data);

    }

    public function saveDuplicateOrder(Request $request)
    {
        $order = Editor::find($request->order);
        $order_id = $this->duplicateOrder($order, $request->all());
        $order_new = Editor::find($order_id);
        CommonHelper::one_time_message('success', 'Duyệt báo giá thành công! Bạn đang ở báo giá chờ duyệt mới!');
        $url = '/admin/product/editor/' . $order_new->id;
        return response()->json([
            'success' => true,
            'url' => $url
        ]);
    }

    public function duplicate(Request $request, $id)
    {
        try {
            $item = $this->model->find($id);
            $order_new = $this->duplicateOrder($item, $request->all());
            CommonHelper::one_time_message('success', 'Kế thừa báo giá thành công!');
            $url = '/admin/product/editor/' . $order_new->id;
            return redirect($url);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', @$ex->getMessage());
            return redirect()->back();
        }
    }

    public function duplicateOrder($order, $data = [], $parent_id = null, $group_id = false)
    {
        $order_new = $order->replicate();
        if (isset($data['name'])) {
            $order_new->name = @$data['name'];
        }
        if (isset($data['user_id'])) {
            $order_new->user_id = @$data['user_id'];
        }
        if (isset($data['factory_id'])) {
            $order_new->factory_id = @$data['factory_id'];
        }
        $order_new->company_id = \Auth::guard('admin')->user()->company_id;

        $order_new->status = 'draft';
        $order_new->type = 'order';
        $order_new->parent_id = $parent_id;
        $order_new->admin_id = \Auth::guard('admin')->user()->id;

        if ($order->is_root == 1) {    //  Tạo group_id cho order gốc
            $group = OrderGroup::find($order->group_id);
            $group_new = $group->replicate();
            $group_new->save();
            $order_new->group_id = $group_new->id;
        } else {
            $order_new->group_id = $group_id;
            $group = OrderGroup::find($group_id);
        }

        $order_new->save();
        OrderCloneDraft::create([
            'order_id' => $order_new->id,
            'admin_id' => \Auth::guard('admin')->user()->id,
            'id_old' => $order->id,
            'id_new' => $order_new->id,
            'table_name' => 'orders'
        ]);
        foreach ($order->rows as $key_row => $row) {
            $row_new = $row->replicate();
            $row_new->order_id = $order_new->id;
            $row_new->save();
            OrderCloneDraft::create([
                'order_id' => $order_new->id,
                'admin_id' => \Auth::guard('admin')->user()->id,
                'id_old' => $row->id,
                'id_new' => $row_new->id,
                'table_name' => 'order_rows'
            ]);
            foreach ($row->cols as $key_col => $col) {
                $col_new = $col->replicate();
                $col_new->order_id = $order_new->id;
                $col_new->order_rows_id = $row_new->id;
                $col_new->save();
                OrderCloneDraft::create([
                    'order_id' => $order_new->id,
                    'admin_id' => \Auth::guard('admin')->user()->id,
                    'id_old' => $col->id,
                    'id_new' => $col_new->id,
                    'table_name' => 'order_cols'
                ]);
            }
        }

        //  Nhân bản các phương án con
        if ($order->parent_id == null) {
            $this->duplicateOrderChilds($order, $order_new);
        }

        //  Nhan bản  các group con
        if ($order->is_root == 1) {     //   Chi đơn hàng gốc mới nhân bản các group
            $this->duplicateOrderGroup($order, $order_new, $group, $group_new);
        }

        //  Nếu là order gốc thì tạo các bản nháp
        if ($order->is_root = 1) {
            $order_clone_draft = OrderCloneDraft::where('order_id', $order_new->id)->where('admin_id', \Auth::guard('admin')->user()->id)->get();
            $order_draft = $row_draft = $col_draft = [];
            foreach ($order_clone_draft as $v) {
                if ($v->table_name == 'orders') {
                    $order_draft[$v->id_old] = $v->id_new;
                } elseif ($v->table_name == 'order_rows') {
                    $row_draft[$v->id_old] = $v->id_new;
                } elseif ($v->table_name == 'order_cols') {
                    $col_draft[$v->id_old] = $v->id_new;
                }
            }

            //  Nhân bản order_col_rule
            $this->duplicateOrderRule($order, $order_new, $order_draft, $row_draft, $col_draft);

            //  Nhân bản order_set_price
            $this->duplicateOrderSetPrice($order, $order_new, $order_draft, $row_draft, $col_draft);

            //  Xóa toàn bộ các bản nháp
            OrderCloneDraft::where('order_id', $order_new->id)->where('admin_id', \Auth::guard('admin')->user()->id)->delete();
        }

        return $order_new;
    }

    public function duplicateOrderChilds($order, $order_new)
    {
        $order_childs = Editor::where('parent_id', $order->id)->get();
        foreach ($order_childs as $v) {
            $this->duplicateOrder($v, [], $order_new->id, $order_new->group_id);
        }
        return true;
    }

    public function duplicateOrderGroup($order, $order_new, $group, $group_new)
    {
        $group_childs = OrderGroup::where('parent_id', $group->id)->get();
        foreach ($group_childs as $group_child) {
            $group_child_new = $group_child->replicate();
            $group_child_new->parent_id = $group_new->id;
            $group_child_new->save();
            $order_child = Editor::where('group_id', $group_child->id)->whereNull('parent_id')->first();
            $this->duplicateOrder($order_child, $data = [], null, $group_child_new->id);
        }
        return true;
    }

    public function duplicateOrderRule($order, $order_new, $order_draft, $row_draft, $col_draft)
    {
        $rules = OrderColRule::where('order_id', $order->id)->get();
        foreach ($rules as $rule) {
            $rule_new = $rule->replicate();
            $rule_new->col_id = $col_draft[$rule_new->col_id];
            $rule_new->order_id = $order_draft[$rule_new->order_id];
            $rule_new->order_rows_id = $row_draft[$rule_new->order_rows_id];
            $rule_new->col_affect = $col_draft[$rule_new->col_affect];
            $rule_new->save();
        }
        return true;
    }

    public function duplicateOrderSetPrice($order, $order_new, $order_draft, $row_draft, $col_draft)
    {
        $set_prices = OrderSetPrice::where('order_id', $order->id)->get();
        foreach ($set_prices as $set_price) {
            $set_price_new = $set_price->replicate();

            if ($set_price->type == 'col') {
                $set_price_new->item_id = $col_draft[$set_price_new->item_id];
            } elseif ($set_price->type == 'row') {
                $set_price_new->item_id = $row_draft[$set_price_new->item_id];
            }

            $set_price_new->order_id = $order_new->id;

            if ($set_price->type == 'col') {
                $set_price_new->object_id = $col_draft[$set_price_new->object_id];
            } elseif ($set_price->type == 'row') {
                $set_price_new->object_id = $row_draft[$set_price_new->object_id];
            } elseif ($set_price->type == 'order') {
                $set_price_new->object_id = $order_draft[$set_price_new->object_id];
            }
            $set_price_new->save();
        }
        return true;
    }

    public function inputLabelInfo(Request $request)
    {
        $label = Label::find($request->label_id);
        $order_id = $request->order_id;
        if (!is_object($label)) return '';
        return view('jdessetting::editor.order_row.input_label_info', compact('label', 'order_id'));
    }

    public function inputUnitPriceInfo(Request $request)
    {
        $unit_price = Editor::find($request->unit_price_id);
        $tab_code = @$request->tab_code;
        $order_id = $request->order_id;
        if (!is_object($unit_price)) return '';
        return view('jdessetting::editor.order_row.input_unit_price_info', compact('unit_price', 'tab_code', 'order_id'));
    }

    public function inputOrderInfo(Request $request)
    {
        $order = Editor::find($request->order_id);
        $tab_code = $request->tab_code;
        if (!is_object($order)) return '';
        return view('jdessetting::editor.order_row.input_order_info', compact('order', 'tab_code'));
    }

    public function postEditOrderCol(Request $request)
    {
        $autoChangeOrderColUnitPrice = false;
        $order_col_data = $request->except('_token', 'col_id');
        $col = OrderCols::find($request->col_id);
        if ($col->from_type == 'unit_price' && $request->value != $col->value) {  //  Neu thay doi cot unit_price thi thay doi cac cot lien quan
            $autoChangeOrderColUnitPrice = true;
        }
        foreach ($order_col_data as $k => $v) {
            $col->{$k} = $v;
        }
        if ($autoChangeOrderColUnitPrice) {  //  Neu thay doi cot unit_price thi thay doi cac cot lien quan
            $unit_price = Editor::find($request->value);
            $this->autoChangeOrderColUnitPrice($col, $unit_price, $col->from_id);
            $col->from_id = $request->value;
            $col->label_parameter = $unit_price->name;
        }
        $col->save();
        return redirect()->back();
    }

    //  Khi thay doi nhan unit_pirce thi thay doi cac col lien quan
    public function autoChangeOrderColUnitPrice($col, $unit_price, $from_id, $file_col = [])
    {
        $cols_relation = OrderCols::where('order_id', $col->order_id)->where('from_type', 'unit_price_item')->where('from_id', $from_id)->get();  //  Lay cac cot lien quan den col nay
        foreach ($cols_relation as $col_relation) {
            //  Lay cot tuong ung voi cot $col_relation o unit_price moi
            $unit_price_col = OrderCols::where('order_id', $unit_price->id)->where('label_id', $col_relation->label_id)->first();
            if (is_object($unit_price_col)) {   //  Neu tim thay unit_price_col tuong tu thi lay gia tri cua no
                $col_relation->value = $unit_price_col->value;
                $col_relation->label_parameter = $unit_price_col->label_parameter;
                $col_relation->unit = $unit_price_col->unit;

                $file_col[$col_relation->id]['value'] = $unit_price_col->value;
                $file_col[$col_relation->id]['label_parameter'] = $unit_price_col->label_parameter;
                $file_col[$col_relation->id]['unit'] = $unit_price_col->unit;
            } else {
                $col_relation->value = null;
                $col_relation->label_parameter = '--';
                $col_relation->unit = null;

                $file_col[$col_relation->id]['value'] = null;
                $file_col[$col_relation->id]['label_parameter'] = $unit_price_col->label_parameter;
                $file_col[$col_relation->id]['unit'] = null;
            }
            $col_relation->from_id = $unit_price->id;

            $file_col[$col_relation->id]['from_id'] = $unit_price->id;


            //  Tu dong thay doi gia tri cua cac order_col lien quan den order_col nay
            $orderRowController = new OrderRowController();
            $orderRowController->autoChangeOrderColOrderSetPrice($col_relation, $file_col);
            $col_relation->save();
        }
        return $file_col;
    }

    public function getHtmlOrderCol(Request $request)
    {
        $col_id = $request->col_id;
        $col = OrderCols::find($col_id);
        return view('jdessetting::editor.order_row.popup.edit_order_col_content', compact('col'));
    }

    //  Tạo mới group order
    public function postAddGroup($id, Request $request)
    {
        $order_current = Editor::find($id);
        $group_current = OrderGroup::find($order_current->group_id);
        if ($request->group_id == null) {
            $group_new = $group_current->replicate();
            $group_new->name = $request->group_name;
            $group_new->parent_id = $group_current->parent_id == null ? $group_current->id : $group_current->parent_id;
            $group_new->save();

            $order_new = $order_current->replicate();
            $order_new->parent_id = null;
            $order_new->group_id = $group_new->id;
            $order_new->is_root = 0;
            $order_new->save();
            $href = '/admin/product/editor/' . $order_new->id;
            CommonHelper::one_time_message('success', 'Tạo group mới thành công!');
        } else {
            $group = OrderGroup::find($request->group_id);
            $group->name = $request->group_name;
            $group->save();
            $href = '/admin/product/editor/' . $order_current->id;
            CommonHelper::one_time_message('success', 'Sửa thành công!');
        }

        return response()->json([
            'status' => true,
            'msg' => 'Thành công',
            'href' => $href
        ]);
    }

    public function postAddGroupProduct($id, Request $request)
    {
        //  Nhân bản sản phẩm
        $item = $this->model->find($request->product_id);
        $order_new = $this->duplicateOrder($item, []);
        $order_new->is_root = 0;
        $order_new->save();

        //  Lấy thông tin order hiện tại
        $order_current = Editor::find($id);
        $order_root = $order_current->order_root;
        $group_root = OrderGroup::find($order_root->group_id);

        //  Trỏ các group vừa tạo vào làm con group của order cha hiện tại
        $group_root_new = OrderGroup::find($order_new->group_id);
        $group_root_new->parent_id = $group_root->id;
        $group_root_new->save();
        OrderGroup::where('parent_id', $group_root_new->id)->update([
            'parent_id' => $group_root->id
        ]);

        //  Nếu order hiện tại trống thì xóa
        /*if (OrderCols::where('order_id', $id)->count() == 0) {
            CommonHelper::one_time_message('success', 'Tạo group mới thành công!');
            return $this->deleteOrderGroup($id, $order_current->group_id);
        }*/

        $href = '/admin/product/editor/' . $order_new->id;
        CommonHelper::one_time_message('success', 'Tạo group mới thành công!');

        return response()->json([
            'status' => true,
            'msg' => 'Thành công',
            'href' => $href
        ]);
    }

    public function deleteOrderGroup($order_id, $order_group_id)
    {
        $order = Editor::find($order_id);
        $order_group = OrderGroup::find($order_group_id);
        $group_parent_id = $order_group->parent_id;
        if ($group_parent_id == null) {  //  Xóa group root
            //  Lấy group khác làm group root
            $group_root_new = OrderGroup::where('parent_id', $order_group->id)->orderBy('id', 'asc')->first();
            if (is_object($group_root_new)) {
                $group_root_new->parent_id = null;
                $group_root_new->save();
                OrderGroup::where('parent_id', $order_group->id)->where('id', '!=', $group_root_new->id)->update(['parent_id' => $group_root_new->id]);
                $group_parent_id = $group_root_new->id;

                //  Lấy order khác làm order root
                Editor::where('group_id', $group_root_new->id)->whereNull('parent_id')->update(['is_root' => 1]);
            } else {
                $group_parent_id = false;
            }
        }
        $orders = Editor::where('group_id', $order_group_id);
        OrderRows::whereIn('order_id', $orders->pluck('id'))->delete();
        OrderCols::whereIn('order_id', $orders->pluck('id'))->delete();
        OrderSetPrice::whereIn('order_id', $orders->pluck('id'))->delete();
        OrderColRule::whereIn('order_id', $orders->pluck('id'))->delete();
        $orders->delete();
        $order_group->delete();

        CommonHelper::one_time_message('success', 'Xóa group thành công!');
        if ($order->group_id == $order_group_id) {  //  Nếu đang ở màn hình group bị xóa thì chuyển đến group bên cạnh
            if (!$group_parent_id) {    //  Nếu đã xóa hết thì quay về trang danh sách
                return redirect('/admin/order');
            }
            $order_redirect = Editor::select(['id', 'product_id'])->where('group_id', $group_parent_id)->orderBy('id', 'asc')->first();
            if (is_object($order_redirect)) {
                return redirect('/admin/product/editor/' . $order_redirect->id);
            }
        }
        return redirect()->back();
    }

    public function approval(Request $request)
    {
        $order = Editor::find($request->id);
        $order_root = $order->order_root;
        $order_root->status = 'pending';
        $order_root->save();
        return response()->json([
            'status' => true,
            'msg' => 'Đã gửi yêu cầu duyệt thành công!'
        ]);
    }

    public function getOrderTemplateOption(Request $request)
    {
        $orders = Editor::select(['id', 'name'])->whereRaw($this->whereRaw)->where('group_product_id', $request->group_product_id)->get();
        $html = '';
        foreach ($orders as $v) {
            $html .= '<option value="' . $v->id . '">' . $v->name . '</option>';
        }
        return $html;
    }

    public function getPrint($id) {
        $data['order'] = Editor::find($id);
        $data['page_title'] = 'Template';
        return view('jdessetting::editor.print')->with($data);
    }

    public function activePlan(Request $request) {
        $order = Editor::find($request->id);
        if ($order->plan_active == 0) {
            $order->plan_active = 1;
        } else {
            $order->plan_active = 0;
        }
        $order->save();

        $msg = $order->plan_active == 1 ? 'Đã xác nhận phương án' : 'Đã hủy phương án';
        CommonHelper::one_time_message('success', $msg);
        return response()->json([
            'status' => true,
            'msg' => $msg
        ]);
    }
}
