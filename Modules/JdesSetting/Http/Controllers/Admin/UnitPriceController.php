<?php

namespace Modules\JdesSetting\Http\Controllers\Admin;

use App\Models\Admin;
use Modules\JdesSetting\Models\Attribute;
use Modules\JdesLabel\Models\Label;
use Modules\JdesSetting\Models\Editor;
use Modules\JdesSetting\Models\OrderCols;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;

class UnitPriceController extends CURDBaseOldController
{
    protected $whereRaw = "status in ('pending', 'active') AND type = 'unit_price' AND group_id is null";

    protected $module = [
        'code' => 'unit_price',
        'table_name' => 'editors',
        'label' => 'Đơn giá mẫu',
        'modal' => '\Modules\JdesSetting\Models\Editor',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'updated_at', 'type' => 'date_vi', 'label' => 'Cập nhật lần cuối'],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'price' => [
            'label' => 'Giá',
            'type' => 'number',
            'query_type' => '='
        ],
        /*'group_label' => [
            'label' => 'Giá',
            'type' => 'group_label',
            'query_type' => 'custom'
        ],*/
        'updated_at' => [
            'label' => 'Ngày cập nhật',
            'type' => 'date',
            'query_type' => '='
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);


        return view('jdessetting::unit_price.list')->with($data);
    }

    public function getDataList(Request $request)
    {
        //  Filter
        $where = $this->filterSimple($request);
        $listItem = $this->model->whereRaw($where);
        if ($this->whereRaw) {
            $listItem = $listItem->whereRaw($this->whereRaw);
        }
        $listItem = $this->appendWhere($listItem, $request);

        //  Export
        if ($request->has('export')) {
            $this->exportExcel($request, $listItem->get());
        }

        //  Sort
        $listItem = $this->sort($request, $listItem);
        if ($request->has('limit')) {
            $data['listItem'] = $listItem->paginate($request->limit);
            $data['limit'] = $request->limit;
        } else {
            $data['listItem'] = $listItem->paginate($this->limit_default);
            $data['limit'] = $this->limit_default;
        }
        $data['page'] = $request->get('page', 1);

        $data['param_url'] = $request->all();

        //  Get data default (param_url, filter, module) for return view
        $data['module'] = $this->module;
        $data['filter'] = $this->filter;
        if ($this->whereRaw) {
            $data['record_total'] = $this->model->whereRaw($this->whereRaw);
        } else {
            $data['record_total'] = $this->model;
        }

        $data['record_total'] = $data['record_total']->whereRaw($where)->count();

        //  Set data for seo
        $data['page_title'] = $this->module['label'];
        $data['page_type'] = 'list';
        return $data;
    }

    public function appendWhere($listItem, $request) {
        if ($request->has('group_label') && !empty($request->group_label)) {
            $listItem = $listItem->where('orders.group_label_id', $request->group_label[count($request->group_label) - 1]);
        }
        return $listItem;
    }

    public function appendData($request, $data, $item = false)
    {
        /*if ($request->has('group_label_ids')) {
            $group_label_ids = '';
            foreach ($request->get('group_label_ids') as $k => $v) {
                if ($k == 0)
                    $group_label_ids .= '|' . $v . '|';
                else
                    $group_label_ids .= $v . '|';
            }
            $data['group_label_ids'] = $group_label_ids;
        }*/

        if ($request->has('tag_ids')) {
            $tag_ids = '';
            foreach ($request->get('tag_ids') as $k => $v) {
                if ($k == 0)
                    $tag_ids .= '|' . $v . '|';
                else
                    $tag_ids .= $v . '|';
            }
            $data['tag_ids'] = $tag_ids;
        }


        if ($request->has('apply_user_ids')) {
            $apply_user_ids = '';
            foreach ($request->get('apply_user_ids') as $k => $v) {
                if ($k == 0)
                    $apply_user_ids .= '|' . $v . '|';
                else
                    $apply_user_ids .= $v . '|';
            }
            $data['apply_user_ids'] = $apply_user_ids;
        }

        if ($request->has('apply_admin_ids')) {
            $apply_admin_ids = '';
            foreach ($request->get('apply_admin_ids') as $k => $v) {
                if ($k == 0)
                    $apply_admin_ids .= '|' . $v . '|';
                else
                    $apply_admin_ids .= $v . '|';
            }
            $data['apply_admin_ids'] = $apply_admin_ids;
        }

        if ($request->has('apply_group_user_id')) {
            $apply_group_user_id = '';
            foreach ($request->get('apply_group_user_id') as $k => $v) {
                if ($k == 0)
                    $apply_group_user_id .= '|' . $v . '|';
                else
                    $apply_group_user_id .= $v . '|';
            }
            $data['apply_group_user_id'] = $apply_group_user_id;
        }
        $data['status'] = 'pending';
        $data['group_label_id'] = $request->group_label_id;
        return $data;
    }

    public function add(Request $request)
    {
        $order = Editor::create([
            'admin_id' => \Auth::guard('admin')->user()->id,
            'type' => 'unit_price',
            'status' => 'pending',
        ]);
        return redirect('/admin/unit_price/' . $order->id . '?action=create');
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);
        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            $data['order_id'] = $request->id;
            return view('jdessetting::unit_price.edit')->with($data);
        } else {

            if ($request->has('duplicate') && $request->duplicate == 'Nhân bản') {     //  Nhan ban unit_price
                $new_unit_price = $this->duplicateOrder($item);
                CommonHelper::one_time_message('success', 'Nhân bản đơn giá thành công! Bạn đang ở đơn giá mới.');
                return redirect('/admin/unit_price/' . $new_unit_price->id);
            }
//            dd($request->all());
            $label_name_id = @Label::where('code', 'name')->first()->id;
            $order_id = $request->id;
            $data = $this->appendData($request, [], $item);

            foreach ($data as $k => $v) {
                $item->$k = $v;
            }
            $item->save();

            //  Lay cac row
            $order_cols_insert = [];
            for ($column = 1; $column <= 3; $column++) {
                if ($request->get('value' . $column) != null && !empty($request->get('value' . $column))) {
                    $order_no = 0;
//                    dd(count($request->get('value' . $column)));
                    foreach ($request->get('value' . $column) as $k => $value) {
                        $label_id = @$request->get('label_id' . $column)[$k];
                        $label = Label::find($label_id);
                        if (is_array($request->get('value' . $column)[$k])) { //  Neu la kieu nhan multi label thi xu ly value lai
                            $value = '';
                            foreach ($request->get('value' . $column)[$k] as $key => $v) {
                                if ($key == 0)
                                    $value .= '|' . $v . '|';
                                else
                                    $value .= $v . '|';
                            }
                        } else {
                            $value = $request->get('value' . $column)[$k];
                        }

                        if(is_object($label) && $label->input_type == 'select') {    //  Neu label kieu select thi lay parameter != value => lay tu bang attribute
                            $parameter = @Attribute::where('table', 'label')->where('type', 'select')->where('item_id', $label_id)->where('value', $value)->first()->key;
                        } else {
                            $parameter = $value;
                        }
                        $orderCol = OrderCols::updateOrCreate([
                            'order_id' => $order_id,
                            'column' => $column,
                            'order_no' => $order_no,
                        ], [
                            'label_name' => @$request->get('label_name' . $column)[$k],
                            'label_id' => $label_id,
                            'type' => @$request->get('label_type' . $column)[$k],
                            'value' => $value,
                            'label_parameter' => $parameter
                        ]);
                        $order_no++;
                        $order_cols_insert[] = $orderCol->id;

                        $this->customUpdate($item, $request, $column, $k, $label_name_id, $value);
                    }
                }
            }
            OrderCols::where('order_id', $order_id)->whereNotIn('id', $order_cols_insert)->delete();

            CommonHelper::one_time_message('success', 'Lưu thành công!');
            return redirect('admin/unit_price/' . $order_id);
        }
    }

    public function getDataUpdate(Request $request, $item)
    {
        $data['module'] = $this->module;
        $data['result'] = $item;
        $data['page_title'] = 'Chỉnh sửa ' . $this->module['label'];
        $data['page_type'] = 'update';
        return $data;
    }

    public function duplicateOrder($order) {
        $order_new = $order->replicate();
        $order_new->save();
        foreach ($order->cols as $col) {
            $col_new = $col->replicate();
            $col_new->order_id = $order_new->id;
            $col_new->save();
        }
        return $order_new;
    }

    public function customUpdate($item, $request, $column, $k, $label_name_id, $value) {
        if (@$request->get('label_id' . $column)[$k] == $label_name_id) {        //  Neu co nhan name thi luu cung luon ten don gia
            $item->name = $value;
            $item->save();
        }
        return true;
    }

    //  Tu dong them cac col vao unit price
    public function autoAddCols(Request $request)
    {
        $item = false;
        $type = $request->type;
        if ($type == 'xuong_in') {
            $item = Admin::find($request->item_id);
            if (!is_object($item)) return '';
            $order_no = $request->order_no;
        }
        return view('jdessetting::unit_price.auto_add_cols', compact('item', 'order_no', 'type'));
    }

    public function addLabel(Request $request) {
        return view('jdessetting::unit_price.blocks.add_label')->with($request->all());
    }
}
