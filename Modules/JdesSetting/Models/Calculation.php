<?php

namespace Modules\JdesSetting\Models;

use Illuminate\Database\Eloquent\Model;

class Calculation extends Model
{
    protected $table = 'calculation';

    public $timestamps = false;

    protected $fillable = [
        'name', 'number_post', 'number_image', 'price', 'intro'
    ];
}
