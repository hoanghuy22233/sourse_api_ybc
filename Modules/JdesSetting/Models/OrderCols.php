<?php

namespace Modules\JdesSetting\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Modules\JdesLabel\Models\Label;

class OrderCols extends Model
{
    protected $table = "order_cols";

    public $timestamps = false;

    protected $fillable = [
        'label_id', 'value', 'type', 'order_id', 'order_rows_id', 'note', 'order_no', 'calculation_id', 'label_name', 'column',
        'label_parameter', 'multi_val', 'from_type', 'from_id'
    ];

    public function label()
    {
        return $this->hasOne(Label::class, 'id', 'label_id');
    }

    public function order()
    {
        return $this->belongsTo(Editor::class, 'order_id');
    }

    public function factory()
    {
        return $this->hasOne(Admin::class, 'id', 'value');
    }

    public function row()
    {
        return $this->belongsTo(OrderRows::class, 'order_rows_id');
    }
    public function rules() {
        return $this->hasMany(OrderColRule::class, 'col_id', 'id');
    }

}
