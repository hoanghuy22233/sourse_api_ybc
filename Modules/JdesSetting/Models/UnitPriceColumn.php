<?php

namespace Modules\JdesSetting\Models;

use Illuminate\Database\Eloquent\Model;

class UnitPriceColumn extends Model
{
    protected $table = 'unit_price_columns';
    protected $fillable = ['name', 'label_ids', 'role_id'];
    public $timestamps = false;
}
