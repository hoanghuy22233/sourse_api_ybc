<?php

namespace Modules\JdesSetting\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\JdesBill\Models\Bill;

class JdesSettingServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Cấu hình menu trái
            $this->rendAsideMenu();

            //  Render ra khối xưởng in ở màn hình sửa bill
            $this->rendBlockFactoryInBillDetail();

            $this->registerPermission();
        }
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('jdessetting::partials.aside_menu.aside_menu_dashboard_after');
        }, 3, 1);
    }

    public function rendBlockFactoryInBillDetail() {
        \Eventy::addFilter('jdesbill.bill_info', function($bill_id) {
            $bill = Bill::find($bill_id);
            print view('jdessetting::partials.bill.block_factory')->with(['bill' => $bill]);
        }, 1, 1);
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['factory_view', 'factory_add', 'factory_edit', 'factory_delete', 'factory_publish',
                'unit_price_view', 'unit_price_add', 'unit_price_edit', 'unit_price_delete', 'unit_price_publish',
               ]);
            return $per_check;
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('jdessetting.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'jdessetting'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/jdessetting');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/jdessetting';
        }, \Config::get('view.paths')), [$sourcePath]), 'jdessetting');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/jdessetting');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'jdessetting');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'jdessetting');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
