<?php

namespace Modules\EworkingCompany\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use App\Models\RoleAdmin;
use Auth;
use DB;
use Illuminate\Http\Request;
use Modules\EworkingAdmin\Http\Controllers\Admin\AdminController;
use Modules\EworkingAdmin\Models\Admin;
use Modules\EworkingCompany\Http\Requests\CreateCompanyRequest;
use Modules\EworkingCompany\Models\Company;
use Modules\EworkingInviteCompany\Models\InviteHistory;
use Validator;

class CompanyController extends CURDBaseController
{
    protected $orderByRaw = 'exp_date desc, id desc';

    protected $module = [
        'code' => 'company',
        'table_name' => 'companies',
        'label' => 'Công ty',
        'modal' => '\Modules\EworkingCompany\Models\Company',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Logo', 'sort' => true],
//                ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'short_name', 'type' => 'text_edit', 'label' => 'Tên', 'sort' => true],
            ['name' => 'tel', 'type' => 'text', 'label' => 'Điện thoại', 'sort' => true],
            ['name' => 'email', 'type' => 'text', 'label' => 'email', 'sort' => true],
            ['name' => 'exp_date', 'type' => 'date_vi', 'label' => 'Ngày hết hạn', 'sort' => true],
            ['name' => 'account_max', 'type' => 'text', 'label' => 'Số người sử dụng'],
            ['name' => 'status', 'type' => 'custom', 'td' => 'eworkingcompany::list.td.status', 'label' => 'Trạng thái'],
//                ['name' => 'admin_id', 'type' => 'company_admin_ids', 'label' => 'Nhân viên', 'display_field' => 'display_name'],
//                ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'short_name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên ngắn gọn', 'group_class' => 'col-md-6'],
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên đầy đủ', 'group_class' => 'col-md-6'],
                ['name' => 'admin_id', 'type' => 'custom', 'field' => 'eworkingcompany::form.fields.select2_ajax_model_admin', 'label' => 'Email chủ sở hữu công ty', 'model' => \App\Models\Admin::class,
                    'object' => 'admin', 'display_field' => 'email'],
                ['name' => 'image', 'type' => 'file_image', 'label' => 'Logo'],
                ['name' => 'msdn', 'type' => 'text', 'label' => 'Mã số doanh nghiệp', 'group_class' => 'col-md-6'],
                ['name' => 'link', 'type' => 'text', 'label' => 'Website', 'group_class' => 'col-md-6'],
                ['name' => 'intro', 'type' => 'textarea_editor', 'label' => 'Vài lời giới thiệu'],
                ['name' => 'tel', 'type' => 'text', 'label' => 'Số điện thoại', 'group_class' => 'col-md-6'],
                ['name' => 'email', 'type' => 'text', 'label' => 'Email', 'group_class' => 'col-md-6'],
                ['name' => 'skype', 'type' => 'text', 'label' => 'Skype', 'group_class' => 'col-md-6'],
                ['name' => 'google_map', 'type' => 'text', 'label' => 'Bản đồ', 'group_class' => 'col-md-6'],
                ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ'],
                ['name' => 'fanpage', 'type' => 'text', 'label' => 'Fanpage'],
                ['name' => 'status', 'type' => 'custom', 'field' => 'eworkingcompany::form.fields.status', 'label' => 'Kích hoạt'],
            ],
            'config_tab' => [
                ['name' => 'before_deadline', 'type' => 'number', 'label' => 'Số ngày thông báo trước Deadline', 'group_class' => 'col-md-6'],
                ['name' => 'after_deadline', 'type' => 'number', 'label' => 'Số ngày thông báo sau Deadline', 'group_class' => 'col-md-6'],
            ],
            'service_info_tab' => [
                ['name' => 'account_max', 'type' => 'number', 'label' => 'Số thành viên tôi đa', 'inner' => 'disabled', 'group_class' => 'col-md-6'],
                ['name' => 'exp_date', 'type' => 'datetimepicker', 'label' => 'Ngày hết hạn', 'inner' => 'disabled',
                    'date_format' => 'd-m-Y', 'group_class' => 'col-md-6'],
            ],
        ]
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'email' => [
            'label' => 'Email',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'tel' => [
            'label' => 'Số điện thoại',
            'type' => 'text',
            'query_type' => '='
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('eworkingcompany::list')->with($data);
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('eworkingcompany::add')->with($data);
            } else if ($_POST) {

                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {

                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    $data['admin_id'] = $request->admin_id == null ? \Auth::guard('admin')->user()->id : $request->admin_id;
                    $data['slug'] = $this->renderSlug(isset($request->id) ? $request->id : false, $request->slug);
                    $data['status'] = $request->has('status') ? 1 : 0;
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->initializeInitialDataForNewCompany($this->model, $data['admin_id']);
                        try {
                            \Eventy::action('company.register', $this->model);
                        } catch (\Exception $ex) {

                        }

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }


                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {
            $item = $this->model->find($request->id);
            //  Chỉ sửa được liệu công ty mình đang vào
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->id . '|') === false && !CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'super_admin')) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
                return back();
            }
            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('eworkingcompany::edit')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    unset($data['account_max']);
                    unset($data['exp_date']);

                    if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'super_admin')) {
                        try {
                            unset($data['status']);
                        } catch (\Exception $ex) {

                        }
                    }
//                    $data['slug'] = $this->renderSlug(isset($request->id) ? $request->id : false, $request->slug);
                    $data['status'] = $request->has('status') ? 1 : 0;
                    #
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    //Thoát khỏi công ty
    public function logout($id)
    {
        $admin = \Auth::guard('admin')->user();
        $this->updateFieldCompanyIds($admin, $id, 'delete');

        RoleAdmin::where('admin_id', \Auth::guard('admin')->user()->id)->where('company_id', $id)->delete();
        CommonHelper::one_time_message('success', 'Đã thoát khỏi công ty!');
        return back();
    }

    public function delete(Request $request)
    {
        try {
            $permissions_company = \DB::table('permissions')
                ->join('permission_role', 'permission_role.permission_id', '=', 'permissions.id')
                ->join('role_admin', 'permission_role.role_id', '=', 'role_admin.role_id')
                ->whereIn('permissions.name', ['company_delete'])
                ->where(function ($query) use ($request) {
                    $query->orWhere('role_admin.company_id', $request->id);
                    $query->orWhereNull('role_admin.company_id');
                })
                ->where('role_admin.admin_id', \Auth::guard('admin')->user()->id)
                ->pluck('permissions.name')->toArray();

            if (!in_array('company_delete', $permissions_company)) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa !');
                return back();
            }
            $item = $this->model->find($request->id);
            $item->delete();
            $this->updateFieldCompanyIds(\Auth::guard('admin')->user(), $request->id, 'delete');
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function deleteByAdmin(Request $request)
    {
        try {
            $permissions_company = \DB::table('permissions')
                ->join('permission_role', 'permission_role.permission_id', '=', 'permissions.id')
                ->join('role_admin', 'permission_role.role_id', '=', 'role_admin.role_id')
                ->whereIn('permissions.name', ['company_delete'])
                ->where(function ($query) use ($request) {
                    $query->orWhere('role_admin.company_id', $request->id);
                    $query->orWhereNull('role_admin.company_id');
                })
                ->where('role_admin.admin_id', \Auth::guard('admin')->user()->id)
                ->pluck('permissions.name')->toArray();

            if (!in_array('company_delete', $permissions_company)) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
                return back();
            }

            if (!\Auth::guard('admin')->attempt(['email' => \Auth::guard('admin')->user()->email, 'password' => trim($request['password'])])) {
                CommonHelper::one_time_message('error', 'Nhập sai mật khẩu!');
                return back();
            }

            $item = $this->model->find($request->id);
            $item->delete();

            $this->updateFieldCompanyIds(\Auth::guard('admin')->user(), $request->id, 'delete');

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return back();
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return back();
        }
    }

    public function multiDelete(Request $request)
    {

        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
                CommonHelper::one_time_message('success', 'Xóa thành công!');
            }
            return response()->json([
                'status' => true,
//                'msg' => 'Xóa thành công!'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    /*public function appendWhere($query, $request)
    {
        $query = $query->where('id', \Auth::guard('admin')->user()->last_company_id);
        return $query;
    }*/

    public function switchCompany($id)
    {
        $company = Company::find($id);

        //  Kiểm tra công ty có tồn tại không
        if (!is_object($company)) {
            CommonHelper::one_time_message('error', 'Công ty này đã bị xóa!');
            return back();
        }

        //  Kiểm tra xem công ty này có bị admin khóa không
        if ($company->status == 0) {
            CommonHelper::one_time_message('error', 'Công ty này đang bị admin khóa!');
            return back();
        }

        //  Kiểm tra xem có ở cty đó không
        if (RoleAdmin::where('company_id', $id)->where('admin_id', Auth::guard('admin')->user()->id)->where('status', 1)->count() == 0) {
            CommonHelper::one_time_message('error', 'Bạn không ở trong công ty này!');
            return back();
        }

        //  Kiểm tra xem bạn có bị chặn khỏi công ty này không
        if (@RoleAdmin::where('admin_id', Auth::guard('admin')->user()->id)->where('company_id', $id)->first()->status != 1) {
            CommonHelper::one_time_message('error', 'Bạn bị khóa ở công ty này!');
            return back();
        }

        Auth::guard('admin')->user()->last_company_id = $id;
        $class = 'error';
        $message = 'Thất bại';
        if (\Auth::guard('admin')->user()->save()) {
            $class = 'success';
            $message = 'Đã chuyển công ty!';
        }
        CommonHelper::one_time_message($class, $message);
        return back();
    }

    /*
     * Cập nhật cột company_ids cho bảng admin
     * type = add => thêm company_id vào. type = 'delete' => xóa company_id đi
     * */
    public function updateFieldCompanyIds($admin, $company_id, $type = 'add')
    {
        if ($type == 'add') {
            if (strpos($admin->company_ids, '|' . $company_id . '|') === false) {
                $company_id_arr = explode('|', $admin->company_ids);
                $company_id_arr[] = $company_id;

                $company_ids = '|';
                foreach ($company_id_arr as $v) {
                    if ($v != '') {
                        $company_ids .= $v . '|';
                    }
                }

                $admin->company_ids = $company_ids == '|' ? null : $company_ids;
                $admin->last_company_id = $company_id;
                $admin->save();
            }
        } else {        //  type = delete
            $company_id_arr = explode('|', $admin->company_ids);
            if (in_array($company_id, $company_id_arr)) {
                $company_ids = '|';
                foreach ($company_id_arr as $v) {
                    if ($v != '' && $v != $company_id) {
                        $company_ids .= $v . '|';
                        $last_company_id = $v;
                    }
                }
                $admin->company_ids = $company_ids == '|' ? null : $company_ids;
                $admin->last_company_id = $company_ids == '|' ? null : $last_company_id;
                $admin->save();
            }
        }
        return true;
    }

    /*
     * Tạo dữ liệu mặc định cho 1 công ty mới
     * - Tạo quyền cao nhất và gán quyền đó cho người tạo công ty
     * */
    public function initializeInitialDataForNewCompany($company, $admin_id = false)
    {

        if ($admin_id) {
            $admin = Admin::find($admin_id);
        } else {
            $admin = \Auth::guard('admin')->user();
        }

        //  Gán tài khoản vào công ty mới tạo
        $adminController = new AdminController();
        $adminController->assignMembersToCompany($admin, $role_id = false, $company, $company_id = false);

        return true;
    }

    public function getCreateCompany(Request $request)
    {
        $data['page_title'] = 'Tạo mới công ty';
        $data['invites'] = InviteHistory::where('email', \Auth::guard('admin')->user()->email)->where('status', 0)->get();
//dd($data['invites']);
        return view('eworkingcompany::create_company', $data);
    }

    public function postCreateCompany(CreateCompanyRequest $request)
    {
        try {
            if ($request->hasFile('img')) {
                // Nếu đã chọn ảnh
                // Tạo đường dẫn tạm thời
                $file = $request->img;
                // Tạo slug tạm thời theo tên ảnh và dưới định dạng ảnh
                $filename = str_slug($request->name_company) . '.' . $file->getClientOriginalExtension();
                move_uploaded_file($file, public_path() . '/filemanager/userfiles/' . $filename);
            }
            DB::beginTransaction();
//        Tạo công ty
            $company = Company::create([
                'name' => $request->name_company,
                'short_name' => $request->short_name_company,
                'email' => $request->your_email,
                'tel' => $request->tel,
                'admin_id' => \Auth::guard('admin')->user()->id,
                'link' => $request->website,
                'msdn' => $request->code_company,
                'address' => $request->address,
                'intro' => $request->info,
                'image' => isset($filename) ? $filename : null,
                'slug' => $this->renderSlug(isset($request->id) ? $request->id : false, $request->slug)
            ]);

            // Khởi tạo dữ liệu mới cho 1 công ty
            $this->initializeInitialDataForNewCompany($company);
            try {
                \Eventy::action('company.register', $company);
            } catch (\Exception $ex) {
            }

            DB::commit();
            CommonHelper::one_time_message('success', $company->short_name . ' đã được tạo');
            return redirect('/admin/dashboard');
        } catch (\Exception $ex) {
            DB::rollBack();
//            CommonHelper::one_time_message('error', 'Có lỗi xảy ra, xin mời liên hệ kĩ thuật !');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect('/admin/dashboard');
        }
    }

    public function getAdmins($company_id)
    {
        $data = \DB::table('admin')
            ->join('role_admin', 'admin.id', '=', 'role_admin.admin_id')
            ->selectRaw('admin.name, admin.id, admin.image')
            ->where('role_admin.status', 1)
            ->where('role_admin.company_id', $company_id)
            ->get();
        return $data;
    }

    public function getCreateCompanyByAdmin()
    {
        $data['module'] = $this->module;
        $data['page_title'] = 'Tạo mới' . $this->module['label'];
        $data['page_type'] = 'create';
        return view('eworkingcompany::create_company_by_admin', $data);
    }

    public function postCreateCompanyByAdmin(CreateCompanyRequest $request)
    {
        if ($request->hasFile('img')) {
            // Nếu đã chọn ảnh
            // Tạo đường dẫn tạm thời
            $file = $request->img;
            // Tạo slug tạm thời theo tên ảnh và dưới định dạng ảnh
            $filename = str_slug($request->name_company) . '.' . $file->getClientOriginalExtension();
            move_uploaded_file($file, public_path() . '/filemanager/userfiles/' . $filename);
        }

//        Tạo công ty
        $company = Company::create([
            'name' => $request->name_company,
            'short_name' => $request->short_name_company,
            'slug' => str_slug($request->short_name_company),
            'email' => $request->email,
            'tel' => $request->tel,
            'admin_id' => $request->admin_id,
            'link' => $request->website,
            'msdn' => $request->msdn,
            'address' => $request->address,
            'intro' => $request->info,
            'image' => isset($filename) ? $filename : null
        ]);
//        $this->initializeInitialDataForNewCompany($company);
//        \Eventy::action('company.register', $company);

        CommonHelper::one_time_message('success', $company->short_name . ' đã được tạo');
        return redirect('/admin/company');
    }

}
