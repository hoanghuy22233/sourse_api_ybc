<?php

namespace Modules\EworkingCompany\Http\Middleware;

use Closure;
use Modules\EworkingCompany\Models\Company;

class GetCompany
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $comSlug = explode('/', $_SERVER['REQUEST_URI'])[1];
        $company = Company::where('slug', $comSlug)->first();
        if (is_object($company)) {
            view()->share('company', $company);
        } else {
            view()->share('company', null);
        }
        return $next($request);
    }
}
