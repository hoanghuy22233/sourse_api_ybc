@if(in_array('bill_view', $permissions))
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/bill"
                                                      class="kt-menu__link "><span
                    class="kt-menu__link-icon"><i class="flaticon-list"></i>
            </span><span class="kt-menu__link-text">Hóa đơn</span></a></li>
@endif
@if(in_array('card_view', $permissions))
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/card"
                                                      class="kt-menu__link "><span
                    class="kt-menu__link-icon"><i class="flaticon-price-tag"></i>
            </span><span class="kt-menu__link-text">Thẻ</span></a></li>
@endif