@extends(config('core.admin_theme').'.template')
@section('main')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <form action="/admin/dashboard/search" method="get" class="">
        <div class="kt-subheader  kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">

                    <h3 class="kt-subheader__title"></h3>

                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>

                    <span class="kt-subheader__desc">Từ ngày</span>

                    <input class="kt-subheader__desc" type="date" name="date_start" >



                    <h3 class="kt-subheader__title"></h3>

                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>

                    <span class="kt-subheader__desc">Đến ngày</span>

                    <input class="kt-subheader__desc" type="date" name="date_end" >
                    <button type="submit" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
                        Lọc
                    </button>
                </div>

            </div>
        </div>
        </form>
        <div class="row" style="margin-top: 70px">
            <div class="col-xs-12 col-lg-12 col-xl-12 col-lg-12 order-lg-1 order-xl-1">
                <!--begin:: Widgets/Finance Summary-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Tổng quan
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget12">
                            <div class="kt-widget12__content">
                                <div class="kt-widget12__item thong_ke_so">
                                    <div class="col-sm-3 kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng khách</span>
                                        <span class="kt-widget12__value">{{number_format(@$total_users, 0, '.', '')}}</span>
                                    </div>

                                    <div class="col-sm-3 kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng số thẻ</span>
                                        <span class="kt-widget12__value">{{number_format(@$total_cards, 0, '.', '')}}</span>
                                    </div>

                                    <div class="col-sm-3 kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng số tiền đã tiêu</span>
                                        <span class="kt-widget12__value">£{{number_format(@$money_used, 2, '.', ' ')}}</span>
                                    </div>

                                    <div class="col-sm-3 kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng số tiền đã nạp</span>
                                        <span class="kt-widget12__value">£{{number_format(@$money_add+@$money_used, 2, '.', ' ')}}</span>
                                    </div>

                                    <div class="col-sm-3 kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng số tiền chưa tiêu </span>
                                        <span class="kt-widget12__value">£{{number_format(@$money_add, 2, '.', ' ')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Finance Summary-->
            </div>


        </div>
    </div>

@endsection
@section('custom_head')
{{--    <link href="https://www.keenthemes.com/preview/metronic/theme/assets/global/css/components.min.css" rel="stylesheet"--}}
{{--          type="text/css">--}}
    <style type="text/css">
        .kt-datatable__cell>span>a.cate {
            color: #5867dd;
            margin-bottom: 3px;
            background: rgba(88, 103, 221, 0.1);
            height: auto;
            display: inline-block;
            width: auto;
            padding: 0.15rem 0.75rem;
            border-radius: 2px;
        }
        .paginate>ul.pagination>li{
            padding: 5px 10px;
            border: 1px solid #ccc;
            margin: 0 5px;
            cursor: pointer;        }

        .paginate>ul.pagination span{
            color: #000;
        }
        .paginate>ul.pagination>li.active{
            background: #0b57d5;
            color: #fff!important;
        }
        .paginate>ul.pagination>li.active span{
            color: #fff!important;
        }
        .kt-widget12__desc, .kt-widget12__value {
            text-align: center;
        }

        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99 list_user
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <style type="text/css">
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }


        @media(max-width: 768px) {
            .kt-widget12 .kt-widget12__content .thong_ke_so {
                display: inline-block;
            }
            .thong_ke_so .col-sm-3 {
                display: inline-block;
                width: 50%;
                float: left;
                padding: 0;
                margin-bottom: 20px;
            }
        }
    </style>

@endsection
@push('scripts')
    <script src="{{ url('public/libs/chartjs/js/Chart.bundle.js') }}"></script>
    <script src="{{ url('public/libs/chartjs/js/utils.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script>
        $(document).ready(function () {
            $('#active_service a').click(function (event) {
                event.preventDefault();
                var object = $(this);
                $.ajax({
                    url: '/admin/service_history/ajax-publish',
                    data: {
                        id: object.data('service_history_id')
                    },
                    success: function (result) {
                        if (result.status == true) {
                            toastr.success(result.msg);
                            object.parents('tr').remove();
                        } else {
                            toastr.error(result.msg);
                        }
                    },
                    error: function (e) {
                        console.log(e.message);
                    }
                })
            });




        })
    </script>
    <script>
        var lineChartData = {
            labels: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7','Tháng 8','Tháng 9','Tháng 10','Tháng 11','Tháng 12'],
            datasets: [{
                label: 'Số lượng hóa đơn',
                borderColor: window.chartColors.red,
                backgroundColor: window.chartColors.red,
                fill: false,
                data: [
                //    dữ liệu của số lượng hóa đơn
                    <?php $total_bill_month=[];
                    for ($i=0;$i<12;$i++){
                        $month = strftime('%m',strtotime(strtotime(date('01')) . " +".$i." month"));
                        $total_bill_month[]= \Modules\CardBill\Models\Bill::select('id')->where('created_at','like','%-'.$month.'-%')
                            ->where('created_at','like',date('Y').'-%')->get()->count();
                        echo($total_bill_month[$i].",");
                    }
                    ?>

                ],
                yAxisID: 'y-axis-1',
            }, {
                label: 'Số tiên thu được',
                borderColor: window.chartColors.blue,
                backgroundColor: window.chartColors.blue,
                fill: false,
                data: [
                //    dữ liệu số tiên thu được theo tháng
                    <?php $total_price1=[];
                        for ($i=0;$i<12;$i++){
                            $month = strftime('%m',strtotime(strtotime(date('01')) . " +".$i." month"));
                            $total_price1[]= \Modules\CardBill\Models\Bill::where('created_at','like','%-'.$month.'-%')->where('created_at','like',date('Y').'-%')->get()->sum('total_price');
                            echo($total_price1[$i].",");
                        }
                    ?>

                ],
                yAxisID: 'y-axis-2'
            }]
        };

        window.onload = function() {
            var ctx = document.getElementById('myChart').getContext('2d');
            window.myLine = Chart.Line(ctx, {
                data: lineChartData,
                options: {
                    responsive: true,
                    hoverMode: 'index',
                    stacked: false,
                    title: {
                        display: true,
                        // text: 'Chart.js Line Chart - Multi Axis'
                    },
                    scales: {
                        yAxes: [{
                            type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                            display: true,
                            position: 'left',
                            id: 'y-axis-1',
                            ticks: {
                                beginAtZero: true,
                            }

                        }, {
                            type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                            display: true,
                            position: 'right',
                            id: 'y-axis-2',
                            ticks: {
                                beginAtZero: true,
                            },
                            // grid line settings
                            gridLines: {
                                drawOnChartArea: false, // only want the grid lines for one axis to show up
                            },
                        }],
                    }
                }
            });
        };
    </script>
@endpush

