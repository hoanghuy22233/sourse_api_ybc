<label style="cursor: pointer" for="Phương thức thanh toán">Phương thức thanh toán
<div class="kt-radio-list mt-3">
    <label class="kt-radio">
        <input type="radio" name="radio1" @if(@$receipt_method=='Thanh toán tiền mặt khi nhận hàng')checked @elseif(@$receipt_method==null)checked @endif value="Thanh toán tiền mặt khi nhận hàng"> Thanh toán tiền mặt khi nhận hàng
        <span></span>
    </label>
    <label class="kt-radio">
        <input type="radio" name="radio1" @if(@$receipt_method=='Thanh toán bằng thẻ quốc tế Visa, Master, JCB')checked @endif value="Thanh toán bằng thẻ quốc tế Visa, Master, JCB"> Thanh toán bằng thẻ quốc tế Visa, Master, JCB
        <span></span>
    </label>
   <label class="kt-radio">
        <input type="radio" name="radio1" @if(@$receipt_method=='Thẻ ATM nội địa/Internet Banking (Miễn phí thanh toán)')checked @endif value="Thẻ ATM nội địa/Internet Banking (Miễn phí thanh toán)"> Thẻ ATM nội địa/Internet Banking (Miễn phí thanh toán)
        <span></span>
    </label>
</div>
</label>