<style>
    .icon-loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% { -webkit-transform: rotate(0deg); }
        100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>

<input name="user_total_money" value="0" type="hidden">
<input name="user_tel" value="" type="hidden">
<input name="" value="" type="hidden">
<label for="{{ $field['name'] }}">{{ @$field['label'] }}</label>
<div class="col-xs-12">
    <div id="user_info"></div>
</div>
<span class="alert alert-danger validatePriceUser" style="display: none;">Tài khoản không đủ tiền</span>

<button class="btn btn-success send_confirm" style="display: none;" type="button">Gửi xác minh</button>

<div class="input-group block_confirm" style="@if(old('card_id') == null) display: none; @endif   margin-top: 10px;">
    <input style="color: #000; border-radius: 40px 0 0 40px; "
           class="form-control m-input text-color"
           name="user_tel_code" type="text" placeholder="Mã xác minh"
           value="">
    <div class="input-group-append">
        <button class="send-opt btn btn-success form-control" type="button" id="verify_user_tel">
            Xác minh
        </button>
    </div>
    <p id="msg-verifi" style="color:red; font-size: medium;margin-left: 4px;line-height: 2.5; width: 100%;"></p>
</div>
{{--{{dd($check)}};--}}
<script>
    $(document).ready(function () {
        @if(old('card_id') != null)
        $.ajax({
            url: '{{route('bill.user_info')}}',
            type: 'GET',
            data: {
                card_id: {{ old('card_id') }}
            },
            success: function (user) {
                var users = user.user_address;
                var address = '<ul>';
                for (var k in users) {
                    if (users[k]['address'] != null) {
                        address += '<li>' + users[k]['address'] + '</li>';
                    }
                }
                address += '</ul>';
                $('#user_info').html('' +
                    '<p><strong>Họ & tên:</strong> ' + user.user_name + '</p>' +
                    '<p><strong>Số điện thoại:</strong> ' + user.user_tel + '</p>' +
                    '<p><strong>Email:</strong> ' + user.user_email + '</p>' +
                    '<p><strong>Số dư:</strong> £' + user.total_money_format + '</p>' +
                    '<p><strong>Địa chỉ:</strong> ' + address);
                $('input[name=user_total_money]').val(user.total_money);
                $('input[name=user_tel]').val(user.user_tel);
                // $('#verify_user_tel').removeAttr('disabled');
                $('.send_confirm').show();
                validatePriceUser();
            },
            error: function () {
                alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại');
                validatePriceUser();
            }
        });
        @endif

        $('select[name=card_id]').change(function () {
            var card_id = $(this).val();
            $.ajax({
                url: '{{route('bill.user_info')}}',
                type: 'GET',
                data: {
                    card_id: card_id
                },
                success: function (user) {
                    var users = user.user_address;
                    var address = '<ul>';
                    for (var k in users) {
                        if (users[k]['address'] != null) {
                            address += '<li>' + users[k]['address'] + '</li>';
                        }
                    }
                    address += '</ul>';
                    $('#user_info').html('' +
                        '<p><strong>Họ & tên:</strong> ' + user.user_name + '</p>' +
                        '<p><strong>Số điện thoại:</strong> ' + user.user_tel + '</p>' +
                        '<p><strong>Email:</strong> ' + user.user_email + '</p>' +
                        '<p><strong>Số dư:</strong> £' + user.total_money_format + '</p>' +
                        '<p><strong>Địa chỉ:</strong> ' + address);
                    $('input[name=user_total_money]').val(user.total_money);
                    $('input[name=user_tel]').val(user.user_tel);
                    // $('#verify_user_tel').removeAttr('disabled');
                    $('.send_confirm').show();
                    validatePriceUser();
                },
                error: function () {
                    alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại');
                    validatePriceUser();
                }
            });
        });

        $('input[name=total_price]').keyup(function () {
            var total_price = $(this).val();
            validatePriceUser(total_price);
        });

        function validatePriceUser(total_price = false) {
            if (!total_price) {
                total_price = $('input[name=total_price]').val();
            }
            if (parseInt(total_price) > parseInt($('input[name=user_total_money]').val())) {
                $('.validatePriceUser').show();
            } else {
                $('.validatePriceUser').hide();
            }
        }


        //  Click vào nút gửi mã xác minh
        $('.send_confirm').click(function () {
            $('input[name=total_price]').attr("readonly", true);
            $(".send_confirm").attr("disabled", true).html('<div class="icon-loader" style="width: 15px; height: 15px;"></div>');
            $('.block_confirm').show();
            $.ajax({
                url: '/admin/bill/send-sms-verify',
                type: 'POST',
                data: {
                    user_tel: $('input[name=user_tel]').val(),
                    total_price: $('input[name=total_price]').val()
                },
                success: function (resp) {
                    if (resp.status == true) {
                        toastr.success(resp.msg);
                    } else {
                        toastr.error(resp.msg);
                    }
                },
                error: function () {
                    alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại');
                }
            });


            var x = setInterval(function() {
                $(".send_confirm").removeAttr("disabled").html('Gửi lại mã');
            }, 10000);
        });

        //  Click vào nút xác minh code gửi về đt
        $('#verify_user_tel').click(function () {
            $.ajax({
                url: '/admin/bill/send-verify-sms',
                type: 'GET',
                data: {
                    user_tel: $('input[name=user_tel]').val(),
                    user_tel_code: $('input[name=user_tel_code]').val(),
                },
                success: function (resp) {
                    $('#msg-verifi').html(resp.msg);
                    $('#msg-verifi').hide();
                    $('#msg-verifi').show();
                    if (resp.status == false) {
                        $('#msg-verifi').css('color', 'red');
                    } else {
                        $('#msg-verifi').css('color', 'green');
                    }
                },
                error: function () {
                    alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại');
                }
            });
        });;
    });
</script>
