<?php

namespace Modules\CardBill\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\VerifyCode;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\CardBill\Helpers\CardBillHelper;
use Modules\CardBill\Models\Bill;
use Modules\CardBill\Models\Card;
use Modules\CardBill\Models\User;
use Modules\CardUser\Models\UserAddress;
use Validator;

class BillController extends CURDBaseController
{
    protected $module = [
        'code' => 'bill',
        'table_name' => 'bills',
        'label' => 'Đơn hàng',
        'modal' => '\Modules\CardBill\Models\Bill',
        'list' => [
            ['name' => 'user_name', 'type' => 'text', 'label' => 'Khách hàng'],
            ['name' => 'card_code', 'type' => 'text', 'label' => 'Mã thẻ'],
//            ['name' => 'user_total', 'type' => 'custom', 'td' => 'cardbill::list.td.user_money', 'label' => 'Tiền trong thẻ'],
            ['name' => 'total_price', 'type' => 'custom', 'td' => 'cardbill::list.td.price_eur', 'label' => 'Số tiền'],
            ['name' => 'created_at', 'type' => 'datetime_vi', 'label' => 'Ngày mua'],
            ['name' => 'admin_id', 'type' => 'relation', 'label' => 'Người tạo', 'object' => 'admin', 'display_field' => 'name'],
//            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],
            ['name' => 'action', 'type' => 'custom', 'td' => 'cardbill::list.td.action', 'class' => '', 'label' => '#'],
        ],
        'form' => [
            'general_tab' => [
//                ['name' => 'card_code', 'type' => 'custom','field'=>'cardbill::form.fields.select2_model', 'class' => '', 'label' => 'Mã thẻ', 'model' => Card::class, 'display_field' => 'code'],
//                ['name' => 'user_id', 'type' => 'select2_ajax_model','object' => 'user_card', 'class' => '', 'label' => 'Khách hàng', 'model' => User::class, 'display_field' => 'name'],
//                ['name' => 'total_money', 'type' => 'text', 'class' => 'number_price', 'label' => 'Tổng tiền'],
                ['name' => 'total_price', 'type' => 'text', 'class' => 'required', 'label' => 'Tiền hóa đơn'],
                ['name' => 'note', 'type' => 'textarea', 'class' => '', 'label' => 'Đơn hàng', 'inner' => 'rows=10'],
//                ['name' => 'receipt_method', 'type' => 'custom', 'field' => 'cardbill::form.fields.checkbox_radio', 'class' => '', 'label' => 'Phương thức thanh toán', 'value' => 1, 'group_class' => ''],

//                ['name' => 'user_id', 'type' => 'hidden', 'class' => 'number_price', 'label' => ''],
            ],
            'info_tab' => [
                ['name' => 'card_id', 'type' => 'select2_ajax_model', 'class' => '', 'label' => 'Mã thẻ', 'model' => Card::class, 'object' => 'card', 'display_field' => 'code','where'=>'status=0'],
                ['name' => 'user_info', 'type' => 'custom', 'field' => 'cardbill::form.fields.user_info', 'class' => '', 'label' => 'Thông tin khách hàng', 'group_class' => ''],
            ],
        ],
    ];

    protected $filter = [
//        'user_id' => [
//            'label' => 'Tên khách hàng',
//            'type' => 'select2_model',
//            'display_field' => 'name',
//            'model' => User::class,
//            'object' => 'user',
//            'query_type' => '='
//        ],
        'user_name' => [
            'label' => 'Tên khách hàng',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'card_code' => [
            'label' => 'Mã thẻ',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'total_price' => [
            'label' => 'Tổng tiền',
            'type' => 'number',
            'query_type' => 'like'
        ],
        'created_at' => [
            'label' => 'Ngày đặt',
            'type' => 'date',
            'query_type' => 'custom'
        ],
        'admin_id' => [
            'label' => 'Người tạo',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'model' => User::class,
            'object' => 'admin',
            'query_type' => '='
        ],
        'user_id' => [
            'label' => 'ID khách',
            'type' => 'hidden',
            'query_type' => '='
        ],
    ];

    public function getInfoByCard(Request $request)
    {
        $user = Card::find($request->card_id)->user;
        if (!is_object($user)) {
            return response()->json([

            ]);
        }
        return response()->json(
            [
                'user_name' => $user->name,
                'user_tel' => $user->tel,
                'user_email' => $user->email,
                'total_money' => $user->total_money,
                'total_money_format' => number_format($user->total_money, 2, '.', ' '),
                'user_address' => UserAddress::where('user_id', $user->id)->get()->toArray(),
            ]
        );
    }

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);
        return view('cardbill::bill.list')->with($data);
    }

    public function add(Request $request)
    {
        try {

            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('cardbill::bill.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
//                    'name' => 'required'
                ], [
//                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['receipt_method'] = $request->radio1;
                    $card = Card::find($request->card_id);
                    $user = $card->user;

                    $tel = $user->tel;
                    if (substr($tel, 0, 1) == '0') {
                        $tel = $user->country_code . substr($tel, 1, 15);
                    }

                    //  Xác minh số điện thoại user (Kiểm tra  xem có mã xác minh nào gửi về sđt này trong 5 phút trở lại)
                    if (VerifyCode::where('object_id', $user->id)->where('type', 'user')->where('code', $request->user_tel_code)
                                ->where('tel', $tel)->where('created_at', '>=', date('Y-m-d H:i:s', time() - 300))->count() == 0) {
                        CommonHelper::one_time_message('error', 'Mã xác minh không chính xác hoặc đã hết hạn!');
                        return back()->withInput();
                    }


                    $data['card_code'] = $card->code;
                    $data['user_name'] = $user->name;
                    $data['user_id'] = $user->id;
                    $data['user_tel'] = $user->tel;
                    $data['user_email'] = $user->email;
                    $data['admin_id'] = \Auth::guard('admin')->id();
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($request->total_price <= $user->total_money) {
                        if ($this->model->save()) {
                            $user->total_money -= $this->model->total_price;
                            $user->save();

                            //  Gửi sms thanh toán đơn hàng thành công cho khách
                            CardBillHelper::sendSmsBillSuccess($user->tel, $this->model->total_price, $user->total_money);

                            CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                        } else {
                            CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                        }
                    } else {
                        CommonHelper::one_time_message('error', 'Tông tiền của bạn không đủ để thực hiện giao dịch này.Vui lòng nạp thêm tiền vào thẻ để tiếp tục!');
                        return back()->withInput();
                    }


                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('cardbill::bill.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
//                    'name' => 'required'
                ], [
//                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;
//                    $data['receipt_method'] = $request->radio1;

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache();
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache();
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false,
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            $item->delete();
            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function send_sms_verify(Request $r) {
        $respon = CardBillHelper::sendSmsVerifiPhone($r->user_tel, $r->total_price);
        unset($respon['data']);
        return response()->json($respon);
    }

    public function send_verify_sms(Request $r) {
        /*return response()->json([
            'status' => true,
            'msg' => 'Xác minh thành công'
        ]);*/

        $tel = $r->user_tel;
        $user = User::select(['id', 'country_code'])->where('tel', $tel)->first();
        if (substr($tel, 0, 1) == '0') {
            $tel = $user->country_code . substr($tel, 1, 15);
        }

        $check = VerifyCode::where('code', $r->user_tel_code)
            ->where('tel', $tel)->where('created_at', '>=', date('Y-m-d H:i:s', time() - 300))->count();

        if ($check == 0) {
            return response()->json([
                'status' => false,
                'msg' => 'Xác minh thất bại'
            ]);
        }
        return response()->json([
            'status' => true,
            'msg' => 'Xác minh thành công'
        ]);
    }
}
