<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {


    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'Admin\DashboardController@dashboard');
        Route::get('search', 'Admin\DashboardController@search');
    });

    Route::group(['prefix' => 'bill'], function () {
        Route::get('', 'Admin\BillController@getIndex')->name('bill')->middleware('permission:bill_view');
        Route::get('publish', 'Admin\BillController@getPublish')->name('bill.publish')->middleware('permission:bill_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BillController@add')->middleware('permission:bill_add');
        Route::get('delete/{id}', 'Admin\BillController@delete')->middleware('permission:bill_delete');
        Route::post('multi-delete', 'Admin\BillController@multiDelete')->middleware('permission:bill_delete');
        Route::get('search-for-select2', 'Admin\BillController@searchForSelect2')->name('bill.search_for_select2')->middleware('permission:bill_view');
        Route::get('user/get-info-by-card_id', 'Admin\BillController@getInfoByCard')->name('bill.user_info');

        Route::post('send-sms-verify', 'Admin\BillController@send_sms_verify');
        Route::get('send-verify-sms', 'Admin\BillController@send_verify_sms');

        Route::get('{id}', 'Admin\BillController@update')->middleware('permission:bill_view');
        Route::post('{id}', 'Admin\BillController@update')->middleware('permission:bill_edit');
    });
    Route::group(['prefix' => 'card'], function () {
        Route::get('', 'Admin\CardController@getIndex')->name('card')->middleware('permission:card_view');
        Route::get('publish', 'Admin\CardController@getPublish')->name('card.publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\CardController@add')->middleware('permission:card_add');
        Route::get('delete/{id}', 'Admin\CardController@delete')->middleware('permission:card_delete');
        Route::post('multi-delete', 'Admin\CardController@multiDelete')->middleware('permission:card_delete');
        Route::get('search-for-select2', 'Admin\CardController@searchForSelect2')->name('card.search_for_select2')->middleware('permission:card_view');
        Route::get('{id}', 'Admin\CardController@update')->middleware('permission:card_view');
        Route::post('{id}', 'Admin\CardController@update')->middleware('permission:card_edit');
    });

});
