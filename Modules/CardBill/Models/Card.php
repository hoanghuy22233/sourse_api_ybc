<?php

namespace Modules\CardBill\Models ;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $table = 'card';
    protected $guarded = [];

    public function user() {
        return $this->hasOne(User::class, 'card_id');
    }
    public function bill() {
        return $this->hasMany(Bill::class, 'card_id');
    }
}

