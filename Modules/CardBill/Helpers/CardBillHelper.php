<?php

namespace Modules\CardBill\Helpers;

use App\Models\AdminLog;
use App\Models\Setting;
use App\Models\VerifyCode;
use Modules\CardBill\Models\User;

class CardBillHelper
{

    public static function sendSms($tel,$sender, $message)
    {
        /*return [
            'status' => true,
            'msg' => 'Gửi thành công',
            'data' => [],
        ];*/
//        $sender = @Setting::where('type', 'send_sms')->where('name', 'sender')->first()->value;
        $settings = Setting::where('type', 'send_sms')->pluck('value', 'name')->toArray();
        $curl = curl_init();

        $body_params = [
            "from" => $sender,
            "to" => [$tel],
            "type" => "mt_text",
            "body" => $message,
            "delivery_report" => "per_recipient",
            "callback_url" => "http://kee.pa/test2.php"
        ];


        $SEND_SMS_URL = $settings['SEND_SMS_URL'];
        $TOKEN = $settings['TOKEN'];

        curl_setopt_array($curl, array(
            CURLOPT_URL => $SEND_SMS_URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($body_params),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Accept: application/json",
                "Authorization: Bearer " . $TOKEN
            ),
        ));

        $response = curl_exec($curl);
        $status = curl_getinfo($curl);
        curl_close($curl);
        if (empty($response)) {
            AdminLog::create([
                'admin_id' => @\Auth::guard('admin')->user()->id,
                'message' => json_encode($response),
                'type' => 'Gửi sms',
                'model' => $tel,
            ]);
            return [
                'status' => false,
                'msg' => 'Không thể thực hiện yêu cầu',
                'data' => $response,
            ];
        } else {
            $response = json_decode($response, true);
            if ($status['http_code'] === 201) {
                return [
                    'status' => true,
                    'msg' => 'Gửi thành công',
                    'data' => $response,
                ];
            } else {
                AdminLog::create([
                    'admin_id' => @\Auth::guard('admin')->user()->id,
                    'message' => json_encode($response),
                    'type' => 'Gửi sms',
                    'model' => $tel,
                ]);
                return [
                    'status' => false,
                    'msg' => 'Gửi thất bại',
                    'data' => $response,
                ];
            }
        }
    }

    /**
     * Gửi mã xác minh về đt
    */
    public static function sendSmsVerifiPhone($tel, $total_price = false)
    {

        $sender = @Setting::where('type', 'send_sms')->where('name', 'sender')->first()->value;
        $message = @Setting::where('type', 'send_sms')->where('name', 'sms_content')->first()->value;
        $code = rand(10000, 99999);
        $user = User::select(['id', 'country_code'])->where('tel', $tel)->first();
        if (!is_object($user)) {
            return [
                'status' => false,
                'msg' => 'Không có khách hàng nào có sđt ' . $tel
            ];
        }
        if (substr($tel, 0, 1) == '0') {
            $tel = $user->country_code . substr($tel, 1, 15);
        }
        VerifyCode::create([
            'tel' => $tel,
            'object_id' => $user->id,
            'type' => 'user',
            'code' => $code,
        ]);

        $message = str_replace('{code}', $code, $message);
        if ($total_price) {
            $message = str_replace('{price}', $total_price, $message);
        }

        /*return [
            'status' => true,
            'msg' => 'Gửi thành công',
            'data' => [],
        ];*/
        return CardBillHelper::sendSms($tel,$sender, $message);
    }

    /**
     * Thông báo tạo đơn thành công
     */
    public static function sendSmsBillSuccess($tel, $total_price, $total_price_of_user)
    {

        $sender = @Setting::where('type', 'send_sms')->where('name', 'sender')->first()->value;
        $message = @Setting::where('type', 'send_sms')->where('name', 'sms_content_bill_success')->first()->value;

        $user = User::select(['id', 'country_code'])->where('tel', $tel)->first();
        if (!is_object($user)) {
            return [
                'status' => false,
                'msg' => 'Không có khách hàng nào có sđt ' . $tel
            ];
        }
        if (substr($tel, 0, 1) == '0') {
            $tel = $user->country_code . substr($tel, 1, 15);
        }

        $message = str_replace('{bill_price}', $total_price, $message);
        $message = str_replace('{user_price}', $total_price_of_user, $message);

        /*return [
            'status' => true,
            'msg' => 'Gửi thành công',
            'data' => [],
        ];*/
        return CardBillHelper::sendSms($tel,$sender, $message);
    }
}