<?php

namespace Modules\CardBill\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class CardBillServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //        $this->registerTranslations();
//        $this->registerConfig();
//        $this->registerFactories();
//        $this->loadMigrationsFrom(module_path('CardBill', 'Database/Migrations'));
            $this->registerViews();

            $this->registerPermission();
            //  Cấu hình menu trái
            $this->rendAsideMenu();

            //  Đăng ký quyền
            $this->registerPermission();
            $this->addSettingService();
        }
    }

    public function addSettingService()
    {
        \Eventy::addFilter('setting.custom_module', function ($module) {
            $module['tabs']['send_sms'] = [
                'label' => 'Cấu hình sms',
                'icon' => '<i class="flaticon-mail"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'sender', 'type' => 'text', 'label' => 'Người gửi',],
                    ['name' => 'sms_content', 'type' => 'textarea', 'label' => 'Nội dung sms xác minh sđt bắt buộc có {code}', 'inner' => 'placeholder="Mã xác minh của bạn là {code}"', 'des' => '{code}: mã xác thực. {price}: giá tiền'],
                    ['name' => 'sms_content_bill_success', 'type' => 'textarea', 'label' => 'Nội dung sms báo đơn thành công', 'inner' => 'placeholder="Đơn hàng của bạn được tạo thành công, giá đơn hàng là {bill_price} số tiền còn lại của bạn {user_price}"', 'des' => '{bill_price}: tổng tiền đơn hàng. {user_price}: Số tiền còn lại của khách'],
                    ['name' => 'SEND_SMS_URL', 'type' => 'text', 'label' => 'SEND_SMS_URL',],
                    ['name' => 'TOKEN', 'type' => 'text', 'label' => 'TOKEN',],
                ]
            ];
            return $module;
        }, 1, 1);
    }


    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['card_view', 'card_add', 'card_edit', 'card_delete',
                'bill_view', 'bill_add']);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('cardbill::partials.aside_menu.aside_menu_dashboard_after');
        }, 100, 1);
    }


/**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('CardBill', 'Config/config.php') => config_path('cardbill.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('CardBill', 'Config/config.php'), 'cardbill'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/cardbill');

        $sourcePath = module_path('CardBill', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/cardbill';
        }, \Config::get('view.paths')), [$sourcePath]), 'cardbill');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/cardbill');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'cardbill');
        } else {
            $this->loadTranslationsFrom(module_path('CardBill', 'Resources/lang'), 'cardbill');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('CardBill', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
