<?php

namespace Modules\JdesUser\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\JdesUser\Models\BillPayment;
use Modules\ThemeSemicolonwebJdes\Models\Company;
use Validator;

class UserController extends CURDBaseController
{

    protected $module = [
        'code' => 'user',
        'table_name' => 'users',
        'label' => 'jdesuser::admin.customer_partner',
        'modal' => '\Modules\JdesUser\Models\User',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'jdesuser::admin.image', 'sort' => true],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'jdesuser::admin.name', 'sort' => true],
            ['name' => 'tel', 'type' => 'text', 'label' => 'jdesuser::admin.phone', 'sort' => true],
            ['name' => 'email', 'type' => 'text', 'label' => 'jdesuser::admin.email', 'sort' => true],
            ['name' => 'gender', 'type' => 'text', 'label' => 'jdesuser::admin.gender', 'sort' => true],
            ['name' => 'birthday', 'type' => 'date_vi', 'label' => 'jdesuser::admin.date', 'sort' => true],
            ['name' => 'address', 'type' => 'text', 'label' => 'jdesuser::admin.address', 'sort' => true],
            ['name' => 'company_id', 'type' => 'relation', 'label' => 'jdesuser::admin.company', 'object' => 'company', 'display_field' => 'name'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'jdesuser::admin.name'],
                ['name' => 'tel', 'type' => 'text', 'label' => 'jdesuser::admin.phone'],
                ['name' => 'email', 'type' => 'text', 'label' => 'jdesuser::admin.email'],
                ['name' => 'gender', 'type' => 'select', 'options' =>
                    [
                        'Nam' => 'jdesuser::admin.boy',
                        'Nữ' => 'jdesuser::admin.girl',
                    ], 'class' => '', 'value' => '','label' => 'jdesuser::admin.gender'],
                ['name' => 'birthday', 'type' => 'datetimepicker', 'label' => 'jdesuser::admin.date'],
                ['name' => 'address', 'type' => 'text', 'label' => 'jdesuser::admin.address'],

            ],
            'image_tab' => [
                ['name' => 'image', 'type' => 'file_image', 'label' => 'jdesuser::admin.image_user'],
            ],
            'company_info' => [
                ['name' => 'company_id', 'type' => 'select2_ajax_model', 'label' => 'jdesuser::admin.company', 'object' => 'company', 'model' => Company::class, 'display_field' => 'short_name'],
            ]
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'jdesuser::admin.name',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'email' => [
            'label' => 'jdesuser::admin.email',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'tel' => [
            'label' => 'jdesuser::admin.phone',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'gender' => [
            'label' => 'jdesuser::admin.gender',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'address' => [
            'label' => 'jdesuser::admin.address',
            'type' => 'text',
            'query_type' => 'like'
        ]
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);


        return view('jdesuser::list')->with($data);
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('jdesuser::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'unique:users,email',
                    'tel' => 'unique:users,tel',
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                    'email.unique' => 'Email '. $request->email .' đã tồn tại',
                    'tel.unique' => 'Số điện thoại '. $request->tel .' đã tồn tại',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;
//                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    $data['admin_id'] = \Auth::guard('admin')->user()->id;
//                    dd($data);

//                    if ($request->has('contact_info_name')) {
//                        $data['contact_info'] = json_encode($this->getContactInfo($request));
//                    }
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
//                if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                    CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                    return back();
//                }
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('jdesuser::edit')->with($data);
            } else if ($_POST) {

//  Chỉ sửa được liệu công ty mình đang vào
            if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
                if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                    CommonHelper::one_time_message('error', 'Bạn không có quyền!');
                    return back();
                }
            }
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'unique:users,email,' . $request->id,
                    'tel' => 'unique:users,tel,' . $request->id,
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                    'email.unique' => 'Email '. $request->email .' đã tồn tại',
                    'tel.unique' => 'Số điện thoại '. $request->tel .' đã tồn tại',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
                    unset($data['company_id']);
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
            if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'super_admin')) {
                if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                    CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
                    return back();
                }
            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            $items = $this->model->whereIn('id', $ids)->get();
            foreach ($items as $item) {
                //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
                if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'super_admin')) {
                    if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                        CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
                        return back();
                    }
                }

                $item->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function checkTelDuplicate(Request $request)
    {
        $tel = BillPayment::where('tel',$request->tel)->where('tel','<>','')->whereNotNull('tel')->get()->count();

        if (isset($request->id)){
            $tel = BillPayment::where('tel',$request->tel)->where('tel','<>','')->where('id','<>',$request->id)->whereNotNull('tel')->get()->count();
        }
        $html_tel='';
        if ($tel>0){
            $html_tel = 'Số điện thoại này đã tồn tại';
        }
        return $html_tel;
    }
    public function checkMailDuplicate(Request $request)
    {
        $email = BillPayment::where('email',$request->email)->whereNotNull('email')->where('email','<>','')->get()->count();
        if (isset($request->id)){
            $email = BillPayment::where('email',$request->email)->where('email','<>','')->where('id','<>',$request->id)->whereNotNull('email')->get()->count();
        }
        $html_email='';
        if ($email>0){
            $html_email = 'Email này đã tồn tại';
        }
        return $html_email;
    }
}
