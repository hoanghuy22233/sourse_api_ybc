<?php

namespace Modules\TShirtCrawller\Entities;

use Illuminate\Database\Eloquent\Model;

class DoomProduct extends Model
{
    protected $table = 'doom_product';
    protected $guarded = [];
    public function website(){
        return $this->belongsTo(Website::class,'website_id','id');
    }
    public function category(){
        return $this->belongsTo(Category::class,'multi_cat','id');
    }
}
