<?php

namespace Modules\TShirtCrawller\Entities;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    protected $table = 'website';
    protected $guarded = [];
    public $timestamps = false;

    public function categories() {
        return $this->hasMany(Category::class, 'website_id', 'id');
    }
}
