<?php

namespace Modules\TShirtCrawller\Http\Controllers;

use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Setting;
use Modules\TShirtCrawller\Entities\Category;
use Validator;
use Excel;
use Storage;

class DoomController extends CURDBaseController
{
    protected $module = [
        'code' => 'doom',
        'table_name' => 'website',
        'label' => 'doom',
        'modal' => '\Modules\TShirtCrawller\Entities\Website',
        'list' => [
        ],
        'form' => [
            'category_tab' => [
                ['name' => 'domain', 'type' => 'custom', 'field' => 'crawler::form.fields.dynamic', 'class' => 'required', 'label' => 'Chuyên mục'],
            ],
            'category_target_tab' => [
//                ['name' => 'url_pagination', 'type' => 'text', 'class' => 'required', 'label' => 'URL phân trang'],
                ['name' => 'foreach_pagination', 'type' => 'checkbox', 'label' => 'Phân trang bị lặp (VD: Trang max = 10 khi click vào page 11 thì sang page 1)'],
                ['name' => 'product_target_block', 'type' => 'text', 'class' => 'required', 'label' => 'Target Mỗi Block Sản Phẩm'],
                ['name' => 'product_target_link', 'type' => 'text', 'class' => 'required', 'label' => 'Target Link Mỗi Sản Phẩm (Lấy từ thẻ con của Block ở trên)'],
            ],
            'product_doom_tab' => [
                ['name' => 'name_product_doom', 'type' => 'text', 'class' => 'required', 'label' => 'Target Tên Sản Phẩm'],
                ['name' => 'image_1_product_doom', 'type' => 'text', 'class' => 'required', 'label' => 'Target Ảnh Đại Diện'],
                ['name' => 'image_2_product_doom', 'type' => 'text', 'class' => '', 'label' => 'Target Ảnh 1'],
                ['name' => 'image_3_product_doom', 'type' => 'text', 'class' => '', 'label' => 'Target Ảnh 2'],
                ['name' => 'type_product_doom', 'type' => 'text', 'class' => '', 'label' => 'Loại sản phẩm muốn lấy', 'des' => 'Mỗi loại phân cách nhau bởi dấu ,'],
                ['name' => 'add_domain_image_doom', 'type' => 'checkbox', 'value' => '1', 'label' => 'Thêm domain vào đầu link ảnh?'],
            ],
        ],
    ];

    protected $filter = [

    ];

    public function update(Request $request, $website_id)
    {
        try {
            $item = $this->model->find($website_id);

            if (!is_object($item)) abort(404);
            if (!$_POST) {

                $data = $this->getDataUpdate($request, $item);
                return view('crawler::doom.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
//                    'domain' => 'required'
                ], [
//                    'domain.required' => 'Bắt buộc phải nhập domain',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    $doom = [];
//                    $doom['category']['url_pagination'] = $request->url_pagination;
                    $doom['category']['foreach_pagination'] = $request->has('foreach_pagination') ? 1 : 0;
                    $doom['product']['product_target_block'] = $request->product_target_block;
                    $doom['product']['product_target_link'] = $request->product_target_link;
                    $doom['product']['name_product_doom'] = $request->name_product_doom;
                    $doom['product']['image_1_product_doom'] = $request->image_1_product_doom;
                    $doom['product']['image_2_product_doom'] = $request->image_2_product_doom;
                    $doom['product']['image_3_product_doom'] = $request->image_3_product_doom;
                    $doom['product']['type_product_doom'] = $request->type_product_doom;
                    $doom['product']['add_domain_image_doom'] = $request->has('add_domain_image_doom') ? 1 : 0;
                    $data['doom'] = json_encode($doom);

                    if ($request->has('id_cate')) {
                        $catUpdate = [];
                        foreach ($request->id_cate as $i => $id_cate) {
                            $cat = Category::updateOrCreate(
                                [
                                    'link' => $request->cate_link[$i],
                                    'website_id' => $website_id,
                                ],
                                [
                                    'name' => $request->cate_name[$i],
                                    'page' => $request->page[$i],
                                ]
                            );
                            $catUpdate[] = $cat->id;
                        }
                        Category::whereNotIn('id', $catUpdate)->where('website_id', $website_id)->delete();
                    }else{
                        @Category::where('website_id', $website_id)->delete();
                    }
                    unset($data['domain']);
                    unset($data['status']);
//                    unset($data['url_pagination']);
                    unset($data['foreach_pagination']);
                    unset($data['product_target_block']);
                    unset($data['product_target_link']);
                    unset($data['name_product_doom']);
                    unset($data['image_1_product_doom']);
                    unset($data['image_2_product_doom']);
                    unset($data['image_3_product_doom']);
                    unset($data['type_product_doom']);
                    unset($data['add_domain_image_doom']);
                    unset($data['image_1_product_doom']);
//                    dd(1);
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }

                    if ($item->save()) {
//                        CommonHelper::flushCache();
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

//                        if ($request->return_direct == 'save_continue') {
//                            return redirect('admin/' . $this->module['code'] . '/' . $item->id);
//                        } elseif ($request->return_direct == 'save_create') {
//                            return redirect('admin/' . $this->module['code'] . '/add');
//                        }
                    return back();
                }
            }

        } catch
        (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }
    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            $item->delete();
            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


}