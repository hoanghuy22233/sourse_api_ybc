<?php

namespace Modules\TShirtCrawller\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\CheckErrorLink\Console\CrawlProduct;

class CrawlerServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Cấu hình Core
//            $this->registerTranslations();
//            $this->registerConfig();
            $this->registerViews();
//            $this->registerFactories();
//            $this->loadMigrationsFrom(module_path('Crawler', 'Database/Migrations'));

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }

        $this->commands($this->moreCommands);
    }

    protected $moreCommands = [
        CrawlProduct::class
    ];

    public function rendAsideMenu()
    {

        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('crawler::partials.aside_menu.dashboard_after_course');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('Crawler', 'Config/config.php') => config_path('crawler.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('Crawler', 'Config/config.php'), 'crawler'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/crawler');

        $sourcePath = module_path('Crawler', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/crawler';
        }, \Config::get('view.paths')), [$sourcePath]), 'crawler');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/crawler');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'crawler');
        } else {
            $this->loadTranslationsFrom(module_path('Crawler', 'Resources/lang'), 'crawler');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('Crawler', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
