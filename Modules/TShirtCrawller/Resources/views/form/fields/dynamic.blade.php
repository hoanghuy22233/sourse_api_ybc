<style>
    .form-group-dynamic .fieldwrapper > div:nth-child(1) {
        padding-left: 0;
    }

    .fieldwrapper {
        padding: 5px;
        border: 1px solid #ccc;
        margin-bottom: 5px;
    }
</style>
<fieldset id="buildyourform-{{ $field['name'] }}" class="{{ @$field['class'] }}">

    <?php
    if (!empty($result->id)) {
        $cates = \Modules\TShirtCrawller\Entities\Category::where('website_id', $result->id);

        if ($cates->exists()){
            $cates =$cates->get();
        }else{
            $cates =[];
            $cates_all = \Modules\TShirtCrawller\Entities\Category::all();
        }
    }
    ?>
    @foreach ($cates as $v)
        <div class="fieldwrapper row" id="field">
            <div class="col-xs-10 col-md-10">
                <div class="row">

                    <div class="col-xs-5 col-md-5">
                        <input type="text" class="form-control fieldname"
                               name="id_cate[]"
                               style="display: none"
                               value="{{ @$v->id }}">
                        <input type="text" class="form-control fieldname"
                               name="cate_name[]"
                               value="{{ @$v->name }}"
                               placeholder="Tên chuyên mục" required>
                    </div>
                    <div class="col-xs-5 col-md-5">
                        <input type="text" class="form-control fieldname"
                               name="cate_link[]"
                               value="{{ @$v->link }}"
                               placeholder="Link chuyên mục" required>
                    </div>

                    <div class="col-xs-2 col-md-2">
                        <input type="text" class="form-control fieldname"
                               name="page[]"
                               value="{{ @$v->page }}"
                               placeholder="số trang">
                    </div>
                </div>
            </div>
            <div class="col-xs-2 col-md-2 " style="text-align: right;">
                <i type="xóa hàng" style="cursor: pointer;"
                   class="btn remove btn btn-danger btn-icon la la-remove"></i>
            </div>
        </div>
    @endforeach
</fieldset>
<a class="btn btn btn-primary btn-add-dynamic" style="color: white; margin-top: 20px; cursor: pointer;">
    <span>
        <i class="la la-plus"></i>
        <span>Thêm Chuyên mục</span>
    </span>
</a>
<script>
    $(document).ready(function () {
        var i = '{{isset($v)?$v->id:$cates_all->count()}}';
        $(".btn-add-dynamic").click(function () {
            i = parseInt(i);
            i++;
            var lastField = $("#buildyourform-{{ $field['name'] }} div:last");
            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $('<div class="fieldwrapper row" style="margin-bottom: 5px;" id="field' + intId + '"/>');
            fieldWrapper.data("idx", intId);
            var fields = $('<div class="col-xs-10 col-md-10">\n' +
                '                            <div class="row">\n' +
                '                                <div class="col-xs-5 col-md-5">\n' +
                '                                    <input type="text" style="display:none" class="form-control fieldname"\n' +
                '                                                                           name="id_cate[]" value="' + i + '"\n' +
                '                                                                           placeholder="Tên chuyên mục">\n' +
                '                                    <input type="text" class="form-control fieldname"\n' +
                '                                                                           name="cate_name[]" value=""\n' +
                '                                                                           placeholder="Tên chuyên mục" required>\n' +
                '                                </div>\n' +
                '                                 <div class="col-xs-5 col-md-5">\n' +
                '                                    <input type="text" class="form-control fieldname"\n' +
                '                                           name="cate_link[]" value=""\n' +
                '                                           placeholder="Link chuyên mục" required>\n' +
                '                                </div>\n' +
                '                                <div class="col-xs-2 col-md-2">\n' +
                '                                    <input type="text" class="form-control fieldname"\n' +
                '                                           name="page[]" value=""\n' +
                '                                           placeholder="Số trang">\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>');
            var removeButton = $('<div class="col-xs-2 col-md-2" style="text-align: right;"><i type="xóa hàng" style="cursor: pointer;" class="btn remove btn btn-danger btn-icon la la-remove" ></i></div>');

            fieldWrapper.append(fields);
            fieldWrapper.append(removeButton);
            $("#buildyourform-{{ $field['name'] }}").append(fieldWrapper);
        });
        $('body').on('click', '.remove', function () {
            $(this).parents('.fieldwrapper').remove();
        });
    });
</script>