<?php

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'website'], function () {
        Route::get('', 'WebsiteDoomController@getIndex')->name('website');
        Route::get('publish', 'WebsiteDoomController@getPublish')->name('website.publish');
        Route::match(array('GET', 'POST'), 'add', 'WebsiteDoomController@add');
        Route::get('delete/{id}', 'WebsiteDoomController@delete');
        Route::post('multi-delete', 'WebsiteDoomController@multiDelete');
        Route::get('crawl', 'WebsiteDoomController@crawlSpreadshirt');
        Route::match(array('GET', 'POST'),'doom/{website_id}', 'DoomController@update')->name('website.doom');
        Route::get('search-for-select2', 'WebsiteDoomController@searchForSelect2')->name('website.search_for_select2');
        Route::get('{id}', 'WebsiteDoomController@update');
        Route::post('{id}', 'WebsiteDoomController@update');
    });

    Route::group(['prefix' => 'doom-product'], function () {
        Route::get('', 'ProductController@getIndex')->name('doom-product');
        Route::get('publish', 'ProductController@getPublish')->name('doom-product.publish');
        Route::match(array('GET', 'POST'), 'add', 'ProductController@add');
        Route::get('delete/{id}', 'ProductController@delete');
        Route::post('multi-delete', 'ProductController@multiDelete');
        Route::get('delete-all', 'ProductController@allDelete');
        Route::get('get-data-export', 'ProductController@getDataExport');
        Route::get('get-data-image-export', 'ProductController@getDataExportImage');
        Route::get('search-for-select2', 'ProductController@searchForSelect2')->name('doom-product.search_for_select2');
        Route::get('{id}', 'ProductController@update');
        Route::post('{id}', 'ProductController@update');
    });
});
