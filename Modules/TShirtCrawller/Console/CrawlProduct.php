<?php

namespace Modules\TShirtCrawller\Console;

use Illuminate\Console\Command;
use Modules\CheckErrorLink\Models\DomainCheck;
use Modules\CheckErrorLink\Models\LinkCheck;
use Modules\TShirtCrawller\Console\Website\CrawlWebsiteBase;
use Modules\TShirtCrawller\Console\Website\Spreadshirt;
use Modules\TShirtCrawller\Console\Website\TeezilyCom;
use Modules\TShirtCrawller\Entities\Website;
use Modules\TShirtExport\Entities\Product;

class CrawlProduct extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'link:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Quet link loi.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $websites = Website::with(['categories'=>function($query){
            $query->select(['id', 'name', 'link', 'website_id','page']);

        }])->select(['domain', 'id', 'doom'])->where('status', 1)->orderBy('created_at', 'desc')->get();

        foreach ($websites as $website) {
            print "Crawl website ".$website->domain."\n";
            if (strpos($website->domain, 'spreadshirt.com')!== false) {
                $crawler = new TeezilyCom();
                $crawler->crawlPageList($website);
            }
//            elseif (strpos($website->domain, 'teezily.com')!== false) {
////                $crawler = new Spreadshirt();
////                $crawler->crawlPageList($website);
//            }else{
//                return false;
//            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
//            ['check', InputArgument::REQUIRED, 'An example argument.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
//            ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
        ];
    }
}
