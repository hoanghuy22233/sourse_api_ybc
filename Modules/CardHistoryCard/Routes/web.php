<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {

    Route::group(['prefix' => 'history-card'], function () {
        Route::get('', 'Admin\HistoryCardController@getIndex')->name('history-card')->middleware('permission:history-card_view');
        Route::get('publish', 'Admin\HistoryCardController@getPublish')->name('history-card.publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\HistoryCardController@add')->middleware('permission:history-card_add');
        Route::get('delete/{id}', 'Admin\HistoryCardController@delete')->middleware('permission:history-card_delete');
        Route::post('multi-delete', 'Admin\HistoryCardController@multiDelete')->middleware('permission:history-card_delete');
        Route::get('search-for-select2', 'Admin\HistoryCardController@searchForSelect2')->name('history-card.search_for_select2')->middleware('permission:history-card_view');
        Route::get('{id}', 'Admin\HistoryCardController@update')->middleware('permission:history-card_view');
        Route::post('{id}', 'Admin\HistoryCardController@update')->middleware('permission:history-card_edit');
    });

});
