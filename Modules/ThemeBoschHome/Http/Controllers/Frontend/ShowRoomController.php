<?php

namespace Modules\ThemeBoschHome\Http\Controllers\Frontend;

use App\Http\Helpers\CommonHelper;
use Modules\ThemeBoschHome\Models\Showroom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShowRoomController extends Controller
{
   public function getShowRoom($slug, Request $request){
       $getLocation = Showroom::pluck('location', 'id');
       $id = [];
       foreach ($getLocation as $key => $val){
            if (\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::convertSlug($val) == $slug){
                array_push($id, $key);
            }
       }
       $showroom = Showroom::whereIn('id', $id);
       $data['showrooms'] = $showroom->orderBy('id', 'desc')->get();
       $data['name'] = $showroom->first();
       return view('themeboschhome::childs.showroom.list')->with($data);
   }
}
