<?php

namespace Modules\ThemeBoschHome\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Session;
use Validator;

class CkFinderController extends Controller
{
    public function browser()
    {
        return view('admin.ckfinder');
    }
}
