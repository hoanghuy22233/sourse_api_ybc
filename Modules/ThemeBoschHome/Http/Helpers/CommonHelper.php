<?php

namespace Modules\ThemeBoschHome\Http\Helpers;

use Modules\ThemeBoschHome\Models\Category;
use Modules\ThemeBoschHome\Models\Product;
use Modules\ThemeBoschHome\Models\ProductSale;
use Modules\ThemeBoschHome\Models\Settings;
use Dompdf\Helpers;
use function foo\func;
use Illuminate\Support\Facades\App;
use Modules\ThemeBoschHome\Models\Menu;
use View;
use Session;
use Modules\ThemeBoschHome\Models\Meta;
use Modules\ThemeBoschHome\Models\Permissions;
use Modules\ThemeBoschHome\Models\RoleAdmin;
use Modules\ThemeBoschHome\Models\PermissionRole;
use Illuminate\Support\Facades\Cache;
use App\Http\Helpers\CommonHelper as CoreHelper;

class CommonHelper
{
    public static function key_value($key, $value, $ar)
    {
        $ret = [];
        foreach ($ar as $k => $v) {
            $ret[$v[$key]] = $v[$value];
        }
        return $ret;
    }

    public static function current_action($route)
    {
        $current = explode('@', $route);
        View::share('current_action', $current[1]);
    }

    public static function convertSlug($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        return $str;
    }

    public static function meta($url, $field)
    {
        $metas = CoreHelper::getFromCache('meta_url' . @$url);
        if (!$metas) {
            $metas = Meta::where('url', $url);
            CoreHelper::putToCache('meta_url' . @$url, $metas);
        }


        if ($metas->count())
            return $metas->first()->$field;
        else if ($field == 'title')
            return 'Page Not Found';
        else
            return '';
    }


    public static function backup_tables($host, $user, $pass, $name, $tables = '*')
    {
        try {
            $con = mysqli_connect($host, $user, $pass, $name);
        } catch (Exception $e) {

        }

        if (mysqli_connect_errno()) {
            CoreHelper::one_time_message('danger', "Failed to connect to MySQL: " . mysqli_connect_error());
            return 0;
        }

        if ($tables == '*') {
            $tables = array();
            $result = mysqli_query($con, 'SHOW TABLES');
            while ($row = mysqli_fetch_row($result)) {
                $tables[] = $row[0];
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }

        $return = '';
        foreach ($tables as $table) {
            $result = mysqli_query($con, 'SELECT * FROM ' . $table);
            $num_fields = mysqli_num_fields($result);


            $row2 = mysqli_fetch_row(mysqli_query($con, 'SHOW CREATE TABLE ' . $table));
            $return .= "\n\n" . str_replace("CREATE TABLE", "CREATE TABLE IF NOT EXISTS", $row2[1]) . ";\n\n";

            for ($i = 0; $i < $num_fields; $i++) {
                while ($row = mysqli_fetch_row($result)) {
                    $return .= 'INSERT INTO ' . $table . ' VALUES(';
                    for ($j = 0; $j < $num_fields; $j++) {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = preg_replace("/\n/", "\\n", $row[$j]);
                        if (isset($row[$j])) {
                            $return .= '"' . $row[$j] . '"';
                        } else {
                            $return .= '""';
                        }
                        if ($j < ($num_fields - 1)) {
                            $return .= ',';
                        }
                    }
                    $return .= ");\n";
                }
            }

            $return .= "\n\n\n";
        }

        $backup_name = date('Y-m-d-His') . '.sql';

        $handle = fopen(storage_path("db-backups") . '/' . $backup_name, 'w+');
        fwrite($handle, $return);
        fclose($handle);

        return $backup_name;
    }
    public static function getUrlImageThumb($file, $width = null, $height = null)
    {
//        return \URL::asset('filemanager/userfiles/'.$file);
        /*if(strpos($file, 'upload') !== false) {
            return 'http://noithatplaza.com/wp-content/' . $file;
        }*/
        $logo = CommonHelper::getFromCache('settings_logo_values');
        if (!$logo) {
            $logo = @Settings::where('name', 'logo')->first()->value;
            CommonHelper::putToCache('settings_logo_values', $logo);
        }

        $file_mime = explode('.', $file)[count(explode('.', $file)) - 1];
        $file_thumb = '/public/filemanager/userfiles/_thumbs/' . str_replace('.' . $file_mime, '', $file) . '-' . $width . 'x' . $height . '.' . $file_mime;
        if (file_exists(base_path() . $file_thumb)) {   //  Nếu đã tồn tại file thumb thì trả về luôn
            return \URL::asset(str_replace(' ', '%20', $file_thumb));
        }

        //  Nếu không tồn tại file thumb thì cắt ảnh
        $path_file = base_path() . '/public/filemanager/userfiles/' . urldecode($file);
        if (!file_exists($path_file)) {
            return "/timthumb.php?src=" . \URL::asset('public/filemanager/userfiles/' . $logo);
        } else {
            if ($file != '') {
                $file_name = explode('/', $file)[count(explode('/', $file)) - 1];
                $folder_path = base_path() . '/public/filemanager/userfiles/_thumbs/' . str_replace('/' . $file_name, '', $file);
                if (!is_dir($folder_path)) {    //  Tạo folder nếu không tồn tại
                    mkdir($folder_path, 0755, true);
                }
                try {
                    \Image::make($path_file)->resize($width, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(base_path() . '/' . $file_thumb);
                } catch (\Exception $ex) {
                    return "/timthumb.php?src=" . \URL::asset('/public/filemanager/userfiles/' . $file);
                }
                return \URL::asset(str_replace(' ', '%20', $file_thumb));
            }
        }
        return "/timthumb.php?src=" . \URL::asset('public/filemanager/userfiles/' . $logo);
    }
    public static function getUrlImageThumbNoCut($file)
    {
//        return \URL::asset('filemanager/userfiles/'.$file);
        /*if(strpos($file, 'upload') !== false) {
            return 'http://noithatplaza.com/wp-content/' . $file;
        }*/
        $logo = CoreHelper::getFromCache('settings_logo_values');
        if (!$logo) {
            $logo = @Settings::where('name', 'logo')->first()->value;
            CoreHelper::putToCache('settings_logo_values', $logo);
        }

//        $height = ($height != false && $height != 'auto') ? "&h=" . $height : "";
        try {
            if ($file != '' && (file_exists(base_path() . '/public/filemanager/userfiles/' . $file)
                    || file_exists(base_path() . '/public/filemanager/userfiles/' . str_replace('%20', ' ', $file)))) {
                $file_exist = true;
            } else {
                $file_exist = false;
            }
            if ($file_exist) {
//                dd("/timthumb.php?src=" . \URL::asset('public/filemanager/userfiles/' . $file) . "&w=" . $width . $height);
                return \URL::asset('public/filemanager/userfiles/' . $file);
            }

//            return \URL::asset('public/filemanager/userfiles/' . $logo);
        } catch (Exception $ex) {
//            return \URL::asset('public/filemanager/userfiles/' . $logo);
        }
    }

    public static function discount($base_price, $final_price, $type = '%')
    {
        $sale = $base_price - $final_price;
        if ($sale <= 0)
            return '';

        switch ($type) {
            case '-' :
                return '-' . number_format($sale, 0, '.', '.') . 'đ';
                break;
            default :
                return '-' . number_format(($sale / $base_price) * 100, 0, '.', '.') . '%';
        }
    }

    public static function getProductSlug($product, $type = 'object')
    {
        $multi_cat = $type == 'object' ? $product->multi_cat : $product['multi_cat'];
        $product_slug = $type == 'object' ? $product->slug : $product['slug'];
        $slug = '';
        $manufacture_id = $type == 'object' ? $product->manufacture_id : $product['manufacture_id'];
        try {
            $cat = CoreHelper::getFromCache('cattory_first_in_multi_cat' . json_encode($multi_cat), ['categories']);
            if (!$cat) {
                $cat = Category::select(['slug', 'parent_id'])->whereIn('id', explode('|', $multi_cat))->first();
                if (is_object($cat)) {
                    $slug .= '/' . $cat->slug;
//                dd($slug);
                    //  Lay slug thuong hieu
//                $manufacturer = Manufacturer::select('slug')->where('id', $manufacture_id)->first();
//                if (is_object($manufacturer)) {
//                    $slug .= '/' . $manufacturer->slug;
//                }

                    //  lay link slug category con
                    if ($cat->parent_id != null && $cat->parent_id != 0) {
                        $cat2 = Category::select(['slug', 'parent_id'])->where('id', $cat->parent_id)->first();
                        if (is_object($cat2)) {
                            $slug = '/' . $cat2->slug . $slug;
                            if ($cat2->parent_id != null && $cat2->parent_id != 0) {
                                $cat3 = Category::select(['slug', 'parent_id'])->where('id', $cat2->parent_id)->first();
                                if (is_object($cat3)) {
                                    $slug = '/' . $cat3->slug . $slug;
                                }
                            }
                        }
                    }
                }
                CoreHelper::putToCache('cattory_first_in_multi_cat' . json_encode($multi_cat), $cat, ['categories']);
            }
            return $slug . '/' . $product_slug . '.html';
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    public static function getProductsSale($product_id, $category_id, $manufacturer_id = false)
    {

        $products = CoreHelper::getFromCache('getProductsSale_' . $product_id . $category_id . $manufacturer_id);
        if (!$products) {
            $getDateToday = date_create(date('Y-m-d H:i:s'));
            $products_sale_id_str = ProductSale::where(function ($query) use ($category_id) {
                $query->orWhere('category_ids', null);
                $query->orWhere('category_ids', 'LIKE', '%|' . $category_id . '|%');
            });
            if ($manufacturer_id != null) {
                $products_sale_id_str = $products_sale_id_str->where(function ($query) use ($manufacturer_id) {
                    $query->orWhere('manufacturer_ids', null);
                    $query->orWhere('manufacturer_ids', 'LIKE', '%|' . $manufacturer_id . '|%');
                });
            }
            $products_sale_id_str = $products_sale_id_str->where(function ($query) use ($product_id) {
                $query->orWhere('id_product', null);
                $query->orWhere('id_product', '');
                $query->orWhere('id_product', 'LIKE', '%|' . $product_id . '|%');
            });
//        dd($products_sale_id_str->count());
            $products_sale_id_str = $products_sale_id_str->where(function ($query) use ($getDateToday) {
                $query->orWhere('time_start', '<=', date_format($getDateToday, 'Y-m-d H:i:s'));
                $query->orWhere('time_start', null);
            })->where(function ($query) use ($getDateToday) {
                $query->orWhere('time_end', '>=', date_format($getDateToday, 'Y-m-d H:i:s'));
                $query->orWhere('time_end', null);
            })->pluck('id_product_sale')->toArray();

            $products_sale_id = [];
            foreach ($products_sale_id_str as $v) {
                $products_sale_id = array_merge($products_sale_id, explode('|', $v));
            }
            $products = Product::select(['id', 'name', 'image', 'base_price'])->where('status', 1)->whereIn('id', $products_sale_id)->get();
            CoreHelper::putToCache('getProductsSale_' . $product_id . $category_id . $manufacturer_id, $products);
        }

        return $products;
    }

    public static function getProductsSaleText($product_id, $category_id, $manufacturer_id = false)
    {

        $products_sale_id_str = CoreHelper::getFromCache('getProductsSaleText_' . $product_id . $category_id . $manufacturer_id, ['products', 'product_sale']);
        if (!$products_sale_id_str) {
            $getDateToday = date_create(date('Y-m-d H:i:s'));
            $products_sale_id_str = ProductSale::where(function ($query) use ($category_id) {
                $query->orWhere('category_ids', null);
                $query->orWhere('category_ids', 'LIKE', '%|' . $category_id . '|%');
            });
            if ($manufacturer_id != null) {
                $products_sale_id_str = $products_sale_id_str->where(function ($query) use ($manufacturer_id) {
                    $query->orWhere('manufacturer_ids', null);
                    $query->orWhere('manufacturer_ids', 'LIKE', '%|' . $manufacturer_id . '|%');
                });
            }
            $products_sale_id_str = $products_sale_id_str->where(function ($query) use ($product_id) {
                $query->orWhere('id_product', null);
                $query->orWhere('id_product', '');
                $query->orWhere('id_product', 'LIKE', '%|' . $product_id . '|%');
            });
//        dd($products_sale_id_str->count());
            $products_sale_id_str = $products_sale_id_str->where(function ($query) use ($getDateToday) {
                $query->orWhere('time_start', '<=', date_format($getDateToday, 'Y-m-d H:i:s'));
                $query->orWhere('time_start', null);
            })->where(function ($query) use ($getDateToday) {
                $query->orWhere('time_end', '>=', date_format($getDateToday, 'Y-m-d H:i:s'));
                $query->orWhere('time_end', null);
            })->pluck('sale_text')->toArray();

            CoreHelper::putToCache('getProductsSaleText_' . $product_id . $category_id . $manufacturer_id, $products_sale_id_str, ['products', 'product_sale']);
        }

        return $products_sale_id_str;
    }

    public static function getPostSlug($post, $type = 'object')
    {
        try {
            $slug = CoreHelper::getFromCache('get_post_by_slug_' . $post->id);
            if (!$slug) {
                $slug = '';
                $catPost = CoreHelper::getFromCache('category_slug_category_product_id' . @$post->multi_cat);
                if (!$catPost) {
                    $catPost = Category::select(['slug', 'category_product_id'])->whereIn('id', explode('|', $post->multi_cat))->first();
                    CoreHelper::putToCache('category_slug_category_product_id' . @$post->multi_cat, $catPost);
                }
                if (is_object($catPost)) {
                    $catProduct = CoreHelper::getFromCache('category_slug_id' . @$catPost->category_product_id);
                    if (!$catProduct) {
                        $catProduct = Category::select('slug')->where('id', $catPost->category_product_id)->first();
                        CoreHelper::putToCache('category_slug_id' . @$catPost->category_product_id, $catProduct);
                    }


                    if (isset($catProduct) && is_object($catProduct)) {
                        $slug .= '/' . $catProduct->slug;
                    } else {
                        $slug .= '/' . $catPost->slug;
                    }
                }

                CoreHelper::putToCache('get_post_by_slug_' . $post->id, $slug);
            }

            return $slug . '/' . $post->slug . '.html';
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    public static function getPriceHtml($product)
    {
        if (!isset($settings)) {
            $settings = CoreHelper::getFromCache('get_all_settings');
            if (!$settings) {
                $settings = \Modules\ThemeBoschHome\Models\Settings::pluck('value', 'name')->toArray();
                CoreHelper::putToCache('get_all_settings', $settings);
            }
        }

        $html = '';
        if ($product->final_price == 0) {
            $html .= '<a href="tel:' . @$settings['ho_tro_truc_tuyen'] . '">Liên hệ</a>';
        } else if ($product->base_price == 0) {
            $html .= ' <div class="special-price">
                                                <span class="price product-price">' . number_format($product->final_price, 0, '.', '.') . 'đ</span></div>';
        } else {
            $html .= ' <div class="special-price">
                                                <span class="price product-price">' . number_format($product->final_price, 0, '.', '.') . 'đ</span>
                                            </div><div class="old-price">
				<span class="price product-price-old">' . number_format($product->base_price, 0, '.', '.') . 'đ</span></div>';
        }

        return $html;
    }

    public static function getSubCate($id)
    {
        $menus = CoreHelper::getFromCache('category_find_id' . @$id);
        if (!$menus) {
            $menus = \Modules\ThemeBoschHome\Models\Category::find($id);
            CoreHelper::putToCache('category_find_id' . @$id, $menus);
        }

        if ($menus->parent_id > 0) {
            return CoreHelper::getSubCate($menus->parent_id);
        }
        return $menus->filters;
    }

    public static function getSubcategory($parent_id)
    {
        $menus = CoreHelper::getFromCache('menu_parent_id' . @$parent_id);
        if (!$menus) {
            $menus = Menu::where('parent_id', $parent_id)->where('status', 1)->get();
            CoreHelper::putToCache('menu_parent_id' . @$parent_id, $menus);
        }

        if (count($menus) > 1) {
            echo '<ul>';
            foreach ($menus as $key => $menu) {
                $id = $menu->id;
                $show = '';
                $url = '/' . \Request::segment(1) . '/' . $menu->url;
                $r = CoreHelper::getFromCache('menu_parent_id' . @$id);
                if (!$r) {
                    $r = \Modules\ThemeBoschHome\Models\Menu::where('parent_id', $id)->get();
                    CoreHelper::putToCache('menu_parent_id' . @$id, $r);
                }

                if (count($r) >= 1) {
                    $show = 'pdd';
                    $url = 'javascript:;';
                }
                if (last(request()->segments()) == $menu->url) {
                    $active = 'a2act';
                } else {
                    $active = '';
                }

                echo '<li>
                    <a class="' . $show . '' . $active . '" href="' . $url . '" title="' . $menu->name . '">' . $menu->name . '
                    </a>';

                CoreHelper::getSubcategory($id);
                echo '</li>';
            }
            echo '</ul>';
        } else {
            foreach ($menus as $key => $menu) {
                $id = $menu->id;
                $show = '';
                $url = '/' . \Request::segment(1) . '/' . $menu->url;
                $r = Menu::where('parent_id', $id)->get();
                if (count($r) >= 1) {
                    $show = '';
                    $url = 'javascript:;';
                }
                if (last(request()->segments()) == $menu->url) {
                    $block = 'style="display:block"';
                    $active = 'a2act';
                } else {
                    $block = '';
                    $active = '';
                }
                echo '<ul ' . $block . '><li>
                    <a class="' . $show . '' . $active . '" href="' . $url . '" title="' . $menu->name . '">' . $menu->name . '
                    </a>';

                CoreHelper::getSubcategory($id);
                echo '</li></ul>';
            }
        }

    }

    public static function getMenusByLocation($location, $limit = 10, $get_childs = true)
    {
        $data = CoreHelper::getFromCache('menus_by_location_' . $location, ['menus']);
        if (!$data) {
            $menus = Menu::where('location', $location)->where(function ($q) {
                $q->where('parent_id', null)->orwhere('parent_id', 0);
            })->where('status', 1)->where('type', 'url')->where('location', $location)->orderBy('order_no', 'desc')->limit($limit)->get();

            foreach ($menus as $menu) {
                //  Lấy menu cấp 1

                $item = [
                    'name' => $menu->name,
                    'link' => $menu->url,
                    'childs' => [],
                    'image' =>$menu->image
                ];

                //  Lấy menu cấp 2
                $menu2Childs = $menu->childs;
                foreach ($menu2Childs as $child2) {
                    $child2_data = [
                        'name' => $child2->name,
                        'link' => $child2->url,
                        'childs' => [],
                        'image' =>$child2->image
                    ];

                    //  Lấy menu cấp 3
                    $menu3Childs = $child2->childs;
                    foreach ($menu3Childs as $menu3) {
                        $child3_data = [
                            'name' => $menu3->name,
                            'link' => $menu3->url,
                            'childs' => [],
                            'image' =>$menu3->image
                        ];

                        //  Gán menu cấp 3 vào mảng con của cấp 2
                        $child2_data['childs'][] = $child3_data;
                    }

                    //  Gán menu cấp 2 vào mảng con của cấp 1
                    $item['childs'][] = $child2_data;
                }
                $data[] = $item;
            }

            CoreHelper::putToCache('menus_by_location_' . $location, $data, ['menus']);

            if (!$data) {
                return [];
            }
        }

        return $data;
    }

    /*
     * Lấy danh sách quà tặng của sản phẩm
     * */
    public static function getGifOfProduct($product, $qua_dang_hieu_luc = false)
    {
        $gif_product_id_arr = [];

        //   Truy vấn lấy quà đang hiệu lực
        if (!$qua_dang_hieu_luc) {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $getDateToday = date_create(date('Y-m-d H:i:s'));
            $qua_dang_hieu_luc = \Modules\ThemeBoschHome\Models\ProductSale::where(function ($query) use ($getDateToday) {
                $query->orWhere('time_start', '<=', date_format($getDateToday, 'Y-m-d H:i:s'));
                $query->orWhere('time_start', null);
            })->where(function ($query) use ($getDateToday) {
                $query->orWhere('time_end', '>=', date_format($getDateToday, 'Y-m-d H:i:s'));
                $query->orWhere('time_end', null);
            })->get();
        }

        foreach ($qua_dang_hieu_luc as $key => $sale_val) {
            if (in_array($product->id, explode('|', $sale_val->id_product))) {  //  Nếu sp được gán trong quà này
                $gif_product_id_arr = array_merge($gif_product_id_arr, explode('|', $sale_val->id_product_sale));
            } else {
                if ($product->manufacture_id != null && (in_array($product->manufacture_id, explode('|', $sale_val->manufacturer_ids))
                        || $sale_val->manufacturer_ids == null)) {        //  Hãng của sp này được KM
                    if ($sale_val->category_ids == null) {  //  Khuyến mại cho tất cả các danh mục
                        $gif_product_id_arr = array_merge($gif_product_id_arr, explode('|', $sale_val->id_product_sale));
                    } else {
                        foreach (explode('|', $product->multi_cat) as $cat_id) {
                            if ($cat_id != '' && strpos($sale_val->category_ids, '|' . $cat_id . '|') !== false) {
                                $gif_product_id_arr = array_merge($gif_product_id_arr, explode('|', $sale_val->id_product_sale));
                            }
                        }
                    }
                }
            }
        }
        return \Modules\ThemeBoschHome\Models\Product::select('id', 'multi_cat', 'name', 'base_price', 'final_price', 'image')->whereIn('id', $gif_product_id_arr)->get();
    }
    public static function getFromCache($key)
    {
        return false;
        return Cache::get($key);
    }
    public static function putToCache($key, $value, $time = 120)
    {
        Cache::put($key, $value, $time);
        return true;
    }

    public static function removedFromCache($key)
    {
        Cache::forget($key);
        return true;
    }
    public static function convert_number_to_words($number)
    {
        $hyphen = ' ';
        $conjunction = '  ';
        $separator = ' ';
        $negative = 'âm ';
        $decimal = ' phẩy ';
        $dictionary = array(
            0 => 'Không',
            1 => 'Một',
            2 => 'Hai',
            3 => 'Ba',
            4 => 'Bốn',
            5 => 'Năm',
            6 => 'Sáu',
            7 => 'Bảy',
            8 => 'Tám',
            9 => 'Chín',
            10 => 'Mười',
            11 => 'Mười một',
            12 => 'Mười hai',
            13 => 'Mười ba',
            14 => 'Mười bốn',
            15 => 'Mười năm',
            16 => 'Mười sáu',
            17 => 'Mười bảy',
            18 => 'Mười tám',
            19 => 'Mười chín',
            20 => 'Hai mươi',
            30 => 'Ba mươi',
            40 => 'Bốn mươi',
            50 => 'Năm mươi',
            60 => 'Sáu mươi',
            70 => 'Bảy mươi',
            80 => 'Tám mươi',
            90 => 'Chín mươi',
            100 => 'trăm',
            1000 => 'ngàn',
            1000000 => 'triệu',
            1000000000 => 'tỷ',
            1000000000000 => 'nghìn tỷ',
            1000000000000000 => 'ngàn triệu triệu',
            1000000000000000000 => 'tỷ tỷ'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int)$number < 0) || (int)$number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error('convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING);
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int)($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                if ($number < 500000) {
                    $number = 500000;
                    $baseUnit = pow(1000, floor(log($number, 1000)));
                    $numBaseUnits = (int)(ceil($number / $baseUnit));
                    $remainder = ceil($number % $baseUnit);
                } elseif ($number < 1000000) {
                    $number = 1000000;
                    $baseUnit = pow(1000, floor(log($number, 1000)));
                    $numBaseUnits = (int)(ceil($number / $baseUnit));
                    $remainder = ceil($number % $baseUnit);
                } else {
                    $baseUnit = pow(1000, floor(log($number, 1000)));
                    $numBaseUnits = (int)(ceil($number * 1.0001 / $baseUnit));
                    $remainder = ceil($number * 1.0001 % $baseUnit);
                }
                $string = $numBaseUnits . ' ' . $dictionary[$baseUnit];
//            if( $remainder )
//            {
//                $string .= $remainder < 100 ? $conjunction : $separator;
//                $string .= convert_number_to_words( $remainder );
//            }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string)$fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }
        return $string;

    }
}