<?php

/**
 * Page Model
 *
 * Page Model manages page operation. 
 *
 * @category   Page
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license    
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\ThemeBoschHome\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';

    protected $fillable = ['name', 'type', 'url', 'parent_id', 'order_no', 'location', 'categori_post_id',
        'tag_post_id', 'category_product_id', 'tag_product_id', 'product_id', 'status', 'created_at', 'updated_at'];

    public function post() {
        return $this->hasMany(Post::class, 'item_id', 'id');
    }

    public function category() {
        return $this->hasMany(Category::class, 'item_id', 'id');
    }

    public function product() {
        return $this->hasMany(Product::class, 'item_id', 'id');
    }

    public function childs()
    {
        return $this->hasMany($this, 'parent_id', 'id')->orderBy('order_no', 'desc');
    }
}
