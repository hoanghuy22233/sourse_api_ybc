<?php

namespace Modules\ThemeBoschHome\Models;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
   protected $table = 'origins';
}
