<?php

namespace Modules\ThemeBoschHome\Models;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{

    protected $table = 'imports';
}
