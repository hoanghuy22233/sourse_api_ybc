<?php

namespace Modules\ThemeBoschHome\Providers;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ThemeBoschHomeServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //  Custom setting
//        $this->registerTranslations();
//        $this->registerConfig();
        $this->registerViews();
//        $this->registerFactories();
//        $this->loadMigrationsFrom(module_path('ThemeBoschHome', 'Database/Migrations'));

        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }

        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/setting') !== false) {
            $this->customSetting();
        }

        //  Cầu hình frontend
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') === false) {
            $this->frontendSettings();
        }
    }

    public function frontendSettings()
    {
        $settings = CommonHelper::getFromCache('frontend_settings');
        if (!$settings) {
            $settings = Setting::whereIn('type', ['general_tab', 'common_tab', 'seo_tab', 'homepage_tab', 'product_page_tab'])->pluck('value', 'name')->toArray();
            CommonHelper::putToCache('frontend_settings', $settings);
        }

        \View::share('settings', $settings);
        return $settings;
    }

    public function customSetting()
    {
        \Eventy::addFilter('setting.custom_module', function ($module) {
            unset($module['tabs']['seo_tab']);
            return $module;
        }, 1, 1);
    }

    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('themeboschhome::admin.partials.aside_menu.dashboard_after');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('ThemeBoschHome', 'Config/config.php') => config_path('themeboschhome.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('ThemeBoschHome', 'Config/config.php'), 'themeboschhome'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/themeboschhome');

        $sourcePath = module_path('ThemeBoschHome', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/themeboschhome';
        }, \Config::get('view.paths')), [$sourcePath]), 'themeboschhome');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/themeboschhome');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'themeboschhome');
        } else {
            $this->loadTranslationsFrom(module_path('ThemeBoschHome', 'Resources/lang'), 'themeboschhome');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('ThemeBoschHome', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    /*public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['showroom_view', 'showroom_delete', 'showroom_edit', 'showroom_add', 'product_publish'
            ]);
            return $per_check;
        }, 1, 1);
    }*/
}
