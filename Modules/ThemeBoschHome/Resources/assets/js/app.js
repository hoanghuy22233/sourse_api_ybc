$(document).ready(function () {
    $('.toptit').click(function () {
        $(this).next().toggle();
        $(this).parent().toggleClass('active');
    })
    $( document ).ajaxComplete(function() {
        checkHeight();
    });
    $('.fa-times').click(function () {
        $(this).parent().fadeOut();
    })
    $('body').on('click','.gri-add-cart',function () {
        let id = $(this).attr('data-id');
        let container = $(this).parent();
        addCartNotice(id,container);
    })
    function addCartNotice(t,container) {
        let a = $(".urlCart").attr("data-text");
        $.ajax({
            type: "get",
            url: a + "?p=" + t,
            success: function (data) {
                if(data.success) {
                    $(".cart b").text(data.count);
                    $(".bg.bCart label").text(data.count);
                    $(container).children().first().find('#number').text(data.data);
                    $(container).children().first().fadeIn();
                    setTimeout(function () {
                        $(container).children().first().fadeOut();
                    },5000);
                }else{
                    alert("Lỗi chưa thêm được sản phẩm !")
                }
            }
        })
    }

    function checkHeight(){
        if($('.banner-category')){
            let height = 0;
            let productContent = $('.product-content-height');
            for( let i = 0 ; i < productContent.length ; i++ ){
                if($(productContent[i]).height() > height ){
                    height = $(productContent[i]).height();
                }
            }
            $('.product-content-height').css('height',height);
        }
    }
    $('.change-layout').click(function (e) {
        e.preventDefault();
        let layout = $(this).attr('data-layout');
        $('.change-layout').removeClass('active');
        $(this).addClass('active');
        if(layout === "list"){
            $('#Product').addClass(layout)
            $('.product-content-height').css('height','auto');
        }else{
            $('#Product').removeClass("list");
            checkHeight();
        }
    })
    $('.filter-reset').click(function () {
        location.reload();
    })
})
