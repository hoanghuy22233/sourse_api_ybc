<!DOCTYPE html>
<html lang="vi-vn" xml:lang="vi-vn">
<?php
function isMobile()
{
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
$banner_header_top = CommonHelper::getFromCache('setting_name_banner_head_top_type_common_tab');
if (!$banner_header_top) {
    $banner_header_top = \App\Models\Setting::where('name', 'photos[banner_head_top]')->where('type', 'common_tab')->first();
    CommonHelper::putToCache('setting_name_banner_head_top_type_common_tab', $banner_header_top);
}

$banner_top = CommonHelper::getFromCache('banner_location_banner_header_top');
if (!$banner_top) {
    $banner_top = \Modules\ThemeBoschHome\Models\Banner::where('status', 1)->where('location', 'banner_header_top')->first();
    CommonHelper::putToCache('banner_location_banner_header_top', $banner_top);
}

$custom_slide_product_hot = CommonHelper::getFromCache('setting_name_custom_slide_product_hot_type_homepage_tab_value');
if (!$custom_slide_product_hot) {
    $custom_slide_product_hot = @\Modules\ThemeBoschHome\Models\Settings::where('name', 'custom_slide_product_hot')->where('type', 'homepage_tab')->first()->value;
    CommonHelper::putToCache('setting_name_custom_slide_product_hot_type_homepage_tab_value', $custom_slide_product_hot);
}



?>
<head>

    @include('themeboschhome::partials.head_meta')
    @include('themeboschhome::partials.header_script')
    <link rel="stylesheet" href="{{asset('public/frontend/themes/stbd/css/custom2.css')}}?v={{ time() }}">
    {!! @$settings['head_code'] !!}

    <style>
        nav.a {
            width: 491px;
        }

        .nav-menu .nav-item {
            padding: 3px 0 !important;
        }

        {{--.f.regis {--}}
        {{--    @if(@$settings['show_email_homepage']==0)--}}
        {{--          display: none !important;--}}
        {{--@endif--}}



        }

        {{--header {--}}
        {{--    @if(@$settings['header_background'] != '')    background: {{@$settings['header_background']}} !important;--}}
        {{--@endif--}}



        {{--}--}}

        {{--footer {--}}
        {{--    @if(@$settings['footer_background'] != '')    background: {{@$settings['footer_background']}} !important;--}}
        {{--@endif--}}



        {{--}--}}

        {{--@if(@$settings['option_background']==0)--}}
        {{--    body > div.f {--}}
        {{--    background: none !important;--}}
        {{--}--}}

        {{--header {--}}
        {{--    @if(@$settings['header_background'] != '')    background: {{@$settings['header_background']}} !important;--}}
        {{--@endif--}}

        {{--}--}}

        {{--@elseif(@$settings['option_background']==1)--}}
        {{--    @if(@$settings['background_color']!='')--}}
        {{--        body > div.f {--}}
        {{--    background-color: {{$settings['background_color']}}   !important;--}}
        {{--}--}}

        {{--@endif--}}
        {{--@elseif(@$settings['option_background']==2)--}}
        {{--    @if(!empty($settings['background_image']))--}}
        {{--        body > div.f {--}}
        {{--    background-image: url('https://demo4.webhobasoft.com/public/filemanager/userfiles/{{$settings['background_image']}}') !important;--}}
        {{--    background-attachment: fixed;--}}
        {{--}--}}

{{--        @endif--}}
{{--        @endif--}}
.tongdai {
            width: 25%;
            display: flex;
            line-height: 24px;
            height: 24px;
        }

        .tongdai:before {
            font: 12px/16px FontAwesome;
            height: 15px;
            width: 15px;
            display: inline-block;
            text-align: center;
            margin-top: 4px;
            margin-right: 5px;
            border-radius: 50%;
        }

        @media (max-width: 991px) {

            .tongdai {
                display: none !important;
            }
        }

        {{--        @if(@$settings['footer_background'] != '')--}}
        {{--            .f.regis {--}}
        {{--            display: none!important;--}}
        {{--        }--}}
        {{--        @endif--}}


.contact_now_parent {
            position: fixed;
            bottom: 10px;
            right: 0;
            width: 50px;
            z-index: 999;
        }

        .fb_customer_chat_bubble_animated_no_badge, .fb_dialog {
            bottom: 0px !important;
            right: 50px !important;
        }

        .phone_now_detail {
            position: relative;
            display: inline-block;
        }

        .phone_now_detail {
            position: absolute;
            display: none;
            bottom: 5px;
            right: 100%;
            height: auto;
            width: max-content;
            z-index: 999;
            background: #fff;
            padding: 15px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        .contact_now-title {
            display: flex;
            height: 30px;
            line-height: 30px;
            margin-bottom: 10px;
        }

        .contact_now-title > span {
            width: 100%;
            margin-right: 10px;
        }

        .phone_now, .zalo_now, .contact_now-title {
            cursor: pointer;
        }

        .zalo_now {
            position: relative;
        }

        .zalo_now > span {
            display: none;
        }

        .zalo_now > span {
            position: absolute;
            top: 5px;
            right: 100%;
            width: max-content;
            background: #fff;
            border: 1px solid #ccc;
            /*display: block;*/
            padding: 10px;
        }

        @media (max-width: 767px) {
            .b.flexJus.mobie_padding {
                padding: 0 15px;
            }

            header .head .b .hright a {
                top: 5px;
                right: 50px;
            }

            #touch-menu {
                top: 15px;
            }
        }

        .menu-item {
            position: relative;
        }

        .menu-item:hover ul.sub-menu {
            display: block;
            width: 300px;
            top: 100%;
            left: 0;
            background: #fff;
            border: 1px solid #ccc;
            z-index: 9999;
        }

        ul.sub-menu {
            position: absolute;
            display: none;
        }

        li.sub-menu-item:last-child {
            border-bottom: none;
        }

        li.sub-menu-item {
            padding: 10px;
            border-bottom: 1px solid #ccc;
        }

        img.lazy {
            opacity: 0;
        }

    </style>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
</head>
<body>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            xfbml: true,
            version: 'v6.0'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    $(document).ready(function () {
        $("blockquote").after('<i class="fa fa-quote-left" aria-hidden="true"></i>');
        $("blockquote").before('<i class="fa fa-quote-right" aria-hidden="true"></i>');


        $(".phone_now").click(function () {
            $('.contact_now .phone_now_detail').toggle();
        });
        $(".zalo_now").hover(function () {
            $('.zalo_now>span').toggle();
        });
    });

    $(window).scroll(function (event) {
        let scrollHeight = $(window).scrollTop();
        if (scrollHeight > 280) {
            $('.contact_now_parent').css({
                'bottom': '180px'
            });
            // $('.fb_customer_chat_bubble_animated_no_badge').css({
            //     'bottom': '295px!important'
            // });
        } else {
            $('.contact_now_parent').css({
                'bottom': '10px'
            });
            // $('.fb_customer_chat_bubble_animated_no_badge').css({
            //     'bottom': '130px!important'
            // });
        }
    });
</script>
@if(!empty($banner_top))
    <a href="{{$banner_top->link}}" class="banner_header_top_image">
        <img data-src=" /public/filemanager/userfiles/{{ $banner_top->image  }}" style="margin: auto;display: block; width: 100%;"
             alt="{{ $banner_top->name  }}" class="lazy">
    </a>
@endif

@include('themeboschhome::partials.menu_home')
{{--@include('themeboschhome::partials.menu_cate_home')--}}
@if(@$settings['option_menu'] == 0)
    @include('themeboschhome::partials.menu_efect.menu_slidedown')
@elseif(@$settings['option_menu'] == 2)
    @include('themeboschhome::partials.menu_efect.menu_default')
@endif

@include('themeboschhome::partials.slide')

@yield('main_content')

{{--{{dd($settings['custom_slide_product_hot'])}}--}}

@include('themeboschhome::partials.experience')





@include('themeboschhome::partials.form_resign')
@include('themeboschhome::partials.set_font')
@if(@$settings['footer_content'] == 1)
    @include('themeboschhome::partials.footer_colum')
@elseif(@$settings['footer_content'] == 0)
    @include('themeboschhome::partials.footer')
@endif
@include('themeboschhome::partials.footer_script')
@include('themeboschhome::partials.contact_us')

{!! @$settings['footer_code'] !!}
<style>
    .fb_customer_chat_bubble_animated_no_badge {
        right: 0 !important;
        bottom: 125px !important;
    }

    .fb_customer_chat_bubble_animated_no_badge_280 {
        bottom: 125px !important;
    }

    .fb_customer_chat_bubble_animated_no_badge_281 {
        bottom: 300px !important;
    }
</style>
<script>
    $(window).scroll(function (event) {
        let scrollHeight = $(window).scrollTop();
        if (scrollHeight > 280) {
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').addClass('fb_customer_chat_bubble_animated_no_badge_281');
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').removeClass('fb_customer_chat_bubble_animated_no_badge_280');
        } else {
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').addClass('fb_customer_chat_bubble_animated_no_badge_280');
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').removeClass('fb_customer_chat_bubble_animated_no_badge_281');
        }
    });
</script>

@if(@$settings['block_contact_fixed_right'] == 1)
    @include('themeboschhome::partials.block_contact_fixed_right_bottom')
@endif
</body>
</html>
