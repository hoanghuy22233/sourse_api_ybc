<!DOCTYPE html>
<html lang="vi-vn" xml:lang="vi-vn">
<?php
    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

$banner_top = CommonHelper::getFromCache('banner_location_banner_header_top');
if (!$banner_top) {
    $banner_top = \Modules\ThemeBoschHome\Models\Banner::where('status', 1)->where('location', 'banner_header_top')->first();
    CommonHelper::putToCache('banner_location_banner_header_top', $banner_top);
}

$banner_right = CommonHelper::getFromCache('banner_location_banner_header_right');
if (!$banner_right) {
    $banner_right = \Modules\ThemeBoschHome\Models\Banner::where('status',1)->where('location','banner_header_right')->first();
    CommonHelper::putToCache('banner_location_banner_header_right', $banner_right);
}

$banner_left = CommonHelper::getFromCache('banner_location_banner_header_left');
if (!$banner_left) {
    $banner_left = \Modules\ThemeBoschHome\Models\Banner::where('status',1)->where('location','banner_header_left')->first();
    CommonHelper::putToCache('banner_location_banner_header_left', $banner_left);
}

?>
<head>

    @include('themeboschhome::partials.header_detail_script')
    @include('themeboschhome::partials.head_meta')
    @include('themeboschhome::partials.header_cate_script')
    @include('themeboschhome::partials.header_script')

    <link rel="stylesheet" href="{{asset('public/frontend/themes/stbd/css/custom2.css')}}?v={{ time() }}">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    {!! @$settings['head_code'] !!}


    <style>
        .tongdai {
            width: 25%;
            display: flex;
            line-height: 24px;
            height: 24px;
        }
        .tongdai:before {
            font: 12px/16px FontAwesome;
            height: 15px;
            width: 15px;
            display: inline-block;
            text-align: center;
            margin-top: 4px;
            margin-right: 5px;
            border-radius: 50%;
        }
        @media (max-width: 991px){

            .tongdai {
                display: none!important;
            }
        }
        header {
            padding: 0 !important;
        }

        .flexJus.hleft > a {
            line-height: 1;
        }

        /*header.f{*/
        /*    margin-top: -15px;*/
        /*}*/
        .f.regis {
            @if(@$settings['show_email_homepage']==0)
 display: none !important;
        @endif

        }
        .custom_fa:before {
            font-size: 24px !important;
        }

        blockquote {
            background: #0a95e2;
            display: inline-block;
            padding: 10px;
        }

        .nav-menu .nav-item {
            padding: 0 !important;
        }

        /*li.menu-san_pham:hover div#menu-show{*/
        /*    top: 100%!important;*/
        /*}*/
        #menu-show > .flexJus > .hover1 > span {
            border-bottom: none !important;
            padding: 0;
        }

        .head {

            height: 40px;
        }

        .contact_now_parent {
            position: fixed;
            bottom: 10px;
            right: 0;
            width: 50px;
            z-index: 999;
        }


        .fb_customer_chat_bubble_animated_no_badge, .fb_dialog  {
            bottom: 0px!important;
            right: 50px!important;
        }

        .phone_now_detail {
            position: relative;
            display: inline-block;
        }

        .phone_now_detail {
            position: absolute;
            display: none;
            bottom: 5px;
            right: 100%;
            height: auto;
            width: max-content;
            z-index: 999;
            background: #fff;
            padding: 15px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        .contact_now-title {
            display: flex;
            height: 30px;
            line-height: 30px;
            margin-bottom: 10px;
        }

        .contact_now-title > span {
            width: 100%;
            margin-right: 10px;
        }

        .phone_now, .zalo_now, .contact_now-title {
            cursor: pointer;
        }

        .zalo_now {
            position: relative;
        }

        .zalo_now > span {
            display: none;
        }

        .zalo_now > span {
            position: absolute;
            top: 5px;
            right: 100%;
            width: max-content;
            background: #fff;
            border: 1px solid #ccc;
            /*display: block;*/
            padding: 10px;
        }

        nav.a {
            width: 500px !important;
        }



        .banner_header_right img,.banner_header_left img{
            max-width: 100%;
            max-height: 100%;

        }

        .banner_header_right{
            position: fixed;
            top: 10%;
            right: 0;
            width: 100px;
            height: 500px;
            /*background: #ccc;*/
        }
        .banner_header_left{
            position: fixed;
            top: 10%;
            left: 0;
            width: 100px;
            max-height: 500px;
        }
        @media (max-width: 1770px) {

            .banner_header_right,.banner_header_left{
                display: none;
            }
        }
        @media (max-width: 767px){
            .b.flexJus.mobie_padding {
                padding: 0 15px;
            }
            header .head .b .hright a {
                top: 5px;
                right: 50px;
            }
            #touch-menu {
                top: 15px;
            }
        }
        .menu-item {
            position: relative;
        }
        .menu-item:hover ul.sub-menu{
            display: block;
            width: 300px;
            top: 100%;
            left: 0;
            background: #fff;
            border: 1px solid #ccc;
            z-index: 9999;
        }
        ul.sub-menu {
            position: absolute;
            display: none;
        }
        li.sub-menu-item:last-child {
            border-bottom: none;
        }
        li.sub-menu-item {
            padding: 10px;
            border-bottom: 1px solid #ccc;
        }
        img.lazy {
            opacity: 0;
        }

        .product-content-height h2 a {
            font-size: 24px;
            line-height: 25px;
        }
        body #Product .action-container {
            position: relative;
            display: inline-block;
            flex-direction: column;
            height: auto !important;
            width: 100%;
        }
        @media(max-width: 768px) {
            .container {
                max-width: unset !important;
                width: 100% !important;
            }
            #nleft, section > #news > #nleft,
            #nright, section > #news > #nright {
                width: 100%  !important;

            }
            ul.pagination.page-numbers.nav-pagination.links.text-center {
                display: inline-block !important;
                height: unset;
            }
            ul.pagination.page-numbers.nav-pagination.links.text-center:after {
                display: none;
            }
            .pager li, .pagination>li {
                display: inline-block;
                float: left;
            }
            .product > a {
                width: 50%;
                float: left;
                margin: 0;
            }
        }
        @media(max-width: 768px) {
            .nav .b a img {
                background-repeat: no-repeat;
                background-position: center top;
                display: block;
                width: 50px;
                height: 30px;
                background-size: contain;
                margin: auto;
                margin-bottom: 4px;
            }
        }
    </style>
</head>
<body>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml : true,
            version : 'v6.0'
        });
    };
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
@if(!empty($banner_right))
<div class="banner_header_right">
    <a href="{{$banner_right->link}}">
    <img class="lazy" data-src="{{CommonHelper::getUrlImageThumb($banner_right->image, '100%', null) }}"
         alt="" width="100%">
    </a>
</div>
@endif
@if(!empty($banner_left))
<div class="banner_header_left">
    <a href="{{$banner_left->link}}">
    <img class="lazy" data-src="{{CommonHelper::getUrlImageThumb($banner_left->image, '100%', null) }}"
         alt="" width="100%">
    </a>
</div>
@endif

@if(@$settings['block_contact_fixed_right'] == 1)
    @include('themeboschhome::partials.block_contact_fixed_right_bottom')
@endif

<script>
    $(document).ready(function () {
        $("blockquote").before('<i class="fa fa-quote-left fa-pull-left custom_fa" STYLE="color: #ccc;    margin-top: 10px;"  aria-hidden="true"></i>');


        $(".phone_now").click(function () {
            $('.contact_now .phone_now_detail').toggle();
        });
        $(".zalo_now").hover(function () {
            $('.zalo_now>span').toggle();
        });
    });

    $(window).scroll(function (event) {
        let scrollHeight = $(window).scrollTop();
        if (scrollHeight > 280) {
            $('.contact_now_parent').css({
                'bottom': '180px'
            });
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').css({
                'bottom': '295px!important'
            });
        } else {
            $('.contact_now_parent').css({
                'bottom': '10px'
            });
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').css({
                'bottom': '130px!important'
            });
        }
    });
</script>

{{--@if(isMobile())--}}
{{--    @include('themeboschhome::partials.menu_home')--}}
{{--    @include('themeboschhome::partials.menu_cate_home')--}}
{{--    @if(@$settings['option_menu'] == 0)--}}
{{--        @include('themeboschhome::partials.menu_efect.menu_slidedown')--}}
{{--    @elseif(@$settings['option_menu'] == 2)--}}
{{--        @include('themeboschhome::partials.menu_efect.menu_default')--}}
{{--    @endif--}}
{{--@else--}}
{{--    @include('themeboschhome::partials.menu_cate_master')--}}

@if(!empty($banner_top))
<a href="{{$banner_top->link}}" class="banner_header_top_image">
    <img class="lazy" data-src=" /public/filemanager/userfiles/{{ $banner_top->image  }}" style="margin: auto;display: block; width: 100%;" alt="{{ $banner_top->name  }}">
</a>
@endif

@if(@$settings['option_menu'] != 2)
    @include('themeboschhome::partials.menu_home')
@endif
@if(@$settings['option_menu'] == 0)

    @include('themeboschhome::partials.menu_efect.menu_slidedown')
@elseif(@$settings['option_menu'] == 2)
    @include('themeboschhome::partials.menu_cate_master')
    {{--        @include('themeboschhome::partials.menu_efect.menu_default')--}}
@endif
{{--@endif--}}
@yield('main_content')

@include('themeboschhome::partials.set_font')
{{--@include('themeboschhome::partials.footer_colum')--}}
@if(@$settings['footer_content'] == 1)
    @include('themeboschhome::partials.footer_colum')
@else
    @include('themeboschhome::partials.footer')
@endif
@include('themeboschhome::partials.footer_script')
@include('themeboschhome::partials.contact_us')
<div id="popup">
    <span class="button b-close"><span>X</span></span>
    <div id="Map"></div>
</div>
{!! @$settings['footer_code'] !!}
<style>
    .fb_customer_chat_bubble_animated_no_badge{
        right: 0!important;
        bottom:125px!important;
    }
    .fb_customer_chat_bubble_animated_no_badge_280{
        bottom:125px!important;
    }
    .fb_customer_chat_bubble_animated_no_badge_281{
        bottom:300px!important;
    }
</style>
<script>
    $(window).scroll(function (event) {
        let scrollHeight = $(window).scrollTop();
        if (scrollHeight > 280) {
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').addClass('fb_customer_chat_bubble_animated_no_badge_281');
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').removeClass('fb_customer_chat_bubble_animated_no_badge_280');
        } else {
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').addClass('fb_customer_chat_bubble_animated_no_badge_280');
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').removeClass('fb_customer_chat_bubble_animated_no_badge_281');
        }
    });
</script>

</body>
</html>