<?php echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">


    {{--Sitemap danh muc--}}
    <?php
    $data = \Modules\ThemeBoschHome\Models\Product::select(['image', 'name', 'slug', 'category_id','multi_cat','manufacture_id', 'updated_at'])->where('status', 1)->where('slug', '<>', '')->get();
    ?>
    @foreach($data as $item)
        <url>
            <loc>{{ URL::to(\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($item)) }}</loc>
            <image:image>
                <image:loc>{{ URL::to(CommonHelper::getUrlImageThumb($item->image, 73, 107)) }}</image:loc>
                <image:caption>{{ $item->name }}</image:caption>
                <image:license>{{ URL::to('/') }}</image:license>
                <image:family_friendly>yes</image:family_friendly>
            </image:image>
            <lastmod>{{ date("Y-m-d", strtotime($item->updated_at)) }}T{{ date("H:i:s", strtotime($item->updated_at)) }}+07:00</lastmod>
            <changefreq>always</changefreq>
            <priority>0.4</priority>
        </url>
    @endforeach
</urlset>