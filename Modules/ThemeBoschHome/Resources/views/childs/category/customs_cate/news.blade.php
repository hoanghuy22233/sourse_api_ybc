<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
{{--<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>--}}
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<style>

    .f2-h {
        width: 30% !important;
    }

    div#hpanel a.manuface{
        width: 23%;
        height: auto;
        padding: 5px;
        margin: 1%;
        border-radius: 10px;
        border: 1px solid #eee;
    }

    .f2-hsx a {
        height: 25%;
    }

    .new_post {
        width: 69% !important;
        margin-left: 1%;

    }

    .new_post label {
        display: block;
        font: 16px/40px arial;
        text-align: center;
        margin-bottom: 16px;
        background: #f6f6f6;
    }

    .cate_new_1_content ul, .cate_new_2_content ul, .cate_new_3_content ul {
        margin-top: 10px;
        max-height: 163px;
        overflow-y: auto;
    }

    .cate_new_1_content li, .cate_new_2_content li, .cate_new_3_content li {
        list-style: disc inside;
        overflow: hidden;
        text-overflow: ellipsis;
        width: 100%;
    }

    .f2-h label, .f2-topic label {
        font: 16px/40px arial;
    }

    .cate_new_1_header h4, .cate_new_2_header h4, .cate_new_3_header h4 {
        text-transform: uppercase;
        color: #fff;
    }

    .cate_new_1_header {
        background: #2ac90e;
    }

    .cate_new_2_header {
        background: #e69138;
    }

    .cate_new_3_header {
        background: #3d85c6;
    }

    .cate_newss {
        margin: 0;
        overflow: hidden;
        /*border: 1px solid #ddd;*/
    }
    .cate_new_content h3 {
        display: inline;
        font-size: 14px;
    }
    /*@media (min-width: 1366px) {*/
    /*    .hpanelwrap, .f2-topic p {*/
    /*        height: 224px;*/
    /*    }*/
    /*}*/
    @media (max-width: 991px) {
        .f2-h, .new_post {
            width: 100% !important;
        }
        .cate_newss {
            height: unset;
        }

        .news_none_mobie{
            display: none;
        }
        .cate_new_1_header h4, .cate_new_2_header h4, .cate_new_3_header h4 {
            margin: 0;
            padding: 10px 0px;
            font-size: 16px;
        }
        .row.cate_newss .cat-item {
            padding: 0;
            margin-bottom: 15px;
        }
        .hpanelwrap {
            height: unset;
            padding: 10px 5px;
        }
        .hpanelwrap #hpanel {
            position: relative !important;
        }
        .f2-head>.b>.f2-h {
            margin-bottom: 20px;
        }
    }
    @media(min-width: 768px) {
        .cate_newss > .cat-item:nth-child(1) {
            padding-left: 0;
        }

        .cate_newss > .cat-item:nth-child(2) {
            padding: 0;
        }

        .cate_newss > .cat-item:nth-child(3) {
            padding-right: 0;
        }
    }

    @media only screen and (max-width: 768px) {
        .f2-hsx a img {
            height: auto;
        }

        .cate_new_content{
            max-height: 130px;
            overflow-y: auto;
        }

    }
    .hpanelwrap, .f2-topic p {
        height: 224px!important;
    }
    @media (max-width: 768px){
        .hpanelwrap {
            height: unset !important;
        }
    }
</style>
<div class="f f2-head">
    <div class="b">
        <div class="flexCol f2-h fl">
            <label>CÁC THƯƠNG HIỆU NỔI TIẾNG</label>
            <div class="f flexCol f2-hsx ">
                <div class="hpanelwrap" style="overflow-y:auto;">
                    <div id="hpanel" style="width:100%;position:absolute;left:0;top:0; bottom: 0">
                        @php
                            $brans = \Modules\ThemeBoschHome\Models\Manufacturer::where('status', 1)->whereIn('id', $bran_ids)->orderBy('order_no', 'asc')->get();
                        @endphp
                        @if(!empty($brans))
                            @foreach($brans as $bran)

                                <a class="manuface" data-value="{{$bran->id}}" title="{{$bran->name}}"
                                   href="{{route('cate.list',['slug' => $category->slug.'/'.$bran->slug])}}">
                                    <img class="lazy" data-src="{{\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getUrlImageThumbNoCut($bran->image)}}"
                                         alt="{{$bran->image}}"/>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>

            </div>
            <script>
                $(document).ready(function () {
                    $('#viewfullsapo').click(function () {
                        $('#sapo').css('height', 'auto');
                        $(this).hide();
                    });
                });
            </script>
        </div>
        <div class="flexCol new_post fl">
            <label>TIN TỨC</label>
            <?php
            $category1 = \Modules\ThemeBoschHome\Models\Category::find(@$category->cate_new_1);
            $category2 = \Modules\ThemeBoschHome\Models\Category::find(@$category->cate_new_2);
            $category3 = \Modules\ThemeBoschHome\Models\Category::find(@$category->cate_new_3);
            $posts1 = \Modules\ThemeBoschHome\Models\Post::where('multi_cat', 'like', '%|' . @$category->cate_new_1 . '|%')->where('status', 1)->limit(15)->orderBy('id', 'decs')->get();
            $posts2 = \Modules\ThemeBoschHome\Models\Post::where('multi_cat', 'like', '%|' . @$category->cate_new_2 . '|%')->where('status', 1)->limit(15)->orderBy('id', 'decs')->get();
            $posts3 = \Modules\ThemeBoschHome\Models\Post::where('multi_cat', 'like', '%|' . @$category->cate_new_3 . '|%')->where('status', 1)->limit(15)->orderBy('id', 'decs')->get();

            ?>
            <div class="row cate_newss" style="">
                @if(!empty($category1))
                    <div class="col-sm-4 cat-item">
                        <div class="row" style="border: 1px solid #ddd; border-radius: 5px; margin: 0">
                            <div class="col-sm-12 col-12 cate_new_1_header">
                                <h4>{{@$category1->name}}</h4>
                            </div>
                            @if($posts1->count()>0)
                                <div class="col-sm-12 col-12 cate_new_content cate_new_1_content">
                                    <ul>

                                        @foreach($posts1 as $k1=>$post1)
                                            <li>
                                                <h3>
                                                    <a title="{{$post1->name}}"
                                                       href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($post1) }}">{{$post1->name}}</a>
                                                </h3>

                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
                @if($posts2->count()>0)
                    <div class="col-sm-4 cat-item">
                        <div class="row" style="border: 1px solid #ddd; border-radius: 5px; margin: 0">
                            <div class="col-sm-12 col-12 cate_new_header cate_new_2_header">
                                <h4>{{@$category2->name}}</h4>
                            </div>
                            <div class="col-sm-12 col-12 cate_new_content cate_new_2_content">
                                <ul>

                                    @foreach($posts2 as $k2=>$post2)
                                        <li>
                                            <h3>
                                            <a title="{{$post2->name}}"
                                               href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($post2) }}">{{$post2->name}}</a>
                                                </h3>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                @if($posts3->count()>0)
                    <div class="col-sm-4 cat-item">
                        <div class="row" style="border: 1px solid #ddd; border-radius: 5px; margin: 0">
                            <div class="col-sm-12 col-12 cate_new_header cate_new_3_header">
                                <h4>{{@$category3->name}}</h4>
                            </div>
                            <div class="col-sm-12 col-12 cate_new_content cate_new_3_content">
                                <ul>

                                    @foreach($posts3 as $k3=>$post3)
                                        <li>
                                            <h3>
                                            <a title="{{$post3->name}}"
                                               href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($post3) }}">{{$post3->name}}</a>
                                                </h3>
                                        </li>
                                    @endforeach


                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>