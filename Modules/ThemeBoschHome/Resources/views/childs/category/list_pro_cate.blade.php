<?php
//if (isset($product_sort_price)){
//    $products=$product_sort_price;
//    dd($product_sort_price);
//}
?>
<style>
    @media (min-width: 990px) {
        .popup-add-cart-content {
            margin-top: 10%;
        }
    }

    .popup-add-cart {
        z-index: 999;
    }

    .gri-center > h3 {
        margin: 0 0 20px 0;
    }

    .gri-center .gi h3 {
        line-height: 30px;
    }

    .gri-center > p {
        text-align: left;
        padding: 0 !important;
    }

    div.gi {
        display: block !important;
        height: auto !important;
    }

    .gri_left {
        width: 25% !important;
        max-width: 25% !important;
        float: left;
        position: relative;
        overflow: hidden;
        min-height: 400px;
    }

    .gri-center {
        width: 50% !important;
        max-width: 50% !important;
        float: left;
        text-align: left;
        padding: 0 20px;
    }

    .gri-right {
        width: 25% !important;
        max-width: 25% !important;
        float: left;
    }


    .gri > div:nth-last-child(1) {
        border: none !important;
    }



    .gri-right p b {
        color: red;
        display: block;
        font-size: 26px;
        margin-top: 0px;
    }

    .gift_content {
        text-align: left;
        margin-bottom: 20px;
    }


    @media screen and (max-width: 991px) and (min-width: 768px) {
        .f > #Product > .gri > .gi p {
            height: auto !important;
        }

        .sale_text {
            left: -20px;
            bottom: 85px;
            margin: auto !important;
            height: 25px !important;
        }
    }
    .product_intro{
        height: 55px!important;
        text-align: justify;
    }
    @media (max-width: 767px) {
        .product_intro{
            height: 85px!important;
            text-align: justify;
        }
        .gri_left {
            width: 100% !important;
            max-width: 100% !important;
            /*float: left;*/
            position: relative;
            overflow: hidden;
            height: auto;
            min-height: auto;
        }

        .gri-center {
            width: 100% !important;
            max-width: 100% !important;

            height: auto !important;
            /*float: left;*/
        }

        .gri-right {
            width: 100% !important;
            max-width: 100% !important;
            height: auto !important;
            /*float: left;*/
        }


        .f > #Product > .gri > .gi p {
            height: auto !important;
        }

        .gift label {
            top: -15px;
        }
    }

    .gi div .manuface_g {
        height: unset !important;
    }

    .reset_content span {
        width: 100% !important;
        border: none !important;
        line-height: 1.5 !important;
    }

    .reset_content p {
        height: auto !important;
    }

    #Product.list .gi .image-product,
    #Product.list .product-content-height,
    #Product.list .product-price {
        display: inline-block;
        float: left;
    }
</style>
<div class="f" style="background:#fff;min-height: 350px;">
    <div class="container list" id="Product">

        @foreach($products as $product)
            @php
                $manufacture = CommonHelper::getFromCache('manufacture_id'.$product['manufacture_id']);
                   if (!$manufacture){
                    $manufacture = \Modules\ThemeBoschHome\Models\Manufacturer::find($product['manufacture_id']);
                    CommonHelper::putToCache('manufacture_id'.$product['manufacture_id'], $manufacture);
                }
            @endphp


            @include('themeboschhome::childs.category.partials.product_item')
        @endforeach

    </div>
</div>
@if(count($products) < $countProduct)
    <div data-value="20" class="f" id="pmore"><p id="showModel">Xem thêm ...</p></div>
@endif



