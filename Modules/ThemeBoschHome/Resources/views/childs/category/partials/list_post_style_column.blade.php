<style>
    .o-teasercontainer:not(:first-child) {
        margin-top: 30px;
    }
    @media (min-width: 900px) {
        .m-teaser.teaser-200-4 {
            width: 25%;
            float: left;
            padding-left: 1.3888888889%;
            padding-right: 1.3888888889%;
        }
        .m-teaser.teaser-200-4 .teaser-media {
            width: 100%;
            float: left;
            margin-bottom: 20px;
            padding: 0 1.3888888889% 0 0;
        }
        .m-teaser.teaser-200-4 .a-image {
            display: block;
            margin: 0;
            position: relative;
            text-align: center;
            width: 100%;
        }
        .m-teaser.teaser-200-4 .a-image img {
            height: auto;
        }

        .m-teaser.teaser-200-4 .a-image img {
            display: inline-block;
            max-width: 100%;
            position: relative;
            vertical-align: top;
        }
        .m-teaser.teaser-200-4 .m-teaser.teaser-200-4 .teaser-content {
            padding-right: 0;
            padding-left: 0;
            padding: 0 0 0 1.3888888889%;
        }
        .m-teaser.teaser-200-4 .m-teaser .a-heading {
            line-height: 1.3;
            font-weight: 100;
            color: #000;
            margin-bottom: 20px;
            font-size: 1.5em;
        }
        .m-teaser.teaser-200-4 .m-teaser .content-inner {
            margin-bottom: 10px;
        }
        .m-teaser.teaser-200-4 .m-teaser .teaser-content .a-button {
            display: inline-block;
        }
        .m-teaser.teaser-200-4 .a-button .text {
            vertical-align: initial;
        }
        .m-teaser.teaser-200-4 .m-teaser .a-heading {
            margin-bottom: 20px;
            font-size: 1.5em;
            display: inline-block;
        }
        .m-teaser.teaser-200-4 .teaser-content {
            float: left;
        }
        .m-teaser.teaser-200-4 h2.a-heading a {
            line-height: 21px;
            margin-top: 0;
            margin-bottom: 20px;
            display: inline-block;
        }
    }
</style>
<div class="o-teasercontainer" data-t-name="TeaserContainer" data-t-id="79">
    @foreach($posts as $post)
        <div class="m-teaser teaser-200-4 row-0" data-t-name="Teaser" id="anc-7490071">
            <div class="teaser-wrap" style="height: 497px;">
                <div class="teaser-media">
                    <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($post) }}">
                        <div class="a-image" data-t-name="Image" data-t-id="80" style="height: 144px;">
                            <picture>
                                <img class="lazy"
                                     data-src="{{ CommonHelper::getUrlImageThumb($post->image, 300, null) }}">
                            </picture>
                        </div>
                    </a>
                </div>
                <div class="teaser-content" style="height: 333px;">
                    <h2 class="a-heading" style="height: 93px;"><a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($post) }}">{{$post->name}}</a></h2>
                    <div class="content-inner" style="height: 126px;">
                        <p>{{str_limit($post->intro, 230)}}</p>
                    </div>
                    <p>
                        <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($post) }}"
                           class="a-link a-link-local a-button a-button-primary "><span class="text">Xem thêm</span></a>
                    </p>
                </div>
            </div>
        </div>
    @endforeach
</div>
{{ @$posts->links() }}