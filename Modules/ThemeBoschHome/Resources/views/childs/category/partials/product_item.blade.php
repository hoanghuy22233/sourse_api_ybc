<div class="gri"
     href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
     title="{{@$product['meta_title']}}">
    <div class="gi">
        @php
            if (@$product['final_price'] != 0 && @$product['base_price'] != 0){
            $discount = 100 - ((@$product['final_price'])/ @$product['base_price'] * 100);
            }
            else{
            $discount = 0;
            }
        @endphp
        @if($discount > 0)
            <div class="badge @if(round($discount) >= 20) red_sale @else orange_sale @endif">
                <div class="sale_text">- {{round($discount)}} %</div>
            </div>
        @endif
        <a class="image-product"
           href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
           title="{{@$product['meta_title']}}">
            <img class="lazy" data-src="{{CommonHelper::getUrlImageThumb($product['image'], 250, null) }}"
                 alt="{{$product['name']}}"/>
        </a>
        <div class="product-content-height">
            <div class="manuface_g">
                @if(!empty($manufacture))
                    <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                       title="{{@$product['meta_title']}}">
                        <img class="lazy" height="50"
                             data-src="{{CommonHelper::getUrlImageThumb($manufacture->image, 150 ,null) }}"
                             alt="{{$manufacture->name}}"/>
                    </a>
                    @else
                    </br></br></br>
                @endif
            </div>
            <h2>
                <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                   title="{{@$product['meta_title']}}">{{$product['name']}}</a></h2>
            <div class="sku">{{$product['code']}}</div>
            <p class="product_intro">{!! $product['intro'] !!}</p>
            <div class="product_content">{!! $product['intro'] !!}</div>
        </div>
        <div class="product-price">
            @if($product['final_price'] < $product['base_price'])
                <span class="real-price" style="padding-top: 12px;"><u
                            style="text-decoration: line-through;font-size: 18px">{{number_format($product['base_price'], 0, '.', '.')}}</u><sup
                            style="font-size: 18px;"> đ</sup></span>
            @else
                <div style="height: 32px;"></div>
            @endif
            @if($product['final_price'] != 0)
                <div class="final-price">@if($product['final_price'] < $product['base_price']) @else
                        Giá
                        bán @endif{{number_format($product['final_price'], 0, '.', '.')}}&nbsp;<sup>đ</sup>
                </div>
                @else
                </br> <span style="font-size: 25px; width: 100%!important;" class="pr">Liên hệ</span>
            @endif
            @if($product['final_price'] < $product['base_price'])
                <span class="down-price">Giảm: {{number_format($product['base_price'] - $product['final_price'], 0, '.', '.')}} &nbsp;<sup>đ</sup></span>
            @else
                <div style="height: 24px;border-top: none"></div>
            @endif
            @if(@$settings['show_detail'] == 1 && @$settings['show_add_cart'] == 0)
                <div class="action-container">
                    <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                       title="{{@$product['meta_title']}}"><span class="gri-view-pro" style="width: 100%!important;
                                        ">Xem Sản Phẩm</span></a>
                </div>
            @elseif(@$settings['show_detail'] == 0 && @$settings['show_add_cart'] == 1)
                <div class="action-container">
                        <span data-text="{{route('order.view')}}" style="cursor: pointer;width: 100%!important;"
                              {{--                              onclick="(addCart1({{@$product['id']}}))"--}}
                              class="gri-add-cart urlCart"><i class="fal fa-shopping-cart"></i> Cho vào giỏ</span>
                </div>

            @elseif(@$settings['show_detail'] == 1 && @$settings['show_add_cart'] == 1)
                <div class="action-container">
                    <div class="added-cart">
                        <i class="fal fa-times"></i>
                        <h2>Item added to cart</h2>
                        <div class="info-item">
                            <div class="img-item">
                                <img class="lazy"
                                     data-src="{{CommonHelper::getUrlImageThumb($product['image'], 250, null) }}"
                                     alt="{{$product['name']}}"/>
                            </div>
                            <div class="content-item">
                                <div class="sku">{{$product['code']}}</div>
                                <div class="final-price">{{number_format($product['final_price'], 0, '.', '.')}}
                                    <sup>đ</sup></div>
                                <div class="quantity">Quantity: <span id="number">1</span></div>
                            </div>
                        </div>
                        <a class="shopping-cart-btn" href="/gio-hang">Xem giỏ hàng</a>
                    </div>
                    <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                       title="{{@$product['meta_title']}}"><span
                                class="gri-view-pro">Xem Sản Phẩm</span></a>
                    <span data-text="{{route('order.view')}}" style="cursor: pointer"
                          data-id="{{@$product['id']}}"
                          class="gri-add-cart urlCart"><i class="fal fa-shopping-cart"></i> Cho vào giỏ
                                    </span>
                </div>
            @elseif(@$settings['show_detail'] == 0 && @$settings['show_add_cart'] == 0)
                <div style="display: none;"></div>
            @endif
        </div>


    </div>

    <?php
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $getDateToday = date_create(date('Y-m-d H:i:s'));
    $sales = [];


    $product_all = Modules\ThemeBoschHome\Models\ProductSale::where(function ($query) use ($getDateToday) {
        $query->orWhere('time_start', '<=', date_format($getDateToday, 'Y-m-d H:i:s'));
        $query->orWhere('time_start', null);
    })->where(function ($query) use ($getDateToday) {
        $query->orWhere('time_end', '>=', date_format($getDateToday, 'Y-m-d H:i:s'));
        $query->orWhere('time_end', null);
    })->get();

    $sale = [];
    foreach ($product_all as $key => $sale_val) {
        $manufacture = CommonHelper::getFromCache('manufacture_id_explode' . @$sale_val->manufacturer_ids);
        if (!$manufacture) {
            $manufacture = \Modules\ThemeBoschHome\Models\Manufacturer::whereIn('id', explode('|', @$sale_val->manufacturer_ids))->get();
            CommonHelper::putToCache('manufacture_id_explode' . @$sale_val->manufacturer_ids, $manufacture);
        }
        $cate_child = CommonHelper::getFromCache('category_id_explode' . @$sale_val->category_ids);
        if (!$cate_child) {
            $cate_child = \Modules\ThemeBoschHome\Models\Category::whereIn('id', explode('|', @$sale_val->category_ids))->get();
            CommonHelper::putToCache('category_id_explode' . @$sale_val->category_ids, $cate_child);
        }

        foreach ($cate_child as $rt) {
            if ($rt->parent_id == 0 && in_array($rt->id, explode('|', @$sale_val->category_ids)) == true && in_array($rt->id, explode('|', @$product->multi_cat)) == true) {
                $sale = Modules\ThemeBoschHome\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
            } elseif (in_array($rt->id, explode('|', @$sale_val->category_ids)) == true && in_array($rt->id, explode('|', @$product->multi_cat)) == true) {
                $sale = Modules\ThemeBoschHome\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
            } else {
                $sale = [];
            }
        }
//                        if ($sale == []) {
        foreach ($manufacture as $rts) {
            if (in_array($rts->id, explode('|', @$sale_val->manufacturer_ids)) == true && in_array($rts->id, explode('|', @$product->manufacture_id)) == true) {
                $sale = Modules\ThemeBoschHome\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
            } else {
                $sale = [];
            }
        }
//                        }
        if (array_diff([@$product->id], explode('|', @$sale_val->id_product)) == []) {
            $sale = CommonHelper::getFromCache('product_id_explode_sale_val_id_product_sale' . @$sale_val->id_product_sale);
            if (!$sale) {
                $sale = Modules\ThemeBoschHome\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                CommonHelper::putToCache('product_id_explode_sale_val_id_product_sale' . @$sale_val->id_product_sale, $sale);
            }

        }
        if (!empty($sale)) {
            $sales = array_merge($sales, $sale->toArray());
        }
    }
    $ids = array_column($sales, 'id');
    $ids = array_unique($ids);
    $sales = array_filter($sales, function ($key, $value) use ($ids) {
        return in_array($value, array_keys($ids));
    }, ARRAY_FILTER_USE_BOTH);
    ?>


</div>