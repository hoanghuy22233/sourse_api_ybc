@extends('themeboschhome::layouts.master')
@section('main_content')
    <div class="b" style="margin:0px auto;">
        @section('name', 'Tìm kiếm')
        @include('themeboschhome::partials.breadcrumb')
    </div>
    <section>
        <h1 class="productname" itemprop="name">Tìm kiếm</h1>
        <div class="dtop">
            <div class="ds">
                Danh sách <b>{{$count}} </b> sản phẩm được tìm thấy cho từ khóa <b>{{$keyword}}</b>
            </div>
        </div>
        <div class="product">
            @if(!empty($products))
                @php
                $getId = '';
                    foreach ($products as $product){
                    $getId .= $product->proprerties_id;
                  }
                $getId = array_unique(explode('|', $getId));
                 if ($getId['0'] == ''){
                        array_shift($getId);
                    }
                @endphp
                @foreach($products as $product)
                    <?php
                        $cate = CommonHelper::getFromCache('get_cate_by_search'.$product->id . $product->multi_cat);
                        if (!$cate){
                            $cate = Modules\ThemeBoschHome\Models\Category::whereIn('id', explode('|', $product->multi_cat))->where('status', 1)->first();
                           if (isset($cate) && $cate->parent_id > 0){
                            $cate = Modules\ThemeBoschHome\Models\Category::where('id', $cate->parent_id)->where('status', 1)->first();
                           }
                            CommonHelper::putToCache('get_cate_by_search'.$product->id . $product->multi_cat, $cate);
                        }
                    ?>
                    <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($product) }}"
                       title="{{@ucfirst(mb_strtolower($cate->name))}}">
                        <div class="pi">
                            <img class="lazy"
                                 data-src="{{CommonHelper::getUrlImageThumb($product->image, 320, null) }}"
                                 alt="{{ucfirst(mb_strtolower($product->name))}}"/>
                        </div>
                        <p class="pn">{{ucfirst(mb_strtolower($product->name))}}</p>
                      <span class="pr">
                            @if($product->final_price != 0)
                                {{number_format($product->final_price, 0, '.', '.')}}&nbsp;<sup>đ</sup>
                            @else
                                <span class="pr">Liên hệ</span>
                            @endif
                        </span>
                        <div class="op">
                        <?php
                            $propreti_value = CommonHelper::getFromCache('propertie_value_properties_name_id_$getId' . implode('|', $getId));
                            if (!$propreti_value) {
                                $propreti_value = Modules\ThemeBoschHome\Models\PropertieValue::whereIn('properties_name_id', $getId)->get()->toArray();
                                CommonHelper::putToCache('propertie_value_properties_name_id_$getId' . implode('|', $getId), $propreti_value);
                            }

                             $propreti_name = CommonHelper::getFromCache('propertieName_id_explode'.@$cate->properties_name_id);
                                if (!$propreti_name) {
                                    $propreti_name = Modules\ThemeBoschHome\Models\PropertieName::whereIn('id', explode('|', @$cate->properties_name_id))
                            ->where('show_field_search', 1)->where('status', 1)->orderBy('order_no', 'asc')->limit(4)->get();
                                    CommonHelper::putToCache('propertieName_id_explode'.@$cate->properties_name_id, $propreti_name);
                                }

                        ?>
                       <label>Đặc điểm</label>
                                <ul class="pp">
                                    @if($product->final_price >= 400000)
                                     <li>Mức giá: Dưới {{\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::convert_number_to_words(ceil($product->final_price))}}</li>
                                    @endif
                                    @if(count($propreti_name) > 0)
                                        @foreach($getId as $l)
                                            <?php
                                                $h = CommonHelper::getFromCache('propertie_value_find'.@$l);
                                                if (!$h) {
                                                    $h = Modules\ThemeBoschHome\Models\PropertieValue::find($l);
                                                    CommonHelper::putToCache('propertie_value_find'.@$l, $h);
                                                }
                                                ?>
                                                @foreach($propreti_name as $name)
                                                    @if(isset($h) && $name->id == $h->properties_name_id && in_array($h->id, explode('|', $product->proprerties_id)))
                                                        <li>{{$name->name}} : {{$h->value}} </li>
                                                    @endif
                                                @endforeach
                                        @endforeach
                                    @endif
                                </ul>
                        </div>
                        <p class="btn bcam" data-item="5441">Thêm vào giỏ hàng</p>
                    </a>
                @endforeach
            @endif
        </div>
    </section>
    <div style="clear: both">
        <div style="margin: auto; text-align: center">{!! $products->appends(['key' => Request::get('key')])->links() !!}</div>
    </div>
    <style>
        .product a {
            float: left;
            width: 24.2%;
            margin: 0 1% 1% 0;
            border-radius: 4px;
            padding: 1%;
            position: relative;
        }

        .product a:nth-child(4n) {
            margin-right: 0;
        }
        @media(max-width: 768px) {
            .product > a {
                width: 50%;
                float: left;
                margin: 0;
            }
        }
    </style>
@endsection