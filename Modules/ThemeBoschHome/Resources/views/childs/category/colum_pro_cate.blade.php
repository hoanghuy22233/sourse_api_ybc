<style>
    .popup-add-cart {
        z-index: 999;
    }

    {{--.gri-view-pro, .gri-add-cart{--}}
    {{--    background: @if(@$settings['header_background']!=''){{@$settings['header_background']}} @else {{'linear-gradient(to bottom,#ffa103,#fb7d0f)'}}@endif!important;--}}
    {{--    border-radius: 4px!important;--}}
    {{--    border: none!important;--}}
    {{--    color: #fff!important;--}}
    {{--}--}}
    @media (min-width: 990px) {
        .popup-add-cart-content {
            margin-top: 10%;
        }
    }

    .product_intro {
        text-align: justify;
    }

    @media (max-width: 768px) {
        .gri {
            width: 46%;
            margin: 2%;
        }

        .banner-abc ul.flexJus {
            height: unset;
        }

        /*.f2-head>.b>.f2-topic ul li{*/
        /*    width: 100% !important;*/
        /*}*/
        .product_intro {
            text-align: justify;
        }
    }

    span.gri-add-cart.urlCart {
        float: right !important;
    }

    @media (max-width: 767px) {
        .f > .inline-bl .flexJus {
            text-align: center;
        }
    }
</style>
<div class="f" style="background:#fff;">
    <div class="container list" id="Product">
        @foreach($products as $product)
            <?php
            $manufacture = CommonHelper::getFromCache('manufacture_id' . @$product['manufacture_id']);
            if (!$manufacture) {
                $manufacture = \Modules\ThemeBoschHome\Models\Manufacturer::find($product['manufacture_id']);
                CommonHelper::putToCache('manufacture_id' . @$product['manufacture_id'], $manufacture);
            }
            ?>
            @include('themeboschhome::childs.category.partials.product_item')

        @endforeach

    </div>
</div>

@if(count($products) < $countProduct)
    <div data-value="20" class="f" id="pmore"><p id="showModel">Xem thêm ...</p></div>
@endif


