@extends('themeboschhome::layouts.master')
@section('main_content')
    <style>
        .cate-list-title > h2 {
            margin: 0;
        }
        .cate-list-title {
            display: flex;
            margin-bottom: 10px;

            background: #2785cb;
            padding: 10px 15px;
        }

        .cate-list-title > a {
            text-align: right;
            width: 20%;
            color: #fff;
            font-size: 16px;
        }

        .cate-list-title > h2 {
            width: 80%;
            color: #fff;
            font-size: 20px;
            text-align: left;
            text-transform: uppercase;
        }

        .cate-list-post-content {
            display: flex;
        }

        .cate-list-post-detail {
            width: 25%;
        }

        .cate-list-post-detail-img {
            width: 100%;
            text-align: center;
            padding: 15px;
            height: unset;
        }

        .cate-list-post-detail-img img {
            max-width: 100%;
            max-height: 100%;
        }

        .cate-list-post-detail-title {
            width: 100%;
        }

        .cate-list-post-detail-title h3 {
            font-size: 16px;
            text-transform: capitalize;
            text-align: center;
            padding: 10px 0;
        }

        .cate-list {
            margin: 20px 15px;
        }

        @media (max-width: 768px) {

            .cate-list-post-content {
                flex-wrap: wrap;
            }

            .cate-list-post-detail {
                width: 50%;
            }
        }
    </style>
    <div class="f">
        <div class="container list">

            <div class="cate-list">
                <div class="cate-list-post">
                    <div class="cate-list-title">
                        <?php
                        $posts = \Modules\ThemeBoschHome\Models\Post::where('multi_cat', 'like', '%|' . $category->id . '|%')->where('status', 1)->orderby('id', 'desc')->take(4)->get();
                        ?>
                        <h2>{{$category->name}}</h2>
                        <a href="{{$category->url}}">Xem toàn bộ...</a>
                    </div>
                    <div class="cate-list-post-content">
                        @foreach($posts as $p=>$post)
                            <div class="cate-list-post-detail">
                                <div class="cate-list-post-detail-img">
                                    <a href="{{asset(\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($post))}}">
                                        <img src="{{asset('public/filemanager/userfiles/'.$post->image)}}"
                                             alt="{{$post->name}}">
                                    </a>

                                </div>
                                <div class="cate-list-post-detail-title">
                                    <a href="{{asset(\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($post))}}">
                                        <h3>{{$post->name}}</h3>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>


            <?php
            $categoryChilds = CommonHelper::getFromCache('categorys_childs' . $category->id);
            if (!$categoryChilds) {
                $categoryChilds = isset($cate) ? $cate->childs : $category->childs;
                CommonHelper::putToCache('categorys_childs' . $category->id, $categoryChilds);
            }
            ?>
            @foreach($categoryChilds as $k=>$cate_child)
                <?php
                $category_post = CommonHelper::getFromCache('category_slug' . $cate_child->url);
                if (!$category_post) {
                    $category_post = @\Modules\ThemeBoschHome\Models\Category::where('slug', trim($cate_child->url, '/'))->first()->id;
                    CommonHelper::putToCache('category_slug' . $cate_child->url, $category_post);
                }
                $posts = [];
                if (!empty($category_post)) {
                    $posts = \Modules\ThemeBoschHome\Models\Post::where('multi_cat', 'like', '%|' . $category_post . '|%')->where('status', 1)->orderby('id', 'desc');
                }
                ?>
                <div class="cate-list">
                    <div class="cate-list-post">
                        <div class="cate-list-title">
                            <h2>{{$cate_child->name}}</h2>
                            <?php
                            $postsGetCount = CommonHelper::getFromCache('posts_get_count');
                            if (!$postsGetCount) {
                                $postsGetCount = $posts->count();
                                CommonHelper::putToCache('posts_get_count', $postsGetCount);
                            }
                            ?>
                            @if($postsGetCount > 4)
                                <a href="{{$cate_child->url}}">Xem toàn bộ...</a>
                            @endif
                        </div>
                        <div class="cate-list-post-content">
                            <?php
                            $postsTakeGet = CommonHelper::getFromCache('posts_take_get');
                            if (!$postsTakeGet) {
                                $postsTakeGet = $posts->take(4)->get();
                                CommonHelper::putToCache('posts_take_get', $postsTakeGet);
                            }
                            ?>
                            @foreach($postsTakeGet as $p=>$post)
                                <div class="cate-list-post-detail">
                                    <div class="cate-list-post-detail-img">
                                        <a href="{{asset(\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($post))}}">
                                            <img src="{{asset('public/filemanager/userfiles/'.$post->image)}}"
                                                 alt="{{$post->name}}">
                                        </a>

                                    </div>
                                    <div class="cate-list-post-detail-title">
                                        <a href="{{asset(\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($post))}}">
                                            <h3>{{$post->name}}</h3>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('custom_header')
    <link href="{{ URL::asset('public/frontend/themes/stbd/css/bpr-products-module4826.css') }}" rel='stylesheet'
          type='text/css'/>
@endsection

@section('custom_footer')

@endsection
