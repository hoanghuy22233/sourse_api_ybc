@extends('themeboschhome::layouts.master')
@section('main_content')
    @include('themeboschhome::partials.breadcrumb')
    <div class="container">
        <div class="row">
            @include('themeboschhome::partials.sidebar_post')
            <section id="custom_main_container" class="main_container collection col-md-9 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="container-fluid">
                        <div class="row">
                            @if(isset($category))
                                <div class="blog-all-background">
                                    <img data-src="{{ CommonHelper::getUrlImageThumb($category->banner_sidebar, 1920, 650) }}" alt="{{ $category->name }}" class="img-responsive lazy" style="width:100%" />
                                    <div class="blog-top-content">
                                        <h1>{{ $category->name }}</h1>
                                        <div class="blog-top-description"> {{ $category->intro }}</div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="feature-blogs-page section-news">
                                    <div class="blogs-content">
                                        <div class="heading a-center">
                                            <h2 class="title-head">
                                                <a href="tin-tuc.html">Xu hướng</a>
                                            </h2>
                                        </div>
                                        <div class="list-blogs-link">
                                            <div class="section-blog-sliders owl-carousel not-dqowl">
                                                <?php
                                                $post = CommonHelper::getFromCache('get_tags_posts_by_category_id_'.$category->id);
                                                if (!$post) {
                                                    $post = \Modules\ThemeBoschHome\Models\Post::select(['name', 'image', 'slug', 'tendency', 'tags'])->where('tags', 'LIKE', '%|'.$category->id.'|%')
                                                        ->where('status', 1)->orderBy('updated_at', 'desc')->limit(4)->get();
                                                    CommonHelper::putToCache('get_posts_by_category_id_'.$category->id, $post);

                                                }
                                                ?>
                                                @foreach($post as $item)
                                                    <div class="item">
                                                        <article class="blog-item">
                                                            <div class="blog-item-thumbnail">
                                                                <a href="{{ URL::to($item->slug) }}">

                                                                    <picture>
                                                                        <img class="lazy" data-src="{{ CommonHelper::getUrlImageThumb($item->image, 367, 275) }}" alt="{{$item->name}}" />
                                                                    </picture>

                                                                </a>
                                                            </div>
                                                            <div class="blog-item-main-header">
                                                                <h3 class="blog-item-name">
                                                                    <a href="{{ URL::to($item->slug) }}" title="{{$item->name}}">{{$item->name}}</a></h3>
                                                                <div class="post-time">
                                                                    {{$item->updated_at}}
                                                                </div>
                                                            </div>
                                                        </article>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="row">
                            <section class="right-content col-md-12">

                                <section class="list-blogs blog-main">
                                    <div class="row">

                                        @foreach($post as $item)
                                            <article class="blog-item col-sm-6 col-xs-12 col-md-4">
                                                <h3 class="blog-item-name"><a href="{{ URL::to($item->slug) }}" title="{{ $item->name }}">{{ $item->name }}</a></h3>
                                                <div class="blog-item-thumbnail">
                                                    <div class="image-effect">
                                                        <a href="{{ URL::to($item->slug) }}">

                                                            <picture>
                                                                <img data-src="{{ CommonHelper::getUrlImageThumb($item->image, 360, 270) }}" alt="{{ $item->name }}" class="lazy img-responsive" />
                                                            </picture>

                                                        </a>
                                                    </div>
                                                </div>
                                                <?php
                                                $admin = $item->admin_id;
                                                if (isset($admin) || $admin == 0){
                                                    $user = \Modules\ThemeBoschHome\Models\Admin::where('id','1')->first();
                                                }
                                                else{
                                                    $user = \Modules\ThemeBoschHome\Models\Admin::where('id',$admin)->first();
                                                }
                                                ?>
                                                <div class="post-time">{{$item->created_at}} - {{$user->username}}</div>
                                                <p class="blog-item-summary margin-bottom-5">  {!! $item->intro !!}</p>
                                                <a href="{{ URL::to($item->slug) }}" class="a-more">Chi tiết <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                                            </article>
                                        @endforeach
                                    </div>
                                    {!! $posts->appends(isset($filter) ? $filter : '')->links() !!}

                                </section>

                            </section>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="bizweb-product-reviews-module"></div>

@endsection

@section('custom_header')
    <link href="{{ URL::asset('public/frontend/themes/stbd/css/bpr-products-module4826.css') }}" rel='stylesheet' type='text/css'/>
@endsection

@section('custom_footer')

@endsection
