@extends('themeboschhome::layouts.master')
@section('main_content')
    <section>
        @include('themeboschhome::partials.breadcrumb')
        <style type="text/css">
            #info, #morei {
                display: none;
            }

            .owl-carousel {
                margin: 0 !important;
            }

            #Showroom {
                margin: 0 auto;
            }

            #Showroom h1, .sdtit {
                float: left;
                text-align: center;
                color: #333;
                font: 23px/26px arial;
                display: block;
                width: 100%;
                text-transform: capitalize;
            }

            #Showroom h1 {
                margin: 20px 0 30px 0
            }

            .sdtit {
                display: block;
                margin: 50px 0 30px 0;
            }

            #infoSR {
                width: 340px;
                float: left;
                margin-right: 30px;
            }

            #infoSR img {
                max-width: 100%;
                margin-bottom: 20px;
            }

            #infoSR span {
                font: 12px/20px arial;
                display: block;
                margin-top: 5px
            }

            #infoSR span b {
                color: #555
            }

            #mapSR {
                width: 830px;
                float: left;
            }

            #introSR {
                width: 100%;
                float: left;
                margin-bottom: 20px;
                font: 13px/20px arial;
                color: #666;
                text-align: justify
            }

            #nvSR {
                width: 100%;
                float: left;
                text-align: center;
                padding: 0;
            }

            #nvSR li {
                width: 320px;
                display: inline-block;
                text-align: left
            }

            #nvSR li img {
                width: 70px;
                float: left;
                height: 80px;
                border-radius: 10px;
                border: 2px solid #ddd;
                box-shadow: 5px 5px 5px #aaa;
                margin-right: 12px;
            }

            #nvSR li p, #nvSR li a {
                color: #666;
                font: bold 12px/20px arial;
                display: block
            }

            #nvSR li p:before, #nvSR li a:before {
                color: #215f8b;
                content: "\f095";
                font: 10px/16px FontAwesome;
                padding: 2px;
                border: 1px solid #215f8b;
                border-radius: 4px;
                text-align: center;
                width: 16px;
                height: 16px;
                margin-right: 5px;
            }

            #nvSR li a:before {
                content: "\f17e";
            }

            #nvSR .m:before {
                content: "\f003";
            }

            #nvSR li span {
                font: 12px/20px arial
            }

            #nvSR li span b {
                color: #215f8b;
                font-size: 13px;
            }

            .pbox ul {
                padding: 0%;
                width: 100%;
            }

            .pbox li {
                width: 14%;
                float: left;
                margin: 1%;
                padding: 1%;
                border: 1px solid #ddd;
                list-style: none;
                min-height: 220px;
                text-align: center;
            }

            .pbox li img {
                max-width: 80%;
            }

            .pbox li h3 {
                margin: 0;
                height: 50px;
                text-align: center;
            }

            .pbox li h3 a {
                color: #333;
                font: bold 15px/50px arial;
            }

            #slideshowroom {
                width: 980px;
                margin: 0 auto;
            }

            .clear {
                clear: both;
            }

            iframe {
                height: auto;
            }

            @media screen and (max-width: 481px) {
                #Showroom {
                    margin: 1%;
                    width: 98%;
                    padding: 0;
                }

                #infoSR, #mapSR {
                    margin: 0 auto 10px;
                    width: 100%
                }

                #Showroom h1, .sdtit {
                    font-size: 20px;
                }

                #nvSR li {
                    width: 320px;
                    margin: 0 auto 10px;
                }

                iframe {
                    display: block;
                }

                .home {
                    width: 45% !important;
                }
            }
        </style>
        <style type="text/css">
            #info, #morei {
                display: none;
            }

            .owl-carousel {
                margin: 0 !important;
            }

            #Showroom {
                margin: 0 auto;
            }

            #Showroom h1, .sdtit {
                float: left;
                text-align: center;
                color: #333;
                font: 23px/26px arial;
                display: block;
                width: 100%
            }

            #Showroom h1 {
                margin: 20px 0 30px 0
            }

            .sdtit {
                display: block;
                margin: 50px 0 30px 0;
            }

            #infoSR {
                width: 340px;
                float: left;
                margin-right: 30px;
            }

            #infoSR img {
                max-width: 100%;
                margin-bottom: 20px;
            }

            #infoSR span {
                font: 12px/20px arial;
                display: block;
                margin-top: 5px
            }

            #infoSR span b {
                color: #555
            }

            #mapSR {
                width: 830px;
                float: left;
            }

            #introSR {
                width: 100%;
                float: left;
                margin-bottom: 20px;
                font: 13px/20px arial;
                color: #666;
                text-align: justify
            }

            #nvSR {
                width: 100%;
                float: left;
                text-align: center;
                padding: 0;
            }

            #nvSR li {
                width: 320px;
                display: inline-block;
                text-align: left
            }

            #nvSR li img {
                width: 70px;
                float: left;
                height: 80px;
                border-radius: 10px;
                border: 2px solid #ddd;
                box-shadow: 5px 5px 5px #aaa;
                margin-right: 12px;
            }

            #nvSR li p, #nvSR li a {
                color: #666;
                font: bold 12px/20px arial;
                display: block
            }

            #nvSR li p:before, #nvSR li a:before {
                color: #215f8b;
                content: "\f095";
                font: 10px/16px FontAwesome;
                padding: 2px;
                border: 1px solid #215f8b;
                border-radius: 4px;
                text-align: center;
                width: 16px;
                height: 16px;
                margin-right: 5px;
            }

            #nvSR li a:before {
                content: "\f17e";
            }

            #nvSR .m:before {
                content: "\f003";
            }

            #nvSR li span {
                font: 12px/20px arial
            }

            #nvSR li span b {
                color: #215f8b;
                font-size: 13px;
            }

            .pbox ul {
                padding: 0%;
                width: 100%;
            }

            .pbox li {
                width: 14%;
                float: left;
                margin: 1%;
                padding: 1%;
                border: 1px solid #ddd;
                list-style: none;
                min-height: 220px;
                text-align: center;
            }

            .pbox li img {
                max-width: 80%;
            }

            .pbox li h3 {
                margin: 0;
                height: 50px;
                text-align: center;
            }

            .pbox li h3 a {
                color: #333;
                font: bold 15px/50px arial;
            }

            #slideshowroom {
                width: 980px;
                margin: 0 auto;
            }

            .clear {
                clear: both;
            }

            iframe {
                height: 400px;
            }

            @media screen and (max-width: 481px) {
                #Showroom {
                    margin: 1%;
                    width: 98%;
                    padding: 0;
                }

                #infoSR, #mapSR {
                    margin: 0 auto 10px;
                    width: 100%
                }

                #Showroom h1, .sdtit {
                    font-size: 20px;
                }

                #nvSR li {
                    width: 320px;
                    margin: 0 auto 10px;
                }

                iframe {
                    display: block;
                }

                .home {
                    width: 45% !important;
                }
            }
            div#mapSR iframe {
                width: 100%!important;
            }
        </style>
        <div id="Showroom">
            @if(!empty($showroom))
                <h1>Showroom {{ucfirst(mb_strtolower($showroom->name))}}</h1>
                <div style="text-align: center; font-size: 15px;" id="introSR">{!! $showroom->intro !!}</div>
                <div id="detailShowroom" style="display:flex;justify-content:center;float:left;width:100%;height: auto">
                    <div id="infoSR">
                        {!! $showroom->address !!}
                        <span style="margin-top: 5px;"></span>
                        {!! $showroom->contact !!}
                    </div>
                    <div id="mapSR">
                        <script type="text/javascript">
                            var srw = $(window).width();
                            var mapw = 830, maph = 400;
                            if (srw < 481) {
                                mapw = srw - 10;
                                maph = 230;
                            }
                            document.write('{!! $showroom->url_map !!}');
                        </script>

                    </div>
                </div>
                <div style="clear: both !important;"></div>
                <span class="sdtit">Đội ngũ nhân viên kinh doanh</span>
                <ul id="nvSR">
                    {!! $showroom->staff !!}
                </ul>

            @endif
            <span class="sdtit">Toàn cảnh cửa hàng</span>
            <br class="clear">

            <div style="width: 80%; margin: auto">
                @include('themeboschhome::partials.slide_showroom')
            </div>

            <span class="sdtit">Mặt hàng kinh doanh tại cửa hàng</span>
            <div class="pbox">
                <ul>
                    @if(!empty($get_category))
                        @foreach($get_category as $cate)
                            <li><a style="background-color: white" title="{{ucfirst(mb_strtolower($cate->name))}}"
                                   href="{{route('cate.list', ['slug' => $cate->slug])}}" class="bg_anh">
                                    <img class="lazy" alt="{{ucfirst(mb_strtolower($cate->name))}}"
                                         data-src="{{CommonHelper::getUrlImageThumb($cate->image, 150, null) }}"></a>
                                <h3><a title="{{$cate->name}}"
                                       href="{{route('cate.list', ['slug' => $cate->slug])}}">{{ucfirst(mb_strtolower($cate->name))}}</a>
                                </h3>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>

        </div>
    </section>
@endsection