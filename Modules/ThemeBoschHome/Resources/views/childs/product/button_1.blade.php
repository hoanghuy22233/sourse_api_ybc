<style>
    .popup-add-cart {
        position: fixed;
        top: 0;
        left: 0;

        right: 0;
        bottom: 0;

    }

    .popup-add-cart-content {
        width: 320px;
        margin: 0 auto;
        border: 1px solid #ccc;
        background: #fff;
    }

    .popup-add-cart p {
        text-align: center;
        margin: 15px 0;
    }

    a.btn-cart {
        background: #63cc50;
        text-transform: capitalize;
    }

    button.close-pop {
        background: #5467f1;
        border: none;
        text-transform: capitalize;
    }

    .btn-check {
        text-align: center;
        padding: 10px;
    }

    a.btn-cart:hover, button.close-pop:hover {
        color: #000;
    }
    {{--        .f2-head > .b > .f2-h {--}}
    {{--            width: 100% !important;--}}
    {{--        }--}}

    {{--        .thuonghieu a {--}}
    {{--            width: 14.6%;--}}
    {{--            padding: 3px;--}}
    {{--            float: left;--}}
    {{--            background: #fff;--}}
    {{--            height: 80px;--}}
    {{--            overflow: hidden;--}}
    {{--            text-align: center;--}}
    {{--            border-radius: 10px;--}}
    {{--            margin: 1%;--}}
    {{--            border: 1px solid #eee;--}}
    {{--            display: flex;--}}
    {{--            justify-content: center;--}}
    {{--            align-items: center;--}}
    {{--        }--}}

    {{--        .cate-none {--}}
    {{--            display: none;--}}
    {{--        }--}}
</style>
<style>
    .addtocart_3CEN {
        background-color: #f7941d;
        margin-bottom: 10px;
    }

    .addtocart_3CEN:active, .addtocart_3CEN:focus, .addtocart_3CEN:hover {
        background-color: #ea8000 !important;
    }

    .btn_2o3k:active, .btn_2o3k:focus, .btn_2o3k:hover {
        outline: none;
        background-color: #c3000c !important;
    }

    .btn_2o3k {
        background-color: #e5101d;
        margin-bottom: 10px;
    }

    .isdt, .form-checksales-detail .btn-check-submit {
        background: linear-gradient(to left, #6161da, #2e0abd);
    }
</style>
<div class="da">
    <p data-text="{{route('order.view')}}"
       onclick="(addCart1({{@$product->id}}))" style="color:white" class=" urlCart btn3 addtocart_3CEN">Thêm vào giỏ
        hàng
    </p>
</div>
<div class="f isdt">
    <label class="label01">ĐĂNG KÝ MUA HÀNG</label>
    <label class="label02">NHẬN ƯU ĐÃI 5-10%</label>
    <small>Ưu đãi đặc quyền chỉ dành cho 100 khách hàng đầu tiên</small>
    <div class="form-group form-checksales-detail">
        <form method="post" class="sell" action="{{route('resign.contact')}}">
            <input type="hidden" name="ProductID" value="{{@$product->id}}">
            <input type="hidden" name="Product" value="{{@$product->name}}">
            <input type="hidden" name="State" value="Nhận ưu đãi ">
            <select name="province_uu_dai"  required="required" id="province_uu_dai" style="color: #000;
    padding: 5px;
    margin-bottom: 10px;
    width: 100%;
">
                <option value="" selected>Chọn tỉnh/thành</option>
                @foreach($province as $pp)
                    <option value="{{$pp->id}}">{{$pp->name}}</option>
                @endforeach
            </select>
            <input type="tel" pattern="(09|01[2|6|8|9])+([0-9]{8})\b"
                   title="Có 10 hoặc 11 số bắt đầu bằng 0" name="Phone" required="required"
                   class="form-controls Phone" placeholder="Nhập số điện thoại.">

            <input style="bottom: 3px; top:unset" class="btn btn-check-submit" type="submit" name="_w_action[AddPOST]"
                   value="ĐĂNG KÝ NGAY">
        </form>
    </div>
    <div class="clearfix"></div>
</div>
<div class="da" style="display: inline-block;width: 100%;">


    <p id="urlCart" data-text="{{route('order.view')}}"
       onclick="(addCart({{@$product->id}}))" style="color:white; display: inline-block;
    width: 100%;" class="btn3 btn_2o3k">MUA
        NGAY<span>(Xem hàng, không mua không sao)</span>
    </p>
    @if(@$settings['show_khao_sat'] == 1)
        <a style="display:block;margin-bottom:10px;" class="btn3 bcam"
           href="{{(@$settings['link_khao_sat']!='')?@$settings['link_khao_sat']:'#'}}" title="Khảo sát tư vấn lắp đặt">KHẢO
            SÁT TƯ VẤN LẮP ĐẶT MIỄN PHÍ<span>(Dịch vụ khảo sát tư vấn miễn phí)</span></a>
    @endif
    @if(@$settings['show_tra_gop'] == 1)
        <a style="display:block;margin-bottom:10px;" class="btn3 bcam mua_tra_gop_btn"
           href="{{(@$settings['link_tra_gop']!='')?@$settings['link_tra_gop']:'#'}}" title="Mua trả góp">MUA TRẢ
            GÓP</a>
    @endif
</div>


<div class="popup-add-cart" style="display: none">
    <div class="popup-add-cart-content">
        <p>Thêm thành công vào giỏ hàng!</p>
        <div class="btn-check">
            <a class="btn-cart btn" href="/gio-hang">Xem giỏ hàng</a>
            <button class="close-pop btn">Tiếp tục mua hàng</button>
        </div>
    </div>
</div>
{{--@include('themeboschhome::childs.product.mua_tra_gop')--}}
{{--<script>--}}
{{--    $(document).ready(function(){--}}
{{--        $('.mua_tra_gop_btn').click(function(){--}}
{{--            $('.mua-tra-gop-form').show();--}}
{{--        })--}}
{{--    })--}}
{{--</script>--}}
<script>
    $(document).ready(function () {
        $('.close-pop').click(function () {
            $('.popup-add-cart').hide();
        })
    })
</script>