<?php
$manufacturer = CommonHelper::getFromCache('manufacturer_id' . @$product->manufacture_id);
if (!$manufacturer) {
    $manufacturer = Modules\ThemeBoschHome\Models\Manufacturer::find($product->manufacture_id);
    CommonHelper::putToCache('manufacturer_id' . @$product->manufacture_id, $manufacturer);
}

$province = CommonHelper::getFromCache('province_orderBy_name_asc_get');
if (!$province) {
    $province = \Modules\ThemeBoschHome\Models\Province::orderBy('name', 'ASC')->get();
    CommonHelper::putToCache('province_orderBy_name_asc_get', $province);
}

$html_province = '<select name="province_home" id="province_home">';
foreach ($province as $p) {
    $html_province .= '<option value="' . $p->id . '">' . $p->name . '</option>';
}
$html_province .= '</select>';

$html_province1 = '<select name="province_tuvan" id="province_tuvan">';
foreach ($province as $p1) {
    $html_province1 .= '<option value="' . $p1->id . '">' . $p1->name . '</option>';
}
$html_province1 .= '</select>';

?>
@extends('themeboschhome::layouts.master')
@section('main_content')
    <style>
        .image-line {
            position: relative;
            padding-bottom: 25px;
        }

        .image-line::after {
            content: '';
            display: block;
            background-image: url(/public/frontend/themes/bosch/image/shape_horizontal.svg);
            height: 2px;
            background-size: cover;
            background-position: top center;
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
            transform: translateY(100%);
        }

        .dbox {
            background-image: url(/public/frontend/themes/bosch/image/supergraphic-gray-light.svg);
            background-size: cover;
            background-position: center center;
            padding-top: 30px;
        }

        @media (max-width: 768px) {
            .product-view .thumb-slide .slick-slide.slick-current {
                border: 1px solid #ccc;
                width: 63px !important;
                outline: none;
            }

            .slick-list.draggale,
            .product-view .main-slide .slick-slide {
                height: unset !important;
            }

            .product-view .addtocart_3CEN {
                width: 100%;
            }

            .product-view .tabdetail span {
                width: 33%;
                float: left;
                line-height: 20px;
                font-size: 10px;
                padding: 5px;
                text-align: center;
                border-right: 1px solid #ccc;
            }
        }

        @media (max-width: 768px) {
            div#SPTT1 {
                height: 417px;
            }

            div#SPTT1 span.psback {
                display: none;
            }

            div#SPTT1 > .pspanel,
            div#SPTT1 .pswrap {
                width: 100%;
            }

            div#SPTT1 a.psitem {
                height: unset;
                width: 50%;
                border: 0;
                float: left;
                display: inline-block;
                margin: 0;
            }
        }
    </style>
    <script>
        /** Init add main class for product page */
        if (window.innerWidth < 1024) {
            document.getElementsByTagName('body')[0].className += "product-view mobile";
        } else {
            document.getElementsByTagName('body')[0].className += "product-view";
        }
    </script>
    <input name="product_id" type="hidden" value="{{ $product->id }}">
    <div class="product-service container-fluid">
        <div class="container">
            <h1 id="name_pr" itemprop="name">{{$product->name}}</h1>
            <div class="image-slide">
                <div class="main-slide">
                    <div class="lazy" data-i="0"
                         data-src="{{ asset('public/filemanager/userfiles/'. $product->image) }}">
                        <img class="lazy" data-src="{{ asset('public/filemanager/userfiles/'. $product->image) }}"
                             id="getImage"
                             src="{{ asset('public/filemanager/userfiles/'. $product->image) }}"
                             alt="{{@$product->name}}"/></div>
                    @if(!empty($image_extras))
                        @foreach($image_extras as $key => $image_extra)
                            <div class="" data-i="{{$key+1}}"
                                 datrainghiem-imgta-src="{{ asset('public/filemanager/userfiles/' . $image_extra) }}">
                                <img class="lazy" src="{{ asset('public/filemanager/userfiles/' . $image_extra) }}"
                                     data-src="{{ asset('public/filemanager/userfiles/' . $image_extra) }}"
                                     alt="{{ $product->name . $key }}"/>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="thumb-slide">
                    <div class="lazy" data-i="0"
                         data-src="{{ asset('public/filemanager/userfiles/'. $product->image) }}">
                        <img class="lazy" data-src="{{ asset('public/filemanager/userfiles/'. $product->image) }}"
                             id="getImage"
                             src="{{ asset('public/filemanager/userfiles/'. $product->image) }}"
                             alt="{{@$product->name}}"/></div>
                    @if(!empty($image_extras))
                        @foreach($image_extras as $key => $image_extra)
                            <div class="" data-i="{{$key+1}}"
                                 datrainghiem-imgta-src="{{ asset('public/filemanager/userfiles/' . $image_extra) }}">
                                <img class="lazy" src="{{ asset('public/filemanager/userfiles/' . $image_extra) }}"
                                     data-src="{{ asset('public/filemanager/userfiles/' . $image_extra) }}"
                                     alt="{{ $product->name . $key }}"/>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="image-line"></div>
                <div class="slide-fullscreen">
                    <div class="fullscreen-box">
                        <div class="close-wrapper">
                            <span class="far"></span>
                        </div>
                        <div class="full-slider">
                            <div data-i="0"
                                 data-src="{{ asset('public/filemanager/userfiles/'. $product->image) }}">
                                <img data-src="{{ asset('public/filemanager/userfiles/'. $product->image) }}"
                                     id="getImage"
                                     src="{{ asset('public/filemanager/userfiles/'. $product->image) }}"
                                     alt="{{@$product->name}}"/></div>
                            @if(!empty($image_extras))
                                @foreach($image_extras as $key => $image_extra)
                                    <div data-i="{{$key+1}}"
                                         datrainghiem-imgta-src="{{ asset('public/filemanager/userfiles/' . $image_extra) }}">
                                        <img src="{{ asset('public/filemanager/userfiles/' . $image_extra) }}"
                                             data-src="{{ asset('public/filemanager/userfiles/' . $image_extra) }}"
                                             alt="{{ $product->name . $key }}"/>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="zoom">
                            <span class="far fa-search-plus zoomin"></span>
                            <span class="far fa-search-minus zoomout disabled"></span>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript" defer>
                const main_slide = $(".main-slide");
                const thumb_slide = $(".thumb-slide");
                const fullscreen_slide = $(".full-slider");
                var zoom_amount = 1,
                    zoom_drag_pos = {
                        x: 0,
                        y: 0,
                        top: 0,
                        left: 0,
                        off_x: 0,
                        off_y: 0,
                        is_press: false
                    },
                    zoom_element = null;
                main_slide.slick({
                    dots: false,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 1,
                    adaptiveHeight: true,
                    asNavFor: '.thumb-slide, .full-slider',
                    nextArrow: '<i class="fal fa-chevron-right"></i>',
                    prevArrow: '<i class="fal fa-chevron-left"></i>'
                });
                thumb_slide.slick({
                    dots: false,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 5,
                    slidesToScroll: 5,
                    asNavFor: '.main-slide, .full-slider',
                    nextArrow: '<i class="fal fa-chevron-right"></i>',
                    prevArrow: '<i class="fal fa-chevron-left"></i>',
                    focusOnSelect: true,
                    responsive: [
                        {
                            breakpoint: 899,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        }
                    ]
                });
                fullscreen_slide.slick({
                    dots: false,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 1,
                    adaptiveHeight: true,
                    nextArrow: '<i class="fal fa-chevron-right"></i>',
                    prevArrow: '<i class="fal fa-chevron-left"></i>'
                });
                fullscreen_slide.on('afterChange', function (event, slick, currentSlide) {
                    $(".full-slider .slick-slide img").css("transform","matrix(1,0,0,1,0,0)");
                    $(".zoomin").removeClass('disabled');
                    $(".zoomout").addClass('disabled');
                    zoom_amount = 1;
                    zoom_drag_pos = {
                        x: 0,
                        y: 0,
                        top: 0,
                        left: 0,
                        off_x: 0,
                        off_y: 0,
                        is_press: false
                    };
                    zoom_element = null;
                    $(".full-slider .slick-current img").css({top: 0, left: 0});
                    fullscreen_slide.slick("slickSetOption", "draggable", true);
                    fullscreen_slide.slick("slickSetOption", "touchMove", true);
                    fullscreen_slide.slick("slickSetOption", "swipe", true);
                });
                $(".main-slide .slick-slide").click(function () {
                    $(".slide-fullscreen").addClass('active');
                });
                $(".slide-fullscreen .close-wrapper span").click(function () {
                    $(".slide-fullscreen").removeClass('active');
                })
                $(".zoomin").click(function(ev) {
                    zoom_amount += 0.5;
                    $(".zoomout").removeClass('disabled');
                    if (zoom_amount > 3) {
                        zoom_amount = 3;
                        $(".zoomin").addClass('disabled');
                    }
                    if (!zoom_element) {
                        zoom_element = $(".full-slider .slick-current img");
                        if ("ontouchstart" in document.documentElement) {
                            fullscreen_slide.slick("slickSetOption", "touchMove", false)
                            fullscreen_slide.slick("slickSetOption", "swipe", false)
                            zoom_element.bind('touchstart', function (ev) {
                                zoom_drag_pos.is_press = true;
                                zoom_drag_pos.x = ev.originalEvent.changedTouches[0].clientX;
                                zoom_drag_pos.y = ev.originalEvent.changedTouches[0].clientY;
                            });
                            zoom_element.bind('touchend', function () {
                                zoom_drag_pos.is_press = false;
                                zoom_drag_pos.top = parseFloat(zoom_element.css('top'));
                                zoom_drag_pos.left = parseFloat(zoom_element.css('left'));
                            });
                            zoom_element.bind('touchmove', function (ev) {
                                ev.preventDefault();
                                if (zoom_element && zoom_drag_pos.is_press) {
                                    zoom_drag_pos.off_x = zoom_drag_pos.x - ev.originalEvent.changedTouches[0].clientX;
                                    zoom_drag_pos.off_y = zoom_drag_pos.y - ev.originalEvent.changedTouches[0].clientY;
                                    zoom_element.css({"top": (zoom_drag_pos.top - zoom_drag_pos.off_y)+ "px", "left": (zoom_drag_pos.left - zoom_drag_pos.off_x) + "px"});
                                }
                            });
                        } else {
                            fullscreen_slide.slick("slickSetOption", "draggable", false)
                            zoom_element.bind('mousedown', function (ev) {
                                zoom_drag_pos.is_press = true;
                                zoom_drag_pos.x = ev.clientX;
                                zoom_drag_pos.y = ev.clientY;
                            });
                            zoom_element.bind('mouseup', function () {
                                zoom_drag_pos.is_press = false;
                                zoom_drag_pos.top = parseFloat(zoom_element.css('top'));
                                zoom_drag_pos.left = parseFloat(zoom_element.css('left'));
                            });
                            zoom_element.bind('mousemove', function (ev) {
                                ev.preventDefault();
                                if (zoom_element && zoom_drag_pos.is_press) {
                                    zoom_drag_pos.off_x = zoom_drag_pos.x - ev.clientX;
                                    zoom_drag_pos.off_y = zoom_drag_pos.y - ev.clientY;
                                    zoom_element.css({"top": (zoom_drag_pos.top - zoom_drag_pos.off_y)+ "px", "left": (zoom_drag_pos.left - zoom_drag_pos.off_x) + "px"});
                                }
                            });
                        }
                    }
                    $(".full-slider .slick-current img").css('transform',`matrix(${zoom_amount},0,0,${zoom_amount},0,0)`);
                })
                $(".zoomout").click(function(ev) {
                    fullscreen_slide.slick("slickSetOption", "draggable", false)
                    zoom_amount -= 0.5;
                    $(".zoomin").removeClass('disabled');
                    if (zoom_amount < 1) {
                        zoom_amount = 1;
                        zoom_drag_pos = {
                            x: 0,
                            y: 0,
                            top: 0,
                            left: 0,
                            off_x: 0,
                            off_y: 0,
                            is_press: false
                        };
                        $(".zoomout").addClass('disabled');
                        zoom_element = null;
                        fullscreen_slide.slick("slickSetOption", "draggable", true);
                        fullscreen_slide.slick("slickSetOption", "touchMove", true);
                        fullscreen_slide.slick("slickSetOption", "swipe", true);
                        $(".full-slider .slick-current img")
                            .animate({
                                top: 0,
                                left: 0,
                                transform:`matrix(1,0,0,1,0,0)`,
                            }, 200, "linear");
                    } else {
                        $(".full-slider .slick-current img").css('transform',`matrix(${zoom_amount},0,0,${zoom_amount},0,0)`);
                    }
                })
            </script>

            <div class="service">
                @if(@$product->final_price != 0)
                    <p class="dbprice">
                        <span>{{number_format(@$product->final_price, 0, '.', '.')}}<sup>&nbsp;đ</sup></span></p>
                    </br>
                    {{--                    <span style="font-size: 20px; padding-bottom: 21px;" class="pr">Liên hệ</span>--}}
                @endif

                @if(@$product['final_price'] < @$product['base_price'])
                    <p><strike>{{number_format(@$product['base_price'], 0, '.', '.')}}</strike><sup
                                style="font-size: 18px">đ</sup></p>
                @endif


                @if(@$settings['custom_button_detail'] == 1)
                    @include('themeboschhome::childs.product.button_1')
                @else
                    @include('themeboschhome::childs.product.button_2')
                @endif

                <?php
                $suportOnline = CommonHelper::getFromCache('widget_location_sp_suportOnline');
                if (!$suportOnline) {
                    $suportOnline = \Modules\ThemeBoschHome\Models\Widget::where('location', 'sp_suportOnline')->first();
                    CommonHelper::putToCache('widget_location_sp_suportOnline', $suportOnline);
                }

                ?>
                @if($suportOnline->status==1)
                    <div class="suportOnline">
                        <label>Tổng đài tư vấn miễn phí</label>
                        <div class="suportOnline-content">
                            <i class="bgb"></i>
                            {!! $suportOnline->content !!}
                        </div>
                    </div>
                @endif


                {{--Khuyến mại dạng text--}}
                    @include('themeboschhome::childs.product.partials.khuyen_mai_text', ['sale_text' => \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductsSaleText($product->id, $category->id, $product->manufacture_id)])
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="product-content container-fluid">
        <div class="container">
            <div class="dbi" id="overview" style="padding-top:0;">
                {!! @$product->content !!}
            </div>
        </div>
    </div>
    <div class="product-content">
        <div class="tabdetail">
            <div class="container-fluid">
                <div class="container">
                    <span data-id="dacdiemnoibat">Đặc điểm nổi bật</span>
                    <span data-id="Pcontent">Đánh giá chi tiết</span>
                    <span data-id="thongso">Thông số kỹ thuật</span>
                </div>
            </div>
        </div>
        <div class="container">
            <div id="detail" class="f">
                <div class="f">
                    <div id="dacdiemnoibat" class="hide">
                        <span class="dtit">Đặc điểm nổi bật</span>

                        {!! @$product->highlight !!}
                    </div>
                    <div id="Pcontent" class="hide">
                        <span class="dtit">Đánh giá chi tiết</span>
                        <div id="ctongquan">
                            {!! @$product->review_detail !!}
                            <?php
                            $child_slug = CommonHelper::getFromCache('category_parent_id_khac_null');
                            if (!$child_slug) {
                                $child_slug = \Modules\ThemeBoschHome\Models\Category::where(function ($query) {
                                    $query->where('parent_id', '<>', null)->where('parent_id', '>', 0);
                                })->whereIn('id', explode('|', @$product->multi_cat))->first();
                                CommonHelper::putToCache('category_parent_id_khac_null', $child_slug);
                            }

                            ?>
                            <br/>
                            @if(!empty($child_slug))
                                Xem tất cả sản phẩm: <a href="/{{@$child_slug->slug}}"
                                                        title="{{@$child_slug->name}}">{{@$child_slug->name}}</a>
                            @else
                                Xem tất cả sản phẩm: <a href="/{{@$category->slug}}"
                                                        title="{{@$category->name}}">{{@$category->name}}</a>
                            @endif
                        </div>
                        <?php
                        $cate_multi = explode('|', trim($product->related_products, '|'));
                        $relate_products = CommonHelper::getFromCache('product_id_cate_multi' . implode("|", $cate_multi));
                        if (!$relate_products) {
                            $relate_products = \Modules\ThemeBoschHome\Models\Product::whereIn('id', $cate_multi)->where('status', 1)->get();
                            CommonHelper::putToCache('product_id_cate_multi' . implode("|", $cate_multi), $relate_products);
                        }


                        ?>
                        <style>
                            @media (max-width: 768px) {
                                .ss .pi {
                                    height: 110px !important;
                                }
                            }
                        </style>
                    </div>
                    <div id="thongso" class="hide">
                        <span class="dtit">Thông số kỹ thuật</span>
                        <div class="f thongso" id="cthongso">
                            <div class="thongso-item">
                                <h3>Mã sản phẩm</h3>
                                <div class="content">{{@$product->code}}</div>
                            </div>
                            <div class="thongso-item">
                                <h3>Nhãn hiệu</h3>
                                <?php
                                $getManu = CommonHelper::getFromCache('manufacture_id' . @$product->manufacture_id);
                                if (!$getManu) {
                                    $getManu = Modules\ThemeBoschHome\Models\Manufacturer::find(@$product->manufacture_id);
                                    CommonHelper::putToCache('manufacture_id' . @$product->manufacture_id, $getManu);
                                }
                                $thuong_hieu = CommonHelper::getFromCache('settings_name_thuong_hieu_slug_type_common_tab');
                                if (!$thuong_hieu) {
                                    $thuong_hieu = \Modules\ThemeBoschHome\Models\Settings::where('name', 'thuong_hieu_slug')->where('type', 'common_tab')->first();
                                    CommonHelper::putToCache('settings_name_thuong_hieu_slug_type_common_tab', $thuong_hieu);
                                }
                                ?>
                                <div class="content">
                                    @if(!empty($getManu)) <a style="color: blue"
                                                             href="/{{$thuong_hieu->value}}/{{ @$getManu->slug }}"
                                                             title="{{ $getManu->intro }}">{{@$getManu->name}}</a> @endif
                                </div>

                            </div>
                            <div class="thongso-item">
                                <h3>Mã quốc tế</h3>
                                <div class="content"> {{@$product->international_Code}}</div>
                            </div>
                            <div class="thongso-item">
                                <h3>Bảo hành</h3>
                                <?php
                                $product_guarantees = CommonHelper::getFromCache('product_guarantees' . $product->id);
                                if (!$product_guarantees) {
                                    $product_guarantees = $product->guarantees;
                                    CommonHelper::putToCache('product_guarantees' . $product->id, $product_guarantees);
                                }
                                ?>
                                <div class="content"> {{@$product_guarantees->name}}</div>
                            </div>
                            <div class="thongso-item">
                                <h3>Xuất xứ</h3>
                                <?php
                                $productOriginId = CommonHelper::getFromCache('product_origin_id' . $product->id);
                                if (!$productOriginId) {
                                    $productOriginId = $product->origin_id;
                                    CommonHelper::putToCache('product_origin_id' . $product->id, $productOriginId);
                                }
                                ?>
                                <div class="content"> {{@\Modules\ThemeBoschHome\Models\Origin::find(@$productOriginId)->name_origin}}</div>
                            </div>
                            <?php
                            $getProprites_val = CommonHelper::getFromCache('propertie_value_id' . @$product->proprerties_id);
                            if (!$getProprites_val) {
                                $property_arr = explode('|', @$product->proprerties_id);
                                $getProprites_val = Modules\ThemeBoschHome\Models\PropertieValue::whereIn('id', $property_arr);
                                if (!empty($property_arr)) {
                                    $orderRaw = 'CASE ';
                                    foreach ($property_arr as $k => $v) {
                                        if ($v != '') {
                                            $orderRaw .= " WHEN id = " . $v . " THEN " . $k;
                                        }
                                    }
                                    $orderRaw .= ' ELSE id END ASC';
                                    if ($orderRaw != 'CASE  ELSE id END ASC') {
                                        $getProprites_val = $getProprites_val->orderByRaw($orderRaw);
                                    }
                                }
                                $getProprites_val = $getProprites_val->where('status', 1)->get();;
                                CommonHelper::putToCache('propertie_value_id' . @$product->proprerties_id, $getProprites_val);
                            }
                            ?>
                            @foreach($getProprites_val as $getProprite_val)
                                <?php
                                $proProps_name = CommonHelper::getFromCache('propertie_name_find' . @$getProprite_val->properties_name_id);
                                if (!$proProps_name) {
                                    $proProps_name = \Modules\ThemeBoschHome\Models\PropertieName::find(@$getProprite_val->properties_name_id);
                                    CommonHelper::putToCache('propertie_name_find' . @$getProprite_val->properties_name_id, $proProps_name);
                                }
                                ?>

                                <div class="thongso-item">
                                    <h3>@if(isset($proProps_name)){{ @$proProps_name->name}}
                                        @endif</h3>
                                    <div class="content">
                                        @if(!empty($getProprite_val->link))
                                            <a style="color: blue"
                                               href="{{$getProprite_val->link}}"
                                               title="{!! @$getProprite_val->description !!}">{!! @$getProprite_val->value !!}</a>
                                        @else
                                            {!! @$getProprite_val->value !!}
                                        @endif

                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <script>
                            $(document).ready(function () {
                                $('.thongso-item .content').slideUp(); /*ẩn nội dung*/

                                $('.thongso-item h3').on('click', function (event) {
                                    event.preventDefault();
                                    $(this).next().stop().slideToggle();
                                    $(this).stop().toggleClass('on');
                                });
                            })
                        </script>
                    </div>
                    <div class="w50n">
                        {{--Sản phẩm liên quan--}}
                        {{--                        @if(@$settings['related_product'] == 1 && isset($relate_products) && count($relate_products) > 0)--}}
                        {{--                            <div>--}}

                        {{--                                <span class="dtit">Sản phẩm liên quan</span>--}}
                        {{--                                <div class="ss" id="SPTT2">--}}
                        {{--                                    <span class="psback"></span>--}}
                        {{--                                    <div class="pspanel">--}}
                        {{--                                        <div class="pswrap">--}}
                        {{--                                            @foreach( $relate_products as $relate_product)--}}
                        {{--                                                @php--}}
                        {{--                                                    $cate_slug =\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getFromCache('get_slug_cate_by_sptt'.@$relate_product->id);--}}
                        {{--                                                    if (!$cate_slug){--}}
                        {{--                                                        $cate_slug = \Modules\ThemeBoschHome\Models\Category::whereIn('id', explode('|', @$relate_product->multi_cat))->first();--}}
                        {{--                                                       \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::putToCache('get_slug_cate_by_sptt'.@$relate_product->id, @$cate_slug);--}}
                        {{--                                                    }--}}
                        {{--                                                @endphp--}}
                        {{--                                                <a class="psitem"--}}
                        {{--                                                   href="{{\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($relate_product) }}"--}}
                        {{--                                                   title="{{$relate_product->name}}">--}}

                        {{--                                                    <div class="pi">--}}
                        {{--                                                        <img alt="{{@$relate_product->name}}"--}}
                        {{--                                                             class="lazy"--}}
                        {{--                                                             data-src="{{CommonHelper::getUrlImageThumb(@$relate_product->image, 160, null) }}"--}}
                        {{--                                                             data-src="{{@$relate_product->image}}"/>--}}
                        {{--                                                    </div>--}}
                        {{--                                                    <p class="pn">{{@$relate_product->name}}</p>--}}
                        {{--                                                    <span class="pr">{{number_format(@$relate_product->final_price, 0, '.', '.')}}<sup--}}
                        {{--                                                                style="margin-left: 5px;">đ</sup></span>--}}
                        {{--                                                </a>--}}
                        {{--                                            @endforeach--}}

                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <span class="psnext"></span>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        @endif--}}
                        {{--Sản phẩm liên quan--}}
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('.tabdetail span').click(function () {
                            let tabid = $(this).data('id');
                            $(`#${tabid}`).removeClass('hide');
                            $('.tabdetail span').not(this).removeClass('on');
                            $(`#${tabid}`).siblings().addClass('hide');
                            $(this).addClass('on');
                        });
                        $('.tabdetail span').first().click();
                    })
                </script>
            </div>
        </div>
    </div>

    <div class="dbox" style="margin-bottom: 0;">
        <div class="container">
            <?php
            $cate_multi = explode('|', trim($product->multi_cat, '|'));

            $productInMultiCats = CommonHelper::getFromCache('products_multi_cat_like_cate_multi' . $cate_multi[0]);
            if (!$productInMultiCats) {
                $productInMultiCats = \Modules\ThemeBoschHome\Models\Product::where('multi_cat', 'like', '%|' . $cate_multi[0] . '|%')->where('status', 1)->get();
                CommonHelper::putToCache('products_multi_cat_like_cate_multi' . $cate_multi[0], $productInMultiCats);
            }
            ?>

            @if($productInMultiCats->count() > 0)
                <span class="dtit">Sản phẩm cùng danh mục</span>
                <div class="ss" id="SPTT1">
                    <span class="psback"></span>
                    <div class="pspanel">
                        <div class="pswrap">
                            @foreach( $productInMultiCats as $productInMultiCat)
                                <?php
                                $cate_slug = CommonHelper::getFromCache('get_slug_cate_by_sptt' . @$productInMultiCat->id);
                                if (!$cate_slug) {
                                    $cate_slug = \Modules\ThemeBoschHome\Models\Category::whereIn('id', explode('|', @$productInMultiCat->multi_cat))->first();
                                    CommonHelper::putToCache('get_slug_cate_by_sptt' . @$productInMultiCat->id, @$cate_slug);
                                }
                                ?>
                                <a class="psitem"
                                   href="{{\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($productInMultiCat) }}"
                                   title="{{$productInMultiCat->name}}">

                                    <div class="pi">
                                        <img alt="{{@$productInMultiCat->name}}"
                                             class="lazy"
                                             data-src="{{CommonHelper::getUrlImageThumb(@$productInMultiCat->image, 160, null) }}"
                                        />
                                    </div>
                                    <p class="pn">{{@$productInMultiCat->name}}</p>
                                    <span class="pr">{{number_format(@$productInMultiCat->final_price, 0, '.', '.')}}<sup
                                                style="margin-left: 5px;">đ</sup></span>
                                </a>
                            @endforeach

                        </div>
                    </div>
                    <span class="psnext"></span>
                </div>
            @endif
        </div>
    </div>

    {{--        SP đã xem--}}
    <div class="dbox" style="margin-top: 0;">
        <?php
        $cate_multi = explode('|', trim($product->multi_cat, '|'));
        $product_vieweds = CommonHelper::getFromCache('products_id_session' . implode('|', $_SESSION['product_viewed']));
        if (!$product_vieweds) {
            $product_vieweds = \Modules\ThemeBoschHome\Models\Product::whereIn('id', $_SESSION['product_viewed'])->where('status', 1)->get();
            CommonHelper::putToCache('products_id_session' . implode('|', $_SESSION['product_viewed']), $product_vieweds);
        }
        ?>
        @if($product_vieweds->count() > 0)

            <div class="container" id="SPTT3">
                <span class="dtit">Sản phẩm đã xem</span>
                <div class="container viewed-product" id="Product">
                    @foreach( $product_vieweds as $product_viewed)
                        <?php
                        $cate_slug = CommonHelper::getFromCache('get_slug_cate_by_sptt' . @$product_viewed->id);
                        if (!$cate_slug) {
                            $cate_slug = \Modules\ThemeBoschHome\Models\Category::whereIn('id', explode('|', @$product_viewed->multi_cat))->first();
                            CommonHelper::putToCache('get_slug_cate_by_sptt' . @$product_viewed->id, @$cate_slug);
                        }
                        ?>
                        <div class="gri"
                             href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                             title="{{@$product_viewed->meta_title}}">
                            <div class="gi">
                                @php
                                    if (@$product_viewed['final_price'] != 0 && @$product_viewed['base_price'] != 0){
                                    $discount = 100 - ((@$product_viewed['final_price'])/ @$product_viewed['base_price'] * 100);
                                    }
                                    else{
                                    $discount = 0;
                                    }
                                @endphp
                                @if($discount > 0)
                                    <div class="badge @if(round($discount) >= 20) red_sale @else orange_sale @endif">
                                        <div class="sale_text">- {{round($discount)}} %</div>
                                    </div>
                                @endif
                                <a class="image-product"
                                   href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($product_viewed, 'array') }}"
                                   title="{{@$product_viewed->name}}">
                                    <img class="lazy"
                                         data-src="{{\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getUrlImageThumb(@$product_viewed->image, 160, null) }}"
                                         alt="{{$product_viewed->name}}"/>
                                </a>
                                <div class="product-content-height">
                                    <div class="manuface_g">
                                        @if(!empty($manufacture))
                                            <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($product_viewed, 'array') }}"
                                               title="{{@$product_viewed->meta_title}}">
                                                <img class="lazy" height="50"
                                                     data-src="{{CommonHelper::getUrlImageThumb($manufacture->image, 150 ,null) }}"
                                                     alt="{{$manufacture->name}}"/>
                                            </a>
                                            @else
                                            </br></br></br>
                                        @endif
                                    </div>
                                    <h2>
                                        <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($product_viewed, 'array') }}"
                                           title="{{@$product_viewed->meta_title}}">{{$product_viewed->name}}</a></h2>
                                    <div class="sku">{{$product_viewed->code}}</div>
                                </div>
                                <div class="product-price">

                                    <div class="final-price">{{number_format($product_viewed['final_price'], 0, '.', '.')}}
                                        &nbsp;<sup>đ</sup></div>

                                    @if(@$settings['show_detail'] == 1 && @$settings['show_add_cart'] == 0)
                                        <div class="action-container">
                                            <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($product_viewed, 'array') }}"
                                               title="{{@$product_viewed->meta_title}}"><span class="gri-view-pro"
                                                                                              style="width: 100%!important;
                                        ">Xem Sản Phẩm</span></a>
                                        </div>
                                    @elseif(@$settings['show_detail'] == 0 && @$settings['show_add_cart'] == 1)
                                        <div class="action-container">
                        <span data-text="{{route('order.view')}}" style="cursor: pointer;width: 100%!important;"
                              onclick="(addCart1({{@$product_viewed->id}}))"
                              class="gri-add-cart urlCart"><i class="fal fa-shopping-cart"></i> Cho vào giỏ</span>
                                        </div>

                                    @elseif(@$settings['show_detail'] == 1 && @$settings['show_add_cart'] == 1)
                                        <div class="action-container">
                                            <div class="added-cart">
                                                <i class="fal fa-times"></i>
                                                <h2>Item added to cart</h2>
                                                <div class="info-item">
                                                    <div class="img-item">
                                                        <img class="lazy"
                                                             data-src="{{\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getUrlImageThumb(@$product_viewed->image, 160, null) }}"
                                                             alt="{{$product_viewed->name}}"/>
                                                    </div>
                                                    <div class="content-item">
                                                        <div class="sku">{{$product_viewed->code}}</div>
                                                        <div class="final-price">{{number_format($product_viewed->final_price, 0, '.', '.')}}
                                                            <sup>đ</sup></div>
                                                        <div class="quantity">Quantity: <span id="number">1</span></div>

                                                    </div>
                                                </div>
                                                <a class="shopping-cart-btn" href="/gio-hang">Xem giỏ hàng</a>
                                            </div>
                                            <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($product_viewed, 'array') }}"
                                               title="{{@$product_viewed->meta_title}}"><span class="gri-view-pro">Xem Sản Phẩm</span></a>
                                            <span data-text="{{route('order.view')}}" style="cursor: pointer"
                                                  data-id="{{@$product_viewed->id}}"
                                                  class="gri-add-cart urlCart"><i class="fal fa-shopping-cart"></i> Cho vào giỏ
                                    </span>
                                        </div>
                                    @elseif(@$settings['show_detail'] == 0 && @$settings['show_add_cart'] == 0)
                                        <div style="display: none;"></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
    <script>
        var option = $('.viewed-product');
        var windowsize = $(window).width();
        if (windowsize < 768) {
            option.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                loop: true
            })
        } else {
            option.slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                dots: false,
                arrows: true,
                loop: true
            })
        }
        $(".image-slide .main-slide .slick-slide").click(function (ev) {
             $("")
        });
    </script>
@endsection
