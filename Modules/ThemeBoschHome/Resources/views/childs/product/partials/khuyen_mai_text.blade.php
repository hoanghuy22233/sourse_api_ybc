@if (!empty($sale_text))
    <style>
        ul.p-promotion {
            border: 1px solid #e1e1e1;
            margin-bottom: 15px;
        }

        ul.p-promotion li {
            padding: 0 10px;
            margin-bottom: 5px;
        }

        ul.p-promotion .promotion-title {
            font-weight: bold;
            color: #fff;
            font-size: 14px;
            background-color: #00639a;
            display: block;
            line-height: 37px;
            padding: 0 10px;
            margin: 0 -10px;
        }
        ul.p-promotion li li {
            position: relative;
            padding: 0 25px !important;
        }
        ul.p-promotion li li:before {
            content: "f";
            content: "\f00c";
            font-weight: 900;
            font-family: 'Font Awesome 5 Free';
            position: absolute;
            left: 10px;
            top: 2px;
            color: #333;
            font-size: 12px;
        }
    </style>
    <ul class="p-promotion">
        <li>
            <span class="promotion-title">Khuyến mại từ {{ @$settings['name'] }}</span>
        </li>
        @foreach($sale_text as $v)
            <li>
                {!! $v !!}
            </li>
        @endforeach
    </ul>
@endif