<style>
    .popup-add-cart {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;

    }
    .popup-add-cart-content {
        width: 320px;
        margin: 0 auto;
        border: 1px solid #ccc;
        background: #fff;
    }
    .popup-add-cart p {
        text-align: center;
        margin: 15px 0;
    }
    a.btn-cart {
        background: #63cc50;
        text-transform: capitalize;
    }
    button.close-pop {
        background: #5467f1;
        border: none;
        text-transform: capitalize;
    }
    .btn-check {
        text-align: center;
        padding: 10px;
    }
    a.btn-cart:hover, button.close-pop:hover {
        color: #000;
    }
    .addtocart_3CEN {
        background-color: #00639a;
        margin-bottom: 10px;
    }
    .addtocart_3CEN:active, .addtocart_3CEN:focus, .addtocart_3CEN:hover {
        background-color: #07507f !important;
    }
</style>
<div class="da">
    <div class="added-cart">
        <i class="fal fa-times"></i>
        <h2>Item added to cart</h2>
        <div class="info-item">
            <div class="img-item">
                <img class="lazy"
                     data-src="{{CommonHelper::getUrlImageThumb($product['image'], 250, null) }}"
                     alt="{{$product['name']}}"/>
            </div>
            <div class="content-item">
                <div class="sku">{{$product['code']}}</div>
                <div class="final-price">{{number_format($product['final_price'], 0, '.', '.')}}<sup>đ</sup></div>
                <div class="quantity">Quantity: <span id="number">1</span></div>
            </div>
        </div>
        <a class="shopping-cart-btn" href="/gio-hang">Xem giỏ hàng</a>
    </div>
    <p data-text="{{route('order.view')}}"
       data-id="{{@$product->id}})" style="color:white" class="gri-add-cart urlCart btn3 addtocart_3CEN"><i class="fal fa-shopping-cart"></i>&nbsp;&nbsp;Thêm vào giỏ
        hàng
    </p>
    <div class="quantity">
        <input name="quantity" value="1" type="number"><i class="fal fa-chevron-down disabled"></i><i class="fal fa-chevron-up"></i>
    </div>
</div>

<div class="popup-add-cart" style="display: none">
    <div class="popup-add-cart-content">
        <p>Thêm thành công vào giỏ hàng!</p>
        <div class="btn-check">
            <a class="btn-cart btn" href="/gio-hang">Xem giỏ hàng</a>
            <button class="close-pop btn">Tiếp tục mua hàng</button>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.close-pop').click(function () {
            $('.popup-add-cart').hide();
        })
        $('.quantity .fa-chevron-up').click(function (ev) {
            let num = parseInt($('.quantity input').val());
            num++;
            $('.quantity input').val(num);
            $('.quantity .fa-chevron-down').removeClass('disabled');
        })

        $('.quantity .fa-chevron-down').click(function (ev) {
            let num = parseInt($('.quantity input').val());
            if (num > 1) {
                num--;
                $('.quantity input').val(num);
                if (num === 1) {
                    $(this).addClass('disabled');
                }
            }
        })

        $('.quantity input').blur(function (ev) {
            let num = parseInt($(this).val());
            if (num > 1) {
                $('.quantity .fa-chevron-down').removeClass('disabled');
            } else {
                $('.quantity .fa-chevron-down').addClass('disabled');
            }
        })

        $('.quantity input').blur();
    })
</script>
