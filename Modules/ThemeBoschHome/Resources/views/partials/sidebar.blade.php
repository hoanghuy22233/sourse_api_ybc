<aside class="sidebar left left-content col-md-3 col-sm-4 col-xs-12">
    <div class="aside-filter">
        <h2 class="fiter-title">Lọc sản phẩm</h2>
        <div class="aside-hidden-mobile">
            <div class="filter-container">
                <div class="filter-container__selected-filter" style="display: none;">
                    <div class="filter-container__selected-filter-header clearfix">
                                    <span class="filter-container__selected-filter-header-title"><i
                                                class="fa fa-arrow-left hidden-sm-up"></i> Bạn chọn</span>
                        <a href="javascript:void(0)" onclick="clearAllFiltered()"
                           class="filter-container__clear-all">Bỏ hết <i class="fa fa-angle-right"></i></a>
                    </div>
                    <div class="filter-container__selected-filter-list">
                        <ul></ul>
                    </div>
                </div>
            </div>


            <?php
            $data = CommonHelper::getFromCache('get_manufacturer_by_updated_at');
            if (!$data) {
                $data = \Modules\ThemeBoschHome\Models\Manufacturer::orderBy('updated_at', 'desc')->limit(6)->get();

                CommonHelper::putToCache('get_manufacturer_by_updated_at', $data);
            }
            ?>

            <aside class="aside-item filter-vendor">
                <div class="aside-title">
                    <h2 class="title-head margin-top-0"><span>Thương hiệu</span></h2>
                </div>
                <div class="aside-content filter-group">
                    @php $manufactureres = isset($_GET['manufactureres']) ? $_GET['manufactureres'] : []; @endphp
                    @foreach($data as $item)
                        <ul>
                            <li class="filter-item filter-item--check-box filter-item--green">
						<span>
							<label for="{{$item->id}}">
								<input type="checkbox" id="{{$item->id}}" value="{{$item->id}}" class="serach-mamufacturer" @if(in_array($item->id, $manufactureres)) checked @endif>
								<i class="fa"></i>
                                {{$item->name}}
							</label>
						</span>
                            </li>
                        </ul>
                    @endforeach
                </div>
            </aside>
            <link href="{{ URL::asset('public/frontend/themes/stbd/css/jquery-ui.min.css') }}" rel='stylesheet'
                  type='text/css'/>
            <script src="{{ URL::asset('public/frontend/themes/stbd/js/jquery-ui.min.js') }}"
                    type='text/javascript'></script>
            <script src="{{ URL::asset('public/frontend/themes/stbd/js/jquery.ui.touch-punch.min.js') }}"
                    type='text/javascript'></script>
            <link href="{{ URL::asset('public/frontend/themes/stbd/css/jquery-ui.min.css') }}" rel="stylesheet" type="text/css">
            <aside class="aside-item filter-price">
                <div class="aside-title">
                    <h2 class="title-head margin-top-0"><span>Giá sản phẩm</span></h2>
                </div>
                <div class="aside-content filter-group">
                    <div id="slider" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                        <div class="ui-slider-range ui-corner-all ui-widget-header"
                             style="left: 22.5368%; width: 77.4632%;"></div>
                        <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"
                              style="left: 0%;"></span><span tabindex="0"
                                                             class="ui-slider-handle ui-corner-all ui-state-default"
                                                             style="left: 53.7596%;"></span>
                        <div class="ui-slider-range ui-corner-all ui-widget-header"
                             style="left: 0%; width: 53.7596%;"></div>
                    </div>
                    <div id="start"><input value="{{ isset($_GET['min_price']) ? $_GET['min_price'] : 0 }}"></div>
                    <div id="stop"><input value="{{ isset($_GET['max_price']) ? $_GET['max_price'] : 100000000 }}"></div>
                    <div class="clearfix"></div>
                    <a id="old-value" href="javascript:;"></a>
                    <a id="filter-value" class="btn btn-gray" href="javascript:;" onclick="_toggleFilterdqdt(this);"
                       data-value="(>-1 AND <5375957)">Lọc giá</a>

                    <script>
                        var maxx = 10000000 / 10;
                        $('#slider').slider({
                            min: '0',
                            max: maxx,
                            range: true,
                            values: [0, 10000000],
                            slide: function (event, ui) {
                                if (ui.values[0] >= ui.values[1]) {
                                    if (ui.handle == $("#slider a")[0]) {
                                        $("#slider").slider("values", 1, ui.value);
                                        ui.values[0] = ui.value;
                                        ui.values[1] = ui.value;
                                    } else {
                                        $("#slider").slider("values", 0, ui.value);
                                        ui.values[0] = ui.value;
                                        ui.values[1] = ui.value;
                                    }
                                }
                                var uimax = ui.values[1] + 1;
                                $('#start input').val(ui.values[0]);
                                $('#stop input').val(ui.values[1] + 1);
                                var uimin = ui.values[0] - 1;
                                var uimax = ui.values[1] + 2;
                                $('#filter-value').attr('data-value', '(>' + uimin + ' AND <' + uimax + ')');
                            }
                        });
                        $(document).on('change', '#start', function (e) {
                            var val = parseInt($('#start input').val()) - 1;
                            var val2 = parseInt($('#stop input').val()) + 1;

                            $("#slider").slider("values", 0, parseInt(val));
                            $('#filter-value').attr('data-value', '(>' + val + ' AND <' + val2 + ')');
                        });
                        $(document).on('change', '#stop', function (e) {
                            var val = parseInt($('#start input').val()) - 1;
                            var val2 = parseInt($('#stop input').val()) + 1;

                            $("#slider").slider("values", 1, parseInt(val2));
                            $('#filter-value').attr('data-value', '(>' + val + ' AND <' + val2 + ')');
                        });

                    </script>
                </div>
            </aside>

        </div>
    </div>

    @if(isset($category) && $category->banner_sidebar != '' && file_exists(base_path() . '/public/filemanager/userfiles/' . $category->banner_sidebar))
    <aside class="aside-item banner hidden-xs">
        <div class="aside-content">
            <div class="image-effect">
                <a href="#">
                    <picture>
                        <img data-src="{{ CommonHelper::getUrlImageThumb($category->banner_sidebar, 263, 197) }}"
                             class="lazy img-responsive center-block" alt="{{$category->name}}"/>
                    </picture>
                </a>
            </div>
        </div>
    </aside>
    @endif

    <aside class="aside-item collection-category hidden-xs">
        <div class="aside-title">
            <h2 class="title-head margin-top-0"><span>Danh mục</span></h2>
        </div>
        <div class="aside-content">
            <nav class="nav-category navbar-toggleable-md">
                <ul class="nav navbar-pills">
                    @if(isset($category))
                    <?php
                    $cat_childs = CommonHelper::getFromCache('get_category_child_by_parent_id'. $category->id);
                    if (!$cat_childs) {
                        $cat_childs = \Modules\ThemeBoschHome\Models\Category::select(['name','slug'])->where('parent_id',$category->id)->get();
                        CommonHelper::putToCache('get_category_child_by_parent_id'. $category->id, $cat_childs);
                    }

                    ?>
                    @foreach($cat_childs as $cat_child)
                        <li class="nav-item">
                        <a class="nav-link"
                        href="{{ URL::to('danh-muc/' .  $cat_child->slug) }}">{{ $cat_child->name }}</a>
                        </li>
                        @endforeach
                    @endif
                </ul>
            </nav>
        </div>
    </aside>

</aside>