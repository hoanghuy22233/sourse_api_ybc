<?php
$category = CommonHelper::getFromCache('get_category_by_experi');
if (!$category) {
    $category = Modules\ThemeBoschHome\Models\Category::where('status', 1)->where('type', 1)->where('show_homepage', 1)->orderBy('order_no', 'asc')->get();
    CommonHelper::putToCache('get_category_by_experi', $category);
}
$getIdCate = CommonHelper::getFromCache('get_all_index');
if (!$getIdCate) {
    $getIdCate = \Modules\ThemeBoschHome\Models\Post::all()->pluck('multi_cat', 'id');
    CommonHelper::putToCache('get_all_index', $getIdCate);
}
$data_id = '|';

?>
<style>


    .bnews .bsum {
        height: 75px;
        overflow-y: hidden;
    }

    .bnews .more {
        display: block;
        padding: 10px 0;
    }

    .bnews a h4 {
        font: 18px/24px arial;
        margin: 20px 0 10px;
        color: #666;
        display: -webkit-box;
        max-width: 100%;
        height: 48px;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .bnews li{
        padding-bottom: 0;
    }
    @media (max-width: 767px){
        .bnews .b ul li {
            width: 45%;
        }
        }
</style>
<div class="f bnews">
        @if(!empty($category) && count($category) > 0)
        <div class="b newest">
            @foreach($category as $k1=>$cat)
                <?php
                    $data = CommonHelper::getFromCache('get_posts_by_experi');
                    if (!$data) {
                        $data = \Modules\ThemeBoschHome\Models\Post::where('status', 1)->where('multi_cat', 'like','%|'.$cat->id.'|%')->limit(4)->orderBy('order_no', 'decs')->orderBy('id', 'decs')->get();
                        CommonHelper::putToCache('get_posts_by_experi', $data);
                    }
//                }
                ?>
                    @if(!empty($data->toArray()))
                        <div class="f flexnew">
                            <img class="lazy" data-src="{{CommonHelper::getUrlImageThumb($cat->image, 675, null) }}" alt="">
                            <h4><a href="{{route('cate.list', ['slug' => $cat->slug])}}">{{ucfirst(mb_strtoupper($cat->name))}}</a></h4>
                            {!!$cat->content!!}
                            <a href="{{route('cate.list', ['slug' => $cat->slug])}}" class="a-link a-link-local  a-link-primary "><span class="text">See more</span></a>
                        </div>
                    @endif
            @endforeach
        </div>
        @endif
</div>

