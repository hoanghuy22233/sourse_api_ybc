<?php
     $slug = urldecode(last(request()->segments()));
     $category = CommonHelper::getFromCache('get_category_by_post'.$slug);
    if (!$category){
        $category = Modules\ThemeBoschHome\Models\Category::where('slug', $slug)->where('status', 1)->first();
       CommonHelper::putToCache('get_category_by_post'.$slug, $category);
    }
     $newPosts = CommonHelper::getFromCache('get_newpost_by_post');
    if (!$newPosts){
        $newPosts = Modules\ThemeBoschHome\Models\Post::where('status', 1)->where('slug', '<>', '#')
                                     ->orderBy('id', 'desc')->where('type_page', '<>', 'page_static')->limit(5)->get();
        CommonHelper::putToCache('get_newpost_by_post', $newPosts);
    }
     $postViews = CommonHelper::getFromCache('get_postviews_by_post');
    if (!$postViews){
        $postViews = Modules\ThemeBoschHome\Models\Post::where('status', 1)->where('slug', '<>', '#')
                                     ->orderBy('view_total', 'desc')->where('type_page', '<>', 'page_static')->limit(5)->get();
        CommonHelper::putToCache('get_postviews_by_post', $postViews);
    }
?>
{{--<div class="newsright bgghi">--}}
    {{--<ul class="news-right2">--}}
        {{--@if(!empty($newPosts))--}}
            {{--@foreach($newPosts as $newPost)--}}
                {{--<li>--}}
                    {{--<a class="img-news" href="{{route('cate.list', ['slug' => $newPost->slug])}}.html" title="{{$newPost->name}}">--}}
                        {{--<img src="{{CommonHelper::getUrlImageThumb($newPost->image, 300, null) }}" alt="{{$newPost->name}}"></a>--}}
                    {{--<h3><a href="{{route('cate.list', ['slug' => $newPost->slug])}}.html" title="{{$newPost->name}}">{{$newPost->name}}</a></h3>--}}
                {{--</li>--}}
            {{--@endforeach--}}
        {{--@endif--}}
    {{--</ul>--}}
{{--</div>--}}
{{--<!--chi danh cho tin thuoc trang tin hay-->--}}
{{--<div class="newsright bgghi">--}}
    {{--<ul class="news-right2">--}}
        {{--@if(!empty($newPosts))--}}
            {{--@foreach($newPosts as $newPost)--}}
                {{--<li>--}}
                    {{--<a class="img-news" href="{{route('cate.list', ['slug' => $newPost->slug])}}.html" title="{{$newPost->name}}">--}}
{{--                        <img src="{{CommonHelper::getUrlImageThumb($newPost->image, 300, null) }}" alt="{{$newPost->name}}"></a>--}}
                    {{--<h3><a href="{{route('cate.list', ['slug' => $newPost->slug])}}.html" title="{{$newPost->name}}">{{$newPost->name}}</a></h3>--}}
                {{--</li>--}}
            {{--@endforeach--}}
        {{--@endif--}}
    {{--</ul>--}}
{{--</div>--}}
<!--chi danh cho tin thuoc trang tin hay-->
<div class="newsright">
    <label style="background: #fff577">Tư vấn xem nhiều nhất</label>
    <ul class="news-list" style="border: 1px solid #fff576">
                <?php
                $cate_id = explode('|',trim(@$post->multi_cat,'|'));

        $possts = CommonHelper::getFromCache('category_id_cate_id_first'. @$cate_id[0]);
        if (!$possts){
            $possts = \Modules\ThemeBoschHome\Models\Category::where('id',$cate_id[0])->where('status',1)->first();
            CommonHelper::putToCache('category_id_cate_id_first'. @$cate_id[0], $possts);
        }

        $xemnhieu = CommonHelper::getFromCache('category_multi_cat_like_cate_id_get');
        if (!$xemnhieu){
            $xemnhieu = \Modules\ThemeBoschHome\Models\Post::where('multi_cat','like','%|'.$cate_id[0].'|%')->where('status',1)->orderBy('view_total', 'desc')->limit(5)->get();
            CommonHelper::putToCache('category_multi_cat_like_cate_id_get', $xemnhieu);
        }
                ?>
        @if(!empty($xemnhieu))
        @foreach($xemnhieu as $key => $postView)
                    <li style="    padding: 5px;    list-style: inside;">
                        <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($postView) }}" title="Nên mua bếp từ hay bếp hồng ngoại">{{$postView->name}}</a>
                    </li>
            @endforeach
        @endif
    </ul>

</div>
<div id="endright" style="float: left; width: 100%;"></div>

<script>
   $(document).ready(function () {
       $('#menu_catalog ul a').each(function () {
           if ($(this).hasClass('a2act')){
               $(this).parents('ul').css('display', 'block');
               $(this).parents('ul').prev('a').addClass('a2act');
           }
       })
   })
</script>