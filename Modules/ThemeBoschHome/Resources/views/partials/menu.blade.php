
<div class="nav-header hidden-xs hidden-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-sm-10 col-xs-12">
                <nav>
                    @php
                        $data = \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getMenusByLocation('main_menu');
                    @endphp

                    <ul id="nav" class="nav">
                        @foreach($data as $lv1)
                            @php
                                $lv2 = $lv1['childs'];
                            @endphp
                            <li class="nav-item  {{ count($lv2) > 0 ? 'has-mega' : '' }}">
                                @if($lv1['name'] == "Trang chủ")
                                    <a href="{{ URL::to($lv1['link']) }}" class="nav-link">{{ $lv1['name'] }}</a>
                                @else
                                <a href="{{ URL::to($lv1['link']) }}"
                                   class="nav-link">{{ $lv1['name'] }}{!! count($lv2) > 0 ? '<i class="fa fa-angle-right" data-toggle="dropdown"></i>' : '' !!}</a>

                                @if(count($lv2) > 0)
                                    <div class="mega-content">
                                        <div class="level0-wrapper2">
                                            <div class="nav-block nav-block-center">
                                                <ul class="level0">
                                                    @foreach($lv2 as $lv2_item)
                                                        @php
                                                            $lv3 = $lv2_item['childs'];
                                                        @endphp

                                                        <li class="level1 parent item"><h2 class="h4"><a
                                                                        href="{{ URL::to($lv2_item['link']) }}"><span>{{ $lv2_item['name'] }}</span></a>
                                                            </h2>
                                                            <ul class="level1">
                                                                @foreach($lv3 as $lv3_item)
                                                                    <li class="level2"><a
                                                                                href="{{ URL::to($lv3_item['link']) }}"><span>{{ $lv3_item['name'] }}</span></a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                    @endif
                            </li>
                        @endforeach
                    </ul>
                </nav>
            </div>
            <div class="col-md-2 col-sm-2 hidden-xs">
                <div class="header-cart-desktop clearfix">
                    <a href="{{ route('order.view') }}" title="Giỏ hàng">
                        <img class="lazy" data-src="{{ URL::asset('public/frontend/themes/stbd/images/shopping-cart.svg') }}"
                             alt="Giỏ hàng"/>
                        <div class="cart-right">
                            <span class="count_item_pr">@if(Session::has('cart'))
                                    @php
                                        $count_cart = 0;
                                    foreach (Session::get('cart')['items'] as $v) {
                                        $count_cart += $v['quantity'];
                                    }
                                    @endphp
                                    {{ $count_cart }}
                                @else
                                    {{ 0 }}
                                @endif</span>
                            <span class="cart-text">Giỏ hàng</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="mobile-nav visible-xs visible-sm">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="navbar-toggle collapsed visible-sm visible-xs" id="trigger-mobile">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="header_search search_form">
                    <?php
                    $action = Route::currentRouteAction();
                    ?>
                    @if($action == 'App\Http\Controllers\Frontend\CategoryPostController@getIndex' || $action == 'App\Http\Controllers\Frontend\PostController@getIndex' || $action == 'App\Http\Controllers\Frontend\PostController@getSearchPost')
                        <form class="input-group search-bar search_form"
                              action="{{ route('search_post') }}" method="GET"
                              role="search">

                            <input type="search" name="keyword_post" value="{{ isset($_GET['keyword']) ? $_GET['keyword'] : '' }}" placeholder="Tìm kiếm bài viết... "
                                   class="input-group-field st-default-search-input search-text"
                                   autocomplete="off">
                            <span class="input-group-btn">
                                                <button class="btn icon-fallback-text">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </span>
                        </form>
                    @else
                            <form class="input-group search-bar search_form"
                                  action="{{ route('search') }}" method="get" role="search">
                                <input type="search" name="keyword" value="{{ isset($_GET['keyword']) ? $_GET['keyword'] : '' }}" placeholder="Tìm kiếm sản phẩm... "
                                       class="input-group-field st-default-search-input search-text" autocomplete="off">
                                                        <span class="input-group-btn">
                                    <button class="btn icon-fallback-text">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </form>mo

                    @endif


                    <div id='search_suggestion'>
                        <div id='search_top'>
                            <div id="product_results"></div>
                            <div id="article_results"></div>
                        </div>
                        <div id='search_bottom'>
                            <a class='show_more' href='#'>Hiển thị tất cả kết quả cho "<span></span>"</a>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function ($) {
                            var settings = {
                                searchArticle: "0",
                                articleLimit: 5,
                                productLimit: 5,
                                showDescription: "0"
                            };
                            var suggestionWrap = document.getElementById('search_suggestion');
                            var searchTop = document.getElementById('search_top');
                            var productResults = document.getElementById('product_results');
                            var articleResults = document.getElementById('article_results');
                            var searchBottom = document.getElementById('search_bottom');
                            var isArray = function (a) {
                                return Object.prototype.toString.call(a) === "[object Array]";
                            }
                            var createEle = function (desc) {
                                if (!isArray(desc)) {
                                    return createEle.call(this, Array.prototype.slice.call(arguments));
                                }
                                var tag = desc[0];
                                var attributes = desc[1];
                                var el = document.createElement(tag);
                                var start = 1;
                                if (typeof attributes === "object" && attributes !== null && !isArray(attributes)) {
                                    for (var attr in attributes) {
                                        el[attr] = attributes[attr];
                                    }
                                    start = 2;
                                }
                                for (var i = start; i < desc.length; i++) {
                                    if (isArray(desc[i])) {
                                        el.appendChild(createEle(desc[i]));
                                    }
                                    else {
                                        el.appendChild(document.createTextNode(desc[i]));
                                    }
                                }
                                return el;
                            }
                            var loadResult = function (data, type) {
                                if (type === 'product') {
                                    productResults.innerHTML = '';
                                }
                                if (type === 'article') {
                                    articleResults.innerHTML = '';
                                }
                                var articleLimit = parseInt(settings.articleLimit);
                                var productLimit = parseInt(settings.productLimit);
                                var showDescription = settings.showDescription;
                                if (data.indexOf('<iframe') > -1) {
                                    data = data.substr(0, (data.indexOf('<iframe') - 1))
                                }
                                var dataJson = JSON.parse(data);
                                if (dataJson.results !== undefined) {
                                    var resultList = [];
                                    searchTop.style.display = 'block';
                                    if (type === 'product') {
                                        productResults.innerHTML = ''
                                        productLimit = Math.min(dataJson.results.length, productLimit);
                                        for (var i = 0; i < productLimit; i++) {
                                            resultList[i] = dataJson.results[i];
                                        }
                                    }
                                    else {
                                        articleResults.innerHTML = '';
                                        articleLimit = Math.min(dataJson.results.length, articleLimit);
                                        for (var i = 0; i < articleLimit; i++) {
                                            resultList[i] = dataJson.results[i];
                                        }
                                    }
                                    var searchTitle = 'Sản phẩm gợi ý'
                                    if (type === 'article') {
                                        searchTitle = 'Bài viết';
                                    }
                                    var searchHeading = createEle(['h3', searchTitle]);
                                    var searchList = document.createElement('ul');
                                    for (var index = 0; index < resultList.length; index++) {
                                        var item = resultList[index];
                                        var priceDiv = '';
                                        var descriptionDiv = '';
                                        if (type == 'product') {
                                            if (item.price_contact) {
                                                priceDiv = ['div', {className: 'item_price'},
                                                    ['ins', item.price_contact]
                                                ];
                                            }
                                            else {
                                                if (item.price_from) {
                                                    priceDiv = ['div', {className: 'item_price'},
                                                        ['span', 'Từ '],
                                                        ['ins', item.price_from]
                                                    ];
                                                }
                                                else {
                                                    priceDiv = ['div', {className: 'item_price'},
                                                        ['ins', parseFloat(item.price) ? item.price : 'Liên hệ']
                                                    ];
                                                }
                                            }
                                            if (item.compare_at_price !== undefined) {
                                                priceDiv.push(['del', item.compare_at_price]);
                                            }
                                        }
                                        if (showDescription == '1') {
                                            descriptionDiv = ['div', {className: 'item_description'}, item.description]
                                        }
                                        var searchItem = createEle(
                                            ['li',
                                                ['a', {href: item.url, title: item.title},
                                                    ['div', {className: 'item_image'},
                                                        ['img', {src: item.thumbnail, alt: item.title}]
                                                    ],
                                                    ['div', {className: 'item_detail'},
                                                        ['div', {className: 'item_title'},
                                                            ['h4', item.title]
                                                        ],
                                                        priceDiv, descriptionDiv
                                                    ]
                                                ]
                                            ]
                                        )
                                        searchList.appendChild(searchItem);
                                    }
                                    if (type === 'product') {
                                        productResults.innerHTML = '';
                                        productResults.appendChild(searchHeading);
                                        productResults.appendChild(searchList);
                                    }
                                    else {
                                        articleResults.innerHTML = '';
                                        articleResults.appendChild(searchHeading);
                                        articleResults.appendChild(searchList);
                                    }
                                }
                                else {
                                    if (type !== 'product' && false) {
                                        searchTop.style.display = 'none'
                                    }
                                }
                            }
                            var loadAjax = function (q) {
                                if (settings.searchArticle === '1') {
                                    loadArticle(q);
                                }
                                loadProduct(q);
                            }
                            var loadProduct = function (q) {
                                var xhttp = new XMLHttpRequest();
                                xhttp.onreadystatechange = function () {
                                    if (this.readyState == 4 && this.status == 200) {
                                        loadResult(this.responseText, 'product')
                                    }
                                }
                                xhttp.open('/GET', '/search?type=product&q=' + q + '&view=json', true);
                                xhttp.send();
                            }
                            var loadArticle = function (q) {
                                var xhttp = new XMLHttpRequest();
                                xhttp.onreadystatechange = function () {
                                    if (this.readyState == 4 && this.status == 200) {
                                        loadResult(this.responseText, 'article')
                                    }
                                }
                                xhttp.open('/GET', '/search?type=article&q=' + q + '&view=json', true);
                                xhttp.send();
                            }
                            var searchForm = document.querySelectorAll('form[action="/search"]');
                            var getPos = function (el) {
                                for (var lx = 0, ly = 0; el != null; lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent) ;
                                return {x: lx, y: ly};
                            }
                            var initSuggestion = function (el) {

                                var parentTop = el.offsetParent.offsetTop;
                                var position = getPos(el);
                                var searchInputHeight = el.offsetHeight;
                                var searchInputWidth = el.offsetWidth;
                                var searchInputX = position.x;
                                var searchInputY = position.y;
                                var suggestionPositionX = searchInputX;
                                var suggestionPositionY = searchInputY + searchInputHeight;
                                suggestionWrap.style.left = '0px';
                                suggestionWrap.style.top = 40 + 'px';
                                suggestionWrap.style.width = searchInputWidth + 'px';
                            }
                            window.__q__ = '';
                            var loadAjax2 = function (q) {
                                if (settings.searchArticle === '1') {
                                }
                                window.__q__ = q;
                                return $.ajax({
                                    url: '/search?type=product&q=' + q + '&view=json',
                                    type: 'GET'
                                }).promise();
                            };
                            if (searchForm.length > 0) {
                                for (var i = 0; i < searchForm.length; i++) {
                                    var form = searchForm[i];
                                    var searchInput = form.querySelector('[name=query]');
                                    var keyup = Rx.Observable.fromEvent(searchInput, 'keyup')
                                        .map(function (e) {
                                            var __q = e.target.value;
                                            initSuggestion(e.target);
                                            if (__q === '' || __q === null) {
                                                suggestionWrap.style.display = 'none';
                                            }
                                            else {
                                                suggestionWrap.style.display = 'block';
                                                var showMore = searchBottom.getElementsByClassName('show_more')[0];
                                                showMore.setAttribute('href', '/search?q=' + __q);
                                                showMore.querySelector('span').innerHTML = __q;
                                            }
                                            return e.target.value;
                                        })
                                        .filter(function (text) {
                                            return text.length > 0;
                                        })
                                        .debounce(300)
                                        .distinctUntilChanged();
                                    var searcher = keyup.flatMapLatest(loadAjax2);
                                    searcher.subscribe(
                                        function (data) {
                                            loadResult(data, 'product');
                                            if (settings.searchArticle === '1') {
                                                loadArticle(window.__q__);
                                            }
                                        },
                                        function (error) {

                                        });
                                }
                            }
                            window.addEventListener('click', function () {
                                suggestionWrap.style.display = 'none';
                            });
                        });

                    </script>

                </div>
                <div class="mobile-cart">
                    <a href="{{ route('order.view') }}" title="Giỏ hàng">
                        <img class="lazy" data-src="{{ URL::asset('public/frontend/themes/stbd/images/shopping-cart.svg') }}"
                             alt="Giỏ hàng"/>
                        <div class="cart-right">
                            <span class="count_item_pr" id="count-cart">
                                @if(Session::has('cart'))
                                    @php
                                        $count_cart = 0;
                                    foreach (Session::get('cart')['items'] as $v) {
                                        $count_cart += $v['quantity'];
                                    }
                                    @endphp
                                    {{ $count_cart }}
                                @else
                                    {{ 0 }}
                                @endif
                            </span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>