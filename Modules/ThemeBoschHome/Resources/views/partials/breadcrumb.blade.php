<ul class="breadcrumb f">
    <?php
    $slug_arr = explode('/', $_SERVER['REQUEST_URI']);
    $breadcrumb = [
        '/' => 'Trang chủ'
    ];
    $url = '';
    foreach ($slug_arr as $slug) {
        if ($slug != '') {
            if (strpos($slug, '.html')) {
                $slug = str_replace('.html', '', $slug);

                $product = CommonHelper::getFromCache('product_name_slug'.$slug);
                if(!$product){
                    $product = \Modules\ThemeBoschHome\Models\Product::select('name')->where('slug', $slug)->first();
                    CommonHelper::putToCache('product_name_slug'.$slug, $product);
                }

                if (is_object($product)) {
                    $url .= '/' . $slug;
                    $breadcrumb[$url] = $product->name;
                } else {
                    $post = CommonHelper::getFromCache('post_name_slug'.$slug);
                    if(!$post){
                        $post = \Modules\ThemeBoschHome\Models\Post::select('name')->where('slug', $slug)->first();
                        CommonHelper::putToCache('post_name_slug'.$slug, $post);
                    }

                    if (is_object($post)) {
                        $url .= '/' . $slug;
                        $breadcrumb[$url] = $post->name;
                    }
                }
            }elseif($slug == 'chinh-hang'){
                $url .= '/' . $slug;
                $breadcrumb[$url] = 'Chính hãng';
            } else {
                if (strpos($slug, '?')){
                    $slug1 = explode('?', $slug);
                    $slug = $slug1[0];
                }
                $cat = CommonHelper::getFromCache('category_name_slug'.@$slug);
                if (!$cat) {

                    $cat = \Modules\ThemeBoschHome\Models\Category::select('name')->where('slug', $slug)->first();

                    CommonHelper::putToCache('category_name_slug'.@$slug, $cat);
                }

                if (is_object($cat)) {
                    $url .= '/' . $slug;
                    $breadcrumb[$url] = $cat->name;
                } else {
                    $manu = CommonHelper::getFromCache('manufacturer_name_slug'.$slug);
                    if (!$manu) {
                        $manu = \Modules\ThemeBoschHome\Models\Manufacturer::select('name')->where('slug', $slug)->first();
                        CommonHelper::putToCache('manufacturer_name_slug'.$slug, $manu);
                    }

                    if (is_object($manu)) {
                        $url .= '/' . $slug;
                        $breadcrumb[$url] = $manu->name;
                    } elseif(count($slug_arr) == 2) {
                        $breadcrumb[$slug] = @$pageOption['title'];
                    }
                }
            }
        }
    }
    ?>
    @foreach($breadcrumb as $slug => $name)
        <li itemscope="itemscope" itemtype="">
            <a itemprop="url" href="{{ url($slug) }}" title="{{ $name }}">
                <span itemprop="title">{{ $name }}</span></a></li>
    @endforeach
</ul>
