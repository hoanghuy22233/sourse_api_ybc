<?php
$cates = CommonHelper::getFromCache('category_type_5_show_menu_1_order_no_asc_parent_id_0_or_null');
if (!$cates) {
    $cates = Modules\ThemeBoschHome\Models\Category::where('status', 1)
        ->where('type', 5)->where('show_menu', 1)->orderBy('order_no', 'asc')
        ->where(function ($query) {
            $query->where('parent_id', 0)->orwhere('parent_id', null);
        })->get();
    CommonHelper::putToCache('category_type_5_show_menu_1_order_no_asc_parent_id_0_or_null', $cates);
}


$dataShowrooms = CommonHelper::getFromCache('get_showroom_footer');
if (!$dataShowrooms) {
    $dataShowrooms = Modules\ThemeBoschHome\Models\Showroom::all();
    CommonHelper::putToCache('get_showroom_footer', $dataShowrooms);
}
$p = [];
$c = ['values' => [], 'location' => []];
foreach ($dataShowrooms as $key => $t) {
    array_push($p, mb_strtoupper($t['location']));
    foreach ($p as $key => $l) {
        $p = array_unique($p);
    }
}
foreach ($p as $keyp => $l) {
    array_push($c['values'], [$l => []]);
    array_push($c['location'], $l);
    foreach ($dataShowrooms as $key => $t) {
        if (mb_strtoupper($l) == mb_strtoupper($t['location'])) {
            array_push($c['values'][$keyp][$l], $t);
        }
    }
}
$fg = [];
foreach ($c['values'] as $f) {
    foreach ($f as $g) {
        foreach ($g as $gj) {
            $fg[] = $gj;
        }
    }
}

?>
<style>
    #menu-show > .flexJus > .hover1 > span a {
        width: 100%;
        text-align: left;
        margin-bottom: 10px;
    }
</style>
<div class="f nav" id="menu-show">
    <ul class="b flexJus">
        @foreach($cates as $cate)
            <li class="hover1">
                <?php
                $id = $cate->getOriginal('id');
                $subcates = CommonHelper::getFromCache('sub-menu-by-' . $id);

                if (!$subcates) {
                    $subcates = Modules\ThemeBoschHome\Models\Category::where('status', 1)
                        ->where('parent_id', $id)->get();
                    CommonHelper::putToCache('sub-menu', $subcates);
                }
                ?>
                <a class="a-link @if($subcates) has-child-level @endif"
                   href="javascript:;" title="{{$cate->name}}">
                    <div class="cat-img">
                        <img src="{{\Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getUrlImageThumb($cate->image, 100, null) }}"
                             alt="{{$cate->name}}"/>
                    </div>
                </a>
                <span class="menu-hover" style="{{ 'color:'. @$cate->color }}"><a
                            href="{{route('cate.list', ['slug' => $cate->slug])}}">{{$cate->name}}</a></span>

                @if($subcates)
                    <ul class="sub-menu list-sub-category">
                        @foreach($subcates as $subcate)
                            <li class="hover1"><a href="{{$subcate->slug}}"
                                                  title="{{$subcate->name}}">{{$subcate->name}}</a></li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
    </ul>
    <div class="f mcontact">
        <p>
            @if(!empty($menus))
                @foreach($menus as $menu)
                    @if($menu['link'] == '#')
                        @if($menu['name'] != 'Sản phẩm')
                            <span class="mobie_menu_f">

                            <a href="javascript:;" onclick="showmenudv();"
                               title="{{$menu['name']}}">{{$menu['name']}}</a>

                    </span>
                        @endif
                    @else
                        <span class="mobie_menu_f">
                        <a href="{{ $menu['link'] }}" rel="nofollow" title="{{$menu['name']}}">{{$menu['name']}}</a>
                    </span>
                    @endif
                @endforeach
            @endif
        </p>
        @if(!empty($c))
            @foreach($c['location'] as $location)
                <?php
                $shoroomLocation = CommonHelper::getFromCache('showroom_location' . $location);
                if (!$shoroomLocation) {
                    $shoroomLocation = Modules\ThemeBoschHome\Models\Showroom::where('location', $location)->orWhere('location', mb_strtolower($location))
                        ->orWhere('location', ucfirst($location))->orWhere('location', ucwords($location))->get();
                    CommonHelper::putToCache('showroom_location' . $location, $shoroomLocation);
                }
                ?>
                <p><a class="fa-showroom"
                      href="{{route('showroom.list', ['slug' => Modules\ThemeBoschHome\Http\Helpers\CommonHelper::convertSlug($location)])}}"
                      title="hệ thống showroom">Xem địa chỉ {{count($shoroomLocation)}} Showroom tại {{$location}}</a>
                </p>
            @endforeach
        @endif
    </div>
</div>

<script>
    $(document).ready(function () {
        $('header').on("click", "header .mclo", function () {
            //     $('.mclo').click(function () {
            $('#dichvu').hide();
            console.log('asadasd')
        });
        $('#touch-menu').click(function () {
            $('#menu-show').slideToggle();
            icon = $('header').find("#touch-menu");
            icon.toggleClass("touch-menu mclo");
        });
        $('header').on("click", "#touch-menu", function () {
            $('#dichvu').slideUp('1000');
        });
    })

</script>
