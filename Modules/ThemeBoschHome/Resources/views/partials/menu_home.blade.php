@php
    $menus = CommonHelper::getFromCache('getFromCache');
    if (!$menus){
            $menus = \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getMenusByLocation('main_menu', 5, false);
            CommonHelper::putToCache('get_menus_by_home', $menus);
    }
    $num_pro_in_cart = \Modules\ThemeSTBD\Http\Controllers\Frontend\OrderController::totalCart();
@endphp
@if(@$settings['option_menu'] == 0)
    <style>
        .head {
            height: 40px;
        }
    </style>
@endif
<style>
</style>
<header class="header">
    <div class="container">
        <div class="header-top">
            <div class="logo">
                <a href="/">
                    <img class="lazy" data-src="{{CommonHelper::getUrlImageThumb(@$settings['logo_brand'], 230, null) }}"alt="brands" />
                </a>
            </div>
            <div class="identifier">
                <div class="lang-btn">
                    <h3><a href="/">{{empty($settings['identifier']) ? 'Home Appliances' : $settings['identifier']}}</a></h3>
                </div>
            </div>
        </div>
        <div class="header-bot">
            <nav>
                @if(!empty($menus))
                    @foreach($menus as $menu)
                        <a href="{{ $menu['link'] }}" data-menu-slug="menu-{{ str_slug($menu['name'], '_') }}">{{ $menu['name'] }}</a>
                    @endforeach
                @endif
            </nav>
            <div class="search-box">
                @if(!isMobile())
                @include('themeboschhome::partials.search')
                @endif
            </div>
        </div>
    </div>
    <div class="menu-items container-fluid">
        <div class="container">
        @if(!empty($menus))
            @foreach($menus as $menu)
                @if($menu['link'] == '#')
                    @if(!isMobile())
                        @if(@$settings['option_menu'] == 1 && str_slug($menu['name'], '_')=='san_pham')
                            <ul class="sub-menu menu-{{ str_slug($menu['name'], '_') }} expandable">
                            @include('themeboschhome::partials.menu_efect.menu_dropdown')
                                <div style="clear:both;width: 100%;text-align: center;padding: 40px 0 30px">
                                    <button onclick="$(this).parent().parent().slideUp()">Close</button>
                                </div>
                            </ul>
                        @endif
                    @endif
                @else
                    @if(!empty($menu['childs']))
                        <ul class="sub-menu menu-{{ str_slug($menu['name'], '_') }}">

                            @foreach($menu['childs'] as $c=>$child)
                                <li class="sub-menu-item">
                                        @if($child['image'])
                                        <img class="lazy" data-src="{{CommonHelper::getUrlImageThumb($child['image'], 250, null) }}" alt="">
                                        @endif
                                        <a href="{{$child['link']}}">{{$child['name']}}</a>
                                    <?php
                                    $sub_menu_3 = \Modules\ThemeBoschHome\Models\Menu::where('url', $child['link'])->first();

                                    ?>
                                    @if(!empty($sub_menu_3->childs))
                                        <ul class="sub-menu3">

                                            @foreach($sub_menu_3->childs as $c3=>$child3)
                                                <li class="sub-menu-item3">
                                                    <a href="{{$child['link'].$child3->url}}">{{$child3->name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                            <div class="btn-close" style="clear:both;width: 100%;text-align: center;padding: 40px 0 30px">
                                <button onclick="$(this).parent().parent().slideUp()">Close</button>
                            </div>
                        </ul>
                    @endif
                @endif
            @endforeach
        @endif
        </div>
    </div>
    <div class="conversion-bar container-fluid">
        <div class="container">
            <i class="fal fa-list mb-menu-btn"><span>menu</span></i>
            @unless(Request::is('/'))
            @include('themeboschhome::partials.breadcrumb')
            @endunless
            <div class="m-conversion">
                {{--<a href="#"><i class="fal fa-map-marker-alt"></i>Dealer Locator</a>--}}
                <a href="/gio-hang"><i class="fal fa-shopping-cart"><span>{{empty($num_pro_in_cart) ? 0 : $num_pro_in_cart}}</span></i>Giỏ hàng</a>
            </div>
            <div class="m-conversion mobile">
                <a href="#" class="to-top-btn search-btn"><i class="fal fa-search"></i></a>
                <a href="#"><i class="fal fa-map-marker-alt"></i></a>
                <a href="/gio-hang"><i class="fal fa-shopping-cart"><span>{{empty($num_pro_in_cart) ? 0 : $num_pro_in_cart}}</span></i></a>
            </div>
        </div>
    </div>
    <div class="nav-sticky header-bot container-fluid">
        <div class="container">
            <i class="fal fa-list mb-menu-btn to-top-btn"><span>Menu</span></i>
            <nav>
                @if(!empty($menus))
                    @foreach($menus as $menu)
                        <a href="{{ $menu['link'] }}">{{ $menu['name'] }}</a>
                    @endforeach
                @endif
            </nav>
            <div class="m-conversion">
                <a href="#" class="to-top-btn search-btn"><i class="fal fa-search"></i></a>
                <a href="#"><i class="fal fa-map-marker-alt"></i></a>
                <a href="/gio-hang"><i class="fal fa-shopping-cart"><span>0</span></i></a>
                <a href="#" class="to-top-btn" data-action="to-top"><i class="fal fa-chevron-up"></i></a>
            </div>
        </div>
    </div>
    <div class="mb-search-box">
        @if (isMobile())
        @include('themeboschhome::partials.search')
        @endif
    </div>
    <div class="mobile-nav">
        <nav class="mb-main-nav">
            @if(!empty($menus))
                <ul>
                    @foreach($menus as $menu)
                        <li>
                            <a href="{{ $menu['link'] }}" class="a-link @if($menu['link'] == '#' || !empty($menu['childs'])) has-child-level @endif" title="{{ $menu['name'] }}">{{ $menu['name'] }}</a>
                            @if($menu['link'] == '#')
                                @if(@$settings['option_menu'] == 1 && str_slug($menu['name'], '_')=='san_pham')
                                    <ul class="sub-menu menu-{{ str_slug($menu['name'], '_') }} expandable">
                                        @include('themeboschhome::partials.menu_efect.menu_dropdown')
                                    </ul>
                                @endif
                            @else
                                @if(!empty($menu['childs']))
                                    <ul class="sub-menu menu-{{ str_slug($menu['name'], '_') }}">

                                        @foreach($menu['childs'] as $c=>$child)
                                            <li class="sub-menu-item">
                                                @if($child['image'])
                                                    <img class="lazy"
                                                         data-src="{{CommonHelper::getUrlImageThumb($child['image'], 250, null) }}"
                                                         alt="">
                                                @endif
                                                <a href="{{$child['link']}}">{{$child['name']}}</a>
                                                <?php
                                                $sub_menu_3 = \Modules\ThemeBoschHome\Models\Menu::where('url', $child['link'])->first();

                                                ?>
                                                @if(!empty($sub_menu_3->childs))
                                                    <ul class="sub-menu3">

                                                        @foreach($sub_menu_3->childs as $c3=>$child3)
                                                            <li class="sub-menu-item3">
                                                                <a href="{{$child['link'].$child3->url}}">{{$child3->name}}</a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            @endif
                        </li>
                    @endforeach
                </ul>

            @endif
        </nav>
        <div class="drop-title">
            <div class="drop-name">Testing</div>
        </div>
        <button class="close-mb-menu">Close</button>
    </div>
</header>
<script>
    var lastScrollTop = 0;
    $(document).ready(function() {
        $(window).scroll(function (ev) {
            let st = $(this).scrollTop();
            if (st > lastScrollTop || st < $('header.header').height()){
                $('.nav-sticky').removeClass('active');
            } else {
                $('.nav-sticky').addClass('active');
            }
            lastScrollTop = st;
        })
        $('.to-top-btn').click(function(ev){
            ev.preventDefault();
            $('html, body').stop().animate({
                scrollTop: 0
            }, 1000);
        })
        $('.mb-menu-btn, .close-mb-menu').click(function(ev){
            ev.preventDefault();
            $('.mobile-nav').toggleClass('active');
            $('.mb-menu-btn').toggleClass('fa-close fa-list');
        })
        $('.search-btn').click(function (ev) {
            ev.preventDefault();
            $(this).toggleClass('active');
            $('.mb-search-box').toggleClass('active')
        })
        $('.header-bot nav a').click(function(ev){
            if($(this).data('menu-slug') == 'menu-san_pham') {
                ev.preventDefault();
                let menu_slug = $(this).data('menu-slug');
                if ($('.menu-items .sub-menu.'+menu_slug).css('display') == 'none') {
                    $('.menu-items .sub-menu').not('.'+menu_slug).hide();
                    $('.menu-items .sub-menu.'+menu_slug).slideDown();
                    checkMenu();
                } else {
                    $('.menu-items .sub-menu.'+menu_slug).slideUp();
                    $('.menu-items .flexJus .hover1').css('height','auto');
                }
            }
        });
        function checkMenu(){
            if($('.banner-category')){
                let height = 0;
                let productContent = $('.menu-items .flexJus > .hover1');
                for( let i = 0 ; i < productContent.length ; i++ ){
                    if($(productContent[i]).height() > height ){
                        height = $(productContent[i]).height();
                    }
                }
                $('.menu-items  .flexJus > .hover1').css('height',height);
            }
        }
        $('.has-child-level').click(function (e) {
            e.preventDefault();
            $('.mobile-nav .drop-name').text($(this).attr('title'));
            if( $('.mb-main-nav').hasClass('level1')){
                $('.mb-main-nav').addClass('level2');
                $('.drop-title').addClass('level2');
                $(this).parent().addClass('level2');
                $(this).next().addClass('level2');
            }else{
                $('.mb-main-nav').addClass('level1');
                $('.drop-title').addClass('level1');
                $(this).parent().addClass('level1');
                $(this).next().addClass('level1');
                $(this).addClass('level1')
            }

        })
        $('.mobile-nav .drop-name').click(function () {
            if($(this).parent().hasClass('level2')){
                $('.mobile-nav .level2').removeClass('level2')
                $(this).parent().removeClass('level2')
                $('.mb-main-nav').removeClass('level2');
                $('.mobile-nav .drop-name').text($('.has-child-level.level1').attr('title'));
            }else{
                $('.mobile-nav .level1').removeClass('level1')
                $(this).parent().removeClass('level1')
                $('.mb-main-nav').removeClass('level1');
            }

        })
    });
</script>
