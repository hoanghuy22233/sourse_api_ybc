<div class="contact_now_parent">
    <div class="contact_now">
        {!! @\Modules\ThemeBoschHome\Models\Widget::where('location', 'fixed_right_contact')->where('status', 1)->first()->content !!}
        @include('themeboschhome::partials.phone_now_detail')
    </div>
</div>