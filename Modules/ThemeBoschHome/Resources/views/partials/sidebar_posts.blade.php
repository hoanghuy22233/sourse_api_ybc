<?php
//dd(($category));
//     $slug = urldecode(last(request()->segments()));
//     $category = CommonHelper::getFromCache('get_category_by_post'.$slug);
//    if (!$category){
//        $category = Modules\ThemeBoschHome\Models\Category::where('slug', $slug)->where('status', 1)->first();
//       CommonHelper::putToCache('get_category_by_post'.$slug, $category);
//    }
//     $newPosts = CommonHelper::getFromCache('get_newpost_by_post');
//    if (!$newPosts){
//        $newPosts = \Modules\ThemeBoschHome\Models\Post::where('status', 1)->where('slug', '<>', '#')
//                                     ->orderBy('id', 'desc')->where('type_page', '<>', 'page_static')->limit(5)->get();
//       CommonHelper::putToCache('get_newpost_by_post', $newPosts);
//    }
$products_hots = CommonHelper::getFromCache('get_products_hot' . @$category->category_product_id);
if (!$products_hots) {
    $products_hots = \Modules\ThemeBoschHome\Models\Product::where('featured', 1)->where('multi_cat', 'like', '%|' . @$category->category_product_id . '|%')->where('status', 1)->take(5)->orderBy('order_no', 'asc')->get();
    CommonHelper::putToCache('get_products_hot' . @$category->category_product_id, $products_hots);
}
?>
<!--chi danh cho tin thuoc trang tin hay-->
{{--<div class="newsright bgghi">--}}
    {{--<ul class="news-right2">--}}
        {{--@if(!empty($newPosts))--}}
            {{--@foreach($newPosts as $newPost)--}}
                {{--<li>--}}
                    {{--<a class="img-news" href="{{route('cate.list', ['slug' => $newPost->slug])}}.html" title="{{$newPost->name}}">--}}
                        {{--<img class="lazy" data-src="{{CommonHelper::getUrlImageThumb($newPost->image, 300, null) }}" alt="{{$newPost->name}}"></a>--}}
                    {{--<h3><a href="{{route('cate.list', ['slug' => $newPost->slug])}}.html" title="{{$newPost->name}}">{{$newPost->name}}</a></h3>--}}
                {{--</li>--}}
            {{--@endforeach--}}
        {{--@endif--}}
    {{--</ul>--}}
{{--</div>--}}
<!--chi danh cho tin thuoc trang tin hay-->
<style>
    .canhbao>label{
        background-color: #d13317;
    }
    .canhbao>p{
        padding: 10px;
    }
    .canhbao {
        border: 1px solid #ccc;
    }
</style>
<div class="newsright">
    <label>Tư vấn xem nhiều nhất</label>
    <ul class="news-list" style="border: 1px solid #ccc">
        <?php
        $postViews = CommonHelper::getFromCache('get_postviews_by_post');
        if (!$postViews){
            $postViews = \Modules\ThemeBoschHome\Models\Post::where('status', 1)->where('slug', '<>', '#')->where('multi_cat', 'like', '%|'.@$category->id.'|%')
                ->orderBy('view_total', 'desc')->where('type_page', '<>', 'page_static')->limit(5)->get();
            CommonHelper::putToCache('get_postviews_by_post', $postViews);
        }
        ?>
        @if(!empty($postViews))
            @foreach($postViews as $key => $postView)
                    @if($postView->slug != '#')
                    <li>
                        <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($postView) }}" title="Nên mua bếp từ hay bếp hồng ngoại">{{$postView->name}}</a>
                        <span>{{$key+1}}</span>
                    </li>
                    @endif
            @endforeach
        @endif
    </ul>
</div>

<div class="newsright spbccc bgghi">
    <h4 style="
    text-align: center;
    padding: 15px;
    font-size: 20px;
    text-transform: uppercase;
    background: #0388fc;font-weight: bold; color: #fff;
">Sản phẩm bán chạy</h4>
    <ul class="news-right2" style="background: #fff;border: 1px solid #fff576;">
        @if(!empty($products_hots))
            @foreach($products_hots as $products_hot)
                <li style="border-bottom: 1px solid #ccc; display: flex;padding: 5px!important;">
                    <a class="img-news" style="width: 30%"
                       href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($products_hot, 'array') }}"
                       title="{{$products_hot->name}}">
                        <img class="lazy" data-src="{{CommonHelper::getUrlImageThumb($products_hot->image, 300, null) }}"
                             alt="{{$products_hot->name}}">
                    </a>
                    <div class="title_price_pro" style="width: 70%">
                        <h3 style="margin: 0; padding: 0 5px;    line-height: 1;">
                            <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getProductSlug($products_hot, 'array') }}"
                               title="{{$products_hot->name}}">{{$products_hot->name}}</a>
                        </h3>
                        <strike>{{number_format($products_hot->base_price, 0,'','.')}}<sup>đ</sup></strike>
                        <p style="font-weight: bold;color: red">{{number_format($products_hot->final_price, 0,'','.')}}
                            <sup>đ</sup></p>
                    </div>


                </li>
            @endforeach
        @endif
    </ul>
</div>
<div id="endright" style="float: left; width: 100%;"></div>

<script>
   $(document).ready(function () {
       $('#menu_catalog ul a').each(function () {
           if ($(this).hasClass('a2act')){
               $(this).parents('ul').css('display', 'block');
               $(this).parents('ul').prev('a').addClass('a2act');
           }
       })
   })
</script>

<?php
$v = CommonHelper::getFromCache('widget_category_post_sidebar_right');
if (!$v) {
    $v = \Modules\ThemeBoschHome\Models\Widget::where('location', 'category_post_sidebar_right')->where('status', 1)->first();
    CommonHelper::putToCache('widget_category_post_sidebar_right', $v);
}
?>
@if(is_object($v))
    <div class="newsright canhbao">
        <label>{!! $v->name !!}</label>
        <p>{!! $v->content !!}</p>
    </div>
@endif