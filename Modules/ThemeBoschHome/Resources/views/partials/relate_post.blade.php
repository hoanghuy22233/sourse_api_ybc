<?php
$data = CommonHelper::getFromCache('get_post_by_multi_cat_like_category_id' . trim(@$post->related_posts,'|'));
if (!$data) {
    $relate_post_id = explode('|',trim(@$post->related_posts,'|'));
    $data = \Modules\ThemeBoschHome\Models\Post::select(['id', 'name', 'slug','intro', 'multi_cat','image'])
        ->where('status', 1)->whereIn('id', $relate_post_id)
        ->where('type_page', 'post_type')->orderBy('order_no', 'asc')->get();
    CommonHelper::putToCache('get_post_by_multi_cat_like_category_id' . trim(@$post->related_posts,'|'), $data);
}

?>
<style>
    .col-post-relate {
        width: 50%;
        padding: 0 10px;
        display: inline-block;
        float: left;
    }
    .read_more_bvlq>span {
        padding: 10px;
        display: inline-block;
        cursor: pointer;
    }

    .read_more_bvlq {
        text-align: center;
    }
    .read_more_bvlq_none {
        display: none;
    }
    .sidebar_home_1 .list_news, .sidebar_home_1 .art_item, .sidebar_home_1 .art_item {
        padding-bottom: 0;
    }
    .relate_post>h2 {
        border-bottom: 3px solid green;
    }
    .list_news, .art_item {
        /*border-bottom: 1px dotted #e2e2e3;*/
        margin-bottom: 10px;
        /*padding-bottom: 10px;*/
        float: left;
        width: 50%;
        padding-left: 2%;
        padding-right: 2%;
        border-right: 1px dashed #ccc;

        height: 210px;
    }
    .list_news:nth-of-type(2n){
        border-right: none;
    }
    .list_news:first-child{
        padding-left: 0!important;
    }
    .list_news:last-child{
        border-right: none;
    }
    .title_news {
        margin-bottom: 5px;
        line-height: normal;
    }.title_news a {
         font: 700 14px arial;
         color: #333;
         position: relative;
         z-index: 3;
     }.title_news a {
          font: 700 14px arial;
          color: #333;
          position: relative;
          z-index: 3;
      }.list_news .thumb_art, .art_item .thumb_art {
           z-index: 1;
       }

    .thumb_art {
        position: relative;
        margin-right: 10px;
        width: 50%;
        margin-bottom: 10px;
    }.thumb_5x3 {
         padding-bottom: 60%;
     }

    .thumb, .thumb_img {
        display: block;
        overflow: hidden;
        height: 1px;
        position: relative;
        width: 100%;
        text-align: center;
    }
    .thumb_art img {
        width: 100%;
    }

    .thumb img, .thumb_img img {
        width: auto;
        float: none;
        object-fit: none;
        max-height: 140px;
    }
    .sidebar_home_1 .description, .sidebar_home_1 .news_lead {
        margin-bottom: 5px;
    }
    p.description{
        width: 50%;
    }
    .description, .news_lead {
        color: #333;
        font: 400 13px/16px arial;
        margin-bottom: 5px;
    }.description a, .news_lead a {
         color: #333;
         overflow: hidden;
         text-overflow: ellipsis;
         display: -webkit-box;
         line-height: 16px;
         max-height: 110px;
         -webkit-line-clamp: 7;
         -webkit-box-orient: vertical;
     }
    .img-content-relate{
        display: flex;
        height: 150px;
    }
    @media (max-width: 768px) {

        .list_news, .art_item {
            width: 100%;
        }
        .list_news{
            border-right: none;
        }
        .read_more_bvccm_none_mobie{
            display: none;
        }
    }
</style>
<div class="relate_post">
    <h2>BÀI LIÊN QUAN</h2>
    @foreach($data as $i=>$item)
    <article class="list_news list_bvlq {{($i>2)?'read_more_bvccm_none_mobie':''}} {{($i>5)?'read_more_bvlq_none':''}}" data-campaign="ThuongVien">
        <h4 class="title_news">
            <a data-medium="Item-7" data-thumb="1" href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($item) }}" title="{{@$item->name}}">{{@$item->name}}</a>
        </h4>
        <div class="img-content-relate">
            <div class="thumb_art">
                <a style="height: 100%;padding: 0;" class="thumb thumb_5x3" data-medium="Item-7" data-thumb="1" href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($item) }}" title="{{@$item->name}}">
                    <img data-src="{{CommonHelper::getUrlImageThumb($item->image, 100, null) }}" class="lazy vne_lazy_image lazyInitial lazyLoaded" alt="{{@$item->name}}" data-was-processed="true">
                </a>
            </div>
            <p class="description">
                <a data-medium="Item-7" data-thumb="1" href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($item) }}" title="{{@$item->name}}">{{@$item->intro}}</a>
            </p>
        </div>
    </article>
    @endforeach
    @if($data->count()>5)
        <div class="f read_more_bvlq">
            <span>Xem thêm...</span>
        </div>
    @endif
</div>
<script>
    $(document).ready(function () {
        $('.read_more_bvlq').click(function () {
            $('.list_bvlq').toggleClass('read_more_bvlq_none');
            $('.list_bvccm').removeClass('read_more_bvccm_none_mobie',1000);
        });
    })
</script>
