@extends('themeboschhome::layouts.layout_homepage')
@section('main_content')
    <style>
        @media (max-width: 768px) {
            .bnews li .bsum {
                display: none;
            }
        }

        @media (min-width: 768px) {
            .tim {
                width: 72% !important;
            }
        }

        @media (min-width: 1300px) {
            .topic img, .topic object {
                display: inline-block;
            }

            @for($i = 0; $i < count($posts); $i++)
            @if($i % 2 == 0)
                .dd-{{ $i }}   {
                display: inline-block;
                text-align: left;
            }

            .dd-{{ $i }}   {
                display: inline-block;
                text-align: right;
            }

            @else
                .dd-{{ $i }}   {
                display: inline-block;
                text-align: right;
            }

            .dd-{{ $i }}   {
                display: inline-block;
                text-align: left;
            }

            @endif
            @endfor

            .fix-box {
                min-height: 0 !important;
            }

        }

        .bnews a div {
            max-height: 150px;
            overflow: hidden;
            text-align: center;
            display: inline-block;
        }

        .bnews li {
            padding-bottom: 0;
            border: 1px #ccc solid;
            border-radius: 4px;
            box-shadow: 1px 1px 2px #666;
            background: #fff;
            padding: 10px !important;
            text-align: center;
        }
    </style>
    <div class="f f123 category-container"
         style="background-image:url('./public/frontend/themes/bosch/image/supergraphic-gray-light.svg')">
        @php
            $menus = \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getFromCache('getFromCache');
            if (!$menus){
                    $menus = \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getMenusByLocation('main_menu', 5, false);
                    \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::putToCache('get_menus_by_home', $menus);
            }
            $cates = CommonHelper::getFromCache('category_type_5_show_menu_1_order_no_asc_parent_id_0_or_null',['categories']);
                if (!$cates) {
                    $cates = Modules\ThemeSTBD\Models\Category::where('status', 1)
                        ->where('type', 5)->where('show_menu', 1)->orderBy('order_no', 'asc')
                        ->where(function ($query) {
                            $query->where('parent_id', 0)->orwhere('parent_id', null);
                        })->get();
                    CommonHelper::putToCache('category_type_5_show_menu_1_order_no_asc_parent_id_0_or_null', $cates,['categories']);
                }
        @endphp

        <div class="slide-category">
            @foreach($cates as $k => $cate)

                <div class="topic">
                    <div class="ddimg flexL dd-{{ $k }}" id="nb0" style="width: 100%;">
                        <img class="lazy0 lazy"
                             data-src="{{CommonHelper::getUrlImageThumb($cate->image, 675, null) }}"
                             alt="{{$cate->name}}"/>
                    </div>
                    <div class="ddinfo padd1 dc-{{ $k }}" style="width: 100%;">
                        <ul class="teaser-links list-unstyled">
                            <li>
                                <a href="{{ $cate->slug }}" class="a-link a-link-primary" tabindex="0">
                                    <span class="text">{{$cate->name}}</span>
                                </a>
                            </li>
                            @if(count($cate->childs) > 0)
                                @foreach($cate->childs as $child)
                                    <li>
                                        <a href="{{ $child->slug }}" class="a-link a-link-primary" tabindex="0">
                                            <span class="text">{{$child->name}}</span>
                                        </a>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            @endforeach
        </div>
        @if(!empty($posts))
            <div class="hide">
                @foreach($posts as $k => $post)
                    <?php
                    $cate_get_by_post = CommonHelper::getFromCache('get_category_by_post' . $post->multi_cat);
                    if (!$cate_get_by_post) {
                        $cate_get_by_post = Modules\ThemeBoschHome\Models\Category::whereIn('id', explode('|', $post->multi_cat))->first();
                        CommonHelper::putToCache('get_category_by_post' . $post->multi_cat, $cate_get_by_post);
                    }
                    ?>
                    <div class="topic">
                        <div class="ddimg flexL dd-{{ $k }}" style="order:{{$post->oder}};" id="nb0">
                            <img class="lazy0 lazy"
                                 data-src="{{CommonHelper::getUrlImageThumb($post->image, 675, null) }}"
                                 alt="{{$post->name}}"/>
                        </div>
                        <div class="ddinfo padd1 dc-{{ $k }}" style="order:@if($post->oder == 1) 2 @else 1 @endif;">
                            <ul class="teaser-links list-unstyled">
                                <li>
                                    <a href="{{ \Modules\ThemeBoschHome\Http\Helpers\CommonHelper::getPostSlug($post, 'array') }}"
                                       class="a-link a-link-primary" tabindex="0">
                                        <span class="text">{{$post->name}}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>

        @endif
        {{--<div class="pagingInfo">1/5</div>--}}

    </div>

    <script>
        if ($(window).width() < 767) {
            $(window).on('load', function () {
                $('.owl-pagination').css('top', $('.item img').height() - $('.owl-pagination').outerHeight());
            })
        }
        $(".slide-category").slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            loop: false,
            responsive: [
                {
                    breakpoint: 767,
                    settings: "unslick"
                }
            ]
        })
        $(".slide-category").on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
            //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
            var i = (currentSlide ? currentSlide : 0) + 1;
            $('.pagingInfo').text(i + '/' + slick.slideCount);
        });
    </script>
    <script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "Store",
        "image": [
            "https://bephoangcuong.com/public/filemanager/userfiles/1.logo/BepHC-logo-Web8.png"
        ],
        "@id": "https://bephoangcuong.com/",
        "name": "Bếp Hoàng Cương - Since 1995",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Số 348 Bạch Đằng",
            "addressLocality": "Quận Bình Thạnh",
            "addressRegion": "Thành phố Hồ Chí Minh",
            "postalCode": "100000",
            "addressCountry": "Vietnam"
        },
        "url": "https://bephoangcuong.com/",
        "priceRange": "VND",
        "telephone": "0974329191",
        "openingHoursSpecification": [
            {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": [
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday",
                    "Saturday"
                ],
                "opens": "08:00",
                "closes": "21:00"
            },
            {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": "Sunday",
                "opens": "08:00",
                "closes": "21:00"
            }
        ]
    }


    </script>
@endsection
