const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/assets/js/app.js', 'frontend/themes/bosch/js/themebosch.js')
    .sass( __dirname + '/Resources/assets/sass/app.scss', 'frontend/themes/bosch/css/themebosch.css');

if (mix.inProduction()) {
    mix.version();
}
