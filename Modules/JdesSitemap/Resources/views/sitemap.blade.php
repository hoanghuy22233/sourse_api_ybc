<?php echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    {{--Sitemap cho danh muc--}}
    <sitemap>
        <loc>{{ URL::to('/category-sitemap.xml') }}</loc>
        <lastmod>{{ date("Y-m-")}}01T08:00:00+07:00</lastmod>
    </sitemap>

    {{--Sitemap cho tin tuc--}}
    <sitemap>
        <loc>{{ URL::to('/post-sitemap.xml') }}</loc>
        <lastmod>{{ date("Y-m-")}}01T08:00:00+07:00</lastmod>
    </sitemap>

    {{--Sitemap cho san pham--}}
    <sitemap>
        <loc>{{ URL::to('/product-sitemap.xml') }}</loc>
        <lastmod>{{ date("Y-m-")}}01T09:00:00+07:00</lastmod>
    </sitemap>
</sitemapindex>