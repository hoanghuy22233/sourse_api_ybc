<?php echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    <?php
    $count = \App\Models\Product::where('status', 1)->where('slug', '!=', '')->count();
    $total_page = ceil($count / 500);
    ?>
    @for($i = 1; $i <= $total_page; $i++)
        <sitemap>
            <loc>{{ URL::to('/product-sitemap/'.$i.'.xml') }}</loc>
            <lastmod>{{ date("Y-m-")}}01T08:00:00+07:00</lastmod>
        </sitemap>
    @endfor
</sitemapindex>