<?php
if (!isset($_GET['parent_id']) && isset($result)) {
    $_GET['parent_id'] = $result->id;
    $course = $result;
} else {
    $course = \Modules\Sach100Exam\Models\Category::find($_GET['parent_id']);
}


?>
<div class="row">
    <div class="col-md-12">
        <p class="" style="margin-left: 5%;">
            <a href="{{ '/admin/subject/'. @$_GET['parent_id']}}"
               class="{{$module['code'] == 'subject'  ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon-list"></i>Trình độ học</a>
            @if($module['code'] == 'chapter')
                <a href="/admin/chapter?parent_id={{ @$_GET['parent_id']}}"
                   class="{{$module['code'] == 'chapter' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                            class="flaticon2-drop"></i>kỹ năng</a>
            @endif
        </p>
    </div>
</div>
