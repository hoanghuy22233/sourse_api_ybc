<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Trang ngoài</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
@if(in_array('question', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon2-open-text-book"></i>
                    </span><span class="kt-menu__link-text">Câu hỏi</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/question" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Tất cả câu hỏi</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/question/add" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Tạo câu hỏi mới</span></a>
                </li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/subject" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Trình độ học</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/question_error" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Câu hỏi lỗi</span></a></li>

            </ul>
        </div>
    </li>
@endif
