@if(in_array('transaction_history_view', $permissions))
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/transaction_history"
                                                      class="kt-menu__link "><span
                    class="kt-menu__link-icon"><i class="flaticon-book"></i>
            </span><span class="kt-menu__link-text">Lịch sử giao dịch</span></a></li>
@endif