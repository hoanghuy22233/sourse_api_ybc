
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Cấu hình thi theo Trình độ
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body">
            <div class="kt-section kt-section--first">
                <?php
                $chapters = \Modules\Sach100Exam\Models\Category::where('parent_id', $result->id)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
                $config_subject = (array) json_decode($result->config_subject);
                ?>

                    <strong>Mức độ : 5-7+</strong></label>
                @foreach($chapters as $chapter)
                    <div class="col-xs-12">
                        {{ $chapter->name }}: &nbsp;&nbsp;&nbsp;<input name="config_subject[7][{{ $chapter->id }}][de]" type="number" style="width: 60px;" min="0" value="{{ isset($config_subject['7']->{$chapter->id}->de) ? $config_subject['7']->{$chapter->id}->de : '' }}"> Cơ bản&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="config_subject[7][{{ $chapter->id }}][tb]" type="number" style="width: 60px;" value="{{ isset($config_subject['7']->{$chapter->id}->tb) ? $config_subject['7']->{$chapter->id}->tb : '' }}" min="0"> Nghiệp dư&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="config_subject[7][{{ $chapter->id }}][kho]" type="number" style="width: 60px;" value="{{ isset($config_subject['7']->{$chapter->id}->kho) ? $config_subject['7']->{$chapter->id}->kho : '' }}" min="0"> Master<br>
                    </div>
                @endforeach

                    <br><br><label><strong>Mức độ : 8+</strong></label>
                @foreach($chapters as $chapter)
                    <div class="col-xs-12">
                        {{ $chapter->name }}: &nbsp;&nbsp;&nbsp;<input name="config_subject[8][{{ $chapter->id }}][de]" type="number" style="width: 60px;" value="{{ isset($config_subject['8']->{$chapter->id}->de) ? $config_subject['8']->{$chapter->id}->de : '' }}" min="0"> Cơ bản&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="config_subject[8][{{ $chapter->id }}][tb]" type="number" style="width: 60px;" value="{{ isset($config_subject['8']->{$chapter->id}->tb) ? $config_subject['8']->{$chapter->id}->tb : '' }}" min="0"> Nghiệp dư&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="config_subject[8][{{ $chapter->id }}][kho]" type="number" style="width: 60px;" value="{{ isset($config_subject['8']->{$chapter->id}->kho) ? $config_subject['8']->{$chapter->id}->kho : '' }}" min="0"> Master<br>
                    </div>
                @endforeach

                    <br><br><label><strong>Mức độ : 9+</strong></label>
                @foreach($chapters as $chapter)
                    <div class="col-xs-12">
                        {{ $chapter->name }}: &nbsp;&nbsp;&nbsp;<input name="config_subject[9][{{ $chapter->id }}][de]" type="number" style="width: 60px;" value="{{ isset($config_subject['9']->{$chapter->id}->de) ? $config_subject['9']->{$chapter->id}->de : '' }}" min="0"> Cơ bản&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="config_subject[9][{{ $chapter->id }}][tb]" type="number" style="width: 60px;" value="{{ isset($config_subject['9']->{$chapter->id}->tb) ? $config_subject['9']->{$chapter->id}->tb : '' }}" min="0"> Nghiệp dư&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="config_subject[9][{{ $chapter->id }}][kho]" type="number" style="width: 60px;" value="{{ isset($config_subject['9']->{$chapter->id}->kho) ? $config_subject['9']->{$chapter->id}->kho : '' }}" min="0"> Master<br>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Cấu hình thi theo mondai/kỹ năng
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body">
            <div class="kt-section kt-section--first">
                <?php
                $config_chapter = (array) json_decode($result->config_chapter);
                ?>
                <label><strong>Mức độ : 5-7+ </strong></label>
                    &nbsp;&nbsp;&nbsp;<input name="config_chapter[7][de]" type="number" style="width: 60px;" value="{{ isset($config_chapter['7']->de) ? $config_chapter['7']->de : '' }}" min="0"> Cơ bản&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="config_chapter[7][tb]" type="number" style="width: 60px;" value="{{ isset($config_chapter['7']->tb) ? $config_chapter['7']->tb : '' }}" min="0"> Nghiệp dư&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input name="config_chapter[7][kho]" type="number" style="width: 60px;" value="{{ isset($config_chapter['7']->kho) ? $config_chapter['7']->kho : '' }}" min="0"> Master<br>

                <label><strong>Mức độ : 8+</strong></label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="config_chapter[8][de]" type="number" style="width: 60px;" value="{{ isset($config_chapter['8']->de) ? $config_chapter['8']->de : '' }}" min="0"> Cơ bản&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input name="config_chapter[8][tb]" type="number" style="width: 60px;" value="{{ isset($config_chapter['8']->tb) ? $config_chapter['8']->tb : '' }}" min="0"> Nghiệp dư&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input name="config_chapter[8][kho]" type="number" style="width: 60px;" value="{{ isset($config_chapter['8']->kho) ? $config_chapter['8']->kho : '' }}" min="0"> Master<br>

                <label><strong>Mức độ : 9+</strong></label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input name="config_chapter[9][de]" type="number" style="width: 60px;" value="{{ isset($config_chapter['9']->de) ? $config_chapter['9']->de : '' }}" min="0"> Cơ bản&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input name="config_chapter[9][tb]" type="number" style="width: 60px;" value="{{ isset($config_chapter['9']->tb) ? $config_chapter['9']->tb : '' }}" min="0"> Nghiệp dư&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input name="config_chapter[9][kho]" type="number" style="width: 60px;" value="{{ isset($config_chapter['9']->kho) ? $config_chapter['9']->kho : '' }}" min="0"> Master<br>

            </div>
        </div>
    </div>
</div>

