@extends(config('core.admin_theme').'.template')
@section('main')
    <form class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid {{ @$module['code'] }}"
          action="" method="POST"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="return_direct" value="save_continue" type="hidden">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                     id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg" style="">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Chỉnh sửa {{ $module['label'] }}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="/admin/{{ $module['code'] }}" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Quay lại</span>
                            </a>
                            <div class="btn-group">
                                {{--                                @if(in_array($module['code'].'_edit', $permissions))--}}
                                <button type="submit" class="btn btn-brand">
                                    <i class="la la-check"></i>
                                    <span class="kt-hidden-mobile">Lưu</span>
                                </button>
                                <button type="button"
                                        class="btn btn-brand dropdown-toggle dropdown-toggle-split"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="kt-nav">
                                        <li class="kt-nav__item">
                                            <a class="kt-nav__link save_option" data-action="save_continue">
                                                <i class="kt-nav__link-icon flaticon2-reload"></i>
                                                Lưu và tiếp tục
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a class="kt-nav__link save_option" data-action="save_exit">
                                                <i class="kt-nav__link-icon flaticon2-power"></i>
                                                Lưu & Thoát
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a class="kt-nav__link save_option" data-action="save_create">
                                                <i class="kt-nav__link-icon flaticon2-add-1"></i>
                                                Lưu và tạo mới
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                {{--                                @endif--}}
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-6">
                <!--begin::Portlet-->
                <div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_1">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Thông tin đơn hàng
                            </h3>
                        </div>
                        <div class="kt-portlet__head-group pt-3">
                            <a title="Xem thêm" href="#" data-ktportlet-tool="toggle"
                               class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-angle-down"></i></a>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                @foreach($module['form']['general_tab'] as $field)
                                    @php
                                        $field['value'] = @$result->{$field['name']};
                                    @endphp
                                    <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                         id="form-group-{{ $field['name'] }}">
                                        @if($field['type'] == 'custom')
                                            @include($field['field'], ['field' => $field])
                                        @else
                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                    <span class="color_btd">*</span>@endif</label>
                                            <div class="col-xs-12">
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                <span class="form-text text-muted">{!! @$field['des'] !!}</span>
                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                                <div class="form-group-div">
                                    <p>Ảnh hóa đơn:</p>
                                    <p>
                                        @if(@$result->students->invoice_image != '')
                                            <img data-src="{{ asset('/public/filemanager/userfiles/'.@$result->students->invoice_image) }}"
                                                 class="file_image_thumb lazy"
                                                 style="
                                                            max-width: 100%;
                                                        "
                                                 alt="Ảnh hóa đơn">
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
            <div class="col-xs-12 col-md-6">
                <!--begin::Portlet-->
                <div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_1">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Thông tin khách hàng
                            </h3>
                        </div>
                        <div class="kt-portlet__head-group pt-3">
                            <a title="Xem thêm" href="#" data-ktportlet-tool="toggle"
                               class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-angle-down"></i></a>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="kt-portlet__body">
                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <td>Họ tên:</td>
                                        <td>{{ @$result->students->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Địa chỉ:</td>
                                        <td>{{ @$result->students->address }}</td>
                                    </tr>
                                    <tr>
                                        <td>Số điện thoại:</td>
                                        <td>{{ @$result->students->phone }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email:</td>
                                        <td>{{ @$result->students->email }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <a href="/admin/student/{{ @$result->students->id }}">Xem khách hàng</a>
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->

            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Trình độ học trong đơn
                            </h3>
                            {{--@if(!empty($result->students->invoice_image))--}}
                            {{--                        @if(in_array(0,$status))--}}
                            {{--<i class="fa fa-check-circle fa-2x active_course_all" data-bill_id="{{$result->id}}"--}}
                            {{--data-status=1--}}
                            {{--style="position: absolute;right: 15px;top: 10px;color: #5bbf1b; cursor: pointer;"></i>--}}
                            <span class="btn btn-success active_course_all" data-bill_id="{{$result->id}}"
                                  data-status=1
                                  style="position: absolute;right: 176px;top: 10px; cursor: pointer;">Kích hoạt tất cả</span>
                            {{--                        @else--}}
                            {{--<i class="fa fa-times-circle fa-2x active_course_all" data-bill_id="{{$result->id}}"--}}
                            {{--data-status=0--}}
                            {{--style="position: absolute;right: 45px;top: 10px; color: red; cursor: pointer;"></i>--}}
                            <sapn class="btn btn-danger active_course_all" data-bill_id="{{$result->id}}"
                                  data-status=0
                                  style="position: absolute;right: 15px;top: 10px; cursor: pointer;">Hủy kich hoạt tất cả
                            </sapn>
                            {{--                        @endif--}}
                            {{--@endif--}}
                        </div>

                    </div>
                    <div class="kt-portlet__body">
                        <!--begin::Widget -->
                        <table class="table table-striped">
                            <thead class="thead-light">
                            <tr>
                                <th>STT</th>
                                <th>Image</th>
                                <th>Tên Trình độ học</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $subjects = \Modules\Sach100Exam\Models\Category::whereIn('id', explode('|', $result->subjects))->get();
                            ?>
                            @foreach($subjects as $k => $item)
                                <tr>
                                    <td>{{ $k + 1 }}</td>
                                    <td>
                                        <img src="{{ CommonHelper::getUrlImageThumb(@$item->image, 100, 100) }}">
                                    </td>
                                    <td>{{ $item->name }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!--end::Widget -->
                </div>
            </div>
        </div>
    </form>
@endsection
@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/form.css') }}">
    <script src="{{asset('public/ckeditor/ckeditor.js') }}"></script>
    <script src="{{asset('public/ckfinder/ckfinder.js') }}"></script>
    <script src="{{asset('public/libs/file-manager.js') }}"></script>
@endsection
@section('custom_footer')
    <script src="{{ asset(config('core.admin_asset').'/js/pages/crud/metronic-datatable/advanced/vertical.js') }}"
            type="text/javascript"></script>
    <script src="{{asset('public/ckeditor/ckeditor.js') }}"></script>
    <script src="{{asset('public/ckfinder/ckfinder.js') }}"></script>
    <script src="{{asset('public/libs/file-manager.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce_editor.js') }}"></script>
    <script type="text/javascript">
        editor_config.selector = ".editor";
        editor_config.path_absolute = "{{ (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" }}/";
        tinymce.init(editor_config);
    </script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/form.js') }}"></script>
    <script>
        $('.active_course').click(function () {
            var order_id = $(this).data('order_id');
            var status = $(this).data('status');
            $.ajax({
                url: '/admin/bill/active-order',
                method: 'post',
                data: {
                    order_id: order_id,
                    status: status
                },
                success: function (result) {
                    if (result.status === true) {
                        location.reload();
                        toastr.success(result.msg);
                    } else {
                        toastr.success(result.msg);
                    }

                }
            })
        });
        $('.active_course_all').click(function () {
            var bill_id = $(this).data('bill_id');
            var status = $(this).data('status');
            $.ajax({
                url: '/admin/bill/active-order-all',
                method: 'post',
                data: {
                    bill_id: bill_id,
                    status: status
                },
                success: function (result) {
                    if (result.status === true) {
                        location.reload();
                        toastr.success(result.msg);
                    } else {
                        toastr.success(result.msg);
                    }

                }
            })
        });
    </script>
@endsection
