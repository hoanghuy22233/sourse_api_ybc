@if(isset($field['height']))
<style>
    div#cke_1_contents {
        height: {{ @$field['height'] }} !important;
    }
</style>
@endif
<label for="{{ $field['name'] }}">{{ @$field['label'] }}</label>
<textarea id="ck_{{ $field['name'] }}" name="{{ @$field['name'] }}"
          {{ strpos(@$field['class'], 'require') !== false ? 'required' : '' }}
          placeholder="{{ trans(@$field['label']) }}" {!! @$field['inner'] !!}
          class="form-control {{ @$field['class'] }}" {{ @$field['disabled']=='true'?'disabled':'' }}>{!! old($field['name']) != null ? old($field['name']) : @$field['value'] !!}</textarea>
<span class="text-danger">{{ $errors->first(@$field['name']) }}</span>

{{--<script src="{{asset('public/libs/ckeditor/ckeditor.js')}}"></script>--}}
{{--<script src="{{asset('public/libs/ckfinder/ckfinder.js')}}"></script>--}}

<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace("ck_{{ $field['name'] }}", {
        filebrowserBrowseUrl: '{{route('browser')}}',
        filebrowserImageBrowseUrl: '{{route("browser")}}?Type=Images',
        filebrowserUploadUrl: '../public/ckfinder/connector?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '../public/ckfinder/connector?command=QuickUpload&type=Images',
        filebrowserWindowWidth: '1000',
        filebrowserWindowHeight: '700'
    });
    CKEDITOR.config.width = '100%';
    CKEDITOR.config.extraPlugins = 'mathjax';
    CKEDITOR.config.mathJaxLib = 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/latest.js?config=TeX-MML-AM_CHTML';
</script>
