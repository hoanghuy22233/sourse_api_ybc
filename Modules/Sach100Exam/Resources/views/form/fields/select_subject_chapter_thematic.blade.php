@if(@$field['subject'] !== false)
    @php
        $fd = ['name' => 'subject_id', 'type' => 'select2_model', 'label' => 'Trình độ', 'model' => \Modules\Sach100Exam\Models\Category::class,
        'object' => 'subject','where' => 'type=10', 'display_field' => 'name', 'class' => $page_type == 'list' ? '' : 'required'];
        $fd['value'] = isset($result->{$fd['name']}) ? $result->{$fd['name']} : @$_GET[$fd['name']];
    @endphp
    <div class="col-xs-12">
        <div class="form-group-div form-group {{ @$fd['group_class'] }}"
             id="form-group-{{ $fd['name'] }}">
            <label for="{{ $fd['name'] }}">{{ @$fd['label'] }} @if(strpos(@$fd['class'], 'required') !== false)
                    <span class="color_btd">*</span>@endif</label>
            @include(config('core.admin_theme').".form.fields." . $fd['type'], ['field' => $fd])
            <span class="text-danger">{{ $errors->first('subject_id') }}</span>
        </div>
    </div>
@endif

@if(@$field['chapter'] !== false)
    @php
        $fd = ['name' => 'chapter_id', 'type' => 'select', 'label' => 'Kỹ năng', 'options' => [], 'class' => $page_type == 'list' ? '' : 'required' ];
        $fd['value'] = isset($result->{$fd['name']}) ? $result->{$fd['name']} : @$_GET[$fd['name']];
    @endphp
    <div class="col-xs-12">
        <div class="form-group-div form-group {{ @$fd['group_class'] }}"
             id="form-group-{{ $fd['name'] }}">
            <label for="{{ $fd['name'] }}">{{ @$fd['label'] }} @if(strpos(@$fd['class'], 'required') !== false)
                    <span class="color_btd">*</span>@endif</label>
            <div id="{{ $fd['name'] }}">
                @include(config('core.admin_theme').".form.fields." . $fd['type'], ['field' => $fd])
            </div>
            <span class="text-danger">{{ $errors->first('chapter_id') }}</span>
        </div>
    </div>
@endif

@if(@$field['thematic'] !== false)
    @php
        $fd = ['name' => 'thematic_id', 'type' => 'select', 'label' => 'Mondai', 'options' => [], 'class' => $page_type == 'list' ? '' : 'required'];
        $fd['value'] = isset($result->{$fd['name']}) ? $result->{$fd['name']} : @$_GET[$fd['name']];
    @endphp
    <div class="col-xs-12">
        <div class="form-group-div form-group {{ @$fd['group_class'] }}"
             id="form-group-{{ $fd['name'] }}">
            <label for="{{ $fd['name'] }}">{{ @$fd['label'] }} @if(strpos(@$fd['class'], 'required') !== false)
                    <span class="color_btd">*</span>@endif</label>
            <div id="{{ $fd['name'] }}">
                @include(config('core.admin_theme').".form.fields." . $fd['type'], ['field' => $fd])
            </div>
            <span class="text-danger">{{ $errors->first('thematic_id') }}</span>
        </div>
    </div>
@endif


<script>
    $(document).ready(function () {
        changeSubject();

        $('body').on('change', 'select[name=subject_id]', function () {
            changeSubject();
        });

        $('body').on('change', 'select[name=chapter_id]', function () {
            changeThematic();
        });

        function changeSubject() {
            var subject_id = $('select[name=subject_id]').val();
            $.ajax({
                url : '/admin/question/chapter/get-data',
                type: 'GET',
                data: {
                    subject_id : subject_id,
                },
                success: function (resp) {
                    var data = resp.data;
                    var html = '<select name="chapter_id" class="form-control">';
                    html += '<option value="">Chọn kỹ năng</option>';
                    @if (isset($_GET['chapter_id']))
                        var chapter_id_select =  parseInt('{{ $_GET['chapter_id'] }}');
                    @else
                        var chapter_id_select =  parseInt('{{ @$result->chapter_id }}');
                    @endif

                    Object.keys(data).map(function(key) {
                        var selected = '';
                        if (chapter_id_select == key) {
                            selected = 'selected';
                        }
                        html += '<option value="'+key+'" '+selected+'>'+data[key]+'</option>';
                    });
                    html += '</select>';
                    $('#chapter_id').html(html);
                    changeThematic();
                },
                error: function () {
                    alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại');
                }
            });
        }

        function changeThematic() {
            var chapter_id = $('select[name=chapter_id]').val();
            $.ajax({
                url : '/admin/question/thematic/get-data',
                type: 'GET',
                data: {
                    chapter_id : chapter_id,
                },
                success: function (resp) {
                    var data = resp.data;
                    var html = '<select name="thematic_id" class="form-control">';
                    html += '<option value="">Chọn mondai</option>';

                    @if (isset($_GET['chapter_id']))
                        var thematic_id_select =  parseInt('{{ @$_GET['thematic_id'] }}');
                    @else
                        var thematic_id_select =  parseInt('{{ @$result->thematic_id }}');
                    @endif

                    Object.keys(data).map(function(key) {
                        var selected = '';
                        if (thematic_id_select == key) {
                            selected = 'selected';
                        }
                        html += '<option value="'+key+'" '+selected+'>'+data[key]+'</option>';
                    });
                    html += '</select>';
                    $('#thematic_id').html(html);
                },
                error: function () {
                    alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại');
                }
            });
        }
    });
</script>