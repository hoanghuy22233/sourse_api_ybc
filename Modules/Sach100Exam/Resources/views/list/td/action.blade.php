<a href="/admin/{{ $module['code'] }}/{{ $item->id }}{{ isset($_GET['parent_id']) ? '?parent_id=' . $_GET['parent_id'] : '' }}"
   style="    font-size: 14px!important;"
   class="{{ isset($field['tooltip_info']) ? 'a-tooltip_info' : '' }}">Xem</a>

@if( (\Auth::guard('admin')->user()->id == $item->admin_id &&
        in_array($module['code'] . '_delete', $permissions)  &&
        @\Modules\Sach100Exam\Models\Category::find(@$_GET['parent_id'])->status != 0)
         || in_array('super_admin', $permissions)
         || (!isset($_GET['parent_id']) && @$item->status == 1) )
    | <a href="{{ url('/admin/'.$module['code'].'/delete/' . $item->id) }}{{ isset($_GET['parent_id']) ? '?parent_id=' . $_GET['parent_id'] : '' }}"
       style="    font-size: 14px!important;" title="Xóa bản ghi"
       class="delete-warning {{ isset($field['tooltip_info']) ? 'a-tooltip_info' : '' }}">Xóa</a>
@endif
