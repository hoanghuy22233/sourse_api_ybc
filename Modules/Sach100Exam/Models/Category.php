<?php

namespace Modules\Sach100Exam\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = 'categories';

    protected $guarded = [];

    public function parent()
    {
        return $this->hasOne($this, 'id', 'parent_id');
    }

    public function thematic()
    {
        return $this->hasMany(Category::class, 'thematic_id', 'id')->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }

    public function chapter()
    {
        return $this->hasMany(Category::class, 'chapter_id', 'id')->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }

    public function subject()
    {
        return $this->hasMany(Category::class, 'subject_id', 'id')->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }
}
