<?php

namespace Modules\Sach100Exam\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{

    protected $table = 'transaction_history';
    public $timestamps = false;
    protected $guarded = [];



}
