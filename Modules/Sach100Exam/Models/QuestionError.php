<?php

namespace Modules\Sach100Exam\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionError extends Model
{

    protected $table = 'questions_error';

    protected $guarded = [];



    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id');
    }

}
