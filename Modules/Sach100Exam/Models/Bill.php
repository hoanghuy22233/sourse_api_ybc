<?php

namespace Modules\Sach100Exam\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\ThemeCard\Models\User;
use Modules\ThemeEdu\Models\Student;

class Bill extends Model
{
    use SoftDeletes;

    protected $table = 'bills';

    protected $guarded = [];


    public function students() {
        return $this->belongsTo(Student::class, 'student_id');
    }

    public function category() {
        return $this->belongsTo(Category::class, 'subject_id');
    }

}
