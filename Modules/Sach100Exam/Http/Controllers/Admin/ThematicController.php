<?php

namespace Modules\Sach100Exam\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\EduCourse\Models\Admin;
use Modules\EduCourse\Models\Category;
use Modules\EduCourse\Models\Lesson;
use Modules\ThemeEduAdmin\Models\Menu;
use Validator;

class ThematicController extends CURDBaseController
{
    protected $orderByRaw = 'order_no desc, id desc';
    protected $module = [
        'code' => 'thematic',
        'table_name' => 'categories',
        'label' => 'Mondai',
        'modal' => '\Modules\Sach100Exam\Models\Category',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh'],
            ['name' => 'name', 'type' => 'text', 'label' => 'Tên'],
            ['name' => 'slug', 'type' => 'text', 'label' => 'Đường dẫn'],
            ['name' => 'lecturer_id', 'type' => 'relation', 'label' => 'Giáo viên', 'object' => 'admin', 'display_field' => 'name'],
            ['name' => 'order_no', 'type' => 'text', 'label' => 'STT'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],
            ['name' => 'action', 'type' => 'custom', 'td' => 'sach100exam::list.td.action', 'class' => '', 'label' => '#'],
        ],
        'form' => [
            'general_tab' => [

                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên'],
                ['name' => 'intro', 'type' => 'textarea', 'class' => '', 'label' => 'giới thiệu'],
                ['name' => 'content', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Nội dung'],
                ['name' => 'number_question', 'type' => 'number', 'class' => '', 'label' => 'Đề chuẩn - Số câu hỏi', 'group_class' => 'col-md-4'],
                ['name' => 'quick_test_number_question', 'type' => 'number', 'class' => '', 'label' => 'Test nhanh - Số câu hỏi', 'group_class' => 'col-md-4'],
            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh'],
            ],
            'info_tab' => [
                ['name' => 'status', 'type' => 'checkbox', 'class' => '', 'label' => 'Kích hoạt', 'value' => '1'],
                ['name' => 'order_no', 'type' => 'number', 'class' => '', 'label' => 'Thứ tự (Số to hiển thị trước)', 'value' => 1],
            ],

            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'Slug', 'des' => 'Đường dẫn trên thanh địa chỉ'],
                ['name' => 'meta_title', 'type' => 'custom', 'field' => 'sach100exam::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta title', 'max_char' => 1000],
                ['name' => 'meta_description', 'type' => 'custom', 'field' => 'sach100exam::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta description', 'max_char' => 1000],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, tên',
        'fields' => 'id, name'
    ];

    protected $filter = [
        'category_id' => [
            'label' => 'Danh mục',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'category',
            'model' => \Modules\Sach100Exam\Models\Category::class,
            'where' => 'type=10',
            'query_type' => 'custom'
        ],
        'parent_id' => [
            'label' => 'hidden',
            'type' => 'text',
            'query_type' => '='
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('sach100exam::thematic.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('type', 12);
        return $query;
    }

    public function add(Request $request)
    {

        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('sach100exam::thematic.add')->with($data);
            } else if ($_POST) {
//                dd($request);
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    $data['parent_id'] = $request->parent_id;
                    $data['type'] = $request->type = 12;

                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
//                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
//dd($data);
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache($this->module['table_name']);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    $url = '';
                    if ($request->has('parent_id')) {
                        $url .= '?parent_id=' . $request->parent_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('sach100exam::thematic.edit')->with($data);
        } else if ($_POST) {
//            dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ], [
                'name.required' => 'Bắt buộc phải nhập tên',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

//                dd($data);
//                  Tùy chỉnh dữ liệu insert
                if ($request->has('multi_cat')) {
                    $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                    $data['category_id'] = $request->multi_cat[0];
                }

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    CommonHelper::flushCache($this->module['table_name']);
                }

                else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                $url = '';
                if ($request->has('parent_id')) {
                    $url .= '?parent_id=' . $request->parent_id;
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id . $url);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add' . $url);
                }

                return redirect('admin/' . $this->module['code'] . $url);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            $url = '';
            if ($request->has('parent_id')) {
                $url .= '?parent_id=' . $request->parent_id;
            }
            return redirect('admin/' . $this->module['code'] . $url);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

//    public function duplicate(Request $request, $id)
//    {
//        $poduct = Lesson::find($id);
//        $poduct_new = $poduct->replicate();
//        $poduct_new->company_id = \Auth::guard('admin')->user()->last_company_id;
//        $poduct_new->admin_id = \Auth::guard('admin')->user()->id;
//        $poduct_new->save();
//        return $poduct_new;
//    }
}
