<?php

namespace Modules\Sach100Exam\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\Sach100Exam\Models\Category;
use Modules\Sach100Exam\Models\Question;
use Validator;

class QuestionErrorController extends CURDBaseController
{
    protected $module = [
        'code' => 'question_error',
        'table_name' => 'questions_error',
        'label' => 'Câu hỏi',
        'modal' => '\Modules\Sach100Exam\Models\QuestionError',
        'list' => [
            ['name' => 'code', 'type' => 'text', 'label' => 'Mã câu hỏi'],
            ['name' => 'reason', 'type' => 'text', 'label' => 'Loại'],
            ['name' => 'content', 'type' => 'text', 'label' => 'Lời nhắn'],
        ],
        'form' => [
            'general_tab' => [

            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID',
        'fields' => 'id, reason, content'
    ];

    protected $filter = [
        /*'subject_id' => [
            'label' => 'Trình độ',
            'type' => 'select2_model',
            'display_field' => 'name',
            'object' => 'category_post',
            'model' => \Modules\Sach100Exam\Models\Category::class,
            'where' => 'type=10',
            'query_type' => '='
        ],
        'chapter_id' => [
            'label' => 'Kỹ năng',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'category_post',
            'model' => \Modules\Sach100Exam\Models\Category::class,
            'where' => 'type=11',
            'query_type' => '='
        ],
        'thematic_id' => [
            'label' => 'Mondai',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'category_post',
            'model' => \Modules\Sach100Exam\Models\Category::class,
            'where' => 'type=12',
            'query_type' => '='
        ],*/
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('sach100exam::question.list')->with($data);
    }

    public function add(Request $request)
    {

        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('sach100exam::question.add')->with($data);
            } else if ($_POST) {

                $validator = Validator::make($request->all(), [
//                    'question' => 'required',
//                    'answer' => 'required',
//                    'level' => 'required',

                ], [
//                    'question.required' => 'Bắt buộc phải nhập câu hỏi',
//                    'answer.required' => 'Bắt buộc phải nhập đáp án chính xác',
//                    'level.required' => 'Bắt buộc phải chọn độ khó',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache($this->module['table_name']);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('sach100exam::question.edit')->with($data);
        } else if ($_POST) {

            $validator = Validator::make($request->all(), [
//                'question' => 'required',
//                'answer' => 'required',
//                'level' => 'required',
            ], [
//                'question.required' => 'Bắt buộc phải nhập câu hỏi',
//                'answer.required' => 'Bắt buộc phải nhập đáp án chính xác',
//                'level.required' => 'Bắt buộc phải chọn độ khó',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }

                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    CommonHelper::flushCache($this->module['table_name']);
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
