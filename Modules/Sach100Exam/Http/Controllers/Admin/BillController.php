<?php

namespace Modules\Sach100Exam\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\Sach100Exam\Models\Bill;
use App\Models\User;
use Modules\EduBill\Models\Order;
use Modules\Sach100Exam\Models\Category;
use Modules\Sach100Exam\Models\Student;
use Validator;

class BillController extends CURDBaseController
{
    protected $module = [
        'code' => 'bill',
        'table_name' => 'bills',
        'label' => 'Đơn hàng',
        'modal' => '\Modules\Sach100Exam\Models\Bill',
        'list' => [
            ['name' => 'student_id', 'type' => 'relation_edit', 'label' => 'Khách hàng', 'object' => 'students', 'display_field' => 'name'],
            ['name' => 'student_id', 'type' => 'custom', 'td' => 'sach100exam::list.td.relation', 'label' => 'SĐT', 'object' => 'students', 'display_field' => 'phone'],
            ['name' => 'student_id', 'type' => 'custom', 'td' => 'sach100exam::list.td.relation', 'label' => 'Email', 'object' => 'students', 'display_field' => 'email'],
//            ['name' => 'student_id', 'type' => 'custom', 'td' => 'sach100exam::list.td.relation', 'label' => 'Địa chỉ', 'object' => 'students', 'display_field' => 'address'],
            ['name' => 'total_price', 'type' => 'price_vi', 'label' => 'Tổng tiền'],
            ['name' => 'subjects', 'type' => 'custom', 'td' => 'sach100exam::list.td.multi_subject', 'label' => 'Các Trình độ đã mua', 'object' => 'subject', 'display_field' => 'name'],
            ['name' => 'created_at', 'type' => 'text', 'label' => 'Ngày đặt'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],
//            ['name' => 'paid', 'type' => 'status', 'label' => 'Thanh toán'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'total_price', 'type' => 'text', 'class' => 'number_price', 'label' => 'Tổng tiền'],
//                ['name' => 'paid', 'type' => 'checkbox', 'class' => '', 'label' => 'Đã thanh toán'],
                ['name' => 'status', 'type' => 'select', 'options' => [
                    0 => 'Chời kích hoạt',
                    1 => 'Kích hoạt',
                ], 'label' => 'Trạng thái'],
            ],
            'info_tab' => [
                ['name' => 'user_name', 'type' => 'text', 'label' => 'Họ tên'],
                ['name' => 'user_email', 'type' => 'text', 'label' => 'Email'],
                ['name' => 'user_tel', 'type' => 'text', 'label' => 'Số điện thoại'],
                ['name' => 'password', 'type' => 'text', 'label' => 'Mật khẩu'],
                ['name' => 're_password', 'type' => 'text', 'label' => 'Nhập lại mật khẩu'],
            ],
        ],
    ];

    protected $filter = [
        'student_id' => [
            'label' => 'Tên khách hàng',
            'type' => 'select2_model',
            'display_field' => 'name',
            'model' => Student::class,
            'object' => 'student',
            'query_type' => '='
        ],
        'total_price' => [
            'label' => 'Tổng tiền',
            'type' => 'number',
            'query_type' => 'like'
        ],
        'created_at' => [
            'label' => 'Ngày đặt',
            'type' => 'date',
            'query_type' => 'custom'
        ],
//        'status' => [
//            'label' => 'Trạng thái',
//            'type' => 'select',
//            'query_type' => '=',
//            'options' => [
//                '' => 'Trạng thái',
//                0 => 'Không kích hoạt',
//                1 => 'Kích hoạt',
//            ],
//        ],

    ];

    protected $quick_search = [
        'label' => 'ID, tên khách, sđt, địa chỉ, email',
        'fields' => 'id, user_name, user_tel, user_address, user_email'
    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);
        return view('sach100exam::bill.list')->with($data);
    }

    public function add(Request $request)
    {
        try {

            if (!$_POST) {
                $data1['user_id'] = $request->user_id;
                $data = $this->getDataAdd($request);
                return view('sach100exam::bill.add', $data1)->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
//                    'name' => 'required'
                ], [
//                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert

                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($request->total_price <= User::where('id', $request->user_id)->first()->total_money) {
                        if ($this->model->save()) {
                            CommonHelper::flushCache($this->module['table_name']);
                            $this->afterAddLog($request, $this->model);

                            CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                        } else {
                            CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                        }
                    } else {
                        CommonHelper::one_time_message('error', 'Tông tiền của bạn không đủ để thực hiện giao dịch này.Vui lòng nạp thêm tiền vào thẻ để tiếp tục!');
                        return back();
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('sach100exam::bill.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
//                    'name' => 'required'
                ], [
//                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    if ($data['status'] == 1 && $item->status == 0) {
                        $this->updateStatusOrder($item->id, 1);
                    }

                    //  Tùy chỉnh dữ liệu insert
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {
            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0) {
                $item->{$request->column} = 1;
                $this->updateStatusOrder($item->id, 1);
            }
            else {
                $item->{$request->column} = 0;
                $this->updateStatusOrder($item->id, 0);
            }

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false,
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function updateStatusOrder($bill_id, $status) {
        if ($status == 1) {
            $bill = Bill::find($bill_id);
            $student = Student::find($bill->student_id);

            //  Thêm Trình độ vào danh sách Trình độ đã mua
            if (strpos($student->subjects, '|' . $bill->subjects .'|') === false) {
                $student->subjects .= '|' . $bill->subjects .'|';
            }

            //  Xóa 2 ký tự || liền nhau
            $student->subjects = preg_replace('/\|\|/', '|', $student->subjects);

            $student->save();
        }

        return true;
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function activeOrder(Request $request)
    {
//        $settings = Setting::whereIn('type', ['general_tab', 'mail'])->pluck('value', 'name')->toArray();
        try {
            Order::findOrFail($request->order_id)->update(['status' => ($request->status == 0) ? 1 : 0]);
//           gửi mail
//            Mail::send(['html' => 'themeedu::mail.order_mail_user'], compact('bill', 'pass'), function ($message) use ($settings, $request) {
//                $message->from($settings['smtp_username'], $settings['mail_name']);
//                $message->to(Auth::guard('student')->check() ? Auth::guard('student')->user()->email : $request->email, Auth::guard('student')->check() ? Auth::guard('student')->user()->name : $request->name);
//                $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] Đơn hàng');
//            });
            return response()->json([
                'status' => true,
                'msg' => 'Cập nhật thành công!'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Không thành công!'
            ]);
        }

    }

    public function activeOrderAll(Request $request)
    {
        try {
            $this->updateStatusOrder($request->bill_id, 1);

            Bill::where('id', $request->bill_id)->update([
                'status' => 1
            ]);

            return response()->json([
                'status' => true,
                'msg' => 'Cập nhật thành công!'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Thất bại! ' . $ex->getMessage()
            ]);
        }

    }

    public function exportExcel($request, $data)
    {
        \Excel::create(str_slug($this->module['label'], '_') . '_' . date('d_m_Y'), function ($excel) use ($data) {

            // Set the title
            $excel->setTitle($this->module['label'] . ' ' . date('d m Y'));

            $excel->sheet(str_slug($this->module['label'], '_') . '_' . date('d_m_Y'), function ($sheet) use ($data) {

                $field_name = ['ID'];
                $field_name[] = 'Trình độ';
                foreach ($this->getAllFormFiled() as $field) {
                    if (!isset($field['no_export']) && isset($field['label'])) {
                        $field_name[] = $field['label'];
                    }
                }
                $field_name[] = 'Tạo lúc';
                $field_name[] = 'Cập nhập lần cuối';

                $sheet->row(1, $field_name);

                $k = 2;
                foreach ($data as $value) {
                    $data_export = [];
                    $data_export[] = $value->id;
                    $subects = Category::whereIn('id', explode('|', $value->subjects))->pluck('name');
                    $str = '';
                    foreach ($subects as $v) {
                        $str .= $v . ',';
                    }
                    $data_export[] = $str;

                    foreach ($this->getAllFormFiled() as $field) {
                        if (!isset($field['no_export']) && isset($field['label'])) {
                            try {
                                if (in_array($field['type'], ['text', 'number', 'textarea', 'textarea_editor', 'date', 'datetime-local', 'email', 'hidden', 'checkbox', 'textarea_editor', 'textarea_editor2'])) {
                                    $data_export[] = $value->{$field['name']};
                                } elseif (in_array($field['type'], [
                                    'relation', 'select_model', 'select2_model', 'select2_ajax_model', 'select_model_tree',

                                ])) {
                                    $data_export[] = @$value->{$field['object']}->{$field['display_field']};
                                } elseif ($field['type'] == 'select') {
                                    $data_export[] = @$field['options'][$value->{$field['name']}];
                                } elseif (in_array($field['type'], ['file', 'file_editor2'])) {
                                    $data_export[] = \URL::asset('public/filemanager/userfiles/' . @$value->{$field['name']});
                                } elseif (in_array($field['type'], ['file_editor_extra'])) {
                                    $items = explode('|', @$value->{$field['name']});
                                    foreach ($items as $item) {
                                        $data_export[] = \URL::asset('public/filemanager/userfiles/' . @$item) . ' | ';
                                    }
                                } else {
                                    $data_export[] = $field['label'];
                                }
                            } catch (\Exception $ex) {
                                $data_export[] = $ex->getMessage();
                            }
                        }
                    }
                    $data_export[] = @$value->created_at;
                    $data_export[] = @$value->updated_at;
                    $sheet->row($k, $data_export);
                    $k++;
                }
            });
        })->download('xls');
    }
}
