<?php

namespace Modules\Sach100Exam\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\Sach100Exam\Models\Category;
use Modules\Sach100Exam\Models\Question;
use Validator;

class QuestionController extends CURDBaseController
{
    protected $whereRaw = 'parent_id is NULL';

    protected $module = [
        'code' => 'question',
        'table_name' => 'questions',
        'label' => 'Câu hỏi',
        'modal' => '\Modules\Sach100Exam\Models\Question',
        'list' => [
            ['name' => 'question', 'type' => 'text_edit', 'label' => 'Câu hỏi'],
//            ['name' => 'a', 'type' => 'text', 'label' => 'A'],
//            ['name' => 'b', 'type' => 'text', 'label' => 'B'],
//            ['name' => 'c', 'type' => 'text', 'label' => 'C'],
//            ['name' => 'd', 'type' => 'text', 'label' => 'D'],
//            ['name' => 'answer', 'type' => 'text', 'label' => 'Câu trả lời'],
            ['name' => 'subject_id', 'type' => 'relation', 'label' => 'Trình độ', 'object' => 'subject', 'display_field' => 'name'],
            ['name' => 'chapter_id', 'type' => 'relation', 'label' => 'Kỹ năng', 'object' => 'chapter', 'display_field' => 'name'],
            ['name' => 'thematic_id', 'type' => 'relation', 'label' => 'Mondai', 'object' => 'thematic', 'display_field' => 'name'],
            ['name' => 'level', 'type' => 'select', 'label' => 'Độ khó', 'options' => [
                'de' => 'Cơ bản',
                'tb' => 'Nghiệp dư',
                'kho' => 'Master',
            ],],
            ['name' => 'type', 'type' => 'text', 'label' => 'Loại đề'],
            ['name' => 'code', 'type' => 'text', 'label' => 'Mã'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'question', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Câu hỏi'],
                ['name' => 'code', 'type' => 'text', 'class' => '', 'label' => 'Mã'],
                ['name' => 'file_audio', 'type' => 'file', 'label' => 'File Audio'],
                ['name' => 'a', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Đáp án A'],
                ['name' => 'b', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Đáp án B'],
                ['name' => 'c', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Đáp án C'],
                ['name' => 'd', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Đáp án D'],
                ['name' => 'answer', 'type' => 'radio', 'options' => [
                    'a' => 'A',
                    'b' => 'B',
                    'c' => 'C',
                    'd' => 'D',
                ], 'class' => '', 'label' => 'Đán án chính xác'],
                ['name' => 'answer_detail', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Chi tiết câu trả lời'],
                ['name' => 'status', 'type' => 'checkbox', 'class' => '', 'label' => 'Kích hoạt', 'value' => '1'],
//                ['name' => 'multi_cat', 'type' => 'custom', 'field' => 'sach100exam::form.fields.multi_cat', 'label' => 'Danh mục cha', 'model' => Category::class,
//                    'object' => 'category', 'display_field' => 'name', 'multiple' => true,],
            ],

            'info_tab' => [
                ['name' => 'point', 'type' => 'number', 'class' => '', 'label' => 'Điểm số', 'value' => 1],
                ['name' => 'level', 'type' => 'radio', 'options' => [
                    'de' => 'Cơ bản',
                    'tb' => 'Nghiệp dư',
                    'kho' => 'Master',
                ], 'class' => '', 'label' => 'Độ khó'],
                ['name' => 'type', 'type' => 'radio', 'options' => [
                    'Đề chuẩn JLPT' => 'Đề chuẩn JLPT',
                    'Đề Test nhanh' => 'Đề Test nhanh',
                ], 'class' => '', 'label' => 'Loại đề'],
                ['name' => 'subject_id', 'type' => 'custom', 'field' => 'sach100exam::form.fields.select_subject_chapter_thematic', 'class' => '', 'label' => ''],

//                ['name' => 'subject_id', 'type' => 'select2_model', 'label' => 'Trình độ', 'model' => Category::class, 'object' => 'subject', 'display_field' => 'name','where' => 'type=10'],
//                ['name' => 'chapter_id', 'type' => 'select2_model', 'label' => 'Chuơng', 'model' => Category::class, 'object' => 'chapter', 'display_field' => 'name','where' => 'type=11'],
//                ['name' => 'thematic_id', 'type' => 'select2_model', 'label' => 'Mondai', 'model' => Category::class, 'object' => 'thematic', 'display_field' => 'name','where' => 'type=12'],
            ],
            'question_childs_tab' => [
                ['name' => 'iframe', 'type' => 'iframe', 'class' => 'col-xs-12 col-md-8 padding-left', 'src' => '/admin/question_item?parent_id={id}', 'inner' => 'style="min-height: 1000px;"'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, câu hỏi, câu trả lời, mã',
        'fields' => 'id, question, a, b, c, d, answer_detail, code'
    ];

    protected $filter = [
        /*'subject_id' => [
            'label' => 'Trình độ',
            'type' => 'select2_model',
            'display_field' => 'name',
            'object' => 'category_post',
            'model' => \Modules\Sach100Exam\Models\Category::class,
            'where' => 'type=10',
            'query_type' => '='
        ],
        'chapter_id' => [
            'label' => 'Kỹ năng',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'category_post',
            'model' => \Modules\Sach100Exam\Models\Category::class,
            'where' => 'type=11',
            'query_type' => '='
        ],
        'thematic_id' => [
            'label' => 'Mondai',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'category_post',
            'model' => \Modules\Sach100Exam\Models\Category::class,
            'where' => 'type=12',
            'query_type' => '='
        ],*/

        'level' => [
            'label' => 'Độ khó',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Tất cả',
                'de' => 'Cơ bản',
                'tb' => 'Nghiệp dư',
                'kho' => 'Master',
            ]
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('sach100exam::question.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        if (!is_null($request->get('subject_id'))) {
            $query = $query->where('subject_id', $request->subject_id);
        }
        if (!is_null($request->get('chapter_id'))) {
            $query = $query->where('chapter_id', $request->chapter_id);
        }
        if (!is_null($request->get('thematic_id'))) {
            $query = $query->where('thematic_id', $request->thematic_id);
        }

        return $query;
    }

    public function add(Request $request)
    {

        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('sach100exam::question.add')->with($data);
            } else if ($_POST) {

                $validator = Validator::make($request->all(), [
//                    'question' => 'required',
//                    'answer' => 'required',
//                    'level' => 'required',

                ], [
//                    'question.required' => 'Bắt buộc phải nhập câu hỏi',
//                    'answer.required' => 'Bắt buộc phải nhập đáp án chính xác',
//                    'level.required' => 'Bắt buộc phải chọn độ khó',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert


                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['multi_cat'] = $request->multi_cat[0];
                    }
                    #
                    $data['chapter_id'] = $request->chapter_id;
                    $data['thematic_id'] = $request->thematic_id;

                    if ($request->file('file_audio') != null) {
                        $data['file_audio'] = CommonHelper::saveFile($request->file('file_audio'), $this->module['code']);
                    }

                    unset($data['iframe']);

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    //  Kiểm tra câu trùng và không lưu câu trùng
                    if (Question::where('question', @$data['question'])->where('subject_id', @$data['subject_id'])->where('chapter_id', @$data['chapter_id'])->where('thematic_id', @$data['thematic_id'])
                            ->where('a', @$data['a'])->where('b', @$data['b'])->where('c', @$data['c'])->where('d', @$data['d'])->count() > 0) {
                        CommonHelper::one_time_message('error', 'Lỗi trùng lặp! Câu hỏi này đã được nhập từ trước');
                        return back()->withErrors($validator)->withInput();
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache($this->module['table_name']);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('sach100exam::question.edit')->with($data);
        } else if ($_POST) {

            $validator = Validator::make($request->all(), [
//                'question' => 'required',
//                'answer' => 'required',
//                'level' => 'required',
            ], [
//                'question.required' => 'Bắt buộc phải nhập câu hỏi',
//                'answer.required' => 'Bắt buộc phải nhập đáp án chính xác',
//                'level.required' => 'Bắt buộc phải chọn độ khó',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

//                  Tùy chỉnh dữ liệu insert
                if ($request->has('multi_cat')) {
                    $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                    $data['multi_cat'] = $request->multi_cat[0];
                }

                $data['chapter_id'] = $request->chapter_id;
                $data['thematic_id'] = $request->thematic_id;

                if ($request->file('file_audio') != null) {
                    $data['file_audio'] = CommonHelper::saveFile($request->file('file_audio'), $this->module['code']);
                } else {
                    unset($data['file_audio']);
                }
                unset($data['iframe']);
                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }

                //  Kiểm tra câu trùng và không lưu câu trùng
                if (Question::where('question', @$data['question'])->where('id', '!=', $item->id)->where('subject_id', @$data['subject_id'])->where('chapter_id', @$data['chapter_id'])->where('thematic_id', @$data['thematic_id'])
                        ->where('a', @$data['a'])->where('b', @$data['b'])->where('c', @$data['c'])->where('d', @$data['d'])->count() > 0) {
                    CommonHelper::one_time_message('error', 'Lỗi trùng lặp! Câu hỏi này đã được nhập từ trước');
                    return back()->withErrors($validator)->withInput();
                }

                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    CommonHelper::flushCache($this->module['table_name']);
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function getDataLocation(Request $r, $table)
    {
        if ($table == 'chapter') {
            $items = Category::where('parent_id', $r->subject_id)->where('parent_id', '!=', 0)->where('type', 11)->pluck('name', 'id');
        } elseif ($table == 'thematic') {
            $items = Category::where('parent_id', $r->chapter_id)->where('parent_id', '!=', 0)->where('type', 12)->pluck('name', 'id');
        }
        return response()->json([
            'status' => true,
            'msg' => '',
            'data' => $items
        ]);
    }

    /**
     * Tối đa import được 999 dòng
     */
    public function importExcel(Request $r)
    {
        $table_import = $r->has('table') ? $r->table : $this->module['table_name'];
        $validator = Validator::make($r->all(), [
            'module' => 'required',
        ], [
            'module.required' => 'Bắt buộc phải nhập module!',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            $importController = new \App\Http\Controllers\Admin\ImportController();
            $data = $this->processingValueInFields($r, $importController->getAllFormFiled());
            //  Tùy chỉnh dữ liệu insert

            if ($r->has('file')) {
                $file_name = $r->file('file')->getClientOriginalName();
                $file_name = str_replace(' ', '', $file_name);
                $file_name_insert = date('s_i_') . $file_name;
                $r->file('file')->move(base_path() . '/public/filemanager/userfiles/imports/', $file_name_insert);
                $data['file'] = 'imports/' . $file_name_insert;
            }

            unset($data['field_options_key']);
            unset($data['field_options_value']);
            #
            $item = new \App\Models\Import();
            foreach ($data as $k => $v) {
                $item->$k = $v;
            }

            if ($item->save()) {
                //  Import dữ liệu vào
                $this->updateAttributes($r, $item);

                $this->processingImport($r, $item);

                CommonHelper::flushCache($table_import);
                CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                return redirect('/admin/import');
            } else {
                CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
            }

            if ($r->ajax()) {
                return response()->json([
                    'status' => true,
                    'msg' => '',
                    'data' => $item
                ]);
            }

            return redirect('/admin/import');
        }
    }

    public function updateAttributes($r, $item)
    {
        $table_import = $r->has('table') ? $r->table : $this->module['table_name'];
        if ($r->has('field_options_key')) {
            $key_update = [];
            foreach ($r->field_options_key as $k => $key) {
                if ($key != null && $r->field_options_value[$k] != null) {
                    $key_update[] = $key;
                    \App\Models\Attribute::updateOrCreate([
                        'key' => $key,
                        'table' => $table_import,
                        'type' => 'field_options',
                        'item_id' => $item->id
                    ], [
                        'value' => $r->field_options_value[$k]
                    ]);
                }
            }
            if (!empty($key_update)) {
                \App\Models\Attribute::where([
                    'table' => $table_import,
                    'type' => 'field_options',
                    'item_id' => $item->id
                ])->whereNotIn('key', $key_update)->delete();
            }
        } else {
            \App\Models\Attribute::where([
                'table' => $table_import,
                'type' => 'field_options',
                'item_id' => $item->id
            ])->delete();
        }
        return true;
    }

    public function processingImport($r, $item)
    {
        $table_import = $r->has('table') ? $r->table : $this->module['table_name'];
        $record_total = $record_success = 0;
        $dataInsertFix = \App\Models\Attribute::where('table', $table_import)->where('type', 'field_options')->where('item_id', @$item->id)->pluck('value', 'key')->toArray();
        \Excel::load('public/filemanager/userfiles/' . $item->file, function ($reader) use ($r, $dataInsertFix, &$model, &$record_total, &$record_success) {
            $reader->each(function ($sheet) use ($r, $reader, $dataInsertFix, &$model, &$record_total, &$record_success) {
                if ($reader->getSheetCount() == 1) {
                    $result = $this->importItem($sheet, $r, $dataInsertFix);
                    if ($result['status']) {
                        $record_total++;
                    }
                    if ($result['import']) {
                        $record_success++;
                    }
                } else {
                    $sheet->each(function ($row) use ($r, $dataInsertFix, &$model, &$record_total, &$record_success) {
                        $result = $this->importItem($row, $r, $dataInsertFix);
                        if ($result['status']) {
                            $record_total++;
                        }
                        if ($result['import']) {
                            $record_success++;
                        }
                    });
                }
            });
        });
        $item->record_total = $record_total;
        $item->record_success = $record_total;
        $item->save();
        return true;
    }

    //  Xử lý import 1 dòng excel
    public function importItem($row, $r, $dataInsertFix)
    {
        //  Nếu dd() trong này mà không chạy chứng tỏ file excel bị sai định dạng, sửa nó trên google driver rồi import lại


        try {
            //  Kiểm tra trường dữ liêu bắt buộc có
            $fields_require = ['trinh_do'];
            foreach ($fields_require as $field_require) {
                if (!isset($row->{$field_require}) || $row->{$field_require} == '' || $row->{$field_require} == null) {
                    return false;
                }
            }

            $row_empty = true;
            foreach ($row->all() as $key => $value) {
                if ($value != null) {
                    $row_empty = false;
                }
            }

            //  Các trường không được trùng
            /*$item_model = new $this->module['modal'];
            $item = $item_model->where('tel', $row->all()['tel'])->first();
            if (!is_object($item)) {
                $item = $item_model;
            }*/

            /*if ($this->import[$request->module]['unique']) {
                $field_name = $this->import[$request->module]['fields'][$this->import[$request->module]['unique']];
                $model_new = new $this->import[$request->module]['modal'];
                $model = $model_new->where($field_name, $row->{$this->import[$request->module]['unique']})->first();
            }*/

            if (!$row_empty) {
                $data = [];

                //  Gán các dữ liệu được fix cứng từ view
                foreach ($dataInsertFix as $k => $v) {
                    $data[$k] = $v;
                }

                //  Set mặc định import là bảo hành Meeg
//                $data['bonus_services'] = 2;

                //  Chèn các dữ liệu lấy vào từ excel
                foreach ($row->all() as $key => $value) {
                    switch ($key) {
                        case 'trinh_do':
                            {
                                $mon = Category::where('name', $value)->where('type', 10)->first();
                                $data['subject_id'] = @$mon->id;
                                break;
                            }
                        //  Trong file excel phải để thứ tự các cột theo đúng trình tự là trinh_do > ky_nang > mondai thì mới lấy được dữ liệu
                        case 'ky_nang':
                            {
                                $chuong = Category::where('name', $value)->where('parent_id', @$mon->id)->where('type', 11)->first();
                                $data['chapter_id'] = @$chuong->id;
                                break;
                            }
                        case 'mondai':
                            {
                                $chuyen_de = Category::where('name', $value)->where('parent_id', @$chuong->id)->where('type', 12)->first();
                                $data['thematic_id'] = @$chuyen_de->id;
                                break;
                            }
                        case 'cau_hoi':
                            {
                                $data['question'] = $value;
                                break;
                            }
                        case 'dap_an_a':
                            {
                                $data['a'] = $value;
                                break;
                            }
                        case 'dap_an_b':
                            {
                                $data['b'] = $value;
                                break;
                            }
                        case 'dap_an_c':
                            {
                                $data['c'] = $value;
                                break;
                            }
                        case 'dap_an_d':
                            {
                                $data['d'] = $value;
                                break;
                            }
                        case 'dap_an_chinh_xac':
                            {
                                if ($value == 1) {
                                    $value = 'a';
                                } elseif ($value == 2) {
                                    $value = 'b';
                                } elseif ($value == 3) {
                                    $value = 'c';
                                } elseif ($value == 4) {
                                    $value = 'd';
                                }
                                $data['answer'] = strtolower($value);
                                break;
                            }
                        case 'cau_tra_loi_chi_tiet':
                            {
                                $value = preg_replace('/\r?\n|\r/', '<br/>', $value);
                                $data['answer_detail'] = $value;
                                break;
                            }
                        case 'diem':
                            {
                                $data['point'] = $value;
                                break;
                            }
                        case 'ma':
                            {
                                $data['code'] = $value;
                                break;
                            }
                        case 'loai_de':
                            {
                                $data['type'] = $value;
                                break;
                            }
                        case 'muc_do': {
                            if ($value == 'Cơ bản') {
                                $value = 'de';
                            } elseif ($value == 'Nghiệp dư') {
                                $value = 'tb';
                            } elseif ($value == 'Master') {
                                $value = 'kho';
                            }

                            $value = str_slug($value, '-');
                            if ($value == 'trung-binh') {
                                $value = 'tb';
                            }
                            $data['level'] = $value;
                            break;
                        }
                        case 'loai_cau': {
                            if ($value == 'con') {
                                //  Câu cha ngay ở trên
                                $question_parent = Question::whereNull('parent_id')->orderBy('id', 'desc')->first();
                                $data['parent_id'] = @$question_parent->id;
                            } elseif($value != null && $value != '') {
                                //  Tìm câu cha theo mã
                                $question_parent = Question::where('code', $value)->first();
                                $data['parent_id'] = @$question_parent->id;
                            }
                            break;
                        }
                        default:
                            {
                                if (\Schema::hasColumn($r->table, $key)) {
                                    $data[$key] = $value;
                                }
                            }
                    }
                }

                //  Lấy lại trình độ
                if (isset($row->all()['trinh_do'])) {
                    $mon = Category::where('name', $row->all()['trinh_do'])->where('type', 10)->first();
                    $data['subject_id'] = @$mon->id;
                }
//                dd($data);

                if(isset($data['code']) && $data['code'] != '' && $data['code'] != null) {
                    //  Kiểm tra câu trùng và không lưu câu trùng
                    if (Question::where('code', @$data['code'])->count() > 0) {
                        return [
                            'status' => true,
                            'import' => false
                        ];
                    }
                }

                if (\DB::table($r->table)->insert($data)) {
                    return [
                        'status' => true,
                        'import' => true
                    ];
                }
            }
        } catch (\Exception $ex) {
            dd($ex->getMessage());
            return [
                'status' => true,
                'import' => false,
                'msg' => $ex->getMessage()
            ];
        }
    }
}
