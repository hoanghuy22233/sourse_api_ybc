<?php

namespace Modules\Sach100Exam\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\Sach100Exam\Models\Category;
use Modules\Sach100Exam\Models\Question;
use Validator;

class QuestionItemController extends CURDBaseController
{

    protected $module = [
        'code' => 'question_item',
        'table_name' => 'questions',
        'label' => 'Câu hỏi con',
        'modal' => '\Modules\Sach100Exam\Models\Question',
        'list' => [
            ['name' => 'question', 'type' => 'text_edit', 'label' => 'Câu hỏi'],
//            ['name' => 'a', 'type' => 'text', 'label' => 'A'],
//            ['name' => 'b', 'type' => 'text', 'label' => 'B'],
//            ['name' => 'c', 'type' => 'text', 'label' => 'C'],
//            ['name' => 'd', 'type' => 'text', 'label' => 'D'],
//            ['name' => 'answer', 'type' => 'text', 'label' => 'Câu trả lời'],
            ['name' => 'subject_id', 'type' => 'relation', 'label' => 'Trình độ', 'object' => 'subject', 'display_field' => 'name'],
            ['name' => 'chapter_id', 'type' => 'relation', 'label' => 'Kỹ năng', 'object' => 'chapter', 'display_field' => 'name'],
            ['name' => 'thematic_id', 'type' => 'relation', 'label' => 'Mondai', 'object' => 'thematic', 'display_field' => 'name'],
            ['name' => 'level', 'type' => 'select', 'label' => 'Độ khó', 'options' => [
                'de' => 'Cơ bản',
                'tb' => 'Nghiệp dư',
                'kho' => 'Master',
            ],],
            ['name' => 'type', 'type' => 'text', 'label' => 'Loại đề'],
            ['name' => 'code', 'type' => 'text', 'label' => 'Mã'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'question', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Câu hỏi'],
                ['name' => 'code', 'type' => 'text', 'class' => '', 'label' => 'Mã'],
                ['name' => 'file_audio', 'type' => 'file', 'label' => 'File Audio'],
                ['name' => 'a', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Đáp án A'],
                ['name' => 'b', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Đáp án B'],
                ['name' => 'c', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Đáp án C'],
                ['name' => 'd', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Đáp án D'],
                ['name' => 'answer', 'type' => 'radio', 'options' => [
                    'a' => 'A',
                    'b' => 'B',
                    'c' => 'C',
                    'd' => 'D',
                ], 'class' => '', 'label' => 'Đán án chính xác'],
                ['name' => 'answer_detail', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Chi tiết câu trả lời'],
                ['name' => 'status', 'type' => 'checkbox', 'class' => '', 'label' => 'Kích hoạt', 'value' => '1'],
//                ['name' => 'multi_cat', 'type' => 'custom', 'field' => 'sach100exam::form.fields.multi_cat', 'label' => 'Danh mục cha', 'model' => Category::class,
//                    'object' => 'category', 'display_field' => 'name', 'multiple' => true,],
            ],

            'info_tab' => [
                ['name' => 'point', 'type' => 'number', 'class' => '', 'label' => 'Điểm số', ],
                ['name' => 'level', 'type' => 'radio', 'options' => [
                    'de' => 'Cơ bản',
                    'tb' => 'Nghiệp dư',
                    'kho' => 'Master',
                ], 'class' => '', 'label' => 'Độ khó'],
                ['name' => 'type', 'type' => 'radio', 'options' => [
                    'Đề chuẩn JLPT' => 'Đề chuẩn JLPT',
                    'Đề Test nhanh' => 'Đề Test nhanh',
                ], 'class' => '', 'label' => 'Loại đề'],
                ['name' => 'subject_id', 'type' => 'custom', 'field' => 'sach100exam::form.fields.select_subject_chapter_thematic', 'class' => '', 'label' => ''],

//                ['name' => 'subject_id', 'type' => 'select2_model', 'label' => 'Trình độ', 'model' => Category::class, 'object' => 'subject', 'display_field' => 'name','where' => 'type=10'],
//                ['name' => 'chapter_id', 'type' => 'select2_model', 'label' => 'Chuơng', 'model' => Category::class, 'object' => 'chapter', 'display_field' => 'name','where' => 'type=11'],
//                ['name' => 'thematic_id', 'type' => 'select2_model', 'label' => 'Mondai', 'model' => Category::class, 'object' => 'thematic', 'display_field' => 'name','where' => 'type=12'],
            ],
        ],
    ];

    protected $filter = [
        'level' => [
            'label' => 'Độ khó',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Tất cả',
                'de' => 'Cơ bản',
                'tb' => 'Nghiệp dư',
                'kho' => 'Master',
            ]
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, câu hỏi, câu trả lời',
        'fields' => 'id, question, a, b, c, d, answer_detail'
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('sach100exam::question_item.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('parent_id', $request->parent_id);

        if (!is_null($request->get('subject_id'))) {
            $query = $query->where('subject_id', $request->subject_id);
        }
        if (!is_null($request->get('chapter_id'))) {
            $query = $query->where('chapter_id', $request->chapter_id);
        }
        if (!is_null($request->get('thematic_id'))) {
            $query = $query->where('thematic_id', $request->thematic_id);
        }

        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('sach100exam::question_item.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
//                    'question' => 'required',
//                    'answer' => 'required',
//                    'level' => 'required',

                ], [
//                    'question.required' => 'Bắt buộc phải nhập câu hỏi',
//                    'answer.required' => 'Bắt buộc phải nhập đáp án chính xác',
//                    'level.required' => 'Bắt buộc phải chọn độ khó',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    $data['parent_id'] = $request->parent_id;
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['multi_cat'] = $request->multi_cat[0];
                    }
                    #
                    $data['chapter_id'] = $request->chapter_id;
                    $data['thematic_id'] = $request->thematic_id;

                    if ($request->file('file_audio') != null) {
                        $data['file_audio'] = CommonHelper::saveFile($request->file('file_audio'), $this->module['code']);
                    }
//dd($data);
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    //  Kiểm tra câu trùng và không lưu câu trùng
                    if (Question::where('question', @$data['question'])->where('subject_id', @$data['subject_id'])->where('chapter_id', @$data['chapter_id'])->where('thematic_id', @$data['thematic_id'])
                            ->where('a', @$data['a'])->where('b', @$data['b'])->where('c', @$data['c'])->where('d', @$data['d'])->count() > 0) {
                        CommonHelper::one_time_message('error', 'Lỗi trùng lặp! Câu hỏi này đã được nhập từ trước');
                        return back()->withErrors($validator)->withInput();
                    }

                    if ($this->model->save()) {

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }
                    $url = '';
                    if ($request->has('parent_id')) {
                        $url .= '?parent_id=' . $request->parent_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);


        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('sach100exam::question_item.edit')->with($data);
        } else if ($_POST) {
            $validator = Validator::make($request->all(), [
//                'question' => 'required',
//                'answer' => 'required',
//                'level' => 'required',
            ], [
//                'question.required' => 'Bắt buộc phải nhập câu hỏi',
//                'answer.required' => 'Bắt buộc phải nhập đáp án chính xác',
//                'level.required' => 'Bắt buộc phải chọn độ khó',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert

                $data['parent_id'] = $request->parent_id;
                if ($request->has('multi_cat')) {
                    $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                    $data['multi_cat'] = $request->multi_cat[0];
                }

                $data['chapter_id'] = $request->chapter_id;
                $data['thematic_id'] = $request->thematic_id;

                if ($request->file('file_audio') != null) {
                    $data['file_audio'] = CommonHelper::saveFile($request->file('file_audio'), $this->module['code']);
                } else {
                    unset($data['file_audio']);
                }

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }

                //  Kiểm tra câu trùng và không lưu câu trùng
                if (Question::where('question', @$data['question'])->where('id', '!=', $item->id)->where('subject_id', @$data['subject_id'])->where('chapter_id', @$data['chapter_id'])->where('thematic_id', @$data['thematic_id'])
                        ->where('a', @$data['a'])->where('b', @$data['b'])->where('c', @$data['c'])->where('d', @$data['d'])->count() > 0) {
                    CommonHelper::one_time_message('error', 'Lỗi trùng lặp! Câu hỏi này đã được nhập từ trước');
                    return back()->withErrors($validator)->withInput();
                }

                if ($item->save()) {

                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }
                $url = '';
                if ($request->has('parent_id')) {
                    $url .= '?parent_id=' . $request->parent_id;
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id . $url);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add' . $url);
                }

                return redirect('admin/' . $this->module['code'] . $url);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            $url = '';
            if ($request->has('parent_id')) {
                $url .= '?parent_id=' . $request->parent_id;
            }

            return redirect('admin/' . $this->module['code'] . $url);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


}
