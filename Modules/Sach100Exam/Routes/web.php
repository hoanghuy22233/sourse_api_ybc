<?php


Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'subject'], function () {
        Route::get('', 'Admin\SubjectController@getIndex')->name('subject');
        Route::get('publish', 'Admin\SubjectController@getPublish')->name('subject.publish')->middleware('permission:subject_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\SubjectController@add')->middleware('permission:subject_add');
        Route::get('delete/{id}', 'Admin\SubjectController@delete')->middleware('permission:subject_delete');
        Route::post('multi-delete', 'Admin\SubjectController@multiDelete')->middleware('permission:subject_delete');
        Route::get('search-for-select2', 'Admin\SubjectController@searchForSelect2')->name('subject.search_for_select2')->middleware('permission:subject_view');

        Route::get('{id}', 'Admin\SubjectController@update')->middleware('permission:subject_view');
        Route::post('{id}', 'Admin\SubjectController@update')->middleware('permission:subject_edit');
    });


    Route::group(['prefix' => 'chapter'], function () {
        Route::get('', 'Admin\ChapterController@getIndex')->name('chapter');
        Route::get('publish', 'Admin\ChapterController@getPublish')->name('chapter.publish')->middleware('permission:chapter_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ChapterController@add')->middleware('permission:chapter_add');
        Route::get('delete/{id}', 'Admin\ChapterController@delete')->middleware('permission:chapter_delete');
        Route::post('multi-delete', 'Admin\ChapterController@multiDelete')->middleware('permission:chapter_delete');
        Route::get('search-for-select2', 'Admin\ChapterController@searchForSelect2')->name('chapter.search_for_select2')->middleware('permission:chapter_view');

        Route::get('{id}', 'Admin\ChapterController@update')->middleware('permission:chapter_view');
        Route::post('{id}', 'Admin\ChapterController@update')->middleware('permission:chapter_edit');
    });

    Route::group(['prefix' => 'thematic'], function () {
        Route::get('', 'Admin\ThematicController@getIndex')->name('thematic');
        Route::get('publish', 'Admin\ThematicController@getPublish')->name('thematic.publish')->middleware('permission:thematic_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ThematicController@add')->middleware('permission:thematic_add');
        Route::get('delete/{id}', 'Admin\ThematicController@delete')->middleware('permission:thematic_delete');
        Route::post('multi-delete', 'Admin\ThematicController@multiDelete')->middleware('permission:thematic_delete');
        Route::get('search-for-select2', 'Admin\ThematicController@searchForSelect2')->name('thematic.search_for_select2')->middleware('permission:thematic_view');

        Route::get('{id}', 'Admin\ThematicController@update')->middleware('permission:thematic_view');
        Route::post('{id}', 'Admin\ThematicController@update')->middleware('permission:thematic_edit');
    });


    Route::group(['prefix' => 'question'], function () {
        Route::get('', 'Admin\QuestionController@getIndex')->name('question');
        Route::get('publish', 'Admin\QuestionController@getPublish')->name('question.publish')->middleware('permission:question_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\QuestionController@add')->middleware('permission:question_add');
        Route::get('delete/{id}', 'Admin\QuestionController@delete')->middleware('permission:question_delete');
        Route::post('multi-delete', 'Admin\QuestionController@multiDelete')->middleware('permission:question_delete');
        Route::get('search-for-select2', 'Admin\QuestionController@searchForSelect2')->name('question.search_for_select2')->middleware('permission:question_view');

        Route::post('import-excel', 'Admin\QuestionController@importExcel')->middleware('permission:question_add');

        Route::get('{id}', 'Admin\QuestionController@update')->middleware('permission:question_view');
        Route::post('{id}', 'Admin\QuestionController@update')->middleware('permission:question_edit');
    });

    Route::group(['prefix' => 'question_error'], function () {
        Route::get('', 'Admin\QuestionErrorController@getIndex')->name('question_error');
        Route::get('publish', 'Admin\QuestionErrorController@getPublish')->name('question_error.publish')->middleware('permission:question_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\QuestionErrorController@add')->middleware('permission:question_add');
        Route::get('delete/{id}', 'Admin\QuestionErrorController@delete')->middleware('permission:question_delete');
        Route::post('multi-delete', 'Admin\QuestionErrorController@multiDelete')->middleware('permission:question_delete');
        Route::get('search-for-select2', 'Admin\QuestionErrorController@searchForSelect2')->name('question_error.search_for_select2')->middleware('permission:question_view');

        Route::post('import-excel', 'Admin\QuestionErrorController@importExcel')->middleware('permission:question_add');

        Route::get('{id}', 'Admin\QuestionErrorController@update')->middleware('permission:question_view');
        Route::post('{id}', 'Admin\QuestionErrorController@update')->middleware('permission:question_edit');
    });

    Route::group(['prefix' => 'question_item'], function () {
        Route::get('', 'Admin\QuestionItemController@getIndex')->name('question_item')->middleware('permission:comment_view');
        Route::get('publish', 'Admin\QuestionItemController@getPublish')->name('question_item.publish')->middleware('permission:comment_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\QuestionItemController@add')->middleware('permission:comment_add');
        Route::get('delete/{id}', 'Admin\QuestionItemController@delete')->middleware('permission:comment_delete');
        Route::post('multi-delete', 'Admin\QuestionItemController@multiDelete')->middleware('permission:comment_delete');
        Route::get('search-for-select2', 'Admin\QuestionItemController@searchForSelect2')->name('question_item.search_for_select2')->middleware('permission:comment_view');
        Route::get('{id}', 'Admin\QuestionItemController@update')->middleware('permission:comment_view');
        Route::post('{id}', 'Admin\QuestionItemController@update')->middleware('permission:comment_edit');
    });

    Route::group(['prefix' => 'service'], function () {
        Route::get('', 'Admin\ServiceController@getIndex')->name('service');
        Route::get('publish', 'Admin\ServiceController@getPublish')->name('service.publish')->middleware('permission:service_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ServiceController@add')->middleware('permission:service_add');
        Route::get('delete/{id}', 'Admin\ServiceController@delete')->middleware('permission:service_delete');
        Route::post('multi-delete', 'Admin\ServiceController@multiDelete')->middleware('permission:service_delete');
        Route::get('search-for-select2', 'Admin\ServiceController@searchForSelect2')->name('service.search_for_select2')->middleware('permission:service_view');

        Route::get('{id}', 'Admin\ServiceController@update')->middleware('permission:service_view');
        Route::post('{id}', 'Admin\ServiceController@update')->middleware('permission:service_edit');
    });

    Route::group(['prefix' => 'transaction_history'], function () {
        Route::get('', 'Admin\TransactionHistoryController@getIndex')->name('transaction_history');
        Route::get('publish', 'Admin\TransactionHistoryController@getPublish')->name('transaction_history.publish')->middleware('permission:transaction_history_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TransactionHistoryController@add')->middleware('permission:transaction_history_add');
        Route::get('delete/{id}', 'Admin\TransactionHistoryController@delete')->middleware('permission:transaction_history_delete');
        Route::post('multi-delete', 'Admin\TransactionHistoryController@multiDelete')->middleware('permission:transaction_history_delete');
        Route::get('search-for-select2', 'Admin\TransactionHistoryController@searchForSelect2')->name('transaction_history.search_for_select2')->middleware('permission:transaction_history_view');

        Route::get('{id}', 'Admin\TransactionHistoryController@update')->middleware('permission:transaction_history_view');
        Route::post('{id}', 'Admin\TransactionHistoryController@update')->middleware('permission:transaction_history_edit');
    });



    Route::group(['prefix' => 'bill'], function () {
        Route::get('', 'Admin\BillController@getIndex')->name('bill');
        Route::get('publish', 'Admin\BillController@getPublish')->name('bill.publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BillController@add');
        Route::get('delete/{id}', 'Admin\BillController@delete');
        Route::post('multi-delete', 'Admin\BillController@multiDelete');
        Route::get('search-for-select2', 'Admin\BillController@searchForSelect2')->name('bill.search_for_select2');
        Route::post('active-order', 'Admin\BillController@activeOrder')->name('bill.active-order');
        Route::post('active-order-all', 'Admin\BillController@activeOrderAll')->name('bill.active-order-all');
        Route::get('{id}', 'Admin\BillController@update');
        Route::post('{id}', 'Admin\BillController@update');

    });
});

Route::group(['prefix' => 'admin'], function () {
    Route::group(['prefix' => 'question'], function () {
        Route::get('{table}/get-data', 'Admin\QuestionController@getDataLocation');
    });
});
