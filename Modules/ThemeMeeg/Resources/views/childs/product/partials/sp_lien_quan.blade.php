<?php
$cate_multi = explode('|', trim($product->related_products, '|'));
$relate_products = CommonHelper::getFromCache('product_id_cate_multi' . implode("|", $cate_multi));
if (!$relate_products) {
    $relate_products = \Modules\ThemeSTBD\Models\Product::whereIn('id', $cate_multi)->where('status', 1)->get();
    CommonHelper::putToCache('product_id_cate_multi' . implode("|", $cate_multi), $relate_products);
}
?>
@if(@$settings['related_product'] == 1 && isset($relate_products) && count($relate_products) > 0)
    <?php
    $cate_multi = explode('|', trim($product->related_products, '|'));
    $relate_products = CommonHelper::getFromCache('product_id_cate_multi' . implode("|", $cate_multi), ['products']);
    if (!$relate_products) {
        $relate_products = \Modules\ThemeMeeg\Models\Product::whereIn('id', $cate_multi)->where('status', 1)->get();
        CommonHelper::putToCache('product_id_cate_multi' . implode("|", $cate_multi), $relate_products, ['products']);
    }
    ?>
    <div>
        <span class="dtit">Sản phẩm liên quan</span>
        <div class="ss" id="SPTT2">
            <span class="psback"></span>
            <div class="pspanel">
                <div class="pswrap">
                    @foreach( $relate_products as $relate_product)
                        @php
                            $cate_slug =CommonHelper::getFromCache('slug_cate_by_sptt'.@$relate_product->id, ['categories']);
                            if (!$cate_slug){
                                $cate_slug = \Modules\ThemeMeeg\Models\Category::whereIn('id', explode('|', @$relate_product->multi_cat))->first();
                               CommonHelper::putToCache('slug_cate_by_sptt'.@$relate_product->id, @$cate_slug, ['categories']);
                            }
                        @endphp
                        <a class="psitem"
                           href="{{\Modules\ThemeMeeg\Http\Helpers\CommonHelper::getProductSlug($relate_product) }}"
                           title="{{$relate_product->name}}">

                            <div class="pi">
                                <img alt="{{@$relate_product->name}}"
                                     class="lazy"
                                     data-src="{{CommonHelper::getUrlImageThumb(@$relate_product->image, 160, null) }}"/>
                            </div>
                            <p class="pn">{{@$relate_product->name}}</p>
                            <span class="pr">{{number_format(@$relate_product->final_price, 0, '.', '.')}}<sup
                                        style="margin-left: 5px;">đ</sup></span>
                        </a>
                    @endforeach

                </div>
            </div>
            <span class="psnext"></span>
        </div>
    </div>
@endif
