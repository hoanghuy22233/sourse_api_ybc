<html lang="vi">
<head>
    @include('themeeduldp::partials.head_meta')

    @include('themeeduldp::partials.head_script')
    <style>
        .topbar.transperent > nav {
            z-index: 99999;
        }

        .top-area > ul {
            padding-left: 10px;
        }

        img.lazy {
            opacity: 0;
        }

        .top-area > ul,
        .shopping_cart {
            margin-top: 15px;
        }

        .topbar.transperent.stick {
            background: rgba(26, 44, 60, .6);
            padding-bottom: 10px;
            padding-top: 10px;
        }

        .topbar .user-img {
            cursor: pointer;
            display: inline-block;
            vertical-align: middle;
            position: relative;
            line-height: 60px;
        }

        .user-img > img {
            border-radius: 50%;
            display: inline-block;
            transform: scale(0.8);
            vertical-align: inherit;
            border: 2px solid rgba(255, 255, 255, .8);
        }

        p {
            color: #959ab5;
        }

        .topbar.transperent .nav-list {
            width: 83%;
        }

        .user-profile figure img {
            max-height: 300px;
        }

        .logo img {
            max-height: 30px;
        }

        .nav-list > li ul {
            opacity: 1;
            visibility: visible;
        }

        p.post {
            display: none;
        }


        @if(@$settings['main_color'] != '')
            footer::before {
            background: rgba(0, 0, 0, 0) linear-gradient(to right, #ffffff 0%, {{ $settings['main_color'] }} 50%, #ffffff 100%) repeat scroll 0 0;
        }

        #nprogress .bar, .timeline-info > ul li a::before, .add-btn > a, .activitiez > li::before, form button, a.underline:before, .setting-row input:checked + label, .user-avatar:hover .edit-phto, .add-butn, .nav.nav-tabs.likes-btn > li a.active, a.dislike-btn, .drop > a:hover, .btn-view.btn-load-more:hover, .accordion .card h5 button[aria-expanded="true"], .f-page > figure em, .inbox-panel-head > ul > li > a, footer .widget-title h4::before, #topcontrol, .sidebar .widget-title::before, .g-post-classic > figure > i::after, .purify > a, .open-position::before, .info > a, a.main-btn, .section-heading::before, .more-branches > h4::before, .is-helpful > a, .cart-optionz > li > a:hover, .paginationz > li a:hover, .paginationz > li a.active, .shopping-cart, a.btn2:hover, .form-submit > input[type="submit"], button.submit-checkout, .delete-cart:hover, .proceed .button, .amount-area .update-cart, a.addnewforum, .attachments li.preview-btn button:hover, .new-postbox .post-btn:hover, .weather-date > span, a.learnmore, .banermeta > a:hover, .add-remove-frnd > li a:hover, .profile-controls > li > a:hover, .edit-seting:hover, .edit-phto:hover, .account-delete > div > button:hover, .radio .check-box::after, .eror::after, .eror::before, .big-font, .event-time .main-btn:hover, .group-box > button:hover, .dropcap-head > .dropcap, .checkbox .check-box::after, .checkbox .check-box::before, .main-btn2:hover, .main-btn3:hover, .jalendar .jalendar-container .jalendar-pages .add-event .close-button, .jalendar .jalendar-container .jalendar-pages .days .day.have-event span::before, .user-log > i:hover, .total > i, .login-frm .main-btn, .search-tab .nav-tabs .nav-item > a.active::after, .mh-head, .job-tgs > a:hover, .owl-prev:hover:before, .owl-next:hover:before, .help-list > a, .title2::before, .fun-box > i, .list-style > li a:hover:before, .postbox .we-video-info > button:hover, .postbox .we-video-info > button.main-btn.color, .copy-email > ul li a:hover, .post-status > ul li:hover, .tags_ > a:hover, .policy .nav-link.active::before, a.circle-btn:hover, .mega-menu > li:hover > a > span, .pit-tags > a:hover, .create-post::before, .amount-select > li:hover, .amount-select > li.active, .pay-methods > li:hover, .pay-methods > li.active, .msg-pepl-list .nav-item.unread::before, .menu .btn:hover, .menu-item-has-children ul.submenu > li a::before, .pagination > li a:hover, .pagination > li a.active, .slick-dots li button, .slick-prev:hover:before, .slick-next:hover:before, .sub-popup::before, .sub-popup::after, a.date, .welcome-area > h2::before, .page-header.theme-bg {
            background: {{ $settings['main_color'] }};
        }

        .product-carousel .owl-nav .owl-prev::before, .product-carousel .owl-nav .owl-next::before, .product-caro .owl-prev:hover:before, .product-caro .owl-next:hover:before, .log-reg-area form .forgot-pwd, .log-reg-area form .already-have, .log-reg-area > p a, .timeline-info > ul li a.active, .timeline-info > ul li a:hover, .dropdowns > a.more-mesg, .activity-meta > h6 a, .activity-meta > span a:hover, .description > p a, .we-comment > p a, .sidebar .widget li:hover > a, .sidebar .widget li:hover > i, .friend-meta > a, .user-setting > a:hover, .we-comet li a.showmore, .twiter-feed > li p a, .tutor-links > li i, .tutor-links > li:hover, .pepl-info > span, .frnds .nav-tabs .nav-item a.active, #work > div a, .basics > li i, .education > li i, .groups > span i, a.forgot-pwd, .friend-meta > h4 a:hover, .x_title > h2, .post-meta .detail > span, .add-btn > a:hover, .top-area > ul.main-menu > li > ul li a:hover, .dropdowns.active > a i, .form-group input.form-file ~ .control-label, .form-group input.has-value ~ .control-label, .form-group input:focus ~ .control-label, .form-group input:valid ~ .control-label, .form-group select ~ .control-label, .form-group textarea.form-file ~ .control-label, .form-group textarea.has-value ~ .control-label, .form-group textarea:focus ~ .control-label, .form-group textarea:valid ~ .control-label, .flaged > h3, .invition .friend-meta a.invite:hover, .more-optns > ul li:hover, .post-title > h4 a:hover, .post-title .p-date a:hover, .l-post .l-post-meta > h4 a:hover, .read:hover, .tags > a:hover, .comment-titles > span, .help-list > ul li a:hover i, .carrer-title > span a, .open-position > h4 a:hover, .option-set.icon-style > li > a.selected, .category-box > i, .branches-box > ul li i, .help-topic-result > h2 a:hover, .product-name > h5 a:hover, .full-postmeta .shopnow, .prices.style2 ins span, .single-btn > li > a.active, .total-box > ul > li.final-total, .logout-meta > p a, .forum-list table tbody tr td i, .widget ul.recent-topics > li > i, .date-n-reply > a, .topic-data > span, .help-list > ul li a:hover, .employer-info h2, .job-detail > ul li i, .company-intro > a, .user-setting > ul li a:hover i, .your-page ul.page-publishes > li span:hover i, .drops-menu > li > a:hover .mesg-meta h6, .we-comment > h5:hover, .inline-itms > a:hover, .mesg-meta figure span, .like-dislike > li a:hover, .we-video-info > ul li .users-thumb-list > span strong, .we-video-info > ul li .users-thumb-list > span a, .add-del-friends > a:hover, .story-box:hover .story-thumb > i, .sugtd-frnd-meta > span > a, .sugtd-frnd-meta > a:hover, .create-post > a, .mesg-meta > h6 > a:hover, .profile-menu > li > a:hover, .profile-menu > li > a.active, .friend-name > ins > a, .more-post-optns > ul > li:hover, .more-post-optns > ul > li:hover i, .origin-name > a, .breadcrumb > .breadcrumb-item, .nav-tabs--left .nav-link.active, .nav-tabs--left .nav-link.active:hover, .set-title > span a, .onoff-options .setting-row > p > a, .checkbox > p a, .notifi-seting > p a, .page-likes .tab-content .tab-pane > a, .personal-head > p a, .f-title i, .more-opotnz > ul li a:hover, .frnd-name > a:hover, .option-list ul li a:hover, .option-list ul li i, .smal-box .fileContainer > i, .from-gallery > i, .over-photo > a:hover i, .featurepost > h5 > i, .widget .fav-community > li a, .radio input:checked ~ .check-box::before, .suggestd > li .sug-like:hover i, .gen-metabox > p > a, .widget .invitepage > li > a i, .see-all, .event-title > h4 a:hover, .event-date, .location-map > p, .event-title > span i, .typography > a, .main-btn2, a.main-btn2, blockquote p strong, .dob-meta > h6 a, .recent-jobs li > span a, .recent-jobs li h6 span, .position-meta > span, .invite-location > span, .invite-figure > h6 > a, .user-add > div > i, .logout-form > p > a, .logout-form > a, .login-frm > a, .c-form.search .radio > a, .frnd-meta > a, .notifi-meta > span > i, .card-body a, .search-meta > span i, .pit-frnz-meta > a:hover, .pit-groups-meta > a:hover, .pit-pages-meta > a:hover, .related-searches > li > a:hover, .wiki-box > h4 > a, .wiki-box > p > a, .p-info > a, .widget .reg-comp-meta > ul > li a, .re-links-meta > h6 > a:hover, .pitnik-video-help > i, h3.resutl-found > span, .related-links > li > a:hover, .attachments > ul .add-loc > i, .colla-apps > li a:hover, .add-location-post > span, footer .widget .colla-apps > li a:hover, .list-style > li a:hover, .page-meta > a:hover, .add-pitrest > a, .pitrest-pst-hding:hover, .fa.fa-heart.like, .log-out > li:last-child a, .log-out > li:last-child a i, .loc-cate > ul.loc > li i, .loc-cate > ul > li a, .loc-cate > ul > li::before, .job-price > ins, .users-thumb-list > span > a, .we-video-info > ul li span:hover, .we-video-info .heart:hover, .job-search-form > a, .user-figure > a, .user-info > li span, .main-color, .pit-points > i, .menu-list > li > a > i, .post-up-time > li a, .number > span.active i, .number > input.active, .pit-uzr > a:hover, .pit-post-deta > h4 > a:hover, .view-pst-style > li.active > a, .pit-opt > li.save, .Rpt-meta > span, .pitred-links > ul > li a:hover, .smilez > li > span, .sidebar .comnity-data > ul > li, .comnty-avatar > a:hover, .usr-fig > a:hover, .post-up-time > li .usr-fig > a:hover, .feature-title > h2 > a:hover, .feature-title > h4 > a:hover, .feature-title > h6 > a:hover, .nave-area > li > a > i, .nave-area > li > a:hover, .save-post.save, .tube-title > h6 > a:hover, .chanle-name > a, .channl-author > em, .pit-tags > span, .tube-pst-meta > h5 a:hover, .addnsend > a i, .follow-me:hover, .follow-me:hover i, .contribute:hover, .contribute:hover i, .links-tab li.nav-item > a.active, .post-meta > h6 > a:hover, .fixed-sidebar .left-menu-full > ul li a.closd-f-menu, .fixed-sidebar .left-menu-full > ul li a:hover, .help-box > span, .post-meta .detail > a:hover, .sugested-photos > h5 a, .our-moto > p > span, .sound-right .send-mesg, .title-block .align-left h5 > i, .audio-user-name > h6 a:hover, .add-send > ul > li a, .add-send .send-mesg, .audio-title:hover, .sound-post-box > h4, .singer-info > span, .playlist-box > ul > li:hover, .song-title > h6 > a:hover, .song-title > a:hover, .playlist-box > h4 i, .prise, .location-area > span > i, .classic-pst-meta > h4 a:hover, .total-area > ul li.order-total > i, .classi-pst-meta > span ins, .classi-pst-meta > h6 a:hover, .classi-pst .user-fig a, .msg-pepl-list .nav-item.unread > a > div h6, .chater-info > h6, .text-box > p a, .description > h2 a:hover, span.ttl, .filter-meta > input, .pagination.borderd > li a:hover, .pricings > h1 span, .count i, .testi-meta > span i, .sec-heading.style9 > h2 span, .sec-heading.style9 > span i, .blog-title > a:hover, .serv-box > i, .heading-2 span, .team > h5 span, .popup-closed:hover, .text-caro-meta > span, .text-caro-meta > h1 > a span, .sub-popup > h4 span, .testi-meta::before, .user > a {
            color: {{ $settings['main_color'] }};
        }
        @endif
    </style>

    {!! @$settings['frontend_head_code'] !!}
    @yield('head_script')
</head>
<body>
@include('themeedu::partials.user_menu_mobile')
@include('themeeduldp::partials.main_menu_mobile')

<div id="mm-0" class="mm-page mm-slideout">
    <div class="theme-layout">

        <div class="responsive-header">
            <div class="mh-head first mm-sticky mh-btns-left mh-btns-right mh-sticky">
			<span class="mh-btns-left">
				<a class="" href="#menu"><i class="fa fa-align-justify"></i></a>
			</span>
                <span class="mh-text">
				<a href="/" title=""><img class="lazy"
                                          data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo'], null, 30) }}"
                                          alt="{{ @$settings['name'] }}"></a>
			</span>
                <span class="mh-btns-right">
				<a class="fa fa-sliders" href="#shoppingbag"></a>
			</span>
            </div>
            <div class="mh-head second">
                <form class="mh-form">
                    <input placeholder="search">
                    <a href="#/" class="fa fa-search"></a>
                </form>
            </div>
        </div><!-- responsive header -->

        @if(strpos($_SERVER['REQUEST_URI'], '/profile') === false && strpos($_SERVER['REQUEST_URI'], '/student') === false)
            @include('themeeduldp::partials.menu')
        @endif

        @yield('main_content')

        @include('themeedu::partials.footer')
    </div>
    <div class="strp-spinner-move" style="display: none;">
        <div class="strp-spinner" style="display: none;">
            <div class="strp-spinner-rotate"
                 style="animation: 1s steps(80) 0s infinite normal none running strp-spinner-spin;">
                <div class="strp-spinner-frame" style="opacity: 0.01; transform: rotate(4.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.03; transform: rotate(9deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.04; transform: rotate(13.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.05; transform: rotate(18deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.06; transform: rotate(22.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.08; transform: rotate(27deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.09; transform: rotate(31.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.1; transform: rotate(36deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.11; transform: rotate(40.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.13; transform: rotate(45deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.14; transform: rotate(49.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.15; transform: rotate(54deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.16; transform: rotate(58.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.18; transform: rotate(63deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.19; transform: rotate(67.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.2; transform: rotate(72deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.21; transform: rotate(76.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.23; transform: rotate(81deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.24; transform: rotate(85.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.25; transform: rotate(90deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.26; transform: rotate(94.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.28; transform: rotate(99deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.29; transform: rotate(103.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.3; transform: rotate(108deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.31; transform: rotate(112.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.33; transform: rotate(117deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.34; transform: rotate(121.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.35; transform: rotate(126deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.36; transform: rotate(130.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.38; transform: rotate(135deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.39; transform: rotate(139.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.4; transform: rotate(144deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.41; transform: rotate(148.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.43; transform: rotate(153deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.44; transform: rotate(157.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.45; transform: rotate(162deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.46; transform: rotate(166.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.48; transform: rotate(171deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.49; transform: rotate(175.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.5; transform: rotate(180deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.51; transform: rotate(184.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.53; transform: rotate(189deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.54; transform: rotate(193.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.55; transform: rotate(198deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.56; transform: rotate(202.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.58; transform: rotate(207deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.59; transform: rotate(211.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.6; transform: rotate(216deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.61; transform: rotate(220.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.63; transform: rotate(225deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.64; transform: rotate(229.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.65; transform: rotate(234deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.66; transform: rotate(238.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.68; transform: rotate(243deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.69; transform: rotate(247.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.7; transform: rotate(252deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.71; transform: rotate(256.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.73; transform: rotate(261deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.74; transform: rotate(265.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.75; transform: rotate(270deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.76; transform: rotate(274.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.78; transform: rotate(279deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.79; transform: rotate(283.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.8; transform: rotate(288deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.81; transform: rotate(292.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.83; transform: rotate(297deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.84; transform: rotate(301.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.85; transform: rotate(306deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.86; transform: rotate(310.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.88; transform: rotate(315deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.89; transform: rotate(319.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.9; transform: rotate(324deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.91; transform: rotate(328.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.93; transform: rotate(333deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.94; transform: rotate(337.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.95; transform: rotate(342deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.96; transform: rotate(346.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.98; transform: rotate(351deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 0.99; transform: rotate(355.5deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
                <div class="strp-spinner-frame" style="opacity: 1; transform: rotate(360deg);">
                    <div class="strp-spinner-line" style="background: rgb(117, 122, 149);"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="strp-window strp-measured strp-window-skin-strip">
        <div class="strp-pages"></div>
        <div class="strp-nav strp-nav-previous" style="display: none;">
            <div class="strp-nav-button" style="display: none;">
                <div class="strp-nav-button-background"></div>
                <div class="strp-nav-button-icon"></div>
            </div>
        </div>
        <div class="strp-nav strp-nav-next" style="display: none;">
            <div class="strp-nav-button" style="display: none;">
                <div class="strp-nav-button-background"></div>
                <div class="strp-nav-button-icon"></div>
            </div>
        </div>
        <div class="strp-close">
            <div class="strp-close-background"></div>
            <div class="strp-close-icon"></div>
        </div>
    </div>
    <div id="topcontrol" title="Scroll Back to Top"
         style="position: fixed; bottom: 10px; right: 5px; opacity: 1; cursor: pointer;"><i class="ti-arrow-up"></i>
    </div>
</div>

@include('themeeduldp::partials.footer_script')
@yield('footer_script')
@include('themeeduldp::partials.chat_facebook')

<div id="mm-blocker" class="mm-slideout"></div>
{!! @$settings['frontend_body_code'] !!}
</body>
</html>