<?php
$order = \Modules\ThemeEduLDP\Models\Order::where('student_id', @\Auth::guard('student')->user()->id)->whereNotNull('student_id')->where('course_id', $course->id)->first();
?>
<div class="central-meta item">
    <div class="classi-pst">
        <div class="row merged10">
            <div class="col-lg-6">
                <div class="image-bunch single">
                    <figure><img class="lazy"
                                data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($course->image,244, null) }}"
                                alt="{{@$course->name}}"></figure>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="classi-pst-meta">
                    <h6><a href="/khoa-hoc/{{(@$course->slug)}}.html"
                           title="{{@$course->name}}">{{@$course->name}}</a></h6>
                </div>
            </div>
            <div class="col-lg-12">
                <p class="classi-pst-des">
                    {!!@$course->intro !!}
                </p>
                @if(is_object($order) && $order->status == 1)
                    <a class="active btn btn-info" href="/khoa-hoc/{{(@$course->slug)}}.html"
                       style="background-color: #fa6342;float: right;border-color: #bc482e;">Học ngay</a>

                @else
                    <a class="active btn btn-info" href="/khoa-hoc/{{(@$course->slug)}}.html"
                       style="background-color: #fa6342;float: right;border-color: #bc482e;">Xem chi tiết</a>
                @endif
            </div>
        </div>
    </div>
</div>
