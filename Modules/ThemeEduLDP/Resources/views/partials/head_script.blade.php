{{--<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700|Roboto:300,400,500,600,700">--}}
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu-ldp/css/main.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu-ldp/css/weather-icon.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu-ldp/css/weather-icons.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu-ldp/css/style.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu-ldp/css/color.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu-ldp/css/responsive.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/common.css') }}?v={{ time() }}">

{{--Custom--}}
<script src="{{ URL::asset('public/libs/jquery-3.4.0.min.js') }}"></script>