<?php
$options = [];
$arr = explode('- Khách hàng:', $widget->content);
foreach ($arr as $item) {
    if ($item != '') {
        $option = [];
        $lines = preg_split('/\n|\r\n?/', $item);
        $option['name'] = trim(@$lines[0]);
        foreach ($lines as $line) {
            if (strpos($line, '+ Nghề nghiệp:') !== false) {
                $option['job'] = trim(str_replace('+ Nghề nghiệp:', '', $line));
            }
            if (strpos($line, '+ Lời nhận xét:') !== false) {
                $option['content'] = trim(str_replace('+ Lời nhận xét:', '', $line));
            }
            if (strpos($line, '+ Link ảnh đại diện:') !== false) {
                $option['image'] = trim(str_replace('+ Link ảnh đại diện:', '', $line));
            }
        }
        $options[] = $option;
    }
}
?>
<div class="col-lg-6 col-md-6">
    <div class="tab-sec">
        <div class="heading-2">
            {!! $widget->name !!}
        </div>
        <div class="dc-toggle">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach($options as $k => $option)
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $k }}"
                                   aria-expanded="false" aria-controls="collapse{{ $k }}" class="collapsed">
                                    <i class="toogle">
                                        <span>{{ @$option['name'] }}</span>
                                        <i class="fa fa-commenting"></i>
                                    </i>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse{{ $k }}" class="panel-collapse in collapse" role="tabpanel"
                             aria-labelledby="headingOne" style="">
                            <div class="panel-body">
                                <p>{!! @$option['content'] !!}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div><!-- tabs -->
</div><!-- bootstrap accordion -->
<div class="col-lg-6 col-md-6">
    <div class="testi-caro owl-carousel owl-theme owl-responsive-1000 owl-loaded">
        @foreach($options as $option)
            <div class="owl-item" style="width: 540px; margin-right: 0px;">
                <div class="testi-silver">
                    <span>@if(@$option['image'] != '')<img alt="" src="{{ @$option['image'] }}">@endif</span>
                    <div class="testi-meta">
                        <p>
                            {!! @$option['content'] !!}
                        </p>
                        <span><i class="fa fa-thumbs-o-up"></i>{!! @$option['name'] !!}</span>
                        <i>{!! @$option['job'] !!}</i>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>