<?php
$count_student = \Modules\ThemeEduLDP\Models\Student::count();
$count_teacher = \Modules\ThemeEduLDP\Models\Admin::where('type', 2)->count();
?>
<div class="fun-fact">
    <div class="row">
        {{--<div class="col-lg-3 col-md-3 col-sm-4">--}}
        {{--<div class="fun-box">--}}
        {{--<i class="ti-check-box"></i>--}}
        {{--<h6>Người đã đăng ký</h6>--}}
        {{--<span>1,01,242</span>--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="col-lg-3 col-md-3 col-sm-4">
            <div class="fun-box">
                <i class="ti-layout-media-overlay-alt-2"></i>
                <h6>Giáo viên</h6>
                <span>{{$count_teacher}}</span>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4">
            <div class="fun-box">
                <i class="ti-user"></i>
                <h6>Học viên</h6>
                <span>{{$count_student}}</span>
            </div>
        </div>
    </div>
</div>