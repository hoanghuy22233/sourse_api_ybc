@extends('themeeduldp::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
    @include('themeedu::template.top_bar')

    <!-- topbar -->
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themeeduldp::partials.student_menu')
                                </div><!-- user profile banner  -->

                                <div class="col-lg-8 col-md-8">
                                    <div class="forum-form">
                                        @if(Session::has('success'))
                                            <p style="top: -15px;" class="alert alert-success">{!! Session::get('success') !!}</p>
                                        @endif
                                        <div class="central-meta">
                                            <span class="create-post">Thông tin cơ bản</span>
                                            <form method="post" class="c-form" action="/profile/edit"
                                                  enctype="multipart/form-data">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="student_id"
                                                       value="{{ Auth::guard('student')->user()->id }}">
                                                <div>
                                                    <label>Họ & tên</label>
                                                    <input type="text" name="name" placeholder="Name"
                                                           value="{{ Auth::guard('student')->user()->name }}">
                                                </div>
                                                <div>
                                                    <label>Số điện thoại</label>
                                                    <input type="text" name="phone" placeholder="Phone"
                                                           value="{{ Auth::guard('student')->user()->phone }}">
                                                </div>
                                                <div>
                                                    <label>Email</label>
                                                    <input type="text" name="email" placeholder="Email"
                                                           value="{{ Auth::guard('student')->user()->email }}">
                                                </div>
                                                <div>
                                                    <label>Địa chỉ</label>
                                                    <input type="text" name="address" placeholder="Address"
                                                           value="{{ Auth::guard('student')->user()->address }}">
                                                </div>
                                                <div>
                                                    <label>Ảnh đại diện</label>
                                                    <img class="lazy" data-src="{{ asset('public/filemanager/userfiles/' . @Auth::guard('student')->user()->image, 100, null) }}">
{{--                                                    <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(Auth::guard('student')->user()->image, 100, null) }}">--}}
                                                    <input type="file" name="image" placeholder="Image"
                                                           value="{{ Auth::guard('student')->user()->image }}">
                                                </div>
                                                <div>
                                                    <label>Ảnh banner</label>
{{--                                                    <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(Auth::guard('student')->user()->banner, 100, null) }}">--}}
                                                    <img class="lazy" data-src="{{ asset('public/filemanager/userfiles/' . @Auth::guard('student')->user()->banner, 100, null) }}">
                                                    <input type="file" name="banner" placeholder="Banner"
                                                           value="{{ Auth::guard('student')->user()->banner }}">
                                                </div>
                                                <div>
                                                    <?php
                                                    $data = CommonHelper::getFromCache('center_all', ['centers']);
                                                    if (!$data) {
                                                        $data = \Modules\ThemeEduLDP\Models\Center::orderBy('order_no', 'desc')->orderBy('name', 'asc')->get();
                                                        CommonHelper::putToCache('center_all', $data, ['centers']);
                                                    }
                                                    ?>
                                                    <label>Chi nhánh</label>
                                                    <select name="center_id">
                                                        <option>Chọn chi nhánh bạn học</option>
                                                        @foreach($data as $v)
                                                            <option value="{{ $v->id }}" {{ Auth::guard('student')->user()->center_id == $v->id ? 'selected' : '' }}>{{ $v->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div>
                                                    <label>Ngày sinh</label>
                                                    <input type="date" name="birthday" placeholder="Birthday"
                                                           value="{{ Auth::guard('student')->user()->birthday }}">
                                                </div>
                                                <div>
                                                    <label>Giới tính</label>
                                                    <div class="col-md-6" style="display: inline-block;">
                                                        <label><input type="radio" name="gender" placeholder="Gender"
                                                                      style="width: 50px;"
                                                                      value="1" {{ Auth::guard('student')->user()->gender === 1 ? 'checked' : '' }}>
                                                            Nam
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6" style="display: inline-block;">
                                                        <label>
                                                            <input type="radio" name="gender" placeholder="Gender"
                                                                   style="width: 50px;"
                                                                   value="0" {{ Auth::guard('student')->user()->gender === 0 ? 'checked' : '' }}>
                                                            Nữ
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <button class="main-btn mr-4" type="submit" data-ripple="">Cập nhật
                                                        </button>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <a class="pt-2" style="padding-left: 73%;" href="/change-pass">Đổi mật khẩu
                                                        </a>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a class="main-btn3 ml-5" href="/profile">Quay lại
                                                        </a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="central-meta stick-widget">
                                        <span class="create-post">Cấu hình khác</span>
                                        <div class="personal-head">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>

@endsection
@section('head_script')
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/profile.css') }}?v={{ time() }}">
@endsection