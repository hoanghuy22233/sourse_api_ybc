@extends('themeeduldp::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
    @include('themeedu::template.top_bar')
    <!-- topbar -->
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themeeduldp::partials.student_menu', ['user' => $user])
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="central-meta">

                                        <span class="create-post">Giới thiệu</span>
                                        <div class="personal-head">
                                            {{--<span class="f-title"><i class="fa fa-user"></i> Về tôi:</span>--}}
                                            {{--<p>--}}
                                            {{--Học viên gương mẫu, thấm nhuần tư tưởng của Đảng--}}
                                            {{--</p>--}}
                                            <span class="f-title"><i class="fa fa-birthday-cake"></i> Ngày sinh:</span>
                                            <p>
                                                @if(@$user->birthday != '' && @$user->birthday != null)
                                                    {{date('d-m-Y',strtotime(@$user->birthday))}}
                                                @endif
                                            </p>
                                            <span class="f-title"><i class="fa fa-phone"></i> Điện thoại:</span>
                                            <p>
                                                {{@$user->phone}}
                                            </p>
                                            <span class="f-title"><i class="fa fa-male"></i> Giới tính:</span>
                                            <p>
                                                @if(@$user->gender==1)
                                                    Nam
                                                @else
                                                    Nữ
                                                @endif
                                            </p>
                                            <span class="f-title"><i class="fa fa-globe"></i> Địa chỉ:</span>
                                            <p>
                                                {{@$user->address}}
                                            </p>
                                            <span class="f-title"><i class="fa fa-envelope"></i> Email:</span>
                                            <p>
                                                <a href="mailto:{{@$user->email}}"
                                                   class="__cf_email__"
                                                   data-cfemail="cb9ba2bfa5a2a08bb2a4beb9a6aaa2a7e5a8a4a6">
                                                    {{@$user->email}}
                                                </a>
                                            </p>
                                            <span class="f-title"><i class="fa fa-location-arrow"></i> Trung tâm:</span>
                                            <p>
                                                {{ @\Modules\ThemeEduLDP\Models\Center::find($user->center_id)->name }}
                                            </p>
                                            @if($user->id == @\Auth::guard('student')->user()->id)
                                                <a href="/profile/edit"
                                                   style="    color: #007bff;text-decoration: underline; cursor: pointer;">Chỉnh
                                                    sửa profile</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8">
                                    <div class="central-meta">
                                        <span class="create-post">Thành tích học viên</span>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="gen-metabox">
                                                    <?php
                                                    $accumulated_points=@\Modules\ThemeEduLDP\Models\QuizLog::where('student_id',$user->id)->sum('accumulated_points')+@\Modules\ThemeEduLDP\Models\Route::where('student_id',$user->id)->sum('accumulated_points')+@\Modules\ThemeEduLDP\Models\HistoryMisson::where('student_id',$user->id)->sum('accumulated_points');
                                                    $rank_max=\Modules\EduSettings\Entities\Ranks::where('accumulated_points','<=',$accumulated_points)->max('accumulated_points');
                                                    $rank=\Modules\EduSettings\Entities\Ranks::where('accumulated_points',$rank_max)->first();
                                                    ?>
                                                    <span><i class="fa fa-trophy"></i> Huân chương</span>

                                                        <figure>
                                                            <img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($rank->image,210,129) }}"
                                                                                                        alt="{{@$rank->name}}">
                                                        </figure>
                                                            <h5>{{@$rank->name}}</h5>
                                                            <span>{!! @$rank->intro !!}</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="gen-metabox">
                                                    <span><i class="fa fa-puzzle-piece"></i> Điểm tích lũy</span>
                                                    <p>
                                                        {{@\Modules\ThemeEduLDP\Models\QuizLog::where('student_id',$user->id)->sum('accumulated_points')+@\Modules\ThemeEduLDP\Models\Route::where('student_id',$user->id)->sum('accumulated_points')+@\Modules\ThemeEduLDP\Models\HistoryMisson::where('student_id',$user->id)->sum('accumulated_points')}}
                                                    </p>
                                                </div>
                                                <div class="gen-metabox">
                                                    <span><i class="fa fa-plus"></i> Số khóa học đã đăng ký</span>
                                                    <p>
                                                        {{@\Modules\EduBill\Models\Order::where('student_id', $user->id)->where('status', 1)->count('course_id')}}
                                                    </p>
                                                </div>
                                                <div class="gen-metabox">
                                                    <span><i class="fa fa-mortar-board"></i> Điểm trung bình bài kiểm tra</span>
                                                    <p>
                                                        <?php
                                                        $count_scores = \Modules\ThemeEduLDP\Models\QuizLog::where('student_id', $user->id)->count('scores');
                                                        ?>
                                                        @if(@$count_scores > 0)
                                                            <?php
                                                            $sum_scores = CommonHelper::getFromCache('quiz_logs_student_id', ['quiz_log']);
                                                            if (!$sum_scores) {
                                                                $sum_scores = \Modules\ThemeEduLDP\Models\QuizLog::where('student_id', $user->id)->get();
                                                                CommonHelper::putToCache('quiz_logs_student_id', $sum_scores, ['quiz_log']);
                                                            }
                                                            $score = 0;
                                                            foreach ($sum_scores as $v) {
                                                                $str = explode('/', $v->scores);
                                                                if (isset($str[0]) && isset($str[1])) {
                                                                    $score += (int)$str[0] / (int)$str[1];
                                                                }

                                                            }
                                                            ?>
                                                            {{round((@$score/@$count_scores)*100)}}/100
                                                        @endif
                                                    </p>
                                                </div>
                                                <div class="gen-metabox">
                                                    <span><i class="fa fa-certificate"></i> Bài kiểm tra đã làm</span>
                                                    <p>
                                                        {{@\Modules\ThemeEduLDP\Models\QuizLog::where('student_id',$user->id)->count('quizz_id')}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="gen-metabox no-margin">
                                                    <span><i class="fa fa-bookmark"></i> Đánh giá</span>
                                                    <p>
                                                        {!! @$user->review !!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="central-meta">
                                        <span class="create-post">Khóa học đã mua <a href="/student/{{ @$user->id }}/khoa-hoc"
                                                                             title="">Xem tất cả</a></span>
                                        <ul class="suggested-frnd-caro">
                                            <?php
                                            $data = CommonHelper::getFromCache('course_of_student_' . $user->id, ['courses']);
                                            if (!$data) {
                                                $data = \Modules\EduCourse\Models\Course::join('orders', 'orders.course_id', '=', 'courses.id')
                                                    ->selectRaw('courses.*')
                                                    ->where('orders.status', 1)->where('orders.student_id', $user->id)->orderBy('orders.created_at', 'desc')->take(8)->get();
                                                CommonHelper::putToCache('course_of_student_' . $user->id, $data, ['courses']);
                                            }
                                            ?>
                                            @foreach($data as $v)
                                                <li >
                                                    <a href="/khoa-hoc/{{@$v->slug}}.html"><img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image,210,129) }}"
                                                         alt="{{@$v->name}}"></a>
                                                    <div class="sugtd-frnd-meta">
                                                        <a href="/khoa-hoc/{{@$v->slug}}.html"
                                                           title="{{@$v->name}}">{{@$v->name}}</a>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="central-meta">
                                        <span class="create-post">Bài kiểm tra <a href="/student/{{ @$user->id }}/bai-kiem-tra"
                                                                                     title="">Xem tất cả</a></span>
                                        <ul class="suggested-frnd-caro">
                                            <?php
                                            $data = CommonHelper::getFromCache('quizz_of_student_' . $user->id, ['quizzes']);
                                            if (!$data) {
                                                $data = \Modules\EduCourse\Models\Quizzes::join('quiz_log', 'quiz_log.quizz_id', '=', 'quizzes.id')
                                                    ->selectRaw('quizzes.*')
                                                    ->where('quiz_log.student_id', $user->id)->orderBy('quiz_log.created_at', 'desc')->take(8)->get();
                                                CommonHelper::putToCache('quizz_of_student_' . $user->id, $data, ['quizzes']);
                                            }
                                            ?>
                                            @foreach($data as $v)
                                                <li >
                                                    <a href="/bai-kiem-tra/{{@$v->slug}}.html"><img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image,210,129) }}"
                                                         alt="{{@$v->name}}" style="height: 50%"></a>
                                                    <div class="sugtd-frnd-meta">
                                                        <a href="/bai-kiem-tra/{{@$v->slug}}.html"
                                                           title="{{@$v->name}}">{{@$v->name}}</a>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection
@section('head_script')
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/profile.css') }}?v={{ time() }}">
@endsection