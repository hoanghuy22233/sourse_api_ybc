@extends('themeeduldp::layouts.default')
@section('lichhoc')
    active
@endsection
@section('main_content')
    @include('themeeduldp::partials.banner_header')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="col-lg-12">
                                    <?php
                                    $schedules = \Modules\EduSettings\Entities\Schedule::where('status', 1)->get();
                                    ?>

                                    <ul class="nav nav-tabs">
                                        @foreach($schedules as $k=>$schedule)
                                            <li class="{{$k==0?'active':''}}">
                                                <a data-toggle="tab" href="#tab_{{$k}}">{{$schedule->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="col-lg-12">
                                        <div class="tab-content">
                                            @foreach($schedules as $k=>$schedule)
                                                <div class="tab-pane {{$k==0?'fade active show':''}}" id="link{{ $k }}">
                                                    <div class="row merged20">
                                                        {!! $schedule->iframe !!}
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection