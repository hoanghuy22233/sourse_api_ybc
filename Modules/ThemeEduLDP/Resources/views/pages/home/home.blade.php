@extends('themeeduldp::layouts.default')
@section('main_content')
    <?php
    $widgets = CommonHelper::getFromCache('widgets_home_sidebar_center', ['widgets']);
    if (!$widgets) {
        $widgets = \Modules\ThemeEdu\Models\Widget::where('location', 'home_content')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
        CommonHelper::putToCache('widgets_home_sidebar_center', $widgets, ['widgets']);
    }
    ?>
    @foreach($widgets as $widget)
        {{--{{ dd(json_decode($widget->config)->view) }}--}}
        <section>
            @if($widget->type == 'html')
                <div class="gap">
                    <div class="container">
                        <div class="row">
                            {!!$widget->content !!}
                        </div>
                    </div>
                </div>
            @elseif($widget->type == 'banner_slides')
                @include(@json_decode($widget->config)->view, ['config' => (array) json_decode($widget->config)])
            @else
                <div class="gap">
                    <div class="container">
                        <div class="row">
                            @include(@json_decode($widget->config)->view, ['config' => (array) json_decode($widget->config)])
                        </div>
                    </div>
                </div>
            @endif
        </section>
    @endforeach
@endsection
@section('head_script')
    <style>
        .funfact {
            text-align: center;
            margin-bottom: 50px;
        }

        .overlap22 {
            margin-top: 0;
        }

        @media (max-width: 768px) {
            .funfact {
                margin: 0;
            }
        }
    </style>
@endsection
@section('footer_script')
    {{--<script src="/public/frontend/themes/edu-ldp/js/jquery.funfact.min.js"></script>--}}
    {{--<script src="/public/frontend/themes/edu-ldp/js/counterup-t-waypoint.js"></script>--}}

@endsection