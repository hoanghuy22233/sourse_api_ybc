@extends('themeeduldp::layouts.default')
@section('baikiemtra')
    active
@endsection
@section('main_content')
    @include('themeeduldp::partials.banner_header')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="col-lg-3">
                                    <aside class="sidebar static left">
                                        <div class="widget stick-widget">
                                            <h4 class="widget-title">Bảng xếp hạng</h4>
                                            <ul class="followers ps-container ps-theme-default ps-active-y"
                                                data-ps-id="42e41a80-d050-998e-adde-c3912d0eac78">
                                                <?php
                                                $quiz_logs = CommonHelper::getFromCache('quiz_logs_sum_accumulated_points', ['quiz_log']);
                                                if (!$quiz_logs) {
                                                    $quiz_logs = \Modules\ThemeEduLDP\Models\QuizLog::select('student_id', DB::raw('sum(accumulated_points) as accumulated_points'))->where('accumulated_points', '<>', 0)->groupBy('student_id')
                                                        ->orderBy('accumulated_points', 'desc')->get();
                                                    CommonHelper::putToCache('quiz_logs_sum_accumulated_points', $quiz_logs, ['quiz_log']);
                                                }
                                                ?>
                                                @foreach($quiz_logs as $quiz_log)
                                                    <?php
                                                        $rankStudent = $quiz_log->student;
                                                    ?>
                                                    @if(is_object($rankStudent))
                                                        <li>
                                                            <figure><img
                                                                        class="lazy" data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$rankStudent->image, 40, 40) }}"
                                                                        alt="">
                                                            </figure>
                                                            <div class="friend-meta">
                                                                <h4><a href="/profile/{{ @$rankStudent->id }}"
                                                                       title="">{{ @$rankStudent->name }}</a></h4>
                                                                <a rel="nofollow" title="Điểm tích lũy đạt được"
                                                                   class="underline">{{ number_format($quiz_log->accumulated_points, 0, '.', '.') }}
                                                                    điểm</a>
                                                            </div>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div><!-- Shortcuts -->
                                        <div class="widget stick-widget">
                                            <?php
                                            $categories = CommonHelper::getFromCache('categorys_type_4_location_menu_cate', ['categories']);
                                            if (!$categories) {
//                                                $categories = \Modules\EduCourse\Models\Category::where('type', 4)->where('location', 'menu_cate')->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
                                                $categories = \Modules\ThemeEduLDP\Models\Category::where('type', 4)->orderBy('order_no', 'desc')->orderBy('name', 'asc')->where('status', 1)->get();
                                                CommonHelper::putToCache('categorys_type_4_location_menu_cate', $categories, ['categories']);
                                            }
                                            ?>
                                            <h4 class="widget-title">Chuyên mục</h4>
                                            <ul class="naves">

                                                @foreach($categories as $cat)
                                                    @if($cat->parent_id==0)
                                                        <li class="nav-item dropdown">
                                                            <i class="ti-clipboard"></i>
                                                            <a id="navbarDropdown" class="nav-link"
                                                               href="/bai-kiem-tra/{{ @$cat->slug }}"
                                                               title="{{ @$cat->name }}">{{ @$cat->name }}</a>

                                                            @foreach($categories as $sub_menu)
                                                                @if($sub_menu->parent_id==$cat->id)
                                                                    <div class="dropdown-content">
                                                                        <a class="dropdown-item"
                                                                           href="/bai-kiem-tra/{{ @$sub_menu->slug }}"
                                                                           title="{{ @$sub_menu->name }}">{{ @$sub_menu->name }}</a>
                                                                        <div class="dropdown-divider"></div>
                                                                    </div>
                                                                @endif
                                                            @endforeach

                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div><!-- Shortcuts -->
                                    </aside>
                                </div><!-- sidebar -->
                                <div class="col-lg-9">
                                    <div class="row merged20">
                                        @foreach($quizs as $quiz)
                                            <div class="col-lg-6 col-md-6">
                                            <div class="central-meta item">
                                                <div class="classi-pst">
                                                    <div class="row merged10">
                                                        <div class="col-lg-6">
                                                            <div class="image-bunch single">
                                                                <figure><img
                                                                            class="lazy" data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($quiz->image,244,122) }}"
                                                                            alt="{{$quiz->name}}"></figure>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="classi-pst-meta">
                                                                <h6><a href="/bai-kiem-tra/{{$quiz->slug}}.html"
                                                                       title="">{{@$quiz->name}}</a>
                                                                </h6>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <p class="classi-pst-des">
                                                                {!! @$quiz->intro !!}
                                                            </p>
                                                            @if(\Auth::guard('student')->check())
                                                                <a class="active btn btn-info"
                                                                   href="/bai-kiem-tra/{{@$quiz->slug}}.html"
                                                                   style="    background-color: #fa6342;float: right;border-color: #bc482e;">Kiểm
                                                                    tra ngay</a>
                                                            @else
                                                                <a class="active btn btn-info"
                                                                   href="/dang-nhap"
                                                                   style="    background-color: #fa6342;float: right;border-color: #bc482e;">Đăng
                                                                    nhập để kiểm tra</a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-12 paginatee">
                                            {{ @$quizs->appends(Request::all())->links() }}
                                        </div>
                                    </div>
                                </div><!-- centerl meta -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection