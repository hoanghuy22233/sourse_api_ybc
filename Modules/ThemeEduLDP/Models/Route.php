<?php

/**
 * Page Model
 *
 * Page Model manages page operation.
 *
 * @category   Page
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\ThemeEduLDP\Models ;

use Illuminate\Database\Eloquent\Model;
use Modules\EduCourse\Models\LessonItem;

class Route extends Model
{
    protected $table = 'routes';
    public $timestamps = false;
//    protected $fillable = [
//        'student_id', 'lesson_item_id', 'accumulated_points','status'
//    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student(){
        return $this->belongsTo(Student::class, 'student_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lessonitem(){
        return $this->belongsTo(LessonItem::class, 'lesson_item__id','id');
    }
}