<?php

namespace Modules\ThemeEduLDP\Models;


use App\Http\Helpers\CommonHelper;
use DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Modules\EduCourse\Models\Quizzes;

class QuizLog extends Model
{
    protected $table = 'quiz_log';
    protected $fillable = ['student_id', 'quizz_id', 'scores', 'email', 'data', 'accumulated_points', 'synch', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }

    public function quiz()
    {
        return $this->belongsTo(Quizzes::class, 'quizz_id', 'id');
    }

}
