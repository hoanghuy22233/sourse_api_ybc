<?php

/**
 * Page Model
 *
 * Page Model manages page operation.
 *
 * @category   Page
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\ThemeEduLDP\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';

    protected $fillable = [
        'name', 'email', 'tel', 'address', 'birthday', 'content', 'user_ip', 'product_id', 'course_id','view','student_id','quiz_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course(){
        return $this->belongsTo(Course::class, 'course_id','id');
    }
    public function student(){
        return $this->belongsTo(\Modules\EduSettings\Entities\Student::class, 'student_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function quizlog(){
        return $this->belongsTo(QuizLog::class, 'quizlog_id','id');
    }
}