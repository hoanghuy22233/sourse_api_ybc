<?php

namespace Modules\ThemeEduLDP\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use Modules\EduCourse\Models\LessonItem;
use Modules\EduCourse\Models\Misson;
use Modules\ThemeEduLDP\Http\Helpers\CommonHelper;
use Modules\ThemeEduLDP\Models\Contact;
use Modules\ThemeEduLDP\Models\HistoryMisson;
use Modules\ThemeEduLDP\Models\Menus;
use Modules\ThemeEduLDP\Models\Route;


class ContactController extends Controller
{
//    function registerCourseWhenLogin(request $r)
//    {
//
//        $contact = new contact;
//        $contact->name = @\Auth::guard('student')->user()->name;
//        $contact->email = @\Auth::guard('student')->user()->email;
//        $contact->tel = @\Auth::guard('student')->user()->phone;
//        $contact->course_id = @$r->course_id;
//        $contact->student_id = @\Auth::guard('student')->user()->id;
//        $contact->save();
//        try {
//            \Eventy::action('admin.deactiveFromCompany', [
//                'email' => $contact->email,
//                'name' => $contact->name,
//            ]);
//        } catch (\Exception $ex) {
//
//        }
//        return response()->json([
//            'status' => true,
//            'msg' => 'Đăng ký khóa học thành công! Chúng tôi sẽ sớm liên hệ lại với bạn'
//        ]);
//
//    }
//
//    function postContact(request $r)
//    {
//        $contact = new contact;
//        $contact->name = @$r->name;
//        $contact->email = @$r->email;
//        $contact->tel = @$r->phone;
//        $contact->content = @$r->message;
//        $contact->course_id = @$r->course_id;
//        $contact->student_id = @$r->student_id;
//        $contact->save();
//
//        try {
//            \Eventy::action('admin.deactiveFromCompany', [
//                'email' => $contact->email,
//                'name' => $contact->name,
//            ]);
//        } catch (\Exception $ex) {
//
//        }
//        return response()->json([
//            'status' => true,
//            'msg' => 'Đăng ký khóa học thành công! Chúng tôi sẽ sớm liên hệ lại với bạn'
//        ]);
//    }

    function route(request $r)
    {
        $route = new Route();
        $route->student_id = @\Auth::guard('student')->user()->id;
        $route->lesson_item_id = @$r->lesson_item_id;
        $route->accumulated_points = LessonItem::find($r->lesson_item_id)->accumulated_points;
        $route->save();
        \App\Http\Helpers\CommonHelper::flushCache();
        return response()->json([
            'status' => true,
            'msg' => 'Chúc mừng bạn đã hoàn thành bài học ngày hôm nay'
        ]);

    }

    function topic(request $r)
    {
        $topic = new Route();
        $topic->student_id = @\Auth::guard('student')->user()->id;
        $topic->topic_id = @$r->topic_id;
        $topic->save();

        return response()->json([
            'status' => true,
            'msg' => 'Chúc mừng bạn đã hoàn thành bước học này'
        ]);

    }

    function misson(request $r)
    {
        $misson = new HistoryMisson();
        $misson->student_id = @$r->id;
        $misson->misson_id = @$r->misson_id;
        $misson->accumulated_points = Misson::find($r->misson_id)->accumulate_points;
        $misson->save();

        return response()->json([
            'status' => true,
            'msg' => 'Chúc mừng bạn đã hoàn thành nhiệm vụ này'
        ]);

    }

    function noMisson(request $r)
    {
        HistoryMisson::where('misson_id', $r->misson_id)->where('student_id', $r->id)->first()->delete();

        return response()->json([
            'status' => true,
            'msg' => 'Bạn đã hủy hoàn thành nhiệm vụ này'
        ]);

    }
}
