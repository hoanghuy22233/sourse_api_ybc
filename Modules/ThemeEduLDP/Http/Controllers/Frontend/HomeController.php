<?php

namespace Modules\ThemeEduLDP\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Session;


class HomeController extends Controller
{
    function getHome()
    {
        $data['user'] = \Auth::guard('student')->user();
        return view('themeeduldp::pages.home.home',$data);
    }

}
