<?php

namespace Modules\ThemeEduLDP\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeEduLDP\Models\Menus;
use Modules\ThemeEduLDP\Models\Post;

class PostController extends Controller
{
//      Trang chi tiết bài viết
    public function detail($slug1, $slug)
    {
        $data['user'] = \Auth::guard('student')->user();
        $slug = str_replace('.html', '', $slug);

//        $data['post'] = CommonHelper::getFromCache('post_slug');
//        if (!$data['post']) {
            $data['post'] = Post::where('slug', $slug)->first();
//            CommonHelper::putToCache('post_slug', $data['post']);
//        }


        if (!is_object($data['post'])) {
            abort(404);
        }

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
            'meta_description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->name,
            'meta_keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themeeduldp::pages.document.document_detail', $data);
    }

    public function about() {
        $data['post'] = Post::where('slug', 'gioi-thieu-gamali')->where('status', 1)->first();
        if (!is_object($data['post'])) {
            abort(404);
        }
        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
            'meta_description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->name,
            'meta_keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->name,
        ];
        view()->share('pageOption', $pageOption);
        return view('themeeduldp::pages.about.about', $data);
    }
}
