<?php

namespace Modules\ThemeEduLDP\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Cart;


class EduThemeController extends CURDBaseController
{
    protected $module = [
        'code' => 'setting',
        'label' => 'Cấu hình theme',
        'table_name' => 'settings',
        'modal' => '\App\Models\Setting',
        'tabs' => [
            /*
            'social_tab' => [
                'label' => 'Mạng xã hội',
                'icon' => '<i class="kt-menu__link-icon flaticon-settings-1"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'fanpage', 'type' => 'text', 'label' => 'Fanpage FB', 'des' => 'Fanpage Facebook'],
                    ['name' => 'google_map', 'type' => 'text', 'label' => 'google_map', 'des' => 'Fanpage Facebook'],
                    ['name' => 'skype', 'type' => 'text', 'label' => 'skype', 'des' => 'Fanpage Facebook'],
                    ['name' => 'instagram', 'type' => 'text', 'label' => 'instagram', 'des' => 'Fanpage Facebook'],
                ]
            ],*/
            'common_tab' => [
                'label' => 'Chung',
                'icon' => '<i class="kt-menu__link-icon flaticon2-layers-2"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'main_color', 'type' => 'text', 'label' => 'Tông màu chủ đạo', 'des' => 'Tông màu chỉnh nổi bật trên website, màu này nên lấy theo màu của logo'],
                    ['name' => 'logo-top', 'type' => 'file_editor', 'label' => 'Logo top'],
                    ['name' => 'banner_top', 'type' => 'file_editor', 'label' => 'Banner top'],
                    ['name' =>'frontend_head_code','type' =>'textarea','label' =>'admin.insert_code','inner' =>'rows=20'],
                    ['name' =>'frontend_body_code','type' =>'textarea','label' =>'Chèn code vào <body>','inner' =>'rows=20'],
                    ['name' =>'google_calendar_iframe','type' =>'textarea','label' =>'google_calendar_iframe','inner' =>'rows=20'],
                    ['name' => 'link_chat_fanpage_facebook', 'type' => 'text', 'label' => 'Link chat fanpage FB', 'des' => 'Đường link chat với fanpage facebook. VD: https://www.messenger.com/t/hobasoftcom'],
                ],
            ],
            'homepage_tab' => [
                'label' => 'Trang chủ',
                'icon' => '<i class="kt-menu__link-icon flaticon2-open-box"></i>',
                'intro' => '',
                'td' => [
                    /*['type' => 'select', 'options' => [
                        0 => '2 cột sidebar',
                        1 => 'Không có sidebar',
                        2 => 'Landingpage'
                    ], 'class' => '', 'label' => 'Style trang chủ', 'name' => 'homepage_style'],*/
                ]
            ],

            'course_page_tab' => [
                'label' => 'Trang khóa học',
                'icon' => '<i class="kt-menu__link-icon flaticon2-open-box"></i>',
                'intro' => '',
                'td' => [

                ]
            ],
        ]
    ];

    public function setting(Request $request)
    {

        $data['page_type'] = 'list';

        $module = \Eventy::filter('theme.custom_module', $this->module);
        if (!$_POST) {
            $listItem = $this->model->get();
            $tabs = [];
            foreach ($listItem as $item) {
                $tabs[$item->type][$item->name] = $item->value;
            }
            #
            $data['tabs'] = $tabs;
            $data['page_title'] = $module['label'];
            $data['module'] = \Eventy::filter('theme.custom_module', $module);
            return view(config('core.admin_theme') . '.setting.view')->with($data);
        } else {
            foreach ($module['tabs'] as $type => $tab) {
                $data = $this->processingValueInFields($request, $tab['td'], $type . '_');

                //  Tùy chỉnh dữ liệu insert

                foreach ($data as $key => $value) {
                    $item = Setting::where('name', $key)->where('type', $type)->first();
                    if (!is_object($item)) {
                        $item = new Setting();
                        $item->name = $key;
                        $item->type = $type;
                    }
                    $item->value = $value;
                    $item->save();
                }
            }

            if (Schema::hasTable('admin_logs')) {
                $this->adminLog($request, $item = false, 'settingTheme');
            }

            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Cập nhật thành công!');

            if ($request->return_direct == 'save_exit') {
                return redirect('admin/dashboard');
            }

            return back();
        }
    }
}
