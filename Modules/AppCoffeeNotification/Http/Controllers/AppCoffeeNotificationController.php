<?php

namespace Modules\AppCoffeeNotification\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\AppCoffeeNotification\Models\Notifications;

class AppCoffeeNotificationController extends Controller
{
    public function pushToAppMobile($notiData)
    {
//        $this->sendMessage($notiData);
        return true;
    }

    public function sendMessage($data)
    {

        $content = array(
            "en" => $data['content'],
        );
        $hashes_array = array();
        array_push($hashes_array, array(
            "id" => "read-more-button",
            "text" => "Read more",
            "icon" => "http://i.imgur.com/N8SN8ZS.png",
            "url" => $data['url']
        ));
//        $onesignal_app_id = Settings::select(['value'])->where('name', 'onesignal_app_id')->first()->value;
        $onesignal_app_id = '';
//        $onesignal_app_key = Settings::select(['value'])->where('name', 'onesignal_app_key')->first()->value;
        $onesignal_app_key = '';

        $fields = array(
            'app_id' => $onesignal_app_id,
            'filters' => array(
                array("field" => "tag", "key" => "user_id", "relation" => "=", "value" => $data['user_id'])
            ),
            'contents' => $content,
            'web_buttons' => $hashes_array,
        );
        $fields = json_encode($fields);
//        print("\nJSON sent:\n");
//        print($td);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic ' . $onesignal_app_key
        ));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public function read(Request $request)
    {
        $item = Notifications::where('id', $request->id)->where('readed', 0)->first();
        if (!is_object($item)) {
            return response()->json([
                'status' => false
            ]);
        }
        $item->readed = 1;
        if ($item->save()) {
            return response()->json([
                'status' => true
            ]);
        }

    }
}
