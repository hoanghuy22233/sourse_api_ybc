<?php

namespace Modules\ThemeEduAdmin\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeEduAdmin\Models\Menu;
use Validator;

class MenuController extends CURDBaseController
{

    protected $orderByRaw = 'status desc, order_no desc, id desc';

    protected $whereRaw = 'parent_id is null';

    protected $module = [
        'code' => 'menu',
        'table_name' => 'menus',
        'label' => 'Menu',
        'modal' => '\Modules\ThemeEduAdmin\Models\Menu',
        'list' => [
            ['name' => 'name', 'type' => 'name', 'label' => 'Tên'],
            ['name' => 'url', 'type' => 'text', 'label' => 'Đường dẫn'],
//            ['name' => 'parent_id', 'type' => 'custom','td' => 'themeeduadmin::list.td.relation', 'label' => 'Menu cha','object' => 'parent', 'display_field' => 'name'],
            ['name' => 'order_no', 'type' => 'text', 'label' => 'Thứ tự'],
            ['name' => 'location', 'type' => 'select', 'label' => 'Vị trí', 'options' => [
//                'menu' => 'Chọn vị trí',
                'main_menu' => 'Menu chính',
                'menu_footer' => 'Menu chân trang',
                'cate_menu' => 'Menu dọc'
            ],],
//            ['name' => 'type', 'type' => 'select', 'label' => 'Thể loại', 'options' => [
//                0 => '',
//                5 => 'Khóa học',
////                2 => 'Lịch học',
//                1 => 'Tài liệu',
//                4 => 'Bài kiểm tra',
//            ],],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên hiển thị'],
                ['name' => 'parent_id', 'type' => 'custom', 'field' => 'themeeduadmin::form.fields.select_model_tree', 'class' => '', 'label' => 'Menu cha', 'model' => Menu::class],
//            ['name' => 'item_id', 'type' => 'text', 'class' => '', 'label' => 'Id danh mục'],
                ['name' => 'type', 'type' => 'select', 'options' =>
                    [
                        'url' => 'URL',
                    ], 'class' => 'required', 'label' => 'Thể loại', 'group_class' => 'col-md-6'],
                ['name' => 'location', 'type' => 'select', 'options' =>
                    [
//                        'menu' => 'Chọn vị trí',
                        'main_menu' => 'Menu chính',
                        'menu_footer' => 'Menu chân trang',
                    ], 'class' => 'required', 'label' => 'Vị trí', 'group_class' => 'col-md-6'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt', 'value' => 1, 'group_class' => 'col-md-3'],
                ['name' => 'order_no', 'type' => 'number', 'class' => '', 'label' => 'Thứ tự (Số lớn hiển thị trước)', 'value' => 0, 'group_class' => 'col-md-3'],
            ],
            'seo_tab' => [
                ['name' => 'url', 'type' => 'slug', 'class' => 'required', 'label' => 'URL', 'des' => 'Đường dẫn bài viết trên thanh địa chỉ'],
//                ['name' => 'meta_title', 'type' => 'text', 'label' => 'Meta title'],
//                ['name' => 'meta_description', 'type' => 'text', 'label' => 'Meta description'],
//                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên menu',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'location' => [
            'label' => 'Vị trí',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Vị trí',
                'main_menu' => 'Menu chính',
                'cate_menu' => 'Menu dọc'
            ]
        ],
//        'type' => [
//            'label' => 'Thể loại',
//            'type' => 'select',
//            'query_type' => '=',
//            'options' => [
//                '' => 'Thể loại',
//                'url' => 'URL',
//            ]
//        ],
//        'order_no' => [
//            'label' => 'Thứ tự',
//            'type' => 'number',
//            'query_type' => '='
//        ],
    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);

        return view('themeeduadmin::menu.list')->with($data);
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('themeeduadmin::menu.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);
//                        $this->adminLog($request,$this->model,'add');
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {

        try {
            $item = $this->model->find($request->id);
            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);

                return view('themeeduadmin::menu.edit')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    $data['url'] = $request->get('url', '');
                    //  Tùy chỉnh dữ liệu insert

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
//                        $this->adminLog($request,$item,'edit');
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
//            $this->adminLog($request,$item,'publish');
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
//            $this->adminLog($request,$item,'delete');
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
//            $this->adminLog($request,$ids,'multi_delete');
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
