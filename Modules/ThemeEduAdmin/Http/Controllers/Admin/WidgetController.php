<?php

namespace Modules\ThemeEduAdmin\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;

class WidgetController extends CURDBaseController
{
    protected $orderByRaw = 'status desc, order_no desc, id desc';

    protected $module = [
        'code' => 'widget',
        'table_name' => 'widgets',
        'label' => 'Widget',
        'modal' => '\Modules\ThemeEduAdmin\Models\Widget',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'location', 'type' => 'select', 'label' => 'Vị trí hiển thị', 'options' => [
                'home_sidebar_left' => 'Trang chủ - cột trái',
                'home_sidebar_right' => 'Trang chủ - cột phải',
                'home_content' => 'Trang chủ - cột giữa',
                'home_content_bottom' => 'Trang chủ - cột giữa - dưới',
                'copyright' => 'Copyright',
                'footer1' => 'Chân trang - cột 1',
                'footer2' => 'Chân trang - cột 2',
                'footer3' => 'Chân trang - cột 3',
                'footer4' => 'Chân trang - cột 4',
                'khoa_hoc' => 'Trang khóa học',
            ],],
            ['name' => 'type', 'type' => 'select', 'label' => 'Loại', 'options' => [
                'html' => 'Html',
                'textarea' => 'Textarea',
                'banner_slides' => 'Banner - slides',
                'posts' => 'Khối tin tức',
                'other' => 'Khác',
            ],],
            ['name' => 'order_no', 'type' => 'text', 'label' => 'Thứ tự'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => '', 'label' => 'Tên'],
                ['name' => 'content', 'type' => 'custom', 'field' => 'themeeduadmin::form.fields.widget_content', 'label' => 'Nội dung',
                    'inner' => 'rows=20', 'height' => '300px'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'Thứ tự', 'value' => 0, 'group_class' => 'col-md-4'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt', 'value' => 1, 'group_class' => 'col-md-4'],
            ],

            'info_tab' => [
                ['name' => 'location', 'type' => 'select', 'options' => [
                    
                    'home_sidebar_left' => 'Trang chủ - cột trái',
                    'home_sidebar_right' => 'Trang chủ - cột phải',
                    'home_content' => 'Trang chủ - cột giữa',
                    'home_content_bottom' => 'Trang chủ - cột giữa - dưới',
                    'copyright' => 'Copyright',
                    'footer1' => 'Chân trang - cột 1',
                    'footer2' => 'Chân trang - cột 2',
                    'footer3' => 'Chân trang - cột 3',
                    'footer4' => 'Chân trang - cột 4',
                    'khoa_hoc' => 'Trang khóa học',
                ], 'label' => 'Vị trí hiển thị'],
                ['name' => 'type', 'type' => 'select', 'options' => [
                    'html' => 'Html',
                    'textarea' => 'Textarea',
                    'banner_slides' => 'Banner - slides',
                    'posts' => 'Khối tin tức',
                    'other' => 'Khác',
                ], 'label' => 'Loại'],
//                ['name' => 'config', 'type' => 'textarea', 'label' => 'Cấu hình'],
                ['name' => 'config', 'type' => 'custom', 'field' => 'themeeduadmin::form.fields.dynamic', 'class' => 'form-action', 'label' => 'Cấu hình', 'cols' => ['key', 'Giá trị'], 'des' => 'Key bao gồm: view, location, limit, category_id, where']
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'location' => [
            'label' => 'Vị trí',
            'type' => 'select',
            'options' => [
                '' => 'Vị trí',
                'home_sidebar_left' => 'Trang chủ - cột trái',
                'home_sidebar_right' => 'Trang chủ - cột phải',
                'home_content' => 'Trang chủ - cột giữa',
                'home_content_bottom' => 'Trang chủ - cột giữa - dưới',
                'copyright' => 'Copyright',
                'footer1' => 'Chân trang - cột 1',
                'footer2' => 'Chân trang - cột 2',
                'footer3' => 'Chân trang - cột 3',
                'footer4' => 'Chân trang - cột 4',
                'khoa_hoc' => 'Trang khóa học',
            ],
            'query_type' => '='
        ],
        'type' => [
            'label' => 'Loại',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Loại',
                'html' => 'Html',
                'textarea' => 'Textarea',
                'banner_slides' => 'Banner - slides',
                'posts' => 'Khối tin tức',
                'other' => 'Khác',
            ]
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Ẩn',
                1 => 'Hiển thị',
            ],
            'query_type' => '='
        ],
    ];

    public function getIndex(Request $r)
    {
        

        $data = $this->getDataList($r);

        return view('themeeduadmin::widget.list')->with($data);
    }

    public function add(Request $r)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($r);
                return view('themeeduadmin::widget.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($r->all(), [
                    'location' => 'required'
                ], [
                    'location.required' => 'Bắt buộc phải nhập vị trí',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($r, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    if ($r->has('config_key')) {
                        $val = [];
                        foreach ($r->config_key as $k => $key) {
                            if ($key != null && $r->config_value[$k] != null) {
                                $val[$key] = $r->config_value[$k];
                            }
                        }
                        $data['config'] = json_encode($val);
                    }

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        $this->afterAddLog($r, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($r->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($r->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($r->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $r)
    {
        try {
                

            $item = $this->model->find($r->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($r, $item);
                return view('themeeduadmin::widget.edit')->with($data);
            } else if ($_POST) {

                    

                $validator = Validator::make($r->all(), [
                    'location' => 'required'
                ], [
                    'location.required' => 'Bắt buộc phải nhập vị trí',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($r, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    if ($r->has('config_key')) {
                        $val = [];
                        foreach ($r->config_key as $k => $key) {
                            if ($key != null && $r->config_value[$k] != null) {
                                $val[$key] = $r->config_value[$k];
                            }
                        }
                        $data['config'] = json_encode($val);
                    }

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {

                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($r->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($r->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($r->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($r)
    {
        $contact_info = [];
        if ($r->has('contact_info_name')) {
            foreach ($r->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $r->contact_info_tel[$k],
                        'email' => $r->contact_info_email[$k],
                        'note' => $r->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $r)
    {
        try {

                

            $id = $r->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$r->column} == 0)
                $item->{$r->column} = 1;
            else
                $item->{$r->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$r->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $r)
    {
        try {

                

            $item = $this->model->find($r->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $r)
    {
        try {

                

            $ids = $r->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
