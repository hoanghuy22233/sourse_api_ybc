<?php

namespace Modules\ThemeEduAdmin\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeEduAdmin\Models\Category;
use Modules\ThemeEduAdmin\Models\Menu;
use Validator;

class PostController extends CURDBaseController
{
    protected $module = [
        'code' => 'post',
        'table_name' => 'posts',
        'label' => 'Bài viết',
        'modal' => '\Modules\ThemeEduAdmin\Models\Post',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên bài viết'],
            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'themeeduadmin::list.td.multi_cat', 'label' => 'Danh mục ', 'object' => 'category'],
//            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'themeeduadmin::list.td.multi_cat', 'label' => 'Danh mục lịch học', 'object' => 'category_post'],
//            ['name' => 'tags', 'type' => 'custom', 'td' => 'themeeduadmin::list.td.multi_cat', 'label' => 'Từ khóa', 'object' => 'tag_post'],
//            ['name' => 'slug', 'type' => 'text', 'label' => 'Đường dẫn tĩnh'],
//            ['name' => 'tin_hot', 'type' => 'status', 'label' => 'Tin hot'],
            ['name' => 'order_no', 'type' => 'number', 'label' => 'Ưu tiên'],
            ['name' => 'show_homepage', 'type' => 'status', 'label' => 'TH. Trang chủ'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trang thái'],
            ['name' => 'view_frontend', 'type' => 'custom', 'td' => 'themeeduadmin::list.td.view_frontend', 'label' => 'Xem'],
//            ['name' => 'faq', 'type' => 'status', 'label' => 'FAQ?'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên'],
                ['name' => 'multi_cat', 'type' => 'custom', 'field' => 'themeeduadmin::form.fields.multi_cat', 'label' => 'Danh mục', 'model' => Category::class,
                    'object' => 'category_post', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=1', 'des' => 'Danh mục đầu tiên chọn là danh mục chính'],
//                ['name' => 'multi_cat', 'type' => 'custom', 'field' => 'themeeduadmin::form.fields.multi_cat', 'label' => 'Danh mục lịch học', 'model' => \Modules\ThemeEduAdmin\Models\Category::class,
//                    'object' => 'category_post', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=1', 'des' => 'Danh mục đầu tiên chọn là danh mục chính'],
//                ['name' => 'type', 'type' => 'select', 'options' => [
//                    0 => 'Chọn chuyên mục',
//                    1 => 'Lịch học',
//                    2 => 'Tài liệu',
//                ], 'class' => 'required', 'label' => 'Chọn thể loại'],
//                ['name' => 'tags', 'type' => 'custom', 'field' => 'themeeduadmin::form.fields.tags', 'label' => 'Từ khóa bài viết', 'model' => \Modules\ThemeEduAdmin\Models\Category::class,
//                    'object' => 'tag_post', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=2'],
                ['name' => 'intro', 'type' => 'custom', 'field' => 'themeeduadmin::form.fields.count_char_intro', 'class' => '', 'label' => 'Mô tả ngắn (Tối đa 120 ký tự)'],
                ['name' => 'content', 'type' => 'textarea_editor', 'label' => 'Nội dung'],
                ['name' => 'include_view', 'type' => 'text', 'label' => 'Chèn mã form liên hệ'],
                ['name' => 'address', 'type' => 'text', 'label' => 'Địa điểm'],
                ['name' => 'date_place', 'type' => 'datetime-local', 'label' => 'Ngày sự kiện diễn ra'],
//                ['name' => 'faq', 'type' => 'checkbox', 'label' => 'FAQ','value' => 1, 'group_class' => 'col-md-2'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt','value' => 1, 'group_class' => 'col-md-3'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'Thứ tự ưu tiên', 'value' => 0, 'group_class' => 'col-md-3', 'des' => 'Số to ưu tiên hiển thị trước'],
//                ['name' => 'tin_hot', 'type' => 'checkbox', 'label' => 'Tin hot', 'group_class' => 'col-md-3'],
                ['name' => 'show_homepage', 'type' => 'checkbox', 'label' => 'Hiển thị trang chủ', 'group_class' => 'col-md-3'],
//                ['name' => 'featured', 'type' => 'checkbox', 'class' => '', 'label' => 'Nổi bật', 'value' => 0, 'group_class' => 'col-md-3'],
                ['name' => 'type_page', 'type' => 'select', 'class' => '', 'label' => 'Thể loại', 'group_class' => 'col-md-3', 'options' => [
                    'post' => 'Bài viết',
                    'page' => 'Trang tĩnh',
                ],],
            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh bài viết'],
            ],

            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'Slug', 'des' => 'Đường dẫn bài viết trên thanh địa chỉ'],
                ['name' => 'meta_title', 'type' => 'custom', 'field' => 'themeeduadmin::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta title', 'max_char' => 1000],
                ['name' => 'meta_description', 'type' => 'custom', 'field' => 'themeeduadmin::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta description', 'max_char' => 1000],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, tên, đường dẫn',
        'fields' => 'id, name, slug'
    ];

    protected $filter = [
        'category_id' => [
            'label' => 'Danh mục',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'category_post',
            'model' => \Modules\ThemeEduAdmin\Models\Category::class,
            'where' => 'type=1',
            'query_type' => 'custom'
        ],
        'tags' => [
            'label' => 'Từ khóa bài viết',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'tag_post',
            'model' => \Modules\ThemeEduAdmin\Models\Category::class,
            'query_type' => 'custom'
        ],

        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
        ],

    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('themeeduadmin::post.list')->with($data);
    }
    public function appendWhere($query, $request)
    {

        //  Lọc theo danh mục
        if (!is_null($request->get('category_id'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }
        if (!is_null($request->get('tags'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {

                $data = $this->getDataAdd($request);

                return view('themeeduadmin::post.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('tags')) {
                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
                    }
                    $data['admin_id'] = \Auth::guard('admin')->id();

                    #

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
//        try {
            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('themeeduadmin::post.edit')->with($data);
            } else if ($_POST) {

                

                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());


                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('tags')) {
                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
                    }
                    $data['admin_id'] = \Auth::guard('admin')->id();

                    #

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
//        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
////            dd($ex->getMessage());
////            CommonHelper::one_time_message('error', $ex->getMessage());
//            return redirect()->back()->withInput();
//        }
    }

    public function getPublish(Request $request)
    {
        try {

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
