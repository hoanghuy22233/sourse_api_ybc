<?php

namespace Modules\ThemeEduAdmin\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ThemeEduAdminServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            // Cấu hình Core
//            $this->registerTranslations();
//            $this->registerConfig();
//            $this->registerFactories();
//            $this->loadMigrationsFrom(module_path('ThemeEduAdmin', 'Database/Migrations'));


            //  Custom setting

            //  Cấu hình menu trái
            $this->rendAsideMenu();

            //  Đăng ký quyền
            $this->registerPermission();
        }

        $this->registerViews();
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['theme',
                'contact_view', 'contact_add', 'contact_edit', 'contact_delete', 'contact_edit',
                'post_view', 'post_add', 'post_edit', 'post_delete', 'post_publish','tag'
                ]);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('themeeduadmin::partials.aside_menu.aside_menu_dashboard_after');
        }, 0, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('ThemeEduAdmin', 'Config/config.php') => config_path('themeeduadmin.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('ThemeEduAdmin', 'Config/config.php'), 'themeeduadmin'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/themeeduadmin');

        $sourcePath = module_path('ThemeEduAdmin', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/themeeduadmin';
        }, \Config::get('view.paths')), [$sourcePath]), 'themeeduadmin');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/themeeduadmin');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'themeeduadmin');
        } else {
            $this->loadTranslationsFrom(module_path('ThemeEduAdmin', 'Resources/lang'), 'themeeduadmin');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('ThemeEduAdmin', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
