<?php
namespace Modules\ThemeEduAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\JdesCategory\Models\CategoryProduct;

class Post extends Model
{

    protected $table = 'posts';
    public $timestamps = false;

    protected $fillable = [
        'name', 'image', 'intro','content'
    ];

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

}
