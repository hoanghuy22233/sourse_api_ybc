<tr data-row="0" class="kt-datatable__row" style="left: 0px;">
    <td style="display: none;"
        class="id id-{{ $item->id }}">{{ $item->id }}</td>
    <td class="kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check"
        data-field="ID"><span style="width: 20px;"><label
                    class="kt-checkbox kt-checkbox--single kt-checkbox--solid"><input
                        name="id[]"
                        type="checkbox" class="ids"
                        value="{{ $item->id }}">&nbsp;<span></span></label></span>
    </td>
    @foreach($module['list'] as $k => $field)
        <td data-field="{{ @$field['name'] }}"
            class="kt-datatable__cell item-{{ @$field['name'] }}">
            {{ $k == 0 ? $prefix : '' }}
            @if($field['type'] == 'custom')
                @include($field['td'], ['field' => $field])
            @else
                @include(config('core.admin_theme').'.list.td.'.$field['type'])
            @endif
        </td>
    @endforeach
</tr>