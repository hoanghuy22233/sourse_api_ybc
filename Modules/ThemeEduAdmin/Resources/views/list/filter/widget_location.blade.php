<?php
$model = new $field['model'];
$data = $model->select($field['display_field'])->where($field['display_field'], '!=', '')->orderBy($field['display_field'], 'asc')->groupBy($field['display_field'])->get();
$value[] = @$_GET[$field['display_field']];
?>
<div class="col-sm-6 col-lg-3 kt-margin-b-10-tablet-and-mobile list-filter-item">
    <label>{{ @$field['label'] }}:</label>
    <select class="form-control {{ $field['class'] or '' }}" id="{{ $field['name'] }}" {!! @$field['inner'] !!}
    {{ strpos(@$field['class'], 'require') !== false ? 'required' : '' }}
    name="{{ $field['name'] }}@if(isset($field['multiple'])){{ '[]' }}@endif"
            @if(isset($field['multiple'])) multiple @endif>
        <option value=""></option>
        @foreach ($data as $v)
            <option value="{{ $v->{$field['display_field']} }}" {{ in_array($v->{$field['display_field']}, $value) ? 'selected':'' }}>{{ $v->{$field['display_field']} }}</option>
        @endforeach
    </select>
</div>