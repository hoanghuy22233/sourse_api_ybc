@if($item->status == 1)
    <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill" data-id="{{ $item->id }}"
          data-column="{{ $field['name'] }}">Đã tham gia</span>
@elseif($item->status == 0)
    <span class="kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill" data-id="{{ $item->id }}"
          data-column="{{ $field['name'] }}">Đang chờ</span>
@else
    <span class="kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill" data-id="{{ $item->id }}"
          data-column="{{ $field['name'] }}">Hủy</span>
@endif