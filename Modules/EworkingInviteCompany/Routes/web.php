<?php

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'invite_company'], function () {
        Route::get('', 'Admin\InviteCompanyController@getIndex')->name('invite_company')->middleware('permission:invite_company_view');
        Route::get('publish', 'Admin\InviteCompanyController@getPublish')->name('invite_company.publish')->middleware('permission:invite_company_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\InviteCompanyController@add')->middleware('permission:invite_company_add');
        Route::get('delete/{id}', 'Admin\InviteCompanyController@delete')->middleware('permission:invite_company_delete');
        Route::post('multi-delete', 'Admin\InviteCompanyController@multiDelete')->middleware('permission:invite_company_delete');

        Route::get('search-for-select2', 'Admin\InviteCompanyController@searchForSelect2')->name('invite_company.search_for_select2')->middleware('permission:invite_company_view');

        Route::get('get-attr-service', 'Admin\InviteCompanyController@getAttrService');
        Route::get('get-location-company', 'Admin\InviteCompanyController@getLocationCompany');
        Route::get('ajax-accept-join-company', 'Admin\InviteCompanyController@ajaxAcceptJoinCompany');
        Route::get('{id}', 'Admin\InviteCompanyController@update')->middleware('permission:invite_company_view');
        Route::post('{id}', 'Admin\InviteCompanyController@update')->middleware('permission:invite_company_edit');
    });

    Route::group(['prefix' => 'invite_history'], function () {
        Route::get('', 'Admin\InviteHistoryController@getIndex');
        Route::get('publish', 'Admin\InviteHistoryController@getPublish');
        Route::get('delete/{id}', 'Admin\InviteHistoryController@delete');
        Route::post('multi-delete', 'Admin\InviteHistoryController@multiDelete');
    });
});