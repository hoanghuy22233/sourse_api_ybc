<?php

namespace Modules\ThemeCLB\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';

    protected $guarded = [];

    public function course() {
        return $this->belongsTo(\Modules\ThemeCLB\Models\Course::class, 'course_id', 'id');
    }

}
