<?php


namespace Modules\ThemeCLB\Models ;

use Illuminate\Database\Eloquent\Model;

class KhkdLog extends Model
{
    protected $table = 'khkd_log';

    public function course() {
        return $this->belongsTo(\Modules\ThemeCLB\Models\Course::class, 'course_id', 'id');
    }
}