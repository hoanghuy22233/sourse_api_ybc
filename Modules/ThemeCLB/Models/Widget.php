<?php


namespace Modules\ThemeCLB\Models ;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    protected $table = 'widgets';

    protected $fillable = ['name', 'content', 'location'];
}