<?php

namespace Modules\ThemeCLB\Helpers;

use Modules\ThemeCLB\Models\Category;
use Modules\ThemeCLB\Models\Menu;
use View;
use Session;
use App\Http\Helpers\CommonHelper as CoreHelper;

class ThemeCLBHelper
{

    public static function getMenusByLocation($location, $limit = 10, $get_childs = true)
    {
        $data = CoreHelper::getFromCache('menus_by_location_' . $location, ['menus']);
        if (!$data) {
            $menus = Menu::where('location', $location)->where(function ($query) {
                $query->where('parent_id', null)->orwhere('parent_id', 0);
            })->where('status', 1)->where('type', 'url')->where('location', $location)->orderBy('order_no', 'desc')->limit($limit)->get();

            foreach ($menus as $menu) {
                //  Lấy menu cấp 1
                $item = [
                    'name' => $menu->name,
                    'url' => $menu->url,
                    'childs' => []
                ];

                //  Lấy menu cấp 2
                $menu2Childs = $menu->childs;
                foreach ($menu2Childs as $child2) {
                    $child2_data = [
                        'name' => $child2->name,
                        'url' => $child2->url,
                        'childs' => []
                    ];

                    //  Lấy menu cấp 3
                    $menu3Childs = $child2->childs;
                    foreach ($menu3Childs as $menu3) {
                        $child3_data = [
                            'name' => $menu3->name,
                            'url' => $menu3->url,
                            'childs' => []
                        ];

                        //  Gán menu cấp 3 vào mảng con của cấp 2
                        $child2_data['childs'][] = $child3_data;
                    }

                    //  Gán menu cấp 2 vào mảng con của cấp 1
                    $item['childs'][] = $child2_data;
                }
                $data[] = $item;
            }

            CoreHelper::putToCache('menus_by_location_' . $location, $data, ['menus']);

            if (!$data) {
                return [];
            }
        }

        return $data;
    }



    public static function getPostSlug($post, $type = 'object')
    {
        try {
            $slug = '';
            $catPost = Category::select(['slug', 'parent_id'])->whereIn('id', explode('|', $post->multi_cat))->first();

            if (is_object($catPost)) {
                $catProduct = Category::select('slug')->where('id', $catPost->parent_id)->first();
                if (isset($catProduct) && is_object($catProduct)) {

                    $slug .= '/' . $catProduct->slug;
                }else{
                    $slug .= '/' . $catPost->slug;
                }
            }
            return $slug . '/' . $post->slug . '.html';
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }
}