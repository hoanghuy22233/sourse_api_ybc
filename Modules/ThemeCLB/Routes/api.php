<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {

    //  Homepage
    Route::get('homepage', 'Admin\HomeController@index');

    Route::get('menu', 'Admin\HomeController@menu');

    Route::get('introduce-app', function () {
        return response()->json([
            'status' => true,
            'msg' => '',
            'data' => [
                [
                    'image' => 'https://ybcstartup.com/public/frontend/themes/YBC/img/logo.png',
                    'name' => 'Khả năng kinh doanh',
                    'content' => 'Khả năng kinh doanh phụ thuộc vào độ kiên trì của bản thân'
                ],
                [
                    'image' => 'https://ybcstartup.com/public/frontend/themes/YBC/img/logo.png',
                    'name' => 'Định hướng chuyên nghành',
                    'content' => 'Định hướng chuyên nghành, kinh doanh thành công'
                ],
                [
                    'image' => 'https://ybcstartup.com/public/frontend/themes/YBC/img/logo.png',
                    'name' => 'Định hướng chuyên nghành',
                    'content' => 'Định hướng chuyên nghành, kinh doanh thành công'
                ],
            ]
        ]);
    });

    //  Post
    Route::group(['prefix' => 'posts'], function () {
        Route::get('', 'Admin\PostController@index');
        Route::get('{id}', 'Admin\PostController@show');
    });

    //  Category
    Route::group(['prefix' => 'categories'], function () {
        Route::get('', 'Admin\CategoryController@index');
        Route::get('{id}', 'Admin\CategoryController@show');
    });


    //  Banner
    Route::group(['prefix' => 'banners'], function () {
        Route::get('', 'Admin\BannerController@index');

    });

    //  courses
    Route::group(['prefix' => 'courses'],function () {
        Route::get('', 'Admin\CourseController@index');
        Route::post('', 'Admin\CourseController@store');
        Route::get('{id}', 'Admin\CourseController@show');
        Route::post('{id}', 'Admin\CourseController@update');
        Route::delete('{id}', 'Admin\CourseController@delete');

    });
});

