<?php

namespace Modules\ThemeCLB\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Admin;
use Illuminate\Support\Facades\Request;
use Modules\ThemeCLB\Http\Requests\CreateRequest;

use Modules\ThemeCLB\Http\Requests\RegisterEmail;
use Modules\ThemeCLB\Http\Requests\RegisterMember;
use Modules\ThemeCLB\Models\Contact;
use Session;


class HomeController extends Controller
{


    function getIndex()
    {

        $data['user'] = \Auth::guard('student')->user();
        return view('themeclb::pages.home.home',$data);
    }

    public function gioiThieu() {
        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Giới thiệu về YBC Startup',
            'meta_description' => 'Giới thiệu về YBC Startup',
            'meta_keywords' => 'Giới thiệu về YBC Startup',
        ];
        view()->share('pageOption', $pageOption);

        $data = [];

        return view('themeclb::pages.profile_page')->with($data);
    }

    public function getSigUpMember()
    {
        $data['page_title'] = 'Đăng ký thành viên';
        $data['page_type'] = 'list';
        return view('themeclb::YBC.home', $data);
    }
    public function postSigUpMember(RegisterEmail $request)
    {
        $contact = new Contact();
        $contact->name = @$request->name;
        $contact->email = @$request->email;
        $contact->tel = @$request->tel;
        $contact->content = @$request->content;
        $contact->save();

        CommonHelper::one_time_message('success', 'Bạn đã đăng ký thành viên thành công!');
        return redirect('/');
    }


    public function listMember($role_name) {
        $admins = Admin::leftJoin('role_admin', 'role_admin.admin_id', '=', 'admin.id')
            ->leftJoin('roles', 'roles.id', '=', 'role_admin.role_id')
            ->where('roles.name', $role_name)
            ->selectRaw('admin.id, admin.email, admin.name, admin.image, admin.tel, admin.intro, roles.display_name as role_name')->paginate(30);

        $data['admins'] = $admins;

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Danh sách thành viên ' . @$admins[0]->role_name,
            'meta_description' => 'Danh sách thành viên' . @$admins[0]->role_name,
            'meta_keywords' => 'Danh sách thành viên' . @$admins[0]->role_name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themeclb::pages.admin.list_by_role')->with($data);
    }
    public function listallMember() {
        $admins = Admin::leftJoin('role_admin', 'role_admin.admin_id', '=', 'admin.id')
            ->leftJoin('roles', 'roles.id', '=', 'role_admin.role_id')
            ->whereNotIn('roles.id', [1])
            ->selectRaw('admin.id, admin.email, admin.name, admin.image, admin.tel, admin.intro, roles.display_name as role_name')->paginate(30);
        $data['admins'] = $admins;
        $data['title'] = 'Tất cả thành viên';

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Tất cả thành viên',
            'meta_description' => 'Tất cả thành viên',
            'meta_keywords' => 'Tất cả thành viên',
        ];
        view()->share('pageOption', $pageOption);

        return view('themeclb::pages.admin.list_by_role')->with($data);
    }
}
