<?php

namespace Modules\ThemeCLB\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use Modules\EduCourse\Models\LessonItem;
use Modules\EduCourse\Models\Misson;
use Modules\ThemeCLB\Http\Helpers\CommonHelper;
use Modules\ThemeCLB\Http\Requests\RegisterMember;
use Modules\ThemeCLB\Models\Contact;
use Modules\ThemeCLB\Models\HistoryMisson;
use Modules\ThemeCLB\Models\Menus;
use Modules\ThemeCLB\Models\Route;


class ContactController extends Controller
{
//    function registerCourseWhenLogin(request $r)
//    {
//
//        $contact = new contact;
//        $contact->name = @\Auth::guard('student')->user()->name;
//        $contact->email = @\Auth::guard('student')->user()->email;
//        $contact->tel = @\Auth::guard('student')->user()->phone;
//        $contact->course_id = @$r->course_id;
//        $contact->student_id = @\Auth::guard('student')->user()->id;
//        $contact->save();
//        try {
//            \Eventy::action('admin.deactiveFromCompany', [
//                'email' => $contact->email,
//                'name' => $contact->name,
//            ]);
//        } catch (\Exception $ex) {
//
//        }
//        return response()->json([
//            'status' => true,
//            'msg' => 'Đăng ký khóa học thành công! Chúng tôi sẽ sớm liên hệ lại với bạn'
//        ]);
//
//    }
//
    function postContact(RegisterMember $r )
    {
        $contact = new Contact();
        $contact->name = @$r->name;
        $contact->email = @$r->email;
        $contact->tel = @$r->tel;
        $contact->level = @$r->level;
        $contact->content = @$r->content;
        $contact->save();
        \App\Http\Helpers\CommonHelper::one_time_message('success', 'Gửi thành công');
        return redirect()->back();
    }

    public function getContact()
    {
        $data['page_title'] = 'Liên hệ';
        $data['page_type'] = 'list';
        return view('themeclb::pages.contact.contact', $data);
    }

    function route(request $r)
    {
        $route = new Route();
        $route->student_id = @\Auth::guard('student')->user()->id;
        $route->lesson_item_id = @$r->lesson_item_id;
        $route->accumulated_points = LessonItem::find($r->lesson_item_id)->accumulated_points;
        $route->save();
        \App\Http\Helpers\CommonHelper::flushCache($this->module['table_name']);
        return response()->json([
            'status' => true,
            'msg' => 'Chúc mừng bạn đã hoàn thành bài học ngày hôm nay'
        ]);

    }

    function topic(request $r)
    {
        $topic = new Route();
        $topic->student_id = @\Auth::guard('student')->user()->id;
        $topic->topic_id = @$r->topic_id;
        $topic->save();

        return response()->json([
            'status' => true,
            'msg' => 'Chúc mừng bạn đã hoàn thành bước học này'
        ]);

    }

    function misson(request $r)
    {
        $misson = new HistoryMisson();
        $misson->student_id = @$r->id;
        $misson->misson_id = @$r->misson_id;
        $misson->accumulated_points = Misson::find($r->misson_id)->accumulate_points;
        $misson->save();

        return response()->json([
            'status' => true,
            'msg' => 'Chúc mừng bạn đã hoàn thành nhiệm vụ này'
        ]);

    }

    function noMisson(request $r)
    {
        HistoryMisson::where('misson_id', $r->misson_id)->where('student_id', $r->id)->first()->delete();

        return response()->json([
            'status' => true,
            'msg' => 'Bạn đã hủy hoàn thành nhiệm vụ này'
        ]);

    }
}
