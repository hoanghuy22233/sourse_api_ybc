<?php

namespace Modules\ThemeCLB\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterEmail extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'content' => 'required',
            'tel' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'name' => 'Họ tên',
            'email' => 'Email',
            'content' => 'Ghi chú',
            'tel' => 'Số điện thoại',
        ];
    }
}
