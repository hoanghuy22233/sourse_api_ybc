<script type="text/javascript">
    window._wpemojiSettings = {
        "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
        "ext": ".png",
        "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
        "svgExt": ".svg",
        "source": {
            "concatemoji": "https:\/\/live.linethemes.com\/cosine\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.9"
        }
    };
    !function (a, b, c) {
        function d(a, b) {
            var c = String.fromCharCode;
            l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
            var d = k.toDataURL();
            l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
            var e = k.toDataURL();
            return d === e
        }

        function e(a) {
            var b;
            if (!l || !l.fillText) return !1;
            switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                case "flag":
                    return !(b = d([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819])) && (b = d([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]), !b);
                case "emoji":
                    return b = d([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340]), !b
            }
            return !1
        }

        function f(a) {
            var c = b.createElement("script");
            c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
        }

        var g, h, i, j, k = b.createElement("canvas"),
            l = k.getContext && k.getContext("2d");
        for (j = Array("flag", "emoji"), c.supports = {
            everything: !0,
            everythingExceptFlag: !0
        }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
        c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
            c.DOMReady = !0
        }, c.supports.everything || (h = function () {
            c.readyCallback()
        }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function () {
            "complete" === b.readyState && c.readyCallback()
        })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
    }(window, document, window._wpemojiSettings);
</script>
<style type="text/css">
    img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
    }
</style>

<link rel="stylesheet" href="{{URL::asset('/public/frontend/themes/YBC/css/login.css')}}">
<link rel='stylesheet' id='jquery.prettyphoto-css'
      href='{{ URL::asset('/public/frontend/themes/YBC/wp-content/plugins/wp-video-lightbox/css/prettyPhoto.css')}}'
      type='text/css' media='all'/>
<link rel='stylesheet' id='video-lightbox-css'
      href='{{ URL::asset('/public/frontend/themes/YBC/wp-content/plugins/wp-video-lightbox/wp-video-lightbox.css')}}'
      type='text/css' media='all'/>
<link rel='stylesheet' id='wp-block-library-css'
      href='{{ URL::asset('/public/frontend/themes/YBC/wp-includes/css/dist/block-library/style.min.css')}}'
      type='text/css' media='all'/>
<link rel='stylesheet' id='wc-block-style-css'
      href='{{ URL::asset('/public/frontend/themes/YBC/wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css')}}'
      type='text/css' media='all'/>
<link rel='stylesheet' id='contact-form-7-css'
      href='{{ URL::asset('/public/frontend/themes/YBC/wp-content/plugins/contact-form-7/includes/css/styles.css')}}'
      type='text/css' media='all'/>
<link rel='stylesheet' id='rs-plugin-settings-css'
      href='{{ URL::asset('/public/frontend/themes/YBC/wp-content/plugins/revslider/public/assets/css/rs6.css')}}'
      type='text/css' media='all'/>
<style id='rs-plugin-settings-inline-css' type='text/css'>
    #rs-demo-id {
    }
</style>
<style id='woocommerce-inline-inline-css' type='text/css'>
    .woocommerce form .form-row .required {
        visibility: visible;
    }
</style>
<link rel='stylesheet' id='theme-components-css'
      href='{{ URL::asset('/public/frontend/themes/YBC/wp-content/themes/cosine/assets/css/components.css')}}'
      type='text/css' media='all'/>
<link rel='stylesheet' id='theme-css'
      href='{{ URL::asset('/public/frontend/themes/YBC/wp-content/themes/cosine/assets/css/style.css')}}'
      type='text/css' media='all'/>
<style id='theme-inline-css' type='text/css'>
    body {
        color: #2f4862;
        font-size: 15px;
        font-family: Roboto;
        font-weight: 0;
        font-style: normal;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-family: Roboto;
        font-weight: 700;
        font-style: normal;
    }

    h1 {
        font-size: 48px;
    }

    h2 {
        font-size: 36px;
    }

    h3 {
        font-size: 30px;
    }

    h4 {
        font-size: 24px;
    }

    h5 {
        font-size: 18px;
    }

    h6 {
        font-size: 14px;
    }

    #site-header #site-navigator .menu > li a {
        color: #174c81;
        font-size: 14px;
        font-family: Roboto;
        font-weight: 700;
        font-style: normal;
    }

    #masthead .brand {
        margin-top: 29px;
        margin-bottom: 37px;
    }

    #masthead .brand .logo img {
        width: 70px;
        /*height: 30px;*/
    }

    #headerbar {
        background-color: ;
        color: ;
    }

    body.layout-boxed {
        background-color: #fff;
    }

    #site-content #page-header {
        background-color: #d8e7ef;
        background-image: url(https://live.linethemes.com/cosine/wp-content/uploads/2016/04/page-title-bg.png);
        background-position: top left;
        background-repeat: no-repeat;
        background-attachment: scroll;
    }

    #site-content #page-header .title,
    #site-content #page-header .subtitle {
        color: #fff;
    }

    #site-content #page-callout {
        background-color: ;
    }

    #site-content #page-callout .callout-content {
        color: ;
    }

    #site-footer {
        background-color: ;
        color: ;
    }

    .wrapper,
    .page-fullwidth #page-body .wrapper .content-wrap .content .vc_row_wrap,
    .page-fullwidth #page-body #respond,
    .page-fullwidth #page-body .nocomments {
        width: 1110px;
    }

    body.layout-boxed #site-wrapper,
    body.layout-boxed #site-wrapper #masthead-sticky,
    body.layout-boxed #site-wrapper #masthead.header-v7 {
        width: 1210px;
    }

    .side-menu.layout-boxed #site-wrapper {
        width: 1360px;
    }

    a,
    a.link:hover,
    .scheme,
    #site-header #headerbar .top-navigator .menu li .sub-menu li a,
    #site-wrapper .imagebox .box-content .box-button a:hover,
    #site-wrapper .testimonial .testimonial-author .author-info a:hover,
    #site-wrapper .vc_tta-tabs .vc_tta-tabs-container .vc_tta-tabs-list li.vc_active a,
    .woocommerce .product .woocommerce-tabs .tabs li.active a,
    input.white[type="submit"],
    button.white[type="submit"],
    .button.white,
    .sidebar .widget.widget_nav_menu .menu > li.current-menu-item > a,
    .sidebar .widget.widget_nav_menu .menu > li.current_page_item > a,
    .sidebar .widget.widget_nav_menu .menu > li.current-menu-item > a::before,
    .sidebar .widget.widget_nav_menu .menu > li.current-page-item > a::before {
        color: #15416e;
    }

    textarea:focus,
    input[type="text"]:focus,
    input[type="password"]:focus,
    input[type="datetime"]:focus,
    input[type="datetime-local"]:focus,
    input[type="date"]:focus,
    input[type="month"]:focus,
    input[type="time"]:focus,
    input[type="week"]:focus,
    input[type="number"]:focus,
    input[type="email"]:focus,
    input[type="url"]:focus,
    input[type="search"]:focus,
    input[type="tel"]:focus,
    input[type="color"]:focus,
    input.input-text:focus,
    select:focus,
    #site-wrapper .vc_tta-accordion .vc_tta-panel-heading .vc_tta-panel-title a i.vc_tta-controls-icon-plus::before,
    #site-wrapper .vc_tta-accordion .vc_tta-panel-heading .vc_tta-panel-title a i.vc_tta-controls-icon-plus::after {
        border-color: #15416e;
    }

    input[type="submit"]:hover,
    button[type="submit"]:hover,
    .button:hover,
    input[type="submit"].scheme2,
    button[type="submit"].scheme2,
    .button.scheme2,
    .bg-scheme,
    #headerbar,
    #site-header #headerbar .top-navigator .menu li .sub-menu li:hover > a,
    #site-header #headerbar .top-navigator .menu li .sub-menu li.current-menu-item > a,
    #site-header #headerbar .top-navigator .menu li .sub-menu li.current-page-item > a,
    #site-header #masthead #site-navigator .menu li .sub-menu li:hover > a,
    #site-header #masthead #site-navigator .menu li .sub-menu > .current-menu-item > a,
    #site-header #masthead #site-navigator .menu li .sub-menu > .current-menu-ancestor > a,
    #site-header #masthead #site-navigator .menu.menu-extra .shopping-cart > a .shopping-cart-items-count,
    #site-wrapper .imagebox .box-header::after,
    #site-wrapper .imagebox .box-content::after,
    #site-wrapper .iconbox .box-header .box-icon i,
    #site-wrapper .blog-shortcode.blog-grid .hentry .entry-cover .entry-time,
    .blog .hentry .entry-content .readmore .more-link:hover,
    #site-footer,
    #site-wrapper .testimonial .testimonial-image::after,
    .blog .hentry.sticky,
    .single-post .hentry .entry-footer .entry-tags a,
    a[rel="wp-video-lightbox"]::before,
    #site-wrapper .owl-controls .owl-buttons > div:hover,
    #site-wrapper .owl-controls .owl-pagination .owl-page span,
    .widget.widget_tag_cloud .tagcloud a,
    table thead,
    table tfoot,
    #site-wrapper .vc_tta-accordion .vc_tta-panel.vc_active .vc_tta-panel-heading .vc_tta-panel-title a,
    #site-wrapper .vc_toggle .vc_toggle_title h4::after,
    #site-wrapper .vc_tta-tabs .vc_tta-tabs-container .vc_tta-tabs-list,
    .header-v4 #site-header #masthead #site-navigator,
    .woocommerce .product .woocommerce-tabs .tabs,
    .projects .projects-filter ul,
    .projects.projects-grid .projects-items .hentry .project-wrap .project-thumbnail a,
    .projects.projects-grid .projects-items .hentry .project-wrap .project-thumbnail .project-buttons a:hover,
    .projects.projects-grid-alt .projects-items .hentry .project-wrap .project-thumbnail a,
    .projects.projects-grid-alt .projects-items .hentry .project-wrap .project-thumbnail .project-buttons a:hover,
    .projects.projects-justified .projects-items .hentry > a::after,
    .sidebar .widget.widget_nav_menu ul,
    .woocommerce .products li .add_to_cart_button,
    .woocommerce-page .products li .add_to_cart_button,
    .widget.widget_product_tag_cloud .tagcloud a,
    .widget_shopping_cart .buttons .button.checkout,
    .widget_shopping_cart_content .buttons .button.checkout,
    .widget.widget_price_filter .price_slider_wrapper .ui-slider .ui-slider-range,
    .navigation.post-navigation .nav-links li a::after,
    .projects.projects-masonry .projects-items .hentry .project-wrap .project-info,
    .goto-top a,
    #site-wrapper .flexslider .flex-direction-nav li a:hover,
    .history li h3,
    #site-header #masthead #site-navigator-mobile.navigator-mobile .navigator-toggle {
        background-color: #15416e;
    }

    a:hover,
    a.link,
    .scheme2,
    #site-header #masthead #site-navigator .menu > .current-menu-item a,
    #site-header #masthead #site-navigator .menu > .current-menu-ancestor a,
    #site-header #masthead #site-navigator .menu > li:hover > a,
    #site-wrapper .iconbox .box-readmore a,
    .blog .hentry .entry-header .entry-time,
    .single-post .hentry .entry-header .entry-time,
    .blog .hentry .entry-header .entry-meta a:hover,
    .single-post .hentry .entry-header .entry-meta a:hover,
    #site-wrapper .blog-shortcode.blog-grid .hentry .entry-footer .entry-meta a:hover,
    #site-wrapper .testimonial .testimonial-author .author-info,
    #site-wrapper .testimonial .testimonial-author .author-info a,
    h1 i,
    h2 i,
    h3 i,
    h4 i,
    h5 i,
    h6 i,
    .navigation.post-navigation .nav-links li a span,
    .widget.widget_recent_comments ul li::after,
    .header-v4 #site-header #masthead #site-brand .wrapper .header-widgets .widget .info-icon i,
    .projects.projects-grid .projects-items .hentry .project-wrap .project-info .project-categories li a:hover,
    .projects.projects-grid-alt .projects-items .hentry .project-wrap .project-info .project-categories li a:hover,
    .woocommerce .star-rating,
    .woocommerce-page .star-rating,
    .projects.projects-masonry .projects-items .hentry .project-wrap .project-info .project-title a:hover,
    .projects.projects-justified .projects-items .hentry .project-info .project-title a:hover,
    #site-wrapper .member .member-info .member-subtitle {
        color: #18ba60;
    }

    #site-header #masthead #site-navigator .menu > .current-menu-item a,
    #site-header #masthead #site-navigator .menu > .current-menu-ancestor a,
    blockquote,
    .woocommerce .onsale::after,
    .woocommerce-page .onsale::after,
    .woocommerce .products li .product-inner:hover,
    .woocommerce-page .products li .product-inner:hover,
    .project-single .project-content ul li::before,
    ul.style1 li::before,
    ul.style2 li::before,
    .projects.projects-grid-alt .projects-items .hentry .project-wrap:hover {
        border-color: #18ba60;
    }

    input[type="submit"],
    button[type="submit"],
    .button,
    input[type="submit"].scheme2:hover,
    button[type="submit"].scheme2:hover,
    .button.scheme2:hover,
    .bg-scheme2,
    #site-header #headerbar .top-navigator .menu li .sub-menu li a::before,
    #site-header #masthead #site-navigator .menu li .sub-menu li a::before,
    #site-header #masthead #site-navigator .menu.menu-extra .shopping-cart > a,
    #site-wrapper .imagebox .box-header::before,
    #site-wrapper .imagebox .box-content .box-button a,
    #site-wrapper .iconbox .box-readmore a::before,
    #site-wrapper .iconbox:hover .box-header .box-icon i,
    #site-wrapper .blog-shortcode.blog-grid .hentry .entry-content-wrap::after,
    #site-wrapper .blog-shortcode.blog-grid .hentry .entry-cover:hover .entry-time,
    .blog .hentry .entry-content .readmore .more-link,
    #site-footer #footer-widgets .widget.widget_nav_menu ul li::before,
    .widget.widget_archive ul li::before,
    .quick-form h1::after,
    .quick-form h2::after,
    .quick-form h3::after,
    .quick-form h4::after,
    .quick-form h5::after,
    .quick-form h6::after,
    h1.style2::after,
    h2.style2::after,
    h3.style2::after,
    h4.style2::after,
    h5.style2::after,
    h6.style2::after,
    .navigation .page-numbers.current,
    .widget .widget-title::before,
    .single-post .hentry .entry-footer .entry-tags a:hover,
    .box .box-title::after,
    #comments .comments-title::after,
    #comments #reply-title::after,
    a[rel="wp-video-lightbox"]:hover::before,
    #site-wrapper .owl-controls .owl-buttons > div,
    #site-wrapper .owl-controls .owl-pagination .owl-page.active span,
    .widget.widget_categories ul li a::before,
    .widget.widget_pages ul li a::before,
    .widget.widget_meta ul li a::before,
    .widget.widget_tag_cloud .tagcloud a:hover,
    .widget.widget_calendar table tbody tr td#today,
    #site-wrapper .vc_toggle.vc_toggle_active .vc_toggle_title h4::after,
    #site-wrapper .vc_tta-tabs .vc_tta-tabs-container .vc_tta-tabs-list li a::before,
    .header-v2 #site-header #masthead #site-navigator .menu > .current-menu-item > a,
    .header-v2 #site-header #masthead #site-navigator .menu > .current-menu-ancestor > a,
    .header-v3 #site-header #masthead #site-navigator .menu > .current-menu-item > a,
    .header-v3 #site-header #masthead #site-navigator .menu > .current-menu-ancestor > a,
    .header-v4 #site-header #masthead #site-navigator .menu > .current-menu-item > a::after,
    .header-v4 #site-header #masthead #site-navigator .menu > .current-menu-ancestor > a::after,
    .woocommerce .product .woocommerce-tabs .tabs li a::before,
    .projects .projects-filter ul li.active a::after,
    .projects.projects-grid .projects-items .hentry .project-wrap .project-thumbnail .project-buttons a,
    .projects.projects-grid-alt .projects-items .hentry .project-wrap .project-thumbnail .project-buttons a,
    .sidebar .widget.widget_nav_menu .menu li .sub-menu li a::before,
    .sidebar .widget.widget_nav_menu .menu > li.current-menu-item > a::after,
    .sidebar .widget.widget_nav_menu .menu > li.current_page_item > a::after,
    .woocommerce .products li .wc-forward,
    .woocommerce-page .products li .wc-forward,
    .woocommerce .onsale,
    .woocommerce-page .onsale,
    .woocommerce .products li .add_to_cart_button:hover,
    .woocommerce-page .products li .add_to_cart_button:hover,
    .woocommerce .woocommerce-pagination ul li .page-numbers.current,
    .woocommerce-page .woocommerce-pagination ul li .page-numbers.current,
    .widget.widget_product_tag_cloud .tagcloud a:hover,
    .widget.widget_product_categories ul li::before,
    .widget_shopping_cart .buttons .button.checkout:hover,
    .widget_shopping_cart_content .buttons .button.checkout:hover,
    .widget.widget_price_filter .price_slider_wrapper .ui-slider .ui-slider-handle,
    .woocommerce .product .related h2::after,
    .woocommerce-page .product .related h2::after,
    .woocommerce .product .upsells h2::after,
    .woocommerce-page .product .upsells h2::after,
    .projects-related .projects-related-title::after,
    #site-header #headerbar .top-navigator .menu > li.current-menu-item > a::after,
    #site-header #headerbar .top-navigator .menu > li.current-page-item > a::after,
    .project-single .project-content ul li::after,
    ul.style1 li::after,
    ul.style2 li::after,
    .navigation.post-navigation .nav-links li a:hover::after,
    .search #main-content .content-inner .search-results article .counter,
    .goto-top a:hover,
    #site-wrapper .counter .counter-image i,
    #site-wrapper .flexslider .flex-direction-nav li a,
    #site-wrapper .member .member-info .social-links a:hover,
    .history li:hover h3,
    .history li::after,
    #site-wrapper .testimonial:not(.has-image) .testimonial-content::after,
    #site-header #masthead #site-navigator-mobile.navigator-mobile.active .navigator-toggle {
        background-color: #18ba60;
    }

    body.header-v4 #masthead #site-navigator {
        -webkit-backface-visibility: hidden !important;
        -moz-backface-visibility: hidden !important;
        -ms-backface-visibility: hidden !important;
        backface-visibility: hidden !important;
    }
</style>
<link rel='stylesheet' id='theme-fonts-css'
      href='https://fonts.googleapis.com/css?family=Hind+Siliguri%3A300%2Cregular%2C500%2C600%2C700%7CHind+Vadodara%3A300%2Cregular%2C500%2C600%2C700%7CHind+Vadodara%3A300%2Cregular%2C500%2C600%2C700&#038;subset=latin&#038;ver=5.2.9'
      type='text/css' media='all'/>
<link rel='stylesheet' id='js_composer_front-css'
      href='{{ URL::asset('/public/frontend/themes/YBC/wp-content/plugins/js_composer/assets/css/js_composer.min.css')}}'
      type='text/css' media='all'/>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC/wp-includes/js/jquery/jquery.js')}}'></script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC/wp-includes/js/jquery/jquery-migrate.min.js')}}'></script>
<script type='text/javascript'
        src='{{ URL::asset('/public/frontend/themes/YBC/wp-content/plugins/wp-video-lightbox/js/jquery.prettyPhoto.min.js')}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var vlpp_vars = {
        "prettyPhoto_rel": "wp-video-lightbox",
        "animation_speed": "fast",
        "slideshow": "5000",
        "autoplay_slideshow": "false",
        "opacity": "0.80",
        "show_title": "true",
        "allow_resize": "true",
        "allow_expand": "true",
        "default_width": "640",
        "default_height": "480",
        "counter_separator_label": "\/",
        "theme": "pp_default",
        "horizontal_padding": "20",
        "hideflash": "false",
        "wmode": "opaque",
        "autoplay": "false",
        "modal": "false",
        "deeplinking": "false",
        "overlay_gallery": "true",
        "overlay_gallery_max": "30",
        "keyboard_shortcuts": "true",
        "ie6_fallback": "true"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='https://live.linethemes.com/cosine/wp-content/plugins/wp-video-lightbox/js/video-lightbox.js?ver=3.1.6'></script>
<script type='text/javascript'
        src='https://live.linethemes.com/cosine/wp-content/plugins/revslider/public/assets/js/revolution.tools.min.js?ver=6.0'></script>
<script type='text/javascript'
        src='https://live.linethemes.com/cosine/wp-content/plugins/revslider/public/assets/js/rs6.min.js?ver=6.1.5'></script>
<script type='text/javascript'
        src='https://live.linethemes.com/cosine/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_add_to_cart_params = {
        "ajax_url": "\/cosine\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/cosine\/?wc-ajax=%%endpoint%%",
        "i18n_view_cart": "View cart",
        "cart_url": "https:\/\/live.linethemes.com\/cosine\/cart\/",
        "is_cart": "",
        "cart_redirect_after_add": "no"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='https://live.linethemes.com/cosine/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=4.1.1'></script>
<script type='text/javascript'
        src='https://live.linethemes.com/cosine/wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js?ver=6.2.0'></script>
<link rel='https://api.w.org/' href='https://live.linethemes.com/cosine/wp-json/'/>
<link rel="EditURI" type="application/rsd+xml" title="RSD"
      href="https://live.linethemes.com/cosine/xmlrpc.php?rsd"/>
<link rel="wlwmanifest" type="application/wlwmanifest+xml"
      href="https://live.linethemes.com/cosine/wp-includes/wlwmanifest.xml"/>
<meta name="generator" content="WordPress 5.2.9"/>
<meta name="generator" content="WooCommerce 4.1.1"/>
<link rel="canonical" href="https://live.linethemes.com/cosine/"/>
<link rel='shortlink' href='https://live.linethemes.com/cosine/'/>
<link rel="alternate" type="application/json+oembed"
      href="https://live.linethemes.com/cosine/wp-json/oembed/1.0/embed?url=https%3A%2F%2Flive.linethemes.com%2Fcosine%2F"/>
<link rel="alternate" type="text/xml+oembed"
      href="https://live.linethemes.com/cosine/wp-json/oembed/1.0/embed?url=https%3A%2F%2Flive.linethemes.com%2Fcosine%2F&#038;format=xml"/>
<script>
    WP_VIDEO_LIGHTBOX_VERSION = "1.8.8";
    WP_VID_LIGHTBOX_URL = "https://live.linethemes.com/cosine/wp-content/plugins/wp-video-lightbox";

    function wpvl_paramReplace(name, string, value) {
        // Find the param with regex
        // Grab the first character in the returned string (should be ? or &)
        // Replace our href string with our new value, passing on the name and delimeter

        var re = new RegExp("[\?&]" + name + "=([^&#]*)");
        var matches = re.exec(string);
        var newString;

        if (matches === null) {
            // if there are no params, append the parameter
            newString = string + '?' + name + '=' + value;
        } else {
            var delimeter = matches[0].charAt(0);
            newString = string.replace(re, delimeter + name + "=" + value);
        }
        return newString;
    }
</script>
<noscript>
    <style>.woocommerce-product-gallery {
            opacity: 1 !important;
        }</style>
</noscript>

<style type="text/css" id="breadcrumb-trail-css">
    .breadcrumbs .trail-browse,
    .breadcrumbs .trail-items,
    .breadcrumbs .trail-items li {
        display: inline-block;
        margin: 0;
        padding: 0;
        border: none;
        background: transparent;
        text-indent: 0;
    }

    .breadcrumbs .trail-browse {
        font-size: inherit;
        font-style: inherit;
        font-weight: inherit;
        color: inherit;
    }

    .breadcrumbs .trail-items {
        list-style: none;
    }

    .trail-items li::after {
        content: "\002F";
        padding: 0 0.5em;
    }

    .trail-items li:last-of-type::after {
        display: none;
    }
</style>
<meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<meta name="generator"
      content="Powered by Slider Revolution 6.1.5 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface."/>
<script type="text/javascript">
    function setREVStartSize(t) {
        try {
            var h, e = document.getElementById(t.c).parentNode.offsetWidth;
            if (e = 0 === e || isNaN(e) ? window.innerWidth : e, t.tabw = void 0 === t.tabw ? 0 : parseInt(t.tabw), t.thumbw = void 0 === t.thumbw ? 0 : parseInt(t.thumbw), t.tabh = void 0 === t.tabh ? 0 : parseInt(t.tabh), t.thumbh = void 0 === t.thumbh ? 0 : parseInt(t.thumbh), t.tabhide = void 0 === t.tabhide ? 0 : parseInt(t.tabhide), t.thumbhide = void 0 === t.thumbhide ? 0 : parseInt(t.thumbhide), t.mh = void 0 === t.mh || "" == t.mh || "auto" === t.mh ? 0 : parseInt(t.mh, 0), "fullscreen" === t.layout || "fullscreen" === t.l) h = Math.max(t.mh, window.innerHeight);
            else {
                for (var i in t.gw = Array.isArray(t.gw) ? t.gw : [t.gw], t.rl) void 0 !== t.gw[i] && 0 !== t.gw[i] || (t.gw[i] = t.gw[i - 1]);
                for (var i in t.gh = void 0 === t.el || "" === t.el || Array.isArray(t.el) && 0 == t.el.length ? t.gh : t.el, t.gh = Array.isArray(t.gh) ? t.gh : [t.gh], t.rl) void 0 !== t.gh[i] && 0 !== t.gh[i] || (t.gh[i] = t.gh[i - 1]);
                var r, a = new Array(t.rl.length),
                    n = 0;
                for (var i in t.tabw = t.tabhide >= e ? 0 : t.tabw, t.thumbw = t.thumbhide >= e ? 0 : t.thumbw, t.tabh = t.tabhide >= e ? 0 : t.tabh, t.thumbh = t.thumbhide >= e ? 0 : t.thumbh, t.rl) a[i] = t.rl[i] < window.innerWidth ? 0 : t.rl[i];
                for (var i in r = a[0], a) r > a[i] && 0 < a[i] && (r = a[i], n = i);
                var d = e > t.gw[n] + t.tabw + t.thumbw ? 1 : (e - (t.tabw + t.thumbw)) / t.gw[n];
                h = t.gh[n] * d + (t.tabh + t.thumbh)
            }
            void 0 === window.rs_init_css && (window.rs_init_css = document.head.appendChild(document.createElement("style"))), document.getElementById(t.c).height = h, window.rs_init_css.innerHTML += "#" + t.c + "_wrapper { height: " + h + "px }"
        } catch (t) {
            console.log("Failure at Presize of Slider:" + t)
        }
    };
</script>
<style type="text/css" data-type="vc_shortcodes-custom-css">
    .vc_custom_1460519094442 {
        padding-top: 40px !important;
        padding-bottom: 40px !important;
    }

    .vc_custom_1460521049287 {
        margin-bottom: 20px !important;
    }

    .vc_custom_1460520975088 {
        padding-top: 40px !important;
        padding-bottom: 40px !important;
        background-color: #f1f2f8 !important;
    }

    .vc_custom_1460535090775 {
        padding-top: 165px !important;
        padding-bottom: 165px !important;
    }

    .vc_custom_1460559155148 {
        padding-top: 40px !important;
        padding-bottom: 40px !important;
    }

    .vc_custom_1460567613213 {
        padding-top: 40px !important;
        background-color: #20242e !important;
    }

    .vc_custom_1460572124805 {
        padding-top: 40px !important;
        padding-bottom: 60px !important;
    }

    .vc_custom_1461768212521 {
        margin-bottom: -50px !important;
        border-top-width: 1px !important;
        border-top-color: rgba(23, 76, 129, 0.1) !important;
        border-top-style: solid !important;
    }

    .vc_custom_1460568709204 {
        padding-top: 50px !important;
        padding-bottom: 50px !important;
    }

    .data-content {
        margin-top: 50px;
    }

    @media(max-width: 768px) {
        #site-header #headerbar .custom-info {
            display: none;
        }
        #site-header #headerbar .social-links {
            margin: 0 !important;
        }
    }
</style>