<div id="masthead" data-placeholder="#masthead-placeholder">
    <div class="wrapper">
        <div id="site-brand">
            <div id="site-logo" class="brand has-logo" style="margin-top: 10px; margin-bottom: 5px">
                <h1 class="logo" itemprop="headline"><a href="/"><img
                                class="logo-standard"
                                src="/public/frontend/themes/YBC/img/logo.png"
                                alt="Cosine"><img class="logo-retina"
                                                  src="/public/frontend/themes/YBC/img/logo.png"
                                                  alt="Cosine"/></a></h1>
            </div>
        </div>

        <nav id="site-navigator" class="navigator" itemscope="itemscope"
             itemtype="http://schema.org/SiteNavigationElement">
            <ul id="menu-main-menu" class="menu">
                <?php
                $menus = CommonHelper::getFromCache('menu_main_menu', ['menus']);
                if (!$menus) {
                    $menus = \Modules\ThemeCLB\Models\Menu::where('status', 1)->where('location', 'main_menu')->whereNull('parent_id')->orderBy('order_no', 'desc')->orderBy('name', 'asc')->get();
                    CommonHelper::putToCache('menu_main_menu', $menus, ['menus']);
                }
                ?>
                @foreach($menus as $menu)
                    <?php
                    $menu_childs = CommonHelper::getFromCache('menus_childs_' . $menu->id, ['menus']);
                    if (!$menu_childs) {
                        $menu_childs = $menu->childs;
                        CommonHelper::putToCache('menus_childs_' . $menu->id, $menu_childs, ['menus']);
                    }
                    ?>

                    <li id="menu-item-23"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-23">
                        <a href="{{@$menu->url}}">{{@$menu->name}}</a>
                        @if(count($menu_childs) > 0)
                            <ul class="sub-menu">
                                @foreach($menu_childs as $child)
                                    <li id="menu-item-66"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66">
                                        <a
                                                href="{{@$child->url}}">{!! $child->name !!}</a></li>
                                @endforeach

                            </ul>
                        @endif
                    </li>
                @endforeach

                @if(\Auth::guard('admin')->check())
                    <li id="menu-item-23"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-23">
                        <a href="javascript:;">Xin chào {{ @\Auth::guard('admin')->user()->name }}</a>
                    </li>
                @else
                    <li id="menu-item-23"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-23">
                        <a href="/dang-nhap">Đăng ký/Đăng nhập</a>
                    </li>
                @endif
            </ul>
        </nav>
    </div>

    <nav id="site-navigator-mobile" class="navigator-mobile" itemscope="itemscope"
         itemtype="http://schema.org/SiteNavigationElement">

        <a href="#" class="navigator-toggle">
            <i class="fa fa-bars"></i>
        </a>
        <!-- /.navigator-toggle -->

        <ul id="menu-main-menu-1" class="menu">
            @foreach($menus as $menu)
                <?php
                $menu_childs = CommonHelper::getFromCache('menus_childs_' . $menu->id, ['menus']);
                if (!$menu_childs) {
                    $menu_childs = $menu->childs;
                    CommonHelper::putToCache('menus_childs_' . $menu->id, $menu_childs, ['menus']);
                }
                ?>
                <li class="menu-item menu-item-type-post_type menu-item-object-page @if(count($menu_childs) > 0) menu-item-has-children @endif menu-item-23">
                    <a href="{{@$menu->url}}">{{@$menu->name}}</a>

                    @if(count($menu_childs) > 0)
                        <ul class="sub-menu">
                            @foreach($menu_childs as $child)
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-66"><a
                                            href="{!! $child->url !!}">{!! $child->name !!}</a></li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </nav>
</div>