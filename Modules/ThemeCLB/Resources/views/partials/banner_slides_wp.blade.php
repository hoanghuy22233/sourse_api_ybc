<div data-vc-full-width="true" data-vc-full-width-init="false"
     data-vc-stretch-content="true"
     class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <!-- START homepage REVOLUTION SLIDER 6.1.5 -->
                <p class="rs-p-wp-fix"></p>
                <rs-module-wrap id="rev_slider_1_1_wrapper" data-source="gallery"
                                style="background:#d8e7ef;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
                    <rs-module id="rev_slider_1_1" style="display:none;"
                               data-version="6.1.5">
                        <rs-slides>
                            <?php
                            $data = CommonHelper::getFromCache('banners_banner_slides', ['banners']);
                            if (!$data) {
                                $data = @\Modules\ThemeCLB\Models\Banner::where('location', 'banner_slides')->where('status', 1)->get();
                                CommonHelper::putToCache('banners_banner_slides', $data, ['banners']);
                            }
                            ?>
                            @foreach($data as $k => $v)
                                <rs-slide data-key="rs-{{ $k + 1 }}" data-title="SLIDE {{ $k + 1 }}"
                                          data-thumb="{{ asset('public/filemanager/userfiles/' . $v->image) }}"
                                          data-anim="ei:d;eo:d;s:d;r:0;t:random;sl:d;">
                                    <img src="{{ asset('public/filemanager/userfiles/' . $v->image) }}"
                                         title="Home" class="rev-slidebg"
                                         data-no-retina>
                                    <!--
-->
                                    <rs-layer id="slider-1-slide-1-layer-1"
                                              data-type="text"
                                              data-color="#15416e" data-rsp_ch="on"
                                              data-xy="x:10px;y:110px;"
                                              data-text="w:normal;s:64;l:70;fw:700;"
                                              data-dim="w:526px;h:43px;"
                                              data-frame_0="x:left;o:1;tp:600;"
                                              data-frame_1="tp:600;e:Power2.easeInOut;st:500;"
                                              data-frame_999="o:0;tp:600;e:nothing;st:w;"
                                              style="z-index:5;font-family:Roboto;text-transform:uppercase;">
                                        {{ $v->name }}
                                    </rs-layer>
                                    <!--

-->
                                    <rs-layer id="slider-1-slide-1-layer-2"
                                              data-type="text"
                                              data-color="#15416e" data-rsp_ch="on"
                                              data-xy="x:10px;y:270px;"
                                              data-text="w:normal;s:16;l:17;fw:700;"
                                              data-dim="w:572px;h:49px;"
                                              data-frame_0="x:left;o:1;tp:600;"
                                              data-frame_1="tp:600;e:Power2.easeInOut;st:500;sp:400;"
                                              data-frame_999="o:0;tp:600;e:nothing;st:w;"
                                              style="z-index:6;font-family:Roboto;text-transform:uppercase;">
                                        {!! $v->intro !!}
                                    </rs-layer>
                                    <!--

-->
                                    @if($v->link != '')
                                        <rs-layer id="slider-1-slide-1-layer-3"
                                                  data-type="text"
                                                  data-color="rgba(255,255,255,1)"
                                                  data-rsp_ch="on"
                                                  data-xy="x:10px;y:b;yo:120px;"
                                                  data-text="w:normal;s:15;l:55;fw:700;"
                                                  data-vbility="t,t,f,f"
                                                  data-frame_0="x:left;o:1;tp:600;"
                                                  data-frame_1="tp:600;e:Power2.easeInOut;st:500;sp:500;"
                                                  data-frame_999="o:0;tp:600;e:nothing;st:w;"
                                                  style="z-index:7;font-family:Roboto;">
                                            <a class="button border lg" href="{{ $v->link }}">Xem thêm<i
                                                        class="fa fa-chevron-right"></i></a>
                                        </rs-layer>
                                @endif
                                <!--
    -->
                                </rs-slide>
                            @endforeach
                        </rs-slides>
                        <rs-progress class="rs-bottom"
                                     style="visibility: hidden !important;"></rs-progress>
                    </rs-module>
                    <script type="text/javascript">
                        setREVStartSize({
                            c: 'rev_slider_1_1',
                            rl: [1240, 1024, 778, 480],
                            el: [500],
                            gw: [1110],
                            gh: [500],
                            layout: 'fullwidth',
                            mh: "0"
                        });
                        var revapi1,
                            tpj;
                        jQuery(function () {
                            tpj = jQuery;
                            if (tpj("#rev_slider_1_1").revolution == undefined) {
                                revslider_showDoubleJqueryError("#rev_slider_1_1");
                            } else {
                                revapi1 = tpj("#rev_slider_1_1").show().revolution({
                                    jsFileLocation: "//live.linethemes.com/cosine/wp-content/plugins/revslider/public/assets/js/",
                                    sliderLayout: "fullwidth",
                                    visibilityLevels: "1240,1024,778,480",
                                    gridwidth: 1110,
                                    gridheight: 500,
                                    minHeight: "",
                                    spinner: "spinner0",
                                    editorheight: "500,768,960,720",
                                    responsiveLevels: "1240,1024,778,480",
                                    disableProgressBar: "on",
                                    navigation: {
                                        mouseScrollNavigation: false,
                                        onHoverStop: false,
                                        arrows: {
                                            enable: true,
                                            tmp: "<div class=\"tp-arr-allwrapper\">	<div class=\"tp-arr-imgholder\"></div>	<div class=\"tp-arr-titleholder\"></div>	</div>",
                                            style: "hermes",
                                            hide_onleave: true,
                                            left: {},
                                            right: {}
                                        },
                                        bullets: {
                                            enable: true,
                                            tmp: "",
                                            style: "hermes"
                                        }
                                    },
                                    fallbacks: {
                                        ignoreHeightChanges: "",
                                        allowHTML5AutoPlayOnAndroid: true
                                    },
                                });
                            }

                        });
                    </script>
                    <script>
                        var htmlDivCss = unescape("%23rev_slider_1_1_wrapper%20.hermes.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3A%2315416e%3B%0A%09width%3A30px%3B%0A%09height%3A110px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A1000%3B%0A%7D%0A%0A%23rev_slider_1_1_wrapper%20.hermes.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%27revicons%27%3B%0A%09font-size%3A18px%3B%0A%09color%3A%23ffffff%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%20110px%3B%0A%09text-align%3A%20center%3B%0A%20%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%280px%29%3B%0A%20%20%20%20transition%3Aall%200.3s%3B%0A%20%20%20%20-webkit-transition%3Aall%200.3s%3B%0A%7D%0A%23rev_slider_1_1_wrapper%20.hermes.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%27%5Ce824%27%3B%0A%7D%0A%23rev_slider_1_1_wrapper%20.hermes.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%27%5Ce825%27%3B%0A%7D%0A%23rev_slider_1_1_wrapper%20.hermes.tparrows.tp-leftarrow%3Ahover%3Abefore%20%7B%0A%20%20%20%20transform%3Atranslatex%28-20px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%28-20px%29%3B%0A%20%20%20%20%20opacity%3A0%3B%0A%7D%0A%23rev_slider_1_1_wrapper%20.hermes.tparrows.tp-rightarrow%3Ahover%3Abefore%20%7B%0A%20%20%20%20transform%3Atranslatex%2820px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%2820px%29%3B%0A%20%20%20%20%20opacity%3A0%3B%0A%7D%0A%0A%23rev_slider_1_1_wrapper%20.hermes%20.tp-arr-allwrapper%20%7B%0A%20%20%20%20overflow%3Ahidden%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%09width%3A180px%3B%0A%20%20%20%20height%3A140px%3B%0A%20%20%20%20top%3A0px%3B%0A%20%20%20%20left%3A0px%3B%0A%20%20%20%20visibility%3Ahidden%3B%0A%20%20%20%20%20%20-webkit-transition%3A%20-webkit-transform%200.3s%200.3s%3B%0A%20%20transition%3A%20transform%200.3s%200.3s%3B%0A%20%20-webkit-perspective%3A%201000px%3B%0A%20%20perspective%3A%201000px%3B%0A%20%20%20%20%7D%0A%23rev_slider_1_1_wrapper%20.hermes.tp-rightarrow%20.tp-arr-allwrapper%20%7B%0A%20%20%20right%3A0px%3Bleft%3Aauto%3B%0A%20%20%20%20%20%20%7D%0A%23rev_slider_1_1_wrapper%20.hermes.tparrows%3Ahover%20.tp-arr-allwrapper%20%7B%0A%20%20%20visibility%3Avisible%3B%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%23rev_slider_1_1_wrapper%20.hermes%20.tp-arr-imgholder%20%7B%0A%20%20width%3A180px%3Bposition%3Aabsolute%3B%0A%20%20left%3A0px%3Btop%3A0px%3Bheight%3A110px%3B%0A%20%20transform%3Atranslatex%28-180px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28-180px%29%3B%0A%20%20transition%3Aall%200.3s%3B%0A%20%20transition-delay%3A0.3s%3B%0A%7D%0A%23rev_slider_1_1_wrapper%20.hermes.tp-rightarrow%20.tp-arr-imgholder%7B%0A%20%20%20%20transform%3Atranslatex%28180px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28180px%29%3B%0A%20%20%20%20%20%20%7D%0A%20%20%0A%23rev_slider_1_1_wrapper%20.hermes.tparrows%3Ahover%20.tp-arr-imgholder%20%7B%0A%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20%20-webkit-transform%3Atranslatex%280px%29%3B%20%20%20%20%20%20%20%20%20%20%20%20%0A%7D%0A%23rev_slider_1_1_wrapper%20.hermes%20.tp-arr-titleholder%20%7B%0A%20%20top%3A110px%3B%0A%20%20width%3A180px%3B%0A%20%20text-align%3Aleft%3B%20%0A%20%20display%3Ablock%3B%0A%20%20padding%3A0px%2010px%3B%0A%20%20line-height%3A30px%3B%20background%3A%23000%3B%0A%20%20background%3A%2315416e%3B%0A%20%20color%3A%23ffffff%3B%0A%20%20font-weight%3A600%3B%20position%3Aabsolute%3B%0A%20%20font-size%3A14px%3B%0A%20%20white-space%3Anowrap%3B%0A%20%20letter-spacing%3A1px%3B%0A%20%20-webkit-transition%3A%20all%200.3s%3B%0A%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transform%3A%20rotatex%28-90deg%29%3B%0A%20%20transform%3A%20rotatex%28-90deg%29%3B%0A%20%20-webkit-transform-origin%3A%2050%25%200%3B%0A%20%20transform-origin%3A%2050%25%200%3B%0A%20%20box-sizing%3Aborder-box%3B%0A%0A%7D%0A%23rev_slider_1_1_wrapper%20.hermes.tparrows%3Ahover%20.tp-arr-titleholder%20%7B%0A%20%20%20%20-webkit-transition-delay%3A%200.6s%3B%0A%20%20transition-delay%3A%200.6s%3B%0A%20%20-webkit-transform%3A%20rotatex%280deg%29%3B%0A%20%20transform%3A%20rotatex%280deg%29%3B%0A%7D%0A%0A%23rev_slider_1_1_wrapper%20.hermes.tp-bullets%20%7B%0A%7D%0A%0A%23rev_slider_1_1_wrapper%20.hermes%20.tp-bullet%20%7B%0A%20%20%20%20overflow%3Ahidden%3B%0A%20%20%20%20border-radius%3A50%25%3B%0A%20%20%20%20width%3A16px%3B%0A%20%20%20%20height%3A16px%3B%0A%20%20%20%20background-color%3A%20rgba%280%2C%200%2C%200%2C%200%29%3B%0A%20%20%20%20box-shadow%3A%20inset%200%200%200%202px%20%23ffffff%3B%0A%20%20%20%20-webkit-transition%3A%20background%200.3s%20ease%3B%0A%20%20%20%20transition%3A%20background%200.3s%20ease%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%7D%0A%0A%23rev_slider_1_1_wrapper%20.hermes%20.tp-bullet%3Ahover%20%7B%0A%09%20%20background-color%3A%20rgba%280%2C0%2C0%2C0.21%29%3B%0A%7D%0A%23rev_slider_1_1_wrapper%20.hermes%20.tp-bullet%3Aafter%20%7B%0A%20%20content%3A%20%27%20%27%3B%0A%20%20position%3A%20absolute%3B%0A%20%20bottom%3A%200%3B%0A%20%20height%3A%200%3B%0A%20%20left%3A%200%3B%0A%20%20width%3A%20100%25%3B%0A%20%20background-color%3A%20%23ffffff%3B%0A%20%20box-shadow%3A%200%200%201px%20%23ffffff%3B%0A%20%20-webkit-transition%3A%20height%200.3s%20ease%3B%0A%20%20transition%3A%20height%200.3s%20ease%3B%0A%7D%0A%23rev_slider_1_1_wrapper%20.hermes%20.tp-bullet.selected%3Aafter%20%7B%0A%20%20height%3A100%25%3B%0A%7D%0A%0A");
                        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                        if (htmlDiv) {
                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                        } else {
                            var htmlDiv = document.createElement('div');
                            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                        }
                    </script>
                    <script>
                        var htmlDivCss = unescape("%0A%0A%0A%0A%0A%0A");
                        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                        if (htmlDiv) {
                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                        } else {
                            var htmlDiv = document.createElement('div');
                            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                        }
                    </script>
                </rs-module-wrap>
                <!-- END REVOLUTION SLIDER -->
            </div>
        </div>
    </div>
</div>