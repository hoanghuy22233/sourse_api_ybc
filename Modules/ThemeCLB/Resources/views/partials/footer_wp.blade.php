<?php
$data = CommonHelper::getFromCache('widget_footer', ['widgets']);
if (!$data) {
    $data = \Modules\ThemeCLB\Models\Widget::select('name', 'content', 'location')->where('status', 1)->whereIn('location', ['copyright', 'footer1', 'footer2', 'footer3', 'footer4'])
        ->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->get();
    CommonHelper::putToCache('widget_footer', $data, ['widgets']);
}

$footers = [];
foreach ($data as $v) {
    $footers[$v->location][] = $v;
}
?>
<div id="site-footer">
    <div id="content-bottom-widgets">
        <div class="wrapper">
            <div class="row">
                @if(isset($footers['footer3']))
                    @foreach($footers['footer3'] as $v)
                        {!! @$v->content !!}
                    @endforeach
                @endif

            </div>
            <!-- /.row -->
        </div>
        <!-- /.wrapper -->

    </div>
    <!-- /#page-footer -->

    <div id="footer-widgets">
        <div class="wrapper">
            <div class="row">
                <div class="columns columns-4">
                    <div id="text-4" class="widget widget_text">
                        <div class="logo-ft"><img
                                    src="/public/frontend/themes/YBC/img/logo.png"
                                    alt="logo" width="90" height="30"></div>
                        <div class="textwidget">
                            @if(isset($footers['footer1']))
                                @foreach($footers['footer1'] as $v)
                                    {!! @$v->content !!}
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="columns columns-4">
                    <div id="nav_menu-2" class="widget widget_nav_menu">
                        <div class="menu-footer-menu-container">
                            @if(isset($footers['footer2']))
                                @foreach($footers['footer2'] as $v)
                                    {!! @$v->content !!}
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="columns columns-4">
                    <form action="{{route('register-member')}}" method="post">
                        @if (Session('success'))
                            <div class="alert bg-success" role="alert">
                                <p style="color: red"><b>{!!session('success')!!}</b></p>
                            </div>
                        @endif
                        @if(Session::has('message') && !Auth::check())
                            <div class="alert text-center text-white " role="alert"
                                 style=" margin: 0; font-size: 16px;">
                                <a href="#" style="float:right;" class="alert-close"
                                   data-dismiss="alert">&times;</a>
                                <p style="color: red"><b>{{ Session::get('message') }}</b></p>
                            </div>
                        @endif
                        <div class="mc4wp-form-fields">
                            <p>
                                <input type="text" name="name" placeholder="Họ & tên" style="width: 100%">
                            @if($errors->has('name'))
                                <p style="color: red"><b>{{ $errors->first('name') }}</b></p>
                                @endif
                            </p>
                            <p>
                                <input type="email" name="email" placeholder="Email của bạn"style="width: 100%">
                                @if($errors->has('email'))
                                    <p style="color: red"><b>{{ $errors->first('email') }}</b></p>
                                    @endif
                            </p>
                            <p>
                                <input type="number" name="tel" placeholder="Số điện thoại"style="width: 100%">
                                    @if($errors->has('tel'))
                                        <p style="color: red"><b>{{ $errors->first('tel') }}</b></p>
                                        @endif
                            </p>
                                        <p>
                                            <textarea type="text" name="content" placeholder="Ghi chú"style="width: 100%" required></textarea>

                            </p>
                            <p>
                                <button type="submit" class="button sm border">Đăng ký</button>
                            </p>
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.row -->

            <div class="goto-top"><a href="#top">Lên đầu trang</a></div>

        </div>
        <!-- /.wrapper -->

    </div>
    <!-- /#page-footer -->

    <div id="footer-content">
        <div class="wrapper">
            <div class="social-links">
                <a href="https://www.facebook.com/ybcstartup" target="_blank">
                    <i class="fa fa-facebook-official"></i>
                </a>
                <a href="https://www.facebook.com/groups/clbdoanhnhantrekhoinghiep.ybcstartup"
                   class="footer-bottom__link">
                    <i class="fas fa-users footer-bottom__link-icon"></i>
                </a>
                <a href="https://www.youtube.com/channel/UCJJVCVUbqSh52oiwU4UDfiQ" class="footer-bottom__link ytb">
                    <i class="fab fa-youtube footer-bottom__link-icon"></i>
                </a>
                <a href="https://www.tiktok.com/@ybcstartup?lang=vi-VN" class="footer-bottom__link">
                    <img src="/public/frontend/themes/YBC/img/tiktok.png" width="28" alt=""
                         class="footer-bottom__link-icon">
                </a>
            </div>
            <div class="copyright">
                <div class="copyright-content">
                    Copyright © 2021 YBC START-UP
                </div>
                <!-- /.copyright-content -->
            </div>
            <!-- /.copyright -->
        </div>
    </div>
    <!-- /.wrapper -->
</div>