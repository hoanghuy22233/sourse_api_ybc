<section>
    <div class="gray-bg">
        <div class="row">
            <div class="col-lg-12">
                <div class="featured-baner mate-black low-opacity" style="max-height: 150px; overflow: hidden;">
                    <img class="lazy" data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['banner_top'], null, null) }}"
                         alt="{{ @$settings['name'] }}">
                    <h3>{!! @$pageOption['meta_title'] !!}</h3>
                </div>
            </div>
        </div>
    </div>
</section>