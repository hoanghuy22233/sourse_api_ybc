@extends('themeclb::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="col-lg-12">
                                    <div class="prod-detail">
                                        <div class="gap no-bottom khkd-content">
                                            <div style="display: none;">
                                                {!! @$khkd_log->course->intro !!}<br>
                                                {!! @$khkd_log->course->content !!}
                                                <?php
                                                $data = (array)json_decode($khkd_log->data);
                                                $fields = \Modules\ThemeCLB\Models\KhkdFormFields::whereIn('id', array_keys($data))->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
                                                ?>
                                                @foreach($fields as $field)
                                                    @if(isset($data[$field->id]) && @$data[$field->id] != '')
                                                        {!! $field->label !!}<br>
                                                        {!! @$data[$field->id] !!}<br>
                                                    @endif
                                                @endforeach
                                            </div>


                                            <iframe src="{{ asset('public/filemanager/userfiles/mau_khkd_hoa_tuoi.pdf') }}" height="100%" width="100%" style="height: 1000px;" scrolling="auto"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="central-meta">
                            <span class="title2 ">Mẫu trả phí</span>
                            <ul class="suggested-frnd-caro">
                                <?php
                                $relates_lecturer = \Modules\ThemeCLB\Models\Course::where('lecturer_id', $khkd_log->course->lecturer_id)->where('id', '<>', $khkd_log->course->id)->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->take(8)->get();
                                ?>
                                @foreach($relates_lecturer as $k => $v)
                                    <li >
                                        <a href="/mau-kinh-doanh/{{@$v->slug}}.html">
                                            <img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image, 259, null) }}"
                                                 alt="{{@$v->name}}">
                                        </a>
                                        <div class="sugtd-frnd-meta">
                                            <a href="/mau-kinh-doanh/{{@$v->slug}}.html"
                                               title="{{@$v->name}}">Mẫu trả phí {{ $k  + 1 }}</a>
                                            <span><i class="fa fa-money-bill"></i> <strike>200.000<sup>đ</sup></strike>  <span
                                                        style="color: red">{!! !empty($v->final_price) ? number_format(@$v->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span></span>
                                            <a class="active btn btn-info" href="/mau-tra-phi/{{ $khkd_log->id }}" style="background-color: #fa6342;float: right;border-color: #bc482e;width: 43%; float: left;">Xem thử</a>
                                            <a class="active btn btn-info" href="javascript:;" style="background-color: #116cb4;
    float: right;
    border-color: #073b64; width: 43%; float: right;">Mua ngay</a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div><!-- suggested friends -->
                    </div>
                </div>
            </div>
        </section>
        <!-- content -->
    </div>
@endsection
@section('head_script')
    <style>
        .khkd-content {
            padding: 10px;
            border: 1px solid;
        }
    </style>
@endsection
@section('footer_script')
    <script>
        var callCount{{ $field['name'] }} = 0

        function countCallFunction{{ $field['name'] }}() {
            if (callCount{{ $field['name'] }} == 0) {
                initFunction{{ $field['name'] }}();
            }
            callCount{{ $field['name'] }} ++;
            return true;
        }

        function initFunction{{ $field['name'] }}() {
            textareaInit{{ $field['name'] }}();
        }


        $('#textarea-{{ $field['name'] }}, #form-group-{{ $field['name'] }}').click(function () {
            $('#textarea-{{ $field['name'] }}').hide();
            $('#{{ $field['name'] }}').show().click();
        });
        $(document).ready(function () {
            $('#{{ $field['name'] }}').click(function () {
                countCallFunction{{ $field['name'] }}();
            });
        });
        var observe;
        if (window.attachEvent) {
            observe = function (element, event, handler) {
                element.attachEvent('on' + event, handler);
            };
        } else {
            observe = function (element, event, handler) {
                element.addEventListener(event, handler, false);
            };
        }

        function textareaInit{{ $field['name'] }}() {
            var note = document.getElementById('{{ $field['name'] }}');

            function resize() {
                note.style.height = 'auto';
                note.style.height = note.scrollHeight + 'px';
            }

            /* 0-timeout to get the already changed note */
            function delayedResize() {
                window.setTimeout(resize, 0);
            }

            observe(note, 'change', resize);
            observe(note, 'cut', delayedResize);
            observe(note, 'paste', delayedResize);
            observe(note, 'drop', delayedResize);
            observe(note, 'keydown', delayedResize);

            note.focus();
            note.select();
            resize();
        }
    </script>
@endsection