<div class="central-meta item">
    <div class="classi-pst">
        <div class="row merged10">
            <div class="col-lg-6">
                <div class="image-bunch single">
                    <figure><img class="lazy"
                                 data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($course->image,244, null) }}"
                                 alt="{{@$course->name}}"></figure>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="classi-pst-meta">
                    <h6><a href="/mau-kinh-doanh/{{(@$course->slug)}}.html"
                           title="{{@$course->name}}">{{@$course->name}}</a></h6>
                    <span><i class="fas fa-user"></i> Quy mô: {{$course->level}}</span>
                    <span><i class="fa fa-money-bill"></i> @if($course->base_price != 0)<strike>{{number_format(@$course->base_price,0,'','.')}}<sup>đ</sup></strike>@endif  <span
                                style="color: red">{!! !empty($course->final_price) ? number_format(@$course->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span></span>
                    <span><i class="fas fa-user"></i> Tác giả: {{@$course->lecturer->name}} </span>
                    <span><i class="fas fa-tags"></i>Ngành: {{ $course->type }}</span>
                    {{--<span><i class="fas fa-play-circle"></i> Bài học: {{@$course->lesson->count()}} bài</span>--}}
                    {{--<span><i class="far fa-clock"></i> Thời hạn: {{$course->date_of_possession==0 ? 'Mãi mãi':$course->date_of_possession}}</span>--}}
                </div>
            </div>
            <div class="col-lg-12">
                <a class="active btn btn-info" href="/mau-kinh-doanh/{{(@$course->slug)}}.html"
                   style="background-color: #fa6342;float: right;border-color: #bc482e;">Xem chi tiết</a>
            </div>
        </div>
    </div>
</div>
