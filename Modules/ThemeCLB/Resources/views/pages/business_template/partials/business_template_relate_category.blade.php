<div class="central-meta">
    <span class="title2 ">MẪU KINH DOANH CÙNG DANH MỤC</span>
    <ul class="suggested-frnd-caro">
        <?php
        $relates = \Modules\ThemeCLB\Models\Course::where('multi_cat', 'like', '%' . $course->multi_cat . '%')->where('id', '<>', $course->id)->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->take(8)->get();
        ?>
        @foreach($relates as $v)
            <li >
                <a href="/mau-kinh-doanh/{{@$v->slug}}.html">
                    <img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image, 259, null) }}"
                         alt="{{@$v->name}}">
                </a>
                <div class="sugtd-frnd-meta">
                    <a href="/mau-kinh-doanh/{{@$v->slug}}.html"
                       title="{{@$v->name}}">{{@$v->name}}</a>
                    <span><i class="fa fa-money-bill"></i> <strike>{{number_format(@$v->base_price,0,'','.')}}<sup>đ</sup></strike>  <span
                                style="color: red">{!! !empty($v->final_price) ? number_format(@$v->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span></span>
                    <span><i class="fas fa-user"></i> Tác giả: {{@$v->lecturer->name}} </span>
                    <span><i class="fas fa-tags"></i>Ngành nghề: {{ $v->type }} </span>
                </div>
            </li>
        @endforeach
    </ul>
</div><!-- suggested friends -->