<?php
$lesson_items = \Modules\ThemeCLB\Models\LessonItem::where('course_id', $course->id)->count();
$carts = Cart::content();
$cart_id = [];
foreach ($carts as $v) {
    $cart_id[] = $v->id;
}
if (isset(\Auth::guard('student')->user()->id)) {
    $order = \Modules\ThemeCLB\Models\Order::where('student_id', @\Auth::guard('student')->user()->id)->where('course_id', $course->id)->first();
}
?>
@extends('themeclb::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="col-lg-12">
                                    <div class="prod-detail">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="prod-avatar">
                                                    <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($course->image,'auto','auto') }}"
                                                         alt="{{ $course->name }}" class="lazy">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="full-postmeta">
                                                    <div class="left-detail-meta">
                                                        <h4>{{$course->name}}</h4>

                                                        <span><i class="fa fa-money-bill"></i> @if($course->base_price != 0)
                                                                <strike>{{number_format(@$course->base_price,0,'','.')}}<sup>đ</sup></strike>@endif  <span
                                                                    style="color: red">{!! !empty($course->final_price) ? number_format(@$course->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span></span>
                                                        <span><i class="fas fa-tags"></i>Ngành nghề: {{ $course->type }}</span>
                                                        @if(is_object($course->lecturer))
                                                            <span><i class="fas fa-user"></i> Tác giả: <a
                                                                        href="{{asset('profile/'.@$course->lecturer->id)}}">{{@$course->lecturer->name}}</a></span>
                                                        @endif
                                                        <span><i class="fas fa-user"></i> Quy mô: {{$course->level}}</span>
                                                    </div>
                                                    <div class="member-des">
                                                        {{--                                                        <h5>Giới thiệu</h5>--}}
                                                        {{--                                                        <div>{{@$course->intro}}</div>--}}
                                                        <div class="bottom-meta">
                                                            @if(!\Auth::guard('student')->check())
                                                                @if (in_array($course->id, $cart_id))
                                                                    <a class="main-btn"
                                                                       href="/gio-hang"
                                                                       title="">Mẫu kinh doanh đã được thêm vào giỏ
                                                                        hàng</a>
                                                                @else
                                                                    <a title=""
                                                                       class="main-btn2"
                                                                       href="/gio-hang/add/{{$course->id}}">Mua ngay</a>
                                                                    <a class="main-btn add_to_cart"
                                                                       style="cursor: pointer;"
                                                                       title="" data-ripple="">Thêm vào giỏ</a>
                                                                @endif
                                                            @elseif(is_object(@$order))
                                                                @if(@$order->status == 1)
                                                                    @if($course->lesson!=null)
                                                                        <a class="main-btn2"
                                                                           title=""
                                                                           href="#list_lesson">Thực hành ngay</a>
                                                                    @endif
                                                                @else
                                                                    <a class="main-btn" rel="nofollow"
                                                                       href="#"
                                                                       title="">Mẫu kinh doanh đang chờ admin kich
                                                                        hoạt</a>
                                                                @endif

                                                            @else
                                                                @if (in_array($course->id, $cart_id))
                                                                    <a class="main-btn"
                                                                       href="/gio-hang"
                                                                       title="">Mẫu kinh doanh đã được thêm vào giỏ
                                                                        hàng</a>
                                                                @else
                                                                    <a title=""
                                                                       class="main-btn2"
                                                                       href="/gio-hang/add/{{$course->id}}">Mua ngay</a>
                                                                    <a class="main-btn add_to_cart"
                                                                       style="cursor: pointer;"
                                                                       title="" data-ripple="">Thêm vào giỏ</a>
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row tu-tao-mau-kd" style="margin-top: 30px;">
                                            <div class="col-lg-4">
                                                <div class="central-meta">
                                                    <?php
                                                    $fields = \Modules\ThemeCLB\Models\KhkdFormFields::where('course_id', $course->id)->whereNull('parent_id')->where('status', 1)->orderBy('order_no', 'asc')->orderBy('id', 'asc')->get();
                                                    ?>
                                                    <ul class="naves menu-khkd">
                                                        @foreach($fields as $k => $field)
                                                            <li class="nav-item dropdown">
                                                                <div class="item">
                                                                    <div class="number">
                                                                        <span>{{ $k + 1 }}</span>
                                                                    </div>
                                                                    <div class="body">
                                                                        <div class="name"><a
                                                                                    href="javascript:;">
                                                                                {{ strip_tags($field->label) }}
                                                                            </a>
                                                                        </div>

                                                                        <?php
                                                                        $fields2 = $field->childs;
                                                                        ?>
                                                                        @if(count($fields2) > 0)
                                                                            <ul class="meta meta-{{ $k }}"
                                                                                style="display: none;">
                                                                                @foreach($fields2 as $k => $field2)
                                                                                    <li class="total total-{{ $k }}">
                                                                                        <a
                                                                                                href="javascript:;">
                                                                                        {{ strip_tags($field2->label) }}
                                                                                        </a>

                                                                                        <?php
                                                                                        $fields3 = $field2->childs;
                                                                                        ?>
                                                                                        @if(count($fields3) > 0)
                                                                                            <ul class="meta meta-child meta-{{ $k }}"
                                                                                                style="display: none;">
                                                                                                @foreach($fields3 as $k => $field3)
                                                                                                    <li class="total total-{{ $k }}">
                                                                                                        <a href="javascript:;">
                                                                                                        {{ strip_tags($field3->label) }}
                                                                                                        </a>

                                                                                                        <?php
                                                                                                        $fields4 = $field3->childs;
                                                                                                        ?>
                                                                                                        @if(count($fields4) > 0)
                                                                                                            <ul class="meta meta-child meta-{{ $k }}"
                                                                                                                style="display: none;">
                                                                                                                @foreach($fields4 as $k => $field4)
                                                                                                                    <li class="total total-{{ $k }}">
                                                                                                                        <a href="javascript:;">
                                                                                                                        {{ strip_tags($field4->label) }}
                                                                                                                        </a>
                                                                                                                        <?php
                                                                                                                        $fields5 = $field4->childs;
                                                                                                                        ?>
                                                                                                                        @if(count($fields5) > 0)
                                                                                                                            <ul class="meta meta-child meta-{{ $k }}"
                                                                                                                                style="display: none;">
                                                                                                                                @foreach($fields5 as $k => $field5)
                                                                                                                                    <li class="total total-{{ $k }}">
                                                                                                                                        <a href="javascript:;">
                                                                                                                                        {{ strip_tags($field5->label) }}
                                                                                                                                        </a>
                                                                                                                                    </li>
                                                                                                                                @endforeach
                                                                                                                            </ul>
                                                                                                                        @endif
                                                                                                                    </li>
                                                                                                                @endforeach
                                                                                                            </ul>
                                                                                                        @endif
                                                                                                    </li>
                                                                                                @endforeach
                                                                                            </ul>
                                                                                        @endif
                                                                                    </li>
                                                                                @endforeach
                                                                            </ul>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                    <div class="col-lg-12">
                                                        <button class="btn btn-default" type="submit"
                                                                data-ripple="">Hủy
                                                        </button>
                                                        <button class="btn btn-primary" type="submit"
                                                                data-ripple="">Hoàn thành
                                                        </button>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="full-postmeta">
                                                    @foreach($fields as $field)
                                                        <div class="social-name social-name-{{ $field->id }}">

                                                            @if($field->type == 'input')
                                                                <input type="text"
                                                                       name="fields[{{ $field->id }}]"
                                                                       data-id="{{ $field->id }}"
                                                                       placeholder="facebook.com/pitnik">
                                                            @elseif($field->type == 'textarea')
                                                                <?php $fd = ['name' => 'field_' . $field->id, 'type' => 'textarea', 'label' => $field->label, 'inner' => 'data-id=' . $field->id, 'value' => $field->default_value];?>
                                                                @include(config('core.admin_theme').".form.fields.".$fd['type'], ['field' => $fd])
                                                            @elseif($field->type == 'textarea_editor')
                                                                <?php $fd = ['name' => 'field_' . $field->id, 'type' => 'textarea_editor', 'label' => $field->label, 'inner' => 'data-id=' . $field->id, 'value' => $field->default_value];?>
                                                                <div id="textarea_editor_{{ $field->id }}"
                                                                     class="textarea_editor">
                                                                    @include(config('core.admin_theme').".form.fields.".$fd['type'], ['field' => $fd])
                                                                </div>
                                                            @endif
                                                            @if($field->des != null)
                                                                <p>{!! $field->des !!}</p>
                                                            @endif
                                                            <button class="btn btn-primary next-slide next-slide_{{ $field->id }}"
                                                                    style="    margin-top: 10px;
    float: right;"
                                                                    type="button">Tiếp
                                                                theo
                                                            </button>
                                                        </div>
                                                        <div class="text-box huong_dan huong_dan_{{ $field->id }}"
                                                             style="display: none; margin-bottom: 10px;">
                                                            <p><strong>Ví dụ nhập:</strong><br></p>
                                                            {!! $field->tutorial !!}
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>

                                        </div>


                                        <div class="gap no-bottom">
                                            <div class="more-about">
                                                <div class="central-meta">
                                                    <span class="title2">Giới thiệu</span>
                                                    <div class="title2-content">{!! $course->intro !!}</div>
                                                </div>
                                                {{--<div class="central-meta">
                                                    <span class="title2">Mục lục</span>
                                                    <div class="title2-content">{!! $course->content !!}</div>
                                                </div>--}}
                                                <div id="list_lesson" class="central-meta">
                                                    <span class="title2">Tự tạo mẫu kinh doanh</span>
                                                    <button class="btn btn-primary khkd_start">Bắt đầu tạo</button>
                                                    <div class="title2-content title2-content-content">
                                                        <div class="col-md-8" style="float: left;">
                                                            <div class="page-createbox">
                                                                <form method="post" class="c-form"
                                                                      action="/tu-tao-mau-kinh-doanh">
                                                                    {!! csrf_field() !!}
                                                                    <input name="course_id" value="{{ $course->id }}"
                                                                           type="hidden">
                                                                    <div class="row">
                                                                        <div class="col-lg-12">

                                                                            @foreach($fields as $field)
                                                                                <div class="social-name">
                                                                                    <label for="field_{{ $field->id }}"
                                                                                           data-id="{{ $field->id }}">{!! strip_tags($field->label) !!}</label>
                                                                                    @if($field->type == 'input')
                                                                                        <input type="text"
                                                                                               name="fields[{{ $field->id }}]"
                                                                                               data-id="{{ $field->id }}"
                                                                                               placeholder="facebook.com/pitnik">
                                                                                    @elseif($field->type == 'textarea')
                                                                                        <?php $fd = ['name' => 'field_' . $field->id, 'type' => 'textarea', 'label' => $field->label, 'inner' => 'data-id=' . $field->id, 'value' => $field->default_value];?>
                                                                                        @include(config('core.admin_theme').".form.fields.".$fd['type'], ['field' => $fd])
                                                                                    @elseif($field->type == 'textarea_editor')
                                                                                        <?php $fd = ['name' => 'field_' . $field->id, 'type' => 'textarea_editor', 'label' => $field->label, 'inner' => 'data-id=' . $field->id, 'value' => $field->default_value];?>
                                                                                        <div id="textarea_editor_{{ $field->id }}"
                                                                                             class="textarea_editor">
                                                                                            @include(config('core.admin_theme').".form.fields.".$fd['type'], ['field' => $fd])
                                                                                        </div>
                                                                                    @endif
                                                                                    @if($field->des != null)
                                                                                        <p>{!! $field->des !!}</p>
                                                                                    @endif
                                                                                    <button class="next-slide next-slide_{{ $field->id }}"
                                                                                            type="button"
                                                                                            style="display: none;">Tiếp
                                                                                        theo
                                                                                    </button>
                                                                                </div>
                                                                            @endforeach
                                                                        </div>
                                                                        <div class="col-lg-12">
                                                                            <button class="main-btn" type="submit"
                                                                                    data-ripple="">Hoàn thành
                                                                            </button>
                                                                            <button class="main-btn3" type="submit"
                                                                                    data-ripple="">Hủy
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        {{--<div class="col-md-4 " style="float: left;">
                                                            <div class="mesge-area box-huong-dan">
                                                                <ul class="conversations ps-container ps-theme-default ps-active-y"
                                                                    data-ps-id="43d52fc7-8045-8363-9a62-f3f1d9445f98">
                                                                    <li class="me">
                                                                        <figure><img
                                                                                    src="/public/filemanager/userfiles/_thumbs/log_63648_636582646787728666-100x.png"
                                                                                    alt=""></figure>

                                                                        @foreach($fields as $field)
                                                                            <div class="text-box huong_dan huong_dan_{{ $field->id }}"
                                                                                 style="display: none;">
                                                                                <p><strong>Ví dụ nhập:</strong><br></p>
                                                                                {!! $field->tutorial !!}
                                                                            </div>
                                                                        @endforeach
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <script>
                                                                $(document).ready(function () {
                                                                    $('.page-createbox input, .page-createbox textarea').focus(function () {
                                                                        var id = $(this).data('id');

                                                                        $('.huong_dan').hide();
                                                                        $('.huong_dan_' + id).show();
                                                                    });
                                                                    $('.page-createbox label').click(function () {
                                                                        var id = $(this).data('id');
                                                                        $('#field_' + id).fadeIn(300);
                                                                        $('#textarea_editor_' + id).fadeIn(300);
                                                                        $('.next-slide').hide();
                                                                        $('.next-slide_' + id).show();
                                                                    });

                                                                    $('.next-slide').click(function () {
                                                                        $(this).hide();
                                                                        $(this).parent('.social-name').next().find('label').click();
                                                                        $(this).parent('.social-name').next().find('.next-slide').show();
                                                                    });
                                                                });

                                                            </script>
                                                        </div>--}}
                                                    </div>
                                                </div>
                                                <div class="central-meta">
                                                    <span class="title2">Thông tin tác giả</span>
                                                    @if(is_object($course->lecturer))
                                                        <div class="title2-content">
                                                            <div class="row">
                                                                <div class="col-12 col-md-2">
                                                                    <div class="avatar">
                                                                        <a href="{{asset('profile/'.@$course->lecturer->id)}}"><img
                                                                                    class="lazy"
                                                                                    data-src="{{asset('public/filemanager/userfiles/'.@$course->lecturer->image)}}"
                                                                                    alt="Tác giả: {{ $course->lecturer->name }}"></a>
                                                                    </div>
                                                                    <div class="uct-rate-gv">
                                                                        <ul>
                                                                            <li>
                                                                                <i class="fa fa-play-circle"
                                                                                   aria-hidden="true"></i>
                                                                                <span>
                                                                                    <?php
                                                                                    $count_lecturer = CommonHelper::getFromCache('count_lecturer' . $course->id, ['courses']);
                                                                                    if (!$count_lecturer) {
                                                                                        $count_lecturer = \Modules\ThemeCLB\Models\Course::where('lecturer_id', @$course->lecturer_id)->count();
                                                                                        CommonHelper::putToCache('count_lecturer' . $course->id, $count_lecturer, ['courses']);
                                                                                    }
                                                                                    ?>
                                                                                    {{ $count_lecturer }}</span>
                                                                                Mẫu kinh doanh
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-md-10">
                                                                    <div class="uct-name-gv">
                                                                        <a target="_blank"
                                                                           href="{{asset('profile/'.@$course->lecturer->id)}}">{{@$course->lecturer->name}}</a>
                                                                    </div>
                                                                    <div class="uct-des-gv">{{@$course->lecturer->level}}</div>
                                                                    <div class="uct-more-gv">
                                                                        {!! @$course->lecturer->intro !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="central-meta">
                                                    <span class="title2">Bình luận</span>
                                                    <div class="fb-comments"
                                                         data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"
                                                         data-numposts="5" data-width="100%"></div>
                                                </div>

                                            </div>
                                        </div>

                                        @include('themeclb::pages.business_template.partials.business_template_relate_category')

                                        @include('themeclb::pages.business_template.partials.business_template_relate_teacher')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- content -->
        {{--Popup thêm vào giỏ hàng--}}

        <div class="full_k_che close-pop" style="display: none"></div>
        <div class="popup-add-cart" style="display: none; border-radius: 5px">
            <i class="fa fa-times-circle-o fa-2x close-pop"
               style="position:absolute;top: -5px; right: -5px; cursor: pointer"></i>
            <div class="popup-add-cart-content">
                <p>Thêm thành công vào giỏ hàng!</p>
                <div class="btn-check">

                    <a class="btn-cart main-btn cmsmasters_button" href="/gio-hang">Xem giỏ hàng</a>
                    <a class="close-pop main-btn3" style="cursor: pointer;">Tiếp tục mua hàng</a>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('.add_to_cart').click(function () {

                    $.ajax({
                        url: '{{route('cart.ajaxAddCart')}}',
                        type: 'post',
                        data: {
                            id: '{{$course->id}}'
                        },
                        success: function (result) {
                            if (result.status == true) {
                                $('.popup-add-cart').show();
                                $('.full_k_che').show();
                            } else {
                                $('.strp-error').html('<span>' + result.msg + '</span>');
                            }
                        },
                        error: function (e) {
                            console.log(e.message);
                        }
                    })
                });
                $('.close-pop').click(function () {
                    $('.popup-add-cart').hide();
                    $('.full_k_che').hide();
                    location.reload();
                });
            });
        </script>
    </div>
@endsection
@section('head_script')
    <style>
        .box-huong-dan {
            position: fixed;
            top: 12%;
            right: 100px;
            width: 30%;
            display: inline-block;
            z-index: 99;
        }

        .conversations > li.me .text-box {
            max-width: unset;
        }

        .more-about .title2-content {
            overflow: hidden;
        }

        textarea.form-control {
            background: none !important;
            border: 0 !important;
        }

        .social-name label {
            cursor: pointer;
        }

        .c-form .social-name > label p {
            margin: 0;
        }

        form .social-name {
            margin: 0;
        }

        textarea.form-control,
        .textarea_editor {
            display: none;
        }
    </style>
    <style>
        .popup-add-cart {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 500px;
            height: 171px;
            background: #fff;
            border: 1px solid #ccc;
            margin: auto;
            z-index: 999;
        }

        .popup-add-cart p {
            text-align: center;
            margin: 15px 0;
        }

        .btn-check {
            text-align: center;
            padding: 10px;
        }

        a.btn-cart {
            background: #63cc50;
            text-transform: capitalize;
        }

        button.close-pop {
            background: #5467f1;
            border: none;
            text-transform: capitalize;
        }

        .full_k_che {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: 999;
            background: #00000038;
        }

        .avatar {
            text-align: center;
        }

        .avatar img {
            border-radius: 50%;
            border: 1px solid #d9d9d9;
            width: 125px;
            margin: auto;
            height: 125px;
        }

        .uct-rate-gv ul li {
            line-height: 24px;
            list-style: none;
            text-align: center;
        }

        .uct-rate-gv i {
            width: 21px;
        }

        .uct-rate-gv span {
            font-weight: bold;
        }

        .uct-name-gv {
            font-size: 16px;
            font-weight: bold;
        }

        a:hover {
            text-decoration: none;
        }

        a:active, a:hover {
            outline: 0;
        }

        .uct-des-gv {
            color: #727272;
            margin: 10px 0 20px 0;
        }

        .title2-content-content ul li {
            display: inline-block;
            width: 100%;
            padding: 0;
            margin: 0;
        }

        .title2-content-content ul li h4 {
            font-size: 15px;
            text-transform: uppercase;
            color: #50ad4e;
            padding: 15px 0;
            margin-bottom: 0;
            line-height: 20px;
            font-weight: 700;
        }

        .title2-content-content ul li span {
            padding: 0;
            display: inline-block;
            width: 100%;
        }

        .title2-content-content ul li span img {
            margin-right: 5px;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .owl-stage-outer li a {
            max-height: 205px;
            overflow: hidden;
            display: inline-block;
            width: 100%;
        }

        @media (min-width: 990px) {
            .popup-add-cart-content {
                margin-top: 10%;
            }
        }

        .c-form .social-name > label:hover {
            text-decoration: underline;
        }

        /*ẩn thanh sửa */
        span.cke_top.cke_reset_all {
            /*display: none;*/
        }

        .c-form .social-name > label {
            color: #1a0dab;
        }


        /*Menu tao mau khkd*/
        .menu-khkd .item .number {
            width: 50px;
            height: 50px;
            float: left;
            margin-right: 10px;
            border: 2px solid #c00000;
            color: #c00000;
            text-align: center;
            line-height: 50px;
            font-weight: bold;
        }

        .menu-khkd .item .name {
            overflow: hidden;
        }

        .menu-khkd .item .name > * {
            color: #000;
            font-weight: bold;
            text-transform: uppercase;
            text-decoration: none;
            cursor: pointer;
        }

        .menu-khkd .meta .total {
            text-align: left;
        }

        .menu-khkd ul.meta {
            padding-left: 70px;
        }

        .menu-khkd li.nav-item {
            width: 100%;
            display: inline-block;
            margin-top: 10px;
        }

        .menu-khkd .item {
            width: 100%;
            display: inline-block;
        }

        .menu-khkd .meta .total.active > a,
        .menu-khkd .meta .total:hover > a {
            cursor: pointer;
            font-weight: bold;
        }

        .menu-khkd .number > span {
            padding: 0;
        }

        div#textarea_editor_13 {
            display: block;
        }

        .social-name {
            margin-bottom: 0;
        }

        .total a {
            width: 100%;
            display: inline-block;
        }


        /*fix cung*/

        ul.meta.meta-3 {
            display: block !important;
        }

        li.total.total-6 > a {
            font-weight: bold;
        }

        .social-name {
            display: none;
        }

        .social-name.social-name-13 {
            display: block;
        }

        .tu-tao-mau-kd .text-box.huong_dan.huong_dan_12 {
            display: block !important;
        }

        ul.meta.meta-child {
            padding-left: 15px;
        }
    </style>
    <script type="text/javascript" src="{{ asset('public\ckeditor\ckeditor.js') }}"></script>
@endsection
@section('footer_script')
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce_editor.js') }}"></script>


    <script>
        $('.khkd_start').click(function () {
            $('div#list_lesson form label:first').click();
            $(this).hide();
        });

        // Click vao menu to hien menu con
        $('.menu-khkd .item .name').click(function () {
            console.log('f');
            $(this).parents('.item').find('ul.meta').slideDown(300);
            $(this).parents('.nav-item').siblings().find('ul.meta').slideUp(100);
        });
    </script>

    <script>
        var callCount{{ $field['name'] }} = 0;

        function countCallFunction{{ $field['name'] }}() {
            if (callCount{{ $field['name'] }} == 0) {
                initFunction{{ $field['name'] }}();
            }
            callCount{{ $field['name'] }} ++;
            return true;
        }

        function initFunction{{ $field['name'] }}() {
            textareaInit{{ $field['name'] }}();
        }


        $('#textarea-{{ $field['name'] }}, #form-group-{{ $field['name'] }}').click(function () {
            $('#textarea-{{ $field['name'] }}').hide();
            $('#{{ $field['name'] }}').show().click();
        });
        $(document).ready(function () {
            $('#{{ $field['name'] }}').click(function () {
                countCallFunction{{ $field['name'] }}();
            });
        });
        var observe;
        if (window.attachEvent) {
            observe = function (element, event, handler) {
                element.attachEvent('on' + event, handler);
            };
        } else {
            observe = function (element, event, handler) {
                element.addEventListener(event, handler, false);
            };
        }

        function textareaInit{{ $field['name'] }}() {
            var note = document.getElementById('{{ $field['name'] }}');

            function resize() {
                note.style.height = 'auto';
                note.style.height = note.scrollHeight + 'px';
            }

            /* 0-timeout to get the already changed note */
            function delayedResize() {
                window.setTimeout(resize, 0);
            }

            observe(note, 'change', resize);
            observe(note, 'cut', delayedResize);
            observe(note, 'paste', delayedResize);
            observe(note, 'drop', delayedResize);
            observe(note, 'keydown', delayedResize);

            note.focus();
            note.select();
            resize();
        }

    </script>
@endsection