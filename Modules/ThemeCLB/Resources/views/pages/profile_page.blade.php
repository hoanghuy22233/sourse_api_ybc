@extends('themeclb::layouts.wp')
@section('head_script')
    <style>
        .div-hr {
            margin-top: 0px !important;
            margin-bottom: 40px !important;
        }
        div#page-container {
            position: relative;
        }
        .w0 {
            width: 100% !important;
        }
        .w1,
        .w3,
        .w6 {
            width: 100% !important;
        }
        div#sidebar {
            display: none !important;
        }
        .he,
        .h5 {
            height: unset !important;
        }
    </style>
@endsection
@section('main_content')
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                        <h4 class="">{{ $pageOption['meta_title'] }}kjsbvsdvbsvbsvdb</h4>

                    </div>
                </div>
                <div class="div-hr vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_custom_1461899049711  vc_custom_1461899049711">
                        <span class="vc_sep_holder vc_sep_holder_l"><span
                                    style="border-color:rgb(54,70,115);border-color:rgba(54,70,115,0.08);"
                                    class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span
                                style="border-color:rgb(54,70,115);border-color:rgba(54,70,115,0.08);"
                                class="vc_sep_line"></span></span>
                </div>

                <div class="vc_row wpb_row vc_row-fluid vc_custom_1460521049287" id="su-kien">
                    @include('themeclb::pages.profile')
                </div>
            </div>
        </div>
    </div>
@endsection