@extends('themeclb::layouts.wp')
@section('head_script')

@endsection
@section('main_content')

    <div id="site-content">
        <!-- /#page-header -->


        <div id="page-body">
            <div class="wrapper">

                <div class="content-wrap">

                    <main id="main-content" class="content" itemprop="mainContentOfPage">
                        <div class="main-content-wrap">

                            <div class="content-inner">
                                <div class="vc_row wpb_row vc_row-fluid">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <h2 style="margin: 50px 0 10px 0;">TĂNG DOANH SỐ BÁN HÀNG CỦA
                                                            BẠN NGAY BÂY GIỜ!</h2>
                                                        <h4 class="no-margin-top">LIÊN HỆ CHUYÊN GIA CỦA NHÓM COSINE
                                                            NGAY HÔM NAY theo số <a class="scheme2"
                                                                                    href="tel:1 800 232 3485">0913 915
                                                                358</a></h4>

                                                    </div>
                                                </div>
                                                <div class="vc_empty_space" style="height: 50px"><span
                                                            class="vc_empty_space_inner"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-vc-full-width="true" data-vc-full-width-init="true"
                                     class="vc_row wpb_row vc_row-fluid vc_custom_1461748192227 vc_row-has-fill"
                                     style="position: relative; left: -104.5px; box-sizing: border-box; width: 1349px; padding-left: 104.5px; padding-right: 104.5px;">
                                    <div class="wpb_column vc_column_container vc_col-sm-4">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="iconbox ">
                                                    <div class="box-header">
                                                        <div class="box-icon"><i class="fa fa-map-marker"></i></div>
                                                        <h5 class="box-title">Chi nhánh Hà Nội</h5>
                                                    </div>
                                                    <div class="box-content">
                                                        <p>Hãy thử các tùy chọn tự phục vụ của chúng tôi. Có sẵn 24/7</p>
                                                        <p><strong>Điên thoại:</strong> 0913 915 358<br>
                                                            {{--<strong>Complaints:</strong> 1 800 232 3488<br>--}}
                                                            <a class="scheme2" href="#">ybcstartup@gmail.com</a></p>


                                                    </div>
                                                </div>
                                                <div class="vc_empty_space" style="height: 30px"><span
                                                            class="vc_empty_space_inner"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-4">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="iconbox ">
                                                    <div class="box-header">
                                                        <div class="box-icon"><i class="fa fa-map-marker"></i></div>
                                                        <h5 class="box-title">Chi nhánh tp Hồ Chí Minh</h5>
                                                    </div>
                                                    <div class="box-content">
                                                        <p>Hãy thử các tùy chọn tự phục vụ của chúng tôi. Có sẵn 24/7</p>
                                                        <p><strong>Điên thoại:</strong> 0913 915 358<br>
                                                            <a class="scheme2" href="#">clbdoanhnhantrehcm@gmail.com</a></p>


                                                    </div>
                                                </div>
                                                <div class="vc_empty_space" style="height: 30px"><span
                                                            class="vc_empty_space_inner"></span></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="vc_row-full-width vc_clearfix"></div>
                                <div class="vc_row wpb_row vc_row-fluid vc_custom_1461748029913">
                                    <div class="wpb_column vc_column_container vc_col-sm-4">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <h4 class="no-margin-top">Liên hệ với chúng tôi</h4>
                                                        <p>Hoàn thành biểu mẫu liên hệ dưới đây và một trong những Nhà tư vấn kinh doanh tận tâm của chúng tôi sẽ sớm liên hệ với bạn.</p>

                                                    </div>
                                                </div>
                                                <div class="vc_empty_space" style="height: 20px"><span
                                                            class="vc_empty_space_inner"></span></div>
                                                <div class="wpb_raw_code wpb_content_element wpb_raw_html">
                                                    <div class="wpb_wrapper">
                                                        <div class="social-links">
                                                            <a href="https://www.facebook.com/ybcstartup" target="_blank">
                                                                <i class="fa fa-facebook-official" style="background-color: #0a6aa1"></i>
                                                            </a>
                                                            <a href="https://www.facebook.com/groups/clbdoanhnhantrekhoinghiep.ybcstartup" class="footer-bottom__link">
                                                                <i class="fas fa-users footer-bottom__link-icon"style="background-color: dodgerblue"></i>
                                                            </a>
                                                            <a href="https://www.youtube.com/channel/UCJJVCVUbqSh52oiwU4UDfiÆQ" class="footer-bottom__link ytb">
                                                                <i class="fab fa-youtube footer-bottom__link-icon"style="background-color: red "></i>
                                                            </a>
                                                            <a href="https://www.tiktok.com/@ybcstartup?lang=vi-VN" class="footer-bottom__link">
                                                                <img src="/public/frontend/themes/YBC/img/tiktok.png" width="28" alt="" class="footer-bottom__link-icon">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="vc_empty_space" style="height: 30px"><span
                                                            class="vc_empty_space_inner"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column vc_column_container vc_col-sm-8">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                    <div class="screen-reader-response"></div>
                                                    <form action="{{route('post-contact')}}" method="post"
                                                          class="">
                                                        @if (Session('success'))
                                                            <div class="alert bg-success" role="alert">
                                                                <p style="color: red"><b>{!!session('success')!!}</b></p>
                                                            </div>
                                                        @endif
                                                        @if(Session::has('message') && !Auth::check())
                                                            <div class="alert text-center text-white " role="alert"
                                                                 style=" margin: 0; font-size: 16px;">
                                                                <a href="#" style="float:right;" class="alert-close"
                                                                   data-dismiss="alert">&times;</a>
                                                                <p style="color: red"><b>{{ Session::get('message') }}</b></p>
                                                            </div>
                                                        @endif
                                                        <div class="contact-form">
                                                            <div class="row">
                                                                <div class="columns columns-6">
                                                                    <span><span class="wpcf7-form-control-wrap text-693"><input
                                                                                    type="text" name="name"
                                                                                    size="40"
                                                                                    class="wpcf7-form-control wpcf7-text"
                                                                                    aria-invalid="false"
                                                                                    placeholder="Tên">
                                                                             @if($errors->has('name'))
                                                                                <p style="color: red"><b>{{ $errors->first('name') }}</b></p>
                                                                            @endif
                                                                        </span></span><span><span
                                                                                class="wpcf7-form-control-wrap email-414"><input
                                                                                    type="email" name="email"
                                                                                     size="40"
                                                                                    class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-email"
                                                                                    aria-invalid="false"
                                                                                    placeholder="Email">
                                                                             @if($errors->has('email'))
                                                                                <p style="color: red"><b>{{ $errors->first('email') }}</b></p>
                                                                            @endif
                                                                        </span></span><span><span
                                                                                class="wpcf7-form-control-wrap tel-577"><input
                                                                                    type="tel" name="tel"
                                                                                    size="40"
                                                                                    class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel"
                                                                                    aria-invalid="false"
                                                                                    placeholder="Điện thoại">
                                                                             @if($errors->has('tel'))
                                                                                <p style="color: red"><b>{{ $errors->first('tel') }}</b></p>
                                                                            @endif
                                                                        </span></span><span><span
                                                                                class="wpcf7-form-control-wrap text-31"><input
                                                                                    type="text" name="level"
                                                                                    size="40"
                                                                                    class="wpcf7-form-control wpcf7-text"
                                                                                    aria-invalid="false"
                                                                                    placeholder="Công việc" required>
                                                                        </span></span>
                                                                </div>
                                                                <div class="columns columns-6">
                                                                    <span><span class="wpcf7-form-control-wrap textarea-880"><textarea
                                                                                    name="content" cols="40"
                                                                                    rows="10"
                                                                                    class="wpcf7-form-control wpcf7-textarea"
                                                                                    aria-invalid="false"
                                                                                    placeholder="Ghi chú" required></textarea></span></span>
                                                                    <span>
                                                                                                        {!! csrf_field() !!}

                                                                       <button type="submit">Gửi</button>
                                                                    </span></div>
                                                            </div>
                                                        </div>
                                                        <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                    </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="vc_row-full-width vc_clearfix"></div>
                            </div>
                            <!-- /.content-inner -->

                        </div>
                    </main>
                    <!-- /#main-content -->

                </div>
                <!-- /.content-wrap -->

            </div>
            <!-- /.wrapper -->
        </div>
        <!-- /#page-body -->

    </div>
@endsection