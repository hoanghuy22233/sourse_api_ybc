<?php
$lesson_items=\Modules\ThemeCLB\Models\LessonItem::where('course_id',$course->id)->count();
$carts = Cart::content();
$cart_id = [];
foreach ($carts as $v) {
    $cart_id[] = $v->id;
}
if (isset(\Auth::guard('student')->user()->id)) {
    $order = \Modules\ThemeCLB\Models\Order::where('student_id', @\Auth::guard('student')->user()->id)->where('course_id', $course->id)->first();
}
?>
@extends('themeclb::layouts.default')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0&appId=1442327099412363&autoLogAppEvents=1"></script>
@section('khoahoc')
    class="active"
@endsection
@section('main_content')

    <style>
        .popup-add-cart {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 500px;
            height: 171px;
            background: #fff;
            border: 1px solid #ccc;
            margin: auto;
            z-index: 999;
        }

        .popup-add-cart p {
            text-align: center;
            margin: 15px 0;
        }

        .btn-check {
            text-align: center;
            padding: 10px;
        }

        a.btn-cart {
            background: #63cc50;
            text-transform: capitalize;
        }

        button.close-pop {
            background: #5467f1;
            border: none;
            text-transform: capitalize;
        }

        .full_k_che {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: 999;
            background: #00000038;
        }

        .avatar {
            text-align: center;
        }

        .avatar img {
            border-radius: 50%;
            border: 1px solid #d9d9d9;
            width: 125px;
            margin: auto;
            height: 125px;
        }

        .uct-rate-gv ul li {
            line-height: 24px;
            list-style: none;
            text-align: center;
        }

        .uct-rate-gv i {
            width: 21px;
        }

        .uct-rate-gv span {
            font-weight: bold;
        }

        .uct-name-gv {
            font-size: 16px;
            font-weight: bold;
        }

        a:hover {
            text-decoration: none;
        }

        a:active, a:hover {
            outline: 0;
        }

        .uct-des-gv {
            color: #727272;
            margin: 10px 0 20px 0;
        }

        .title2-content-content ul li {
            display: inline-block;
            width: 100%;
            padding: 0;
            margin: 0;
        }

        .title2-content-content ul li h4 {
            font-size: 15px;
            text-transform: uppercase;
            color: #50ad4e;
            padding: 15px 0;
            margin-bottom: 0;
            line-height: 20px;
            font-weight: 700;
        }

        .title2-content-content ul li span {
            padding: 0;
            display: inline-block;
            width: 100%;
        }

        .title2-content-content ul li span img {
            margin-right: 5px;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .owl-stage-outer li a {
            max-height: 205px;
            overflow: hidden;
            display: inline-block;
            width: 100%;
        }

        @media (min-width: 990px) {
            .popup-add-cart-content {
                margin-top: 10%;
            }
        }
    </style>
    @include('themeclb::partials.banner_header')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="col-lg-12">
                                    <div class="prod-detail">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="prod-avatar">
                                                    <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($course->image,'auto','auto') }}"
                                                         alt="{{ $course->name }}" class="lazy">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="full-postmeta">
                                                    <div class="left-detail-meta">
                                                        <h4>{{$course->name}}</h4>

                                                        <span><i class="fa fa-money-bill"></i> @if($course->base_price != 0)<strike>{{number_format(@$course->base_price,0,'','.')}}<sup>đ</sup></strike>@endif  <span
                                                                    style="color: red">{!! !empty($course->final_price) ? number_format(@$course->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span></span>
                                                        <span><i class="fas fa-tags"></i>Thể loại: {{ $course->type }}</span>
                                                        @if(is_object($course->lecturer))
                                                            <span><i class="fas fa-user"></i> Giảng viên: <a
                                                                        href="{{asset('profile/'.@$course->lecturer->id)}}">{{@$course->lecturer->name}}</a></span>
                                                        @endif
                                                        @if(@$course->contact != null)
                                                            <span><i class="fas fa-users"></i> {{@$course->contact->count()}} học viên đăng ký học</span>
                                                        @endif
                                                        <span><i class="fas fa-user"></i> Trình độ: {{$course->level}}</span>
                                                        @if(@$course->lesson != null)
                                                            @if(@$course->type == 'Text-Audio')
                                                            <span><i class="fas fa-play-circle"></i> Bài học: {{@$course->lesson->count()}} bài</span>
                                                                @else
                                                                <span><i class="fas fa-play-circle"></i> Số video: {{@$lesson_items}} video</span>
                                                                @endif
                                                        @endif
                                                        <span><i class="far fa-eye"></i> Xem được trên: {{$course->utilities}}</span>
                                                        <span><i class="far fa-clock"></i> Thời hạn: {{$course->date_of_possession==0 ? 'Mãi mãi':$course->date_of_possession}}</span>
                                                        <span><i class="fas fa-graduation-cap"></i> Chứng chỉ: {{$course->certificate}}</span>
                                                    </div>
                                                    <div class="member-des">
                                                        {{--                                                        <h5>Giới thiệu</h5>--}}
                                                        {{--                                                        <div>{{@$course->intro}}</div>--}}
                                                        <div class="bottom-meta">
                                                            @if(!\Auth::guard('student')->check())
                                                                @if (in_array($course->id, $cart_id))
                                                                    <a class="main-btn"
                                                                       href="/gio-hang"
                                                                       title="">Khóa học đã được thêm vào giỏ hàng</a>
                                                                @else
                                                                    <a title=""
                                                                       class="main-btn2"
                                                                       href="/gio-hang/add/{{$course->id}}">Mua ngay</a>
                                                                    <a class="main-btn add_to_cart"
                                                                       style="cursor: pointer;"
                                                                       title="" data-ripple="">Thêm vào giỏ</a>
                                                                @endif
                                                            @elseif(is_object(@$order))
                                                                @if(@$order->status == 1)
                                                                    @if($course->lesson!=null)
                                                                        {{--<a class="main-btn2"--}}
                                                                           {{--title=""--}}
                                                                           {{--href="{{asset('khoa-hoc/'.$course->slug.'/'.@$course->lesson->first()->lessonitem->first()->slug.'.html')}}">Học--}}
                                                                            {{--ngay</a>--}}
                                                                        <a class="main-btn2"
                                                                           title=""
                                                                           href="#list_lesson">Học
                                                                            ngay</a>
                                                                    @endif
                                                                @else
                                                                    <a class="main-btn" rel="nofollow"
                                                                       href="#"
                                                                       title="">Khóa học đang chờ admin kich hoạt</a>
                                                                @endif

                                                            @else
                                                                @if (in_array($course->id, $cart_id))
                                                                    <a class="main-btn"
                                                                       href="/gio-hang"
                                                                       title="">Khóa học đã được thêm vào giỏ hàng</a>
                                                                @else
                                                                    <a title=""
                                                                       class="main-btn2"
                                                                       href="/gio-hang/add/{{$course->id}}">Mua ngay</a>
                                                                    <a class="main-btn add_to_cart"
                                                                       style="cursor: pointer;"
                                                                       title="" data-ripple="">Thêm vào giỏ</a>
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="gap no-bottom">
                                            <div class="more-about">
                                                <div class="central-meta">
                                                    <span class="title2">Giới thiệu khóa học</span>
                                                    <div class="title2-content">{{$course->intro}}</div>
                                                </div>
                                                <div id="list_lesson" class="central-meta">
                                                    <span class="title2">Nội Dung Khóa Học</span>
                                                    <div class="title2-content title2-content-content">
                                                        <ul class="wrap-detail">
                                                            <?php
                                                            $lessons = $course->lesson;
                                                            ?>
                                                            @if(@$lessons != null)
                                                                @foreach(@$lessons as $k => $lesson)
                                                                    <li class="part">
                                                                        <h4 class="icon-arrow-down">{{$lesson->name}}</h4>
                                                                        <?php
                                                                        $lessonItem = $lesson->lessonItem;
                                                                        ?>
                                                                        @if(@$lessonItem->count()>0)
                                                                            <ul>
                                                                                @foreach(@$lessonItem as $lessonItem)
                                                                                    <li>
                                                                                        <span>
                                                                                            @if(@$order->status == 1)
                                                                                                <a href="{{asset('khoa-hoc/'.$course->slug.'/'.$lessonItem->slug.'.html')}}">
                                                                                                    <img class="lazy" data-src="https://media-kyna.cdn.vccloud.vn/img/icon-arrow-circle-right-line.png"
                                                                                                         alt=""> {{$lessonItem->name}}
                                                                                                </a>
                                                                                            @else
                                                                                                @if($lessonItem->publish == 1 && (!is_object(@$order) || @$order->status == 0))
                                                                                                    <img class="lazy" data-src="https://media-kyna.cdn.vccloud.vn/img/icon-arrow-circle-right-line.png"
                                                                                                         alt="">
                                                                                                    {{$lessonItem->name}}
                                                                                                    <a href="{{asset('khoa-hoc/'.$course->slug.'/'.$lessonItem->slug.'.html')}}"
                                                                                                       class="btn-sm btn-success">Học thử</a>
                                                                                                @elseif(is_object(@$order) && @$order->status == 1)
                                                                                                    <img class="lazy" data-src="https://media-kyna.cdn.vccloud.vn/img/icon-arrow-circle-right-line.png"
                                                                                                         alt="">
                                                                                                    <a href="{{asset('khoa-hoc/'.$course->slug.'/'.@$lessonItem->slug.'.html')}}"> {{$lessonItem->name}}</a>
                                                                                                @else
                                                                                                    <img class="lazy" data-src="https://media-kyna.cdn.vccloud.vn/img/icon-arrow-circle-right-line.png"
                                                                                                         alt=""> {{$lessonItem->name}}
                                                                                                @endif
                                                                                            @endif
                                                                                        </span>
                                                                                    </li>
                                                                                @endforeach
                                                                            </ul>
                                                                        @endif
                                                                    </li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="central-meta">
                                                    <span class="title2">Thông tin giảng viên</span>
                                                    @if(is_object($course->lecturer))
                                                        <div class="title2-content">
                                                            <div class="row">
                                                                <div class="col-12 col-md-2">
                                                                    <div class="avatar">
                                                                        <a href="{{asset('profile/'.@$course->lecturer->id)}}"><img
                                                                                    class="lazy"
                                                                                    data-src="{{asset('public/filemanager/userfiles/'.@$course->lecturer->image)}}"
                                                                                    alt="Giảng viên: {{ $course->lecturer->name }}"></a>
                                                                    </div>
                                                                    <div class="uct-rate-gv">
                                                                        <ul>
                                                                            <li>
                                                                                <i class="fa fa-users"
                                                                                   aria-hidden="true"></i>

                                                                                <span>0</span> học viên
                                                                            </li>
                                                                            <li>
                                                                                <i class="fa fa-play-circle"
                                                                                   aria-hidden="true"></i>
                                                                                <span>
                                                                                    <?php
                                                                                    $count_lecturer = CommonHelper::getFromCache('count_lecturer'. $course->id, ['courses']);
                                                                                    if (!$count_lecturer) {
                                                                                        $count_lecturer = \Modules\ThemeCLB\Models\Course::where('lecturer_id', @$course->lecturer_id)->count();
                                                                                        CommonHelper::putToCache('count_lecturer'. $course->id, $count_lecturer, ['courses']);
                                                                                    }
                                                                                    ?>
                                                                                        {{ $count_lecturer }}</span>
                                                                                khóa học
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-md-10">
                                                                    <div class="uct-name-gv">
                                                                        <a target="_blank"
                                                                           href="{{asset('profile/'.@$course->lecturer->id)}}">{{@$course->lecturer->name}}</a>
                                                                    </div>
                                                                    <div class="uct-des-gv">{{@$course->lecturer->level}}</div>
                                                                    <div class="uct-more-gv">
                                                                        {!! @$course->lecturer->intro !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="central-meta">
                                                    <span class="title2">Bình luận</span>
                                                    <div class="fb-comments"
                                                         data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"
                                                         data-numposts="5" data-width="100%"></div>
                                                </div>

                                            </div>
                                        </div>

                                        @include('themeclb::pages.course.partials.courses_relate_category')

                                        @include('themeclb::pages.course.partials.courses_relate_teacher')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- content -->
        {{--Popup thêm vào giỏ hàng--}}

        <div class="full_k_che close-pop" style="display: none"></div>
        <div class="popup-add-cart" style="display: none; border-radius: 5px">
            <i class="fa fa-times-circle-o fa-2x close-pop"
               style="position:absolute;top: -5px; right: -5px; cursor: pointer"></i>
            <div class="popup-add-cart-content">
                <p>Thêm thành công vào giỏ hàng!</p>
                <div class="btn-check">

                    <a class="btn-cart main-btn cmsmasters_button" href="/gio-hang">Xem giỏ hàng</a>
                    <a class="close-pop main-btn3" style="cursor: pointer;">Tiếp tục mua hàng</a>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('.add_to_cart').click(function () {

                    $.ajax({
                        url: '{{route('cart.ajaxAddCart')}}',
                        type: 'post',
                        data: {
                            id: '{{$course->id}}'
                        },
                        success: function (result) {
                            if (result.status == true) {
                                $('.popup-add-cart').show();
                                $('.full_k_che').show();
                            } else {
                                $('.strp-error').html('<span>' + result.msg + '</span>');
                            }
                        },
                        error: function (e) {
                            console.log(e.message);
                        }
                    })
                });
                $('.close-pop').click(function () {
                    $('.popup-add-cart').hide();
                    $('.full_k_che').hide();
                    location.reload();
                });
            });
        </script>
    </div>
@endsection