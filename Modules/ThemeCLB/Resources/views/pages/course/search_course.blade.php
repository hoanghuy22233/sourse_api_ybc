@extends('themeclb::layouts.default')

@section('main_content')
    @include('themeclb::partials.banner_header')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <style>
            .name_contact {
                margin-left: 5%;
                margin-top: 5%;
                width: 90%;
                height: 50%;
            }

            .mess_contact {
                margin-left: 5%;
                margin-top: 5%;
                width: 90%;
                height: 100%;
            }

            .input_contact {
                height: 45px;
            }

            .input_mess_contact {
                height: 130px;
            }
        </style>
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <h3 class="ml-3">Kết quả tìm kiếm với từ khóa: {{@$q}} </h3>
                                <div class="col-lg-12">
                                    <div class="row merged20">
                                        @if(isset($_GET['type'])&& $_GET['type']==2)
                                            @foreach($posts as $post)
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="central-meta">
                                                        <div class="blog-post">
                                                            <div class="friend-info">
                                                                <div class="friend-name">
                                                                    <span><i class="fa fa-globe"></i> {{date('d-m-Y',strtotime($post->created_at))}} </span>
                                                                </div>
                                                                <div class="post-meta">
                                                                    <figure>
                                                                        <a title="{{@$post->name}}"
                                                                           href="/tai-lieu/{{@$post->slug}}.html">
                                                                            <img class="lazy" alt="{{@$post->name}}"
                                                                                 data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image,361,200) }}">
                                                                        </a>
                                                                    </figure>
                                                                    <div class="description">
                                                                        <h2><a href="/tai-lieu/{{$post->slug}}.html"
                                                                               title="">{{@$post->name}}</a></h2>
                                                                        <p>
                                                                            {!!$post->intro!!}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @elseif(isset($_GET['type'])&& $_GET['type']==3)
                                            @foreach($quizzes as $quiz)
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="central-meta item">
                                                        <div class="classi-pst">
                                                            <div class="row merged10">
                                                                <div class="col-lg-4">
                                                                    <div class="image-bunch single">
                                                                        <figure><img class="lazy"
                                                                                    data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($quiz->image,244,122) }}"
                                                                                    alt="{{$quiz->name}}"></figure>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <div class="classi-pst-meta">
                                                                        <h6><a href="/bai-kiem-tra/{{$quiz->slug}}.html"
                                                                               title="">{{@$quiz->name}}</a>
                                                                        </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <p class="classi-pst-des">
                                                                        {!! @$quiz->intro !!}
                                                                    </p>
                                                                    @if(\Auth::guard('student')->check())
                                                                        <a class="active btn btn-info"
                                                                           href="/bai-kiem-tra/{{@$quiz->slug}}.html"
                                                                           style="    background-color: #fa6342;float: right;border-color: #bc482e;">Kiểm
                                                                            tra ngay</a>
                                                                    @else
                                                                        <a class="active btn btn-info"
                                                                           href="/dang-nhap"
                                                                           style="    background-color: #fa6342;float: right;border-color: #bc482e;">Đăng
                                                                            nhập để kiểm tra</a>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <?php
                                            //   Lấy ra các khóa học đã mua
                                            $course_buyed = [];
                                            if (\Auth::guard('student')->check()) {
                                                $course_ids = [];
                                                foreach($courses as $course){
                                                    $course_ids[] = $course->id;
                                                }

                                                $course_buyed = \Modules\ThemeCLB\Models\Order::where('student_id', @\Auth::guard('student')->user()->id)->whereNotNull('student_id')
                                                    ->whereIn('course_id', $course_ids)->where('status', 1)->pluck('course_id')->toArray();
                                            }
                                            ?>
                                            @foreach($courses as $course)
                                                <div class="col-lg-6 col-md-6">
                                                    @include('themeclb::partials.course_item')
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-12 paginatee">
                                            @if(isset($_GET['type'])&& $_GET['type']==2)
                                                {{ @$posts->appends(Request::all())->links() }}
                                            @elseif(isset($_GET['type'])&& $_GET['type']==3)
                                                {{ @$quizzes->appends(Request::all())->links() }}
                                            @else
                                                {{ @$courses->appends(Request::all())->links() }}
                                            @endif
                                        </div>
                                    </div>
                                </div><!-- centerl meta -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection