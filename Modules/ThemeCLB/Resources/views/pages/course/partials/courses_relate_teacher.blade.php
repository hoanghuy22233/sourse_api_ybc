@if(is_object($course->lecturer))
    <div class="central-meta">
        <span class="title2 ">KHÓA HỌC KHÁCH CỦA GIẢNG VIÊN: <strong>{{@$course->lecturer->name}}</strong></span>
        <ul class="suggested-frnd-caro">
            <?php
            $relates_lecturer = \Modules\ThemeCLB\Models\Course::where('lecturer_id', $course->lecturer_id)->where('id', '<>', $course->id)->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->take(8)->get();
            ?>
            @foreach($relates_lecturer as $v)
                <li >
                    <a href="/khoa-hoc/{{@$v->slug}}.html">
                    <img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image, 259, null) }}"
                         alt="{{@$v->name}}">
                    </a>
                    <div class="sugtd-frnd-meta">
                            <a href="/khoa-hoc/{{@$v->slug}}.html"
                               title="{{@$v->name}}">{{@$v->name}}</a>
                        <span><i class="fa fa-money-bill"></i> <strike>{{number_format(@$v->base_price,0,'','.')}}<sup>đ</sup></strike>  <span
                                    style="color: red">{!! !empty($v->final_price) ? number_format(@$v->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span></span>
                        <span><i class="fas fa-user"></i> Giảng viên: {{@$v->lecturer->name}} </span>
                        <span><i class="fas fa-tags"></i>Thể loại: {{ $v->type }} </span>
                        <span><i class="fas fa-play-circle"></i> Bài học: {{@$v->lesson->count()}} bài</span>
                        <span><i class="far fa-clock"></i> Thời hạn: {{$v->date_of_possession==0 ? 'Mãi mãi':$v->date_of_possession}}</span>
                    </div>
                </li>
            @endforeach
        </ul>
    </div><!-- suggested friends -->
@endif