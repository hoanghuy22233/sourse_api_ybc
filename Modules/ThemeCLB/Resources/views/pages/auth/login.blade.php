@extends('themeclb::layouts.wp')
@section('main_content')
    <section>
        <div class="gap no-gap signin whitish medium-opacity">
            <div class="bg-image"
                 style="background-image:url(/public/frontend/themes/edu/images/resources/theme-bg.jpg)"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="big-ad">
                            <figure><a href="/"><img class="lazy"
                                            data-src="{{ URL::asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                                            alt="{{ @$settings['name'] }}" style="max-width: 150px;"></a></figure>
                            <h1>Chào mừng bạn đến với {{ @$settings['name'] }}</h1>
                            <p>
                                {!! @$settings['web_description'] !!}
                            </p>

                            @include('themeclb::pages.auth.count_student')
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="we-login-register" style="background-color: #15416e; width: 350px">
                            <div class="form-title">
                                <i class="fa fa-key"></i>Đăng nhập
                                <span>Đăng nhập ngay bây giờ và gặp gỡ những người bạn tuyệt vời trên khắp thế giới.</span>
                            </div>
                            @if (Session('success'))
                                <div class="alert bg-success" role="alert">
                                    <p style="color: red"><b>{!!session('success')!!}</b></p>
                                </div>
                            @endif
                            @if(Session::has('message') && !Auth::check())
                                <div class="alert text-center text-white " role="alert"
                                     style=" margin: 0; font-size: 16px;">
                                    <a href="#" style="float:right;" class="alert-close"
                                       data-dismiss="alert">&times;</a>
                                    <p style="color: red"><b>{{ Session::get('message') }}</b></p>
                                </div>
                            @endif
                            <form class="we-form" method="post" action="/dang-nhap">
                                <div class="we-form">
                                    <input type="text" name="email" placeholder="Email hoặc điện thoại">
                                    @if($errors->has('email'))
                                        <p style="color: red"><b>{{ $errors->first('email') }}</b></p>
                                    @endif
                                </div>
                                <div class="we-form">
                                    <input type="password" name="password" placeholder="Password">
                                    @if($errors->has('password'))
                                        <p style="color: red"><b>{{ $errors->first('password') }}</b></p>
                                    @endif
                                </div>
                                <label><input type="checkbox" checked name="remember_me"> Nhớ đăng nhập</label>
                                <button type="submit" data-ripple="">Đăng nhập</button>
                                <a class="forgot underline" href="/quen-mat-khau" title="">Quên mật khẩu?</a>
                            </form>
                            <a class="with-smedia facebook" href="/login/facebook/redirect/" title="" data-ripple="">Đăng
                                nhập với facebook</a>
                            <a class="with-smedia google" href="/login/google/redirect/" title="" data-ripple="">Đăng
                                nhập với google</a>
                            <span>Bạn chưa có tài khoản ? <a class="we-account underline" href="/dang-ky" style="color: white"
                                                             title="">Đăng ký</a></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection