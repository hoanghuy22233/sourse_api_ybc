@extends('themeclb::layouts.wp')
@section('main_content')
    <section>
        <div class="gap no-gap signin whitish medium-opacity">
            <div class="bg-image"
                 style="background-image:url(/public/frontend/themes/edu/images/resources/theme-bg.jpg)"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="big-ad">
                            <figure><a href="/"><img class="lazy"
                                            data-src="{{ URL::asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                                            alt="{{ @$settings['name'] }}" style="max-width: 150px;"></a></figure>
                            <h1>Chào mừng bạn đến với {{ @$settings['name'] }}</h1>
                            <p>
                                {!! @$settings['web_description'] !!}
                            </p>

                            @include('themeclb::pages.auth.count_student')
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="we-login-register" style="background-color: #15416e; width: 350px; text-align: unset; color: unset">
                            <div class="form-title" style="color: white">
                                <i class="fa fa-key"></i>Đăng ký
                                <span>Đăng nhập ngay bây giờ và gặp gỡ những người bạn tuyệt vời trên khắp thế giới.</span>
                            </div>
                            <form class="we-form" method="post" action="/dang-ky">
                                <div class="we-form">
                                    <input type="text" name="name" placeholder="Họ tên" value="{{old('name')}}" style="width: 100%!important;">
                                    @if($errors->has('name'))
                                        <p style="color: red"><b>{{ $errors->first('name') }}</b></p>
                                    @endif
                                </div>
                                <div class="we-form">
                                    <input type="email" name="email" placeholder="Email" value="{{old('email')}}">
                                    @if($errors->has('email'))
                                        <p style="color: red"><b>{{ $errors->first('email') }}</b></p>
                                    @endif
                                </div>
                                <div class="we-form">
                                    <input type="password" name="password" placeholder="Mật khẩu">
                                    @if($errors->has('password'))
                                        <p style="color: red"><b>{{ $errors->first('password') }}</b></p>
                                    @endif
                                </div>
                                <div class="we-form">

                                <input type="text" name="tel" placeholder="SĐT">
                                @if($errors->has('tel'))
                                    <p style="color: red"><b>{{ $errors->first('tel') }}</b></p>
                                @endif
                                </div>
                                <?php $roles = \App\Models\Roles::where('admin_id', Auth::guard('admin')->user()->id)->pluck('display_name', 'id');
                                $role_id = @\App\Models\RoleAdmin::where('admin_id', $result->id)->where('company_id', Auth::guard('admin')->user()->id)->first()->role_id;
                                ?>
                                <div class="we-form">
                                <select name="role_id" style="width: 100%;">
                                    @foreach($roles as $id => $name)
                                        <option value="{{ $id }}" {{ $role_id == $id ? 'selected' : '' }}>{{ $name }}</option>
                                    @endforeach
                                </select>
                                </div>
                                {!! csrf_field() !!}
                                <button type="submit" data-ripple="">Đăng ký</button>
                            </form>
                            <a class="forgot underline" href="/quen-mat-khau" title="">Quên mật khẩu?</a>
                            <a class="with-smedia facebook center" href="/login/facebook/redirect/" title="" data-ripple="">Đăng
                                nhập với facebook</a>
                            <a class="with-smedia google center" href="/login/google/redirect/" title="" data-ripple="">Đăng
                                nhập với google</a>
                            <span>Bạn chưa có tài khoản? <a class="we-account underline" href="/dang-nhap" title="">Đăng nhập</a></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection