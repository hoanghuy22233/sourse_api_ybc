@extends('themeclb::layouts.default')
@section('main_content')
    <section>
        <div class="gap no-gap signin whitish medium-opacity">
            <div class="bg-image"
                 style="background-image:url(/public/frontend/themes/edu/images/resources/theme-bg.jpg)"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="big-ad">
                            <figure><img class="lazy" data-src="{{ URL::asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                                         alt="{{ @$settings['name'] }}" style="max-width: 150px;"></figure>
                            <h1>Chào mừng bạn đến với {{ @$settings['name'] }}</h1>
                            <p>
                                {!! @$settings['web_description'] !!}
                            </p>

                            @include('themeclb::pages.auth.count_student')
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="we-login-register" style="background-color: #003300;">
                            <div class="form-title">
                                @if (session('success'))
                                    <div class="alert bg-success" role="alert">
                                        <p style="color: red"><b>{!!session('success')!!}</b></p>
                                    </div>
                                @endif
                                <i class="fa fa-key"></i>Quên mật khẩu
                                <span>Nhập email của bạn vào, chúng tôi sẽ gửi vào mail cho bạn link để thay đổi mật khẩu</span>
                            </div>
                            <form class="we-form" method="post">
                                <input type="email" name="email" placeholder="Email" required>
                                <button type="submit" data-ripple="">Gửi yêu cầu</button>
                            </form>
                            {{--<a class="with-smedia facebook" href="#" title="" data-ripple="">Register with facebook</a>
                            <a class="with-smedia twitter" href="#" title="" data-ripple="">Register with twitter</a>--}}
                            <span>Bạn đã có tài khoản? <a class="we-account underline" href="/dang-nhap" title="">Đăng nhập</a></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection