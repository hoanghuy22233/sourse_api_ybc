@extends('themeclb::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
    @include('themeclb::template.top_bar')
    <!-- topbar -->
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themeclb::partials.student_menu', ['user' => $user])
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="central-meta">

                                        <span class="create-post">Giới thiệu</span>
                                        <div class="personal-head">
                                            <span class="f-title"><i class="fa fa-birthday-cake"></i> Ngày sinh:</span>
                                            <p>
                                                @if(@$user->birthday != '' && @$user->birthday != null)
                                                    {{date('d-m-Y',strtotime(@$user->birthday))}}
                                                @endif
                                            </p>
                                            <span class="f-title"><i class="fa fa-phone"></i> Điện thoại:</span>
                                            <p>
                                                {{@$user->phone}}
                                            </p>
                                            <span class="f-title"><i class="fa fa-male"></i> Giới tính:</span>
                                            <p>
                                                @if(@$user->gender === 1)
                                                    Nam
                                                @elseif(@$user->gender === 0)
                                                    Nữ
                                                @endif
                                            </p>
                                            <span class="f-title"><i class="fa fa-globe"></i> Địa chỉ:</span>
                                            <p>
                                                {{@$user->address}}
                                            </p>
                                            <span class="f-title"><i class="fa fa-envelope"></i> Email:</span>
                                            <p>
                                                <a href="mailto:{{@$user->email}}"
                                                   class="__cf_email__"
                                                   data-cfemail="cb9ba2bfa5a2a08bb2a4beb9a6aaa2a7e5a8a4a6">
                                                    {{@$user->email}}
                                                </a>
                                            </p>
                                            <span class="f-title"><i class="fa fa-location-arrow"></i> Trung tâm:</span>
                                            <p>
                                                {{ @\Modules\ThemeCLB\Models\Center::find($user->center_id)->name }}
                                            </p>
                                            @if($user->id == @\Auth::guard('student')->user()->id)
                                                <a href="/profile/edit"
                                                   style="    color: #007bff;text-decoration: underline; cursor: pointer;">Chỉnh
                                                    sửa profile</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8">
                                    <div class="central-meta">
                                        <span class="create-post">Thành tích học viên</span>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="gen-metabox">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="gen-metabox">
                                                    <span><i class="fa fa-plus"></i> Số khóa học đã đăng ký</span>
                                                    <p>
                                                        {{@\Modules\EduBill\Models\Order::where('student_id', $user->id)->where('status', 1)->count('course_id')}}
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="gen-metabox no-margin">
                                                    <span><i class="fa fa-bookmark"></i> Đánh giá</span>
                                                    <p>
                                                        {!! @$user->review !!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="central-meta">
                                        <span class="create-post">Khóa học đã mua <a href="/student/{{ @$user->id }}/khoa-hoc"
                                                                             title="">Xem tất cả</a></span>
                                        <ul class="suggested-frnd-caro">
                                            <?php
                                            $data = CommonHelper::getFromCache('course_of_student_' . $user->id, ['courses']);
                                            if (!$data) {
                                                $data = \Modules\EduCourse\Models\Course::join('orders', 'orders.course_id', '=', 'courses.id')
                                                    ->selectRaw('courses.*')
                                                    ->where('orders.status', 1)->where('orders.student_id', $user->id)->orderBy('orders.created_at', 'desc')->take(8)->get();
                                                CommonHelper::putToCache('course_of_student_' . $user->id, $data, ['courses']);
                                            }
                                            ?>
                                            @foreach($data as $v)
                                                <li >
                                                    <a href="/khoa-hoc/{{@$v->slug}}.html"><img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image,210,129) }}"
                                                         alt="{{@$v->name}}"></a>
                                                    <div class="sugtd-frnd-meta">
                                                        <a href="/khoa-hoc/{{@$v->slug}}.html"
                                                           title="{{@$v->name}}">{{@$v->name}}</a>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection
@section('head_script')
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/profile.css') }}?v={{ time() }}">
@endsection