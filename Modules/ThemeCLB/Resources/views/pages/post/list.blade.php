@extends('themeclb::layouts.wp')
@section('head_script')
    <style>
        .div-hr {
            margin-top: 0px !important;
            margin-bottom: 40px !important;
        }
    </style>
@endsection
@section('main_content')
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner">
            <div class="wpb_wrapper">
                <div class="wpb_text_column wpb_content_element ">
                    <div class="wpb_wrapper">
                        <h4 class="">{{ @$category->name }}{{ isset($keyword) ? 'Tìm kiếm với từ khóa: ' . $keyword : '' }}</h4>
                        <p>{{ @$category->intro }}</p>

                    </div>
                </div>
                <div class="div-hr vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_custom_1461899049711  vc_custom_1461899049711">
                        <span class="vc_sep_holder vc_sep_holder_l"><span
                                    style="border-color:rgb(54,70,115);border-color:rgba(54,70,115,0.08);"
                                    class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span
                                style="border-color:rgb(54,70,115);border-color:rgba(54,70,115,0.08);"
                                class="vc_sep_line"></span></span>
                </div>

                <div class="vc_row wpb_row vc_row-fluid vc_custom_1460521049287" id="su-kien">
                    @foreach($posts as $item)
                        <div class="wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                    <!-- BEGIN .imagebox -->
                                    <div class="imagebox  ">
                                        <div class="box-wrapper">

                                            <div class="box-image">
                                                <img width="700" height="500"
                                                     src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$item->image ) }}"
                                                     class="attachment-full" alt=""
                                                     sizes="(max-width: 700px) 100vw, 700px"/></div>


                                            <div class="box-header">
                                                <h3 class="box-title">
                                                    <a href="{{ \Modules\ThemeCLB\Helpers\ThemeCLBHelper::getPostSlug($item) }}"
                                                       target="_self">
                                                        {{$item->name}}</a>
                                                </h3>

                                            </div>

                                            <div class="box-content">
                                                <div class="box-desc">
                                                    {{$item->intro}}
                                                </div>


                                                <div class="box-button">
                                                    <a href="{{ \Modules\ThemeCLB\Helpers\ThemeCLBHelper::getPostSlug($item) }}"
                                                       target="_self">
                                                        Xem thêm </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- End .imagebox -->
                                    <div class="vc_empty_space" style="height: 30px"><span
                                                class="vc_empty_space_inner"></span></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                {{ @$posts->appends(Request::all())->links() }}
            </div>
        </div>
    </div>
@endsection