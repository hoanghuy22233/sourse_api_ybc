<div style="max-width:700px;margin:10px auto 20px">
    <div>
        <div style="display:inline-block;margin:0px auto">

            <table style="margin:0 auto;border-collapse:collapse;background-color:#fff;border-radius:10px">
                <tbody>
                <tr>
                    <td style="padding:10px;border-bottom:1px solid #e5e5e5">
                        <table style="width:100%;border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td style="width:140px">
                                    <img src="{{ asset('public/filemanager/userfiles/' . @$settings['logo'], null, 150) }}" style="max-width: 150px; max-height: 150px;">

                                </td>
                                <td style="padding-left:10px;font:16px/1.6 Arial;color:#000;font-weight:bold;text-align:right">
                                    {{@$settings['header_mail']}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>


                <tr>
                    <td style="padding:0 20px">
                        <div style="margin:20px 0 0">
                            <table style="width:660px;border-collapse:collapse;word-break:break-all;box-sizing:border-box; border: 1px solid #ddd">
                                <tbody>
                                <tr>
                                    <td colspan="3"
                                        style="text-align:center;font-weight: bold;font-size: 16px;padding: 10px;">
                                        Thông tin đơn hàng
                                        <br>
                                        <a href="{{ url('/admin/bill/' . $bill->id) }}" style="font-size: 13px;">Xem chi tiết đơn hàng</a>
                                    </td>
                                </tr>
                                <tr style="text-align: center; font-weight: bold; font-size: 14px; border: 1px solid #ddd;">
                                    <td style="border-right:1px solid #ddd">STT</td>
                                    <td style="border-right:1px solid #ddd">Tên khóa học</td>
                                    <td>Thành tiền</td>
                                </tr>

                                @foreach(@$bill->orders as $k=>$order)
                                    <tr style="border:1px solid #ddd">
                                        <td style="text-align: center; border-right: 1px solid #ddd ">{{ $k+1 }}</td>
                                        <td style="text-align: left; border-right: 1px solid #ddd "><a href="{{ asset('khoa-hoc/').$order->course->slug.'html' }}">{{ @$order->course->name }}</a></td>
                                        <td style="text-align: right; ">{{ number_format(@$order->course->final_price,0,'',',') }} <sup>đ</sup></td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="2" style="font-weight: bold;font-size: 16px; border-right: 1px solid #ddd; text-align: right;">Tổng tiền</td>
                                    <td style="text-align: right; ">{{ \Cart::total(0,'',',') }} <sup>đ</sup></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding:0 20px">
                        <div style="margin:20px 0 0">
                            <h3>Thông tin khách hàng</h3>
                            <strong>Họ & tên:</strong> {{ @$bill->student->name }}<br>
                            <strong>Email:</strong> {{ @$bill->student->email }}<br>
                            <strong>Số điện thoại:</strong> {{ @$bill->student->phone }}<br>
                            <strong>Địa chỉ:</strong> {{ @$bill->student->address }}<br>
                            <strong>Ảnh chụp hóa đơn:</strong><br> @if(@$bill->student->invoice_image != '')<img src="{{ asset('public/filemanager/userfiles/').@$bill->student->invoice_image }}">@endif<br>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="padding:10px 20px;background:#f8f8f8;border-top:1px solid #e5e5e5;border-radius:0 0 10px 10px"
                        id="m_-3744433325623262737m_5338770243025797393m_2723588182153450401m_5561831045043488677vi-VN">
                        <table style="width:100%;border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td style="width:160px;text-align:center">
                                    <img src="{{ asset('public/filemanager/userfiles/' . @$settings['logo'], null, 150) }}" style="max-width: 150px; max-height: 150px;">
                                </td>
                                <td style="padding-left:30px;font:15px/1.6 Arial;color:#666">
                                    {{@$settings['footer_mail']}}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
<tr><td></td></tr>
                </tbody>
            </table>

        </div>
    </div>
</div>
