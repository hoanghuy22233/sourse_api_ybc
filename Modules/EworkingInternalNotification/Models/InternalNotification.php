<?php

namespace Modules\EworkingInternalNotification\Models;


use Illuminate\Database\Eloquent\Model;
use Modules\EworkingAdmin\Models\Admin;

class InternalNotification extends Model
{
    protected $table = "internal_notifications";
    protected $fillable = ['id',
        'title','content','admin_id','company_id','deadline','admin_ids',
    ];
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }


    public function Admin_ids()
    {
        return $this->belongsTo(Admin::class,'admin_id');
    }
}
