<?php

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'internal_notification'], function () {
        Route::get('', 'Admin\InternalNotificationController@getIndex')->name('internal_notification')->middleware('permission:internal_notification_view');
        Route::get('publish', 'Admin\InternalNotificationController@getPublish')->name('internal_notification.publish')->middleware('permission:internal_notification_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\InternalNotificationController@add')->middleware('permission:internal_notification_add');
        Route::get('delete/{id}', 'Admin\InternalNotificationController@delete')->middleware('permission:internal_notification_delete');
        Route::post('multi-delete', 'Admin\InternalNotificationController@multiDelete')->middleware('permission:internal_notification_delete');

        Route::get('search-for-select2', 'Admin\InternalNotificationController@searchForSelect2')->name('internal_notification.search_for_select2')->middleware('permission:internal_notification_view');
        Route::get('{id}', 'Admin\InternalNotificationController@update')->middleware('permission:internal_notification_view');
        Route::post('{id}', 'Admin\InternalNotificationController@update')->middleware('permission:internal_notification_edit');
    });
});
