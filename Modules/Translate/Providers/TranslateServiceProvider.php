<?php

namespace Modules\Translate\Providers;

use App\Http\Helpers\CommonHelper;
use Cookie;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\Translate\Http\Controllers\TranslateController;

class TranslateServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('Translate', 'Database/Migrations'));
        $this->rendAsideMenu();
        $this->rendMultiLanguager();
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Đăng ký quyền
            $this->registerPermission();
        }
    }

    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('translate::partials.aside_menu.dashboard_after_booking');
        }, 2, 1);
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['translate_view', 'translate_add', 'translate_edit', 'translate_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function rendMultiLanguager()
    {
//        dd(Cookie::get('language'));
        $languageController = new TranslateController();
        $render_list_langs = $languageController->getHtmlLanguage();

        \Eventy::addFilter('block.header_topbar', function () use ($render_list_langs) {
            print '<div class="kt-header__topbar-item kt-header__topbar-item--langs">
	    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px" aria-expanded="false">
	        <span class="kt-header__topbar-icon" style="width: 50px">
				<img class="" src="' . CommonHelper::getUrlImageThumb($render_list_langs['present']['image'], 'false', 'false') . '" alt="' . $render_list_langs['present']['name'] . '">
			</span>
	    </div>
	    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround" >
	        <ul class="kt-nav kt-margin-t-1 kt-margin-b-10" style="width: 50px !important;">
                ' . $render_list_langs['list'] . '
            </ul>	    
        </div>
	</div>';
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('Translate', 'Config/config.php') => config_path('translate.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('Translate', 'Config/config.php'), 'translate'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/translate');

        $sourcePath = module_path('Translate', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/translate';
        }, \Config::get('view.paths')), [$sourcePath]), 'translate');

    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/translate');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'translate');
        } else {
            $this->loadTranslationsFrom(module_path('Translate', 'Resources/lang'), 'translate');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('Translate', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
