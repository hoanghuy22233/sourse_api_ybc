{{--{{dd($settings['default_language'])}}--}}

<a href="{{\Illuminate\Support\Facades\URL::to('/admin/translate/default')}}?language={{$item->code}}"
   title=" @if($settings['default_language'] != $item->code) Chọn làm @endif ngôn ngữ mặc định">
    <i style="font-size: 25px; cursor: pointer"
       class=" @if($settings['default_language'] == $item->code)flaticon2-check-mark @else text-danger flaticon2-cancel-music @endif "
    ></i></a>
