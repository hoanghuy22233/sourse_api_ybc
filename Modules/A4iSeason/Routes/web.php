<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'season'], function () {
        Route::get('', 'Admin\SeasonController@getIndex')->middleware('permission:season_view');
        Route::get('publish', 'Admin\SeasonController@getPublish')->name('season.publish')->middleware('permission:season_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\SeasonController@add')->middleware('permission:season_add');
        Route::get('delete/{id}', 'Admin\SeasonController@delete')->middleware('permission:season_delete');
        Route::post('multi-delete', 'Admin\SeasonController@multiDelete')->middleware('permission:season_delete');
        Route::get('search-for-select2', 'Admin\SeasonController@searchForSelect2')->name('season.search_for_select2')->middleware('permission:season_view');
        Route::get('{id}/timeline', 'Admin\SeasonController@timeLine');
        Route::get('{id}', 'Admin\SeasonController@update')->middleware('permission:season_edit');
        Route::post('{id}', 'Admin\SeasonController@update')->middleware('permission:season_edit');
    });

    Route::group(['prefix' => 'seedling'], function () {
        Route::get('', 'Admin\SeedlingController@getIndex')->middleware('permission:seedling_view');
        Route::get('publish', 'Admin\SeedlingController@getPublish')->name('seed.publish')->middleware('permission:seedling_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\SeedlingController@add')->middleware('permission:seedling_add');
        Route::get('delete/{id}', 'Admin\SeedlingController@delete')->middleware('permission:seedling_delete');
        Route::post('multi-delete', 'Admin\SeedlingController@multiDelete')->middleware('permission:seedling_delete');
        Route::get('search-for-select2', 'Admin\SeedlingController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:seedling_view');
        Route::get('{id}', 'Admin\SeedlingController@update')->middleware('permission:seedling_edit');
        Route::post('{id}', 'Admin\SeedlingController@update')->middleware('permission:seedling_edit');
    });
    Route::group(['prefix' => 'drainage'], function () {
        Route::get('', 'Admin\DrainageController@getIndex')->middleware('permission:drainage_view');
        Route::get('publish', 'Admin\DrainageController@getPublish')->name('seed.publish')->middleware('permission:drainage_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\DrainageController@add')->middleware('permission:drainage_add');
        Route::get('delete/{id}', 'Admin\DrainageController@delete')->middleware('permission:drainage_delete');
        Route::post('multi-delete', 'Admin\DrainageController@multiDelete')->middleware('permission:drainage_delete');
        Route::get('search-for-select2', 'Admin\DrainageController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:drainage_view');
        Route::get('{id}', 'Admin\DrainageController@update')->middleware('permission:drainage_edit');
        Route::post('{id}', 'Admin\DrainageController@update')->middleware('permission:drainage_edit');
    });
    Route::group(['prefix' => 'weeding'], function () {
        Route::get('', 'Admin\WeedingController@getIndex')->middleware('permission:weeding_view');
        Route::get('publish', 'Admin\WeedingController@getPublish')->name('seed.publish')->middleware('permission:weeding_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\WeedingController@add')->middleware('permission:weeding_add');
        Route::get('delete/{id}', 'Admin\WeedingController@delete')->middleware('permission:weeding_delete');
        Route::post('multi-delete', 'Admin\WeedingController@multiDelete')->middleware('permission:weeding_delete');
        Route::get('search-for-select2', 'Admin\WeedingController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:weeding_view');
        Route::get('{id}', 'Admin\WeedingController@update')->middleware('permission:weeding_edit');
        Route::post('{id}', 'Admin\WeedingController@update')->middleware('permission:weeding_edit');
    });
    Route::group(['prefix' => 'fertilizer'], function () {
        Route::get('', 'Admin\FertilizerController@getIndex')->middleware('permission:fertilizer_view');
        Route::get('publish', 'Admin\FertilizerController@getPublish')->name('seed.publish')->middleware('permission:fertilizer_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\FertilizerController@add')->middleware('permission:fertilizer_add');
        Route::get('delete/{id}', 'Admin\FertilizerController@delete')->middleware('permission:fertilizer_delete');
        Route::post('multi-delete', 'Admin\FertilizerController@multiDelete')->middleware('permission:fertilizer_delete');
        Route::get('search-for-select2', 'Admin\FertilizerController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:fertilizer_view');
        Route::get('{id}', 'Admin\FertilizerController@update')->middleware('permission:fertilizer_edit');
        Route::post('{id}', 'Admin\FertilizerController@update')->middleware('permission:fertilizer_edit');
    });
    Route::group(['prefix' => 'nutrition'], function () {
        Route::get('', 'Admin\NutritionController@getIndex')->middleware('permission:nutrition_view');
        Route::get('publish', 'Admin\NutritionController@getPublish')->name('seed.publish')->middleware('permission:nutrition_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\NutritionController@add')->middleware('permission:nutrition_add');
        Route::get('delete/{id}', 'Admin\NutritionController@delete')->middleware('permission:nutrition_delete');
        Route::post('multi-delete', 'Admin\NutritionController@multiDelete')->middleware('permission:nutrition_delete');
        Route::get('search-for-select2', 'Admin\NutritionController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:nutrition_view');
        Route::get('{id}', 'Admin\NutritionController@update')->middleware('permission:nutrition_edit');
        Route::post('{id}', 'Admin\NutritionController@update')->middleware('permission:nutrition_edit');
    });

    Route::group(['prefix' => 'plant_growth'], function () {
        Route::get('', 'Admin\PlantGrowthController@getIndex')->middleware('permission:plant_growth_view');
        Route::get('publish', 'Admin\PlantGrowthController@getPublish')->name('seed.publish')->middleware('permission:plant_growth_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\PlantGrowthController@add')->middleware('permission:plant_growth_add');
        Route::get('delete/{id}', 'Admin\PlantGrowthController@delete')->middleware('permission:plant_growth_delete');
        Route::post('multi-delete', 'Admin\PlantGrowthController@multiDelete')->middleware('permission:plant_growth_delete');
        Route::get('search-for-select2', 'Admin\PlantGrowthController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:plant_growth_view');
        Route::get('{id}', 'Admin\PlantGrowthController@update')->middleware('permission:plant_growth_edit');
        Route::post('{id}', 'Admin\PlantGrowthController@update')->middleware('permission:plant_growth_edit');
    });

    Route::group(['prefix' => 'technical_care'], function () {
        Route::get('', 'Admin\TechnicalCareController@getIndex')->middleware('permission:technical_care_view');
        Route::get('publish', 'Admin\TechnicalCareController@getPublish')->name('seed.publish')->middleware('permission:technical_care_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TechnicalCareController@add')->middleware('permission:technical_care_add');
        Route::get('delete/{id}', 'Admin\TechnicalCareController@delete')->middleware('permission:technical_care_delete');
        Route::post('multi-delete', 'Admin\TechnicalCareController@multiDelete')->middleware('permission:technical_care_delete');
        Route::get('search-for-select2', 'Admin\TechnicalCareController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:technical_care_view');
        Route::get('{id}', 'Admin\TechnicalCareController@update')->middleware('permission:technical_care_edit');
        Route::post('{id}', 'Admin\TechnicalCareController@update')->middleware('permission:technical_care_edit');
    });
    Route::group(['prefix' => 'harvest'], function () {
        Route::get('', 'Admin\HarvestController@getIndex')->middleware('permission:harvest_view');
        Route::get('publish', 'Admin\HarvestController@getPublish')->name('seed.publish')->middleware('permission:harvest_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\HarvestController@add')->middleware('permission:harvest_add');
        Route::get('delete/{id}', 'Admin\HarvestController@delete')->middleware('permission:harvest_delete');
        Route::post('multi-delete', 'Admin\HarvestController@multiDelete')->middleware('permission:harvest_delete');
        Route::get('search-for-select2', 'Admin\HarvestController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:harvest_view');

        Route::get('edit-last-item', 'Admin\HarvestController@editLastItem');
        Route::get('{id}', 'Admin\HarvestController@update')->middleware('permission:harvest_edit');
        Route::post('{id}', 'Admin\HarvestController@update')->middleware('permission:harvest_edit');
    });
    Route::group(['prefix' => 'weather_information'], function () {
        Route::get('', 'Admin\WeatherInformationController@getIndex')->middleware('permission:weather_information_view');
        Route::get('publish', 'Admin\WeatherInformationController@getPublish')->name('seed.publish')->middleware('permission:weather_information_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\WeatherInformationController@add')->middleware('permission:weather_information_add');
        Route::get('delete/{id}', 'Admin\WeatherInformationController@delete')->middleware('permission:weather_information_delete');
        Route::post('multi-delete', 'Admin\WeatherInformationController@multiDelete')->middleware('permission:weather_information_delete');
        Route::get('search-for-select2', 'Admin\WeatherInformationController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:weather_information_view');
        Route::get('{id}', 'Admin\WeatherInformationController@update')->middleware('permission:weather_information_edit');
        Route::post('{id}', 'Admin\WeatherInformationController@update')->middleware('permission:weather_information_edit');
    });
    Route::group(['prefix' => 'investment_material'], function () {
        Route::get('', 'Admin\InvestmentMaterialController@getIndex')->middleware('permission:investment_material_view');
        Route::get('publish', 'Admin\InvestmentMaterialController@getPublish')->name('seed.publish')->middleware('permission:investment_material_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\InvestmentMaterialController@add')->middleware('permission:investment_material_add');
        Route::get('delete/{id}', 'Admin\InvestmentMaterialController@delete')->middleware('permission:investment_material_delete');
        Route::post('multi-delete', 'Admin\InvestmentMaterialController@multiDelete')->middleware('permission:investment_material_delete');
        Route::get('search-for-select2', 'Admin\InvestmentMaterialController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:investment_material_view');
        Route::get('{id}', 'Admin\InvestmentMaterialController@update')->middleware('permission:investment_material_edit');
        Route::post('{id}', 'Admin\InvestmentMaterialController@update')->middleware('permission:investment_material_edit');
    });
    Route::group(['prefix' => 'disease'], function () {
        Route::get('', 'Admin\DiseaseController@getIndex')->middleware('permission:disease_view');
        Route::get('publish', 'Admin\DiseaseController@getPublish')->name('seed.publish')->middleware('permission:disease_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\DiseaseController@add')->middleware('permission:disease_add');
        Route::get('delete/{id}', 'Admin\DiseaseController@delete')->middleware('permission:disease_delete');
        Route::post('multi-delete', 'Admin\DiseaseController@multiDelete')->middleware('permission:disease_delete');
        Route::get('search-for-select2', 'Admin\DiseaseController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:disease_view');
        Route::get('{id}', 'Admin\DiseaseController@update')->middleware('permission:disease_edit');
        Route::post('{id}', 'Admin\DiseaseController@update')->middleware('permission:disease_edit');
    });
    Route::group(['prefix' => 'plant_medicine'], function () {
        Route::get('', 'Admin\PlantMedicineController@getIndex')->middleware('permission:plant_medicine_view');
        Route::get('publish', 'Admin\PlantMedicineController@getPublish')->name('seed.publish')->middleware('permission:plant_medicine_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\PlantMedicineController@add')->middleware('permission:plant_medicine_add');
        Route::get('delete/{id}', 'Admin\PlantMedicineController@delete')->middleware('permission:plant_medicine_delete');
        Route::post('multi-delete', 'Admin\PlantMedicineController@multiDelete')->middleware('permission:plant_medicine_delete');
        Route::get('search-for-select2', 'Admin\PlantMedicineController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:plant_medicine_view');
        Route::get('{id}', 'Admin\PlantMedicineController@update')->middleware('permission:plant_medicine_edit');
        Route::post('{id}', 'Admin\PlantMedicineController@update')->middleware('permission:plant_medicine_edit');
    });
    Route::group(['prefix' => 'analysis'], function () {
        Route::get('', 'Admin\AnalysisController@getIndex')->middleware('permission:analysis_view');
        Route::get('publish', 'Admin\AnalysisController@getPublish')->name('seed.publish')->middleware('permission:analysis_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\AnalysisController@add')->middleware('permission:analysis_add');
        Route::get('delete/{id}', 'Admin\AnalysisController@delete')->middleware('permission:analysis_delete');
        Route::post('multi-delete', 'Admin\AnalysisController@multiDelete')->middleware('permission:analysis_delete');
        Route::get('search-for-select2', 'Admin\AnalysisController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:analysis_view');
        Route::get('{id}', 'Admin\AnalysisController@update')->middleware('permission:analysis_edit');
        Route::post('{id}', 'Admin\AnalysisController@update')->middleware('permission:analysis_edit');
    });
});


Route::group(['prefix' => 'admin'], function () {
    Route::group(['prefix' => 'season'], function () {
        Route::get('export', 'Admin\SeasonController@export');
    });
});