<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'seasons', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\SeasonController@index')->middleware('api_permission:season_view');
        Route::post('', 'Admin\SeasonController@store')->middleware('api_permission:season_add');
        Route::get('{id}', 'Admin\SeasonController@show')->middleware('api_permission:season_view');
        Route::post('{id}', 'Admin\SeasonController@update')->middleware('api_permission:season_edit');
        Route::delete('{id}', 'Admin\SeasonController@delete')->middleware('api_permission:season_delete');
    });

    Route::group(['prefix' => 'nutritions', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\NutritionController@index')->middleware('api_permission:nutrition_view');
        Route::post('', 'Admin\NutritionController@store')->middleware('api_permission:nutrition_add');
        Route::get('{id}', 'Admin\NutritionController@show')->middleware('api_permission:nutrition_view');
        Route::post('{id}', 'Admin\NutritionController@update')->middleware('api_permission:nutrition_edit');
        Route::delete('{id}', 'Admin\NutritionController@delete')->middleware('api_permission:nutrition_delete');
    });

    Route::group(['prefix' => 'drainages', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\DrainageController@index')->middleware('api_permission:drainage_view');
        Route::post('', 'Admin\DrainageController@store')->middleware('api_permission:drainage_add');
        Route::get('{id}', 'Admin\DrainageController@show')->middleware('api_permission:drainage_view');
        Route::post('{id}', 'Admin\DrainageController@update')->middleware('api_permission:drainage_edit');
        Route::delete('{id}', 'Admin\DrainageController@delete')->middleware('api_permission:drainage_delete');
    });


    Route::group(['prefix' => 'fertilizers', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\FertilizerController@index')->middleware('api_permission:fertilizer_view');
        Route::post('', 'Admin\FertilizerController@store')->middleware('api_permission:fertilizer_add');
        Route::get('{id}', 'Admin\FertilizerController@show')->middleware('api_permission:fertilizer_view');
        Route::post('{id}', 'Admin\FertilizerController@update')->middleware('api_permission:fertilizer_edit');
        Route::delete('{id}', 'Admin\FertilizerController@delete')->middleware('api_permission:fertilizer_delete');
    });


    Route::group(['prefix' => 'diseases', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\DiseaseController@index')->middleware('api_permission:disease_view');
        Route::post('', 'Admin\DiseaseController@store')->middleware('api_permission:disease_add');
        Route::get('{id}', 'Admin\DiseaseController@show')->middleware('api_permission:disease_view');
        Route::post('{id}', 'Admin\DiseaseController@update')->middleware('api_permission:disease_edit');
        Route::delete('{id}', 'Admin\DiseaseController@delete')->middleware('api_permission:disease_delete');
    });


    Route::group(['prefix' => 'plant_growths', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\PlantGrowthController@index')->middleware('api_permission:plant_growth_view');
        Route::post('', 'Admin\PlantGrowthController@store')->middleware('api_permission:plant_growth_add');
        Route::get('{id}', 'Admin\PlantGrowthController@show')->middleware('api_permission:plant_growth_view');
        Route::post('{id}', 'Admin\PlantGrowthController@update')->middleware('api_permission:plant_growth_edit');
        Route::delete('{id}', 'Admin\PlantGrowthController@delete')->middleware('api_permission:plant_growth_delete');
    });


    Route::group(['prefix' => 'plant_medicines', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\PlantMedicineController@index')->middleware('api_permission:plant_medicine_view');
        Route::post('', 'Admin\PlantMedicineController@store')->middleware('api_permission:plant_medicine_add');
        Route::post('{id}', 'Admin\PlantMedicineController@update')->middleware('api_permission:plant_medicine_edit');
        Route::delete('{id}', 'Admin\PlantMedicineController@delete')->middleware('api_permission:plant_medicine_delete');
        Route::get('{id}', 'Admin\PlantMedicineController@show')->middleware('api_permission:plant_medicine_view');
    });

    Route::group(['prefix' => 'technical_cares', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\TechnicalCareController@index')->middleware('api_permission:technical_care_view');
        Route::post('', 'Admin\TechnicalCareController@store')->middleware('api_permission:technical_care_add');
        Route::get('{id}', 'Admin\TechnicalCareController@show')->middleware('api_permission:technical_care_view');
        Route::post('{id}', 'Admin\TechnicalCareController@update')->middleware('api_permission:technical_care_edit');
        Route::delete('{id}', 'Admin\TechnicalCareController@delete')->middleware('api_permission:technical_care_delete');
    });



    Route::group(['prefix' => 'harvests'], function () {
        Route::post('search-by-qr-code', 'Admin\HarvestController@qrCode');
        Route::post('search-by-qr-code-analysis', 'Admin\HarvestController@qrCodeAnalysis');
        Route::get('', 'Admin\HarvestController@index')->middleware('api_permission:harvest_view');
        Route::post('', 'Admin\HarvestController@store')->middleware('api_permission:harvest_add');
        Route::get('{id}', 'Admin\HarvestController@show')->middleware('api_permission:harvest_view');
        Route::post('{id}', 'Admin\HarvestController@update')->middleware('api_permission:harvest_edit');
        Route::delete('{id}', 'Admin\HarvestController@delete')->middleware('api_permission:harvest_delete');
    });

    Route::group(['prefix' => 'investment_materials', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\InvestmentMaterialController@index')->middleware('api_permission:investment_material_view');
        Route::post('', 'Admin\InvestmentMaterialController@store')->middleware('api_permission:investment_material_add');
        Route::get('{id}', 'Admin\InvestmentMaterialController@show')->middleware('api_permission:investment_material_view');
        Route::post('{id}', 'Admin\InvestmentMaterialController@update')->middleware('api_permission:investment_material_edit');
        Route::delete('{id}', 'Admin\InvestmentMaterialController@delete')->middleware('api_permission:investment_material_delete');
    });

    Route::group(['prefix' => 'weather_informations', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\WeatherInformationController@index')->middleware('api_permission:weather_information_view');
        Route::post('', 'Admin\WeatherInformationController@store')->middleware('api_permission:weather_information_add');
        Route::get('{id}', 'Admin\WeatherInformationController@show')->middleware('api_permission:weather_information_view');
        Route::post('{id}', 'Admin\WeatherInformationController@update')->middleware('api_permission:weather_information_edit');
        Route::delete('{id}', 'Admin\WeatherInformationController@delete')->middleware('api_permission:weather_information_delete');
    });
    Route::group(['prefix' => 'seedlings', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\SeedlingController@index')->middleware('api_permission:seedling_view');
        Route::post('', 'Admin\SeedlingController@store')->middleware('api_permission:seedling_add');
        Route::get('{id}', 'Admin\SeedlingController@show')->middleware('api_permission:seedling_view');
        Route::post('{id}', 'Admin\SeedlingController@update')->middleware('api_permission:seedling_edit');
        Route::delete('{id}', 'Admin\SeedlingController@delete')->middleware('api_permission:seedling_delete');
    });


    Route::group(['prefix' => 'analysiss', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\AnalysisController@index')->middleware('api_permission:analysis_view');
        Route::post('', 'Admin\AnalysisController@store')->middleware('api_permission:analysis_add');
        Route::get('{id}', 'Admin\AnalysisController@show')->middleware('api_permission:analysis_view');
        Route::post('{id}', 'Admin\AnalysisController@update')->middleware('api_permission:analysis_edit');
        Route::delete('{id}', 'Admin\AnalysisController@delete')->middleware('api_permission:analysis_delete');

    });

});

