<?php

namespace Modules\A4iSeason\Http\Controllers\Api\Admin;

use App\Models\Admin;
use Modules\A4iMedicineWarehouse\Models\MedicineWarehouse;
use Modules\A4iSeason\Http\Helpers\SeasonHelper;
use Modules\A4iSeason\Models\{Analysis,
    Disease,
    Drainage,
    Fertilizer,
    FertilizerWarehouse,
    Herbicide,
    Season,
    Seed,
    PlantMedicine,
    Nutrition,
    TechnicalCare,
    InvestmentMaterial};
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\A4iSeason\Models\Harvest;
use ParagonIE\Sodium\Core\Curve25519\H;
use Validator;

class HarvestController extends Controller
{

    protected $module = [
        'code' => 'harvest',
        'table_name' => 'harvests',
        'label' => 'Lịch sử thu hoạch',
        'modal' => 'Modules\A4iSeason\Models\Harvest',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'land_id' => [
            'query_type' => '='
        ],
        'season_id' => [
            'query_type' => '='
        ]
    ];
    protected $method_hervest = [
        1 => 'Chuyển sang trồng cây khác',
        2 => 'Trồng tiếp nhưng theo chu kỳ mới',
        3 => 'Cho đất nghỉ, để ải',
    ];
    protected $price_unit_type = [
        '/kg' => '/kg',
        '/tấn' => '/tấn',
    ];
    protected $quantity_unit_type = [
        'kg' => 'kg',
        'tấn' => 'tấn',
    ];


    protected $standards_achieved_type = [
    '0' => 'không có tiêu chuẩn',
    '1' => 'vietgap',
    '2' => 'global',
    '3' => 'gap',
    '4' => 'organicc',
];



    public function qrCode(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [

                'id' => 'required',
            ], [
                'id.required' => 'Bắt buộc phải nhập id!'
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Thất bại',
                    'errors' => $validator->errors(),
                    'data' => null,
                    'code' => 401
                ]);
            } else {

                $harvest = Harvest::find($request->id);
                $ressult = [
                    'tree_name' => $harvest->season->tree_name,
                    'date_season' => $harvest->season->implementation_date,
                    'cultivated_area' => $harvest->season->land->acreage,
                    'user_name' => $harvest->season->land->admin->name,
                    'lat' => $harvest->season->land->lat,
                    'long' => $harvest->season->land->long,
                    'supplier' => $harvest->season->seed,
                    'disease' => $harvest->season->disease,
                    'time_harvest' => $harvest->implementation_date,
                    'quantity_harvest' => $harvest->quantity,
                    'address' => $harvest->season->land->address,
                    'analysis_results' => $harvest->analysis,

                ];

                $harvest_prev = Harvest::where('season_id', $harvest->season->id)->where('implementation_date', '<', $harvest->implementation_date)->orderBy('implementation_date', 'desc')->first();
                if (is_object($harvest_prev)) {
                    $harvest_prev_implementation_date = $harvest_prev->implementation_date;
                } else {
                    $harvest_prev_implementation_date = '1990-12-12 00:00:00';
                }

                $fertilizer_warehouse_ids = Fertilizer::where('season_id', $harvest->season->id)->where('implementation_date', '>=', $harvest_prev_implementation_date)
                    ->where('implementation_date', '<=', $harvest->implementation_date)->pluck('fertilizer_warehouse_id')->toArray();
                if (!empty($fertilizer_warehouse_ids)) {
                    $ressult['fertilizers'] = FertilizerWarehouse::whereIn('id', $fertilizer_warehouse_ids)->pluck('name')->toArray();
                }

                $plant_medicine_ids = PlantMedicine::where('season_id', $harvest->season->id)->where('implementation_date', '>=', $harvest_prev_implementation_date)
                    ->where('implementation_date', '<=', $harvest->implementation_date)->pluck('medicine_warehous_id')->toArray();
                if (!empty($plant_medicine_ids)) {
                    $ressult['plant_medicine'] = MedicineWarehouse::whereIn('id', $plant_medicine_ids)->pluck('name')->toArray();
                }



                $technical_care_ids = TechnicalCare::where('season_id', $harvest->season->id)->where('implementation_date', '>=', $harvest_prev_implementation_date)
                    ->where('implementation_date', '<=', $harvest->implementation_date)->pluck('herbicide_id')->toArray();
                if (!empty($technical_care_ids)) {
                    $ressult['herbicides'] = Herbicide::whereIn('id', $technical_care_ids)->pluck('name')->toArray();
                }




                $plant_medicine_date = Harvest::where('season_id', $harvest->season->id)->where('implementation_date', '>=', $harvest_prev_implementation_date)
                    ->where('implementation_date', '<=', $harvest->implementation_date)->pluck('season_id')->toArray();
                if (!empty($plant_medicine_date)) {
                    $ressult['plant_medicine_date'] = PlantMedicine::whereIn('id', $plant_medicine_date)->pluck('implementation_date')->toArray();
                }


                $ressult['standards_achieved'] = array_key_exists($harvest->standards_achieved, $this->standards_achieved_type) ? $this->standards_achieved_type[$harvest->standards_achieved] : '';
                $ressult['image_standard'] = asset('public/filemanager/userfiles/' . $harvest->image_standard);

                return response()->json([
                    'status' => true,
                    'msg' => 'lấy danh sách thành công',
                    'errors' => (object)[],
                    'data' => $ressult,
                    'code' => 201
                ]);
            }

        } catch (\Exception $ex) {

            return response()->json([
                'status' => false,
                'msg' => 'Thất bại',
                'errors' => $ex->getMessage(),
                'data' => null,
                'code' => 401

            ]);
        }
    }














//    public function qrCodeAnalysis(Request $request)
//    {
//
//        try {
//
//            $validator = Validator::make($request->all(), [
//
//                'id' => 'required',
//            ], [
//                'id.required' => 'Bắt buộc phải nhập id!'
//            ]);
//            if ($validator->fails()) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Thất bại',
//                    'errors' => $validator->errors(),
//                    'data' => null,
//                    'code' => 401
//                ]);
//            } else {
//
//                $harvest = Harvest::find($request->id);
//                $admin_id=[];
//                foreach ($harvest->analysis as $k => $v) {
//                    $v->admin_id = [
//                        'admin_name' => $v->admin->name,
//                        'admin_avatar' => asset('public/filemanager/userfiles/' . $harvest->admin->image),
//                    ];
//                    unset($v->admin);
//
//                }
//                $ressult = [
//                    'user_name' => $harvest->admin->name,
//                    'analysis_results' => $harvest->analysis,
//                ];
//
//
//
//
//                return response()->json([
//                    'status' => true,
//                    'msg' => 'lấy danh sách thành công',
//                    'errors' => (object)[],
//                    'data' => $ressult,
//                    'code' => 201
//                ]);
//            }
//
//        } catch (\Exception $ex) {
//
//            return response()->json([
//                'status' => false,
//                'msg' => 'Thất bại',
//                'errors' => $ex->getMessage(),
//                'data' => null,
//                'code' => 401
//
//            ]);
//        }
//    }






    public function index(Request $request)
    {

        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Harvest::leftJoin('seasons', 'seasons.id', '=', 'harvests.season_id')
                ->join('lands', 'lands.id', '=', 'harvests.land_id')
                ->selectRaw('harvests.id,
                 harvests.quantity,
                 harvests.productivity,
                 harvests.method,
                  harvests.price,
                  harvests.implementation_date,
                  harvests.image,
                   lands.id as land_id,
                 lands.acreage as lands_acreage')
                ->whereRaw($where);


            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());

            foreach ($listItem as $k => $v) {
                $v->method = array_key_exists($v->method, $this->method_hervest) ? $this->method_hervest[$v->method] : '';
                $v->image = asset('public/filemanager/userfiles/' . $v->image);
                $v->land_acreage = [
                    'land_id' => $v->land_id,
                    'acreage' => $v->lands_acreage,
                ];
                unset($v->land_id);
                unset($v->lands_acreage);
            }


            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = Harvest::leftJoin('seasons', 'seasons.id', '=', 'harvests.season_id')
                ->join('lands', 'lands.id', '=', 'harvests.land_id')
                ->selectRaw('harvests.id,
                 harvests.quantity,
                 harvests.productivity,
                 harvests.method,
                  harvests.price,
                  harvests.implementation_date,
                  harvests.image,
                  harvests.lands_acreage,
                  harvests.quantity_unit,
                  harvests.price_unit,
                  harvests.season_id,
                 lands.acreage as lands_acreage')->where('harvests.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }
            $item->price_unit_optons = $this->price_unit_type;
            $item->quantity_unit_optons = $this->quantity_unit_type;
            $item->total_revenue = SeasonHelper::getPriceHarvest($item);
            $item->total_cost = SeasonHelper::getCostSeason($item->season_id,$item);
            $item->image = asset('public/filemanager/userfiles/' . $item->image);
            foreach (explode('|', $item->image_extra) as $img) {
                if ($img != '') {
                    $image_extra[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->method = array_key_exists($item->method, $this->method_hervest) ? $this->method_hervest[$item->method] : '';
            unset($item->season_id);



            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();

            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = CommonHelper::saveFile($image, 'harvest');
                    }
                } else {
                    $data['image'] = CommonHelper::saveFile($request->file('image'), 'harvest');
                }
            }
            $item = Harvest::create($data);
            $season = Season::find($item->season_id);
            if (in_array($request->get('method'), ['1', '3'])) {   //  Dừng mùa vụ
                $season->status = 0;
                $season->save();
            } else {
                $season->status = 1;
                $season->save();
            }
            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {

        $item = Harvest::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = CommonHelper::saveFile($image, 'harvest');
                }
            } else {
                $data['image'] = CommonHelper::saveFile($request->file('image'), 'harvest');
            }
        }

        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();


        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (Harvest::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }


    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
