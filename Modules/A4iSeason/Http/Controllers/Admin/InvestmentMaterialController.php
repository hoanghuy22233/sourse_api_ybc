<?php

namespace Modules\A4iSeason\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Validator;
use Illuminate\Http\Request;
use Auth;

class InvestmentMaterialController extends CURDBaseController
{

    protected $module = [
        'code' => 'investment_material',
        'table_name' => 'investment_materials',
        'label' => 'Nguyên liệu đầu vào',
        'modal' => '\Modules\A4iSeason\Models\InvestmentMaterial',
        'list' => [
            ['name' => 'image_seedling', 'type' => 'image', 'class' => '', 'label' => 'Hình ảnh'],
            ['name' => 'fertilizer_id', 'type' => 'relation', 'label' => 'Phân bón', 'object' => 'fertilizer', 'display_field' => 'name'],
            ['name' => 'plant_medicine_id', 'type' => 'relation', 'label' => 'Thuốc bảo vệ thực vật', 'object' => 'plant_medicine', 'display_field' => 'name'],
            ['name' => 'herbicide_id', 'type' => 'relation', 'label' => 'Thuốc diệt cỏ', 'object' => 'herbicide', 'display_field' => 'name'],
            ['name' => 'amount', 'type' => 'text', 'class' => '', 'label' => 'Số lượng'],
            ['name' => 'facility_name', 'type' => 'text', 'class' => '', 'label' => 'Tên cơ sở bán'],
            ['name' => 'implementation_date', 'type' => 'date_vi', 'class' => '', 'label' => 'Ngày thực hiện'],
            ['name' => 'action', 'type' => 'custom', 'td' => 'a4iseason::list.td.action', 'class' => '', 'label' => '#'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'fertilizer_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Tên phân bón', 'model' => \Modules\A4iSeason\Models\FertilizerWarehouse::class, 'display_field' => 'name',],
                ['name' => 'plant_medicine_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Thuốc bảo vệ thực vật', 'model' => \Modules\A4iMedicineWarehouse\Models\MedicineWarehouse::class, 'display_field' => 'name',],
                ['name' => 'herbicide_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Thuốc diệt cỏ', 'model' => \Modules\A4iSeason\Models\Herbicide::class, 'display_field' => 'name',],
                ['name' => 'amount', 'type' => 'text', 'class' => '', 'label' => 'Số lượng', 'des' => 'Ví dụ: 1 bao'],
                ['name' => 'image_seedling', 'type' => 'file_image', 'class' => '', 'label' => 'Hình ảnh cây giống'],
                ['name' => 'image_bill', 'type' => 'file_image', 'class' => '', 'label' => 'Hình ảnh hóa đơn'],
                ['name' => 'cost', 'type' => 'text', 'class' => 'number_price', 'label' => 'Chi phí', 'des' => 'Đơn vị: VNĐ'],
                ['name' => 'implementation_date', 'type' => 'date', 'class' => '', 'label' => 'Ngày thực hiện', 'object' => 'details'],

            ],
            'base_tab' => [
                ['name' => 'facility_name', 'type' => 'text', 'class' => '', 'label' => 'Tên cơ sở bán'],
                ['name' => 'tel', 'type' => 'text', 'class' => '', 'label' => 'SĐT'],
                ['name' => 'email', 'type' => 'text', 'class' => '', 'label' => 'Email'],
                ['name' => 'province_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Tỉnh/Thành', 'model' => \Modules\A4iLocation\Models\Province::class, 'display_field' => 'name',],
                ['name' => 'district_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Quận/Huyện', 'model' => \Modules\A4iLocation\Models\District::class, 'display_field' => 'name',],
                ['name' => 'ward_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Phường/Xã', 'model' => \Modules\A4iLocation\Models\Ward::class, 'display_field' => 'name',],
                ['name' => 'village', 'type' => 'text', 'class' => '', 'label' => 'Thôn/Xóm'],

            ]
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên nguyên liệu',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'facility_name' => [
            'label' => 'Tên cơ sở bán',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'amount' => [
            'label' => 'Số lượng',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Số lượng',
                1 => 'Gram',
                2 => 'Túi',
                3 => 'Cây',
                4 => 'Chiếc'
            ]
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);
        return view('a4iseason::childs.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('season_id', $request->season_id);
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('a4iseason::childs.add')->with($data);

            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
//                    'category_seeds' => 'required',
//                    'name_supplier' => 'required',
//                    'email' => 'required'
                ], [
//                    'category_seeds.required' => 'Bắt buộc phải chọn loại giống',
//                    'name_supplier.required' => 'Bắt buộc phải nhập tên nhà cung cấp',
//                    'email.required' => 'Bắt buộc phải nhập email',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    $data['season_id'] = $request->season_id;
                    $data['admin_id'] = \Auth::guard('admin')->id();

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {


                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    $url = '';
                    if ($request->has('season_id')) {
                        $url .= '?season_id=' . $request->season_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('a4iseason::childs.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
//                    'category_seeds' => 'required',
//                    'name_supplier' => 'required',
//                    'email' => 'required'
                ], [
//                    'category_seeds.required' => 'Bắt buộc phải chọn loại giống',
//                    'name_supplier.required' => 'Bắt buộc phải nhập tên nhà cung cấp',
//                    'email.required' => 'Bắt buộc phải nhập email',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    $url = '';
                    if ($request->has('season_id')) {
                        $url .= '?season_id=' . $request->season_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }
//            dd($item);
            $item->delete();
            $url = '';
            if ($request->has('season_id')) {
                $url .= '?season_id=' . $request->season_id;
            }

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code'] . $url);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
