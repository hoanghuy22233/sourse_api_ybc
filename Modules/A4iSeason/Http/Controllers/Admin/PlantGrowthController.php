<?php

namespace Modules\A4iSeason\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Validator;
use Illuminate\Http\Request;
use Auth;

class PlantGrowthController extends CURDBaseController
{

    protected $module = [
        'code' => 'plant_growth',
        'table_name' => 'plant_growths',
        'label' => 'Sinh trưởng cây trồng',
        'modal' => '\Modules\A4iSeason\Models\PlantGrowth',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh'],
            ['name' => 'implementation_date', 'type' => 'date_vi', 'class' => '', 'label' => 'Ngày thực hiện'],
            ['name' => 'seedling', 'type' => 'select', 'options' =>
                [
                    1 => 'Mọc từ hạt',
                    2 => 'Cây nuôi cấy mô',
                ], 'class' => '', 'label' => 'Quá trình sinh trưởng'],
//            ['name' => 'phenomena', 'type' => 'select', 'options' =>
//                [
//                    1 => 'Cây ở vườn ươm',
//                    2 => 'Ra hoa',
//                    3 => 'Ra hoa',
//                    4 => 'Đậu quả',
//                    5 => 'Quả già/chín',
//                ], 'class' => '', 'label' => 'Hiện tượng'],
            ['name' => 'action', 'type' => 'custom', 'td' => 'a4iseason::list.td.action', 'class' => '', 'label' => '#'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'seedling', 'type' => 'select', 'options' =>
                    [
                        1 => 'Mọc từ hạt',
                        2 => 'Cây nuôi cấy mô',
                    ], 'class' => '', 'label' => 'Quá trình sinh trưởng'],
//                ['name' => 'phenomena', 'type' => 'select', 'options' =>
//                    [
//                        1 => 'Cây ở vườn ươm',
//                        2 => 'Ra hoa',
//                        3 => 'Đậu quả',
//                        4 => 'Quả già/chín',
//                    ], 'class' => '', 'label' => 'Hiện tượng'],
//                        ['name' => 'time', 'type' => 'date', 'class' => '', 'label' => 'Thời gian'],
                ['name' => 'implementation_date', 'type' => 'date', 'class' => '', 'label' => 'Ngày thực hiện'],

            ],
            'image_tab' => [
                ['name' => 'image', 'type' => 'file_image', 'class' => '', 'label' => 'Ảnh'],

            ]
        ],
    ];

    protected $filter = [
        'seedling' => [
            'label' => 'Cây giống',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Cây giống',
                1 => 'Mọc từ hạt',
                2 => 'Cây nuôi cấy mô',
            ]
        ],
        'phenomena' => [
            'label' => 'Hiện tượng',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Hiện tượng',
                1 => 'Cây ở vườn ươm',
                2 => 'Ra hoa',
                3 => 'Đậu quả',
                4 => 'Quả già/chín',
            ]
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);
        return view('a4iseason::childs.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('season_id', $request->season_id);
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('a4iseason::childs.add')->with($data);

            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
//                    'category_seeds' => 'required',
//                    'name_supplier' => 'required',
//                    'email' => 'required'
                ], [
//                    'category_seeds.required' => 'Bắt buộc phải chọn loại giống',
//                    'name_supplier.required' => 'Bắt buộc phải nhập tên nhà cung cấp',
//                    'email.required' => 'Bắt buộc phải nhập email',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    $data['season_id'] = $request->season_id;
                    $data['admin_id'] = \Auth::guard('admin')->id();
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {


                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    $url = '';
                    if ($request->has('season_id')) {
                        $url .= '?season_id=' . $request->season_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('a4iseason::childs.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
//                    'category_seeds' => 'required',
//                    'name_supplier' => 'required',
//                    'email' => 'required'
                ], [
//                    'category_seeds.required' => 'Bắt buộc phải chọn loại giống',
//                    'name_supplier.required' => 'Bắt buộc phải nhập tên nhà cung cấp',
//                    'email.required' => 'Bắt buộc phải nhập email',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    $url = '';
                    if ($request->has('season_id')) {
                        $url .= '?season_id=' . $request->season_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
