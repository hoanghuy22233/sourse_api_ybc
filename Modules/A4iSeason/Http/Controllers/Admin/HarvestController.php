<?php

namespace Modules\A4iSeason\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Modules\A4iSeason\Models\Harvest;
use Modules\A4iSeason\Models\Season;
use Validator;
use Illuminate\Http\Request;
use Auth;

class HarvestController extends CURDBaseController
{

    protected $module = [
        'code' => 'harvest',
        'table_name' => 'harvests',
        'label' => 'Thu hoạch',
        'modal' => '\Modules\A4iSeason\Models\Harvest',
        'list' => [
            //                ['name' => 'land_id', 'type' => 'relation_edit', 'label' => 'Mảnh đất', 'object' => 'land', 'display_field' => 'name'],
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh'],
            ['name' => 'price', 'type' => 'custom', 'td' => 'a4iseason::list.td.price', 'label' => 'Giá'],
            ['name' => 'quantity', 'type' => 'text', 'label' => 'Sản lượng'],
//            ['name' => 'name_product', 'type' => 'text', 'label' => 'Tên nông sản'],
            ['name' => 'method', 'type' => 'select', 'options' =>
                [
                    1 => 'Chuyển sang trồng cây khác',
                    2 => 'Trồng tiếp nhưng theo chu kỳ mới',
                    3 => 'Cho đất nghỉ, để ải',
                ], 'class' => '', 'label' => 'Trường hợp'],

            ['name' => 'implementation_date', 'type' => 'date_vi', 'class' => '', 'label' => 'Thời gian thu hoạch'],
            ['name' => 'action', 'type' => 'custom', 'td' => 'a4iseason::list.td.action', 'class' => '', 'label' => '#'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'image', 'type' => 'custom', 'field' => 'a4iseason::harvest.qr_code', 'label' => 'QR code'],
                ['name' => 'quantity', 'type' => 'number', 'class' => 'required', 'label' => 'Sản lượng', 'object' => 'name', 'group_class' => 'col-xs-6 col-md-6'],
                ['name' => 'quantity_unit', 'type' => 'select', 'options' => [
                    'kg' => 'kg',
                    'tấn' => 'tấn',
                ], 'class' => 'required', 'label' => 'Đơn vị', 'object' => 'name', 'group_class' => 'col-xs-6 col-md-6'],
                ['name' => 'price', 'type' => 'text', 'class' => 'number_price', 'label' => 'Giá', 'group_class' => 'col-xs-6 col-md-6'],
                ['name' => 'price_unit', 'type' => 'select', 'options' => [
                    '/kg' => '/kg',
                    '/tấn' => '/tấn',
                ], 'class' => 'required', 'label' => 'Đơn vị', 'object' => 'name', 'group_class' => 'col-xs-6 col-md-6'],
                ['name' => 'method', 'type' => 'select', 'options' =>
                    [
                        1 => 'Chuyển sang trồng cây khác',
                        2 => 'Trồng tiếp nhưng theo chu kỳ mới',
                        3 => 'Cho đất nghỉ, để ải',
                    ], 'class' => '', 'label' => 'Sau khi thu hoạch'],
                ['name' => 'standards_achieved', 'type' => 'select', 'options' =>
                    [
                        '0' => 'không có tiêu chuẩn',
                        '1' => 'vietgap',
                        '2' => 'global',
                        '3' => 'gap',
                        '4' => 'organicc',
                    ], 'class' => '', 'label' => 'Tiêu chuẩn đạt được'],
//                        ['name' => 'land_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Mảnh đất', 'model' => \App\Models\Land::class, 'display_field' => 'name',],
//                ['name' => 'name_product', 'type' => 'text', 'class' => '', 'label' => 'Tên sản phẩm', 'object' => 'name'],
                ['name' => 'implementation_date', 'type' => 'date', 'class' => '', 'label' => 'Thời gian thu hoạch', 'object' => 'details'],
//                ['name' => 'implementation_date', 'type' => 'date_vi', 'class' => '', 'label' => 'Ngày thực hiện', 'object' => 'details'],

            ],
            'image_tab' => [
                ['name' => 'image', 'type' => 'file_image', 'class' => '', 'label' => 'Ảnh đại diện'],
                ['name' => 'image_standard', 'type' => 'file_image', 'class' => '', 'label' => 'Ảnh tiêu chuẩn'],
                ['name' => 'image_extra', 'type' => 'multiple_image_dropzone', 'class' => '', 'object' => 'tours', 'label' => 'Ảnh khác', 'count' => 6],
            ],
            'harvest_calculation' => [

            ],
             'analysis_tab' => [
                ['name' => 'iframe', 'type' => 'iframe', 'class' => 'col-xs-12 col-md-8 padding-left', 'src' => '/admin/analysis?harvest_id={id}', 'inner' => 'style="min-height: 785px;"'],
            ],
        ],
    ];

    protected $filter = [
        'method' => [
            'label' => 'Trường hợp',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trường hợp',
                1 => 'Chuyển sang trồng cây khác',
                2 => 'Trồng tiếp nhưng theo chu kỳ mới',
                3 => 'Cho đất nghỉ, để ải',
            ]
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);
        return view('a4iseason::childs.list')->with($data);

    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('season_id', $request->season_id);
        return $query;
    }

    public function add(Request $request)
    {

        try {
            if (!$_POST) {

                $data = $this->getDataAdd($request);
                return view('a4iseason::childs.add')->with($data);

            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
//                    'category_seeds' => 'required',
//                    'name_supplier' => 'required',
//                    'email' => 'required'
                ], [
//                    'category_seeds.required' => 'Bắt buộc phải chọn loại giống',
//                    'name_supplier.required' => 'Bắt buộc phải nhập tên nhà cung cấp',
//                    'email.required' => 'Bắt buộc phải nhập email',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    $data['season_id'] = $request->season_id;
                    $data['admin_id'] = \Auth::guard('admin')->id();


                    if ($request->has('image_extra')) {
                        $data['image_extra'] = implode('|', $request->image_extra);
                    }
                    unset($data['iframe']);

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        if (in_array($request->get('method'), ['1', '3'])) {   //  Dừng mùa vụ
                            $season = Season::find($request->season_id);
                            $season->status = 0;
                            $season->save();
                        }

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    $url = '';
                    if ($request->has('season_id')) {
                        $url .= '?season_id=' . $request->season_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('a4iseason::childs.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
//                    'category_seeds' => 'required',
//                    'name_supplier' => 'required',
//                    'email' => 'required'
                ], [
//                    'category_seeds.required' => 'Bắt buộc phải chọn loại giống',
//                    'name_supplier.required' => 'Bắt buộc phải nhập tên nhà cung cấp',
//                    'email.required' => 'Bắt buộc phải nhập email',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    if ($request->has('image_extra')) {
                        $data['image_extra'] = implode('|', $request->image_extra);
                    }
                    unset($data['iframe']);

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        $season = Season::find($item->season_id);
                        if (in_array($request->get('method'), ['1', '3'])) {   //  Dừng mùa vụ
                            $season->status = 0;
                            $season->save();
                        } else {
                            $season->status = 1;
                            $season->save();
                        }

                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    $url = '';
                    if ($request->has('season_id')) {
                        $url .= '?season_id=' . $request->season_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            $url = '';
            if ($request->has('season_id')) {
                $url .= '?season_id=' . $request->season_id;
            }

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code'] . $url);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function editLastItem(Request $request)
    {
        $last_item = Harvest::where('season_id', $request->season_id)->whereIn('method', ['1', '3'])->orderBy('id', 'desc')->first();
        if (!is_object($last_item)) {
            CommonHelper::one_time_message('error', 'Không tìm thấy bản ghi');
            return back();
        }
        return redirect('/admin/harvest/' . $last_item->id . '?season_id=' . $request->season_id);
    }
}
