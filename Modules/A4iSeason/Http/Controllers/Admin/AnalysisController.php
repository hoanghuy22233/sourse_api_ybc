<?php

namespace Modules\A4iSeason\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Modules\A4iLand\Models\Land;
use Validator;
use Illuminate\Http\Request;
use Auth;

class AnalysisController extends CURDBaseController
{

    protected $module = [
        'code' => 'analysis',
        'table_name' => 'analysiss',
        'label' => 'Phân tích',
        'modal' => '\Modules\A4iSeason\Models\Analysis',
        'list' => [
            ['name' => 'user_name', 'type' => 'text', 'class' => '', 'label' => 'Người phân tích'],
            ['name' => 'phone', 'type' => 'text', 'class' => '', 'label' => 'SĐT'],
            ['name' => 'email', 'type' => 'text', 'class' => '', 'label' => 'Email'],
            ['name' => 'time', 'type' => 'date_vi', 'class' => '', 'label' => 'Thời gian phân tích'],
            ['name' => 'device', 'type' => 'text', 'class' => '', 'label' => 'Thiết bị phân tích'],

            ['name' => 'action', 'type' => 'custom', 'td' => 'a4iseason::list.td.action_harvest', 'class' => '', 'label' => '#'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'user_name', 'type' => 'text', 'class' => 'required', 'label' => 'Người phân tích'],
                ['name' => 'phone', 'type' => 'text', 'class' => '', 'label' => 'SĐT'],
                ['name' => 'email', 'type' => 'text', 'class' => '', 'label' => 'Email'],
                ['name' => 'time', 'type' => 'date', 'class' => '', 'label' => 'Thời gian phân tích'],
                ['name' => 'device', 'type' => 'text', 'class' => '', 'label' => 'Thiết bị phân tích'],
            ],
            'info_tab' => [
                ['name' => 'analysis_results', 'type' => 'custom', 'fields' => 'a4iseason::form.fields.dynamic4', 'object' => 'tours', 'class' => 'required', 'label' => 'Kết quả phân tích'],
//                ['name' => 'product_name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên gọi'],
//                ['name' => 'result', 'type' => 'text', 'class' => 'required', 'label' => 'Kết quả'],
//                ['name' => 'standard', 'type' => 'text', 'class' => '', 'label' => 'Tiêu chuẩn'],
            ]
        ],
    ];

    protected $filter = [
        'user_name' => [
            'label' => 'Người phân tích',
            'type' => 'text',
            'query_type' => 'like',
        ],
        'time' => [
            'label' => 'Thời gian phân tích',
            'type' => 'date',
            'query_type' => 'like'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);
        return view('a4iseason::analysis.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('harvest_id', $request->harvest_id);
        return $query;
    }

    public function add(Request $request)
    {
        try {

            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('a4iseason::analysis.add')->with($data);

            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'user_name' => 'required',
                ], [
                    'user_name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    if ($request->has('parameters')) {
                        $data['parameters'] = implode('|', $request->parameters);
                    }

                    if ($request->has('product_name')) {
                        $data['product_name'] = implode('|', $request->product_name);
                    }

                    if ($request->has('result')) {
                        $data['result'] = implode('|', $request->result);
                    }
                    if ($request->has('standard')) {
                        $data['standard'] = implode('|', $request->standard);
                    }

                    if ($request->has('analysis_results_parameters')) {
                        $data['analysis_results'] = [];
                        foreach ($request->analysis_results_parameters as $k => $v) {
                            $analysis_result[] = [
                                'parameters' => $v,
                                'product_name' => $request->analysis_results_product_name[$k],
                                'result' => $request->analysis_results_result[$k],
                                'standard' => $request->analysis_results_standard[$k],
                            ];
                        }
                        $data['analysis_results'][] = $analysis_result;
                    }

                    $data['analysis_results'] = json_encode($data['analysis_results']);

                    //  Tùy chỉnh dữ liệu insert
                    $data['harvest_id'] = $request->harvest_id;
                    $data['admin_id'] = \Auth::guard('admin')->id();
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {


                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    $url = '';
                    if ($request->has('harvest_id')) {
                        $url .= '?harvest_id=' . $request->harvest_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);

                return view('a4iseason::analysis.edit')->with($data);
            } else if ($_POST) {

                $validator = Validator::make($request->all(), [
                    'user_name' => 'required',
                ], [
                    'user_name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    if ($request->has('parameters')) {
                        $data['parameters'] = implode('|', $request->parameters);
                    }

                    if ($request->has('product_name')) {
                        $data['product_name'] = implode('|', $request->product_name);
                    }

                    if ($request->has('result')) {
                        $data['result'] = implode('|', $request->result);
                    }
                    if ($request->has('standard')) {
                        $data['standard'] = implode('|', $request->standard);
                    }
//                    $data['parameters'] = json_decode($request->parameters);
//                    $data['product_name'] = json_decode($request->product_name);
//                    $data['result'] = json_decode($request->result);
//                    $data['standard'] = json_decode($request->standard);

                    if ($request->has('analysis_results_parameters')) {
                        $data['analysis_results'] = [];
                        foreach ($request->analysis_results_parameters as $k => $v) {
                            $analysis_result[] = [
                                'parameters' => $v,
                                'product_name' => $request->analysis_results_product_name[$k],
                                'result' => $request->analysis_results_result[$k],
                                'standard' => $request->analysis_results_standard[$k],
                            ];
                        }
                        $data['analysis_results'][] = $analysis_result;
                    }

                    $data['analysis_results'] = json_encode($data['analysis_results']);

                    //  Tùy chỉnh dữ liệu insert
                    $data['harvest_id'] = $request->harvest_id;
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    $url = '';
                    if ($request->has('harvest_id')) {
                        $url .= '?harvest_id=' . $request->harvest_id;
                    }


                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);
            $harvest_id = $item->harvest_id;
            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            $url='';
            if ($request->has('harvest_id')) {
                $url .= '?harvest_id=' . $request->harvest_id;
            }
            return redirect('admin/' . $this->module['code']. $url);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
