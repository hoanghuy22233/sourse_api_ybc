@if(isset($result))
    <?php
    $qr_code = base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')->size(100)->generate(json_encode([
        'harvest_id' => $result->id,
        'harvest_implementation_date' => $result->implementation_date,
        'tree_name' => @$result->season->tree_name,
        'land_address' => @$result->land->address,
        'default_show' => 'season info',
    ])))
    ?>
    <img src="data:image/png;base64, {!! $qr_code!!} ">
    <a href="data:application/octet-stream;base64,{!! $qr_code !!} " download="{{$result->id}}_{{str_slug(@$result->season->tree_name,'_')}}_{{str_slug(@$result->admin->name,'_')}}.png">Tải QR code</a>
    (Thông tin mùa vụ)
    <br>

    <?php
    $qr_code = base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')->size(100)->generate(json_encode([
        'harvest_id' => $result->id,
        'harvest_implementation_date' => $result->implementation_date,
        'tree_name' => @$result->season->tree_name,
        'land_address' => @$result->land->address,
        'default_show' => 'analysis info', 
    ])))
    ?>
    <img src="data:image/png;base64, {!! $qr_code!!} ">
    <a href="data:application/octet-stream;base64,{!! $qr_code !!} " download="{{$result->id}}_{{str_slug(@$result->season->tree_name,'_')}}_{{str_slug(@$result->admin->name,'_')}}.png">Tải QR code</a>
    (Thông tin phân tích)
    <br>
@endif
