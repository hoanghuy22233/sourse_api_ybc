<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Thống kê tiền
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body">
            <div class="kt-section kt-section--first">
                @isset($result)
                    <?php

                    $tong_thu = \Modules\A4iSeason\Http\Helpers\SeasonHelper::getPriceHarvest($result);
                    ?>
                    <strong class="kt-font-success kt-font-bold">Tổng thu:</strong> <strong class="kt-font-danger kt-font-bold">{{ number_format($tong_thu, 0, '.', '.') }}đ</strong>
                @endif
                <br>
                @if (isset($result))
                    <?php
                    $season_id = isset($result->season_id) ? $result->season_id : $_GET['season_id'];
                    $tong_chi = \Modules\A4iSeason\Http\Helpers\SeasonHelper::getCostSeason($season_id, $result);
                    ?>
                        <strong class="kt-font-success kt-font-bold">Tổng chi:</strong><strong class="kt-font-danger kt-font-bold"> {{ number_format($tong_chi, 0, '.', '.') }}đ</strong>
                @endif
            </div>
        </div>
    </div>
    <!--end::Form-->
</div>