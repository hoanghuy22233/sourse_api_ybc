<style>
    .form-group-dynamic .fieldwrapper > div:nth-child(1) {
        padding-left: 0;
    }

    .fieldwrapper {
        padding: 5px;
        border: 1px solid #ccc;
        margin-bottom: 5px;
    }
</style>
<fieldset id="buildyourform-{{ $field['name'] }}" class="{{ @$field['class'] }}">
    <?php
    $data1 = [];
    if (isset($result)) {
        $data1 = json_decode($result->analysis_results);
    }

    ?>
    @if(@is_array($data1))
        @foreach(@$data1 as $v)
            <div class="fieldwrapper" id="field">
                <div class="col-xs-10 col-md-10">
                    <div class="row">
                        <div class="col-xs-6 col-md-6">
                            <input type="text" class="form-control fieldname"
                                   name="{{ $field['name'] }}_parameters[]" value="{{ @$v->parameters }}"
                                   placeholder="Tham số" required>
                            <input type="text" class="form-control fieldname mt-2"
                                   name="{{ $field['name'] }}_product_name[]" value="{{ @$v->product_name }}"
                                   placeholder="Tên gọi" required>
                        </div>
                        <div class="col-xs-6 col-md-6">
                            <input type="text" class="form-control fieldname mt-2"
                                   name="{{ $field['name'] }}_result[]" value="{{ @$v->result }}"
                                   placeholder="Kết quả " required>
                            <input type="text" class="form-control fieldname mt-2"
                                   name="{{ $field['name'] }}_standard[]" value="{{ @$v->standard }}"
                                   placeholder="TCVN" required>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 col-md-2" style="float: right; bottom: 83px; left: 20px;">
                    <i type="xóa hàng" style="cursor: pointer;"
                       class="btn remove btn btn-danger btn-icon la la-remove"></i>
                </div>
            </div>
        @endforeach
    @else
        @if(isset($data))
            @foreach($data as $v)
                <div class="fieldwrapper" id="field">
                    <div class="col-xs-10 col-md-10">
                        <div class="row">
                            <div class="col-xs-6 col-md-6">
                                <input type="text" class="form-control fieldname"
                                       name="{{ $field['name'] }}_parameters[]" value="{{ @$v->parameters }}"
                                       placeholder="Tham số" required>
                                <input type="text" class="form-control fieldname mt-2"
                                       name="{{ $field['name'] }}_product_name[]" value="{{ @$v->product_name }}"
                                       placeholder="Tên gọi" required>
                            </div>
                            <div class="col-xs-6 col-md-6">
                                <input type="text" class="form-control fieldname mt-2"
                                       name="{{ $field['name'] }}_result[]" value="{{ @$v->result }}"
                                       placeholder="Kết quả " required>
                                <input type="text" class="form-control fieldname mt-2"
                                       name="{{ $field['name'] }}_standard[]" value="{{ @$v->standard }}"
                                       placeholder="TCVN" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2 col-md-2" style="float: right; bottom: 83px; left: 20px;">
                        <i type="xóa hàng" style="cursor: pointer;"
                           class="btn remove btn btn-danger btn-icon la la-remove"></i>
                    </div>
                </div>
            @endforeach
        @endif
    @endif
    {{--@endif--}}
</fieldset>
<a class="btn btn btn-primary btn-add-dynamic" style="color: white; margin-top: 20px; cursor: pointer;">
    <span>
        <i class="la la-plus"></i>
        <span>Thêm</span>
    </span>
</a>
<script>
    $(document).ready(function () {
        $(".btn-add-dynamic").click(function () {
            var lastField = $("#buildyourform-{{ $field['name'] }} div:last");
            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $('<div class="fieldwrapper" style="margin-bottom: 5px;" id="field' + intId + '"/>');
            fieldWrapper.data("idx", intId);
            var fields = $('<div class="col-xs-10 col-md-10">\n' +
                '                            <div class="row">\n' +
                '                                <div class="col-xs-6 col-md-6">\n' +
                '                                    <input type="text" class="form-control fieldname"\n' +
                '                                                                           name="{{ $field['name'] }}_parameters[]" value=""\n' +
                '                                                                           placeholder="Tham số" required>\n' +
                '                                    <input type="text" class="form-control fieldname mt-2"\n' +
                '                                           name="{{ $field['name'] }}_product_name[]" value=""\n' +
                '                                           placeholder="Tên gọi" required>\n' +
                '                                </div>\n' +
                '                                <div class="col-xs-6 col-md-6">\n' +
                '                                    <input type="text" class="form-control fieldname"\n' +
                '                                           name="{{ $field['name'] }}_result[]" value=""\n' +
                '                                           placeholder="Kết quả" required>\n' +
                '                                    <input type="text" class="form-control fieldname mt-2"\n' +
                '                                           name="{{ $field['name'] }}_standard[]" value=""\n' +
                '                                           placeholder="TCVN" required>\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>');
            var removeButton = $('<div class="col-xs-2 col-md-2" style="float: right; bottom: 83px; left: 20px"><i type="xóa hàng" style="cursor: pointer;" class="btn remove btn btn-danger btn-icon la la-remove" ></i>');

            fieldWrapper.append(fields);
            fieldWrapper.append(removeButton);
            $("#buildyourform-{{ $field['name'] }}").append(fieldWrapper);
        });
        $('body').on('click', '.remove', function () {
            $(this).parents('.fieldwrapper').remove();
        });
    });
</script>