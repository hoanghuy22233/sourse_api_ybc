<?php

namespace Modules\A4iSeason\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\A4iLand\Models\Land;
use Modules\EworkingAdmin\Models\Admin;

class Analysis extends Model
{

    protected $table = 'analysiss';

    protected $fillable = [
        'user_name', 'email', 'phone', 'device', 'parameters', 'product_name', 'result', 'standard', 'time', 'analysis_results', 'name_tree', 'admin_id'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function land()
    {
        return $this->belongsTo(Land::class, 'land_id');
    }
    public function harvest()
    {
        return $this->belongsTo(Harvest::class, 'harvest_id');
    }

}
