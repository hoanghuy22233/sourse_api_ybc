<?php

namespace Modules\A4iSeason\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Admin;

class Drainage extends Model
{

    protected $table = 'drainages';

    protected $fillable = [
        'methods', 'pump_capacity', 'irrigation_time', 'time', 'admin_id', 'season_id', 'image', 'amount_water', 'humidity', 'implementation_date',
        'image_extra','cost'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function land()
    {
        return $this->belongsTo(Land::class, 'land_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }

}
