<?php

namespace Modules\A4iSeason\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\A4iOrganization\Models\Organization;
use Modules\EworkingAdmin\Models\Admin;

class Fertilizer extends Model
{

    protected $table = 'fertilizers';
    public $timestamps = false;
    protected $fillable = [
        'name', 'image', 'fertilizers', 'fertilizer_formula', 'quantity', 'time', 'admin_id', 'season_id', 'fertilizer_warehouse_id', 'cost', 'implementation_date'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function land()
    {
        return $this->belongsTo(Organization::class, 'land_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }

    public function fertilizer_warehouses()
    {
        return $this->belongsTo(FertilizerWarehouse::class, 'fertilizer_warehouse_id');
    }
}
