<?php

/**
 * Admin Model
 *
 * Admin Model manages Admin operation.
 *
 * @category   Admin
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\A4iSeason\Models;

use App\Http\Helpers\CommonHelper;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Modules\A4iLocation\Models\Province;
use \Modules\EworkingAdmin\Models\Admin;
use DB;
use Modules\A4iLand\Models\Land;
use Modules\A4iSeason\Models\Season;

class Harvest extends Model
{
    protected $table = 'harvests';

    protected $fillable = [
        'time', 'method', 'land_id', 'admin_id', 'season_id', 'image', 'price', 'name_product', 'quantity', 'implementation_date', 'productivity', 'unit', 'season_id', 'lands_acreage', 'price_unit', 'quantity_unit', 'standards_achieved', 'image_standard'
    ];

    public function land()
    {
        return $this->belongsTo(Land::class, 'land_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }

    public function season_view()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
    public function analysis()
    {
        return $this->hasMany(Analysis::class, 'harvest_id');
    }


}
