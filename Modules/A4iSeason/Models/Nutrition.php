<?php

namespace Modules\A4iSeason\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\A4iOrganization\Models\Organization;
use Modules\EworkingAdmin\Models\Admin;

class Nutrition extends Model
{

    protected $table = 'nutritions';
    protected $fillable = [
        'phenomena', 'image', 'results_analysis', 'admin_id', 'time', 'created_at', 'season_id', 'cost', 'implementation_date'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function land()
    {
        return $this->belongsTo(Organization::class, 'land_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }

}
