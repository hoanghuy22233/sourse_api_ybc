<?php

namespace Modules\A4iSeason\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\A4iOrganization\Models\Organization;
use Modules\EworkingAdmin\Models\Admin;

class FertilizerWarehouse extends Model
{

    protected $table = 'fertilizer_warehouses';
    public $timestamps = false;
    protected $fillable = [
        'name', 'image', 'fertilizer_type', 'quantity', 'time', 'season_id', 'admin_id'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function land()
    {
        return $this->belongsTo(Organization::class, 'land_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }

}
