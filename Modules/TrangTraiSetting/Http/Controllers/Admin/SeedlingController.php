<?php

namespace Modules\TrangTraiSetting\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Modules\TrangTraiSetting\Models\TreeGroup;
use Validator;
use Illuminate\Http\Request;
use Auth;

class SeedlingController extends CURDBaseController
{

    protected $module = [
        'code' => 'seed',
        'table_name' => 'seeds',
        'label' => 'Giống',
        'modal' => '\Modules\TrangTraiSetting\Models\Seed',
        'list' => [
//            ['name' => 'image', 'type' => 'image', 'class' => '', 'label' => 'Hình ảnh'],
            ['name' => 'name_supplier', 'type' => 'text', 'class' => '', 'label' => 'Tên cây'],
            ['name' => 'information', 'type' => 'text', 'class' => '', 'label' => 'Những thông tin về thời vụ'],
            ['name' => 'tree_group_id', 'type' => 'relation', 'label' => 'Nhóm cây', 'object' => 'tree_group', 'display_field' => 'name'],
            ['name' => 'action', 'type' => 'custom', 'td' => 'trangtraisetting::list.td.action', 'class' => '', 'label' => '#'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name_supplier', 'type' => 'text', 'class' => '', 'label' => 'Tên cây'],
                ['name' => 'information', 'type' => 'text', 'class' => 'text', 'label' => 'Những thông tin về thời vụ',],
                ['name' => 'tree_group_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Nhóm cây', 'model' => TreeGroup::class, 'display_field' => 'name',],
                ['name' => 'note', 'type' => 'textarea', 'class' => '', 'label' => 'Lưu ý'],
            ],

        ],
    ];

    protected $filter = [
        'name_supplier' => [
            'label' => 'Tên đơn vị',
            'type' => 'text',
            'query_type' => 'like'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('trangtraisetting::seedling.list')->with($data);

    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('season_id', $request->season_id);
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);


                return view('trangtraisetting::seedling.add')->with($data);

            } else if ($_POST)
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multiple_image')) {
                        $data['multiple_image'] = implode('|', $request->multiple_image);
                    }
//                    $data['season_id'] = $request->season_id;
//                    $data['admin_id'] = \Auth::guard('admin')->id();
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    $url = '';
                    if ($request->has('season_id')) {
                        $url .= '?season_id=' . $request->season_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);


        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();

        }
    }


    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('trangtraisetting::seedling.edit')->with($data);
            } else if ($_POST) {


                $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                //  Tùy chỉnh dữ liệu insert
                if ($request->has('multiple_image')) {
                    $data['multiple_image'] = implode('|', $request->multiple_image);
                }
                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                $url = '';
                if ($request->has('season_id')) {
                    $url .= '?season_id=' . $request->season_id;
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id . $url);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add' . $url);
                }

                return redirect('admin/' . $this->module['code'] . $url);
            }

        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);
            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            $url = '';
            if ($request->has('season_id')) {
                $url .= '?season_id=' . $request->season_id;
            }
            return redirect('admin/' . $this->module['code'] . $url);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
