<?php

namespace Modules\TrangTraiSetting\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Modules\A4iLand\Models\Land;
use Modules\TrangTraiSetting\Models\Criteria;
use Validator;
use Illuminate\Http\Request;
use Auth;

class DiarySeasonController extends CURDBaseController
{

    protected $module = [
        'code' => 'diary_season',
        'table_name' => 'diary_seasons',
        'label' => 'Nhật ký canh tác',
        'modal' => '\Modules\TrangTraiSetting\Models\DiarySeason',
        'list' => [
//            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh hiện tượng'],
            ['name' => 'criteria_id', 'type' => 'relation', 'label' => 'Tiêu chí đánh giá', 'object' => 'criteria', 'display_field' => 'name'],
            ['name' => 'amount', 'type' => 'text', 'class' => '', 'label' => 'Số lượng'],
            ['name' => 'created_at', 'type' => 'date_vi', 'class' => '', 'label' => 'Thời gian ghi nhận'],
            ['name' => 'action', 'type' => 'custom', 'td' => 'trangtraisetting::list.td.action', 'class' => '', 'label' => '#'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'criteria_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Tiêu chí đánh giá', 'model' => Criteria::class, 'display_field' => 'name',],
                ['name' => 'created_at', 'type' => 'date', 'class' => '', 'label' => 'Thời gian ghi nhận'],
                ['name' => 'amount', 'type' => 'text', 'class' => 'text', 'label' => 'Số lượng'],
            ],
            'image_tab' => [
//                ['name' => 'image', 'type' => 'file_image', 'class' => '', 'label' => 'Ảnh hiện tượng'],
//                ['name' => 'disease_image', 'type' => 'file_image', 'class' => '', 'label' => 'Ảnh tác nhân gây bệnh'],

            ]
        ],
    ];

    protected $filter = [
        'phenomena' => [
            'label' => 'Hiện tượng',
            'type' => 'text',
            'query_type' => 'like',
        ],
        'amount' => [
            'label' => 'Thời gian',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'created_at' => [
            'label' => 'Thời gian ghi nhận',
            'type' => 'date',
            'query_type' => 'like'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);
        return view('trangtraisetting::childs.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        $query = $query->where('season_id', $request->season_id);
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('trangtraisetting::childs.add')->with($data);

            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'amount' => 'required',
                ], [
                    'amount.required' => 'Bắt buộc phải nhập',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    $data['season_id'] = $request->season_id;
//                    $data['admin_id'] = \Auth::guard('admin')->id();
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {


                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    $url = '';
                    if ($request->has('season_id')) {
                        $url .= '?season_id=' . $request->season_id;
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('trangtraisetting::childs.edit')->with($data);
            } else if ($_POST) {

                $validator = Validator::make($request->all(), [
                    'amount' => 'required',
                ], [
                    'amount.required' => 'Bắt buộc phải nhập',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    $url = '';
                    if ($request->has('season_id')) {
                        $url .= '?season_id=' . $request->season_id;
                    }


                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id . $url);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add' . $url);
                    }

                    return redirect('admin/' . $this->module['code'] . $url);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);
            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            $url='';
            if ($request->has('season_id')) {
                $url .= '?season_id=' . $request->season_id;
            }
            return redirect('admin/' . $this->module['code']. $url);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
