<?php

namespace Modules\TrangTraiSetting\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\A4iLand\Models\Land;
use Modules\A4iLocation\Models\Province;
use Modules\A4iOrganization\Models\Organization;
use Modules\A4iSeason\Http\Controllers\Admin\CURDBaseController;
use App\Models\Admin;
use App\Models\RoleAdmin;
use Modules\A4iSeason\Models\Disease;
use Modules\A4iSeason\Models\Drainage;
use Modules\A4iSeason\Models\Fertilizer;
use Modules\A4iSeason\Models\Harvest;
use Modules\A4iSeason\Models\InvestmentMaterial;
use Modules\A4iSeason\Models\Nutrition;
use Modules\A4iSeason\Models\PlantGrowth;
use Modules\A4iSeason\Models\PlantMedicine;
use Modules\A4iSeason\Models\Season;
use Modules\A4iSeason\Models\Seed;
use Modules\A4iSeason\Models\TechnicalCare;
use Modules\A4iSeason\Models\WeatherInformation;
use Modules\A4iSeason\Models\Weeding;
use Modules\TrangTraiSetting\Models\Unit;
use Validator;
use Illuminate\Http\Request;
use Auth;

class SeasonController extends CURDBaseController
{

    protected $orderByRaw = 'status desc, updated_at desc';

    protected $module = [
        'code' => 'season',
        'table_name' => 'seasons',
        'label' => 'Mùa vụ',
        'modal' => '\Modules\TrangTraiSetting\Models\Season',
        'list' => [
//            ['name' => 'image', 'type' => 'image', 'class' => '', 'label' => 'Hình ảnh', 'style' => 'kt-media--xl'],
//            ['name' => 'status', 'type' => 'custom', 'td' => 'trangtraisetting::list.td.status', 'class' => '', 'label' => 'Trạng thái'],
            ['name' => 'seed_id', 'type' => 'relation', 'object' => 'seedling', 'display_field' => 'name_supplier', 'label' => 'Giống cây',],
            ['name' => 'quantum', 'type' => 'text', 'class' => '', 'label' => 'Số lượng trồng'],
            ['name' => 'expected_output', 'type' => 'text', 'class' => '', 'label' => 'Sản lượng dự kiến'],
            ['name' => 'unit_id', 'type' => 'relation', 'object' => 'unit', 'display_field' => 'name', 'label' => 'Đơn vị',],
            ['name' => 'admin_id', 'type' => 'admin_id', 'label' => 'Người canh tác',],
            ['name' => 'action', 'type' => 'custom', 'td' => 'trangtraisetting::list.td.action', 'class' => '', 'label' => '#'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'seed_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Giống cây', 'model' => \Modules\TrangTraiSetting\Models\Seed::class, 'display_field' => 'name_supplier',],
                ['name' => 'land_id', 'type' => 'custom', 'field' => 'trangtraisetting::fields.select_land', 'class' => 'required', 'label' => 'Khu đất trồng', 'model' => \Modules\TrangTraiSetting\Models\Land::class, 'display_field' => 'name'],
                ['name' => 'unit_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Đơn vị tính', 'model' => Unit::class, 'display_field' => 'name',],
                ['name' => 'expected_output', 'type' => 'text', 'class' => '', 'label' => 'Sản lượng dự kiến', 'des' => 'Ví dụ: 1.000 tấn'],
                ['name' => 'implementation_date', 'type' => 'date', 'class' => '', 'label' => 'Ngày bắt đầu', 'object' => 'details'],
                ['name' => 'quantum', 'type' => 'text', 'class' => '', 'label' => 'Số lượng trồng'],
                ['name' => 'note', 'type' => 'textarea', 'class' => '', 'label' => 'Ghi chú'],
                ['name' => 'status', 'type' => 'select', 'options' =>
                    [
                        1 => 'Đang diễn ra',
                        0 => 'Kết thúc',
                    ], 'class' => '', 'label' => 'Trạng thái', 'value' => 1],
            ],
            'image_tab' => [
//                ['name' => 'image', 'type' => 'file_image', 'class' => '', 'object' => 'tours', 'label' => 'Ảnh  đại diện'],
//                ['name' => 'image_extra', 'type' => 'multiple_image_dropzone', 'class' => '', 'object' => 'tours', 'label' => 'Ảnh khác', 'count' => 6],
            ]
        ],
    ];

    protected $filter = [
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                1 => 'Đang diễn ra',
                0 => 'Kết thúc',
            ]
        ],
        'quantity_expected' => [
            'label' => 'Sản lượng dự kiến',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'admin_id' => [
            'label' => 'Người tạo',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'display_field2' => 'email',
            'model' => \App\Models\Admin::class,
            'object' => 'admin',
            'query_type' => '='
        ],
        'from_date' => [
            'label' => 'Thu hoạch từ ngày',
            'type' => 'date',
            'query_type' => 'custom'
        ],
        'to_date' => [
            'label' => 'Thu hoạch đến ngày',
            'type' => 'date',
            'query_type' => 'custom'
        ],
        'province_id' => [
            'label' => 'Tỉnh Thành',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'model' => \Modules\A4iLocation\Models\Province::class,
            'object' => 'province',
            'query_type' => 'custom'
        ],
        'district_id' => [
            'label' => 'Quận Huyện',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'model' => \Modules\A4iLocation\Models\District::class,
            'object' => 'district',
            'query_type' => 'custom'
        ],
        'ward_id' => [
            'label' => 'Phường Xã',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'model' => \Modules\A4iLocation\Models\Ward::class,
            'object' => 'ward',
            'query_type' => 'custom'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('trangtraisetting::season.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        if (!$request->has('view')) {
            $query = $query->where('admin_id', \Auth::guard('admin')->id());
        }

        if (!is_null($request->get('province_id'))) {
            $land_ids = Land::where('province_id', $request->province_id)->pluck('id')->toArray();
            $query = $query->whereIn('land_id', $land_ids);
        }
        if (!is_null($request->get('district_id'))) {
            $land_ids = Land::where('district_id', $request->district_id)->pluck('id')->toArray();
            $query = $query->whereIn('land_id', $land_ids);
        }
        if (!is_null($request->get('ward_id'))) {
            $land_ids = Land::where('ward_id', $request->ward_id)->pluck('id')->toArray();
            $query = $query->whereIn('land_id', $land_ids);
        }
        if (!is_null($request->get('from_date'))) {
            $query = $query->where('expected_date', '>=', date('Y-m-d 00:00:00', strtotime($request->get('from_date'))));
        }
        if (!is_null($request->get('to_date'))) {
            $query = $query->where('expected_date', '<=', date('Y-m-d 23:59:59', strtotime($request->get('to_date'))));
        }

        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('trangtraisetting::season.add')->with($data);
            } else if ($_POST) {

                $validator = Validator::make($request->all(), [
                    'quantum' => 'required',
                    'land_id' => 'required'
                ], [
                    'quantum.required' => 'Bắt buộc phải nhập số lương',
                    'land_id.required' => 'Bắt buộc phải chọn khu đất trồng',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    $data['admin_id'] = \Auth::guard('admin')->id();

                    if ($request->has('image_extra')) {
                        $data['image_extra'] = implode('|', $request->image_extra);
                    }


                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {


                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('trangtraisetting::season.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'quantum' => 'required',
                    'land_id' => 'required'
                ], [
                    'quantum.required' => 'Bắt buộc phải nhập số lượng',
                    'land_id.required' => 'Bắt buộc phải chọn khu đất trồng',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert

                    if ($request->has('image_extra')) {
                        $data['image_extra'] = implode('|', $request->image_extra);
                    }


                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
            if ($item->admin_id != \Auth::guard('admin')->id() && !CommonHelper::has_permission(\Auth::guard('admin')->id(), 'super_admin')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


    public function timeLine(Request $request, $id)
    {
        $data['page_title'] = 'Lịch sử vụ mùa';
        $data['page_type'] = 'list';
        $data['module'] = $this->module;

        $data['items'] = [];

        //  Thêm tưới tiêu vào timeline
        $items = Drainage::where('season_id', $id)->orderBy('implementation_date', 'desc')->get();
        $methods = [
            1 => 'Tưới truyền thống',
            2 => 'Tưới tràn',
            3 => 'Tưới rãnh',
            4 => 'Tưới phun mưa',
            5 => 'Tưới phun sương',
            6 => 'Tưới nhỏ giọt',
            7 => 'Tưới mù',
        ];
        foreach ($items as $v) {
            $data['items'][strtotime($v->created_at)][] = [
                'title' => '<a href="/admin/drainage/' . $v->id . '?season_id=' . $id . '" class="kt-link kt-link--brand kt-font-bolder" target="_blank">Tưới tiêu</a>',
                'content' => '<strong>Phương pháp : </strong>' . @$methods[$v->methods] . '<br>' .
                    '<strong>Công suất : </strong>' . $v->pump_capacity . '<br>' .
                    '<strong>Thời lượng : </strong>' . $v->irrigation_time . '<br>' .
                    '<strong>Thời gian : </strong>' . $v->time . '<br>' .
                    '<strong>Lượng nước : </strong>' . $v->amount_water . '<br>',
                'images' => [
                    [
                        'src' => asset('public/filemanager/userfiles/' . $v->image),
                        'href' => '#'
                    ]
                ]
            ];
        }


        //  Thêm giống vào timeline
        $items = Seed::where('season_id', $id)->orderBy('created_at', 'desc')->get();
        $category_seeds = [
            1 => 'Hạt giống',
            2 => 'Cây nuôi cấy mô',
            3 => 'Cây triết',
            4 => 'Cây ghép',
        ];
        foreach ($items as $v) {
            $data['items'][strtotime($v->created_at)][] = [
                'title' => '<a href="/admin/seedling/' . $v->id . '?season_id=' . $id . '" class="kt-link kt-link--brand kt-font-bolder" target="_blank">Giống</a>',
                'content' => '<strong>Loại giống : </strong>' . $category_seeds[$v->category_seeds] . '<br>' .
                    '<strong>Tên : </strong>' . $v->name_supplier . '<br>' .
                    '<strong>Địa chỉ : </strong>' . $v->address . '<br>' .
                    '<strong>Email : </strong>' . $v->email . '<br>' .
                    '<strong>Trang web : </strong>' . $v->website . '<br>',
                'images' => [
                    [
                        'src' => asset('public/filemanager/userfiles/' . $v->image),
                        'href' => '#'
                    ]
                ]
            ];
        }


        //  Thêm sinh trưởng cây trồng vào timeline
        $items = PlantGrowth::where('season_id', $id)->orderBy('implementation_date', 'desc')->get();
        $seedling = [
            1 => 'Mọc từ hạt',
            2 => 'Cây nuôi cấy mô',
        ];

        foreach ($items as $v) {
            $data['items'][strtotime($v->implementation_date)][] = [
                'title' => '<a href="/admin/plant_growth/' . $v->id . '?season_id=' . $id . '" class="kt-link kt-link--brand kt-font-bolder" target="_blank">Sinh trưởng cây trồng</a>',
                'content' => '<strong>Quá trình sinh trưởng : </strong>' . @$seedling[$v->seedling] . '<br>',
                'images' => [
                    [
                        'src' => asset('public/filemanager/userfiles/' . $v->image),
                        'href' => '#'
                    ]
                ]
            ];
        }


        //  Thêm kỹ thuật chăm sóc vào timeline
        $items = TechnicalCare::where('season_id', $id)->orderBy('implementation_date', 'desc')->get();
        $technology = [
            1 => 'Tỉa cành',
            2 => 'Vun gốc',
            3 => 'Bao trái',
            4 => 'Làm cỏ',
            5 => 'Thụ phấn ong',
        ];
        $method = [
            1 => 'Làm thủ công',
            2 => 'Làm bằng máy',
        ];
        foreach ($items as $v) {
            $data['items'][strtotime($v->implementation_date)][] = [
                'title' => '<a href="/admin/technical_care/' . $v->id . '?season_id=' . $id . '" class="kt-link kt-link--brand kt-font-bolder" target="_blank">Kỹ thuật chăm sóc</a>',
                'content' => '<strong>Kỹ thuật : </strong>' . @$technology[$v->technology] . '<br>' .
                    '<strong>Phương pháp làm cỏ : </strong>' . @$method[$v->method] . '<br>' .
                    '<strong>Chi phí : </strong>' . number_format($v->cost) . '<br>',
                'images' => [
                    [
                        'src' => asset('public/filemanager/userfiles/' . $v->image),
                        'href' => '#'
                    ]
                ]
            ];
        }


        //  Thêm thông tin thời tiết vào timeline
        $items = WeatherInformation::where('season_id', $id)->orderBy('implementation_date', 'desc')->get();
        $warning_from = [
            1 => 'Cảm biến',
            2 => 'Hệ thống IoT',
            3 => 'Khác',
        ];
        foreach ($items as $v) {
            $data['items'][strtotime($v->implementation_date)][] = [
                'title' => '<a href="/admin/technical_care/' . $v->id . '?season_id=' . $id . '" class="kt-link kt-link--brand kt-font-bolder" target="_blank">Thông tin thời tiết</a>',
                'content' => '<strong>Cảnh báo từ : </strong>' . @$warning_from[$v->warning_from] . '<br>' .
                    '<strong>Độ ẩm : </strong>' . $v->humidity . '<br>' .
                    '<strong>Tốc độ gió : </strong>' . $v->wind_speed . '<br>' .
                    '<strong>Lượng mưa : </strong>' . $v->amount_rain . '<br>' .
                    '<strong>Hướng gió : </strong>' . $v->wind_direction . '<br>' .
                    '<strong>CO2 : </strong>' . $v->CO2 . '<br>' .
                    '<strong>Nhiệt độ : </strong>' . $v->temperature . '<br>',
                'images' => [
                    [
                        'src' => asset('public/filemanager/userfiles/' . $v->image),
                        'href' => '#'
                    ]
                ]
            ];
        }


        //  Thêm phân bón vào timeline
        $items = Fertilizer::where('season_id', $id)->orderBy('implementation_date', 'desc')->get();
        foreach ($items as $v) {
            $data['items'][strtotime($v->implementation_date)][] = [
                'title' => '<a href="/admin/fertilizer/' . $v->id . '?season_id=' . $id . '" class="kt-link kt-link--brand kt-font-bolder" target="_blank">Phân bón</a>',
                'content' =>
                    '<strong>Tên : </strong>' . @$v->fertilizer_warehouses->name . '<br>' .
                    '<strong>Số lượng sử dụng : </strong>' . $v->quantity . '<br>' .
                    '<strong>Chí phí : </strong>' . number_format($v->cost) . '<br>',
                'images' => [
                    [
                        'src' => asset('public/filemanager/userfiles/' . $v->image),
                        'href' => '#'
                    ]
                ]
            ];
        }


        //  Thêm dinh dưỡng vào timeline
        $items = Nutrition::where('season_id', $id)->orderBy('implementation_date', 'desc')->get();
        foreach ($items as $v) {
            $data['items'][strtotime($v->implementation_date)][] = [
                'title' => '<a href="/admin/nutrition/' . $v->id . '?season_id=' . $id . '" class="kt-link kt-link--brand kt-font-bolder" target="_blank">Dinh dưỡng</a>',
                'content' =>
                    '<strong>Hiện tượng : </strong>' . $v->phenomena . '<br>' .
                    '<strong>Thời gian cập nhật thông tin : </strong>' . $v->time . '<br>' .
                    '<strong>Kết quả sau phân tích : </strong>' . $v->results_analysis . '<br>',
                'images' => [
                    [
                        'src' => asset('public/filemanager/userfiles/' . $v->image),
                        'href' => '#'
                    ]
                ]
            ];
        }


        //  Thêm dịch bệnh vào timeline
        $items = Disease::where('season_id', $id)->orderBy('implementation_date', 'desc')->get();
        foreach ($items as $v) {
            $data['items'][strtotime($v->implementation_date)][] = [
                'title' => '<a href="/admin/disease/' . $v->id . '?season_id=' . $id . '" class="kt-link kt-link--brand kt-font-bolder" target="_blank">Dịch bệnh</a>',
                'content' =>
                    '<strong>Hiện tượng : </strong>' . $v->phenomena . '<br>' .
                    '<strong>Chi phí : </strong>' . $v->cost . '<br>' .
                    '<strong>Tác nhân gây bệnh : </strong>' . $v->pathogen . '<br>',
                'images' => [
                    [
                        'src' => asset('public/filemanager/userfiles/' . $v->image),
                        'href' => '#'
                    ]
                ],
            ];
        }


        //  Thêm thuốc bảo vệ thực vật vào timeline
        $items = PlantMedicine::where('season_id', $id)->orderBy('created_at', 'desc')->get();
        foreach ($items as $v) {
            $data['items'][strtotime($v->implementation_date)][] = [
                'title' => '<a href="/admin/plant_medicine/' . $v->id . '?season_id=' . $id . '" class="kt-link kt-link--brand kt-font-bolder" target="_blank">Thuốc bảo vệ thực vật</a>',
                'content' =>
                    '<strong>Tên : </strong>' . @$v->medicine_warehouse->name . '<br>' .
                    '<strong>Số lượng : </strong>' . $v->quantity . '<br>' .
                    '<strong>Tác dụng : </strong>' . $v->effect . '<br>',
                'images' => [
                    [
                        'src' => asset('public/filemanager/userfiles/' . $v->image),
                        'href' => '#'
                    ]
                ],
            ];
        }


        //  Thêm nguyên liệu đầu vào vào timeline
        $items = InvestmentMaterial::where('season_id', $id)->orderBy('implementation_date', 'desc')->get();
        foreach ($items as $v) {
            $data['items'][strtotime($v->implementation_date)][] = [
                'title' => '<a href="/admin/plant_medicine/' . $v->id . '?season_id=' . $id . '" class="kt-link kt-link--brand kt-font-bolder" target="_blank">Nguyên liệu đầu vào</a>',
                'content' =>
                    '<strong>Tên cơ sở bán : </strong>' . $v->facility_name . '<br>' .
                    '<strong>Số lượng : </strong>' . $v->amount . '<br>',
                'images' => [
                    [
                        'src' => asset('public/filemanager/userfiles/' . $v->image),
                        'href' => '#'
                    ]
                ],
            ];
        }


        //  Thêm thu hoạch timeline
        $items = Harvest::where('season_id', $id)->orderBy('implementation_date', 'desc')->get();
        $method = [
            1 => 'Chuyển sang trồng cây khác',
            2 => 'Trồng tiếp nhưng theo chu kỳ mới',
            3 => 'Cho đất nghỉ, để ải',
        ];
        foreach ($items as $v) {
            $data['items'][strtotime($v->implementation_date)][] = [
                'title' => '<a href="/admin/drainage/' . $v->id . '?season_id=' . $id . '" class="kt-link kt-link--brand kt-font-bolder" target="_blank">Thu hoạch</a>',
                'content' => '<strong>Trường hợp : </strong>' . $method[$v->method] . '<br>' .
                    '<strong>Đơn vị : </strong>' . $v->unit . '<br>' .
                    '<strong>Số lượng : </strong>' . $v->quantity . '<br>' .
                    '<strong>Giá : </strong>' . number_format($v->price) . '<br>',
                'images' => [
                    [
                        'src' => asset('public/filemanager/userfiles/' . $v->image),
                        'href' => '#'
                    ]
                ]
            ];
        }

        ksort($data['items']);

        return view('trangtraisetting::season.timeline')->with($data);
    }
}
