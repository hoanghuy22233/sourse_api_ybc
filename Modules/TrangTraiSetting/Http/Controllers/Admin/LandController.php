<?php

namespace Modules\TrangTraiSetting\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use DB;
use Illuminate\Http\Request;
use Modules\TrangTraiSetting\Models\Seed;
use Validator;


class LandController extends CURDBaseController
{

    protected $module = [
        'code' => 'land',
        'table_name' => 'lands',
        'label' => 'Mảnh đất',
        'modal' => '\Modules\TrangTraiSetting\Models\Land',
        'list' => [
//            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'style' => 'kt-media--xl'],
            ['name' => 'name', 'type' => 'custom', 'td' => 'trangtraisetting::list.td.text_edit', 'label' => 'Tên'],
            ['name' => 'admin_id', 'type' => 'admin_id', 'label' => 'Người canh tác',],
//            ['name' => 'season_id', 'type' => 'relation', 'object' => 'season', 'display_field' => 'tree_name', 'label' => 'Vụ mùa',],
            ['name' => 'address', 'type' => 'custom', 'td' => 'trangtraisetting::list.td.address', 'label' => 'Địa chỉ',],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'require', 'label' => 'Tên mảnh đất', 'group_class' => 'col-md-6'],
                ['name' => 'acreage', 'type' => 'number', 'class' => '', 'label' => 'Diện tích (m2)', 'group_class' => 'col-md-6'],
                ['name' => 'address', 'type' => 'text', 'class' => '', 'label' => 'Địa điểm'],
                ['name' => 'province_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Tỉnh/Thành', 'model' => \Modules\A4iLocation\Models\Province::class, 'display_field' => 'name',],
                ['name' => 'district_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Quận/Huyện', 'model' => \Modules\A4iLocation\Models\District::class, 'display_field' => 'name',],
                ['name' => 'ward_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Phường/Xã', 'model' => \Modules\A4iLocation\Models\Ward::class, 'display_field' => 'name',],
//                ['name' => 'admin_id', 'type' => 'custom', 'td' => 'a4iland::td.admin_id', 'class' => '', 'label' => 'Người sở hữu', 'model' => \App\Models\Admin::class, 'display_field' => 'name',],
            ],
            'info_tab' => [
//                ['name' => 'image', 'type' => 'file_image', 'class' => '', 'object' => 'tours', 'label' => 'Ảnh  đại diện'],
//                ['name' => 'image_extra', 'type' => 'multiple_image_dropzone', 'class' => '', 'object' => 'tours', 'label' => 'Ảnh khác', 'count' => 6],
            ],
            'land_parameters' => [
//                ['name' => 'land_parameters_image', 'type' => 'multiple_image_dropzone', 'class' => '', 'object' => 'tours', 'label' => 'Ảnh thông số đất'],
//                ['name' => 'water_parameters_image', 'type' => 'multiple_image_dropzone', 'class' => '', 'object' => 'tours', 'label' => 'Ảnh thông số nước'],
            ],
            /*'admin_id_tab' => [
                ['name' => 'admin_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Chọn người canh tác', 'model' => \Modules\A4iAdmin\Models\Admin::class, 'display_field' => 'name',],
            ]*/
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'admin_id' => [
            'label' => 'Người tạo',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'display_field2' => 'email',
            'object' => 'admin',
            'model' => \Modules\A4iAdmin\Models\Admin::class,
            'query_type' => '='
        ],
        'province_id' => [
            'label' => 'Tỉnh Thành',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'province',
            'model' => \Modules\A4iLocation\Models\Province::class,
            'query_type' => '='
        ],
        'district_id' => [
            'label' => 'Quận Huyện',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'district',
            'model' => \Modules\A4iLocation\Models\District::class,
            'query_type' => '='
        ],
        'ward_id' => [
            'label' => 'Phường Xã',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'ward',
            'model' => \Modules\A4iLocation\Models\Ward::class,
            'query_type' => '='
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);


        return view('trangtraisetting::list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        if (!$request->has('view')) {
            $query = $query->where('admin_id', \Auth::guard('admin')->id());
        }
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('trangtraisetting::add')->with($data);
            } else if ($_POST) {

                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    $data['admin_id'] = \Auth::guard('admin')->id();

                    DB::beginTransaction();
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {

                        DB::commit();


                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            DB::rollback();
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('trangtraisetting::edit')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên gắn gọn',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());



                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia


            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia


            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
