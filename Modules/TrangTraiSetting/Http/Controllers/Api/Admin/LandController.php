<?php

namespace Modules\TrangTraiSetting\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\A4iAdmin\Models\Admin;
use Modules\TrangTraiSetting\Models\Land;
use Illuminate\Http\Request;
use Modules\TrangTraiSetting\Models\Seed;
use Modules\TrangTraiSetting\Models\Unit;
use Validator;

class LandController extends Controller
{

    protected $module = [
        'code' => 'land',
        'table_name' => 'lands',
        'label' => 'Mảnh đất',
        'modal' => 'Modules\TrangTraiSetting\Models\Land',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'admin_name' => [
            'query_type' => 'custom'
        ],
        'province_id' => [
            'query_type' => '='
        ],
        'district_id' => [
            'query_type' => '='
        ],
        'ward_id' => [
            'query_type' => '='
        ],
    ];

    protected $type_owneds = [
        1 => 'Đất sử hữu',
        2 => 'Đất thuê',
        3 => 'Đất mượn',
        4 => 'Khác',
    ];

    public function index(Request $request)
    {
        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = land::leftJoin('seasons', 'lands.id', '=', 'seasons.land_id')
                ->leftJoin('admin', 'lands.admin_id', '=', 'admin.id')
                ->selectRaw('lands.id, lands.name, lands.province_id, lands.district_id, lands.ward_id, lands.address, lands.created_at, lands.acreage, 
                seasons.id as season_id, admin.id as admin_id,admin.name as admin_name,seasons.id as season_id, seasons.seed_id as season_seed, seasons.implementation_date as season_implementation_date, 
                seasons.created_at as season_created_at, seasons.quantum as season_quantum, seasons.unit_id as season_unit_id')
                ->whereRaw($where)->groupBy('lands.id');

            //  Sort
            $listItem = $this->sort($request, $listItem);
            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());

            foreach ($listItem as $k => $item) {
                $item->province = @$item->province->name;
                $item->district = @$item->district->name;
                $item->ward = @$item->ward->name;
                $item->admin = [
                    'id' => @$item->admin_id,
                    'name' => @$item->admin_name,
                ];

                $item->season = [
                    'id' => $item->season_id,
                    'tree_name' => @Seed::find($item->season_seed)->name_supplier,
                    'implementation_date' => $item->season_implementation_date
                ];

                unset($item->province_id);
                unset($item->district_id);
                unset($item->ward_id);
                unset($item->season_id);
                unset($item->admin_id);
                unset($item->admin_name);
                unset($item->province);
                unset($item->district);
                unset($item->ward);
                unset($item->season_id);
                unset($item->season_seed);
                unset($item->season_created_at);
                unset($item->season_quantum);
                unset($item->season_unit_id);
                unset($item->season_implementation_date);
            }
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {
            //  Check permission

            $item = land::leftJoin('seasons', 'lands.id', '=', 'seasons.land_id')
                ->leftJoin('districts', 'districts.id', '=', 'lands.district_id')
                ->leftJoin('provinces', 'provinces.id', '=', 'lands.province_id')
                ->leftJoin('wards', 'wards.id', '=', 'lands.ward_id')
                ->leftJoin('admin', 'admin.id', '=', 'lands.admin_id')
                ->selectRaw('lands.*, seasons.id as season_id, seasons.seed_id as season_seed, 
                seasons.created_at as season_created_at, seasons.quantum as season_quantum, seasons.unit_id as season_unit_id,
                districts.name as district_name, provinces.name as province_name, wards.name as ward_name,
                admin.id as admin_id, admin.name as admin_name')->where('lands.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }


            $item->season = [
                'id' => $item->season_id,
                'tree_name' => @Seed::find($item->season_seed)->name_supplier,
                'start_date' => $item->season_created_at,
                'quantum' => $item->season_quantum,
                'unit' => @Unit::find($item->season_unit_id)->name,
            ];

            unset($item->season_id);
            unset($item->season_seed);
            unset($item->season_created_at);
            unset($item->season_quantum);
            unset($item->season_unit_id);

            $item->admin = [
                'id' => $item->admin_id,
                'name' => $item->admin_name,
            ];
            unset($item->admin_id);
            unset($item->admin_name);

            $item->province = @$item->province->name;
            $item->district = @$item->district->name;
            $item->ward = @$item->ward->name;
            unset($item->province_id);
            unset($item->district_id);
            unset($item->ward_id);
            unset($item->district_name);
            unset($item->province_name);
            unset($item->ward_name);
            unset($item->province);
            unset($item->district);
            unset($item->ward);

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();

            $item = land::create($data);

            return $this->show($item->id);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $item = land::find($id);
            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Validate errors',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $data = $request->except('api_token');


            foreach ($data as $k => $v) {
                $item->{$k} = $v;
            }
            $item->save();

            return $this->show($item->id);
        }
    }

    public function delete($id)
    {
        if (Land::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . $this->module['table_name'] . '.id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                } elseif ($filter_option['query_type'] == 'custom') {
                    $admin = Admin::where('name', 'like', '%' . $request->get($filter_name) . '%')->pluck('id')->toArray();
                    if (!empty($admin)) {
                        $where .= " AND lands.admin_id IN (" . implode(',', $admin) . ")";
                    }
                }


            }
        }
//        dd($where);
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
