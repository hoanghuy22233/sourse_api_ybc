<?php

namespace Modules\TrangTraiSetting\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\A4iAdmin\Models\Admin;
use Modules\TrangTraiSetting\Models\Land;
use Illuminate\Http\Request;
use Modules\TrangTraiSetting\Models\DiarySeason;
use Validator;

class DiarySeasonController extends Controller
{

    protected $module = [
        'code' => 'diary_season',
        'table_name' => 'diary_seasons',
        'label' => 'Nhật ký canh tác',
        'modal' => 'Modules\TrangTraiSetting\Models\DiarySeason',
    ];

    protected $filter = [

        'admin_id' => [
            'query_type' => '='
        ],
        'land_id' => [
            'query_type' => '='
        ],
        'season_id' => [
            'query_type' => '='
        ],
        'created_at' => [
            'query_type' => 'custom'
        ],

    ];


    public function index(Request $request)
    {
        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = DiarySeason::leftJoin('lands', 'diary_seasons.id', '=', 'diary_seasons.land_id')
                ->join('criterias', 'diary_seasons.criteria_id', '=', 'criterias.id')
                ->selectRaw('diary_seasons.id, diary_seasons.criteria_id, diary_seasons.created_at, diary_seasons.land_id,
                 diary_seasons.amount, criterias.id as criteria_id,criterias.name as criteria_name, criterias.unit as criteria_unit')
                ->whereRaw($where)->groupBy('diary_seasons.id');

            if (!is_null($request->get('land_id'))) {
                $land_ids = Land::where('id', $request->land_id)->pluck('id')->toArray();
                $listItem = $listItem->whereIn('land_id', $land_ids);
            }

            if (!is_null($request->get('created_at'))) {
                $listItem = $listItem->where('diary_seasons.created_at', '>=', date('Y-m-d 00:00:00', strtotime($request->created_at)))->where('diary_seasons.created_at', '<=', date('Y-m-d 23:59:59', strtotime($request->created_at)));
            }

            //  Sort
            $listItem = $this->sort($request, $listItem);
            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());

            foreach ($listItem as $k => $item) {
                $item->criteria = [
                    'id' => @$item->criteria_id,
                    'name' => @$item->criteria_name,
                    'unit' => @$item->criteria_unit,
                ];

                unset($item->criteria_id);
                unset($item->criteria_name);
                unset($item->criteria_unit);
            }
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {
            //  Check permission

            $item = DiarySeason::leftJoin('lands', 'diary_seasons.id', '=', 'diary_seasons.land_id')
                ->join('criterias', 'diary_seasons.criteria_id', '=', 'criterias.id')
                ->selectRaw('diary_seasons.*,
                  
                  
                  
                  criterias.id as criteria_id, criterias.name as criteria_name')
                ->where('diary_seasons.id', $id)
                ->first();
            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }


            $item->criteria = [
                'id' => @$item->criteria_id,
                'name' => @$item->criteria_name,
            ];

            unset($item->criteria_id);
            unset($item->criteria_name);

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required'
        ], [
            'amount.required' => 'Bắt buộc phải nhập số lượng',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert


            $item = DiarySeason::create($data);

            return $this->show($item->id);

        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required'
        ], [
            'amount.required' => 'Bắt buộc phải nhập số lượng',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $item = DiarySeason::find($id);
            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Validate errors',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $data = $request->except('api_token');
            //  Tùy chỉnh dữ liệu insert


            foreach ($data as $k => $v) {
                $item->{$k} = $v;
            }
            $item->save();

            return $this->show($item->id);
        }
    }

    public function delete($id)
    {
        if (DiarySeason::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . $this->module['table_name'] . '.id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                } elseif ($filter_option['query_type'] == 'custom') {
                    $admin = Admin::where('name', 'like', '%' . $request->get($filter_name) . '%')->pluck('id')->toArray();
                    if (!empty($admin)) {
                        $where .= " AND lands.admin_id IN (" . implode(',', $admin) . ")";
                    }
                }


            }
        }
//        dd($where);
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
