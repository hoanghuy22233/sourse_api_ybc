<?php

namespace Modules\TrangTraiSetting\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\A4iAdmin\Models\Admin;
use Modules\TrangTraiSetting\Models\Land;
use Illuminate\Http\Request;
use Modules\TrangTraiSetting\Models\DiarySeason;
use Modules\TrangTraiSetting\Models\UnusualRecord;
use Validator;

class UnusualRecordController extends Controller
{

    protected $module = [
        'code' => 'unusual_record',
        'table_name' => 'unusual_records',
        'label' => 'Ghi nhận bất thường',
        'modal' => 'Modules\TrangTraiSetting\Models\UnusualRecord',
    ];

    protected $filter = [

        'admin_id' => [
            'query_type' => '='
        ],
        'land_id' => [
            'query_type' => '='
        ],
        'season_id' => [
            'query_type' => '='
        ],
        'created_at' => [
            'query_type' => 'custom'
        ],
    ];


    public function index(Request $request)
    {
        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = UnusualRecord::leftJoin('lands', 'unusual_records.id', '=', 'unusual_records.land_id')
                ->leftJoin('seasons', 'unusual_records.id', '=', 'unusual_records.season_id')
                ->selectRaw('unusual_records.*')
                ->whereRaw($where)->groupBy('unusual_records.id');

            if (!is_null($request->get('created_at'))) {
                $listItem = $listItem->where('unusual_records.created_at', '>=', date('Y-m-d 00:00:00', strtotime($request->created_at)))->where('unusual_records.created_at', '<=', date('Y-m-d 23:59:59', strtotime($request->created_at)));
            }

            //  Sort
            $listItem = $this->sort($request, $listItem);
            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            foreach ($listItem as $k => $v) {
                if ($v->image != null) {
                    $v->image = asset('public/filemanager/userfiles/' . $v->image);
                }
                foreach (explode('|', $v->image_extra) as $img) {
                    if ($img != '') {
                        $image_extra[] = asset('public/filemanager/userfiles/' . $img);
                    }
                }
                $v->image_extra = @$image_extra;

                //video
                if ($v->video != null) {
                    $v->video = asset('public/filemanager/userfiles/' . $v->video);
                }
                foreach (explode('|', $v->video_extra) as $img) {
                    if ($img != '') {
                        $video_extra[] = asset('public/filemanager/userfiles/' . $img);
                    }
                }
                $v->video_extra = @$video_extra;

            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {
            //  Check permission

            $item = UnusualRecord::leftJoin('lands', 'unusual_records.id', '=', 'unusual_records.land_id')
                ->leftJoin('seasons', 'unusual_records.id', '=', 'unusual_records.season_id')
                ->selectRaw('unusual_records.*')
                ->where('unusual_records.id', $id)
                ->first();
            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            if ($item->image != null) {
                $item->image = asset('public/filemanager/userfiles/' . $item->image);
            }
            foreach (explode('|', $item->image_extra) as $img) {
                if ($img != '') {
                    $image_extra[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->image_extra = @$image_extra;

            //video
            if ($item->video != null) {
                $item->video = asset('public/filemanager/userfiles/' . $item->video);
            }
            foreach (explode('|', $item->video_extra) as $img) {
                if ($img != '') {
                    $video_extra[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->video_extra = @$video_extra;


            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phenomena' => 'required'
        ], [
            'phenomena.required' => 'Bắt buộc phải nhập hiện tượng',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($image, 'diary_season');
                    }
                } else {
                    $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($request->file('image'), 'diary_season');
                }
                $data['image_extra'] = implode('|', $data['image_extra']);
            }


            if ($request->has('video')) {
                if (is_array($request->file('video'))) {
                    foreach ($request->file('video') as $image) {
                        $data['video'] = $data['video_extra'][] = CommonHelper::saveFile($image, 'diary_season');
                    }
                } else {
                    $data['video'] = $data['video_extra'][] = CommonHelper::saveFile($request->file('video'), 'diary_season');
                }
                $data['video_extra'] = implode('|', $data['video_extra']);
            }


            $item = UnusualRecord::create($data);

            return $this->show($item->id);

        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'phenomena' => 'required'
        ], [
            'phenomena.required' => 'Bắt buộc phải nhập hiện tượng',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $item = UnusualRecord::find($id);
            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Validate errors',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $data = $request->except('api_token');
            //  Tùy chỉnh dữ liệu insert

            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($image, 'diary_season');
                    }
                } else {
                    $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($request->file('image'), 'diary_season');
                }
                $data['image_extra'] = implode('|', $data['image_extra']);
            }


            if ($request->has('video')) {
                if (is_array($request->file('video'))) {
                    foreach ($request->file('video') as $image) {
                        $data['video'] = $data['video_extra'][] = CommonHelper::saveFile($image, 'diary_season');
                    }
                } else {
                    $data['video'] = $data['video_extra'][] = CommonHelper::saveFile($request->file('video'), 'diary_season');
                }
                $data['video_extra'] = implode('|', $data['video_extra']);
            }


            foreach ($data as $k => $v) {
                $item->{$k} = $v;
            }
            $item->save();

            return $this->show($item->id);
        }
    }

    public function delete($id)
    {
        if (UnusualRecord::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . $this->module['table_name'] . '.id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                } elseif ($filter_option['query_type'] == 'custom') {
                    $admin = Admin::where('name', 'like', '%' . $request->get($filter_name) . '%')->pluck('id')->toArray();
                    if (!empty($admin)) {
                        $where .= " AND lands.admin_id IN (" . implode(',', $admin) . ")";
                    }
                }


            }
        }
//        dd($where);
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
