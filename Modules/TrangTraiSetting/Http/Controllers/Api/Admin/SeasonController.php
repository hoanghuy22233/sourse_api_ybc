<?php

namespace Modules\TrangTraiSetting\Http\Controllers\Api\Admin;

use App\Http\Controllers\Admin\ImportController;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\TrangTraiSetting\Models\Season;
use Modules\A4iLand\Models\Land;
use Illuminate\Http\Request;
use Modules\TrangTraiSetting\Models\Seed;
use Modules\A4iSeason\Models\TechnicalCare;
use Modules\TrangTraiSetting\Models\Unit;
use Validator;

class SeasonController extends Controller
{

    protected $module = [
        'code' => 'season',
        'table_name' => 'seasons',
        'label' => 'Mùa vụ',
        'modal' => 'Modules\TrangTraiSetting\Models\Season',
    ];

    protected $filter = [
        'quantity_expected' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'land_id' => [
            'query_type' => '='
        ],
        'status' => [
            'query_type' => '='
        ],
        'from_date' => [
            'query_type' => 'custom'
        ],
        'to_date' => [
            'query_type' => 'custom'
        ],
        'province_id' => [
            'query_type' => 'custom'
        ],
        'district_id' => [
            'query_type' => 'custom'
        ],
        'ward_id' => [
            'query_type' => 'custom'
        ],
    ];

    protected $land_type_owneds = [
        1 => 'Đất sử hữu',
        2 => 'Đất thuê',
        3 => 'Đất mượn',
        4 => 'Khác',
    ];

    protected $status = [
        1 => 'Đang diễn ra',
        0 => 'Kết thúc',
    ];

    public function index(Request $request)
    {
        try {


            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Season::leftJoin('lands', 'lands.id', '=', 'seasons.land_id')
                ->join('seeds', 'seeds.id', '=', 'seasons.seed_id')
                ->join('units', 'units.id', '=', 'seasons.unit_id')
                ->selectRaw('seasons.id, seasons.seed_id, seasons.expected_output,
                 seasons.quantum, seasons.created_at, seasons.unit_id, seasons.note,
                   seeds.name_supplier as seed_name,
                   units.id as unit_id,units.name as unit_name
                ')->whereRaw($where);


            //  Sort
            $listItem = $this->sort($request, $listItem);
            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());

            foreach ($listItem as $k => $v) {
                $v->seed = [
                    'id' => $v->seed_id,
                    'name' => $v->seed_name,

                ];
                $v->unit = [
                    'id' => $v->seed_id,
                    'name' => $v->unit_name,

                ];
                unset($v->seed_id);
                unset($v->unit_id);

            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {
            $item = Season::join('lands', 'lands.id', '=', 'seasons.land_id')
                ->join('seeds', 'seeds.id', '=', 'seasons.seed_id')
                ->join('units', 'units.id', '=', 'seasons.unit_id')
                ->selectRaw('seasons.id, seasons.seed_id, seasons.implementation_date,
                 seasons.expected_output, seasons.quantum,
                  seasons.created_at, seasons.unit_id,
                   seasons.note,seeds.id as seed_id,
                   seeds.name_supplier as seed_name,
                   units.id as unit_id,units.name as unit_name
                 ')->where('seasons.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }


            $item->seed = [
                'id' => $item->seed_id,
                'name' => $item->seed_name,

            ];
            $item->unit = [
                'id' => $item->seed_id,
                'name' => $item->unit_name,

            ];
            unset($item->seed_id);
            unset($item->unit_id);
            unset($item->unit_name);
            unset($item->seed_name);


            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'expected_output' => 'required'
        ], [
            'expected_output.required' => 'Bắt buộc phải nhập sản lượng',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();

            $item = Season::create($data);

            return $this->show($item->id);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'expected_output' => 'required'
        ], [
            'expected_output.required' => 'Bắt buộc phải nhập sản lượng',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {

            $item = Season::find($id);

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Validate errors',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $data = $request->except('api_token');

            //  Tùy chỉnh dữ liệu insert
            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($image, 'season');
                    }
                } else {
                    $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($request->file('image'), 'season');
                }
                $data['image_extra'] = implode('|', $data['image_extra']);
            }

            foreach ($data as $k => $v) {
                $item->{$k} = $v;
            }
            $item->save();
            return $this->show($item->id);
        }
    }

    function tong_thu($harvest)
    {
        $tong_thu = 0;
        if ($harvest->quantity_unit == 'tấn') {
            $tong_thu = (int)$harvest->quantity * 1000;
            if ($harvest->price_unit == '/tấn') {
                $tong_thu *= ((int)$harvest->price / 1000);
            } else {
                $tong_thu *= (int)$harvest->price;
            }
        } elseif ($harvest->quantity_unit == 'kg') {
            $tong_thu = (int)$harvest->quantity;
            if ($harvest->price_unit == '/tấn') {
                $tong_thu *= ((int)$harvest->price / 1000);
            } else {
                $tong_thu *= (int)$harvest->price;
            }
        }
        return $tong_thu;
    }

    public function delete($id)
    {
        if (Season::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . $this->module['table_name'] . '.id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }

    public function createSeasonsLand(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'expected_output' => 'required',
        ], [
            'expected_output.required' => 'Bắt buộc phải nhập sản lượng',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();

            $land = new \Modules\TrangTraiSetting\Models\Land();
            $land->admin_id = \Auth::guard('api')->id();
            if (isset($data['land_name'])) {
                $land->name = $request->get('land_name', '');
                unset($data['land_name']);
            }
            if (isset($data['land_acreage'])) {
                $land->acreage = $request->get('land_acreage', '');
                unset($data['land_acreage']);
            }
            if (isset($data['land_address'])) {
                $land->address = $request->get('land_address', '');
                unset($data['land_address']);
            }

            $land->save();
            $data['land_id'] = $land->id;
            $data['admin_id'] = \Auth::guard('api')->id();

            $item = Season::create($data);

            return $this->show($item->id);
        }
    }
}
