<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'lands', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\LandController@index')->middleware('api_permission:land_view');
        Route::post('', 'Admin\LandController@store')->middleware('api_permission:land_add');
        Route::get('{id}', 'Admin\LandController@show')->middleware('api_permission:land_view');
        Route::post('{id}', 'Admin\LandController@update')->middleware('api_permission:land_edit');
        Route::delete('{id}', 'Admin\LandController@delete')->middleware('api_permission:land_delete');
    });

    Route::group(['prefix' => 'seasons', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\SeasonController@index')->middleware('api_permission:season_view');
        Route::post('', 'Admin\SeasonController@store')->middleware('api_permission:season_add');
        Route::post('seasons-land', 'Admin\SeasonController@createSeasonsLand')->middleware('api_permission:season_add');
        Route::get('{id}', 'Admin\SeasonController@show')->middleware('api_permission:season_view');
        Route::post('{id}', 'Admin\SeasonController@update')->middleware('api_permission:season_edit');
        Route::delete('{id}', 'Admin\SeasonController@delete')->middleware('api_permission:season_delete');
    });

    Route::group(['prefix' => 'diary_seasons', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\DiarySeasonController@index');
        Route::post('', 'Admin\DiarySeasonController@store');
        Route::get('{id}', 'Admin\DiarySeasonController@show');
        Route::post('{id}', 'Admin\DiarySeasonController@update');
        Route::delete('{id}', 'Admin\DiarySeasonController@delete');
    });

    Route::group(['prefix' => 'units', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\UnitController@index');
        Route::post('', 'Admin\UnitController@store');
        Route::get('{id}', 'Admin\UnitController@show');
        Route::post('{id}', 'Admin\UnitController@update');
        Route::delete('{id}', 'Admin\UnitController@delete');
    });

    Route::group(['prefix' => 'seeds', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\SeedlingController@index');
        Route::post('', 'Admin\SeedlingController@store');
        Route::get('{id}', 'Admin\SeedlingController@show');
        Route::post('{id}', 'Admin\SeedlingController@update');
        Route::delete('{id}', 'Admin\SeedlingController@delete');
    });
    Route::group(['prefix' => 'tree_groups', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\TreeGroupController@index');
        Route::post('', 'Admin\TreeGroupController@store');
        Route::get('{id}', 'Admin\TreeGroupController@show');
        Route::post('{id}', 'Admin\TreeGroupController@update');
        Route::delete('{id}', 'Admin\TreeGroupController@delete');
    });

    Route::group(['prefix' => 'criterias', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\CriteriaController@index');
        Route::post('', 'Admin\CriteriaController@store');
        Route::get('{id}', 'Admin\CriteriaController@show');
        Route::post('{id}', 'Admin\CriteriaController@update');
        Route::delete('{id}', 'Admin\CriteriaController@delete');
    });

    Route::group(['prefix' => 'unusual_records', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\UnusualRecordController@index');
        Route::post('', 'Admin\UnusualRecordController@store');
        Route::get('{id}', 'Admin\UnusualRecordController@show');
        Route::post('{id}', 'Admin\UnusualRecordController@update');
        Route::delete('{id}', 'Admin\UnusualRecordController@delete');
    });

});