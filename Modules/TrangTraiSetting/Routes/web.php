<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'land'], function () {
        Route::get('', 'Admin\LandController@getIndex')->name('land')->middleware('permission:land_view');
        Route::get('publish', 'Admin\LandController@getPublish')->name('land.publish')->middleware('permission:land_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\LandController@add')->middleware('permission:land_add');
        Route::get('delete/{id}', 'Admin\LandController@delete')->middleware('permission:land_delete');
        Route::post('multi-delete', 'Admin\LandController@multiDelete')->middleware('permission:land_delete');
        Route::get('search-for-select2', 'Admin\LandController@searchForSelect2')->name('land.search_for_select2')->middleware('permission:land_view');
        Route::get('{id}', 'Admin\LandController@update')->middleware('permission:land_view');
        Route::post('{id}', 'Admin\LandController@update')->middleware('permission:land_edit');
    });

    Route::group(['prefix' => 'season'], function () {
        Route::get('', 'Admin\SeasonController@getIndex')->name('season')->middleware('permission:season_view');
        Route::get('publish', 'Admin\SeasonController@getPublish')->name('season.publish')->middleware('permission:season_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\SeasonController@add')->middleware('permission:season_add');
        Route::get('delete/{id}', 'Admin\SeasonController@delete')->middleware('permission:season_delete');
        Route::post('multi-delete', 'Admin\SeasonController@multiDelete')->middleware('permission:season_delete');
        Route::get('search-for-select2', 'Admin\SeasonController@searchForSelect2')->name('season.search_for_select2')->middleware('permission:season_view');
        Route::get('{id}', 'Admin\SeasonController@update')->middleware('permission:season_view');
        Route::post('{id}', 'Admin\SeasonController@update')->middleware('permission:season_edit');

    });
        Route::group(['prefix' => 'diary_season'], function () {
        Route::get('', 'Admin\DiarySeasonController@getIndex')->name('diary_season')->middleware('permission:diary_season_view');
        Route::get('publish', 'Admin\DiarySeasonController@getPublish')->name('diary_season.publish')->middleware('permission:diary_season_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\DiarySeasonController@add')->middleware('permission:diary_season_add');
        Route::get('delete/{id}', 'Admin\DiarySeasonController@delete')->middleware('permission:diary_season_delete');
        Route::post('multi-delete', 'Admin\DiarySeasonController@multiDelete')->middleware('permission:diary_season_delete');
        Route::get('search-for-select2', 'Admin\DiarySeasonController@searchForSelect2')->name('diary_season.search_for_select2')->middleware('permission:diary_season_view');
        Route::get('{id}', 'Admin\DiarySeasonController@update')->middleware('permission:diary_season_view');
        Route::post('{id}', 'Admin\DiarySeasonController@update')->middleware('permission:diary_season_edit');
    });

    Route::group(['prefix' => 'unusual_record'], function () {
        Route::get('', 'Admin\UnusualRecordController@getIndex')->name('unusual_record')->middleware('permission:unusual_record_view');
        Route::get('publish', 'Admin\UnusualRecordController@getPublish')->name('unusual_record.publish')->middleware('permission:unusual_record_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\UnusualRecordController@add')->middleware('permission:unusual_record_add');
        Route::get('delete/{id}', 'Admin\UnusualRecordController@delete')->middleware('permission:unusual_record_delete');
        Route::post('multi-delete', 'Admin\UnusualRecordController@multiDelete')->middleware('permission:unusual_record_delete');
        Route::get('search-for-select2', 'Admin\UnusualRecordController@searchForSelect2')->name('unusual_record.search_for_select2')->middleware('permission:unusual_record_view');
        Route::get('{id}', 'Admin\UnusualRecordController@update')->middleware('permission:unusual_record_view');
        Route::post('{id}', 'Admin\UnusualRecordController@update')->middleware('permission:unusual_record_edit');
    });


    Route::group(['prefix' => 'seedling'], function () {
        Route::get('', 'Admin\SeedlingController@getIndex')->middleware('permission:seedling_view');
        Route::get('publish', 'Admin\SeedlingController@getPublish')->name('seedling.publish')->middleware('permission:seedling_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\SeedlingController@add')->middleware('permission:seedling_add');
        Route::get('delete/{id}', 'Admin\SeedlingController@delete')->middleware('permission:seedling_delete');
        Route::post('multi-delete', 'Admin\SeedlingController@multiDelete')->middleware('permission:seedling_delete');
        Route::get('search-for-select2', 'Admin\SeedlingController@searchForSelect2')->name('seed.search_for_select2')->middleware('permission:seedling_view');
        Route::get('{id}', 'Admin\SeedlingController@update')->middleware('permission:seedling_edit');
        Route::post('{id}', 'Admin\SeedlingController@update')->middleware('permission:seedling_edit');
    });


    Route::group(['prefix' => 'unit'], function () {
        Route::get('', 'Admin\UnitController@getIndex')->middleware('permission:unit_view');
        Route::get('publish', 'Admin\UnitController@getPublish')->name('unit.publish')->middleware('permission:unit_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\UnitController@add')->middleware('permission:unit_add');
        Route::get('delete/{id}', 'Admin\UnitController@delete')->middleware('permission:unit_delete');
        Route::post('multi-delete', 'Admin\UnitController@multiDelete')->middleware('permission:unit_delete');
        Route::get('search-for-select2', 'Admin\UnitController@searchForSelect2')->name('unit.search_for_select2')->middleware('permission:unit_view');
        Route::get('{id}', 'Admin\UnitController@update')->middleware('permission:unit_edit');
        Route::post('{id}', 'Admin\UnitController@update')->middleware('permission:unit_edit');
    });


    Route::group(['prefix' => 'tree_group'], function () {
        Route::get('', 'Admin\TreeGroupController@getIndex')->middleware('permission:tree_group_view');
        Route::get('publish', 'Admin\TreeGroupController@getPublish')->name('unit.publish')->middleware('permission:tree_group_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TreeGroupController@add')->middleware('permission:tree_group_add');
        Route::get('delete/{id}', 'Admin\TreeGroupController@delete')->middleware('permission:tree_group_delete');
        Route::post('multi-delete', 'Admin\TreeGroupController@multiDelete')->middleware('permission:tree_group_delete');
        Route::get('search-for-select2', 'Admin\TreeGroupController@searchForSelect2')->name('unit.search_for_select2')->middleware('permission:tree_group_view');
        Route::get('{id}', 'Admin\TreeGroupController@update')->middleware('permission:tree_group_edit');
        Route::post('{id}', 'Admin\TreeGroupController@update')->middleware('permission:tree_group_edit');
    });


    Route::group(['prefix' => 'criteria'], function () {
        Route::get('', 'Admin\CriteriaController@getIndex')->middleware('permission:criteria_view');
        Route::get('publish', 'Admin\CriteriaController@getPublish')->name('unit.publish')->middleware('permission:criteria_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\CriteriaController@add')->middleware('permission:criteria_add');
        Route::get('delete/{id}', 'Admin\CriteriaController@delete')->middleware('permission:criteria_delete');
        Route::post('multi-delete', 'Admin\CriteriaController@multiDelete')->middleware('permission:criteria_delete');
        Route::get('search-for-select2', 'Admin\CriteriaController@searchForSelect2')->name('unit.search_for_select2')->middleware('permission:criteria_view');
        Route::get('{id}', 'Admin\CriteriaController@update')->middleware('permission:criteria_edit');
        Route::post('{id}', 'Admin\CriteriaController@update')->middleware('permission:criteria_edit');
    });

});
