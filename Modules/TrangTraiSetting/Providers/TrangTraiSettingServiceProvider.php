<?php

namespace Modules\TrangTraiSetting\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class TrangTraiSettingServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('TrangTraiSetting', 'Database/Migrations'));
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //Đăng ký quyền
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['land_view', 'land_add', 'land_edit', 'land_delete', 'land_publish',
                'season_view', 'season_add', 'season_edit', 'season_delete', 'season_publish',
                    'diary_season_view', 'diary_season_add', 'diary_season_edit', 'diary_season_delete', 'diary_season_publish',
                    'unusual_record_view', 'unusual_record_add', 'unusual_record_edit', 'unusual_record_delete', 'unusual_record_publish',
                    'seedling_view', 'seedling_add', 'seedling_edit', 'seedling_delete', 'seedling_publish',
                    'unit_view', 'unit_add', 'unit_edit', 'unit_delete', 'unit_publish',
                    'criteria_view', 'criteria_add', 'criteria_edit', 'criteria_delete', 'criteria_publish',
                    'tree_group_view', 'tree_group_add', 'tree_group_edit', 'tree_group_delete', 'tree_group_publish',



                ]);
            return $per_check;
        }, 1, 1);
    }


    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('trangtraisetting::partials.aside_menu.dashboard_after_land');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('trangtraisetting::partials.aside_menu.aside_menu_dashboard_after');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('trangtraisetting::partials.aside_menu.aside_menu_dashboad_after_data');
        }, 1, 1);

    }
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('TrangTraiSetting', 'Config/config.php') => config_path('trangtraisetting.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('TrangTraiSetting', 'Config/config.php'), 'trangtraisetting'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/trangtraisetting');

        $sourcePath = module_path('TrangTraiSetting', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/trangtraisetting';
        }, \Config::get('view.paths')), [$sourcePath]), 'trangtraisetting');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/trangtraisetting');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'trangtraisetting');
        } else {
            $this->loadTranslationsFrom(module_path('TrangTraiSetting', 'Resources/lang'), 'trangtraisetting');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('TrangTraiSetting', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
