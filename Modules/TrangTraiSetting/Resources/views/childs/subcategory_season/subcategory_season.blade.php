<?php
if (!isset($_GET['season_id']) && isset($result)) {
    $_GET['season_id'] = $result->id;
    $season = $result;
} else {
    $season = \Modules\TrangTraiSetting\Models\Season::find($_GET['season_id']);
}

?>
<div class="row">
    <div class="col-md-12">
        <p class="pull-right btn-action-job" @if($module['code'] != 'season') style=" padding: 0 20px;" @endif>
            <a href="{{ '/admin/season/'. @$_GET['season_id']}}"
               class="{{$module['code'] == 'season' && last(request()->segments()) != 'timeline'  ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon-list"></i>Thông tin mùa vụ</a>
            <a href="/admin/diary_season?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'diary_season' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon2-drop"></i>Nhật ký canh tác</a>
            <a href="/admin/unusual_record?season_id={{ @$_GET['season_id']}}"
               class="{{$module['code'] == 'unusual_record' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon2-correct"></i>Ghi nhận bất thường</a>
            {{--@if(isset($_GET['season_id']))--}}
                {{--<a href="/admin/season/{{ @$_GET['season_id'] }}/timeline?season_id={{ @$_GET['season_id']}}"--}}
                   {{--class="{{last(request()->segments()) == 'timeline' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i--}}
                            {{--class="flaticon-download-1"></i>Lịch sử vụ mùa</a>--}}
            {{--@endif--}}
        </p>
    </div>
</div>
@if($season->status == 0 && $season->admin_id == \Auth::guard('admin')->user()->id)
    <div class="col-md-12">
        <div class="alert alert-solid-danger alert-bold" role="alert">
            <div class="alert-text">Mùa vụ này đã kết thúc! Không thể chỉnh sửa. Để mở lại mùa vụ này, vui lòng <a href="/admin/harvest/edit-last-item?season_id={{ $season->id }}">Click Vào Đây</a> để sửa lại option "Sau khi thu hoạch" thành "Trồng tiếp nhưng theo chu kỳ mới"</div>
        </div>
    </div>
@endif

