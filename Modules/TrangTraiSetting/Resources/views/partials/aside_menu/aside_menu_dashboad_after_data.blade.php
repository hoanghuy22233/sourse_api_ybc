@if(in_array('season_view', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                            <i
                                    class="kt-menu__link-icon flaticon2-avatar"></i>
                        </span><span class="kt-menu__link-text">Dữ liệu</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                @if(in_array('tree_group_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/tree_group" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Nhóm cây</span></a></li>
                @endif
                @if(in_array('criteria_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/criteria" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Tiêu chí</span></a></li>
                @endif
                @if(in_array('seedling_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/seedling" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Giống cây</span></a></li>
                @endif

                @if(in_array('unit_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/unit" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Đơn vị</span></a></li>
                @endif
            </ul>
        </div>
    </li>
@endif