<label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
        <span class="color_btd">*</span>@endif</label>
<div class="col-xs-12">
    <?php
        if (in_array('super_admin', $permissions) && @$result->admin_id != \Auth::guard('admin')->user()->id) {
            $lands = \Modules\TrangTraiSetting\Models\Land::select(['id', 'name'])->get();
        } else {
            $lands = \Modules\TrangTraiSetting\Models\Land::select(['id', 'name'])->where('admin_id', \Auth::guard('admin')->user()->id)->get();
        }
    ?>
    <select class="form-control" name="{{ $field['name'] }}">
        @foreach($lands as $v)
            <option value="{{ $v->id }}" {{ @$_GET['land_id'] == $v->id || @$result->land_id == $v->id ? 'selected' : '' }}>{{ $v->name }}</option>
        @endforeach
    </select>
    <span class="form-text text-muted">Bạn chưa tạo mảnh đất nào? <a href="/admin/land/add">Tạo mới</a> </span>
    <span class="text-danger">{{ $errors->first($field['name']) }}</span>
</div>