<?php


namespace Modules\TrangTraiSetting\Models;

use App\Http\Helpers\CommonHelper;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;
use Modules\A4iLocation\Models\{Ward,District,Province};
use Modules\TrangTraiSetting\Models\Season;


class Unit extends Model
{
    protected $table = 'units';
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }
}
