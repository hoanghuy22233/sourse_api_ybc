<?php

namespace Modules\TrangTraiSetting\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\A4iSeason\Models\Season;
use App\Models\Admin;
use Modules\A4iSeason\Models\Seed;
use Modules\TrangTraiSetting\Models\Criteria;

class DiarySeason extends Model
{
    protected $table = 'diary_seasons';

    protected $fillable = [
        'id','criteria_id', 'created_at', 'updated_at', 'amount', 'land_id', 'season_id'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function season()
    {
        return $this->hasOne(Season::class, 'season_id');
    }
    public function land()
    {
        return $this->hasOne(Land::class, 'land_id');
    }
    public function criteria()
    {
        return $this->belongsTo(Criteria::class, 'criteria_id');
    }


}
