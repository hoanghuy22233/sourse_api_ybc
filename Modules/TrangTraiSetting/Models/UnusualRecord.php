<?php

namespace Modules\TrangTraiSetting\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\A4iSeason\Models\Season;
use App\Models\Admin;
use Modules\A4iSeason\Models\Seed;
use Modules\TrangTraiSetting\Models\Criteria;

class UnusualRecord extends Model
{
    protected $table = 'unusual_records';


    protected $fillable = [
        'id', 'phenomena', 'reason', 'solution', 'image', 'created_at',
        'image_extra', 'video', 'video_extra', 'land_id', 'season_id', 'warning'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function season()
    {
        return $this->hasOne(Season::class, 'season_id');
    }
    public function land()
    {
        return $this->hasOne(Land::class, 'land_id');
    }



}
