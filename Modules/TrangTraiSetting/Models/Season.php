<?php

namespace Modules\TrangTraiSetting\Models;

use App\Http\Helpers\CommonHelper;
use Illuminate\Auth\Authenticatable;
Use App\Http\Controllers\Admin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;
use Modules\A4iLand\Models\Land;
use Modules\A4iLocation\Models\Province;
use Modules\A4iOrganization\Models\Organization;

class Season extends Model
{

    protected $table = 'seasons';

    protected $fillable = [
        'id','status', 'land_id', 'seed_id', 'expected_output', 'admin_id', 'created_at', 'date', 'quantum', 'note', 'unit_id',
        'implementation_date'
    ];

    public function Land()
    {
        return $this->belongsTo(Land::class, 'land_id');
    }

    public function admin()
    {
        return $this->belongsTo(\Modules\A4iAdmin\Models\Admin::class, 'admin_id');
    }

    public function provinces()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function delete()
    {
        $this->disease()->delete();
        $this->drainage()->delete();
        $this->fertilizer()->delete();
        $this->harvest()->delete();
        $this->herbicide()->delete();
        $this->investment_material()->delete();
        $this->nutrition()->delete();
        $this->plant_growth()->delete();
        $this->plant_medicine()->delete();
        $this->seed()->delete();
        $this->technical_care()->delete();
        $this->weather_information()->delete();
        $this->analysis()->delete();
        parent::delete();
    }

    public function disease()
    {
        return $this->hasMany(Disease::class, 'season_id', 'id');
    }

    public function drainage()
    {
        return $this->hasMany(Drainage::class, 'season_id', 'id');
    }

    public function fertilizer()
    {
        return $this->hasMany(Fertilizer::class, 'season_id', 'id');
    }

    public function harvest()
    {
        return $this->hasMany(Harvest::class, 'season_id', 'id');
    }

    public function herbicide()
    {
        return $this->hasMany(Herbicide::class, 'season_id', 'id');
    }

    public function investment_material()
    {
        return $this->hasMany(InvestmentMaterial::class, 'season_id', 'id');
    }

    public function nutrition()
    {
        return $this->hasMany(Nutrition::class, 'season_id', 'id');
    }

    public function plant_growth()
    {
        return $this->hasMany(PlantGrowth::class, 'season_id', 'id');
    }

    public function plant_medicine()
    {
        return $this->hasMany(PlantMedicine::class, 'season_id', 'id');
    }

    public function seedling()
    {
        return $this->belongsTo(Seed::class, 'seed_id');
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }

    public function technical_care()
    {
        return $this->hasMany(TechnicalCare::class, 'season_id', 'id');
    }

    public function weather_information()
    {
        return $this->hasMany(WeatherInformation::class, 'season_id', 'id');
    }



}
