<?php


namespace Modules\TrangTraiSetting\Models;

use App\Http\Helpers\CommonHelper;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;
use Modules\A4iLocation\Models\{Ward,District,Province};
use Modules\TrangTraiSetting\Models\Season;


class Seed extends Model
{
    protected $table = 'seeds';

    protected $fillable = [
        'id','name_supplier', 'information', 'note', 'tree_group_id'
    ];


    public function admin()
    {
        return $this->belongsTo(\Modules\EworkingAdmin\Models\Admin::class, 'admin_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }
    public function tree_group()
    {
        return $this->belongsTo(TreeGroup::class, 'tree_group_id');
    }
}
