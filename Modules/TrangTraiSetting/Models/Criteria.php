<?php


namespace Modules\TrangTraiSetting\Models;

use App\Http\Helpers\CommonHelper;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;


class Criteria extends Model
{
    protected $table = 'criterias';
    public $timestamps = false;

    protected $fillable = [
        'id','name','unit'
    ];

    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }
}
