<?php

namespace Modules\EworkingSetting\Providers;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;

class EworkingSettingServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
//        $this->registerTranslations();
//        $this->registerConfig();
        $this->registerViews();
//        $this->registerFactories();
//        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');


        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Custom setting

            //  Cấu hình menu trái
            $this->rendAsideMenu();

            //  Cấu hình header
            $this->rendHeaderTopbar();

            // Tài khoản chưa ở công ty nào bị direct sang trang chào mừng
            // $this->middlewareAdminCommon();
        }
    }

    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.admin', function () {
            return false;
        }, 10, 1);
    }

    public function rendHeaderTopbar()
    {
        \Eventy::addFilter('block.header_topbar', function () {
            print view('eworkingsetting::partials.header_topbar');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('eworkingsetting.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'eworkingsetting'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/eworkingsetting');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/eworkingsetting';
        }, \Config::get('view.paths')), [$sourcePath]), 'eworkingsetting');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/eworkingsetting');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'eworkingsetting');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'eworkingsetting');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
