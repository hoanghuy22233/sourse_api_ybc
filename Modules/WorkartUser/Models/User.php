<?php
namespace Modules\WorkartUser\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Modules\ThemeSemicolonwebJdes\Models\Company;
use Modules\ThemeWorkartAdmin\Models\LevelUser;
use Modules\WorkartProduct\Models\SendingMarketing;

class User extends Model
{

    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password', 'tel', 'image', 'address', 'image', 'gender', 'birthday','balance','stk','change_password','api_token'
    ];

    public function admin(){
        return $this->belongsTo(Admin::class,'admin_id');
    }
    public function company(){
        return $this->belongsTo(Company::class,'company_id');
    }
    public function level_users(){
        return $this->belongsTo(LevelUser::class,'user_level');
    }



}
