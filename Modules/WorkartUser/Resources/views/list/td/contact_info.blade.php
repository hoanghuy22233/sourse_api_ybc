<?php
$data = json_decode($item->contact_info);
?>
@if(is_array($data) && count($data) > 0)
    @foreach($data as $v)
        {{ @$v->name }} : {{ @$v->tel }}<br>
    @endforeach
@endif