@extends(config('core.admin_theme').'.template')
@section('main')
    <form class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid {{ @$module['code'] }}"
          action="" method="POST"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="return_direct" value="save_continue" type="hidden">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                     id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg" style="">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">{{trans('workartuser::admin.edit')}} {{ trans($module['label']) }}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="/admin/{{ $module['code'] }}" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">{{trans('workartuser::admin.back')}}</span>
                            </a>
                            <div class="btn-group">
                                @if(in_array($module['code'].'_edit', $permissions))
                                    <button type="submit" class="btn btn-brand dis">
                                        <i class="la la-check"></i>
                                        <span class="kt-hidden-mobile">{{trans('workartuser::admin.save')}}</span>
                                    </button>
                                    <button type="button"
                                            class="btn btn-brand dropdown-toggle dropdown-toggle-split dis"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_continue">
                                                    <i class="kt-nav__link-icon flaticon2-reload"></i>
                                                    {{trans('workartuser::admin.save_and_continue')}}
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_exit">
                                                    <i class="kt-nav__link-icon flaticon2-power"></i>
                                                    {{trans('workartuser::admin.save_and_quit')}}
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a class="kt-nav__link save_option" data-action="save_create">
                                                    <i class="kt-nav__link-icon flaticon2-add-1"></i>
                                                    {{trans('workartuser::admin.save_and_create')}}
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 @if(in_array('super_admin', $permissions)) col-md-6 @else col-md-6 @endif">
                <!--begin::Portlet-->
                <div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_1">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{trans('workartuser::admin.basic_information')}}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-group pt-3">
                            <a title="See more" href="#" data-ktportlet-tool="toggle"
                               class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-angle-down"></i></a>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                @foreach($module['form']['general_tab'] as $field)
                                    @php
                                        $field['value'] = @$result->{$field['name']};
                                    @endphp
                                    <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                         id="form-group-{{ $field['name'] }}">
                                        @if($field['type'] == 'custom')
                                            @include($field['field'], ['field' => $field])
                                        @else
                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                    <span class="color_btd">*</span>@endif</label>
                                            <div class="col-xs-12">
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                <span class="form-text text-muted">{!! @$field['des'] !!}</span>
                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
            <div class="col-xs-12 col-md-6">
                <!--begin::Portlet-->
                <div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_1">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{trans('workartuser::admin.image')}}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-group pt-3">
                            <a title="See more" href="#" data-ktportlet-tool="toggle"
                               class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-angle-down"></i></a>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                @foreach($module['form']['image_tab'] as $field)
                                    @php
                                        $field['value'] = @$result->{$field['name']};
                                    @endphp
                                    <div class="form-group-div form-group {{ @$field['group_class'] }}"
                                         id="form-group-{{ $field['name'] }}">
                                        @if($field['type'] == 'custom')
                                            @include($field['field'], ['field' => $field])
                                        @else
                                            <label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
                                                    <span class="color_btd">*</span>@endif</label>
                                            <div class="col-xs-12">
                                                @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
                                                <span class="form-text text-muted">{!! @$field['des'] !!}</span>
                                                <span class="text-danger">{{ $errors->first($field['name']) }}</span>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-12">
                <!--begin::Portlet-->
                <div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_1">
                {{--<div class="kt-portlet__head">--}}
                {{--<div class="kt-portlet__head-label">--}}
                {{--<h3 class="kt-portlet__head-title">--}}
                {{--Đơn hàng gần nhất--}}
                {{--</h3>--}}
                {{--</div>--}}
                {{--<div class="kt-portlet__head-group pt-3">--}}
                {{--<a title="See more" href="#" data-ktportlet-tool="toggle"--}}
                {{--class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-angle-down"></i></a>--}}
                {{--</div>--}}
                {{--</div>--}}
                <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                <table class="table table-striped">
                                    <thead class="kt-datatable__head">
                                    <tr class="kt-datatable__row" style="left: 0px;">

                                        <th data-field="id" class="kt-datatable__cell kt-datatable__cell--sort "
                                            onclick="sort('id')">
                                            Đơn hàng gần nhất
                                        </th>
                                        <th data-field="id" class="kt-datatable__cell kt-datatable__cell--sort "
                                            onclick="sort('id')">
                                            Tổng số đơn hàng đã đặt
                                        </th>
                                        <th data-field="total_price"
                                            class="kt-datatable__cell kt-datatable__cell--sort "
                                            onclick="sort('total_price')">
                                            Tổng số tiền đơn hàng
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody class="kt-datatable__body ps ps--active-y" style="max-height: 496px;">

                                    <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                        <?php



                                        $bill = \Modules\WorkartBill\Models\Bill::orderBy('created_at', 'desc')->first();

                                        $date1 = date_create(date('Y-m-d'));
                                        $date2 = date_create(date('Y-m-d', strtotime($bill->created_at)));
                                        $diff = date_diff($date1, $date2);
                                        ?>
                                        <td data-field="id" class="kt-datatable__cell item-id"><span>
                                                        {{$diff->format("%R%a ngày")    }}
                                                    </span>
                                        </td>

                                        <td data-field="total_price"
                                            class="kt-datatable__cell item-total_price">
                                            <span>{{ number_format(\Modules\WorkartBill\Models\Bill::where('user_id', $result->id)->count(), 0, '.', '.') }}</span>
                                        </td>
                                        <?php
                                        $count = \Modules\WorkartBill\Models\Bill::where('user_id', $result->id)->where('status', 1)->get()->sum('total_price');

                                        ?>
                                        <td data-field="id" class="kt-datatable__cell item-id">
                                            <a href="" target="_blank">{{'$'.number_format($count, 2, '.', '')}}</a>
                                        </td>

                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>


        <div class="row">
            <div class="col-xs-12 col-md-12">
                <!--begin::Portlet-->
                <div class="kt-portlet" data-ktportlet="true" id="kt_portlet_tools_1">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                Các đơn đã đặt
                            </h3>
                        </div>
                        <div class="kt-portlet__head-group pt-3">
                            <a title="See more" href="#" data-ktportlet-tool="toggle"
                               class="btn btn-sm btn-icon btn-clean btn-icon-md"><i class="la la-angle-down"></i></a>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="kt-form">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                <table class="table table-striped">
                                    <thead class="kt-datatable__head">
                                    <tr class="kt-datatable__row" style="left: 0px;">
                                        <th data-field="id" class="kt-datatable__cell kt-datatable__cell--sort "
                                            onclick="sort('image')">
                                            Ảnh
                                        </th>
                                        <th data-field="receipt_method"
                                            class="kt-datatable__cell kt-datatable__cell--sort "
                                            onclick="sort('receipt_method')">
                                            Phương thức thanh toán
                                        </th>
                                        <th data-field="total_price"
                                            class="kt-datatable__cell kt-datatable__cell--sort " onclick="sort('id')">
                                            Mã sản phẩm
                                        </th>
                                        <th data-field="total_price"
                                            class="kt-datatable__cell kt-datatable__cell--sort "
                                            onclick="sort('total_price')">
                                            Tổng tiền
                                        </th>
                                        <th data-field="total_price"
                                            class="kt-datatable__cell kt-datatable__cell--sort "
                                            onclick="sort('user_address')">
                                            Địa chỉ
                                        </th>


                                        <th data-field="updated_at" class="kt-datatable__cell kt-datatable__cell--sort "
                                            onclick="sort('updated_at')">
                                            Ngày đặt
                                        </th>

                                        <th data-field="receipt_method"
                                            class="kt-datatable__cell kt-datatable__cell--sort "
                                            onclick="sort('receipt_method')">
                                            Xem chi tiết
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="kt-datatable__body ps ps--active-y" style="max-height: 496px;">
                                    <?php
                                    $data = \Modules\WorkartBill\Models\Bill::where('user_id', @$result->id)->get();
                                    ?>
                                    @foreach($data as $v)
                                        <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                            <td data-field="updated_at" class="kt-datatable__cell item-updated_at">
                                                <img width="60" height="60"
                                                     src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image,95,95)}}">
                                            </td>

                                            <td data-field="id" class="kt-datatable__cell item-id">
                                                <?php
                                                $code = '|';
                                                foreach (@$v->orders as $prd) {
                                                    $code .= @$prd->product->code . '|';
                                                }
                                                ?>
                                                {{$code}}
                                            </td>
                                            <td data-field="receipt_method"
                                                class="kt-datatable__cell item-receipt_method">
                                                {{ $v->receipt_method }}
                                            </td>
                                            <td data-field="total_price" class="kt-datatable__cell item-total_price">
                                                <span>${{ number_format($v->total_price, 2, '.', '') }}</span>
                                            </td>
                                            <td data-field="total_price" class="kt-datatable__cell item-total_price">
                                                <span>{{ $v->user_address}}</span>
                                            </td>

                                            <td data-field="updated_at" class="kt-datatable__cell item-updated_at">
                                                {{ date('d/m/Y', strtotime($v->updated_at)) }}
                                            </td>

                                            <td data-field="updated_at" class="kt-datatable__cell item-updated_at">
                                                <a href="/admin/bill/{{ $v->id }}" target="_blank">Xem chi tiết</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </form>
@endsection
@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/form.css') }}">
    {{--    <link type="text/css" rel="stylesheet" charset="UTF-8"--}}
    {{--          href="{{ asset('Modules\EworkingUser\Resources\assets\css\custom.css') }}">--}}
    {{--    <script src="{{asset('Modules\EworkingUser\Resources\assets\js\custom.js')}}"></script>--}}
@endsection
@section('custom_footer')
    <script src="{{ asset(config('core.admin_asset').'/js/pages/crud/metronic-datatable/advanced/vertical.js') }}"
            type="text/javascript"></script>

    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/tinymce/tinymce_editor.js') }}"></script>
    <script type="text/javascript">
        editor_config.selector = ".editor";
        editor_config.path_absolute = "{{ (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" }}/";
        tinymce.init(editor_config);
    </script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/form.js') }}"></script>
    <script>
        $(document).ready(function () {
            $("input#tel").blur(function () {
                $.ajax({
                    url: "/check-tel-duplicate",
                    type: "post",
                    dataType: "text",
                    data: {
                        id: '{{$result->id}}',
                        tel: $('#tel').val()
                    },
                    success: function (result) {
                        $('#check_tel').html(result);
                        // if(result !==''){
                        //     $('button.dis').attr("disabled", true);
                        // }else{
                        //     $('button.dis').attr("disabled", false);
                        // }
                    }
                });
            });
            $("input#email").blur(function () {
                $.ajax({
                    url: "/check-mail-duplicate",
                    type: "post",
                    dataType: "text",
                    data: {
                        id: '{{$result->id}}',
                        email: $('#email').val()
                    },
                    success: function (result) {
                        $('#check_mail').html(result);
                        // if(result !==''){
                        //     $('button.dis').attr("disabled", true);
                        // }else{
                        //     $('button.dis').attr("disabled", false);
                        // }
                    }
                });
            });
        });
    </script>
@endsection
