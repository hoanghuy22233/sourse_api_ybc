<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'user'], function () {
        Route::get('', 'Admin\UserController@getIndex')->name('user')->middleware('permission:user_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\UserController@add')->middleware('permission:user_add');
        Route::get('search-for-select2', 'Admin\UserController@searchForSelect2')->name('user.search_for_select2')->middleware('permission:user_view');
        Route::get('{id}', 'Admin\UserController@update');
        Route::post('{id}', 'Admin\UserController@update')->middleware('permission:user_edit');

    });
});
