<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'product'], function () {
        Route::get('', 'Admin\ProductController@getIndex')->name('product')->middleware('permission:product_view');
        Route::post('multi-publish', 'Admin\ProductController@enabledStatus')->name('publish-status')->middleware('permission:product_view');
        Route::post('multi-dispublish', 'Admin\ProductController@disabledStatus')->name('dispublish-status')->middleware('permission:product_view');
        Route::get('publish', 'Admin\ProductController@getPublish')->name('product.publish')->middleware('permission:product_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ProductController@add')->middleware('permission:product_add');
        Route::get('delete/{id}', 'Admin\ProductController@delete')->middleware('permission:product_delete');
        Route::post('multi-delete', 'Admin\ProductController@multiDelete')->middleware('permission:product_delete');
        Route::get('search-for-select2', 'Admin\ProductController@searchForSelect2')->name('product.search_for_select2')->middleware('permission:product_view');

        Route::get('ajax-get-html-price-option', 'Admin\ProductController@ajaxGetHtmlPriceOption');
        Route::get('{id}/duplicate', 'Admin\ProductController@duplicate')->middleware('permission:product_add');

        Route::get('{id}', 'Admin\ProductController@update')->middleware('permission:product_edit');
        Route::post('{id}', 'Admin\ProductController@update')->middleware('permission:product_edit');
    });

    Route::group(['prefix' => 'category_product'], function () {
        Route::get('', 'Admin\CategoryProductController@getIndex')->name('category_product')->middleware('permission:category_product_view');
        Route::get('publish', 'Admin\CategoryProductController@getPublish')->name('category_product.publish')->middleware('permission:category_product_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\CategoryProductController@add')->middleware('permission:category_product_add');
        Route::get('delete/{id}', 'Admin\CategoryProductController@delete')->middleware('permission:category_product_delete');
        Route::post('multi-delete', 'Admin\CategoryProductController@multiDelete')->middleware('permission:category_product_delete');
        Route::get('search-for-select2', 'Admin\CategoryProductController@searchForSelect2')->name('category_product.search_for_select2')->middleware('permission:category_product_view');
        Route::get('{id}', 'Admin\CategoryProductController@update')->middleware('permission:category_product_view');
        Route::post('{id}', 'Admin\CategoryProductController@update')->middleware('permission:category_product_edit');
    });

    Route::group(['prefix' => 'tag_product'], function () {
        Route::get('', 'Admin\TagProductController@getIndex')->name('tag_product')->middleware('permission:tag_product_view');
        Route::get('publish', 'Admin\TagProductController@getPublish')->name('tag_product.publish')->middleware('permission:tag_product_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TagProductController@add')->middleware('permission:tag_product_add');
        Route::get('delete/{id}', 'Admin\TagProductController@delete')->middleware('permission:tag_product_delete');
        Route::post('multi-delete', 'Admin\TagProductController@multiDelete')->middleware('permission:tag_product_delete');
        Route::get('search-for-select2', 'Admin\TagProductController@searchForSelect2')->name('tag_product.search_for_select2')->middleware('permission:tag_product_view');
        Route::get('{id}', 'Admin\TagProductController@update')->middleware('permission:tag_product_view');
        Route::post('{id}', 'Admin\TagProductController@update')->middleware('permission:tag_product_edit');
    });
});
