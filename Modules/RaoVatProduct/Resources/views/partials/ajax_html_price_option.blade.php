<?php $k = time() . rand(1,1000);?>
<li style="margin-left: 20px; margin-bottom: 39px; border-bottom: 1px solid #ccc;">
    <a class="kt-avatar__cancel center-content attr-delete" data-toggle="kt-tooltip" data-original-title="Xóa tùy chọn giá" style="position: absolute;
    right: 0;
    top: -22px;
    background: #fff;
    padding: 3px;
    color: red;
    cursor: pointer;
    border: 1px solid red;
    border-radius: 47%;">
        <i class="fa fa-trash"></i>
    </a>
    <div class="col-xs-12">
        <?php $field = ['name' => 'attributes' . $k, 'type' => 'select2_ajax_model', 'object' => 'properties_value', 'multiple' => true, 'class' => '', 'label' => 'Attribute sản phẩm',
            'model' => \Modules\RaoVatProduct\Models\PropertyValue::class, 'display_field' => 'value', 'display_field2' => 'id', 'value' => @$attr->properties_value_ids];?>
        @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
    </div>
    <div class="row" style="margin-top: 15px;">
        <div class="col-xs-6">
            <?php $field = ['name' => 'image' . $k, 'type' => 'file_editor', 'label' => 'Ảnh mô tả', 'value' => @$attr->image];?>
            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
        </div>
        <div class="col-xs-6" style="    padding-left: 15px; max-width: 59%;">
            <?php $field = ['name' => 'final_price' . $k, 'type' => 'text', 'label' => 'Giá bán', 'class' => 'number_price', 'group_class' => 'col-md-4', 'value' => @$attr->final_price];?>
            @include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])
        </div>
    </div>
</li>