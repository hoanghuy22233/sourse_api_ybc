<?php

namespace Modules\RaoVatProduct\Http\Helpers;

use Modules\RaoVatProduct\Models\PropertieValue;
use Modules\ThemeRaoVat\Models\Category;
use View;
use Session;
use Modules\RaoVatProduct\Models\Meta;

class CommonHelper
{

    /*
     * Lấy danh sách quà tặng của sản phẩm
     * */
    public static function getAttributesProduct($product_id)
    {
        $product = \Modules\RaoVatProduct\Models\Product::find($product_id);
        if (!is_object($product)) {
            return [];
        }

        $attribute_values = PropertieValue::leftJoin('properties_name', 'properties_name.id', '=', 'properties_value.properties_name_id')
            ->selectRaw('properties_value.value, properties_value.description, properties_value.order_no, properties_value.link, properties_value.status, properties_name.name, properties_name.price_option')
            ->whereIn('properties_value.id', explode('|', $product->proprerties_id))->get()->toArray();
        return $attribute_values;
    }

    public static function getProductSlug($product, $type = 'object')
    {
        $multi_cat = $type == 'object' ? $product->multi_cat : $product['multi_cat'];
        $product_slug = $type == 'object' ? $product->slug : $product['slug'];
        $slug = '';
        $manufacture_id = $type == 'object' ? $product->manufacture_id : $product['manufacture_id'];
        try {
//            $cat = CommonHelper::getFromCache('get_category_slug_parent_id'.$multi_cat->id);
//            if (!$cat) {
            $cat = Category::select(['slug', 'parent_id'])->whereIn('id', explode('|', $multi_cat))->first();
            if (is_object($cat)) {
                $slug .= '/' . $cat->slug;
//                dd($slug);
                //  Lay slug thuong hieu
//                $manufacturer = Manufacturer::select('slug')->where('id', $manufacture_id)->first();
//                if (is_object($manufacturer)) {
//                    $slug .= '/' . $manufacturer->slug;
//                }

                //  lay link slug category con
                if ($cat->parent_id != null && $cat->parent_id != 0) {
                    $cat2 = Category::select(['slug', 'parent_id'])->where('id', $cat->parent_id)->first();
                    if (is_object($cat2)) {
                        $slug = '/' . $cat2->slug . $slug;
                        if ($cat2->parent_id != null && $cat2->parent_id != 0) {
                            $cat3 = Category::select(['slug', 'parent_id'])->where('id', $cat2->parent_id)->first();
                            if (is_object($cat3)) {
                                $slug = '/' . $cat3->slug . $slug;
                            }
                        }
                    }
                }
//                }
//                CommonHelper::putToCache('get_category_slug_parent_id'.$multi_cat->id, $cat);
            }
            return $slug . '/' . $product_slug . '.html';
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }


}