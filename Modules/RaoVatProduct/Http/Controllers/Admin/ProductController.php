<?php

namespace Modules\RaoVatProduct\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Auth;
use Illuminate\Http\Request;
use Modules\STBDBill\Models\Order;
use Modules\RaoVatProduct\Models\Category;
use Modules\RaoVatProduct\Models\Guarantees;
use Modules\RaoVatProduct\Models\Origin;
use Modules\RaoVatProduct\Models\Post;
use Modules\RaoVatProduct\Models\Product;
use Modules\RaoVatProduct\Models\ProductAttribute;
use Modules\RaoVatProduct\Models\PropertieName;
use Modules\RaoVatProduct\Models\PropertyValue;
use Validator;

class ProductController extends CURDBaseController
{
    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Sản phẩm',
        'modal' => '\Modules\RaoVatProduct\Models\Product',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'foder' => 'product_files'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'raovatproduct::list.td.multi_cat', 'label' => 'Danh mục'],
            ['name' => 'price_intro', 'type' => 'text', 'label' => 'Giá bán'],
//            ['name' => 'price_content', 'type' => 'text', 'label' => 'Giá bán'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],
            ['name' => 'view', 'type' => 'custom', 'td' => 'raovatproduct::list.td.view_frontend', 'label' => 'Xem'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên sản phẩm'],
                ['name' => 'final_price', 'type' => 'price_vi', 'class' => '', 'label' => 'Giá bán lẻ', 'group_class' => 'col-md-6'],
//                ['name' => 'price_intro', 'type' => 'text', 'class' => '', 'label' => 'Giá bán lẻ', 'group_class' => 'col-md-6'],
                ['name' => 'price_content', 'type' => 'textarea', 'label' => 'Giá mua nhiều', 'class' => '', 'des' => 'vd: < 300 chiếc là 400k/chiếc. 300-500 chiếc là 300k/chiếc'],
                ['name' => 'instock', 'type' => 'checkbox','label' => 'Còn hàng', 'class' => '', 'group_class' => 'col-md-3', 'value' => 1],
                ['name' => 'min_buy', 'type' => 'number','label' => 'Mua tối thiểu', 'class' => '', 'group_class' => 'col-md-3', 'des' => 'Số lượng tối thiểu phải mua'],
                ['name' => 'multi_cat', 'class' => 'required', 'type' => 'select2_model', 'field' => 'raovatproduct::form.fields.multi_cat', 'label' => 'Danh mục', 'model' => Category::class,
                    'object' => 'category_post', 'display_field' => 'name', 'where' => 'type=10 AND parent_id is null', 'group_class' => 'col-md-4'],
                ['name' => 'category_child_id', 'type' => 'custom', 'field' => 'raovatproduct::form.fields.category_child_id', 'label' => 'Ngành hàng', 'model' => Category::class, 'object' => 'category', 'display_field' => 'name', 'where' => 'type=10', 'group_class' => 'col-md-4'],
                ['name' => 'tags', 'type' => 'text', 'label' => 'Từ khóa bài viết', 'des' => 'Viết cách nhau bởi dấu phẩy. VD: từ khóa 1, từ khóa 2, từ khóa 3'],

                ['name' => 'production', 'type' => 'text', 'label' => 'Nơi sản xuát','group_class' => 'col-md-6'],
                ['name' => 'deadline', 'type' => 'datetimepicker', 'class' => '', 'label' => 'Ngày hết hạn','group_class' => 'col-md-6'],

                ['name' => 'intro', 'type' => 'custom', 'field' => 'themeraovatadmin::form.fields.count_char_intro', 'class' => '', 'label' => 'Mô tả ngắn (Tối đa 120 ký tự)'],
                ['name' => 'content', 'type' => 'textarea', 'class' => '', 'label' => 'Nội dung - Mô tả cụ thể', 'inner' => 'rows=20'],

                ['name' => 'province_id', 'type' => 'select_location', 'label' => 'Địa điểm bán', 'des' => 'Mặc định sẽ chọn địa chỉ tài khoản của bạn'],
                ['name' => 'address', 'type' => 'text', 'label' => 'Địa điểm cụ thể'],

                ['name' => 'status', 'type' => 'checkbox', 'class' => '', 'label' => 'Kích hoạt', 'value' => 1, 'group_class' => 'col-md-4'],
            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_image', 'label' => 'Ảnh đại diện'],
                ['name' => 'image_extra', 'type' => 'multiple_image_dropzone', 'count' => '6', 'label' => 'Thêm nhiều ảnh khác'],
            ],
            'contact_tab' => [
                ['name' => 'inner', 'type' => 'inner', 'label' => '', 'html' => 'Nếu bạn không điền gì thì mặc định lấy thông tin tài khoản của bạn'],
                ['name' => 'contact_name', 'type' => 'text', 'label' => 'Tên người liên hệ', ],
                ['name' => 'contact_tel', 'type' => 'text', 'label' => 'Điện thoại liên hệ', ],
                ['name' => 'contact_zalo', 'type' => 'text', 'label' => 'Zalo liên hệ', ],
                ['name' => 'contact_address', 'type' => 'text', 'label' => 'Địa chỉ liên hệ', ],
            ],
            'related_products_tab' => [

//                ['name' => 'related_products',
//                    'type' => 'select2_ajax_model', 'class' => '',
//                    'label' => 'Sản phẩm liên quan',
//                    'object' => 'product',
//                    'model' => Product::class,
//                    'multiple' => true,
//                    'display_field' => 'name',
//                    'display_field2' => 'id'],
//                ['name' => 'related_post',
//                    'type' => 'custom',  'field' => 'raovatproduct::form.fields.related_post', 'class' => '',
//                    'label' => 'Tin tức liên quan',
//                    'display_field' => 'name',
//                    'display_field2' => 'id'],
//
//                ['name' => 'gift_list', 'type' => 'text', 'class' => '', 'label' => 'Quà tặng'],
//                ['name' => 'gift_max', 'type' => 'text', 'class' => '', 'label' => 'Giới hạn giá trị quà'],
//                ['name' => 'gift_list',
//                    'type' => 'select2_ajax_model', 'class' => '',
//                    'label' => 'Quà tặng',
//                    'object' => 'product',
//                    'model' => Product::class,
//                    'multiple' => true,
////                    'where' => 'gift=1',
//                    'display_field' => 'name',
//                    'display_field2' => 'id'],
//

//                ['name' => 'gift_max', 'type' => 'number', 'label' => 'Số quà cho phép'],

            ],
            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'slug', 'des' => 'Đường dẫn sản phẩm trên thanh địa chỉ'],
                ['name' => 'meta_title', 'type' => 'custom', 'field' => 'raovatproduct::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta title', 'max_char' => 1000],
                ['name' => 'meta_description', 'type' => 'custom', 'field' => 'raovatproduct::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta description', 'max_char' => 1000],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
//                ['name' => 'star_number', 'type' => 'number', 'label' => 'Số sao', 'group_class' => 'col-md-4'],
//                ['name' => 'review_number', 'type' => 'number', 'label' => 'số đánh giá', 'group_class' => 'col-md-4'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, tên, mã, giá',
        'fields' => 'id, name, final_price, base_price'
    ];

    protected $filter = [
        /*'name' => [
            'label' => 'Tên sản phẩm',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'code' => [
            'label' => 'Mã sản phẩm',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'final_price' => [
            'label' => 'Giá bán',
            'type' => 'number',
            'query_type' => 'like'
        ],*/
//        'count_buy_product' => [
//            'label' => 'Lượt mua',
//            'type' => 'number',
//            'query_type' => 'custom'
//        ],

//        'category_id' => [
//            'label' => 'Danh mục',
//            'type' => 'select2_ajax_model',
//            'display_field' => 'name',
//            'object' => 'category_product',
//            'model' => \Modules\RaoVatProduct\Models\Category::class,
//            'query_type' => '='
//        ],

        'multi_cat' => [
            'label' => 'Danh mục',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'category_product',
            'model' => \Modules\RaoVatProduct\Models\Category::class,
            'query_type' => 'custom'
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
        ],
    ];

    public function getIndex(Request $request)
    {

        $data = $this->getDataList($request);

        return view('raovatproduct::list')->with($data);
    }

    public function appendWhere($query, $request)
    {
//        dd($request);
        //  Lọc theo danh mục
        if (!is_null($request->get('category_id'))) {
            $category = Category::find($request->category_id);


            if (is_object($category)) {
                $query = $query->where(function ($query) use ($category) {
                    $cat_childs = $category->childs; //  Lấy các id của danh mục con
                    $query->orWhere('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các tin thuộc danh mục hiện tại
                    foreach ($cat_childs as $cat_child) {
                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
                    }
                });
            }
        }


        if (!is_null($request->get('multi_cat'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->multi_cat . '|%');
        }
        if (!is_null($request->get('tags'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }

        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu mình tạo
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('admin_id', \Auth::guard('admin')->user()->id);
        }

        return $query;
    }

    public function add(Request $request)
    {
//        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('raovatproduct::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
//                    if ($request->has('multi_cat')) {
//                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
//                        $data['category_id'] = $request->multi_cat[0];
//                    }

                    if ($request->has('final_price') && $request->final_price != null) {
                        $data['price_intro'] = number_format($request->final_price, 0, '.', '.');
                    }

                    if ($request->has('image_extra')) {
                        $data['image_extra'] = '|' . implode('|', $request->image_extra) . '|';

                    }
//
//                    if ($request->has('manufacture_id')) {
//                        $data['manufacture_id'] = '|' . implode('|', $request->manufacture_id) . '|';
//                    }
                    if ($request->has('related_post')) {
                        $data['related_post'] = '|' . implode('|', $request->related_post) . '|';
                    }

                    $data['admin_id'] = \Auth::guard('admin')->user()->id;
//                    if ($request->has('tags')) {
//                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
//                    }

                    $data = $this->appendData($request, $data);
                    if (isset($data['error'])) {
                        return $this->returnError($data, $request);
                    }
                    if ($request->has('input_image_extra')) {
                        $data['input_image_extra'] = implode('|', $request->input_image_extra);
                    }

                    if ($request->has('proprerties_id')) {
                        $data['proprerties_id'] = '|' . implode('|', $request->proprerties_id) . '|';
                    }

                    $data['district_id'] = @$request->district_id;
                    $data['ward_id'] = @$request->ward_id;

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

//                    if (!empty($data['final_price']) && !empty($data['base_price'])) {
//                        $data['sale'] = ceil(($data['base_price'] - $data['final_price']) * 100 / $data['base_price']) . '%';
//                    } elseif (empty($data['final_price']) || empty($data['base_price'])) {
//                        $data['sale'] = '0%';
//                    } elseif (empty($data['final_price']) && empty($data['base_price'])) {
//                        $data['sale'] = '0%';
//                    }
                    if ($this->model->save()) {
                        if (\Schema::hasTable('product_attributes')) {
                            //  Cập nhật attribute cho sản phẩm
                            $product_attribute_updated = [];
                            foreach ($request->all() as $k => $v) {
                                if (strpos($k, 'attributes') !== false) {
                                    $key = str_replace('attributes', '', $k);
                                    $properties_value_ids = '|' . implode('|', $request->get('attributes'.$key)) . '|';
                                    if (strpos(@$request->get('image'.$key), 'filemanager')) {
                                        $image = @explode('filemanager/userfiles/', urldecode($request->get('image'.$key)))[1];
                                    } else {
                                        $image = urldecode(@$request->get('image'.$key));
                                    }
                                    $productAttr = ProductAttribute::updateOrCreate([
                                        'product_id' => $this->model->id,
                                        'properties_value_ids' => $properties_value_ids
                                    ], [
                                        'image' => $image,
                                        'final_price' => str_replace(',', '', $request->get('final_price'.$key))
                                    ]);
                                    $product_attribute_updated[] = $productAttr->id;
                                }
                            }
                            ProductAttribute::where('product_id', $this->model->id)->whereNotIn('id', $product_attribute_updated)->delete();
                        }

                        //  Lưu log
//                        $this->adminLog($request, $this->model, 'add');
                        $this->afterAddLog($request, $this->model);

                        //  Update sản phẩm liên quan
                        if (!empty($request->related_products)) {
                            foreach ($request->related_products as $related_product) {
                                $prd = Product::find($related_product);
                                if (empty($prd->related_products)) {

                                    $prd->related_products = '|' . $this->model->id . '|';
                                } else {
                                    $prd->related_products .= $this->model->id . '|';
                                }
                                $prd->save();
                            }
                        }

                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
//        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', $ex->getMessage());
//            return redirect()->back()->withInput();
//        }
    }

    public function appendData($request, $data)
    {

        return $data;
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);
        if (!is_object($item)) abort(404);

        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu mình tạo
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            if ($item->admin_id != \Auth::guard('admin')->user()->id) {
                abort(404);
            }
        }

        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('raovatproduct::edit')->with($data);
        } else if ($_POST) {
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ], [
                'name.required' => 'Bắt buộc phải nhập tên',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                //  Tùy chỉnh dữ liệu insert
                $data['price_intro'] = number_format($request->final_price, 0, '.', '.');

//                if ($request->has('multi_cat')) {
//                    $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
//                    $data['category_id'] = $request->multi_cat[0];
//                }

                if ($request->has('related_products')) {
                    $data['related_products'] = '|' . implode('|', $request->related_products) . '|';

                }
//
//                    if ($request->has('manufacture_id')) {
//                        $data['manufacture_id'] = '|' . implode('|', $request->manufacture_id) . '|';
//                    }
                if ($request->has('image_extra')) {
                    $data['image_extra'] = '|' . implode('|', $request->image_extra) . '|';

                }
                if ($request->has('related_post')) {
                    $data['related_post'] = '|' . implode('|', $request->related_post) . '|';
                }


                if ($request->has('image_extra')) {
                    $img_extra = '';
                    foreach ($request->image_extra as $img) {
                        if ($img != null && strpos($img, 'filemanager')) {
                            $img_extra .= (@explode('filemanager/userfiles/', $img)[1] . '|');
                        } elseif ($img != null) {
                            $img_extra .= $img . '|';
                        }
                    }
                    $data['image_extra'] = $img_extra;
                }
                $data = $this->appendData($request, $data);
                if (isset($data['error'])) {
                    return $this->returnError($data, $request);
                }
                if ($request->has('input_image_extra')) {
                    $data['input_image_extra'] = implode('|', $request->input_image_extra);
                }
                if ($request->has('proprerties_id')) {
                    $data['proprerties_id'] = '|' . implode('|', $request->proprerties_id) . '|';
                }
                #
//                if (!empty($data['final_price']) && !empty($data['base_price'])) {
//                    $data['sale'] = CEIL(($data['base_price'] - $data['final_price']) * 100 / $data['base_price']) . '%';
//                } elseif (empty($data['final_price']) || empty($data['base_price'])) {
//                    $data['sale'] = '0%';
//                } elseif (empty($data['final_price']) && empty($data['base_price'])) {
//                    $data['sale'] = '0%';
//                }

                $data['district_id'] = @$request->district_id;
                $data['ward_id'] = @$request->ward_id;

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    if (\Schema::hasTable('product_attributes')) {
                        //  Cập nhật attribute cho sản phẩm
                        $product_attribute_updated = [];
                        foreach ($request->all() as $k => $v) {
                            if (strpos($k, 'attributes') !== false) {
                                $key = str_replace('attributes', '', $k);
                                $properties_value_ids = '|' . implode('|', $request->get('attributes'.$key)) . '|';
                                if (strpos(@$request->get('image'.$key), 'filemanager')) {
                                    $image = @explode('filemanager/userfiles/', urldecode($request->get('image'.$key)))[1];
                                } else {
                                    $image = urldecode(@$request->get('image'.$key));
                                }
                                $productAttr = ProductAttribute::updateOrCreate([
                                    'product_id' => $item->id,
                                    'properties_value_ids' => $properties_value_ids
                                ], [
                                    'image' => $image,
                                    'final_price' => str_replace(',', '', $request->get('final_price'.$key))
                                ]);
                                $product_attribute_updated[] = $productAttr->id;
                            }
                        }
                        ProductAttribute::where('product_id', $item->id)->whereNotIn('id', $product_attribute_updated)->delete();
                    }

                    // admin log
//                    $this->adminLog($request, $item, 'edit');
                    CommonHelper::flushCache($this->module['table_name']);
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function returnError($data, $request)
    {
        CommonHelper::one_time_message('error', $data['msg']);
        return redirect()->back();
    }

    public function getPublish(Request $request)
    {
        try {

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            $this->adminLog($request, $item, 'publish');
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => $ex->getMessage(),
//                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu mình tạo
            if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
                if ($item->admin_id != \Auth::guard('admin')->user()->id) {
                    abort(404);
                }
            }

            $item->delete();
//            $this->adminLog($request, $item, 'delete');
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
//            $this->adminLog($request, $ids, 'multi_delete');
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }

            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function duplicate(Request $request, $id)
    {
        try {
            $item = $this->model->find($id);
            $new_item = $item->replicate();
            $new_item->admin_id = \Auth::guard('admin')->user()->id;
            $new_item->save();
            CommonHelper::one_time_message('success', 'Nhân bản thành công! Bạn đang ở bản ghi mới');
            return redirect('/admin/'.$this->module['code'].'/' . $new_item->id);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return redirect()->withInput();
        }
    }

    public function enabledStatus(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                foreach ($ids as $product) {
                    $product = $this->model->find($product);
                    $product->status = 1;
                    $product->save();
                }
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Đổi trang thái sang kích hoạt thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function disabledStatus(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                foreach ($ids as $product) {
                    $product = $this->model->find($product);
                    $product->status = 0;
                    $product->save();
                }
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Đổi trạng thái sang hủy kích hoạt thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

//    public function searchForSelect2(Request $request)
//    {
//        $data = $this->model->select([$request->col, 'id'])->where($request->col, 'like', '%' . $request->keyword . '%');
//
//        if ($request->where != '') {
//            $data = $data->whereRaw(urldecode(str_replace('&#039;', "'", $request->where)));
//        }
//        if (@$request->company_id != null) {
//            $data = $data->where('company_id', $request->company_id);
//        }
//        $data = $data->limit(5)->get();
//        return response()->json([
//            'status' => true,
//            'items' => $data
//        ]);
//    }

    public function ajaxGetHtmlPriceOption(Request $r) {
        return view('raovatproduct::partials.ajax_html_price_option')->render();
    }
}
