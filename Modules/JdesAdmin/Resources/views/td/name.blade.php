<a
        href="{{ url('/admin/profile/' . $item->id) }}"
        style="    font-size: 14px!important; ">{!! $item->{$field['name']} !!}</a>
<div class="row-actions" style="    font-size: 13px;">
    <span class="edit" title="ID của bản ghi">ID: {{ @$item->id }} | </span>
    <span class="edit"><a
                href="{{ url('/admin/profile/' . $item->id) }}"
                title="Sửa bản ghi này">Profile</a> | </span>
    @if(in_array($module['code'].'_delete', $permissions))
        <span class="trash"><a class="delete-warning"
                               href="{{ url('/admin/'.$module['code'].'/delete/' . $item->id) }}"
                               title="Xóa bản ghi">Xóa</a> | </span>
    @endif
</div>