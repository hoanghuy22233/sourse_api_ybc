<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::get('login-other-account', 'Admin\AdminController@loginOtherAccount')->middleware('permission:super_admin');
    Route::match(array('GET', 'POST'), 'profile', 'Admin\AdminController@profile')->name('admin.profile');
    Route::get( 'get-roles-company', 'Admin\AdminController@getHtmlRolesWithCompany');
    Route::group(['prefix' => 'admin'], function () {
        Route::get('', 'Admin\AdminController@getIndex')->name('admin')->middleware('permission:admin_company_view');
        Route::get('publish', 'Admin\AdminController@getCompanyPublish')->name('admin.company_publish')->middleware('permission:admin_company_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\AdminController@add')->middleware('permission:admin_company_add');
        Route::get('delete/{id}', 'Admin\AdminController@delete')->middleware('permission:admin_company_delete');
        Route::post('multi-delete', 'Admin\AdminController@multiDelete')->middleware('permission:admin_company_delete');

        Route::get('search-for-select2', 'Admin\AdminController@searchForSelect2')->name('admin.search_for_select2')->middleware('permission:admin_company_view');
        Route::get('search-for-select2-all', 'Admin\AdminController@searchForSelect2All')->middleware('permission:admin_company_view');

        Route::group(['prefix' => 'all'], function () {
            Route::get('', 'Admin\AdminController@all')->name('admin.all')->middleware('permission:super_admin');
            Route::get('publish', 'Admin\AdminController@getPublish')->name('admin.publish')->middleware('permission:admin_publish');
        });



        Route::get( '{id}', 'Admin\AdminController@update')->middleware('permission:admin_company_view');
        Route::post( '{id}', 'Admin\AdminController@update')->middleware('permission:admin_company_edit');
    });

    Route::match(array('GET', 'POST'), 'profile/{id}', 'Admin\AdminController@profileAdmin')->name('admin.profile_admin');

    Route::post('edit-single', 'Admin\AdminController@addSingleAdmin')->name('edit-single');
});

