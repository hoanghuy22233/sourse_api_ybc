<?php

namespace Modules\JdesAdmin\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class JdesAdminServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('JdesAdmin', 'Database/Migrations'));
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Custom setting
            $this->registerPermission();
        }
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['admin_view', 'admin_add', 'admin_edit', 'admin_delete', 'admin_publish',
                'admin_company_view', 'admin_company_edit', 'admin_company_delete']);
            return $per_check;
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('JdesAdmin', 'Config/config.php') => config_path('jdesadmin.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('JdesAdmin', 'Config/config.php'), 'jdesadmin'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/jdesadmin');

        $sourcePath = module_path('JdesAdmin', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/jdesadmin';
        }, \Config::get('view.paths')), [$sourcePath]), 'jdesadmin');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/jdesadmin');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'jdesadmin');
        } else {
            $this->loadTranslationsFrom(module_path('JdesAdmin', 'Resources/lang'), 'jdesadmin');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('JdesAdmin', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
