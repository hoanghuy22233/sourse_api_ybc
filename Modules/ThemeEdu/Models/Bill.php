<?php

namespace Modules\ThemeEdu\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bill extends Model
{
    use SoftDeletes;

    protected $table = 'bills';

    protected $guarded = [];

    protected $dates = ['deleted_at'];
    protected $softDelete = true;


    public function student() {
        return $this->belongsTo(Student::class, 'student_id');
    }
    public function orders() {
        return $this->hasMany(Order::class, 'bill_id','id');
    }

}
