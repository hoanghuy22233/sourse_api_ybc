<?php

namespace Modules\ThemeEdu\Models;


use App\Http\Helpers\CommonHelper;
use DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Modules\EduCourse\Models\Misson;
use Modules\EduCourse\Models\Quizzes;

class HistoryMisson extends Model
{
    protected $table = 'history_missons';
    protected $fillable = ['student_id', 'misson_id', 'accumulated_points', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }

    public function misson()
    {
        return $this->belongsTo(Misson::class, 'misson_id', 'id');
    }

}
