<?php

namespace Modules\ThemeEdu\Models;

use Illuminate\Database\Eloquent\Model;

class Center extends Model
{

    protected $table = 'centers';

    protected $guarded = [];

}
