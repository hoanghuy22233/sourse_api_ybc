<?php

namespace Modules\ThemeEdu\Models;

use App\Http\Helpers\CommonHelper;
use Illuminate\Database\Eloquent\Model;
use Modules\ThemeEduAdmin\Models\Category;

class Menu extends Model
{
    protected $table = 'menus';

    protected $fillable = ['name', 'type', 'slug', 'parent_id', 'order_no', 'location', 'categori_post_id',
        'tag_post_id', 'category_product_id', 'tag_product_id', 'product_id', 'status', 'created_at', 'updated_at'];

    public function post() {
        return $this->hasMany(Post::class, 'item_id', 'id');
    }

    public function category() {
        return $this->hasMany(Category::class, 'item_id', 'id');
    }
    public function parent()
    {
        return $this->hasOne($this, 'id', 'parent_id');
    }
    public function product() {
        return $this->hasMany(Product::class, 'item_id', 'id');
    }

    public function childs()
    {
        return $this->hasMany($this, 'parent_id', 'id')->where('status', 1)->orderBy('order_no', 'desc');
    }
}
