<?php

namespace Modules\ThemeEdu\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\ThemeEdu\Models\LessonItem;

class Lesson extends Model
{

    protected $table = 'lessons';

    protected $guarded = [];
    public $timestamps = false;

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
    public function lessonItem()
    {
        return $this->hasMany(LessonItem::class)->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }
    public function quiz()
    {
        return $this->hasMany(Quizzes::class, 'lesson_id', 'id')->orderBy('order_no', 'desc')->orderBy('id', 'asc');
    }
}
