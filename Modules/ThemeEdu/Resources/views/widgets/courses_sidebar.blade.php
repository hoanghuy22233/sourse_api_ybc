<?php
$where = @$config['where'];
$limit = @$config['limit'];
$category_id = @$config['category_id'];

$data = CommonHelper::getFromCache('courses_' . json_encode($config), ['courses']);
if (!$data) {
    $data = \Modules\ThemeEdu\Models\Course::select(['category_id', 'multi_cat', 'name', 'image', 'slug'])->where('status', 1);
    if ($where != '') {
        $data = $data->whereRaw($where);
    }
    if ($limit != '') {
        $data = $data->limit($limit);
    }
    if ($category_id != '') {
        $data = $data->where(function ($query) use ($category_id) {
            foreach (explode(',', $category_id) as $cat_id) {
                $query->orWhere('multi_cat', 'LIKE', '%|' . trim($cat_id) . '|%');
            }
        });
    }
    $data = $data->orderBy('order_no', 'desc')->orderBy('id', 'desc');
    $data = $data->get();
    CommonHelper::putToCache('courses_' . json_encode($config), $data, ['courses']);
}
?>
<ul class="recent-links">
    @foreach($data as $course)
        <li>
            <figure>
                <a title="{{ @$course->name }}" href="/{{ @$course->category->slug }}/{{ $course->slug }}.html">
                    <img alt="{{ @$course->name }}" class="lazy" data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($course->image,361,200) }}">
                </a>
            </figure>
            <div class="re-links-meta">
                <h6><a title="{{ @$course->name }}" href="/{{ @$course->category->slug }}/{{ $course->slug }}.html">{{ @$course->name }}</a></h6>
                <span><a title="{{ @$course->category->name }}" href="/{{ @$course->category->slug }}">{{ @$course->category->name }}</a></span>
            </div>
        </li>
    @endforeach
</ul>
