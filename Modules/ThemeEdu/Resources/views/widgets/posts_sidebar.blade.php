<?php
$where = @$config['where'];
$limit = @$config['limit'];
$category_id = @$config['category_id'];

$data = CommonHelper::getFromCache('posts_' . json_encode($config), ['posts']);
if (!$data) {
    $data = \Modules\ThemeEdu\Models\Post::select(['category_id', 'multi_cat', 'name', 'image', 'slug'])->where('status', 1);
    if ($where != '') {
        $data = $data->whereRaw($where);
    }
    if ($limit != '') {
        $data = $data->limit($limit);
    }
    if ($category_id != '') {
        $data = $data->where(function ($query) use ($category_id) {
            foreach (explode(',', $category_id) as $cat_id) {
                $query->orWhere('multi_cat', 'LIKE', '%|' . trim($cat_id) . '|%');
            }
        });
    }
    $data = $data->orderBy('order_no', 'desc')->orderBy('id', 'desc');
    $data = $data->get();
    CommonHelper::putToCache('posts_' . json_encode($config), $data, ['posts']);
}
?>
<ul class="recent-links">
    @foreach($data as $post)
        <li>
            <figure>
                <a title="{{ @$post->name }}" href="/{{ @$post->category->slug }}/{{ $post->slug }}.html">
                    <img alt="{{ @$post->name }}" class="lazy"
                         data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image,361,200) }}">
                </a>
            </figure>
            <div class="re-links-meta">
                <h6><a title="{{ @$post->name }}"
                       href="/{{ @$post->category->slug }}/{{ $post->slug }}.html">{{ @$post->name }}</a></h6>
                <span><a title="{{ @$post->category->name }}"
                         href="/{{ @$post->category->slug }}">{{ @$post->category->name }}</a></span>
            </div>
        </li>
    @endforeach
</ul>
