@extends('themeedu::layouts.default')
@section('khoahoc')
    active
@endsection
@section('main_content')
    <style>
        .contain {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .contain input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        .contain:hover input ~ .checkmark {
            background-color: #ccc;
        }

        .contain input:checked ~ .checkmark {
            background-color: #2196F3;
        }

        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        .contain input:checked ~ .checkmark:after {
            display: block;
        }

        .contain .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        .btn-complete {
            margin-bottom: 20px;
        }
    </style>
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>
        @include('themeedu::template.top_bar')
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themeedu::template.menu')
                                </div><!-- user profile banner  -->
                                <div class="col-lg-3">
                                    <aside class="sidebar static left">
                                        <div class="widget stick-widget">
                                            <h4 class="widget-title">Lesson</h4>
                                            <ul class="naves">
                                                <?php
                                                $course = \Modules\ThemeEdu\Models\Course::where('id', $lesson_item->course_id)->where('status', 1)->first();

                                                $student = @\Modules\ThemeEdu\Models\Route::where('student_id', @\Auth::guard('student')->user()->id)->where('lesson_item_id', $lesson_item->id)->first();

                                                $lessonitems = CommonHelper::getFromCache('lesson_item_lesson_id' . $lesson_item->lesson_id, ['lesson_items']);
                                                if (!$lessonitems) {
                                                    $lessonitems = \Modules\EduCourse\Models\LessonItem::where('lesson_id', $lesson_item->lesson_id)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();

                                                    CommonHelper::putToCache('lesson_item_lesson_id' . $lesson_item->lesson_id, $lessonitems, ['lesson_items']);
                                                }
                                                ?>
                                                @foreach($lessonitems as $k=>$v)
                                                    <?php
                                                        $check = \Modules\ThemeEdu\Models\Route::where('student_id', @\Auth::guard('student')->user()->id)->where('lesson_item_id', $v->id)->count();
                                                    ?>
                                                    <li>
                                                        @if($check > 0 || $k==0)
                                                            <a href="/khoa-hoc/{{@$course->slug}}/{{@$v->slug}}.html"
                                                               title="{{@$v->name}}">{{@$v->name}}</a>
                                                        @else
                                                            <a rel="nofollow" title="{{@$v->name}}">{{@$v->name}}</a>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div><!-- Shortcuts -->
                                    </aside>
                                </div><!-- sidebar -->
                                <div class="col-lg-9">
                                    <div class="single_lesson_content central-meta">
                                        <div class="single_lesson_content_inner"><h1
                                                    style="font-size: 35px;margin-top: 20px;">{{@$lesson_item->name}}</h1>
                                        </div>
                                        <div>
                                            @foreach($lessonitems as $v)
                                                <?php
                                                //                                                $toPic = CommonHelper::getFromCache('topic_by_lesson_item' . $lessonitem->id);
                                                //                                                if (!$toPic) {
                                                $toPic = $v->topic;
                                                //                                                    CommonHelper::putToCache('topic_by_lesson_item' . $lessonitem->id, $toPic);
                                                //                                                }
                                                ?>
                                                @foreach($toPic as $k => $toppic)
                                                    @if($toppic->lesson_item_id==$lesson_item->id&&$toppic->parent==0)
                                                        <div class="row">
                                                            <div class="col-md-9"><h2
                                                                        style="font-size: 27px;">{{@$toppic->name}}</h2>
                                                            </div>
                                                            <div class="col-md-3" style="padding-left:30px;">
                                                                <?php
                                                                //                                                                $student_topic = CommonHelper::getFromCache('routes_topic_id'.$toppic->id);
                                                                //                                                                if (!$student_topic) {
                                                                $student_topic = @\Modules\ThemeEdu\Models\Route::where('student_id', @\Auth::guard('student')->user()->id)->where('topic_id', $toppic->id)->first();
                                                                //
                                                                //                                                                    CommonHelper::putToCache('routes_topic_id'.$toppic->id, $student_topic);
                                                                //                                                                }
                                                                ?>
                                                                @if(empty($student_topic))
                                                                    <label class="contain comp-topic">Hoàn thành
                                                                        <input type="checkbox" name="complete"
                                                                               data-topic_id="{{$toppic->id}}">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                @else
                                                                    <label class="contain comp-topic">Hoàn thành
                                                                        <input type="checkbox" name="complete"
                                                                               data-topic_id="{{$toppic->id}} " checked>
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="su-list" style="margin-left:0px">
                                                                {!! @$toppic->content !!}
                                                            </div>
                                                            <?php
                                                            //                                                        $reCording = CommonHelper::getFromCache('recording_by_topic' . $toppic->id);
                                                            //                                                        if (!$reCording) {
                                                            $reCording = $toppic->recording;
                                                            //                                                            CommonHelper::putToCache('recording_by_topic' . $toppic->id, $reCording);
                                                            //                                                        }
                                                            ?>

                                                            @foreach($reCording as $k => $recording)
                                                                <div class="su-spoiler su-spoiler-style-glass-green su-spoiler-icon-plus-circle su-spoiler-closed">
                                                                    <div class="recording su-spoiler-title"
                                                                         tabindex="{{ $k }}"
                                                                         role="button"><span
                                                                                class=""></span><strong>{{@$recording->name}}</strong><i
                                                                                style="float: right; margin-top: 5px; margin-right: -23px;"
                                                                                class="fa fa-plus-circle"></i></div>
                                                                    <div class="spoiler" style="display: none">
                                                                        <div class=" su-spoiler-content su-clearfix">
                                                                            <div class="audiofv">
                                                                                <div name="fwdmspPlaylist" data-plid="3"
                                                                                     style="background-color: #ffffff;"
                                                                                     data-playlistid="3"
                                                                                     class="VittiPlayer3"
                                                                                     onclick="testPass(fwdmspPlayer0, 'VittiPlayer3')"
                                                                                     data-player-id="0">
                                                                                    <table style="border:none;padding:0 0 0 25px">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td class="leftPlayer"
                                                                                                width="64px"
                                                                                                style="border:none">
                                                                                            </td>
                                                                                            <td class="rightPlayer"
                                                                                                valign="top"
                                                                                                style="vertical-align:top;border:none">
                                                                                                <div class="controlPlayer">

                                                                                                    @if(!empty($recording->file_audio))
                                                                                                        <audio controls>
                                                                                                            <source src="{{ asset('public/filemanager/userfiles/' . $recording->file_audio) }}"
                                                                                                                    type="audio/mpeg">
                                                                                                            Your browser
                                                                                                            does
                                                                                                            not support
                                                                                                            the
                                                                                                            audio
                                                                                                            element.
                                                                                                        </audio>
                                                                                                        <!--marquee-->
                                                                                                        <div class="titlePlayer">
                                                                                                            <span class="titlePlayerSpan">{{ $recording->file_audio_name != '' ? $recording->file_audio_name : @$lesson_item->name }}</span>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                    <div class="desPlayer">
                                                                                                        <span class="desPlayerSpan">{{ $recording->file_audio_intro != '' ? $recording->file_audio_intro : @$val->name }}</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>

                                                                                </div>
                                                                            </div>

                                                                            {!! @$recording->content !!}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            @endforeach

                                                            <div class="audiofv"></div>

                                                            <div></div>
                                                        </div>
                                                    @elseif($toppic->lesson_item_id==$lesson_item->id&&$toppic->parent==1)

                                                        <hr>
                                                        <div><h2 style="font-size: 31px;">{{@$toppic->name}}</h2></div>
                                                        <br>

                                                    @endif
                                                @endforeach
                                            @endforeach

                                        </div>
                                        <?php
                                        //                                    $lessonitem_next = CommonHelper::getFromCache('lesson_item_lesson_id'.$lessonitem->lesson_id.$lessonitem->id.$lessonitem->order_no);
                                        //                                    if (!$lessonitem_next) {
                                        $lessonitem_next = \Modules\EduCourse\Models\LessonItem::where('lesson_id', $lesson_item->lesson_id)->where('id', '>', $lesson_item->id)->where('order_no', '<', $lesson_item->order_no)->first();
                                        //
                                        //                                        CommonHelper::putToCache('lesson_item_lesson_id'.$lessonitem->lesson_id.$lessonitem->id.$lessonitem->order_no, $lessonitem_next);
                                        //                                    }

                                        //                                    $lessonitem_id_max = CommonHelper::getFromCache('lesson_item_lesson_id_max_id'.$lessonitem->lesson_id);
                                        //                                    if (!$lessonitem_id_max) {
                                        $lessonitem_id_max = \Modules\EduCourse\Models\LessonItem::where('lesson_id', $lesson_item->lesson_id)->max('id');
                                        //
                                        //                                        CommonHelper::putToCache('lesson_item_lesson_id_max_id'.$lessonitem->lesson_id, $lessonitem_id_max);
                                        //                                    }

                                        //                                    $lessonitem_order_min = CommonHelper::getFromCache('lesson_item_lesson_id_min_order_no'.$lessonitem->lesson_id);
                                        //                                    if (!$lessonitem_order_min) {
                                        $lessonitem_order_min = \Modules\EduCourse\Models\LessonItem::where('lesson_id', $lesson_item->lesson_id)->min('order_no');
                                        //
                                        //                                        CommonHelper::putToCache('lesson_item_lesson_id_min_order_no'.$lessonitem->lesson_id, $lessonitem_order_min);
                                        //                                    }
                                        $course = \Modules\ThemeEdu\Models\Course::where('id', $lesson_item->course_id)->first();
                                        ?>

                                        @if(!isset($student))
                                            <button class="hoan-thanh btn btn-info btn-complete"
                                                    data-lesson_item_id="{{$lesson_item->id}}">Hoàn thành
                                            </button>
                                        @elseif($lesson_item->id==$lessonitem_id_max||$lesson_item->order_no==$lessonitem_order_min)
                                            <button class="btn btn-success btn-complete">Đã hoàn thành</button>
                                        @else
                                            <button class="btn btn-success btn-complete">Đã hoàn thành</button>
                                            <a href="/khoa-hoc/{{$course->slug}}/{{@$lessonitem_next->slug}}.html"
                                               class="btn btn-success" style="margin-top: -2.5%;">Bài
                                                tiếp theo </a>
                                        @endif
                                    </div><!-- centerl meta -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>

    @if(\Auth::guard('student')->check())
        <script>
            $("input[type=checkbox]").click(function () {
                var topic_id = $(this).data('topic_id');
                var that = $(this).prop('checked');
                if (that === false) {
                    alert('Bạn đã hoàn thành bước này rồi, hãy học tiếp bước học kế tiếp nhé');
                    $(this).prop('checked', true);
                } else {
                    $.ajax({
                        url: '{{route('topic')}}',
                        type: 'get',
                        data: {
                            topic_id: topic_id,
                        },
                        // success: function (data) {
                        //     if (data.status) {
                        //         alert(data.msg);
                        //         // location.reload();
                        //     } else {
                        //         alert('Có lỗi xảy ra. Vui lòng load lại website và thử lại!');
                        //     }
                        // }
                    });
                }
            });
        </script>
    @endif
    <script>
        $('.hoan-thanh').click(function () {
            var lesson_item_id = $(this).data('lesson_item_id');
            var x = $("input[type=checkbox]").length;
            var y = $("input[type=checkbox]:checked").length;
            if (x === y) {
                $.ajax({
                    url: '{{route('route')}}',
                    type: 'get',
                    data: {
                        lesson_item_id: lesson_item_id,

                    },
                    success: function (data) {
                        if (data.status) {
                            alert(data.msg);
                            location.reload();
                        } else {
                            alert('Có lỗi xảy ra. Vui lòng load lại website và thử lại!');
                        }
                    }
                });
            } else {
                alert('Bạn chưa hoàn thành hết các bước');
            }
        });
    </script>

    <script>
        $(document).ready(function () {
            $(".recording").click(function () {
                $(this).parents('.su-spoiler').find(".spoiler").toggle();
            });
        });
    </script>
@endsection
@section('head_script')
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/course_text_audio.css') }}?v={{ time() }}">
@endsection