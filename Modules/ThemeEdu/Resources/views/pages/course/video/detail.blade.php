<html>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v6.0&appId=1442327099412363&autoLogAppEvents=1"></script>
<head>
    @include('themeedu::partials.head_meta')
    <style class="vjs-styles-defaults">
        .video-js {
            width: 300px;
            height: 150px;
        }

        .vjs-fluid {
            padding-top: 56.25%
        }
    </style>
    <style class="vjs-styles-dimensions">
        .myPlayerID-dimensions {
            width: 854px;
            height: 479.99999999999994px;
        }

        .myPlayerID-dimensions.vjs-fluid {
            padding-top: 56.20608899297424%;
        }
    </style>
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/font-awesome.css') }}">
    {{--<link href="https://unica.vn/media/styles_v2018/bootstrap.css" rel="stylesheet">--}}
    {{--<link href="https://unica.vn/media/styles_v2018/font-awesome.css" rel="stylesheet">--}}
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/main.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/player.css') }}">
    {{--<link href="media/styles_v2018/main.css" rel="stylesheet">--}}
    {{--<link href="media/styles_v2018/player.css" rel="stylesheet">        --}}
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    {{--<script type="text/javascript" src="https://static.unica.vn/media/js_v2017/jquery-3.2.1.min.js"></script>--}}
    <script src="{{ URL::asset('public/frontend/themes/edu/js/jquery-3.2.1.min.js') }}"></script>
</head>
<body style="transition: background-color .5s;" class="">
<div class="container-fluid">
    <div class="row">
        <div id="mySidenav" class="sidenav" auth-lesson="777" auth-course="75" style="width: 320px;">
            <span class="u-video-course-title"> <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a></span>
            <div class="u-video-list-course">
                <div class="content">
                    <div class="panel-group" id="accordion">
                        <?php
                        $order = \Modules\ThemeEdu\Models\Order::where('student_id', @\Auth::guard('student')->user()->id)->where('course_id', $courses->id)->first();
                        ?>
                        @foreach(@$courses->lesson as $k=>$lesson)
                            <div class="panel panel-default">
                                <!-- phần -->
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="#collapse1" class="tit-bold"> {{$lesson->name}}</a>
                                    </h4>
                                </div>
                                <!-- bài -->
                                @if(isset($lesson->lessonitem))
                                    <div>
                                        <div class="panel-body">
                                            <span class="alert alert-danger" style="display: inline-block;">Nếu không phát video được bạn nãy mở trình duyệt ẩn danh (tổ hợp phím ctrl + shift + n) để xem nhé</span>
                                            @foreach(@$lesson->lessonitem as $i => $lessonItem)
                                                <div class="col" id="learn-lesson-{{$lessonItem->id}}">
                                                    <div class="title">
                                                        @if(@$order->status == 1)
                                                            <a href="{{asset('khoa-hoc/'.$courses->slug.'/'.$lessonItem->slug.'.html')}}">
                                                                <i class="fa fa-play-circle"
                                                                   aria-hidden="true"
                                                                   style="color: {{$lesson_item->slug==$lessonItem->slug?'green':''}};"></i> {{$lessonItem->name}}
                                                            </a>
                                                        @else
                                                            @if($lessonItem->publish == 1)
                                                                <i class="fa fa-play-circle"
                                                                   aria-hidden="true"
                                                                   style="color: {{$lesson_item->slug==$lessonItem->slug?'green':''}};"></i> {{$lessonItem->name}}
                                                                <a href="{{asset('khoa-hoc/'.$courses->slug.'/'.$lessonItem->slug.'.html')}}"
                                                                   class="btn-sm btn-success">Học thử</a>
                                                            @else

                                                                <i class="fa fa-play-circle"
                                                                   aria-hidden="true"
                                                                   style="color: {{$lesson_item->slug==$lessonItem->slug?'green':''}};"></i> {{$lessonItem->name}}
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="unica-learn-page" id="main">
            <div class="unica-learn-top">
                <div class="unica-list-video-course" onclick="openNav()">
                    <div class="u-btn-list">
                        <span><i class="fa fa-list-ol" aria-hidden="true"></i></span>
                    </div>
                    <div class="u-txt-list">
                        <p>{{$lesson_item->name}}</p>
                        <span>Danh sách bài học</span>
                        <!--                                <div style="font-size: 12px;">-->
                        <!--                                    Vì lý do mạng Internet chậm nên Unica tạm thời chỉ phát hành video 480P để đảm bảo tất cả học viên có thể xem được bình thường.-->
                        <!--                                    <br> Hết thời gian cách ly, Unica sẽ mở lại video HD. Mong quý học viên thông cảm.-->
                        <!--                                </div>-->
                    </div>
                </div>
                <div class="unica-back-course">
                    <div class="u-txt-back">
                        <div class="pull-right">
                            <a href="{{asset('khoa-hoc/'.$courses->slug.'.html')}}">Về khóa học</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="unica-learn-video">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/video-js.css') }}">
                <link rel="stylesheet"
                      href="{{ URL::asset('public/frontend/themes/edu/css/video-quality-selector.css') }}">
            {{--<link href="media/js/video-js.css" rel="stylesheet">--}}
            {{--<link href="media/js/video-quality-selector.css" rel="stylesheet">--}}
            <!--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="http://videojs.github.io/videojs-contrib-hls/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
-->
                <script src="{{ URL::asset('public/frontend/themes/edu/js/ie10-viewport-bug-workaround.js') }}"></script>
            {{--<script src="media/js/ie10-viewport-bug-workaround.js"></script>--}}

            <!-- [if lt IE 9]> -->
                <!--
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                -->
                <script src="{{ URL::asset('public/frontend/themes/edu/js/video.js') }}"></script>
                <script src="{{ URL::asset('public/frontend/themes/edu/js/videojs-contrib-hls.js') }}"></script>
                <script src="{{ URL::asset('public/frontend/themes/edu/js/videojs-ie8.min.js') }}"></script>
                <script src="{{ URL::asset('public/frontend/themes/edu/js/videojs-resolution-switcher.js') }}"></script>
                {{--<script src="media/js/video.js"></script>--}}
                {{--<script src="media/js/videojs-contrib-hls.js"></script>--}}
                {{--<script src="media/js/videojs-ie8.min.js"></script>--}}
                {{--<script src="media/js/videojs-resolution-switcher.js"></script>--}}

                {{--<link href="media/js/video-js.min.css" rel="stylesheet">--}}
                {{--<link href="media/js/videojs-resolution-switcher.css" rel="stylesheet">--}}
                <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/video-js.min.css') }}">
                <link rel="stylesheet"
                      href="{{ URL::asset('public/frontend/themes/edu/css/videojs-resolution-switcher.css') }}">
                <style>
                    #player_videojs iframe {
                        width: 100% !important;
                        height: 100% !important;
                    }

                    .navbar-toggle .icon-bar {
                        background-color: #868688;
                    }

                    .starter-template {
                        padding: 40px 15px;
                        text-align: center;
                    }

                    .video-js {
                        margin: 0 auto;
                    }

                    #player_ok {
                        position: relative;
                        width: 100%;
                    }

                    #myPlayerID {
                        margin-top: 10px;
                        height: 93%;
                        /*height: -webkit-fill-available;*/
                    }

                    @media only screen and (max-device-width: 414px) {
                        #myPlayerID {
                            height: 90%;
                        }
                    }

                    .quality {
                        position: absolute;
                        bottom: 32px;
                        right: 1px;
                        text-align: center;
                        color: #fff;
                        float: right;
                        z-index: 9999;
                        width: 120px;
                    <!-- background-color: #73859f;
                    background-color: rgba(115, 133, 159, 0.5);
                    --> height: auto;
                        display: block;
                        background-color: #313131;
                    }

                    .quality .hide {
                        display: none;
                    }

                    .quality .show {
                        display: block;
                    }

                    .quality a {
                        color: #c3c3c3;
                        display: block;
                        text-decoration: none;
                        width: 100%;
                        padding: 5px 0;
                        text-align: right;
                        font-size: 13px;
                        font-weight: 600;
                        line-height: 25px;
                        border-bottom: 1px solid #3e3e3e;
                        padding-right: 10px;
                    }

                    .quality a i {
                        float: left;
                        line-height: 25px;
                        margin-left: 10px;
                    }

                    .quality a:last-child {
                        border-bottom: none;
                    }

                    .quality a:hover {
                        color: #d62525;
                        background: #1b1b1b;
                    }

                    .quality a.active {
                        background: #a17474;
                    }

                    #example-video {
                        width: 100% !important;
                    }

                    @media screen and (max-width: 480px) {
                        /*        #player_videojs{
                                    width:100%;
                                    height:350px;
                                    position:relative;

                                }*/
                    }

                    @media screen and (min-width: 1020px) {
                        #player_videojs {
                            width: 100%;
                            height: 100% !important;
                            position: relative;
                            margin: -15px auto !important;
                        }
                    }

                    @media (min-width: 768px) and (max-width: 991px) {
                        .vjs-control-bar {
                            display: flex !important;
                            margin-bottom: 50px;
                            z-index: 2;
                        }

                        #myPlayerID {
                            height: -webkit-fill-available;
                        }
                    }

                    @media (max-width: 767px) {
                        .vjs-control-bar {
                            display: flex !important;
                            margin-bottom: 66px;
                            z-index: 2;
                        }

                        .ua-btn-bn a {
                            padding: 3px;
                        }

                        .ua-btn-bn a:last-child {
                            padding: 3px;
                        }

                        .ua-btn-sv button {
                            padding: 3px;
                        }

                        .ua-btn-qa-bm a {
                            padding: 3px;
                        }

                        .ua-btn-ap-rp a {
                            padding: 3px;
                        }
                    }

                    #quality_op {
                        position: absolute;
                        bottom: 40px;
                        right: 40px;
                    }

                    #myPlayerID {
                        width: 100%;

                    }

                    #video_1 {
                        width: 100%;
                        height: 100%;
                    }

                    .vjs-custom-control-spacer, .vjs-spacer {
                        /*background: url(https://unica.vn/media/images/hd.png) no-repeat !important;*/
                        background: url(http://trungtam.webhobasoft.com/public/filemanager/userfiles/hd.png) no-repeat !important;
                        width: 22px !important;
                        height: 21px !important;
                        display: block !important;
                        margin: 4px 12px !important;
                        cursor: pointer !important;
                    }
                </style>
                <input type="hidden" id="course_id_hidden" value="75">
                <input type="hidden" id="lession_id_hidden" value="777">
                <div id="page-video-jwplayer"></div>
                <script>
                    function change(q) {
                        switch (q) {
                            case 240:
                                $('.quality a.240 i').addClass('show').removeClass('hide');
                                $('.quality a.360 i').removeClass('show').addClass('hide');
                                $('.quality a.480 i').removeClass('show').addClass('hide');
                                $('.quality a.720 i').removeClass('show').addClass('hide');
                                $('.quality a.auto i').removeClass('show').addClass('hide');
                                break;
                            case 360:
                                $('.quality a.360 i').addClass('show').removeClass('hide');
                                $('.quality a.240 i').removeClass('show').addClass('hide');
                                $('.quality a.480 i').removeClass('show').addClass('hide');
                                $('.quality a.720 i').removeClass('show').addClass('hide');
                                $('.quality a.auto i').removeClass('show').addClass('hide');
                                break;
                            case 480:
                                $('.quality a.480 i').addClass('show').removeClass('hide');
                                $('.quality a.240 i').removeClass('show').addClass('hide');
                                $('.quality a.360 i').removeClass('show').addClass('hide');
                                $('.quality a.720 i').removeClass('show').addClass('hide');
                                $('.quality a.auto i').removeClass('show').addClass('hide');
                                break;
                            case 720:
                                $('.quality a.720 i').addClass('show').removeClass('hide');
                                $('.quality a.240 i').removeClass('show').addClass('hide');
                                $('.quality a.360 i').removeClass('show').addClass('hide');
                                $('.quality a.480 i').removeClass('show').addClass('hide');
                                $('.quality a.auto i').removeClass('show').addClass('hide');
                                break;
                            case auto:
                                $('.quality a.auto i').addClass('show').removeClass('hide');
                                $('.quality a.240 i').removeClass('show').addClass('hide');
                                $('.quality a.360 i').removeClass('show').addClass('hide');
                                $('.quality a.480 i').removeClass('show').addClass('hide');
                                $('.quality a.720 i').removeClass('show').addClass('hide');
                                break;
                        }
                    }
                </script>

                <div id="player_videojs">
                    <div controlslist="nodownload"
                         data-setup="{&quot;playbackRates&quot;: [0.5,0.75,0.85,1,1.25,1.5,2 ]}" preload="auto"
                         autoplay="true"
                         class="video-js vjs-default-skin vjs-controls-enabled vjs-workinghover vjs-has-started myPlayerID-dimensions vjs-user-active vjs-paused"
                         data-embed="default" id="myPlayerID" onmouseleave="$('.quality').fadeOut(3200);" ref="video"
                         data-attr="https://stream.unica.vn/unica/_definst_//unica/2016/07/2lm2vt9uysy/iv_480_2lm2vt9uysybai11.mp4/playlist.m3u8?wowzatokenendTime=1588757292&amp;wowzatokenstartTime=1588750092&amp;wowzatokenuserId=390342&amp;wowzatokenhash=QnWNjgZ5B-cySu6jDg9bwBboZHEUHyREJh-fKnA424w="
                         role="region" aria-label="video player">
                        {!! $lesson_item->driver_video !!}
                    </div>
                </div>
                <script type="text/javascript">
                    var Player;
                    var myPlayer;
                    var duration;
                    var i = 0;
                    change720;
                    change480;
                    change360;
                    change240;
                    changeauto;
                    var real_duration = 341;
                    var path = '/learn/75/lecture/777';
                    path = path.split('/');
                    var hls_url = [
                        {
                            src: "https://stream.unica.vn/unica/_definst_//unica/2016/07/2lm2vt9uysy/iv_240_2lm2vt9uysybai11.mp4/playlist.m3u8?wowzatokenendTime=1588757292&wowzatokenstartTime=1588750092&wowzatokenuserId=390342&wowzatokenhash=IN-TLJQRAQRrqln4epDzkZ_9KaDU7WhJSMoIRmixIQc=",
                            type: "application/x-mpegURL",
                            label: "240",
                            res: "240"
                        },
                        {
                            src: "https://stream.unica.vn/unica/_definst_//unica/2016/07/2lm2vt9uysy/iv_3602lm2vt9uysybai11.mp4/playlist.m3u8?wowzatokenendTime=1588757292&wowzatokenstartTime=1588750092&wowzatokenuserId=390342&wowzatokenhash=UZYDVCQmMNAzgWWqSkPbaW9KsAfko9l15doNQ3OUMXo=",
                            type: "application/x-mpegURL",
                            label: "360",
                            res: "360"
                        },
                        {
                            src: "https://stream.unica.vn/unica/_definst_//unica/2016/07/2lm2vt9uysy/iv_480_2lm2vt9uysybai11.mp4/playlist.m3u8?wowzatokenendTime=1588757292&wowzatokenstartTime=1588750092&wowzatokenuserId=390342&wowzatokenhash=QnWNjgZ5B-cySu6jDg9bwBboZHEUHyREJh-fKnA424w=",
                            type: "application/x-mpegURL",
                            label: "480",
                            res: "480"
                        },
                        {
                            src: "https://stream.unica.vn/unica/_definst_//unica/2016/07/2lm2vt9uysy/iv_720_2lm2vt9uysybai11.mp4/playlist.m3u8?wowzatokenendTime=1588757292&wowzatokenstartTime=1588750092&wowzatokenuserId=390342&wowzatokenhash=SGhezw-XlYEQjTulVTTmOjONYfHAkqpZ2YmNlTPYX3M=",
                            type: "application/x-mpegURL",
                            label: "720",
                            res: "720"
                        }
                    ]

                    videojs("myPlayerID").ready(function () {
                        myPlayer = this;
                        duration = myPlayer.duration();
                        this.on('progress', function () {
                            i++;
                            if (i < 15) {
                                let current = myPlayer.currentTime();
                                myPlayer.currentTime(current + 0.15);
                            }
                        });
                        this.on('error', function () {
                            let codeError = this.error().code;
                            let current = myPlayer.currentTime();
                            if (codeError == 4) {
                                change480();
                            } else {
                                myPlayer.currentTime(current + 1);
                            }

                        });
                    });
                    videojs('myPlayerID', {
                        controls: true,
                        plugins: {
                            videoJsResolutionSwitcher: {
                                dynamicLabel: true
                            }
                        }
                    }, function () {
                        var player = this;
                        window.player = player;
                        player.on('resolutionchange', function () {

                        });
                    });

                    function change720() {
                        myPlayer.src({
                            "type": "application/x-mpegURL",
                            "src": "https://stream.unica.vn/unica/_definst_//unica/2016/07/2lm2vt9uysy/iv_720_2lm2vt9uysybai11.mp4/playlist.m3u8?wowzatokenendTime=1588757292&wowzatokenstartTime=1588750092&wowzatokenuserId=390342&wowzatokenhash=SGhezw-XlYEQjTulVTTmOjONYfHAkqpZ2YmNlTPYX3M="
                        });
                        setQuantity('720');
                        myPlayer.play();
                    }

                    function change480() {
                        myPlayer.src({
                            "type": "application/x-mpegURL",
                            "src": "https://stream.unica.vn/unica/_definst_//unica/2016/07/2lm2vt9uysy/iv_480_2lm2vt9uysybai11.mp4/playlist.m3u8?wowzatokenendTime=1588757292&wowzatokenstartTime=1588750092&wowzatokenuserId=390342&wowzatokenhash=QnWNjgZ5B-cySu6jDg9bwBboZHEUHyREJh-fKnA424w="
                        });
                        setQuantity('480');
                        myPlayer.play();
                    }

                    function change360() {
                        myPlayer.src({
                            "type": "application/x-mpegURL",
                            "src": "https://stream.unica.vn/unica/_definst_//unica/2016/07/2lm2vt9uysy/iv_3602lm2vt9uysybai11.mp4/playlist.m3u8?wowzatokenendTime=1588757292&wowzatokenstartTime=1588750092&wowzatokenuserId=390342&wowzatokenhash=UZYDVCQmMNAzgWWqSkPbaW9KsAfko9l15doNQ3OUMXo="
                        });
                        setQuantity('360');
                        myPlayer.play();
                    }

                    function change240() {
                        myPlayer.src({
                            "type": "application/x-mpegURL",
                            "src": "https://stream.unica.vn/unica/_definst_//unica/2016/07/2lm2vt9uysy/iv_240_2lm2vt9uysybai11.mp4/playlist.m3u8?wowzatokenendTime=1588757292&wowzatokenstartTime=1588750092&wowzatokenuserId=390342&wowzatokenhash=IN-TLJQRAQRrqln4epDzkZ_9KaDU7WhJSMoIRmixIQc="
                        });
                        setQuantity('240');
                        myPlayer.play();
                    }

                    function changeauto() {
                        myPlayer.src({
                            "type": "application/x-mpegURL",
                            "src": "https://stream.unica.vn/unica/_definst_//unica/2016/07/2lm2vt9uysy/w_2lm2vt9uysybai11.smil/playlist.m3u8"
                        });
                        myPlayer.play();
                    }

                    var interval = 1000;

                    function executeQuery() {
                        var isPaused = myPlayer.paused();
                        if (!isPaused) {
                            //if(myPlayer.currentTime())
                            setTimeout(executeQuery, interval); // you could choose not to continue on failure...
                        }
                    }

                    setInterval(function () {
                        Player = videojs('myPlayerID');
                        var whereYouAt = Player.currentTime();
                        var minutes = Math.floor(whereYouAt / 60);
                        var seconds = Math.floor(whereYouAt);
                        oldLearned = getLocalStorage('oldLearned', 'object');
                        if (oldLearned != null) {
                            oldLearned.time = $('.vjs-progress-holder').attr('aria-valuetext');
                            setLocalStorage('oldLearned', oldLearned);
                        }

                        //==========================================
                        secondsMem = getLocalStorage('secondsMem', 'object');
                        if (!myPlayer.paused() && whereYouAt > 0) {
                            secondsMem.seconds++;
                        }
                        setLocalStorage('secondsMem', secondsMem);
                        var y = seconds < 10 ? "0" + seconds : seconds;
                        if (typeof autoplay != 'undefined' && autoplay) {
                            if (real_duration - y <= 3) {
                                var link = $('#next_lesson').attr('href');
                                if (link) {
                                    var check = $('input[name=checkPassed]').val();
                                    if (check == 1) {
                                        $('.modal-rating').modal('show');
                                        $('.modal-rating').on('hidden.bs.modal', function () {
                                            $('.modal-rating').modal('hide');
                                            location.href = link;
                                        });
                                    } else {
                                        location.href = link;
                                    }

                                } else {
                                    location.href = '/learn/' + 75 + '/finish';
                                }

                            }
                        }
                    }, 1000);

                    $('body').mouseenter(function () {
                        $('.vjs-control-bar').removeAttr('style');
                    }).mouseleave(function () {
                        $('.vjs-control-bar').hide();
                    });

                    $('#mySidenav').mouseenter(function () {
                        $('.vjs-control-bar').hide();
                    }).mouseleave(function () {
                        $('.vjs-control-bar').removeAttr('style');
                    });

                    $(document).ready(function () {
                        $('.quality a.480 i').addClass('show').removeClass('hide');
                        $('.quality').hide();
                        b = true;
                        $('.vjs-spacer').click(function () {
                            if (b) {
                                $('.quality').show().animate({'bottom': '60px', 'opacity': '1'});
                                b = false;
                            } else {
                                $('.quality').stop().animate({'bottom': '60px', 'opacity': '0'});
                                b = true;
                            }
                        });
                    });

                    function readCookieObj(name) {
                        var result = document.cookie.match(new RegExp(name + '=([^;]+)'));
                        result && (result = JSON.parse(result[1]));
                        return result;
                    }

                    function removeCookie(name) {
                        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
                    }

                    function setCookie(name, value, days) {
                        var expires;
                        if (days) {
                            var date = new Date();
                            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                            expires = "; expires=" + date.toGMTString();
                        } else {
                            expires = "";
                        }
                        document.cookie = name + "=" + value + expires + "; path=/";
                    }

                    function setQuantity(quantity) {
                        setCookie('quantity', 'quantity-' + quantity);
                    }

                    $(document).ready(function () {
                        /**
                         * Set bookmarker for videojs SBD
                         */

                        var getAllMakers = function () {
                            $.ajax({
                                url: '/create/getbookmark',
                                type: 'POST',
                                dataType: 'json',
                                data: {
                                    course_id: '75',
                                    lession_id: '777',
                                },
                                success: function (res) {
                                    bindMakers(res.data);
                                }
                            });
                        }

                        getAllMakers();

                        var bindMakers = function (markers) {
                            $('.vjs-progress-holder').append(markers);
                        };

                        $('.btn-content-lession').on('click', function () {
                            Player.pause();
                            $('#modalContentLession').click();
                            var heightScreen = $(window).height();
                            $('.modal-diablog-content-lession .modal-content .modal-body').height(heightScreen * 90 / 100);
                        });

                    });
                    //==================================END=================================//

                </script>
                <div class="unica-learn-bottom">
                    <div class="u-area-btn" style="margin-left: 15px; margin-right: 5px;">
                        <div class="row" style="background: #2c2f37 !important;">
                            <?php
                            $lesson_item_next = \Modules\ThemeEdu\Models\LessonItem::where('lesson_id', $lesson_item->lesson_id)->where('order_no', '<=', $lesson_item->order_no)
                                ->where('id', '!=', $lesson_item->id)->orderBy('order_no', 'DESC')->orderBy('id', 'ASC')->first();
                            ?>
                            @if(is_object($lesson_item_next))
                                <div class="ua-btn-bn col-lg-3">
                                    <a id="next_lesson"
                                       href="/khoa-hoc/{{@$courses->slug}}/{{@$lesson_item_next->slug}}.html"><p>Bài
                                            sau</p> <i
                                                class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                </div>
                            @endif
                            <div class="ua-btn-qa-bm col-lg-5">
                                <a href="javascript:void(0)" id="btn-note" class="btn-note" data-toggle="modal"
                                   data-target="#myModal" data-course-id="75" data-lession-id="777">
                                    <p> Ghi chép </p><i class="fa fa-edit" aria-hidden="true"></i></a>
                                <a href="javascript:void(0)" class="btn-qa" onclick="openNav2()">
                                    <p> Thảo luận </p><i class="fa fa-question-circle" aria-hidden="true"></i></a>
                                <a href="javascript:void(0)" class="btn-file-lession hidden-xs" data-toggle="modal"
                                   data-target="#fileLession">
                                    <p>Tài liệu</p> <i class="fa fa-file" aria-hidden="true"></i></a>
                            </div>
                            <div class="ua-btn-ap-rp col-lg-3">
                                <a href="javascript:void(0)" class="btn-ap">
                                    <p>Autoplay</p> <i id="autoplay" class="fa fa-toggle-off" aria-hidden="true"></i>
                                </a>
                                <a style="cursor: pointer;" class="bao-loi btn-rp ">
                                    Báo lỗi <i class="fa fa-exclamation-circle" ></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="mySidenav2" class="sidenav2" style="width: 0px;">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav2()">×</a>
            <span class="u-video-qa-title"> <i class="fa fa-question-circle" aria-hidden="true"></i> Đặt câu hỏi</span>
            <div class="u-video-qa-block">
                <div class="fb-comments"
                     data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"
                     data-numposts="5" data-width="100%"></div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h3 class="modal-title"><label for="">Ghi chép</label></h3>
            </div>
            <div class="form-create-note col-md-12">
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <input placeholder="Ghi chép của bạn..." type="text" class="note-new-message form-control"
                                   name="message" required="">
                        </div>
                    </div>
                    <div class="col-md-3 note-time hidden" style="display: none;">
                        <div class="form-group">
                            <input placeholder="VD 1:25 " type="text" class="note_time form-control hidden"
                                   name="note_time" required="" data-lession-id="777" data-course-id="75">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" style="padding-top: 4px;">
                            <button type="submit" class="ghi-chep btn btn-info btn-save-note" data-lesson_item_id="{{$lesson_item->id}}">Lưu</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body modal-lession-notes">
<?php
$notes=\Modules\ThemeEdu\Models\Note::where('student_id',@\Auth::guard('student')->user()->id)->where('lesson_item_id',@$lesson_item->id)->get();
?>
                @foreach($notes as $note)
                <p class="text-note-lession">
                    {{date('d/m/Y - H:i:s',strtotime(@$note->created_at))}}        <b class="text-content-note">
                        <span class="text-string">- {{@$note->note}}</span>
                    </b>
                </p>
@endforeach
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
<div id="contentLession" class="modal fade " role="dialog">
    <div class="modal-dialog modal-diablog-content-lession">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn-close-modal btn-close-modal-content" data-dismiss="modal">
                    <b>X</b></button>
                <h3 class="modal-title"><label for="">Nội dung của bài học</label></h3>
            </div>
            <div class="modal-body">
            </div>
        </div>

    </div>
</div>
<div id="fileLession" class="modal fade" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn-close-modal" data-dismiss="modal">×</button>
                <h3 class="modal-title"><label for="">Tài liệu của bài học</label></h3>
            </div>
            <div class="modal-body" style="height: 500px;overflow: scroll;">
                <?php
                $course = \Modules\ThemeEdu\Models\Course::where('id', $lesson_item->course_id)->first();
                $document_course = json_decode($course->document);
                ?>
                @if(!empty($document_course))
                    @foreach($document_course as $k => $value)
                        <p>{{$k+=1}}.{{$value->names}}: <a href="{{$value->links}}">{{$value->links}}</a></p>
                    @endforeach
                @endif
            </div>
        </div>

    </div>
</div>
<script>var _csrf_code = 'Qm5xd3I4aEggBAYkFVBaOBsUJSFEFV8MJ18yMit9BXtyADNFQ14bLg==';</script>
{{--<script src="https://unica.vn/media/js_v2018/bootstrap.min.js"></script>--}}
<script src="{{ URL::asset('public/frontend/themes/edu/js/bootstrap.min.js') }}"></script>

<script src="{{ URL::asset('public/frontend/themes/edu/js/ejs.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/edu/js/tmpl.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/edu/js/popup.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/edu/js/learn_v2.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/edu/js/jquery-ui.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/edu/js/congratulation.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/edu/js/TweenMax.min.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/edu/js/underscore-min.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/edu/js/global.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/edu/js/player.js') }}"></script>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "320px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0"
    }
</script>
<script type="text/javascript">
    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parent().find(".fa-plus-square").removeClass("fa-plus-square").addClass("fa-minus-square");
    }).on('hidden.bs.collapse', function () {
        $(this).parent().find(".fa-minus-square").removeClass("fa-minus-square").addClass("fa-plus-square");
    });
</script>
<script>
    function openNav2() {
        document.getElementById("mySidenav2").style.width = "320px";
    }

    function closeNav2() {
        document.getElementById("mySidenav2").style.width = "0";
    }

</script>
<!-- Large modal -->
<div class="modal fade modal-rating" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="uom-block-rate">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            {{--                <link href="media/styles/rating.css" rel="stylesheet" type="text/css">--}}
            <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/rating.css') }}">
            <script src="{{ URL::asset('public/frontend/themes/edu/js/rating.js') }}"></script>
            {{--<script type="text/javascript" src="media/js/rating.js"></script>--}}
            <div class="uom-block-rate">
                <h3>Đánh giá khóa học</h3>

                <br>
                <input name="rating" value="0" id="rating_star1" type="hidden" course_id="75" style="float: left;">
                <ul class="codexworld_rating_widget">
                    <li style="background-image:url(/media/images//widget_star.gif)"><span>1</span></li>
                    <li style="background-image:url(/media/images//widget_star.gif)"><span>2</span></li>
                    <li style="background-image:url(/media/images//widget_star.gif)"><span>3</span></li>
                    <li style="background-image:url(/media/images//widget_star.gif)"><span>4</span></li>
                    <li style="background-image:url(/media/images//widget_star.gif)"><span>5</span></li>
                </ul>
                <div style="clear:both;"></div>
                <p class="uom-notice"></p>
                <div class="uom-txtbox">
                    <textarea class="form-control" id="comment_danhgia1"
                              placeholder="Nhập nội dung đánh giá (tối thiểu 20 kí tự)" rows="4"></textarea>
                </div>
                <p class="show_error" style="display: none; color: red; margin-top: 5px;">Bạn cần nhập đủ nội dung đánh
                    giá và đánh giá sao!</p>
                <a class=" btn-send-rate btnGuidanhgia" href="javascript:void(0);">Gửi đánh giá</a>
                <a href="javascript:void(0)" class="btn btn-default" data-dismiss="modal" aria-label="Close">Tôi sẽ đánh
                    giá sau</a>
            </div>
            <script type="text/javascript">
                function processRating(val, attrVal) {
                    var user_id = $('#user_id').val();
                    if (user_id == '') {
                        alert('Bạn cần đăng nhập để thực hiện chức năng này!');
                    } else {
                        $('#rating_star1').val(val);
                    }
                }

                $(document).ready(function () {

                    $('.ghi-chep').click(function () {
                        var lesson_item_id = $(this).data('lesson_item_id');
                        var message = $('input[name="message"]').val();
                        $.ajax({
                            url: '{{route('note')}}',
                            type: 'get',
                            data: {
                                lesson_item_id: lesson_item_id,
                                message:message,
                            },
                            success: function (data) {
                                if (data.status) {
                                    alert(data.msg);
                                    location.reload();
                                } else {
                                    alert('Có lỗi xảy ra. Vui lòng load lại website và thử lại!');
                                }
                            }
                        });

                    });

                    $("#rating_star1").codexworld_rating_widget({
                        starLength: '5',
                        initialValue: '',
                        callbackFunctionName: 'processRating',
                        imageDirectory: '/media/images/',
                        inputAttr: 'course_id'
                    });
                    $('.btnGuidanhgia').click(function (e) {
                        var comment = $('#comment_danhgia1').val();
                        var course_id = $('#rating_star1').attr('course_id');
                        var value = $('#rating_star1').val();
                        var user_id = $('#user_id').val();
                        if (user_id == '') {
                            alert('Bạn cần đăng nhập để thực hiện chức năng này!');
                            e.preventDefault();
                            return false;
                        }
                        if (comment.length > 0 && comment.length < 20) {
                            alert('Nội dung đánh giá ít nhất từ 20 kí tự trở lên!');
                            e.preventDefault();
                            return false;
                        }
                        if (comment.length > 20 && value > 0) {
                            $.ajax({
                                url: '/course_action/add_danh_gia',
                                type: 'POST',
                                dateType: 'text',
                                data: {
                                    'course_id': course_id,
                                    'rating_value': value,
                                    'comment': comment,
                                },
                                success: function (message) {
                                    location.reload();
                                }
                            });
                        } else {
                            $('.show_error').css("display", "block");
                            setTimeout(function () {
                                $('.show_error').css("display", "none");
                            }, 3000);
                        }
                    });
                });


            </script>
        </div>
    </div>
</div>
</body>
</html>
