@extends('themeedu::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        @include('themeedu::template.top_bar')
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themeedu::template.menu')
                                </div>
                                @if(@$settings['homepage_style']==0)
                                    <div class="col-lg-3">
                                        <aside class="sidebar static left">
                                            @include('themeedu::partials.widget_left')
                                        </aside>
                                    </div>
                                @endif
                                <div class=" {{@$settings['homepage_style']!=0?'col-lg-12':'col-lg-6'}}">
                                    <?php
                                    $data = CommonHelper::getFromCache('posts_created_at_desc', ['posts']);
                                    if (!$data) {
                                        $data = \Modules\ThemeEdu\Models\Post::where('status', 1)->where('show_homepage', 1)->orderBy('created_at', 'DESC')->orderBy('id', 'DESC')->get();
                                        CommonHelper::putToCache('posts_created_at_desc', $data, ['posts']);
                                    }
                                    ?>
                                    @if(count($data) > 0)
                                        <div class="central-meta">
                                        <span class="create-post">TIN MỚI <a href="/tai-lieu"
                                                                             title="">Xem tất cả</a></span>
                                            <ul class="suggested-frnd-caro">

                                                @foreach($data as $v)
                                                    <li >
                                                        <a href="/tai-lieu/{{@$v->slug}}.html"
                                                           title="{{@$v->name}}"><img data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image,116,116) }}" class="lazy" data-ll-status="observed"
                                                                                      alt="{{@$v->name}}"></a>
                                                        <div class="sugtd-frnd-meta">
                                                            <a href="/tai-lieu/{{@$v->slug}}.html"
                                                               title="{{@$v->name}}">{{@$v->name}}</a>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div><!-- suggested friends -->
                                    @endif
                                    <div class="">
                                        <?php
                                        $data = CommonHelper::getFromCache('courses_created_at_desc', ['courses']);
                                        if (!$data) {
                                            $data = \Modules\EduCourse\Models\Course::where('status', 1)->orderBy('created_at', 'DESC')->orderBy('id', 'DESC')->take(4)->get();
                                            CommonHelper::putToCache('courses_created_at_desc', $data, ['courses']);
                                        }
                                        ?>
                                        <?php
                                        //   Lấy ra các khóa học đã mua
                                        $course_buyed = [];
                                        if (\Auth::guard('student')->check()) {
                                            $course_ids = [];
                                            foreach($data as $course){
                                                $course_ids[] = $course->id;
                                            }

                                            $course_buyed = \Modules\ThemeEdu\Models\Order::where('student_id', @\Auth::guard('student')->user()->id)->whereNotNull('student_id')
                                                ->whereIn('course_id', $course_ids)->where('status', 1)->pluck('course_id')->toArray();
                                        }
                                        ?>
                                        @foreach($data as $course)
                                            @include('themeedu::partials.course_item')
                                        @endforeach
                                            {{--</div>--}}
                                    </div>
                                        <a href="/khoa-hoc" class="btn-view btn-load-more" title="Xem tất cả khóa học">Xem thêm</a>
                                </div>

                                @if(@$settings['homepage_style']==0)
                                    <div class="col-lg-3">
                                        <aside class="sidebar static right">
                                            @include('themeedu::partials.widget_right')
                                        </aside>
                                    </div>
                                @endif

                                @include('themeedu::partials.form_contact')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection
@section('head_script')
    <link rel="stylesheet" href="{{ asset('/public/frontend/themes/edu/css/homepage.css') }}">
    <style>
        .sidebar.left span.mail {
            font-size: 10px;
        }
    </style>
@endsection