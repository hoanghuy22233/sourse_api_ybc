@extends('themeedu::layouts.default')
@section('main_content')
    <div id="mm-0" class="mm-page mm-slideout">
        <div class="se-pre-con"></div>
        <div class="theme-layout">
            @include('themeedu::template.top_bar')
            <section>
                <div class="gap2 gray-bg">
                    <div class="container">
                        <div class="row">
                            <div class="user-profile">
                                @include('themeedu::partials.student_menu')
                            </div><!-- user profile banner  -->
                            <div class="col-lg-12" style="min-height: 500px;">
                                <div class="row merged20" id="page-contents">
                                    <div class="col-lg-12">
                                        <div class="post-title">
                                            <h6><i class="fa fa-link"></i> {{@$misson->name}}</h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="central-meta item">
                                            <div class="user-post">
                                                <div class="friend-info">
                                                    {{--<div class="friend-name">--}}
                                                    {{--<span>{{date('d-m-Y',strtotime(@$misson->created_at))}}</span>--}}
                                                    {{--</div>--}}
                                                    <div class="post-meta">
                                                        {{--<figure>--}}
                                                        {{--<img src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image,786,424) }}"--}}
                                                        {{--alt="{{@$post->name}}">--}}
                                                        {{--</figure>--}}
                                                        <div class="description">
                                                            @if(!empty(@$misson->content))
                                                            <p>
                                                                {!! @$misson->content !!}
                                                            </p>
                                                                @else
                                                            <p>Nội dung nhiệm vụ đang cập nhật</p>
                                                                @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $misson_next = \Modules\EduCourse\Models\Misson::where('status', 1)->where('id', '>', $misson->id)->where('order_no', '<', $misson->order_no)->first();
                                        $misson_id_max = \Modules\EduCourse\Models\Misson::where('status', 1)->max('id');
                                        $misson_order_min = \Modules\EduCourse\Models\Misson::where('status', 1)->min('order_no');
                                        $student = @\Modules\ThemeEdu\Models\HistoryMisson::where('student_id', $user->id)->where('misson_id', $misson->id)->first();
                                        ?>
                                        @if(!isset($student))
                                            @if(\App\Http\Helpers\CommonHelper::has_permission(@\Auth::guard('admin')->user()->id,'misson_finish'))
                                                <button class="hoan-thanh btn btn-info"
                                                        data-misson_id="{{$misson->id}}">Hoàn thành
                                                </button>
                                            @endif
                                        @elseif($misson->id==$misson_id_max||$misson->order_no==$misson_order_min)
                                            @if(\App\Http\Helpers\CommonHelper::has_permission(@\Auth::guard('admin')->user()->id,'misson_finish'))
                                                <button class="chua-hoan-thanh btn btn-success"
                                                        data-misson_id="{{$misson->id}}">Đã hoàn thành <i
                                                            class="fa fa-check"></i></button>
                                            @endif
                                        @else
                                            @if(\App\Http\Helpers\CommonHelper::has_permission(@\Auth::guard('admin')->user()->id,'misson_finish'))
                                                <button class="chua-hoan-thanh btn btn-success"
                                                        data-misson_id="{{$misson->id}}">Đã hoàn thành <i
                                                            class="fa fa-check"></i></button>
                                            @endif
                                            <a href="/student/{{ @$user->id }}/{{@$misson_next->slug}}"
                                               class="btn btn-success">Nhiệm vụ tiếp theo <i
                                                        class="fa fa-arrow-circle-right"></i></a>
                                        @endif
                                    </div><!-- centerl meta -->
                                    <div class="col-lg-3">
                                        <aside class="sidebar static right">
                                            <div class="widget">
                                                <?php
                                                $categories = \Modules\EduCourse\Models\Misson::where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
                                                ?>
                                                <h4 class="widget-title">Nhiệm vụ</h4>
                                                <ul class="naves">
                                                    @foreach($categories as $k=>$misson)
                                                        <?php
                                                        $check = \Modules\ThemeEdu\Models\HistoryMisson::where('student_id', \Auth::guard('student')->user()->id)->where('misson_id', $misson->id)->count();
                                                        ?>
                                                        <li>
                                                            <i class="ti-clipboard"></i>
                                                            @if($check > 0 || $k==0)
                                                                <a href="/student/{{ @$user->id }}/{{@$misson->slug}}"
                                                                   title="{{ @$misson->name }}">{{ @$misson->name }}</a>
                                                            @else
                                                                <a rel="nofollow"
                                                                   title="{{ @$misson->name }}">{{ @$misson->name }}</a>
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                </ul>

                                            </div><!-- Shortcuts -->
                                            <div class="widget">
                                            </div><!-- twitter feed-->
                                            {{--<div class="widget">--}}
                                            {{--@include('themeedu::partials.widget_right')--}}
                                            {{--</div>--}}
                                        </aside>
                                    </div><!-- sidebar -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <script>
        $('.hoan-thanh').click(function () {
            var misson_id = $(this).data('misson_id');
            $.ajax({
                url: '{{route('misson')}}',
                type: 'get',
                data: {
                    misson_id: misson_id,
                    id:'{{$id}}'
                },
                success: function (data) {
                    if (data.status) {
                        alert(data.msg);
                        location.reload();
                    } else {
                        alert('Có lỗi xảy ra. Vui lòng load lại website và thử lại!');
                    }
                }
            })
        });

        $('.chua-hoan-thanh').click(function () {
            var misson_id = $(this).data('misson_id');
            $.ajax({
                url: '{{route('no-misson')}}',
                type: 'get',
                data: {
                    misson_id: misson_id,
                    id:'{{$id}}'
                },
                success: function (data) {
                    if (data.status) {
                        alert(data.msg);
                        location.reload();
                    } else {
                        alert('Có lỗi xảy ra. Vui lòng load lại website và thử lại!');
                    }
                }
            })
        });
    </script>
@endsection
@section('head_script')
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/profile.css') }}?v={{ time() }}">
@endsection