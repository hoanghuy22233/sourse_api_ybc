@extends('themeedu::layouts.default')
@section('baikiemtra')
    class="active"
@endsection
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>
        @include('themeedu::template.top_bar')
        <section style="min-height: 850px;background-color: #edf2f6;">
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themeedu::partials.student_menu')
                                </div><!-- user profile banner  -->

                                <div class="col-lg-12 single_lesson_content">
                                    <div class="central-meta">
                                        <div class="bg-white">
                                            <h6 style="color: red;"><i>Ghi chú: Bài kiểm tra chỉ ghi nhận lần làm đầu
                                                    tiên.
                                                    Nếu làm lại bài kiểm tra đã làm bạn sẽ không được tính thêm điểm
                                                    tích
                                                    lũy và không thay đổi được điểm bài kiểm tra đó</i></h6><br>
                                        </div>
                                        <div class="quiz_list">
                                            <table class="table table-striped" style="background-color: white">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tên bài kiểm tra</th>
                                                    <th>Điểm làm bài</th>
                                                    <th>Điểm tích lũy</th>
                                                    <th>Thời gian làm</th>
                                                    <th>Hành động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
//                                                $data = CommonHelper::getFromCache('quizz_of_student_paginate' . $user->id);
//                                                if (!$data) {
                                                    $data = \Modules\ThemeEdu\Models\QuizLog::where('student_id', $user->id)->orderBy('created_at', 'desc')->paginate(20);
//                                                    dd($data);
//                                                    CommonHelper::putToCache('quizz_of_student_paginate' . $user->id, $data);
                                                //                                                }
                                                ?>
                                                @foreach($data as $quizLog)
                                                    <tr>
                                                        <th scope="row">{{@$k+=1}}</th>
                                                        <td>
                                                            <a href="/bai-kiem-tra/{{@$quizLog->quiz->slug}}.html">{{@$quizLog->quiz->name}}</a>
                                                        </td>
                                                        <td>{{@$quizLog->scores}}</td>
                                                        <td>{{@$quizLog->accumulated_points}}</td>
                                                        <td>{{date('H:i:s d-m-Y',strtotime($quizLog->created_at))}}</td>
                                                        <td><a class="btn btn-info"
                                                               href="/bai-kiem-tra/{{@$quizLog->quiz->slug}}.html">Làm
                                                                lại</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection
@section('head_script')
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/profile.css') }}?v={{ time() }}">
    <style>
        .quiz_list {
            overflow: scroll;
        }
    </style>
@endsection