@extends('themeedu::layouts.default')
@section('lichhoc')
    active
@endsection
@section('main_content')
    <div id="mm-0" class="mm-page mm-slideout">
        <div class="se-pre-con"></div>
        <div class="theme-layout">
            @include('themeedu::template.top_bar')
            <section>
                <div class="gap2 gray-bg">
                    <div class="container">
                        <div class="row">
                            <div class="user-profile">
                                @include('themeedu::template.menu')
                            </div><!-- user profile banner  -->
                            <div class="col-lg-12">
                                <div class="row merged20" id="page-contents">
                                    <div class="col-lg-12">
                                        <div class="post-title">
                                            <h6><i class="fa fa-link"></i> {{@$post->name}}</h6>
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="central-meta item">
                                            <div class="user-post">
                                                <div class="friend-info">
                                                    <div class="friend-name">
                                                        <span>{{date('d-m-Y',strtotime($post->created_at))}}</span>
                                                    </div>
                                                    <div class="post-meta">
                                                        <figure>
                                                            <img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image,786,424) }}"
                                                                 alt="{{@$post->name}}">
                                                        </figure>
                                                        <div class="description">
                                                            <p>
                                                                {!! @$post->content !!}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div id="comments" class="clearfix">
                                                        <h3 id="comments-title">Bình luận</h3>
                                                        <div id="fb-root"></div>
                                                        <script>(function (d, s, id) {
                                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                                if (d.getElementById(id)) return;
                                                                js = d.createElement(s);
                                                                js.id = id;
                                                                js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=1618627438366755&autoLogAppEvents=1';
                                                                fjs.parentNode.insertBefore(js, fjs);
                                                            }(document, 'script', 'facebook-jssdk'));</script>
                                                        <div class="fb-comments"
                                                             data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"
                                                             data-numposts="5"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- centerl meta -->
                                    <div class="col-lg-3">
                                        <aside class="sidebar static right">
                                            <div class="widget">
                                                <?php
                                                $categories = CommonHelper::getFromCache('categorys_type_2_location_menu_cate', ['menus']);
                                                if (!$categories) {
                                                    $categories = \Modules\ThemeEduAdmin\Models\Menu::where('type', 2)->where('parent_id', 0)->where('location', 'cate_menu')->orderBy('order_no', 'desc')->orderBy('name', 'asc')->where('status', 1)->get();
                                                    CommonHelper::putToCache('categorys_type_2_location_menu_cate', $categories, ['menus']);
                                                }
                                                ?>
                                                <h4 class="widget-title">Chuyên mục</h4>
                                                <ul class="naves">
                                                    @foreach($categories as $cat)
                                                        <li>
                                                            <i class="ti-clipboard"></i>
                                                            <a href="/lich-hoc/{{ @$cat->slug }}"
                                                               title="{{ @$cat->name }}">{{ @$cat->name }}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div><!-- Shortcuts -->
                                            <div class="widget">
                                            </div><!-- twitter feed-->
                                            <div class="widget">
                                                @include('themeedu::partials.widget_right')
                                            </div>
                                        </aside>
                                    </div><!-- sidebar -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection