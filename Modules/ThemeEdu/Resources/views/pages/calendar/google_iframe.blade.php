@extends('themeedu::layouts.default')
@section('lichhoc')
    active
@endsection
@section('main_content')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        @include('themeedu::template.top_bar')
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themeedu::template.menu')
                                </div><!-- user profile banner  -->
                                <div class="col-lg-12">
                                    <?php
                                    $schedules = \Modules\EduSettings\Entities\Schedule::where('status', 1)->get();
                                    ?>
                                    <ul class="nav nav-tabs">
                                        @foreach($schedules as $k=>$schedule)
                                            <li class="{{$k==0?'active':''}}">
                                                <a data-toggle="tab" href="#tab_{{$k}}">{{$schedule->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <div class="tab-content">
                                        @foreach($schedules as $k=>$schedule)
                                            <div id="tab_{{$k}}" class="tab-pane fade  {{$k==0?'in active':''}}">
                                                {!! $schedule->iframe !!}
                                            </div>
                                        @endforeach
                                    </div>
{{--                                    <div class="col-lg-12">--}}
{{--                                        {!! @\App\Models\Setting::where('name', 'google_calendar_iframe')->first()->value !!}--}}
{{--                                    </div><!-- sidebar -->--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
@endsection