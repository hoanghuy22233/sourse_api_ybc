<!DOCTYPE html>
<html lang="vi">
{{--class="loading-site no-js">--}}
<head>
    @include('themeedu::partials.head_meta')
    @include('themeedu::partials.head_script')
    <style>
        .top-area > ul {
            padding-left: 10px;
        }
        footer ul.location li {
            padding: 0;
            margin: 0;
        }
        .user-profile figure img {
            max-height: 300px;
        }
        img.lazy {
            opacity: 0;
        }
        .profile-menu > li {
            margin: 0 9px;
        }
        .profile-menu > li > a {
            padding: 0px 16px;
        }
        @media(max-width: 768px) {
            #page-contents .user-profile {
                display: none;
            }
        }
    </style>
    {!! @$settings['frontend_head_code'] !!}
    @yield('head_script')
</head>

<body class="stretched">

{{--@include('themesemicolonwebjdes::partials.header')--}}
@yield('main_content')
<!-- #wrapper -->
@include('themeedu::partials.footer')
@include('themeedu::partials.footer_script')
@yield('footer_script')
@include('themeedu::partials.user_menu_mobile')
@include('themeedu::partials.main_menu_mobile')
@include('themeedu::partials.chat_facebook')

{!! @$settings['frontend_body_code'] !!}
</body>
</html>
