<div class="user-img">
    <h5 style="margin-bottom: 5px;">{{@\Auth::guard('student')->user()->name}}</h5>
    <img style="margin-bottom: 10px;"
         class="lazy" data-src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb(\Auth::guard('student')->user()->image,45,45)}}"
         alt="{{@\Auth::guard('student')->user()->name}}">
    <span class="status f-online"></span>
    <div class="user-setting">
        <ul class="log-out">
            <li><a href="/profile" title=""><i class="ti-user"></i> Xem profile</a></li>
            <li><a href="/profile/edit" title=""><i class="ti-pencil-alt"></i>Sửa profile</a></li>
            <hr style="margin-bottom: 0px;margin-top: 10px;">
            <li><a href="/student/{{ @$user->id }}/khoa-hoc" title=""><i class="fa fa-book"></i>Khóa học</a>
            </li>
            <li><a href="/student/{{ @$user->id }}/bai-kiem-tra" title=""><i
                            class="far fa-list-alt"></i>Bài kiểm tra</a></li>
            <hr style="margin-bottom: 0px;margin-top: 10px;">
            <li><a href="/lich-hoc" title=""><i
                            class="far fa-calendar-alt"></i>Lịch học</a></li>
            <li><a href="/student/{{ @$user->id }}/diem-thi" title=""><i
                            class="far fa-sticky-note"></i>Điểm thi</a></li>
            <hr style="margin-bottom: 0px;margin-top: 10px;">
            <li><a href="/dang-xuat" title=""><i class="ti-power-off"></i>Đăng xuất</a></li>
        </ul>
    </div>
</div>

<script>
    $('.user-img').hover(
        function () {
            $('.user-setting').addClass('active');
        },
        function () {
            $('.user-setting').removeClass('active');
        }
    );
</script>