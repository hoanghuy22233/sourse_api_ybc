<?php

namespace Modules\ThemeEdu\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\EduCourse\Models\Misson;
use Modules\ThemeEdu\Models\QuizLog;
use Modules\ThemeEdu\Models\Student;
use Socialite;
use Validator;

class StudentController extends Controller
{

    public function myProfile($user = false)
    {
        if (!$user) {
            $user = \Auth::guard('student')->user();
        }
        $data['user'] = $user;

        //  Kiểm tra các điểm số chưa được update thì update
        $this->updateQuizLogOfStudent($user->id);

        return view('themeedu::pages.student.profile')->with($data);
    }

    public function getProfile($id)
    {
        if ($id == @\Auth::guard('student')->user()->id) {
            return redirect('/profile');
        }

        $student = Student::find($id);
        if (is_object($student)) {
            return $this->myProfile($student);
        }
        return back();
    }

    public function updateQuizLogOfStudent($student_id)
    {
        $quizz_log_no_update = CommonHelper::getFromCache('quiz_logs_synch_0', ['quiz_log']);
        if (!$quizz_log_no_update) {
            $quizz_log_no_update = QuizLog::where('student_id', $student_id)->where('synch', 0)->get();
            CommonHelper::putToCache('quiz_logs_synch_0', $quizz_log_no_update, ['quiz_log']);
        }

        foreach ($quizz_log_no_update as $quizz_log) {
            $quizController = new QuizController();
            $quizController->updateScores($quizz_log);
        }
        return true;
    }

    public function getEditProfile()
    {
        $data['user'] = \Auth::guard('student')->user();
        return view('themeedu::pages.student.edit_profile')->with($data);
    }

    public function postEditProfile(Request $request)
    {

//        $data = $request->only('name','phone','email','address','image','banner');
//        $repository->update(Auth::guard('student')->user()->id,$data,'success','Đã sửa thành công');
        $student = Student::find(\Auth::guard('student')->user()->id);
        $student->name = $request->name;
        $student->phone = $request->phone;
        $student->email = $request->email;
        $student->address = $request->address;
        $student->center_id = $request->center_id;
        $student->birthday = $request->birthday;
        $student->gender = $request->gender;
        if ($request->hasFile('image')) {
            $student->image = CommonHelper::saveFile($request->image, 'student');
        }
        if ($request->hasFile('banner')) {
            $student->banner = CommonHelper::saveFile($request->banner, 'student/banner');
        }
        $student->save();
        \Session::flash('success', 'Cập nhật thành công');
        CommonHelper::flushCache($this->module['table_name']);
        return redirect()->back();
    }

    public function getChangePass()
    {
        $data['user'] = \Auth::guard('student')->user();
        return view('themeedu::pages.student.change_pass')->with($data);
    }

    public function postChangePass(Request $request)
    {
        $validator = Validator::make($request->all(), [
            're_new_password' => 'same:new_password',
        ], [
            're_new_password.same' => 'Mật khẩu nhập lại không trùng khớp!',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            if (\Auth::guard('student')->user()->password != '') {
                if (\Auth::guard('student')->user()->email == '') {
                    if (!\Auth::guard('student')->attempt(array('phone' => \Auth::guard('student')->user()->phone, 'password' => $request->get('password')))) {
                        \Session::flash('password-error', 'Mật khẩu không chính xác');
                        return back()->withInput();
                    }
                } else {
                    if (!\Auth::guard('student')->attempt(array('email' => \Auth::guard('student')->user()->email, 'password' => $request->get('password')))) {
                        \Session::flash('password-error', 'Mật khẩu không chính xác');
                        return back()->withInput();
                    }
                }
            }

            $student = \Auth::guard('student')->user();
            $student->password = bcrypt($request->new_password);
            $student->save();
            \Session::flash('success', 'Đổi mật khẩu thành công');
        }
        CommonHelper::flushCache($this->module['table_name']);
        return redirect()->back();
    }

    public function course($id)
    {
        $data['user'] = Student::find($id);
        return view('themeedu::pages.student.course', $data);
    }

    public function quiz($id)
    {
        $data['user'] = Student::find($id);

        return view('themeedu::pages.student.quiz', $data);
    }

    public function misson($id, $slug)
    {
        $data['user'] = Student::find($id);
        $data['misson'] = Misson::where('slug', $slug)->first();
        $data['id'] = $id;

        return view('themeedu::pages.student.misson_detail', $data);
    }

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {
        $user_social = Socialite::driver($provider)->user();
        $user = Student::updateOrCreate(
            ['email' => $user_social->email],
            [
                $provider . '_id' => $user_social->id,
                'name' => $user_social->name,
                $provider . '_token' => $user_social->token
            ]);
        \Auth::guard('student')->login($user);
        return redirect('/');
    }

    public function getPoint()
    {
        $data['user'] = Auth::guard('student')->user();
        return view('themeedu::pages.student.point', $data);
    }
}
