<?php

namespace Modules\ThemeEdu\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Mail;
use Modules\ThemeEdu\Http\Requests\CreateRequest;
use Modules\ThemeEdu\Http\Requests\ForgotPasswordRequest;
use Modules\ThemeEdu\Http\Requests\LoginRequest;
use Modules\ThemeEdu\Models\Student;
use Session;
use Validator;


class AuthController extends Controller
{
//    function getLogin()
//    {
//        return view('themeedu::pages.auth.login');
//    }
//
//    function getRegister()
//    {
//        return view('themeedu::pages.auth.register');
//    }
//
//    public function getForgotPassword() {
//        return view('themeedu::pages.auth.forgot_password');
//    }
    public function getLogin()
    {
        $data['page_title'] = 'Đăng nhập';
        $data['page_type'] = 'list';
        return view('themeedu::pages.auth.login');
    }

    public function authenticate(LoginRequest $request)
    {
        $student = Student::where('email', $request['email'])->orWhere('phone', $request['email'])->first();
        if (!is_object($student)) {
            CommonHelper::one_time_message('danger', 'Sai email hoặc số điện thoại');
            return redirect('/dang-nhap');
        }
        if (@$student->status == 0) {
            CommonHelper::one_time_message('danger', 'Tài khoản của bạn chưa được kích hoạt!');
            return redirect('/dang-nhap');
        }

        if (@$student->status == -1) {
            CommonHelper::one_time_message('danger', 'Tài khoản của bạn đã bị khóa!');
            return redirect('/dang-nhap');
        }

        if (\Auth::guard('student')->attempt(['email' => trim($request['email']), 'password' => trim($request['password'])], true)) {
            return redirect()->intended('/');
        } elseif (\Auth::guard('student')->attempt(['phone' => trim($request['email']), 'password' => trim($request['password'])], true))
            return redirect()->intended('/');
        else {
            CommonHelper::one_time_message('danger', 'Sai mật khẩu');
            return redirect('/dang-nhap');
        }
    }

    public function getRegister()
    {
        $data['page_title'] = 'Đăng ký';
        $data['page_type'] = 'list';
        return view('themeedu::pages.auth.register', $data);
    }

    public function postRegister(CreateRequest $request)
    {
        $data = $request->except('_token');
        $data['api_token'] = base64_encode(rand(1, 100) . time());
        $data['password'] = bcrypt($data['password']);
        Student::create($data);
        CommonHelper::one_time_message('success', 'Bạn đã đăng ký tài khoản thành công! Vui lòng đăng nhập!');
        return redirect('/dang-nhap');
    }

    public function getForgotPassword(Request $request, $change_password)
    {
        if (!$_POST) {

            $query = Student::where('change_password', $change_password);

            if (!$query->exists() || !isset($change_password)) {
                abort(404);
            }
            $data['page_title'] = 'Lấy lại mật khẩu';
            $data['page_type'] = 'list';
            return view('themeedu::pages.auth.change_password')->with($data);
        } else {

            if ($request->password == $request->re_password) {
                $student = Student::where('change_password', $change_password)->first();
                $student->password = bcrypt($request->password);
                $student->change_password = $student->id . '_' . time();
                $student->save();
                CommonHelper::one_time_message('success', 'Đổi mật khẩu thành công! vui lòng đăng nhập!');
                return redirect('/dang-nhap');
            } else {
                return back()->with('alert_re_password', 'Nhập lại mật khâu không khớp!');
            }
        }
    }


    public function getEmailForgotPassword(Request $request)
    {
        if (!$_POST) {
            $data['page_title'] = 'Quên mật khẩu';

            return view('themeedu::pages.auth.forgot_password')->with($data);

        } else {
            $query = Student::where('email', $request->email);

            if (!$query->exists()) {
                return back()->with('success', 'Email chưa được đăng ký');
            }
            $student = $query->first();
            $student->change_password = $student->id . '_' . time();
            $student->save();
            try {
                \Eventy::action('admin.restorePassword', [
                    'link' => \URL::to('forgot-password/' . @$student->change_password),
                    'name' => @$student->name,
                    'email' => $student->email
                ]);
                //CommonHelper::one_time_message('success', 'Email sẽ được gửi trong ít phút. Bạn vui lòng kiểm tra mail để lấy lại mật khẩu!');
                return back()->with('success', 'Email sẽ được gửi trong ít phút. Bạn vui lòng kiểm tra mail để lấy lại mật khẩu!');
            } catch (\Exception $ex) {
                CommonHelper::one_time_message('error', 'Xin vui lòng thử lại!');
            }
        }
    }


}




