<?php

if(env('DOMAIN') == 'courses.actionenglish.edu.vn') {
    Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions', 'locale']], function () {
        Route::group(['prefix' => 'theme'], function () {
            Route::match(['get', 'post'], 'setting', 'Admin\EduThemeController@setting')->name('themeedu')->middleware('permission:theme');
        });
    });
//Route::get('register-course-when-login', 'Frontend\ContactController@registerCourseWhenLogin')->name('register-course-when-login');
    Route::get('route', 'Frontend\ContactController@route')->name('route');
    Route::get('topic', 'Frontend\ContactController@topic')->name('topic');
    Route::get('misson', 'Frontend\ContactController@misson')->name('misson');
    Route::get('no-misson', 'Frontend\ContactController@noMisson')->name('no-misson');
    Route::group(['prefix' => '', 'middleware' => 'no_auth:student'], function () {
        Route::get('dang-nhap', 'Frontend\AuthController@getLogin');
        Route::post('dang-nhap', 'Frontend\AuthController@authenticate');
        Route::get('dang-ky', 'Frontend\AuthController@getRegister');
        Route::post('dang-ky', 'Frontend\AuthController@postRegister');
    });

    Route::match(array('GET', 'POST'), 'quen-mat-khau', 'Frontend\AuthController@getEmailForgotPassword');
    Route::match(array('GET', 'POST'), 'forgot-password/{change_password}', 'Frontend\AuthController@getForgotPassword');
    Route::get('dang-xuat', function () {
        \Auth::guard('student')->logout();
        return redirect('/');
    });

//Route::match(array('GET', 'POST'), 'profile/{id}', 'Frontend\AuthController@profileAdmin')->name('profile');

    Route::group(['prefix' => '', 'middleware' => ['guest:student']], function () {
        Route::get('profile', 'Frontend\StudentController@myProfile');
        Route::get('profile/edit', 'Frontend\StudentController@getEditProfile');
        Route::post('profile/edit', 'Frontend\StudentController@postEditProfile');

        Route::group(['prefix' => 'student'], function () {
            Route::get('doi-mat-khau', 'Frontend\StudentController@getChangePass');
            Route::post('doi-mat-khau', 'Frontend\StudentController@postChangePass');

            Route::get('{id}/khoa-hoc', 'Frontend\StudentController@course');
            Route::get('{id}/bai-kiem-tra', 'Frontend\StudentController@quiz');
            Route::get('{id}/diem-thi', 'Frontend\StudentController@getPoint');
            Route::get('{id}/{slug}', 'Frontend\StudentController@misson');
        });
    });

    Route::get('detail', 'Frontend\CourseController@getDetail');

    Route::get('profile/{id}', 'Frontend\StudentController@getProfile');
//Route::get('error', 'Frontend\CourseController@Error')->name('error');
    Route::get('note', 'Frontend\CourseController@Note')->name('note');
//Route::post('dang-ky-khoa-hoc', 'Frontend\ContactController@postContact')->name('contact.post');
    Route::get('tim-kiem', 'Frontend\CourseController@getSearch');
//Route::get('tim-kiem-post', 'Frontend\CourseController@getSearchPost');
//Route::get('tim-kiem-quizz', 'Frontend\CourseController@getSearchQuizz');

    Route::get('lich-hoc', 'Frontend\CourseController@lichHoc');

    Route::group(['prefix' => 'gio-hang'],function () {
        Route::get('', 'Frontend\CartController@getCart');
        Route::post('', 'Frontend\CartController@postCart');
        Route::get('add/{id}', 'Frontend\CartController@addCart');
        Route::get('del_item/{rowId}','Frontend\CartController@delItemCart');
        Route::get('del_all_item','Frontend\CartController@delAllItemCart');
        Route::get('update_item/{rowId}/{qty}','Frontend\CartController@updateItemCart');
        Route::get('thank-you', 'Frontend\CartController@getComplete')->name('cart.complete');
        Route::post('ajax-add-cart', 'Frontend\CartController@ajaxAddCart')->name('cart.ajaxAddCart');
    });

    Route::get('{slug1}', 'Frontend\CourseController@oneParam');
    Route::get('{slug1}/{slug2}', 'Frontend\CourseController@twoParam');
    Route::get('{slug1}/{slug2}/{slug3}', 'Frontend\CourseController@threeParam');
    Route::get('{slug1}/{slug2}/{slug3}/{slug4}', 'Frontend\CourseController@fourParam');

    Route::get('', 'Frontend\HomeController@getHome');

//  Dang nhap fb - gg
    Route::get('/login/{param}/redirect/', 'Frontend\StudentController@redirect')->name('auth_redirect');//dang nhap
    Route::get('/login/{param}/callback/', 'Frontend\StudentController@callback');
}