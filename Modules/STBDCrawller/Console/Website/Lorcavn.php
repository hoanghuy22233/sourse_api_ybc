<?php

namespace Modules\STBDCrawller\Console\Website;

use App\Http\Helpers\CommonHelper;
use Mail;
use Modules\STBDCrawller\Entities\Manufacturer;
use Modules\STBDCrawller\Entities\Origin;
use Modules\STBDCrawller\Entities\PropertieName;
use Modules\STBDCrawller\Entities\PropertieValue;
use Modules\STBDCrawller\Entities\Guarantees;
use Session;

class Lorcavn extends CrawlProductBase
{

    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Sản phẩm',
        'modal' => '\Modules\STBDCrawller\Entities\Product',
    ];

    public function appendData($data)
    {

        //  Thuộc tính
        if (isset($data['proprerties_id'])) {
            $proprerties_id = [];
            foreach ($data['proprerties_id'] as $name => $value) {
                if ($name == 'Xuất xứ') {
                    $origin_slug = str_slug($value);
                    $origin = Origin::where('slug', $origin_slug)->first();
                    if (!is_object($origin)) {
                        $origin = Origin::create([
                            'name_origin' => $value,
                            'status' => 0,
                            'crawl_from' => $this->_domain
                        ]);
                    }

                    $data['origin_id'] = $origin->id;
                } elseif ($name == 'Thương Hiệu') {
                    $manufacturer_slug = str_slug($value);
                    $manufacturer = Manufacturer::where('slug', $manufacturer_slug)->first();
                    if (!is_object($manufacturer)) {
                        $manufacturer = Manufacturer::create([
                            'name' => $value,
                            'slug' => $manufacturer_slug,
                            'status' => 0,
                            'crawl_from' => $this->_domain
                        ]);
                    }

                    $data['manufacture_id'] = $manufacturer->id;
                } elseif ($name == 'Bảo Hành') {
                    $guarantee = Guarantees::where('name', $value)->first();
                    if (!is_object($guarantee)) {
                        $guarantee = Guarantees::create([
                            'name' => $value,
                        ]);
                    }

                    $data['guarantee'] = $guarantee->id;
                } else {
                    if ($value != '') {
                        $propertyName = PropertieName::where('name', $name)->first();
                        if (!is_object($propertyName)) {
                            $propertyName = PropertieName::create([
                                'name' => $name
                            ]);
                        }

                        $propertyValue = PropertieValue::where('properties_name_id', $propertyName->id)->where('value', $value)->first();
                        if (!is_object($propertyValue)) {
                            $propertyValue = PropertieValue::create([
                                'properties_name_id' => $propertyName->id,
                                'value' => trim($value)
                            ]);
                        }

                        $proprerties_id[] = $propertyValue->id;
                    }
                }
            }
            $data['proprerties_id'] = '|' . implode('|', $proprerties_id) . '|';
        }

        $data['highlight'] = @$data['intro'];
        $data['review_detail'] = @$data['content'];
        unset($data['content']);

        return $data;
    }

    /**
     * Lấy ảnh của sản phẩm
     */
    public function getImage($data, $html)
    {
        $v = $html->find(@$this->_doom_setting->image, 0);

        if ($v != null) {
            $data['image'] = '';
            if ($v->getAttribute('data-src') !== false) {
                $data['image'] = @$v->getAttribute('data-src');
            } elseif ($v->getAttribute('src') !== false) {
                $data['image'] = trim(@$v->getAttribute('src'));
            } elseif ($v->getAttribute('style') !== false) {    //  Ảnh trong thuộc tính background-image
                $data['image'] = trim(@$v->getAttribute('style'));
                $data['image'] = @explode('url(', $data['image'])[1];
                $data['image'] = @explode(');', $data['image'])[0];
                $data['image'] = str_replace('"', '', $data['image']);
            }

            $duoi = @explode('-', $data['image'])[count(explode('-', $data['image'])) - 1];
            $duoi = explode('.', $duoi)[0];
            if ($duoi != '') {
                $data['image'] = str_replace('-' . $duoi, '', $data['image']);
            }

            if ($data['image'] != '') {
                $data['image'] = explode('?', $data['image'])[0];

                $data['image'] = $this->attachDomainToLink($data['image']);

                try {
                    $data['image'] = CommonHelper::saveFile($data['image'], 'product/' . time());
                } catch (\Exception $ex) {

                }
            }
        }
        return $data;
    }


    /**
     * Lấy ảnh thêm của sản phẩm
     */
    public function getImageExtra($data, $html)
    {
        $v = $html->find(@$this->_doom_setting->image_extra);
        if ($v != null) {
            $image_extra_arr = [];
            foreach ($v as $image_extra) {
                $image_extra_src = '';
                if ($image_extra->getAttribute('data-src') !== false) {
                    $image_extra_src = @$image_extra->getAttribute('data-src');
                } elseif ($image_extra->getAttribute('src') !== false) {
                    $image_extra_src = trim(@$image_extra->getAttribute('src'));
                } elseif ($image_extra->getAttribute('style') !== false) {    //  Ảnh trong thuộc tính background-image
                    $image_extra_src = trim(@$image_extra->getAttribute('style'));
                    $image_extra_src = @explode('url(', $image_extra_src)[1];
                    $image_extra_src = @explode(');', $image_extra_src)[0];
                    $image_extra_src = str_replace('"', '', $image_extra_src);
                }

                $duoi = @explode('-', $image_extra_src)[count(explode('-', $image_extra_src)) - 1];
                $duoi = explode('.', $duoi)[0];
                if ($duoi != '') {
                    $image_extra_src = str_replace('-' . $duoi, '', $image_extra_src);
                }

                $image_extra_src = explode('?', $image_extra_src)[0];
                if ($image_extra_src != '') {

                    $image_extra_src = $this->attachDomainToLink($image_extra_src);
                    try {
                        $image_extra_arr[] = CommonHelper::saveFile($image_extra_src, 'product/' . str_slug($data['name']));
                    } catch (\Exception $ex) {

                    }
                }
            }
            $data['image_extra'] = '|' . implode('|', $image_extra_arr) . '|';
        }
        return $data;
    }

    /**
     * Lấy các thuộc tính của sản phẩm
     */
    public function getAttribute($data, $html)
    {

        $v = $html->find(@$this->_doom_setting->attributes, 0);
        if ($v != null) {
            $arr = explode('</br>', $v->innertext);

            $data['proprerties_id'] = [];
            foreach ($arr as $attribute) {
                $name = strip_tags(explode(':', $attribute)[0]);
                $name = str_replace('✔ ', '', $name);

                $val = strip_tags(trim(@explode(':', $attribute)[1]));
                $val = str_replace('&nbsp;', '', $val);

                $data['proprerties_id'][trim($name)] = trim($val);
            }
        }
        return $data;
    }

    /**
     * Lưu ảnh trong phần nội dung về server
     */
    public function saveImgInContent($content, $content_doom, $path = 'uploads/')
    {
        //  Tìm và lấy các thẻ <img
        $img_doom_arr = $content_doom->find('img');
        foreach ($img_doom_arr as $img_doom) {
            $img_doom_src2 = false;

            // lấy link ảnh trong data-src hay trong src
            if ($img_doom->getAttribute('data-src') !== false) {
                $img_doom_src = @$img_doom->getAttribute('data-src');
                $img_doom_src2 = @$img_doom->getAttribute('src');
            } else {
                $img_doom_src = trim(@$img_doom->getAttribute('src'));
            }

            //  Xóa các ký tự thừa trong link ảnh
            $img_doom_src = explode('?', $img_doom_src)[0];

            //  Gắn tên miền vào link ảnh
            $img_src = $this->attachDomainToLink($img_doom_src);

            //  Lưu link ảnh
            $img_src = CommonHelper::saveFile($img_src, $path);

            //  Thay link ảnh mới ở server mình vào link ảnh cũ ở server web nguồn
            $content = str_replace($img_doom_src, '/public/filemanager/userfiles/' . $img_src, $content);
            if ($img_doom_src2) {
                $content = str_replace($img_doom_src2, '/public/filemanager/userfiles/' . $img_src, $content);
            }
        }

        //  Tìm và lấy các thẻ <input
        $img_doom_arr = $content_doom->find('input');
        foreach ($img_doom_arr as $img_doom) {
            $img_doom_src2 = false;

            // lấy link ảnh trong data-src hay trong src
            if ($img_doom->getAttribute('data-src') !== false) {
                $img_doom_src = @$img_doom->getAttribute('data-src');
                $img_doom_src2 = @$img_doom->getAttribute('src');
            } else {
                $img_doom_src = trim(@$img_doom->getAttribute('src'));
            }

            //  Xóa các ký tự thừa trong link ảnh
            $img_doom_src = explode('?', $img_doom_src)[0];

            //  Gắn tên miền vào link ảnh
            $img_src = $this->attachDomainToLink($img_doom_src);

            //  Lưu link ảnh
            $img_src = CommonHelper::saveFile($img_src, $path);

            //  Thay link ảnh mới ở server mình vào link ảnh cũ ở server web nguồn
            $content = str_replace($img_doom_src, '/public/filemanager/userfiles/' . $img_src, $content);
            if ($img_doom_src2) {
                $content = str_replace($img_doom_src2, '/public/filemanager/userfiles/' . $img_src, $content);
            }

            $content = str_replace('<input', '<img', $content);
        }
        return $content;
    }

    /**
     * Lấy link danh sách sản phẩm
     */
    public function getPageListLink($cat_doom, $doom_setting, $i)
    {
        $link = str_replace('{i}', $i, $doom_setting->category_pagination);
        return str_replace('.html', $link . '.html', $cat_doom->link_crawl);
    }

    public function updateProduct($product, $data)
    {
        print "        => Updated product " . $product->id . ':' . $product->name . "\n";
        return true;
    }

    public function getFinalPrice($data, $html)
    {
        $v = $html->find(@$this->_doom_setting->final_price, 0);
        if ($v != null) {
            $data['final_price'] = trim($v->innertext);

            $data['final_price'] = @explode('<bdi>', $data['final_price'])[1];
            $data['final_price'] = @explode('<span', $data['final_price'])[0];
            $data['final_price'] = $this->cleanPrice($data['final_price']);
        }
        return $data;
    }
}
