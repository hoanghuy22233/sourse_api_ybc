<?php

namespace Modules\STBDCrawller\Console\Website;

use App\Http\Helpers\CommonHelper;
use App\Models\Error;
use Mail;
use Modules\STBDCrawller\Entities\Manufacturer;
use Modules\STBDCrawller\Entities\Origin;
use Modules\STBDCrawller\Entities\Product;
use Modules\STBDCrawller\Entities\PropertieName;
use Modules\STBDCrawller\Entities\PropertieValue;
use Modules\STBDCrawller\Entities\Guarantees;
use Session;

class Hmhcomvn extends CrawlProductBase
{

    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Sản phẩm',
        'modal' => '\Modules\STBDCrawller\Entities\Product',
    ];

    public function appendData($data)
    {
        $data['highlight'] = @$data['intro'];
        $data['review_detail'] = @$data['content'];
        unset($data['content']);

        return $data;
    }

    public function crawlPageList($test = false)
    {
        $result = [
            'total_created' => 0,
            'total_updated' => 0
        ];

        //  Lấy cấu hình doom
        $doom_setting = json_decode($this->_website->doom);

        //  Thực hiện quét các danh mục đã cấu hình
        foreach ($this->_website->categories as $cat_doom) {
            $i = -1;
            $stop = false;
            while (!$stop) {
                $i++;

                //  Chỉ chạy 1 trang đầu
                /*if ($i == 2) {
                    $stop = true;
                    break;
                }*/

                $page_list_link = $this->getPageListLink($cat_doom, $doom_setting, $i * 16);
                $html = file_get_html($page_list_link);

                $products_find = $html->find($doom_setting->target);
                $link_old = '';

                //  Nếu không tìm thấy sản phẩm nào thì dừng lại
                if ($products_find == null || empty($products_find)) {
                    $stop = true;
                    break;
                }

                foreach ($products_find as $k => $product) {

                    try {
                        //  Lấy link sản phẩm
                        $product_link = $product->find($doom_setting->link, 0);
                        if ($product_link == null) {
                            return false;
                        }
                        $product_link = $product_link->getAttribute('href');
                        $product_link = $this->attachDomainToLink($product_link);

                        //  Nếu chưa tồn tại lưu nhớ link sản phẩm đầu thì tạo lưu nhớ cho link sản phẩm đầu tiên lấy được
                        if (!isset($product_first_link)) {
                            $product_first_link = $product_link;
                        } else {
                            //  Nếu link sản phẩm này trùng với link sản phẩm đầu tiên lấy được tức là đang bị chạy vòng tròn lặp lại sẽ dừng chạy
                            if ($product_link == $product_first_link) {
                                $stop = true;
                                break;
                            }
                        }

//                        $product_link = 'http://cata.com.vn/tf-5060-ex.html';
//                        $product_link = 'https://hafelehome.com.vn/products/tam-lot-hoc-tu-bep?variant=7092452392993';
                        if ($product_link != $link_old) {
                            $link_old = $product_link;

                            //  Kiểm tra trong db xem đã crawl sản phẩm này chưa
                            $product_exist = Product::where('crawl_link', $product_link)->first();

                            //  Đã có thì  cập nhật - chưa thì tạo mới
                            if (is_object($product_exist)) {
                                $product_data = $this->getDataProduct($product_link);
                                $product_data['crawl_link'] = $product_link;
                                $product_data = $this->cleanData($product_data);
                                $product_data = $this->bhcAppendData($product_data);
                                $product_data = $this->appendData($product_data);
                                if ($test) {
                                    dd($product_data);
                                    return $this->printDemo($product_data);
                                }
                                $this->updateProduct($product_exist, $product_data);
                                if ($test == 1) {
                                    dd($product_data);
                                    return $this->printDemo($product_data);
                                }
                                $result['total_updated']++;
                            } else {
                                $product_data = $this->getDataProduct($product_link);
                                $product_data['crawl_link'] = $product_link;

                                $product_data = $this->cleanData($product_data);
                                $product_data = $this->bhcAppendData($product_data);
                                $product_data = $this->appendData($product_data);
                                if ($test === true) {
                                    dd($product_data);
                                    return $this->printDemo($product_data);
                                }
                                $prd = $this->createProduct($cat_doom, $product_data);
                                if ($test == 1) {
                                    dd($product_data);
                                    return $this->printDemo($product_data);
                                }
                                $result['total_created']++;
                            }
                        }
                    } catch (\Exception $ex) {
                        Error::create([
                            'module' => 'stbdcrawller',
                            'message' => $ex->getLine() . ' : ' . $ex->getMessage(),
                            'file' => $ex->getFile(),
                            'code' => $this->_domain
                        ]);
                    }
                }
            }
        }
        return $result;
    }

    public function updateProduct($product, $data)
    {
        print "        => Updated product " . $product->id . ':' . $product->name . "\n";
        return true;
    }
}
