<?php

namespace Modules\STBDCrawller\Console\Website;

use App\Http\Helpers\CommonHelper;
use Mail;
use Modules\STBDCrawller\Entities\Manufacturer;
use Modules\STBDCrawller\Entities\Origin;
use Modules\STBDCrawller\Entities\PropertieName;
use Modules\STBDCrawller\Entities\PropertieValue;
use Modules\STBDCrawller\Entities\Guarantees;
use Session;

class Speliervn extends CrawlProductBase
{

    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Sản phẩm',
        'modal' => '\Modules\STBDCrawller\Entities\Product',
    ];

    public function appendData($data)
    {

        $data['highlight'] = @$data['intro'];
        $data['review_detail'] = @$data['content'];
        unset($data['content']);

        return $data;
    }

    public function updateProduct($product, $data)
    {
        print "        => Updated product " . $product->id . ':' . $product->name . "\n";
        return true;
    }

    /**
     * Lấy ảnh của sản phẩm
     */
    public function getImage($data, $html)
    {
        $v = $html->find(@$this->_doom_setting->image, 0);
//        dd($this->_doom_setting->image);
        if ($v != null) {
            $data['image'] = '';
            if ($v->getAttribute('data-src') !== false) {
                $data['image'] = @$v->getAttribute('data-src');
            } elseif ($v->getAttribute('src') !== false) {
                $data['image'] = trim(@$v->getAttribute('src'));
            } elseif ($v->getAttribute('style') !== false) {    //  Ảnh trong thuộc tính background-image
                $data['image'] = trim(@$v->getAttribute('style'));
                $data['image'] = @explode('url(', $data['image'])[1];
                $data['image'] = @explode(');', $data['image'])[0];
                $data['image'] = str_replace('"', '', $data['image']);
            }

            $data['image'] = str_replace('-100x100', '', $data['image']);

            if ($data['image'] != '') {
                $data['image'] = explode('?', $data['image'])[0];

                $data['image'] = $this->attachDomainToLink($data['image']);


                try {
                    $data['image'] = CommonHelper::saveFile($data['image'], 'product/' . str_slug($data['name']));
                } catch (\Exception $ex) {

                }
            }
        }

        return $data;
    }
}
