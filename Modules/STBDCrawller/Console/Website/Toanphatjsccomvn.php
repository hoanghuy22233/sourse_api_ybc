<?php

namespace Modules\STBDCrawller\Console\Website;

use App\Http\Helpers\CommonHelper;
use Mail;
use Modules\STBDCrawller\Entities\Manufacturer;
use Modules\STBDCrawller\Entities\Origin;
use Modules\STBDCrawller\Entities\PropertieName;
use Modules\STBDCrawller\Entities\PropertieValue;
use Modules\STBDCrawller\Entities\Guarantees;
use Session;

class Toanphatjsccomvn extends CrawlProductBase
{

    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Sản phẩm',
        'modal' => '\Modules\STBDCrawller\Entities\Product',
    ];

    public function appendData($data)
    {
        //  Thuộc tính
        if (isset($data['proprerties_id'])) {
            $proprerties_id = [];
            foreach ($data['proprerties_id'] as $name => $value) {
                if ($name == 'Mã sản phẩm') {
                    $data['code'] = $value;
                } elseif ($name == '$value') {
                    $data['intro'] = $value;
                } elseif ($name == 'Tình trạng') {

                } elseif ($name == 'Giá sản phẩm') {
                    $data['final_price'] = $this->cleanPrice($value);
                } else {
                    if ($value != '') {
                        $propertyName = PropertieName::where('name', $name)->first();
                        if (!is_object($propertyName)) {
                            $propertyName = PropertieName::create([
                                'name' => $name
                            ]);
                        }

                        $propertyValue = PropertieValue::where('properties_name_id', $propertyName->id)->where('value', $value)->first();
                        if (!is_object($propertyValue)) {
                            $propertyValue = PropertieValue::create([
                                'properties_name_id' => $propertyName->id,
                                'value' => trim($value)
                            ]);
                        }

                        $proprerties_id[] = $propertyValue->id;
                    }
                }
            }
            $data['proprerties_id'] = '|' . implode('|', $proprerties_id) . '|';
        }

        $data['highlight'] = @$data['intro'];
        $data['review_detail'] = @$data['content'];
        unset($data['content']);

        return $data;
    }

    public function getAttribute($data, $html)
    {
        $v = $html->find('.right-content .properties-product table.table tr');
        if ($v != null) {
            foreach ($v as $attribute) {
                $str = trim(strip_tags($attribute->innertext));
                $str = preg_replace('/\s+\s+/', ' ', $str);
                $name = strip_tags(explode(':', $str)[0]);
                $val = strip_tags(trim(@explode(':', $str)[1]));
                $val = str_replace('&nbsp;', '', $val);
                $data['proprerties_id'][trim($name)] = trim($val);
            }
        }
        return $data;
    }

    public function updateProduct($product, $data)
    {
        print "        => Updated product " . $product->id . ':' . $product->name . "\n";
        return true;
    }
}
