<?php

namespace Modules\STBDCrawller\Console\Website;

use App\Http\Helpers\CommonHelper;
use Mail;
use Modules\STBDCrawller\Entities\Manufacturer;
use Modules\STBDCrawller\Entities\Origin;
use Modules\STBDCrawller\Entities\PropertieName;
use Modules\STBDCrawller\Entities\PropertieValue;
use Modules\STBDCrawller\Entities\Guarantees;
use Session;

class Munchencomvn extends CrawlProductBase
{

    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Sản phẩm',
        'modal' => '\Modules\STBDCrawller\Entities\Product',
    ];

    /**
     * Lấy link danh sách sản phẩm
     */
    public function getPageListLink($cat_doom, $doom_setting, $i)
    {
        $cat_doom->link_crawl = str_replace('.html', '', $cat_doom->link_crawl);
        return $cat_doom->link_crawl .'-'.  $i . '.html';
    }

    public function appendData($data)
    {
        $data['code'] = str_replace('Mã SP : ', '', $data['code']);

        $data['highlight'] = @$data['intro'];
        $data['review_detail'] = @$data['content'];
        unset($data['content']);

        return $data;
    }

    public function getName($data, $html)
    {
        $v = $html->find('table.table.tbnober tr td', 0);
        $data['name'] = trim(strip_tags($v->innertext));

        $v = $html->find('table.table.tbnober tr td', 2);
        $data['final_price'] = trim(strip_tags($v->innertext));
        $data['final_price'] = $this->cleanPrice($data['final_price']);

        return $data;
    }

    public function updateProduct($product, $data)
    {
        print "        => Updated product " . $product->id . ':' . $product->name . "\n";
        return true;
    }
}
