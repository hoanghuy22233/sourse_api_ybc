<?php

namespace Modules\STBDCrawller\Console\Website;

use App\Http\Helpers\CommonHelper;
use Mail;
use Modules\STBDCrawller\Entities\Manufacturer;
use Modules\STBDCrawller\Entities\Origin;
use Modules\STBDCrawller\Entities\PropertieName;
use Modules\STBDCrawller\Entities\PropertieValue;
use Session;

class Bepnamduongvn extends CrawlProductBase
{

    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Sản phẩm',
        'modal' => '\Modules\STBDCrawller\Entities\Product',
    ];

    public function appendData($data)
    {
        //  Thuộc tính
        if (isset($data['proprerties_id'])) {
            $proprerties_id = [];
            foreach ($data['proprerties_id'] as $name => $value) {
                if ($value != '') {
                    if ($name == 'Nh&atilde;n hiệu') {
                        $data['manufacture_id'] = $this->saveManufacturer($value)->id;
                    } elseif($name == 'M&atilde; sản phẩm') {
                        $data['code'] = $value;
                    } else {
                        $proprerties_id[] = $this->saveAttribute($name, $value)->id;
                    }
                }
            }
            $data['proprerties_id'] = '|' . implode('|', $proprerties_id) . '|';
        }

        $data['highlight'] = @$data['intro'];
        $data['review_detail'] = @$data['content'];
        unset($data['content']);

        return $data;
    }

    /**
     * Lấy các thuộc tính của sản phẩm
     */
    public function getAttribute($data, $html)
    {
        $v = $html->find(@$this->_doom_setting->attributes);
//        dd($v);
        if ($v != null) {
            $data['proprerties_id'] = [];
            foreach ($v as $attribute) {
                $name = @strip_tags($attribute->find('td', 0)->innertext);
                $name = str_replace('&nbsp;', '', $name);
                $name = trim(str_replace(':', '', $name));

                $val = @strip_tags($attribute->find('td', 1)->innertext);
                $val = str_replace('&nbsp;', '', $val);
                $data['proprerties_id'][trim($name)] = trim($val);
            }
        }

        return $data;
    }

    /**
     * Lưu ảnh trong phần nội dung về server
     */
    public function saveImgInContent($content, $content_doom, $path = 'uploads/')
    {
        //  Tìm và lấy các thẻ <img
        $img_doom_arr = $content_doom->find('img');
        foreach ($img_doom_arr as $img_doom) {
            $img_doom_src2 = false;

            // lấy link ảnh trong data-src hay trong src
            $img_doom_src = trim(@$img_doom->getAttribute('src'));

            //  Xóa các ký tự thừa trong link ảnh
            $img_doom_src = explode('?', $img_doom_src)[0];

            if ($img_doom_src != '') {
                //  Gắn tên miền vào link ảnh
                $img_src = $this->attachDomainToLink($img_doom_src);
                //  Lưu link ảnh
                $img_src = CommonHelper::saveFile($img_src, $path);

                //  Thay link ảnh mới ở server mình vào link ảnh cũ ở server web nguồn
                $content = str_replace($img_doom_src, '/public/filemanager/userfiles/' . $img_src, $content);
                if ($img_doom_src2) {
                    $content = str_replace($img_doom_src2, '/public/filemanager/userfiles/' . $img_src, $content);
                }
            }
        }
        return $content;
    }

    /**
     * Lấy ảnh thêm của sản phẩm
     */
    public function getImageExtra($data, $html)
    {
        $v = $html->find(@$this->_doom_setting->image_extra);
        if ($v != null) {
            $image_extra_arr = [];
            foreach ($v as $image_extra) {
                $image_extra_src = '';
                if ($image_extra->getAttribute('data-src') !== false) {
                    $image_extra_src = @$image_extra->getAttribute('data-src');
                } elseif ($image_extra->getAttribute('src') !== false) {
                    $image_extra_src = trim(@$image_extra->getAttribute('src'));
                } elseif ($image_extra->getAttribute('style') !== false) {    //  Ảnh trong thuộc tính background-image
                    $image_extra_src = trim(@$image_extra->getAttribute('style'));
                    $image_extra_src = @explode('url(', $image_extra_src)[1];
                    $image_extra_src = @explode(');', $image_extra_src)[0];
                    $image_extra_src = str_replace('"', '', $image_extra_src);
                }

                $image_extra_src = explode('?', $image_extra_src)[0];
                $image_extra_src = str_replace('x70x70', 'x400x400', $image_extra_src);

                if ($image_extra_src != '') {

                    $image_extra_src = $this->attachDomainToLink($image_extra_src);
                    try {
                        $image_extra_arr[] = CommonHelper::saveFile($image_extra_src, 'product/' . str_slug($data['name']));
                    } catch (\Exception $ex) {

                    }
                }
            }
            $data['image_extra'] = '|' . implode('|', $image_extra_arr) . '|';
        }
        return $data;
    }

    public function updateProduct($product, $data)
    {
        print "        => Updated product " . $product->id . ':' . $product->name . "\n";
        return true;
    }
}
