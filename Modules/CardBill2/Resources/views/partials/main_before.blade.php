<style>
    .bottom-header {
        display: none;
    }
    @media (max-width: 768px) {
        button#kt_header_mobile_topbar_toggler {
            display: none;
        }


        .bottom-header {
            border-top: 1px solid #ccc;
            background: #fff;
        }

        .bottom-header {
            background: #fff;
            bottom: 0;
            display: block;
            left: 0;
            padding: 0 15px;
            position: fixed;
            text-align: center;
            width: 100%;
            z-index: 999;
        }

        ul.menu-left, ul.menu-right {
            display: flex;
            align-items: center;
            list-style: outside none none;
            margin: 0;
            padding: 0;
            vertical-align: middle;
        }

        .menu-left {
            display: flex;
        }

        ul.menu-left a.active {
            border-top: 2px solid #1877f2;
        }

        ul.menu-left a {
            display: flex;
            flex-direction: column;
            flex-wrap: wrap;
            width: 25%;
            padding: 20px 0;
        }

        ul.menu-left a a {
            font-size: 11px;
        }

        ul.menu-left a.active a {
            color: #1877f2;
        }

        ul.menu-left a.active i {
            color: #1877f2;
        }

        ul.menu-left i {
            font-size: 25px;
            margin-bottom: 3px;
        }
        .bottom-header {
            display: block;
        }
    }

</style>


<div class="bottom-header">
    <ul class="menu-left" style="width: 100%;">
        <a class="active" href="/admin/dashboard">
            <i class="fas fa-home"></i>
            <span class="ico-hover" href="/" title="">Trang chủ</span>
        </a>
        <a class="" href="/admin/card">
            <i class="fa fa-file-invoice-dollar"></i>
            <span class="ico-hover" href="/kenh" title="">Nạp tiền</span>
        </a>
        <a class="">
            <i class="fas fa-file-invoice"></i>
            <span class="ico-hover sidebar-profile" title="">Thanh toán</span>
        </a>
        <a class="" href="/admin/transaction_history">
            <i class="fas fa-history"></i>
            <span class="ico-hover sidebar-profile" title="">LS giao dịch</span>
        </a>
    </ul>
</div>