<style>
    @media (max-width: 768px) {
        button#kt_header_mobile_topbar_toggler {
            display: none;
        }
    }

</style>
@if(in_array('card_view', $permissions))
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/card"
                                                      class="kt-menu__link "><span
                    class="kt-menu__link-icon"><i class="flaticon-price-tag"></i>
            </span><span class="kt-menu__link-text">Thẻ</span></a></li>
@endif
@if(in_array('transaction_history_view', $permissions))
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/transaction_history"
                                                      class="kt-menu__link "><span
                    class="kt-menu__link-icon"><i class="flaticon-list"></i>
            </span><span class="kt-menu__link-text">Lịch sử giao dịch</span></a></li>
@endif
@if(in_array('admin_log_view', $permissions))
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/admin_logs"
                                                      class="kt-menu__link "><span
                    class="kt-menu__link-icon"><i class="flaticon-list"></i>
            </span><span class="kt-menu__link-text">Lịch sử thao tác</span></a></li>
@endif