<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Khách hàng
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body">
            <div class="row kt-section kt-section--first">
                <div class="col-md-4 kt-avatar kt-avatar--outline" id="kt_user_avatar_image">
                    <div class="kt-avatar__holder" style="width: unset;height: unset;">
                        <img style="max-width:100%;padding-top:5px; cursor: pointer;height: unset;"
                             src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$result->user->image, 120, 120) }}"
                             class="file_image_thumb" title="CLick để phóng to ảnh">
                    </div>
                </div>
                <div class="col-md-8 kt-widget__body">
                    <div class="kt-widget__content">
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Họ & tên:</span>
                            <a class="kt-widget__data"><strong>{{ @$result->user_name }}</strong></a>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">SĐT:</span>
                            <span class="kt-widget__data"><strong>{{ @$result->user_tel }}</strong></span>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Email:</span>
                            <span class="kt-widget__data"><strong>{{ @$result->user_email }}</strong></span>
                        </div>
                        <?php
                        $users_address = \Modules\CardUser\Models\UserAddress::where('user_id', @$result->user_id)->get();
                        ?>
                        @foreach($users_address as $k=>$user_address)
                            <div class="kt-widget__info">
                                <span class="kt-widget__label">Địa chỉ {{@$k+=1}}:</span>
                                <span class="kt-widget__data"><strong>{{ @$user_address->address }}</strong></span>
                            </div>
                        @endforeach
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Mã thẻ:</span>
                            <a class="kt-widget__data"><strong>{{ @$result->card_code }}</strong></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Form-->
</div>