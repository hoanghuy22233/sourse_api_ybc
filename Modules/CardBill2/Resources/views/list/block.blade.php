<style>
    .row-actions {
        display: none;
    }
    .btn_detail {
        display: inline-block;
        float: left;
        margin-right: 5px;
    }
</style>
@foreach($listItem as $item)
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
        <!--begin::Card-->
        <div class="card card-custom gutter-b card-stretch">
            <!--begin::Body-->
            <div class="card-body pt-4">
                <!--begin::Info-->
                <div class="mb-7">
                    @foreach($module['list'] as $field)
                        @if($field['type'] == 'company')
                            @if(\Auth::guard('admin')->user()->super_admin == 1)
                                <div class="d-flex justify-content-between align-items-center transaction_list-mb">
                                    <span class="text-dark-75 font-weight-bolder mr-2">{{ trans($field['label']) }}:</span>
                                    <span class="text-muted text-hover-primary">
                                        @include(config('core.admin_theme').'.list.td.'.$field['td'])
                                    </span>
                                </div>
                                @php $count_sort++; @endphp
                            @endif
                        @else
                            <div class="d-flex justify-content-between align-items-center transaction_list-mb">
                                <span class="text-dark-75 font-weight-bolder mr-2">{{ trans($field['label']) }}:</span>
                                <span class="text-muted text-hover-primary">
                                    @if($field['type'] == 'custom')
                                        @include($field['td'], ['field' => $field])
                                    @else
                                        @include(config('core.admin_theme').'.list.td.'.$field['type'])
                                    @endif
                                </span>
                            </div>
                            @php $count_sort++; @endphp
                        @endif
                    @endforeach


                </div>
                <!--end::Info-->
                <p class="btn_detail btn-pd">
                    <a href="{{ url('/admin/'.$module['code'].'/' . $item->id) }}" class="">Chi Tiết</a>
                </p>
                @if(in_array($module['code'] . '_delete', $permissions) || in_array('super_admin', $permissions))
                    <p class="btn_detail btn-pd"">
                        <a href="{{ url('/admin/'.$module['code'].'/delete/' . $item->id) }}" class="">Xóa</a>
                    </p>
                @endif
            </div>
            <!--end::Body-->
        </div>
        <!--end::Card-->
    </div>
@endforeach