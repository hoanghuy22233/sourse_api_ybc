<table class="table table-striped">
    <thead class="kt-datatable__head">
    <tr class="kt-datatable__row" style="left: 0px;">
        <th style="display: none;"></th>
        <th data-field="id"
            class="kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check"><span
                    style="width: 20px;"><label
                        class="kt-checkbox kt-checkbox--single kt-checkbox--all kt-checkbox--solid"><input
                            type="checkbox"
                            class="checkbox-master">&nbsp;<span></span></label></span></th>
        @foreach($module['list'] as $field)
            @if($field['type'] == 'company')
                @if(\Auth::guard('admin')->user()->super_admin == 1)
                    <th data-field="{{ $field['name'] }}"
                        class="kt-datatable__cell kt-datatable__cell--sort {{ @$_GET['sorts'][$count_sort] != '' ? 'kt-datatable__cell--sorted' : '' }}"
                        @if(isset($field['sort']))
                        onclick="sort('{{ $field['name'] }}')"
                            @endif
                    >
                        {{ $field['label'] }}
                        @if(isset($field['sort']))
                            @if(@$_GET['sorts'][$count_sort] == $field['name'].'|asc')
                                <i class="flaticon2-arrow-up"></i>
                            @else
                                <i class="flaticon2-arrow-down"></i>
                            @endif
                        @endif
                    </th>
                    @php $count_sort++; @endphp
                @endif
            @else
                <th data-field="{{ $field['name'] }}"
                    class="kt-datatable__cell kt-datatable__cell--sort {{ @$_GET['sorts'][$count_sort] != '' ? 'kt-datatable__cell--sorted' : '' }}"
                    @if(isset($field['sort']))
                    onclick="sort('{{ $field['name'] }}')"
                        @endif
                >
                    {{ trans($field['label']) }}
                    @if(isset($field['sort']))
                        @if(@$_GET['sorts'][$count_sort] == $field['name'].'|asc')
                            <i class="flaticon2-arrow-up"></i>
                        @else
                            <i class="flaticon2-arrow-down"></i>
                        @endif
                    @endif
                </th>
                @php $count_sort++; @endphp
            @endif
        @endforeach
    </tr>
    </thead>
    <tbody class="kt-datatable__body ps ps--active-y" style="max-height: 496px;">
    @foreach($listItem as $item)
        <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
            <td style="display: none;"
                class="id id-{{ $item->id }}">{{ $item->id }}</td>
            <td class="kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check"
                data-field="ID"><span style="width: 20px;"><label
                            class="kt-checkbox kt-checkbox--single kt-checkbox--solid"><input
                                name="id[]"
                                type="checkbox" class="ids"
                                value="{{ $item->id }}">&nbsp;<span></span></label></span>
            </td>
            @foreach($module['list'] as $field)
                @if($field['type'] == 'company')
                    @if(\Auth::guard('admin')->user()->super_admin == 1)
                        <td data-field="{{ @$field['name'] }}"
                            class="kt-datatable__cell item-{{ @$field['name'] }}">
                            @include(config('core.admin_theme').'.list.td.'.$field['td'])
                        </td>
                    @endif
                @else
                    <td data-field="{{ @$field['name'] }}"
                        class="kt-datatable__cell item-{{ @$field['name'] }}">
                        @if($field['type'] == 'custom')
                            @include($field['td'], ['field' => $field])
                        @else
                            @include(config('core.admin_theme').'.list.td.'.$field['type'])
                        @endif
                    </td>
                @endif
            @endforeach
        </tr>
    @endforeach
    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
    </div>
    <div class="ps__rail-y" style="top: 0px; height: 496px; right: 0px;">
        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 207px;"></div>
    </div>
    </tbody>
</table>