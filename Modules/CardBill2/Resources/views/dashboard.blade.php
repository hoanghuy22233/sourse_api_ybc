<?php
$whereRaw = '1=1';
if (\Auth::guard('admin')->user()->super_admin != 1) {
    //  Nếu ko phải super_admin thì truy vấn theo dữ liệu cty đó
    $whereRaw .= ' AND company_id = ' . \Auth::guard('admin')->user()->last_company_id;
}
?>

@extends(config('core.admin_theme').'.template')
@section('main')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <form action="/admin/dashboard/search" method="get" class="">
            <div class="kt-subheader  kt-grid__item" id="kt_subheader">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-subheader__main my-5">

                        <div class="date__input-box d-flex w-100">
                            {{--                        <h3 class="kt-subheader__title" id="nopadding"></h3>--}}

                            {{--                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>--}}

                            <div class="form-group mb-3 date-input">
                                <label for="startDate" class="kt-subheader__desc">Từ ngày</label>

                                <input class="kt-subheader__desc form-control" type="date" name="date_start"
                                       id="startDate">

                            </div>


                            {{--                        <h3 class="kt-subheader__title" id="nopadding"></h3>--}}

                            {{--                    <span class="kt-subheader__separator kt-subheader__separator--v"></span>--}}

                            <div class="form-group mb-3 date-input ml-50px">
                                <label for="endDate" class="kt-subheader__desc ">Đến ngày</label>

                                <input class="kt-subheader__desc  form-control" type="date" name="date_end"
                                       id="endDate">
                            </div>
                        </div>


                        <div class="filter__box d-flex justify-content-around w-100">
                            <select class="form-control w-60px">
                                <option>Chọn nhân viên</option>
                            </select>
                            <button type="submit"
                                    class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
                                Lọc
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
        <div class="row" style="margin-top: 70px; margin-bottom: 70px;">
            <div class="col-xs-12 col-lg-12 col-xl-12 col-lg-12 order-lg-1 order-xl-1">
                <!--begin:: Widgets/Finance Summary-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head w-100">
                        <div class="kt-portlet__head-label w-100">
                            <h3 class="kt-portlet__head-title bold uppercase w-100 text-center">
                                Tổng quan
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget12">
                            <div class="kt-widget12__content">
                                <div class="kt-widget12__item thong_ke_so">
                                    @if(\Auth::guard('admin')->user()->super_admin == 1)
                                        <div class="col-sm-3 kt-widget12__info">
                                            <span class="kt-widget12__desc">Tổng số thẻ</span>
                                            <span class="kt-widget12__value">{{number_format(@$total_cards, 0, '.', '')}}</span>
                                        </div>

                                        <div class="col-sm-3 kt-widget12__info">
                                            <span class="kt-widget12__desc">Số thẻ đang kích hoạt</span>
                                            <span class="kt-widget12__value">10</span>
                                        </div>
                                        <div class="col-sm-3 kt-widget12__info">
                                            <span class="kt-widget12__desc">Tổng số tiền đã tiêu</span>
                                            <span class="kt-widget12__value">£{{number_format(@$money_used, 2, '.', ' ')}}</span>
                                        </div>

                                        <div class="col-sm-3 kt-widget12__info">
                                            <span class="kt-widget12__desc">Tổng số tiền đã nạp</span>
                                            <span class="kt-widget12__value">£{{number_format(@$money_add+@$money_used, 2, '.', ' ')}}</span>
                                        </div>

                                        <div class="col-sm-3 kt-widget12__info">
                                            <span class="kt-widget12__desc">Tổng số tiền chưa tiêu </span>
                                            <span class="kt-widget12__value">£{{number_format(@$money_add, 2, '.', ' ')}}</span>
                                        </div>
                                        <div class="col-sm-3 kt-widget12__info">
                                            <span class="kt-widget12__desc">Tổng số shop</span>
                                            <span class="kt-widget12__value">{{number_format(\App\Models\Admin::groupBy('last_company_id')->count())}}</span>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Finance Summary-->
            </div>
        </div>

        <div class="row" style="margin-top: 70px; margin-bottom: 70px;">
            <div class="kt-container  kt-container--fluid ">
                <div class="khoi-thong-ke">
                    <img src="http://card.vuasoft.com/Modules/CardBill2/Resources/assets/img/img-card.png"
                         class="img-fluid">
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 70px; margin-bottom: 70px;">
            <div class="kt-container  kt-container--fluid ">
                <!--begin::Advance Table Widget 4-->
                <div class="card card-custom card-stretch gutter-b">
                    <!--begin::Header-->
                    <div class="card-header border-0 py-3">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label font-weight-bolder text-dark">Bảng xếp hạng khách hàng</span>
                        </h3>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body pt-0 pb-3">
                        <div class="tab-content">
                            <!--begin::Table-->
                            <div class="table-responsive">
                                <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                                    <thead>
                                    <tr class="text-left text-uppercase">
                                        <th id="thName">Họ và tên</th>
                                        <th id="thPhone">Số điện thoại</th>
                                        <th id="thMoney">Tổng tiền đã tiêu</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="pl-0 py-8">
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-50 symbol-light mr-2">
                                                <span class="symbol-label">
                                                    <img src="http://card.vuasoft.com/Modules/CardBill2/Resources/assets/img/user1.svg"
                                                         class="h-75 align-self-end  w-40px" alt="">
                                                </span>
                                                </div>
                                                <div>
                                                    <a href="#"
                                                       class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Brad
                                                        Simmons</a>
                                                    <span class="text-muted font-weight-bold d-block">TOP 1</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="text-dark font-weight-bolder d-block font-size-lg">0123456789</span>
                                            <span class="text-muted font-weight-bold">+84(VN)</span>
                                        </td>
                                        <td>
                                            <span class="text-dark font-weight-bolder d-block font-size-lg">$1,000,000</span>
                                            <span class="text-muted font-weight-bold">USD</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="pl-0 py-8">
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-50 symbol-light mr-2">
                                                <span class="symbol-label">
                                                    <img src="http://card.vuasoft.com/Modules/CardBill2/Resources/assets/img/user2.svg"
                                                         class="h-75 align-self-end  w-40px" alt="">
                                                </span>
                                                </div>
                                                <div>
                                                    <a href="#"
                                                       class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Jessie
                                                        Clarcson</a>
                                                    <span class="text-muted font-weight-bold d-block">TOP 2</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="text-dark font-weight-bolder d-block font-size-lg">0123456789</span>
                                            <span class="text-muted font-weight-bold">+84(VN)</span>
                                        </td>
                                        <td>
                                            <span class="text-dark font-weight-bolder d-block font-size-lg">$50,600</span>
                                            <span class="text-muted font-weight-bold">AUD</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="pl-0 py-8">
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-50 symbol-light mr-2">
                                                <span class="symbol-label">
                                                    <img src="http://card.vuasoft.com/Modules/CardBill2/Resources/assets/img/user3.svg"
                                                         class="h-75 align-self-end  w-40px" alt="">
                                                </span>
                                                </div>
                                                <div>
                                                    <a href="#"
                                                       class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Lebron
                                                        Wayde</a>
                                                    <span class="text-muted font-weight-bold d-block">TOP 3</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="text-dark font-weight-bolder d-block font-size-lg">0123456789</span>
                                            <span class="text-muted font-weight-bold">+84(VN)</span>
                                        </td>
                                        <td>
                                            <span class="text-dark font-weight-bolder d-block font-size-lg">$30,700</span>
                                            <span class="text-muted font-weight-bold">SGD</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="pl-0 py-8">
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-50 symbol-light mr-2">
                                                <span class="symbol-label">
                                                    <img src="http://card.vuasoft.com/Modules/CardBill2/Resources/assets/img/user4.svg"
                                                         class="h-75 align-self-end  w-40px" alt="">
                                                </span>
                                                </div>
                                                <div>
                                                    <a href="#"
                                                       class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Natali
                                                        Trump</a>
                                                    <span class="text-muted font-weight-bold d-block">TOP 4</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-left pr-0">
                                            <span class="text-dark font-weight-bolder d-block font-size-lg">0123456789</span>
                                            <span class="text-muted font-weight-bold">+84(VN)</span>
                                        </td>
                                        <td>
                                            <span class="text-dark font-weight-bolder d-block font-size-lg">$14,000</span>
                                            <span class="text-muted font-weight-bold">AUD</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="pl-0 py-8">
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-50 symbol-light mr-2">
                                                <span class="symbol-label">
                                                    <img src="http://card.vuasoft.com/Modules/CardBill2/Resources/assets/img/user1.svg"
                                                         class="h-75 align-self-end w-40px" alt="">
                                                </span>
                                                </div>
                                                <div>
                                                    <a href="#"
                                                       class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Natali
                                                        Trump</a>
                                                    <span class="text-muted font-weight-bold d-block">TOP 5</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-left pr-0">
                                            <span class="text-dark font-weight-bolder d-block font-size-lg">0123456789</span>
                                            <span class="text-muted font-weight-bold">+84(VN)</span>
                                        </td>
                                        <td>
                                            <span class="text-dark font-weight-bolder d-block font-size-lg">$4,000</span>
                                            <span class="text-muted font-weight-bold">USD</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--end::Table-->
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Advance Table Widget 4-->
            </div>
        </div>
    </div>

@endsection
@section('custom_head')
    {{--    <link href="https://www.keenthemes.com/preview/metronic/theme/assets/global/css/components.min.css" rel="stylesheet"--}}
    {{--          type="text/css">--}}
    <style type="text/css">
        .kt-datatable__cell > span > a.cate {
            color: #5867dd;
            margin-bottom: 3px;
            background: rgba(88, 103, 221, 0.1);
            height: auto;
            display: inline-block;
            width: auto;
            padding: 0.15rem 0.75rem;
            border-radius: 2px;
        }

        .paginate > ul.pagination > li {
            padding: 5px 10px;
            border: 1px solid #ccc;
            margin: 0 5px;
            cursor: pointer;
        }

        .paginate > ul.pagination span {
            color: #000;
        }

        .paginate > ul.pagination > li.active {
            background: #0b57d5;
            color: #fff !important;
        }

        .paginate > ul.pagination > li.active span {
            color: #fff !important;
        }

        .kt-widget12__desc, .kt-widget12__value {
            text-align: center;
        }

        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99 list_user
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }

        .date-input {
            width: 130px;
        }

        .khoi-thong-ke {
            padding-top: 30px;
            padding-bottom: 30px;
            background-color: white;
        }

        .w-40px {
            width: 40px;
        }

        .symbol {
            background-color: #bcb8b8;
        }

        .ml-50px {
            margin-left: 50px;
        }

        th {
            vertical-align: middle !important;
        }

        #thMoney {
            min-width: 160px;
            font-weight: 700;
            font-size: 14px;
        }

        #thName {
            min-width: 140px;
            font-weight: 700;
            font-size: 14px;
        }

        #thPhone {
            min-width: 140px;
            font-weight: 700;
            font-size: 14px;
        }

    </style>
    <style type="text/css">
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }


        @media (max-width: 768px) {
            .kt-widget12 .kt-widget12__content .thong_ke_so {
                display: inline-block;
            }

            .thong_ke_so .col-sm-3 {
                display: inline-block;
                width: 50%;
                float: left;
                padding: 0;
                margin-bottom: 20px;
            }
        }
    </style>

@endsection
@push('scripts')
    <script src="{{ url('public/libs/chartjs/js/Chart.bundle.js') }}"></script>
    <script src="{{ url('public/libs/chartjs/js/utils.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script>
        $(document).ready(function () {
            $('#active_service a').click(function (event) {
                event.preventDefault();
                var object = $(this);
                $.ajax({
                    url: '/admin/service_history/ajax-publish',
                    data: {
                        id: object.data('service_history_id')
                    },
                    success: function (result) {
                        if (result.status == true) {
                            toastr.success(result.msg);
                            object.parents('tr').remove();
                        } else {
                            toastr.error(result.msg);
                        }
                    },
                    error: function (e) {
                        console.log(e.message);
                    }
                })
            });


        })
    </script>
    <script>
        var lineChartData = {
            labels: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
            datasets: [{
                label: 'Số lượng hóa đơn',
                borderColor: window.chartColors.red,
                backgroundColor: window.chartColors.red,
                fill: false,
                data: [
                    //    dữ liệu của số lượng hóa đơn
                    <?php $total_bill_month = [];
                    for ($i = 0; $i < 12; $i++) {
                        $month = strftime('%m', strtotime(strtotime(date('01')) . " +" . $i . " month"));
                        $total_bill_month[] = \Modules\CardBill2\Models\Bill::select('id')->where('created_at', 'like', '%-' . $month . '-%')
                            ->where('created_at', 'like', date('Y') . '-%')->get()->count();
                        echo($total_bill_month[$i] . ",");
                    }
                    ?>

                ],
                yAxisID: 'y-axis-1',
            }, {
                label: 'Số tiên thu được',
                borderColor: window.chartColors.blue,
                backgroundColor: window.chartColors.blue,
                fill: false,
                data: [
                    //    dữ liệu số tiên thu được theo tháng
                    <?php $total_price1 = [];
                    for ($i = 0; $i < 12; $i++) {
                        $month = strftime('%m', strtotime(strtotime(date('01')) . " +" . $i . " month"));
                        $total_price1[] = \Modules\CardBill2\Models\Bill::where('created_at', 'like', '%-' . $month . '-%')->where('created_at', 'like', date('Y') . '-%')->get()->sum('total_price');
                        echo($total_price1[$i] . ",");
                    }
                    ?>

                ],
                yAxisID: 'y-axis-2'
            }]
        };

        window.onload = function () {
            var ctx = document.getElementById('myChart').getContext('2d');
            window.myLine = Chart.Line(ctx, {
                data: lineChartData,
                options: {
                    responsive: true,
                    hoverMode: 'index',
                    stacked: false,
                    title: {
                        display: true,
                        // text: 'Chart.js Line Chart - Multi Axis'
                    },
                    scales: {
                        yAxes: [{
                            type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                            display: true,
                            position: 'left',
                            id: 'y-axis-1',
                            ticks: {
                                beginAtZero: true,
                            }

                        }, {
                            type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                            display: true,
                            position: 'right',
                            id: 'y-axis-2',
                            ticks: {
                                beginAtZero: true,
                            },
                            // grid line settings
                            gridLines: {
                                drawOnChartArea: false, // only want the grid lines for one axis to show up
                            },
                        }],
                    }
                }
            });
        };
    </script>
@endpush

