<?php

namespace Modules\CardBill2\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\CardBill2\Models\Card;
use Modules\CardBill2\Models\HistoryCard;
use Modules\CardBill2\Models\User;
use Validator;

class TransactionHistoryController extends CURDBaseController
{
    protected $module = [
        'code' => 'transaction_history',
        'table_name' => 'transaction_history',
        'label' => 'Lịch sử giao dịch',
        'modal' => '\Modules\CardBill2\Models\TransactionHistory',
        'list' => [
            ['name' => 'created_at', 'type' => 'datetime_vi', 'label' => 'Ngày giao dịch'],
            ['name' => 'card_code', 'type' => 'text_edit', 'label' => 'Mã thẻ'],
            ['name' => 'money', 'type' => 'custom', 'td' => 'cardbill2::transaction_history.list.td.money', 'label' => 'Số tiền'],
            ['name' => 'balance', 'type' => 'custom', 'td' => 'cardbill2::transaction_history.list.td.price_eur', 'label' => 'Số dư',],
            ['name' => 'user_name', 'type' => 'text', 'label' => 'Khách hàng'],
            ['name' => 'admin_id', 'type' => 'relation', 'label' => 'Người tạo', 'object' => 'admin', 'display_field' => 'name'],
            ['name' => 'error', 'type' => 'custom', 'td' => 'cardbill2::list.td.error', 'label' => 'Lỗi',],
            ['name' => 'company_id', 'type' => 'company', 'td' => 'text', 'label' => 'C.Ty',],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'card_id', 'type' => 'text', 'label' => 'ID thẻ'],
                ['name' => 'card_code', 'type' => 'text', 'label' => 'Mã thẻ'],
                ['name' => 'type', 'type' => 'select', 'options' => [
                    '-' => 'Trừ tiền',
                    '+' => 'nạp tiền'
                ], 'label' => 'Loại giao dịch', 'group_class' => 'col-md-6'],
                ['name' => 'money', 'type' => 'text', 'label' => 'Số tiền', 'group_class' => 'col-md-6'],
                ['name' => 'note', 'type' => 'textarea', 'label' => 'Ghi chú'],
                ['name' => 'balance', 'type' => 'text', 'label' => 'Số dư'],
                ['name' => 'error', 'type' => 'select', 'options' => [
                    0 => 'Không',
                    1 => 'Lỗi',
                ], 'label' => 'Giao dịch lỗi'],
                ['name' => 'admin_id', 'type' => 'custom', 'field' => 'cardbill2::transaction_history.form.admin_id', 'label' => 'Người thực hiện'],
            ],
            'info_tab' => [
                ['name' => 'user_name', 'type' => 'text', 'label' => 'Tên khách hàng'],
                ['name' => 'user_tel', 'type' => 'text', 'label' => 'SĐT khách'],
                ['name' => 'user_address', 'type' => 'text', 'label' => 'Địa chỉ khách'],
            ],

        ],
    ];

    protected $quick_search = [
        'label' => 'ID, card_code, money, note, balance, user_name, user_tel, user_address',
        'fields' => 'id, card_code, money, note, balance, user_name, user_tel, user_address, company_id'
    ];

    protected $filter = [
//
//        'card_id' => [
//            'label' => 'Mã thẻ',
//            'type' => 'select2_ajax_model',
//            'display_field' => 'code',
//            'model' => Card::class,
//            'object' => 'card',
//            'query_type' => '='
//        ],
//        'money' => [
//            'label' => 'Số tiền nạp',
//            'type' => 'number',
//            'query_type' => 'like'
//        ],
        'created_at' => [
            'label' => 'Thời gian',
            'type' => 'from_to_date',
            'query_type' => 'from_to_date'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('cardbill2::transaction_history.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        //  Nếu không phải super_admin thì nó chỉ được xem dữ liệu shop nó tạo
        if(\Auth::guard('admin')->user()->super_admin != 1) {
            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        }
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('cardbill2::transaction_history.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'card_id' => 'required'
                ], [
                    'user_id.required' => 'Bắt buộc phải chọn khách hàng',
                    'card_id.required' => 'Bắt buộc phải chọn mã thẻ',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
//                    $data['type'] = 1;
                    #
                    $data['admin_id'] = \Auth::guard('admin')->user()->id;
                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache();
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('cardbill2::transaction_history.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'card_id' => 'required'
                ], [
                    'user_id.required' => 'Bắt buộc phải chọn khách hàng',
                    'card_id.required' => 'Bắt buộc phải chọn mã thẻ',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache();
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache();
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
