<?php

namespace Modules\CardBill2\Models ;

use Illuminate\Database\Eloquent\Model;
use Modules\CardBill2\Models\Admin;

class TransactionHistory extends Model
{

    protected $table = 'transaction_history';
    protected $guarded = [];

    public function card() {
        return $this->belongsTo(Card::class, 'card_id');
    }
    public function admin() {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
}

