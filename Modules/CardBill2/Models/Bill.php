<?php

namespace Modules\CardBill2\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CardUser\Models\UserAddress;
use Modules\ThemeCard\Models\User;

class Bill extends Model
{
    use SoftDeletes;

    protected $table = 'bills';

    protected $guarded = [];

    protected $dates = ['deleted_at'];
    protected $softDelete = true;
//    protected $fillable = [
//        'receipt_method' , 'user_gender', 'date' , 'coupon_code' , 'note' , 'status' , 'total_price' , 'user_id', 'user_tel', 'user_name', 'user_email', 'user_address', 'user_wards', 'user_city_id'
//    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function admin() {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function card() {
        return $this->belongsTo(Card::class, 'card_id');
    }

}
