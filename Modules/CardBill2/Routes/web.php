<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    //  Role
    Route::group(['prefix' => 'role'], function () {
        Route::get('', 'Admin\RoleController@getIndex')->name('role')->middleware('permission:role_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\RoleController@add')->middleware('permission:role_add');
        Route::get('delete/{id}', 'Admin\RoleController@delete')->middleware('permission:role_delete');
        Route::post('multi-delete', 'Admin\RoleController@multiDelete')->middleware('permission:role_delete');
        Route::get('search-for-select2', 'Admin\RoleController@searchForSelect2')->name('role.search_for_select2')->middleware('permission:role_view');
        Route::match(array('GET', 'POST'), '{id}', 'Admin\RoleController@update')->middleware('permission:role_edit');
    });


    //  Admin
    Route::group(['prefix' => 'admin'], function () {

        Route::get('', 'Admin\AdminController@getIndex')->name('admin')->middleware('permission:admin_view');
        Route::get('publish', 'Admin\AdminController@getPublish')->name('admin.admin_publish')->middleware('permission:admin_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\AdminController@add')->middleware('permission:admin_add');
        Route::get('delete/{id}', 'Admin\AdminController@delete')->middleware('permission:admin_delete');
        Route::post('multi-delete', 'Admin\AdminController@multiDelete')->middleware('permission:admin_delete');

        Route::get('search-for-select2', 'Admin\AdminController@searchForSelect2')->name('admin.search_for_select2')->middleware('permission:admin_view');
        Route::get('search-for-select2-all', 'Admin\AdminController@searchForSelect2All')->middleware('permission:admin_view');

        Route::get('{id}', 'Admin\AdminController@update')->middleware('permission:admin_view');
        Route::post('{id}', 'Admin\AdminController@update')->middleware('permission:admin_edit');
    });

    //   Lịch sử lỗi
    Route::group(['prefix' => 'admin_logs'], function () {
        Route::get('', 'Admin\AdminLogController@getIndex')->middleware('permission:admin_log_view');
    });
});


Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {


    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'Admin\DashboardController@dashboard');
        Route::get('search', 'Admin\DashboardController@search');
    });

    Route::group(['prefix' => 'bill'], function () {
        Route::get('', 'Admin\BillController@getIndex')->name('bill')->middleware('permission:bill_view');
        Route::get('publish', 'Admin\BillController@getPublish')->name('bill.publish')->middleware('permission:bill_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BillController@add')->middleware('permission:bill_add');
        Route::get('delete/{id}', 'Admin\BillController@delete')->middleware('permission:bill_delete');
        Route::post('multi-delete', 'Admin\BillController@multiDelete')->middleware('permission:bill_delete');
        Route::get('search-for-select2', 'Admin\BillController@searchForSelect2')->name('bill.search_for_select2')->middleware('permission:bill_view');
        Route::get('user/get-info-by-card_id', 'Admin\BillController@getInfoByCard')->name('bill.user_info');

        Route::post('send-sms-verify', 'Admin\BillController@send_sms_verify');
        Route::get('send-verify-sms', 'Admin\BillController@send_verify_sms');

        Route::get('{id}', 'Admin\BillController@update')->middleware('permission:bill_view');
        Route::post('{id}', 'Admin\BillController@update')->middleware('permission:bill_edit');
    });
    
    Route::group(['prefix' => 'card'], function () {
        Route::get('', 'Admin\CardController@getIndex')->name('card')->middleware('permission:card_view');
        Route::get('publish', 'Admin\CardController@getPublish')->name('card.publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\CardController@add')->middleware('permission:card_add');
        Route::get('delete/{id}', 'Admin\CardController@delete')->middleware('permission:card_delete');
        Route::post('multi-delete', 'Admin\CardController@multiDelete')->middleware('permission:card_delete');
        Route::get('search-for-select2', 'Admin\CardController@searchForSelect2')->name('card.search_for_select2')->middleware('permission:card_view');
        Route::get('{id}', 'Admin\CardController@update')->middleware('permission:card_view');
        Route::post('{id}', 'Admin\CardController@update')->middleware('permission:card_edit');
    });

    Route::group(['prefix' => 'transaction_history'], function () {
        Route::get('', 'Admin\TransactionHistoryController@getIndex')->name('transaction_history')->middleware('permission:transaction_history_view');
        Route::get('{id}', 'Admin\TransactionHistoryController@update')->middleware('permission:transaction_history_view');
    });
});

