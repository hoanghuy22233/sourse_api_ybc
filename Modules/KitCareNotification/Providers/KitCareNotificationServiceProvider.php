<?php

namespace Modules\KitCareNotification\Providers;

use App\Models\Setting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class KitCareNotificationServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
//        $this->registerTranslations();
//        $this->registerConfig();
        $this->registerViews();
//        $this->registerFactories();
//        $this->loadMigrationsFrom(module_path('KitCareNotification', 'Database/Migrations'));

        $this->settingOnesignal();
//        $this->setConfigOnesignal();
    }

    public function settingOnesignal() {
        //  Add to setting
        \Eventy::addFilter('setting.custom_module', function ($module) {
            $module['tabs']['onesignal_tab'] = [
                'label' => 'Cấu hình thông báo Onesignal',
                'icon' => '<i class="flaticon-mail"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'onesignal_app_id', 'type' => 'text', 'label' => 'App id',],
                    ['name' => 'onesignal_rest_api_key', 'type' => 'text', 'label' => 'Rest api key',],
                ]
            ];
            return $module;
        }, 1, 1);
        return true;
    }

    public function setConfigOnesignal() {
        $settings = Setting::whereIn('type', ['onesignal_tab'])->pluck('value', 'name')->toArray();

        $config['services.onesignal'] =
            [
                'app_id' => trim(@$settings['onesignal_app_id']),
                'rest_api_key' => trim(@$settings['onesignal_rest_api_key']),
            ];
        config($config);
        return $settings;
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('KitCareNotification', 'Config/config.php') => config_path('kitcarenotification.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('KitCareNotification', 'Config/config.php'), 'kitcarenotification'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/kitcarenotification');

        $sourcePath = module_path('KitCareNotification', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/kitcarenotification';
        }, \Config::get('view.paths')), [$sourcePath]), 'kitcarenotification');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/kitcarenotification');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'kitcarenotification');
        } else {
            $this->loadTranslationsFrom(module_path('KitCareNotification', 'Resources/lang'), 'kitcarenotification');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('KitCareNotification', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
