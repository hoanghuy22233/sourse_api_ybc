<?php

namespace Modules\SmartHome\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\A4iOrganization\Models\Organization;
use Illuminate\Http\Request;
use Modules\A4iLocation\Models\Province;
use Modules\A4iLocation\Models\Ward;
use Modules\KitCareBooking\Models\TagBooking;
use Modules\SmartHome\Models\LogRelay;
use Validator;

class LogRelayController extends Controller
{

    protected $module = [
        'code' => 'log_relay',
        'table_name' => 'log_relays',
        'label' => 'log_relays',
        'modal' => 'Modules\SmartHome\Models\LogRelay',
    ];

    protected $filter = [
        'rid' => [
            'query_type' => 'like'
        ],
        'ip' => [
            'query_type' => 'like'
        ],
        'rn' => [
            'query_type' => 'like'
        ],
        'rs' => [
            'query_type' => 'like'
        ],
        'from_date' => [
            'query_type' => 'from_to_date'
        ],
        'to_date' => [
            'query_type' => 'from_to_date'
        ],
    ];

    public function index(Request $request)
    {
        try {
            //  Filter
            $where = $this->filterSimple($request);
            $listItem = LogRelay::whereRaw($where);

            //  Sort
            $listItem = $this->sort($request, $listItem);
            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = LogRelay::selectRaw('log_relays.id, log_relays.rid, log_relays.ip, log_relays.rn, log_relays.rs, log_relays.created_at')->where('log_relays.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }


            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
//            'rid' => 'required'
        ], [
//            'rid.required' => 'Bắt buộc phải nhập mã phòng',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
//            if ($request->has('image')) {
//                if (is_array($request->file('image'))) {
//                    foreach ($request->file('image') as $image) {
//                        $data['image'] = CommonHelper::saveFile($image, 'disease');
//                    }
//                } else {
//                    $data['image'] = CommonHelper::saveFile($request->file('image'), 'disease');
//                }
//            }
//
//            if ($request->has('disease_image')) {
//                if (is_array($request->file('disease_image'))) {
//                    foreach ($request->file('disease_image') as $image) {
//                        $data['disease_image'] = CommonHelper::saveFile($image, 'disease');
//                    }
//                } else {
//                    $data['disease_image'] = CommonHelper::saveFile($request->file('disease_image'), 'disease');
//                }
//            }

            $item = LogRelay::create($data);


            return $this->show($item->id);
        }
    }


    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {

                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . ".created_at >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . ".created_at <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
