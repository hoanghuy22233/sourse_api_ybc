<?php

namespace Modules\SmartHome\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\SmartHome\Models\Booking;
use Validator;

class LogRelayController extends CURDBaseController
{
    protected $module = [
        'code' => 'log_relay',
        'table_name' => 'log_relays',
        'label' => 'LogRelay',
        'modal' => '\Modules\SmartHome\Models\LogRelay',
        'list' => [
            ['name' => 'rid', 'type' => 'text_edit', 'label' => 'Tên thiết bị'],
            ['name' => 'ip', 'type' => 'select','options' => [
                '0.0.0.0' => 'Khởi động',
            ], 'label' => 'IP máy điều khiển'],
            ['name' => 'rn', 'type' => 'text', 'label' => 'Công tắc'],
            ['name' => 'rs', 'type' => 'select','options' => [
                0 => 'Tắt',
                1 => 'Bật'
            ] , 'label' => 'Trạng thái'],
            ['name' => 'created_at', 'type' => 'datetime_vi', 'label' => 'Ngày tạo'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'rid', 'type' => 'text', 'class' => 'required', 'label' => 'Mã phòng'],
                ['name' => 'ip', 'type' => 'text', 'class' => '', 'label' => 'Địa chỉ IP của thiết bị'],
                ['name' => 'rn', 'type' => 'text', 'class' => '', 'label' => 'Relay Name'],
                ['name' => 'rs', 'type' => 'text', 'class' => '', 'label' => 'Relay State'],


            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, mã phòng, ip, relay name,...',
        'fields' => 'id, rid, ip, rn, rs'
    ];

    protected $filter = [
        'rid' => [
            'label' => 'Tên thiết bị',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'ip' => [
            'label' => 'IP máy điều khiển',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'rn' => [
            'label' => 'Công tắc',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'rs' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'options' => [
                0 => 'Tắt',
                1 => 'Bật'
            ],
            'query_type' => 'like'
        ],
        'created_at' => [
            'label' => 'Từ ngày',
            'type' => 'from_to_date',
            'query_type' => 'from_to_date'
        ],

//        'rn' => [
//            'label' => 'Relay Name',
//            'type' => 'select',
//            'query_type' => '=',
//            'options' => [
//                '' => 'Relay Name',
//                1 => 'Tương ứng khi điều khiển THIẾT BỊ 1',
//                2 => 'Tương ứng khi điều khiển THIẾT BỊ 2',
//                3 => 'Tương ứng khi điều khiển THIẾT BỊ 3',
//                4 => 'Tương ứng khi điều khiển THIẾT BỊ 4',
//                5 => 'Tương ứng khi điều khiển THIẾT BỊ 5',
//                6 => 'Tương ứng khi điều khiển THIẾT BỊ 6',
//                7 => 'Tương ứng khi điều khiển THIẾT BỊ 7',
//                8 => 'Tương ứng khi điều khiển THIẾT BỊ 8',
//                9 => 'Tương ứng khi điều khiển TẤT CẢ',
//            ],
//        ],
//        'rs' => [
//            'label' => 'Trạng thái',
//            'type' => 'select',
//            'query_type' => '=',
//            'options' => [
//                '' => 'Trạng thái',
//                0 => 'Không kích hoạt',
//                1 => 'Kích hoạt',
//            ],
//        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        if (@$data['action'] == 'direct') {
            return redirect($data['url']);
        }

        return view('smarthome::list')->with($data);
    }

    public function getDataList(Request $request) {
        //  Filter
        $where = $this->filterSimple($request);
        $listItem = $this->model->whereRaw($where);
        $listItem = $this->quickSearch($listItem, $request);
        if ($this->whereRaw) {
            $listItem = $listItem->whereRaw($this->whereRaw);
        }
        $listItem = $this->appendWhere($listItem, $request);

        //  Export
        if ($request->has('export')) {
            $this->exportExcel($request, $listItem->get());
        }

        //  Xóa theo bộ lọc
        if ($request->has('delete-by-filter')) {
            $listItem->delete();

            CommonHelper::one_time_message('success', 'Đã xóa bản ghi theo bộ lọc thh công!');
            return [
                'action' => 'direct',
                'url' => 'admin/' . $this->module['code']
            ];
        }

        //  Sort
        $listItem = $this->sort($request, $listItem);
        if ($request->has('limit')) {
            $data['listItem'] = $listItem->paginate($request->limit);
            $data['limit'] = $request->limit;
        } else {
            $data['listItem'] = $listItem->paginate($this->limit_default);
            $data['limit'] = $this->limit_default;
        }
        $data['page'] = $request->get('page', 1);

        $data['param_url'] = $request->all();

        //  Get data default (param_url, filter, module) for return view
        $data['module'] = $this->module;
        $data['quick_search'] = $this->quick_search;
        $data['filter'] = $this->filter;
        if ($this->whereRaw) {
            $data['record_total'] = $this->model->whereRaw($this->whereRaw);
        } else {
            $data['record_total'] = $this->model;
        }

        $data['record_total'] = $data['record_total']->whereRaw($where)->count();

        //  Set data for seo
        $data['page_title'] = $this->module['label'];
        $data['page_type'] = 'list';
        return $data;
    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('smarthome::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'rid' => 'required'
                ], [
                    'rid.required' => 'Bắt buộc phải nhập Mã phòng',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    #

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('smarthome::edit')->with($data);
            } else if ($_POST) {



                $validator = Validator::make($request->all(), [
                    'rid' => 'required'
                ], [
                    'rid.required' => 'Bắt buộc phải nhập Mã phòng',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;


                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }



    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }


    public function multiDelete(Request $request)
    {
        try {



            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {

                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . ".created_at >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . ".created_at <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }
}
