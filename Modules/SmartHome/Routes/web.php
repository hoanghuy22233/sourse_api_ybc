
<?php
Route::get('admin/login', function () {
    return redirect('/');
});
Route::get('/', function () {
    if (!\Auth::guard('admin')->check()) {
        \Auth::guard('admin')->login(\App\Models\Admin::find(1));
    }
    return redirect('/admin/log_relay');
});
Route::get('admin/dashboard', function () {
    return redirect('/admin/log_relay');
});
Route::group(['prefix' => 'admin', 'middleware' => ['get_permissions']], function () {

    Route::group(['prefix' => 'log_relay'], function () {
        Route::get('', 'Admin\LogRelayController@getIndex')->name('log_relay');
        Route::get('publish', 'Admin\LogRelayController@getPublish')->name('log_relay.publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\LogRelayController@add');
        Route::get('delete/{id}', 'Admin\LogRelayController@delete');
        Route::post('multi-delete', 'Admin\LogRelayController@multiDelete');
        Route::get('search-for-select2', 'Admin\LogRelayController@searchForSelect2')->name('log_relay.search_for_select2');
        Route::get('{id}', 'Admin\LogRelayController@update');
        Route::post('{id}', 'Admin\LogRelayController@update');

    });
});

