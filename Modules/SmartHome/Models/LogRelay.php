<?php
namespace Modules\SmartHome\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogRelay extends Model
{

    protected $table = 'log_relays';



    protected $fillable = [
        'id','rid', 'ip', 'rn','rs','created_at','updated_at',
    ];



}
