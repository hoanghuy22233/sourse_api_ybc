<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',255)->comment('Lưu tên folder');
            $table->text('description')->comment('Lưu mô tả nhập vào');
            $table->integer('regular_price')->comment('Lưu giá nhập của từng size');
            $table->string('tags',255)->comment('Lưu tag nhập vào ');
            $table->string('category',255)->comment('Lưu category nhập vào');
            $table->string('color',255)->comment('Lưu từng màu nhập trong ô text');
            $table->string('size',255)->comment('Lưu từng size');
            $table->string('image',255)->comment('Lưu tên ảnh');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
