<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {

    Route::group(['prefix' => 'product'], function () {
        Route::get('', 'ProductExportTshirtController@getIndex')->name('product');
        Route::get('publish', 'ProductExportTshirtController@getPublish')->name('product.publish');
        Route::match(array('GET', 'POST'), 'add', 'ProductExportTshirtController@add');
        Route::get('delete/{id}', 'ProductExportTshirtController@delete');
        Route::post('multi-delete', 'ProductExportTshirtController@multiDelete');
        Route::get('search-for-select2', 'ProductExportTshirtController@searchForSelect2')->name('product.search_for_select2');
        Route::get('export-excel', 'ProductExportTshirtController@export')->name('product.export');
        Route::get('get-data-export', 'ProductExportTshirtController@getDataExport')->name('product.getdataexport');
        Route::get('{id}', 'ProductExportTshirtController@update');
        Route::post('{id}', 'ProductExportTshirtController@update');
    });
    Route::group(['prefix' => 'color-setting'], function () {
        Route::match(['get', 'post'], '', 'ColorSettingTshirtController@setting')->name('color.color_setting');
    });
});