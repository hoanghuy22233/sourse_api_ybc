<?php

namespace Modules\ExportTShirt\Entities;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];
}
