<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'trace_element'], function () {
        Route::get('', 'Admin\TraceElementController@getIndex')->middleware('permission:trace_element_view');
        Route::get('publish', 'Admin\TraceElementController@getPublish')->name('season.publish')->middleware('permission:trace_element_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TraceElementController@add')->middleware('permission:trace_element_add');
        Route::get('delete/{id}', 'Admin\TraceElementController@delete')->middleware('permission:trace_element_delete');
        Route::post('multi-delete', 'Admin\TraceElementController@multiDelete')->middleware('permission:trace_element_delete');
        Route::get('search-for-select2', 'Admin\TraceElementController@searchForSelect2')->name('season.search_for_select2')->middleware('permission:trace_element_view');
        Route::get('{id}', 'Admin\TraceElementController@update')->middleware('permission:trace_element_edit');
        Route::post('{id}', 'Admin\TraceElementController@update')->middleware('permission:trace_element_edit');
    });
});