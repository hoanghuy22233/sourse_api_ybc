<?php

namespace Modules\ThemeLogistics\Providers;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ThemeLogisticsServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //  Custom setting
//        $this->registerTranslations();
//        $this->registerConfig();
        $this->registerViews();
//        $this->registerFactories();
//        $this->loadMigrationsFrom(module_path('ThemeLogistics', 'Database/Migrations'));

        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Cấu hình menu trái
//            $this->rendAsideMenu();
        }

        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/setting') !== false) {
            $this->customSetting();
        }

        //  Cầu hình frontend
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') === false) {
            $this->frontendSettings();
        }
    }

    public function frontendSettings()
    {
        $settings = CommonHelper::getFromCache('frontned_settings');
        if (!$settings) {
            $settings = Setting::whereIn('type', ['general_tab', 'common_tab', 'seo_tab', 'homepage_tab', 'product_page_tab'])->pluck('value', 'name')->toArray();
            CommonHelper::putToCache('frontned_settings', $settings);
        }

        \View::share('settings', $settings);
        return $settings;
    }

    public function customSetting()
    {

    }

    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('themelogistics::admin.partials.aside_menu.dashboard_after');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('ThemeLogistics', 'Config/config.php') => config_path('themelogistics.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('ThemeLogistics', 'Config/config.php'), 'themelogistics'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/themelogistics');

        $sourcePath = module_path('ThemeLogistics', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/themelogistics';
        }, \Config::get('view.paths')), [$sourcePath]), 'themelogistics');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/themelogistics');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'themelogistics');
        } else {
            $this->loadTranslationsFrom(module_path('ThemeLogistics', 'Resources/lang'), 'themelogistics');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('ThemeLogistics', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    /*public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['showroom_view', 'showroom_delete', 'showroom_edit', 'showroom_add', 'product_publish'
            ]);
            return $per_check;
        }, 1, 1);
    }*/
}
