<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */

namespace Modules\ThemeLogistics\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeLogistics\Models\Author;
use Modules\ThemeLogistics\Models\Category;
use Modules\ThemeLogistics\Models\Company;
use Modules\ThemeLogistics\Models\Manufacturer;
use Modules\ThemeLogistics\Models\Menu;
use Modules\ThemeLogistics\Models\Origin;
use Modules\ThemeLogistics\Models\Product;
use Modules\ThemeLogistics\Models\Post;
use Modules\ThemeLogistics\Models\Settings;
use Modules\ThemeLogistics\Models\Showroom;
use Modules\ThemeLogistics\Models\ProductSale;
use function GuzzleHttp\Promise\all;
use function GuzzleHttp\Psr7\str;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function PHPSTORM_META\type;

class CategoryController extends Controller
{
    public $result = '';

    public function getIndex($slug, Request $request)
    {

        if (strpos($slug, '.html')) {
            $slug = str_replace('.html', '', $slug);
            $product = Product::where('slug', $slug)->where('status', 1)->first();
            if (is_object($product)) {
                $productController = new ProductController();
                return $productController->getDetail('', $slug, $request, $product);
            } else {
                $postController = new PostController();
                return $postController->getDetail($slug, '');
            }
        }

        $data['category'] = CommonHelper::getFromCache('category_slug' . $slug);
        if (!$data['category']) {
            $data['category'] = Category::where('slug', $slug)->where('status', 1)->first();
            CommonHelper::putToCache('category_slug' . $slug, $data['category']);
        }

        if (!is_object($data['category'])) {
            abort(404);
        }

        $data['posts'] = Post::select('id', 'name', 'image', 'slug', 'category_id', 'multi_cat', 'created_at')
            ->where('status', 1)
            ->where('multi_cat', 'like', '%|'.$data['category']->id.'|%')
            ->orderBy('order_no', 'desc')
            ->orderBy('id', 'desc')->paginate(11);
        $pageOption = [
            'title' => @$data['category']->meta_title != '' ? @$data['category']->meta_title : @$data['category']->name,
            'description' => @$data['category']->meta_description != '' ? @$data['category']->meta_description : @$data['category']->name,
            'keywords' => @$data['category']->meta_keywords != '' ? @$data['category']->meta_keywords : @$data['category']->name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themelogistics::childs.post.list')->with($data);
    }

    function getAllPluckProduct($category)
    {
        $getIdCate = CommonHelper::getFromCache('get_all_index');
        if (!$getIdCate) {
            $getIdCate = Product::all()->pluck('multi_cat', 'id');
            CommonHelper::putToCache('get_all_index', $getIdCate);
        }

        $data_id = '|';
        if ($category) {
            if ($category->parent_id == 0 || $category->parent_id == null) {

                $data['category_childs'] = CommonHelper::getFromCache('category_parent_id' . $category->id);
                if (!$data['category_childs']) {
                    $data['category_childs'] = Category::where('parent_id', $category->id)->get();
                    CommonHelper::putToCache('category_parent_id' . $category->id, $data['category_childs']);
                }

                $data['category_childs']->push($category);
                foreach ($data['category_childs'] as $val) {
                    foreach ($getIdCate as $key => $val1) {
                        $t = explode('|', $val1);
                        if (in_array($val->id, $t) == true) {
                            $data_id .= $key . '|';
                        }
                    }
                }
            } else {
                foreach ($getIdCate as $key => $val1) {
                    $t = explode('|', $val1);
                    if (in_array($category->id, $t) == true) {
                        $data_id .= $key . '|';
                    }
                }
            }
        }
        $data_id = explode('|', $data_id);
        $data_id = array_unique($data_id);
        $arrayCate_id = [];
        if (!empty($category)) {
            array_push($arrayCate_id, $category->id);
        }
        if (!isset($data['category_childs'])) {
            $data['category_childs'] = null;
        }
        return [$data_id, $arrayCate_id, $data['category_childs']];
    }

    function getAllPluckPost($category)
    {
        $getIdCate = CommonHelper::getFromCache('get_all_index');
        if (!$getIdCate) {
            $getIdCate = Post::all()->pluck('multi_cat', 'id');
            CommonHelper::putToCache('get_all_index', $getIdCate);
        }
        $data_id = '|';
        if ($category) {
            if ($category->parent_id == 0 || $category->parent_id == null) {
                $data['category_childs'] = CommonHelper::getFromCache('category_parent_ids' . $category->id);
                if (!$data['category_childs']) {
                    $data['category_childs'] = Category::where('parent_id', $category->id)->get();
                    CommonHelper::putToCache('category_parent_ids' . $category->id, $data['category_childs']);
                }

                $data['category_childs']->push($category);
                foreach ($data['category_childs'] as $val) {
                    foreach ($getIdCate as $key => $val1) {
                        $t = explode('|', $val1);
                        if (in_array($val->id, $t) == true) {
                            $data_id .= $key . '|';
                        }
                    }
                }
            } else {
                foreach ($getIdCate as $key => $val1) {
                    $t = explode('|', $val1);
                    if (in_array($category->id, $t) == true) {
                        $data_id .= $key . '|';
                    }
                }
            }
        }
        $data_id = explode('|', $data_id);
        $data_id = array_unique($data_id);
        $arrayCate_id = [];
        if (!empty($category)) {
            array_push($arrayCate_id, $category->id);
        }
        if (!isset($data['category_childs'])) {
            $data['category_childs'] = null;
        }
        return [$data_id, $arrayCate_id, $data['category_childs']];
    }

    function getArrayIdProduct($getData, $getPrs)
    {
        $getDataArr = explode(',', $getData);
        $p = '';
        foreach ($getPrs as $product) {
            $proprerties_id = explode('|', $product->proprerties_id);
            $proprerties_id['0'] ? '' : array_shift($proprerties_id);
            end($proprerties_id) ? '' : array_pop($proprerties_id);
            $r = array_diff($getDataArr, $proprerties_id);
            if ($r == []) {
                $p .= $product->id . "|";
            }
        }
        $p = explode('|', $p);
        if (end($p) == '') {
            array_pop($p);
        }
        return $p;
    }

    function getProductCate($getData, $data_id)
    {
        $getPrs = CommonHelper::getFromCache('product_type1_in_ids' . implode('|', $data_id));

        if (!$getPrs) {
            $getPrs = Product::whereIn('id', $data_id)->where('status', 1)->where('type', 1)->get();
            CommonHelper::putToCache('product_type1_in_ids' . implode('|', $data_id), $getPrs);
        }
        return $getPrs;
    }

    function getProductCateAndManu($getData, $data_id, $manufacturer)
    {
        $getPrs = CommonHelper::getFromCache('product_type1_in_ids' . implode('|', $data_id) . $manufacturer->id);
        if (!$getPrs) {
            $getPrs = Product::whereIn('id', $data_id)->where('manufacture_id', $manufacturer->id)->where('status', 1)->where('type', 1)->get();
            CommonHelper::putToCache('product_type1_in_ids' . implode('|', $data_id) . $manufacturer->id, $getPrs);
        }
        return $getPrs;
    }

    function getDataAjaxCate($getData, $pageShow, $category, $data_id, $request)
    {

        $getPrs = $this->getProductCate($getData, $data_id);

        if ($getData == null) {
            $products = CommonHelper::getFromCache('get_product_by_' . implode('|', $data_id) . 'limit' . $pageShow);
            if (!$products) {
                $products = Product::whereIn('id', $data_id);
                //  Sắp xếp tăng giảm giá
                $products = $this->filterCategory($products, $request);
                $products = $products->orderBy('order_no', 'desc')->orderBy('id', 'desc')->where('status', 1)->where('type', 1)->orderBy('order_no', 'asc')->limit($pageShow)->get();
                CommonHelper::putToCache('get_product_by_' . implode('|', $data_id) . 'limit' . $pageShow, $products);
            }
            $productNolimit = CommonHelper::getFromCache('get_product_noLimit_by' . $getData);
            if (!$productNolimit) {
                $productNolimit = Product::whereIn('id', $data_id)->where('status', 1)->where('type', 1)->get();
                CommonHelper::putToCache('get_product_noLimit_by' . $getData, $productNolimit);
            }
        } else {
            $p = $this->getArrayIdProduct($getData, $getPrs);
            $products = CommonHelper::getFromCache('get_products_by' . implode('|', $p) . 'limit' . $pageShow);
            if (!$products) {
                $products = Product::whereIn('id', $p);
                //  Sắp xếp tăng giảm giá
                $products = $this->filterCategory($products, $request);
                $products = $products->orderBy('order_no', 'desc')->orderBy('id', 'desc')->limit($pageShow)->get();
                CommonHelper::putToCache('get_products_by' . implode('|', $p) . 'limit' . $pageShow, $products);
            }

            $productNolimit = CommonHelper::getFromCache('produdct_in_ids' . implode('|', $p));
            if (!$productNolimit) {
                $productNolimit = Product::whereIn('id', $p)->get();
                CommonHelper::putToCache('produdct_in_ids' . implode('|', $p), $productNolimit);
            }
        }
        return $this->getDataProductAjax($products, $productNolimit, $category);
    }

    public function filterCategory($query, $r) {
        if (@$r->sort_prd_price == 1) {
            $query = $query->orderBy('final_price', 'desc');
        } elseif (@$r->sort_prd_price == 0 && $r->sort_prd_price != null) {
            $query = $query->orderBy('final_price', 'asc');
        }
        return $query;
    }

    function getDataAjaxCateAndManu($getData, $pageShow, $category, $data_id, $manufacturer)
    {
        $getPrs = $this->getProductCateAndManu($getData, $data_id, $manufacturer);
        if ($getData == null) {
            $products = CommonHelper::getFromCache('get_product_by_' . implode('|', $data_id) . 'manufacture_id' . $manufacturer->id);
            if (!$products) {
                $products = Product::whereIn('id', $data_id)->where('manufacture_id', $manufacturer->id)->orderBy('id', 'desc')->where('status', 1)->where('type', 1)->limit($pageShow)->get();
                CommonHelper::putToCache('get_product_by_' . implode('|', $data_id) . 'manufacture_id' . $manufacturer->id, $products);
            }

            $productNolimit = CommonHelper::getFromCache('get_product_noLimit_by' . implode('|', $data_id) . 'manufacture_id' . $manufacturer->id);
            if (!$productNolimit) {
                $productNolimit = Product::whereIn('id', $data_id)->where('manufacture_id', $manufacturer->id)->where('status', 1)->where('type', 1)->get();
                CommonHelper::putToCache('get_product_noLimit_by' . implode('|', $data_id) . 'manufacture_id' . $manufacturer->id, $productNolimit);
            }
        } else {
            $p = $this->getArrayIdProduct($getData, $getPrs);
            $products = CommonHelper::getFromCache('get_products_by' . implode('|', $p) . 'manufacture_id' . $manufacturer->id);
            if (!$products) {
                $products = Product::whereIn('id', $p)->where('manufacture_id', $manufacturer->id)->orderBy('id', 'desc')->where('status', 1)->where('type', 1)->orderBy('order_no', 'asc')->limit($pageShow)->get();
                CommonHelper::putToCache('get_products_by' . implode('|', $p) . 'manufacture_id' . $manufacturer->id, $products);
            }

            $productNolimit = CommonHelper::getFromCache('get_productsNolimit_by' . implode('|', $p) . 'manufacture_id' . $manufacturer->id);
            if (!$productNolimit) {
                $productNolimit = Product::whereIn('id', $p)->where('manufacture_id', $manufacturer->id)->get();
                CommonHelper::putToCache('get_productsNolimit_by' . implode('|', $p) . 'manufacture_id' . $manufacturer->id, $productNolimit);
            }
        }
        return $this->getDataProductAjax($products, $productNolimit, $category);
    }

    function getDataProductAjax($products, $productNolimit, $category)
    {
        $settings = Settings::where('name', 'logo_qua_tang')->pluck('value', 'name')->toArray();
        foreach ($products as $product) {
            $manufacture = CommonHelper::getFromCache('manufacture_id' . $product->manufacture_id);
            if (!$manufacture) {
                $manufacture = Manufacturer::find($product->manufacture_id);
                CommonHelper::putToCache('manufacture_id' . $product->manufacture_id, $manufacture);
            }

            if ($product->final_price < $product->base_price) {
                $base_price = '<span style="padding-top: 12px;"><u>' . number_format($product->base_price, 0, '.', '.') . '</u><sup style="font-size: 18px;"> đ</sup></span>';
                $priceName = '';
            } else {
                $base_price = '<div style="height: 32px;"></div>';
                $priceName = 'Giá bán ';
            }
            if ($product->final_price > 0) {
                $price = '<span style="padding: 0px 2px;">' . $priceName . '<b>' . number_format($product->final_price, 0, '.', '.') . '&nbsp;<sup>đ</sup></b></span>';
            } else {
                $price = '</br> <span class="pr">Liên hệ</span>';
            }
            if ($product->final_price < $product->base_price) {
                $priceCheck = '<span> Giảm: ' . number_format($product->base_price - $product->final_price, 0, '.', '.') . '&nbsp;<sup > đ</sup ></span >';
            } else {
                $priceCheck = '<div style = "height: 24px;border-top: none" ></div>';
            }
            $sale = [];
            $getDateToday = date_create(date('Y-m-d H:i:s'));
            $product_all = ProductSale::where(function ($query) use ($getDateToday) {
                $query->orWhere('time_start', '<=', date_format($getDateToday, 'Y-m-d H:i:s'));
                $query->orWhere('time_start', null);
            })->where(function ($query) use ($getDateToday) {
                $query->orWhere('time_end', '>=', date_format($getDateToday, 'Y-m-d H:i:s'));
                $query->orWhere('time_end', null);
            })->get();
            foreach ($product_all as $sale_val) {
                $cate_child = Category::whereIn('id', explode('|', $sale_val->category_ids))->get();
                if (in_array($category->id, explode('|', $sale_val->category_ids)) == true) {
                    $sale = Product::whereIn('id', explode('|', $sale_val->id_product_sale))->get();
                } elseif (!empty($manufacture) && in_array($manufacture->id, explode('|', $sale_val->manufacturer_ids)) == true) {
                    $sale = Product::whereIn('id', explode('|', $sale_val->id_product_sale))->get();
                } elseif (array_diff([$product['id']], explode('|', $sale_val->id_product)) == []) {
                    $sale = Product::whereIn('id', explode('|', $sale_val->id_product_sale))->get();
                } else {
                    $data['sales'] = [];
                }
                foreach ($cate_child as $rt) {
                    if (in_array($rt->id, explode('|', $sale_val->category_ids)) == true && in_array($rt->id, explode('|', $product['multi_cat'])) == true) {
                        $sale = Product::whereIn('id', explode('|', $sale_val->id_product_sale))->get();
                    } else {
                        $sale = [];
                    }
                }
            }

            //  Khối khuyến mại
            $sales = $this->getSales($product);
            $gift = '<div class="qua-content">
        <label>Quà tặng</label>
        <span class="qua-anh"><img style="width: 60px; height: 60px;"
                   src="' . CommonHelper::getUrlImageThumb(@$settings['logo_qua_tang'], 60, false) . '"
                   alt="Qùa tặng"/></span>
        <span class="gift_content">';
            $gift .= '<ul>';
            $totalPrice = 0;
            foreach ($sales as $data) {
                $gift .= '<li>- ' . @$data['name'] . '</li>';
                $totalPrice += $data['base_price'];
            }
            $gift .= '</ul>';
            $gift .= 'Trị giá: <b>' . number_format($totalPrice, 0, '.', '.') . ' <sup>đ</sup></b>';
            $gift .= '</span></div>';
            if (@$manufacture->discount >= 20) {
                $class = 'red_sale';
            } else {
                $class = 'orange_sale';
            }
            if (@$category->discount >= 20) {
                $class1 = 'red_sale';
            } else {
                $class1 = 'orange_sale';
            }
            /*if (@$manufacture->discount > 0) {
                $get_Sale = '<div class="badge ' . $class . '"><div class="sale_text">- ' . @$manufacture->discount . ' %</div></div>';
            } elseif (@$manufacture->discount == 0 && @$category->discount > 0) {
                $get_Sale = '<div class="badge ' . $class1 . '"><div class="sale_text">- ' . @$category->discount . ' %</div></div>';
            } else {
                $get_Sale = '';
            }*/
            $sale_text = \Modules\ThemeLogistics\Http\Helpers\CommonHelper::discount(@$product['base_price'], @$product['final_price']);
            if ($sale_text != '') {
                $get_Sale = '<div class="badge ' . $class . '"><div class="sale_text">' . $sale_text . '</div></div>';
            } else {
                $get_Sale = '';
            }
            $show_detail = Settings::where('name', 'show_detail')->first();
            $show_add_cart = Settings::where('name', 'show_add_cart')->first();
            $html0 = '';
            $html1 = '';

            if ($show_detail->value == 1 && $show_add_cart->value == 0) {
                $html0 = '<a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '"><span class="gri-view-pro" style="width: 100%!important;
                                        ">XEM HÀNG</span></a>';
                $html1 = '<a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '">
                                    <span class="gri-view-pro" style="width: 100%!important;">XEM HÀNG</span>
                                </a>';
            } elseif ($show_detail->value == 0 && $show_add_cart->value == 1) {
                $html0 = '<span data-text="/gio-hang" style="cursor: pointer;width: 100%!important;"
                              onclick="(addCart1(' . @$product['id'] . '))"
                              class="gri-add-cart urlCart">Cho vào giỏ</span>';
                $html1 = '<span data-text="/gio-hang" style="cursor: pointer;width: 100%!important;"
                                  onclick="(addCart1(' . @$product['id'] . '))"
                                  class="gri-add-cart urlCart">Cho vào giỏ</span>';
            } elseif ($show_detail->value == 1 && $show_add_cart->value == 1) {
                $html0 = '<a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '">
                            <span class="gri-view-pro">XEM HÀNG</span>
                        </a>
                        <span data-text="/gio-hang" style="cursor: pointer"
                              onclick="(addCart1(' . @$product['id'] . '))"
                              class="gri-add-cart urlCart">Cho vào giỏ</span>';
                $html1 = '<a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '">
                                <span class="gri-view-pro">XEM HÀNG</span>
                            </a>
                            <span data-text="/gio-hang" style="cursor: pointer"
                                  onclick="(addCart1(' . @$product['id'] . '))"
                                  class="gri-add-cart urlCart">Cho vào giỏ</span>';
            } elseif ($show_detail->value == 0 && $show_add_cart->value == 0) {
                $html0 = '<div style="display: none;"></div>';
                $html1 = '<div style="display: none;"></div>';
            }

            if ($category->option_show_cate == 1) {

                $this->result .= '<div class="gri" href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '" title="' . $product->meta_title . '">
                                        <div class="gi">' . $get_Sale . '
                                            <a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '" title="' . $product->meta_title . '">
                                                <img src="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getUrlImageThumb($product['image'], 250, false) . '">
                                            </a>
                                            <div class="manuface_g">
                                                <a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '" title="' . $product->meta_title . '">
                                                    <img height="50" src="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getUrlImageThumb(Manufacturer::find($product->manufacture_id)['image'], 150, false) . '" 
                                                  alt="' . $product->name . '">
                                                </a>
                                            </div>
                                            <h2 style="margin-bottom: 10px">
                                                <a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '" title="' . $product->meta_title . '">' . $product->name . '</a>
                                            </h2>
                                            <p class="product_intro">' . $product->intro . '</p>
                                            <span style="padding-top: 12px;">
                                                <u style="text-decoration: line-through;font-size: 18px">' . number_format($product->base_price, 0, '.', '.') . '</u><sup style="font-size: 18px;"> đ</sup></span>
                                                    <span style="padding: 0px 2px;">
                                                        <b style="margin: 0!important;">' . number_format($product->final_price, 0, '.', '.') . '<sup>đ</sup></b>
                                                    </span>
                                                    ' . $priceCheck . '
                                    </div>
                                    <div style="border: none!important;">
                                        <a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '" title="' . $product->meta_title . '"><span class="gri-view-pro">XEM HÀNG</span></a>
                                        <span data-text="/gio-hang" style="cursor: pointer" onclick="(addCart1('.$product->id.'))" class="gri-add-cart urlCart">Cho vào giỏ</span>
                                    </div>
                                </div>';


//                $this->result .= '<div class="gri" href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '" title="' . $product->meta_title . '">
//                                        <div class="gi">
//                                          ' . $get_Sale . '
//                                            <a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '" title="' . $product->meta_title . '">
//                                                <img src="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getUrlImageThumb($product['image'], 250, false) . '"
//                                                 alt="' . $product->name . '">
//                                            </a>
//                                            <div class="manuface_g">
//                                                <a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '" title="' . $product->meta_title . '">
//                                                    <img height="50" src="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getUrlImageThumb(Manufacturer::find($product->manufacture_id)['image'], 150, false) . '"
//                                                  alt="' . $product->name . '">
//                                                </a>
//                                            </div>
//                                            <h3 style="margin-bottom: 10px">
//                                                <a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '" title="' . $product->meta_title . '">' . $product->name . '</a>
//                                            </h3>
//                                            <p class="product_intro">' . $product->intro . '</p>
//                                            ' . $base_price . '
//                                            ' . $price . '
//                                            ' . $priceCheck . '
//                                        </div>
//
//                                        <div class="gift">
//                                        ' . $gift . '
//                                        </div>
//                                        <div>' . $html1 . '</div>
//                                    </div>
//                                ';
            } elseif ($category->option_show_cate == 0) {
                $this->result .= '
                <div class="gri" href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '"
                    title="' . $product->meta_title . '">
                    <div class="gi">
                        <div class="gri_left">
                            <div class="badge  red_sale ">
                                        <div class="sale_text">- 67 %</div>
                                    </div>
                            <a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '" title="">
                                <img src="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getUrlImageThumb($product['image'], 250, false) . '" alt="' . $product->name . '">
                            </a>
                            <div class="manuface_g" style="height: auto!important;">
                                <a href="#" title="' . $product->name . '">
                                    <img height="50" src="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getUrlImageThumb(Manufacturer::find($product->manufacture_id)['image'], 150, false) . '" alt="Malloca">
                                </a>

                            </div>
                        </div>
                        <div class="gri-center">
                            <h3>
                                <a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '" title="">' . $product->name . '</a>
                            </h3>
                            ' . $product->intro . '
                        </div>
                        
                        <div class="gri-right">
                        
                            <p style="padding-top: 12px;">
                                <u style="text-decoration: line-through;font-size: 18px">' . number_format($product->base_price, 0, '.', '.') . '</u>
                                <sup style="font-size: 18px;"> đ</sup>
                            </p>
                            <p style="padding: 0px 2px;"> <b>' . number_format($product->final_price, 0, '.', '.') . '<sup>đ</sup></b></p>

                            <p>' . number_format($product->base_price - $product->final_price, 0, '.', '.') . '<sup>đ</sup></p>
                        
                            <div class="gift">
                            
                            </div>
                            
                            <div>' . $html0 . '</div>
                        </div>
                        
                    </div>
                   
                </div>
            ';
            }
        }
        $countProduct = count($products);
        $countProductNolimit = count($productNolimit);
        return response()->json([
            'success' => true,
            'result' => $this->result,
            'countProduct' => $countProduct,
            'countProductNolimit' => $countProductNolimit,
        ]);
    }

    public function getSales($product)
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $getDateToday = date_create(date('Y-m-d H:i:s'));
        $sales = [];
        $sale = [];
        $product_all = ProductSale::where(function ($query) use ($getDateToday) {
            $query->orWhere('time_start', '<=', date_format($getDateToday, 'Y-m-d H:i:s'));
            $query->orWhere('time_start', null);
        })->where(function ($query) use ($getDateToday) {
            $query->orWhere('time_end', '>=', date_format($getDateToday, 'Y-m-d H:i:s'));
            $query->orWhere('time_end', null);
        })->get();
        foreach ($product_all as $key => $sale_val) {
            $manufacture = Manufacturer::whereIn('id', explode('|', @$sale_val->manufacturer_ids))->get();
            $cate_child = Category::whereIn('id', explode('|', @$sale_val->category_ids))->get();
            foreach ($cate_child as $rt) {
                if ($rt->parent_id == 0 && in_array($rt->id, explode('|', @$sale_val->category_ids)) == true && in_array($rt->id, explode('|', @$product->multi_cat)) == true) {
                    $sale = Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                } elseif (in_array($rt->id, explode('|', @$sale_val->category_ids)) == true && in_array($rt->id, explode('|', @$product->multi_cat)) == true) {
                    $sale = Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                } else {
                    $sale = [];
                }
            }
            if ($sale == []) {
                foreach ($manufacture as $rts) {
                    if (in_array($rts->id, explode('|', @$sale_val->manufacturer_ids)) == true && in_array($rts->id, explode('|', @$product->manufacture_id)) == true) {
                        $sale = Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                    } else {
                        $sale = [];
                    }
                }
            }
            if (array_diff([@$product->id], explode('|', @$sale_val->id_product)) == []) {
                $sale = Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
            }
            if (!empty($sale)) {
                $sales = array_merge($sales, $sale->toArray());
            }
        }
        $ids = array_column($sales, 'id');
        $ids = array_unique($ids);
        $sales = array_filter($sales, function ($key, $value) use ($ids) {
            return in_array($value, array_keys($ids));
        }, ARRAY_FILTER_USE_BOTH);
        return $sales;
    }

    public function getSearch(Request $request)
    {
        $keyword = $request->key;
        $data['keyword'] = $keyword;
        if ($request->ajax()) {
            $keyword = $request['keyword'];
            $result = '';
            if ($keyword != '') {
                $data['products_ajax'] = CommonHelper::getFromCache('get_products_ajax_by_key' . $keyword);
                if (!$data['products_ajax']) {
                    $data['products_ajax'] = Product::where('status', 1)->where('type', 1)
                        ->where(function ($q) use ($keyword) {
                            $q->where('name', 'LIKE', '%' . $keyword . '%')
                                ->orWhere('final_price', 'LIKE', '%' . $keyword . '%');
                        })->limit(5)->get();
                    CommonHelper::putToCache('get_products_ajax_by_key' . $keyword, $data['products_ajax']);
                }
                foreach ($data['products_ajax'] as $product) {
                    $cate = CommonHelper::getFromCache('get_cate_by_product_ajax' . $product->id);
                    if (!$cate) {
                        $cate = Category::whereIn('id', explode('|', $product->multi_cat))->first();
                        CommonHelper::putToCache('get_cate_by_product_ajax' . $product->id, $cate);
                    }
                    if ($product->final_price == 0) {
                        $j = '<p>Liên hệ</p>';
                    } else {
                        $j = number_format($product->final_price, 0, ".", ".") . '&nbsp;<sup>đ</sup>';
                    }
                    $result .= '
                 <li>
                     <div class="search-img">
                        <img src="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getUrlImageThumb($product->image, false, false) . '" alt="' . $product->name . '">
                    </div>
                    <div class="search-name">
                        <h4>' . $product->name . '</h4><span class="price_search"> ' . $j . '</span></div>
                    <a href="' . \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) . '" class="search-link"></a>
                  </li>
                ';
                }
                return response()->json([
                    'success' => true,
                    'result' => $result,
                ]);
            } else {
                $result = '';
                return response()->json([
                    'success' => false,
                    'result' => $result,
                ]);
            }
        } else {
            $data['products'] = CommonHelper::getFromCache('get_products_by_key_keyword' . $keyword);
            if (!$data['products']) {
                $data['products'] = Product::where('status', 1)->where('type', 1)
                    ->where(function ($q) use ($keyword) {
                        $q->where('name', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('final_price', 'LIKE', '%' . $keyword . '%');
                    })
                    ->paginate(20);
                CommonHelper::putToCache('get_products_by_key_keyword' . $keyword, $data['products']);
            }
            $data['countProducts'] = CommonHelper::getFromCache('get_countProducts_by_key_keyword' . $keyword);
            if (!$data['countProducts']) {
                $data['countProducts'] = Product::where('status', 1)->where('type', 1)
                    ->where(function ($q) use ($keyword) {
                        $q->where('name', 'LIKE', '%' . $keyword . '%')
                            ->orWhere('final_price', 'LIKE', '%' . $keyword . '%');
                    })
                    ->get();
                CommonHelper::putToCache('get_countProducts_by_key_keyword' . $keyword, $data['countProducts']);
            }
            $data['count'] = count($data['countProducts']);
            $data['countShow'] = count($data['products']);

        }
        $pageOption = [
            'title' => 'Tìm kiếm với từ khóa ' . $keyword,
            'description' => 'Tìm kiếm với từ khóa ' . $keyword,
            'keywords' => 'Tìm kiếm với từ khóa ' . $keyword,
        ];
        view()->share('pageOption', $pageOption);
        return view('themelogistics::childs.category.search')->with($data);
    }


    public function sortPriceProduct(Request $request)
    {
        if ($request->sort_prd_price == 0) {
            $data['product_sort_price'] = CommonHelper::getFromCache('products_multi_cat_order_by_final_price_desc' . $request->category_sort);
            if (!$data['product_sort_price']) {
                $data['product_sort_price'] = Product::where('status', 1)->where('multi_cat', 'like', '%|' . $request->category_sort . '|%')->orderby('final_price', 'desc')->get();

                CommonHelper::putToCache('products_multi_cat_order_by_final_price_desc' . $request->category_sort, $data['product_sort_price']);
            }
        } else {
            $data['product_sort_price'] = CommonHelper::getFromCache('products_multi_cat_order_by_final_price_asc' . $request->category_sort);
            if (!$data['product_sort_price']) {
                $data['product_sort_price'] = Product::where('status', 1)->where('multi_cat', 'like', '%|' . $request->category_sort . '|%')->orderby('final_price', 'ASC')->get();

                CommonHelper::putToCache('products_multi_cat_order_by_final_price_asc' . $request->category_sort, $data['product_sort_price']);
            }
        }
        return view('themelogistics::childs.category.index', $data);
    }
}