<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */

namespace Modules\ThemeLogistics\Http\Controllers\Frontend;
use App\Http\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use Modules\ThemeLogistics\Models\Category;
//use Modules\ThemeLogistics\Http\Helpers\CommonHelper;
use Modules\ThemeLogistics\Models\Contact;
use Modules\ThemeLogistics\Models\Coupon;
use Modules\ThemeLogistics\Models\District;
use Modules\ThemeLogistics\Models\HistoryTransport;
use Modules\ThemeLogistics\Models\Manufacturer;
use Modules\ThemeLogistics\Models\Menu;
use Modules\ThemeLogistics\Models\NTPost;
use Modules\ThemeLogistics\Models\NTTerm;
use Modules\ThemeLogistics\Models\NTTermTaxonomy;
use Modules\ThemeLogistics\Models\Post;
use Modules\ThemeLogistics\Models\Product;
use Modules\ThemeLogistics\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Modules\ThemeLogistics\Models\Banner;
use DB;
use Modules\ThemeLogistics\Models\ShippingMethod;
use Modules\ThemeLogistics\Models\TransportProductType;
use Modules\ThemeLogistics\Models\Transports;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Response;
use Illuminate\Support\Str;
class HomeController extends Controller
{
    public $trans_status = [
        'chuathanhtoan' => 'Chưa thanh toán',
        'cholayhang' => 'Chờ lấy hàng',
        'dalayhang' => 'Đã lấy hàng',
        'dagiaohang' => 'Đã giao hàng',
        'nocode' => 'Nợ COD',
        'datracode' => 'Đã trả COD',
        'khongphatduoc' => 'Không phát được',
        'dahuy' => 'Đã hủy',
        'dangchuyenhoan' => 'Đang chuyển hoàn',
        'dachuyenhoan' => 'Đã chuyển hoàn',
        'suco' => 'Sự cố'
    ];
    public $dactinhhanghoa = [
        1 => 'Tài liệu',
        2 => 'Thời trang Mỹ phẩm',
        3 => 'Đồ điện tử',
        4 => 'Dược phẩm',
        5 => 'Thực phẩm',
        6 => 'Đồ tươi sống',
        7 => 'Đồ có pin',
        8 => 'Chất bột nước',
        9 => 'Chất dễ cháy'
    ];
    public function __construct()
    {
        view()->share('dataDacTinh', $this->dactinhhanghoa);
    }
    public function ajaxSendRequest(Request $request){
        $code = $request->code;
        $text = $request->text;
        $transport = Transports::select('id')->where('code', $code)->first();

        $history = new HistoryTransport();
        $history->transport_id = $transport->id;
        $history->name = $text;
        $history->save();

        // render list comment
        $trans = Transports::find($transport->id);
        $historyList = $trans->histories()->orderBy('id','desc')->get();
        $html = '';
        foreach($historyList as $k => $item){
            $date=date_create($item['created_at']);
            $html .= '<p><span>['. date_format($date, 'h:i:s d-m-Y') .']</span> '.$item->name.'</p>';
        }
        echo $html;

    }
    public function checkCouponAjax(Request $request){

        $code = $request->code;
        $coupont = Coupon::where('code', $code)->first();

        if($coupont){
            if($coupont->type == 1){
                $tien = "đ" . number_format($coupont->value);
            }else if($coupont->type == 2){
                $tien = $coupont->value . "%";
            }
            return Response::json(array('success' => true, 'message' => 'Giảm ' . $tien), 200);
        }else{
            return Response::json(array('success' => false, 'message' => 'Mã không hoạt động'), 200);
        }
    }
    public function ajaxVanDon(Request $request){

        $rules = [
            'tenhang' => 'required',
            'quantity' => 'integer|min:1',
            'shipping_hoten' => 'required|max:255',
            'shipping_sdt' => 'required|max:255',
            'shipping_diachi' => 'required|max:255',
            'billing_hoten' => 'required',
            'billing_sdt' => 'required',
            'billing_diachi' => 'required',
            'kieuvanchuyen' => 'required',
            'shipping_method_id' => 'required',
            'so_km' => 'required',
            'package_no' => 'required',
            'paypal' => 'required'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ));
        }

        // upload hình
            $new_name = "";
             if($request->file('picture')) {
                $image = $request->file('picture');
                $new_name = rand() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path() . "/themelogistic/images/transports/", $new_name);
            }
        // tính tổng tiền
        $cod = $request->cod;
        if(empty($cod)){
            $cod = 0;
        }
        $shipping = ShippingMethod::find($request->shipping_method_id);
        $final_price = $shipping->final_price;
        $vat = $final_price * 0.1;
        $total_price = $final_price + $vat;
        $coupont = Coupon::where('code', $request->coupon)->first();
        if($coupont){
            if($coupont->type == 1){
                $total_price = $total_price - $coupont->value;
            }else if($coupont->type == 2){
                $total_price = round($total_price - $total_price * $coupont->value / 100);
            }

        }
        $total_price_with_cod = $total_price + $cod;


            $user = \Auth::guard('admin')->user();
            $madonhang =  substr(str_shuffle("0123456789"), 0, 9);
            $transport = new Transports();
            $transport->admin_id = $user->id;
            $transport->code = $madonhang;
            $transport->name = $request->tenhang;
            $transport->shipping_name = $request->shipping_hoten;
            $transport->shipping_tel = $request->shipping_sdt;
            $transport->shipping_address = $request->shipping_diachi;
            $transport->shipping_email = $request->shipping_email;
            $transport->billing_name = $request->billing_hoten;
            $transport->billing_tel = $request->billing_sdt;
            $transport->billing_address = $request->billing_diachi;
            $transport->billing_email = $request->billing_email;
            $transport->note = $request->ghichu;
            $transport->payment_methods = $request->paypal;
            $transport->discount_code = $request->coupon;
            $transport->image = $new_name;
            $transport->quantity = $request->quantity;
            $transport->type_quantity = $request->type_quantity;
            $transport->quanduong = $request->so_km;
            $transport->cod = $request->cod;

            $transport->transport_product_type_id = $request->package_no;
            $transport->transport_shipping_method_id = $request->shipping_method_id;
            $transport->transport_type_id = $request->kieuvanchuyen;
            $transport->tongtien = $total_price_with_cod;
            $transport->save();

//        $cod = $request->cod;
//        if(empty($cod) || $cod == null ){
//            $cod = 0;
//        }
//        $tongTien = $this->tinhGiaCuoc($request);
//
//        $tongTienFinal = $tongTien;
//        $vat = $tongTien * 0.1;
//        $coupont = Coupon::where('code', $request->coupon)->first();
//        if($coupont){
//            if($coupont->type == 1){
//                $tongTienFinal = $tongTienFinal - $coupont->value;
//                $coupont->value = "đ".number_format($coupont->value);
//            }else if($coupont->type == 2){
//                $tongTienFinal = round($tongTienFinal - $tongTienFinal * $coupont->value / 100);
//                $coupont->value = $coupont->value . "%";
//            }
//
//        }
//        $tongTienFinal = $tongTienFinal + $vat;
//        $thanhtien = $tongTienFinal + $cod;
//
//        $dataImage = [];
//        if($request->type == 'insert') {
//            // upload image
//            $new_name = "";
//            if($request->file('picture')) {
//                $image = $request->file('picture');
//                $new_name = rand() . '.' . $image->getClientOriginalExtension();
//                $image->move(public_path() . "/themelogistic/images/transports/", $new_name);
//                $dataImage = [
//                    'uploaded_image' => '<img style="width:100px" src="/public/themelogistic/images/transports/' . $new_name . '" class="img-thumbnail"/>',
//                    'class_name' => 'alert-success'
//                ];
//            }
//            $user = \Auth::guard('admin')->user();
//            $madonhang =  substr(str_shuffle("0123456789"), 0, 9);
//            $transport = new Transports();
//            $transport->admin_id = $user->id;
//            $transport->code = $madonhang;
//            $transport->type_commodities = $request->mahang;
//            $transport->name = $request->tenhang;
//            $transport->shipping_name = $request->shipping_hoten;
//            $transport->shipping_tel = $request->shipping_sdt;
//            $transport->shipping_address = $request->shipping_diachi;
//            $transport->shipping_email = $request->shipping_email;
//            $transport->billing_name = $request->billing_hoten;
//            $transport->billing_tel = $request->billing_sdt;
//            $transport->billing_address = $request->billing_diachi;
//            $transport->billing_email = $request->billing_email;
//            $transport->note = $request->ghichu;
//            $transport->payment_methods = $request->paypal;
//            $transport->shipping_method_id = $request->shipping_method_id;
//            $transport->discount_code = $request->coupon;
//            $transport->image = $new_name;
//            $transport->quantity = $request->quantity;
//            $transport->type_quantity = $request->type_quantity;
//            $transport->quanduong = $request->so_km;
//            $transport->cod = $request->cod;
//            $transport->tongtien = $tongTien;
//            $transport->save();
//        }

        /*if($request->weight && !empty($request->weight) && is_numeric($request->weight) ){
            $tongTien = 100000 * $request->weight;
        }elseif($request->sokienhang && !empty($request->sokienhang) && is_numeric($request->sokienhang) ){
            $tongTien = 100000 * $request->sokienhang;
        }else{
            $tongTien = 100000;
        }*/


//        $res = [
//            'tongtien' =>   number_format($tongTien),
//            'vat' => number_format($vat),
//            'tongTienFinal' => number_format($tongTienFinal),
//            'thanhtien' => number_format($thanhtien),
//            'cod' => number_format($cod),
//            'image' => $dataImage,
//            'coupon' => $coupont
//        ];
//        return Response::json(array('success' => true, 'data' => $res), 200);
    }
    public function tinhGiaCuoc(Request $request){
        $tong = $request->quantity * $request->so_km * 100000;
        return $tong;
        // Giá Cước Nội Địa Việt Nam
        $tong = 0;
        if($request->kieuvanchuyen == "noidia"){
//            if($request->type_quantity == "trongluong"){
//                $tong = $request->quantity * $request->so_km * 100000;
//            }
            $tong = $request->quantity * $request->so_km * 100000;
        }
        return $tong;
    }
    public function  ajaxGetTransportProductType(Request $request){
        $type_id = $request->type_id;
        $items = TransportProductType::where('transport_type_id', $type_id)->get();
        return $this->renderTransportType($items);
    }
    public function renderTransportType($items){
        $html = '';
        foreach($items as $k => $v){
            $html .= '<li class="border bg-fff translate rounded pt-1 pb-1 mb-1"  data-value="'.$v->id.'">
                             <div class="w-100 text-dark text-center ">
                                   <span class="icon ntl-Letter d-block">
                                           <img src="'.CommonHelper::getUrlImageThumb($v->image).'">
                                    </span>
                                   <div class="mt-2">'.$v->name.'</div>
                            </div>
                        </li>';
        }
        return $html;
    }
    public function ajaxGetModalTransport(Request $request){
        $data = $request->all();
        return view('themelogistics::ajax.modalCreateTransport')->with($data);
    }
    public function ajaxGetShippingMethod(Request $request){
        $transport_type = $request->kieuvanchuyen;
        $package_no = $request->package_no;
        $so_km = $request->so_km;
        $type_quantity = $request->type_quantity;
        $quantity = $request->quantity;
        $shippingMethodObj = CommonHelper::getShippingMethod($transport_type, $package_no, $so_km, $type_quantity,$quantity );
        return view('themelogistics::ajax.shipping_method_template', [ 'shippingMethodObj' => $shippingMethodObj ]);
    }
    public function ajaxGetPrice(Request $request){

        $cod = $request->cod;
        if(empty($cod)){
            $cod = 0;
        }
        $shipping = ShippingMethod::find($request->shipping_method_id);
        $final_price = $shipping->final_price * $request->quantity;
        $vat = $final_price * 0.1;
        $total_price = $final_price + $vat;
        $htmlDiscount = '';
        $coupont = Coupon::where('code', $request->coupon)->first();
        if($coupont){
            if($coupont->type == 1){
                $total_price = $total_price - $coupont->value;
                $htmlDiscount .= '<div class="vat-price b-p">
                                <p>- Mã: '.$request->coupon.'</p>
                                <span class="text-success">-'.number_format($coupont->value).' vnđ</span>
                            </div>';
            }else if($coupont->type == 2){
                $total_price = round($total_price - $total_price * $coupont->value / 100);
                $htmlDiscount .= '<div class="vat-price b-p">
                                <p>- Mã: '.$request->coupon.'</p>
                                <span class="text-success">-'.number_format($coupont->value).' %</span>
                            </div>';
            }

        }
//        $res = [
//            'final_price' => number_format($final_price),
//            'vat' => number_format($vat),
//            'totalPriceNotDiscount' =>  number_format($total_price)
//        ];


        $result = '';
        $result .= ' <div class="total-price b-p">
                                <p>- Tổng tiền</p>
                                <span id="totalPrice">'.number_format($final_price).' vnđ</span>
                       </div>
                            <div id="box_coupon"></div>
                            <div class="vat-price b-p">
                                <p>- 10% VAT</p>
                                <span id="vat">'.number_format($vat).' vnđ</span>
                            </div>
                            '.$htmlDiscount.'
                             <div class="vat-price b-p">
                                <p>- COD</p>
                                <span id="cod">'.number_format($cod).' vnđ</span>
                            </div>
                            <div id="box_thuho"></div>
                            <div class="done-price b-p">
                                <p>Thành tiền</p>
                                <span id="totalPriceFinal">'.number_format($total_price + $cod).' vnđ</span>
                            </div>';
        return $result;
    }
    public function getIndex()
    {

        $data['posts'] = CommonHelper::getFromCache('get_post_home');
        if (!$data['posts']) {
            $data['posts'] = Post::where('status', 1)->where('show_home', 1)->where('type_page', '<>', 'page_static')->orderBy('order_no', 'desc')->take(11)->get();
            CommonHelper::putToCache('get_post_home', $data['posts']);
        }
        $data['category'] = CommonHelper::getFromCache('get_category_home');
        if (!$data['category']) {
            $data['category'] = Category::where('status', 1)->where('show_homepage', 1)->where('show_menu', 1)->orderBy('order_no', 'asc')->get();
            CommonHelper::putToCache('get_category_home', $data['category']);
        }
        $dataDefaultTinh = [0 => 'Tỉnh/TP'];
        $dataDefaultHuyen = [0 => 'Quận/Huyện'];
        $data['province'] =  $dataDefaultTinh + Province::select('id','name')->orderBy('id', 'asc')->pluck('name','id')->toArray();

        $data['district'] = $dataDefaultHuyen + District::select('id','name')->where('province_id', 1)->orderBy('id', 'asc')->pluck('name','id')->toArray();

        return view('themelogistics::home.index')->with($data);
    }
    public function trackingBill(Request $request){

        if(\Auth::guard('admin')->check()){
            $code = $request->bill;
            $validated = $request->validate([
                'bill' => 'required',
            ]);
            $user = \Auth::guard('admin')->user();
            $trans = Transports::where('code', $code)->first();
            if($trans) {
                $histories  = $trans->histories()->orderBy('id','desc')->get();
                $status = $trans->statusObj->name;
                $trans->shipping_province;
                $trans->shipping_district;
                $trans->billing_province;
                $trans->billing_district;
                $trans->shipping_method;

                $trans = $trans->toArray();
                $trans['histories'] = $histories->toArray();
                $trans['status'] = $status;
                $res['data'] = $trans;
                $res['status'] = 'success';


                return view('themelogistics::ajax.modalTracking')->with($res);
                //return response()->json($res);
            }else{
                return view('themelogistics::ajax.modalTrackingNotFound');
            }
        }else{
            $res['status'] = 'auth';
        }
    }
    public function ajax_get_district(Request $request){
        $id = $request->province_id;
        $dataDefaultTinh = [0 => 'Tỉnh/TP'];
        $dataDefaultHuyen = [0 => 'Quận/Huyện'];
        $districts = $dataDefaultHuyen + District::select('id','name')->where('province_id', $id)->orderBy('id', 'asc')->pluck('name','id')->toArray();
        $html = '';
        foreach($districts as $k => $item){
            $selected = 0 == $k ? "selected" : "";
            $html .= '<option '.$selected.' value="'.$k.'">'.$item.'</option>';
        }
        return $html;
    }
    public function getAllManufacturer()
    {
        $data['manufacturer'] = CommonHelper::getFromCache('get_all_manufacturer');
        if (!$data['manufacturer']) {
            $data['manufacturer'] = Manufacturer::where('status', 1)->get();
            CommonHelper::putToCache('get_all_manufacturer', $data['manufacturer']);
        }
        $pageOption = [
            'title' => 'Thương hiệu',
            'description' => 'Thương hiệu',
            'keywords' => 'Thương hiệu',
        ];
        view()->share('pageOption', $pageOption);
        return view('themelogistics::childs.manufacturer.index')->with($data);
    }

    public function getManufacturer($slug, Request $request)
    {
        $data['get_manufacturer'] = CommonHelper::getFromCache('manufacturer_slug'.$slug);
        if (!$data['get_manufacturer']) {
            $data['get_manufacturer'] = Manufacturer::where('slug', $slug)->first();
            CommonHelper::putToCache('manufacturer_slug'.$slug, $data['get_manufacturer']);
        }


        $data['products'] = CommonHelper::getFromCache('product_manufacturery_id' . $data['get_manufacturer']->id);
        if (!$data['products']) {
            $data['products'] = Product::where('manufacture_id', $data['get_manufacturer']->id)->paginate(20);
            CommonHelper::putToCache('product_manufacturery_id' . $data['get_manufacturer']->id, $data['products']);
        }

        if (!empty($data['get_manufacturer'])) {
            $cate_id = explode('|', $data['get_manufacturer']->filers_category);
            $post_id = explode('|', $data['get_manufacturer']->filers_post);
            $data['manu_filers_cate'] = CommonHelper::getFromCache('get_cate_filers_by_manu' . $data['get_manufacturer']->id);
        }
        if (!$data['manu_filers_cate']) {
            $data['manu_filers_cate'] = Category::whereIn('id', $cate_id)
                ->where('status', 1)->where('type', 5)->get();
            CommonHelper::putToCache('get_cate_filers_by_manu' . $data['get_manufacturer']->id, $data['manu_filers_cate']);
        }

        $data['manu_filers_post'] = CommonHelper::getFromCache('get_post_filers_by_manu' . $data['get_manufacturer']->id);
        if (!$data['manu_filers_post']) {
            $data['manu_filers_post'] = Post::whereIn('id', $post_id)->get();
            CommonHelper::putToCache('get_post_filers_by_manu' . $data['get_manufacturer']->id, $data['manu_filers_post']);
        }

        $pageOption = [
            'title' => $data['get_manufacturer']->meta_title != '' ? $data['get_manufacturer']->meta_title : $data['get_manufacturer']->name,
            'description' => $data['get_manufacturer']->meta_description != '' ? $data['get_manufacturer']->meta_description : $data['get_manufacturer']->name,
            'keywords' => $data['get_manufacturer']->meta_keywords != '' ? $data['get_manufacturer']->meta_keywords : $data['get_manufacturer']->name,
            'robots' => $data['get_manufacturer']->meta_robot != '' ? $data['get_manufacturer']->meta_robot : $data['get_manufacturer']->name,
        ];
        view()->share('pageOption', $pageOption);
        return view('themelogistics::childs.manufacturer.manufacturer_child')->with($data);
    }

    public function getSaleList()
    {
        $data['category'] = CommonHelper::getFromCache('get_all_cate_url_sale');
        if (!$data['category']) {
            $data['category'] = Category::where('status', 1)->where('type', 7)->get();
            CommonHelper::putToCache('get_all_cate_url_sale', $data['category']);
        }
        $pageOption = [
            'title' => 'Khuyến mãi',
            'description' => 'Khuyến mãi',
            'keywords' => 'Khuyến mãi',
        ];
        view()->share('pageOption', $pageOption);
        return view('themelogistics::childs.saleproduct.index')->with($data);
    }

    public function cache_flush()
    {
        \Artisan::call('view:clear');
        \Artisan::call('cache:clear');
        \Cache::flush();
        return redirect('/');
    }

    public function trungSP()
    {
        $html = '<h3>Các sản phẩm trùng</h3><br>
    <table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Tên</th>
        <th>Mã</th>
        <th>Hành động</th>
    </tr>
    </thead>
    <tbody>';
        $products = Product::select(['id', 'name', 'code'])->orderBy('id', 'asc')->paginate(30);
        foreach ($products as $product) {
            $prds = Product::select(['id', 'name', 'code'])->where('code', $product->code)->where('id', '>', $product->id)->get();
            if (count($prds) > 0) {
                $html .= '<tr style="border: 1px solid #ccc; font-weight: bold;font-size: 18px;">
                                <td>'.$product->id.'</td>
                                <td>'.$product->name.'</td>
                                <td>'.$product->code.'</td>
                                <td><a href="/admin/edit_product/'.$product->id.'" target="_blank" style="color: blue;">Sửa</a><a href="/admin/delete_product/'.$product->id.'" target="_blank" style="color: red; margin-left: 10px;">Xóa</a></td>
                            </tr>';

                foreach ($prds as $p) {
                    $html .= '<tr>
                                <td>'.$p->id.'</td>
                                <td>'.$p->name.'</td>
                                <td>'.$p->code.'</td>
                                <td><a href="/admin/edit_product/'.$p->id.'" target="_blank" style="color: blue;">Sửa</a><a href="/admin/delete_product/'.$p->id.'" target="_blank" style="color: red; margin-left: 10px;">Xóa</a></td>
                            </tr>';
                }
            }
        }

        $html .= '</tbody>
</table>';
        $html .= $products->links();

        echo $html;
    }
    public function advisory(){
        $pageOption = [
            'title' => 'Tư vấn | Tin tức',
            'description' => 'Tư vấn | Tin tức',
            'keywords' => 'Tư vấn bếp, Tin tức bếp',
        ];
        $data['category'] = CommonHelper::getFromCache('menu_url_tu_van');
        if (! $data['category']) {
            $data['category'] = Menu::where('url','/tu-van')->where('status',1)->first();
            CommonHelper::putToCache('menu_url_tu_van',  $data['category']);
        }

        view()->share('pageOption', $pageOption);
        return view('themelogistics::childs.category_post.category_post_all',$data);
    }
    public function updateSale(){
        $a = Product::where('base_price','<>','')->where('base_price','<>',Null)
                        ->where('final_price','<>','')->where('final_price','<>',Null)->get();
//        $a=Product::get();
        foreach ($a as $b){
            $c = Product::find($b->id);
            $c->sale = ceil(($c->base_price - $c->final_price)*100/ $c->base_price) .'%';
//            $c->sale = Null;
            $c->save();
        }

    }
    public  function searchTransport(Request $request){

    }
}




