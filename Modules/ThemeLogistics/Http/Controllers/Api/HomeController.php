<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */

namespace Modules\ThemeLogistics\Http\Controllers\Api;
use App\Http\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use Modules\ThemeLogistics\Models\Category;
//use Modules\ThemeLogistics\Http\Helpers\CommonHelper;
use Modules\ThemeLogistics\Models\Contact;
use Modules\ThemeLogistics\Models\Coupon;
use Modules\ThemeLogistics\Models\District;
use Modules\ThemeLogistics\Models\HistoryTransport;
use Modules\ThemeLogistics\Models\Manufacturer;
use Modules\ThemeLogistics\Models\Menu;
use Modules\ThemeLogistics\Models\NTPost;
use Modules\ThemeLogistics\Models\NTTerm;
use Modules\ThemeLogistics\Models\NTTermTaxonomy;
use Modules\ThemeLogistics\Models\Post;
use Modules\ThemeLogistics\Models\Product;
use Modules\ThemeLogistics\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Modules\ThemeLogistics\Models\Banner;
use DB;
use Modules\ThemeLogistics\Models\ShippingMethod;
use Modules\ThemeLogistics\Models\TransportProductType;
use Modules\ThemeLogistics\Models\Transports;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Modules\ThemeLogistics\Models\TransportType;
use Response;
use Illuminate\Support\Str;
class HomeController extends Controller
{

    public function __construct()
    {
       // view()->share('dataDacTinh', $this->dactinhhanghoa);
    }
    public function getTransportType(){
        $items = TransportType::orderBy('id','ASC')->get();

        foreach ($items as $k => $item){
            $items[$k]['image'] = 'https://etalgroup.net/public/filemanager/userfiles/' . $item['image'];

        }
        return Response::json(array('success' => true, 'items' => $items), 200);
    }
    public function getTransportProductType(Request $request){
        $type_id = $request->type_id;
        $items = TransportProductType::where('transport_type_id', $type_id)->get();
        foreach ($items as $k => $item){
            $items[$k]['image'] = 'https://etalgroup.net/public/filemanager/userfiles/' . $item['image'];

        }
        return Response::json(array('success' => true, 'items' => $items), 200);
    }
    public function getShippingMethod(Request $request){
        $transport_type = $request->kieuvanchuyen;
        $package_no = $request->product_type;
        $so_km = $request->so_km;
        $type_quantity = $request->type_quantity;
        $quantity = $request->quantity;
        $shippingMethodObj = CommonHelper::getShippingMethod($transport_type, $package_no, $so_km, $type_quantity,$quantity );
        foreach ($shippingMethodObj as $k => $item){
            $shippingMethodObj[$k]['image'] = 'https://etalgroup.net/public/filemanager/userfiles/' . $item['image'];

        }
        return Response::json(array('success' => true, 'items' => $shippingMethodObj), 200);
    }
    public function getPrice(Request $request){
        $cod = $request->cod;
        if(empty($cod)){
            $cod = 0;
        }
        $shipping = ShippingMethod::find($request->shipping_method_id);
        $final_price = $shipping->final_price * $request->quantity;
        $vat = $final_price * 0.1;
        $total_price = $final_price + $vat;
        $coupont = Coupon::select('type','value')->where('code', $request->coupon)->first();
        if($coupont){
            if($coupont->type == 1){
                $total_price = $total_price - $coupont->value;

            }else if($coupont->type == 2){
                $total_price = round($total_price - $total_price * $coupont->value / 100);
            }

        }

        return Response::json(array(
            'success' => true,
            'item' => [
                'cod' => $cod,
                'price' => $final_price,
                'coupon' => $coupont,
                'vat' => $vat,
                'totalPrice' => $total_price + $cod
            ]
        ), 200);
    }
    public function checkCouponAjax(Request $request){

        $code = $request->code;
        $coupont = Coupon::where('code', $code)->first();

        if($coupont){
            if($coupont->type == 1){
                $tien = "đ" . number_format($coupont->value);
            }else if($coupont->type == 2){
                $tien = $coupont->value . "%";
            }
            return Response::json(array('success' => true, 'message' => 'Giảm ' . $tien), 200);
        }else{
            return Response::json(array('success' => false, 'message' => 'Mã không hoạt động'), 200);
        }
    }
    public function saveTransport(Request $request){
        $rules = [
            'tenhang' => 'required',
            'quantity' => 'integer|min:1',
            'shipping_hoten' => 'required|max:255',
            'shipping_sdt' => 'required|max:255',
            'shipping_diachi' => 'required|max:255',
            'billing_hoten' => 'required',
            'billing_sdt' => 'required',
            'billing_diachi' => 'required',
            'transport_type' => 'required',
            'shipping_method_id' => 'required',
            'so_km' => 'required',
            'transport_product_type' => 'required',
            'paypal' => 'required'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ));
        }

        // upload hình
        $new_name = "";
        if($request->file('picture')) {
            $image = $request->file('picture');
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path() . "/themelogistic/images/transports/", $new_name);
        }
        // tính tổng tiền
        $cod = $request->cod;
        if(empty($cod)){
            $cod = 0;
        }
        $shipping = ShippingMethod::find($request->shipping_method_id);
        $final_price = $shipping->final_price;
        $vat = $final_price * 0.1;
        $total_price = $final_price + $vat;
        $coupont = Coupon::where('code', $request->coupon)->first();
        if($coupont){
            if($coupont->type == 1){
                $total_price = $total_price - $coupont->value;
            }else if($coupont->type == 2){
                $total_price = round($total_price - $total_price * $coupont->value / 100);
            }

        }
        $total_price_with_cod = $total_price + $cod;


        $user = \Auth::guard('admin')->user();
        $madonhang =  substr(str_shuffle("0123456789"), 0, 9);
        $transport = new Transports();
        $transport->admin_id = $user->id;
        $transport->code = $madonhang;
        $transport->name = $request->tenhang;
        $transport->shipping_name = $request->shipping_hoten;
        $transport->shipping_tel = $request->shipping_sdt;
        $transport->shipping_address = $request->shipping_diachi;
        $transport->shipping_email = $request->shipping_email;
        $transport->billing_name = $request->billing_hoten;
        $transport->billing_tel = $request->billing_sdt;
        $transport->billing_address = $request->billing_diachi;
        $transport->billing_email = $request->billing_email;
        $transport->note = $request->ghichu;
        $transport->payment_methods = $request->paypal;
        $transport->discount_code = $request->coupon;
        $transport->image = $new_name;
        $transport->quantity = $request->quantity;
        $transport->type_quantity = $request->type_quantity;
        $transport->quanduong = $request->so_km;
        $transport->cod = $request->cod;

        $transport->transport_product_type_id = $request->transport_product_type;
        $transport->transport_shipping_method_id = $request->shipping_method_id;
        $transport->transport_type_id = $request->transport_type;
        $transport->tongtien = $total_price_with_cod;
        $transport->save();
    }

}




