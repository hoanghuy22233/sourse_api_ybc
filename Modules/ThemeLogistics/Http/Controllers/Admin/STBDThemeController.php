<?php

namespace Modules\ThemeLogistics\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Illuminate\Http\Request;
use Modules\ThemeLogistics\Models\Category;
use Illuminate\Support\Facades\Schema;
use Modules\ThemeLogistics\Models\Manufacturer;

class STBDThemeController extends CURDBaseController
{
    protected $module = [
        'code' => 'setting',
        'label' => 'Cấu hình theme',
        'modal' => '\App\Models\Setting',
        'tabs' => [
            /*
            'social_tab' => [
                'label' => 'Mạng xã hội',
                'icon' => '<i class="kt-menu__link-icon flaticon-settings-1"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'fanpage', 'type' => 'text', 'label' => 'Fanpage FB', 'des' => 'Fanpage Facebook'],
                    ['name' => 'google_map', 'type' => 'text', 'label' => 'google_map', 'des' => 'Fanpage Facebook'],
                    ['name' => 'skype', 'type' => 'text', 'label' => 'skype', 'des' => 'Fanpage Facebook'],
                    ['name' => 'instagram', 'type' => 'text', 'label' => 'instagram', 'des' => 'Fanpage Facebook'],
                ]
            ],*/
            'common_tab' => [
                'label' => 'Chung',
                'icon' => '<i class="kt-menu__link-icon flaticon2-layers-2"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'zalo', 'type' => 'text', 'label' => 'Link chat Zalo', 'des' => ''],
                    ['name' => 'fanpage', 'type' => 'text', 'label' => 'Link chat Fanpage', 'des' => ''],
                    ['name' => 'post_banner', 'type' => 'file_editor', 'label' => 'Banner trang tin tức'],
                    ['name' =>'frontend_head_code','type' =>'textarea','label' =>'admin.insert_code','inner' =>'rows=20'],
                    ['name' =>'frontend_footer_code','type' =>'textarea','label' =>'admin.insert_code_footer','inner' =>'rows=20'],
                ],
            ],
            'homepage_tab' => [
                'label' => 'Trang chủ',
                'icon' => '<i class="kt-menu__link-icon flaticon2-open-box"></i>',
                'intro' => '',
                'td' => [

                ]
            ],

            'product_page_tab' => [
                'label' => 'Trang sản phẩm',
                'icon' => '<i class="kt-menu__link-icon flaticon2-open-box"></i>',
                'intro' => '',
                'td' => [

                ]
            ],
        ]
    ];

    public function setting(Request $request)
    {

        $data['page_type'] = 'list';

        $module = \Eventy::filter('theme.custom_module', $this->module);
        if (!$_POST) {
            $listItem = $this->model->get();
            $tabs = [];
            foreach ($listItem as $item) {
                $tabs[$item->type][$item->name] = $item->value;
            }
            #
            $data['tabs'] = $tabs;
            $data['page_title'] = $module['label'];
            $data['module'] = \Eventy::filter('theme.custom_module', $module);
            return view(config('core.admin_theme') . '.setting.view')->with($data);
        } else {
            foreach ($module['tabs'] as $type => $tab) {
                $data = $this->processingValueInFields($request, $tab['td'], $type . '_');

                //  Tùy chỉnh dữ liệu insert
                if (isset($data['category_post_ids'])) {
                    $data['category_post_ids'] = '|' . implode('|', $data['category_post_ids']) . '|';
                }
                if (isset($data['category_product_ids'])) {
                    $data['category_product_ids'] = '|' . implode('|', $data['category_product_ids']) . '|';
                }
                #

                foreach ($data as $key => $value) {
                    $item = Setting::where('name', $key)->where('type', $type)->first();
                    if (!is_object($item)) {
                        $item = new Setting();
                        $item->name = $key;
                        $item->type = $type;
                    }
                    $item->value = $value;
                    $item->save();
                }
            }

            if(Schema::hasTable('admin_logs')){
                $this->adminLog($request,$item=false,'settingTheme');
            }
            $robots = Setting::where('name', 'robots')->where('type', 'seo_tab')->first();
            if (!empty($robots->value)){
                @Category::whereIn('type',[1,5])->update([
                    'meta_robot' => @$robots->value
                ]);
                @Manufacturer::whereRaw('1=1')->update([
                    'meta_robot' => @$robots->value
                ]);
            }

            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Cập nhật thành công!');

            if ($request->return_direct == 'save_exit') {
                return redirect('admin/dashboard');
            }

            return back();
        }
    }
}
