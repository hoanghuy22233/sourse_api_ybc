<?php

namespace Modules\ThemeLogistics\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserSignInRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nameaccount' => 'required|max:255',
            'target' => 'required',
            'quymo' => 'required',
            'specialized' => 'required',
            'email' => 'required|unique:admin',
            'password' => 'required|min:8|max:32',
            'repassword' => 'required|same:password',
            'phone' => 'required|min:10',
        ];
    }
    public function messages()
    {
        return [
            'nameaccount.required' => 'Bắt buộc phải nhập tên tài khoản',
            'target.required' => 'Trường này không được để rỗng',
            'quymo.required' => 'Trường này không được để rỗng',
            'specialized.required' => 'Trường này không được để rỗng',
            'email.unique' => 'Email đã tồn tại',
            'email.required' => 'Bắt buộc phải nhập email',
            'password.required' => 'Bắt buộc phải nhập mật khẩu',
            'repassword.required' => 'Bắt buộc phải nhập mật khẩu',
            'repassword.same' => 'Mật khẩu chưa khớp,vui lòng nhập lại',
            'phone.required' => 'Bắt buộc phải nhập số điện thoại',
            'phone.min' => 'Số điện thoại phải có ít nhất 10 kí tự',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
