<?php
$banners = CommonHelper::getFromCache('get_banners_by_slide');
if (!$banners) {
    $banners = \Modules\ThemeLogistics\Models\Banner::where('status', 1)->where('location', 'banner_left_partner')->orderBy('order_no', 'ASC')->first();
    CommonHelper::putToCache('get_banners_by_slide', $banners);
}
?>
<section class="partner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="font-weight-bold" style="font-size:25px">Đối tác của chúng tôi</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="ytvideo" style="width:100%;">
                    <div class="partner-banner">
                        <img src="{{asset('public/filemanager/userfiles/'.$banners->image) }}" alt="" style="height: 499px;">
                    </div>
                    <div class="video-btn">
                            <span class="btn">
                                <a data-fancybox href="https://www.youtube.com/watch?v=5EVzFzM8ay8">
                                    <i class="fas fa-play"></i>
                                </a>
                            </span>
                    </div>
                    <source src="https://www.youtube.com/watch?v=5EVzFzM8ay8" type="video/mp4" />
                </div>
            </div>

            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <?php
                $partners = CommonHelper::getFromCache('widgets_home_sidebar_center', ['widgets']);
                if (!$partners) {
                    $partners =  \Modules\ThemeLogistics\Models\Banner::where('location', 'banner_partner')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
                    CommonHelper::putToCache('widgets_home_sidebar_center', $partners, ['widgets']);
                }

                ?>
                <div class="owl-carousel owl-theme pagination-bottom-carousel slider-home-customers" id="owl-partner">
                    @foreach($partners as $p)
                    <div class="partner-feedback">
                        <div class="feedback-inner">
                            <div class="inner-img">
                                <img src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($p->image) }}" alt="">
                            </div>
                            {!! $p->intro !!}
                            <p class="inner-name">{{$p->name}}</p>
                            <small>{{$p->link}}</small>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>