<section class="network">
    <div class="container do-you-know">
        <div class="row">
        <?php
            $widgets = CommonHelper::getFromCache('widgets_home_sidebar_center', ['widgets']);
            if (!$widgets) {
                $widgets =  \Modules\ThemeLogistics\Models\Widget::where('location', 'home3')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
                CommonHelper::putToCache('widgets_home_sidebar_center', $widgets, ['widgets']);
            }
            $networks = CommonHelper::getFromCache('get_banners_by_slide');
            if (!$networks) {
                $networks = \Modules\ThemeLogistics\Models\Banner::where('status', 1)->where('location', 'banner_network')->orderBy('order_no', 'DESC')->get();
                CommonHelper::putToCache('get_banners_by_slide', $networks);
            }
            ?>

          @foreach ($widgets as $ws)
                <div class="col-12 col-lg-6 col-md-6 col-sm-12">
                    {!! $ws->content !!}
                    <p class="btn-register"><a class="btn btn-primary btn-100" href="#" data-toggle="modal" data-target="#login-modal">Đăng ký ngay</a></p>
                </div>
            @endforeach

            <div class="col-12 col-lg-6 col-md-6 col-sm-12">
                <ul>
{{--                    @foreach($networks as $key =>  $nk)--}}
{{--                    <li>--}}
{{--                        <img  style="{{$key == 0 ? 'position: absolute; left: 0; top: 10px; height: 75px;left: 15px;' : '' }}"--}}
{{--                             src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($nk->image) }}"><br>--}}
{{--                        <strong><br>--}}
{{--                            {{$nk->name}}<br>--}}
{{--                        </strong><br>--}}
{{--                        {!! $nk->intro !!}</li>--}}
{{--                    @endforeach--}}
                    <li>
                        <img style="position: absolute; left: 0; top: 10px; height: 75px;left: 15px;" src="https://giaohangtietkiem.vn/wp-content/uploads/2020/11/vn-01.png">
                        <br>
                        <strong><br>
                           PHỦ SÓNG 3 MIỀN  VÀ QUỐC TẾ<br>
                        </strong><br>
                        <span style="line-height: 1.5;">Lấy và giao hàng tại 63 tỉnh thành ở Việt Nam và một số nước trên thế giới như Đài Loan, Nhật Bản, Hàn Quốc, Malaysia, Thái Lan…</span>

                    </li>
                    <li>
                        <img  src="https://giaohangtietkiem.vn/wp-content/themes/giaohangtk/images/icon-02.png">
                        <br>
                        <strong><br>
                            KIỂM SOÁT HÀNG HÓA MỌI LÚC MỌI NƠI<br>
                        </strong><br>
                        <span style="line-height: 1.5;"> Khách hàng luôn biết thực tế gói hàng của mình ở vỊ T, ai đang vận chuyển và ước tính thời gian được giao đến</span>

                    </li>
                    <li>
                        <img  src="https://giaohangtietkiem.vn/wp-content/themes/giaohangtk/images/icon-05.png">
                        <br>
                        <strong><br>
                            PHÁT TRIỂN APP TRÊN ĐIỆN THOẠI<br>
                        </strong><br>
                        <span style="line-height: 1.5;"> Tạo vận đơn – Tracking đơn hàng – Theo dõi, xử lý đơn hàng mọi lúc mọi nơi trên điện thoại!</span>

                    </li>
                    <li>
                        <img  src="https://giaohangtietkiem.vn/wp-content/themes/giaohangtk/images/icon-06.png">
                        <br>
                        <strong><br>
                            GIAO HÀNG NHANH VÀ LINH HOẠT<br>
                        </strong><br>
                        <span style="line-height: 1.5;"> Linh hoạt giao hàng cho khách chọn, đổi địa chỉ giao, đổi tiền thu hộ, đổi SĐT, đổi người nhận hàng</span>

                    </li>
                    <li>
                        <img  src="https://giaohangtietkiem.vn/wp-content/themes/giaohangtk/images/icon-06.png">
                        <br>
                        <strong><br>
                            ĐỐI SOÁT ĐƠN HÀNG TRẢ TIỀN NGAY<br>
                        </strong><br>
                        <span style="line-height: 1.5;">  Chuyển khoản ngay vào tài khoản ngân hàng mỗi ngày với các đơn hàng được giao hàng thành công trong ngày</span>

                    </li>
                    <li>
                        <img  src="https://giaohangtietkiem.vn/wp-content/themes/giaohangtk/images/icon-06.png">
                        <br>
                        <strong><br>
                            MIỄN PHÍ GIAO LẠI NHIỀU LẦN<br>
                        </strong><br>
                        <span style="line-height: 1.5;"> Hỗ trợ giao lại các đơn hàng tới nguời mua hàng nhiều lần trước khi đơn hàng được trả lại cho nhà bán hàng nếu người mua không lấy hàng.</span>

                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
{{--<div class="show_dangky modal fade" id="exampleModalLong" tabindex="-1" role="dialog"--}}
{{--     aria-labelledby="exampleModalLongTitle" aria-hidden="true">--}}
{{--    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">--}}
{{--        <div class="modal-content p-3">--}}
{{--           <h4>Bạn cần đăng nhập để tạo vận đơn</h4>--}}
{{--            <button type="button" class="btn btn-warning" style="    background-color: #FCD804;--}}
{{--    border-color: #FCD804;">Đăng nhập ngay</button>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
@section('script')
    <script type="text/javascript">
        // $(document).ready(function () {
        //     $(".btn-register").click(function () {
        //         $('.show_dangky').modal('show');
        //         // var course_id = $(this).data('course_id');
        //         // console.log(course_id);
        //         // $('form.dang-ky-course input[name=course_id]').val(course_id);
        //     });
        // });
    </script>
@endsection
