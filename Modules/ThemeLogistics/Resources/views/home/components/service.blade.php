<div class="w-100 wp-service ">
    <div class="container container-service">
        <!-- <div class="row m-0">
            <a href="chuyen-phat-hoa-toc.html#content"
               class="item-service active  bg-fff translate text-center position-relative p-3">
                <p class="mb-3 mb-sm-0" style="height: 50px;">
                    <img src="images/img/icon-hoa-toc.png" alt="" class="icon-hoa-toc">
                </p>
                Chuyển phát hỏa tốc
            </a>
            <a href="chuyen-phat-nhanh.html#content"
               class="item-service  bg-fff translate text-center position-relative p-3">
                <p class="ntl-CPN1 mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span
                        class="path3"></span><span class="path4"></span><span class="path5"></span><span
                        class="path6"></span><span class="path7"></span><span class="path8"></span><span
                        class="path9"></span><span class="path10"></span></p>
                Chuyển phát nhanh
            </a>
            <a href="chuyen-phat-ket-hop.html#content"
               class="item-service   bg-fff translate text-center position-relative p-3">
                <p class="ntl-CP-Ket-Hop mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span
                        class="path3"></span><span class="path4"></span><span class="path5"></span><span
                        class="path6"></span><span class="path7"></span><span class="path8"></span><span
                        class="path9"></span><span class="path10"></span><span class="path11"></span><span
                        class="path12"></span></p>
                Chuyển phát tiết kiệm
            </a>
            <a href="chuyen-phat-duong-bo.html#content"
               class="item-service   bg-fff translate text-center position-relative p-3">
                <p class="ntl-CP-duong-bo mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span
                        class="path3"></span><span class="path4"></span><span class="path5"></span><span
                        class="path6"></span><span class="path7"></span><span class="path8"></span><span
                        class="path9"></span><span class="path10"></span><span class="path11"></span><span
                        class="path12"></span><span class="path13"></span><span class="path14"></span><span
                        class="path15"></span><span class="path16"></span><span class="path17"></span><span
                        class="path18"></span><span class="path19"></span><span class="path20"></span><span
                        class="path21"></span><span class="path22"></span><span class="path23"></span><span
                        class="path24"></span><span class="path25"></span><span class="path26"></span><span
                        class="path27"></span><span class="path28"></span><span class="path29"></span></p>
                Chuyển phát đường bộ
            </a>
            <a href="chuyen-phat-thu-ho.html#content"
               class="item-service   bg-fff translate text-center position-relative p-3">
                <p class="ntl-Thu-ho-COD mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span
                        class="path3"></span><span class="path4"></span><span class="path5"></span></p>
                Chuyển phát thu hộ (COD)
            </a>
            <a href="chuyen-phat-nguyen-xe.html#content"
               class="item-service   bg-fff translate text-center position-relative p-3">
                <p class="ntl-Nguyen-xe mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span
                        class="path3"></span></p>Thuê xe nguyên chuyến
            </a>
        </div> -->
        <div class="row bx-sh">
            @php
                $transport_type = \Modules\ThemeLogistics\Models\TransportType::orderBy('order_no')->get();
            @endphp

            @foreach($transport_type as $k => $item)
            <div class="col-lg-4 col-sm-4 col-4">
                <a data-id={{ $item->id }} data-value="tab_service_{{ $k  }}" href="javascript:void(0)" class="service-box @if($k == 0) active @endif" >
                    <div class="box-img">
                        <img src="{{ CommonHelper::getUrlImageThumb($item->image) }}" alt="">
                    </div>
                    <p class="box-content">{{ $item->name  }}</p>
                </a>
            </div>
            @endforeach

{{--            <div class="col-lg-4 col-sm-4 col-4">--}}
{{--                <a data-value="quocte" href="javascript:void(0)" class="service-box" >--}}
{{--                    <div class="box-img">--}}
{{--                        <p class="ntl-CPN1 mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span--}}
{{--                                    class="path3"></span><span class="path4"></span><span class="path5"></span><span--}}
{{--                                    class="path6"></span><span class="path7"></span><span class="path8"></span><span--}}
{{--                                    class="path9"></span><span class="path10"></span></p>--}}
{{--                    </div>--}}
{{--                    <p class="box-content">Vận chuyển quốc tế</p>--}}
{{--                </a>--}}
{{--            </div>--}}

{{--            <div class="col-lg-4 col-sm-4 col-4">--}}
{{--                <a data-value="noidiadailoan" href="javascript:void(0)" class="service-box" >--}}
{{--                    <div class="box-img">--}}
{{--                        <p class="ntl-CP-Ket-Hop mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span--}}
{{--                                    class="path3"></span><span class="path4"></span><span class="path5"></span><span--}}
{{--                                    class="path6"></span><span class="path7"></span><span class="path8"></span><span--}}
{{--                                    class="path9"></span><span class="path10"></span><span class="path11"></span><span--}}
{{--                                    class="path12"></span></p>--}}
{{--                    </div>--}}
{{--                    <p class="box-content">Vận chuyển nội địa Đài Loan</p>--}}
{{--                </a>--}}
{{--            </div>--}}
        </div>
    </div>
</div>