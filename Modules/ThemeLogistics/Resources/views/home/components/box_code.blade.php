@php

    function createSelect($data, $keyselect = null){
        $html = '';
        foreach($data as $k => $item){
            $selected = $keyselect == $k ? "selected" : "";
            $html .= '<option '.$selected.' value="'.$k.'">'.$item.'</option>';
        }

        return $html;
    }
    $service = CommonHelper::getFromCache('widgets_home_home_service');
     if (!$service) {
    $service = \Modules\ThemeLogistics\Models\Widget::where('status', 1)->where('location', 'home_service')->orderBy('order_no', 'ASC')->first();
    CommonHelper::putToCache('widgets_home_home_service', $service);
}
@endphp
<section class="w-100 mt-2 mb-3">
    <div class="container">
        <div class="row align-items-center d-flex">
            @php
                $transport_type = \Modules\ThemeLogistics\Models\TransportType::orderBy('order_no')->get();
            @endphp
            @foreach($transport_type as $k => $item)
            <div class="box-cp-l box-cp tab_box @if($k == 0) active @endif" id="tab_service_{{ $k  }}">
                <h1 class="entry-cp font-IntelBold mb-2">{{ $item->name  }}</h1>
                <div class="summary text-justify">Dịch vụ CPTH (COD) là dịch vụ chuyển phát hàng hóa kết hợp
                    thu tiền cho khách hàng. Phù hợp với những khách hàng kinh doanh thương mại điện tử
                    (E-commerce). Thời gian chuyển phát tiêu chuẩn theo lựa chọn của khách hàng từ 01 ngày
                    đến 07 ngày tùy theo khu vực giao hàng.
                </div>
                <a href="tin-tuc/dv-van-chuyen-thuong-mai-dien-tu-cod/index.html" class="d-inline-block mt-2 mb-2">Xem
                    thêm <i class="ntl-Thin-Arrow-4"></i></a>
            </div>
           @endforeach

            <div class="box-cp-r">
                <div class="w-100 border pt-2 pl-md-3 pr-md-3">
                    <form class="row position-relative form-service pb-5 pb-md-0"
                          action="#" method="get" id="tra_cuoc">
                        <input type="hidden" name="package_no" value="0">
                        <input type="hidden" name="transport_type" value="1">
                        <input type="hidden" name="so_km" value="0">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <label id="label_noigui" class="text-dark font-size-16">Nơi gửi</label>
                            <div class="form-group">
                                <input id="from_places_home" placeholder="Nơi gửi"  type="text" name="shipping_diachi"  class="input__control">
                                <input type="hidden" name="hidden_noigui" value="">
                            </div>
                            {{--                            <div class="row">--}}

                            {{--                                <div class="col-lg-6 col-sm-6 col-12 pr-sm-1">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <!--start section -->--}}
                            {{--                                        <div class="dropdown bootstrap-select form-control change_city border-cam">--}}
                            {{--                                            <select name="send_city_id" class="selectpicker form-control change_city border-cam" id="send_city_id" data-id="send_district_id" data-live-search="true" data-size="10" tabindex="-98">--}}
                            {{--                                            {!! createSelect($province, 0) !!}--}}
                            {{--                                            </select>--}}
                            {{--                                        </div>--}}
                            {{--                                        <!-- end section -->--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-lg-6 col-sm-6 col-12 pl-sm-1">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <!--start section -->--}}
                            {{--                                        <div class="dropdown bootstrap-select form-control change_city border-cam">--}}
                            {{--                                            <select name="send_district_id" class="selectpicker form-control border-cam" id="send_district_id" data-id="send_district_id" data-live-search="true" data-size="10" tabindex="-98">--}}
                            {{--                                                {!! createSelect($district,0) !!}--}}
                            {{--                                            </select>--}}
                            {{--                                        </div>--}}
                            {{--                                        <!-- end section -->--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 pl-md-1">
                            <label id="label_noinhan" class="text-dark font-size-16">Nơi nhận</label>
                            <div class="form-group">
                                <input id="to_places_home" placeholder="Nơi nhận"  type="text" name="billing_diachi"  class="input__control">
                                <input type="hidden" name="hidden_noinhan" value="">
                            </div>
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-lg-6 col-sm-6 col-12 pr-sm-1">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <!--start section -->--}}
                            {{--                                        <div class="dropdown bootstrap-select form-control change_city border-cam">--}}
                            {{--                                            <select name="recieve_city_id" class="selectpicker form-control change_city border-cam" id="recieve_city_id" data-id="recieve_city_id" data-live-search="true" data-size="10" tabindex="-98">--}}
                            {{--                                                {!! createSelect($province, 0) !!}--}}
                            {{--                                            </select>--}}
                            {{--                                        </div>--}}
                            {{--                                        <!-- end section -->--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="col-lg-6 col-sm-6 col-12 pl-sm-1">--}}
                            {{--                                    <div class="form-group">--}}
                            {{--                                        <!--start section -->--}}
                            {{--                                        <div class="dropdown bootstrap-select form-control change_city border-cam">--}}
                            {{--                                            <select name="recieve_district_id" class="selectpicker form-control border-cam" id="recieve_district_id" data-id="recieve_district_id" data-live-search="true" data-size="10" tabindex="-98">--}}
                            {{--                                                {!! createSelect($district,0) !!}--}}
                            {{--                                            </select>--}}
                            {{--                                        </div>--}}
                            {{--                                        <!-- end section -->--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 position-static">
                            <div class="form-group">
                                {{--                                <label id="label_trongluong" class="text-dark font-size-16">Trọng lượng (kg)</label>--}}
                                {{--                                <div class="w-100">--}}
                                {{--                                    <input id="inp_weight" type="number" class="form-control" name="weight"--}}
                                {{--                                           placeholder="Nhập trọng lượng">--}}
                                {{--                                </div>--}}
                                {{--                                <label id="label_sokienhang" class="text-dark font-size-16">Số kiện hàng (thùng)</label>--}}
                                {{--                                <div class="w-100">--}}
                                {{--                                    <input id="inp_kienhang" type="number" class="form-control" name="sokienhang"--}}
                                {{--                                           placeholder="Số kiện hàng">--}}
                                {{--                                </div>--}}
                                <div class="msg-error"></div>
                                <div class="form-check-inline mb-3">
                                    <label class="form-check-label">
                                        <input checked value="0" type="radio" class="form-check-input" name="type_quantity"><span class="text_type">Trọng lượng</span>
                                    </label>
                                </div>
                                <div class="form-check-inline mb-3">
                                    <label class="form-check-label">
                                        <input value="1" type="radio" class="form-check-input" name="type_quantity"><span class="text_type">Số kiện</span>
                                    </label>
                                </div>
                                <input placeholder="Số Kg" id="inp_quantity" type="number" class=" form-control" name="quantity">
                                <div class="w-100 font-size-14 text-dark mt-sm-3 mt-1">* Chọn đúng đặc tính sẽ giúp
                                    hàng của bạn đi đúng lịch trình, ước lượng và tìm được đúng giá cước
                                </div>
                                <div class="w-100 mt-sm-3 position-submit">
                                    <input type="hidden" id="feature" name="feature" value="1">
                                    <input type="hidden" id="cargo_content" name="cargo_content" value="Tài liệu">
                                    <a href="javascript:void(0)" id="tracuu" class="btn btn-primary">Tra cứu giá cước</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 pl-md-1">
                            <div class="form-group">
                                <label id="label_dactinh" class="text-dark font-size-16">Đặc tính hàng hóa</label>
                                <ul id="dactinh_home" class="row dactinh">

{{--                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1"--}}
{{--                                        data-id="Tài liệu" data-value="1">--}}
{{--                                        <div class="w-100 text-dark text-center ">--}}
{{--                                                <span class="icon ntl-Letter d-block"><span class="path1"></span><span--}}
{{--                                                            class="path2"></span><span class="path3"></span><span--}}
{{--                                                            class="path4"></span><span class="path5"></span><span--}}
{{--                                                            class="path6"></span><span class="path7"></span><span--}}
{{--                                                            class="path8"></span><span class="path9"></span><span--}}
{{--                                                            class="path10"></span><span class="path11"></span><span--}}
{{--                                                            class="path12"></span><span class="path13"></span><span--}}
{{--                                                            class="path14"></span><span class="path15"></span><span--}}
{{--                                                            class="path16"></span></span>--}}
{{--                                            <div class="mt-2">Tài liệu</div>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1"--}}
{{--                                        data-id="Thời trang Mỹ phẩm" data-value="2">--}}
{{--                                        <div class="w-100 text-dark text-center">--}}
{{--                                                <span class="icon ntl-Coat-and-lipstick d-block"><span--}}
{{--                                                            class="path1"></span><span class="path2"></span><span--}}
{{--                                                            class="path3"></span><span class="path4"></span><span--}}
{{--                                                            class="path5"></span><span class="path6"></span><span--}}
{{--                                                            class="path7"></span><span class="path8"></span><span--}}
{{--                                                            class="path9"></span><span class="path10"></span><span--}}
{{--                                                            class="path11"></span><span class="path12"></span><span--}}
{{--                                                            class="path13"></span><span class="path14"></span><span--}}
{{--                                                            class="path15"></span><span class="path16"></span><span--}}
{{--                                                            class="path17"></span><span class="path18"></span><span--}}
{{--                                                            class="path19"></span><span class="path20"></span><span--}}
{{--                                                            class="path21"></span></span>--}}
{{--                                            <div class="mt-2">Thời trang Mỹ phẩm</div>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1" data-id="Đồ điện tử"--}}
{{--                                        data-value="3">--}}
{{--                                        <div class="w-100 text-dark text-center">--}}
{{--                                                <span class="icon ntl-Phone d-block"><span class="path1"></span><span--}}
{{--                                                            class="path2"></span><span class="path3"></span><span--}}
{{--                                                            class="path4"></span><span class="path5"></span><span--}}
{{--                                                            class="path6"></span><span class="path7"></span><span--}}
{{--                                                            class="path8"></span><span class="path9"></span><span--}}
{{--                                                            class="path10"></span><span class="path11"></span><span--}}
{{--                                                            class="path12"></span><span class="path13"></span><span--}}
{{--                                                            class="path14"></span><span class="path15"></span><span--}}
{{--                                                            class="path16"></span><span class="path17"></span><span--}}
{{--                                                            class="path18"></span><span class="path19"></span><span--}}
{{--                                                            class="path20"></span><span class="path21"></span><span--}}
{{--                                                            class="path22"></span><span class="path23"></span><span--}}
{{--                                                            class="path24"></span><span class="path25"></span><span--}}
{{--                                                            class="path26"></span><span class="path27"></span></span>--}}
{{--                                            <div class="mt-2">Đồ điện tử--}}
{{--                                                <!-- <br>tử --></div>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1"--}}
{{--                                        data-id="Dược phẩm TPCN" data-value="4">--}}
{{--                                        <div class="w-100 text-dark text-center">--}}
{{--                                                <span class="icon ntl-Medicine d-block"><span class="path1"></span><span--}}
{{--                                                            class="path2"></span><span class="path3"></span><span--}}
{{--                                                            class="path4"></span><span class="path5"></span><span--}}
{{--                                                            class="path6"></span><span class="path7"></span><span--}}
{{--                                                            class="path8"></span><span class="path9"></span><span--}}
{{--                                                            class="path10"></span><span class="path11"></span><span--}}
{{--                                                            class="path12"></span><span class="path13"></span><span--}}
{{--                                                            class="path14"></span></span>--}}
{{--                                            <div class="mt-2">Dược phẩm--}}
{{--                                                <!-- <br>TPCN --></div>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1" data-id="Thực phẩm"--}}
{{--                                        data-value="5">--}}
{{--                                        <div class="w-100 text-dark text-center">--}}
{{--                                                <span class="icon ntl-Bread d-block"><span class="path1"></span><span--}}
{{--                                                            class="path2"></span><span class="path3"></span><span--}}
{{--                                                            class="path4"></span><span class="path5"></span><span--}}
{{--                                                            class="path6"></span><span class="path7"></span><span--}}
{{--                                                            class="path8"></span><span class="path9"></span><span--}}
{{--                                                            class="path10"></span><span class="path11"></span><span--}}
{{--                                                            class="path12"></span></span>--}}
{{--                                            <div class="mt-2">Thực phẩm</div>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1"--}}
{{--                                        data-id="Đồ tươi sống" data-value="6">--}}
{{--                                        <div class="w-100 text-dark text-center">--}}
{{--                                                <span class="icon ntl-Meat d-block"><span class="path1"></span><span--}}
{{--                                                            class="path2"></span><span class="path3"></span><span--}}
{{--                                                            class="path4"></span><span class="path5"></span><span--}}
{{--                                                            class="path6"></span><span class="path7"></span><span--}}
{{--                                                            class="path8"></span><span class="path9"></span><span--}}
{{--                                                            class="path10"></span><span class="path11"></span><span--}}
{{--                                                            class="path12"></span></span>--}}
{{--                                            <div class="mt-2">Đồ tươi sống</div>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1" data-id="Đồ có pin"--}}
{{--                                        data-value="7">--}}
{{--                                        <div class="w-100 text-dark text-center">--}}
{{--                                                <span class="icon ntl-Battery d-block"><span class="path1"></span><span--}}
{{--                                                            class="path2"></span><span class="path3"></span><span--}}
{{--                                                            class="path4"></span></span>--}}
{{--                                            <div class="mt-2">Đồ có pin</div>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1"--}}
{{--                                        data-id="Chất bột nước" data-value="8">--}}
{{--                                        <div class="w-100 text-dark text-center">--}}
{{--                                                <span class="icon ntl-Water d-block"><span class="path1"></span><span--}}
{{--                                                            class="path2"></span><span class="path3"></span><span--}}
{{--                                                            class="path4"></span><span class="path5"></span><span--}}
{{--                                                            class="path6"></span></span>--}}
{{--                                            <div class="mt-2 mb-1">Chất bột nước</div>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1"--}}
{{--                                        data-id="Chất dễ cháy" data-value="9">--}}
{{--                                        <div class="w-100 text-dark text-center">--}}
{{--                                                <span class="icon ntl-Fire d-block"><span class="path1"></span><span--}}
{{--                                                            class="path2"></span><span class="path3"></span><span--}}
{{--                                                            class="path4"></span></span>--}}
{{--                                            <div class="mt-2">Chất dễ cháy</div>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
                                </ul>
                            </div>
                        </div>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="box-modal-service" id="box-modal-service">

</div>
{{--<div class="modal fade" id="service-modal" role="dialog">--}}
{{--    <div class="modal-dialog modal-lg">--}}
{{--        <div class="modal-content">--}}
{{--            <form id="frmVanDon" action="#" method="POST" enctype="multipart/form-data">--}}
{{--                {{ csrf_field() }}--}}
{{--                <div class="modal-body modal_vandon">--}}
{{--                    <div class="shipping-method box-sd">--}}
{{--                        <h4 class="box-title">1. Tab phương thức vận chuyển</h4>--}}
{{--                        <div class="row m-0">--}}
{{--                            --}}{{--                        <div class="method-content">--}}
{{--                            --}}{{--                            <a href="chuyen-phat-hoa-toc.html#content"--}}
{{--                            --}}{{--                               class="item-service bg-fff translate text-center position-relative p-3">--}}
{{--                            --}}{{--                                <p class="mb-3 mb-sm-0" style="height: 50px;">--}}
{{--                            --}}{{--                                    <img src="./images/img/icon-hoa-toc.png" alt="" class="icon-hoa-toc">--}}
{{--                            --}}{{--                                </p>--}}
{{--                            --}}{{--                                <p class="service-text">--}}
{{--                            --}}{{--                                    Chuyển phát hỏa tốc--}}
{{--                            --}}{{--                                </p>--}}
{{--                            --}}{{--                            </a>--}}
{{--                            --}}{{--                            <span class="service-price">100.000VND</span>--}}
{{--                            --}}{{--                        </div>--}}
{{--                            <div class="method-content disabled">--}}

{{--                                <a data-id="3" href="javascript:void(0)"--}}
{{--                                   class="item-service  bg-fff translate text-center position-relative  p-3">--}}
{{--                                    <p class="ntl-CPN1 mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span--}}
{{--                                                class="path3"></span><span class="path4"></span><span class="path5"></span><span--}}
{{--                                                class="path6"></span><span class="path7"></span><span class="path8"></span><span--}}
{{--                                                class="path9"></span><span class="path10"></span></p>--}}
{{--                                    <p class="service-text">--}}
{{--                                        Chuyển phát hỏa tốc--}}
{{--                                    </p>--}}
{{--                                </a>--}}
{{--                                <input type="hidden" name="hidden_price">--}}
{{--                                <span class="service-price">100.000VNĐ</span>--}}
{{--                            </div>--}}
{{--                            <div class="method-content disabled">--}}

{{--                                <a data-id="4" href="javascript:void(0)"--}}
{{--                                   class="item-service  bg-fff translate text-center position-relative p-3">--}}
{{--                                    <p class="ntl-CPN1 mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span--}}
{{--                                                class="path3"></span><span class="path4"></span><span class="path5"></span><span--}}
{{--                                                class="path6"></span><span class="path7"></span><span class="path8"></span><span--}}
{{--                                                class="path9"></span><span class="path10"></span></p>--}}
{{--                                    <p class="service-text">--}}
{{--                                        Chuyển phát nhanh--}}
{{--                                    </p>--}}
{{--                                </a>--}}
{{--                                <input type="hidden" name="hidden_price">--}}
{{--                                <span class="service-price">100.000VNĐ</span>--}}
{{--                            </div>--}}
{{--                            <div class="method-content disabled">--}}

{{--                                <a data-id="5" href="javascript:void(0)"--}}
{{--                                   class="item-service   bg-fff translate text-center position-relative p-3">--}}
{{--                                    <p class="ntl-CP-Ket-Hop mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span--}}
{{--                                                class="path3"></span><span class="path4"></span><span class="path5"></span><span--}}
{{--                                                class="path6"></span><span class="path7"></span><span class="path8"></span><span--}}
{{--                                                class="path9"></span><span class="path10"></span><span class="path11"></span><span--}}
{{--                                                class="path12"></span></p>--}}
{{--                                    <p class="service-text">--}}
{{--                                        Chuyển phát tiết kiệm--}}
{{--                                    </p>--}}
{{--                                </a>--}}
{{--                                <input type="hidden" name="hidden_price">--}}
{{--                                <span class="service-price">100.000VNĐ</span>--}}
{{--                            </div>--}}
{{--                            <div class="method-content disabled">--}}

{{--                                <a data-id="6" href="javascript:void(0)"--}}
{{--                                   class="item-service   bg-fff translate text-center position-relative p-3">--}}
{{--                                    <p class="ntl-CP-duong-bo mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span--}}
{{--                                                class="path3"></span><span class="path4"></span><span class="path5"></span><span--}}
{{--                                                class="path6"></span><span class="path7"></span><span class="path8"></span><span--}}
{{--                                                class="path9"></span><span class="path10"></span><span class="path11"></span><span--}}
{{--                                                class="path12"></span><span class="path13"></span><span class="path14"></span><span--}}
{{--                                                class="path15"></span><span class="path16"></span><span class="path17"></span><span--}}
{{--                                                class="path18"></span><span class="path19"></span><span class="path20"></span><span--}}
{{--                                                class="path21"></span><span class="path22"></span><span class="path23"></span><span--}}
{{--                                                class="path24"></span><span class="path25"></span><span class="path26"></span><span--}}
{{--                                                class="path27"></span><span class="path28"></span><span class="path29"></span></p>--}}
{{--                                    <p class="service-text">--}}
{{--                                        Chuyển phát đường bộ--}}
{{--                                    </p>--}}
{{--                                </a>--}}
{{--                                <input type="hidden" name="hidden_price">--}}
{{--                                <span class="service-price">100.000VNĐ</span>--}}
{{--                            </div>--}}
{{--                            <div class="method-content disabled">--}}

{{--                                <a data-id="7" href="javascript:void(0)"--}}
{{--                                   class="item-service   bg-fff translate text-center position-relative p-3">--}}
{{--                                    <p class="ntl-Thu-ho-COD mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span--}}
{{--                                                class="path3"></span><span class="path4"></span><span class="path5"></span></p>--}}
{{--                                    <p class="service-text">--}}
{{--                                        Chuyển phát thu hộ (COD)--}}
{{--                                    </p>--}}
{{--                                </a>--}}
{{--                                <input type="hidden" name="hidden_price">--}}
{{--                                <span class="service-price">100.000VNĐ</span>--}}
{{--                            </div>--}}
{{--                            <div class="method-content disabled">--}}

{{--                                <a data-id="8" href="javascript:void(0)"--}}
{{--                                   class="item-service   bg-fff translate text-center position-relative p-3">--}}
{{--                                    <p class="ntl-Nguyen-xe mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span--}}
{{--                                                class="path3"></span></p><p class="service-text">--}}
{{--                                        Thuê xe nguyên chuyến--}}
{{--                                    </p>--}}
{{--                                </a>--}}
{{--                                <input type="hidden" name="hidden_price">--}}
{{--                                <span class="service-price">100.000VNĐ</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="box-info box-sd">--}}
{{--                        <h4 class="box-title">2. Nhập thông tin hàng hóa </h4>--}}

{{--                        <div class="box-input">--}}
{{--                    <span>--}}
{{--                        <label for="">Tên hàng</label>--}}

{{--                        <input data-placeholder="Tên" placeholder="Tên" name="tenhang" type="text" class="input__control check_require">--}}
{{--                    </span>--}}
{{--                            <span>--}}
{{--                        <label for="">Mặt hàng</label>--}}
{{--                        <div class="dropdown bootstrap-select form-control change_city border-cam">--}}
{{--                            <select name="mahang" class="selectpicker form-control mahang border-cam" id="mahang" data-id="mahang" data-live-search="true" data-size="8" tabindex="-98">--}}
{{--                               {!! createSelect($dataDacTinh, 1) !!}--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                    </span>--}}
{{--                            --}}{{--                        <span>--}}
{{--                            --}}{{--                            <label for="">Trọng lượng(kg):</label>--}}
{{--                            --}}{{--                            <input name="weight" type="number" class="input__control" placeholder="kg">--}}
{{--                            --}}{{--                        </span>--}}
{{--                            --}}{{--                        <span>--}}
{{--                            --}}{{--                            <label for="">Số kiện hàng(thùng):</label>--}}
{{--                            --}}{{--                            <input name="sokienhang" type="number" class="input__control" placeholder="thùng">--}}
{{--                            --}}{{--                        </span>--}}
{{--                            <div class="">--}}
{{--                                <div class="form-check-inline mb-3">--}}
{{--                                    <label class="form-check-label">--}}
{{--                                        <input checked="" value="trongluong" type="radio" class="form-check-input" name="type_quantity"><span class="text_type">Trọng lượng</span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                                <div class="form-check-inline mb-3">--}}
{{--                                    <label class="form-check-label">--}}
{{--                                        <input value="kienhang" type="radio" class="form-check-input" name="type_quantity"><span class="text_type">Số kiện</span>--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                                <div class="form-check-inline mb-3">--}}
{{--                                    <label class="form-check-label">--}}
{{--                                        <span class="text_type">Số lượng</span> <input style="width:initial;margin-left:4px" name="quantity" type="number" class="input__control check_require">--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            --}}{{--                        <span>--}}
{{--                            --}}{{--                            <label for="">Số lượng</label>--}}
{{--                            --}}{{--                            <input name="quantity" type="number" class="input__control check_require">--}}
{{--                            --}}{{--                         </span>--}}
{{--                            <span>--}}
{{--                        <label for="">Hình ảnh</label>--}}
{{--                        <div class="file">--}}
{{--                            <input name="picture" type="file" class="file-choose">--}}
{{--                            <div class="file-btn btn-m">--}}
{{--                                File--}}
{{--                                <div id="preview_img"></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </span>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                    <div class="box-address box-sd">--}}
{{--                        <div action="" class="shipping-address address-form mg-r">--}}
{{--                            <h4 class="box-title" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">2. Địa chỉ người bán </h4>--}}

{{--                            <span class="form__control">--}}
{{--                        <div class="control-top">--}}
{{--                            <input data-placeholder="Họ và tên" type="text" name="shipping_hoten" placeholder="Họ và tên" class="input__control check_require">--}}
{{--                            <input data-placeholder="SĐT" type="text" name="shipping_sdt" placeholder="SĐT" class="input__control check_require">--}}
{{--                        </div>--}}
{{--                        <input id="from_places"  data-placeholder="Địa chỉ" type="text" name="shipping_diachi" placeholder="Địa chỉ: " class="check_require input__control">--}}
{{--                            <input id="origin" name="hidden_noigui" required="" type="hidden" />--}}
{{--                            <div class="control-body">--}}

{{--                                --}}{{--                                            <select data-placeholder="Tỉnh/TP" name="send_city_id" class="check_select selectpicker form-control change_city border-cam" id="send_city_id" data-id="send_district_id" data-live-search="true" data-size="10" tabindex="-98">--}}
{{--                                --}}{{--                                            {!! createSelect($province, 1) !!}--}}
{{--                                --}}{{--                                            </select>--}}


{{--                                --}}{{--                                            <select data-placeholder="Huyện/Quận" name="send_district_id" class="check_select selectpicker form-control border-cam" id="send_district_id" data-id="send_district_id" data-live-search="true" data-size="10" tabindex="-98">--}}
{{--                                --}}{{--                                                {!! createSelect($district) !!}--}}
{{--                                --}}{{--                                            </select>--}}

{{--                                --}}{{--                        </div>--}}
{{--                        <input  data-placeholder="Email" type="email" name="shipping_email" placeholder="Email " class="input__control">--}}
{{--                    </span>--}}
{{--                        </div>--}}

{{--                        <div action="" class="billing-address address-form">--}}
{{--                            <h4 class="box-title">2. Địa chỉ người mua</h4>--}}
{{--                            <span class="form__control">--}}
{{--                        <div class="control-top">--}}
{{--                            <input data-placeholder="Họ và tên" type="text" name="billing_hoten" placeholder="Họ và tên" class="input__control check_require">--}}
{{--                            <input data-placeholder="SĐT" type="text" name="billing_sdt" placeholder="SĐT" class="input__control check_require">--}}
{{--                        </div>--}}
{{--                        <input id="to_places" data-placeholder="Địa chỉ" type="text" name="billing_diachi" placeholder="Địa chỉ: " class="input__control check_require">--}}
{{--                        <input id="destination" name="hidden_noinhan" required="" type="hidden" />--}}
{{--                            <div class="control-body">--}}

{{--                                --}}{{--                                            <select data-placeholder="Tỉnh/TP" name="recieve_city_id" class="check_select selectpicker form-control change_city border-cam" id="recieve_city_id" data-id="recieve_city_id" data-live-search="true" data-size="10" tabindex="-98">--}}
{{--                                --}}{{--                                                {!! createSelect($province, 1) !!}--}}
{{--                                --}}{{--                                            </select>--}}


{{--                                --}}{{--                                            <select data-placeholder="Huyện/Quận" name="recieve_district_id" class="check_select selectpicker form-control border-cam" id="recieve_district_id" data-id="recieve_district_id" data-live-search="true" data-size="10" tabindex="-98">--}}
{{--                                --}}{{--                                                {!! createSelect($district) !!}--}}
{{--                                --}}{{--                                            </select>--}}

{{--                                --}}{{--                        </div>--}}
{{--                        <input data-placeholder="Email" type="email" name="billing_email" placeholder="Email " class="input__control">--}}
{{--                    </span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    @if (@\Auth::guard('admin')->check())--}}
{{--                        <div class="box-voucher box-sd" id="accordion3">--}}
{{--                            <h4 class="box-title" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">3. Mã giảm giá <span><i class="fas fa-chevron-down"></i></span></h4>--}}
{{--                            <div id="collapseThree" class="collapse" aria-labelledby="headingOne" data-parent="#accordion3">--}}
{{--                                <div class="box-input">--}}
{{--                                    <input name="coupon" type="text" placeholder="Nhập mã giảm giá" class="input__control">--}}
{{--                                    <button class="v-btn">--}}
{{--                                        <a href="javascript:checkCoupon()" class="b-btn">Nhập mã</a>--}}
{{--                                    </button>--}}
{{--                                </div>--}}
{{--                                <p style="margin-bottom:0px" class="status-coupon"></p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="box-comment box-sd" id="accordion4">--}}
{{--                            <h4 class="box-title" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">4. Ghi chú cho đơn hàng <span><i class="fas fa-chevron-down"></i></span></h4>--}}
{{--                            <div id="collapseFour" class="collapse" aria-labelledby="headingOne" data-parent="#accordion4">--}}
{{--                                <textarea name="ghichu" rows="3" class="input__control"></textarea>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="box-paypal box-sd" id="accordion5">--}}
{{--                            <h4 class="box-title" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">5. Tiền thu hộ <span><i class="fas fa-chevron-down"></i></span></h4>--}}
{{--                            <div id="collapseFive" class="collapse" aria-labelledby="headingOne" data-parent="#accordion5">--}}
{{--                                <input type="number" data-placeholder="Tiền thu hộ"  name="cod" type="text" class="input__control">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="box-paypal box-sd" id="accordion6">--}}
{{--                            <h4 class="box-title" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">6. Phương thức thanh toán <span><i class="fas fa-chevron-down"></i></span></h4>--}}
{{--                            <div id="collapseSix" class="collapse" aria-labelledby="headingOne" data-parent="#accordion6">--}}
{{--                                <div action="">--}}
{{--                        <span>--}}
{{--                            <input value="1" type="radio" name= "paypal" id="paypal-online">--}}
{{--                            <label for="paypal-online">Người bán trả</label>--}}
{{--                        </span>--}}
{{--                                    <span>--}}
{{--                            <input value="2" type="radio" name= "paypal" id="paypal-money">--}}
{{--                            <label for="paypal-money">Người mua trả</label>--}}
{{--                        </span>--}}
{{--                                </div>--}}
{{--                                <div class="paypal-img">--}}
{{--                                    <img src="./images/img/paypal.jpg" alt="">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                    <div class="box-total box-sd">--}}
{{--                        <div class="box-title">7. Thành tiền ( chưa VAT )</div>--}}
{{--                        <div class="total-price b-p">--}}
{{--                            <p>- Tổng tiền</p>--}}
{{--                            <span id="totalPrice">0</span>--}}
{{--                        </div>--}}
{{--                        <div id="box_coupon"></div>--}}
{{--                        <div class="vat-price b-p">--}}
{{--                            <p>- 10% VAT</p>--}}
{{--                            <span id="vat">0</span>--}}
{{--                        </div>--}}
{{--                        <div id="box_thuho"></div>--}}
{{--                        <div class="done-price b-p">--}}
{{--                            <p>Thành tiền</p>--}}
{{--                            <span id="totalPriceFinal">0</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    @if (@\Auth::guard('admin')->check())--}}
{{--                        <button class="box-footer" type="button" id="btn_footer_vandon">--}}
{{--                            Tạo Vận đơn--}}
{{--                        </button>--}}
{{--                    @else--}}
{{--                        <button id="btn-modal-login" class="box-footer" type="button">--}}
{{--                            Tạo vận đơn--}}
{{--                        </button>--}}
{{--                    @endif--}}
{{--                </div>--}}
{{--                <input id="shipping_method_id_hidden" value="" type="hidden" name="shipping_method_id">--}}
{{--                <input name="type" type="hidden" id="type">--}}
{{--                <input type="hidden" name="kieuvanchuyen" value="noidia">--}}
{{--                <input type="hidden" name="so_km" value="0">--}}

{{--            </form>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

