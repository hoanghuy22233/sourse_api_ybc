<div class="w-100 coutdown" id="coutdown">
    <div class="container">
        <div class="row d-flex align-items-center text-center">
            <div class="col-lg2 pb-3 pt-5 pb-sm-5">
                <div class="icon"><span class="ntl-Buu-cuc"><span class="path1"></span><span
                                class="path2"></span><span class="path3"></span><span class="path4"></span><span
                                class="path5"></span><span class="path6"></span><span class="path7"></span><span
                                class="path8"></span><span class="path9"></span><span class="path10"></span><span
                                class="path11"></span><span class="path12"></span><span class="path13"></span><span
                                class="path14"></span></span></div>
                <div class="count-tk">
                    <span class="timer counter-number appear" data-to="318" data-speed="7000">318</span>
                    <br/><span class="SanFranciscoDisplay-Bold">Bưu cục</span>
                </div>
            </div>
            <div class="col-lg2 pt-3 pb-3 pt-sm-5 pb-sm-5">
                <div class="icon"><span class="ntl-Nguyen-xe"><span class="path1"></span><span class="path2"></span><span
                                class="path3"></span></span></div>
                <div class="count-tk">
                    <span class="timer counter-number appear" data-to="350" data-speed="7000">350</span>
                    <br/><span class="SanFranciscoDisplay-Bold">Xe</span>
                </div>
            </div>
            <div class="col-lg2 pt-3 pb-3 pt-sm-5 pb-sm-5">
                <div class="icon"><span class="ntl-Doi-tac"><span class="path1"></span><span
                                class="path2"></span><span class="path3"></span><span class="path4"></span></span></div>
                <div class="count-tk">
                    <span class="timer counter-number appear" data-to="36000" data-speed="7000">36000</span>
                    <br/><span class="SanFranciscoDisplay-Bold">Đối tác</span>
                </div>
            </div>
            <div class="col-lg2 pt-3 pt-sm-5 pb-5">
                <div class="icon"><span class="ntl-NV"><span class="path1"></span><span class="path2"></span><span
                                class="path3"></span><span class="path4"></span><span class="path5"></span><span
                                class="path6"></span><span class="path7"></span><span class="path8"></span><span
                                class="path9"></span><span class="path10"></span><span class="path11"></span><span
                                class="path12"></span></span></div>
                <div class="count-tk">
                    <span class="timer counter-number appear" data-to="2600" data-speed="7000">2300</span>
                    <br/><span class="SanFranciscoDisplay-Bold">Nhân viên</span>
                </div>
            </div>
            <div class="col-lg2 pt-3 pt-sm-5 pb-5">
                <div class="icon"><span class="ntl-kho"><span class="path1"></span><span class="path2"></span><span
                                class="path3"></span><span class="path4"></span><span class="path5"></span></span></div>
                <div class="count-tk">
                    <span class="timer counter-number appear" data-to="80000" data-speed="7000">50000</span><span> m<sup>2</sup></span>
                    <br/><span class="SanFranciscoDisplay-Bold">Diện tích kho bãi</span>
                </div>
            </div>
        </div>
    </div>
</div>