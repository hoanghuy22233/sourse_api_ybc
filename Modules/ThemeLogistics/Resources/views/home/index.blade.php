@extends('themelogistics::layouts.layout_homepage')
@section('main_content')
    @include('themelogistics::partials.slide')
    @include('themelogistics::home.components.service')
    @include('themelogistics::home.components.box_code')
    @include('themelogistics::home.components.about_me')
    @include('themelogistics::home.components.count_down')
    @include('themelogistics::home.components.partner')
    @include('themelogistics::home.components.hotline')
@endsection

@section('head')
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>
    <script  defer src="https://maps.googleapis.com/maps/api/js?libraries=places&language=en&key=AIzaSyDKz8Qv3Rr0uyBohNsrdXDtGD_XW-qgZ-o"  type="text/javascript"></script>
@stop

@section('script_footer')
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
    <script src="{{ asset('public/frontend/themes/themelogistic/js/select.js') }}"></script>
    <script>
        {{--$("select[name='send_city_id']").change(function(){--}}
        {{--    var id_pro = $(this).val();--}}

        {{--    $('select[name=send_city_id]').val(id_pro);--}}
        {{--    $('.selectpicker').selectpicker('refresh');--}}
        {{--    $("body").append('<div class="loading">Loading&#8230;</div>');--}}
        {{--    $.ajax({--}}
        {{--        url: '{{ route('ajax_get_district') }}',--}}
        {{--        data: { 'province_id' : id_pro},--}}
        {{--        dataType:'html',--}}
        {{--        success: function(data){--}}
        {{--            $(".loading").remove();--}}
        {{--            var current = $("select[name='send_district_id']");--}}
        {{--            current.empty();--}}
        {{--            current.html(data);--}}
        {{--            current.selectpicker('refresh');--}}

        {{--        },--}}
        {{--        error: function(){--}}
        {{--            $(".loading").remove();--}}
        {{--            alert('Có lỗi Server, vui lòng bấm f5 để load lại trang');--}}
        {{--        }--}}
        {{--    });--}}
        {{--});--}}
        {{--$("select[name='recieve_city_id']").change(function(){--}}
        {{--    var id_pro = $(this).val();--}}
        {{--    $('select[name=recieve_city_id]').val(id_pro);--}}
        {{--    $('.selectpicker').selectpicker('refresh');--}}
        {{--    $("body").append('<div class="loading">Loading&#8230;</div>');--}}
        {{--    $.ajax({--}}
        {{--        url: '{{ route('ajax_get_district') }}',--}}
        {{--        data: { 'province_id' : id_pro},--}}
        {{--        dataType:'html',--}}
        {{--        success: function(data){--}}
        {{--            $(".loading").remove();--}}
        {{--            var current = $("select[name='recieve_district_id']");--}}
        {{--            current.empty();--}}
        {{--            current.html(data);--}}
        {{--            current.selectpicker('refresh');--}}
        {{--        },--}}
        {{--        error: function(){--}}
        {{--            $(".loading").remove();--}}
        {{--            alert('Có lỗi Server, vui lòng bấm f5 để load lại trang');--}}
        {{--        }--}}
        {{--    });--}}
        {{--})--}}
        {{--$("select[name='send_district_id']").change(function(){--}}
        {{--   var value = $(this).val();--}}
        {{--   var select = $("select[name='send_district_id']").val(value);--}}
        {{--    select.selectpicker('refresh');--}}
        {{--})--}}
        {{--$("select[name='recieve_district_id']").change(function(){--}}
        {{--    var value = $(this).val();--}}
        {{--    var select = $("select[name='recieve_district_id']").val(value);--}}
        {{--    select.selectpicker('refresh');--}}
        {{--})--}}
    </script>
    <script>
        $("#tracking_top").submit(function(t) {
            t.preventDefault();
            $("body").append('<div class="loading">Loading&#8230;</div>');
            let e = $("#bill").val();
            if(e==""){
                $(".loading").remove();
                alertify.alert('Thông báo', 'Bạn chưa nhập mã !');
                return false;
            }
            $.ajax({
                type: "POST",
                url: "tracking_bill",
                dataType:'html',
                data: {
                    bill: e,
                    _token: $('meta[name="csrf-token"]').attr("content")
                },
                success: function(data) {
                    $("#order-status").html(data);
                    $("#order-status-modal").modal("show");
                    // if(data.status == 'success'){
                    //     var html = renderModalOrder(data.data);
                    //     $("#order-status").html(html);
                    //     $("#order-status-modal").modal("show");
                    //     $(".loading").remove();
                    // }else if(data.status == 'notfound'){
                    //     var html = renderModalNotFound();
                    //     $("#order-status").html(html);
                    //     $("#order-status-modal").modal("show");
                    //     $(".loading").remove();
                    // }
                    $(".loading").remove();
                },
                error:function(){
                    $(".loading").remove();
                    $("#order-status").html(data);
                    $("#order-status-modal").modal("show");
                }
            })
        })
        function renderModalNotFound(){
            var html = `<div class="modal fade" id="order-status-modal" role="dialog">
        <div class="modal-dialog order-status modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="status-top">

                        <div class="status-title">
                            <h4>Không tìm thấy dữ liệu phù hợp</h4>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>`;
            return html;
        }
        function renderModalOrder(data){
            var historyObj = data.histories;
            var htmlHistory = '';
            $.each( historyObj, function( key, value ) {
                const monthNames = ["01", "02", "03", "04", "05", "06",
                    "07", "08", "09", "10", "11", "12"];
                var d = new Date(value.created_at);
                var str_date = d.getDate() + "-" + monthNames[d.getMonth()] + "-" + d.getFullYear();
                htmlHistory += `<p><span>[` + str_date + `] </span>`+value.name+`</p>`;
            });
            @if(@\Auth::guard('admin')->check())
            var isCustomer = true;
            @else
            var isCustomer = false;
            @endif
            var htmlSendRequest = '';
            if(isCustomer){
                htmlSendRequest = `<div class="status-content">
                                            <div class="status-box status st-frame">
                                            <h3>Yêu cầu chỉnh sửa đơn hàng</h3>
                                                <textarea class="form-control" rows="5" id="text_request"></textarea>
                                                <a id="btn_request" class="btn btn-request" href="javascript:sendrequest()">Gửi</a>
                                            </div>
                                    </div>`;
            }
            var note = (data.note == null) ? "" : data.note;
            var html = `<div class="modal fade" id="order-status-modal" role="dialog">
                            <div class="modal-dialog order-status modal-lg">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="status-top">
                                            <input type="hidden" id="id_ma" value="`+data.code+`">
                                            <div class="status-title">
                                                <h4>Mã đơn hàng :</h4>
                                                <strong >`+data.code+`</strong>
                                            </div>
                                            <div class="status-title">
                                                <h4>Trạng thái đơn hàng</h4>
                                                <span class="status-detail">`+data.status+`</span>
                                            </div>
                                            <div class="name-order">
                                                <h4>Tên hàng hóa: </h4>
                                                <span class="name-detail">`+data.name+`</span>
                                            </div>
                                        </div>
                                        <div class="status-content">
                                            <div class="status-box status">
                                                <h3>Ghi chú</h3>
                                                <p>`+ note +`</p>
                                            </div>
                                            <div class="status-box status">
                                                <h3>Lịch sử vận đơn</h3>
                                                <div id="area_history">
                                                    <p>`+ htmlHistory +`</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="status-content">
                                            <div class="status-box billding-adress">
                                                <p class="box-top">Thông tin nơi nhận hàng <span> (lấy hàng)</span></p>
                                                <h3>Billding Address</h3>
                                                <div class="box-detail">
                                                    <div class="detail-name detail-m">
                                                        <p class="description">Họ và tên:</p>
                                                        <span>`+data.shipping_name+`</span>
                                                    </div>
                                                    <div class="detail-phone detail-m">
                                                        <p class="description">Số điện thoại:</p>
                                                        <span>`+data.shipping_tel+`</span>
                                                    </div>
                                                    <div class="detail-address detail-m">
                                                        <p class="description">Địa chỉ:</p>
                                                        <span>`+data.shipping_address+`</span>
                                                    </div>
                                                    <div class="detail-city detail-m">
                                                        <p class="description">Tỉnh/Thành Phố:</p>
                                                        <span>`+data.shipping_province.name+`</span>
                                                    </div>
                                                    <div class="detail-district detail-m">
                                                        <p class="description">Quận/Huyện:</p>
                                                       <span>`+data.shipping_district.name+`</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="status-box shipping">
                                                <p class="box-top">Thông tin người nhận hàng</p>
                                                <h3>Shipping</h3>
                                                <div class="box-detail">
                                                    <div class="detail-name detail-m">
                                                        <p class="description">Họ và tên:</p>
                                                        <span>`+data.billing_name+`</span>
                                                    </div>
                                                    <div class="detail-address detail-m">
                                                        <p class="description">Địa chỉ:</p>
                                                         <span>`+data.billing_address+`</span>
                                                    </div>
                                                    <div class="detail-city detail-m">
                                                        <p class="description">Tỉnh/Thành Phố:</p>
                                                        <span>`+data.billing_province.name+`</span>
                                                    </div>
                                                    <div class="detail-district detail-m">
                                                        <p class="description">Quận/Huyện:</p>
                                                        <span>`+data.billing_district.name+`</span>
                                                    </div>
                                                    <div class="detail-email detail-m">
                                                        <p class="description">Email</p>
                                                        <a href="#" class="email-t">`+data.billing_email+`</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="status-content">
                                            <div class="status-box status st-frame">
                                                <h3>Phương thức vận chuyển</h3>
                                                <div class="item-service  bg-fff icon_pop_up">`+ data.shipping_method.html_icon +`</div>
                                                <p>`+ data.shipping_method.name +`</p>
                                            </div>
                                        </div>
                                        `+htmlSendRequest+`
                                        <button class="btn-detail">
                                            <span></span>
                                            <a href="/" class="btn-click">Xem chi tiết đơn hàng</a>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>`;
            html += `<style>
                        .status-content .status-box.st-frame {
                            flex-basis: 100%;
                        }
                    </style>`;
            return html;
        }
    </script>
    <script>
        function sendrequest(){
            $("body").append('<div class="loading">Loading&#8230;</div>');
            var value = $("#text_request").val();
            var id_code = $("#code_hidden").val();
            $.ajax({
                url:"{{ route('ajaxSendRequest') }}",
                method:"POST",
                data:{"text" : value,"code":id_code, _token: $('meta[name="csrf-token"]').attr("content") },
                dataType:'html',
                success:function(data)
                {
                    $("#area_history").html(data);
                    $(".loading").remove();
                    $("#text_request").val('');
                    $('#order-status-modal').animate({scrollTop : 0},800);
                },
                error:function(data){
                    $(".loading").remove();
                }
            });
        }

        // $("input[name='weight']").change(function(){
        //     var weight = $(this).val();
        //     $("input[name='weight']").val(weight);
        // });
        // $("input[name='sokienhang']").change(function(){
        //     var sokienhang = $(this).val();
        //     $("input[name='sokienhang']").val(sokienhang);
        // });
        $("#tracuu").click(function(){


            $(".form-service p.error").remove();
            // var noiguiTinh = $("select[name='send_city_id']").val();
            // var noiguiHuyen = $("select[name='send_district_id']").val();
            // var noinhanTinh = $("select[name='recieve_city_id']").val();
            // var noinhanHuyen = $("select[name='recieve_district_id']").val();
            var noigui = $("input[name='shipping_diachi']").val();
            var noinhan = $("input[name='billing_diachi']").val();
            var dactinh = $("input[name='package_no']").val();

            // var kienhang = $("#inp_kienhang").val();
            var radType = $("input[name='type_quantity']");
            var quantity = $("input[name='quantity']").val();
            var flag = true;
            if(quantity == ""){
                var mgs = $("#inp_quantity").attr("placeholder") + " không được rỗng";
                $( '<p class="error">'+mgs+'</p>' ).insertBefore( "#inp_quantity" );
                flag = false;
            }
            if(noigui == ""){
                $("#label_noigui + p.error").remove();
                $("#label_noigui").after( '<p class="error">Bạn chưa chọn nơi gửi</p>' );
                flag = false;
            }
            if(noinhan == ""){
                $("#label_noinhan + p.error").remove();
                $("#label_noinhan").after( '<p class="error">Bạn chưa chọn nơi nhận</p>' );
                flag = false;
            }
            if(dactinh == 0){
                $("#label_dactinh + p.error").remove();
                $("#label_dactinh").after( '<p class="error">Bạn chưa chọn đặc tính hàng hóa</p>' );
                flag = false;
            }
            var flagRad = false;
            $.each(radType, function(index,input){
                var iObj = $(input);
                if(iObj.prop("checked")){
                    flagRad = true;
                }
            });
            if(!flagRad){
                $(".msg-error").html('<p class="error">Vui lòng chọn Trọng lượng hoặc Số kiện hàng</p>');
            }
            if(flag){
                if(!flagRad){
                    flag = false;
                }
            }
            if(flag){
                $("body").append('<div class="loading">Loading&#8230;</div>');
                calculateDistance().done(function(response){
                    tinhSoKm(response);
                    $("#tra_cuoc").submit();

                });
               // tinhDonGia();
                //$("#service-modal").modal('show');
            }
        })
        $('#tra_cuoc').submit(function(event) {
            event.preventDefault(); // <- avoid reloading
            $.ajax({
                url:"{{ route('ajaxGetModalTransport') }}",
                method:"POST",
                data:new FormData(this),
                dataType:'html',
                contentType: false,
                cache: false,
                processData: false,
                success:function(data)
                {
                    $(".loading").remove();
                    $("#box-modal-service").html(data);
                    $("#service-modal").modal('show');

                },
                error:function(data){
                    $(".loading").remove();
                }
            });

        });

        function tinhDonGia(){
            var soKm = $("input[name='so_km']").val();
            var quantity = $("input[name='quantity']").val();
            var tien = soKm * 100000 * quantity;
            var tienStr = number_format(tien.toFixed(0));
            $(".service-price").html(tienStr);
            $("input[name='hidden_price']").val(tien);
        }
    </script>
    <script>

        $('#frmVanDon').submit(function(event) {

            event.preventDefault(); // <- avoid reloading

            $.ajax({
                url:"{{ route('ajaxVanDon') }}",
                method:"POST",
                data:new FormData(this),
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                success:function(data)
                {
                    $(".loading").remove();
                    if(data.success == false){
                        var errors = data.errors;
                        $.each( errors, function( key, value ) {
                            var inputCurrent = $("[name='"+key+"']");
                            inputCurrent.attr("placeholder", value);
                            inputCurrent.addClass('border border-danger');
                        });
                        $('.file .file-btn').html("File");
                        //$(".box-total").hide();
                    }else{
                        $("#totalPrice").text("đ" + data.data.tongtien);
                        $("#vat").text("đ" + data.data.vat);
                        $("#totalPriceFinal").text("đ" + data.data.thanhtien);
                        $("#preview_img").html(data.data.image.uploaded_image);
                        if(data.data.coupon){
                            $("#box_coupon").html('<div class="voucher-price b-p"><p>- Mã giảm giá: '+data.data.coupon.code+'</p><span class="text-success">'+data.data.coupon.value+'</span></div>');
                        }else{
                            $("#box_coupon").html('');
                        }
                        if(data.data.cod != 0){
                            $("#box_thuho").html('<div class="voucher-price b-p"><p>- Thu hộ: </p><span>đ'+data.data.cod+'</span></div>');
                        }else{
                            $("#box_thuho").html('');
                        }
                        $('.file .file-btn').html("File");
                        //$(".box-total").show();
                    }
                    var height = $(".modal_vandon").height();
                    console.log(height);
                    $('#service-modal').animate({scrollTop : height},800);
                },
                error:function(data){
                    $(".loading").remove();
                }
            });

        });
        function tinhSoKm(response){
            var origin = response.originAddresses[0];
            var destination = response.destinationAddresses[0];
            if (response.rows[0].elements[0].status === "ZERO_RESULTS") {
            } else {
                var distance = response.rows[0].elements[0].distance;
                var duration = response.rows[0].elements[0].duration;
                var distance_in_kilo = distance.value / 1000; // the kilom
                var kmValue = distance_in_kilo.toFixed(0);
                $("input[name='so_km']").val(kmValue);

            }
        }
        $(".method-content a").click(function(){
            var flag = validateStep1();
            if(flag){
                $("body").append('<div class="loading">Loading&#8230;</div>');
                let t = this;
                calculateDistance().done(function(response){
                    tinhSoKm(response);
                    $(".shipping-method .method-content").removeClass("active");
                    $(t).parents(".method-content").addClass("active");
                    var shipping_id = $(t).data("id");
                    $("#shipping_method_id_hidden").val(shipping_id);
                    $("#type").val('no_insert');
                    $("#frmVanDon").submit();
                });

            }else{
                $(".shipping-method .method-content").removeClass("active");
                $(this).parents(".method-content").addClass("disable");
                $('#service-modal').animate({scrollTop : 200},800);
            }
            // if($(this).parents(".method-content ").hasClass("disabled")){
            //
            //     alertify.alert('Thông báo', 'Bạn Cần Nhập Đầy Đủ Thông Tin Bên Dưới');
            //     return;
            // }
            // var shipping_id = $(this).data("id");
            //
            // $("#shipping_method_id_hidden").val(shipping_id);
            // $(".shipping-method .method-content").removeClass("active");
            // $(this).parents(".method-content").addClass("active");

            // $("#type").val('no_insert');
            //$("#frmVanDon").submit();
        })
        function validateStep1(){
            var arrCheck = $(".check_require");
            var flag = true;
            $.each(arrCheck, function(index,input){
                // reset attr
                var inputCurrent = $(input);
                inputCurrent.attr("placeholder", inputCurrent.data('placeholder'));
                inputCurrent.removeClass('border border-danger');
                if($(input).val() == ""){
                    var inputCurrent = $(input);
                    inputCurrent.attr("placeholder", inputCurrent.data('placeholder') + ' không được rỗng');
                    inputCurrent.addClass('border border-danger');
                    flag = false;
                }
            });
            // xử lý phần trọng lượng và kiện hàng
            var truongluongObj = $("input[name='weight']");
            var kienhangObj = $("input[name='sokienhang']");
            truongluongObj.attr("placeholder", "kg");
            truongluongObj.removeClass('border border-danger');
            if(truongluongObj.val() == "" && kienhangObj.val() == ""){
                truongluongObj.attr("placeholder", "Trọng lượng không được rỗng");
                truongluongObj.addClass('border border-danger');
                flag = false;
            }
            // xử lý phần địa chỉ
            $(".check_select + p.error").remove();
            var arrSelectCheck = $(".check_select");
            $.each(arrSelectCheck, function(index,input){
                var select = $(input);
                if(select.val() == "0"){
                    select.after('<p style="margin-bottom: 0px;" class="error">'+select.data("placeholder")+' phải chọn giá trị hợp lệ</p>');
                    flag = false;
                }
            });
            return flag;
        }
        $("#btn_footer_vandon").click(function(){
            var flag = validateStep1();
            if(flag){
                // kiểm tra method vận chuyển
                var id_method = $("#shipping_method_id_hidden").val();
                if(id_method == ""){
                    $(".shipping-method h4 + p.error").remove();
                    $(".shipping-method h4").after('<p style="margin-left: 19px;" class="error">Vui lòng chọn phương thức vận chuyển</p>');
                    $('#service-modal').animate({scrollTop : 0},800);
                }else{
                    $("#type").val('insert');
                    $("#frmVanDon").submit();
                    window.location = "/admin/transport";
                }

            }else{
                $('#service-modal').animate({scrollTop : 200},800);
            }
        })
    </script>
    <script>
        $("#btn-modal-login").click(function(){
            $("#login-modal").modal("show");
        })
    </script>

    <script>
        function checkCoupon(){
            $("body").append('<div class="loading">Loading&#8230;</div>');
            var value = $("input[name='coupon']").val();
            if(value == ""){
                alert('Mã không được rỗng');
            }
            $.ajax({
                url:"{{ route('checkCouponAjax') }}",
                method:"POST",
                data:{"code" : value, _token: $('meta[name="csrf-token"]').attr("content") },
                dataType:'JSON',
                success:function(data)
                {
                    if(data.success){
                        $(".status-coupon").html('<span class="success text-success"> '+data.message+'</span>');
                    }else{
                        $(".status-coupon").html('<span class="error text-danger"> '+data.message+'</span>');
                        $("input[name='coupon']").val("");
                    }

                    $(".loading").remove();
                },
                error:function(data){
                    $(".loading").remove();
                }
            });
        }
    </script>
    <script>
        $(".check_require, input[name='weight'],input[name='sokienhang'], .check_select").change(function(){
            var arrCheck = $(".check_require");
            var flag = true;

            $.each(arrCheck, function(index,input){
                if($(input).val() == ""){
                    flag = false;
                }
            });
            // xử lý phần trọng lượng và kiện hàng
            var truongluongObj = $("input[name='weight']");
            var kienhangObj = $("input[name='sokienhang']");
            if(truongluongObj.val() == "" && kienhangObj.val() == ""){
                flag = false;
            }
            // xử lý phần địa chỉ
            var arrSelectCheck = $(".check_select");
            $.each(arrSelectCheck, function(index,input){
                var select = $(input);
                if(select.val() == "0"){
                    flag = false;
                }
            });
            if(flag){
                $(".modal_vandon input.border").removeClass("border-danger");
                $(".modal_vandon input.border").addClass("border-success");
                $(".shipping-method h4 + p.error").remove();
                $(".shipping-method h4").after('<p style="margin-left: 19px;" class="error">Vui lòng chọn phương thức vận chuyển</p>');
                $('#service-modal').animate({scrollTop : 0},800);
                $(".method-content").removeClass("disabled");
                $(".service-price").show();
            }else{
                $(".shipping-method .method-content").removeClass("active");
                $(".shipping-method .method-content").addClass("disabled");
            }
            return true;
            // if(flag){
            //     var weight = $("input[name='weight']").val();
            //     var sokienhang = $("input[name='sokienhang']").val();
            //     if((weight == "" || weight == 0) && (sokienhang == "" || sokienhang == 0)){
            //         flag = false;
            //     }
            //     if(flag){
            //         $(".method-content").removeClass("disabled");
            //         $(".service-price").show();
            //     }else{
            //         $(".method-content").removeClass("disabled");
            //         $(".method-content").addClass("disabled");
            //         $(".service-price").hide();
            //     }
            // }else{
            //     $(".method-content").removeClass("disabled");
            //     $(".method-content").addClass("disabled");
            //     $(".service-price").hide();
            // }
        });
    </script>
    <script>
        $("#inp_weight").change(function(){
            var value = $(this).val();
            if(value != null){
                $("#tracuu").removeClass("disable_tra");
            }
        });
        $("input[name='quantity']").change(function(){
            var value = $(this).val();
            $("input[name='quantity']").val(value);
            tinhDonGia();
        })
        $("input[name='shipping_diachi']").change(function(){
            calculateDistance().done(function(response){
                tinhSoKm(response);
                $("input[name='shipping_diachi']").val($(this).val());
                tinhDonGia();
            });

        });
        $("input[name='billing_diachi']").change(function(){
            calculateDistance().done(function(response){
                tinhSoKm(response);
                $("input[name='billing_diachi']").val($(this).val());
                tinhDonGia();
            });

        });
    </script>
    <script>
        $(".service-box").click(function(){
            var value = $(this).data('value');
            var id = $(this).data('id');
            $("input[name='transport_type']").val(id);
            getTransportProductTypeAjaxByIdType(id);
            $(".service-box").removeClass("active");
            $(this).addClass("active");
            var id_tab = "#"+value;
            $(".tab_box ").removeClass("active");
            $(id_tab).addClass("active");
            resetFormStep1();
        });
        getTransportProductTypeAjaxByIdType(1, "first");
        function getTransportProductTypeAjaxByIdType(id, type=""){
            if(type != "first"){
                $("body").append('<div class="loading">Loading&#8230;</div>');
            }
            $.ajax({
                url:"{{ route('ajaxGetTransportProductType') }}",
                method:"POST",
                data:{"type_id" : id, _token: $('meta[name="csrf-token"]').attr("content") },
                dataType:'html',
                success:function(data)
                {
                    $("#dactinh_home").html(data);
                    setActionDacTinhHome();
                    $(".loading").remove();
                },
                error:function(data){
                    $(".loading").remove();
                }
            });
        }
    </script>
    <script>
        function resetFormStep1(){
            $("input[name='package_no']").val(0);
            //$("input[name='package_no']").val(0);
        }
        function setActionDacTinhHome(){
            $("#dactinh_home li").click(function(){
                $("#dactinh_home li").removeClass("active");
                $(this).toggleClass('active');
                var value = $(this).data('value');
                $("input[name='package_no']").val(value);
                $('select[name=mahang]').val(value);
                $('.selectpicker').selectpicker('refresh');
            });
        }
    </script>
    <script>

        $("input[name='picture']").change(function(){
            $("#preview_img").remove();
            $('.file .file-btn').html("File");
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    var src = e.target.result;
                    var img = '<img style="width: 100px;margin-left: 10px;" src="'+src+'">';
                    $('.file .file-btn ').append(img);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
        $("input[name='type_quantity']").change(function(){
            var v = $(this).val();
            var o = $("input[name='quantity']");
            $("input[name=type_quantity][value=" + v + "]").attr('checked', 'checked');
            o.show();
            if(v == "trongluong"){
                o.attr("placeholder", "Số Kg");
            }else{
                o.attr("placeholder", "Số kiện");
            }
        });

    </script>
    <script>
        function number_format (number, decimals, dec_point, thousands_sep) {
            // Strip all characters but numerical ones.
            number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function (n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        }
    </script>
    <!-- api -->

    <script>

        $(function() {
            // add input listeners
            google.maps.event.addDomListener(window, 'load', function () {
                var from_places = new google.maps.places.Autocomplete(document.getElementById('from_places'));
                var to_places = new google.maps.places.Autocomplete(document.getElementById('to_places'));

                google.maps.event.addListener(from_places, 'place_changed', function () {
                    var from_place = from_places.getPlace();
                    var from_address = from_place.formatted_address;
                    $("input[name='hidden_noigui']").val(from_address);
                    $("input[name='shipping_diachi']").val(from_address);
                    calculateDistance().done(function(response){
                        tinhSoKm(response);
                        tinhDonGia();
                    });
                });

                google.maps.event.addListener(to_places, 'place_changed', function () {
                    var to_place = to_places.getPlace();
                    var to_address = to_place.formatted_address;
                    $("input[name='hidden_noinhan']").val(to_address);
                    $("input[name='billing_diachi']").val(to_address);
                    calculateDistance().done(function(response){
                        tinhSoKm(response);
                        tinhDonGia();
                    });
                });

                var from_places_home = new google.maps.places.Autocomplete(document.getElementById('from_places_home'));
                var to_places_home = new google.maps.places.Autocomplete(document.getElementById('to_places_home'));

                google.maps.event.addListener(from_places_home, 'place_changed', function () {
                    var from_place = from_places_home.getPlace();
                    var from_address = from_place.formatted_address;

                    $("input[name='hidden_noigui']").val(from_address);
                    $("input[name='shipping_diachi']").val(from_address);
                    calculateDistance().done(function(response){
                        tinhSoKm(response);
                        tinhDonGia();
                    });
                });

                google.maps.event.addListener(to_places_home, 'place_changed', function () {
                    var to_place = to_places_home.getPlace();
                    var to_address = to_place.formatted_address;
                    $("input[name='hidden_noinhan']").val(to_address);
                    $("input[name='billing_diachi']").val(to_address);
                    calculateDistance().done(function(response){
                        tinhSoKm(response);
                        tinhDonGia();
                    });
                });
            });

            // print results on submit the form
            // $('#distance_form').submit(function(e){
            //     e.preventDefault();
            //     calculateDistance();
            // });

        });
        function calculateDistance() {
            var origin = $("input[name='hidden_noigui']").val();
            var destination = $("input[name='hidden_noinhan']").val();
            var dfd = $.Deferred();
            if(origin != "" && destination != ""){
                var service = new google.maps.DistanceMatrixService();

                service.getDistanceMatrix(
                    {
                        origins: [origin],
                        destinations: [destination],
                        travelMode: google.maps.TravelMode.DRIVING,
                        unitSystem: google.maps.UnitSystem.IMPERIAL, // miles and feet.
                        // unitSystem: google.maps.UnitSystem.metric, // kilometers and meters.
                        avoidHighways: false,
                        avoidTolls: false
                    }, function(response, status) {
                        if (status == google.maps.DistanceMatrixStatus.OK)
                            dfd.resolve(response);
                        else
                            dfd.reject(status);
                    });
            }
            return dfd.promise();

        }
    </script>
    <!-- end api -->
@stop
