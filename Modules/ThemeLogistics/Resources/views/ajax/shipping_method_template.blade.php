@if(count($shippingMethodObj) > 0)
    @foreach($shippingMethodObj as $k => $item)
        <div class="method-content">

            <a data-id="{{ $item->id }}" href="javascript:void(0)"
               class="item-service  bg-fff translate text-center position-relative">
                <p class="ntl-CPN1 mb-3 mb-sm-0">
                    <img src="{{  CommonHelper::getUrlImageThumb($item->image)  }}" alt="">
                </p>
                <p class="service-text">
                    {{ $item->name }}
                </p>
            </a>
            <input type="hidden" name="hidden_price">
            <span class="service-price">{{ number_format($item->final_price)  }} VNĐ</span>
        </div>
    @endforeach
@else
    <h3 class="box-title">Không có phương thức vận chuyển phù hợp</h3>
@endif