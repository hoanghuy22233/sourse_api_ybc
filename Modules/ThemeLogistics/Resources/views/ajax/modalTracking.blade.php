@php

@endphp
<div class="modal fade" id="order-status-modal" role="dialog">
    <div class="modal-dialog order-status modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="status-top">
                    <input type="hidden" id="id_ma" value="`+data.code+`">
                    <div class="status-title">
                        <h4>Mã đơn hàng :</h4>
                        <strong >{{ $data['code']  }}</strong>
                        <input type="hidden" id="code_hidden" name="code" value="{{ $data['code'] }}">
                    </div>
                    <div class="status-title">
                        <h4>Trạng thái đơn hàng</h4>
                        <span class="status-detail">{{ $data['status']  }}</span>
                    </div>
                    <div class="name-order">
                        <h4>Tên hàng hóa: </h4>
                        <span class="name-detail">{{ $data['name']  }}</span>
                    </div>
                </div>
                <div class="status-content">
                    <div class="status-box status">
                        <h3>Ghi chú</h3>
                        <p>{{ $data['note']  }}</p>
                    </div>
                    <div class="status-box status">
                        <h3>Lịch sử vận đơn</h3>
                        <div id="area_history">
                            <p>
                                @foreach($data['histories'] as $k => $v)
                                    @php
                                        $date=date_create($v['created_at']);
                                    @endphp
                                    <p><span>[{{ date_format($date, 'h:i:s d-m-Y') }}] </span> {{ $v['name'] }}</p>
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>

                <div class="status-content">
                    <div class="status-box billding-adress">
{{--                        <p class="box-top">Thông tin nơi nhận hàng <span> (lấy hàng)</span></p>--}}
                        <h3>Thông tin người gửi</h3>
                        <div class="box-detail">
                            <div class="detail-name detail-m">
                                <p class="description">Họ và tên:</p>
                                <span>{{ $data['shipping_name']  }}</span>
                            </div>
                            <div class="detail-phone detail-m">
                                <p class="description">Số điện thoại:</p>
                                <span>{{ $data['shipping_tel']  }}</span>
                            </div>
                            <div class="detail-address detail-m">
                                <p class="description">Địa chỉ:</p>
                                <span>{{ $data['shipping_address']  }}</span>
                            </div>
                            <div class="detail-email detail-m">
                                <p class="description">Email</p>
                                <a href="#" class="email-t">{{ $data['shipping_email']  }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="status-box shipping">
{{--                        <p class="box-top">Thông tin người nhận hàng</p>--}}
                        <h3>Thông tin người nhận</h3>
                        <div class="box-detail">
                            <div class="detail-name detail-m">
                                <p class="description">Họ và tên:</p>
                                <span>{{ $data['billing_name']  }}</span>
                            </div>
                            <div class="detail-phone detail-m">
                                <p class="description">Số điện thoại:</p>
                                <span>{{ $data['billing_tel']  }}</span>
                            </div>
                            <div class="detail-address detail-m">
                                <p class="description">Địa chỉ:</p>
                                <span>{{ $data['billing_address']  }}</span>
                            </div>

                            <div class="detail-email detail-m">
                                <p class="description">Email</p>
                                <a href="#" class="email-t">{{ $data['billing_email']  }}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="status-content">
                    <div class="status-box status st-frame">
                        <h3>Phương thức vận chuyển</h3>
                        <div class="item-service  bg-fff icon_pop_up"><img src="{{ CommonHelper::getUrlImageThumb($data['shipping_method']['image']) }}" alt=""></div>
                        <p>{{ $data['shipping_method']['name']  }}</p>
                    </div>
                    <div class="status-box status st-frame">
                        <h3>Yêu cầu chỉnh sửa đơn hàng</h3>
                        <textarea class="form-control" rows="5" id="text_request"></textarea>
                        <a id="btn_request" class="btn btn-request" href="javascript:sendrequest()">Gửi</a>
                    </div>
                </div>

                <button class="btn-detail">
                    <span></span>
                    <a href="/admin/transport" class="btn-click">Xem chi tiết đơn hàng</a>
                </button>
            </div>
        </div>
    </div>
</div>