@php
    /**
        Truyền vào
 *          $transport_type : id kiểu vận chuyển ( nội địa vn, đài loan....)
 *          $package_no: id đặc tính hàng hóa, để checked vào
 *          $type_quantity: "0:trongluong" hoặc "1:kienhang"
 *          $quantity : số lượng
 *
 *          $shipping_diachi : địa chỉ người GỬI chính xác
 *          $hidden_noigui:  địa chỉ người GỬI theo google map
 *          $billing_diachi : địa chỉ người NHẬN chính xác
 *          $hidden_noinhan:  địa chỉ người NHẬN theo google map
 *          $so_km : số km quảng đường đi
 *
    **/

    // lấy shiping method tương ứng với Transport Type, Transport Product Type, Số Km, cân nặng hoặc số kiện phù hợp

    $shippingMethodObj = CommonHelper::getShippingMethod($transport_type, $package_no, $so_km, $type_quantity,$quantity );
    /*function createSelect($data, $keyselect = null){
        $html = '';
        foreach($data as $k => $item){
            $selected = $keyselect == $k ? "selected" : "";
            $html .= '<option '.$selected.' value="'.$k.'">'.$item.'</option>';
        }

        return $html;
    }*/

    $dataDacTinh = \Modules\ThemeLogistics\Models\TransportProductType::where('transport_type_id', $transport_type)->get();
@endphp

<div class="modal fade" id="service-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="frmVanDon" action="#" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body modal_vandon">
                    <div class="shipping-method box-sd">
                        <h4 class="box-title">1. Tab phương thức vận chuyển</h4>
                        <div class="row m-0" id="box_shipping_method">
                            @include('themelogistics::ajax.shipping_method_template', [ 'shippingMethodObj' => $shippingMethodObj ])
{{--                            <div class="method-content disabled">--}}

{{--                                <a data-id="4" href="javascript:void(0)"--}}
{{--                                   class="item-service  bg-fff translate text-center position-relative p-3">--}}
{{--                                    <p class="ntl-CPN1 mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span--}}
{{--                                                class="path3"></span><span class="path4"></span><span class="path5"></span><span--}}
{{--                                                class="path6"></span><span class="path7"></span><span class="path8"></span><span--}}
{{--                                                class="path9"></span><span class="path10"></span></p>--}}
{{--                                    <p class="service-text">--}}
{{--                                        Chuyển phát nhanh--}}
{{--                                    </p>--}}
{{--                                </a>--}}
{{--                                <input type="hidden" name="hidden_price">--}}
{{--                                <span class="service-price">100.000VNĐ</span>--}}
{{--                            </div>--}}
{{--                            <div class="method-content disabled">--}}

{{--                                <a data-id="5" href="javascript:void(0)"--}}
{{--                                   class="item-service   bg-fff translate text-center position-relative p-3">--}}
{{--                                    <p class="ntl-CP-Ket-Hop mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span--}}
{{--                                                class="path3"></span><span class="path4"></span><span class="path5"></span><span--}}
{{--                                                class="path6"></span><span class="path7"></span><span class="path8"></span><span--}}
{{--                                                class="path9"></span><span class="path10"></span><span class="path11"></span><span--}}
{{--                                                class="path12"></span></p>--}}
{{--                                    <p class="service-text">--}}
{{--                                        Chuyển phát tiết kiệm--}}
{{--                                    </p>--}}
{{--                                </a>--}}
{{--                                <input type="hidden" name="hidden_price">--}}
{{--                                <span class="service-price">100.000VNĐ</span>--}}
{{--                            </div>--}}
{{--                            <div class="method-content disabled">--}}

{{--                                <a data-id="6" href="javascript:void(0)"--}}
{{--                                   class="item-service   bg-fff translate text-center position-relative p-3">--}}
{{--                                    <p class="ntl-CP-duong-bo mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span--}}
{{--                                                class="path3"></span><span class="path4"></span><span class="path5"></span><span--}}
{{--                                                class="path6"></span><span class="path7"></span><span class="path8"></span><span--}}
{{--                                                class="path9"></span><span class="path10"></span><span class="path11"></span><span--}}
{{--                                                class="path12"></span><span class="path13"></span><span class="path14"></span><span--}}
{{--                                                class="path15"></span><span class="path16"></span><span class="path17"></span><span--}}
{{--                                                class="path18"></span><span class="path19"></span><span class="path20"></span><span--}}
{{--                                                class="path21"></span><span class="path22"></span><span class="path23"></span><span--}}
{{--                                                class="path24"></span><span class="path25"></span><span class="path26"></span><span--}}
{{--                                                class="path27"></span><span class="path28"></span><span class="path29"></span></p>--}}
{{--                                    <p class="service-text">--}}
{{--                                        Chuyển phát đường bộ--}}
{{--                                    </p>--}}
{{--                                </a>--}}
{{--                                <input type="hidden" name="hidden_price">--}}
{{--                                <span class="service-price">100.000VNĐ</span>--}}
{{--                            </div>--}}
{{--                            <div class="method-content disabled">--}}

{{--                                <a data-id="7" href="javascript:void(0)"--}}
{{--                                   class="item-service   bg-fff translate text-center position-relative p-3">--}}
{{--                                    <p class="ntl-Thu-ho-COD mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span--}}
{{--                                                class="path3"></span><span class="path4"></span><span class="path5"></span></p>--}}
{{--                                    <p class="service-text">--}}
{{--                                        Chuyển phát thu hộ (COD)--}}
{{--                                    </p>--}}
{{--                                </a>--}}
{{--                                <input type="hidden" name="hidden_price">--}}
{{--                                <span class="service-price">100.000VNĐ</span>--}}
{{--                            </div>--}}
{{--                            <div class="method-content disabled">--}}

{{--                                <a data-id="8" href="javascript:void(0)"--}}
{{--                                   class="item-service   bg-fff translate text-center position-relative p-3">--}}
{{--                                    <p class="ntl-Nguyen-xe mb-3 mb-sm-0"><span class="path1"></span><span class="path2"></span><span--}}
{{--                                                class="path3"></span></p><p class="service-text">--}}
{{--                                        Thuê xe nguyên chuyến--}}
{{--                                    </p>--}}
{{--                                </a>--}}
{{--                                <input type="hidden" name="hidden_price">--}}
{{--                                <span class="service-price">100.000VNĐ</span>--}}
{{--                            </div>--}}
                        </div>
                    </div>

                    <div class="box-info box-sd">
                        <h4 class="box-title">2. Nhập thông tin hàng hóa </h4>

                        <div class="box-input">
                    <span>
                        <label for="">Tên hàng</label>

                        <input data-placeholder="Tên" placeholder="Tên" name="tenhang" type="text" class="check_require input__control">
                    </span>
                            <span>
                        <div class="form-group dactinh_modal">
                                <label id="label_dactinh" class="text-dark font-size-16">Đặc tính hàng hóa</label>
                                <ul id="dactinh" class="row dactinh">
                                    @foreach($dataDacTinh as $k => $item)
                                    <li  class="@if($package_no == $item->id) active @endif border bg-fff translate rounded pt-1 pb-1 mb-1" data-value="{{ $item->id }}">
                                         <div class="w-100 text-dark text-center ">
                                               <span class="icon ntl-Letter d-block">
                                                       <img src="{{ CommonHelper::getUrlImageThumb($item->image)  }}">
                                                </span>
                                               <div class="mt-2">{{ $item->name  }}</div>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                    </span>

                            <div class="">
                                <div class="form-check-inline mb-3">
                                    <label class="form-check-label">
                                        <input @if($type_quantity == "0") checked @endif value="0" type="radio" class="check-change-submit form-check-input" name="type_quantity"><span class="text_type">Trọng lượng</span>
                                    </label>
                                </div>
                                <div class="form-check-inline mb-3">
                                    <label class="form-check-label">
                                        <input @if($type_quantity == "1") checked @endif value="1" type="radio" class="check-change-submit form-check-input" name="type_quantity"><span class="text_type">Số kiện</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-check-label">
                                    <span class="text_type">Số lượng</span> <input min="1" value="{{ $quantity  }}" style="width:initial;margin-left:4px" name="quantity" type="number" class="check-change-submit input__control check_require"><span class="unit">( @if($type_quantity == 0) kg @else Số kiện @endif )</span>
                                </label>
                            </div>
                            <span>
                        <label for="">Hình ảnh</label>
                        <div class="file">
                            <input name="picture" type="file" class="file-choose">
                            <div class="file-btn btn-m">
                                File
                                <div id="preview_img"></div>
                            </div>
                        </div>

                    </span>
                        </div>

                    </div>
                    <div class="box-address box-sd">
                        <div action="" class="shipping-address address-form mg-r">
                            <h4 class="box-title" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">2. Địa chỉ người gửi </h4>

                            <span class="form__control">
                        <div class="control-top">
                            <input  data-placeholder="Họ và tên" type="text" name="shipping_hoten" placeholder="Họ và tên" class="check_require input__control">
                            <input data-placeholder="SĐT" type="text" name="shipping_sdt" placeholder="SĐT" class="check_require input__control">
                        </div>
                        <input value="{{ $shipping_diachi }}" id="from_places"  data-placeholder="Địa chỉ" type="text" name="shipping_diachi" placeholder="Địa chỉ: " class="check_require input__control">
                            <input value="{{ $hidden_noigui }}" id="origin" name="hidden_noigui" required="" type="hidden" />

                        <input  data-placeholder="Email" type="email" name="shipping_email" placeholder="Email " class="input__control">
                    </span>
                        </div>

                        <div action="" class="billing-address address-form">
                            <h4 class="box-title">2. Địa chỉ người nhận</h4>
                            <span class="form__control">

                        <div class="control-top">
                            <input data-placeholder="Họ và tên" type="text" name="billing_hoten" placeholder="Họ và tên" class="check_require input__control">
                            <input data-placeholder="SĐT" type="text" name="billing_sdt" placeholder="SĐT" class="check_require input__control">
                        </div>
                        <input value="{{ $billing_diachi }}" id="to_places" data-placeholder="Địa chỉ" type="text" name="billing_diachi" placeholder="Địa chỉ: " class="input__control check_require">
                        <input value="{{ $hidden_noinhan }}" id="destination" name="hidden_noinhan" required="" type="hidden" />

                        <input data-placeholder="Email" type="email" name="billing_email" placeholder="Email " class="input__control">
                    </span>
                        </div>
                    </div>
                    @if (@\Auth::guard('admin')->check())
                        <div class="box-voucher box-sd" id="accordion3">
                            <h4 class="box-title" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">3. Mã giảm giá <span><i class="fas fa-chevron-down"></i></span></h4>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingOne" data-parent="#accordion3">
                                <div class="box-input">
                                    <input name="coupon" type="text" placeholder="Nhập mã giảm giá" class="input__control">

                                        <a href="javascript:checkCouponModal()" class="b-btn btncoupon">Kiểm tra</a>

                                </div>
                                <p style="margin-bottom:0px" class="status-coupon"></p>
                            </div>
                        </div>
                        <div class="box-comment box-sd" id="accordion4">
                            <h4 class="box-title" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">4. Ghi chú cho đơn hàng <span><i class="fas fa-chevron-down"></i></span></h4>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingOne" data-parent="#accordion4">
                                <textarea name="ghichu" rows="3" class="input__control"></textarea>
                            </div>
                        </div>
                        <div class="box-paypal box-sd" id="accordion5">
                            <h4 class="box-title" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">5. Tiền thu hộ COD<span><i class="fas fa-chevron-down"></i></span></h4>
                            <div id="collapseFive" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion5">
                                <input id="cod" type="number" data-placeholder="Tiền thu hộ"  name="cod" type="text" class="input__control">
                            </div>
                        </div>
                        <div class="box-paypal box-sd" id="accordion6">
                            <h4 class="box-title" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">6. Phương thức thanh toán <span><i class="fas fa-chevron-down"></i></span></h4>
                            <div id="collapseSix" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion6">
                                <div>
                                       <div class="error_pay"></div>
                                        <span>
                                            <input value="1" type="radio" name= "paypal" id="paypal-online">
                                            <label for="paypal-online">Người Gửi Trả</label>
                                        </span>
                                                    <span>
                                            <input value="2" type="radio" name= "paypal" id="paypal-money">
                                            <label for="paypal-money">Người Nhận Trả</label>
                                        </span>
                                </div>
                                <div class="paypal-img">
                                    <img src="./images/img/paypal.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="box-total box-sd">
                        <div class="box-title">7. Thành tiền ( chưa VAT )</div>
                        <div class="box_total_price">
                            <div class="total-price b-p">
                                <p>- Tổng tiền</p>
                                <span id="totalPrice">0</span>
                            </div>
                            <div id="box_coupon"></div>
                            <div class="vat-price b-p">
                                <p>- 10% VAT</p>
                                <span id="vat">0</span>
                            </div>
                            <div id="box_thuho"></div>
                            <div class="done-price b-p">
                                <p>Thành tiền</p>
                                <span id="totalPriceFinal">0</span>
                            </div>
                        </div>

                    </div>
                    @if (@\Auth::guard('admin')->check())
                        <button class="box-footer" type="button" id="btn_footer_vandon">
                            Tạo Vận đơn
                        </button>
                    @else
                        <button id="btn-modal-login" class="box-footer" type="button">
                            Tạo vận đơn
                        </button>
                    @endif
                </div>
                <input id="shipping_method_id_hidden" value="" type="hidden" name="shipping_method_id">
                <input name="type" type="hidden" id="type">
                <input type="hidden" name="kieuvanchuyen" value="{{ $transport_type  }}">
                <input id="sokm" type="hidden" name="so_km" value="{{ $so_km }}">
                <input class="check-change-submit" type="hidden" name="package_no" id="package_no" value="{{ $package_no }}">
                <input type="hidden" name="shipping_method_id">
                <input type="hidden" name="type_submit" id="type_submit">
            </form>
        </div>
    </div>
</div>
<script>
    $('#frmVanDon').submit(function(event) {
        $("body").append('<div class="loading">Loading&#8230;</div>');
        event.preventDefault(); // <- avoid reloading
        var linkSubmit = $(this).attr("action");
        var typeSubmit = $("input[name='type_submit']").val();

        $.ajax({
            url:linkSubmit,
            method:"POST",
            data:new FormData(this),
            dataType:'html',
            contentType: false,
            cache: false,
            processData: false,
            success:function(data)
            {
                if(typeSubmit == "get_method"){
                    $("#box_shipping_method").html(data);
                    actionShippingMethod();
                    $('#service-modal').animate({scrollTop : 0},800);
                    $("input[name='shipping_method_id']").val('');
                }else if(typeSubmit == "no_insert"){
                    $(".box_total_price").html(data);
                    var height = $(".modal_vandon").height();
                    $('#service-modal').animate({scrollTop : height},800);
                }else if(typeSubmit == "insert" ){
                    window.location = "/admin/transport";
                }
                $(".loading").remove();

            },
            error:function(data){
                $(".loading").remove();
            }
        });

    });
    $('.selectpicker').selectpicker('refresh');
    $(".check-change-submit").change(function(){
        calculateDistance().done(function(response){
            var form = $("#frmVanDon");
            $("input[name='type_submit']").val('get_method');
            form.attr("action", "/ajax/get-shipping-method");
            tinhSoKm(response);
            form.submit();
        });

    });
    $("input[name='type_quantity']").change(function(){
        var v = $(this).val();
        var t = "";
        if(v == 0){
            t = "( kg )";
        }else{
            t = "( Số kiện)";
        }
        $(".unit").text(t);
    });
    $("#cod, input[name='coupon']").change(function(){
        // var v_method = $("input[name='shipping_method_id']").val();
        // var flag = true;
        if(_validateStep1()){
            $("#type_submit").val('no_insert');
            $("#frmVanDon").attr("action", '/ajax/get-price');
            $("#frmVanDon").submit();
        }else{
            $('#service-modal').animate({scrollTop : 0},800);
        }


    })
    function actionShippingMethod(){
        $(".method-content a").click(function(){
            $(".shipping-method .method-content").removeClass("active");
            $(this).parents(".method-content").addClass("active");
            $("input[name='shipping_method_id']").val($(this).data('id'));
            var flag = _validateStep1();
            if(flag){
                $("#type_submit").val('no_insert');
                $("#frmVanDon").attr("action", '/ajax/get-price');
                $("#frmVanDon").submit();
                // $("body").append('<div class="loading">Loading&#8230;</div>');
                // let t = this;
                // calculateDistance().done(function(response){
                //     tinhSoKm(response);
                //     $(".shipping-method .method-content").removeClass("active");
                //     $(t).parents(".method-content").addClass("active");
                //     var shipping_id = $(t).data("id");
                //     $("#shipping_method_id_hidden").val(shipping_id);
                //     $("#type").val('no_insert');
                //     $("#frmVanDon").submit();
                // });

            }else{
                $(".shipping-method .method-content").removeClass("active");
                $(this).parents(".method-content").addClass("disable");
                $('#service-modal').animate({scrollTop : 200},800);
            }
        })
    }
    actionShippingMethod();
    var from_places = new google.maps.places.Autocomplete(document.getElementById('from_places'));
    var to_places = new google.maps.places.Autocomplete(document.getElementById('to_places'));

    google.maps.event.addListener(from_places, 'place_changed', function () {
        var from_place = from_places.getPlace();
        var from_address = from_place.formatted_address;

        $("#from_places").val(from_address);
        $("#origin").val(from_address);
        calculateDistance().done(function(response){
            var form = $("#frmVanDon");
            $("input[name='type_submit']").val('get_method');
            form.attr("action", "/ajax/get-shipping-method");
            tinhSoKm(response);
            form.submit();
        });

    });
    google.maps.event.addListener(to_places, 'place_changed', function () {
        var to_place = to_places.getPlace();
        var to_address = to_place.formatted_address;
        $("#to_places").val(to_address);
        $("#destination").val(to_address);
        calculateDistance().done(function(response){
            var form = $("#frmVanDon");
            $("input[name='type_submit']").val('get_method');
            form.attr("action", "/ajax/get-shipping-method");
            tinhSoKm(response);
            form.submit();
        });
    });
    function _validateStep1(){




        var arrCheck = $(".check_require");
        var flag = true;
        var v_method = $("input[name='shipping_method_id']").val();
        $(".shipping-method h4 + p.error").remove();
        if(v_method == ""){
            $(".shipping-method h4").after('<p style="margin-left: 19px;" class="error">Vui lòng chọn phương thức vận chuyển</p>');
            // $('#service-modal').animate({scrollTop : 0},800);
            flag = false;
        }
        // $.each(arrCheck, function(index,input){
        //     // reset attr
        //     var inputCurrent = $(input);
        //     inputCurrent.attr("placeholder", inputCurrent.data('placeholder'));
        //     inputCurrent.removeClass('border border-danger');
        //     if($(input).val() == ""){
        //         var inputCurrent = $(input);
        //         inputCurrent.attr("placeholder", inputCurrent.data('placeholder') + ' không được rỗng');
        //         inputCurrent.addClass('border border-danger');
        //         flag = false;
        //     }
        // });
        // xử lý phần trọng lượng và kiện hàng
        var truongluongObj = $("input[name='weight']");
        var kienhangObj = $("input[name='sokienhang']");
        truongluongObj.attr("placeholder", "kg");
        truongluongObj.removeClass('border border-danger');
        if(truongluongObj.val() == "" && kienhangObj.val() == ""){
            truongluongObj.attr("placeholder", "Trọng lượng không được rỗng");
            truongluongObj.addClass('border border-danger');
            flag = false;
        }


        // xử lý phần địa chỉ
        $(".check_select + p.error").remove();
        var arrSelectCheck = $(".check_select");
        $.each(arrSelectCheck, function(index,input){
            var select = $(input);
            if(select.val() == "0"){
                select.after('<p style="margin-bottom: 0px;" class="error">'+select.data("placeholder")+' phải chọn giá trị hợp lệ</p>');
                flag = false;
            }
        });
        return flag;
    }
    function calculateDistance() {
        var origin = $("#origin").val();
        var destination = $("#destination").val();
        var dfd = $.Deferred();
        if(origin != "" && destination != ""){
            var service = new google.maps.DistanceMatrixService();

            service.getDistanceMatrix(
                {
                    origins: [origin],
                    destinations: [destination],
                    travelMode: google.maps.TravelMode.DRIVING,
                    unitSystem: google.maps.UnitSystem.IMPERIAL, // miles and feet.
                    // unitSystem: google.maps.UnitSystem.metric, // kilometers and meters.
                    avoidHighways: false,
                    avoidTolls: false
                }, function(response, status) {
                    if (status == google.maps.DistanceMatrixStatus.OK)
                        dfd.resolve(response);
                    else
                        dfd.reject(status);
                });
        }
        return dfd.promise();

    }
    function tinhSoKm(response){
        var origin = response.originAddresses[0];
        var destination = response.destinationAddresses[0];
        if (response.rows[0].elements[0].status === "ZERO_RESULTS") {
        } else {
            var distance = response.rows[0].elements[0].distance;
            var duration = response.rows[0].elements[0].duration;
            var distance_in_kilo = distance.value / 1000; // the kilom
            var kmValue = distance_in_kilo.toFixed(0);
            $("input[name='so_km']").val(kmValue);

        }
    }
    function setActionDacTinh(){
        $(".dactinh_modal ul.dactinh li").click(function(){
            $(".dactinh_modal ul.dactinh li").removeClass("active");
            $(this).toggleClass('active');
            var value = $(this).data('value');
            $("#package_no").val(value);

            var form = $("#frmVanDon");
            $("input[name='type_submit']").val('get_method');
            form.attr("action", "/ajax/get-shipping-method");
            form.submit();
        });
    }
    $("#btn_footer_vandon").click(function(){
        var flag = validateStepModal1();
        if(flag){
            // $("#type").val('insert');
            // $("#frmVanDon").submit();
            // window.location = "/admin/transport";
            var form = $("#frmVanDon");
            $("input[name='type_submit']").val('insert');
            form.attr("action", "/ajax/van-don");
            form.submit();


        }else{
            $('#service-modal').animate({scrollTop : 0},800);
        }
    })
    function validateStepModal1(){

        var arrCheck = $(".check_require");
        var flag = true;

        var v_method = $("input[name='shipping_method_id']").val();
        $(".shipping-method h4 + p.error").remove();
        if(v_method == ""){
            $(".shipping-method h4").after('<p style="margin-left: 19px;" class="error">Vui lòng chọn phương thức vận chuyển</p>');
            flag = false;
        }

        $.each(arrCheck, function(index,input){
            // reset attr
            var inputCurrent = $(input);
            inputCurrent.attr("placeholder", inputCurrent.data('placeholder'));
            inputCurrent.removeClass('border border-danger');
            if($(input).val() == ""){
                var inputCurrent = $(input);
                inputCurrent.attr("placeholder", inputCurrent.data('placeholder') + ' không được rỗng');
                inputCurrent.addClass('border border-danger');
                flag = false;
            }
        });
        // xử lý phần trọng lượng và kiện hàng
        var truongluongObj = $("input[name='weight']");
        var kienhangObj = $("input[name='sokienhang']");
        truongluongObj.attr("placeholder", "kg");
        truongluongObj.removeClass('border border-danger');
        if(truongluongObj.val() == "" && kienhangObj.val() == ""){
            truongluongObj.attr("placeholder", "Trọng lượng không được rỗng");
            truongluongObj.addClass('border border-danger');
            flag = false;
        }
        // xử lý phần địa chỉ
        $(".check_select + p.error").remove();
        var arrSelectCheck = $(".check_select");
        $.each(arrSelectCheck, function(index,input){
            var select = $(input);
            if(select.val() == "0"){
                select.after('<p style="margin-bottom: 0px;" class="error">'+select.data("placeholder")+' phải chọn giá trị hợp lệ</p>');
                flag = false;
            }
        });

        var checkPaypay = $("input[name='paypal']").is(':checked');
        $(".error_pay").html('');
        if(!checkPaypay){

            $(".error_pay").html('<p class="error">Bạn phải chọn phương thức thanh toán</p>');
        }

        return flag;
    }
    function checkCouponModal(){
        $("body").append('<div class="loading">Loading&#8230;</div>');
        var value = $("input[name='coupon']").val();
        if(value == ""){
            alert('Mã không được rỗng');
        }

        $.ajax({
            url:"{{ route('checkCouponAjax') }}",
            method:"POST",
            data:{"code" : value, _token: $('meta[name="csrf-token"]').attr("content") },
            dataType:'JSON',
            success:function(data)
            {
                if(data.success){
                    $(".status-coupon").html('<span class="success text-success"> '+data.message+'</span>');
                }else{
                    $(".status-coupon").html('<span class="error text-danger"> '+data.message+'</span>');
                    $("input[name='coupon']").val("");
                }

                $(".loading").remove();
            },
            error:function(data){
                $(".loading").remove();
            }
        });
    }
    setActionDacTinh();
</script>
<style>
    .form-group.dactinh_modal {
        width: 100%;
    }

    .form-group.dactinh_modal .dactinh li {
        -webkit-box-flex: 0;
        flex: 0 0 10%;
        max-width: 10%;
    }
    .modal .modal-dialog .modal-content .box-info .box-input span.unit {
        display: inline-block;
        color: black;
        margin-left: 5px;
        font-size: 16px;
    }
    a.b-btn.btncoupon {
        background: #fdd800;
        line-height: 34px;
        padding: 0px 7px;
        border-radius: 5px;
        border: 1px solid #c7c7c7;
        font-size: 16px;
    }

    a.b-btn.btncoupon:hover {
        color: black;
    }
</style>

