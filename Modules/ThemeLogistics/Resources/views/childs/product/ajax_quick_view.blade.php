<div class="product-left-column col-xs-12 col-sm-6 col-md-6 col-lg-6">
    <div class="clearfix image-block">
					<span class="view_full_size">
						<a class="img-product" title="" href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) }}">
							<img id="product-featured-image-quickview"
                                 class="img-responsive product-featured-image-quickview center-block"
                                 src="{{ CommonHelper::getUrlImageThumb($product->image, 470, 350) }}" alt="quickview">
						</a>
					</span>
        <div class="loading-imgquickview" style="display:none;"></div>
    </div>
    <div class="more-view-wrapper clearfix">
        <div class="thumbs_quickview thumbs_list_quickview" id="thumbs_list_quickview">
            <ul class="product-photo-thumbs quickview-more-views-owlslider owl-loaded owl-drag"
                id="thumblist_quickview" style="visibility: visible;">
                <div class="owl-stage-outer">
                    <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: 0s; width: 640px;">
                        @foreach($image_extras as $item)
                            <div class="owl-item active" style="width: 150px; margin-right: 10px;">
                                <li><a href="javascript:void(0)" data-imageid="{{$product->id}}"
                                       data-zoom-image="{{ CommonHelper::getUrlImageThumb($item, 150, 90) }}"><img
                                                src="{{ CommonHelper::getUrlImageThumb($item, 150, 90) }}"
                                                alt="{{$product->name}}" style="max-width:120px; max-height:120px;"></a></li>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="owl-nav">
                    <div class="owl-prev disabled"></div>
                    <div class="owl-next"></div>
                </div>
                <div class="owl-dots">
                    <div class="owl-dot active"><span></span></div>
                    <div class="owl-dot"><span></span></div>
                </div>
            </ul>
        </div>
    </div>
</div>
<div class="product-center-column product-info product-item col-xs-5 col-sm-6 col-md-6 col-lg-6" id="product-8507826">
    <div class="head-qv">
        <h3 class="qwp-name"><a class="text2line" href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) }}"
                                title="{{$product->name}}">{{$product->name}}</a></h3>

    </div>
    <div class="quickview-info">

        <span class="prices"><span class="price h2">
                @if($product->final_price != 0)
                    {{ number_format($product->final_price, 0, '.', '.') }}
                @else
                    Liên hệ
                @endif
            </span></span>
    </div>
    <div class="product-description rte text4line">
        {!!$product->intro!!}
    </div>
    <a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product) }}" class="view-more">Xem chi tiết</a>
    @if($product->final_price != 0)
        <form action="/cart/add" method="post" enctype="multipart/form-data"
              class="quick_option variants form-ajaxtocart" id="product-actions-8507801">
					<span class="price-product-detail hidden" style="opacity: 0;">
						<span class=""></span>
					</span>

            <div class="clearfix"></div>
            <div class="quantity_wanted_p">
                <div class="input_qty_qv">
                    <a class="btn_num num_1 button button_qty"
                       onclick="var result = document.getElementById('quantity-detail'); var qtyqv = result.value; if( !isNaN( qtyqv ) &amp;&amp; qtyqv > 1 ) result.value--;return false;">-</a>
                    <input type="text" id="quantity-detail" name="quantity" value="1" onkeypress="validate(event)"
                           onkeyup="valid(this,'numbers')" onblur="valid(this,'numbers')"
                           class="form-control prd_quantity">
                    <a class="btn_num num_2 button button_qty"
                       onclick="var result = document.getElementById('quantity-detail'); var qtyqv = result.value; if( !isNaN( qtyqv )) result.value++;return false;">+</a>
                </div>
                <button type="submit" name="add"
                        class="btn btn-primary fix_add_to_cart button_cart_buy_enable add_to_cart_detail ajax_addtocart buy">
                    <span>Thêm vào giỏ hàng</span>
                </button>
            </div>
            <div class="total-price" style="display:none">{{ number_format($product->final_price, 0, '.', '.') }}₫</div>

            <input type="hidden" name="id" value="8507801"><input type="hidden" name="variantId" value="{{$product->id}}">
        </form>@endif

</div>

<script>
    $('.buy').click(function () {
        loading();
        $(this).attr('disabled', 'disabled');
        var quantity = $('input[name=quantity]').val();
        if (quantity < 1) {
            alert('Vui lòng nhập số lượng > 0');
        } else {
            $.ajax({
                url: '{{ route('order.add_to_cart') }}',
                type: 'GET',
                dataType: 'json',
                data: {
                    product_id: '{{ $product->id }}',
                    quantity: quantity
                },
                success: function (result) {
                    stopLoading();
                    $(this).removeAttr('disabled');
                    if (result.status == true) {
                        window.location.replace("{{ route('order.view') }}");
                        $('#count-cart').html(result.count_cart);
                        $('#cart-total-price').html(result.cart_total_price_format);
                        // alert('Đặt hàng thành công');
                    } else {
                        alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại');
                    }
                },
                error: function () {
                    stopLoading();
                    alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại');
                }
            });
        }
    });
</script>