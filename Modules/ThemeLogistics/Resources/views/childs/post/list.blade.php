@extends('themelogistics::layouts.layout_homepage')
@section('head')
    <style>

        .logo {
            z-index: 0;
        }

        section.bg_cont img {
            max-height: 200px;
            min-width: 100%;
            width: unset !important;
            height: unset;
        }
    </style>
@stop
@section('main_content')
    <section class="bg_cont">
        <img
                src="{{ asset('public/filemanager/userfiles/' . @$settings['post_banner']) }}"
                class="img_bg_cont"
        />
        <h1 class="t_cont">{{ $category->name }}</h1>
        <ul class="ul_breacrum" xmlns:v="http://rdf.data-vocabulary.org/#">
            <li class="list__item item--af">
                <a href="/"> Trang chủ </a>
                <i class="fas fa-angle-double-right"></i>
            </li>
            <li class="list__item">
                <a href="#"> {{ $category->name }} </a>
            </li>
        </ul>
        <!-- End .ul-breacrum -->
    </section>

    <section class="f_cont min_wrap">
    {{--<ul class="list_news filter-button-group">
        <li class="active news-item border_lb" data-filter=".tt"><a href="javascript:void(0)">Tin tức</a></li>
        <li class="news-item" data-filter=".ttn"><a href="javascript:void(0)">Tin Trong Ngành</a></li>
        <li class="news-item border_rb"data-filter=".bl"><a href="javascript:void(0)">Blog</a></li>
    </ul>--}}
    <!-- End .list_news -->
        <ul class="cont__list clearfix grid">
            @foreach($posts as $post)
                <li class="list-item grid-item tt ttn">
                    <a href="/{{ is_object($post->category) ? $post->category->slug : 'bai-viet' }}/{{ $post->slug }}.html">
                        <figure class="item__img">
                            <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image, 252, null) }}"
                                 class="img_object_fit" alt=""/>
                        </figure>
                        <div class="nd_news">
                            <h3>{{ $post->name }}</h3>
                            <ol>
                                <li>
                                    <svg
                                            class="svg-inline--fa fa-calendar-alt fa-w-14"
                                            aria-hidden="true"
                                            data-fa-processed=""
                                            data-prefix="far"
                                            data-icon="calendar-alt"
                                            role="img"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 448 512"
                                    >
                                        <path
                                                fill="currentColor"
                                                d="M148 288h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm108-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 96v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm192 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96-260v352c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h48V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h128V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h48c26.5 0 48 21.5 48 48zm-48 346V160H48v298c0 3.3 2.7 6 6 6h340c3.3 0 6-2.7 6-6z"
                                        ></path>
                                    </svg
                                    ><!-- <i class="far fa-calendar-alt"></i> -->
                                    {{ date('d/m/Y', strtotime($post->created_at)) }}
                                </li>
                            </ol>
                        </div>
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="page">
            {{ $posts->appends(Request::all())->links() }}
        </div>
    </section>
@endsection