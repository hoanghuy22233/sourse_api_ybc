
<style>
    .popup-add-cart{
        z-index: 999;
    }
    .gri-view-pro, .gri-add-cart{
        background: @if(@$settings['header_background']!=''){{@$settings['header_background']}} @else {{'linear-gradient(to bottom,#ffa103,#fb7d0f)'}}@endif!important;
        border-radius: 4px!important;
        border: none!important;
        color: #fff!important;
    }
    @media (min-width:990px) {
        .popup-add-cart-content{
            margin-top: 10%;
        }
    }
    .product_intro{
        height: 55px!important;
        text-align: justify;
    }
    @media (max-width:768px) {
        .gri{
            width: 46%;
            margin: 2%;
        }
        .banner-abc ul.flexJus{
            height: unset;
        }
        /*.f2-head>.b>.f2-topic ul li{*/
        /*    width: 100% !important;*/
        /*}*/
        .product_intro{
            height: 85px!important;
            text-align: justify;
        }
    }
    span.gri-add-cart.urlCart {
        float: right!important;
    }
    .gi h2{
        color: #000;
        font-size: 15px;
        text-transform: uppercase;
        height: 30px;
    }
    @media (max-width: 767px){
        .f>.inline-bl .flexJus {
            text-align: center;
        }
        #Product .gri .gi h2{
            height: 51px!important;
            line-height: 17px!important;
            font-size: 12px;
            -webkit-line-clamp: 3!important;
        }
    }
    .gri{
        height: 570px;
    }

</style>
<div class="f" style="background:#f6f6f6;">
    <div class="b" id="Product">
        @foreach($products as $product)
            <?php
                $manufacture = CommonHelper::getFromCache('manufacture_id'.@$product['manufacture_id']);
                if (!$manufacture){
                    $manufacture = \Modules\ThemeLogistics\Models\Manufacturer::find($product['manufacture_id']);
                CommonHelper::putToCache('manufacture_id'.@$product['manufacture_id'], $manufacture);
                }
            ?>
            <div class="gri"
                 href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                 title="{{@$product['meta_title']}}">
                <div class="gi">
                    @php
                        if (@$product['final_price'] != 0 && @$product['base_price'] != 0){
                        $discount = 100 - ((@$product['final_price'])/ @$product['base_price'] * 100);
                        }
                        else{
                        $discount = 0;
                        }
                    @endphp
                    @if($discount > 0)
                        <div class="badge @if(round($discount) >= 20) red_sale @else orange_sale @endif">
                            <div class="sale_text">{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::discount(@$product['base_price'], @$product['final_price']) }}</div>
                        </div>
                    @endif
                    <a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                       title="{{@$product['meta_title']}}">
                        <img class="lazy" data-src="{{CommonHelper::getUrlImageThumb($product['image'], 250, null) }}"
                             alt="{{$product['name']}}"/>
                    </a>
                    <div class="manuface_g">
                        @if(!empty($manufacture))
                            <a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                               title="{{@$product['meta_title']}}">
                                <img class="lazy" height="50"
                                     data-src="{{CommonHelper::getUrlImageThumb($manufacture->image, 150 ,null) }}"
                                     alt="{{$manufacture->name}}"/>
                            </a>
                            @else
                            </br></br></br>
                        @endif
                    </div>
                    <h2 style="margin-bottom: 10px"><a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                                                       title="{{@$product['meta_title']}}">{{$product['name']}}</a></h2>
                    <p class="product_intro">{!! $product['intro'] !!}</p>
                    @if($product['final_price'] < $product['base_price'])
                        <span style="padding-top: 12px;"><u
                                    style="text-decoration: line-through;font-size: 18px">{{number_format($product['base_price'], 0, '.', '.')}}</u><sup
                                    style="font-size: 18px;"> đ</sup></span>
                    @else
                        <div style="height: 32px;"></div>
                    @endif
                    @if($product['final_price'] != 0)
                        <span style="padding: 0px 2px;">@if($product['final_price'] < $product['base_price']) @else
                                Giá
                                bán @endif<b style="margin: 0!important;">{{number_format($product['final_price'], 0, '.', '.')}}&nbsp;<sup>đ</sup></b></span>
                        @else
                        </br> <span style="font-size: 25px; width: 100%!important;" class="pr">Liên hệ</span>
                    @endif
                    @if($product['final_price'] < $product['base_price'])
                        <span>Giảm: {{number_format($product['base_price'] - $product['final_price'], 0, '.', '.')}} &nbsp;<sup>đ</sup></span>
                    @else
                        <div style="height: 24px;border-top: none"></div>
                    @endif
                </div>


<?php
                    date_default_timezone_set('Asia/Ho_Chi_Minh');
                    $getDateToday = date_create(date('Y-m-d H:i:s'));
                    $sales = [];


                    $product_all = Modules\ThemeLogistics\Models\ProductSale::where(function ($query) use ($getDateToday) {
                        $query->orWhere('time_start', '<=', date_format($getDateToday, 'Y-m-d H:i:s'));
                        $query->orWhere('time_start', null);
                    })->where(function ($query) use ($getDateToday) {
                        $query->orWhere('time_end', '>=', date_format($getDateToday, 'Y-m-d H:i:s'));
                        $query->orWhere('time_end', null);
                    })->get();

                    $sale = [];
                    foreach ($product_all as $key => $sale_val) {
                        $manufacture = CommonHelper::getFromCache('manufacture_id_explode'.@$sale_val->manufacturer_ids);
                        if (!$manufacture) {
                            $manufacture = \Modules\ThemeLogistics\Models\Manufacturer::whereIn('id', explode('|', @$sale_val->manufacturer_ids))->get();
                            CommonHelper::putToCache('manufacture_id_explode'.@$sale_val->manufacturer_ids, $manufacture);
                        }
                        $cate_child = CommonHelper::getFromCache('category_id_explode'.@$sale_val->category_ids);
                        if (!$cate_child) {
                            $cate_child = \Modules\ThemeLogistics\Models\Category::whereIn('id', explode('|', @$sale_val->category_ids))->get();
                            CommonHelper::putToCache('category_id_explode'.@$sale_val->category_ids, $cate_child);
                        }

                        foreach ($cate_child as $rt) {
                            if ($rt->parent_id == 0 && in_array($rt->id, explode('|', @$sale_val->category_ids)) == true && in_array($rt->id, explode('|', @$product->multi_cat)) == true) {
                                $sale = Modules\ThemeLogistics\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                            } elseif (in_array($rt->id, explode('|', @$sale_val->category_ids)) == true && in_array($rt->id, explode('|', @$product->multi_cat)) == true) {
                                $sale = Modules\ThemeLogistics\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                            } else {
                                $sale = [];
                            }
                        }
//                        if ($sale == []) {
                            foreach ($manufacture as $rts) {
                                if (in_array($rts->id, explode('|', @$sale_val->manufacturer_ids)) == true && in_array($rts->id, explode('|', @$product->manufacture_id)) == true) {
                                    $sale = Modules\ThemeLogistics\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                                } else {
                                    $sale = [];
                                }
                            }
//                        }
                        if (array_diff([@$product->id], explode('|', @$sale_val->id_product)) == []) {
                            $sale = CommonHelper::getFromCache('product_id_explode_sale_val_id_product_sale' . @$sale_val->id_product_sale);
                            if (!$sale){
                                $sale = Modules\ThemeLogistics\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                                CommonHelper::putToCache('product_id_explode_sale_val_id_product_sale' . @$sale_val->id_product_sale, $sale);
                            }

                        }
                        if (!empty($sale)) {
                            $sales = array_merge($sales, $sale->toArray());
                        }
                    }
                    $ids = array_column($sales, 'id');
                    $ids = array_unique($ids);
                    $sales = array_filter($sales, function ($key, $value) use ($ids) {
                        return in_array($value, array_keys($ids));
                    }, ARRAY_FILTER_USE_BOTH);
                    ?>


                @if(@$settings['show_detail'] == 1 && @$settings['show_add_cart'] == 0)
                    <div style="border: none!important;">
                        <a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                           title="{{@$product['meta_title']}}"><span class="gri-view-pro" style="width: 100%!important;
                                        ">XEM HÀNG</span></a>
                    </div>
                @elseif(@$settings['show_detail'] == 0 && @$settings['show_add_cart'] == 1)
                    <div  style="border: none!important;">
                        <span data-text="{{route('order.view')}}" style="cursor: pointer;width: 100%!important;"
                              onclick="(addCart1({{@$product['id']}}))"
                              class="gri-add-cart urlCart">Cho vào giỏ</span>
                    </div>

                @elseif(@$settings['show_detail'] == 1 && @$settings['show_add_cart'] == 1)
                    <div style="border: none!important;">
                        <a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                           title="{{@$product['meta_title']}}"><span class="gri-view-pro">XEM HÀNG</span></a>
                        <span data-text="{{route('order.view')}}" style="cursor: pointer"
                              onclick="(addCart1({{@$product['id']}}))"
                              class="gri-add-cart urlCart">Cho vào giỏ</span>
                    </div>
                @elseif(@$settings['show_detail'] == 0 && @$settings['show_add_cart'] == 0)
                    <div style="display: none;"></div>
                @endif
            </div>
        @endforeach

    </div>
</div>

@if(count($products) < $countProduct)
    <div data-value="20" class="f" id="pmore"><p id="showModel">Xem thêm ...</p></div>
@endif
<style>

    .gri>.gi>a{
        height: 200px!important;
    }
</style>

