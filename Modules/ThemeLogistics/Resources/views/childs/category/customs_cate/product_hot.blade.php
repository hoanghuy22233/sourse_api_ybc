<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
{{--<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>--}}
<style>
    @import url(https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
    .product_featured {
        width: 69%!important;
        margin-left: 1%;

    }
    .hpanelwrap{
        height: 270px;
    }
    .product_featured label {
        display: block;
        font: 16px/40px arial;
        text-align: center;
        margin-bottom: 16px;
        background: #f6f6f6;
    }
    .f2-h {
        width: 30%!important;
    }
    div#hpanel a.manuface{
        width: 23%;
        height: auto;
        padding: 5px;
        margin: 1%;
        border-radius: 10px;
        border: 1px solid #eee;
    }


    .col-item
    {
        /*border: 1px solid #E1E1E1;*/
        border-radius: 5px;
    }
    .price>h3>a:hover {
        text-decoration: none;
        color: #ffa103;
    }
    .price>h3{
        padding: 0 10px;
        font-size: 17px;
        margin: 0;
        text-decoration: none;
        text-transform: uppercase;
    }
    .price>h3>a{

        font-weight: 600;
        font-size: 12px;
        color: #1d537f;

    }
    .col-item .photo img
    {
        margin: 0 auto;
        width: 100%;
        transition: all .3s ease-in-out;
        object-fit: contain;
    }
    .photo {
        height: 120px;
        overflow: hidden;
        margin: 0;
    }
    .photo>a {
        display: block;
        height: 100%;
    }
    .col-item .info
    {
        padding: 0 0 10px 0;
        border-radius: 0 0 5px 5px;
        margin-top: 1px;
    }

    .col-item:hover .info {
        /*background-color: #F5F5DC;*/
    }
    .col-item .price
    {
        /*width: 50%;*/
        /*float: left;*/
        margin-top: 5px;
    }

    .col-item .price h5
    {
        line-height: 20px;
        margin: 0;
    }

    .price-text-color
    {
        color: #219FD1;
    }

    .col-item .info .rating
    {
        color: #777;
    }

    .col-item .rating
    {
        /*width: 50%;*/
        float: left;
        font-size: 17px;
        text-align: right;
        line-height: 52px;
        margin-bottom: 10px;
        height: 52px;
    }

    .col-item .separator
    {
        /*border-top: 1px solid #E1E1E1;*/
    }

    .clear-left
    {
        clear: left;
    }

    .col-item .separator p
    {
        line-height: 20px;
        margin-bottom: 0;
        margin-top: 10px;
        width: 100%;
        text-align: center;
    }

    .col-item .separator p i
    {
        margin-right: 5px;
    }
    .col-item .btn-add
    {
        width: 50%;
        float: left;
    }

    .col-item .btn-add
    {
        /*border-right: 1px solid #E1E1E1;*/
    }

    .col-item .btn-details
    {
        width: 50%;
        float: left;
        padding-left: 10px;
    }
    .controls
    {
        margin-top: 20px;
    }
    [data-slide="prev"]
    {
        margin-right: 10px;
    }
    .width20 {
        width: 33.333%;!important;
        padding: 0 10px;
    }
    .f2-h label, .f2-topic label {
        font: 16px/40px arial;
    }
    span.gri-view-pro {
        background: linear-gradient(to top,#ffa103,#fb7d0f);
        color: #fff;
        font: bold 12px/33px Arial;
        padding: 0 10px;
        border-radius: 5px;
        text-transform: uppercase;
    }
    span.gri-add-cart {
        background: linear-gradient(to top,#ffa103,#fb7d0f);
        color: #fff;
        font: bold 12px/33px Arial;
        padding: 0 10px;
        border-radius: 5px;
        text-transform: uppercase;
    }
    a.arow-left:before {

        background: #e4e4e4!important;
        position: absolute!important;
        left: 5px!important;
        top: 50%!important;
        border-radius: 3px!important;
        height: 42px!important;
        width: 40px!important;
        line-height: 42px!important;
        text-align: center!important;
        z-index: 1!important;
        font-size: 24px;
        opacity: 0.5;
        color: #999;
    }
    a.arow-right:before {
        background: #e4e4e4!important;
        position: absolute!important;
        right: 0!important;
        top: 47%!important;
        border-radius: 3px!important;
        opacity: 0.5;
        color: #999;
        height: 42px!important;
        width: 42px!important;
        line-height: 42px!important;
        font-size: 24px;
        text-align: center!important;
        z-index: 1!important;
    }
    .pro_hot>h3{
        border-left: 5px solid red;
        background: #eee;
        padding: 10px 0 10px 10px;
        margin: 20px 0 10px 0;
    }
    .pro_hot {
        margin-top: 20px;
        padding: 0;
    }
    @media (max-width: 991px){
        .f2-h, .product_featured{
            width: 100%!important;
        }
    }
    @media (max-width: 500px){
        .photo{
            height: unset;
        }
    }
    @media (max-width: 768px){
        #Product .gri{
            width: 100%!important;
        }
    }
    .slide_product{
        border: 1px solid #ddd;
        display: flex
    }
    /*.hpanelwrap{*/
    /*    height: 290px!important;*/
    /*}*/
    .slide_product_content {
        width:100%;
        border: 1px solid red!important
    }
    .list_product {
        width: 33.333%;
        padding-right: 2%;
    }
    .list_products {
        display: flex;
    }

    @media only screen and (max-width: 768px) {
        .f2-hsx a img {
            height: auto;
        }

        .list_product {
            width: 50%;
        }
    }
</style>
<div class="f f2-head">
    <div class="b">
        <div class="flexCol f2-h fl">
            <label>CÁC THƯƠNG HIỆU NỔI TIẾNG</label>
            <div class="f flexCol f2-hsx ">
                <div class="hpanelwrap" style="overflow-y:auto;">
                    <div id="hpanel" style="display: flex;flex-wrap: wrap;">
                        @php
                            $brans = \Modules\ThemeLogistics\Models\Manufacturer::where('status', 1)->whereIn('id', $bran_ids)->orderBy('order_no', 'asc')->get();
                        @endphp
                        @if(!empty($brans))
                            @foreach($brans as $bran)
                                <a class="manuface" data-value="{{$bran->id}}" title="{{$bran->name}}"
                                   href="{{route('cate.list',['slug' => $category->slug.'/'.$bran->slug])}}">
                                    <img class="lazy" data-src="{{CommonHelper::getUrlImageThumbNoCut($bran->image)}}"
                                         alt="{{$bran->image}}"/>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>

            </div>
            <script>
                $(document).ready(function () {
                    $('#viewfullsapo').click(function () {
                        $('#sapo').css('height', 'auto');
                        $(this).hide();
                    });
                });
            </script>
        </div>
        <div class="flexCol product_featured fl">
            <label>SẢN PHẨM BÁN CHẠY</label>
            <div class="slide_product">
                <?php

                $products = \Modules\ThemeLogistics\Models\Product::where('multi_cat','like', '%|'.@$category->id.'|%')->where('featured',1)->where('status',1)->orderBy('id', 'decs')->get();

                ?>
@if($products->count()>0)
                    <div id="carousel-example" class="carousel slide slide_product_content" data-ride="carousel">
                        <a class=" arow-left left fa fa-chevron-left " href="#carousel-example"
                           data-slide="prev"></a>
                        <a class="arow-right right fa fa-chevron-right"href="#carousel-example" data-slide="next"></a>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            @foreach($products as $k=>$product)
                                @if($k%3==0)
                                    <div class="item {{($k==0)?'active':''}}">
                                        <div class="list_products">
                                            @endif
                                            <div class="list_product">
                                                <div class="col-item">
                                                    <div class="photo">
                                                        <a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                                                           title="{{@$product->meta_title}}">
                                                            <img class="lazy img-responsive"
                                                                 data-src="{{CommonHelper::getUrlImageThumb($product->image, 150 ,null) }}"
                                                                 alt="{{$product->name}}"/>
                                                        </a>
                                                    </div>
                                                    <div class="info">
                                                        <div class="">
                                                            <div class="price  text-center">
                                                                <h3>
                                                                    <a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                                                                       title="{{@$product->meta_title}}">{{$product->name}}</a>
                                                                </h3>
                                                                @if($product->final_price < $product->base_price)
                                                                    <p  class="base_price">
                                                                        <strike>{{number_format($product->base_price, 0, '.', '.')}}</strike><sup> đ</sup></p>
                                                                @else
                                                                    <div style="height: 32px;"></div>
                                                                @endif
                                                                @if($product->final_price != 0)
                                                                    <p class="final_price">
                                                                        @if($product->final_price < $product->base_price) @else
                                                                            @endif<b  class="final_price">{{number_format($product->final_price, 0, '.', '.')}}&nbsp;<sup>đ</sup></b></p>
                                                                    @else
                                                                    </br> <p  class="final_price" class="pr">Liên hệ</p>
                                                                @endif
{{--                                                                @if($product->final_price < $product->base_price)--}}
{{--                                                                    <p class="giam">Giảm: {{number_format($product->base_price - $product->final_price, 0, '.', '.')}} &nbsp;<sup>đ</sup></p>--}}
{{--                                                                @else--}}
{{--                                                                    <div style="height: 24px;border-top: none"></div>--}}
{{--                                                                @endif--}}
                                                            </div>

                                                        </div>
                                                        <div class="separator clear-left">
                                                            <p class="btn-add">
                                                                <a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                                                                   title="{{@$product->meta_title}}">
                                                                    <span class=" gri-view-pro btn">XEM HÀNG</span>
                                                                </a>
                                                            </p>
                                                        </div>
                                                        <div class="clearfix">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if(($k+1)%3==0)
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>







                    </div>

@else
    <p style="    margin: 20px auto;">Chưa có sản phẩm nổi bật nào!!!</p>
@endif
{{--                @foreach($products as $product)--}}
{{--                    <div class="col-sm-3 width20">--}}
{{--                        <div class="col-item">--}}
{{--                            <div class="photo">--}}
{{--                                <a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}" title="{{$product->name}}">--}}
{{--                                    <img class="lazy img-responsive" data-src="{{CommonHelper::getUrlImageThumb($product->image, 400 ,null) }}" alt="{{$product->name}}">--}}
{{--                                </a>--}}

{{--                            </div>--}}
{{--                            <div class="info">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="price col-md-12 text-center">--}}
{{--                                        <h3><a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"--}}
{{--                                               title="{{@$product->meta_title}}">{{$product->name}}</a>--}}
{{--                                        </h3>--}}

{{--                                        @if($product->final_price < $product->base_price)--}}
{{--                                            <p style="margin: 0">--}}
{{--                                                <u style="text-decoration: line-through;font-size: 15px">{{number_format($product->base_price, 0, '.', '.')}}</u><sup--}}
{{--                                                   style="font-size: 15px;"> đ</sup>--}}
{{--                                            </p>--}}
{{--                                        @else--}}
{{--                                            <div style="height: 32px;"></div>--}}
{{--                                        @endif--}}
{{--                                        @if($product->final_price != 0)--}}
{{--                                            <p style="  color: red;--}}
{{--                                                        font-size: 23px;--}}
{{--                                                        font-weight: 900;--}}
{{--                                                        padding-bottom: 5px;--}}
{{--                                                        margin: 0;">--}}
{{--                                                @if($product->final_price >= $product->base_price)--}}
{{--                                                    Giá bán--}}
{{--                                                @endif--}}
{{--                                                    <b>{{number_format($product->final_price, 0, '.', '.')}}&nbsp;<sup>đ</sup></b>--}}
{{--                                            </p>--}}
{{--                                        @else--}}
{{--                                            </br>--}}
{{--                                            <p style="font-size: 25px; width: 100%!important;" class="pr">Liên hệ</p>--}}
{{--                                        @endif--}}
{{--                                        @if($product->final_price < $product->base_price)--}}
{{--                                            <p>Giảm: {{number_format($product->base_price - $product->final_price, 0, '.', '.')}} &nbsp;<sup>đ</sup></p>--}}
{{--                                        @else--}}
{{--                                            <div style="height: 24px;border-top: none"></div>--}}
{{--                                        @endif--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="separator clear-left">--}}
{{--                                    <p class="btn-add">--}}
{{--                                        <a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}" title="{{$product->name}}">--}}
{{--                                            <span class=" gri-view-pro btn">XEM HÀNG</span>--}}
{{--                                        </a>--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                                <div class="clearfix"></div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
            </div>
        </div>
    </div>
</div>