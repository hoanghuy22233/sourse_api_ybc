<style>
    .f2-head >.b>.f2-h{
        width: 100%!important;
    }

    .thuonghieu a {
        width: 14.6%;
        padding: 3px;
        float: left;
        background: #fff;
        height: 80px;
        overflow: hidden;
        text-align: center;
        border-radius: 10px;
        margin: 1%;
        border: 1px solid #eee;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    @media only screen and (max-width: 768px) {
        .f2-hsx a img {
            height: auto;
        }
        .thuonghieu a {
            width: 23%;
            height: auto;
            padding: 5px;
        }
    }
</style>
<div class="f f2-head">
    <div class="b">
        <div class="flexCol f2-h fl">
            <label>CÁC THƯƠNG HIỆU NỔI TIẾNG</label>
            <div class="thuonghieu">
                <?php
                $brans = CommonHelper::getFromCache('manufacturer_id_order_no_asc' . @implode('|', $bran_ids));
                    if (!$brans) {
                        $brans = \Modules\ThemeLogistics\Models\Manufacturer::where('status', 1)->whereIn('id', $bran_ids)->orderBy('order_no', 'asc')->get();
                    CommonHelper::putToCache('manufacturer_id_order_no_asc' . @implode('|', $bran_ids), $brans);
                }

                ?>
                @if(!empty($brans))
                    @foreach($brans as $bran)
                        <a class="manuface" data-value="{{$bran->id}}" title="{{$bran->name}}"
                           href="{{route('cate.list',['slug' => $category->slug.'/'.$bran->slug])}}">
                            <img class="lazy" data-src="{{CommonHelper::getUrlImageThumbNoCut($bran->image)}}"
                                 {{--                                    <img src="https://sieuthibepdien.com/timthumb.php?src=https://sieuthibepdien.com/public/filemanager/userfiles/manufacturer_files/teka.png&w=126"--}}
                                 alt="{{$bran->image}}"/>
                        </a>
                    @endforeach
                @endif
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('#viewfullsapo').click(function () {
                    $('#sapo').css('height', 'auto');
                    $(this).hide();
                });
            });
        </script>
    </div>

</div>