<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet"  type="text/css" href="{{URL::asset('public/frontend/themes/themelogistic/css/account.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans|Roboto&amp;display=swap" rel="stylesheet">
    <title></title>
</head>
<body>
<div class="container-fluid hidden-scroll">
    <div class="row">
        <div class="left-content content">
            <div class="background-left" style="background-image: url(https://logistics.webhobasoft.com/static/media/4753605.jpg);">
                <div class="content-note"><a href="/v2/ssoLogin?app=import&amp;returnUrl=http://khachhang.ghn.vn/sso-login?token=">
                        <div class="logo"><img class="img-fulid" width="170" height="70"
                                               src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo'], null, null) }}"
                                               alt="Etal Group Logistic">
                        </div>
                    </a>
                    <div class="left-row2">THIẾT KẾ CHO GIẢI PHÁP GIAO NHẬN HÀNG<br>TỐT NHẤT TỪ TRƯỚC ĐẾN NAY</div>
                    <i class="left-row3">Nhanh hơn, rẻ hơn và thông minh hơn</i></div>
                <div class="backdrop"></div>
            </div>
        </div>
        <div class="right-content">
            <div class="form-content">
                <div class="register-content">
                    <div class="title-register"><span class="register-row-1">TẠO TÀI KHOẢN Etal Group</span>
                        <p class="register-row-2">Etal Group luôn đồng hành cùng bạn</p></div>
                    @if(Session::has('message'))
                        <div class="alert alert-success contact__msg" role="alert">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <form action="{{route('user.postregister')}}" method="post">
                        {{ csrf_field() }}
                    <div class="register-form">

                        <div class="row">
                            <div class="col-1"></div>
                            <div class="col-5">
                                <div class="form-group"><label class="">Mục đích sử dụng</label>
                                    <input tabindex="5" name="target" value="{{old('target')}}"  placeholder="Nhập mục đích sử dụng" type="text" class="form-control {{ $errors->first('target', 'is-invalid') }}">
                                    <div class="text-danger">{{ $errors->first('target') }}</div>
                                </div>
                                <div class="form-group"><label class="">Quy mô vận chuyển</label>
                                    <input tabindex="5" name="quymo" value="{{old('quymo')}}"  placeholder="Nhập quy mô vận chuyển" type="text" class="form-control {{ $errors->first('quymo', 'is-invalid') }}">
                                    <div class="text-danger">{{ $errors->first('quymo') }}</div>
                                </div>
                                <div class="form-group"><label class="">Số điện thoại</label>
                                    <input tabindex="5" name="phone" value="{{old('phone')}}"  autocomplete="Phone" placeholder="Nhập số điện thoại" type="number"
                                           class="form-control {{ $errors->first('phone', 'is-invalid') }}">

                                    <div class="text-danger">{{ $errors->first('phone') }}</div>

                                </div>
                                <div class="form-group"><label class="">Mật khẩu</label>
                                    <input tabindex="7" maxlength="50" minlength="8" placeholder="Tối thiểu 8 ký tự bao gồm chữ cái và số" name="password" autocomplete="current password" type="password" class="form-control {{ $errors->first('password', 'is-invalid') }}">
                                    <div class="text-danger">{{ $errors->first('password') }}</div>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group"><label class="">Tên tài khoản</label>
                                    <input maxlength="225" tabindex="2" value="{{old('nameaccount')}}" name="nameaccount" placeholder="Tên tài khoản" type="text" class="form-control {{ $errors->first('nameaccount', 'is-invalid') }}">
                                    <div class="text-danger">{{ $errors->first('nameaccount') }}</div>
                                </div>
                                <div class="form-group"><label class="">Ngành hàng</label>
                                    <input tabindex="4" name="specialized" placeholder="Chọn ngành hàng" type="text" class="form-control {{ $errors->first('specialized', 'is-invalid') }}" value="{{old('specialized')}}">
                                    <div class="text-danger">{{ $errors->first('specialized') }}</div>
                                </div>
                                <div class="form-group"><label class="">Email</label>
                                    <input tabindex="6" name="email" autocomplete="Email" placeholder="Nhập Email" type="email" class="form-control {{ $errors->first('email', 'is-invalid') }}">
                                    <div class="text-danger">{{ $errors->first('email') }}</div>
                                </div>
                                <div class="form-group"><label class="">Nhập lại mật khẩu</label>
                                    <input tabindex="8" placeholder="Tối thiểu 8 ký tự bao gồm chữ cái và số" maxlength="50" minlength="8" name="repassword" type="password" class="form-control {{ $errors->first('repassword', 'is-invalid') }}">
                                    <div class="text-danger">{{ $errors->first('repassword') }}</div>
                                </div>
                            </div>
                            <div class="col-1"></div>
                        </div>

                        <div class="register-row-4 row">
                            <div class="content-register">
{{--                                <div class="form-group">--}}
{{--                                    <div render="explicit" class="g-recaptcha" data-sitekey="6LcyhrUUAAAAABV80jmGsnfJ3Fe_nLSRSxth1IlT" data-callback="_grecaptcha.data-callback" data-expired-callback="_grecaptcha.data-expired-callback">--}}
{{--                                        <div style="width: 304px; height: 78px;">--}}
{{--                                            <div>--}}
{{--                                                <iframe src="https://www.google.com/recaptcha/api2/anchor?ar=1&amp;k=6LcyhrUUAAAAABV80jmGsnfJ3Fe_nLSRSxth1IlT&amp;co=aHR0cHM6Ly9zc28uZ2huLnZuOjQ0Mw..&amp;hl=vi&amp;v=qc5B-qjP0QEimFYUxcpWJy5B&amp;size=normal&amp;cb=rf6v74jx30z8" width="304" height="78" role="presentation" name="a-rwyde6q5xcl7" frameborder="0" scrolling="no" sandbox="allow-forms allow-popups allow-same-origin allow-scripts allow-top-navigation allow-modals allow-popups-to-escape-sandbox"></iframe>--}}
{{--                                            </div>--}}
{{--                                            <textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 40px; border: 1px solid rgb(193, 193, 193); margin: 10px 25px; padding: 0px; resize: none; display: none;"></textarea>--}}
{{--                                        </div>--}}
{{--                                        <iframe style="display: none;"></iframe>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="form-group"><p style="margin-top: 12px;"> Bằng việc nhấn "Đăng ký", bạn
                                        đồng ý với <br><a target="_blank" rel="noopener noreferrer" href="https://ghn.vn/pages/dieu-khoan-su-dung"><span style="color: rgb(255,220,25); font-weight: 700;">Điều khoản dịch vụ</span></a>
                                        và <a target="_blank" rel="noopener noreferrer" href="https://ghn.vn/pages/chinh-sach-bao-mat"><span style="color: rgb(255,220,25); font-weight: 700;">Quy định Riêng tư Cá nhân</span></a>
                                        <br>của chúng tôi.</p>
                                    <button style="color: black !important;" type="submit" tabindex="9" class="btn-primay-page btn-register btn btn-secondary">Đăng ký
                                    </button>
                                </div>
                                <div class="register-row-3"><label class="">Bạn đã có tài khoản?</label><a href="{{route('user.login')}}" style="color: rgb(255, 99, 57); font-weight: 700; text-decoration: none;">
                                        Đăng nhập ngay</a></div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-otp"></div>
    <div></div>
    <div></div>
    <div class="Toastify"></div>
</div>
</body>
</html>


