@if(!isset($_SESSION['block_adsPopup']))
    <div class="block_adsPopup" id="block_adsPopup" style="display: none;">
        <div class="block_background" onclick="close_popup()" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></div>
        <div class="block_content" style="position: absolute; width: 600px;height: 450px;top: 50%; margin-top: -225px;left: 50%; margin-left: -300px;">
            <div class="item">
                <div class="item_content" id="video_content">
                    <a href="{!! $setting->popup_link !!}" target="_blank"><img class="lazy" data-src="{{ URL::asset('filemanager/userfiles/' . $setting->popup_image) }}"/></a>			</div>
                <a href="javascript:void(0)" onclick="close_popup()" class="close">Close</a>
            </div>
        </div>
        <script type="text/javascript">
            setTimeout(function(){
                $('#block_adsPopup').show();
                var w_window = $(window).width();
                if(w_window < 600) {
                    var w_p = w_window - 40;
                    $('#block_adsPopup .block_content').css({'width': w_p + 'px'});
                }
                var h_window = $(window).height();
                var h_item = $('#block_adsPopup .block_content').height();
                var top_item = (h_window - h_item)/2;
            },3000);
            /*
             setTimeout(function(){
             close_popup();
             },15000);*/
            function close_popup(){
                $('#block_adsPopup').remove();
            }
        </script>
    </div>
    <?php $_SESSION['block_adsPopup'] = true;?>
@endif