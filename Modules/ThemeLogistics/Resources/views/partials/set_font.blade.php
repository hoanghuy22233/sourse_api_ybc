<?php
//$list_fonts =  explode("\r\n",@$settings['list_font']);
$settingss = CommonHelper::getFromCache('settings_name_list_font_type_common_tab');
if (!$settingss){
    $settingss =\Modules\ThemeLogistics\Models\Settings::where('name','list_font')->where('type','common_tab')->first();
    CommonHelper::putToCache('settings_name_list_font_type_common_tab', $settingss);
}

$set_font = CommonHelper::getFromCache('settings_name_set_font_type_common_tab');
if (!$set_font){
    $set_font =\Modules\ThemeLogistics\Models\Settings::where('name','set_font')->where('type','common_tab')->first();
    CommonHelper::putToCache('settings_name_set_font_type_common_tab', $set_font);
}
$list_fonts =  explode("\r\n",@$settingss->value);
//dd($settings);
?>
@foreach($list_fonts as $k=>$v)
    @if (@$set_font->value==$k)
        <style>
            @import url('https://fonts.googleapis.com/css?family={{str_replace(' ','+',$v)}}&display=swap');
            *,body {
                font-family: '{{$v}}', sans-serif!important;
            }
        </style>
    @endif
@endforeach
{{--    @if (@$settings['set_font']==0)--}}
{{--        <style>--}}
{{--            @import url('https://fonts.googleapis.com/css?family=Roboto&display=swap');--}}
{{--            *,body {--}}
{{--                font-family: 'Roboto', sans-serif!important;--}}
{{--            }--}}
{{--        </style>--}}
{{--    @elseif (@$settings['set_font']==1)--}}
{{--    <style>--}}
{{--        @import url('https://fonts.googleapis.com/css?family=Open+Sans&display=swap');--}}
{{--        *,body {--}}
{{--            font-family: "Open Sans", sans-serif!important;--}}
{{--        }--}}
{{--    </style>--}}
{{--    @elseif (@$settings['set_font']==2)--}}
{{--    <style>--}}
{{--        @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap');--}}
{{--        *,body {--}}
{{--            font-family: 'Source Sans Pro', sans-serif!important;--}}
{{--        }--}}
{{--    </style>--}}
{{--    @elseif (@$settings['set_font']==3)--}}
{{--    <style>--}}
{{--        @import url('https://fonts.googleapis.com/css?family=Montserrat&display=swap');--}}
{{--        *,body {--}}
{{--            font-family: 'Montserrat', sans-serif!important;--}}
{{--        }--}}
{{--    </style>--}}
{{--    @elseif (@$settings['set_font']==4)--}}
{{--    <style>--}}
{{--        @import url('https://fonts.googleapis.com/css?family=Lato&display=swap');--}}
{{--        *,body {--}}
{{--            font-family: 'Lato', sans-serif!important;--}}
{{--        }--}}
{{--    </style>--}}
{{--    @elseif (@$settings['set_font']==5)--}}
{{--        <link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">--}}
{{--    <style>--}}
{{--        *,body {--}}
{{--            font-family: 'Raleway', sans-serif!important;--}}
{{--        }--}}
{{--    </style>--}}
{{--    @elseif (@$settings['set_font']==6)--}}
{{--    <style>--}}
{{--        @import url('https://fonts.googleapis.com/css?family=Poppins&display=swap');--}}
{{--        *, body {--}}
{{--            font-family: 'Poppins', sans-serif!important;--}}
{{--        }--}}
{{--    </style>--}}
{{--    @endif--}}