<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
{{--<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>--}}
<!------ Include the above in your HEAD tag ---------->
<style>
    @import url(https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
    .col-item
    {
        /*border: 1px solid #E1E1E1;*/
        border-radius: 5px;
        background: #FFF;
    }
    .price>h3>a:hover {
        text-decoration: none;
        color: #ffa103;
    }
    .price>h3{
        padding: 0 10px;
        font-size: 17px;
        margin: 0;
        text-decoration: none;
        text-transform: uppercase;
    }
    .price>h3>a{

        font-weight: 600;
        font-size: 12px;
        color: #1d537f;

    }
    .col-item .photo img
    {
        margin: 0 auto;
        /*width: 100%;*/
        transition: all .3s ease-in-out;
        object-fit: contain;
    }
    .photo {
        height: 100px;
        overflow: hidden;
        margin: 0;
    }
    .photo>a {
        display: block;
        height: 100%;
    }
    .col-item .info
    {
        padding: 0 0 10px 0;
        border-radius: 0 0 5px 5px;
        margin-top: 1px;
    }

    .col-item:hover .info {
        /*background-color: #F5F5DC;*/
    }
    .col-item .price
    {
        /*width: 50%;*/
        float: left;
        height: 125px;
        margin-top: 5px;
    }

    .col-item .price h5
    {
        line-height: 20px;
        margin: 0;
    }

    .price-text-color
    {
        color: #219FD1;
    }

    .col-item .info .rating
    {
        color: #777;
    }

    .col-item .rating
    {
        /*width: 50%;*/
        float: left;
        font-size: 17px;
        text-align: right;
        line-height: 52px;
        margin-bottom: 10px;
        height: 52px;
    }

    .col-item .separator
    {
        /*border-top: 1px solid #E1E1E1;*/
    }

    .clear-left
    {
        clear: left;
    }

    .col-item .separator p
    {
        line-height: 20px;
        margin-bottom: 0;
        margin-top: 10px;
        width: 100%;
        text-align: center;
    }

    .col-item .separator p i
    {
        margin-right: 5px;
    }
    .col-item .btn-add
    {
        width: 50%;
        float: left;
    }

    .col-item .btn-add
    {
        /*border-right: 1px solid #E1E1E1;*/
    }

    .col-item .btn-details
    {
        width: 50%;
        float: left;
        padding-left: 10px;
    }
    .controls
    {
        margin-top: 20px;
    }
    [data-slide="prev"]
    {
        margin-right: 10px;
    }
    .width20 {
        width: 20%;!important;
    }
    span.gri-view-pro {
        background: linear-gradient(to top,#ffa103,#fb7d0f);
        color: #fff;
        font: bold 12px/33px Arial;
        padding: 0 10px;
        border-radius: 5px;
        text-transform: uppercase;
    }
    span.gri-add-cart {
        background: linear-gradient(to top,#ffa103,#fb7d0f);
        color: #fff;
        font: bold 12px/33px Arial;
        padding: 0 10px;
        border-radius: 5px;
        text-transform: uppercase;
    }
    a.arow-left:before {

        background: #e4e4e4!important;
        position: absolute!important;
        left: 5px!important;
        top: 50%!important;
        border-radius: 3px!important;
        height: 42px!important;
        width: 40px!important;
        line-height: 42px!important;
        text-align: center!important;
        z-index: 1!important;
        font-size: 24px;
        opacity: 0.5;
        color: #999;
    }
    a.arow-right:before {
        background: #e4e4e4!important;
        position: absolute!important;
        right: 0!important;
        top: 47%!important;
        border-radius: 3px!important;
        opacity: 0.5;
        color: #999;
        height: 42px!important;
        width: 42px!important;
        line-height: 42px!important;
        font-size: 24px;
        text-align: center!important;
        z-index: 1!important;
    }
    .pro_hot>h3{
        border-left: 5px solid red;
        background: #eee;
        padding: 10px 0 10px 10px;
        margin: 20px 0 10px 0;
    }
    .pro_hot {
        margin-top: 20px;
        padding: 0;
    }

    .final_price{
        padding: 0px 2px;
        color: red;
        font-size: 26px;
        font-weight: 900;
        margin: 0;
        line-height: 30px;
        padding-bottom: 5px;
        width: 100%!important;
    }
    .base_price{
        text-decoration: line-through;
        font-size: 18px;
        margin: 0;
    }

    @media (max-width: 768px) {
        .width20{

        }
    }
    @media (max-width: 768px) {
        .final_price{
            font-size: 16px;
        }
        .base_price{
            font-size: 13px;
        }
        .giam{
            font-size: 13px;
        }
    }
    @media (max-width: 468px) {
        .final_price{
            font-size: 14px;
        }
        .base_price{
            font-size: 11px;
        }
        .giam{
            font-size: 11px;
        }
        .price>h3>a{
            font-size: 11px;
        }
    }
</style>
<?php
$features = \Modules\ThemeLogistics\Models\Product::where('featured',1)->where('status',1)->get();
?>
<div class="f">
    <div class=" container-fluid" style="margin-bottom: 30px;">
        <div class="">
            <div class="row" style="margin: 0!important;">
                <div class="col-md-12 pro_hot">
                    <h3>Sản phẩm bán chạy</h3>
                </div>
            </div>
            <div id="carousel-example" class="carousel slide " data-ride="carousel"  style="border: 1px solid red!important">
                <a class=" arow-left left fa fa-chevron-left " href="#carousel-example"
                   data-slide="prev"></a>
                <a class="arow-right right fa fa-chevron-right  " href="#carousel-example"
                   data-slide="next"></a>
                <!-- Wrapper for slides -->
                <div class="carousel-inner ">

                    @foreach($features as $k=>$featured)
                        @if($k%2==0)
                            <div class="item {{($k==0)?'active':''}}">
                                <div class="row">
                                    @endif
                                    <div class="col-6 col-xs-6">
                                        <div class="col-item">
                                            <div class="photo">
                                                <a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($featured, 'array') }}"
                                                   title="{{@$featured->meta_title}}">
                                                    <img class=" lazy img-responsive"
                                                         data-src="{{CommonHelper::getUrlImageThumb($featured->image, 150 ,null) }}"
                                                         alt="{{$featured->name}}"/>
                                                </a>
                                            </div>
                                            <div class="info">
                                                <div class="row">
                                                    <div class="price col-md-12 col-12 col-xs-12 text-center">
                                                        <h3><a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($featured, 'array') }}"
                                                               title="{{@$featured->meta_title}}">{{$featured->name}}</a></h3>

                                                        @if($featured->final_price < $featured->base_price)
                                                            <p  class="base_price">
                                                                <u>{{number_format($featured->base_price, 0, '.', '.')}}</u><sup> đ</sup></p>
                                                        @else
                                                            <div style="height: 32px;"></div>
                                                        @endif
                                                        @if($featured->final_price != 0)
                                                            <p class="final_price">
                                                                @if($featured->final_price < $featured->base_price) @else
                                                                     @endif<b  class="final_price">{{number_format($featured->final_price, 0, '.', '.')}}&nbsp;<sup>đ</sup></b></p>
                                                            @else
                                                            </br> <p  class="final_price" class="pr">Liên hệ</p>
                                                        @endif
                                                        @if($featured->final_price < $featured->base_price)
                                                            <p class="giam">Giảm: {{number_format($featured->base_price - $featured->final_price, 0, '.', '.')}} &nbsp;<sup>đ</sup></p>
                                                        @else
                                                            <div style="height: 24px;border-top: none"></div>
                                                        @endif
                                                    </div>

                                                </div>
                                                <div class="separator clear-left">
                                                    <p class="btn-add">
                                                        <a href="{{ \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getProductSlug($featured, 'array') }}"
                                                           title="{{@$featured->meta_title}}">
                                                            <span class=" gri-view-pro btn">XEM HÀNG</span>
                                                        </a>
                                                    </p>
                                                </div>
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(($k+1)%2==0)
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

            </div>
        </div>
    </div>
</div>
