<section>
    <ul class="nav navbar-pills pills_post">
        @php $data = \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getFromCache('get_main_menu_post');
                            if (!$data) {
                                $data = \Modules\ThemeLogistics\Models\Category::select(['id', 'name', 'slug', 'type'])
                                        ->where('type', 1)->orderBy('order_no', 'asc')->get();

                                \Modules\ThemeLogistics\Http\Helpers\CommonHelper::putToCache('get_main_menu_post', $data);
                            }
        @endphp
        @foreach($data as $lv1)
            @php
                $lv2 = \Modules\ThemeLogistics\Http\Helpers\CommonHelper::getFromCache('get_main_menu_post_childs_of_item_' . $lv1->id);
        if (!$lv2) {
            $lv2 = $lv1->childs;

            \Modules\ThemeLogistics\Http\Helpers\CommonHelper::putToCache('get_main_menu_post_childs_of_item_' . $lv1->id, $lv2);
        }
            @endphp

            <li class="nav-item {{ count($lv2) > 0 ? 'has-mega' : '' }}">
                @if($lv1->name == "Trang chủ")
                    <a href="/" class="nav-link">{{ $lv1->name }}</a>
                @else
                    <a href="{{ URL::to('category/' .  $lv1->slug) }}" class="nav-link">{{ $lv1->name }}</a>
                    {!! count($lv2) > 0 ? '<i class="fa fa-angle-down"></i>' : '' !!}
                    @if(count($lv2) > 0)
                        <ul class="dropdown-menu">
                            @foreach($lv2 as $lv2_item)
                                <li class="dropdown-submenu nav-item">
                                    <a class="nav-link"
                                       href="{{ URL::to('category/' .  $lv2_item->slug) }}">{{ $lv2_item->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                @endif
            </li>
        @endforeach
    </ul>
</section>