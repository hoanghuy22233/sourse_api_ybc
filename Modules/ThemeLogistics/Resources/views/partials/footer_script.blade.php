
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ URL::asset('public/frontend/themes/themelogistic/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('public/frontend/themes/themelogistic/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/frontend/themes/themelogistic/vendor/select2.full.min.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/themelogistic/vendor/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/themelogistic/vendor/bootstrap-select.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('public/frontend/themes/themelogistic/vendor/jquery.lazyload.min.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/themelogistic/vendor/fastclick.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('public/frontend/themes/themelogistic/vendor/timber.min.js') }}" type="text/javascript"></script>
<script type='text/javascript' src="{{ URL::asset('public/frontend/themes/themelogistic/vendor/slick.min.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/themelogistic/vendor/main_home.min8f26.js?v=55') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script src="{{ URL::asset('public/frontend/themes/themelogistic/js/owl.carousel.min.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/themelogistic/js/isotope.pkgd.min.js') }}"></script>
<script src="{{ URL::asset('public/frontend/themes/themelogistic/js/script.js') }}"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

@yield('script')
<script type="text/javascript">
    // $('.select2').select2();
    $('.selectpicker').selectpicker();
    var swiper = new Swiper('.swiper-container', {
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 'auto',
        initialSlide: 2,
        loop: true,
        autoplay: {
            delay: 10000,
            disableOnInteraction: false,
        },
        coverflowEffect: {
            rotate: 0,
            stretch: 0,
            depth: 700,
            modifier: 1,
            slideShadows: true,
        },
    });

</script>
<script>
    document.querySelector('.mini-photo-wrapper').addEventListener('click', function() {
        document.querySelector('.menu-container').classList.toggle('active');
    });
    $('#order-status-modal').on('show.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        var name = button.data('name');
        console.log(name);
    })
</script>
<script>
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }


    $(document).ready(function () {
        $(".subscribe_to_newsletter-btn").click(function () {
            var email = $('.subscribe_to_newsletter_email').val();

            if (email.length == 0) {
                alert('Bạn chưa nhập vào email!');
            }
            else {
                if (!isEmail(email)) {
                    alert('Email nhập sai!');
                }
            }
        });
    });
</script>
{{--Lazy load --}}
<script>
    setTimeout(function(){
        !function (e) {
            document.createElement("style").innerHTML = "img:not([src]) {visibility: hidden;}";

            function t(e, t) {
                var n = new Image, r = e.getAttribute("data-src");
                n.onload = function () {
                    e.parent ? e.parent.replaceChild(n, e) : e.src = r, e.style.opacity = "1", t && t()
                }, n.src = r
            }

            for (var n = new Array, r = function (e, t) {
                if (document.querySelectorAll) t = document.querySelectorAll(e); else {
                    var n = document, r = n.styleSheets[0] || n.createStyleSheet();
                    r.addRule(e, "f:b");
                    for (var i = n.all, l = 0, c = [], o = i.length; l < o; l++) i[l].currentStyle.f && c.push(i[l]);
                    r.removeRule(0), t = c
                }
                return t
            }("img.lazy"), i = function () {
                for (var r = 0; r < n.length; r++) i = n[r], l = void 0, (l = i.getBoundingClientRect()).top >= 0 && l.left >= 0 && l.top <= (e.innerHeight || document.documentElement.clientHeight) && t(n[r], function () {
                    n.splice(r, r)
                });
                var i, l
            }, l = 0; l < r.length; l++) n.push(r[l]);
            i(), function (t, n) {
                e.addEventListener ? this.addEventListener(t, n, !1) : e.attachEvent ? this.attachEvent("on" + t, n) : this["on" + t] = n
            }("scroll", i)
        }(this);
    }, 20);

</script>

{!! @$settings['frontend_footer_code'] !!}