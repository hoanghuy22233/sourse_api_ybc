<?php
$widgets = CommonHelper::getFromCache('widgets_home_address_top_header', ['widgets']);
if (!$widgets) {
    $widgets = \Modules\ThemeLogistics\Models\Widget::where('location', 'address_top_header')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
    CommonHelper::putToCache('widgets_home_address_top_header', $widgets, ['widgets']);
}
$timeopen = CommonHelper::getFromCache('widgets_home_time_top_header', ['widgets']);
if (!$timeopen) {
    $timeopen = \Modules\ThemeLogistics\Models\Widget::where('location', 'time_top_header')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
    CommonHelper::putToCache('widgets_home_time_top_header', $widgets, ['widgets']);
}

$tongdai = CommonHelper::getFromCache('widgets_home_tongdai', ['widgets']);
if (!$tongdai) {
    $tongdai = \Modules\ThemeLogistics\Models\Widget::where('location', 'tongdai')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
    CommonHelper::putToCache('widgets_home_tongdai', $widgets, ['widgets']);
}
?>
<div class="w-100 top-header font-size-14 pt-2 pb-2 d-none d-lg-block">
    <div class="container">
        <div class="row d-flex align-content-center">
            <div class="col-lg-7 col-sm-7 col-12">

                @foreach($widgets as $v)
                    {!! @$v->content !!}
                @endforeach

                <span class="border-left pl-2">Giờ làm việc:
                    @foreach($timeopen as $v)
                        {!! @$v->content !!}
                    @endforeach
                </span></div>
            <div class="col-lg-5 col-sm-5 col-12 text-right">
                <a href="http://logistics2.webhobasoft.com/admin/vao-shop" title="Giới thiệu về Etal Group">Về Etal
                    Group</a>
                <!--                    <a class="border-left pl-2 ml-1"-->
                <!--                       href="https://online.ntlogistics.vn/auth/login?url=https://ntlogistics.vn/" title="Đăng nhập">Đăng-->
                <!--                        nhập</a>-->
                <a class="border-left pl-2"><i class="ico ico-phone">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -45 512.00132 512" width="25px">
                            <path d="m479.730469 0h-48.140625c-4.1875 0-7.585938 3.398438-7.585938 7.585938 0 4.191406 3.398438 7.589843 7.585938 7.589843h48.140625c9.425781 0 17.09375 7.667969 17.09375 17.09375v115.804688c0 9.425781-7.667969 17.09375-17.09375 17.09375h-177.417969c-2.011719 0-3.941406.800781-5.367188 2.222656l-43.699218 43.699219v-38.332032c0-4.191406-3.394532-7.589843-7.585938-7.589843h-14.449218c-9.425782 0-17.09375-7.667969-17.09375-17.09375v-115.804688c0-9.425781 7.667968-17.09375 17.09375-17.09375h175.085937c4.191406 0 7.589844-3.398437 7.589844-7.589843 0-4.1875-3.398438-7.585938-7.589844-7.585938h-175.085937c-17.792969 0-32.269532 14.476562-32.269532 32.269531v115.804688c0 17.792969 14.476563 32.269531 32.269532 32.269531h6.863281v49.0625c0 3.070312 1.847656 5.835938 4.683593 7.011719.9375.386719 1.921876.578125 2.902344.578125 1.972656 0 3.914063-.773438 5.367188-2.222656l54.425781-54.429688h174.277344c17.792969 0 32.269531-14.476562 32.269531-32.269531v-115.804688c0-17.792969-14.476562-32.269531-32.269531-32.269531zm0 0"></path>
                            <path d="m274.285156 97.324219c8.996094-6.15625 20.195313-13.824219 20.195313-26.933594 0-6.21875-2.519531-11.519531-7.28125-15.328125-4.210938-3.371094-10.015625-5.304688-15.925781-5.304688-10.914063 0-22.511719 6.710938-22.511719 19.144532 0 5.484375 2.308593 8.152344 7.058593 8.152344 5.359376 0 8.25-2.976563 8.25-5.773438 0-4.640625 2.660157-7.304688 7.300782-7.304688 5.464844 0 7.402344 3.722657 7.402344 6.90625 0 5.894532-6.949219 10.996094-14.308594 16.398438-8.304688 6.09375-16.890625 12.398438-16.890625 20.648438v9.609374c0 2.980469 4.0625 4.882813 6.859375 4.882813h35.265625c2.546875 0 4.78125-3.253906 4.78125-6.964844 0-3.601562-2.234375-6.761719-4.78125-6.761719h-25.828125v-.765624c0-3.476563 4.828125-6.777344 10.414062-10.605469zm0 0"></path>
                            <path d="m336.453125 122.417969c5.394531 0 7.851563-2.527344 7.851563-4.878907v-10.375h4.132812c2.464844 0 4.882812-3.59375 4.882812-7.257812 0-4.988281-2.53125-7.257812-4.882812-7.257812h-4.132812v-6.808594c0-2.203125-2.054688-4.78125-7.851563-4.78125s-7.851563 2.574218-7.851563 4.78125v6.808594h-8.1875l17.441407-34.988282c.265625-.613281.507812-1.269531.507812-1.933594 0-1.617187-1.035156-3.167968-2.917969-4.367187-1.535156-.972656-3.46875-1.601563-4.933593-1.601563-2.882813 0-5.207031 1.429688-6.550781 4.03125l-22.882813 44.867188c-.703125 1.289062-.808594 2.453125-.808594 3.03125 0 3.277344 2.125 5.476562 5.28125 5.476562h23.050781v10.371094c0 2.355469 2.457032 4.882813 7.851563 4.882813zm0 0"></path>
                            <path d="m395.671875 48.246094-34.765625 71.414062c-.324219.648438-.511719 1.351563-.511719 1.9375 0 2.597656 2.28125 5.378906 5.675781 5.378906 1.992188 0 3.871094-1.050781 4.570313-2.546874l34.867187-71.417969c.339844-.679688.410157-1.429688.410157-1.9375 0-3.21875-3.085938-5.378907-5.972657-5.378907-1.953124
0-3.511718.929688-4.273437 2.550782zm0 0"></path>
                            <path d="m422.234375 74.875c4.3125 0 7.953125-2.234375 7.953125-4.882812v-6.511719h17.476562v6.773437l-24.351562 42.171875c-.488281.882813-.707031 1.695313-.707031 2.636719 0 4.683594 5.234375 7.949219 9.933593 7.949219 2.234376 0 3.699219-.699219 4.46875-2.148438l23.179688-44.375c.746094-1.398437 3.183594-6.222656 3.183594-9.46875v-12.382812c0-2.128907-3.328125-4.878907-6.863282-4.878907h-35.164062c-3.460938 0-6.863281 2.417969-6.863281 4.878907v15.355469c0 2.691406 3.480469 4.882812 7.753906 4.882812zm0 0"></path>
                            <path d="m313.769531 263.433594c-3.945312-3.945313-9.195312-6.117188-14.777343-6.117188-5.582032 0-10.832032 2.171875-14.777344 6.117188l-46.378906 46.378906c-18.46875-9.652344-34.535157-18.855469-52.488282-33.996094-3.203125-2.699218-7.988281-2.292968-10.691406.910156-2.699219 3.203126-2.292969 7.988282.910156 10.691407 20.734375 17.480469 38.78125 27.347656 60.21875 38.429687 2.933594 1.515625 6.511719.960938 8.847656-1.375l24.953126-24.949218 82.261718 82.261718c-26.570312 22.808594-64.199218 30.75-106.921875 22.351563-48.046875-9.449219-98.890625-39.03125-143.164062-83.304688-44.273438-44.273437-73.859375-95.121093-83.308594-143.167969-8.398437-42.726562-.457031-80.355468 22.347656-106.921874l82.273438 82.269531-24.960938 24.957031c-2.335937 2.335938-2.890625 5.917969-1.371093 8.851562 15.109374 29.214844 27.300781 50.652344 57.839843 81.195313 2.964844 2.960937 7.769531 2.960937 10.730469 0 2.964844-2.964844 2.964844-7.769531 0-10.730469-27.378906-27.378906-38.851562-46.339844-52.535156-72.515625l46.382812-46.386719c3.949219-3.945312 6.121094-9.195312 6.121094-14.777343s-2.171875-10.832031-6.121094-14.777344l-74.382812-74.382813c-3.945313-3.949218-9.195313-6.125-14.777344-6.125s-10.832031 2.175782-14.777344 6.121094l-19.996094 19.996094c-30.429687 30.429688-41.675781 75.234375-31.664062 126.152344 10.023438 50.972656 41.085938 104.589844 87.46875 150.972656 46.378906 46.378906 99.996094 77.441406 150.96875 87.460938 12.132812 2.386718 23.914062 3.566406 35.242188 3.5625 36.207031 0 67.730468-12.046876 90.910156-35.226563l19.992187-19.992187c3.949219-3.949219 6.121094-9.195313 6.121094-14.777344 0-5.582032-2.171875-10.832032-6.121094-14.777344zm-247.816406-218.261719c1.078125-1.078125 2.515625-1.675781 4.046875-1.675781s2.96875.59375 4.046875 1.675781l74.386719 74.386719c1.078125 1.078125 1.675781 2.515625 1.675781 4.046875s-.597656 2.96875-1.675781 4.046875l-14.628906 14.628906-82.480469-82.480469zm311.464844 311.464844-14.632813 14.628906-82.46875-82.472656 14.628906-14.628907c1.078126-1.082031 2.519532-1.675781 4.046876-1.675781 1.527343 0 2.964843.59375 4.046874 1.675781l74.378907 74.375c2.230469 2.234376 2.230469 5.867188 0 8.097657zm0 0"></path>
                        </svg>
                    </i>@foreach($tongdai as $v)
                        {!! @$v->content !!}
                    @endforeach</a>
                <!--<a class="border-left border-right pl-2 pr-2 ml-1 mr-2"-->
                <!--href="http://logistics2.webhobasoft.com/admin/vao-van-don"-->
                <!--style="border-right: 0px !important; padding-right: 0px !important">Kênh vận đơn </a>-->
                <div class="border-left pl-2 ml-1 d-inline-block">
                    <form>
                        <a href="index.html?locale=vi">
                            <img src="https://cdndev.ntlogistics.vn/images/vi.png" alt="" class="img-fluid">
                        </a>
                        <a href="en.html?locale=en" style="opacity: 0.6;">
                            <img src="https://cdndev.ntlogistics.vn/images/en.png" alt="" class="img-fluid">
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<header class="w-100 wp-header text-white py-3 pt-lg-0 pb-lg-0">
    <div class="container">
        <div class="row d-flex justify-content-between">
            <div class="col-lg-3 col-sm-4 col-4 align-self-center">
                <a href="/" title="Etal Group Logistic"><img class="img-fulid" width="300" height="60"
                                                             src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo'], null, null) }}"
                                                             alt="Etal Group Logistic"></a>
            </div>
            <div class="col-lg-6 col-sm-8 d-none d-lg-block align-self-end">
                <nav id="site-navigation" class="main-navigation">
                    <?php
                    $menus = CommonHelper::getFromCache('menu_main_menu', ['menus']);
                    if (!$menus) {
                        $menus = \Modules\ThemeLogistics\Models\Menu::where('status', 1)->where('location', 'main_menu')->whereNull('parent_id')->orderBy('order_no', 'desc')->orderBy('name', 'asc')->get();
                        CommonHelper::putToCache('menu_main_menu', $menus, ['menus']);
                    }
                    ?>
                    <ul id="main-menu" class="menu">
                        @foreach($menus as $menu)
                            <?php
                            $menu_childs = CommonHelper::getFromCache('menus_childs_' . $menu->id, ['menus']);
                            if (!$menu_childs) {
                                $menu_childs = $menu->childs;
                                CommonHelper::putToCache('menus_childs_' . $menu->id, $menu_childs, ['menus']);
                            }
                            ?>
                            <li @if(count($menu_childs) > 0)class="menu-item-has-children"@endif><a
                                        href="{{@$menu->url}}" title="{{@$menu->name}}">{{@$menu->name}}</a>
                                @if(count($menu_childs) > 0)
                                    <ul class="sub-menu">
                                        @foreach($menu_childs as $child)
                                            <li><a href="{{@$child->url}}"
                                                   title="{!! $child->name !!}">{!! $child->name !!}</a></li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </nav>
            </div>

            <ul class=" pt-2 pb-2 align-self-center text-center hotline-top font-size-14 d-none d-lg-block">
                @if(@\Auth::guard('admin')->check())
                    {{--                     <li class="w-100 d-block">--}}
                    {{--                    <a href="{{route('user.logout')}}" style="color:black;border-radius: 5px;" class="w-100 p-2 justify-content-center align-items-center" >--}}
                    {{--                      Đăng xuất--}}
                    {{--                    </a></li>--}}
                    <div class="user-menu-wrap">

                        <a class="mini-photo-wrapper" href="#" style="color: #fdd800;">
                            {{@\Auth::guard('admin')->user()->name}} <i class="fa fa-caret-down ml-2"
                                                                        style="color: white;"></i>
                        </a>


                        <div class="menu-container">
                            <ul class="user-menu">
                                {{--                                <div class="profile-highlight">--}}
                                {{--                                    <img src="https://images.unsplash.com/photo-1578976563986-fb8769ab695e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=934&q=80" alt="profile-img" width=36 height=36>--}}
                                {{--                                    <div class="details">--}}
                                {{--                                        <div id="profile-name">{{@\Auth::guard('admin')->user()->nameaccount}}</div>--}}
                                {{--                                        <div id="profile-footer">Team Hallaway</div>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                                <li class="user-menu__item">
                                    <a class="user-menu-link" href="#">
                                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1604623/trophy.png"
                                             alt="trophy_icon" width=20 height=20>
                                        <div>Thông báo</div>
                                    </a>
                                </li>
                                <li class="user-menu__item">
                                    <a class="user-menu-link" href="#">
                                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1604623/trophy.png"
                                             alt="trophy_icon" width=20 height=20>
                                        <div>Tài khoản của tôi</div>
                                    </a>
                                </li>
                                <li class="user-menu__item">
                                    <a class="user-menu-link" href="https://etalgroup.net/admin/dashboard">
                                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1604623/team.png"
                                             alt="team_icon" width=20 height=20>
                                        <div>Đơn hàng</div>
                                    </a>
                                </li>
                                {{--                                <li class="user-menu__item">--}}
                                {{--                                    <a class="user-menu-link" href="#">--}}
                                {{--                                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1604623/book.png" alt="team_icon" width=20 height=20 >--}}
                                {{--                                        <div>Shop của tôi</div>--}}
                                {{--                                    </a>--}}
                                {{--                                </li>--}}
                                <div class="footer">
                                    <li class="user-menu__item"><a class="user-menu-link" href="/dang-xuat">Đăng
                                            xuất</a></li>
                                    {{--                                    <li class="user-menu__item"><a class="user-menu-link" href="#">Cài đặt</a></li>--}}
                                </div>
                            </ul>
                        </div>
                    </div>
                @else
                    <li class="w-100 d-block">
                        <a href="{{route('user.login')}}" style="color:black;border-radius: 5px;"
                           class="w-100 pt-2 pb-2 pl-5 pr-5 justify-content-center align-items-center button_dangky">
                            {{--                        <img src="{{URL::asset('public/frontend/themes/themelogistic/images/img_avatar.png')}}" alt="Avatar" class="avatar"><span style="color: white;">Admin</span>--}}
                            Đăng ký/Đăng nhập
                        </a></li>
                @endif
                <li class="w-100 d-block">

                </li>

            </ul>
            <ul class="col-lg-3 col-sm-8 col-8 text-right d-lg-none">
                <li class="d-inline-block mr-2">
                    <a class="text-fff" href="#"><span
                                class="ntl-Login fs1"></span></a>
                </li>
                <li class="d-inline-block">
                    <button type="button"
                            class="js-drawer-open-left ntl-menu d-inline-block border-0 text-center text-white"
                            aria-controls="NavDrawer" aria-expanded="false">
                        <i class="fs1 ntl-Hamburger-Menu"></i>
                    </button>
                </li>
            </ul>
        </div>
    </div>
</header>
<style>
    /** {*/
    /*    box-sizing: border-box;*/
    /*    margin: 0;*/
    /*    padding: 0;*/
    /*}*/
    /*body {*/
    /*    background-color: #fff;*/
    /*    font-family: sans-serif;*/
    /*}*/
    /*ul {*/
    /*    list-style: none;*/
    /*}*/
    .user-menu-wrap {
        position: relative;

    }

    .menu-container {
        visibility: hidden;
        opacity: 0;
        position: absolute;
        right: -50%;
        top: 100%;
        z-index: 999;
        transform: translateX(19%);
        background-color: #fff;
        min-width: 256px;
        border-radius: 2px;
        border: 1px solid rgba(0, 0, 0, 0.15);
        padding-top: 5px;
        padding-bottom: 5px;
        margin-top: 20px;
    }

    .menu-container.active {
        visibility: visible;
        opacity: 1;
        transition: all 0.2s ease-in-out;
    }

    .user-menu {
        /*position: absolute;*/
        /*right: -22px;*/
        /*background-color: #fff;*/
        /*width: 256px;*/
        /*border-radius: 2px;*/
        /*border: 1px solid rgba(0, 0, 0, 0.15);*/
        /*padding-top: 5px;*/
        /*padding-bottom: 5px;*/
        /*margin-top: 20px;*/
    }

    .user-menu .profile-highlight {
        display: flex;
        border-bottom: 1px solid #e0e0e0;
        padding: 12px 16px;
        margin-bottom: 6px;
    }

    .user-menu .profile-highlight img {
        width: 48px;
        height: 48px;
        border-radius: 25px;
        object-fit: cover;
    }

    .user-menu .profile-highlight .details {
        display: flex;
        flex-direction: column;
        margin: auto 12px;
    }

    .user-menu .profile-highlight .details #profile-name {
        font-weight: 600;
        font-size: 16px;
    }

    .user-menu .profile-highlight .details #profile-footer {
        font-weight: 300;
        font-size: 14px;
        margin-top: 4px;
    }

    .user-menu .footer {
        border-top: 1px solid #e0e0e0;
        padding-top: 6px;
        margin-top: 6px;
    }

    .user-menu .footer .user-menu-link {
        font-size: 13px;
    }

    .user-menu .user-menu-link {
        display: flex;
        text-decoration: none;
        color: #333;
        font-weight: 400;
        font-size: 14px;
        padding: 12px 16px;
    }

    .user-menu .user-menu-link div {
        margin: auto 10px;
    }

    .user-menu .user-menu-link:hover {
        background-color: #f5f5f5;
        color: #333;
    }

    .user-menu:before {
        position: absolute;
        top: -16px;
        left: 120px;
        display: inline-block;
        content: "";
        border: 8px solid tran . user-menu-wrapsparent;
        border-bottom-color: #e0e0e0;
    }

    .user-menu:after {
        position: absolute;
        top: -14px;
        left: 121px;
        display: inline-block;
        content: "";
        border: 7px solid transparent;
        border-bottom-color: #fff;
    }

</style>
