
<!--Slide-->

    <?php
    $banners = CommonHelper::getFromCache('get_banners_by_slide');
    if (!$banners) {
        $banners = \Modules\ThemeLogistics\Models\Banner::where('status', 1)->where('location', 'banners_home')->orderBy('order_no', 'ASC')->first();
        CommonHelper::putToCache('get_banners_by_slide', $banners);
    }
    ?>



<div class="w-100 position-relative slide-v2">
    <!-- banner pc -->
    <div class="banner-pc  d-none d-md-block"
         style=" background:url(https://cdn.ntlogistics.vn/uploads/banners/2020/11/03/hanh-trinh-chuyen-cho-yeu-thuong-2.jpg);height: 80vh;background-size: cover;"></div>
    <!-- end banner pc -->
    <!-- banner mobile -->

    <div class="banner-pc d-md-none"
         style=" background:url(https://cdn.ntlogistics.vn/uploads/banners/2020/11/03/hanh-trinh-chuyen-cho-yeu-thuong-2.jpg);height: 80vh;background-size: cover;"></div>
    <!-- end banner mobile -->

    <div class="col-lg-6 col-md-12 col-sm-12 col-12 offset-lg-3 wp-slide flex-container center "
         style="height: auto;">
        <div class="mt-md-n5 mt-n3 more-than-log">
            <!-- <img src="images/MoreThanLogistic.png" alt="" class="img-fluid"> -->
            <!-- More Than Logistics -->
            <!-- Hơn cả một dịch vụ -->
        </div>
        <div class="d-flex justify-content-center align-items-center" style="width: 350px; margin: 80px auto; transform: scale(2.2);">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" style="width: 160px;">
                        <span class="mx-auto d-block top-slide"></span>
                        <div class="box-cube">
                            <a href="tra-cuoc.html">
                                <span class="icon-slide font-size-30 ntl-Document-2">
                                    <span class="path1"></span><span id="ntl-Document-2" class="path2"></span><span
                                            class="path3"></span><span class="path4"></span><span class="path5"></span><span
                                            class="path6"></span><span class="path7"></span><span class="path8"></span><span
                                            class="path9"></span><span class="path10"></span><span class="path11"></span><span
                                            class="path12"></span><span class="path13"></span><span class="path14"></span></span>
                                <p class="mt-1 mt-sm-2 mb-1 mb-sm-3 font-size-14">Ecommerce</p>
                                <span class="d-block line"></span>
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 160px;">
                        <span class="mx-auto d-block top-slide"></span>
                        <div class="box-cube">
                            <a href="tra-van-don.html">
                                    <span class="icon-slide font-size-30 ntl-Box-1"><span id="ntl-Box-1"
                                                                                          class="path1"></span><span
                                                class="path2"></span><span class="path3"></span><span class="path4"></span><span
                                                class="path5"></span></span>
                                <p class="mt-2 font-size-14">Nội địa Việt Nam</p>
                                <span class="d-block line"></span>
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 160px;">
                        <span class="mx-auto d-block top-slide"></span>
                        <div class="box-cube">
                            <a href="danh-sach-buu-cuc.html">
                                    <span class="icon-slide font-size-30 ntl-Location-2"><span id="ntl-Location-2"
                                                                                               class="path1"></span><span
                                                id="ntl-Location-22" class="path2"></span><span class="path3"></span><span
                                                class="path4"></span><span class="path5"></span></span>
                                <p class="mt-1 mt-sm-2 mb-1 mb-sm-3 font-size-14">Quốc tế</p>
                                <span class="d-block line"></span>
                            </a>
                        </div>
                    </div>
                    <div class="swiper-slide" style="width: 160px;">
                        <span class="mx-auto d-block top-slide"></span>
                        <div class="box-cube">
                            <a href="danh-sach-buu-cuc.html">
                                    <span class="icon-slide font-size-30 ntl-Location-2"><span id="ntl-Location-2"
                                                                                               class="path1"></span><span
                                                id="ntl-Location-22" class="path2"></span><span class="path3"></span><span
                                                class="path4"></span><span class="path5"></span></span>
                                <p class="mt-1 mt-sm-2 mb-1 mb-sm-3 font-size-14">Nội địa Đài Loan</p>
                                <span class="d-block line"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="w-100 d-inline-block pl-1 pr-1">
            <form class="row position-relative search mt-1 mt-md-3" action="#"
                  method="post" role="search" id="tracking_top">
                {{ csrf_field() }}
                <div class="col-lg-10 col-sm-10 col-9 pr-0 pr-sm-2">
                    <input type="text" name="code" id="bill" value="" placeholder="Nhập mã vận đơn"
                           class="form-control rounded-0 border-0 tukhoa" autocomplete="off">
                </div>
                <div class="col-lg-2 col-sm-2 col-3 pl-0">
                    @if(@\Auth::guard('admin')->check())
                    <button class="btn w-100 bg-FCD804 rounded-0 pl-1 pr-1" data-name="code" id="btn-tracking">Tra cứu</button>
                        @else
                        <button type="button" class="btn w-100 bg-FCD804 rounded-0 pl-1 pr-1" data-toggle="modal" data-target="#login-modal">Tra cứu</button>
                        @endif
                </div>
                <span class="error col-lg-12" id="error_bill" style="display:none">Nhập mã vận đơn</span>
            </form>
        </div>
        <div class="row text-dark-min mx-auto mt-1 mt-md-3">
            <div class="col-lg-9 col-sm-8 col-12">Nhập tối đa 30 mã vận đơn, mỗi mã cách nhau dấu phẩy<br>VD:
                392773302,968835288
            </div>
        </div>
    </div>
</div>

