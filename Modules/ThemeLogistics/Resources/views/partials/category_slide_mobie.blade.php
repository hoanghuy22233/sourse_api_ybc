
<div class="dbox f">

    @if($category->count() > 0)
        <div class="ss" id="SPTT1">
            <span class="psback"></span>
            <div class="pspanel" style="width: 90%!important;">
                <div class="pswrap">
                    @foreach( $category as $productInMultiCat)

                        <a class="psitem"
                           href="{{route('cate.list', ['slug' => $productInMultiCat->slug])}}"
                           title="{{$productInMultiCat->name}}">

                            <div class="pi">
                                <img alt="{{@$productInMultiCat->name}}"
                                     class="lazy"
                                     src="{{CommonHelper::getUrlImageThumb(@$productInMultiCat->image, 160, 'auto')}}"
                                     data-src="{{@$productInMultiCat->image}}"/>
                            </div>
                            <p class="pn">{{@$productInMultiCat->name}}</p>

                        </a>
                    @endforeach

                </div>
            </div>
            <span class="psnext"></span>
        </div>
    @endif
</div>
<script>
    $(document).ready(function () {
        PSLide0('#SPTT1', 1, true, false, 10);
        $('.faqtab li').click(function () {
            var id = $(this).attr('data-rel');

            $('.faqtab .faqcur').removeClass('faqcur');
            $(this).addClass('faqcur');

            $('.fcw .show').removeClass('show');
            $('#' + id).addClass('show');
        });
        PSLide0('#SPTT', 6, true, false, 10);

        function PSLide0(Panel, ItemPer, isBoxImg, isLoop, margin, Step) {
            var ni = $(Panel + ' .psitem').length;
            if (ni <= ItemPer) {
                $(Panel + ' .psnext').hide();
                $(Panel + ' .psback').hide();
                $(Panel + ' .pspanel').css('width', '100%');
            }

            //
            var PanelWidth = $(Panel + ' .pspanel').width();
            var WidthItem = PanelWidth / ItemPer - margin;
            var StartPosition = (WidthItem + margin) * (-1);
            var Maxstep = $(Panel + ' .psitem').length - ItemPer;
            //css
            var item = Panel + ' .psitem';
            $(Panel + ' .psitem').each(function () {
                $(this).css('width', WidthItem + 'px');
            });

            if (Step > 0) {
                var NPosition = Step * StartPosition;
                $(Panel + ' .pswrap').animate({left: '' + NPosition + 'px', direction: "left"});
            } else Step = 0;

            $(Panel + ' .psnext').click(function () {
                loadImg();
                Step = Step < Maxstep ? Step + 1 : 0;
                var NewPosition = Step * StartPosition;
                $(Panel + ' .pswrap').animate({left: '' + NewPosition + 'px', direction: "left"});
                if (Panel == '#ProImg') forceChildSlide();
            });
            $(Panel + ' .psback').click(function () {
                loadImg();
                Step = Step > 0 ? Step - 1 : Maxstep;
                var NewPosition = Step * StartPosition;
                $(Panel + ' .pswrap').animate({left: '' + NewPosition + 'px', direction: "left"});
                if (Panel == '#ProImg') forceChildSlide();
            });

            //danh cho slide anh dai dien san pham chi tiet
            $('#listimg .psitem').click(function () {
                if (Panel == '#ProImg') {
                    var newstep = $(this).attr('id').replace('thum', '');
                    $('#listimg .psitem').removeClass('psselect');
                    $('#thum' + newstep).addClass('psselect');
                    forceParentSlide(newstep);
                }
            });

            function forceParentSlide(newstep) {
                loadImg();
                Step = parseInt(newstep);
                var NewPosition = Step * StartPosition;
                $(Panel + ' .pswrap').animate({left: '' + NewPosition + 'px', direction: "left"});
            }

            function forceChildSlide() {
                $('#listimg .psitem').removeClass('psselect');
                $('#thum' + Step).addClass('psselect');
                //var childstep = parseInt(Step / 5);
                //if(isnext)
                //    childstep += childstep > 0 && parseInt(Step % 5) > 0 ? 1 : 0;
                //else childstep -= childstep > 0 && parseInt(Step % 5) > 0 ? 1 : 0;
                //alert(Step +' , '+childstep);
                //PSLide0('#listimg', 5, true, false, 10,childstep);

            }

            //
            if (isLoop) {
                setInterval(function () {
                    loadImg();
                    Step = Step < Maxstep ? Step + 1 : 0;
                    var NewPosition = Step * StartPosition;
                    $(Panel + ' .pswrap').animate({left: '' + NewPosition + 'px', direction: "left"});
                }, 3000);
            }
        }
    });
</script>