<style>
    @media(max-width: 768px) {
        .phone_now_detail {
            max-width: 268px;
            font-size: 11px;
        }
    }
</style>
<div class="phone_now_detail">
    <?php
    $dataShowrooms = CommonHelper::getFromCache('get_showroom_footer');
    if (!$dataShowrooms) {
        $dataShowrooms = \Modules\ThemeLogistics\Models\Showroom::all();
        CommonHelper::putToCache('get_showroom_footer', $dataShowrooms);
    }
    ?>
    @foreach($dataShowrooms as $k=>$dataShowroom)
        <div class="phone_now_detail1">
            <div class="contact_now-title">
                <span style="line-height: 14px;">{!! $dataShowroom->address !!}</span>
                <a href="tel:{{ preg_replace('/\s+/', '', $dataShowroom->hotline) }}" rel="nofollow">
                    <img data-src="https://i.imgur.com/fmhOHUr.gif" width="30px" class="lazy">
                </a>
            </div>

        </div>
    @endforeach
    Hotline: {{ @$settings['hotline'] }}
</div>