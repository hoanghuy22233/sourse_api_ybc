@if (isset($prd_gifts) && count($prd_gifts)>0)
    <div class="qua-content">
        {{--    <span class="gift_content">--}}
        @php  $totalPrice = 0; @endphp
        {{--        <ul>--}}
        @foreach($prd_gifts as $data)
            <p style=" margin: 0 10px ">
                - {{@$data['name']}}
            </p>
            @php $totalPrice += $data['base_price'];  @endphp
        @endforeach
        <?php
        $prd_category_id = \Modules\ThemeLogistics\Models\Product::find($cart['id'])->category->gift_value_max;
        if ($totalPrice > $prd_category_id) {
            $totalPrice -= $prd_category_id;
        }
        ?>
        {{--        </ul>--}}
        @if ($totalPrice>0)
            GHI CHÚ: Số Tiền Bạn Phải Bù Thêm: <b>{{number_format($totalPrice, 0, '.', '.')}} <sup>đ</sup></b>
        @endif

        {{--    </span>--}}
    </div>
@else
    - Không chọn quà
@endif
