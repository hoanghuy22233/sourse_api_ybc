<ul class="mobile-nav">
    <?php
    $menus = CommonHelper::getFromCache('menu_main_menu', ['menus']);
    if (!$menus) {
        $menus = \Modules\ThemeLogistics\Models\Menu::where('status', 1)->where('location', 'main_menu')->whereNull('parent_id')->orderBy('order_no', 'desc')->orderBy('name', 'asc')->get();
        CommonHelper::putToCache('menu_main_menu', $menus, ['menus']);
    }
    ?>
    @foreach($menus as $menu)
        <?php
        $menu_childs = CommonHelper::getFromCache('menus_childs_' . $menu->id, ['menus']);
        if (!$menu_childs) {
            $menu_childs = $menu->childs;
            CommonHelper::putToCache('menus_childs_' . $menu->id, $menu_childs, ['menus']);
        }
        ?>
        <li class="mobile-nav__item" aria-haspopup="true">
            <div class="mobile-nav__has-sublist">
                <a href="#" class="mobile-nav__link">{{@$menu->name}}</a>
                @if(count($menu_childs) > 0)
                    <div class="mobile-nav__toggle">
                        <button type="button" class="icon-fallback-text mobile-nav__toggle-open">
                            <span class="ntl-Black-Arrow-1"></span>
                        </button>
                        <button type="button" class="icon-fallback-text mobile-nav__toggle-close">
                            <span class="ntl-Black-Arrow-3"></span>
                            <span class="fallback-text"></span>
                        </button>
                    </div>
                @endif
            </div>
            @if(count($menu_childs) > 0)
                <ul class="mobile-nav__sublist">
                    @foreach($menu_childs as $child)
                        <li class="mobile-nav__item"><a href="{{@$child->url}}" title="{!! $child->name !!}"
                                                        class="mobile-nav__link">{!! $child->name !!}</a></li>
                    @endforeach
                </ul>
            @endif
        </li>
    @endforeach
    {{--<li class="mobile-nav__item" aria-haspopup="true">
        <div class="mobile-nav__has-sublist">
            <a href="#" class="mobile-nav__link">Dịch vụ</a>
            <div class="mobile-nav__toggle">
                <button type="button" class="icon-fallback-text mobile-nav__toggle-open">
                    <span class="ntl-Black-Arrow-1"></span>
                </button>
                <button type="button" class="icon-fallback-text mobile-nav__toggle-close">
                    <span class="ntl-Black-Arrow-3"></span>
                    <span class="fallback-text"></span>
                </button>
            </div>
        </div>
        <ul class="mobile-nav__sublist">
            <li class="mobile-nav__item"><a href="/dich-vu/van-chuyen-noi-dia-viet-nam.html"
                                            title="Vận chuyển nội địa Viêt Nam" class="mobile-nav__link">Vận chuyển nđịa
                    Viêt Nam</a></li>
            <li class="mobile-nav__item"><a href="/dich-vu/van-chuyen-quoc-te.html"
                                            title="Chuyển phát hỏa tốc" class="mobile-nav__link">Vận chuyển quốc tế</a>
            </li>
            <li class="mobile-nav__item"><a href="/dich-vu/van-chuyen-noi-dia-dai-loan.html"
                                            title="Chuyển phát hỏa tốc" class="mobile-nav__link">Vận chuyển nội địa Đài
                    Loan</a></li>
        </ul>
    </li>
    <li class="mobile-nav__item" aria-haspopup="true">
        <div class="mobile-nav__has-sublist">
            <a href="#" class="mobile-nav__link">Giới thiệu </a>
            <div class="mobile-nav__toggle">
                <button type="button" class="icon-fallback-text mobile-nav__toggle-open">
                    <span class="ntl-Black-Arrow-1"></span>
                </button>
                <button type="button" class="icon-fallback-text mobile-nav__toggle-close">
                    <span class="ntl-Black-Arrow-3"></span>
                    <span class="fallback-text"></span>
                </button>
            </div>
        </div>
        <ul class="mobile-nav__sublist">
            <li class="mobile-nav__item"><a href="#" title="Tin chuyên ngành"
                                            class="mobile-nav__link">Về Etal Group</a></li>
            <li class="mobile-nav__item"><a href="#" title="Tin hoạt động"
                                            class="mobile-nav__link">Giá trị cốt lõi</a></li>
            <li class="mobile-nav__item"><a href="#" title="Tin tuyển dụng"
                                            class="mobile-nav__link">Tin tức</a></li>
        </ul>
    </li>
    <li class="mobile-nav__item" aria-haspopup="true">
        <div class="mobile-nav__has-sublist">
            <a href="#" class="mobile-nav__link">Giới thiệu</a>
            <div class="mobile-nav__toggle">
                <button type="button" class="icon-fallback-text mobile-nav__toggle-open">
                    <span class="ntl-Black-Arrow-1"></span>
                </button>
                <button type="button" class="icon-fallback-text mobile-nav__toggle-close">
                    <span class="ntl-Black-Arrow-3"></span>
                    <span class="fallback-text"></span>
                </button>
            </div>
        </div>
        <ul class="mobile-nav__sublist">
            <li class="mobile-nav__item"><a href="tin-tuc/gioi-thieu-chung/index.html" title="Về Etal Group"
                                            class="mobile-nav__link">Về Etal Group</a></li>
            <li class="mobile-nav__item"><a href="tin-tuc/nhan-su-chung-toi/index.html" title="Nhân sự NTL"
                                            class="mobile-nav__link">Nhân sự NTL</a></li>
            <li class="mobile-nav__item"><a href="tin-tuc/trach-nhiem/index.html" title="Giá trị cốt lõi"
                                            class="mobile-nav__link">Giá trị cốt lõi</a></li>
        </ul>
    </li>
    <li class="mobile-nav__item"><a class="mobile-nav__link mobile-nav__border" href="tin-tuc/lien-he/index.html"
                                    title="Liên hệ">Liên hệ</a></li>


    <li class="mobile-nav__item" style="display:block">
        <div class="mobile-nav__link font-IntelBold d-flex justify-content-between align-items-center"
             style="padding:10px 0;">
            <form style="display: flex;">
                <a href="index.html?locale=vi">
                    <img src="https://cdndev.ntlogistics.vn/images/vi.png" alt="" class="img-fluid">
                </a>
                <a href="en.html?locale=en" style="opacity: 0.6;">
                    <img src="https://cdndev.ntlogistics.vn/images/en.png" alt="" class="img-fluid">
                </a>
            </form>
        </div>
    </li>--}}
</ul>