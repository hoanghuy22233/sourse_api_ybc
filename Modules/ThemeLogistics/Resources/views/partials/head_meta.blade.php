<title>@if(isset($pageOption)){{ isset($pageOption['title']) && $pageOption['title'] != '' ? $pageOption['title'] : @$pageOption['pageName'] }}@else{!! @$settings['default_meta_title'] !!}@endif</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
{{--<meta name="viewport" content="width=device-width, initial-scale=1.0">--}}
@if (isMobile())
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
@endif

<meta content="@if(isset($pageOption)){{ isset($pageOption['title']) && $pageOption['title'] != '' ? $pageOption['title'] : @$pageOption['pageName'] }}@else{!! @$settings['default_meta_title'] !!}@endif"
      name="title">
<meta content="@if(isset($pageOption)){{ isset($pageOption['keywords']) && $pageOption['keywords'] != '' ? $pageOption['keywords'] : @$pageOption['pageName'] }}@else{!! @$settings['default_meta_keywords'] !!}@endif"
      name="keywords">
<meta content="@if(isset($pageOption)){!! isset($pageOption['description']) && $pageOption['description'] != '' ? $pageOption['description'] : @$pageOption['pageName'] !!}@else{!! @$settings['default_meta_description'] !!}@endif"
      name="description">
<meta name="robots" content="@if(isset($pageOption)){{ isset($pageOption['robots']) && $pageOption['robots'] != '' ?  $pageOption['robots'] : @$settings['robots'] }}@else{{ @$settings['robots'] }}@endif" />
<link rel="shortcut icon"
      href="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['favicon'], 150, null) }}"
      type="image/x-icon">
<link rel="alternate" hreflang="vi" href="{{ URL::to('/') }}"/>
{{--@if(isset($canonical))--}}
{{--    <link rel="canonical" href="{{ asset($canonical) }}"/>--}}
{{--@endif--}}
<meta property="og:site_name" content="{{ URL::to('/') }}"/>
{{--<link rel="stylesheet"--}}
{{--      href="{{asset('public/frontend/themes/stbd/frontend/css/style-responsive.css')}}">--}}
<meta property="og:url"                content="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>" />
<meta property="og:type"               content="{{isset($post)?'article':'website'}}" />
{{--<meta property="og:title"              content="{{isset($post)?$post->name:@$product->name}}" />
<meta property="og:description"        content="{{isset($post)?$post->intro:@$product->intro}}" />
<meta property="og:image"              content="{{isset($post) ? URL::asset('public/filemanager/userfiles/' .$post->image) : URL::asset('public/filemanager/userfiles/' .@$product->image)}}" />--}}
<meta property="og:title"              content="@if(isset($pageOption)){{ isset($pageOption['title']) && $pageOption['title'] != '' ? $pageOption['title'] : @$pageOption['pageName'] }}@else{!! @$settings['default_meta_title'] !!}@endif" />
<meta property="og:description"        content="@if(isset($pageOption)){!! isset($pageOption['description']) && $pageOption['description'] != '' ? $pageOption['description'] : @$pageOption['pageName'] !!}@else{!! @$settings['default_meta_description'] !!}@endif" />
<meta property="og:image"              content="@if(isset($pageOption['image'])){{ $pageOption['image'] }}@else{{ URL::asset('public/filemanager/userfiles/' .@$settings['logo']) }}@endif" />
