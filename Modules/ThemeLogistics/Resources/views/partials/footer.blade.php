<?php
$data = CommonHelper::getFromCache('widget_footer', ['widgets']);
if (!$data) {
    $data = \Modules\ThemeLogistics\Models\Widget::select('name', 'content', 'location')->where('status', 1)->whereIn('location', ['footer_0', 'footer_1', 'footer_2', 'footer_3', 'copy_right'])
        ->orderBy('order_no', 'DESC')->orderBy('id', 'ASC')->get();
    CommonHelper::putToCache('widget_footer', $data, ['widgets']);
}

$footers = [];
foreach ($data as $v) {
    $footers[$v->location][] = $v;
}
//dd($footers);
?>
<footer class="footer">
    <div class="container">
        <div class="row">

            <div class="footer-item span-2">
{{--                <h4>CÔNG TY CỔ PHẦN DỊCH VỤ GIAO HÀNG NHANH</h4>--}}
{{--                <p>Công ty giao nhận đầu tiên tại Việt Nam được thành lập với sứ mệnh phục vụ nhu cầu vận chuyển chuyên nghiệp--}}
{{--                    của các đối tác Thương mại điện tử trên toàn quốc.--}}
{{--                    Giấy CNĐKDN: 0311 907 295 do Sở Kế Hoạch và Đầu Tư TP HCM cấp lần đầu ngày 02/08/2012, cấp thay đổi lần thứ--}}
{{--                    16 ngày 10/5/2019.</p>--}}
{{--                <div class="line"></div>--}}
{{--                <div class="page-info">--}}
{{--                    <span><img src="./pin.svg" alt=""></span>--}}
{{--                    <b>Trụ sợ chính</b>--}}
{{--                    <span>405/15 Xô Viết Nghệ Tĩnh, Phường 24, Quận Bình Thạnh, TP HCM</span>--}}
{{--                </div>--}}
{{--                <div class="page-info">--}}
{{--                    <span><img src="./email.svg" alt=""></span>--}}
{{--                    <b>Email</b>--}}
{{--                    <a href="#">cskh@ghn.vn</a>--}}
{{--                </div>--}}
{{--                <div class="page-info">--}}
{{--                    <span><img src="./phone-call.svg" alt=""></span>--}}
{{--                    <b>Hotline</b>--}}
{{--                    <a href="#">1800 6328</a>--}}
{{--                </div>--}}
                @if(isset($footers['footer_0']))
                    @foreach($footers['footer_0'] as $v)
                        {!! @$v->content !!}
                    @endforeach
                @endif
            </div>

            <div class="footer-item">
                @if(isset($footers['footer_1']))
                    @foreach($footers['footer_1'] as $v)
                        <h4 class="font-weight-bold">{{$v->name}}</h4>
                        {!! @$v->content !!}
                    @endforeach
                @endif

            </div>
            <div class="footer-item">
                @if(isset($footers['footer_2']))
                    @foreach($footers['footer_2'] as $v)
                        <h4 class="font-weight-bold">{{$v->name}}</h4>
                        {!! @$v->content !!}
                    @endforeach
                @endif
            </div>
            <div class="footer-item">
                @if(isset($footers['footer_3']))
                    @foreach($footers['footer_3'] as $v)
                        <h4 class="font-weight-bold">{{$v->name}}</h4>
                        {!! @$v->content !!}
                    @endforeach
                @endif
            </div>
            <div class="footer-item span-2 responsive">
                <h4>KẾT NỐI VỚI CHÚNG TÔI</h4>
                <div class="social-network">

                    <a href="#" class="fb"><i class="fab fa-facebook-f"></i></a>
                    <a href="#" class="yt"><i class="fab fa-youtube"></i></a>
                    <a href="#" class="ita"><i class="fab fa-instagram"></i></a>
                </div>
            </div>
            <div class="footer-item">
                <h4>HỆ THỐNG BƯU CỤC ETAL GROUP</h4>
                <a href="#" class="button">Tìm địa chỉ gần bạn</a>
            </div>
            <div class="footer-item span-2">
                <h4>ĐĂNG KÝ NHẬN TIN MỚI</h4>
                <form action="#">
                    <input type="text" class="subscribe_to_newsletter_email" placeholder="Vui lòng nhập email của bạn">
                    <button type="button" class="subscribe_to_newsletter-btn">Gửi</button>
                </form>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="footer-wrap">
                @if(isset($footers['copy_right']))
                    @foreach($footers['copy_right'] as $v)
                        {!! @$v->content !!}
                    @endforeach
                @endif
{{--                <img src="./bo-cong-thuong.svg" alt="">--}}
            </div>
        </div>
    </div>
</footer>
