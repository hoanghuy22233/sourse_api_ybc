<?php

namespace Modules\ThemeLogistics\Models;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
   protected $table = 'origins';
}
