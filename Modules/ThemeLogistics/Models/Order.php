<?php

namespace Modules\ThemeLogistics\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';

    protected $fillable = [
        'bill_id', 'price', 'product_id', 'quantity', 'product_name', 'product_price', 'product_image','gift_list'
    ];

    public function product() {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

}
