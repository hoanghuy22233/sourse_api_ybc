<?php

namespace Modules\ThemeLogistics\Models;

use Illuminate\Database\Eloquent\Model;

class Transports extends Model
{
    protected $table = 'transports';
    public function billing_province(){
        return $this->belongsTo(Province::class, 'billing_province_id', 'id');
    }
    public function billing_district(){
        return $this->belongsTo(District::class, 'billing_district_id', 'id');
    }

    public function shipping_province(){
        return $this->belongsTo(Province::class, 'shipping_province_id', 'id');
    }
    public function shipping_district(){
        return $this->belongsTo(District::class, 'shipping_district_id', 'id');
    }
    public function histories(){
        return $this->hasMany(HistoryTransport::class,'transport_id');
    }
    public function shipping_method(){
        return $this->hasOne(ShippingMethod::class,'id','transport_shipping_method_id');
    }
    public function statusObj(){
        return $this->belongsTo(StatusTransport::class, 'status_id', 'id');
    }
}
