<?php

namespace Modules\ThemeLogistics\Models;

use Illuminate\Database\Eloquent\Model;

class TransportType extends Model
{
    protected $table = 'transport_types';

}
