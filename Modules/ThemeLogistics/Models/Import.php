<?php

namespace Modules\ThemeLogistics\Models;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{

    protected $table = 'imports';
}
