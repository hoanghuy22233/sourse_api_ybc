<?php

Route::get('sitemap.xml', function () {
    return response()->view('themelogistics::sitemap.sitemap')->header('Content-Type', 'text/xml');
});
Route::get('post-sitemap.xml', function () {
    return response()->view('themelogistics::sitemap.post_sitemap')->header('Content-Type', 'text/xml');
});
Route::get('category-sitemap.xml', function () {
    return response()->view('themelogistics::sitemap.category_sitemap')->header('Content-Type', 'text/xml');
});
Route::get('product-sitemap.xml', function () {
    return response()->view('themelogistics::sitemap.product_sitemap')->header('Content-Type', 'text/xml');
});


Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions', 'locale']], function () {
    Route::group(['prefix' => 'theme'], function () {
        Route::match(['get', 'post'], 'setting', 'Admin\STBDThemeController@setting')->name('theme')->middleware('permission:theme');
    });
});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//  Xóa cache

//user can view it anytime with or without logged in
Route::group(['middleware' => ['locale']], function () {
    Route::get('signup', 'LoginController@signup');

});
Route::get('admin/browser', 'CkFinderController@browser')->name('browser');
Route::post('set_session', 'HomeController@set_session');

Route::post('contact', 'Frontend\HomeController@postContact');

Route::get('load-san-pham-html', 'Frontend\PostController@loadSanPhamHtml');
Route::get('load-san-pham-trong-san-pham-html', 'Frontend\ProductController@loadSanPhamTrongSanPhamHtml');
Route::get('/admin/register', function () {
    return redirect('/admin/login');
});

//  Ajax routing
Route::get('ajax-get-district', 'Frontend\HomeController@ajax_get_district')->name('ajax_get_district');
Route::get('ajax-get-product', 'Frontend\ProductController@ajax_get_product')->name('product.ajax_get_product');
Route::get('ajax-quick-view', 'Frontend\ProductController@ajax_quick_view')->name('product.ajax_quick_view');
Route::post('ajax-contact', 'Frontend\ContactController@ajax_contact')->name('contact.ajax_contact');
Route::get('order.refresh-cart-quick-view', 'Frontend\OrderController@refresh_cart_quick_view')->name('order.refresh_cart_quick_view');
Route::post('tracking_bill', 'Frontend\HomeController@trackingBill')->name('trackingBill');
//Route::group(['middleware' => ['no_auth:users', 'locale']], function () {
//    Route::post('register', 'Frontend\UserController@postRegister')->name('user.postRegister');
//    Route::post('login', 'Frontend\UserController@postLogin')->name('user.postLogin');
//});
Route::get('dang-ky', 'Frontend\UserController@getRegister')->name('user.register');
Route::post('dang-ky', 'Frontend\UserController@postRegister')->name('user.postregister');
Route::get('dang-nhap', 'Frontend\UserController@getLogin')->name('user.login');
Route::post('dang-nhap', 'Frontend\UserController@postLogin')->name('user.postlogin');
Route::get('dang-xuat', function () {
    \Auth::guard('admin')->logout();
    return redirect('/');
})->name('user.logout');
Route::get('quen-mat-khau', 'Frontend\UserController@postDangKy')->name('user.forgot_password');
#
Route::get('add-to-cart', 'Frontend\OrderController@addToCart')->name('order.add_to_cart');
Route::get('delete-from-cart', 'Frontend\OrderController@deleteFromCart')->name('order.delete_from_cart');
Route::get('update-cart', 'Frontend\OrderController@updateCart')->name('order.update_cart');
#
Route::get('seach-post', 'Frontend\PostController@getSearchPost')->name('search_post');     // Tim kiem
Route::get('tag/{tag}', 'Frontend\CategoryController@getSearch');
Route::get('seed/{action}', 'Frontend\SeedController@getIndex');
// Tim kiem

Route::get('tag_post/{slug}', 'Frontend\PostController@getIndexTags');
Route::get('/tags/{slug}', 'Frontend\CategoryController@getIndex');  //  Danh sach san pham
Route::get('/category/{slug}', 'Frontend\CategoryPostController@getIndex');  //  Danh sach tin tuc


Route::get('giao-hang', 'Frontend\OrderController@getDelivery')->name('order.getDelivery');
Route::post('giao-hang', 'Frontend\OrderController@postDelivery')->name('order.postDelivery');
Route::get('thanh-toan', 'Frontend\OrderController@getPay')->name('order.pay');
Route::post('thanh-toan', 'Frontend\OrderController@createBill')->name('order.createBill');
Route::get('thanh-toan-thanh-cong', 'Frontend\OrderController@pay_success')->name('order.pay_success');

Route::post('/create-bill', 'Frontend\OrderController@createBill')->name('order.create_bill'); // add đơn hàng vào database
Route::get('/remove-to-cart', 'Frontend\OrderController@getRemoveCart')->name('remove.cart'); // xoa san pham gio hang
Route::get('/gio-hang', 'Frontend\OrderController@getIndex')->name('order.view'); // xem gio hang

Route::post('/send/contact', 'Frontend\ContactController@sendContact')->name('send.contact'); // post phone contact chi tiet san pham
Route::post('/action/phone', 'Frontend\ContactController@addPhoneContact')->name('resign.contact'); // post phone contact chi tiet san pham
Route::post('/ajax/contacts', 'Frontend\ContactController@contactsAjax')->name('contacts.form'); // form trong chi tiết sp;
Route::get('/tim-kiem', 'Frontend\CategoryController@getSearch')->name('search'); // search  ajax

Route::post('/send-mail', 'Frontend\HomeController@getAllManufacturer')->name('send.mail');

Route::post('/search-transports', 'Frontend\HomeController@searchTransport')->name('search.transports');
Route::post('/ajax/van-don', 'Frontend\HomeController@ajaxVanDon')->name('ajaxVanDon');
Route::post('/ajax/send-request', 'Frontend\HomeController@ajaxSendRequest')->name('ajaxSendRequest');
Route::post('/ajax/check-coupon', 'Frontend\HomeController@checkCouponAjax')->name('checkCouponAjax');
Route::post('/ajax/get-transport-product-type-by-id', 'Frontend\HomeController@ajaxGetTransportProductType')->name('ajaxGetTransportProductType');
Route::post('/ajax/get-modal-create-transport', 'Frontend\HomeController@ajaxGetModalTransport')->name('ajaxGetModalTransport');
Route::post('/ajax/get-shipping-method', 'Frontend\HomeController@ajaxGetShippingMethod')->name('ajaxGetShippingMethod');
Route::post('/ajax/get-price', 'Frontend\HomeController@ajaxGetPrice')->name('ajaxGetPrice');

Route::get('/{slug}/{productSlug}', 'Frontend\ProductController@detailTwo')->name('two-slug');    // Chi tiet san pham hoac chi tiet bai viet / danh mục tin tức
Route::get('/{slug}/{slugTwo}/{productSlug}', 'Frontend\ProductController@detailThree');    // Chi tiet san pham hoac chi tiet bai viet
Route::get('/{slug}/{slugTwo}/{slugThree}/{productSlug}', 'Frontend\ProductController@detailFor');    // Chi tiet san pham hoac chi tiet bai viet
Route::get('/{slug}', 'Frontend\CategoryController@getIndex')->name('cate.list');  //  Danh sach danh muc

Route::get('/', 'Frontend\HomeController@getIndex')->name('home');    // Trang chu !