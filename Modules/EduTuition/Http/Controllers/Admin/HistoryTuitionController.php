<?php

namespace Modules\EduTuition\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\EduCourse\Models\MaketingMail;
use Modules\EduMarketing\Models\Category;
use Modules\EduMarketing\Models\Course;
use Modules\EduMarketing\Models\Lesson;
use Modules\EduMarketing\Models\Tuition;
use Modules\ThemeEdu\Models\Student;
use Validator;

class HistoryTuitionController extends CURDBaseController
{
//    protected $orderByRaw = 'order_no desc, id desc';
    protected $module = [
        'code' => 'history-tuition',
        'table_name' => 'history_tuitions',
        'label' => 'Lịch sử nộp học phí',
        'modal' => '\Modules\EduTuition\Models\HistoryTuition',
        'list' => [
            ['name' => 'tuition_id', 'type' => 'relation', 'label' => 'Học phí', 'object' => 'tuition', 'display_field' => 'name'],
            ['name' => 'student_id', 'type' => 'relation_edit', 'label' => 'Học viên', 'object' => 'student', 'display_field' => 'name'],
            ['name' => 'course_id', 'type' => 'relation', 'label' => 'Khóa học', 'object' => 'course','display_field' => 'name'],
            ['name' => 'tuition_fee', 'type' => 'price', 'label' => 'Học phí'],
            ['name' => 'updated_at', 'type' => 'date_vi', 'label' => 'Ngày đóng tiền'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'tuition_id', 'type' => 'select2_ajax_model', 'object' => 'tuition', 'class' => 'required', 'label' => 'Học phí', 'model' => \Modules\EduTuition\Models\Tuition::class, 'display_field' => 'name'],
                ['name' => 'student_id', 'type' => 'select2_ajax_model', 'object' => 'student', 'class' => 'required', 'label' => 'Học viên', 'model' => Student::class, 'display_field' => 'name'],
                ['name' => 'course_id', 'type' => 'select2_ajax_model', 'object' => 'course', 'class' => 'required', 'label' => 'Khóa học', 'model' => \Modules\ThemeEdu\Models\Course::class, 'display_field' => 'name'],
                ['name' => 'tuition_fee', 'type' => 'text', 'class' => 'number_price', 'label' => 'Học phí'],
            ],

        ],
    ];

    protected $filter = [
        'student_id' => [
            'label' => 'Học viên',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'student',
            'model' => Student::class,
            'query_type' => '='
        ],
        'course_id' => [
            'label' => 'Khóa học',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'course',
            'model' => \Modules\ThemeEdu\Models\Course::class,
            'query_type' => '='
        ],
        'tuition_id' => [
            'label' => 'Học phí',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'tuition',
            'model' => \Modules\EduTuition\Models\Tuition::class,
            'query_type' => '='
        ],
        'updated_at' => [
            'label' => 'Ngày đóng tiền',
            'type' => 'date',
            'query_type' => 'custom'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('edutuition::historytuition.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        }

        //  Lọc theo danh mục
        if (!is_null($request->get('category_id'))) {
            $category = Category::find($request->category_id);
            if (is_object($category)) {
                $query = $query->where(function ($query) use ($category) {
                    $cat_childs = $category->childs; //  Lấy các id của danh mục con
                    $query->orWhere('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các tin thuộc danh mục hiện tại
                    foreach ($cat_childs as $cat_child) {
                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
                    }
                });
            }
        }
        if (!is_null($request->get('tags'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }
        return $query;
    }

    public function add(Request $request)
    {

        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('edutuition::historytuition.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
//                    'name' => 'required'
//                ], [
//                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code'].'/'.$this->model->id);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('edutuition::historytuition.edit')->with($data);
        } else if ($_POST) {

            $validator = Validator::make($request->all(), [
//                'name' => 'required'
//            ], [
//                'name.required' => 'Bắt buộc phải nhập tên',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

//                  Tùy chỉnh dữ liệu insert


                #

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function duplicate(Request $request, $id)
    {
        $poduct = Lesson::find($id);
        $poduct_new = $poduct->replicate();
        $poduct_new->company_id = \Auth::guard('admin')->user()->last_company_id;
        $poduct_new->admin_id = \Auth::guard('admin')->user()->id;
        $poduct_new->save();
        return $poduct_new;
    }
}
