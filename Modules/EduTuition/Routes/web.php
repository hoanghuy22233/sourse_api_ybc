<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'tuition'], function () {
        Route::get('', 'Admin\TuitionController@getIndex')->name('tuition')->middleware('permission:tuition');
        Route::get('publish', 'Admin\TuitionController@getPublish')->name('tuition.publish')->middleware('permission:tuition');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TuitionController@add')->middleware('permission:tuition');
        Route::get('delete/{id}', 'Admin\TuitionController@delete')->middleware('permission:tuition');
        Route::post('multi-delete', 'Admin\TuitionController@multiDelete')->middleware('permission:tuition');
        Route::get('search-for-select2', 'Admin\TuitionController@searchForSelect2')->name('tuition.search_for_select2')->middleware('permission:tuition');

        Route::get('{id}', 'Admin\TuitionController@update')->middleware('permission:customer');
        Route::post('{id}', 'Admin\TuitionController@update')->middleware('permission:customer');
    });

    Route::group(['prefix' => 'history-tuition'], function () {
        Route::get('', 'Admin\HistoryTuitionController@getIndex')->name('history-tuition')->middleware('permission:tuition');
        Route::get('publish', 'Admin\HistoryTuitionController@getPublish')->name('history-tuition.publish')->middleware('permission:tuition');
        Route::match(array('GET', 'POST'), 'add', 'Admin\HistoryTuitionController@add')->middleware('permission:tuition');
        Route::get('delete/{id}', 'Admin\HistoryTuitionController@delete')->middleware('permission:tuition');
        Route::post('multi-delete', 'Admin\HistoryTuitionController@multiDelete')->middleware('permission:tuition');
        Route::get('search-for-select2', 'Admin\HistoryTuitionController@searchForSelect2')->name('history-tuition.search_for_select2')->middleware('permission:tuition');

        Route::get('{id}', 'Admin\HistoryTuitionController@update')->middleware('permission:tuition');
        Route::post('{id}', 'Admin\HistoryTuitionController@update')->middleware('permission:tuition');
    });
});
