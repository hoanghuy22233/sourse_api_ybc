<label style="cursor: pointer" for="Đối tượng gửi mail">Đối tượng gửi mail
<div class="kt-checkbox-list mt-3">
    <?php
    $objects=explode( '|', @$result->object);
    ?>
    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
        <input type="checkbox" name="checkbox[]" value="Học viên" @if(in_array("Học viên", $objects))checked @endif> Học viên
        <span></span>
    </label>
    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
        <input type="checkbox" name="checkbox[]" value="Giáo viên" @if(in_array('Giáo viên', $objects))checked @endif> Giáo viên
        <span></span>
    </label>
   <label class="kt-checkbox kt-checkbox--tick kt-checkbox--brand">
        <input type="checkbox" name="checkbox[]"  value="Khách hàng" @if(in_array('Khách hàng', $objects))checked @endif> Khách hàng
        <span></span>
    </label>
</div>
</label>