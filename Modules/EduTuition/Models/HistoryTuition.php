<?php

namespace Modules\EduTuition\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\EduCourse\Models\Classs;
use Modules\ThemeEdu\Models\Course;
use Modules\ThemeEdu\Models\Student;

class HistoryTuition extends Model
{

    protected $table = 'history_tuitions';

    protected $guarded = [];
//    public $timestamps = false;
//    protected $fillable = [
//        'name', 'lesson_id','content'
//    ];

    public function tuition()
    {
        return $this->belongsTo(Tuition::class, 'tuition_id');
    }
    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }
}
