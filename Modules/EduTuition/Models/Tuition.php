<?php

namespace Modules\EduTuition\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\EduCourse\Models\Classs;
use Modules\ThemeEdu\Models\Contact;
use Modules\ThemeEdu\Models\Course;
use Modules\ThemeEdu\Models\Student;

class Tuition extends Model
{
    protected $table = 'tuitions';
    protected $guarded = [];
//    protected $fillable = [
//        'admin_id','category_id',
//    ];

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id');
    }
    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }

}
