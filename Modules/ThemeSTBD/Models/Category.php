<?php

namespace Modules\ThemeSTBD\Models;

use App\Http\Helpers\CommonHelper;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = 'categories';

    protected $fillable = [
        'name' , 'slug' , 'parent_id' , 'intro' , 'image' , 'user_id' , 'status', 'type', 'order_no', 'banner', 'created_at', 'banner_sidebar', 'featured',
        'show_menu', 'show_homepage', 'filters', 'banner_child', 'title_banner_child', 'description_banner_child', 'banner_child_left',
        'featured_description', 'video', 'fields_id_product', 'category_product_id'
    ];

    public function parent()
    {
        return $this->hasOne($this, 'id', 'parent_id');
    }

    public function posts() {
        return $this->hasMany(Post::class, 'category_id', 'id');
    }

    public function exps() {
        return $this->hasMany(Exp::class, 'category_id', 'id');
    }

    public function childs()
    {
        return $this->hasMany($this, 'parent_id', 'id')->orderBy('order_no', 'asc');
    }

    public function childsMenu()
    {
        return $this->hasMany($this, 'parent_id', 'id')->select(['id', 'name', 'slug'])->orderBy('order_no', 'asc');
    }

    public function user() {
        return $this->belongTo(User::class, 'user_id');
    }

//    public function getImageUrlAttribute()
//    {
//        return url('/').'/public/filemanager/userfiles/slides/'.$this->attributes['banner'];
//    }
}
