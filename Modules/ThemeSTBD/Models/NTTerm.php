<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace Modules\ThemeSTBD\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NTTerm extends Model {

    protected $table = 'wp_terms';
}