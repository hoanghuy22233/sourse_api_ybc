<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 7/31/2018
 * Time: 12:47 PM
 */

namespace Modules\ThemeSTBD\Models;


class Manufacturer extends Module
{
    protected $table = 'manufactureres';

    public function category() {
        return $this->belongsTo(Category::class, 'category_id');
    }

}