<!DOCTYPE html>
<html lang="vi" xml:lang="vi">
<?php
function isMobile()
{
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
$banner_header_top = CommonHelper::getFromCache('setting_name_banner_head_top_type_common_tab', ['settings']);
if (!$banner_header_top) {
    $banner_header_top = \App\Models\Setting::where('name', 'photos[banner_head_top]')->where('type', 'common_tab')->first();
    CommonHelper::putToCache('setting_name_banner_head_top_type_common_tab', $banner_header_top, ['settings']);
}

$banner_top = CommonHelper::getFromCache('banner_location_banner_header_top', ['banners']);
if (!$banner_top) {
    $banner_top = \Modules\ThemeSTBD\Models\Banner::where('status', 1)->where('location', 'banner_header_top')->first();
    CommonHelper::putToCache('banner_location_banner_header_top', $banner_top, ['banners']);
}

$custom_slide_product_hot = CommonHelper::getFromCache('setting_name_custom_slide_product_hot_type_homepage_tab_value', ['settings']);
if (!$custom_slide_product_hot) {
    $custom_slide_product_hot = @\Modules\ThemeSTBD\Models\Settings::where('name', 'custom_slide_product_hot')->where('type', 'homepage_tab')->first()->value;
    CommonHelper::putToCache('setting_name_custom_slide_product_hot_type_homepage_tab_value', $custom_slide_product_hot, ['settings']);
}



?>
<head>

    @include('themestbd::partials.head_meta')
    @include('themestbd::partials.header_script')
    <link rel="stylesheet" href="{{asset('public/frontend/themes/stbd/css/custom2.css')}}?v={{ time() }}">
    {!! @$settings['head_code'] !!}

    <style>
        nav.a {
            width: 491px;
        }

        .nav-menu .nav-item {
            padding: 3px 0 !important;
        }

        {{--.f.regis {--}}
        {{--    @if(@$settings['show_email_homepage']==0)--}}
        {{--          display: none !important;--}}
        {{--@endif--}}



        }

        header {
            @if(@$settings['header_background'] != '')    background: {{@$settings['header_background']}} !important;
        @endif



        }

        footer {
            @if(@$settings['footer_background'] != '')    background: {{@$settings['footer_background']}} !important;
        @endif



        }

        @if(@$settings['option_background']==0)
            body > div.f {
            background: none !important;
        }

        header {
            @if(@$settings['header_background'] != '')    background: {{@$settings['header_background']}} !important;
        @endif



        }

        @elseif(@$settings['option_background']==1)
            @if(@$settings['background_color']!='')
                body > div.f {
            background-color: {{$settings['background_color']}}   !important;
        }

        @endif
        @elseif(@$settings['option_background']==2)
            @if(!empty($settings['background_image']))
                body > div.f {
            background-image: url('https://demo4.webhobasoft.com/public/filemanager/userfiles/{{$settings['background_image']}}') !important;
            background-attachment: fixed;
        }

        @endif
        @endif
.tongdai {
            width: 25%;
            display: flex;
            line-height: 24px;
            height: 24px;
        }

        .tongdai:before {
            font: 12px/16px FontAwesome;
            height: 15px;
            width: 15px;
            display: inline-block;
            text-align: center;
            margin-top: 4px;
            margin-right: 5px;
            border-radius: 50%;
        }

        @media (max-width: 991px) {

            .tongdai {
                display: none !important;
            }
        }

        {{--        @if(@$settings['footer_background'] != '')--}}
        {{--            .f.regis {--}}
        {{--            display: none!important;--}}
        {{--        }--}}
        {{--        @endif--}}


.contact_now_parent {
            position: fixed;
            bottom: 10px;
            right: 0;
            width: 50px;
            z-index: 999;
        }

        .fb_customer_chat_bubble_animated_no_badge, .fb_dialog {
            bottom: 0px !important;
            right: 50px !important;
        }

        .phone_now_detail {
            position: relative;
            display: inline-block;
        }

        .phone_now_detail {
            position: absolute;
            display: none;
            bottom: 5px;
            right: 100%;
            height: auto;
            width: max-content;
            z-index: 999;
            background: #fff;
            padding: 15px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        .contact_now-title {
            display: flex;
            height: 30px;
            line-height: 30px;
            margin-bottom: 10px;
        }

        .contact_now-title > span {
            width: 100%;
            margin-right: 10px;
        }

        .phone_now, .zalo_now, .contact_now-title {
            cursor: pointer;
        }

        .zalo_now {
            position: relative;
        }

        .zalo_now > span {
            display: none;
        }

        .zalo_now > span {
            position: absolute;
            top: 5px;
            right: 100%;
            width: max-content;
            background: #fff;
            border: 1px solid #ccc;
            /*display: block;*/
            padding: 10px;
        }

        @media (max-width: 767px) {
            .b.flexJus.mobie_padding {
                padding: 0 15px;
            }

            header .head .b .hright a {
                top: 5px;
                right: 50px;
            }

            #touch-menu {
                top: 15px;
            }
        }

        .menu-item {
            position: relative;
        }

        .menu-item:hover ul.sub-menu {
            display: block;
            width: 300px;
            top: 100%;
            left: 0;
            background: #fff;
            border: 1px solid #ccc;
            z-index: 9999;
        }

        ul.sub-menu {
            position: absolute;
            display: none;
        }

        li.sub-menu-item:last-child {
            border-bottom: none;
        }

        li.sub-menu-item {
            padding: 10px;
            border-bottom: 1px solid #ccc;
        }

        img.lazy {
            opacity: 0;
        }

        @if(@$settings['menu_text_color'] != '')
            .nav-menu .nav-item .nav-link {
            color: {{ $settings['menu_text_color'] }};
        }
        @endif

        @if(@$settings['footer_text_color'] != '')
            footer#footer,
        footer#footer a,
        footer#footer li {
            color: {{ $settings['menu_text_color'] }};
        }
        @endif

        @media(min-width: 768px) {
            .bnews li {
                min-height: 330px;
            }

        }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            xfbml: true,
            version: 'v6.0'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>


<div class="contact_now_parent">
    <div class="contact_now">
        <ul>
            <li class="fb_now">
                <a href="https://www.messenger.com/t/BepHoangCuong.VN" target="_blank">
                    <svg width="60px" height="60px" viewBox="0 0 60 60"><svg x="0" y="0" width="60px" height="60px"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g><circle fill="#0084ff" cx="30" cy="30" r="30"></circle><svg x="10" y="10"><g transform="translate(0.000000, -10.000000)" fill="#FFFFFF"><g id="logo" transform="translate(0.000000, 10.000000)"><path d="M20,0 C31.2666,0 40,8.2528 40,19.4 C40,30.5472 31.2666,38.8 20,38.8 C17.9763,38.8 16.0348,38.5327 14.2106,38.0311 C13.856,37.9335 13.4789,37.9612 13.1424,38.1098 L9.1727,39.8621 C8.1343,40.3205 6.9621,39.5819 6.9273,38.4474 L6.8184,34.8894 C6.805,34.4513 6.6078,34.0414 6.2811,33.7492 C2.3896,30.2691 0,25.2307 0,19.4 C0,8.2528 8.7334,0 20,0 Z M7.99009,25.07344 C7.42629,25.96794 8.52579,26.97594 9.36809,26.33674 L15.67879,21.54734 C16.10569,21.22334 16.69559,21.22164 17.12429,21.54314 L21.79709,25.04774 C23.19919,26.09944 25.20039,25.73014 26.13499,24.24744 L32.00999,14.92654 C32.57369,14.03204 31.47419,13.02404 30.63189,13.66324 L24.32119,18.45264 C23.89429,18.77664 23.30439,18.77834 22.87569,18.45674 L18.20299,14.95224 C16.80079,13.90064 14.79959,14.26984 13.86509,15.75264 L7.99009,25.07344 Z"></path></g></g></svg></g></g></svg></svg>
                </a>
            </li>
            <li class="phone_now">
                <img data-src="{{ asset('public/filemanager/userfiles/1.logo/call-bhc-2.gif') }}" class="lazy"
                     width="50px">
            </li>
            <li class="zalo_now">
                <a href="https://zalo.me/0974329191" target="_blank">
                    <img data-src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb('1.logo/zalo.png', '50px', null) }}"
                         class="lazy"
                         alt="">

                </a>
                <span>Zalo: 0974 32 91 91</span>
            </li>
        </ul>
        @include('themestbd::partials.phone_now_detail')
    </div>

</div>
<script>
    $(document).ready(function () {
        $("blockquote").after('<i class="fa fa-quote-left" aria-hidden="true"></i>');
        $("blockquote").before('<i class="fa fa-quote-right" aria-hidden="true"></i>');


        $(".phone_now").click(function () {
            $('.contact_now .phone_now_detail').toggle();
        });
        $(".zalo_now").hover(function () {
            $('.zalo_now>span').toggle();
        });
    });

    $(window).scroll(function (event) {
        let scrollHeight = $(window).scrollTop();
        if (scrollHeight > 280) {
            $('.contact_now_parent').css({
                'bottom': '180px'
            });
            // $('.fb_customer_chat_bubble_animated_no_badge').css({
            //     'bottom': '295px!important'
            // });
        } else {
            $('.contact_now_parent').css({
                'bottom': '10px'
            });
            // $('.fb_customer_chat_bubble_animated_no_badge').css({
            //     'bottom': '130px!important'
            // });
        }
    });
</script>
@if(!empty($banner_top))
    <a href="{{$banner_top->link}}">
        <img data-src=" /public/filemanager/userfiles/{{ $banner_top->image  }}" style="margin: auto;display: block;"
             alt="{{ $banner_top->name  }}" class="lazy">
    </a>
@endif

@include('themestbd::partials.menu_home')
{{--@include('themestbd::partials.menu_cate_home')--}}
@if(@$settings['option_menu'] == 0)
    @include('themestbd::partials.menu_efect.menu_slidedown')
@elseif(@$settings['option_menu'] == 2)
    @include('themestbd::partials.menu_efect.menu_default')
@endif

@include('themestbd::partials.slide')

@include('themestbd::partials.branlogo')
@yield('main_content')
@if (isMobile())
    @include('themestbd::partials.category_slide_mobie')
@else
    @include('themestbd::partials.category_slide')
@endif
{{--{{dd($settings['custom_slide_product_hot'])}}--}}

@if($custom_slide_product_hot == 1)
    @if (isMobile())
        @include('themestbd::partials.slide_mobie_pro_hot')
    @else
        @include('themestbd::partials.slide_product_hot')
    @endif

@endif
@include('themestbd::partials.experience')





@include('themestbd::partials.form_resign')
@include('themestbd::partials.set_font')
@if(@$settings['footer_content'] == 1)
    @include('themestbd::partials.footer_colum')
@elseif(@$settings['footer_content'] == 0)
    @include('themestbd::partials.footer')
@endif
@include('themestbd::partials.footer_script')
@include('themestbd::partials.contact_us')

{!! @$settings['footer_code'] !!}
<style>
    .fb_customer_chat_bubble_animated_no_badge {
        right: 0 !important;
        bottom: 125px !important;
    }

    .fb_customer_chat_bubble_animated_no_badge_280 {
        bottom: 125px !important;
    }

    .fb_customer_chat_bubble_animated_no_badge_281 {
        bottom: 300px !important;
    }
</style>
<script>
    $(window).scroll(function (event) {
        let scrollHeight = $(window).scrollTop();
        if (scrollHeight > 280) {
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').addClass('fb_customer_chat_bubble_animated_no_badge_281');
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').removeClass('fb_customer_chat_bubble_animated_no_badge_280');
        } else {
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').addClass('fb_customer_chat_bubble_animated_no_badge_280');
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').removeClass('fb_customer_chat_bubble_animated_no_badge_281');
        }
    });
</script>
</body>
</html>
