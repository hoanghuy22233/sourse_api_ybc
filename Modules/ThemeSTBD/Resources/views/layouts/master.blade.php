<!DOCTYPE html>
<html lang="vi" xml:lang="vi">
<?php
    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

$banner_top = CommonHelper::getFromCache('banner_location_banner_header_top');
if (!$banner_top) {
    $banner_top = \Modules\ThemeSTBD\Models\Banner::where('status',1)->where('location','banner_header_top')->first();
    CommonHelper::putToCache('banner_location_banner_header_top', $banner_top);
}

$banner_right = CommonHelper::getFromCache('banner_location_banner_header_right');
if (!$banner_right) {
    $banner_right = \Modules\ThemeSTBD\Models\Banner::where('status',1)->where('location','banner_header_right')->first();
    CommonHelper::putToCache('banner_location_banner_header_right', $banner_right);
}

$banner_left = CommonHelper::getFromCache('banner_location_banner_header_left');
if (!$banner_left) {
    $banner_left = \Modules\ThemeSTBD\Models\Banner::where('status',1)->where('location','banner_header_left')->first();
    CommonHelper::putToCache('banner_location_banner_header_left', $banner_left);
}

?>
<head>

    @include('themestbd::partials.header_detail_script')
    @include('themestbd::partials.head_meta')
    @include('themestbd::partials.header_cate_script')
    @include('themestbd::partials.header_script')

    <link rel="stylesheet" href="{{asset('public/frontend/themes/stbd/css/custom2.css')}}?v={{ time() }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    {!! @$settings['head_code'] !!}


    <style>
        .tongdai {
            width: 25%;
            display: flex;
            line-height: 24px;
            height: 24px;
        }
        .tongdai:before {
            font: 12px/16px FontAwesome;
            height: 15px;
            width: 15px;
            display: inline-block;
            text-align: center;
            margin-top: 4px;
            margin-right: 5px;
            border-radius: 50%;
        }
        @media (max-width: 991px){

            .tongdai {
                display: none!important;
            }
        }
        header {
            padding: 0 !important;
        }

        .flexJus.hleft > a {
            line-height: 1;
        }

        /*header.f{*/
        /*    margin-top: -15px;*/
        /*}*/
        .f.regis {
            @if(@$settings['show_email_homepage']==0)
 display: none !important;
        @endif

        }

        header {
            @if(@$settings['header_background'] != '')  background: {{@$settings['header_background']}}  !important;
        @endif

        }

        footer {
            @if(@$settings['footer_background'] != '')  background: {{@$settings['footer_background']}}  !important;
        @endif

        }


        @if(@$settings['option_background']==0)
            body > div.f {
            background: none !important;
        }

        @elseif(@$settings['option_background']==1)
            @if(@$settings['background_color']!='')
                body > div.f {
            background-color: {{$settings['background_color']}}  !important;
        }

        @endif
        @elseif(@$settings['option_background']==2)
            @if(!empty($settings['background_image']))
                body > div.f {
            background-image: url('https://demo4.webhobasoft.com/public/filemanager/userfiles/{{$settings['background_image']}}') !important;
            background-attachment: fixed;
        }

        @endif
    @endif
 /*form.tim{*/
        /*    width: 70%;*/
        /*}*/
        /*.tongdai {*/
        /*    width: 50%;*/
        /*}*/

        .custom_fa:before {
            font-size: 24px !important;
        }

        blockquote {
            background: #0a95e2;
            display: inline-block;
            padding: 10px;
        }

        .nav-menu .nav-item {
            padding: 0 !important;
        }

        /*li.menu-san_pham:hover div#menu-show{*/
        /*    top: 100%!important;*/
        /*}*/
        #menu-show > .flexJus > .hover1 > span {
            border-bottom: none !important;
            padding: 0;
        }

        .head {

            height: 40px;
        }

        .contact_now_parent {
            position: fixed;
            bottom: 10px;
            right: 0;
            width: 50px;
            z-index: 999;
        }


        .fb_customer_chat_bubble_animated_no_badge, .fb_dialog  {
            bottom: 0px!important;
            right: 50px!important;
        }

        .phone_now_detail {
            position: relative;
            display: inline-block;
        }

        .phone_now_detail {
            position: absolute;
            display: none;
            bottom: 5px;
            right: 100%;
            height: auto;
            width: max-content;
            z-index: 999;
            background: #fff;
            padding: 15px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        .contact_now-title {
            display: flex;
            height: 30px;
            line-height: 30px;
            margin-bottom: 10px;
        }

        .contact_now-title > span {
            width: 100%;
            margin-right: 10px;
        }

        .phone_now, .zalo_now, .contact_now-title {
            cursor: pointer;
        }

        .zalo_now {
            position: relative;
        }

        .zalo_now > span {
            display: none;
        }

        .zalo_now > span {
            position: absolute;
            top: 5px;
            right: 100%;
            width: max-content;
            background: #fff;
            border: 1px solid #ccc;
            /*display: block;*/
            padding: 10px;
        }

        nav.a {
            width: 500px !important;
        }



        .banner_header_right img,.banner_header_left img{
            max-width: 100%;
            max-height: 100%;

        }

        .banner_header_right{
            position: fixed;
            top: 10%;
            right: 0;
            width: 100px;
            height: 500px;
            /*background: #ccc;*/
        }
        .banner_header_left{
            position: fixed;
            top: 10%;
            left: 0;
            width: 100px;
            max-height: 500px;
        }
        @media (max-width: 1770px) {

            .banner_header_right,.banner_header_left{
                display: none;
            }
        }
        @media (max-width: 767px){
            .b.flexJus.mobie_padding {
                padding: 0 15px;
            }
            header .head .b .hright a {
                top: 5px;
                right: 50px;
            }
            #touch-menu {
                top: 15px;
            }
        }
        .menu-item {
            position: relative;
        }
        .menu-item:hover ul.sub-menu{
            display: block;
            width: 300px;
            top: 100%;
            left: 0;
            background: #fff;
            border: 1px solid #ccc;
            z-index: 9999;
        }
        ul.sub-menu {
            position: absolute;
            display: none;
        }
        li.sub-menu-item:last-child {
            border-bottom: none;
        }
        li.sub-menu-item {
            padding: 10px;
            border-bottom: 1px solid #ccc;
        }
        img.lazy {
            opacity: 0;
        }

        @if(@$settings['menu_text_color'] != '')
            .nav-menu .nav-item .nav-link {
            color: {{ $settings['menu_text_color'] }};
        }
        @endif

        @if(@$settings['footer_text_color'] != '')
            footer#footer,
            footer#footer a,
            footer#footer li {
            color: {{ $settings['menu_text_color'] }};
        }
        @endif
    </style>
    @yield('head_script')
</head>
<body>

@if(!empty($banner_right))
<div class="banner_header_right">
    <a href="{{$banner_right->link}}">
    <img class="lazy" data-src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($banner_right->image, '100%', null) }}"
         alt="" width="100%">
    </a>
</div>
@endif
@if(!empty($banner_left))
<div class="banner_header_left">
    <a href="{{$banner_left->link}}">
    <img class="lazy" data-src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($banner_left->image, '100%', null) }}"
         alt="" width="100%">
    </a>
</div>
@endif

<div class="contact_now_parent">
    <div class="contact_now">
        <ul>
            <li class="fb_now">
                <a href="https://www.messenger.com/t/BepHoangCuong.VN" target="_blank">
                    <svg width="60px" height="60px" viewBox="0 0 60 60"><svg x="0" y="0" width="60px" height="60px"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g><circle fill="#0084ff" cx="30" cy="30" r="30"></circle><svg x="10" y="10"><g transform="translate(0.000000, -10.000000)" fill="#FFFFFF"><g id="logo" transform="translate(0.000000, 10.000000)"><path d="M20,0 C31.2666,0 40,8.2528 40,19.4 C40,30.5472 31.2666,38.8 20,38.8 C17.9763,38.8 16.0348,38.5327 14.2106,38.0311 C13.856,37.9335 13.4789,37.9612 13.1424,38.1098 L9.1727,39.8621 C8.1343,40.3205 6.9621,39.5819 6.9273,38.4474 L6.8184,34.8894 C6.805,34.4513 6.6078,34.0414 6.2811,33.7492 C2.3896,30.2691 0,25.2307 0,19.4 C0,8.2528 8.7334,0 20,0 Z M7.99009,25.07344 C7.42629,25.96794 8.52579,26.97594 9.36809,26.33674 L15.67879,21.54734 C16.10569,21.22334 16.69559,21.22164 17.12429,21.54314 L21.79709,25.04774 C23.19919,26.09944 25.20039,25.73014 26.13499,24.24744 L32.00999,14.92654 C32.57369,14.03204 31.47419,13.02404 30.63189,13.66324 L24.32119,18.45264 C23.89429,18.77664 23.30439,18.77834 22.87569,18.45674 L18.20299,14.95224 C16.80079,13.90064 14.79959,14.26984 13.86509,15.75264 L7.99009,25.07344 Z"></path></g></g></svg></g></g></svg></svg>
                </a>
            </li>
            <li class="phone_now">
                <img class="lazy" data-src="https://i.imgur.com/fmhOHUr.gif" width="50px">
            </li>
            <li class="zalo_now">
                <a href="https://zalo.me/0974329191" target="_blank">
                    <img class="lazy" data-src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb('1.logo/zalo.png', '50px', null) }}"
                         alt="">

                </a>
                <span>Zalo: 0974 32 91 91</span>
            </li>
        </ul>
        @include('themestbd::partials.phone_now_detail')
    </div>

</div>
<script>
    $(document).ready(function () {
        $("blockquote").before('<i class="fa fa-quote-left fa-pull-left custom_fa" STYLE="color: #ccc;    margin-top: 10px;"  aria-hidden="true"></i>');


        $(".phone_now").click(function () {
            $('.contact_now .phone_now_detail').toggle();
        });
        $(".zalo_now").hover(function () {
            $('.zalo_now>span').toggle();
        });
    });

    $(window).scroll(function (event) {
        let scrollHeight = $(window).scrollTop();
        if (scrollHeight > 280) {
            $('.contact_now_parent').css({
                'bottom': '180px'
            });
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').css({
                'bottom': '295px!important'
            });
        } else {
            $('.contact_now_parent').css({
                'bottom': '10px'
            });
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').css({
                'bottom': '130px!important'
            });
        }
    });
</script>

{{--@if(isMobile())--}}
{{--    @include('themestbd::partials.menu_home')--}}
{{--    @include('themestbd::partials.menu_cate_home')--}}
{{--    @if(@$settings['option_menu'] == 0)--}}
{{--        @include('themestbd::partials.menu_efect.menu_slidedown')--}}
{{--    @elseif(@$settings['option_menu'] == 2)--}}
{{--        @include('themestbd::partials.menu_efect.menu_default')--}}
{{--    @endif--}}
{{--@else--}}
{{--    @include('themestbd::partials.menu_cate_master')--}}

@if(!empty($banner_top))
<a href="{{$banner_top->link}}">
    <img class="lazy" data-src=" /public/filemanager/userfiles/{{ $banner_top->image  }}" style="margin: auto;display: block;" alt="{{ $banner_top->name  }}">
</a>
@endif
@if(@$settings['option_menu'] != 2)
    @include('themestbd::partials.menu_home')
@endif
@if(@$settings['option_menu'] == 0)

    @include('themestbd::partials.menu_efect.menu_slidedown')
@elseif(@$settings['option_menu'] == 2)
    @include('themestbd::partials.menu_cate_master')
    {{--        @include('themestbd::partials.menu_efect.menu_default')--}}
@endif
{{--@endif--}}
@yield('main_content')

@include('themestbd::partials.set_font')
{{--@include('themestbd::partials.footer_colum')--}}
@if(@$settings['footer_content'] == 1)
    @include('themestbd::partials.footer_colum')
@else
    @include('themestbd::partials.footer')
@endif
@include('themestbd::partials.footer_script')
@include('themestbd::partials.contact_us')
<div id="popup">
    <span class="button b-close"><span>X</span></span>
    <div id="Map"></div>
</div>
{!! @$settings['footer_code'] !!}
<style>
    .fb_customer_chat_bubble_animated_no_badge{
        right: 0!important;
        bottom:125px!important;
    }
    .fb_customer_chat_bubble_animated_no_badge_280{
        bottom:125px!important;
    }
    .fb_customer_chat_bubble_animated_no_badge_281{
        bottom:300px!important;
    }
</style>
<script>
    $(window).scroll(function (event) {
        let scrollHeight = $(window).scrollTop();
        if (scrollHeight > 280) {
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').addClass('fb_customer_chat_bubble_animated_no_badge_281');
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').removeClass('fb_customer_chat_bubble_animated_no_badge_280');
        } else {
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').addClass('fb_customer_chat_bubble_animated_no_badge_280');
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').removeClass('fb_customer_chat_bubble_animated_no_badge_281');
        }
    });
</script>

</body>
</html>