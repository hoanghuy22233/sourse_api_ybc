<?php
    $brans = CommonHelper::getFromCache('get_brans');
    if (!$brans){
        $brans = \Modules\ThemeSTBD\Models\Manufacturer::where('status', 1)->orderBy('order_no', 'asc')->get();
        CommonHelper::putToCache('get_brans', $brans);
    }

$show_bran_homepage = CommonHelper::getFromCache('settings_name_show_bran_homepage_type_homepage_tab');
if (!$show_bran_homepage){
    $show_bran_homepage = @\Modules\ThemeSTBD\Models\Settings::where('name','show_bran_homepage')->where('type','homepage_tab')->first()->value;
    CommonHelper::putToCache('settings_name_show_bran_homepage_type_homepage_tab', $show_bran_homepage);
}
?>
<div class="f">
    <div class="b">
        <div class="f flexCen list-launcher">
            <div class="bra"><img class="lazy" data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo_brand'])  }}" alt="Thương hiệu" /></div>
            <div class="flexCol">
                <span class="intro-thuonghieu">{{ @$settings['logo_brand_intro'] }}</span>
                <span class="bg_h bghtop"></span>
                @if($show_bran_homepage == 1)
                <div>
                    @if(!empty($brans))
                        @foreach($brans as $data)
                            <a href="{{route('manufacturer.detail', ['slug' => $data->slug])}}" title="{{$data->name}}">
                                <img src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($data->image, 153, null) }}" alt="{{$data->name}}">
                            </a>
                        @endforeach
                    @endif
                </div>
                <span class="bg_h bghbot"></span>
                @endif
            </div>
        </div>
    </div>
</div>
