<div class="PositionBR">
    {{--<iframe src="javascript:''" id="__gwt_historyFrame" style="width:0;height:0;border:0"></iframe>--}}
    <div id="mainElement" class="MainElement InicializingState ">
        <style>
            .PositionBR {
                background-color: white;
                display: none;
                position: fixed;
                border: 0px;
                overflow: hidden;
                width: 500px;
                height: 475px;
                z-index: 999997;
                top: 35%;
                left: 50%;
                margin-left: -250px;
                margin-top: -150px;
            }

            @media only screen and (max-width: 540px) {
                .PositionBR {
                    width: 320px;
                    top: 15%;
                    margin: auto;
                    left: 5%;
                    right: 5%;
                }
            }

            @media only screen and (max-width: 375px) {
                .PositionBR {
                    width: 310px;
                    top: 8%;
                    margin: auto;
                    left: 1.8%;
                    right: 1.8%;
                }
            }

            #mainElement {
                padding: 0 15px;
            }

            #windowCloseButton {
                position: absolute;
                top: 10px;
                right: 10px;
                display: none;
            }

            .CloseButton {
                cursor: pointer;
            }

            .TextBox {
                width: 100%;
                height: 28px;
            }

            .buttonText {
                color: #FFFFFF;
            }

            .buttonText {
                font-size: 13px;
                padding: 5px 17px;
                line-height: 23px;
                background-color: #f76422f0;
                border-radius: 50px;
                cursor: pointer;
                position: relative;
                top: 25px;
            }

            #showform_lh {
                display: none;
            }

            textarea {
                width: 100%;
                height: 80px;
            }

            label {
                line-height: 30px;
                font-weight: bold;
            }

        </style>
        <div class="ContactHeader ">
            <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo'], 'auto', '30')}}"
                 alt="{{@$settings['name']}}" title="{{@$settings['name']}}">
        </div>
        <div class="ContactBody">
            <div class="success"></div>
            <div id="body" class="ContactForm">
                <div id="formTitle" class="FormTitle">Chào mừng</div>
                <div class="ContactFormBorder">
                    <div id="formContent">
                        <div id="533main" class="g-FormField2" style="">
                            <div id="533label" class="g-FormField2-Label g-FormField2-Label-mandatory" style="">
                                <label>Tên</label>
                            </div>
                            <div class="g-FormField2-InputContainer">
                                <div id="533inputPanel" class="g-FormField2-InputPanel">
                                    <div id="533container" class="TextBoxContainer">
                                        <input id="533input" aria-labelledby="533label" type="text" class="TextBox"
                                               name="name" value="">
                                    </div>
                                </div>
                                <div id="533description" class="gwt-Label g-FormField2-Description"
                                     style="display: none;"></div>
                                <div id="533errorIcon" class="gwt-Label g-FormField2-ErrorIcon"
                                     style="display: none;"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div id="534main" class="g-FormField2" style="">
                            <div id="534label" class="g-FormField2-Label g-FormField2-Label-mandatory" style="">
                                <label>Số điện thoại</label>
                            </div>
                            <div class="g-FormField2-InputContainer">
                                <div id="534inputPanel" class="g-FormField2-InputPanel">
                                    <div id="534container" class="TextBoxContainer"><input id="11534input"
                                                                                           aria-labelledby="534label"
                                                                                            type="text" class="TextBox"
                                                                                           name="tel" value=""></div>
                                </div>
                                <div id="534description" class="gwt-Label g-FormField2-Description"
                                     style="display: none;"></div>
                                <div id="534errorIcon" class="gwt-Label g-FormField2-ErrorIcon"
                                     style="display: none;"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div id="534main" class="g-FormField2" style="">
                            <div id="534label" class="g-FormField2-Label g-FormField2-Label-mandatory" style="">
                                <label>email</label>
                            </div>
                            <div class="g-FormField2-InputContainer">
                                <div id="534inputPanel" class="g-FormField2-InputPanel">
                                    <div id="534container" class="TextBoxContainer"><input id="534input"
                                                                                           aria-labelledby="534label"
                                                                                           type="text" class="TextBox"
                                                                                           name="email" value=""></div>
                                </div>
                                <div id="534description" class="gwt-Label g-FormField2-Description"
                                     style="display: none;"></div>
                                <div id="534errorIcon" class="gwt-Label g-FormField2-ErrorIcon"
                                     style="display: none;"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div id="534main" class="g-FormField2" style="">
                            <div id="534label" class="g-FormField2-Label g-FormField2-Label-mandatory" style="">
                                <label>Tỉnh/Thành</label>
                            </div>
                            <?php
                            $province = CommonHelper::getFromCache('province_order_by_name_asc');
                            if (!$province){
                                $province = \Modules\ThemeSTBD\Models\Province::orderBy('name','ASC')->get();
                                CommonHelper::putToCache('province_order_by_name_asc', $province);
                            }
                            ?>
                            <div class="g-FormField2-InputContainer">
                                <div id="534inputPanel" class="g-FormField2-InputPanel">
                                    <div id="534container" class="TextBoxContainer">
                                        <select class="TextBox" name="tinh_thanh_contact" id="tinh_thanh_contact">
                                        @foreach($province as $vv)
                                                <option value="{{$vv->id}}">{{$vv->name}}</option>
                                        @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div id="534description" class="gwt-Label g-FormField2-Description"
                                     style="display: none;"></div>
                                <div id="534errorIcon" class="gwt-Label g-FormField2-ErrorIcon"
                                     style="display: none;"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div id="535main" class="g-FormField2" style="">
                            <div id="535label" class="g-FormField2-Label g-FormField2-Label-mandatory" style="">
                                <label>Tin nhắn</label>
                            </div>
                            <div class="g-FormField2-InputContainer">
                                <div id="535inputPanel" class="g-FormField2-InputPanel">
                                    <div id="535container" class="MessageArea"><textarea id="535area"
                                                                                         name="message"></textarea>
                                        <div class="MessageFormFilesWrapper">
                                            <div id="535files" class="MessageFormFiles NoFiles"></div>
                                            <div id="535attach" class="MessageFormAttach"></div>
                                        </div>
                                    </div>
                                </div>
                                <div id="535description" class="gwt-Label g-FormField2-Description"
                                     style="display: none;"></div>
                                <div id="535errorIcon" class="gwt-Label g-FormField2-ErrorIcon"
                                     style="display: none;"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <input name="url" value="{{ "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}" type="hidden">
                    </div>
                </div>
            </div>
        </div>
        <div id="contactFooter" class="ContactFooter ">
            <div id="buttonsPanelWrap" class="ButtonsPanel">
                <div id="sendmainButton" class="ImLeButton ImLeButtonMainOut ButtonUnwrapped Send" role="button">
                    <div id="sendmain" class="ImLeButtonMain ImLeButtonMainOut buttonBgColor buttonBorderColor send">
                        <div id="sendinnerBox" class="ImLeButtonMainInnerBox buttonInnerBorderColor">
                            <div id="sendinner" class="ImLeButtonMainInner">
                                <div id="sendmainShadow"
                                     class="ImLeButtonMainShadow buttonBgShadowColor buttonBgGradColor"></div>
                                <div class="ImLeButtonMainContent">
                                    <span id="sendtextSpan" class="buttonText">Gửi</span>
                                    <div id="sendiconDiv" class="buttonIcon"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="chatmainButton" class="ImLeButton ImLeButtonMainOut ButtonUnwrapped Chat" aria-hidden="true"
                     style="display: none;" role="button">
                    <div id="chatmain" class="ImLeButtonMain ImLeButtonMainOut buttonBgColor buttonBorderColor chat">
                        <div id="chatinnerBox" class="ImLeButtonMainInnerBox buttonInnerBorderColor">
                            <div id="chatinner" class="ImLeButtonMainInner">
                                <div id="chatmainShadow"
                                     class="ImLeButtonMainShadow buttonBgShadowColor buttonBgGradColor"></div>
                                <div class="ImLeButtonMainContent"><span id="chattextSpan"
                                                                         class="buttonText">chat</span>
                                    <div id="chaticonDiv" class="buttonIcon"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="callmainButton" class="ImLeButton ImLeButtonMainOut ButtonUnwrapped Call" aria-hidden="true"
                     style="display: none;" role="button">
                    <div id="callmain" class="ImLeButtonMain ImLeButtonMainOut buttonBgColor buttonBorderColor call">
                        <div id="callinnerBox" class="ImLeButtonMainInnerBox buttonInnerBorderColor">
                            <div id="callinner" class="ImLeButtonMainInner">
                                <div id="callmainShadow"
                                     class="ImLeButtonMainShadow buttonBgShadowColor buttonBgGradColor"></div>
                                <div class="ImLeButtonMainContent"><span id="calltextSpan"
                                                                         class="buttonText">Call</span>
                                    <div id="calliconDiv" class="buttonIcon"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="closemainButton" class="ImLeButton ImLeButtonMainOut ButtonUnwrapped SuccessCloseButton"
                     aria-hidden="true" style="display: none;" role="button">
                    <div id="closemain" class="ImLeButtonMain ImLeButtonMainOut buttonBgColor buttonBorderColor close">
                        <div id="closeinnerBox" class="ImLeButtonMainInnerBox buttonInnerBorderColor">
                            <div id="closeinner" class="ImLeButtonMainInner">
                                <div id="closemainShadow"
                                     class="ImLeButtonMainShadow buttonBgShadowColor buttonBgGradColor"></div>
                                <div class="ImLeButtonMainContent"><span id="closetextSpan"
                                                                         class="buttonText">Đóng</span>
                                    <div id="closeiconDiv" class="buttonIcon"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="Kb">
            <div id="kb_entry_panel" class="KbEntryPanel"></div>
            <div id="kb_side_panel" class="KbSidePanel">
                <div class="KbResultsWrapper">
                    <div id="kb_results" class="KbResults">
                    </div>
                </div>
            </div>
        </div>
        <div class="ContactWinButtons">
            <div class="CloseButton" id="windowCloseButton"><span>Đóng</span></div>
        </div>
        <div id="brandingPanel" class="Branding"></div>
    </div>
</div>
<div id="showform_lh" class=" " style="cursor: pointer; visibility: visible;"><img
            src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb('1.logo/float-button-01-off.png', '41', 'auto')}}"
            alt="Liên hệ với chúng tôi !" style="right: 0px;bottom:0px;z-index: 999997; position: fixed;"></div>
<div class="showbg"></div>
<script>
    $(document).ready(function () {
        $('#showform_lh').on('click', function () {
            $(this).hide();
            $('.PositionBR').show();
            $('.showbg').css({
                'display': 'block',
                'background': 'rgb(0,0,0)',
                'opacity': '0.6',
                'z-index': '555',
                'position': 'fixed',
                'top': '0',
                'left': '0',
                'width': '100%',
                'height': '100%'
            });
            $('#menu-show').css({
                'height': 'unset'
            });
            $('body').css({
                'overflow': 'hidden',
                'height': '100vh'
            })
            $('#windowCloseButton').css({
                'display': 'block'
            })

        });
        $(window).scroll(function (event) {
            let scrollHeight = $(window).scrollTop();
            if (scrollHeight > 280) {
                $('#showform_lh').show();
            } else {
                $('#showform_lh').hide();
            }
        });
        $('.CloseButton').on('click', function () {
            $('.PositionBR').hide();
            $('#showform_lh').show();
            $('body').css({
                'overflow': 'unset',
                'height': 'unset'
            })
            $(this).hide();
            $('.showbg').hide();
            $('.ContactForm').show();
            $('.buttonText').show();
            $('.success').children().remove();
        })
    })
</script>
<script type="text/javascript" src="{{asset('public/frontend/themes/stbd/js/custom.js')}}"></script>