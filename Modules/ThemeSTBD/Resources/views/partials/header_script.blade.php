<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/home.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/footer.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/slide.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/detailproduct.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/frontend/css/style-responsive.css')}}">
<script type="text/javascript" src="{{URL::asset('public/frontend/themes/stbd/front/js/jquery.js')}}"></script>

{!! @$settings['frontend_head_code'] !!}
