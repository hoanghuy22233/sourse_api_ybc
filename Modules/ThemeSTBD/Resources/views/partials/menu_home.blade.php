@php
    $menus = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getMenusByLocation('main_menu', 10, false);
@endphp
@if(@$settings['option_menu'] == 0)
    <style>

        .head {
            height: 40px;
        }
    </style>
@endif
<header class="f">
    <div class="f head">
        <div class="b flexJus mobie_padding">
            <div class="flexJus hleft">
                @if(@$settings['logo_position']==1)
                    @include('themestbd::partials.logo')
                @endif
                <nav class="a">
                    <ul id="menu" class="nav-menu">
                        @php
                            $menus = \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getMenusByLocation('main_menu', 10, false);
                        @endphp
                        @if(!empty($menus))
                            @foreach($menus as $menu1)
                                @if($menu1['link'] == '#')
                                    <li class="nav-item menu-{{ str_slug($menu1['name'], '_') }}">
                                        <a class="nav-link" href="javascript:;"
                                           @if($menu1['name'] != 'Sản phẩm') onclick="showmenudv();"
                                           @endif title="{{$menu1['name']}}">{{$menu1['name']}}</a>
                                        @if(!isMobile())
                                            @if(@$settings['option_menu'] == 1 && str_slug($menu1['name'], '_')=='san_pham')
                                                @include('themestbd::partials.menu_efect.menu_dropdown')
                                            @endif
                                        @endif
                                    </li>
                                @else
                                    <li class="nav-item menu-{{ str_slug($menu1['name'], '_') }} menu-item">
                                        <a class="nav-link" href="{{ $menu1['link'] }}"
                                           title="{{$menu1['name']}}">{{$menu1['name']}}</a>
                                        @if(!empty($menu1['childs']))
                                            <ul class="sub-menu">
                                                @foreach($menu1['childs'] as $c => $menu2)
                                                    <li class="sub-menu-item">
                                                        <a href="{{$menu2['link']}}">{{$menu2['name']}}</a>
                                                        @if(!empty($menu2['childs']))
                                                            <ul class="sub-menu3">
                                                                @foreach($menu2['childs'] as $c3 => $menu3)
                                                                    <li class="sub-menu-item3">
                                                                        <a href="{{$menu3['link']}}">{{$menu3['name']}}</a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>

                                        @endif
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                </nav>
                @if(@$settings['logo_position']==0)
                    @include('themestbd::partials.logo')
                @endif
                <div style="clear: both; "></div>
                <?php
                $tongdai = CommonHelper::getFromCache('widget_location_tong_dai', ['widgets']);
                if (!$tongdai) {
                    $tongdai = \Modules\ThemeSTBD\Models\Widget::where('location', 'tongdai')->first();
                    CommonHelper::putToCache('widget_location_tong_dai', $tongdai, ['widgets']);
                }
                ?>
                @if($tongdai->status==1)
                    <div class="tongdai">
                        {!! @$tongdai->content !!}
                    </div>
                @endif

            </div>
            <div class="flexR hright">
                @include('themestbd::partials.search')
                @include('themestbd::partials.cart_total')
            </div>
        </div>
    </div>
    @include('themestbd::partials.show_dv')
    <i id="touch-menu" class="touch-menu"></i>
    @if(isMobile())
        @if(@$settings['option_menu'] == 1)
            @include('themestbd::partials.menu_efect.menu_dropdown')
        @endif
    @endif
</header>
<style>

    li.sub-menu-item {
        position: relative;
    }

    ul.sub-menu3 {
        position: absolute;
        display: none;
        top: -1px;
        left: 100%;
        width: 100%;
    }

    li.sub-menu-item3 {
        padding: 10px;
        background: #fff;
        border: 1px solid #ccc;
    }

    li.sub-menu-item:hover ul.sub-menu3 {
        display: block;
    }
</style>
