<!--Form dang ki-->
<?php
$show_email_homepage = CommonHelper::getFromCache('settings_name_show_email_homepage_type_homepage_tab');
if (!$show_email_homepage){
    $show_email_homepage = @\Modules\ThemeSTBD\Models\Settings::where('name','show_email_homepage')->where('type','homepage_tab')->first()->value;
    CommonHelper::putToCache('settings_name_show_email_homepage_type_homepage_tab', $show_email_homepage);
}

$background_color_email = CommonHelper::getFromCache('settings_name_background_color_email_type_homepage_tab');
if (!$background_color_email){
    $background_color_email = @\Modules\ThemeSTBD\Models\Settings::where('name','background_color_email')->where('type','homepage_tab')->first()->value;
    CommonHelper::putToCache('settings_name_background_color_email_type_homepage_tab', $background_color_email);
}
$back = '';
if($background_color_email !=''){
    $back = 'background:'.$background_color_email.'!important';
}else{
    $back = 'background:none!important';
}

?>
<div class="f regis " style="{{($show_email_homepage == 0) ? 'display:none!important;' : ''}}{{$back}}">
    <img class="lazy" data-src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['bg_dang_ky'], 1350, 265)}}" alt="đăng kí ưu đãi" />
    <div class="flexCen">
        <div class="regisform">
            <label>Đăng kí để nhận thông tin và ưu đãi
                <span>Cập nhật tin tức và các sản phẩm mới nhất, ưu đãi đặc quyền của chúng tôi</span>
            </label>
            <div class="flexJus">
                <form method="post" name="regis" id="rs" action="{{route('contacts.form')}}">
                    {{csrf_field()}}
                    <input class="clearVal" id="id" name="_token" type="hidden">
                    <input class="clearVal" id="rg_Name" name="Name" value="" placeholder="Tên" />
                    <input class="clearVal" id="rg_Phone" name="Phone" value="" type="tel" placeholder="Số điện thoại" />
                    <input class="clearVal" id="rg_Email" name="Email" value="" placeholder="Email" />

                    <button id="">Đăng kí</button>
                    <input type="submit" name="_w_action[RegisPOST]" style="display:none;" />
                    <input type="hidden" name="_w_action" value="RegisPOST" style="display: none" />
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#rs').on('submit', function (e) {
            if ($('#rg_Name').val() == '' || $('#rg_Phone').val() == '' || $('#rg_Email').val() == ''){
                e.preventDefault();
                let err =  "Bạn chưa nhập đầy đủ dữ liệu !";
                if ($('#rg_Name').val() == '') {
                    err += "\n Chưa Nhập Tên !";
                }
                else {err += ''}
                if ($('#rg_Phone').val() == '') {
                    err += "\n Chưa Nhập số điện thoại !";
                }
                else {err += ''}
                if ($('#rg_Email').val() == '') {
                    err += "\n Chưa Nhập email !";
                }
                else {err += ''}
                alert(err)
            }
            else {
                e.preventDefault();
                var getUrl = $(this).attr('action');
                var formData = new FormData($(this)[0]);
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                    type: 'post',
                    url: getUrl,
                    data: formData,
                    async: true,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data.success == true) {
                            $('.clearVal').val('');
                            alert('Đăng ký thành công');
                        }
                    }
                })
            }
        })
    });
</script>