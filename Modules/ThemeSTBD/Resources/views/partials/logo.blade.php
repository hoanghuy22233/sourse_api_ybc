<a href="{{route('home')}}" title="{{@$settings['name']}}">
    <img class="lazy" height="30"
            data-src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo'], 210, null) }}"
            alt="{{@$settings['name']}}"/>
</a>

