@extends('themestbd::layouts.master')
@section('main_content')
    <section>
        @include('themestbd::partials.breadcrumb')

        <style>
            #nleft {
                float: left;
                width: 78%;
                padding-right: 3%;
            }
            #nright {
                float: left;
                width: 22%;
                display: flex;
                flex-direction: column;
            }
            .tt li div a, .other ul li div a {
                color: #ff6100
            }

            .tt li h3, .other ul li h3 {
                margin: 0 0 15px 0;
            }

            .tt li h3 a {
                font: bold 20px/24px arial;
            }

            .tt li p {
                font: 13px/22px arial;
                color: #333;
                text-align: justify;
            }

            .tt li span, .other ul li span {
                font: 12px/22px arial;
                color: #666;
            }

            .tt li span:before, .views:before, .clock:before {
                font: 13px/1px FontAwesome;
                margin-right: 5px;
                color: #1d537f;
            }

            .clock:before, .tt li span:nth-child(1):before {
                content: '\f017';
            }

            .views:before, .tt li span:nth-child(2):before {
                content: "\f06e";
            }

            .tt li .more {
                font: 13px/20px arial;
                float: right;
                color: #ff6000;
            }

            .tt li .more:after {
                content: "\f105";
                font: 16px/1px FontAwesome;
                margin-left: 5px;
            }

            .other label {
                font: 22px/25px arial;
                display: block;
                color: #333;
                padding-bottom: 15px;
                border-bottom: 1px solid #ddd;
                margin-bottom: 15px;
            }

            .other ul {
                padding: 0;
            }

            .other ul li {
                width: 49%;
                float: left;
                padding: 5px 0px;
                min-height: 70px;
            }

            .other ul li:nth-child(2n+1) {
                margin-right: 2%;
            }

            .other ul li div a {
                color: #ff6100
            }

            .other ul li a {
                font: bold 16px/20px arial;
            }

            .newsright label {
                display: block;
                font: bold 18px/45px arial;
                background: #ddd;
                text-align: center;
            }
            h1 {
                margin: 20px 0;
            }

            article h3 {
                color: blue !important;
            }

            acticle h4 {
                font-weight: bold !important;
            }

            .content {
                font-size: 15px;
                font-family: "Open Sans", Helvetica, Arial, sans-serif;
                line-height: 1.55;
                text-align: justify;
            }

            .desc {
                font-weight: bold;
                margin-bottom: 10px;
                color: #444;
                text-align: justify;
            }
            h2, h2 a {
                font: bold 16px/30px arial;
            }
            .dtop, .share {
                display: flex;
            }
            .dtop{
                float: left;
                width: 100%;
                padding: 15px 0;
            }
            @media (max-width: 767px) {
                #nleft {
                    width: 100%;
                }
                #nright {
                    width: 100%;
                }
                ul.breadcrumb {
                    display: inline-block;
                }
                div#nright {
                    margin-top: 50px;
                }
                .newsright.bgghi li {
                    margin-bottom: 26px;
                }
            }
            .lis-menu-post>p {
                font-weight: bold;
                font-size: 20px;
                text-transform: uppercase;
                padding-bottom: 20px;
            }

            .lis-menu-post {
                background: #dbedf9;
                padding: 20px;
                margin: 10px 0;
            }
        </style>

        <div id="nleft" class="hentry" itemscope="" itemtype="https://schema.org/Article">
            <div id="news">
                <div id="nleft">
                    <div>
                        <h1 class="entry-title" itemprop="name">{{@$post->name}}</h1>
                        <div class="lis-menu-post">
                            <p>Xem nhanh</p>
                            <ul>
                            </ul>
                        </div>

                        <div class="dtop">
                            <div class="clock published" itemprop="datePublished" content="2019-01-21T11:20+07:00"
                                 datetime="2019-01-21T11:20+07:00">{{@$post->created_at}} &nbsp;
                            </div>
                            <div class="hide"><span class="updated" itemprop="dateModified" content="2019-01-21T11:37+07:00" datetime="2019-01-21T11:37+07:00"></span></div>
                            <div class="hide"><span itemprop="author" class="author vcard"><b class="fn">{{ @$settings['name'] }}</b></span>
                            </div>
                            <meta itemprop="image"
                                  content="https://bepnamduong.vn/public/frontend/upload/images/SEO/mayruabat/mayruabatbaonhiutien/may-rua-bat-cong-nghiep-loai-bang-tai.jpg"/>
                            <meta itemprop="headline" content="Máy rửa bát công nghiệp bao nhiêu tiền"/>
                            <div class="hide" itemprop="publisher" itemscope="" itemtype="https://schema.org/Organization">
                                <b itemprop="name">{{ @$settings['name'] }}</b>
                                <meta itemprop="logo" content="https://bepnamduong.vn/Content/img/logo.png"/>
                            </div>
                            <div class="views">{{number_format(@$post->view_total, 0, '.', '.')}}</div>
                            <div>
                            </div>
                        </div>
                    </div>
                    <p class="content">
                    <p class="f desc entry-summary">
                        {{@$post->intro}}
                    </p>
                    <article class="entry-content" id="search-menu">
                        {!! @$post->content !!}
                    </article>
                    @include('themestbd::partials.relate_post')
                </div>
                <div id="nright">
                    @include('themestbd::partials.sidebar_posts')
                </div>
            </div>
        </div>
        <div id="nright">
            @include('themestbd::partials.sidebar_posts')
        </div>
        <br/>

        <div class="f other bg">
            @if(!empty($cate_tags))
            <label class="dtit">Danh mục sản phẩm HOT có thể bạn quan tâm</label>
                <div class="f flexJus" style="margin-top: 10px;">
                    @foreach(@$cate_tags as $cate)
                        <a target="_blank" href="{{route('cate.list', ['slug' => @$cate->slug])}}" title="{{@$cate->name}}">
                            <img class="lazy lazy0" data-src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb(@$cate->image, 200, null) }}"
                                 alt="{{@$cate->name}}"/><h2 style="text-align: center">{{@$cate->name}}</h2></a>
                    @endforeach
                </div>
            @endif
        </div>

        <div class="f other bg" style="margin-top: 30px;">
            @if(!empty($post_tags) || count(@$post_tags) > 0)
                @if(count(@$post_tags) > 0)
                     <label class="dtit">Tin cùng chuyên mục</label>
                @endif
                <ul>
                    @foreach(@$post_tags as $tag)
                        <li>
                            <a href="{{route('cate.list', ['slug' => $tag->slug])}}.html"
                               title="{{$tag->name}}">{{$tag->name}}</a>
                            <div><span class="clock">{{$tag->created_at}}</span> | <span class="views">{{number_format($tag->view_total, 0, '.', '.')}}</span></div>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>

    </section>

@endsection
<script>
    $(document).ready(function(){
        var h2 = $('#search-menu').find('h2');
        var html='';
        h2.each(function(index) {
            h2.eq(index).attr('id','h2_'+index);
            html = html+'<li><a href="#h2_'+index+'">'+h2.eq(index).text()+'</a></li>';
        });
        $('.lis-menu-post>ul').html(html);
    });
</script>