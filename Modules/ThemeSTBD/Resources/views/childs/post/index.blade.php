<?php
$muc_luc = explode('<h2>', $post->content);
unset($muc_luc[0]);
$string_heading = '<ol>';
foreach ($muc_luc as $k => $v) {
    $string = explode('</h2>', $v);
    $string_heading .= '<li ><a class="h2_menu" href="">' . $string[0] . '</a></li>';
//    lấy h3
    $attr3 = $string[1];
    $muc_luc3 = explode('<h3>', $attr3);
    unset($muc_luc3[0]);
    if (!empty($muc_luc3)) {
        foreach ($muc_luc3 as $k3 => $v3) {
            $string3 = explode('</h3>', $v3);
            $string_heading .= '<ol>';
            $string_heading .= '<li style="margin-left: 30px; list-style: disc" ><a class="h3_menu" href=""> ' . $string3[0] . '</a></li>';


            //    lấy h4;
//            dd($string3[1]);
            $muc_luc4 = explode('<h4>', $string3[1]);
            unset($muc_luc4[0]);

            if (!empty($muc_luc4)) {

                foreach ($muc_luc4 as $k4 => $v4) {
                    $string4 = explode('</h4>', $v4);
                    $string_heading .= '<ol>';
                    $string_heading .= '<li  style="margin-left: 60px; list-style: disc"><a class="h4_menu" href=""> ' . $string4[0] . '</a></li>';


                    //    lấy h5
                    $muc_luc5 = explode('<h5>', $string4[1]);
                    unset($muc_luc5[0]);

                    if (!empty($muc_luc5)) {
                        foreach ($muc_luc5 as $k5 => $v5) {
                            $string5 = explode('</h5>', $v5);
                            $string_heading .= '<ol>';
                            $string_heading .= '<li  style="margin-left: 90px; list-style: disc"><a class="h5_menu" href=""> ' . $string5[0] . '</a></li>';

                            //    lấy h6
                            $muc_luc6 = explode('<h6>', $string5[1]);
                            unset($muc_luc6[0]);
                            if (!empty($muc_luc6)) {
                                foreach ($muc_luc6 as $k6 => $v6) {
                                    $string6 = explode('</h6>', $v6);
                                    $string_heading .= '<li  style="margin-left: 120px; list-style: disc"><a class="h6_menu" href=""> ' . $string6[0] . '</a></li>';
                                }
                            }
                            $string_heading .= '</ol>';
                        }
                    }
                    $string_heading .= '</ol>';
                }
            }
            $string_heading .= '</ol>';
        }
    }
}
$string_heading .= '</ol>';
?>
@extends('themestbd::layouts.master')
@section('main_content')

    <link href="{{ URL::asset('public/frontend/themes/stbd/css/bpr-products-module4826.css') }}" rel='stylesheet'
          type='text/css'/>
    <link href="{{ URL::asset('public/frontend/themes/stbd/css/kinh_nghiem_hay.css') }}" rel='stylesheet'
          type='text/css'/>
    <style>
        a.h2_menu, a.h3_menu, a.h4_menu, a.h5_menu, a.h6_menu {
            font-weight: unset;
            font-size: 14px !important;
        }

        a.h2_menu strong, a.h3_menu strong, a.h4_menu strong, a.h5_menu strong, a.h6_menu strong {
            font-weight: unset;
        }

        a.h2_menu span, a.h3_menu span, a.h4_menu span, a.h5_menu span, a.h6_menu span {
            font-size: 14px !important;
        }

        #nleft article h3 {
            color: #000 !important;
        }

        .lis-menu-post > p {
            font-weight: bold;
            font-size: 20px;
            text-align: center;
            padding-bottom: 20px;
        }

        .postby {
            margin-bottom: 5%;
        }

        .lis-menu-post {
            background: #dbedf9;
            padding: 20px;
            margin-bottom: 5%;
            display: inline-block;
            width: 50%;
            border: 1px solid;
            float: left;
            margin-right: 5%;
            text-shadow: 0 0 black;
        }

        .blog_related ul li {
            width: 50%;
            float: left;
            clear: none !important;
        }

        .main_container.collection .title-head {
            position: relative;
        }
        a.img-news {
            min-height: 162px;
            vertical-align: middle;
        }
        .prd-in-content {
            border: 1px solid #ccc;
        }

        #nleft {
            float: left;
            width: 78%;
            padding-right: 3%;
        }

        #nright {
            float: left;
            width: 22%;
            display: flex;
            flex-direction: column;
        }

        .tt li div a, .other ul li div a {
            color: #ff6100
        }

        .tt li h3, .other ul li h3 {
            margin: 0 0 15px 0;
        }

        .tt li h3 a {
            font: bold 20px/24px arial;
        }

        .tt li p {
            font: 13px/22px arial;
            color: #333;
            text-align: justify;
        }

        .tt li span, .other ul li span {
            font: 12px/22px arial;
            color: #666;
        }

        .tt li span:before, .views:before, .clock:before {
            font: 13px/1px FontAwesome;
            margin-right: 5px;
            color: #1d537f;
        }

        .clock:before, .tt li span:nth-child(1):before {
            content: '\f017';
        }

        .views:before, .tt li span:nth-child(2):before {
            content: "\f06e";
        }

        .tt li .more {
            font: 13px/20px arial;
            float: right;
            color: #ff6000;
        }

        .tt li .more:after {
            content: "\f105";
            font: 16px/1px FontAwesome;
            margin-left: 5px;
        }

        .other label {
            font: 22px/25px arial;
            display: block;
            color: #333;
            padding-bottom: 15px;
            border-bottom: 1px solid #ddd;
            margin-bottom: 15px;
        }

        .other ul {
            padding: 0;
        }

        .other ul li {
            width: 49%;
            float: left;
            padding: 5px 0px;
            min-height: 70px;
        }

        .other ul li:nth-child(2n+1) {
            margin-right: 2%;
        }

        .other ul li div a {
            color: #ff6100
        }

        .other ul li a {
            font: bold 16px/20px arial;
        }

        .newsright label {
            display: block;
            font: bold 18px/45px arial;
            background: #ddd;
            text-align: center;
        }

        h1 {
            margin: 20px 0;
        }

        article h3 {
            color: blue !important;
        }

        acticle h4 {
            font-weight: bold !important;
        }

        .content {
            font-size: 15px;
            font-family: "Open Sans", Helvetica, Arial, sans-serif;
            line-height: 1.55;
            text-align: justify;
        }

        .desc {
            font-weight: bold;
            margin-bottom: 10px;
            color: #444;
            text-align: justify;
        }

        h2, h2 a {
            font: bold 16px/30px arial;
        }

        .dtop, .share {
            display: flex;
        }

        .dtop {
            float: left;
            width: 100%;
            padding: 15px 0;
        }

        @media (max-width: 767px) {

            .article-content iframe {
                width: 100%;
            }

            .lis-menu-post {
                margin: 10px 0 !important;
                width: auto !important;
            }

            .blog_related ul li {
                width: 100%;
                clear: both !important;
            }

            #nleft {
                width: 100%;
            }

            #nright {
                width: 100%;
            }

            ul.breadcrumb {
                display: inline-block;
            }

            div#nright {
                margin-top: 50px;
            }

            .newsright.bgghi li {
                margin-bottom: 26px;
            }

            h1.title-head {
                line-height: 31px;
            }
        }
        form.form-top {
            padding: 40px 20px;
            background: #eee;
            position: relative;
        }

        .box-form-register {
            background: #fff;
            border-radius: 15px;
            padding: 40px 30px;
            margin-top: 25px;
            box-shadow: 0 8px 20px -6px black;
            width: 500px;
            margin: 0 auto;
            left: calc(50% - 225px);
            top: -165px;
        }

        .formTopLeft {
            width: 320px;
            margin: 0 auto;
            font-family: tahoma;
        }

        @media (max-width: 991px) {

            .formTopLeft {
                width: auto;
            }

            .article-content img {
                width: auto !important;
                height: auto !important;
            }

            .box-form-register {
                width: 100%;
            }
        }

        .form-top .tx02 {
            font-size: 18px;
            margin-bottom: 15px;
            background: linear-gradient(to right, #edcf89, #ddb968, #f7e1a8, #f0d697, #eccd85, #d6d014);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
        }

        .form-group {
            width: 100%;
            margin-bottom: 10px;
            text-align: left;
        }

        input.form-controls {
            height: 38px;
            width: 100%;
            padding: 5px 10px;
            border-radius: 5px;
            border: 1px solid #efefef;
            color: #5d5d5d;
            background: #efefef;
        }

        .btn-register-top {
            background: #dc2013;
            background: -webkit-linear-gradient(to right, #ff693a, #dc2013);
            background: linear-gradient(to right, #ff693a, #dc2013);
            border: none;
            padding: 10px 20px;
            color: #fff;
            border-radius: 5px;
            width: 100%;
            font-weight: bold;
            font-size: 16px;
            cursor: pointer;
            margin-top: 10px;
            margin-bottom: 5px;
        }

        .article-content img {
            width: auto;
            height: auto;
        }

        .article-content p {
            line-height: 30px;
        }

        .select_abc {
            width: 100%;
            padding: 9px;
            border-radius: 3px;
            margin: 0;
        }

        @media (max-width: 991px) {

            .formTopLeft {
                width: auto;
            }

            .article-content img {
                width: auto !important;
                height: auto !important;
            }

            .box-form-register {
                width: 100%;
            }
        }

        @media (max-width: 768px) {
            .form-group {
                display: flex;
            }

            .form-group label {
                width: 40%;
                font-size: 11px;
                line-height: 18px;
            }

            input.form-controls, .select_abc {
                width: 60%;
                float: right;
            }

        }

        @media (min-width: 768px) {
            .box-form-register label {
                width: 36%;
                float: left;
                display: inline-block;
                text-align: right;
                margin-right: 10px;
            }

            .box-form-register input,
            .box-form-register select {
                width: 60% !important;
                float: left;
                display: inline-block;
                margin-bottom: 20px;
            }

            input.btn-register-top {
                width: 100% !important;
            }
            .article-details td img {
                max-width: unset;
            }

            .article-content a.img-news {
                text-align: center;
            }
            .article-content a.img-news img {
                max-height: 138px;
                margin: auto;
                float: none;
            }
        }
        @media (max-width: 768px) {
            .article-details table {
                width: unset !important;
            }
        }
    </style>
    <?php
    $province = CommonHelper::getFromCache('province_orderBy_name_asc');
    if (!$province) {
        $province = \Modules\ThemeSTBD\Models\Province::orderBy('name', 'ASC')->get();
        CommonHelper::putToCache('province_orderBy_name_asc', $province);
    }

    $distric = CommonHelper::getFromCache('district_orderBy_name_asc');
    if (!$distric) {
        $distric = \Modules\ThemeSTBD\Models\District::orderBy('name', 'ASC')->get();
        CommonHelper::putToCache('district_orderBy_name_asc', $distric);
    }
    $content = str_replace('http:', 'https:', $post->content);

    ?>

    <div class="f container">
        <div class="row">
            @if(isset($post))
                <?php

                $pro_sidebar = (array_map('intval', explode('|', trim(@$post->product_sidebar, '|'))));
                //                dd($pro_sidebar);
                ?>
                <?php
                $product_sidebar = CommonHelper::getFromCache('get_product_by_product_sidebar' . implode('|', $pro_sidebar));
                if (!$product_sidebar) {
                    $product_sidebar = \Modules\ThemeSTBD\Models\Product::select(['id', 'name', 'slug', 'image'])->whereIn('id', $pro_sidebar)->where('status', 1)->get();
                    CommonHelper::putToCache('get_product_by_product_sidebar' . implode('|', $pro_sidebar), $product_sidebar);
                }

                ?>
                <?php
                $admin = $post->admin_id;
                if (isset($admin) || $admin == 0) {
                    $user = CommonHelper::getFromCache('admin_id_1');
                    if (!$user) {
                        $user = \Modules\ThemeSTBD\Models\Admin::find(1);
                        CommonHelper::putToCache('admin_id_1', $user);
                    }

                } else {
                    $user = \Modules\ThemeSTBD\Models\Admin::where('id', $admin)->first();
                }
                ?>
            @endif
            {{--            @include('themestbd::partials.sidebar_post', ['product_sidebar' => $product_sidebar])--}}
            <section
                    class="main_container collection @if(isset($post) && isset($product_sidebar) && count($product_sidebar) > 0) col-md-9 col-sm-8 col-xs-12 @else col-xs-12 @endif">
                <div class="row">
                    <section class="right-content col-md-12">
                        <div id="news">
                            {{--                            <div class="flexJus btop">--}}
                            {{--                                <div class="simM">--}}
                            {{--                                    @if(!empty($menus))--}}
                            {{--                                        @foreach($menus as $menu)--}}
                            {{--                                            @if($menu['link'] == '#')--}}
                            {{--                                                <a href="javascript:;" onclick="showmenudv();"--}}
                            {{--                                                   title="{{$menu['name']}}">{{$menu['name']}}</a>--}}
                            {{--                                            @else--}}
                            {{--                                                <a href="{{$menu['link']}}"--}}
                            {{--                                                   title="{{$menu['name']}}">{{$menu['name']}}</a>--}}
                            {{--                                            @endif--}}
                            {{--                                        @endforeach--}}
                            {{--                                    @endif--}}
                            {{--                                </div>--}}
                            {{--                                <a class="bg bCart" href="{{route('order.view')}}" title="Xem giỏ hàng" rel="nofollow">--}}
                            {{--                                    <label>@php echo Modules\ThemeSTBD\Http\Controllers\Frontend\OrderController::totalCart() @endphp</label>Giỏ--}}
                            {{--                                    hàng</a>--}}
                            {{--                            </div>--}}
                            @include('themestbd::partials.breadcrumb')
                            <div id="nleft">
                                <article class="article-main">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h1 class="title-head">{{@$post->name}}</h1>
                                            <div class="postby">
                                                <span>Đăng bởi <b>{{@$post->admin->name}}</b>
{{--                                                    vào lúc {{@$post->created_at}}--}}
                                                </span>
                                            </div>


                                            {{--                                            Mục lục--}}

                                            <div class="lis-menu-post">
                                                <p>Xem nhanh</p>

                                                {!! $string_heading !!}

                                            </div>


                                            <div class="article-details">
                                                <div class="article-content" id="search-menu">
                                                    <div class="rte">
                                                        {!! $content !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="margin: 20px 0;display: block" class="fb-like"
                                             data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"

                                             data-width="" data-layout="button" data-action="like" data-size="small"
                                             data-share="true" data-show-faces="true"></div>


                                        <div class="col-md-12">
                                            <?php
                                            $post_id = explode('|', $post->tags);
                                            $tags = CommonHelper::getFromCache('categorys_tags_id_pluck_name_slug' . implode('|', $post_id));
                                            if (!$tags) {
                                                $tags = \Modules\ThemeSTBD\Models\Category::whereIn('id', $post_id)->pluck('name', 'slug');
                                                CommonHelper::putToCache('categorys_tags_id_pluck_name_slug' . implode('|', $post_id), $tags);
                                            }

                                            ?>
                                            Tags :
                                            @foreach($tags as $tag_slug => $tag_name)
                                                <a href="/tag_post/{{ $tag_slug }}"
                                                   style="text-decoration: none">{{$tag_name}}</a>,
                                            @endforeach
                                        </div>

                                        <form class="form-top f contact-post" method="post">
                                            <input name="url"
                                                   value="{{ "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"
                                                   type="hidden">
                                            <div class="box-form-register">
                                                <div class="formTopLeft">
                                                    <h3 class="txt tx02 cen">ĐĂNG KÝ NHẬN DỊCH VỤ</h3>
                                                    <div class="form-group">
                                                        <input type="hidden" name="Campaign"
                                                               value="Khảo sát tư vấn lắp đặt miễn phí">
                                                        <label>Họ tên:</label>
                                                        <input name="Name" type="text" required="required"
                                                               class="form-controls clears name"
                                                               placeholder="Họ tên">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Phone:</label>
                                                        <input type="Phone"
                                                               {{--                                                               pattern="(0)+([0-9]{8})\b"--}}
                                                               {{--                                                               title="Có 10 số bắt đầu bằng 0"--}}
                                                               name="Phone"
                                                               required="required" class="form-controls clears phone"
                                                               placeholder="Số điện thoại.">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Tên sản phẩm:</label>
                                                        <input type="text" name="prd_name"
                                                               class="form-controls clears product_code"
                                                               required="required"
                                                               placeholder="Tên sản phẩm.">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Chọn dịch vụ:</label>
                                                        <select class="form-group contents select_abc"
                                                                required="required" name="content">
                                                            <option value="Dịch vụ trả góp">Dịch vụ trả góp</option>
                                                            <option value="Tư vấn sản phẩm">Tư vấn sản phẩm</option>
                                                            <option value="Dịch vụ khảo sát">Dịch vụ khảo sát</option>
                                                            <option value="Dịch vụ bảo dưỡng">Dịch vụ bảo dưỡng</option>
                                                            <option value="Dịch vụ lắp đặt">Dịch vụ lắp đặt</option>
                                                            <option value="Dịch vụ sửa chữa">Dịch vụ sửa chữa</option>
                                                            <option value="Đăng ký đại lý, CTV">Đăng ký đại lý, CTV
                                                            </option>
                                                            <option value="Vấn đề khác">Vấn đề khác</option>

                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Tỉnh/Thành: </label>
                                                        <select class="form-group contents  select_abc"
                                                                name="province_contact_post"
                                                                required="required"
                                                                id="province_contact_post">
                                                            <option value="" selected>Chọn tỉnh/thành</option>
                                                            @foreach($province as $pp)
                                                                <option value="{{$pp->id}}">{{$pp->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Quận/Huyện: </label>
                                                        <select class="form-group contents select_abc"
                                                                name="distric_contact_post"
                                                                required="required"
                                                                id="distric_contact_post">
                                                            <option value="" selected>Chọn quận/huyện</option>
                                                        </select>
                                                    </div>
                                                    <input type="hidden" name="State" value="4">
                                                    <input type="submit" name="" class="btn-register-top"
                                                           value="ĐĂNG KÝ NGAY">
                                                    <small style="display:block;" class="cen">Cam kết mọi thông tin của
                                                        bạn sẽ được bảo mật
                                                    </small>
                                                </div>
                                            </div>
                                        </form>
                                        <script>
                                            $(document).ready(function () {
                                                $('#province_contact_post').change(function () {
                                                    var province_contact_post = $(this).val();
                                                    $.ajax({
                                                        url: '/ajax/province_contact_post',
                                                        data: {
                                                            'province_contact_post': province_contact_post,
                                                        },
                                                        type: "POST",
                                                        success: function (data) {
                                                            if (data.success == true) {
                                                                $('#distric_contact_post').html(data.html);
                                                            }
                                                        }
                                                    });
                                                });


                                                $('.form-top').on('submit', function (e) {
                                                    e.preventDefault();
                                                    $.ajax({
                                                        url: '/ajax/contacts?Name=' + $('.name').val() + '&Phone=' + $('.phone').val() + '&Content=' + $('.contents').val() + '&tinh_thanh=' + $('#province_contact_post').val() + '&quan_huyen=' + $('#distric_contact_post').val() + '&product_code=' + $('.product_code').val() + '&url=' + $('.contact-post input[name=url]').val(),
                                                        type: "POST",
                                                        success: function (data) {
                                                            if (data.success == true) {
                                                                $('.clears').val('');
                                                                alert('Đăng ký thành công');
                                                            }
                                                        }
                                                    })
                                                })
                                            })
                                        </script>

                                        <style>
                                            .col-post-relate {
                                                width: 50%;
                                                /*float: left;*/
                                                padding: 0 10px;
                                                /*border-right: 1px solid #000;*/
                                            }

                                            .post-relate-last {
                                                display: flex;
                                            }

                                            .col-post-relate:last-child {
                                                /*border-right: none;*/
                                            }
                                        </style>
                                        @include('themestbd::partials.post_relate_same_category')
                                    </div>
                                </article>

                            </div>
                            <div id="nright">
                                @include('themestbd::partials.sidebar_detail_post')
                                @include('themestbd::partials.relate_product_post')
                            </div>
                        </div>
                    </section>
                </div>
            </section>
        </div>
    </div>
    <div class="bizweb-product-reviews-module"></div>
    <script>
        //  Load san pham trong content bai viet
        $('.prd-in-content').each(function () {
            var product_id = $(this).data('id');
            var object = $(this);
            $.ajax({
                url: '/load-san-pham-html',
                data: {
                    product_id: product_id
                },
                type: "GET",
                success: function (html) {
                    object.html(html)
                }
            })
        });
    </script>
    <script>
        $(document).ready(function () {
            var h2 = $('#search-menu').find('h2');
            var h3 = $('#search-menu').find('h3');
            var h4 = $('#search-menu').find('h4');
            var h5 = $('#search-menu').find('h5');
            var h6 = $('#search-menu').find('h6');
            h2.each(function (index) {
                h2.eq(index).attr('id', 'h2_' + index);
            });
            h3.each(function (index) {
                h3.eq(index).attr('id', 'h3_' + index);
            });
            h4.each(function (index) {
                h4.eq(index).attr('id', 'h4_' + index);
            });
            h5.each(function (index) {
                h5.eq(index).attr('id', 'h5_' + index);
            });
            h6.each(function (index) {
                h6.eq(index).attr('id', 'h6_' + index);
            });


            var a2 = $('.lis-menu-post').find('a.h2_menu');
            var a3 = $('.lis-menu-post').find('a.h3_menu');
            var a4 = $('.lis-menu-post').find('a.h4_menu');
            var a5 = $('.lis-menu-post').find('a.h5_menu');
            var a6 = $('.lis-menu-post').find('a.h6_menu');
            a2.each(function (index) {
                a2.eq(index).attr('href', '#h2_' + index);
            });
            a3.each(function (index) {
                a3.eq(index).attr('href', '#h3_' + index);
            });
            a4.each(function (index) {
                a4.eq(index).attr('href', '#h4_' + index);
            });
            a5.each(function (index) {
                a5.eq(index).attr('href', '#h5_' + index);
            });
            a6.each(function (index) {
                a6.eq(index).attr('href', '#h6_' + index);
            });


        });


    </script>
    <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "NewsArticle",
      "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "https://google.com/article"
      },
      "headline": "{{@$post->name}}",
      "image": [
        "{{'https://bephoangcuong.com' . \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($post,'100%',null) }}"
       ],
      "datePublished": "{{$post->created_at}}",
      "dateModified": "{{$post->updated_at}}",
      "author": {
        "@type": "Person",
        "name": "{{$settings['name']}}"
      },
       "publisher": {
        "@type": "Organization",
        "name": "Google",
        "logo": {
          "@type": "ImageObject",
          "url": "{{'https://bephoangcuong.com' . \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($settings['logo'],'100%',null) }}"
        }
      },
      "description": "{{$post->intro}}"
    }



    </script>

@endsection
{{--{!! dd($post->content) !!}--}}