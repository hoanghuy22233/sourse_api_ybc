<style>

    .product-in-content {
        display: flex;
    }

    @media (max-width: 768px) {
        .product-in-content {
            display: block;
        }

        .prd-in-content h3 > a {
            font-size: 11px;
        }

        .prd-in-content {
            width: 33.33%;
            float: left;
        }

    }
</style>
<div style="padding: 10px;    max-width: 200px;">
    <a class="img-news" style="height: unset!important"
       href="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($product) }}" title="{{$product->name}}">
        <img src="{{ asset('public/filemanager/userfiles/'.$product->image) }}" alt="{{$product->name}}" class=""></a>
    <p style="text-align: center"><strike>{{number_format($product->base_price,0,'','.')}}<sup>đ</sup></strike></p>
    <p style=" font-weight: bold;color: red;text-align: center">{{number_format($product->final_price,0,'','.')}}
        <sup>đ</sup></p>
    <h3 style="text-align: center">
        <a href="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($product) }}"
           title="{{$product->name}}">{{$product->name}}</a>
    </h3>
</div>
