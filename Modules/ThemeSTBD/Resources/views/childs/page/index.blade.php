@extends('themestbd::layouts.master')
@section('main_content')
    <style>
        .box img{
            width: 100% !important;
            height: 100% !important;
        }
    </style>
    <section style="width: 100%">
        @section('name', @$category->name)
        @include('themestbd::partials.breadcrumb')
        <div style="padding-top: 10px;"></div>
        <div class="box" style="width: 100%;">{!! @$post->content !!}</div>
        <style>
            .content img{display:block;}
            form.form-top {
                padding: 40px 20px;background:#eee;position:relative;height:350px;
            }
            .form-top .tx02 {
                font-size:18px;
                margin-bottom: 15px;
                background: linear-gradient(to right, #edcf89, #ddb968, #f7e1a8, #f0d697, #eccd85, #d6d014 );
                -webkit-background-clip: text;
                -webkit-text-fill-color: transparent;
            }

            .box-form-register {
                background: #fff;
                border-radius: 15px;
                padding: 40px 30px;
                margin-top: 25px;
                box-shadow: 0 8px 20px -6px black;width:500px;margin:0 auto;position:absolute;left:calc(50% - 225px);top:-165px;
            }

            .form-group {
                width: 100%;
                margin-bottom: 10px;
                text-align: left;
            }

            input.form-controls {
                height: 38px;
                width: 100%;
                padding: 5px 10px;
                border-radius: 5px;
                border: 1px solid #efefef;
                color: #5d5d5d;
                background: #efefef;
            }

            select.form-controls {
                width: 100%;
                height: 38px;
                padding: 8px 5px;
                border-radius: 5px;
                border: 1px solid #efefef;
                color: #5d5d5d;
                background: #efefef;
            }

            .formTopLeft {
                width: 320px;
                margin: 0 auto;
                font-family: tahoma;
            }

            :focus {
                outline: unset !important;
            }

            .btn-register-top {
                background: #dc2013;
                background: -webkit-linear-gradient(to right, #ff693a, #dc2013);
                background: linear-gradient(to right, #ff693a, #dc2013);
                border: none;
                padding: 10px 20px;
                color: #fff;
                border-radius: 5px;
                width: 100%;
                font-weight: bold;
                font-size: 16px;
                cursor: pointer;
                margin-top: 10px;
                margin-bottom: 5px;
            }

            .btn-register-top:hover {
                opacity: 0.9;
            }
        </style>
        <form class="form-top f" method="post">
            <div class="box-form-register">
                <div class="formTopLeft">
                    <h3 class="txt tx02 cen">ĐĂNG KÝ NHẬN DỊCH VỤ</h3>
                    <div class="form-group">
                        <input type="hidden" name="Campaign" value="Khảo sát tư vấn lắp đặt miễn phí">
                        <input name="Name" type="text" required="required" class="form-controls clears name" placeholder="Nhập họ tên">
                    </div>
                    <div class="form-group">
                        <input type="Phone" pattern="(09|01[2|6|8|9])+([0-9]{8})\b" title="Có 10 hoặc 11 số bắt đầu bằng 0" name="Phone" required="required" class="form-controls clears phone" placeholder="Nhập số điện thoại.">
                    </div>
                    <div class="form-group">
                        <input type="text" name="Content" class="form-controls clears contents" required="required" placeholder="Nhập sản phẩm.">
                    </div>
                    <div class="form-group">
                        <input type="text" name="Address" class="form-controls clears address" required="required" placeholder="Nhập địa chỉ.">
                    </div>
                    <input type="hidden" name="State" value="4">
                    <input type="submit" name="" class="btn-register-top" value="ĐĂNG KÝ NGAY">
                    <small style="display:block;" class="cen">Cam kết mọi thông tin của bạn sẽ được bảo mật</small>
                </div>
            </div>
        </form>
    </section>
@endsection
{{--
@section('head_code')
    <link rel="canonical" href="{{ url(@$category->slug .'/'. $post->slug) }}.html">
@endif--}}
