@foreach($products as $product)
    <div class="col-xs-6 col-sm-6 col-lg-4">
        <div class="product-box product-box-edits">
            <div class="product-thumbnail">
                <?php
                if ($product->final_price < $product->base_price) {
                    echo '<div class="sale-flash">SALE</div>';
                } else {
                    echo '<div class="sale-flash hide">SALE</div>';
                }
                ?>

                <a href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($product) }}"
                   title="{{$product->name}}">

                    <picture><img
                                src="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($product->image, 263, 197) }}"
                                alt="{{$product->name}}"
                                class="img-responsive center-block"/>
                    </picture>

                </a>
                    <div class="prod-icons">
                        <div class="variants form-nut-grid"
                             data-id="product-actions-8702198"
                             enctype="multipart/form-data">

                            @if($product->final_price == 0)
                                <a href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($product) }}"
                                   data-toggle="tooltip" data-placement="top"
                                   class="fa fa-link"
                                   title="Chọn sản phẩm"></a>
                            @else
                                <a class="fa fa-shopping-basket add_to_cart"
                                   href="javascript:;" data-product_id="{{ $product->id }}" title="{{$product->name}}"
                                   data-original-title="Mua ngay"></a>
                            @endif

                            <a
                                    href="javascript:;" data-product_id="{{ $product->id }}"
                                    title="Xem nhanh"
                                    class="fa fa-search quick-view"></a>

                        </div>
                    </div>
            </div>
            <div class="product-info">
                <h3 class="product-name"><a
                            href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($product) }}"
                            title="{{$product->name}}">{{$product->name}}</a></h3>


                <div class="price-box clearfix">

                    {!! \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getPriceHtml($product) !!}
                </div>


            </div>
        </div>
    </div>
@endforeach
{!! $products->setPath($setPath)->appends(isset($filter) ? $filter : '')->links() !!}

<script>
    //  Custom code
    $('.quick-view').click(function () {
        loading();
        console.log('xem nhanh');
        var product_id = $(this).data('product_id');
        $.ajax({
            url: '{{ route('product.ajax_quick_view') }}',
            type: 'GET',
            data: {
                product_id: product_id
            },
            success: function (result) {
                $('div#quick-view-product').show();
                $('.quick-view-product .block-quickview.primary_block').html(result);
            },
            error: function () {
                alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại');
            }
        });
        stopLoading();
    });

    function refreshCartQuickview() {
        $.ajax({
            url: '{{ route('order.refresh_cart_quick_view') }}',
            type: 'GET',
            success: function (result) {
                $('#popup-cart #popup-cart-desktop').html(result);
                $('div#popup-cart').show();
            },
            error: function () {
                alert('Lỗi load giỏ hàng! Vui lòng load lại trang và thử lại');
            }
        });
    }

    $('.add_to_cart').click(function () {
            loading();
            console.log('Mua nhanh');
            var product_id = $(this).data('product_id');
            var quantity = $('input[name=quantity]').val();
            if (quantity < 1) {
                alert('Vui lòng nhập số lượng > 0');
            } else {
                $.ajax({
                    url: '{{ route('order.add_to_cart') }}',
                    type: 'GET',
                    data: {
                        product_id: product_id,
                        quantity: quantity
                    },
                    success: function (result) {
                        if (result.status == true) {
                            refreshCartQuickview();
                        } else {
                            alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại');
                        }
                    },
                    error: function () {
                        alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại');
                    }
                });
            }
            stopLoading();
        }
    );
</script>
