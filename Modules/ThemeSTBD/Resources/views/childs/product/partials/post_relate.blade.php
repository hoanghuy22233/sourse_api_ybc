@if(@$settings['related_news'] == 1)
    <?php

    $v = CommonHelper::getFromCache('posts_id_name_slug_multi_cat_post_ids' . $product->id);
    if (!$v) {
        $v = \Modules\ThemeSTBDAdmin\Models\Post::select('id', 'name', 'slug', 'multi_cat')->where('related_products', 'like', '%|' . @$product->id . '|%')->orderBy('related_products_order_no','desc')->get();

        CommonHelper::putToCache('posts_id_name_slug_multi_cat_post_ids' . $product->id, $v);
    }
    ?>
    @if($v->count()> 0)
        <div class="relate_post_product">
            <h2 style="color: #fff; background-color: red;">Tin liên quan</h2>
            <ul>
                @foreach($v as $k => $post)
                    <li class="{{($k>4)?'xem_them_none':''}}" {{--style="{{($k>4)?'display:none':''}}"--}}>
                        <a href="{{ '/' . @$category->slug . '/' . $post->slug . '.html'}}"
                           style="font-size: 14px">{{$post->name}}</a>
                    </li>

                @endforeach
                @if($v->count()>5)
                    {{--<div class="xem_them">Xem thêm...</div>
                    <div class="thu_gon" style="display: none">Thu gọn!</div>--}}
                @endif
            </ul>

        </div>
    @endif
@endif