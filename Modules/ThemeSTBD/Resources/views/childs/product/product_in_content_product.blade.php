@if($products != null)
    <div style="padding: 10px;    max-width: 200px;">
        <a class="img-news" style="height: unset!important" href="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($products) }}" title="{{$products->name}}">
            <img class="lazy" data-src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($products->image, 300, 300)}}" alt="{{$products->name}}"></a>
        <p style="text-align: center"><strike >{{number_format($products->base_price,0,'','.')}}<sup>đ</sup></strike></p>
        <p style=" font-weight: bold;color: red;text-align: center">{{number_format($products->base_price,0,'','.')}}<sup>đ</sup></p>
        <h3 style="text-align: center" >
            <a   href="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getProductSlug($products) }}" title="{{$products->name}}">{{$products->name}}</a>
        </h3>
    </div>
@endif