<style>
    div#hpanel a.manuface{
        width: 23%;
        height: auto;
        padding: 5px;
        margin: 1%;
        border-radius: 10px;
        border: 1px solid #eee;
    }
    .f2-h{
        width: 25%;
    }
    .f2-topic{
        width: 69%;
    }
    .f2-topic li{
        width: 25%;
    }
    .f2-topic li:nth-child(4){
        margin-right: 0;
    }
    @media only screen and (max-width: 1300px) {
        .f2-hsx a img {
            height: auto;
        }
    }
    @media only screen and (max-width: 768px) {
        .f2-hsx a img {
            height: auto;
        }
        .hpanelwrap,.f2-topic p{height:unset !important;}
        .hpanelwrap #hpanel {
            display: inline-block;
            position: relative !important;
        }
    }
</style>
<div class="f f2-head">
    <div class="b">
        <div class="flexCol f2-h fl">
            <label>CÁC THƯƠNG HIỆU NỔI TIẾNG</label>
            <div class="f flexCol f2-hsx ">
                <div class="hpanelwrap" style="overflow-y:auto;">
                    <div id="hpanel" style="width:100%;position:absolute;left:0;top:0;height: 100%">
                        @php
                            $brans = \Modules\ThemeSTBD\Models\Manufacturer::where('status', 1)->whereIn('id', $bran_ids)->orderBy('order_no', 'asc')->get();
                        @endphp
                        @if(!empty($brans))
                            @foreach($brans as $bran)
                                <a class="manuface" data-value="{{$bran->id}}" title="{{$bran->name}}"
                                   href="{{route('cate.list',['slug' => $category->slug.'/'.$bran->slug])}}">
                                    <img class="lazy" data-src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumbNoCut($bran->image)}}"
                                         alt="{{$bran->image}}"/>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>

            </div>
            <script>
                $(document).ready(function () {
                    $('#viewfullsapo').click(function () {
                        $('#sapo').css('height', 'auto');
                        $(this).hide();
                    });
                });
            </script>
        </div>
        <div class="flexCol f2-topic fl" >
            <label>DANH MỤC {{mb_strtoupper($category->name)}}</label>
            <ul class="flexJus">
                @foreach($child_category as $cate)
                    <li>
                        <figure>
                            <div>
                                <h2>{{$cate->name}}</h2>
                                <a title="{{$cate->name}}" href="{{route('cate.list', ['slug' => $cate->slug])}}"
                                   target="_blank">Xem thêm</a>
                            </div>
                        </figure>
                        <p>
                            <img class="lazy" data-src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($cate->banner_sidebar, 400, null) }}"
                                 alt="{{$cate->name}}"/>
                        </p>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>