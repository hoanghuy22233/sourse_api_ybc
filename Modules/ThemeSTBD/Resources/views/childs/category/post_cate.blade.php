
@extends('themestbd::layouts.master')
@section('main_content')
    <style>
        .nav-menu .nav-item .nav-link{
            padding: 0.7rem 5px!important;
        }
        section {
            margin: auto!important;
        }
        /*phan trang*/
        .phantrang {
            float: left;
            width: 100%;
            padding: 15px 0;
            margin: 10px 0 0;
            text-align: center;
            border-top: 1px solid #ddd
        }
        header.f {
            margin-top: unset!important;
        }
        li.menu-san_pham:hover div#menu-show{
            top:38px!important;
            /*left: -12vw!important;*/
        }
        .phantrang li {
            display: inline-block
        }

        .phantrang li a {
            margin: 0 2px;
            padding: 5px 10px;
            border-radius: 50%
        }

        .phantrang li:hover a, .phantrang li.active a {
            background-color: #1d537f;
            color: #FFF
        }

        .phantrang li a {
            color: #000
        }


        h2,h2 a {font:bold 16px/30px arial;
        }
        h3,h3 a {font:bold 15px/30px arial;
        }
        .newsright{float:left;margin-bottom:10px;}
        #news{position:relative;margin-top:15px;width:100%;float:left;}
        #nleft{float:left;width:78%;padding-right:3%;}
        #nright{float:left;width:22%;display:flex;flex-direction:column;}

        .tt{width:100%;float:left;padding:10px 0;padding:0;}
        .tt li{width:100%;float:left;margin:15px 0;padding:10px 0px;border-top:1px solid #ddd;}
        .tt li .ttimg{width:190px;float:left;margin-right:20px;}

        .tt li:nth-child(1){padding:0 0 15px;border-top:none;margin-top:0;}
        .tt li:nth-child(1) .ttimg{width:290px;float:right;margin-left:20px;margin-right:0;}

        .tt li .ttimg img{width:100%;}
        .tt li div a,.other ul li div a{color:#ff6100}
        .tt li h3,.other ul li h3{margin:0 0 15px 0;}
        .tt li h3 a{font:bold 20px/24px arial;}
        .tt li p{font:13px/22px arial;color:#333;text-align:justify;}
        .tt li span,.other ul li span{font:12px/22px arial;color:#666;}
        .tt li span:before,.views:before,.clock:before{font:13px/1px FontAwesome;margin-right: 5px;color:#1d537f;}
        .clock:before,.tt li span:nth-child(1):before{content: '\f017';}
        .views:before,.tt li span:nth-child(2):before{content:"\f06e";}
        .tt li .more{font:13px/20px arial;float:right;color:#ff6000;}
        .tt li .more:after{content:"\f105";font:16px/1px FontAwesome;margin-left:5px;}

        .other label{font:22px/25px arial;display:block;color:#333;padding-bottom:15px;border-bottom:1px solid #ddd;margin-bottom:15px;}
        .other ul{padding:0;}
        .other ul li{width:49%;float:left;padding:5px 0px;min-height:70px;}
        .other ul li:nth-child(2n+1){margin-right:2%;}
        .other ul li div a{color:#ff6100}
        .other ul li a{font:bold 16px/20px arial;}

        .newsright label {display: block;font: bold 18px/45px arial;background: #ddd;text-align: center;}

        #menu_catalog a{text-decoration:none;}
        #menu_catalog ul{margin: 0;padding: 0;list-style: none;}
        #menu_catalog .level-1{border-bottom:1px solid #f0f0f0;position: relative;width:100%;float:left;}
        #menu_catalog .level-1:nth-last-child(1),#menu_catalog .level-1 ul li:nth-last-child(1){border-bottom:none;}
        #menu_catalog .alv1{padding-left:20px;font:bold 13px/40px arial;}
        #menu_catalog .alv1:hover,.a1act{color:#f76b1c !important;}
        #menu_catalog .level-1 ul{padding-left:22px;list-style-position: inside;border-top: 1px solid #f0f0f0;display:none;}
        #menu_catalog .level-1 ul li{line-height: 25px;border-bottom:1px solid #f0f0f0;}
        #menu_catalog .level-1 ul li a{padding-left: 7px;color: #555;font:13px/30px arial;}
        #menu_catalog .level-1 ul li a:hover,.a2act,.a3act{color:#f76b1c !important}
        #menu_catalog .level-1 ul li ul li{border-bottom:none}
        #menu_catalog .level-1 ul li ul li:before{content:"\f111";font:4px/1px FontAwesome;vertical-align:middle;color:#337ab7}
        #menu_catalog .pdd:after{content:"\f107";font:18px/40px FontAwesome;vertical-align:middle;margin-right:20px;float:right;}

        .bgghi{background:#ddd;}
        .news-right2{float:left;padding:0;width:100%;}
        .news-right2 li{padding:10px 15px;float:left;position:relative;width:100%;text-align:center}
        .news-right2 li a{font:bold 13px/20px arial; }
        .news-right2 li h3{margin-top:5px;}
        .news-right2 li span{font:bold 20px/25px arial;display:block;position:absolute;top:30%;right:8px;color:red;width:25px;height:25px; }
        .img-news{width:100%;overflow:hidden;display:block;}
        .img-news img{width:100%;}

        .news-list{float:left;padding:0;}
        .news-list li{padding:18px 40px 18px 10px;float:left;position:relative;border-bottom:1px solid #ddd;width:100%;}
        .news-list li:nth-last-child(1){border:none;}
        .news-list li a{font:13px/20px arial; }
        .news-list li span{font:bold 20px/25px arial;display:block;position:absolute;top:30%;right:8px;color:red;width:25px;height:25px; }

    </style>
{{--    @include('themestbd::partials.menu_master')--}}
    @if($category->id != '121')
        <section>
            @section('name', $category->name)
            @include('themestbd::partials.breadcrumb')
            <div id="news">
                <div id="nleft">
                    @if(!empty($categorysub))
                        <div class="f" style="padding:20px;background:#eee;margin-bottom:30px;border-bottom:1px solid #ddd;">
                            <h1>{{$categorysub->intro}}</h1>
                        </div>
                    @else
                        <div class="f" style="padding:20px;background:#eee;margin-bottom:30px;border-bottom:1px solid #ddd;">
                            <h1>{!! $category->intro !!}</h1>
                        </div>
                    @endif
                    <ul class="tt">
                        @if(!empty($posts))
                            @foreach($posts as $post)
                                <li>
                                    <a class="ttimg" href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getPostSlug($post) }}" title="{{$post->name}}">
                                        <img class="lazy" style="min-height: 100px;" data-src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($post->image, 300, null) }}" alt="{{$post->name}}" /></a>
                                    <h3><a href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getPostSlug($post) }}" title="{{$post->name}}">{{$post->name}}</a></h3>
                                    <p>{{str_limit($post->intro, 230)}}</p>
                                    <div><span>{{number_format($post->view_total, 0, '.', '.')}} lượt xem</span></div>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                    {{ @$posts->links() }}
                </div>
                <div id="nright">
                    @include('themestbd::partials.sidebar_posts')
                </div>
            </div>
            <script>
                $(document).ready(function () {
                    $('.pdd').click(function () {
                        $(this).next().slideToggle(500);
                        var cla = $(this).attr('class');
                        var newcla = ''
                        if (cla.indexOf('pddup') > -1) newcla = cla.replace(' pddup', ''); else newcla = cla + ' pddup';
                        $(this).attr('class', newcla);
                    });
                    $('.a1act').each(function () {
                        var cla = $(this).attr('class');

                        $(this).next().show();
                        var newcla = cla + ' pddup';
                        $(this).attr('class', newcla);
                    });
                    $('.a2act').each(function () {
                        var cla = $(this).attr('class');

                        $(this).next().show();
                        var newcla = cla + ' pddup';
                        $(this).attr('class', newcla);
                    });
                });
            </script>
        </section>
    @else
        <style>
            .polaroid {
                width: 21.7%;
                background-color: white;
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                float: left;
                margin: 0 21px 30px 22px;
            }

            .tts img {width: 100%}

            .container {
                text-align: center;
                padding: 10px 20px;
            }
        </style>
        <section>
            @include('themestbd::partials.breadcrumb')
            <div id="news">
                <div id="nleft" style="width: 100%; padding-right: 0px;">
                    @if(!empty($categorysub))
                        <div class="f" style="padding:20px;background:#eee;margin-bottom:30px;border-bottom:1px solid #ddd;">
                            <h1>{{$categorysub->name}}</h1>
                        </div>
                    @else
                        <div class="f" style="padding:20px;background:#eee;margin-bottom:30px;border-bottom:1px solid #ddd;">
                            <h1>{{$category->name}}</h1>
                        </div>
                    @endif
                    <ul class="tts">
                        @if(!empty($posts))
                            @foreach($posts as $post)
                                <li class="polaroid">
                                    <a class="ttimg" href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getPostSlug($post) }}" title="{{$post->name}}">
                                        <img class="lazy" style="min-height: 100px;" data-src="{{\Modules\ThemeSTBD\Http\Helpers\CommonHelper::getUrlImageThumb($post->image, 300, null) }}" alt="{{$post->name}}" /></a>
                                    <h3 class="container"><a href="{{ \Modules\ThemeSTBD\Http\Helpers\CommonHelper::getPostSlug($post) }}" title="{{$post->name}}">{{$post->name}}</a></h3>

                                </li>
                            @endforeach
                        @endif
                    </ul>
                    {{ @$posts->links() }}
                </div>
            </div>
            <script>
                $(document).ready(function () {
                    $('.pdd').click(function () {
                        $(this).next().slideToggle(500);
                        var cla = $(this).attr('class');
                        var newcla = ''
                        if (cla.indexOf('pddup') > -1) newcla = cla.replace(' pddup', ''); else newcla = cla + ' pddup';
                        $(this).attr('class', newcla);
                    });
                    $('.a1act').each(function () {
                        var cla = $(this).attr('class');

                        $(this).next().show();
                        var newcla = cla + ' pddup';
                        $(this).attr('class', newcla);
                    });
                    $('.a2act').each(function () {
                        var cla = $(this).attr('class');

                        $(this).next().show();
                        var newcla = cla + ' pddup';
                        $(this).attr('class', newcla);
                    });
                });
            </script>

        </section>
    @endif

@endsection
