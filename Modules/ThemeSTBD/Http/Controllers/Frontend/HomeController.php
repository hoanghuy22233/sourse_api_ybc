<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */

namespace Modules\ThemeSTBD\Http\Controllers\Frontend;
use App\Http\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use Modules\ThemeSTBD\Models\Category;
//use Modules\ThemeSTBD\Http\Helpers\CommonHelper;
use Modules\ThemeSTBD\Models\Contact;
use Modules\ThemeSTBD\Models\District;
use Modules\ThemeSTBD\Models\Manufacturer;
use Modules\ThemeSTBD\Models\Menu;
use Modules\ThemeSTBD\Models\NTPost;
use Modules\ThemeSTBD\Models\NTTerm;
use Modules\ThemeSTBD\Models\NTTermTaxonomy;
use Modules\ThemeSTBD\Models\Post;
use Modules\ThemeSTBD\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Modules\ThemeSTBD\Models\Banner;
use DB;

class HomeController extends Controller
{

    public function getIndex()
    {
        $data['posts'] = CommonHelper::getFromCache('get_post_home');
        if (!$data['posts']) {
            $data['posts'] = Post::where('status', 1)->where('show_home', 1)->where('type_page', '<>', 'page_static')->orderBy('order_no', 'desc')->take(11)->get();
            CommonHelper::putToCache('get_post_home', $data['posts']);
        }
        $data['category'] = CommonHelper::getFromCache('get_category_home');
        if (!$data['category']) {
            $data['category'] = Category::where('status', 1)->where('show_homepage', 1)->where('show_menu', 1)->orderBy('order_no', 'asc')->get();
            CommonHelper::putToCache('get_category_home', $data['category']);
        }
        return view('themestbd::home.index')->with($data);
    }

    public function getAllManufacturer()
    {
        $data['manufacturer'] = CommonHelper::getFromCache('get_all_manufacturer');
        if (!$data['manufacturer']) {
            $data['manufacturer'] = Manufacturer::where('status', 1)->get();
            CommonHelper::putToCache('get_all_manufacturer', $data['manufacturer']);
        }
        $pageOption = [
            'title' => 'Thương hiệu',
            'description' => 'Thương hiệu',
            'keywords' => 'Thương hiệu',
        ];
        view()->share('pageOption', $pageOption);
        return view('themestbd::childs.manufacturer.index')->with($data);
    }

    public function getManufacturer($slug, Request $request)
    {
        $data['get_manufacturer'] = CommonHelper::getFromCache('manufacturer_slug'.$slug);
        if (!$data['get_manufacturer']) {
            $data['get_manufacturer'] = Manufacturer::where('slug', $slug)->first();
            CommonHelper::putToCache('manufacturer_slug'.$slug, $data['get_manufacturer']);
        }


        $data['products'] = CommonHelper::getFromCache('product_manufacturery_id' . $data['get_manufacturer']->id);
        if (!$data['products']) {
            $data['products'] = Product::where('manufacture_id', $data['get_manufacturer']->id)->paginate(20);
            CommonHelper::putToCache('product_manufacturery_id' . $data['get_manufacturer']->id, $data['products']);
        }

        if (!empty($data['get_manufacturer'])) {
            $cate_id = explode('|', $data['get_manufacturer']->filers_category);
            $post_id = explode('|', $data['get_manufacturer']->filers_post);
            $data['manu_filers_cate'] = CommonHelper::getFromCache('get_cate_filers_by_manu' . $data['get_manufacturer']->id);
        }
        if (!$data['manu_filers_cate']) {
            $data['manu_filers_cate'] = Category::whereIn('id', $cate_id)
                ->where('status', 1)->where('type', 5)->get();
            CommonHelper::putToCache('get_cate_filers_by_manu' . $data['get_manufacturer']->id, $data['manu_filers_cate']);
        }

        $data['manu_filers_post'] = CommonHelper::getFromCache('get_post_filers_by_manu' . $data['get_manufacturer']->id);
        if (!$data['manu_filers_post']) {
            $data['manu_filers_post'] = Post::whereIn('id', $post_id)->get();
            CommonHelper::putToCache('get_post_filers_by_manu' . $data['get_manufacturer']->id, $data['manu_filers_post']);
        }

        $pageOption = [
            'title' => $data['get_manufacturer']->meta_title != '' ? $data['get_manufacturer']->meta_title : $data['get_manufacturer']->name,
            'description' => $data['get_manufacturer']->meta_description != '' ? $data['get_manufacturer']->meta_description : $data['get_manufacturer']->name,
            'keywords' => $data['get_manufacturer']->meta_keywords != '' ? $data['get_manufacturer']->meta_keywords : $data['get_manufacturer']->name,
            'robots' => $data['get_manufacturer']->meta_robot != '' ? $data['get_manufacturer']->meta_robot : $data['get_manufacturer']->name,
        ];
        view()->share('pageOption', $pageOption);
        return view('themestbd::childs.manufacturer.manufacturer_child')->with($data);
    }

    public function getSaleList()
    {
        $data['category'] = CommonHelper::getFromCache('get_all_cate_url_sale');
        if (!$data['category']) {
            $data['category'] = Category::where('status', 1)->where('type', 7)->get();
            CommonHelper::putToCache('get_all_cate_url_sale', $data['category']);
        }
        $pageOption = [
            'title' => 'Khuyến mãi',
            'description' => 'Khuyến mãi',
            'keywords' => 'Khuyến mãi',
        ];
        view()->share('pageOption', $pageOption);
        return view('themestbd::childs.saleproduct.index')->with($data);
    }

    public function cache_flush()
    {
        \Artisan::call('view:clear');
        \Artisan::call('cache:clear');
        \Cache::flush();
        return redirect('/');
    }

    public function trungSP()
    {
        $html = '<h3>Các sản phẩm trùng</h3><br>
    <table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Tên</th>
        <th>Mã</th>
        <th>Hành động</th>
    </tr>
    </thead>
    <tbody>';
        $products = Product::select(['id', 'name', 'code'])->orderBy('id', 'asc')->paginate(30);
        foreach ($products as $product) {
            $prds = Product::select(['id', 'name', 'code'])->where('code', $product->code)->where('id', '>', $product->id)->get();
            if (count($prds) > 0) {
                $html .= '<tr style="border: 1px solid #ccc; font-weight: bold;font-size: 18px;">
                                <td>'.$product->id.'</td>
                                <td>'.$product->name.'</td>
                                <td>'.$product->code.'</td>
                                <td><a href="/admin/edit_product/'.$product->id.'" target="_blank" style="color: blue;">Sửa</a><a href="/admin/delete_product/'.$product->id.'" target="_blank" style="color: red; margin-left: 10px;">Xóa</a></td>
                            </tr>';

                foreach ($prds as $p) {
                    $html .= '<tr>
                                <td>'.$p->id.'</td>
                                <td>'.$p->name.'</td>
                                <td>'.$p->code.'</td>
                                <td><a href="/admin/edit_product/'.$p->id.'" target="_blank" style="color: blue;">Sửa</a><a href="/admin/delete_product/'.$p->id.'" target="_blank" style="color: red; margin-left: 10px;">Xóa</a></td>
                            </tr>';
                }
            }
        }

        $html .= '</tbody>
</table>';
        $html .= $products->links();

        echo $html;
    }
    public function advisory(){
        $pageOption = [
            'title' => 'Tư vấn | Tin tức',
            'description' => 'Tư vấn | Tin tức',
            'keywords' => 'Tư vấn bếp, Tin tức bếp',
        ];
        $data['category'] = CommonHelper::getFromCache('menu_url_tu_van');
        if (! $data['category']) {
            $data['category'] = Menu::where('url','/tu-van')->where('status',1)->first();
            CommonHelper::putToCache('menu_url_tu_van',  $data['category']);
        }

        view()->share('pageOption', $pageOption);
        return view('themestbd::childs.category_post.category_post_all',$data);
    }
    public function updateSale(){
        $a = Product::where('base_price','<>','')->where('base_price','<>',Null)
                        ->where('final_price','<>','')->where('final_price','<>',Null)->get();
//        $a=Product::get();
        foreach ($a as $b){
            $c = Product::find($b->id);
            $c->sale = ceil(($c->base_price - $c->final_price)*100/ $c->base_price) .'%';
//            $c->sale = Null;
            $c->save();
        }

    }
}




