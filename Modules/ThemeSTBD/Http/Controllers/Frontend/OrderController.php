<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */

namespace Modules\ThemeSTBD\Http\Controllers\Frontend;
use App\Http\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use App\Mail\MailServer;
use App\Models\Setting;
use Modules\ThemeSTBD\Models\Bill;
use Modules\ThemeSTBD\Models\Category;
use Modules\ThemeSTBD\Models\District;
use Modules\ThemeSTBD\Models\Order;
use Modules\ThemeSTBD\Models\Product;
use Modules\ThemeSTBD\Models\Settings;
use Modules\ThemeSTBD\Models\Showroom;
use Modules\ThemeSTBD\Models\User;
use Auth;
use Illuminate\Http\Request;
use Mail;
use Session;

class OrderController extends Controller
{
    public function getIndex(Request $request)
    {

        if ($request->ajax()) {
            if (!$request->p) {
                $id = $request->data;
                if (!empty($id)) {
                    $ditric = District::where('province_id', $id)->get();
                    $html = '';
                    foreach ($ditric as $data) {
                        $html .= '<option value=' . $data->id . ' id=' . $data->id . '>' . $data->type . ' ' . $data->name . '</option>';
                    }
                    return response()->json($html);
                }
            }
        }
        $total = 0;
        $data['msg'] = Session()->get('msg');
        $data['cate'] = null;
        $getRoom = Showroom::all();
        $cart = Session()->get('cart');
        if ($cart === null) {
            session()->put('cart', null);
            Session::save();
        }
        if (!empty($cart)) {
            $id = end($cart);
            $data['product'] = Product::find($id['id']);
            $data['cate'] = Category::whereIn('id', explode('|', $data['product']->multi_cat))->first();
//            dd($data['cate']);
            if (@$data['cate']->parent_id > 0) {
                $data['cate'] = Category::where('id', @$data['cate']->parent_id)->first();
            }
        }

        if ($request->ajax()) {
            $data['product'] = Product::find($request->p);
            $type = $request->type;
            $item = [
                'id' => $data['product']->id,
                'name' => $data['product']->name,
                'price' => $data['product']->final_price,
                'image' => $data['product']->image,
                'multi_cat' => $data['product']->multi_cat,
                'gift' => $request->gift,
//                'gift_price_value' => $request->gift_price_value,
                'quantity' => 1
            ];

            $cart = Session::get('cart');
            if ($cart === null) {
                session()->push('cart', $item);
                Session::save();
            } else {
                $flag = false;
                for ($i = 0; $i < count($cart); $i++) {
                    if ($cart[$i]['id'] == $data['product']->id) {
                        if (empty($type)) {
                            $cart[$i]['quantity']++;
                            session()->put('cart', $cart);
                            Session::save();
                        } else {
                            if ($type === '+') {
                                $cart[$i]['quantity']++;
                            } elseif ($type === '-') {
                                if ($cart[$i]['quantity'] <= 1) {
                                    $cart[$i]['quantity'] = 1;
                                } else {
                                    $cart[$i]['quantity']--;
                                }
                            } else {
                                $cart[$i]['quantity'] = (int)$type;
                            }
                            session()->put('cart', $cart);
                            Session::save();
                        }
                        $flag = true;
                        break;
                    }
                }
                if ($flag == false) {
                    session()->push('cart', $item);
                    Session::save();
                }
            }
            return Response()->json([
                'success' => true,
                'count'=>count(session()->get('cart'))
            ]);
        }
        return view('themestbd::childs.order.view', ['cart' => $cart, 'total' => $total, 'data' => $data, 'getRoom' => $getRoom]);
    }

    public function getRemoveCart(Request $request)
    {
        if ($request->ajax()) {
            $id = $request['id'];
            $cart = session()->get('cart');
            for ($i = 0; $i < count($cart); $i++) {
                if ($id == $cart[$i]['id']) {
                    unset($cart[$i]);
                    session()->put('cart', array_values($cart));
                    Session::save();
                }
            }
            return Response()->json([
                'success' => true
            ]);
        }
    }

    public static function totalCart()
    {
        $countItemCart = '';
        $arrCart = session()->get('cart');
        if (!empty($arrCart)) {
            $countItemCart = count($arrCart);
        }
        return $countItemCart;
    }

    public function getDelivery()
    {
        if (Session::get('cart') == null) {
            $data['products'] = [];
        } else {
            $cart = Session::get('cart');
            $product_id_arr = array_keys($cart['items']);
            $data['products'] = Product::select(['id', 'name', 'slug', 'multi_cat', 'image', 'base_price', 'final_price'])
                ->where('status', 1)->whereIn('id', $product_id_arr)->get();
        }

        $pageOption = [
            'type' => 'page',
            'pageName' => 'Giao hàng',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('themestbd::childs.order.delivery')->with($data);
    }

    public function postDelivery(Request $request)
    {
        if (!Auth::check()) {
            return redirect()->route('user.login');
        }

        $data = $request->except('_token');
        Session::put('cart_delivery', $data);

        return redirect()->route('order.pay');
    }

    public function getPay()
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Thanh toán',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('themestbd::childs.order.pay');
    }

    function configMail()
    {
        $settings = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();
        if (isset($settings['driver'])) {
            $username = $settings['driver'] == 'mailgun' ? @$settings['mailgun_username'] : @$settings['smtp_username'];
            $config =
                [
                    'mail.from' => [
                        'address' => $username,
                        'name' => @$settings['mail_name'],
                    ],
                    'mail.driver' => @$settings['driver'],
                ];

            if ($settings['driver'] == 'mailgun') {
                $config['services.mailgun'] =
                    [
                        'domain' => trim(@$settings['mailgun_domain']),
                        'secret' => trim(@$settings['mailgun_secret']),
                    ];
                $config['mail.port'] = @$settings['mailgun_port'];
                $config ['mail.username'] = @$settings['mailgun_username'];
            } else {
                $config['mail.port'] = @$settings['smtp_port'];
                $config['mail.password'] = @$settings['smtp_password'];
                $config['mail.encryption'] = @$settings['smtp_encryption'];
                $config['mail.host'] = @$settings['smtp_host'];
                $config['mail.username'] = @$settings['smtp_username'];
            }
//            $config['services.onesignal'] = [
//                'app_id' => '420af10d-5030-4f34-af19-68078fd6467c',
//                'rest_api_key' => 'MTY0MjA5NTktNjgwNS00NGM3LTg3YmYtNzcwMmRhZDUyZmE2'
//            ];
//            config onesignal
//            dd($config);
            config($config);
        }
        return $settings;
    }

    public function createBill(Request $request)
    {
        $settings = $this->configMail();

        $getCart = session()->get('cart');
        $total_price = 0;
        $msg = 'Đặt hàng thành công! Chúng tôi sẽ sớm liên hệ cho bạn để giao hàng';
        if (!empty($getCart)) {
            $tel = $request->Phone;
            $user_email = $request->Email;
            $user = User::where('tel', base64_encode($tel))->orWhere('email', base64_encode($user_email))->first();
            if (empty($user)) {
                $user = User::create([
                    'name' => $request->Name,
                    'tel' => base64_encode($request->Phone),
                    'email' =>base64_encode($request->Email)
                ]);
            }

            foreach ($getCart as $value) {
                $total_price += $value['quantity'] * $value['price'];
            }
            $gift_list='';
            foreach ($getCart as $v) {
                $gift_list .= '|' . str_replace(',','|',$v['gift']);
            }
            $note='';
            $price_giftsss=0;
            if($request->price_giftsss){
                $price_giftsss = $request->price_giftsss;
                $note .= 'Cộng thêm tiền bù khi chọn quà: '.number_format($request->price_giftsss,0,'','.').'đ';
            }
            $bill = Bill::create([
                'user_id' => $user->id,
                'user_name' => $request->Name,
                'user_email' => base64_encode($request->Email),
                'user_tel' => base64_encode($request->Phone),
                'note' => $request->Note .$note,
                'total_price' => $total_price + $price_giftsss,
                'user_address' => $request->Address,
                'receipt_method' => $request->PayType,
                'user_gender' => $request->Sex,
                'user_city_id' => $request->user_city_id,
                'gift_list' => $gift_list,
                'user_district_id' => $request->user_district_id

            ]);

            $bill_id = $bill->id;

            foreach ($getCart as $item) {
                $order = Order::create([
                    'bill_id' => $bill_id,
                    'price' => $item['quantity'] * $item['price'],
                    'product_id' => $item['id'],
                    'quantity' => $item['quantity'],
                    'product_name' => $item['name'],
                    'product_price' => !empty($item['price'])?$item['price']:Null,
                    'product_image' => $item['image'],
                    'gift_list' => '|' . str_replace(',','|',$item['gift'])
                ]);
            }
            $admin_email = Settings::where('name', 'email_notifi')->first();
            $user_name = $request->Name;
            $user_email = base64_decode($user->email);
            $user_tel = base64_decode($user->tel);
            $receipt_method = $bill->receipt_method;

            $url_mail = route('send.mail');


            if (!empty($settings['mailgun_username']) || filter_var($settings['mailgun_username'], FILTER_VALIDATE_EMAIL)) {

                try {
                    $admin_emails = explode(',',$settings['admin_emails']);
                    foreach ($admin_emails as $admin_email) {
                        Mail::send(['html' => 'themestbd::childs.mails.mail_admin'], compact('url_mail', 'getCart', 'user_name', 'user_email','user_tel','receipt_method', 'total_price', 'request'), function ($message) use ($settings, $admin_email) {
                            $message->from($settings['smtp_username'], @$settings['mail_name']);
                            $message->to($admin_email, 'Admin');
                            $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] Đơn hàng mới');
                        });
                    }
                } catch (\Exception $ex) {
                    $msg .= '. Gửi mail cho admin không thành công.' . $ex->getMessage();
                }
            }
            if (!empty($user_email) || filter_var($url_mail, FILTER_VALIDATE_EMAIL)) {

                try {

                    Mail::send(['html' => 'themestbd::childs.mails.mail_oder'], compact('url_mail', 'getCart', 'user_name', 'user_email', 'total_price'), function ($message) use ($settings,$user_email, $user_name) {
                        $message->from($settings['mailgun_username'], $settings['mail_name']);
                        $message->to($user_email, $user_name);
                        $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] Xác nhận đơn hàng bạn đã đặt.');
                    });
                } catch (\Exception $ex) {
                    $msg .= '. Gửi mail hóa đơn cho khách hàng không thành công.' . $ex->getMessage();
                }
            }
            Session::forget('cart');
        }
        return redirect()->back()->with('msg', $msg);
    }


    public function pay_success()
    {
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Thanh toán thành công',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('themestbd::childs.order.pay_success');
    }
}
