<?php
namespace Modules\ThemeWorkartAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class TagPost extends Model
{

    protected $table = 'categories';
    public $timestamps = false;



    protected $fillable = [
        'name', 'slug','content', 'intro','image', 'parent_id','status', 'intro', 'content'
    ];



}
