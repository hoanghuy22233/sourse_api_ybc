<?php
namespace Modules\ThemeWorkartAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\JdesCategory\Models\CategoryProduct;

class Post extends Model
{

    protected $table = 'posts';
    public $timestamps = false;


    protected $fillable = [
        'name', 'slug', 'user_id', 'intro', 'content', 'status', 'image', 'category_id', 'video', 'order_no',
        'important', 'data', 'updated_at', 'tags', 'show_home'
    ];

    public function category() {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function admin() {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function product() {
        return $this->hasMany(Product::class, 'product_sidebar','id');
    }
}