<?php
namespace Modules\ThemeWorkartAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class LevelUser extends Model
{

    protected $table = 'level_users';
    public $timestamps = false;



    protected $fillable = [
        'name', 'number_point','discount_enjoyed',
    ];



}
