<?php
namespace Modules\ThemeWorkartAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{

    protected $table = 'banners';


    protected $fillable = [
        'name', 'intro', 'link', 'image', 'created_at', 'updated_at', 'location', 'status','order_no'
    ];



}
