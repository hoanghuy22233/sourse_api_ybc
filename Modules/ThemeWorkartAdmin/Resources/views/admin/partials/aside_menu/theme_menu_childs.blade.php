@if(in_array('theme', $permissions))
    <li class="kt-menu__item " aria-haspopup="true"><a
                href="/admin/theme/setting" class="kt-menu__link "><i
                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                    class="kt-menu__link-text">Setting theme</span></a></li>
@endif