@if(in_array('theme', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon-imac"></i>
                    </span><span class="kt-menu__link-text">{{ trans('themeworkartadmin::admin.theme') }}</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/theme?status=1" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">{{ trans('themeworkartadmin::admin.theme') }}</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/banner" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">{{ trans('themeworkartadmin::admin.banner') }}</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/widget" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">{{ trans('themeworkartadmin::admin.widget') }}</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/menu" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">{{ trans('themeworkartadmin::admin.menu') }}</span></a></li>
                {!! Eventy::filter('aside_menu.theme_menu_childs', '') !!}
            </ul>
        </div>
    </li>
@endif

@if(in_array('level_user_view', $permissions))
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/level_user"
                                                      class="kt-menu__link "><span
                    class="kt-menu__link-icon"><i class="flaticon-book"></i>
            </span><span class="kt-menu__link-text">Level user</span></a></li>
@endif