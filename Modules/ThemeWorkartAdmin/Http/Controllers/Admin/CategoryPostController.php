<?php

namespace Modules\ThemeWorkartAdmin\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeWorkartAdmin\Models\Category;
use Validator;

class CategoryPostController extends CURDBaseController
{

    protected $whereRaw = 'type in (1)';

    protected $module = [
        'code' => 'category_post',
        'table_name' => 'categories',
        'label' => 'Category post',
        'modal' => '\Modules\ThemeWorkartAdmin\Models\Category',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Image', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Name'],
            ['name' => 'slug', 'type' => 'text', 'label' => 'Slug'],
            ['name' => 'parent_id', 'type' => 'custom','td' => 'themeworkartadmin::list.td.parent_id', 'label' => 'Category parent'],
            ['name' => 'order_no', 'type' => 'text', 'label' => 'Order'],
            ['name' => 'category_product_id', 'type' => 'custom','td' => 'themeworkartadmin::list.td.category_product_id', 'label' => 'Category product'],
            ['name' => 'id', 'route_name' => 'post', 'model' => '\Modules\ThemeWorkartAdmin\Models\Post', 'type' => 'custom', 'td' => 'themeworkartadmin::list.td.count_product_by_category', 'label' => 'Number post', ],
            ['name' => 'show_homepage', 'type' => 'status', 'label' => 'Home'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Status'],
            ['name' => 'updated_at', 'type' => 'text', 'label' => 'Update at'],
            ['name' => 'id', 'type' => 'custom', 'td' => 'workartproduct::list.td.view_cate_frontend', 'label' => 'View'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Name'],
                ['name' => 'parent_id', 'type' => 'custom', 'field' => 'themeworkartadmin::form.fields.select_model_tree', 'class' => '', 'label' => 'Category parent', 'model' => \Modules\ThemeWorkartAdmin\Models\Category::class, 'where' => 'type = 1'],
                ['name' => 'category_product_id', 'type' => 'select2_model', 'class' => '', 'label' => 'List of related products', 'model' => Category::class,
                    'object' => 'category_post', 'where' => 'type in (5)', 'display_field' => 'name', 'display_field2' => 'id'],
                ['name' => 'intro', 'type' => 'textarea_editor', 'label' => 'Intro'],
//                ['name' => 'featured', 'type' => 'select', 'options' =>
//                    [
//                        0 => 'Ẩn',
//                        1 => 'Hiển thị trang chủ',
//                    ], 'class' => '', 'label' => 'Hiển thị trang chủ'],
                ['name' => 'show_homepage', 'type' => 'select', 'options' =>
                    [
                        0 => 'Ẩn',
                        1 => 'Hiển thị',
                    ], 'class' => '', 'label' => 'Show homepage'],
                ['name' => 'type', 'type' => 'select', 'options' =>
                    [
                        0 => 'Ẩn',
                        1 => 'Hiển thị',
                    ], 'class' => '', 'label' => 'Post home'],
                ['name' => 'status', 'type' => 'select', 'options' => [1 => 'Kích hoạt', 0 => 'Ẩn'], 'class' => 'validate_field', 'label' => 'Status', 'value' => '1'],
                ['name' => 'order_no', 'type' => 'number', 'class' => '', 'label' => 'Order', 'value' => 1],
            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Image'],
            ],

            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'Slug', 'des' => 'Đường dẫn sản phẩm trên thanh địa chỉ'],
                ['name' => 'meta_title', 'type' => 'text', 'label' => 'Meta title'],
                ['name' => 'meta_description', 'type' => 'text', 'label' => 'Meta description'],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
                ['name' => 'meta_robot', 'type' => 'select', 'options' =>
                    [
                        '' => 'Chọn Meta robots',
                        'noindex,nofollow' => 'noindex,nofollow',
                        'index,follow' => 'index,follow',
                        'index,nofollow' => 'index,nofollow',
                        'noindex,follow' => 'noindex,follow',
                    ],
                    'label' => 'Meta robots'],
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên danh mục',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'slug' => [
            'label' => 'Đường dẫn',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'intro' => [
            'label' => 'Chứa số bản ghi',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'order_no' => [
            'label' => 'Thứ tự',
            'type' => 'number',
            'query_type' => '='
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Ẩn',
                1 => 'Hiển thị',
            ]
        ],

    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('themeworkartadmin::category.list')->with($data);
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('themeworkartadmin::category.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Required to enter tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    $data['type'] = 1;
                    #

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);
                        $this->adminLog($request,$this->model,'add');
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'New successfully created!');
                    } else {
                        CommonHelper::one_time_message('error', 'Im new. Please reload the page and try again!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('themeworkartadmin::category.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Required to enter tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        $this->adminLog($request,$item,'edit');
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            $this->adminLog($request,$item,'publish');
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            $this->adminLog($request,$item,'delete');
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            $this->adminLog($request,$ids,'multi_delete');
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
