<?php

namespace Modules\ThemeWorkartAdmin\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;

class ProductController extends CURDBaseController
{
    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Sản phẩm',
        'modal' => '\Modules\WorkartProduct\Models\Product',
    ];
}
