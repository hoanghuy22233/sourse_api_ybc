<?php

namespace Modules\ThemeWorkartAdmin\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;

class WidgetController extends CURDBaseController
{
    protected $orderByRaw = 'status desc, order_no desc, id desc';

    protected $module = [
        'code' => 'widget',
        'table_name' => 'widgets',
        'label' => 'Widget',
        'modal' => '\Modules\ThemeWorkartAdmin\Models\Widget',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Name'],
            ['name' => 'location', 'type' => 'select', 'label' => 'location', 'options' => [
                'home0' => 'Widget trang chủ 1',
                'home1' => 'Widget trang chủ 2',
                'footer_0' => 'Widget footer 1',
                'footer_1' => 'Widget footer 2',
                'footer_2' => 'Widget footer 3',
                'footer_3' => 'Widget footer 4',
                'footer_bot_1' => 'Widget footer bot 1',
                'footer_bot_2' => 'Widget footer bot 2',
                'user0' => 'Widget khách hàng',
                'facebook0' => 'Widget facebook',
                'faq0' => 'Widget faq 1',
                'faq1' => 'Widget faq 2',
                'faq2' => 'Widget faq 3',
                'product0' => 'Widget chi tiết sản phẩm 1',
                'product1' => 'Widget chi tiết sản phẩm 2',
                'product2' => 'Widget chi tiết sản phẩm 3',
                'title1' => 'Widget header sản phẩm',
                'title2' => 'Widget header giỏ hàng',
                'title3' => 'Widget header thanh toán',
                'title4' => 'Widget header hỏi đáp',
                'title5' => 'Widget header liên hệ',
                'title6' => 'Widget header tin tức',
                'title7' => 'Widget header công ty',
                'background_image' => 'Background image',
                'sp_suportOnline' => 'SP - Tổng đài tư vấn miễn phí',
                'tongdai' => 'SĐT Tổng đài - Menu',
                'post_detail_sidebar_right' => 'Sidebar chi tiết tin',
                'category_post_sidebar_right' => 'Sidebar DS tin',
                'home_top_address' => 'Trang chủ - Khối địa chỉ dưới banner',
                'fixed_right_contact' => 'Khối liên hệ fixed',
            ],],
            ['name' => 'type', 'type' => 'select', 'label' => 'type', 'options' => [
                'html' => 'Html',
                'textarea' => 'Textarea',
                'banner_slides' => 'Banner - slides',
                'posts' => 'Khối tin tức',
                'other' => 'Khác',
            ],],
            ['name' => 'order_no', 'type' => 'text', 'label' => 'order'],
            ['name' => 'status', 'type' => 'status', 'label' => 'status'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => '', 'label' => 'name'],
                ['name' => 'content', 'type' => 'custom', 'field' => 'themeworkartadmin::form.fields.widget_content', 'label' => 'content',
                    'inner' => 'rows=20', 'height' => '300px'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'order', 'value' => 0, 'group_class' => 'col-md-4'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'status', 'value' => 1, 'group_class' => 'col-md-4'],
            ],

            'info_tab' => [
                ['name' => 'location', 'type' => 'select', 'options' => [
                    'home0' => 'Widget trang chủ 1',
                    'home1' => 'Widget trang chủ 2',
                    'footer_0' => 'Widget footer 1',
                    'footer_1' => 'Widget footer 2',
                    'footer_2' => 'Widget footer 3',
                    'footer_3' => 'Widget footer 4',
                    'footer_bot_1' => 'Widget footer bot 1',
                    'footer_bot_2' => 'Widget footer bot 2',
                    'user0' => 'Widget khách hàng',
                    'facebook0' => 'Widget facebook',
                    'faq0' => 'Widget faq 1',
                    'faq1' => 'Widget faq 2',
                    'faq2' => 'Widget faq 3',
                    'product0' => 'Widget chi tiết sản phẩm 1',
                    'product1' => 'Widget chi tiết sản phẩm 2',
                    'product2' => 'Widget chi tiết sản phẩm 3',
                    'title1' => 'Widget header sản phẩm',
                    'title2' => 'Widget header giỏ hàng',
                    'title3' => 'Widget header thanh toán',
                    'title4' => 'Widget header hỏi đáp',
                    'title5' => 'Widget header liên hệ',
                    'title6' => 'Widget header tin tức',
                    'title7' => 'Widget header công ty',
                    'background_image' => 'Background image',
                    'sp_suportOnline' => 'SP - Tổng đài tư vấn miễn phí',
                    'tongdai' => 'SĐT Tổng đài - Menu',
                    'post_detail_sidebar_right' => 'Sidebar chi tiết tin',
                    'category_post_sidebar_right' => 'Sidebar DS tin',
                    'home_top_address' => 'Trang chủ - Khối địa chỉ dưới banner',
                    'fixed_right_contact' => 'Khối liên hệ fixed',
                ], 'label' => 'location'],
                ['name' => 'type', 'type' => 'select', 'options' => [
                    'html' => 'Html',
                    'textarea' => 'Textarea',
                    'banner_slides' => 'Banner - slides',
                    'posts' => 'Khối tin tức',
                    'other' => 'Khác',
                ], 'label' => 'type'],
//                ['name' => 'config', 'type' => 'textarea', 'label' => 'Cấu hình'],
                ['name' => 'config', 'type' => 'custom', 'field' => 'themeworkartadmin::form.fields.dynamic', 'class' => 'form-action', 'label' => 'config', 'cols' => ['key', 'Giá trị'], 'des' => 'Key bao gồm: view, location, limit, category_id, where']
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'location' => [
            'label' => 'Vị trí',
            'type' => 'select',
            'options' => [
                '' => 'Vị trí',
                'home0' => 'Widget trang chủ 1',
                'home1' => 'Widget trang chủ 2',
                'footer_0' => 'Widget footer 1',
                'footer_1' => 'Widget footer 2',
                'footer_2' => 'Widget footer 3',
                'footer_3' => 'Widget footer 4',
                'footer_bot_1' => 'Widget footer bot 1',
                'footer_bot_2' => 'Widget footer bot 2',
                'user0' => 'Widget khách hàng',
                'facebook0' => 'Widget facebook',
                'faq0' => 'Widget faq 1',
                'faq1' => 'Widget faq 2',
                'faq2' => 'Widget faq 3',
                'product0' => 'Widget chi tiết sản phẩm 1',
                'product1' => 'Widget chi tiết sản phẩm 2',
                'product2' => 'Widget chi tiết sản phẩm 3',
                'title1' => 'Widget header sản phẩm',
                'title2' => 'Widget header giỏ hàng',
                'title3' => 'Widget header thanh toán',
                'title4' => 'Widget header hỏi đáp',
                'title5' => 'Widget header liên hệ',
                'title6' => 'Widget header tin tức',
                'title7' => 'Widget header công ty',
                'background_image' => 'Background image',
                'sp_suportOnline' => 'SP - Tổng đài tư vấn miễn phí',
                'tongdai' => 'SĐT Tổng đài - Menu',
                'post_detail_sidebar_right' => 'Sidebar chi tiết tin',
                'category_post_sidebar_right' => 'Sidebar DS tin',
                'home_top_address' => 'Trang chủ - Khối địa chỉ dưới banner',
                'fixed_right_contact' => 'Khối liên hệ fixed',
            ],
            'query_type' => '='
        ],
        'type' => [
            'label' => 'Loại',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Loại',
                'html' => 'Html',
                'textarea' => 'Textarea',
                'banner_slides' => 'Banner - slides',
                'posts' => 'Khối tin tức',
                'other' => 'Khác',
            ]
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Ẩn',
                1 => 'Hiển thị',
            ],
            'query_type' => '='
        ],
    ];

    public function getIndex(Request $r)
    {
        

        $data = $this->getDataList($r);

        return view('themeworkartadmin::widget.list')->with($data);
    }

    public function add(Request $r)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($r);
                return view('themeworkartadmin::widget.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($r->all(), [
                    'location' => 'required'
                ], [
                    'location.required' => 'Required to enter vị trí',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($r, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    if ($r->has('config_key')) {
                        $val = [];
                        foreach ($r->config_key as $k => $key) {
                            if ($key != null && $r->config_value[$k] != null) {
                                $val[$key] = $r->config_value[$k];
                            }
                        }
                        $data['config'] = json_encode($val);
                    }

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        $this->afterAddLog($r, $this->model);

                        CommonHelper::one_time_message('success', 'New successfully created!');
                    } else {
                        CommonHelper::one_time_message('error', 'Im new. Please reload the page and try again!');
                    }

                    if ($r->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($r->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($r->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $r)
    {
        try {
                

            $item = $this->model->find($r->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($r, $item);
                return view('themeworkartadmin::widget.edit')->with($data);
            } else if ($_POST) {

                    

                $validator = Validator::make($r->all(), [
                    'location' => 'required'
                ], [
                    'location.required' => 'Required to enter vị trí',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($r, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    if ($r->has('config_key')) {
                        $val = [];
                        foreach ($r->config_key as $k => $key) {
                            if ($key != null && $r->config_value[$k] != null) {
                                $val[$key] = $r->config_value[$k];
                            }
                        }
                        $data['config'] = json_encode($val);
                    }

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($r->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($r->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($r->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            dd($ex->getMessage());
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $r)
    {
        try {

                

            $id = $r->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$r->column} == 0)
                $item->{$r->column} = 1;
            else
                $item->{$r->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$r->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $r)
    {
        try {

                

            $item = $this->model->find($r->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $r)
    {
        try {

                

            $ids = $r->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
}
