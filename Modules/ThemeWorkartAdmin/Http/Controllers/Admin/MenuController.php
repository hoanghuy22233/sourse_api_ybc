<?php

namespace Modules\ThemeWorkartAdmin\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeWorkartAdmin\Models\Menu;
use Validator;

class MenuController extends CURDBaseController
{

    protected $orderByRaw = 'status desc, id desc';

    protected $module = [
        'code' => 'menu',
        'table_name' => 'menus',
        'label' => 'Menu',
        'modal' => '\Modules\ThemeWorkartAdmin\Models\Menu',
        'list' => [
            ['name' => 'name', 'type' => 'name', 'label' => 'name'],
            ['name' => 'url', 'type' => 'text', 'label' => 'url'],
            ['name' => 'parent_id', 'type' => 'custom','td' => 'themeworkartadmin::list.td.menu.parent_id', 'label' => 'Category parent'],
            ['name' => 'type', 'type' => 'text', 'label' => 'type'],
            ['name' => 'order_no', 'type' => 'text', 'label' => 'order'],
            ['name' => 'location', 'type' => 'text', 'label' => 'location'],
            ['name' => 'status', 'type' => 'status', 'label' => 'status'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Name'],
                ['name' => 'url', 'type' => 'text', 'class' => '', 'label' => 'URL'],
                ['name' => 'location', 'type' => 'select', 'options' =>
                    [
                        'main_menu' => 'Menu chính',
                        'sidebar_menu_post' => 'sidebar tin tức'
                    ], 'class' => 'required', 'label' => 'location'],
                ['name' => 'parent_id', 'type' => 'custom', 'field' => 'themeworkartadmin::form.fields.select_model_tree', 'class' => '', 'label' => 'Menu parent', 'model' => Menu::class, 'where' => 'status = 1'],
//                ['name' => 'parent_id', 'type' => 'custom', 'field' => 'themeworkartadmin::form.fields.select_model_tree', 'class' => '', 'label' => 'Danh mục cha', 'model' => Menu::class, 'where' => 'status = 1'],
                ['name' => 'type', 'type' => 'select', 'options' =>
                    [
                        /*'category' => 'Danh mục',
                        'product' => 'Sản phẩm',
                        'post' => 'Bài viết',
                        'page' => 'Trang tĩnh',*/
                        'url' => 'URL',
                    ], 'class' => 'required', 'label' => 'Menu type'],
//            ['name' => 'item_id', 'type' => 'text', 'class' => '', 'label' => 'Id danh mục'],
                ['name' => 'order_no', 'type' => 'number', 'class' => '', 'label' => 'Order', 'value' => 0]
//                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên'],
//                ['name' => 'type', 'type' => 'select', 'options' =>
//                    [
//                        'category' => 'Danh mục',
//                        'product' => 'Sản phẩm',
//                        'post' => 'Bài viết',
//                        'page' => 'Trang tĩnh',
//                        'url' => 'Đường dẫn',
//                    ], 'class' => 'required', 'label' => 'Menu dẫn tới'],
//                ['name' => 'parent_id', 'type' => 'custom', 'field' => 'themeworkartadmin::form.fields.select_model_tree', 'class' => '', 'label' => 'Danh mục cha', 'model' => \Modules\ThemeWorkartAdmin\Models\Category::class, 'where' => 'type = 1'],
////
//                ['name' => 'intro', 'type' => 'textarea_editor', 'label' => 'Thông tin'],
//                ['name' => 'order_no', 'type' => 'number', 'class' => '', 'label' => 'Thứ tự (Số lớn hiển thị trước)', 'value' => 0]
            ],

            'info_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Image icon'],
            ],

        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên menu',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'location' => [
            'label' => 'Vị trí',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Vị trí',
                'main_menu' => 'Menu chính',
                'sidebar_menu_post' => 'sidebar tin tức'
            ]
        ],
        'order_no' => [
            'label' => 'Thứ tự',
            'type' => 'number',
            'query_type' => '='
        ],
    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);

        return view('themeworkartadmin::menu.list')->with($data);
    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('themeworkartadmin::menu.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Required to enter tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;


                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);
                        $this->adminLog($request,$this->model,'add');
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'New successfully created!');
                    } else {
                        CommonHelper::one_time_message('error', 'Im new. Please reload the page and try again!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);
            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);

                return view('themeworkartadmin::menu.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Required to enter tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;


                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        $this->adminLog($request,$item,'edit');
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            $this->adminLog($request,$item,'publish');
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            $this->adminLog($request,$item,'delete');
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            $this->adminLog($request,$ids,'multi_delete');
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
