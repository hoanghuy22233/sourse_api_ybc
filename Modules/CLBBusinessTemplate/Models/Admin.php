<?php

namespace Modules\CLBBusinessTemplate\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{

    protected $table = 'admin';

    protected $guarded = [];

}
