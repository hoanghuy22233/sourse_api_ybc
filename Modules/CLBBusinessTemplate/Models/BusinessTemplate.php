<?php

namespace Modules\CLBBusinessTemplate\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessTemplate extends Model
{
    protected $table = 'business_templates';
    protected $guarded = [];
//    protected $fillable = [
//        'admin_id','category_id',
//    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'multi_cat');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'lecturer_id');
    }

}
