<?php

namespace Modules\CLBBusinessTemplate\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = 'categories';

    protected $guarded = [];

    public function parent()
    {
        return $this->hasOne($this, 'id', 'parent_id');
    }

}
