<li class="kt-menu__section ">
    <h4 class="kt-menu__section-text">Trang ngoài</h4>
    <i class="kt-menu__section-icon flaticon-more-v2"></i>
</li>
@if(in_array('course_view', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon2-open-text-book"></i>
                    </span><span class="kt-menu__link-text">Khóa học</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/course" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Tất cả khóa học</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/course/add" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Tạo khóa học mới</span></a>
                </li>
                {{--<li class="kt-menu__item " aria-haspopup="true"><a--}}
                            {{--href="/admin/quizzes" class="kt-menu__link "><i--}}
                                {{--class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span--}}
                                {{--class="kt-menu__link-text">Bài kiểm tra</span></a></li>--}}
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/tag_course" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Thẻ</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/category" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Chuyên mục</span></a></li>
            </ul>
        </div>
    </li>
@endif
@if(in_array('quizzes_view', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon-clipboard"></i>
                    </span><span class="kt-menu__link-text">Bài kiểm tra</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/quizzes" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Tất cả bài kiểm tra</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/quizzes/add" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Tạo bài kiểm tra</span></a>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/category_quizz" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Chuyên mục</span></a></li>
            </ul>
        </div>
    </li>
@endif