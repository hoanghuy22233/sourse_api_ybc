<?php
//if (isset($_GET['course_id'])){
//
//}else{
//    $_GET['course_id']
//}
//dd(\Modules\CLBBusinessTemplate\Models\Lesson::find($_GET['lesson_id'])->course);
if (!isset($_GET['course_id']) && isset($result)) {
    $_GET['course_id'] = $result->id;
    $course = $result;
} else {
    $course = \Modules\CLBBusinessTemplate\Models\Course::find($_GET['course_id']);
}


?>
<div class="row">
    <div class="col-md-12">
        <p class="" style="margin-left: 5%;">
            <a href="{{ '/admin/course/'. @$_GET['course_id']}}"
               class="{{$module['code'] == 'course'  ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon-list"></i>Khóa học</a>
            @if($module['code'] == 'lesson')
            <a href="/admin/lesson?course_id={{ @$_GET['course_id']}}"
               class="{{$module['code'] == 'lesson' ? 'active' : '' }} mb-2 icon-header btn btn-success btn-elevate btn-pill btn-elevate-air btn-sm"><i
                        class="flaticon2-drop"></i>Bài học</a>
            @endif

        </p>
    </div>
</div>
{{--@if($season->status == 0 && $season->admin_id == \Auth::guard('admin')->user()->id)--}}
    {{--<div class="col-md-12">--}}
        {{--<div class="alert alert-solid-danger alert-bold" role="alert">--}}
            {{--<div class="alert-text">Mùa vụ này đã kết thúc! Không thể chỉnh sửa. Để mở lại mùa vụ này, vui lòng <a href="/admin/harvest/edit-last-item?season_id={{ $season->id }}">Click Vào Đây</a> để sửa lại option "Sau khi thu hoạch" thành "Trồng tiếp nhưng theo chu kỳ mới"</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--@endif--}}

