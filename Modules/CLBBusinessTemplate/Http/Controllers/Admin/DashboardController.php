<?php

namespace Modules\CLBBusinessTemplate\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Auth;
use DB;
use Illuminate\Http\Request;
use Mail;

class DashboardController extends Controller
{
    protected $module = [
    ];

    public function dashboard()
    {
        $data['page_title'] = 'Thống kê';
        $data['page_type'] = 'list';

//        $data['total_users'] = User::select('id')->get()->count();
//        $data['total_cards'] = Card::select('id')->get()->count();
//        $data['money_add'] = User::select('total_money')->get()->sum('total_money');
//        $data['money_used'] = Bill::select('total_price')->get()->sum('total_price');


        return view('clbbusinesstemplate::dashboard.dashboard', $data);
    }

    public function search(request $r)
    {
        $data['page_title'] = 'Thống kê';
        $data['page_type'] = 'list';
        $data['start']=$r->date_start;
        $data['end']=$r->date_end;
        if (empty($r->date_start) || empty($r->date_end)) {
            $data['total_users_search'] = User::select('id')->get()->count();
        } else {
            $data['total_users_search'] = User::whereDate('created_at', '>=', $r->date_start)->whereDate('created_at', '<=', $r->date_end)->get()->count();
        }

        if (empty($r->date_start) || empty($r->date_end)) {
            $data['total_cards_search'] = Card::select('id')->get()->count();
        } else {
            $data['total_cards_search'] = Card::whereDate('created_at','>=',$r->date_start)->whereDate('created_at','<=',$r->date_end)->get()->count();
        }

        if (empty($r->date_start) || empty($r->date_end)) {
            $data['money_add_search'] = User::select('total_money')->get()->sum('total_money');
        } else {
            $data['money_add_search'] = User::whereDate('created_at', '>=', $r->date_start)->whereDate('created_at', '<=', $r->date_end)->get()->sum('total_money');
        }

        if (empty($r->date_start) || empty($r->date_end)) {
            $data['money_used_search'] = Bill::select('total_price')->get()->sum('total_price');
        } else {
            $data['money_used_search'] = Bill::whereDate('created_at', '>=', $r->date_start)->whereDate('created_at', '<=', $r->date_end)->get()->sum('total_price');
        }


        return view('cardbill::dashboard_search', $data);
    }

    public function tooltipInfo(Request $request)
    {
        $modal = new $request->modal;
        $data['item'] = $modal->find($request->id);
        $data['tooltip_info'] = $request->tooltip_info;

        return view('admin.common.modal.tooltip_info', $data);
    }

    public function ajax_up_file(Request $request)
    {
        if ($request->has('file')) {
            $file = CommonHelper::saveFile($request->file('file'));
        }
        return $file;
    }
}
