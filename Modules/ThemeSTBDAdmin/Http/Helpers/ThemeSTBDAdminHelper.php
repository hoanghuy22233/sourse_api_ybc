<?php

namespace Modules\ThemeSTBDAdmin\Http\Helpers;

use App\Models\Category;
use App\Models\Meta;
use Auth;
use Modules\EworkingCompany\Models\Company;
use Modules\EworkingJob\Models\Job;
use Modules\EworkingJob\Models\Task;
use Session;
use View;

class ThemeSTBDAdminHelper
{

    public static function getPostSlug($post, $type = 'object')
    {
        try {
            $slug = '';
            $catPost = \Modules\ThemeSTBDAdmin\Models\Category::select(['slug', 'category_product_id'])->whereIn('id', explode('|', $post->multi_cat))->first();

            if (is_object($catPost)) {
                $catProduct = \Modules\ThemeSTBDAdmin\Models\Category::select('slug')->where('id', $catPost->category_product_id)->first();
                if (isset($catProduct) && is_object($catProduct)) {

                    $slug .= '/' . $catProduct->slug;
                }else{
                    $slug .= '/' . $catPost->slug;
                }
            }
            return $slug . '/' . $post->slug . '.html';
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }
}