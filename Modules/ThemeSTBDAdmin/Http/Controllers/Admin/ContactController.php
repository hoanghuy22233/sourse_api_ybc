<?php

namespace Modules\ThemeSTBDAdmin\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;

class ContactController extends CURDBaseController
{
    protected $module = [
        'code' => 'contact',
        'table_name' => 'contacts',
        'label' => 'Liên hệ',
        'modal' => '\Modules\ThemeSTBDAdmin\Models\Contact',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'tel', 'type' => 'custom','td' => 'theme::list.td.tel_email_decode', 'label' => 'SĐT'],
            ['name' => 'email', 'type' => 'custom','td' => 'theme::list.td.tel_email_decode', 'label' => 'Email'],
            ['name' => 'id', 'type' => 'relation', 'object' => 'product', 'display_field' => 'name', 'label' => 'Sản phẩm quan tâm tới',],
            ['name' => 'content', 'type' => 'text', 'label' => 'Nội dung'],
            ['name' => 'content', 'type' => 'custom','td' => 'theme::list.td.form_contact', 'label' => 'Form'],
            ['name' => 'province', 'type' => 'custom','td' => 'theme::list.td.user_city_id', 'label' => 'Tỉnh/Thành'],
            ['name' => 'distric', 'type' => 'custom','td' => 'theme::list.td.user_district_id', 'label' => 'Quận/Huyện'],
            ['name' => 'url', 'type' => 'text', 'label' => 'URL'],
            ['name' => 'updated_at', 'type' => 'custom','td' => 'theme::list.td.hour', 'label' => 'Giờ'],
            ['name' => 'updated_at', 'type' => 'date_vi', 'label' => 'Ngày đặt'],
            ['name' => 'user_ip', 'type' => 'text', 'label' => 'IP'],
//            ['name' => 'status', 'type' => 'status', 'label' => 'Kích hoạt'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => '', 'label' => 'Tên', 'group_class' => 'col-md-6'],
                ['name' => 'tel', 'type' => 'custom', 'field' => 'theme::form.fields.base64_decode', 'label' => 'Điện thoại', 'group_class' => 'col-md-6'],
                ['name' => 'email', 'type' => 'custom', 'field' => 'theme::form.fields.base64_decode', 'label' => 'Email', 'group_class' => 'col-md-6'],
                ['name' => 'gender', 'type' => 'select', 'options' => [
                    '0' => 'Nam',
                    '1' => 'Nữ',
                    '2' => 'Khác',
                ], 'label' => 'Giới tính', 'group_class' => 'col-md-6'],
                ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ'],
                ['name' => 'url', 'type' => 'text', 'class' => '', 'label' => 'URL', 'group_class' => 'col-md-6'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Đã kích hoạt','value' => 1, 'group_class' => 'col-md-6'],
            ],
            'info_tab' => [
                ['name' => 'content', 'type' => 'textarea', 'label' => 'Nội dung'],
                ['name' => 'product_id', 'type' => 'text', 'label' => 'ID Sản phẩm quan tâm'],
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'tel' => [
            'label' => 'Điện thoại',
            'type' => 'number',
            'query_type' => 'like'
        ],
        'email' => [
            'label' => 'Email',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Khóa',
                1 => 'Đã kích hoạt',
            ]
        ],
        'type' => [
            'label' => 'Loại',
            'type' => 'select',
            'query_type' => 'custom',
            'options' => [
                '' => 'Loại form',
                'Cần tư vấn' => 'Cần tư vấn',
                'Nhận ưu đãi' => 'Nhận ưu đãi',
                'Tư vấn sản phẩm' => 'Tư vấn sản phẩm',
                'dịch vụ xem hàng tại nhà' => 'Dịch vụ xem hàng tại nhà',
                'Dịch vụ trả góp' => 'Dịch vụ trả góp',
                'Dịch vụ khảo sát' => 'Dịch vụ khảo sát',
                'Dịch vụ bảo dưỡng' => 'Dịch vụ bảo dưỡng',
                'Dịch vụ lắp đặt' => 'Dịch vụ lắp đặt',
                'Dịch vụ sửa chữa' => 'Dịch vụ sửa chữa',
                'Form email' => 'Form email',
                'Góc phải' => 'Góc phải',
            ]
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);
        return view('theme::contact.list')->with($data);
    }
    public function appendWhere($query, $request)
    {
        if ($request->has('type')) {
            $query = $query->where('content', 'like', '%'.$request->type.'%');
        }

        return $query;
    }
    public function add(Request $request)
    {
        try {

            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('theme::contact.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [

                ], [
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);
                        $this->adminLog($request,$this->model,'add');
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('theme::contact.edit')->with($data);
            } else if ($_POST) {

                

                $validator = Validator::make($request->all(), [
                ], [
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        $this->adminLog($request,$item,'edit');
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {

                

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            $this->adminLog($request,$item,'publish');
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false,
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {

            

            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            $this->adminLog($request,$item,'delete');
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            $this->adminLog($request,$ids,'multi_delete');
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
