<?php

namespace Modules\ThemeSTBDAdmin\Providers;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ThemeSTBDAdminServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            // Cấu hình core
//            $this->registerTranslations();
//            $this->registerConfig();
            $this->registerViews();
//            $this->registerFactories();
//            $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');


            //  Cấu hình menu trái
            $this->rendAsideMenu();

            //  Đăng ký quyền
            $this->registerPermission();
        } else {
            $this->settings();
        }
    }

    public function settings()
    {
        $settings = CommonHelper::getFromCache('settings_frontend');
        if (!$settings) {
            $settings = Setting::whereIn('type', ['general_tab', 'common_tab', 'homepage_tab', 'product_page_tab'])->pluck('value', 'name')->toArray();
            CommonHelper::putToCache('settings_frontend', $settings);
        }

        \View::share('settings', $settings);
        return $settings;
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['theme']);
            return $per_check;
        }, 1, 1);
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['banner_view', 'banner_add', 'banner_edit', 'banner_delete', 'banner_publish',]);
            return $per_check;
        }, 1, 1);
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['menu_view', 'menu_add', 'menu_edit', 'menu_delete', 'menu_publish',]);
            return $per_check;
        }, 1, 1);
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['contact_view', 'contact_add', 'contact_edit', 'contact_delete',]);
            return $per_check;
        }, 1, 1);
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['post_view', 'post_add', 'post_edit', 'post_delete', 'post_publish',
                'category_post_view', 'category_post_add', 'category_post_edit', 'category_post_delete', 'category_post_publish',
                'tag_post_view', 'tag_post_add', 'tag_post_edit', 'tag_post_delete', 'tag_post_publish',]);
            return $per_check;
        }, 1, 1);

        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['widget_view', 'widget_add', 'widget_edit', 'widget_delete', 'widget_publish',]);
            return $per_check;
        }, 1, 1);


    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('theme::partials.aside_menu.dashboard_after_contact');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('theme::partials.aside_menu.dashboard_after_post');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('theme::partials.aside_menu.aside_menu_dashboard_after');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.theme_menu_childs', function () {
            print view('theme::admin.partials.aside_menu.theme_menu_childs');
        }, 100, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('theme.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'theme'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/themestbdadmin');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/themestbdadmin';
        }, \Config::get('view.paths')), [$sourcePath]), 'theme');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/themestbdadmin');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'theme');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'theme');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
