<label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)
        <span class="color_btd">*</span>@endif</label>
<div class="col-xs-12">
    <input type="text" name="{{ @$field['name'] }}" class="form-control {{ @$field['class'] }}"
           id="{{ $field['name'] }}" {!! @$field['inner'] !!}
           value="{{ old($field['name']) != null ? old($field['name']) : base64_decode(@$field['value']) }}"
           {{ strpos(@$field['class'], 'require') !== false ? 'required' : '' }}
           placeholder="{{ trans(@$field['label']) }}"
    >
    <span class="form-text text-muted">{!! @$field['des'] !!}</span>
    <span class="text-danger">{{ $errors->first($field['name']) }}</span>
</div>