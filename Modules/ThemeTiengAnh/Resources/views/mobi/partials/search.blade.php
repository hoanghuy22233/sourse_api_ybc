<style>
    .search-post.active {
        opacity: 0;
        visibility: hidden;
        transform: scale(0);
    }
    .search-post.show {
        opacity: 1;
        visibility: visible;
        transform: translateY(0px);
    }

    #search-post-mb {
        height:50px;
    }

    #search-area-mb {
        left: 48%;
    }

    #select-do-kho-mb {
        background: transparent none repeat scroll 0 0;
        border: 1px solid #737373;
        border-radius: 5px;
        color: inherit;
        padding: 7.7px;
        width: 100%;
    }

    #input-search-mb {
        background: transparent none repeat scroll 0 0;
        border: 1px solid #737373;
        border-radius: 5px;
        color: inherit;
        padding: 8.7px;
        width: 100%;
    }

    #button-search-mb {
        float: right;
        cursor: pointer;
        display: inline-block;
        vertical-align: middle;
        position: relative;
        background: transparent none repeat scroll 0 0;
        border: none;
        margin-top: 0;
        padding: 0;
        left: -20px;
        width:100%;
        color: black;
        cursor: pointer;
        transition: all 0.2s linear 0s;
    }

    #button-search-mb > i {
        font: normal normal normal 0.75em/1 LineIcons;
    }

    #close-mb {
        color: black;
        top: 14px;
    }

    .select-box {
        width: 30%;
    }

    .input-box {
        width: 64%;
        margin-left: 15px;
    }

    .tags {
        display: inline-block;
        width: 100%;
        padding-top: 10px;
        padding-bottom: 10px;
        background: #F1F3F7;
        height: 50px;
    }

    .tags .item {
        background-color: white;
        border: 0.5px solid #737373;
        border-radius: 5px;
        height: 30px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-weight: 400;
        text-transform: capitalize;
        margin-left: 5px;
        margin-right: 5px;
    }
</style>
<div class="search-post" id="search-post-mb">
    <span class="close-btn" id="close-mb"><i class="lni lni-cross-circle"></i></span>
    <div class="search-area" id="search-area-mb">
        <form method="GET" action="/tim-kiem" class="d-flex align-items-center">
            <div class="form-group select-box">
                <select class="form-control" id="select-do-kho-mb" name="do_kho">
                    <option value="">Mọi trình độ</option>
                    <option value="dễ" {{ @$_GET['do_kho'] == 'dễ' ? 'selected' : '' }}>Trình độ dễ</option>
                    <option value="trung bình" {{ @$_GET['do_kho'] == 'trung bình' ? 'selected' : '' }}>Trình độ Trung bình</option>
                    <option value="khó" {{ @$_GET['do_kho'] == 'khó' ? 'selected' : '' }}>Trình độ khó</option>
                </select>
            </div>
            <div class="form-group input-box">
                <input id="input-search-mb" class="form-control" type="text" name="keyword" value="{{ @$_GET['keyword'] }}" placeholder="Nhập từ khóa để tìm kiếm">
            </div>
            <div class="form-group">
                <button data-ripple="" class="form-control" id="button-search-mb"><i class="lni lni-search-alt"></i></button>
            </div>
        </form>
    </div>
</div><!-- search -->
<script>
    $('.top-search').click(function () {
        $('.search-post').addClass('show');
    });

    $('.search-post .close-btn').click(function () {
        $('.search-post').removeClass('show');
    });
</script>