{{--<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700|Roboto:300,400,500,600,700">--}}
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/tieng_anh/mobi/css/main.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/tieng_anh/mobi/css/style.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/tieng_anh/mobi/css/color.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/tieng_anh/mobi/css/custom.css') }}?v={{ time() }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/tieng_anh/mobi/css/social.css') }}">
{{--Custom--}}
<script src="{{ URL::asset('public/libs/jquery-3.4.0.min.js') }}"></script>