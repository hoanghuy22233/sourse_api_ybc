<!DOCTYPE html>
<html lang="vi">
<head>
    @include('themetienganh::partials.head_meta')
    @include('themetienganh::mobi.partials.head_script')

    {!! @$settings['frontend_head_code'] !!}

    @yield('head_script')
</head>

<body class="full-page">
<div class="app-layout">

    <header class="topbar">
        <div class="logo-place"><a href="/" title=""><img class="lazy"
                                                          data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                                                          style="    max-height: 25px;"
                                                          alt="{{ @$settings['name'] }}"></a></div>
        <div class="ac-settings ico-hover" title="Settings"><i class="lni lni-radio-button"></i></div>
        <div class="top-search ico-hover" title="Search"><i class="lni lni-search-alt"></i></div>
    </header>

    <ul class="tags" id="tags-mb">
        <?php
        $tags = \Modules\ThemeTiengAnh\Models\Tag::rightJoin('post_tags', 'post_tags.tag_id', 'tags.id')
            ->select('tags.name', 'tags.id')->get();
        ?>
            @php
                $strTag = isset($_GET['idtag']) ?  $_GET['idtag'] : "";
                $arrTag = explode(",", $strTag);
            @endphp
            @foreach($tags as $tag)
                {{--            <li class="{{ @$_GET['tag_id'] == $tag->id ? 'active' : '' }}"><a href="/tim-kiem?tag_id={{ $tag->id }}" title="{{ $tag->name }}">{{ $tag->name }}</a></li>--}}
                <li data-id="{{ $tag->id }}" onclick="changeTag(this)" class="@if(in_array($tag->id, $arrTag)) active @endif item"><a href="javascript:void(0)" title="{{ $tag->name }}">{{ $tag->name }}</a></li>
            @endforeach

    </ul>

    <style>
        #tags-mb {
            overflow:auto;
            display:inline-block;
            white-space: nowrap;
            padding-top:15px;
            height:50px;
        }

        #tags-mb .item {
            display:inline;
            padding:5px;
        }
    </style>


    <div class="bottom-header">
        <ul class="menu-left" style="width: 100%;">
            <?php
            if (strpos($_SERVER['REQUEST_URI'], '/kenh') !== false) {
                $tab = 'kenh';
            } else {
                $tab = 'home';
            }
            ?>
            <li class="{{ $tab == 'home' ? 'active' : '' }}"><a class="ico-hover" href="/" title=""><i
                            class="lni lni-home"></i>Trang chủ</a></li>
            <li class="{{ $tab == 'kenh' ? 'active' : '' }}"><a class="ico-hover" href="/kenh" title=""><i
                            class="fa fa-desktop"></i>Kênh</a></li>
            <li class=""><a class="ico-hover sidebar-profile" title=""><i class="lni lni-user"></i>Cá nhân</a></li>
        </ul>
    </div>

    <nav>
        <ul class="menu">
            <li data-text="Home"><a href="/" title=""><i><img
                                src="{{ URL::asset('public/frontend/themes/tieng_anh/mobi/images/svg/home.png') }}"
                                alt=""></i><span>Home</span></a>
            </li>
            <li data-text="Home"><a href="/dang-nhap" title=""><i><img
                                src="{{ URL::asset('public/frontend/themes/tieng_anh/mobi/images/svg/user.png') }}"
                                alt=""></i><span>My Profile</span></a>
            </li>
        </ul>
    </nav><!-- compact nav menu -->
    @yield('main_content')

    <div class="side-slide">
        <span class="close-btn"><i class="lni lni-cross-circle"></i></span>
        <div class="user-avatar" style="margin-bottom: 12px;">
            @if(\Auth::guard('admin')->check())
                <figure style="width: 49px; height: 49px; overflow: hidden;">
                    <img alt="{{ \Auth::guard('admin')->user()->name }}" style="    vertical-align: middle;
    width: 40px;
    height: 40px;
    border-radius: 50%;
    object-fit: cover;
border: none !important;"
                         data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb(\Auth::guard('admin')->user()->image, 49, 49) }}"
                         class="lazy">
                    <span class="status status greenbg"></span>
                </figure>
                <div class="user-options">
                    <h5>{{ \Auth::guard('admin')->user()->name }}</h5>
                </div>
            @endif
        </div>
        <ul>
            {{--<li><a href="/admin/profile" title=""><i class="lni lni-user"></i>Xem profile</a></li>--}}
            @if(\Auth::guard('admin')->check())
                <li><a href="/admin/profile" title=""><i class="lni lni-user"></i>Xem profile</a></li>

                @if(\App\Http\Helpers\CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'post_add'))
                    <li><a href="/admin/post" title=""><i class="lni lni-add-files"></i>Đăng bài</a></li>
                @else
                    <li><a href="/tao-kenh" title=""><i class="lni lni-add-files"></i>Đăng bài</a></li>
                @endif

                <li><a href="/dang-xuat" title=""><i class="lni lni-power-switch"></i>Đăng xuất</a></li>
            @else
                <li><a href="/dang-nhap" title=""><i class="lni lni-power-switch"></i>Đăng nhập</a></li>
            @endif
        </ul>
    </div><!-- side panel -->
    @include('themetienganh::mobi.partials.search')

</div>

@include('themetienganh::mobi.partials.footer_script')
@yield('footer_script')
@include('themetienganh::partials.chat_facebook')


<script>
    $(document).ready(function () {
        $(document).on('scrolltop', function () {
            console.log('fa');
            $('header.topbar').addClass('fixed');
        });
        $(document).on('scrollbottom', function () {
            $('header.topbar').removeClass('fixed');
        });
    });

    jQuery(document).mouseup(function (e) {
        var container = jQuery(".side-slide");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            jQuery('.side-slide .close-btn').click();
        }
    });
</script>

{!! @$settings['frontend_body_code'] !!}


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-EJ07G206GX"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-EJ07G206GX');
</script>



@include('themetienganh::widgets.log_ip')
</body>
</html>
