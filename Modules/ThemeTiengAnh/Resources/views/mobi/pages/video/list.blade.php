@extends('themetienganh::mobi.layouts.mobi')
@section('main_content')
<section>
    <div class="gap">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php
                    $data = CommonHelper::getFromCache('courses_created_at_desc', ['courses']);
                    if (!$data) {
                        $data = \Modules\EduCourse\Models\Course::where('status', 1)->orderBy('created_at', 'DESC')->orderBy('id', 'DESC')->take(4)->get();
                        CommonHelper::putToCache('courses_created_at_desc', $data, ['courses']);
                    }
                    ?>
                    <?php
                    //   Lấy ra các khóa học đã mua
                    $course_buyed = [];
                    if (\Auth::guard('admin')->check()) {
                        $course_ids = [];
                        foreach ($data as $course) {
                            $course_ids[] = $course->id;
                        }

                        $course_buyed = \Modules\ThemeTiengAnh\Models\Order::where('student_id', @\Auth::guard('admin')->user()->id)->whereNotNull('student_id')
                            ->whereIn('course_id', $course_ids)->where('status', 1)->pluck('course_id')->toArray();
                    }
                    ?>
                    @foreach($data as $course)
                        <div class="user-post">
                            <figure><img src="http://wpkixx.com/html/pitnik/images/resources/nearly1.jpg" alt=""></figure>
                            <div class="user-name">
                                <h5>Học tiếng anh qua truyện cổ tích</h5>
                                <span>Ngày đăng: January,5 2010 19:PM</span>
                            </div>
                            <div class="post-meta">
                                <strong>
                                    Đọc truyện tiếng anh: KILL 24 PEOPLE IN A MINUTES
                                </strong>
                                <img alt="" src="https://ybcstartup.com/public/filemanager/userfiles/banner/SharedScreenshot.jpg">
                                <i>
                                    <svg version="1.1" class="play" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="55px" width="55px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
															  <path class="stroke-solid" fill="none" stroke="" d="M49.9,2.5C23.6,2.8,2.1,24.4,2.5,50.4C2.9,76.5,24.7,98,50.3,97.5c26.4-0.6,47.4-21.8,47.2-47.7
																C97.3,23.7,75.7,2.3,49.9,2.5"></path>
                                        <path class="icon" fill="" d="M38,69c-1,0.5-1.8,0-1.8-1.1V32.1c0-1.1,0.8-1.6,1.8-1.1l34,18c1,0.5,1,1.4,0,1.9L38,69z"></path>
																</svg>
                                </i>
                            </div>
                            <div class="stat-tools">
								<span class="views" title="views" style="position: relative;    margin-right: 25px;">
																	<i class="lni lni-eye"></i>
																	<ins style="font-size: 11px;
    left: 20px;
    position: absolute;
    text-decoration: none;
    top: -10px;">1.2k</ins>
																</span>
                                <a class="comment-btn" href="javascript:;" title=""><i class="lni lni-comments"></i></a>
                                <a class="question-btn" href="javascript:;" title=""><i
                                            class="lni lni-comments"></i></a>
                                <div style="    width: unset;
    display: inline-block;

    float: right;">
                                    <div class="fb-like"
                                         data-href="https://developers.facebook.com/docs/plugins/"
                                         data-width="" data-layout="button_count"
                                         data-action="like" data-size="small"
                                         data-share="true"></div>
                                </div>
                            </div>


                            <div class="comment-area" style="display: none;">
                                <div class="fb-comments"
                                     data-href="https://developers.facebook.com/docs/plugins/comments#configurator"
                                     data-numposts="5" data-width=""></div>
                            </div>
                            <div class="question-area" style="display: none;">
                                *English below<br>
                                HACHINET CÓ MẶT TẠI NGÀY CHUYỂN ĐỔI SỐ VIỆT NAM 2020<br>
                                <img src="http://wpkixx.com/html/pitnik/images/resources/user-post.jpg"
                                     alt="">
                                Sự kiện bao gồm một loạt các hoạt động đa dạng giúp
                                người tham dự hiểu sâu hơn về quá trình chuyển đổi
                                số trong doanh nghiệp như Thảo luận chuyên đề, Kết
                                nối giao thương 1:1, Triển lãm công nghệ.<br>
                                Thông tin sự kiện:<br>
                                ◾ Địa điểm: InterContinental Hanoi Landmark 72 -
                                Urban Area, Keangnam Hanoi Landmark Tower Plot E6,
                                Cầu Giấy, Hà Nội<br>
                                ◾ Thời gian: ngày 14 và 15 tháng 12, 2020<br>
                                --------------------------------------<br>
                                VIETNAM DX DAY 2020 - Digital Transformation
                                conference by VINASA<br>
                                There’s going to be a variety of activities such as
                                panel discussions, business matchings, exhibitions,
                                and so on.<br>
                                ◾ Location: InterContinental Hanoi Landmark72 -
                                Urban Area, Keangnam Hanoi Landmark Tower Plot E6,
                                Cau Giay, Ha Noi<br>
                                <img src="http://wpkixx.com/html/pitnik/images/resources/user-post.jpg"
                                     alt="">
                                Sự kiện bao gồm một loạt các hoạt động đa dạng giúp
                                người tham dự hiểu sâu hơn về quá trình chuyển đổi
                                số trong doanh nghiệp như Thảo luận chuyên đề, Kết
                                nối giao thương 1:1, Triển lãm công nghệ.<br>
                                Thông tin sự kiện:<br>
                                <a href="javascript:;" class="view-question-less"
                                   style="color: #007bff;">Ẩn bớt</a>
                            </div>
                        </div><!-- with image -->
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('head_script')
    @include('themetienganh::mobi.pages.home.partials.timeline_css')
@endsection
@section('footer_script')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
            src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v9.0&appId=561583071141201&autoLogAppEvents=1"
            nonce="R5zAQPxH"></script>

    <script>
        $('.view-more').click(function () {
            $(this).parents('.user-post').find('.more-box').slideDown(200);
        });

        $('.view-less').click(function () {
            $(this).parents('.user-post').find('.more-box').slideUp(200);
        });

        $('.view-question-less').click(function () {
            $(this).parents('.question-area').slideUp(200);
        });

        $('.question-btn').click(function () {
            $(this).parents('.user-post').find('.question-area').slideToggle(200);
        });

        $('.comment-btn').click(function () {
            $(this).parents('.user-post').find('.comment-area').slideToggle(200);
        });
    </script>
@endsection
