@extends('themetienganh::mobi.layouts.mobi')
@section('main_content')
    <section>
        <div class="gap no-gap">
            <div class="profile-pic">
                <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$channel->banner, null) }}" alt="{{$channel->channel_name != '' ? $channel->channel_name : $channel->name}}">
                <div class="group-info">
                    <figure><img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($channel->image, 114, null) }}" alt="{{$channel->channel_name != '' ? $channel->channel_name : $channel->name}}">
                    </figure>
                    <div class="group-name" style="width: 62%;">
                        <h5>{{$channel->channel_name != '' ? $channel->channel_name : $channel->name}}</h5>
                    </div>
                </div>
                @if(\Modules\ThemeTiengAnh\Models\Follow::where('channel_id', $channel->id)->where('admin_id', @\Auth::guard('admin')->user()->id)->count() == 0)
                <a href="javascript:;" class="likes heart" data-id="{{ $channel->id }}" title="Theo dõi/Hủy theo dõi"><i class="lni lni-checkmark"></i> Theo dõi</a>
                @else
                    <a href="javascript:;" class="likes heart" data-id="{{ $channel->id }}" title="Theo dõi/Hủy theo dõi"><i class="lni lni-checkmark"></i> Bỏ theo dõi</a>
                @endif
            </div>
        </div>
    </section>
    <section>
        <div class="gap" style="padding: 30px 0; padding-bottom: 0; border-bottom: 1px dashed #eee;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-12">
                        <ul class="group-detail">
                            <li>
                                <span><i class="lni lni-heart"></i>Lượt theo dõi</span>
                                <i><span class="channel_follow_count channel_follow_{{ $channel->id }}" id="">{{ number_format(\Modules\ThemeTiengAnh\Models\Follow::where('channel_id', $channel->id)->count(), 0, '.', '.') }}</span></i>
                            </li>
                            <li>
                                <span><i class="lni lni-book"></i>Số bài viết</span>
                                <i>{{ number_format(\Modules\ThemeTiengAnh\Models\Post::where('status', 1)->where('admin_id', $channel->id)->count(), 0, '.', '.') }}</i>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="gap">
            <div class="container">
                <div class="row list-user-post post-content">
                    @php
                        $user = @\Auth::guard('admin')->user();
                       if($user){
                           $user = \Modules\ThemeTiengAnh\Models\Admin::find($user->id);
                       }
                    @endphp
                    @foreach($posts as $post)
                        @include('themetienganh::mobi.pages.home.partials.post_item')
                    @endforeach
                </div>
                <div class="div-load-more" style="width: 100%; display: inline-block; text-align: center;">

                </div>
            </div>
        </div>
    </section>
@endsection
@section('head_script')
    @include('themetienganh::mobi.pages.home.partials.timeline_css')

    <style>

        .form-radio .radio > label {
            padding: 14px 16px;
            width: 100%;
            background: #f0f2f5;
            margin-bottom: 8px;
            font-size: 14px;

        }

        .select-radio {
            padding: 0;
        }

        .radio .check-box::before,
        .radio input:checked ~ .check-box::after {
            left: 13px;
            top: 16px;
        }

        span.red.ten-cau {
            background: #1877f2;
        }

        .group-detail > li > span {
            width: 42%;
        }
        .profile-pic a.likes.heart {
            bottom: 8px;
            top: unset;
            right: 8px;
        }
    </style>
@endsection
@section('footer_script')
    @include('themetienganh::pages.home.partials.newsfeed_js')
    <script>
        //  Load thêm content
        var loaded = false;
        $(window).scroll(function() {
            if( ( $(window).scrollTop() + $(window).height() ) >= $(document).height() ) {
                if(!loaded) {
                    loaded = true;
                    $('.div-load-more').html('<img src="{{ asset('public/images_core/loading.gif') }}">');
                    $.ajax({
                        url: '/tim-kiem/load-more',
                        type: 'GET',
                        data: {
                            keyword: '{{ @$_GET['keyword'] }}',
                            do_kho: '{{ @$_GET['do_kho'] }}',
                            admin_id: '{{ @\Auth::guard('admin')->user()->id }}',
                            skip: $('.user-post').length,
                            take: 5,
                            channel_id: '{{ $channel->id }}',
                        },
                        success: function (html) {
                            if (html != '') {
                                $('.post-content').append(html);
                                $('.div-load-more').html('');
                            } else {
                                $('.div-load-more').remove();
                            }
                            loaded = false;
                        },
                        error: function () {
                            console.log('Lỗi load thêm dữ liệu');
                            $('.div-load-more').html('');
                            loaded = false;
                        }
                    });
                }
            }
        });
    </script>


    <script>
        $('.likes.heart').click(function (){
            var channel_id = $(this).data('id');
            var object = $(this);
            $.ajax({
                url: '/chanel/follow',
                type: 'GET',
                data: {
                    channel_id: channel_id,
                    admin_id: '{{ @\Auth::guard('admin')->user()->id }}'
                },
                success: function (result){
                    location.reload();
                }
            });
        });
    </script>
    <script>
        function checkSubmitCmt(t){
            var tObj = $(t);
            var v = tObj.val().trim();
            if(v != ""){
                tObj.parents(".box-cmt").children(".btn-send-cmt").show(100);
            }else{
                tObj.parents(".box-cmt").children(".btn-send-cmt").hide(100);
            }
        }
        function submitComment(t){
            ajaxComment(t);
        }
        function likepage(tHtml){
            var t = $(tHtml);
            var postId = t.data('postid');
            $.ajax({
                url: "{{ route('user.like') }}",
                data: {'postid': postId},
                dataType: 'json',
                success:function(repsonse){
                    if(repsonse.status == 'success'){
                        if(repsonse.class == "liked"){
                            t.removeClass("dislike");
                            t.addClass(repsonse.class);
                        }else{
                            t.removeClass("liked");
                            t.addClass(repsonse.class);
                        }

                        t.find(".count_like").stop().html(repsonse.count);
                    }

                },
                error:function(e){
                    if(e.status == 403){
                        window.location.href = "/dang-nhap";
                    }
                }
            });
        }

        function actionComment(selector){

            $(selector).keyup(function (e) {
                if (e.keyCode == 13) {
                    var text = $(this).val();
                    var lines = text.split("\n");
                    console.log(lines);
                    var count = lines.length + 1;
                    count = count <= 0 ? 1 : count;
                    $(this).attr('rows', count);
                    return true;
                }
                if (e.keyCode == 8) {
                    var text = $(this).val();
                    text = text.replace(/\s$/, '');
                    var lines = text.split("\n");
                    var count = lines.length;
                    count = count <= 0 ? 1 : count;
                    $(this).attr('rows', count);
                }
                return false;
            });
            return false;
        }

        $(document).ready(function () {
            actionComment('.box-cmt textarea');
        });
        // $('.box-cmt textarea').change(function (e) {
        //     var text = $(this).val();
        //     var lines = text.split("\n");
        //     var count = lines.length;
        //     count = count <= 0 ? 1 : count;
        //     $(this).attr('rows', count);
        // });
        function ajaxComment(t){
            var post_id = $(t).parents(".box-cmt").find("input[name='post_id']").stop().val();
            var parent_id = $(t).parents(".box-cmt").find("input[name='parent_id']").stop().val();

            var objText = $(t).parents(".box-cmt").children("textarea");
            let tValue = objText.val();
            $.ajax({
                url: "/user/comment",
                type:"POST",
                dataType: "json",
                data:{
                    parent_id: parent_id,
                    post_id: post_id,
                    text : tValue,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){
                    objText.attr('rows', 1);
                    objText.val("");
                    objText.blur();

                    //$(t).parents(".list-comment").stop().append(response);
                    var lstParent = objText.parents(".list-comment");

                    $(lstParent[0]).children(".content_list").append(response.html);
                    //location.reload();
                    actionComment('.box-cmt textarea.text_' + response.comment_id);
                    //actionComment(t);

                }
            });
        }
        function toogleReply(id){
            var id = "#rep_" + id;
            $(id).toggle(200);
        }
        function showMoreComment(t, postid, idcomment){
            $.ajax({
                url: "user/comment-more",
                type:"POST",
                data:{
                    comment_id: idcomment,
                    post_id: postid,

                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){
                    console.log(response);
                    $("#area_cmt_more_" + postid).html(response.html);
                    console.log($(t));
                    $(t).hide();
                    var strSelector = "";
                    $.each(response.array_id_comment, function( index, value ) {
                        strSelector += ',.normal-post .box-cmt textarea.text_' + value;
                    });
                    strSelector = strSelector.substr(1);
                    actionComment(strSelector);
                    //location.reload();
                    //actionComment();
                }
            });
        }
        function sendCommentAjax(post_id, parent_id, tValue, t){

            $.ajax({
                url: "/user/comment",
                type:"POST",
                dataType: "json",
                data:{
                    parent_id: parent_id,
                    post_id: post_id,
                    text : tValue,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){
                    // $(t).attr('rows', 1);
                    // $(t).val("");
                    // $(t).blur();


                    var lstParent = t.parents(".list-comment");

                    $(lstParent[0]).children(".content_list").append(response.html);

                    actionComment('.box-cmt textarea.text_' + response.comment_id);


                }
            });
        }

        // autoComment();
    </script>
@endsection
