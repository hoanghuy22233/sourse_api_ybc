@extends('themetienganh::mobi.layouts.mobi')
@section('main_content')
    <section>
        <div class="gap redish high-opacity">
            <div class="content-bg-wrap"
                 style="background: url({{ URL::asset('public/frontend/themes/tieng_anh/mobi/images/resources/animate-bg.png') }}"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="head-meta">
                            <h5>Danh sách kênh</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="gap">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <?php
                        $not_user = \App\Models\RoleAdmin::where('role_id', '!=', 2)->pluck('admin_id')->toArray();
                        $channels = \App\Models\Admin::select('id', 'name', 'image', 'channel_name', 'follow_count', 'post_count')
                            ->whereNotIn('id', $not_user)->where('post_count', '>', 0)
                            ->orderBy('follow_count', 'desc')->orderBy('id', 'asc')->get();
                        $channel_followed = \Modules\ThemeTiengAnh\Models\Follow::where('admin_id', @\Auth::guard('admin')->user()->id)->pluck('channel_id')->toArray();
                        ?>
                        @foreach($channels as $channel)
                            <div class="activity" id="activity">
                                <figure>
                                    <a href="/kenh/{{ @$channel->id }}">
                                        <img alt="{{$channel->channel_name != '' ? $channel->channel_name : $channel->name}}"
                                             src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$channel->image ,45, null) }}">
                                    </a>
                                </figure>
                                <div class="history-meta" id="history-meta">
                                    <h5><a title="{{$channel->channel_name != '' ? $channel->channel_name : $channel->name}}"
                                           href="/kenh/{{ $channel->id }}">{{$channel->channel_name != '' ? $channel->channel_name : $channel->name}}</a></h5>
                                    <span class="channel-intro">
                                        {{ number_format($channel->post_count, 0, '.', '.') }} bài viết -
                                        <span class="channel_follow_count channel_follow_{{ $channel->id }}"
                                                id="">{{ number_format($channel->follow_count, 0, '.', '.') }}</span> theo dõi</span>
                                </div>
                                <div class="likes heart {{ in_array($channel->id, $channel_followed) ? 'liked' : '' }}"
                                     data-id="{{ $channel->id }}" title="Like/Dislike"><i class="lni lni-heart"></i>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('head_script')
    <style>
        .activity > figure {
            width: 51px;
            height: 51px;
            overflow: hidden;
        }

        .likes.liked {
            color: red;
        }

        .likes.heart {
            position: absolute;
            top: 0;
            right: 0;
            font-size: 29px;
        }

        .activity {
            position: relative;
        }

        #activity {
            display: flex;
        }

        .history-meta {
            padding-right: 30px;
            padding-left: 0;
        }

        #history-meta {
            width: 90%;
            margin-left:10px;
        }
    </style>
@endsection
@section('footer_script')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
            src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v9.0&appId=561583071141201&autoLogAppEvents=1"
            nonce="R5zAQPxH"></script>

    <script>
        $('.view-more').click(function () {
            $(this).parents('.user-post').find('.more-box').slideDown(200);
        });

        $('.view-less').click(function () {
            $(this).parents('.user-post').find('.more-box').slideUp(200);
        });

        $('.view-question-less').click(function () {
            $(this).parents('.question-area').slideUp(200);
        });

        $('.question-btn').click(function () {
            $(this).parents('.user-post').find('.question-area').slideToggle(200);
        });

        $('.comment-btn').click(function () {
            $(this).parents('.user-post').find('.comment-area').slideToggle(200);
        });
    </script>


    <script>
        $('.likes.heart').click(function () {
            var channel_id = $(this).data('id');
            var object = $(this);
            $.ajax({
                url: '/chanel/follow',
                type: 'GET',
                data: {
                    channel_id: channel_id,
                    admin_id: '{{ @\Auth::guard('admin')->user()->id }}'
                },
                success: function (result) {
                    if (result.status) {
                        if (result.follow) {
                            object.toggleClass('liked');
                        } else {
                            object.removeClass('liked');
                        }
                        $('.channel_follow_' + channel_id).html(result.count);
                    } else {
                        alert(result.msg)
                        // toastr.error(result.msg);
                    }
                }
            });
        });
    </script>
    <script>
        function checkSubmitCmt(t){
            var tObj = $(t);
            var v = tObj.val().trim();
            if(v != ""){
                tObj.parents(".box-cmt").children(".btn-send-cmt").show(100);
            }else{
                tObj.parents(".box-cmt").children(".btn-send-cmt").hide(100);
            }
        }
        function submitComment(t){
            ajaxComment(t);
        }
        function likepage(tHtml){
            var t = $(tHtml);
            var postId = t.data('postid');
            $.ajax({
                url: "{{ route('user.like') }}",
                data: {'postid': postId},
                dataType: 'json',
                success:function(repsonse){
                    if(repsonse.status == 'success'){
                        if(repsonse.class == "liked"){
                            t.removeClass("dislike");
                            t.addClass(repsonse.class);
                        }else{
                            t.removeClass("liked");
                            t.addClass(repsonse.class);
                        }

                        t.find(".count_like").stop().html(repsonse.count);
                    }

                },
                error:function(e){
                    if(e.status == 403){
                        window.location.href = "/dang-nhap";
                    }
                }
            });
        }

        function actionComment(selector){

            $(selector).keyup(function (e) {
                if (e.keyCode == 13) {
                    var text = $(this).val();
                    var lines = text.split("\n");
                    console.log(lines);
                    var count = lines.length + 1;
                    count = count <= 0 ? 1 : count;
                    $(this).attr('rows', count);
                    return true;
                }
                if (e.keyCode == 8) {
                    var text = $(this).val();
                    text = text.replace(/\s$/, '');
                    var lines = text.split("\n");
                    var count = lines.length;
                    count = count <= 0 ? 1 : count;
                    $(this).attr('rows', count);
                }
                return false;
            });
            return false;
        }

        $(document).ready(function () {
            actionComment('.box-cmt textarea');
        });
        // $('.box-cmt textarea').change(function (e) {
        //     var text = $(this).val();
        //     var lines = text.split("\n");
        //     var count = lines.length;
        //     count = count <= 0 ? 1 : count;
        //     $(this).attr('rows', count);
        // });
        function ajaxComment(t){
            var post_id = $(t).parents(".box-cmt").find("input[name='post_id']").stop().val();
            var parent_id = $(t).parents(".box-cmt").find("input[name='parent_id']").stop().val();

            var objText = $(t).parents(".box-cmt").children("textarea");
            let tValue = objText.val();
            $.ajax({
                url: "/user/comment",
                type:"POST",
                dataType: "json",
                data:{
                    parent_id: parent_id,
                    post_id: post_id,
                    text : tValue,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){
                    objText.attr('rows', 1);
                    objText.val("");
                    objText.blur();

                    //$(t).parents(".list-comment").stop().append(response);
                    var lstParent = objText.parents(".list-comment");

                    $(lstParent[0]).children(".content_list").append(response.html);
                    //location.reload();
                    actionComment('.box-cmt textarea.text_' + response.comment_id);
                    //actionComment(t);

                }
            });
        }
        function toogleReply(id){
            var id = "#rep_" + id;
            $(id).toggle(200);
        }
        function showMoreComment(t, postid, idcomment){
            $.ajax({
                url: "user/comment-more",
                type:"POST",
                data:{
                    comment_id: idcomment,
                    post_id: postid,

                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){
                    console.log(response);
                    $("#area_cmt_more_" + postid).html(response.html);
                    console.log($(t));
                    $(t).hide();
                    var strSelector = "";
                    $.each(response.array_id_comment, function( index, value ) {
                        strSelector += ',.normal-post .box-cmt textarea.text_' + value;
                    });
                    strSelector = strSelector.substr(1);
                    actionComment(strSelector);
                    //location.reload();
                    //actionComment();
                }
            });
        }
        function sendCommentAjax(post_id, parent_id, tValue, t){

            $.ajax({
                url: "/user/comment",
                type:"POST",
                dataType: "json",
                data:{
                    parent_id: parent_id,
                    post_id: post_id,
                    text : tValue,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){
                    // $(t).attr('rows', 1);
                    // $(t).val("");
                    // $(t).blur();


                    var lstParent = t.parents(".list-comment");

                    $(lstParent[0]).children(".content_list").append(response.html);

                    actionComment('.box-cmt textarea.text_' + response.comment_id);


                }
            });
        }

        // autoComment();
    </script>
@endsection
