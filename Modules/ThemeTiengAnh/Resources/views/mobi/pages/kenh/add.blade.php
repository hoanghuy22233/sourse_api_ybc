@extends('themetienganh::mobi.layouts.mobi')
@section('main_content')
    <section>
        <div class="gap redish high-opacity">
            <div class="content-bg-wrap"
                 style="background: url({{ URL::asset('public/frontend/themes/tieng_anh/mobi/images/resources/animate-bg.png') }}"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="head-meta">
                            <h5>Danh sách kênh</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="gap">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        Tao kenh
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('head_script')
    <style>
        .activity > figure {
            width: 51px;
            height: 51px;
            overflow: hidden;
        }

        .likes.liked {
            color: red;
        }

        .likes.heart {
            position: absolute;
            top: 0;
            right: 0;
            font-size: 29px;
        }

        .activity {
            position: relative;
        }

        #activity {
            display: flex;
        }

        .history-meta {
            padding-right: 30px;
            padding-left: 0;
        }

        #history-meta {
            width: 90%;
            margin-left:10px;
        }
    </style>
@endsection
@section('footer_script')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
            src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v9.0&appId=561583071141201&autoLogAppEvents=1"
            nonce="R5zAQPxH"></script>

    <script>
        $('.view-more').click(function () {
            $(this).parents('.user-post').find('.more-box').slideDown(200);
        });

        $('.view-less').click(function () {
            $(this).parents('.user-post').find('.more-box').slideUp(200);
        });

        $('.view-question-less').click(function () {
            $(this).parents('.question-area').slideUp(200);
        });

        $('.question-btn').click(function () {
            $(this).parents('.user-post').find('.question-area').slideToggle(200);
        });

        $('.comment-btn').click(function () {
            $(this).parents('.user-post').find('.comment-area').slideToggle(200);
        });
    </script>


    <script>
        $('.likes.heart').click(function () {
            var channel_id = $(this).data('id');
            var object = $(this);
            $.ajax({
                url: '/chanel/follow',
                type: 'GET',
                data: {
                    channel_id: channel_id,
                    admin_id: '{{ @\Auth::guard('admin')->user()->id }}'
                },
                success: function (result) {
                    if (result.status) {
                        if (result.follow) {
                            object.toggleClass('liked');
                        } else {
                            object.removeClass('liked');
                        }
                        $('.channel_follow_' + channel_id).html(result.count);
                    } else {
                        alert(result.msg)
                        // toastr.error(result.msg);
                    }
                }
            });
        });
    </script>
@endsection
