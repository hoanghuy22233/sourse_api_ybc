@extends('themetienganh::mobi.layouts.mobi')
@section('main_content')
    <label>
        <?php
        $str = '';
        if (@$_GET['keyword'] != null) {
            $str .= ' Tìm kiếm với từ khóa: "'.$_GET['keyword'].'".';
        }
        if (@$_GET['do_kho'] != null) {
            $str .= ' Độ khó: "'.$_GET['do_kho'].'".';
        }

        ?>
        {{ $str }}
    </label>
    <section>
        <div class="gap">
            <div class="container">
                <div class="row list-user-post post-content">

                    @include('themetienganh::mobi.pages.home.partials.tim_kiem_items', [
                                            'posts' => $posts_search,])
                </div>
                @if(count($posts_search) == 5)
                    <div class="div-load-more" style="width: 100%; display: inline-block; text-align: center;">

                    </div>
                @endif
            </div>
        </div>
    </section>
@endsection
@section('head_script')
    @include('themetienganh::mobi.pages.home.partials.timeline_css')

    <style>

        .form-radio .radio > label {
            padding: 14px 16px;
            width: 100%;
            background: #f0f2f5;
            margin-bottom: 8px;
            font-size: 14px;

        }

        .select-radio {
            padding: 0;
        }

        .radio .check-box::before,
        .radio input:checked ~ .check-box::after {
            left: 13px;
            top: 16px;
        }

        span.red.ten-cau {
            background: #1877f2;
        }

        .dap_an {
            display: none;
            margin-top: 10px;
        }


    </style>
@endsection
@section('footer_script')
    @include('themetienganh::mobi.pages.home.partials.newsfeed_js')
    @include('themetienganh::pages.home.partials.hen_gio_load_bai_moi')
{{--    <script>--}}
{{--        //  Load thêm content--}}
{{--        var loaded = false;--}}
{{--        $(window).scroll(function () {--}}
{{--            if ($(window).scrollTop() == $(document).height() - $(window).height()) {--}}
{{--                if(!loaded) {--}}
{{--                    loaded = true;--}}
{{--                    $('.div-load-more').html('<img src="{{ asset('public/images_core/loading.gif') }}">');--}}
{{--                    $.ajax({--}}
{{--                        url: '/tim-kiem/load-more',--}}
{{--                        type: 'GET',--}}
{{--                        data: {--}}
{{--                            do_kho: '{{ @$_GET['do_kho'] }}',--}}
{{--                            admin_id: '{{ @\Auth::guard('admin')->user()->id }}',--}}
{{--                            skip: $('.user-post').length,--}}
{{--                            take: 5,--}}
{{--                            idtag: '{{ isset($_GET['idtag']) ? $_GET['idtag'] : "" }}',--}}
{{--                            keyword: '{{ isset($_GET['keyword']) ? $_GET['keyword'] : "" }}',--}}
{{--                        },--}}
{{--                        success: function (html) {--}}
{{--                            if (html.html != '') {--}}
{{--                                $('.post-content').append(html.html);--}}
{{--                                $('.div-load-more').html('');--}}
{{--                                $('.comment.ajax').on('click', function () {--}}
{{--                                    $(this).parents(".post-meta").siblings(".coment-area").slideToggle("slow");--}}
{{--                                });--}}
{{--                                var strSelector = "";--}}
{{--                                $.each(html.post_id, function( index, value ) {--}}
{{--                                    strSelector += ',.ajax-post-'+value+' .box-cmt textarea';--}}
{{--                                });--}}
{{--                                strSelector = strSelector.substr(1);--}}
{{--                                actionComment(strSelector);--}}
{{--                                //checkLoadMore(".overlow-post:not(.loaded)");--}}
{{--                                var myVar = setInterval(checkLoadMore, 1000, ".overlow-post:not(.loaded)");--}}
{{--                            } else {--}}
{{--                                $('.div-load-more').remove();--}}
{{--                            }--}}
{{--                            loaded = false;--}}
{{--                        },--}}
{{--                        error: function () {--}}
{{--                            console.log('Lỗi load thêm dữ liệu');--}}
{{--                            $('.div-load-more').html('');--}}
{{--                            loaded = false;--}}
{{--                        }--}}
{{--                    });--}}
{{--                }--}}
{{--            }--}}
{{--        });--}}
{{--    </script>--}}
    <script>

        //  Load thêm content
        var loaded = false;
        $(window).scroll(function() {
            if( ( $(window).scrollTop() + $(window).height() ) >= $(document).height() ) {
                if(!loaded) {
                    loaded = true;
                    $('.div-load-more').html('<img src="{{ asset('public/images_core/loading.gif') }}">');

                    //  Bỏ qua số bài đang có trên newsfeed và só bài vừa rend mới bằng ajax
                    var skip = parseInt($('.central-meta.item').length) + parseInt($('input[name=ajax_rend_content]').val());
                    console.log(parseInt($('input[name=ajax_rend_content]').val()));

                    $.ajax({
                        url: '/tim-kiem/load-more',
                        type: 'GET',
                        data: {
                            admin_id: '{{ @\Auth::guard('admin')->user()->id }}',
                            skip: $('.user-post').length,
                            take: 5,
                            idtag: '{{ isset($_GET['idtag']) ? $_GET['idtag'] : "" }}',
                            keyword: '{{ isset($_GET['keyword']) ? $_GET['keyword'] : "" }}',
                        },
                        success: function (html) {
                            if (html.html != '') {
                                $('.post-content').append(html.html);
                                $('.div-load-more').html('');
                                $('.comment.ajax').on('click', function () {
                                    $(this).parents(".post-meta").siblings(".coment-area").slideToggle("slow");
                                });
                                var strSelector = "";
                                $.each(html.post_id, function( index, value ) {
                                    strSelector += ',.ajax-post-'+value+' .box-cmt textarea';
                                });
                                strSelector = strSelector.substr(1);
                                actionComment(strSelector);
                                //checkLoadMore(".overlow-post:not(.loaded)");
                                var myVar = setInterval(checkLoadMore, 1000, ".overlow-post:not(.loaded)");
                            } else {
                                $('.div-load-more').remove();
                            }
                            loaded = false;
                        },
                        error: function () {
                            console.log('Lỗi load thêm dữ liệu');
                            $('.div-load-more').html('');
                            loaded = false;
                        }
                    });
                }
            }
        });
    </script>
    <script>
        function checkSubmitCmt(t){
            var tObj = $(t);
            var v = tObj.val().trim();
            if(v != ""){
                tObj.parents(".box-cmt").children(".btn-send-cmt").show(100);
            }else{
                tObj.parents(".box-cmt").children(".btn-send-cmt").hide(100);
            }
        }
        function submitComment(t){
            ajaxComment(t);
        }
        function likepage(tHtml){
            var t = $(tHtml);
            var postId = t.data('postid');
            $.ajax({
                url: "{{ route('user.like') }}",
                data: {'postid': postId},
                dataType: 'json',
                success:function(repsonse){
                    if(repsonse.status == 'success'){
                        if(repsonse.class == "liked"){
                            t.removeClass("dislike");
                            t.addClass(repsonse.class);
                        }else{
                            t.removeClass("liked");
                            t.addClass(repsonse.class);
                        }

                        t.find(".count_like").stop().html(repsonse.count);
                    }

                },
                error:function(e){
                    if(e.status == 403){
                        window.location.href = "/dang-nhap";
                    }
                }
            });
        }

        function actionComment(selector){

            $(selector).keyup(function (e) {
                if (e.keyCode == 13) {
                    var text = $(this).val();
                    var lines = text.split("\n");
                    console.log(lines);
                    var count = lines.length + 1;
                    count = count <= 0 ? 1 : count;
                    $(this).attr('rows', count);
                    return true;
                }
                if (e.keyCode == 8) {
                    var text = $(this).val();
                    text = text.replace(/\s$/, '');
                    var lines = text.split("\n");
                    var count = lines.length;
                    count = count <= 0 ? 1 : count;
                    $(this).attr('rows', count);
                }
                return false;
            });
            return false;
        }

        $(document).ready(function () {
            actionComment('.box-cmt textarea');
        });
        // $('.box-cmt textarea').change(function (e) {
        //     var text = $(this).val();
        //     var lines = text.split("\n");
        //     var count = lines.length;
        //     count = count <= 0 ? 1 : count;
        //     $(this).attr('rows', count);
        // });
        function ajaxComment(t){
            var post_id = $(t).parents(".box-cmt").find("input[name='post_id']").stop().val();
            var parent_id = $(t).parents(".box-cmt").find("input[name='parent_id']").stop().val();

            var objText = $(t).parents(".box-cmt").children("textarea");
            let tValue = objText.val();
            $.ajax({
                url: "/user/comment",
                type:"POST",
                dataType: "json",
                data:{
                    parent_id: parent_id,
                    post_id: post_id,
                    text : tValue,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){
                    objText.attr('rows', 1);
                    objText.val("");
                    objText.blur();

                    //$(t).parents(".list-comment").stop().append(response);
                    var lstParent = objText.parents(".list-comment");

                    $(lstParent[0]).children(".content_list").append(response.html);
                    //location.reload();
                    actionComment('.box-cmt textarea.text_' + response.comment_id);
                    //actionComment(t);

                }
            });
        }
        function toogleReply(id){
            var id = "#rep_" + id;
            $(id).toggle(200);
        }
        function showMoreComment(t, postid, idcomment){
            $.ajax({
                url: "user/comment-more",
                type:"POST",
                data:{
                    comment_id: idcomment,
                    post_id: postid,

                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){
                    console.log(response);
                    $("#area_cmt_more_" + postid).html(response.html);
                    console.log($(t));
                    $(t).hide();
                    var strSelector = "";
                    $.each(response.array_id_comment, function( index, value ) {
                        strSelector += ',.normal-post .box-cmt textarea.text_' + value;
                    });
                    strSelector = strSelector.substr(1);
                    actionComment(strSelector);
                    //location.reload();
                    //actionComment();
                }
            });
        }
        function sendCommentAjax(post_id, parent_id, tValue, t){

            $.ajax({
                url: "/user/comment",
                type:"POST",
                dataType: "json",
                data:{
                    parent_id: parent_id,
                    post_id: post_id,
                    text : tValue,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){
                    // $(t).attr('rows', 1);
                    // $(t).val("");
                    // $(t).blur();


                    var lstParent = t.parents(".list-comment");

                    $(lstParent[0]).children(".content_list").append(response.html);

                    actionComment('.box-cmt textarea.text_' + response.comment_id);


                }
            });
        }

        // autoComment();
    </script>
@endsection
