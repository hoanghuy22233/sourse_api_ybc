<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v9.0&appId=561583071141201&autoLogAppEvents=1"
        nonce="R5zAQPxH"></script>

<script>

    $(document).on('click', '.view-more', function () {
        $(this).parents('.user-post').find('.more-box').slideDown(200);
        $(this).parents('.user-post').find('.post-content_html').addClass('show-more');
        $(this).parents('.user-post').find('.view-less').show();
        $(this).hide();
    });

    $(document).on('click', '.view-less', function () {
        $(this).parents('.user-post').find('.more-box').slideUp(200);
        $(this).parents('.user-post').find('.view-more').show();
        $(this).parents('.user-post').find('.post-content_html').removeClass('show-more');
        $(this).hide();
    });

    $(document).on('click', '.view-question-less', function () {
        $(this).parents('.question-area').slideUp(200);
    });

    $(document).on('click', '.question-btn', function () {
        $(this).parents('.user-post').find('.question-area').slideToggle(200);
        $(this).parents('.user-post').find('.comment-area').slideUp(1);
    });

    $(document).on('click', '.comment-btn', function () {
        $(this).parents('.user-post').find('.comment-area').slideToggle(200);
        $(this).parents('.user-post').find('.question-area').slideUp(1);
    });

    $(document).on('click', '.nav-tabs .nav-link', function () {
        $(this).parents('li').addClass('active');
        $(this).parents('li').siblings().removeClass('active');

        $(this).parents('.myTab-box').find($(this).data('tab')).addClass('show active');
        $(this).parents('.myTab-box').find($(this).data('tab')).siblings().removeClass('show active');
    });

    $(document).on('click', '.xac-nhan', function () {
        $(this).parents('.cau-hoi').find('.dap_an').show();
    });
    $(document).ready(function () {
        checkLoadMore(".overlow-post:not(.loaded)");
    });
    function checkLoadMore(selector){
        var arrOverPost = $(selector);
        console.log(arrOverPost);
        $.each(arrOverPost, function( index, value ) {
            var itempost = $(value);
            var hBefore = value.scrollHeight;
            var hAfter = itempost.height();
            //itempost.addClass("loaded");

            if(hAfter < hBefore){
                // show more
                var parent =  itempost.parents(".description");
                parent.children(".view-less-custom").remove();
                parent.append('<a onclick="showMoreOrLess(this)" href="javascript:void(0);" class="view-less-custom show-more" style="color: #007bff">Đọc thêm</a>');
            }else{
                console.log(hAfter + "-" + hBefore);
                var parent =  itempost.parents(".description");
                parent.children(".view-less-custom:not(.loaded)").remove();
            }
        });
    }
    function showMoreOrLess(t){
        var tObj = $(t);
        tObj.addClass("loaded");
        var classAdd = "show-more";
        var classRemove = "show-less";
        var txtAdd = "Đọc thêm";
        var maxHeight = "300px";
        if(tObj.hasClass("show-more")){
            var classAdd = "show-less";
            var classRemove = "show-more";
            var txtAdd = "Ít hơn";
            var maxHeight = "initial";
        }
        tObj.parents(".description").children(".overlow-post").css({"maxHeight":maxHeight});

        tObj.removeClass(classRemove);
        tObj.addClass(classAdd);
        tObj.text(txtAdd);
    }
</script>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '455116562321104',
            xfbml      : true,
            version    : 'v2.8'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function openFbPopUp(fburl,fbimgurl,fbtitle,fbsummary) {
        FB.ui(
            {
                method: 'feed',
                name: fbtitle,
                link: fburl,
                picture: fbimgurl,
                caption: fbtitle,
                description: fbsummary
            }
        );
    }
</script>