<style>
        .group-info > figure img{
            vertical-align: middle;
            width: 100px;
            height: 100px;
            border-radius: 50%;
            object-fit: cover;
        }
        p{
            line-height: 24px;
            color: #151414;

        }
    .red {
        background: #ed6b75;
    }

    span.ten-cau {
        margin: 0px -11px;
        width: calc(100% + 22px) !important;
        max-width: unset;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
        display: inline-block;

    }

    .question-area {
        border-top: 1px dashed #eee;
        margin-top: 10px;
        padding-top: 10px;
    }

    .coment-area iframe {
        width: 100%;
    }

    .comment-area {
        max-height: 500px;
        overflow-y: auto;
    }

    .user-post {
        margin-bottom: 16px;
        padding-top: 12px;
        background: #fff;
        border: 0;
    }

    .user-post-content {
        padding: 0px 15px;
    }

    .user-post figure {
        display: inline-block;
        vertical-align: middle;
    }

    .list-user-post {
        background: #dadde1;
    }

    .user-post figure img {
        vertical-align: middle;
        width: 40px;
        height: 40px;
        border-radius: 50%;
        object-fit: cover;

    }

    .gap {
        padding-top: 0;
    }

    a.view-more {
        color: #007bff;
    }

    .hanh-dong {
        width: 100%;
        margin-top: 10px;
        display: inline-block;
    }

    .hanh-dong a {
        width: calc(50% - 6px) !important;
        display: inline-block;
        float: left;

        font-weight: bold;
        padding: 4px 18px;
        background: #f5f4f9 none repeat scroll 0 0;
        border-radius: 4px;
        color: #000;
        display: inline-block;
        font-size: 12px;
        margin: 0 3px;
        vertical-align: middle;
        transition: all 0.2s linear 0s;
    }

    .img-content img {
        max-height: 250px;
        width: unset;
    }

    .img-content {
        /*margin: 0px -25px;*/
        overflow: hidden;
        text-align: center;
        background: #f7f7f7;
        /*width: calc(100% + 40px) !important;*/
    }

    audio {
        height: 28px;
    }

    .cau-hoi {
        border: 1px solid #1877f2;
        border-radius: 10px;
        padding-top: 0;
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .comment-btn {
        float: left;
    }

    .nav-tabs .nav-link {
        cursor: pointer;
        text-align: center;
        font-weight: bold;
        border: 0 !important;
    }

    .nav-tabs li.active .nav-link,
    .nav-tabs li.full-width .nav-link{
        border-bottom: 0;
        background: #1877f2;
        color: #fff;
    }

    ul#myTab {
        width: 100%;
        max-width: unset;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
        display: inline-block;
        float: left;
    }

    .tab-content {
        padding: 10px;
    }

    .nav-tabs .nav-item {
        width: 50%;
        float: left;
        margin: 0;
        list-style: none;
    }

    .nav-tabs .nav-link {
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;
    }

    .nav-tabs li .nav-link {
        border-bottom: 1px solid #1877f2 !important;
    }
    .nav-tabs li:nth-child(1) .nav-link {
        border-top-right-radius: 7px;
        border-top-left-radius: 7px;
    }

    .nav-tabs li:nth-child(2) .nav-link {
        border-top-left-radius: 0 !important;
    }

    .nav-tabs li .nav-link {
        padding: 8px 16px;
        text-align: center;
        width: 100%;
        display: inline-block;
    }

    .tab-pane.fade {
        display: none;
    }

    .tab-pane.fade.show {
        display: block;
    }

    .post-content_html.show-more {
        max-height: unset;
    }

    .post-meta img {
        height: unset !important;
    }
        .overlow-post {
            max-height:300px;
        }
</style>
<script>
    function autoComment(t){
        var tObj = $(t);
        var post_item_parent = tObj.parents(".central-meta.item");
        let post_id = post_item_parent.data("post_id");
        var arrRadio = tObj.parents(".select-radio").find("input[type='radio']");
        tObj.parents('.item').find('.dap_an').show();
        $.each(arrRadio, function( index, value ) {
            var radObj = $(value);
            if(radObj.is(":checked")){
                var t = radObj.parents("label").text();
                t = t.trim();
                t = t.replace(/\s+/g, ' ');
                var classtext = ".text_post_" + post_id;
                var tObj = $(classtext);
                sendCommentAjax(post_id, 0, t,tObj);
            }
        });

    }
</script>