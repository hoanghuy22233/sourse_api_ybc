@if($post->post_id == null && $post->id == null)
@else
@php
    $typeClass = isset($type) && $type =="ajax" ? "ajax-post-" . $post->id : "normal-post";

    $id = isset($post->post_id) ? $post->post_id : $post->id;

@endphp
<div class="{{ $typeClass }} user-post item" data-post_id="{{ $id }}" data-newsfeed_id="{{ $post->newsfeed_id }}">
    <div class="user-post-content">
        <figure>
            <a href="/kenh/{{ @$post->admin->id }}">
                <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$post->admin->image, null, 40) }}"
                     alt="{{ @$post->admin->name }}">
            </a>
        </figure>
        <div class="user-name">
            <h5>
                <a href="/kenh/{{ @$post->admin->id }}">{{ @$post->admin->channel_name != null ? @$post->admin->channel_name : @$post->admin->name }}</a>
            </h5>
            <span><a href="/bai-viet/{{ @$post->id }}">Ngày đăng: {{ date('h:i a d/m/Y', strtotime($post->created_at)) }}</a></span>
        </div>
        <div class="post-meta">
            <strong>{{ $post->name }}</strong>
            <div class="img-content">
                @if($post->link_youtube != null)
                    <?php
                    if (strpos($post->link_youtube, '?v=') !== false) {
                        $code = explode('?v=', $post->link_youtube)[1];
                        $code = explode('&', $code)[0];
                    }

                    ?>
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/{{ @$code }}"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>

                @endif
                @if($post->file_audio != null)
                    <audio controls>
                        <source src="{{ asset('public/filemanager/userfiles/' . $post->file_audio) }}"
                                type="audio/mpeg">
                        Trình duyệt của bạn không hỗ trợ các yếu tố âm thanh.
                    </audio>
                @endif
            </div>
            <div class="description">
                <div class="overlow-post">
                    <div class="post-content_html">
                        @if($post->content != null)
                            <p style="height: unset !important;     width: 100%;
        margin: 0;
    ">
                                {!! $post->content !!}
                            </p>
                        @else
                            @include('themetienganh::mobi.pages.home.partials.hoc_them_cau_hoi')
                        @endif

                    </div>

                    @if($post->content != null && ( $post->content_extra != null || $post->question != null) )
                        @include('themetienganh::mobi.pages.home.partials.hoc_them_cau_hoi', ['class' => 'more-box'])
                    @endif

                </div>
                <a onclick="showMoreOrLess(this)" href="javascript:void(0);" class="view-less-custom show-more"
                   style="color: #007bff">Đọc thêm</a>
            </div>
            <div class="stat-tools d-flex justify-content-around" style="margin-bottom:20px;">
                <div class="like-page">
                    @php
                        $classLike = "";
                            if(@$user){
                                $classLike = $user->likes()->where('post_id', $id)->first() ? "liked" : "";
                            }
                          $post = \Modules\ThemeTiengAnh\Models\Post::find($id);
                          $count_like = $post->likes()->count();
                    @endphp
                    <a data-postId="{{ $id }}" class="btn_like_action {{ $classLike }}" onclick="likepage(this)" href="javascript:void(0)"><i class="fas fa-thumbs-up"></i><span class="like_label">Like</span><span class="count_like">{{ $count_like  }}</span></a>
                </div>
                <a class="comment-btn" href="javascript:;" title="" style="background-color:white;"><i class="lni lni-comments"></i> Bình luận</a>
                <div style="        width: 130px;
    display: inline-block; float: left;" class="fb-share">
{{--                    <div class="fb-share-button"--}}
{{--                         data-href="{{ url('/bai-viet/'.$id) }}"--}}
{{--                         data-width="" data-layout="button_count"--}}
{{--                         data-action="like" data-size="small"--}}
{{--                         data-share="true">--}}
{{--                    </div>--}}
                    <a class="btn-fb-share" onclick="openFbPopUp('{{ url('/bai-viet/'.$id) }}','','','')" value="SHARE">
                        <i class="fab fa-facebook-f"></i><span>Chia sẻ</span>
                    </a>
                </div>
            </div>

            <style>

                .stat-tools .like-page > a {
                    line-height: 18px;
                    border-radius: 4px;
                    padding-top: 5px;
                    padding-bottom: 5px;
                    padding-left: 20px;
                    padding-right: 20px;
                    cursor: pointer;
                }


                .stat-tools .comment-btn {
                    line-height: 18px;
                    border-radius: 4px;
                    padding-top: 5px;
                    padding-bottom: 5px;
                    padding-left: 20px;
                    padding-right: 20px;
                    cursor: pointer;
                }

                .stat-tools .fb-share {
                    line-height: 18px;
                    border-radius: 4px;
                    padding-top: 5px;
                    padding-bottom: 5px;
                    padding-left: 20px;
                    padding-right: 20px;
                    cursor: pointer;
                }

            </style>


            <div class="coment-area" style="display: block;">
                @php
                    $comments = $post->comments()->where('parent_id', 0)->orderBy('id','desc')->paginate(1);
                @endphp
                @if($comments->total() > 1)
                    <a onclick="showMoreComment(this,{{ $id }}, {{ $comments[0]->id }})" class="view_more_cmt" href="javascript:void(0)" style="color: dodgerblue;">Xem thêm bình luận</a>
                @endif
                <div id="list-comment" class="list-comment">
                    <div class="content_list">
                        <div id="area_cmt_more_{{ $id }}"></div>
                        @include('themetienganh::mobi.pages.home.partials.comment', [ 'comments' =>  $comments, 'post' => $post ])
                    </div>
                    @if($user)
                        <div class="box-input-cmt">
                            <div class="box-avatar">
                                <a href="#"><img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($user->image ,45, null) }}" style="border-radius: 100%;height: 24px;width: 24px;" alt=""></a>
                            </div>
                            <div class="box-cmt">
                                <textarea onkeyup="checkSubmitCmt(this)" class="text_post_{{ $id }}" rows="1" style="resize:none;" placeholder="Viết bình luận..."></textarea>
                                <input type="hidden" name="post_id" value="{{ $id }}">
                                <input type="hidden" name="parent_id" value="0">
                                <a onclick="submitComment(this)" class="btn-send-cmt" href="javascript:void(0)"><span><i class="fas fa-paper-plane"></i></span></a>
                            </div>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div><!-- with image -->
</div>
@endif