
@foreach($comments as $k => $comment)

    <div class="item-comment">
        <div class="box-avatar">
            <a href="#"><img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($comment->admin->image ,45, null) }}" alt="" style="border-radius: 100%;
height: 24px;
width: 24px;"></a>
        </div>
        <div class="box_comment">
            <div class="content_comment">
                <a class="name_author" href="#">{{ $comment->admin->name }}</a>
                <p class="content_comment_body">{!! $comment->content !!}</p>
            </div>
            <div class="list-aciton">
                @if($comment->parent_id == 0)
                    <a href="javascript:toogleReply({{ $comment->id }})">Trả lời</a>
                @else
                    <a href="javascript:toogleReply({{ $comment->parent_id }})">Trả lời</a>
                @endif
                <span class="time_ago">{{ \App\Http\Helpers\CommonHelper::formatTimePastCustom($comment->created_at) }}</span>
            </div>
            @php
                $subcomments = $comment->childrens()->orderBy('id','ASC')->paginate(1);
            @endphp
            <div class="list-comment">
                <div class="content_list">
                    @foreach($subcomments as $k => $subcomment)
                        <div class="item-comment">
                            <div class="box-avatar">
                                <a href="#"><img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($comment->admin->image ,45, null) }}" alt="" style="border-radius: 100%;height: 24px;width: 24px;" ></a>
                            </div>
                            <div class="box_comment">
                                <div class="content_comment">
                                    <a class="name_author" href="#">{{ $subcomment->admin->name }}</a>
                                    <p class="content_comment_body">{!!  $subcomment->content !!}}</p>
                                </div>
                                <div class="list-aciton">
                                    <a href="javascript:toogleReply({{ $comment->id }})">Trả lời</a>
                                    <span class="time_ago" style="margin-left: 4px;color: #BEC3C9;font-size: 12px;">{{ \App\Http\Helpers\CommonHelper::formatTimePastCustom($subcomment->created_at) }}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div id="rep_{{ $comment->id }}" class="box-input-cmt" style="display: none">
                    <div class="box-avatar">
                        <a href="#"><img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($comment->admin->image ,45, null) }}" alt="" style="border-radius: 100%;height:24px;width: 24px;"></a>
                    </div>
                    <div class="box-cmt">
                        <textarea onkeyup="checkSubmitCmt(this)" class="text_{{ $comment->id }}" rows="1" style="resize:none;font-family: 'Roboto';" placeholder="Viết bình luận..."></textarea>
                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                        <input type="hidden" name="parent_id" value="{{ $comment->id }}">
                        <a onclick="submitComment(this)" class="btn-send-cmt" href="javascript:void(0)"><span><i class="fas fa-paper-plane"></i></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endforeach

<style>
    .time_ago {
        margin-left:7px;
        color:#BEC3C9;
        font-size:11px;
    }
</style>
