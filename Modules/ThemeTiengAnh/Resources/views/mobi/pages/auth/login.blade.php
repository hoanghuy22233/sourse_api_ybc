<html lang="en">
<head>
    @include('themetienganh::partials.head_meta')

    @include('themetienganh::mobi.partials.head_script')

    <style>
        .menu-button, .profile-info, .follow-men > a, .post-form button, .attachments > ul li:hover, .share-options > ul > li a:hover, .share-options > ul > li a.active, .main-btn, .album > figure span, .main-title::before, .profile-pic > a, .event-timing .postdate, .event-meta > a, ul.tabs li.active a, ul.tabs li.active a:hover, input[type=checkbox]:checked + label:before, .search-area > form button, .form2 button, .paginations > a:hover, .paginations > a.active, .more-attachments.active, .chat-head, .owl-dot.active::before
        {
            background: #1877f2;
        }

        .login-area ul.tabs li.active a::before {
            border-bottom: 8px solid #1877f2;
        }
        .other-option ul li .lni {
            margin-top: 10px;
        }
        .other-option {
            padding: 30px;
        }
        .other-option > ul {
            margin-top: 0;
        }
        .login-area .tabContent {
            padding-bottom: 0;
        }
        .login-area .tabContent {
            padding-top: 15px;
        }
    </style>
</head>
<body class="full-page">
<div class="app-layout">

    <section>
        <div class="gap no-bottom blackish low-opacity 60-vh">
            <div class="bg-image" style="background-image: url({{ URL::asset('public/frontend/themes/edu/images/resources/theme-bg.jpg') }}"></div>
            <!--<div class="content-bg-wrap" style="background: url(images/resources/login-bg.jpg)"></div>-->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="login-area">
                            <div class="logo">
                                <a href="/">
                                    <img class="lazy"
                                         data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo'], null, 50) }}"
                                         style="    max-height: 50px;"
                                         alt="{{ @$settings['name'] }}">
                                </a>
                            </div>
                            <div class="tabBox">
                                <ul class="tabs">
                                    <li class="active"><a href="/dang-nhap"><i class="lni lni-key"></i> Đăng nhập</a></li>
                                    <li><a href="/dang-ky"><i class="lni lni-pencil"></i> Đăng ký</a></li>
                                </ul>
                                <div class="tabContainer">
                                    <div id="signin" class="tabContent" style="display: block;">
                                        <h5>tiếp tục với đăng nhập</h5>
                                        <h4><i class="lni lni-key"></i> Đăng nhập</h4>
                                        @if (Session('success'))
                                            <div class="alert bg-success" role="alert">
                                                <p style="color: red"><b>{!!session('success')!!}</b></p>
                                            </div>
                                        @endif
                                        @if(Session::has('message') && !Auth::check())
                                            <div class="alert text-center text-white " role="alert"
                                                 style=" margin: 0; font-size: 16px;">
                                                <a href="#" style="float:right;" class="alert-close"
                                                   data-dismiss="alert">&times;</a>
                                                <p style="color: red"><b>{{ Session::get('message') }}</b></p>
                                            </div>
                                        @endif
                                        <form method="post" class="form2">
                                            <div>
                                                <input type="text" placeholder="Tên đăng nhập" value="" name="email" required>
                                                <label><i class="lni lni-user"></i></label>
                                            </div>
                                            <div>
                                                <input type="password" placeholder="Password" value="" name="password" required>
                                                <label><i class="lni lni-lock"></i></label>
                                            </div>
                                            <button class="main-btn" type="submit">Đăng nhập</button>
                                            <a href="/quen-mat-khau" title="">Quên mật khẩu?</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="col-lg-12">
        <div class="other-option">
            <h1>hoặc</h1>
            <h5>Đăng ký với</h5>
            <ul>
                <li><a class="facebook" href="/login/facebook/redirect/" title=""><i class="lni lni-facebook"></i></a></li>
                <li><a class="google" href="/login/google/redirect/" title=""><i class="lni lni-google"></i></a></li>
            </ul>
        </div>
    </div>


</div>

@include('themetienganh::mobi.partials.footer_script')

</body>
</html>