<html lang="en">
<head>
    @include('themetienganh::partials.head_meta')

    @include('themetienganh::mobi.partials.head_script')

    <style>
        .menu-button, .profile-info, .follow-men > a, .post-form button, .attachments > ul li:hover, .share-options > ul > li a:hover, .share-options > ul > li a.active, .main-btn, .album > figure span, .main-title::before, .profile-pic > a, .event-timing .postdate, .event-meta > a, ul.tabs li.active a, ul.tabs li.active a:hover, input[type=checkbox]:checked + label:before, .search-area > form button, .form2 button, .paginations > a:hover, .paginations > a.active, .more-attachments.active, .chat-head, .owl-dot.active::before
        {
            background: #1877f2;
        }

        .login-area ul.tabs li.active a::before {
            border-bottom: 8px solid #1877f2;
        }
        .other-option ul li .lni {
            margin-top: 10px;
        }
        .other-option {
            padding: 30px;
        }
        .other-option > ul {
            margin-top: 0;
        }
        .login-area .tabContent {
            padding-bottom: 0;
        }
        .login-area .tabContent {
            padding-top: 15px;
        }
    </style>
</head>
<body class="full-page">
<div class="app-layout">

    <section>
        <div class="gap no-bottom blackish low-opacity 60-vh">
            <div class="bg-image" style="background-image: url({{ URL::asset('public/frontend/themes/edu/images/resources/theme-bg.jpg') }}"></div>
            <!--<div class="content-bg-wrap" style="background: url(images/resources/login-bg.jpg)"></div>-->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="login-area">
                            <div class="logo">
                                <a href="/">
                                    <img class="lazy"
                                         data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo'], null, 50) }}"
                                         style="    max-height: 50px;"
                                         alt="{{ @$settings['name'] }}">
                                </a>
                            </div>
                            <div class="tabBox">
                                <ul class="tabs">
                                    <li class="active"><a href="/dang-nhap"><i class="lni lni-key"></i> Đăng nhập</a></li>
                                    <li><a href="/dang-ky"><i class="lni lni-pencil"></i> Đăng ký</a></li>
                                </ul>
                                <div class="tabContainer">
                                    <div id="signup" class="tabContent" style="">
                                        <h5>tiến hành đăng ký của bạn</h5>
                                        <h4><i class="lni lni-lock"></i> Đăng ký</h4>
                                        <form method="post" class="form2" action="/dang-ky">
                                            <div>
                                                <input type="text" placeholder="Họ & tên" name="name" required>
                                                <label><i class="lni lni-user"></i></label>
                                            </div>
                                            @if($errors->has('name'))
                                                <p style="color: red"><b>{{ $errors->first('name') }}</b></p>
                                            @endif
                                            <div>
                                                <input type="text" placeholder="Số điện thoại" name="tel" required>
                                                <label><i class="lni lni-user"></i></label>
                                            </div>
                                            @if($errors->has('tel'))
                                                <p style="color: red"><b>{{ $errors->first('tel') }}</b></p>
                                            @endif
                                            <div>
                                                <input type="email" placeholder="Email" name="email" required>
                                                <label><i class="lni lni-user"></i></label>
                                            </div>
                                            @if($errors->has('email'))
                                                <p style="color: red"><b>{{ $errors->first('email') }}</b></p>
                                            @endif
                                            <div>
                                                <input type="password" placeholder="Mật khẩu" name="password" required>
                                                <label><i class="lni lni-lock"></i></label>
                                            </div>
                                            @if($errors->has('password'))
                                                <p style="color: red"><b>{{ $errors->first('password') }}</b></p>
                                            @endif
                                            <div>
                                                <input type="password" placeholder="Nhập lại mật khẩu" name="re_password" required>
                                                <label><i class="lni lni-lock"></i></label>
                                            </div>
                                            @if($errors->has('re_password'))
                                                <p style="color: red"><b>{{ $errors->first('re_password') }}</b></p>
                                            @endif
                                            <button type="submit">Đăng ký</button>
                                            <a href="/dang-nhap" title="">Bạn có sẵn sàng để tạo một tài khoản?</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="col-lg-12">
        <div class="other-option">
            <h1>hoặc</h1>
            <h5>Đăng ký với</h5>
            <ul>
                <li><a class="facebook" href="/login/facebook/redirect/" title=""><i class="lni lni-facebook"></i></a></li>
                <li><a class="google" href="/login/google/redirect/" title=""><i class="lni lni-google"></i></a></li>
            </ul>
        </div>
    </div>


</div>

@include('themetienganh::mobi.partials.footer_script')

</body>
</html>