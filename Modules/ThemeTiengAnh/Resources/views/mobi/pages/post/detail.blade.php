@extends('themetienganh::mobi.layouts.mobi')
@section('main_content')
    <section>
        <div class="gap">
            <div class="container">
                <div class="row list-user-post post-content">
                    <?php
                    $posts = [$post]
                    ?>
                    @foreach($posts as $post)
                        @include('themetienganh::mobi.pages.home.partials.post_item')
                    @endforeach
                </div>
                <div class="div-load-more" style="width: 100%; display: inline-block; text-align: center;">

                </div>
            </div>
        </div>
    </section>
@endsection
@section('head_script')
    @include('themetienganh::mobi.pages.home.partials.timeline_css')

    <style>

        .form-radio .radio > label {
            padding: 14px 16px;
            width: 100%;
            background: #f0f2f5;
            margin-bottom: 8px;
            font-size: 14px;

        }

        .select-radio {
            padding: 0;
        }

        .radio .check-box::before,
        .radio input:checked ~ .check-box::after {
            left: 13px;
            top: 16px;
        }

        span.red.ten-cau {
            background: #1877f2;
        }

        .dap_an {
            display: none;
            margin-top: 10px;
        }


    </style>
@endsection
@section('footer_script')
    @include('themetienganh::mobi.pages.home.partials.newsfeed_js')
@endsection
