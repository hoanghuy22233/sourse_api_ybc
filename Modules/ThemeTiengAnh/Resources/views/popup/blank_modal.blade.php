<div class="modal fade" id="blank_modal" role="dialog" style="z-index:1060;">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="width:100%;height:100%">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('click', '.file_image_thumb', function () {
        // alert($(this).attr('src').split('timthumb.php?src=')[1]);
        if ($(this).attr('src').split('timthumb.php?src=')[1] == undefined) {
            $('#blank_modal .modal-body').html('<img src="' + $(this).attr('src') + '"/>');
        } else {
            $('#blank_modal .modal-body').html('<img src="' + $(this).attr('src').split('timthumb.php?src=')[1].split('&w=')[0].split('&h=')[0] + '"/>');
        }
        $('#blank_modal').modal();
    });
</script>