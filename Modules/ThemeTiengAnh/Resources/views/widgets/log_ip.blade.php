<script>
    setTimeout(function () {
        $.ajax({
            url: '/ajax/log-ip',
            type: 'GET',
            data: {
                url_action: 'https://docs.google.com/forms/u/0/d/e/1FAIpQLScy70P6xjcBkcrNSzq-g-R6bUaYXsoEjLHzvVpkLZzO6UrnkQ/formResponse',
                ip: '{{ @$_SERVER['REMOTE_ADDR'] }}',
                url: '{{ substr(@$_SERVER['REQUEST_URI'], 1) }}',
                from: '{{ urlencode(@$_SERVER['HTTP_REFERER']) }}',
            },
            success: function (resp) {
                console.log(resp.msg);
            },
            error: function () {
                console.log('Lỗi ajax log ip');
            }
        });
    }, 2000);
</script>