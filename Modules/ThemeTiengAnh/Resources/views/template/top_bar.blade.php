
<div class="responsive-header">
    <div class="mh-head first mm-sticky mh-btns-left mh-btns-right mh-sticky" style="">
			<span class="mh-btns-left">
				<a href="#menu"><i class="fa fa-align-justify"></i></a>
			</span>
        <span class="mh-text">
				<a href="" title=""><img class="lazy"
                            data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo-top'], null, 95) }}"
                            alt="{{ @$settings['name'] }}"></a>
			</span>
        <span class="mh-btns-right">
				<a class="fa fa-sliders" href="#shoppingbag"></a>
			</span>
    </div>
    <div class="mh-head second">
        <form class="mh-form">
            <input placeholder="Tìm kiếm">
            <a href="#/" class="fa fa-search"></a>
        </form>
    </div>
</div>

{{--menu--}}

{{--end-menu--}}
<div class="topbar stick">

    <div class="logo">
        <a title="{{ @$settings['name'] }}" href="/"><img class="lazy" data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                                  style="    max-height: 45px;"
                                  alt="{{ @$settings['name'] }}"></a>
    </div>
    <div class="top-search">
        <form action="/tim-kiem" method="get" class="">
            <select class="select-do-kho form-control" name="do_kho">
                <option value="">Mọi trình độ</option>
                <option value="dễ" {{ @$_GET['do_kho'] == 'dễ' ? 'selected' : '' }}>Trình độ dễ</option>
                <option value="trung bình" {{ @$_GET['do_kho'] == 'trung bình' ? 'selected' : '' }}>Trình độ Trung bình</option>
                <option value="khó" {{ @$_GET['do_kho'] == 'khó' ? 'selected' : '' }}>Trình độ khó</option>
            </select>
            <input type="search" name="keyword" class="form-control tim-kiem" value="{{ @$_GET['keyword'] }}"
                   placeholder="Tìm kiếm bài viết">
            <button data-ripple=""><iz class="ti-search"></iz></button>
        </form>
    </div>
    <div class="top-area">
        @if(\Auth::guard('admin')->check())
            @include('themetienganh::partials.user_image')
        @else
            <div class="user-img">
                <h5><a href="/dang-nhap">Đăng nhập</a></h5>
                <h5><a href="/dang-ky">Đăng ký</a></h5>

            </div>
        @endif


        <span class="" data-ripple=""></span>
    </div>
</div>

<style>
    .top-search .tim-kiem {
        padding-right: 54px;
        border-radius: 5px;
    }
    .user-img > h5 {
        width: 70px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
    .top-search .select-do-kho{
        font-size: 13px;
        background-color: white;
        border: none;
    }

    @media (min-width: 768px) {
        .top-search .select-do-kho {
            border-radius: 5px;
        }
    }
</style>