<style>
    .form-group-status,
    .form-group-role {
        display: none;
    }
</style>
<?php
$result = \App\Models\Admin::find(@\Auth::guard('admin')->user()->id);
?>
<div class="form-group row">
    <label class="col-xl-3 col-lg-3 col-form-label">Tên kênh</label>
    <div class="col-lg-9 col-xl-6">
        <?php
        $field = ['name' => 'channel_name', 'type' => 'text', 'label' => 'Tên kênh', 'value' => @$result->channel_name, 'des' => 'Nếu không điền sẽ lấy họ & tên làm tên kênh'];
        ?>
        @include(config('core.admin_theme').'.form.fields.' . $field['type'], ['field' => $field])
        <span class="form-text text-muted">{{ $field['des'] }}</span>
    </div>
</div>
<div class="form-group row">
    <label class="col-xl-3 col-lg-3 col-form-label">Banner</label>
    <div class="col-lg-9 col-xl-6">
        <?php
        $field = ['name' => 'banner', 'type' => 'file_image', 'label' => 'Ảnh banner', 'value' => @$result->banner,];
        if (@$result->id != \Auth::guard('admin')->user()->id) {

            $field['disabled'] = true;
        }
        ?>
        @include(config('core.admin_theme').'.form.fields.' . $field['type'], ['field' => $field])
    </div>
</div>