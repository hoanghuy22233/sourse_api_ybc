<!DOCTYPE html>
<html lang="vi">
{{--class="loading-site no-js">--}}
<head>
    @include('themetienganh::partials.head_meta')
    @include('themetienganh::partials.head_script')
    {!! @$settings['frontend_head_code'] !!}
    @yield('head_script')
</head>

<body class="stretched">

{{--@include('themesemicolonwebjdes::partials.header')--}}
@yield('main_content')
<!-- #wrapper -->
{{--@include('themetienganh::partials.footer')--}}
@include('themetienganh::partials.footer_script')
@yield('footer_script')
@include('themetienganh::partials.user_menu_mobile')
@include('themetienganh::partials.main_menu_mobile')
@include('themetienganh::partials.chat_facebook')

{!! @$settings['frontend_body_code'] !!}


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-EJ07G206GX"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'G-EJ07G206GX');
</script>


@include('themetienganh::widgets.log_ip')
</body>
</html>
