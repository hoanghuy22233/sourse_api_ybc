{{--<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700|Roboto:300,400,500,600,700">--}}
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/main.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/style.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/color.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/responsive.css') }}">
{{--<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/style(2).css') }}">--}}
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/common.css') }}">
<link rel="stylesheet" href="{{ asset('public/libs/toastr/toastr.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/tieng_anh/css/custom.css') }}?v={{ time() }}">
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/tieng_anh/css/social.css') }}">
{{--Custom--}}
<script src="{{ URL::asset('public/libs/jquery-3.4.0.min.js') }}"></script>