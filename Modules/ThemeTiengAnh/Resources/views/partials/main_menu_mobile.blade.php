<?php
$menus = CommonHelper::getFromCache('menu_main_menu', ['menus']);
if (!$menus) {
    $menus = \Modules\ThemeTiengAnh\Models\Menu::where('status', 1)->where('location', 'main_menu')->whereNull('parent_id')->orderBy('order_no', 'desc')->orderBy('name', 'asc')->get();
    CommonHelper::putToCache('menu_main_menu', $menus, ['menus']);
}
?>
<style>
    @media(max-width: 768px) {
        .mm-menu .mm-listview>li .has-childs {
            width: 80%;
            display: inline-block;
        }
        .mm-menu .mm-listview>li.has-childs:after {
            content: '';
            border-top: 2px solid transparent;
            border-left: 2px solid transparent;
            border-bottom: 0;
            display: block;
            width: 8px;
            height: 8px;
            margin: auto;
            position: absolute;
            top: 0;
            bottom: 0;
            border-color: rgba(0,0,0,.3);
            transform: rotate(135deg);
            right: 23px;
            left: auto;
        }
        .mm-menu .menu-child li a {
            padding: 5px 0px;
            display: inline-block;
            width: 100%;
        }

    }
</style>
<nav id="menu" class="res-menu mm-menu mm-offcanvas" aria-hidden="true">
    <div class="mm-panels">
        <div class="mm-panel mm-hasnavbar mm-opened" id="mm-1">
            <div class="mm-navbar"><a class="mm-title">Menu</a></div>
            <ul class="mm-listview">
                @foreach($menus as $menu)
                    <?php
                    $menu_childs = CommonHelper::getFromCache('menus_childs_' . $menu->id, ['menus']);
                    if (!$menu_childs) {
                        $menu_childs = $menu_childs = $menu->childs;
                        CommonHelper::putToCache('menus_childs_' . $menu->id, $menu_childs, ['menus']);
                    }
                    ?>
                    <li class="{{ count($menu_childs) > 0 ? 'has-childs' : '' }}"><a class="nav-link {{ count($menu_childs) > 0 ? 'has-childs' : '' }}" href="{{ $menu->url }}">{!! $menu->name !!}</a>
                        @if(count($menu_childs) > 0)
                            <div class="menu-child">
                                <ul>
                                    @foreach($menu_childs as $child)
                                        <li><a href="{{@$child->url}}" title="">{!! $child->name !!}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</nav>
<script>
    $('.mm-menu .mm-listview>li').click(function () {
        $(this).find('.menu-child').slideToggle(100);
    });
</script>