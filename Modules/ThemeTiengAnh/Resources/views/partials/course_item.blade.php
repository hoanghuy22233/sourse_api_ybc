
<div class="central-meta item">
    <div class="classi-pst">
        <div class="row merged10">
            <div class="col-lg-6">
                <div class="image-bunch single">
                    <figure><img class="lazy"
                                data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($course->image,244, null) }}"
                                alt="{{@$course->name}}"></figure>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="classi-pst-meta">
                    <h6><a href="/khoa-hoc/{{(@$course->slug)}}.html"
                           title="{{@$course->name}}">{{@$course->name}}</a></h6>
                    @if(!isset($is_mau_kinh_doanh))
                    <span><i class="fas fa-user"></i> Trình độ: {{$course->level}}</span>
                    <span><i class="fa fa-money-bill"></i> @if($course->base_price != 0)<strike>{{number_format(@$course->base_price,0,'','.')}}<sup>đ</sup></strike>@endif  <span
                                style="color: red">{!! !empty($course->final_price) ? number_format(@$course->final_price,0,'','.').'<sup>đ</sup>' : 'Miễn phí' !!}</span></span>
                    <span><i class="fas fa-user"></i> Giảng viên: {{@$course->lecturer->name}} </span>
                    <span><i class="fas fa-tags"></i>Thể loại: {{ $course->type }}</span>
                    @endif
                    {{--<span><i class="fas fa-play-circle"></i> Bài học: {{@$course->lesson->count()}} bài</span>--}}
                    {{--<span><i class="far fa-clock"></i> Thời hạn: {{$course->date_of_possession==0 ? 'Mãi mãi':$course->date_of_possession}}</span>--}}
                </div>
            </div>
            <div class="col-lg-12">
                {{--<p class="classi-pst-des">
                    {!!@$course->intro !!}
                </p>--}}
                @if(in_array($course->id, $course_buyed))
                    <a class="active btn btn-info" href="/khoa-hoc/{{(@$course->slug)}}.html"
                       style="background-color: #fa6342;float: right;border-color: #bc482e;">Học ngay</a>

                @else
                    <a class="active btn btn-info" href="/khoa-hoc/{{(@$course->slug)}}.html"
                       style="background-color: #fa6342;float: right;border-color: #bc482e;">Xem chi tiết</a>
                @endif
            </div>
        </div>
    </div>
</div>
