<div class="user-img">
    <h5 style="margin-bottom: 5px;">{{@\Auth::guard('admin')->user()->name}}</h5>
    <img
            class="lazy" style="margin-bottom: 10px;max-width: 40px;max-height:40px;"
            data-src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb(\Auth::guard('admin')->user()->image,40,40)}}"
            alt="{{@\Auth::guard('admin')->user()->name}}">
    <span class="status f-online"></span>
    <div class="user-setting">
        <ul class="log-out">
            @if(\Auth::guard('admin')->check())
                <li><a href="/admin/profile" title=""><i class="ti-user"></i>Xem profile</a></li>
                @if(\App\Http\Helpers\CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'post_add'))
                    <li><a href="/admin/post" title=""><i class="ti-pencil-alt"></i>Đăng bài</a></li>
                @else
                    <li><a href="/tao-kenh" title=""><i class="ti-pencil-alt"></i>Đăng bài</a></li>
                @endif
            @endif
            <hr style="margin-bottom: 0px;margin-top: -1px;">
            <li><a href="/dang-xuat" title=""><i class="ti-power-off"></i>Đăng xuất</a></li>
        </ul>
    </div>
</div>

<script>
    $('.user-img').hover(
        function () {
            $('.user-setting').addClass('active');
        },
        function () {
            $('.user-setting').removeClass('active');
        }
    );
</script>