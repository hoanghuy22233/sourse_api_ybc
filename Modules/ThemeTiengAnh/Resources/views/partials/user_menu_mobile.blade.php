<nav id="shoppingbag" class="mm-menu mm-offcanvas mm-right" aria-hidden="true">
    <div class="mm-panels">
        <div class="mm-panel mm-hasnavbar mm-opened" id="mm-13">
            <div class="">
                @if(\Auth::guard('admin')->check())
                    <div class="setting-row">
                        <span><a href="/admin/profile" title=""><i class="ti-user"></i> Xem profile</a></span>
                    </div>
                    <div class="setting-row">
                        <span><a href="/profile/edit" title=""><i class="ti-pencil-alt"></i> Sửa profile</a></span>
                    </div>
                    <div class="setting-row">
                        <span><a href="/profile/khoa-hoc" title=""><i class="fa fa-book"></i> Khóa học</a></span>
                    </div>
                    <div class="setting-row">
                        <span><a href="/dang-xuat" title=""><i class="ti-power-off"></i> Đăng xuất</a></span>
                    </div>
                @else
                    <div class="setting-row">
                        <span><a href="/dang-nhap"> Đăng nhập</a></span>
                    </div>
                @endif
            </div>
        </div>
    </div>
</nav>