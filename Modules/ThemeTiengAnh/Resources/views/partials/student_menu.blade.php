<div class="che_man" style="display: none"></div>
<figure>
    <img data-src="{{ asset('public/filemanager/userfiles/' . @$user->banner,1110,300) }}" class="lazy"
         alt="{{@@$user->name}}">
</figure>
<div class="profile-section">
    <div class="row">
        <div class="col-lg-2">
            <div class="profile-author">
                <a class="profile-author-thumb"
                   href="/profile/{{ @$user->id }}">
                    <img alt="{{@@$user->name}}"
                         data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($user->image,155) }}" class="lazy">
                </a>
                <div class="author-content">
                    <a class="h4 author-name"
                       href="/profile/{{ @$user->id }}">{{ @$user->name }}</a>
                    <div class="country">{{ @$user->type != '' ? @$user->type : 'Học viên' }}</div>
                </div>
            </div>
        </div>
        <div class="col-lg-10">
            <ul class="profile-menu">
                <li>
                    <a @yield('khoahoc') href="/student/{{ @$user->id }}/khoa-hoc">Khóa
                        học</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div id="nhiem-vu" style="display: none;" class="popup login">
    <span class="popup-closed xxx"><i class="ti-close"></i></span>
    <div class="popup-meta ">
        <div class="login-frm">
            <?php
            $missons = \Modules\EduCourse\Models\Misson::where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'asc')->take(3)->get();
            ?>
            @foreach($missons as $k=>$misson)
                <?php
                $check = \Modules\ThemeTiengAnh\Models\HistoryMisson::where('student_id', @$user->id)->where('misson_id', @$misson->id)->count();
                $student = @\Modules\ThemeTiengAnh\Models\HistoryMisson::where('student_id', @$user->id)->where('misson_id', @$misson->id)->first();
                ?>
                @if($check > 0 || $k==0)
            <a href="/student/{{ @$user->id }}/{{@$misson->slug}}"
               class=" d-flex justify-content-between align-items-center "
               style="font-size: large;padding: 0.5em 16px; border: 1px solid #ccc!important; border-radius: 16px; margin-bottom: 10px; ">
                {{$misson->name}}
                @if(isset($student))
                    <i class="fa fa-check" style="color: limegreen;"></i>
                    @endif
            </a>
                    @else
                        <a rel="nofollow"
                           class=" d-flex justify-content-between align-items-center "
                           style="color:#757a95 ;font-size: large;padding: 0.5em 16px; border: 1px solid #ccc!important; border-radius: 16px; margin-bottom: 10px; ">
                            {{$misson->name}}
                        </a>
                    @endif
                @endforeach
                <span style="color:red;"><b><i>Lưu ý: Phải thực hiện lần lượt từng nhiệm vụ</i></b></span>
        </div>
    </div>
</div>



<script>
    $('.chua-hoan-thanh').click(function () {
        var misson_id = $(this).data('misson_id');
        $.ajax({
            url: '{{route('no-misson')}}',
            type: 'get',
            data: {
                misson_id: misson_id,
            },
            success: function (data) {
                if (data.status) {
                    alert(data.msg);
                    location.reload();
                } else {
                    alert('Có lỗi xảy ra. Vui lòng load lại website và thử lại!');
                }
            }
        })
    });
</script>
<script>
    $(document).ready(function () {
        $(".show_nhiem_vu").click(function () {
            $('#nhiem-vu').show();
            $('.che_man').show();
        });


    });
</script>
<script>
    $(document).ready(function () {
        $("#nhiem-vu .xxx").click(function () {
            $('#nhiem-vu').hide();
            $('.che_man').hide();
        });
        $(".che_man").click(function () {
            $('#nhiem-vu').hide();
            $('.che_man').hide();
        });
    });
</script>
{{--@endif--}}
