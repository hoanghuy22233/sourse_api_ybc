{{--{{dd($course)}}--}}
<title>@if(isset($pageOption)){{ isset($pageOption['meta_title']) && @$pageOption['meta_title'] != '' ? @$pageOption['meta_title'] : @$course->name }}@else{!! @$settings['name'] !!}@endif</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="@if(isset($pageOption)){{ isset($pageOption['meta_title']) && $pageOption['meta_title'] != '' ? $pageOption['meta_title'] : @$course->name }}@else{!! @$settings['name'] !!}@endif"
      name="title">
<meta content="@if(isset($pageOption)){{ isset($pageOption['meta_keywords']) && $pageOption['meta_keywords'] != '' ? $pageOption['meta_keywords'] : @$course->name }}@else{!!@$settings['default_meta_keywords'] !!}@endif"
      name="keywords">
<meta content="@if(isset($pageOption)){!! isset($pageOption['meta_description']) && $pageOption['meta_description'] != '' ? $pageOption['meta_description'] : @$course->name !!}@else{!!@$settings['default_meta_description'] !!}@endif"
      name="description">
<meta content="{{@$settings['robots'] }}" name="robots">
<link rel="shortcut icon" href="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['favicon'], null, 16) }}"
      type="image/x-icon">
<link rel="alternate" hreflang="vi" href="{{ URL::to('/') }}"/>
<meta property="og:title"
      content="@if(isset($pageOption)){{ isset($pageOption['meta_title']) && $pageOption['meta_title'] != '' ? $pageOption['meta_title'] : @$course->name }}@else{!!@$settings['name'] !!}@endif"/>
<meta property="og:description"
      content="@if(isset($pageOption)){!! isset($pageOption['meta_description']) && $pageOption['meta_description'] != '' ? $pageOption['meta_description'] : @$course->name !!}@else{!!@$settings['default_meta_description'] !!}@endif"/>
<meta property="og:image"
      content="@if(isset($pageOption['image'])){{ $pageOption['image'] }}@else{{ URL::asset('public/filemanager/userfiles/' .@$settings['logo']) }}@endif"/>
<meta property="og:url"
      content="@if(isset($pageOption['link'])){{ $pageOption['link'] }}@else{{ "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}@endif"/>
<link rel="canonical"
      href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
      integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<meta property="og:type" content="website"/>
<meta property="og:site_name" content="{{ URL::to('/') }}"/>
<meta name="csrf-token" content="{{ csrf_token() }}" />
{{--<link rel='stylesheet' id='slideshow-jquery-image-gallery-stylesheet_functional-css'  href='{{ URL::asset(config('frontend_asset').'/css/functional.css') }}' type='text/css' media='all' />--}}
{{--<link rel='stylesheet' id='slideshow-jquery-image-gallery-stylesheet_style-light-css'  href='{{ URL::asset(config('frontend_asset').'/css/style-light.css') }}' type='text/css' media='all' />--}}

<script data-ad-client="ca-pub-8048945401852085" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>