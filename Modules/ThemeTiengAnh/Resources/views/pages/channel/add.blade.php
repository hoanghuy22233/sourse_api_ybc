@extends('themetienganh::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        @include('themetienganh::template.top_bar')
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">

                                <div class="col-lg-3 cot-1">
                                    <aside class="sidebar static left ">
                                        @include('themetienganh::pages.home.partials.tags')
                                    </aside>
                                </div>

                                <form action="" method="POST" class="col-lg-6 cot-2" enctype="multipart/form-data">
                                    <div class="title1">Tạo kênh</div>
                                    <div class="social-name">
                                        <label><i class="fa fa-facebook-square"></i> Tên kênh</label>
                                        <input type="text" name="channel_name" placeholder="" required>
                                    </div>
                                    <div class="social-name">
                                        <label><i class="fa fa-twitter-square"></i> Ảnh đại diện</label>
                                        <input type="file" name="channel_image" required>
                                    </div>
                                    <div class="social-name">
                                        <label><i class="fa fa-twitter-square"></i> Ảnh banner</label>
                                        <input type="file" name="banner" required>
                                    </div>
                                    <div class="social-name">
                                        <label><i class="fa fa-instagram"></i> Điều khoản</label>
                                        <p>{!! @\App\Models\Setting::where('name', 'dieu_khoan')->first()->value !!}</p>
                                        <label><input type="checkbox" placeholder="" required> Chấp nhận</label>
                                    </div>
                                    <div class="col-lg-12">
                                        <button class="main-btn" type="submit" data-ripple="">Tạo kênh</button>
                                    </div>
                                </form>

                                <div class="col-lg-3 cot-3">
                                    <aside class="sidebar static left">
                                        @include('themetienganh::pages.home.partials.kenh')
                                    </aside>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection
@section('head_script')
    @include('themetienganh::pages.home.partials.newsfeed_css')
@endsection
@section('footer_script')
    <script>
        var loaded = false;
    </script>
@endsection