@extends('themetienganh::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        @include('themetienganh::template.top_bar')
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">

                                {{--Hiển thị khối widgets--}}
                                <?php
                                $widgets = CommonHelper::getFromCache('widgets_home_sidebar_center', ['widgets']);
                                if (!$widgets) {
                                    $widgets = \Modules\ThemeEdu\Models\Widget::where('location', 'home_content')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
                                    CommonHelper::putToCache('widgets_home_sidebar_center', $widgets, ['widgets']);
                                }
                                ?>
                                @foreach($widgets as $widget)
                                    {{--{{ dd(json_decode($widget->config)->view) }}--}}
                                    <div class="col-lg-12">
                                        @if($widget->type == 'html')
                                            <div class="gap">
                                                <div class="container">
                                                    <div class="row">
                                                        {!!$widget->content !!}
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            @include(@json_decode($widget->config)->view, ['config' => (array) json_decode($widget->config)])
                                        @endif
                                    </div>
                                @endforeach
                                {{--END: Hiển thị khối widget--}}

                                <div class="col-lg-3 cot-1">
                                    <aside class="sidebar static left ">
                                    </aside>
                                </div>

                                <div class="col-lg-6 cot-2">

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="pittop-banner">
                                                <figure><img
                                                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$channel->banner, null) }}"
                                                            alt="{{ $channel->channel_name != '' ? $channel->channel_name : $channel->name }}"></figure>
                                                <div class="pittop-meta">
                                                    <figure><img
                                                                src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($channel->image, 114, null) }}"
                                                                alt=""></figure>
                                                    <div style="width: 350px;">
                                                        <h6>{{$channel->channel_name != '' ? $channel->channel_name : $channel->name}}</h6>
                                                        <span><i class="fa fa-heart"></i> <span
                                                                    class="channel_follow_count channel_follow_{{ $channel->id }}"
                                                                    id="">{{ number_format(\Modules\ThemeTiengAnh\Models\Follow::where('channel_id', $channel->id)->count(), 0, '.', '.') }}</span> theo dõi - <i
                                                                    class="fa fa-book"></i> {{ number_format(\Modules\ThemeTiengAnh\Models\Post::where('status', 1)->where('admin_id', $channel->id)->count(), 0, '.', '.') }} bài viết</span>
                                                    </div>
                                                </div>
                                                @if(\Modules\ThemeTiengAnh\Models\Follow::where('channel_id', $channel->id)->where('admin_id', @\Auth::guard('admin')->user()->id)->count() == 0)
                                                    <a href="javascript:;" title="Theo dõi/Bỏ theo dõi"
                                                       class="theo-doi likes heart channel_follow_icon_{{ $channel->id }}" data-id="{{ $channel->id }}"><i
                                                                class="lni lni-checkmark"></i> Theo dõi</a>
                                                @else
                                                    <a href="javascript:;" title="Theo dõi/Bỏ theo dõi"
                                                       class="theo-doi likes heart liked channel_follow_icon_{{ $channel->id }}"
                                                       data-id="{{ $channel->id }}"><i
                                                                class="lni lni-checkmark"></i> Theo dõi</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="post-content">
                                        @php
                                            $user = \Auth::guard('admin')->user();
                                            if($user){
                                                  $user = \Modules\ThemeTiengAnh\Models\Admin::find($user->id);
                                            }

                                        @endphp
                                        @foreach($posts as $post)
                                            @include('themetienganh::pages.home.partials.post_item')
                                        @endforeach

                                    </div>
                                    @if(count($posts) >= 5)
                                        <div class="div-load-more"
                                             style="width: 100%; display: inline-block; text-align: center;">

                                        </div>
                                    @endif
                                </div>

                                <div class="col-lg-3 cot-3">
                                    <aside class="sidebar static left">
                                        @include('themetienganh::pages.home.partials.kenh')
                                    </aside>
                                </div>

                                @include('themetienganh::partials.form_contact')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection
@section('head_script')
    @include('themetienganh::pages.home.partials.newsfeed_css')
@endsection
@section('footer_script')
    @include('themetienganh::pages.home.partials.newsfeed_js')
    <script>

        //  Load thêm content
        var loaded = false;
        $(window).scroll(function() {
            if( ( $(window).scrollTop() + $(window).height() ) >= $(document).height() ) {
                if(!loaded) {
                    loaded = true;
                    $('.div-load-more').html('<img src="{{ asset('public/images_core/loading.gif') }}">');

                    //  Bỏ qua số bài đang có trên newsfeed và só bài vừa rend mới bằng ajax
                    var skip = parseInt($('.central-meta.item').length) + parseInt($('input[name=ajax_rend_content]').val());
                    console.log(parseInt($('input[name=ajax_rend_content]').val()));

                    $.ajax({
                        url: '/newsfeed/load-more',
                        type: 'GET',
                        data: {
                            admin_id: '{{ @\Auth::guard('admin')->user()->id }}',
                            skip: skip,
                            take: 5
                        },
                        success: function (html) {
                            if (html.html != '') {
                                $('.post-content').append(html.html);
                                $('.div-load-more').html('');
                                $('.comment.ajax').on('click', function () {
                                    $(this).parents(".post-meta").siblings(".coment-area").slideToggle("slow");
                                });
                                var strSelector = "";
                                $.each(html.post_id, function( index, value ) {
                                    strSelector += ',.ajax-post-'+value+' .box-cmt textarea';
                                });
                                strSelector = strSelector.substr(1);
                                actionComment(strSelector);
                                checkLoadMore(".overlow-post:not(.loaded)");
                                var myVar = setInterval(checkLoadMore, 1000, ".overlow-post:not(.loaded)");
                            } else {
                                $('.div-load-more').remove();
                            }
                            loaded = false;
                        },
                        error: function () {
                            console.log('Lỗi load thêm dữ liệu');
                            $('.div-load-more').html('');
                            loaded = false;
                        }
                    });
                }
            }
        });
    </script>
    <script>
        function likepage(tHtml){
            var t = $(tHtml);
            var postId = t.data('postid');
            $.ajax({
                url: "{{ route('user.like') }}",
                data: {'postid': postId},
                dataType: 'json',
                success:function(repsonse){
                    if(repsonse.status == 'success'){
                        if(repsonse.class == "liked"){
                            t.removeClass("dislike");
                            t.addClass(repsonse.class);
                        }else{
                            t.removeClass("liked");
                            t.addClass(repsonse.class);
                        }

                        t.find(".count_like").stop().html(repsonse.count);
                    }

                },
                error:function(e){
                    if(e.status == 403){
                        window.location.href = "/dang-nhap";
                    }
                }
            });
        }

        function actionComment(selector){

            $(selector).keyup(function (e) {
                if (e.keyCode == 13) {
                    if (e.shiftKey) {
                        var text = $(this).val();
                        var lines = text.split("\n");
                        console.log(lines);
                        var count = lines.length;
                        count = count <= 0 ? 1 : count;
                        $(this).attr('rows', count);
                        return true;
                    }else if(e.ctrlKey){

                        var text = $(this).val();
                        $(this).val(text + "\n" );
                        var lines = text.split("\n");
                        console.log(lines);
                        var count = lines.length;
                        count = count <= 0 ? 1 : count;
                        $(this).attr('rows', count);
                        return true;
                    }else{

                        ajaxComment(this);
                        //console.log('submit');
                        return false;
                    }
                }
                if (e.keyCode == 8) {
                    var text = $(this).val();
                    text = text.replace(/\s$/, '');
                    var lines = text.split("\n");
                    var count = lines.length;
                    count = count <= 0 ? 1 : count;
                    $(this).attr('rows', count);
                }
                return false;
            });
            return false;
        }
        $(document).ready(function () {
            actionComment('.box-cmt textarea');
        });
        // $('.box-cmt textarea').change(function (e) {
        //     var text = $(this).val();
        //     var lines = text.split("\n");
        //     var count = lines.length;
        //     count = count <= 0 ? 1 : count;
        //     $(this).attr('rows', count);
        // });
        function ajaxComment(t){
            var post_id = $(t).parents(".box-cmt").find("input[name='post_id']").stop().val();
            var parent_id = $(t).parents(".box-cmt").find("input[name='parent_id']").stop().val();
            let tValue = $(t).val();
            $.ajax({
                url: "/user/comment",
                type:"POST",
                dataType: "json",
                data:{
                    parent_id: parent_id,
                    post_id: post_id,
                    text : tValue,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){
                    $(t).attr('rows', 1);
                    $(t).val("");
                    $(t).blur();

                    //$(t).parents(".list-comment").stop().append(response);
                    var lstParent = $(t).parents(".list-comment");

                    $(lstParent[0]).children(".content_list").append(response.html);
                    //location.reload();
                    actionComment('.box-cmt textarea.text_' + response.comment_id);
                    //actionComment(t);

                }
            });
        }
        function toogleReply(id){
            var id = "#rep_" + id;
            $(id).toggle(200);
        }
        function showMoreComment(t, postid, idcomment){
            $.ajax({
                url: "/user/comment-more",
                type:"POST",
                data:{
                    comment_id: idcomment,
                    post_id: postid,

                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){

                    $("#area_cmt_more_" + postid).html(response.html);

                    $(t).hide();
                    var strSelector = "";
                    $.each(response.array_id_comment, function( index, value ) {
                        strSelector += ',.normal-post .box-cmt textarea.text_' + value;
                    });
                    strSelector = strSelector.substr(1);
                    actionComment(strSelector);
                    //location.reload();
                    //actionComment();
                }
            });
        }
        function sendCommentAjax(post_id, parent_id, tValue, t){

            $.ajax({
                url: "/user/comment",
                type:"POST",
                dataType: "json",
                data:{
                    parent_id: parent_id,
                    post_id: post_id,
                    text : tValue,
                    _token: $('meta[name="csrf-token"]').attr('content')
                },
                success:function(response){
                    // $(t).attr('rows', 1);
                    // $(t).val("");
                    // $(t).blur();


                    var lstParent = t.parents(".list-comment");

                    $(lstParent[0]).children(".content_list").append(response.html);

                    actionComment('.box-cmt textarea.text_' + response.comment_id);


                }
            });
        }

        // autoComment();
    </script>
@endsection