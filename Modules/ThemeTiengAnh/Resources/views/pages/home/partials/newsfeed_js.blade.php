<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v9.0&appId=561583071141201&autoLogAppEvents=1"
        nonce="R5zAQPxH"></script>
<script>

    $(document).on('click', '.view-more', function () {
        var hExactly = $(this).parents(".description");
        console.log(hExactly);
        $(this).parents('.description').find('.more').slideDown(200);
        $(this).parents('.description').find('.post-content_html').addClass('show-more');
        $(this).parents('.description').find('.view-less').show();
        $(this).hide();
    });

    $(document).on('click', '.view-less', function () {
        $(this).parents('.description').find('.more').slideUp(200);
        $(this).parents('.description').find('.view-more').show();
        $(this).parents('.description').find('.post-content_html').removeClass('show-more');
        $(this).hide();
    });

    $(document).on('click', '.view-question-less', function () {
        $(this).parents('.question-area').slideUp(200);
    });

    $(document).on('click', '.question-btn', function () {
        $(this).parents('.user-post').find('.question-area').slideToggle(200);
        $(this).parents('.user-post').find('.coment-area').slideUp(1);
    });

    $(document).on('click', 'span.comment', function () {
        $(this).parents('.user-post').find('.question-area').slideUp(1);
    });

    $(document).on('click', '.xac-nhan', function () {
        $(this).parents('.cau-hoi').find('.dap_an').show();
    });


</script>

<script>
    $(document).ready(function () {
        var $obj = $('.block-fixed');
        var top = $obj.offset().top - parseFloat($obj.css('marginTop').replace(/ajax_rend_content/, 0));

        $(window).scroll(function (event) {
            // what the y position of the scroll is
            var y = $(this).scrollTop();

            // whether that's below the form
            if (y >= top) {
                // if so, ad the fixed class
                $obj.addClass('fixed');
            } else {
                // otherwise remove it
                $obj.removeClass('fixed');
            }
        });


        $(document).on('click', '.nav-tabs .nav-link', function () {
            $(this).parents('li').addClass('active');
            $(this).parents('li').siblings().removeClass('active');

            $(this).parents('.myTab-box').find($(this).data('tab')).addClass('show active');
            $(this).parents('.myTab-box').find($(this).data('tab')).siblings().removeClass('show active');
        });
    });

    $(document).ready(function () {
        checkLoadMore(".overlow-post:not(.loaded)");
    });
    function checkLoadMore(selector){
        var arrOverPost = $(selector);

        $.each(arrOverPost, function( index, value ) {
            var itempost = $(value);
            var hBefore = value.scrollHeight;
            var hAfter = itempost.height();
            //itempost.addClass("loaded");
            if(hAfter < hBefore){
                // show more
                var parent =  itempost.parents(".description");
                parent.children(".view-less-custom").remove();
                parent.append('<a onclick="showMoreOrLess(this)" href="javascript:void(0);" class="view-less-custom show-more" style="color: #007bff">Đọc thêm</a>');
            }else{
                var parent =  itempost.parents(".description");
                parent.children(".view-less-custom:not(.loaded)").remove();
            }
        });
    }
    function showMoreOrLess(t){
        var tObj = $(t);
        tObj.addClass("loaded");
        var classAdd = "show-more";
        var classRemove = "show-less";
        var txtAdd = "Đọc thêm";
        var maxHeight = "800px";
        if(tObj.hasClass("show-more")){
            var classAdd = "show-less";
            var classRemove = "show-more";
            var txtAdd = "Ít hơn";
            var maxHeight = "initial";
        }
        tObj.parents(".description").children(".overlow-post").css({"maxHeight":maxHeight});
        tObj.removeClass(classRemove);
        tObj.addClass(classAdd);
        tObj.text(txtAdd);
    }
</script>
<script>
    function changeTag(t){
        var tObj = $(t);
       tObj.toggleClass("active");
       var liActive = tObj.parents("ul").children("li.active");
       var params = "";
        $.each( liActive, function( key, value ) {
           var liObj = $(value);
           var idtag = liObj.data('id');
            params += "," + idtag;
        });
        params = params.substr(1);
        window.location.href = "/tim-kiem?idtag=" + params;
    }
</script>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '455116562321104',
            xfbml      : true,
            version    : 'v2.8'
        });
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    function openFbPopUp(fburl,fbimgurl,fbtitle,fbsummary) {
        FB.ui(
            {
                method: 'feed',
                name: fbtitle,
                link: fburl,
                picture: fbimgurl,
                caption: fbtitle,
                description: fbsummary
            }
        );
    }
</script>