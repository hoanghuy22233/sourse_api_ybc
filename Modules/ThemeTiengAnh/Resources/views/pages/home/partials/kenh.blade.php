<div class="widget list-channel block-fixed">
    <h4 class="widget-title">Các kênh</h4>
    <?php
    $not_user = \App\Models\RoleAdmin::where('role_id', '!=', 2)->pluck('admin_id')->toArray();
    $channels = \App\Models\Admin::select('id', 'name', 'image', 'channel_name', 'follow_count', 'post_count')
        ->whereNotIn('id', $not_user)->where('post_count', '>', 0)
        ->orderBy('follow_count', 'desc')->orderBy('id', 'asc')->get();
    $channel_followed = \Modules\ThemeTiengAnh\Models\Follow::where('admin_id', @\Auth::guard('admin')->user()->id)->pluck('channel_id')->toArray();
    ?>
    <ul class="company-posts">
        @foreach($channels as $channel)
            <li>
                <figure>
                    <a href="/kenh/{{ @$channel->id }}">
                        <img
                                src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$channel->image ,45, null) }}"
                                alt="">
                    </a>
                </figure>
                <div class="position-meta">
                    <h6><a href="/kenh/{{ $channel->id }}" title="">{{$channel->channel_name != '' ? $channel->channel_name : $channel->name}}</a>
                    </h6>
                    <div class="likes heart {{ in_array($channel->id, $channel_followed) ? 'liked' : '' }} channel_follow_icon channel_follow_icon_{{ $channel->id }}"
                         data-id="{{ $channel->id }}"
                         title="Theo dõi/Bỏ theo dõi">❤
                    </div>
                    <span class="channel-intro">
                        {{ number_format($channel->post_count, 0, '.', '.') }} bài viết - <span class="channel_follow_count channel_follow_{{ $channel->id }}"
                                id="">{{ number_format($channel->follow_count, 0, '.', '.') }}</span> theo dõi
                    </span>
                </div>
            </li>
        @endforeach
    </ul>
</div>
<script>
    $('.likes.heart').click(function () {
        var channel_id = $(this).data('id');
        $.ajax({
            url: '/chanel/follow',
            type: 'GET',
            data: {
                channel_id: channel_id,
                admin_id: '{{ @\Auth::guard('admin')->user()->id }}'
            },
            success: function (result) {
                if (result.status) {
                    if (result.follow) {
                        $('.channel_follow_icon_' + channel_id).addClass('liked');
                    } else {
                        $('.channel_follow_icon_' + channel_id).removeClass('liked');
                    }
                    $('.channel_follow_' + channel_id).html(result.count);
                } else {
                    alert(result.msg)
                    // toastr.error(result.msg);
                }
            }
        });
    });
</script>