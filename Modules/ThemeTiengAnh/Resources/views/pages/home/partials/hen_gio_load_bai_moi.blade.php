<input name="ajax_rend_content" type="hidden" value="0">
@if(\Auth::guard('admin')->check())
    <script>
        setTimeout(function () {
            $.ajax({
                url: '/newsfeed/ajax-rend-content',
                type: 'GET',
                data: {
                    admin_id: '{{ @\Auth::guard('admin')->user()->id }}',
                },
                success: function (resp) {
                    console.log(resp.msg);
                    $('input[name=ajax_rend_content]').val(resp.count);
                },
                error: function () {
                    console.log('Lỗi ajax tự rend thêm bài mới');
                }
            });
        }, 4000);
    </script>
@endif