<style>
    .sidebar .tags li.active > a {
        color: rgb(24, 119, 242);
        border: 0.5px solid rgb(24, 119, 242);
    }

    .sidebar .tags li a {
        border: 0.5px solid #737373;
    }

    #hashtag-sidebar > ul {
        max-height: 400px;
        overflow-x: hidden;
        overflow-y: auto;
        padding:5px;
    }

    #hashtag-sidebar > ul > li {
    }

    #hashtag-sidebar > ul > li > a{
        vertical-align: middle;
        background-color: white;
        border-radius: 5px;
        font-weight: 400;
        text-transform: capitalize;
        margin: 5px;
        text-align: center;
        width: auto;
    }
</style>
<div class="widget pitnik-links stick-widget tags" id="hashtag-sidebar">
    <h4 class="widget-title">Chủ đề</h4>
    <?php
    $tags = \Modules\ThemeTiengAnh\Models\Tag::rightJoin('post_tags', 'post_tags.tag_id', 'tags.id')
            ->select('tags.name', 'tags.id')->groupBy('tags.id')->get();
    ?>
    <ul>
        @php
            $strTag = isset($_GET['idtag']) ?  $_GET['idtag'] : "";
            $arrTag = explode(",", $strTag);
        @endphp
        @foreach($tags as $tag)
{{--            <li class="{{ @$_GET['tag_id'] == $tag->id ? 'active' : '' }}"><a href="/tim-kiem?tag_id={{ $tag->id }}" title="{{ $tag->name }}">{{ $tag->name }}</a></li>--}}
            <li data-id="{{ $tag->id }}" onclick="changeTag(this)" class="@if(in_array($tag->id, $arrTag)) active @endif"><a href="javascript:void(0)" title="{{ $tag->name }}">{{ $tag->name }}</a></li>
        @endforeach

    </ul>
</div>
