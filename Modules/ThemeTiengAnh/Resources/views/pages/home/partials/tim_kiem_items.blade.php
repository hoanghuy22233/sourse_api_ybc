<?php

?>
@php
    $user = @\Auth::guard('admin')->user();

   if($user){
       $user = \Modules\ThemeTiengAnh\Models\Admin::find($user->id);
   }
@endphp
@foreach($posts as $post)
    @include('themetienganh::pages.home.partials.post_item', ['user' => $user, 'post' => $post ])
@endforeach

