@php
    $typeClass = isset($type) && $type =="ajax" ? "ajax-post-" . $post->id : "normal-post";
    $id = isset($post->post_id) ? $post->post_id : $post->id;

@endphp
<div class="{{ $typeClass }} central-meta item" data-post_id="{{ $id }}" data-newsfeed_id="{{ $post->newsfeed_id }}"
     style="display: inline-block;">
    <div class="user-post">
        <div class="friend-info">
            <figure>
                <a href="/kenh/{{ @$post->admin->id }}">
                    <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$post->admin->image, null, 40) }}"
                         alt="{{ @$post->admin->name }}">
                </a>
            </figure>
            <div class="friend-name">
                <ins><a href="/kenh/{{ @$post->admin->id }}"
                        title="">{{ @$post->admin->channel_name != null ? @$post->admin->channel_name : @$post->admin->name }}</a>
                </ins>
                <span><a href="/bai-viet/{{ @$post->id }}"> Ngày đăng: {{ date('h:i a d/m/Y', strtotime($post->created_at)) }}</a> </span>
            </div>
            <div class="post-meta">
                <strong>{{ $post->name }}</strong>

                <div class="description">
                    <div class="overlow-post">
                        <div class="img-content">
                            @if ($post->link_youtube != null)
                                <?php
                                if (strpos($post->link_youtube, '?v=') !== false) {
                                    $code = explode('?v=', $post->link_youtube)[1];
                                    $code = explode('&', $code)[0];
                                }

                                ?>
                                <iframe width="100%" height="315" src="https://www.youtube.com/embed/{{ @$code }}"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>

                            @endif
                            @if ($post->file_audio != null)
                                <audio controls>
                                    <source src="{{ asset('public/filemanager/userfiles/' . $post->file_audio) }}" type="audio/mpeg">
                                    Trình duyệt của bạn không hỗ trợ các yếu tố âm thanh.
                                </audio>
                            @endif
                        </div>
                        <div class="post-content_html">
                            @if($post->content != null)
                                {{--Nếu có content thì hiển thị ra--}}
                                <p style="height: unset !important; width: 100%">
                                    {!! $post->content !!}
                                </p>
                            @else
                                {{--nếu ko có content thì hiển thị câu hỏi, đọc thêm vào phần content--}}

                                @include('themetienganh::pages.home.partials.hoc_them_cau_hoi', ['class' => 'box-cau-hoi'])
                            @endif
                        </div>

    {{--                    <a href="javascript:;" class="view-more"--}}
    {{--                       style="color: #007bff;">Đọc thêm</a>--}}


                        @if($post->content != null && ( $post->content_extra != null || $post->question != null) )
                            @include('themetienganh::pages.home.partials.hoc_them_cau_hoi', ['class' => 'more box-cau-hoi'])
                        @endif
                    </div>
                    <a onclick="showMoreOrLess(this)" href="javascript:void(0);" class="view-less-custom show-more"
                       style="color: #007bff">Đọc thêm</a>
                </div>
                <div class="we-video-info">
                    <ul style="    width: 100%;">
{{--                        <li style="    width: 42px;">--}}
{{--																<span class="views" title="views">--}}
{{--																	<i class="fa fa-eye"></i>--}}
{{--																	<ins>{{ number_format($post->view_total, 0, '.', '.') }}</ins>--}}
{{--																</span>--}}
{{--                        </li>--}}


                        <li class="like-page">
                            @php
                                $classLike = "";
$post = \Modules\ThemeTiengAnh\Models\Post::find($id);
 $count_like = $post->likes()->where('like', 1)->count();
                                    if(@$user){
                                        $classLike = $user->likes()->where('post_id', $id)->where('like', 1)->first() &&  $count_like > 0? "liked" : "";
                                    }



                            @endphp
                            <a data-postId="{{ $id }}" class="btn_like_action {{ $classLike }}" onclick="likepage(this)" href="javascript:void(0)"><i class="fas fa-thumbs-up"></i><span class="like_label">Like</span><span class="count_like">{{ $count_like  }}</span></a>
                        </li>
                        <li>
                            <span class="comment @if(isset($type) && $type =="ajax") ajax @endif" title="Comments">
                                <i class="fa fa-commenting"></i> Bình luận</span>
                        </li>
                        <li style="    width: unset;">
{{--                            <div class="fb-share-button"--}}
{{--                                 data-href="{{ url('/bai-viet/'.$id) }}"--}}
{{--                                 data-width="" data-layout="button_count"--}}
{{--                                 data-action="like" data-size="small"--}}
{{--                                 data-share="true"></div>--}}
                            <a class="btn-fb-share" onclick="openFbPopUp('{{ url('/bai-viet/'.$id) }}','','','')" value="SHARE">
                                <i class="fab fa-facebook-f"></i><span>Chia sẻ</span>
                            </a>
                        </li>
                    </ul>
                    <style>
                        .we-video-info ul {
                            display:flex;
                            justify-content: space-around;
                            align-items: center;
                            margin-top:0;
                            margin-bottom:10px;
                        }

                        .we-video-info li .btn_like_action  {
                            line-height: 18px;
                            border-radius: 4px;
                            padding-top: 15px;
                            padding-bottom: 15px;
                            padding-left: 60px;
                            padding-right: 56px;
                            cursor: pointer;
                        }

                        .we-video-info li .btn_like_action:hover {
                            background-color: #ced0ce;
                        }

                        .we-video-info li .comment {
                            line-height: 18px;
                            border-radius: 4px;
                            padding-top: 15px;
                            padding-bottom: 15px;
                            padding-left: 60px;
                            padding-right: 56px;
                            cursor: pointer;
                        }

                        .we-video-info li .comment:hover {
                            background-color: #ced0ce;
                        }

                        .we-video-info li .fb-share-button {
                            line-height: 18px;
                            border-radius: 4px;
                            padding-top: 15px;
                            padding-bottom: 15px;
                            padding-left: 60px;
                            padding-right: 56px;
                            cursor: pointer;
                        }

                        .we-video-info li .fb-share-button:hover {
                            background-color: #ced0ce;
                        }
                    </style>
                </div>
            </div>
            <div class="coment-area" style="display: block; margin-top:0;">
                @php
                    $comments = $post->comments()->where('parent_id', 0)->orderBy('id','desc')->paginate(1);
                @endphp
                @if($comments->total() > 1)
                    <a onclick="showMoreComment(this,{{ $id }}, {{ $comments[0]->id }})" class="view_more_cmt" href="javascript:void(0)" style="color:dodgerblue;">Xem thêm bình luận</a>
                @endif
                <div id="list-comment" class="list-comment">
                    <div class="content_list">
                        <div id="area_cmt_more_{{ $post->id }}"></div>
                        @include('themetienganh::pages.home.partials.comment', [ 'comments' =>  $comments, 'post' => $post, 'user' => $user ])
                    </div>
                    @if($user)
                        <div class="box-input-cmt">
                            <div class="box-avatar">
                                <a href="#"><img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($user->image ,45, null) }}" alt="" style="border-radius: 100%;
height: 32px;
width: 32px;"></a>
                            </div>
                            <div class="box-cmt">
                                <textarea style="resize:none;" placeholder="Viết bình luận..." class="text_post_{{ $id }}" rows="1"></textarea>
                                <input type="hidden" name="post_id" value="{{ $id }}">
                                <input type="hidden" name="parent_id" value="0">
                            </div>
                        </div>
                    @endif
                    <style>

                         .time_ago {
                             margin-left:7px;
                             color:#BEC3C9;
                             font-size:11px;
                         }

                         .list-action > a {
                             font-weight: bold;
                             font-size:13px;
                         }
                    </style>
                </div>
            </div>
        </div>
    </div>
</div>

