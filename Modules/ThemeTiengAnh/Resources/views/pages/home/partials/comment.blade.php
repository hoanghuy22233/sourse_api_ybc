@php
    $id = isset($post->post_id) ? $post->post_id : $post->id;
@endphp
    @foreach($comments as $k => $comment)

        <div class="item-comment">
            <div class="box-avatar">
                <a href="#"><img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($comment->admin->image ,45, null) }}" alt="" style="border-radius:100%;height: 32px;
width: 32px;"></a>
            </div>
            <div class="box_comment">
                <div class="content_comment">
                    <a class="name_author" href="#">{{ $comment->admin->name }}</a>
                    <p class="content_comment_body">{!! $comment->content !!}</p>
                </div>
                <div class="list-aciton">
                    @if($comment->parent_id == 0)
                    <a href="javascript:toogleReply({{ $comment->id }})">Trả lời</a>
                    @else
                        <a href="javascript:toogleReply({{ $comment->parent_id }})">Trả lời</a>
                    @endif
                    <span class="time_ago">{{ \App\Http\Helpers\CommonHelper::formatTimePastCustom($comment->created_at) }}</span>
                </div>
                @php
                    $subcomments = $comment->childrens()->orderBy('id','ASC')->paginate(1);
                @endphp
                <div class="list-comment">
                    <div class="content_list">
                        @foreach($subcomments as $k => $subcomment)
                            <div class="item-comment">
                                <div class="box-avatar">
                                    <a href="#"><img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($comment->admin->image ,45, null) }}" alt="" style="border-radius:100%;height: 32px;
width: 32px;"></a>
                                </div>
                                <div class="box_comment">
                                    <div class="content_comment">
                                        <a class="name_author" href="#">{{ $subcomment->admin->name }}</a>
                                        <p class="content_comment_body">{!!  $subcomment->content !!}}</p>
                                    </div>
                                    <div class="list-aciton">

                                            <a href="javascript:toogleReply({{ $comment->id }})">Trả lời</a>

                                        <span class="time_ago">{{ \App\Http\Helpers\CommonHelper::formatTimePastCustom($subcomment->created_at) }}</span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div id="rep_{{ $comment->id }}" class="box-input-cmt" style="display: none">
                        <div class="box-avatar">
                            <a href="#"><img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($user->image ,45, null) }}" alt="" style="border-radius:100%;height: 32px;
width: 32px;"></a>
                        </div>
                        <div class="box-cmt">
                            <textarea style="resize:none;" class="text_{{ $comment->id }}" rows="1" placeholder="Viết bình luận..."></textarea>
                            <input type="hidden" name="post_id" value="{{ $id }}">
                            <input type="hidden" name="parent_id" value="{{ $comment->id }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>


    @endforeach

