<div class="{{ @$class }}">
    @if($post->content_extra != null || $post->question != null)
        <div class="gen-metabox cau-hoi" data-q-id="{{ $post->id }}"
             id="question-{{ $post->id }}">

            <div class="myTab-box">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    @if($post->question != null)
                        <li class="nav-item {{ $post->content_extra == null ? 'full-width' : '' }} active" role="presentation">
                            <a class="nav-link" id="home-tab" data-bs-toggle="tab"
                               data-tab=".cau-hoi-box"
                               role="tab" aria-controls="home" aria-selected="true">Câu hỏi</a>
                        </li>
                    @endif

                    @if($post->content_extra != null)
                        <li class="nav-item {{ $post->question == null ? 'full-width' : '' }} {{ $post->question == null ? 'active' : '' }}"
                            role="presentation">
                            <a class="nav-link" id="profile-tab" data-bs-toggle="tab"
                               data-tab=".hoc-them" role="tab" aria-controls="profile"
                               aria-selected="false">Học thêm</a>
                        </li>
                    @endif
                </ul>
                <div class="tab-content" id="myTabContent">
                    @if($post->question != null)
                        <div class="tab-pane fade cau-hoi-box show active" role="tabpanel"
                             aria-labelledby="home-tab">
                            <p style="font-weight: bold;">{!! $post->question !!}</p>
                            <div class="col-md-12 select-radio">
                                <div class="form-radio">
                                    @if($post->answer_a != null)
                                        <div class="col-xs-12 radio"
                                             style="width: 100%;">
                                            <label>
                                                <input type="radio"
                                                       name="dapan[{{ $post->id }}]"
                                                       value="a"><i
                                                        class="check-box "></i><span
                                                        style="color: blue;">A:</span>
                                                {!! $post->answer_a !!}
                                            </label>
                                        </div>
                                    @endif

                                    @if($post->answer_b != null)
                                        <div class="col-xs-12 radio"
                                             style="width: 100%;">
                                            <label>
                                                <input type="radio"
                                                       name="dapan[{{ $post->id }}]"
                                                       value="b"><i
                                                        class="check-box "></i><span
                                                        style="color: blue;">B:</span>
                                                {!! $post->answer_b !!}
                                            </label>
                                        </div>
                                    @endif

                                    @if($post->answer_c != null)
                                        <div class="col-xs-12 radio"
                                             style="width: 100%;">
                                            <label>
                                                <input type="radio"
                                                       name="dapan[{{ $post->id }}]"
                                                       value="c"><i
                                                        class="check-box "></i><span
                                                        style="color: blue;">C:</span>
                                                {!! $post->answer_c !!}
                                            </label>
                                        </div>
                                    @endif

                                    @if($post->answer_d != null)
                                        <div class="col-xs-12 radio"
                                             style="width: 100%;">
                                            <label>
                                                <input type="radio"
                                                       name="dapan[{{ $post->id }}]"
                                                       value="d"><i
                                                        class="check-box "></i><span
                                                        style="color: blue;">D:</span>
                                                {!! $post->answer_d !!}
                                            </label>
                                        </div>
                                    @endif
                                </div>
                                <button onclick="autoComment(this)" class="form-control xac-nhan">Xác nhận
                                </button>
                            </div>
                            <div class="dap_an">
                                <strong>Đáp án chính
                                    xác: {{ strtoupper($post->answer_exactly) }}</strong><br>
                                @if($post->detailed_answer != null)
                                    Giải thích:
                                    {!! $post->detailed_answer !!}
                                @endif
                            </div>
                        </div>
                    @endif

                    @if($post->content_extra != null)
                        <div class="tab-pane fade hoc-them {{ $post->question == null ? 'show active' : '' }}"
                             id="" role="tabpanel"
                             aria-labelledby="profile-tab">
                            {!! $post->content_extra !!}
                        </div>
                    @endif
                </div>
            </div>

        </div>

    @endif
</div>