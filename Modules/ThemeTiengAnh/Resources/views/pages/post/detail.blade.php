@extends('themetienganh::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        @include('themetienganh::template.top_bar')
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">

                                {{--Hiển thị khối widgets--}}
                                <?php
                                $widgets = CommonHelper::getFromCache('widgets_home_sidebar_center', ['widgets']);
                                if (!$widgets) {
                                    $widgets = \Modules\ThemeEdu\Models\Widget::where('location', 'home_content')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
                                    CommonHelper::putToCache('widgets_home_sidebar_center', $widgets, ['widgets']);
                                }
                                ?>
                                @foreach($widgets as $widget)
                                    {{--{{ dd(json_decode($widget->config)->view) }}--}}
                                    <div class="col-lg-12">
                                        @if($widget->type == 'html')
                                            <div class="gap">
                                                <div class="container">
                                                    <div class="row">
                                                        {!!$widget->content !!}
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            @include(@json_decode($widget->config)->view, ['config' => (array) json_decode($widget->config)])
                                        @endif
                                    </div>
                                @endforeach
                                {{--END: Hiển thị khối widget--}}


                                <div class="col-lg-3 cot-1">
                                    <aside class="sidebar static left ">
                                    </aside>
                                </div>

                                <div class="col-lg-6 cot-2">

                                    <div class="post-content">
                                        <?php $posts = [$post];?>
                                        @foreach($posts as $post)
                                            @include('themetienganh::pages.home.partials.post_item')
                                        @endforeach

                                        {{--</div>--}}
                                    </div>
                                </div>

                                <div class="col-lg-3 cot-3">
                                    <aside class="sidebar static left">
                                        @include('themetienganh::pages.home.partials.kenh')
                                    </aside>
                                </div>

                                @include('themetienganh::partials.form_contact')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection
@section('head_script')
    @include('themetienganh::pages.home.partials.newsfeed_css')
@endsection
@section('footer_script')
    @include('themetienganh::pages.home.partials.newsfeed_js')

@endsection