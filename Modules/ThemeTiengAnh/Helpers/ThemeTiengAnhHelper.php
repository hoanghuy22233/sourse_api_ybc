<?php

namespace Modules\ThemeTiengAnh\Helpers;

use Modules\ThemeTiengAnh\Models\Follow;
use Modules\ThemeTiengAnh\Models\Menu;
use Modules\ThemeTiengAnh\Models\Newsfeed;
use Modules\ThemeTiengAnh\Models\Post;
use View;
use Session;
use App\Http\Helpers\CommonHelper as CoreHelper;

class ThemeTiengAnhHelper
{

    public static function getPostNewsFeed($admin_id, $skip = 0, $limit = 5) {
        if ($admin_id == null) {
            //  Nếu chưa đăng nhập thì lấy bừa
            $data = Post::orderBy('id', 'desc')->skip($skip)->limit($limit)->get();
            return $data;
        }

        //  Nếu newsfeed hết bài thì tạo thêm
        /*$count_new = Newsfeed::where('admin_id', $admin_id)->where('viewed', 0)->count();
        if ($count_new < 5) {
            ThemeTiengAnhHelper::rendNewsFeed($admin_id, 5 - $count_new);
        }*/

        $data = Post::join('newsfeed', 'newsfeed.post_id', 'posts.id')
            ->select('posts.id', 'posts.admin_id', 'posts.created_at', 'posts.name',
                'posts.link_youtube', 'posts.content', 'posts.content_extra', 'posts.question',
                'posts.view_total', 'posts.answer_a', 'posts.answer_b', 'posts.answer_c', 'posts.answer_d'
                , 'posts.answer_exactly', 'posts.detailed_answer', 'newsfeed.id as newsfeed_id', 'posts.file_audio')
            ->where('newsfeed.admin_id', $admin_id)->where('posts.status', 1)
            ->orderBy('newsfeed.viewed', 'asc')->orderBy('newsfeed.level', 'desc')->orderBy('newsfeed.created_at', 'desc')
            ->skip($skip)->limit($limit)->get();

        //  Cập nhât đã xem cho các newsfeed
        $newsfeed_ids = [];
        foreach ($data as $v) {
            $newsfeed_ids[] = $v->newsfeed_id;
        }
        Newsfeed::whereIn('id', $newsfeed_ids)->where('viewed', 0)->update([
            'viewed' => 1
        ]);

        return $data;
    }

    //  Lấy thêm bài viết cho chân news feed
    public static function getPostNewsFeedOld($admin_id, $skip = 0, $limit = 10) {
        if ($admin_id == null) {
            //  Nếu chưa đăng nhập thì lấy bừa
            $data = Post::where('status', 1)->orderBy('id', 'desc')->skip($skip)->limit($limit)->get();
            return $data;
        }

        //  Đếm số bài trên newsfeed mà ko đủ số bài skip thì tạo thêm
        $count_new = Newsfeed::where('admin_id', $admin_id)->count();
        if ($count_new <= $skip) {
            ThemeTiengAnhHelper::rendNewsFeedFooter($admin_id, 10);
        }

        $data = Post::join('newsfeed', 'newsfeed.post_id', 'posts.id')
            ->select('posts.id', 'posts.admin_id', 'posts.created_at', 'posts.name',
                'posts.link_youtube', 'posts.content', 'posts.content_extra', 'posts.question',
                'posts.view_total', 'posts.answer_a', 'posts.answer_b', 'posts.answer_c', 'posts.answer_d'
                , 'posts.answer_exactly', 'posts.detailed_answer', 'newsfeed.id as newsfeed_id', 'posts.file_audio')
            ->where('newsfeed.admin_id', $admin_id)->where('posts.status', 1)
            ->orderBy('newsfeed.viewed', 'asc')->orderBy('newsfeed.level', 'desc')->orderBy('newsfeed.created_at', 'desc')
            ->skip($skip)->limit($limit)->get();

        return $data;
    }

    //  Load thêm bài vào trên đầu newsfeed
    public static function rendNewsFeed($admin_id, $count = 10) {
        $post_ids = ThemeTiengAnhHelper::getPostIdsForNewFeed($admin_id, $count);

        $newsfeed_rend = [];
        foreach ($post_ids as $post) {
            $newsfeed = new Newsfeed();
            $newsfeed->post_id = $post->id;
            $newsfeed->admin_id = $admin_id;
            $newsfeed->level = 1;
            $newsfeed->save();
            $newsfeed_rend[] = $newsfeed;

            //  Tăng lượt xem cho bài viết
            $post->view_total ++;
            $post->save();
        }
        return $newsfeed_rend;
    }

    //  Tạo thêm bài vào chân newsfeed
    public static function rendNewsFeedFooter($admin_id, $count = 10) {
        //  Lấy thời gian tạo bài cũ nhất trên news feed
        $last_created_at = @Newsfeed::select('created_at')->where('admin_id', $admin_id)->orderBy('level', 'asc')->orderBy('created_at', 'asc')->first()->created_at;

        $post_ids = ThemeTiengAnhHelper::getPostIdsForNewFeed($admin_id, $count);

        $newsfeed_rend = [];
        foreach ($post_ids as $post) {
            $newsfeed = new Newsfeed();
            $newsfeed->post_id = $post->id;
            $newsfeed->admin_id = $admin_id;
            $newsfeed->level = 1;
            $newsfeed->viewed = 1;
            $newsfeed->created_at = date('Y-m-d H:i:s', strtotime($last_created_at) - 100);
            $newsfeed->save();
            $newsfeed_rend[] = $newsfeed;

            //  Tăng view cho bài viết
            $post->view_total ++;
            $post->save();
        }
        return $newsfeed_rend;
    }

    public static function getPostIdsForNewFeed($admin_id, $count) {
        //  Lấy các bài viết chưa xem
        $post_ids = Post::whereRaw('not exists (
                select 1 from newsfeed where posts.id = newsfeed.post_id and newsfeed.admin_id = '.$admin_id.'
            )
            ')->where('status', 1);

        //  Lấy id các kênh đang theo dõi & ưu tiên các bài của cá kênh đó
        $channel_ids = Follow::where('admin_id', $admin_id)->pluck('channel_id');
        $orderRaw = 'CASE ';
        foreach ($channel_ids as $k => $v) {
            if ($v != '') {
                $orderRaw .= " WHEN posts.admin_id = " . $v . " THEN " . $k ;
            }
        }
        $orderRaw .= ' ELSE posts.id END ASC';
        if ($orderRaw != 'CASE  ELSE posts.id END ASC') {
            $post_ids = $post_ids->orderByRaw($orderRaw);
        }

        $post_ids = $post_ids->orderBy('id', 'desc')->limit($count)->get();

        //  Đảo ngược mảng để bài mới lên đầu
        $post_ids = $post_ids->reverse();
        return $post_ids;
    }

    public static function getMenusByLocation($location, $limit = 10, $get_childs = true)
    {
        $data = CoreHelper::getFromCache('menus_by_location_' . $location, ['menus']);
        if (!$data) {
            $menus = Menu::where('location', $location)->where(function ($query) {
                $query->where('parent_id', null)->orwhere('parent_id', 0);
            })->where('status', 1)->where('type', 'url')->where('location', $location)->orderBy('order_no', 'desc')->limit($limit)->get();

            foreach ($menus as $menu) {
                //  Lấy menu cấp 1
                $item = [
                    'name' => $menu->name,
                    'url' => $menu->url,
                    'childs' => []
                ];

                //  Lấy menu cấp 2
                $menu2Childs = $menu->childs;
                foreach ($menu2Childs as $child2) {
                    $child2_data = [
                        'name' => $child2->name,
                        'url' => $child2->url,
                        'childs' => []
                    ];

                    //  Lấy menu cấp 3
                    $menu3Childs = $child2->childs;
                    foreach ($menu3Childs as $menu3) {
                        $child3_data = [
                            'name' => $menu3->name,
                            'url' => $menu3->url,
                            'childs' => []
                        ];

                        //  Gán menu cấp 3 vào mảng con của cấp 2
                        $child2_data['childs'][] = $child3_data;
                    }

                    //  Gán menu cấp 2 vào mảng con của cấp 1
                    $item['childs'][] = $child2_data;
                }
                $data[] = $item;
            }

            CoreHelper::putToCache('menus_by_location_' . $location, $data, ['menus']);

            if (!$data) {
                return [];
            }
        }

        return $data;
    }
}