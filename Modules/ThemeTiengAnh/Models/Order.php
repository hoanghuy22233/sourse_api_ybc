<?php

namespace Modules\ThemeTiengAnh\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';

    protected $guarded = [];

    public function course() {
        return $this->belongsTo(\Modules\ThemeTiengAnh\Models\Course::class, 'course_id', 'id');
    }

}
