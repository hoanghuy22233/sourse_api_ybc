<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace Modules\ThemeTiengAnh\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends Model {

    protected $table = 'likes';

    //use SoftDeletes;


    protected $fillable = [
        'admin_id','post_id'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class,'admin_id');
    }
    public function post(){
        return $this->belongsTo(Post::class,'post_id');
    }

}
