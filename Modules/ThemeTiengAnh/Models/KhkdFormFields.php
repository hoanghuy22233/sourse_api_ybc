<?php


namespace Modules\ThemeTiengAnh\Models ;

use Illuminate\Database\Eloquent\Model;

class KhkdFormFields extends Model
{
    protected $table = 'khkd_form_fields';

    protected $fillable = ['name', 'content', 'location'];

    public function childs()
    {
        return $this->hasMany($this, 'parent_id', 'id')->orderBy('order_no', 'asc');
    }
}