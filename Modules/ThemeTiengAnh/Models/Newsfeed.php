<?php

/**
 * Banners Model
 *
 * Banners Model manages Banners operation. 
 *
 * @category   Banners
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license    
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\ThemeTiengAnh\Models ;

use Illuminate\Database\Eloquent\Model;

class Newsfeed extends Model
{
	protected $table = 'newsfeed';
}
