<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace Modules\ThemeTiengAnh\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model {

    protected $table = 'comments';


    protected $fillable = [
        'admin_id','post_id', 'parent_id', 'content'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class,'admin_id');
    }
    public function post(){
        return $this->belongsTo(Post::class,'post_id');
    }
    public function childrens(){
        return $this->hasMany(Comment::class,'parent_id');
    }

}
