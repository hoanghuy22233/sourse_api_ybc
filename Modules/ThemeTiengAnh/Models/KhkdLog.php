<?php


namespace Modules\ThemeTiengAnh\Models ;

use Illuminate\Database\Eloquent\Model;

class KhkdLog extends Model
{
    protected $table = 'khkd_log';

    public function course() {
        return $this->belongsTo(\Modules\ThemeTiengAnh\Models\Course::class, 'course_id', 'id');
    }
}