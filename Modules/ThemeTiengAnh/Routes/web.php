<?php
/*Route::get('test', function () {
    $admins = \App\Models\Admin::all();
    foreach ($admins as $admin) {
        $count = \Modules\ThemeTiengAnh\Models\Post::where('admin_id', $admin->id)->count();
        $admin->count_post = $count;
        $admin->save();
    }
    dd('ok');
});*/
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions', 'locale']], function () {
    Route::group(['prefix' => 'theme'], function () {
        Route::match(['get', 'post'], 'setting', 'Admin\EduThemeController@setting')->name('themetienganh')->middleware('permission:theme');
    });

    //  Admin
    Route::group(['prefix' => 'profile'], function () {
        Route::match(array('GET', 'POST'), '', 'Admin\AdminController@profile')->name('admin.profile');
    });
});

Route::get('admin/login', function () {
    return redirect('/dang-nhap');
});

Route::group(['prefix' => '', 'middleware' => 'no_auth:admin'], function () {
    Route::get('dang-nhap', 'Frontend\AuthController@getLogin');
    Route::post('dang-nhap', 'Frontend\AuthController@authenticate');
    Route::get('dang-ky', 'Frontend\AuthController@getRegister');
    Route::post('dang-ky', 'Frontend\AuthController@postRegister');
});

Route::match(array('GET', 'POST'), 'quen-mat-khau', 'Frontend\AuthController@getEmailForgotPassword');
Route::match(array('GET', 'POST'), 'forgot-password/{change_password}', 'Frontend\AuthController@getForgotPassword');
Route::get('dang-xuat', function () {
    \Auth::guard('admin')->logout();
    return redirect('/');
});

//Route::match(array('GET', 'POST'), 'profile/{id}', 'Frontend\AuthController@profileAdmin')->name('profile');
Route::get('user/like', 'Frontend\StudentController@likePage')->name('user.like');
Route::post('user/comment', 'Frontend\StudentController@comment')->name('user.comment');
Route::post('user/comment-more', 'Frontend\StudentController@ajaxMoreComment')->name('user.commentMore');
Route::group(['prefix' => '', 'middleware' => ['guest:student']], function () {
    Route::get('profile', 'Frontend\StudentController@myProfile');
    Route::get('profile/edit', 'Frontend\StudentController@getEditProfile');
    Route::post('profile/edit', 'Frontend\StudentController@postEditProfile');

    Route::post('tu-tao-mau-kinh-doanh', 'Frontend\CourseController@tuTaoMauKinhDoanh');

    Route::group(['prefix' => 'student'], function () {
        Route::get('doi-mat-khau', 'Frontend\StudentController@getChangePass');
        Route::post('doi-mat-khau', 'Frontend\StudentController@postChangePass');
    });
});

Route::get('profile/{id}', 'Frontend\StudentController@getProfile');

Route::get('video', 'Frontend\HomeController@video');
Route::get('kenh', 'Frontend\HomeController@kenh');
Route::match(['GET', 'POST'], 'tao-kenh', 'Frontend\HomeController@taoKenh')->middleware('guest:admin');
Route::get('kenh/{id}', 'Frontend\HomeController@chiTietKenh');
Route::get('tim-kiem', 'Frontend\HomeController@timKiem');
Route::get('newsfeed/load-more', 'Frontend\HomeController@newsFeedLoadMore');
Route::get('newsfeed/ajax-rend-content', 'Frontend\HomeController@ajaxRendContent');
Route::get('tim-kiem/load-more', 'Frontend\HomeController@timKiemLoadMore');
Route::get('chanel/follow', 'Frontend\HomeController@follow');
Route::get('bai-viet/{slug}', 'Frontend\PostController@detail');

//  Dang nhap fb - gg
Route::get('/login/{param}/redirect/', 'Frontend\StudentController@redirect')->name('auth_redirect');//dang nhap
Route::get('/login/{param}/callback/', 'Frontend\StudentController@callback');

Route::get('{slug1}', 'Frontend\CourseController@oneParam');
Route::get('{slug1}/{slug2}', 'Frontend\CourseController@twoParam');
Route::get('{slug1}/{slug2}/{slug3}', 'Frontend\CourseController@threeParam');
Route::get('{slug1}/{slug2}/{slug3}/{slug4}', 'Frontend\CourseController@fourParam');

Route::get('', 'Frontend\HomeController@getHome');
