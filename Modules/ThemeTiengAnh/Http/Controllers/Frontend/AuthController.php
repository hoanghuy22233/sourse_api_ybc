<?php

namespace Modules\ThemeTiengAnh\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Admin;
use App\Models\RoleAdmin;
use App\Models\Setting;
use Auth;
use Illuminate\Http\Request;
use Mail;
use Modules\ThemeTiengAnh\Http\Requests\CreateRequest;
use Modules\ThemeTiengAnh\Http\Requests\ForgotPasswordRequest;
use Modules\ThemeTiengAnh\Http\Requests\LoginRequest;
use Modules\ThemeTiengAnh\Models\Student;
use Session;
use Validator;


class AuthController extends Controller
{
//    function getLogin()
//    {
//        return view('themetienganh::pages.auth.login');
//    }
//
//    function getRegister()
//    {
//        return view('themetienganh::pages.auth.register');
//    }
//
//    public function getForgotPassword() {
//        return view('themetienganh::pages.auth.forgot_password');
//    }
    public function getLogin()
    {
        $data['page_title'] = 'Đăng nhập';
        $data['page_type'] = 'list';

        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|tel|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|tel)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            return view('themetienganh::mobi.pages.auth.login', $data);
        }
        return view('themetienganh::pages.auth.login');
    }

    public function authenticate(LoginRequest $request)
    {
        $admin = Admin::where('email', $request['email'])->orWhere('tel', $request['email'])->first();
        if (!is_object($admin)) {
            CommonHelper::one_time_message('danger', 'Sai email hoặc số điện thoại');
            return redirect('/dang-nhap');
        }

        if (@$admin->status == 0) {
            CommonHelper::one_time_message('danger', 'Tài khoản của bạn chưa được kích hoạt!');
            return redirect('/dang-nhap');
        }

        if (@$admin->status == -1) {
            CommonHelper::one_time_message('danger', 'Tài khoản của bạn đã bị khóa!');
            return redirect('/dang-nhap');
        }


        if (\Auth::guard('admin')->attempt(['email' => trim($request['email']), 'password' => trim($request['password'])], true)) {
            $this->setFilemanager();
            return redirect('/');
        } elseif (\Auth::guard('admin')->attempt(['tel' => trim($request['email']), 'password' => trim($request['password'])], true)) {
            $this->setFilemanager();
            return redirect('/');
        } else {
            CommonHelper::one_time_message('danger', 'Sai mật khẩu');
            return redirect('/dang-nhap');
        }
    }

    public function setFilemanager() {
        session_start();
        $role_name = CommonHelper::getRoleName(\Auth::guard('admin')->user()->id, 'name');
        if (in_array($role_name, ['nguoi-hoc', 'kenh'])) {
            //  nếu là người đọc hoặc kênh thì tạo folder quản lý file cho họ và gán cho folder cho họ
            $path = '/admin/' . \Auth::guard('admin')->user()->id;
            $base_path = public_path() . '/filemanager/userfiles';
            $dir_name = $base_path . $path;
            if (!is_dir($dir_name)) {
                // Tạo thư mục của chúng tôi nếu nó không tồn tại
                mkdir($dir_name, 0755, true);
            }

            $_SESSION['filemanager'] = $path;
        } elseif(isset($_SESSION['filemanager'])) {
            unset($_SESSION['filemanager']);
        }

        return true;
    }

    public function getRegister()
    {
        $data['page_title'] = 'Đăng ký';
        $data['page_type'] = 'list';
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|tel|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|tel)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            return view('themetienganh::mobi.pages.auth.register', $data);
        }
        return view('themetienganh::pages.auth.register', $data);
    }

    public function postRegister(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'name' => 'required',
            'email' => 'required|unique:admin,email',
            'password' => 'required|min:6',
            'tel' => 'required|unique:admin,tel',
            're_password' => 'required|same:password|min:6'
        ], [
            'name' => 'Họ tên',
            'email' => 'Email',
            'password' => 'Mật khẩu',
            'tel.unique' => 'Số điện thoại đã được sử dụng',
            'tel.required' => 'Số điện thoại này đã sử dụng',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            $data = $r->except('_token');
            $data['api_token'] = base64_encode(rand(1, 100) . time());
            $data['password'] = bcrypt($data['password']);
            $admin = Admin::create($data);

            $settings = @Setting::select(['name', 'value'])->where('type', 'role_tab')->pluck('value', 'name')->toArray();
            $this->setDefaultRoleToAdmin($admin, $settings['role_default_id']);

            CommonHelper::one_time_message('success', 'Bạn đã đăng ký tài khoản thành công! Vui lòng đăng nhập!');

            \Auth::guard('admin')->login($admin);
            return redirect('/');
        }
    }

    public function setDefaultRoleToAdmin($admin, $role_id = false)
    {
        if (!$role_id) {
            $settings = @Setting::select(['name', 'value'])->where('type', 'role_tab')->pluck('value', 'name')->toArray();
            $role_id = $settings['role_default_id'];
        }

        //  Set quyền mặc định khi mới đăng ký tài khoản
        if (isset($role_id)) {
            RoleAdmin::insert([
                'admin_id' => $admin->id,
                'role_id' => $role_id,
            ]);
        }
        return true;
    }

    public function getForgotPassword(Request $request, $change_password)
    {
        if (!$_POST) {

            $query = Admin::where('change_password', $change_password);

            if (!$query->exists() || !isset($change_password)) {
                abort(404);
            }
            $data['page_title'] = 'Lấy lại mật khẩu';
            $data['page_type'] = 'list';
            return view('themetienganh::pages.auth.change_password')->with($data);
        } else {

            if ($request->password == $request->re_password) {
                $admin = Admin::where('change_password', $change_password)->first();
                $admin->password = bcrypt($request->password);
                $admin->change_password = $admin->id . '_' . time();
                $admin->save();
                CommonHelper::one_time_message('success', 'Đổi mật khẩu thành công! vui lòng đăng nhập!');
                return redirect('/dang-nhap');
            } else {
                return back()->with('alert_re_password', 'Nhập lại mật khâu không khớp!');
            }
        }
    }


    public function getEmailForgotPassword(Request $request)
    {
        if (!$_POST) {
            $data['page_title'] = 'Quên mật khẩu';

            return view('themetienganh::pages.auth.forgot_password')->with($data);

        } else {
            $query = Admin::where('email', $request->email);

            if (!$query->exists()) {
                return back()->with('success', 'Email chưa được đăng ký');
            }
            $admin = $query->first();
            $admin->change_password = $admin->id . '_' . time();
            $admin->save();
            try {
                \Eventy::action('admin.restorePassword', [
                    'link' => \URL::to('forgot-password/' . @$admin->change_password),
                    'name' => @$admin->name,
                    'email' => $admin->email
                ]);
                //CommonHelper::one_time_message('success', 'Email sẽ được gửi trong ít phút. Bạn vui lòng kiểm tra mail để lấy lại mật khẩu!');
                return back()->with('success', 'Email sẽ được gửi trong ít phút. Bạn vui lòng kiểm tra mail để lấy lại mật khẩu!');
            } catch (\Exception $ex) {
                CommonHelper::one_time_message('error', 'Xin vui lòng thử lại!');
            }
        }
    }


}




