<?php

namespace Modules\ThemeTiengAnh\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Admin;
use App\Models\RoleAdmin;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Modules\EduCourse\Models\Misson;
use Modules\ThemeTiengAnh\Models\Comment;
use Modules\ThemeTiengAnh\Models\Like;
use Modules\ThemeTiengAnh\Models\Post;
use Modules\ThemeTiengAnh\Models\QuizLog;
use Modules\ThemeTiengAnh\Models\Setting;
use Modules\ThemeTiengAnh\Models\Student;
use Socialite;
use Validator;

class StudentController extends Controller
{
    public function ajaxMoreComment(Request $request){
        $post = Post::find($request->post_id);
        $comments = $post->comments()->where('parent_id', 0)->where('id', '!=', $request->comment_id)->orderBy('id','desc')->get();
        $user = \Auth::guard('admin')->user();
        $user = \Modules\ThemeTiengAnh\Models\Admin::find($user->id);
        $view =  view('themetienganh::pages.home.partials.comment')->with('comments', $comments)
                                                                ->with('post', $post)
                                                            ->with('user',$user)->render();
        $arridcmt = [];
        foreach($comments as $k => $cmt){
            $arridcmt[] = $cmt->id;
        }
        return [
            'html' => $view,
            'array_id_comment' => $arridcmt
        ];
    }
    public function comment(Request $request){
        if(!\Auth::guard('admin')->check()){
            return response('Unauthorized action.', 403);
        }
        $text = $request->text;
        $post_id = $request->post_id;
        $text = strip_tags($text);
        $text = preg_replace('/\n+/', '<br>', $text);

        $user = \Auth::guard('admin')->user();
        $user = \Modules\ThemeTiengAnh\Models\Admin::find($user->id);

       // $comment = $user->comments()->where('post_id', $post_id)->first();
        $comment = new Comment();
        $comment->admin_id = $user->id;
        $comment->parent_id = $request->parent_id;
        $comment->post_id = $post_id;
        $comment->content = $text;
        $comment->save();

        $post = Post::find($post_id);
        $comments = $post->comments()->where('id', $comment->id)->orderBy('id','desc')->limit(1)->get();
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            $view = view('themetienganh::mobi.pages.home.partials.comment')->with('comments', $comments)
                ->with('post', $post)->with('user', $user)->render();
        }else{
            $view = view('themetienganh::pages.home.partials.comment')->with('comments', $comments)
                ->with('post', $post)->with('user', $user)->render();
        }

        return [
          'html' => $view,
          'comment_id' => $comment->id
        ];
    }
    public function likePage(Request $request){
        // kiểm tra login
        $user = \Auth::guard('admin')->user();
        $user = \Modules\ThemeTiengAnh\Models\Admin::find($user->id);
        $post_id = $request->postid;
        $post = Post::find($post_id);
        if(!$post_id){
            return null;
        }

        $like = $user->likes()->where('post_id', $post_id)->first();
        if($like){
            $likeUpdate = $like->like == 1 ? 0 : 1;
            $like->like = $likeUpdate;
            $like->save();
            $count = $post->likes()->where('like', 1)->count();
            if($likeUpdate == 0){
                return [
                    'status' => 'success','count' =>  $count, 'class' => 'dislike'
                ];
            }else{
                return [
                    'status' => 'success','count' =>  $count, 'class' => 'liked'
                ];
            }

        }else{
            $like = new Like();
            $like->admin_id = $user->id;
            $like->post_id = $post_id;
            $like->like = 1;
            $like->save();
            $count = $post->likes()->where('like', 1)->count();
            return [
                'status' => 'success','count' =>  $count, 'class' => 'liked'
            ];
        }


    }

    public static function saveFile($file, $path)
    {
        $base_path = public_path() . '/filemanager/userfiles/';
        $dir_name = $base_path . $path;
        if (!is_dir($dir_name)) {
            // Tạo thư mục của chúng tôi nếu nó không tồn tại
            mkdir($dir_name, 0755, true);
        }
        if (is_string($file)) {
            $name = explode('.', $file);
            $file_name = explode('/', $file)[count(explode('/', $file)) - 1];
            $file_name_insert = time() . '.jpg';
            try {
                $v = file_get_contents($file);
            } catch (\Exception $ex) {
                $arrContextOptions=array(
                    "ssl"=>array(
                        "verify_peer"=>false,
                        "verify_peer_name"=>false,
                    ),
                );
                $v = file_get_contents($file, false, stream_context_create($arrContextOptions));
            }
            file_put_contents(base_path() . '/public/filemanager/userfiles/' . $path . '/' . $file_name_insert, $v);
            return $path . '/' . $file_name_insert;
        } else {
            $file_name = $file->getClientOriginalName();
            $name = explode('.', $file_name);
            $file_name_insert = str_slug(str_replace(end($name), '', $file_name), '-') . '.' . end($name);
            $file->move(base_path() . '/public/filemanager/userfiles/' . $path, $file_name_insert);
            return $path . '/' . $file_name_insert;
        }
    }
    public function myProfile($user = false)
    {
        if (!$user) {
            $user = \Auth::guard('admin')->user();
        }
        $data['user'] = $user;

        //  Kiểm tra các điểm số chưa được update thì update
        $this->updateQuizLogOfStudent($user->id);

        return view('themetienganh::pages.student.profile')->with($data);
    }

    public function getProfile($id)
    {
        if ($id == @\Auth::guard('admin')->user()->id) {
            return redirect('/profile');
        }

        $admin = Admin::find($id);
        if (is_object($admin)) {
            return $this->myProfile($admin);
        }
        return back();
    }

    public function updateQuizLogOfStudent($admin_id)
    {
        $quizz_log_no_update = CommonHelper::getFromCache('quiz_logs_synch_0', ['quiz_log']);
        if (!$quizz_log_no_update) {
            $quizz_log_no_update = QuizLog::where('admin_id', $admin_id)->where('synch', 0)->get();
            CommonHelper::putToCache('quiz_logs_synch_0', $quizz_log_no_update, ['quiz_log']);
        }

        foreach ($quizz_log_no_update as $quizz_log) {
            $quizController = new QuizController();
            $quizController->updateScores($quizz_log);
        }
        return true;
    }

    public function getEditProfile()
    {
        $data['user'] = \Auth::guard('admin')->user();
        return view('themetienganh::pages.student.edit_profile')->with($data);
    }

    public function postEditProfile(Request $request)
    {

//        $data = $request->only('name','tel','email','address','image','banner');
//        $repository->update(Auth::guard('student')->user()->id,$data,'success','Đã sửa thành công');
        $admin = Admin::find(\Auth::guard('admin')->user()->id);
        $admin->name = $request->name;
        $admin->tel = $request->tel;
        $admin->email = $request->email;
        $admin->address = $request->address;
        $admin->center_id = $request->center_id;
        $admin->birthday = $request->birthday;
        $admin->gender = $request->gender;
        if ($request->hasFile('image')) {
            $admin->image = CommonHelper::saveFile($request->image, 'admin');
        }
        if ($request->hasFile('banner')) {
            $admin->banner = CommonHelper::saveFile($request->banner, 'admin/banner');
        }
        $admin->save();
        Session::flash('success', 'Cập nhật thành công');
//      CommonHelper::flushCache($this->module['table_name']);
        return redirect()->back();
    }

    public function getChangePass()
    {
        $data['user'] = \Auth::guard('admin')->user();
        return view('themetienganh::pages.student.change_pass')->with($data);
    }

    public function postChangePass(Request $request)
    {
        $validator = Validator::make($request->all(), [
            're_new_password' => 'same:new_password',
        ], [
            're_new_password.same' => 'Mật khẩu nhập lại không trùng khớp!',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            if (\Auth::guard('admin')->user()->password != '') {
                if (\Auth::guard('admin')->user()->email == '') {
                    if (!\Auth::guard('admin')->attempt(array('tel' => \Auth::guard('admin')->user()->tel, 'password' => $request->get('password')))) {
                        \Session::flash('password-error', 'Mật khẩu không chính xác');
                        return back()->withInput();
                    }
                } else {
                    if (!\Auth::guard('admin')->attempt(array('email' => \Auth::guard('admin')->user()->email, 'password' => $request->get('password')))) {
                        \Session::flash('password-error', 'Mật khẩu không chính xác');
                        return back()->withInput();
                    }
                }
            }

            $admin = \Auth::guard('admin')->user();
            $admin->password = bcrypt($request->new_password);
            $admin->save();
            \Session::flash('success', 'Đổi mật khẩu thành công');
        }
        CommonHelper::flushCache($this->module['table_name']);
        return redirect()->back();
    }

    public function course($id)
    {
        $data['user'] = Admin::find($id);
        return view('themetienganh::pages.student.course', $data);
    }

    public function quiz($id)
    {
        $data['user'] = Admin::find($id);

        return view('themetienganh::pages.student.quiz', $data);
    }

    public function misson($id, $slug)
    {
        $data['user'] = Admin::find($id);
        $data['misson'] = Misson::where('slug', $slug)->first();
        $data['id'] = $id;

        return view('themetienganh::pages.student.misson_detail', $data);
    }

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {
        $user_social = Socialite::driver($provider)->user();
        if ($user_social->email == null || $user_social->email == '') {
            //  nếu fb//gg không trả về email thì set bừa cho họ cái email là id facebook/gg
            $user_social->email = $user_social->id . '@gmail.com';
        }
        $user = \Modules\ThemeTiengAnh\Models\Admin::updateOrCreate(
            ['email' => $user_social->email],
            [
                $provider . '_id' => $user_social->id,
                'name' => $user_social->name,
                $provider . '_token' => $user_social->token,
                'image' => $this->saveFile($user_social->getAvatar(), 'admin'),
            ]);

        if (RoleAdmin::where('admin_id', $user->id)->count() == 0) {
            //  nếu chưa gán quyền
            //  Set quyền mặc định cho admin
            $this->setDefaultRoleToAdmin($user);
        }

        \Auth::guard('admin')->login($user);
        return redirect('/');
    }


    public function setDefaultRoleToAdmin($admin, $role_id = false) {
        if (!$role_id) {
            $settings = @Setting::select(['name', 'value'])->where('type', 'role_tab')->pluck('value', 'name')->toArray();
            $role_id = $settings['role_default_id'];
        }

        //  Set quyền mặc định khi mới đăng ký tài khoản
        if (isset($role_id)) {
            RoleAdmin::insert([
                'admin_id' => $admin->id,
                'role_id' => $role_id,
            ]);
        }
        return true;
    }
    public function getPoint()
    {
        $data['user'] = Auth::guard('admin')->user();
        return view('themetienganh::pages.student.point', $data);
    }
}
