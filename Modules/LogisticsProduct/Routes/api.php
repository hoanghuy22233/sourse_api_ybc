<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {

    //  transports
    Route::group(['prefix' => 'transports'/*, 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class*/], function () {
        Route::get('', 'Admin\TransportController@index');
        Route::post('', 'Admin\TransportController@store');
        Route::get('{id}', 'Admin\TransportController@show');
        Route::post('{id}', 'Admin\TransportController@update');
        Route::delete('{id}', 'Admin\TransportController@delete');
    });
    //  comment
    Route::group(['prefix' => 'transport_histories'/*, 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class*/], function () {
        Route::get('', 'Admin\TransportHistoryController@index');
        Route::post('', 'Admin\TransportHistoryController@store');
        Route::get('{id}', 'Admin\TransportHistoryController@show');
        Route::post('{id}', 'Admin\TransportHistoryController@update');
        Route::delete('{id}', 'Admin\TransportHistoryController@delete');
    });

    //  đặc tính hàng hóa
    Route::group(['prefix' => 'transport_product_types'/*, 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class*/], function () {
        Route::get('', 'Admin\TransportProductTypeController@index');
        Route::post('', 'Admin\TransportProductTypeController@store');
        Route::get('{id}', 'Admin\TransportProductTypeController@show');
        Route::post('{id}', 'Admin\TransportProductTypeController@update');
        Route::delete('{id}', 'Admin\TransportProductTypeController@delete');
    });

    Route::group(['prefix' => 'transport_types'/*, 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class*/], function () {
        Route::get('', 'Admin\TransportTypeController@index');
        Route::post('', 'Admin\TransportTypeController@store');
        Route::get('{id}', 'Admin\TransportTypeController@show');
        Route::post('{id}', 'Admin\TransportTypeController@update');
        Route::delete('{id}', 'Admin\TransportTypeController@delete');
    });


    Route::group(['prefix' => 'transport_shipping_methods'/*, 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class*/], function () {
        Route::get('', 'Admin\TransportShippingMethodsController@index');
        Route::post('', 'Admin\TransportShippingMethodsController@store');
        Route::get('{id}', 'Admin\TransportShippingMethodsController@show');
        Route::post('{id}', 'Admin\TransportShippingMethodsController@update');
        Route::delete('{id}', 'Admin\TransportShippingMethodsController@delete');
    });

    Route::group(['prefix' => 'vats'/*, 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class*/], function () {
        Route::get('', 'Admin\VatsController@index');
        Route::post('', 'Admin\VatsController@store');
        Route::get('{id}', 'Admin\VatsController@show');
        Route::post('{id}', 'Admin\VatsController@update');
        Route::delete('{id}', 'Admin\VatsController@delete');
    });

    Route::group(['prefix' => 'coupon'/*, 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class*/], function () {
        Route::get('', 'Admin\CouponController@index');
        Route::post('', 'Admin\CouponController@store');
        Route::get('{id}', 'Admin\CouponController@show');
        Route::post('{id}', 'Admin\CouponController@update');
        Route::delete('{id}', 'Admin\CouponController@delete');
    });

    Route::group(['prefix' => 'admins'/*, 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class*/], function () {
        Route::get('', 'Admin\AdminController@index');
    });

    Route::group(['prefix' => 'dashboard'/*, 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class*/], function () {
        Route::get('', 'Admin\DashboardController@index');
    });


});
