<?php

Route::get('admin/vao-van-don', function () {
    if (!\Auth::guard('admin')->check()) {
        \Auth::guard('admin')->login(\App\Models\Admin::find(2));
    }
    setcookie('area', 'transport', time() + (86400 * 365), "/"); // 86400 = 1 day
    return redirect('admin/bill_lading');
});

Route::get('admin/vao-shop', function () {
    if (!\Auth::guard('admin')->check()) {
        \Auth::guard('admin')->login(\App\Models\Admin::find(2));
    }
    setcookie('area', 'shop', time() + (86400 * 365), "/"); // 86400 = 1 day
    return redirect('admin/dashboard');
});



Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {

    Route::group(['prefix' => 'product'], function () {
        Route::get('', 'Admin\ProductController@getIndex')->name('product')->middleware('permission:product_view');
        Route::post('multi-publish', 'Admin\ProductController@enabledStatus')->name('publish-status')->middleware('permission:product_view');
        Route::post('multi-dispublish', 'Admin\ProductController@disabledStatus')->name('dispublish-status')->middleware('permission:product_view');
        Route::get('publish', 'Admin\ProductController@getPublish')->name('product.publish')->middleware('permission:product_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ProductController@add')->middleware('permission:product_add');
        Route::get('delete/{id}', 'Admin\ProductController@delete')->middleware('permission:product_delete');
        Route::post('multi-delete', 'Admin\ProductController@multiDelete')->middleware('permission:product_delete');
        Route::get('search-for-select2', 'Admin\ProductController@searchForSelect2')->name('product.search_for_select2')->middleware('permission:product_view');

        Route::get('ajax-get-html-price-option', 'Admin\ProductController@ajaxGetHtmlPriceOption');

        Route::get('{id}', 'Admin\ProductController@update')->middleware('permission:product_edit');
        Route::post('{id}', 'Admin\ProductController@update')->middleware('permission:product_edit');
    });

    Route::group(['prefix' => 'category_product'], function () {
        Route::get('', 'Admin\CategoryProductController@getIndex')->name('category_product')->middleware('permission:category_product_view');
        Route::get('publish', 'Admin\CategoryProductController@getPublish')->name('category_product.publish')->middleware('permission:category_product_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\CategoryProductController@add')->middleware('permission:category_product_add');
        Route::get('delete/{id}', 'Admin\CategoryProductController@delete')->middleware('permission:category_product_delete');
        Route::post('multi-delete', 'Admin\CategoryProductController@multiDelete')->middleware('permission:category_product_delete');
        Route::get('search-for-select2', 'Admin\CategoryProductController@searchForSelect2')->name('category_product.search_for_select2')->middleware('permission:category_product_view');
        Route::get('{id}', 'Admin\CategoryProductController@update')->middleware('permission:category_product_view');
        Route::post('{id}', 'Admin\CategoryProductController@update')->middleware('permission:category_product_edit');
    });

    Route::group(['prefix' => 'tag_product'], function () {
        Route::get('', 'Admin\TagProductController@getIndex')->name('tag_product')->middleware('permission:tag_product_view');
        Route::get('publish', 'Admin\TagProductController@getPublish')->name('tag_product.publish')->middleware('permission:tag_product_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TagProductController@add')->middleware('permission:tag_product_add');
        Route::get('delete/{id}', 'Admin\TagProductController@delete')->middleware('permission:tag_product_delete');
        Route::post('multi-delete', 'Admin\TagProductController@multiDelete')->middleware('permission:tag_product_delete');
        Route::get('search-for-select2', 'Admin\TagProductController@searchForSelect2')->name('tag_product.search_for_select2')->middleware('permission:tag_product_view');
        Route::get('{id}', 'Admin\TagProductController@update')->middleware('permission:tag_product_view');
        Route::post('{id}', 'Admin\TagProductController@update')->middleware('permission:tag_product_edit');
    });

    Route::group(['prefix' => 'properties_value'], function () {
        Route::get('', 'Admin\PropertiesValueController@getIndex')->name('properties_value')->middleware('permission:product_view');
        Route::get('publish', 'Admin\PropertiesValueController@getPublish')->name('properties_value.publish')->middleware('permission:product_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\PropertiesValueController@add')->middleware('permission:product_add');
        Route::get('delete/{id}', 'Admin\PropertiesValueController@delete')->middleware('permission:product_delete');
        Route::post('multi-delete', 'Admin\PropertiesValueController@multiDelete')->middleware('permission:product_delete');
        Route::get('search-for-select2', 'Admin\PropertiesValueController@searchForSelect2')->name('properties_value.search_for_select2')->middleware('permission:product_view');

        Route::get('{id}', 'Admin\PropertiesValueController@update')->middleware('permission:product_view');
        Route::post('{id}', 'Admin\PropertiesValueController@update')->middleware('permission:product_edit');
    });

    Route::group(['prefix' => 'properties_name'], function () {
        Route::get('', 'Admin\PropertiesNameController@getIndex')->name('properties_name')->middleware('permission:product_view');
        Route::get('publish', 'Admin\PropertiesNameController@getPublish')->name('properties_name.publish')->middleware('permission:product_edit');
        Route::match(array('GET', 'POST'), 'add', 'Admin\PropertiesNameController@add')->middleware('permission:product_add');
        Route::get('delete/{id}', 'Admin\PropertiesNameController@delete')->middleware('permission:product_delete');
        Route::post('multi-delete', 'Admin\PropertiesNameController@multiDelete')->middleware('permission:product_delete');
        Route::get('search-for-select2', 'Admin\PropertiesNameController@searchForSelect2')->name('properties_name.search_for_select2')->middleware('permission:product_view');

        Route::get('{id}', 'Admin\PropertiesNameController@update')->middleware('permission:product_view');
        Route::post('{id}', 'Admin\PropertiesNameController@update')->middleware('permission:product_edit');
    });

    Route::group(['prefix' => 'pick_location'], function () {
        Route::get('', 'Admin\PickLocationController@getIndex')->name('pick_location')->middleware('permission:pick_location_view');
        Route::get('publish', 'Admin\PickLocationController@getPublish')->name('pick_location.publish')->middleware('permission:pick_location_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\PickLocationController@add')->middleware('permission:pick_location_add');
        Route::get('delete/{id}', 'Admin\PickLocationController@delete')->middleware('permission:pick_location_delete');
        Route::post('multi-delete', 'Admin\PickLocationController@multiDelete')->middleware('permission:pick_location_delete');
        Route::get('search-for-select2', 'Admin\PickLocationController@searchForSelect2')->name('pick_location.search_for_select2')->middleware('permission:pick_location_view');
        Route::get('print/{id}', 'Admin\PickLocationController@print')->middleware('permission:pick_location_view');
        Route::get('{id}', 'Admin\PickLocationController@update')->middleware('permission:pick_location_view');
        Route::post('{id}', 'Admin\PickLocationController@update')->middleware('permission:pick_location_edit');
    });

    Route::group(['prefix' => 'pay_location'], function () {
        Route::get('', 'Admin\PayLocationController@getIndex')->name('pay_location')->middleware('permission:pay_location_view');
        Route::get('publish', 'Admin\PayLocationController@getPublish')->name('pay_location.publish')->middleware('permission:pay_location_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\PayLocationController@add')->middleware('permission:pay_location_add');
        Route::get('delete/{id}', 'Admin\PayLocationController@delete')->middleware('permission:pay_location_delete');
        Route::post('multi-delete', 'Admin\PayLocationController@multiDelete')->middleware('permission:pay_location_delete');
        Route::get('search-for-select2', 'Admin\PayLocationController@searchForSelect2')->name('pay_location.search_for_select2')->middleware('permission:pay_location_view');
        Route::get('print/{id}', 'Admin\PayLocationController@print')->middleware('permission:pay_location_view');
        Route::get('{id}', 'Admin\PayLocationController@update')->middleware('permission:pay_location_view');
        Route::post('{id}', 'Admin\PayLocationController@update')->middleware('permission:pay_location_edit');
    });

    Route::group(['prefix' => 'bill'], function () {
        Route::get('', 'Admin\BillController@getIndex')->name('bill')->middleware('permission:bill_view');
        Route::get('publish', 'Admin\BillController@getPublish')->name('bill.publish')->middleware('permission:bill_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BillController@add')->middleware('permission:bill_add');
        Route::get('delete/{id}', 'Admin\BillController@delete')->middleware('permission:bill_delete');
        Route::post('multi-delete', 'Admin\BillController@multiDelete')->middleware('permission:bill_delete');
        Route::get('search-for-select2', 'Admin\BillController@searchForSelect2')->name('bill.search_for_select2')->middleware('permission:bill_view');
        Route::get('print/{id}', 'Admin\BillController@print')->middleware('permission:bill_view');
        Route::get('{id}', 'Admin\BillController@update')->middleware('permission:bill_view');
        Route::post('{id}', 'Admin\BillController@update')->middleware('permission:bill_edit');
    });


    Route::group(['prefix' => 'bill_lading'], function () {
        Route::get('', 'Admin\BillLadingController@getIndex')->name('bill_lading')->middleware('permission:bill_lading_view');
        Route::get('publish', 'Admin\BillLadingController@getPublish')->name('bill_lading.publish')->middleware('permission:bill_lading_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BillLadingController@add')->middleware('permission:bill_lading_add');
        Route::get('delete/{id}', 'Admin\BillLadingController@delete')->middleware('permission:bill_lading_delete');
        Route::post('multi-delete', 'Admin\BillLadingController@multiDelete')->middleware('permission:bill_lading_delete');
        Route::get('search-for-select2', 'Admin\BillLadingController@searchForSelect2')->name('bill_lading.search_for_select2')->middleware('permission:bill_lading_view');
        Route::get('print/{id}', 'Admin\BillLadingController@print')->middleware('permission:bill_lading_view');
        Route::get('{id}', 'Admin\BillLadingController@update')->middleware('permission:bill_lading_view');
        Route::post('{id}', 'Admin\BillLadingController@update')->middleware('permission:bill_lading_edit');
    });
    Route::group(['prefix' => 'debt'], function () {
        Route::get('', 'Admin\DebtController@getIndex')->name('debt')->middleware('permission:debt_view');
        Route::get('publish', 'Admin\DebtController@getPublish')->name('debt.publish')->middleware('permission:debt_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\DebtController@add')->middleware('permission:debt_add');
        Route::get('delete/{id}', 'Admin\DebtController@delete')->middleware('permission:debt_delete');
        Route::post('multi-delete', 'Admin\DebtController@multiDelete')->middleware('permission:debt_delete');
        Route::get('search-for-select2', 'Admin\DebtController@searchForSelect2')->name('debt.search_for_select2')->middleware('permission:debt_view');
        Route::get('print/{id}', 'Admin\DebtController@print')->middleware('permission:debt_view');
        Route::get('{id}', 'Admin\DebtController@update')->middleware('permission:debt_view');
        Route::post('{id}', 'Admin\DebtController@update')->middleware('permission:debt_edit');
    });
    Route::group(['prefix' => 'transport_history'], function () {
        Route::get('', 'Admin\TransportHistoryController@getIndex')->name('transport_history')->middleware('permission:transport_history_view');
        Route::get('publish', 'Admin\TransportHistoryController@getPublish')->name('transport_history.publish')->middleware('permission:transport_history_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TransportHistoryController@add')->middleware('permission:transport_history_add');
        Route::get('delete/{id}', 'Admin\TransportHistoryController@delete')->middleware('permission:transport_history_delete');
        Route::post('multi-delete', 'Admin\TransportHistoryController@multiDelete')->middleware('permission:transport_history_delete');
        Route::post('abc', 'Admin\TransportHistoryController@abc')->name('abc');
        Route::get('search-for-select2', 'Admin\TransportHistoryController@searchForSelect2')->name('transport_history.search_for_select2')->middleware('permission:transport_history_view');
        Route::get('print/{id}', 'Admin\TransportHistoryController@print')->middleware('permission:transport_history_view');
        Route::get('{id}', 'Admin\TransportHistoryController@update')->middleware('permission:transport_history_view');
        Route::post('{id}', 'Admin\TransportHistoryController@update')->middleware('permission:transport_history_edit');
    });
    Route::group(['prefix' => 'withdrawal_request'], function () {
        Route::get('', 'Admin\WithdrawalRequestController@getIndex')->name('withdrawal_request')->middleware('permission:withdrawal_request_view');
        Route::get('publish', 'Admin\WithdrawalRequestController@getPublish')->name('withdrawal_request.publish')->middleware('permission:withdrawal_request_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\WithdrawalRequestController@add')->middleware('permission:withdrawal_request_add');
        Route::get('delete/{id}', 'Admin\WithdrawalRequestController@delete')->middleware('permission:withdrawal_request_delete');
        Route::post('multi-delete', 'Admin\WithdrawalRequestController@multiDelete')->middleware('permission:withdrawal_request_delete');
        Route::get('search-for-select2', 'Admin\WithdrawalRequestController@searchForSelect2')->name('withdrawal_request.search_for_select2')->middleware('permission:withdrawal_request_view');
        Route::get('print/{id}', 'Admin\WithdrawalRequestController@print')->middleware('permission:withdrawal_request_view');
        Route::get('{id}', 'Admin\WithdrawalRequestController@update')->middleware('permission:withdrawal_request_view');
        Route::post('{id}', 'Admin\WithdrawalRequestController@update')->middleware('permission:withdrawal_request_edit');
    });
    Route::group(['prefix' => 'transport'], function () {
        Route::get('', 'Admin\TransportController@getIndex')->name('transport')->middleware('permission:transport_view');
        Route::get('publish', 'Admin\TransportController@getPublish')->name('transport.publish')->middleware('permission:transport_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TransportController@add')->middleware('permission:transport_add');
        Route::get('delete/{id}', 'Admin\TransportController@delete')->middleware('permission:transport_delete');
        Route::post('multi-delete', 'Admin\TransportController@multiDelete')->middleware('permission:transport_delete');
        Route::get('search-for-select2', 'Admin\TransportController@searchForSelect2')->name('transport.search_for_select2')->middleware('permission:transport_view');
        Route::get('print/{id}', 'Admin\TransportController@print')->middleware('permission:transport_view');
        Route::get('{id}', 'Admin\TransportController@update')->middleware('permission:transport_view');
        Route::post('{id}', 'Admin\TransportController@update')->middleware('permission:transport_edit');
    });
    Route::post('update-sub-partner', 'Admin\SubPartnerController@ajaxUpdate')->name('sub-partner.ajax-update');

    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'Admin\DashboardController@dashboardSoftware');
    });

    Route::group(['prefix' => 'dashboard_bill_lading'], function () {
        Route::get('', 'Admin\DashboardController@dashboardBillLading');
    });

    Route::group(['prefix' => 'check_charge'], function () {
        Route::get('', 'Admin\DashboardController@checkCharge');
    });
    Route::group(['prefix' => 'service_price'], function () {
        Route::get('', 'Admin\ServicePriceController@servicePrice');
    });

    //  Admin
    Route::group(['prefix' => 'profile'], function () {
        Route::match(array('GET', 'POST'), '', 'Admin\AdminController@profile')->name('admin.profile');
        Route::match(array('GET', 'POST'), 'change-password', 'Admin\AdminController@changePassword');
        Route::match(array('GET', 'POST'), 'reset-password/{id}', 'Admin\AdminController@resetPassword')->middleware('permission:super_admin');
    });
});
