<?php

namespace Modules\LogisticsProduct\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Auth;
use DB;
use Illuminate\Http\Request;
use Mail;
use Modules\JdesBill\Models\Bill;
use Modules\Theme\Models\Post;
use Modules\JdesProduct\Models\Product;
use Modules\ThemeSemicolonwebJdes\Models\Company;

class DashboardController extends Controller
{
    protected $module = [
    ];

    public function dashboardSoftware()
    {
        $data['page_title'] = 'Thống kê';
        $data['page_type'] = 'list';
        return view('logisticsproduct::dashboard', $data);
    }

    public function dashboardBillLading()
    {
        $data['page_title'] = 'Thống kê';
        $data['page_type'] = 'list';
        return view('logisticsproduct::dashboard_bill_lading', $data);
    }

    public function checkCharge()
    {
        $data['page_title'] = 'Tra cước & tạo đơn';
        $data['page_type'] = 'list';
        return view('logisticsproduct::check_charges', $data);
    }


    public function tooltipInfo(Request $request)
    {
        $modal = new $request->modal;
        $data['item'] = $modal->find($request->id);
        $data['tooltip_info'] = $request->tooltip_info;

        return view('admin.common.modal.tooltip_info', $data);
    }

    public function ajax_up_file(Request $request)
    {
        if ($request->has('file')) {
            $file = CommonHelper::saveFile($request->file('file'));
        }
        return $file;
    }
}
