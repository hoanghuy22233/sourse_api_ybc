<?php

namespace Modules\LogisticsProduct\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Auth;
use DB;
use Illuminate\Http\Request;
use Mail;
use Modules\JdesBill\Models\Bill;
use Modules\Theme\Models\Post;
use Modules\JdesProduct\Models\Product;
use Modules\ThemeSemicolonwebJdes\Models\Company;

class ServicePriceController extends Controller
{
    protected $module = [
    ];



    public function servicePrice()
    {
        $data['page_title'] = 'Bảng giá dịch vụ';
        $data['page_type'] = 'list';
        return view('logisticsproduct::service_price', $data);
    }



}
