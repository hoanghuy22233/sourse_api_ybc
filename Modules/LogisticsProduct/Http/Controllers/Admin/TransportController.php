<?php

namespace Modules\LogisticsProduct\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\LogisticsProduct\Models\Bill;
use Modules\LogisticsProduct\Models\ShippingMethod;
use Modules\ThemeLogistics\Models\Admin;
use Modules\ThemeLogistics\Models\District;
use Modules\ThemeLogistics\Models\Province;
use Modules\ThemeLogistics\Models\TransportProductType;
use Modules\ThemeLogistics\Models\TransportType;
use Modules\ThemeLogistics\Models\Ward;
use Modules\ThemeLogisticsAdmin\Models\TransportShippingMethods;
use Validator;

class TransportController extends CURDBaseController
{

    protected $orderByRaw = 'id DESC';

    protected $module = [
        'code' => 'transport',
        'table_name' => 'transports',
        'label' => 'Quản lý vận đơn',
        'modal' => '\Modules\LogisticsProduct\Models\Transport',
        'list' => [
            ['name' => 'code', 'type' => 'text_edit', 'label' => 'Mã đơn hàng'],
            ['name' => 'admin_owneds', 'type' => 'select', 'options' =>
                [
                    1 => 'Etal group',
                    2 => 'Shop',
                ], 'class' => '', 'label' => 'Người sở hữu', 'value' => 1, 'group_class' => ''],
            ['name' => 'admin_id', 'type' => 'relation', 'object' => 'admin', 'display_field' => 'name', 'label' => 'Người tạo'],
            ['name' => 'name', 'type' => 'text', 'label' => 'Tên hàng'],
            ['name' => 'billing_address', 'type' => 'text', 'label' => 'Địa chỉ thanh toán'],
            ['name' => 'shipping_address', 'type' => 'text', 'label' => 'Địa chỉ giao hàng'],
            ['name' => 'discount_price', 'type' => 'text', 'label' => 'Thành tiền sau triết khấu'],
            ['name' => 'note', 'type' => 'text', 'label' => 'Ghi chú'],
            ['name' => 'shipping_method_id', 'type' => 'relation', 'label' => 'Phương thức vận chuyển', 'object' => 'shipping_method', 'display_field' => 'name'],

            ['name' => 'mass', 'type' => 'text', 'label' => 'Trọng lượng (Kg)'],
            ['name' => 'status', 'type' => 'select', 'options' => [
                0 => 'Chưa thanh toán',
                1 => 'Chờ lấy hàng',
                2 => 'Đã lấy hàng',
                3 => 'Đã giao hàng',
                4 => 'Nợ COD',
                5 => 'Đã trả COD',
                6 => 'Không phát được',
                7 => 'Đã hủy',
                8 => 'Đang chuyển hoàn',
                9 => 'Đã chuyển hoàn',
                10 => 'Sự cố',
            ], 'label' => 'Trang thái'], ['name' => 'view', 'type' => 'custom', 'td' => 'logisticsproduct::list.td.view_frontend', 'label' => 'Xem'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'shipping_name', 'type' => 'text', 'class' => '', 'label' => 'Họ và tên'],
                ['name' => 'shipping_tel', 'type' => 'text', 'class' => '', 'label' => 'Số điện thoại'],
                ['name' => 'shipping_address', 'type' => 'text', 'label' => 'Địa chỉ'],
//                ['name' => 'shipping_province_id', 'type' => 'select2_ajax_model', 'label' => 'Tỉnh', 'model' => Province::class, 'object' => 'province', 'display_field' => 'name', 'group_class' => ''],
//                ['name' => 'shipping_district_id', 'type' => 'select2_ajax_model', 'label' => 'Huyện', 'model' => District::class, 'object' => 'district', 'display_field' => 'name', 'group_class' => ''],
                ['name' => 'shipping_ward_id', 'type' => 'select2_ajax_model', 'label' => 'Xã', 'model' => Ward::class, 'object' => 'ward', 'display_field' => 'name', 'group_class' => ''],
                ['name' => 'shipping_email', 'type' => 'text', 'label' => 'Email'],

            ],
            'info_tab' => [

                ['name' => 'billing_name', 'type' => 'text', 'class' => '', 'label' => 'Họ và tên'],
                ['name' => 'billing_tel', 'type' => 'text', 'class' => '', 'label' => 'Số điện thoại'],
                ['name' => 'billing_address', 'type' => 'text', 'label' => 'Địa chỉ'],
//                ['name' => 'billing_province_id', 'type' => 'select2_ajax_model', 'label' => 'Tỉnh', 'model' => Province::class, 'object' => 'province', 'display_field' => 'name', 'group_class' => ''],
//                ['name' => 'billing_district_id', 'type' => 'select2_ajax_model', 'label' => 'Huyện', 'model' => District::class, 'object' => 'district', 'display_field' => 'name', 'group_class' => ''],
                ['name' => 'billing_ward_id', 'type' => 'select2_ajax_model', 'label' => 'Xã', 'model' => Ward::class, 'object' => 'ward', 'display_field' => 'name', 'group_class' => ''],
                ['name' => 'billing_email', 'type' => 'text', 'class' => '', 'label' => 'Email'],

            ],

            'info2_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => '', 'label' => 'Tên hàng', 'group_class' => 'col-md-6'],
                ['name' => 'code', 'type' => 'text', 'class' => '', 'label' => 'Mã hàng', 'group_class' => 'col-md-6'],
                ['name' => 'mass', 'type' => 'number', 'class' => '', 'label' => 'Trọng lượng (Kg)', 'group_class' => 'col-md-6'],
                ['name' => 'discount_code', 'type' => 'text', 'label' => 'Mã giảm giá', 'group_class' => 'col-md-6'],
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Hình ảnh'],
                ['name' => 'note', 'type' => 'textarea', 'class' => '', 'label' => 'Ghi chú'],

                ['name' => 'payment_methods', 'type' => 'select', 'options' =>
                    [
                        1 => 'Người bán trả',
                        2 => 'Người mua trả',

                    ], 'class' => '', 'label' => 'Phương thức thanh toán', 'value' => 1, 'group_class' => ''],
//                ['name' => 'type_commodities', 'type' => 'select', 'options' =>
//                    [
//                        1 => 'Tài liệu',
//                        2 => 'Thời trang mỹ phẩm',
//                        3 => 'Đồ điện tử',
//                        4 => 'Dược phẩm',
//                        5 => 'Thực phẩm',
//                        6 => 'Đồ tươi sống',
//                        7 => 'Đồ có pin',
//                        8 => 'Chất bột nước',
//                        9 => 'Chất dễ cháy',
//
//                    ], 'class' => '', 'label' => 'Đặc tính hàng hóa', 'value' => 1, 'group_class' => 'col-md-6'],
                ['name' => 'transport_type_id', 'type' => 'select2_model', 'label' => 'Loại vận chuyển', 'model' => TransportType::class, 'object' => 'transport_type', 'display_field' => 'name', 'group_class' => ''],
                ['name' => 'transport_shipping_method_id', 'type' => 'select2_model', 'label' => 'Phương thức vận chuyển', 'model' => TransportShippingMethods::class, 'object' => 'transport_shipping_method', 'display_field' => 'name', 'group_class' => ''],
                ['name' => 'transport_product_type_id', 'type' => 'select2_model', 'label' => 'Đặc tính hàng hóa', 'model' => TransportProductType::class, 'object' => 'transport_product_type', 'display_field' => 'name', 'group_class' => ''],

                ['name' => 'package_number', 'type' => 'number', 'class' => '', 'label' => 'Số kiện', 'group_class' => 'col-md-6'],
                ['name' => 'mass', 'type' => 'number', 'class' => '', 'label' => 'Khối lượng', 'group_class' => 'col-md-6'],
                ['name' => 'discount_price', 'type' => 'price_vi', 'class' => '', 'label' => 'Thành tiền', 'group_class' => 'col-md-6'],
                ['name' => 'shipping_fee', 'type' => 'price_vi', 'class' => '', 'label' => 'Phí ship', 'group_class' => 'col-md-6'],
                ['name' => 'vat', 'type' => 'text', 'class' => '', 'label' => 'VAT', 'group_class' => 'col-md-6'],
                ['name' => 'shipping_method_id', 'type' => 'select2_model', 'label' => 'Phương thức vận chuyển', 'model' => ShippingMethod::class, 'object' => 'shipping_method', 'display_field' => 'name', 'group_class' => ''],

                ['name' => 'owned', 'type' => 'select', 'options' =>
                    [
                        1 => 'Etal group',
                        2 => 'Shop',
                    ], 'class' => '', 'label' => 'Người sở hữu', 'value' => 1, 'group_class' => 'col-md-6'],
                ['name' => 'status', 'type' => 'select', 'options' =>
                    [
                        0 => 'Chưa thanh toán',
                        1 => 'Chờ lấy hàng',
                        2 => 'Đã lấy hàng',
                        3 => 'Đã giao hàng',
                        4 => 'Nợ COD',
                        5 => 'Đã trả COD',
                        6 => 'Không phát được',
                        7 => 'Đã hủy',
                        8 => 'Đang chuyển hoàn',
                        9 => 'Đã chuyển hoàn',
                        10 => 'Sự cố',
                    ], 'class' => '', 'label' => 'Trạng thái', 'value' => 0, 'group_class' => 'col-md-6'],

            ],

            'history_tab' => [
                ['name' => 'iframe', 'type' => 'iframe', 'class' => 'col-xs-12 col-md-8 padding-left', 'src' => '/admin/transport_history?transport_id={id}', 'inner' => 'style="min-height: 1000px;"'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, mã vận đơn, nơi gửi, tên người nhận, địa chỉ',
        'fields' => 'id, code, place_submission, recipient_name, address, billing_address, shipping_address',
    ];

    protected $filter = [
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                0 => 'Chưa thanh toán',
                1 => 'Chờ lấy hàng',
                2 => 'Đã lấy hàng',
                3 => 'Đã giao hàng',
                4 => 'Nợ COD',
                5 => 'Đã trả COD',
                6 => 'Không phát được',
                7 => 'Đã hủy',
                8 => 'Đang chuyển hoàn',
                9 => 'Đã chuyển hoàn',
                10 => 'Sự cố',
            ],
        ],
        'owned' => [
            'label' => 'Người sở hữu',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                1 => 'Etal group',
                2 => 'Shop',
            ],
        ],
        'province_id' => [
            'label' => 'Tỉnh',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'province',
            'model' => Province::class,
            'query_type' => '='
        ],
        'district_id' => [
            'label' => 'Huyện',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'district',
            'model' => District::class,
            'query_type' => '='
        ],
        'ward_id' => [
            'label' => 'Xã',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'ward',
            'model' => Ward::class,
            'query_type' => 'custom'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('logisticsproduct::transport.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('admin_id', \Auth::guard('admin')->user()->id);

        }
        return $query;
    }

    public function add(Request $request)
    {

        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('logisticsproduct::transport.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'code' => 'required'
                ], [
                    'code.required' => 'Bắt buộc phải nhập mã',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;
//                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
                    unset($data['iframe']);

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);


            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('logisticsproduct::transport.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'code' => 'required'
                ], [
                    'code.required' => 'Bắt buộc phải nhập mã',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    unset($data['iframe']);

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

//            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
