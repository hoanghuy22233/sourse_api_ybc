<?php

namespace Modules\LogisticsProduct\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\LogisticsProduct\Models\Category;
use Modules\LogisticsProduct\Models\Debt;
use Modules\LogisticsProduct\Models\Manufacturer;
use Modules\LogisticsProduct\Models\Product;
use Modules\LogisticsProduct\Models\PropertieValue;
use Modules\LogisticsProduct\Models\PropertyValue;
use Validator;

class DebtController extends CURDBaseController
{
    protected $module = [
        'code' => 'debt',
        'table_name' => 'debts',
        'label' => 'Doanh thu',
        'modal' => '\Modules\LogisticsProduct\Models\Debt',
        'list' => [
            ['name' => 'transaction_amount','type' => 'custom', 'td' => 'logisticsproduct::list.td.transaction_amount', 'label' => 'Số tiền giao dịch'],
            ['name' => 'surplus', 'type' => 'text_edit', 'label' => 'Số dư'],
            ['name' => 'transaction_reason', 'type' => 'text', 'label' => 'Lý do giao dịch'],
            ['name' => 'date', 'type' => 'datetime_vi', 'label' => 'Thời gian'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'transaction_amount', 'type' => 'text', 'class' => 'number_price', 'label' => 'Số tiền giao dịch'],
                ['name' => 'surplus', 'type' => 'text', 'class' => '', 'label' => 'Số dư'],
                ['name' => 'transaction_reason', 'type' => 'text', 'class' => '', 'label' => 'Lý do giao dịch'],
                ['name' => 'date', 'type' => 'datetime-local', 'class' => '', 'label' => 'Thời gian'],
                ['name' => 'type_deal', 'type' => 'select', 'options' =>
                    [
                        0 => 'Trừ tiền',
                        1 => 'Cộng tiền',
                    ], 'class' => '', 'label' => 'Loại giao dịch', 'value' => 0, 'group_class' => ''],
            ],



        ],
    ];

    protected $quick_search = [
        'label' => 'ID, Số tiền giao dịch',
        'fields' => 'id, transaction_amount'
    ];

    protected $filter = [
//        'show_field_search' => [
//            'label' => 'Hiển thị search',
//            'type' => 'select',
//            'query_type' => '=',
//            'options' => [
//                '' => 'Chọn',
//                0 => 'Ẩn',
//                1 => 'Hiển thị',
//            ],
//        ],

    ];

    public function getIndex(Request $request)
    {

        $data = $this->getDataList($request);

        return view('logisticsproduct::debt.list')->with($data);
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('logisticsproduct::debt.add')->with($data);
            } else if ($_POST) {
//                dd($request->all());
                $validator = Validator::make($request->all(), [
                    'surplus' => 'required'
                ], [
                    'surplus.required' => 'Bắt buộc phải nhập số dư',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    unset($data['iframe']);
                    #

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->adminLog($request,$this->model,'add');
                        $this->afterAddLog($request, $this->model);
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }


                    if ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }
                    return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('logisticsproduct::debt.edit')->with($data);
        } else if ($_POST) {
//            dd($request->all());
            $validator = Validator::make($request->all(), [
                'surplus' => 'required'
            ], [
                'surplus.required' => 'Bắt buộc phải nhập số dư',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert
                unset($data['iframe']);
                #

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    $this->adminLog($request,$item,'edit');
                    CommonHelper::flushCache($this->module['table_name']);
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            $this->adminLog($request,$item,'publish');
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            $this->adminLog($request,$item,'delete');
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            $this->adminLog($request,$ids,'multi_delete');
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function duplicate(Request $request, $id)
    {
        $poduct = Product::find($id);
        $poduct_new = $poduct->replicate();
//        $poduct_new->company_id = \Auth::guard('admin')->user()->last_company_id;
        $poduct_new->admin_id = \Auth::guard('admin')->user()->id;
        $poduct_new->save();
        return $poduct_new;
    }

    public function print($id, Request $request)
    {
        $data['page_title'] = 'In công nợ';
        $bill = Debt::findOrFail($id);
        $data['result'] = $bill;
        $data['header_print'] = @$bill->company->header_print;
        $data['footer_print'] = @$bill->company->footer_print;
        return view('logisticsproduct::debt.print')->with($data);
    }

//    public function searchForSelect2(Request $request)
//    {
//        $data = $this->model->select([$request->col, 'id'])->where($request->col, 'like', '%' . $request->keyword . '%');
//
//        if ($request->where != '') {
//            $data = $data->whereRaw(urldecode(str_replace('&#039;', "'", $request->where)));
//        }
//        if (@$request->company_id != null) {
//            $data = $data->where('company_id', $request->company_id);
//        }
//        $data = $data->limit(5)->get();
//        return response()->json([
//            'status' => true,
//            'items' => $data
//        ]);
//    }
}
