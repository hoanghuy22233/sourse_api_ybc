<?php

namespace Modules\LogisticsProduct\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\LogisticsProduct\Models\Bill;
use Modules\ThemeLogistics\Models\Admin;
use Modules\ThemeLogistics\Models\District;
use Modules\ThemeLogistics\Models\Province;
use Modules\ThemeLogistics\Models\Ward;
use Validator;

class BillLadingController extends CURDBaseController
{

    protected $orderByRaw = 'id DESC';

    protected $module = [
        'code' => 'bill_lading',
        'table_name' => 'bill_ladings',
        'label' => 'Vận đơn',
        'modal' => '\Modules\LogisticsProduct\Models\BillLading',
        'list' => [
            ['name' => 'bill_lading_code', 'type' => 'text_edit', 'label' => 'Mã tham chiếu'],
            ['name' => 'payment_methods', 'type' => 'select', 'options' =>
                [
                    1 => 'Người gửi thanh toán ngay',
                    2 => 'Người nhận thanh toán ngay',

                ], 'class' => '', 'label' => 'Phương thức thanh toán', 'value' => 1, 'group_class' => ''],
            ['name' => 'type_commodities', 'type' => 'select', 'options' =>
                [
                    1 => 'Chứng từ',
                    2 => 'Hàng hóa',

                ], 'class' => '', 'label' => 'Chọn loại hàng hóa', 'value' => 1, 'group_class' => ''],
            ['name' => 'package_number', 'type' => 'text', 'label' => 'Số kiện'],
            ['name' => 'mass', 'type' => 'text', 'label' => 'Đơn vị'],
            ['name' => 'date_creation', 'type' => 'date_vi', 'label' => 'Thời gian lấy hàng'],
            ['name' => 'collect_household', 'type' => 'text', 'label' => 'Tiền thu hộ'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'sender_admin_id', 'type' => 'select2_model', 'label' => 'Chọn điểm lấy hàng', 'model' => Admin::class, 'object' => 'admin', 'display_field' => 'name'],
                ['name' => 'sender_reminiscent', 'type' => 'text', 'class' => '', 'label' => 'Tên gợi nhớ'],
                ['name' => 'sender_name', 'type' => 'text', 'class' => '', 'label' => 'Tên gợi nhớ'],
                ['name' => 'sender_tel', 'type' => 'text', 'class' => '', 'label' => 'Số điện thoại'],
                ['name' => 'sender_address', 'type' => 'text', 'label' => 'Địa chỉ người nhận'],
                ['name' => 'sender_province_id', 'type' => 'select2_ajax_model', 'label' => 'Tỉnh', 'model' => Province::class, 'object' => 'province', 'display_field' => 'name', 'group_class' => ''],
                ['name' => 'sender_district_id', 'type' => 'select2_ajax_model', 'label' => 'Huyện', 'model' => District::class, 'object' => 'district', 'display_field' => 'name', 'group_class' => ''],
                ['name' => 'sender_ward_id', 'type' => 'select2_ajax_model', 'label' => 'Xã', 'model' => Ward::class, 'object' => 'ward', 'display_field' => 'name', 'group_class' => ''],

            ],
            'info_tab' => [
                ['name' => 'receiver_admin_id', 'type' => 'select2_model', 'label' => 'Chọn điểm lấy hàng', 'model' => Admin::class, 'object' => 'admin', 'display_field' => 'name'],
                ['name' => 'receiver_reminiscent', 'type' => 'text', 'class' => '', 'label' => 'Tên gợi nhớ'],
                ['name' => 'receiver_name', 'type' => 'text', 'class' => '', 'label' => 'Tên gợi nhớ'],
                ['name' => 'receiver_tel', 'type' => 'text', 'class' => '', 'label' => 'Số điện thoại'],
                ['name' => 'receiver_address', 'type' => 'text', 'label' => 'Địa chỉ người nhận'],
                ['name' => 'receiver_province_id', 'type' => 'select2_ajax_model', 'label' => 'Tỉnh', 'model' => Province::class, 'object' => 'province', 'display_field' => 'name', 'group_class' => ''],
                ['name' => 'receiver_district_id', 'type' => 'select2_ajax_model', 'label' => 'Huyện', 'model' => District::class, 'object' => 'district', 'display_field' => 'name', 'group_class' => ''],
                ['name' => 'receiver_ward_id', 'type' => 'select2_ajax_model', 'label' => 'Xã', 'model' => Ward::class, 'object' => 'ward', 'display_field' => 'name', 'group_class' => ''],
            ],

            'info2_tab' => [
                ['name' => 'bill_lading_code', 'type' => 'text', 'class' => '', 'label' => 'Mã tham chiếu', 'group_class' => ''],
                ['name' => 'payment_methods', 'type' => 'select', 'options' =>
                    [
                        1 => 'Người gửi thanh toán ngay',
                        2 => 'Người nhận thanh toán ngay',

                    ], 'class' => '', 'label' => 'Phương thức thanh toán', 'value' => 1, 'group_class' => ''],
                ['name' => 'type_commodities', 'type' => 'select', 'options' =>
                    [
                        1 => 'Chứng từ',
                        2 => 'Hàng hóa',

                    ], 'class' => '', 'label' => 'Chọn loại hàng hóa', 'value' => 1, 'group_class' => ''],

                ['name' => 'package_number', 'type' => 'number', 'class' => '', 'label' => 'Số kiện', 'group_class' => 'col-md-6'],
                ['name' => 'mass', 'type' => 'number', 'class' => '', 'label' => 'Khối lượng', 'group_class' => 'col-md-6'],
                ['name' => 'long_commodity', 'type' => 'number', 'class' => '', 'label' => 'Dài (cm)', 'group_class' => 'col-md-4'],
                ['name' => 'width_commodity', 'type' => 'number', 'class' => '', 'label' => 'Rộng (cm)', 'group_class' => 'col-md-4'],
                ['name' => 'high_commodity', 'type' => 'number', 'class' => '', 'label' => 'Cao (cm)', 'group_class' => 'col-md-4'],
                ['name' => 'commodity_value', 'type' => 'text', 'class' => '', 'label' => 'Giá trị hàng hóa', 'group_class' => 'col-md-6'],
                ['name' => 'collect_household', 'type' => 'number', 'class' => '', 'label' => 'Số tiền thu hộ', 'group_class' => 'col-md-6'],
                ['name' => 'license_moved', 'type' => 'checkbox', 'label' => 'Chuyển hoàn chứng từ','value' => 1, 'group_class' => ''],
                ['name' => 'pickup_request', 'type' => 'select', 'options' =>
                    [
                        1 => 'Lấy hàng bằng xe máy',
                        2 => 'Lấy hàng bằng ô tô',
                    ], 'class' => '', 'label' => 'Yêu cầu lấy hàng', 'value' => 1, 'group_class' => ''],
                ['name' => 'date_creation', 'type' => 'date', 'label' => 'Thời gian lấy hàng', 'group_class' => 'col-md-6'],
                ['name' => 'hours', 'type' => 'select', 'options' =>
                    [
                        8 => '8',
                        9 => '9',
                        10 => '10',
                        11 => '11',
                        12 => '12',
                        13 => '13',
                        14 => '14',
                        15 => '15',
                        16 => '16',
                        17 => '17',
                        18 => '18',
                    ], 'class' => '', 'label' => 'Giờ', 'value' => 1, 'group_class' => 'col-md-3'],
                ['name' => 'minute', 'type' => 'select', 'options' =>
                    [
                        0 => '0',
                        15 => '15',
                        30 => '30',
                        45 => '45',

                    ], 'class' => '', 'label' => 'Phút', 'value' => 1, 'group_class' => 'col-md-3'],
                ['name' => 'status', 'type' => 'select', 'options' =>
                    [
                        0 => 'Chưa thanh toán',
                        1 => 'Chờ lấy hàng',
                        2 => 'Đã lấy hàng',
                        3 => 'Đã giao hàng',
                        4 => 'Nợ COD',
                        5 => 'Đã trả COD',
                        6 => 'Không phát được',
                        7 => 'Đã hủy',
                        8 => 'Đang chuyển hoàn',
                        9 => 'Đã chuyển hoàn',
                        10 => 'Sự cố',
                    ], 'class' => '', 'label' => 'Trạng thái', 'value' => 0, 'group_class' => ''],
                ['name' => 'commodity_value', 'type' => 'textarea', 'class' => '', 'label' => 'Nội dung hàng hóa'],
                ['name' => 'note', 'type' => 'textarea', 'class' => '', 'label' => 'Ghi chú'],

            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, mã vận đơn, nơi gửi, tên người nhận, địa chỉ',
        'fields' => 'id, bill_lading_code, place_submission, recipient_name, address',
    ];

    protected $filter = [
        'bill_lading_code' => [
            'label' => 'Mã vận đơn',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'place_submission' => [
            'label' => 'Nơi gửi',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'recipient_name' => [
            'label' => 'Tên người nhận',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'address' => [
            'label' => 'Địa chì',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'date_creation' => [
            'label' => 'Ngày tạo đơn',
            'type' => 'date',
            'query_type' => 'like'
        ],
        'province_id' => [
            'label' => 'Tỉnh',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'province',
            'model' => Province::class,
            'query_type' => '='
        ],
        'district_id' => [
            'label' => 'Huyện',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'district',
            'model' => District::class,
            'query_type' => '='
        ],
        'ward_id' => [
            'label' => 'Xã',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'ward',
            'model' => Ward::class,
            'query_type' => 'custom'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('logisticsproduct::bill_lading.list')->with($data);
    }

    public function print($id, Request $request)
    {
        $data['page_title'] = 'In báo giá';
        $bill = Bill::findOrFail($id);
        $data['result'] = $bill;
        $data['header_print'] = @$bill->company->header_print;
        $data['footer_print'] = @$bill->company->footer_print;
        return view('logisticsproduct::bill_lading.print')->with($data);
    }

//    public function appendWhere($query, $request)
//    {
//        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
//        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
//            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
//        }
//
//        return $query;
//    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('logisticsproduct::bill_lading.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'bill_lading_code' => 'required'
                ], [
                    'bill_lading_code.required' => 'Bắt buộc phải nhập mã',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;
//                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);


            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('logisticsproduct::bill_lading.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'bill_lading_code' => 'required'
                ], [
                    'bill_lading_code.required' => 'Bắt buộc phải nhập mã',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;


                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

//            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
