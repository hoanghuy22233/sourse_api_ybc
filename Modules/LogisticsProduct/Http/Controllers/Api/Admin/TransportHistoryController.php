<?php

namespace Modules\LogisticsProduct\Http\Controllers\Api\Admin;

use \Modules\A4iSeason\Models\Disease;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\KitCareBooking\Models\Producer;
use Modules\KitCareBooking\Models\Product;
use Modules\LogisticsProduct\Models\TransportHistory;
use Modules\Theme\Models\Post;
use Validator;

class TransportHistoryController extends Controller
{

    protected $module = [
        'code' => 'transport_history',
        'table_name' => 'transport_histories',
        'label' => 'Comment',
        'modal' => 'Modules\LogisticsProduct\Models\TransportHistory',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'transport_id' => [
            'query_type' => '='
        ],
        'created_at' => [
            'query_type' => '='
        ]
    ];


    public function index(Request $request)
    {

        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = TransportHistory::selectRaw('transport_histories.id, transport_histories.name, transport_histories.transport_id, transport_histories.admin_id, transport_histories.created_at, transport_histories.updated_at')
                ->whereRaw($where);


            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            foreach ($listItem as $k => $v) {
//                $v->image = asset('public/filemanager/userfiles/' . $v->image);
                $admin_name = @$v->admin->name;
                unset($v->admin);
                $v->admin = [
                    'id' => @$v->admin_id,
                    'name' => $admin_name,
                ];
                unset($v->admin_id);

                $transport_name = @$v->transport->name;
                unset($v->transport);
                $v->transport = [
                    'id' => @$v->transport_id,
                    'name' => $transport_name,
                ];
                unset($v->transport_id);
            }
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = TransportHistory::selectRaw('transport_histories.id, transport_histories.name, transport_histories.transport_id, transport_histories.admin_id, transport_histories.created_at, transport_histories.updated_at')->where('transport_histories.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }


//            $item->image = asset('public/filemanager/userfiles/' . $item->image);
            $transport_name = @$item->transport->name;
            unset($item->transport);
            $item->transport = [
                'id' => @$item->transport_id,
                'name' => $transport_name,
            ];
            unset($item->transport_id);
            $admin_name = @$item->admin->name;
            unset($item->admin);
            $item->admin = [
                'id' => @$item->admin_id,
                'name' => $admin_name,
            ];
            unset($item->admin_id);

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('api')->id();
//            if ($request->has('image')) {
//                if (is_array($request->file('image'))) {
//                    foreach ($request->file('image') as $image) {
//                        $data['image'] = CommonHelper::saveFile($image, 'disease');
//                    }
//                } else {
//                    $data['image'] = CommonHelper::saveFile($request->file('image'), 'disease');
//                }
//            }
//
//            if ($request->has('disease_image')) {
//                if (is_array($request->file('disease_image'))) {
//                    foreach ($request->file('disease_image') as $image) {
//                        $data['disease_image'] = CommonHelper::saveFile($image, 'disease');
//                    }
//                } else {
//                    $data['disease_image'] = CommonHelper::saveFile($request->file('disease_image'), 'disease');
//                }
//            }

            $item = TransportHistory::create($data);

            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {

        $item = TransportHistory::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

//        if ($request->has('image')) {
//            if (is_array($request->file('image'))) {
//                foreach ($request->file('image') as $image) {
//                    $data['image'] = CommonHelper::saveFile($image, 'disease');
//                }
//            } else {
//                $data['image'] = CommonHelper::saveFile($request->file('image'), 'disease');
//            }
//        }
//        if ($request->has('disease_image')) {
//            if (is_array($request->file('disease_image'))) {
//                foreach ($request->file('disease_image') as $image) {
//                    $data['disease_image'] = CommonHelper::saveFile($image, 'disease');
//                }
//            } else {
//                $data['disease_image'] = CommonHelper::saveFile($request->file('disease_image'), 'disease');
//            }
//        }

        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (TransportHistory::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
