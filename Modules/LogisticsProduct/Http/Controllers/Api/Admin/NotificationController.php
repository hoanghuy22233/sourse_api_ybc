<?php

namespace Modules\KitCareBooking\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\KitCareBooking\Models\Notifications;

class NotificationController extends Controller
{
    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'type' => [
            'query_type' => '='
        ],
        'readed' => [
            'query_type' => 'like',
        ]
    ];

    public function removePlayer($player_ids)
    {
//        $onesignal_app_id = Settings::select(['value'])->where('name', 'onesignal_app_id')->first()->value;
//        $onesignal_app_key = Settings::select(['value'])->where('name', 'onesignal_app_key')->first()->value;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://onesignal.com/api/v1/players/" . $player_ids . "?app_id=" . $onesignal_app_id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic " . $onesignal_app_key,
                "cache-control: no-cache"
            ),
        ));

        curl_exec($curl);
        curl_error($curl);
        curl_close($curl);

        return true;
    }

    public function index(Request $request)
    {
        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Notifications::leftJoin('admin', 'admin.id', '=', 'notifications.from_admin_id')
                ->selectRaw('notifications.*, admin.id as from_id, admin.name as from_name, admin.image as from_image')
                ->where('notifications.to_admin_id', \Auth::guard('api')->id())
                ->whereRaw($where);
            //  Sort
            $listItem = $this->sort($request, $listItem);
            if ($request->has('limit')) {
                $listItem = $listItem->paginate($request->limit);
            } else {
                $listItem = $listItem->paginate(20);
            }
            $listItem = $listItem->appends($request->all());

            foreach ($listItem as $item) {
                $item->from = [
                    'id' => $item->from_id,
                    'name' => $item->from_name,
                    'image' => asset('public/filemanager/userfiles/' . $item->from_image)
                ];
                unset($item->from_id);
                unset($item->from_name);
                unset($item->from_image);
                unset($item->from_admin_id);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }

    public function count_no_readed(Request $request)
    {
        return response()->json([
            'status' => true,
            'msg' => '',
            'errors' => (object)[],
            'data' => Notifications::where('to_admin_id', \Auth::guard('api')->id())->where('readed', 0)->count(),
            'code' => 201
        ]);
    }

    public function read($id)
    {
        $notification = Notifications::where('id', $id)->where('to_admin_id', \Auth::guard('api')->id())->first();
        if (!is_object($notification)) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $notification->update([
            'readed' => 1
        ]);
        return response()->json([
            'status' => true,
            'msg' => 'Đã đọc',
            'errors' => (object)[],
            'data' => [],
            'code' => 201
        ]);
    }

    public function readAll() {
        Notifications::where('to_admin_id', \Auth::guard('api')->id())->update(['readed' => 1]);
        return response()->json([
            'status' => true,
            'msg' => 'Đã đọc tất cả thông báo',
            'errors' => (object)[],
            'data' => [],
            'code' => 201
        ]);
    }
}
