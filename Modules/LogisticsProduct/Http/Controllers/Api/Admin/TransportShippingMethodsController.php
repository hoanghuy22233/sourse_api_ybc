<?php

namespace Modules\LogisticsProduct\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\LogisticsProduct\Models\TransportShippingMethods;
use Validator;

class TransportShippingMethodsController extends Controller
{

    protected $module = [
        'code' => 'transport_shipping_methods',
        'table_name' => 'transport_shipping_methods',
        'label' => 'Phương thức vận chuyển',
        'modal' => 'Modules\LogisticsProduct\Models\TransportShippingMethods',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'transport_type_id' => [
            'query_type' => '='
        ],
        'transport_product_type_id' => [
            'query_type' => '='
        ],
        'type_calculation' => [
            'query_type' => '='
        ],
        'amount_from' => [
            'query_type' => '='
        ],
        'amount_to' => [
            'query_type' => '='
        ],
        'range_from' => [
            'query_type' => '='
        ],
        'range_to' => [
            'query_type' => '='
        ],
        'base_price' => [
            'query_type' => '='
        ],
        'final_price' => [
            'query_type' => '='
        ],
        'final_price_date_from' => [
            'query_type' => '='
        ],
        'final_price_date_to' => [
            'query_type' => '='
        ],
        'order_no' => [
            'query_type' => 'like'
        ],
        'created_at' => [
            'query_type' => '='
        ],
        'updated_at' => [
            'query_type' => '='
        ],
    ];


    public function index(Request $request)
    {

        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = TransportShippingMethods::selectRaw('transport_shipping_methods.*')
                ->whereRaw($where);


            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            foreach ($listItem as $k => $v) {
                $v->image = asset('public/filemanager/userfiles/' . $v->image);


                $transport_type_name = @$v->transport_type->name;
                unset($v->transport_type);
                $v->transport_type = [
                    'id' => @$v->transport_type_id,
                    'name' => $transport_type_name,
                ];
                unset($v->transport_type_id);

                $transport_product_type_name = @$v->transport_product_type->name;
                unset($v->transport_product_type);
                $v->transport_product_type = [
                    'id' => @$v->transport_product_type_id,
                    'name' => $transport_product_type_name,
                ];
                unset($v->transport_product_type_id);
            }
            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = TransportShippingMethods::selectRaw('transport_shipping_methods.*')->where('transport_shipping_methods.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $item->image = asset('public/filemanager/userfiles/' . $item->image);


            $transport_type_name = @$item->transport_type->name;
            unset($item->transport_type);
            $item->transport_type = [
                'id' => @$item->transport_type_id,
                'name' => $transport_type_name,
            ];
            unset($item->transport_type_id);

            $transport_product_type_name = @$item->transport_product_type->name;
            unset($item->transport_product_type);
            $item->transport_product_type = [
                'id' => @$item->transport_product_type_id,
                'name' => $transport_product_type_name,
            ];
            unset($item->transport_type_id);

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = CommonHelper::saveFile($image, 'disease');
                    }
                } else {
                    $data['image'] = CommonHelper::saveFile($request->file('image'), 'disease');
                }
            }

            $item = new TransportShippingMethods();
            unset($data['api_token']);
            foreach ($data as $k => $v) {
                $item->{$k} = $v;
            }

            $item->save();

            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {

        $item = TransportShippingMethods::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = CommonHelper::saveFile($image, 'disease');
                }
            } else {
                $data['image'] = CommonHelper::saveFile($request->file('image'), 'disease');
            }
        }
//        if ($request->has('disease_image')) {
//            if (is_array($request->file('disease_image'))) {
//                foreach ($request->file('disease_image') as $image) {
//                    $data['disease_image'] = CommonHelper::saveFile($image, 'disease');
//                }
//            } else {
//                $data['disease_image'] = CommonHelper::saveFile($request->file('disease_image'), 'disease');
//            }
//        }

        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (TransportShippingMethods::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
