<?php

namespace Modules\LogisticsProduct\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\LogisticsProduct\Models\Transport;
use Modules\LogisticsProduct\Models\Vats;
use Validator;

class DashboardController extends Controller
{

    protected $module = [
        'code' => 'vats',
        'table_name' => 'vats',
        'label' => 'VAT',
        'modal' => 'Modules\LogisticsProduct\Models\Vats',
    ];

    protected $filter = [
        'transport_type_id' => [
            'query_type' => '='
        ],
        'transport_product_type_id' => [
            'query_type' => '='
        ],
    ];


    public function index(Request $request)
    {

        try {

            $whereRaw = '1=1 ';
            $role_name = CommonHelper::getRoleName(\Auth::guard('api')->user()->id, 'name');
            if ($role_name == 'khach_hang') {
                $whereRaw .= 'AND admin_id = ' . \Auth::guard('api')->user()->id;
            }

            $data = [
                'count' => [
                    'da_lay_hang' => number_format(Transport::whereRaw($whereRaw)->whereIn('status', [2])->count()),
                    'da_giao_hang' => number_format(Transport::whereRaw($whereRaw)->whereIn('status', [3])->count()),
                    'khong_phat_duoc' => number_format(Transport::whereRaw($whereRaw)->whereIn('status', [6])->count()),
                    'da_chuyen_hoan' => number_format(Transport::whereRaw($whereRaw)->whereIn('status', [9])->count()),
                    'su_co' => number_format(Transport::whereRaw($whereRaw)->whereIn('status', [10])->count()),
                    'cho_lay_hang' => number_format(Transport::whereRaw($whereRaw)->whereIn('status', [1])->count()),
                ],
                'sum' => [
                    'cod_da_giao_hang' => number_format(Transport::whereRaw($whereRaw)->whereIn('status', [3])->sum('cod')),
                    'cod_khong_phat_duoc' => number_format(Transport::whereRaw($whereRaw)->whereIn('status', [6])->sum('cod')),
                    'shipping_fee' => number_format(Transport::whereRaw($whereRaw)->sum('shipping_fee')),
                ]
            ];

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $data,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = Vats::selectRaw('vats.*')->where('vats.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $transport_type_name = @$item->transport_type->name;
            unset($item->transport_type);
            $item->transport_type = [
                'id' => @$item->transport_type_id,
                'name' => $transport_type_name,
            ];
            unset($item->transport_type_id);

            $transport_product_type_name = @$item->transport_product_type->name;
            unset($item->transport_product_type);
            $item->transport_product_type = [
                'id' => @$item->transport_product_type_id,
                'name' => $transport_product_type_name,
            ];
            unset($item->transport_product_type_id);

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'number' => 'required'
        ], [
            'number.required' => 'Bắt buộc phải nhập số',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert

            $item = new Vats();
            unset($data['api_token']);
            foreach ($data as $k => $v) {
                $item->{$k} = $v;
            }

            $item->save();

            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {

        $item = Vats::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (Vats::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
