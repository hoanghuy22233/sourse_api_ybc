<?php

namespace Modules\LogisticsProduct\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\LogisticsProduct\Models\Product;
use Modules\LogisticsProduct\Models\Transport;
use Modules\Theme\Models\Post;
use Validator;

class TransportController extends Controller
{

    protected $module = [
        'code' => 'transport',
        'table_name' => 'transports',
        'label' => 'Vận đơn',
        'modal' => 'Modules\LogisticsProduct\Models\Transport',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'code' => [
            'query_type' => '='
        ],
        'owned' => [
            'query_type' => '='
        ],
        'billing_address' => [
            'query_type' => '='
        ],
        'shipping_address' => [
            'query_type' => '='
        ],
        'discount_price' => [
            'query_type' => '='
        ],
        'note' => [
            'query_type' => '='
        ],
        'shipping_method_id' => [
            'query_type' => '='
        ],
        'mass' => [
            'query_type' => '='
        ],
        'status' => [
            'query_type' => '='
        ],
        'status_id' => [
            'query_type' => '='
        ],
        'payment_methods' => [
            'query_type' => '='
        ],
        'created_at' => [
            'query_type' => 'from_to_date'
        ],

    ];
    protected $type_owned = [
        1 => 'Etal group',
        2 => 'Shop',

    ];
    protected $type_quantitys = [
        0 => 'Trọng lượng',
        1 => 'Số kiện',

    ];
    protected $type_status = [
        0 => 'Chưa thanh toán',
        1 => 'Chờ lấy hàng',
        2 => 'Đã lấy hàng',
        3 => 'Đã giao hàng',
        4 => 'Nợ COD',
        5 => 'Đã trả COD',
        6 => 'Không phát được',
        7 => 'Đã hủy',
        8 => 'Đang chuyển hoàn',
        9 => 'Đã chuyển hoàn',
        10 => 'Sự cố',

    ];

    public function index(Request $request)
    {
        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Transport::selectRaw('transports.*')
                ->whereRaw($where);


            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem2 = $listItem;
            $listItem = $listItem->paginate($limit)->appends($request->all());

            foreach ($listItem as $k => $v) {
                $v->image = asset('public/filemanager/userfiles/' . $v->image);

                $v->owned = array_key_exists($v->owned, $this->type_owned) ? $this->type_owned[$v->owned] : '';
                $v->status_code = $v->status;
                $v->status = array_key_exists($v->status, $this->type_status) ? $this->type_status[$v->status] : '';
                $v->type_quantity = array_key_exists($v->type_quantity, $this->type_quantitys) ? $this->type_quantitys[$v->type_quantity] : '';
                $v->transports_status = [
                    'id' => $v->status_id,
                    'name' => @$v->statusObj->name
                ];

                $transport_shipping_method_name = @$v->transport_shipping_method->name;
                unset($v->transport_shipping_method);
                $v->transport_shipping_method = [
                    'id' => @$v->transport_shipping_method_id,
                    'name' => $transport_shipping_method_name,
                ];
                unset($v->transport_shipping_method_id);

                $transport_product_type_name = @$v->transport_product_type->name;
                unset($v->transport_product_type);
                $v->transport_product_type = [
                    'id' => @$v->transport_product_type_id,
                    'name' => $transport_product_type_name,
                ];
                unset($v->transport_product_type_id);


                $transport_type_name = @$v->transport_type->name;
                unset($v->transport_type);
                $v->transport_type = [
                    'id' => @$v->transport_type_id,
                    'name' => $transport_type_name,
                ];
                unset($v->transport_type_id);


                $admin_name = @$v->admin->name;
                unset($v->admin);
                $v->admin = [
                    'id' => @$v->admin_id,
                    'name' => $admin_name,
                ];
                unset($v->admin_id);

                $shipping_ward_name = @$v->shipping_ward->name;
                unset($v->shipping_ward);
                $v->shipping_ward = [
                    'id' => @$v->shipping_ward_id,
                    'name' => $shipping_ward_name,
                ];
                $billing_ward_name = @$v->billing_ward->name;
                unset($v->billing_ward);
                $v->billing_ward = [
                    'id' => @$v->billing_ward_id,
                    'name' => $billing_ward_name,
                ];
                unset($v->billing_ward_id);


                $shipping_method_name = @$v->shipping_method->name;
                $shipping_method_price = @$v->shipping_method->price;
                unset($v->shipping_method);
                $v->shipping_method = [
                    'id' => @$v->shipping_method_id,
                    'name' => $shipping_method_name,
                    'price' => $shipping_method_price,
                ];
                unset($v->shipping_method_id);
                unset($v->shipping_province_id);
                unset($v->shipping_district_id);
                unset($v->billing_province_id);
                unset($v->billing_district_id);

            }

            $cod_real = $listItem2->whereIn('status', [2, 3, 4])->sum('cod'); //  đã lấy hàng, đã giao hàng, nợ cod
            $paid = $listItem2->whereIn('status', [5])->sum('cod'); //  đã trả COD

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'dashboard' => [
                    'cod' => $listItem2->sum('cod'),
                    'cod_real' => $cod_real,
                    'paid' => $paid,
                    'missing' => $cod_real - $paid
                ],
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = Transport::selectRaw('transports.*')->where('transports.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }
            $item->image = asset('public/filemanager/userfiles/' . $item->image);


            $item->owned = array_key_exists($item->owned, $this->type_owned) ? $this->type_owned[$item->owned] : '';
            $item->status = array_key_exists($item->status, $this->type_status) ? $this->type_status[$item->status] : '';


            $transport_shipping_method_name = @$item->transport_shipping_method->name;
            unset($item->transport_shipping_method);
            $item->transport_shipping_method = [
                'id' => @$item->transport_shipping_method_id,
                'name' => $transport_shipping_method_name,
            ];
            unset($item->transport_shipping_method_id);

            $transport_product_type_name = @$item->transport_product_type->name;
            unset($item->transport_product_type);
            $item->transport_product_type = [
                'id' => @$item->transport_product_type_id,
                'name' => $transport_product_type_name,
            ];
            unset($item->transport_product_type_id);


            $transport_type_name = @$item->transport_type->name;
            unset($item->transport_type);
            $item->transport_type = [
                'id' => @$item->transport_type_id,
                'name' => $transport_type_name,
            ];
            unset($item->transport_type_id);


            $admin_name = @$item->admin->name;
            unset($item->admin);
            $item->admin = [
                'id' => @$item->admin_id,
                'name' => $admin_name,
            ];
            unset($item->admin_id);

            $shipping_ward_name = @$item->shipping_ward->name;
            unset($item->shipping_ward);
            $item->shipping_ward = [
                'id' => @$item->shipping_ward_id,
                'name' => $shipping_ward_name,
            ];
            $billing_ward_name = @$item->billing_ward->name;
            unset($item->billing_ward);
            $item->billing_ward = [
                'id' => @$item->billing_ward_id,
                'name' => $billing_ward_name,
            ];
            unset($item->billing_ward_id);


            $shipping_method_name = @$item->shipping_method->name;
            $shipping_method_price = @$item->shipping_method->price;
            unset($item->shipping_method);
            $item->shipping_method = [
                'id' => @$item->shipping_method_id,
                'name' => $shipping_method_name,
                'price' => $shipping_method_price,
            ];
            unset($item->shipping_method_id);

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'code' => 'required'
        ], [
            'code.required' => 'Bắt buộc phải nhập mã đơn hàng',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();

            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('admin_api')->id();
            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = CommonHelper::saveFile($image, 'transport');
                    }
                } else {
                    $data['image'] = CommonHelper::saveFile($request->file('image'), 'transport');
                }
            }


            $item = Transport::create($data);

            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {

        $item = Transport::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Validate errors',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = CommonHelper::saveFile($image, 'transport');
                }
            } else {
                $data['image'] = CommonHelper::saveFile($request->file('image'), 'transport');
            }
        }


        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (Transport::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
            if ($filter_option['query_type'] == 'from_to_date') {
                if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                }
                if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }

}
