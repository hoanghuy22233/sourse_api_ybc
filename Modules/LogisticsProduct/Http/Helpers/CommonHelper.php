<?php

namespace Modules\LogisticsProduct\Http\Helpers;

use Modules\LogisticsProduct\Models\PropertieValue;
use View;
use Session;
use Modules\ThemeWorkart\Models\Meta;

class CommonHelper
{

    /*
     * Lấy danh sách quà tặng của sản phẩm
     * */
    public static function getAttributesProduct($product_id)
    {
        $product = \Modules\LogisticsProduct\Models\Product::find($product_id);
        if (!is_object($product)) {
            return [];
        }

        $attribute_values = PropertieValue::leftJoin('properties_name', 'properties_name.id', '=', 'properties_value.properties_name_id')
            ->selectRaw('properties_value.value, properties_value.description, properties_value.order_no, properties_value.link, properties_value.status, properties_name.name, properties_name.price_option')
            ->whereIn('properties_value.id', explode('|', $product->proprerties_id))->get()->toArray();
        return $attribute_values;
    }


}