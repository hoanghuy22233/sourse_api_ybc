<div class="modal fade" id="popup-add-sub-partner" role="dialog" style="z-index: 100000">
    <form id="form-sub-partner" action="post" autocomplete="off">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content " style="height:auto">
                <div class="modal-header">
                    <h4 style="display:inline-block;" class="title-sub-partner">Chi tiết vận đơn</h4>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <div class="box-info box-sd">
                        <h4 class="box-title">Thông tin hàng hóa</h4>
                        <div class="box-input">
                                <span>
                        <label for="">Tên hàng</label>

                        <input name="tenhang" type="text" class="input__control check_require" readonly>
                    </span>
                            <span>
                        <label for="">Mã hàng</label>
                        <div class="dropdown bootstrap-select form-control change_city border-cam">
                            <div class="dropdown bootstrap-select form-control mahang border-cam"><select name="mahang"
                                                                                                          class="selectpicker form-control mahang border-cam"
                                                                                                          id="mahang"
                                                                                                          data-id="mahang"
                                                                                                          data-live-search="true"
                                                                                                          data-size="8"
                                                                                                          tabindex="-98">
                               <option selected="" value="1">Tài liệu</option><option
                                            value="2">Thời trang Mỹ phẩm</option><option value="3">Đồ điện tử</option><option
                                            value="4">Dược phẩm</option><option value="5">Thực phẩm</option><option
                                            value="6">Đồ tươi sống</option><option value="7">Đồ có pin</option><option
                                            value="8">Chất bột nước</option><option value="9">Chất dễ cháy</option>
                            </select><button type="button" class="btn dropdown-toggle btn-light" data-toggle="dropdown"
                                             role="button" data-id="mahang" title="Thực phẩm"><div
                                            class="filter-option"><div class="filter-option-inner"><div
                                                    class="filter-option-inner-inner">Thực phẩm</div></div> </div></button><div
                                        class="dropdown-menu " role="combobox"
                                        style="max-height: 304px; overflow: hidden; min-width: 183px;"><div
                                            class="bs-searchbox"><input type="text" class="form-control"
                                                                        autocomplete="off" role="textbox"
                                                                        aria-label="Search"></div><div
                                            class="inner show" role="listbox" aria-expanded="false" tabindex="-1"
                                            style="max-height: 240px; overflow-y: auto;"><ul
                                                class="dropdown-menu inner show"><li><a role="option"
                                                                                        class="dropdown-item"
                                                                                        aria-disabled="false"
                                                                                        tabindex="0"
                                                                                        aria-selected="false"><span
                                                            class=" bs-ok-default check-mark"></span><span class="text">Tài liệu</span></a></li><li><a
                                                        role="option" class="dropdown-item" aria-disabled="false"
                                                        tabindex="0" aria-selected="false"><span
                                                            class=" bs-ok-default check-mark"></span><span class="text">Thời trang Mỹ phẩm</span></a></li><li><a
                                                        role="option" class="dropdown-item" aria-disabled="false"
                                                        tabindex="0" aria-selected="false"><span
                                                            class=" bs-ok-default check-mark"></span><span class="text">Đồ điện tử</span></a></li><li><a
                                                        role="option" class="dropdown-item" aria-disabled="false"
                                                        tabindex="0" aria-selected="false"><span
                                                            class=" bs-ok-default check-mark"></span><span class="text">Dược phẩm</span></a></li><li
                                                    class="selected active"><a role="option"
                                                                               class="dropdown-item selected active"
                                                                               aria-disabled="false" tabindex="0"
                                                                               aria-selected="true"><span
                                                            class=" bs-ok-default check-mark"></span><span class="text">Thực phẩm</span></a></li><li><a
                                                        role="option" class="dropdown-item" aria-disabled="false"
                                                        tabindex="0" aria-selected="false"><span
                                                            class=" bs-ok-default check-mark"></span><span class="text">Đồ tươi sống</span></a></li><li><a
                                                        role="option" class="dropdown-item" aria-disabled="false"
                                                        tabindex="0" aria-selected="false"><span
                                                            class=" bs-ok-default check-mark"></span><span class="text">Đồ có pin</span></a></li><li><a
                                                        role="option" class="dropdown-item" aria-disabled="false"
                                                        tabindex="0" aria-selected="false"><span
                                                            class=" bs-ok-default check-mark"></span><span class="text">Chất bột nước</span></a></li><li><a
                                                        role="option" class="dropdown-item" aria-disabled="false"
                                                        tabindex="0" aria-selected="false"><span
                                                            class=" bs-ok-default check-mark"></span><span class="text">Chất dễ cháy</span></a></li></ul></div></div></div>
                        </div>
                    </span>
                            <span>
                        <label for="">Trọng lượng(kg):</label>
                        <input name="weight" type="number" class="check_require input__control" placeholder="kg">
                    </span>
                            <span>
                        <label for="">Hình ảnh</label>
                        <div class="file">
                            <input name="picture" type="file" class="file-choose">
                            <div class="file-btn btn-m">File</div>
                        </div>
                            <div id="preview_img"></div>
                    </span>
                        </div>
                    </div>
                    <div class="box-address box-sd">
                        <div action="" class="shipping-address address-form mg-r">
                            <h4 class="box-title">2. Shipping address</h4>
                            <span class="form__control">
                        <div class="control-top">
                            <input type="text" name="shipping_hoten" placeholder="Họ và tên"
                                   class="input__control check_require">
                            <input type="text" name="shipping_sdt" placeholder="SĐT"
                                   class="input__control check_require">
                        </div>
                        <input type="text" name="shipping_diachi" placeholder="Địa chỉ: "
                               class="check_require input__control">
                        <div class="control-body">

                             <div class="dropdown bootstrap-select form-control change_city border-cam">
                                            <div class="dropdown bootstrap-select form-control change_city border-cam"><select
                                                        name="send_city_id"
                                                        class="selectpicker form-control change_city border-cam"
                                                        id="send_city_id" data-id="send_district_id"
                                                        data-live-search="true" data-size="10" tabindex="-98">
                                            <option value="0">Tỉnh/TP</option><option selected=""
                                                                                      value="1">Hồ Chí Minh</option><option
                                                            value="2">Hà Nội</option><option value="3">Đà Nẵng</option><option
                                                            value="4">Bình Dương</option><option
                                                            value="5">Đồng Nai</option><option
                                                            value="6">Khánh Hòa</option><option
                                                            value="7">Hải Phòng</option><option
                                                            value="8">Long An</option><option
                                                            value="9">Quảng Nam</option><option value="10">Bà Rịa Vũng Tàu</option><option
                                                            value="11">Đắk Lắk</option><option
                                                            value="12">Cần Thơ</option><option
                                                            value="13">Bình Thuận  </option><option
                                                            value="14">Lâm Đồng</option><option value="15">Thừa Thiên Huế</option><option
                                                            value="16">Kiên Giang</option><option
                                                            value="17">Bắc Ninh</option><option
                                                            value="18">Quảng Ninh</option><option
                                                            value="19">Thanh Hóa</option><option
                                                            value="20">Nghệ An</option><option
                                                            value="21">Hải Dương</option><option
                                                            value="22">Gia Lai</option><option
                                                            value="23">Bình Phước</option><option
                                                            value="24">Hưng Yên</option><option
                                                            value="25">Bình Định</option><option
                                                            value="26">Tiền Giang</option><option
                                                            value="27">Thái Bình</option><option
                                                            value="28">Bắc Giang</option><option
                                                            value="29">Hòa Bình</option><option
                                                            value="30">An Giang</option><option
                                                            value="31">Vĩnh Phúc</option><option
                                                            value="32">Tây Ninh</option><option
                                                            value="33">Thái Nguyên</option><option
                                                            value="34">Lào Cai</option><option
                                                            value="35">Nam Định</option><option
                                                            value="36">Quảng Ngãi</option><option
                                                            value="37">Bến Tre</option><option
                                                            value="38">Đắk Nông</option><option
                                                            value="39">Cà Mau</option><option
                                                            value="40">Vĩnh Long</option><option
                                                            value="41">Ninh Bình</option><option
                                                            value="42">Phú Thọ</option><option
                                                            value="43">Ninh Thuận</option><option
                                                            value="44">Phú Yên</option><option
                                                            value="45">Hà Nam</option><option
                                                            value="46">Hà Tĩnh</option><option
                                                            value="47">Đồng Tháp</option><option
                                                            value="48">Sóc Trăng</option><option
                                                            value="49">Kon Tum</option><option
                                                            value="50">Quảng Bình</option><option
                                                            value="51">Quảng Trị</option><option
                                                            value="52">Trà Vinh</option><option
                                                            value="53">Hậu Giang</option><option
                                                            value="54">Sơn La</option><option
                                                            value="55">Bạc Liêu</option><option
                                                            value="56">Yên Bái</option><option
                                                            value="57">Tuyên Quang</option><option
                                                            value="58">Điện Biên</option><option
                                                            value="59">Lai Châu</option><option
                                                            value="60">Lạng Sơn</option><option
                                                            value="61">Hà Giang</option><option
                                                            value="62">Bắc Kạn</option><option
                                                            value="63">Cao Bằng</option><option value="64">Tỉnh thành khác</option>
                                            </select><button type="button" class="btn dropdown-toggle btn-light"
                                                             data-toggle="dropdown" role="button" data-id="send_city_id"
                                                             title="Hồ Chí Minh"><div class="filter-option"><div
                                                                class="filter-option-inner"><div
                                                                    class="filter-option-inner-inner">Hồ Chí Minh</div></div> </div></button><div
                                                        class="dropdown-menu " role="combobox"
                                                        style="max-height: 364px; overflow: hidden; min-width: 159px;"><div
                                                            class="bs-searchbox"><input type="text" class="form-control"
                                                                                        autocomplete="off"
                                                                                        role="textbox"
                                                                                        aria-label="Search"></div><div
                                                            class="inner show" role="listbox" aria-expanded="false"
                                                            tabindex="-1" style="max-height: 300px; overflow-y: auto;"><ul
                                                                class="dropdown-menu inner show"><li><a role="option"
                                                                                                        class="dropdown-item"
                                                                                                        aria-disabled="false"
                                                                                                        tabindex="0"
                                                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Tỉnh/TP</span></a></li><li
                                                                    class="selected active"><a role="option"
                                                                                               class="dropdown-item selected active"
                                                                                               aria-disabled="false"
                                                                                               tabindex="0"
                                                                                               aria-selected="true"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hồ Chí Minh</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hà Nội</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Đà Nẵng</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bình Dương</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Đồng Nai</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Khánh Hòa</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hải Phòng</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Long An</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quảng Nam</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bà Rịa Vũng Tàu</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Đắk Lắk</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Cần Thơ</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bình Thuận  </span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Lâm Đồng</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Thừa Thiên Huế</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Kiên Giang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bắc Ninh</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quảng Ninh</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Thanh Hóa</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Nghệ An</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hải Dương</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Gia Lai</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bình Phước</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hưng Yên</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bình Định</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Tiền Giang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Thái Bình</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bắc Giang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hòa Bình</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">An Giang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Vĩnh Phúc</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Tây Ninh</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Thái Nguyên</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Lào Cai</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Nam Định</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quảng Ngãi</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bến Tre</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Đắk Nông</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Cà Mau</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Vĩnh Long</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Ninh Bình</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Phú Thọ</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Ninh Thuận</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Phú Yên</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hà Nam</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hà Tĩnh</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Đồng Tháp</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Sóc Trăng</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Kon Tum</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quảng Bình</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quảng Trị</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Trà Vinh</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hậu Giang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Sơn La</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bạc Liêu</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Yên Bái</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Tuyên Quang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Điện Biên</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Lai Châu</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Lạng Sơn</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hà Giang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bắc Kạn</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Cao Bằng</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Tỉnh thành khác</span></a></li></ul></div></div></div>
                                        </div>
                            <div class="dropdown bootstrap-select form-control change_city border-cam">
                                            <div class="dropdown bootstrap-select form-control border-cam"><select
                                                        name="send_district_id"
                                                        class="selectpicker form-control border-cam"
                                                        id="send_district_id" data-id="send_district_id"
                                                        data-live-search="true" data-size="10" tabindex="-98"><option
                                                            selected="" value="0">Quận/Huyện</option><option value="1">Bình Chánh</option><option
                                                            value="2">Bình Tân</option><option
                                                            value="3">Bình Thạnh</option><option
                                                            value="4">Cần Giờ</option><option value="5">Củ Chi</option><option
                                                            value="6">Gò Vấp</option><option value="7">Hóc Môn</option><option
                                                            value="8">Nhà Bè</option><option
                                                            value="9">Phú Nhuận</option><option
                                                            value="10">Quận 1</option><option
                                                            value="11">Quận 10</option><option
                                                            value="12">Quận 11</option><option
                                                            value="13">Quận 12</option><option
                                                            value="14">Quận 2</option><option value="15">Quận 3</option><option
                                                            value="16">Quận 4</option><option value="17">Quận 5</option><option
                                                            value="18">Quận 6</option><option value="19">Quận 7</option><option
                                                            value="20">Quận 8</option><option value="21">Quận 9</option><option
                                                            value="22">Tân Bình</option><option
                                                            value="23">Tân Phú</option><option
                                                            value="24">Thủ Đức</option></select><button type="button"
                                                                                                        class="btn dropdown-toggle btn-light"
                                                                                                        data-toggle="dropdown"
                                                                                                        role="button"
                                                                                                        data-id="send_district_id"
                                                                                                        title="Bình Tân"><div
                                                            class="filter-option"><div class="filter-option-inner"><div
                                                                    class="filter-option-inner-inner">Bình Tân</div></div> </div></button><div
                                                        class="dropdown-menu " role="combobox"
                                                        style="max-height: 364px; overflow: hidden; min-width: 138px;"><div
                                                            class="bs-searchbox"><input type="text" class="form-control"
                                                                                        autocomplete="off"
                                                                                        role="textbox"
                                                                                        aria-label="Search"></div><div
                                                            class="inner show" role="listbox" aria-expanded="false"
                                                            tabindex="-1" style="max-height: 300px; overflow-y: auto;"><ul
                                                                class="dropdown-menu inner show"><li><a role="option"
                                                                                                        class="dropdown-item"
                                                                                                        aria-disabled="false"
                                                                                                        tabindex="0"
                                                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận/Huyện</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bình Chánh</span></a></li><li
                                                                    class="selected active"><a role="option"
                                                                                               class="dropdown-item selected active"
                                                                                               aria-disabled="false"
                                                                                               tabindex="0"
                                                                                               aria-selected="true"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bình Tân</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bình Thạnh</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Cần Giờ</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Củ Chi</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Gò Vấp</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hóc Môn</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Nhà Bè</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Phú Nhuận</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận 1</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận 10</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận 11</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận 12</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận 2</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận 3</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận 4</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận 5</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận 6</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận 7</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận 8</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận 9</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Tân Bình</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Tân Phú</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Thủ Đức</span></a></li></ul></div></div></div>
                                        </div>
                        </div>
                        <input type="email" name="shipping_email" placeholder="Email "
                               class="input__control check_require">
                    </span>
                        </div>

                        <div action="" class="billing-address address-form">
                            <h4 class="box-title">2. Billing address</h4>
                            <span class="form__control">
                        <div class="control-top">
                            <input type="text" name="billing_hoten" placeholder="Họ và tên"
                                   class="input__control check_require">
                            <input type="text" name="billing_sdt" placeholder="SĐT"
                                   class="input__control check_require">
                        </div>
                        <input type="text" name="billing_diachi" placeholder="Địa chỉ: "
                               class="input__control check_require">
                        <div class="control-body">
                            <div class="dropdown bootstrap-select form-control change_city border-cam">
                                            <div class="dropdown bootstrap-select form-control change_city border-cam"><select
                                                        name="recieve_city_id"
                                                        class="selectpicker form-control change_city border-cam"
                                                        id="recieve_city_id" data-id="recieve_city_id"
                                                        data-live-search="true" data-size="10" tabindex="-98">
                                                <option value="0">Tỉnh/TP</option><option selected="" value="1">Hồ Chí Minh</option><option
                                                            value="2">Hà Nội</option><option value="3">Đà Nẵng</option><option
                                                            value="4">Bình Dương</option><option
                                                            value="5">Đồng Nai</option><option
                                                            value="6">Khánh Hòa</option><option
                                                            value="7">Hải Phòng</option><option
                                                            value="8">Long An</option><option
                                                            value="9">Quảng Nam</option><option value="10">Bà Rịa Vũng Tàu</option><option
                                                            value="11">Đắk Lắk</option><option
                                                            value="12">Cần Thơ</option><option
                                                            value="13">Bình Thuận  </option><option
                                                            value="14">Lâm Đồng</option><option value="15">Thừa Thiên Huế</option><option
                                                            value="16">Kiên Giang</option><option
                                                            value="17">Bắc Ninh</option><option
                                                            value="18">Quảng Ninh</option><option
                                                            value="19">Thanh Hóa</option><option
                                                            value="20">Nghệ An</option><option
                                                            value="21">Hải Dương</option><option
                                                            value="22">Gia Lai</option><option
                                                            value="23">Bình Phước</option><option
                                                            value="24">Hưng Yên</option><option
                                                            value="25">Bình Định</option><option
                                                            value="26">Tiền Giang</option><option
                                                            value="27">Thái Bình</option><option
                                                            value="28">Bắc Giang</option><option
                                                            value="29">Hòa Bình</option><option
                                                            value="30">An Giang</option><option
                                                            value="31">Vĩnh Phúc</option><option
                                                            value="32">Tây Ninh</option><option
                                                            value="33">Thái Nguyên</option><option
                                                            value="34">Lào Cai</option><option
                                                            value="35">Nam Định</option><option
                                                            value="36">Quảng Ngãi</option><option
                                                            value="37">Bến Tre</option><option
                                                            value="38">Đắk Nông</option><option
                                                            value="39">Cà Mau</option><option
                                                            value="40">Vĩnh Long</option><option
                                                            value="41">Ninh Bình</option><option
                                                            value="42">Phú Thọ</option><option
                                                            value="43">Ninh Thuận</option><option
                                                            value="44">Phú Yên</option><option
                                                            value="45">Hà Nam</option><option
                                                            value="46">Hà Tĩnh</option><option
                                                            value="47">Đồng Tháp</option><option
                                                            value="48">Sóc Trăng</option><option
                                                            value="49">Kon Tum</option><option
                                                            value="50">Quảng Bình</option><option
                                                            value="51">Quảng Trị</option><option
                                                            value="52">Trà Vinh</option><option
                                                            value="53">Hậu Giang</option><option
                                                            value="54">Sơn La</option><option
                                                            value="55">Bạc Liêu</option><option
                                                            value="56">Yên Bái</option><option
                                                            value="57">Tuyên Quang</option><option
                                                            value="58">Điện Biên</option><option
                                                            value="59">Lai Châu</option><option
                                                            value="60">Lạng Sơn</option><option
                                                            value="61">Hà Giang</option><option
                                                            value="62">Bắc Kạn</option><option
                                                            value="63">Cao Bằng</option><option value="64">Tỉnh thành khác</option>
                                            </select><button type="button" class="btn dropdown-toggle btn-light"
                                                             data-toggle="dropdown" role="button"
                                                             data-id="recieve_city_id" title="Đà Nẵng"><div
                                                            class="filter-option"><div class="filter-option-inner"><div
                                                                    class="filter-option-inner-inner">Đà Nẵng</div></div> </div></button><div
                                                        class="dropdown-menu " role="combobox"
                                                        style="max-height: 364px; overflow: hidden; min-width: 159px;"><div
                                                            class="bs-searchbox"><input type="text" class="form-control"
                                                                                        autocomplete="off"
                                                                                        role="textbox"
                                                                                        aria-label="Search"></div><div
                                                            class="inner show" role="listbox" aria-expanded="false"
                                                            tabindex="-1" style="max-height: 300px; overflow-y: auto;"><ul
                                                                class="dropdown-menu inner show"><li><a role="option"
                                                                                                        class="dropdown-item"
                                                                                                        aria-disabled="false"
                                                                                                        tabindex="0"
                                                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Tỉnh/TP</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hồ Chí Minh</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hà Nội</span></a></li><li
                                                                    class="selected active"><a role="option"
                                                                                               class="dropdown-item selected active"
                                                                                               aria-disabled="false"
                                                                                               tabindex="0"
                                                                                               aria-selected="true"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Đà Nẵng</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bình Dương</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Đồng Nai</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Khánh Hòa</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hải Phòng</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Long An</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quảng Nam</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bà Rịa Vũng Tàu</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Đắk Lắk</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Cần Thơ</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bình Thuận  </span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Lâm Đồng</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Thừa Thiên Huế</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Kiên Giang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bắc Ninh</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quảng Ninh</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Thanh Hóa</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Nghệ An</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hải Dương</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Gia Lai</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bình Phước</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hưng Yên</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bình Định</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Tiền Giang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Thái Bình</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bắc Giang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hòa Bình</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">An Giang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Vĩnh Phúc</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Tây Ninh</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Thái Nguyên</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Lào Cai</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Nam Định</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quảng Ngãi</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bến Tre</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Đắk Nông</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Cà Mau</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Vĩnh Long</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Ninh Bình</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Phú Thọ</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Ninh Thuận</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Phú Yên</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hà Nam</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hà Tĩnh</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Đồng Tháp</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Sóc Trăng</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Kon Tum</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quảng Bình</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quảng Trị</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Trà Vinh</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hậu Giang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Sơn La</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bạc Liêu</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Yên Bái</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Tuyên Quang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Điện Biên</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Lai Châu</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Lạng Sơn</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hà Giang</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Bắc Kạn</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Cao Bằng</span></a></li><li><a
                                                                        role="option" class="dropdown-item"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Tỉnh thành khác</span></a></li></ul></div></div></div>
                                        </div>
                            <div class="dropdown bootstrap-select form-control change_city border-cam">
                                            <div class="dropdown bootstrap-select form-control border-cam"><select
                                                        name="recieve_district_id"
                                                        class="selectpicker form-control border-cam"
                                                        id="recieve_district_id" data-id="recieve_district_id"
                                                        data-live-search="true" data-size="10" tabindex="-98"><option
                                                            selected="" value="0">Quận/Huyện</option><option value="55">Cẩm Lệ</option><option
                                                            value="56">Hải Châu</option><option
                                                            value="57">Hòa Vang</option><option
                                                            value="58">Hoàng Sa</option><option
                                                            value="59">Liên Chiểu</option><option value="60">Ngũ Hành Sơn</option><option
                                                            value="61">Sơn Trà</option><option
                                                            value="62">Thanh Khê</option></select><button type="button"
                                                                                                          class="btn dropdown-toggle btn-light"
                                                                                                          data-toggle="dropdown"
                                                                                                          role="button"
                                                                                                          data-id="recieve_district_id"
                                                                                                          title="Hoàng Sa"><div
                                                            class="filter-option"><div class="filter-option-inner"><div
                                                                    class="filter-option-inner-inner">Hoàng Sa</div></div> </div></button><div
                                                        class="dropdown-menu " role="combobox"
                                                        style="max-height: 364px; overflow: hidden;"><div
                                                            class="bs-searchbox"><input type="text" class="form-control"
                                                                                        autocomplete="off"
                                                                                        role="textbox"
                                                                                        aria-label="Search"></div><div
                                                            class="inner show" role="listbox" aria-expanded="false"
                                                            tabindex="-1" style="max-height: 300px; overflow-y: auto;"><ul
                                                                class="dropdown-menu inner show"><li><a role="option"
                                                                                                        class="dropdown-item"
                                                                                                        aria-disabled="false"
                                                                                                        tabindex="0"
                                                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Quận/Huyện</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Cẩm Lệ</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hải Châu</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hòa Vang</span></a></li><li
                                                                    class="selected active"><a role="option"
                                                                                               class="dropdown-item selected active"
                                                                                               aria-disabled="false"
                                                                                               tabindex="0"
                                                                                               aria-selected="true"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Hoàng Sa</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Liên Chiểu</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Ngũ Hành Sơn</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Sơn Trà</span></a></li><li><a
                                                                        role="option" class="dropdown-item"
                                                                        aria-disabled="false" tabindex="0"
                                                                        aria-selected="false"><span
                                                                            class=" bs-ok-default check-mark"></span><span
                                                                            class="text">Thanh Khê</span></a></li></ul></div></div></div>
                                        </div>
                        </div>
                        <input type="email" name="billing_email" placeholder="Email "
                               class="input__control check_require">
                    </span>
                        </div>
                    </div>
                    <div class="box-voucher box-sd">
                        <h4 class="box-title">3. Mã giảm giá</h4>
                        <div class="box-input">
                            <input name="coupon" type="text" placeholder="Nhập mã giảm giá" class="input__control">
                            <button class="v-btn">
                                <a href="javascript:checkCoupon()" class="b-btn">Nhập mã</a>
                            </button>
                            <p style="margin-bottom:0px" class="status-coupon"></p>
                        </div>
                    </div>
                    <div class="box-comment box-sd">
                        <h4 class="box-title">4. Ghi chú cho đơn hàng</h4>
                        <textarea name="ghichu" rows="3" class="input__control"></textarea>
                    </div>
                    <div class="box-paypal box-sd">
                        <h4 class="box-title">5. Phương thức thanh toán</h4>
                        <div action="">
                    <span>
                        <input value="1" type="radio" name="paypal" id="paypal-online">
                        <label for="paypal-online">Thanh toán online</label>
                    </span>
                            <span>
                        <input value="2" type="radio" name="paypal" id="paypal-money">
                        <label for="paypal-money">Thanh toán bằng tiền mặt</label>
                    </span>
                        </div>
                        <div class="paypal-img">
                            <img src="./images/img/paypal.jpg" alt="">
                        </div>
                    </div>

                    <div class="box-total box-sd">
                        <div class="box-title">6. Thành tiền ( chưa VAT )</div>
                        <div class="total-price b-p">
                            <p>- Tổng tiền</p>
                            <span id="totalPrice">0</span>
                        </div>
                        <div id="box_coupon"></div>
                        <div class="vat-price b-p">
                            <p>- 10% VAT</p>
                            <span id="vat">0</span>
                        </div>

                        <div class="done-price b-p">
                            <p>Thành tiền</p>
                            <span id="totalPriceFinal">0</span>
                        </div>
                    </div>
                    <button class="box-footer" type="submit">
                        Vận đơn
                    </button>
                </div>
                <div class="modal-footer">
                    {{--<button class="btn btn-default" data-dismiss="modal"></button>--}}
                    <button class="btn btn-success" type="submit"><i class="icon-header fa fa-check-circle"></i> Hoàn
                        thành
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
<style>
    .modal .modal-dialog .modal-content .modal-body .box-address {
        display: flex;
        flex-wrap: wrap;
        border: 1px solid #ccc;
        border-radius: 5px;
        margin-bottom: 15px;
    }


    .modal .modal-dialog .modal-content .box-voucher, .modal .modal-dialog .modal-content .box-comment, .modal .modal-dialog .modal-content .box-paypal, .modal .modal-dialog .modal-content .box-total, .modal .modal-dialog .modal-content .box-info {
        padding: 10px 20px;
        border-radius: 5px;
        border: 1px solid #ccc;
        margin-bottom: 15px;
    }

    .box-sd {
        box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.4);
    }

    .box-title {
        font-size: 18px;
        font-weight: 600;
        color: #000000fa;
        margin-bottom: 15px;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span label {
        color: #000;
        font-size: 14px;
        margin-right: 10px;
        margin-bottom: 0;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span {
        display: flex;
        align-items: center;
        margin-bottom: 10px;
    }

    .modal .modal-dialog .modal-content .modal-body .form__control input, textarea {
        border: none;
        outline: none;
        resize: none;
    }

    .input__control {
        border-radius: 5px;
        width: 100% !important;
        border: 1px solid #c7c7c7 !important;
        margin-bottom: 8px;
        padding: 5px;
        font-size: 14px;
    }

    input {
        border: none;
        outline: none;
    }

    button, input {
        overflow: visible;
    }

    input, button, select, optgroup, textarea {
        margin: 0;
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
    }

    label {
        display: inline-block;
        margin-bottom: 0.5rem;
        color: #fff;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span .input__control {
        width: 80% !important;
        padding: 5px;
        margin-bottom: 0;
    }

    .input__control {
        border-radius: 5px;
        width: 100% !important;
        border: 1px solid #c7c7c7 !important;
        margin-bottom: 8px;
        padding: 5px;
        font-size: 14px;
    }

    input {
        border: none;
        outline: none;
    }

    .bootstrap-select .dropdown-toggle .filter-option-inner-inner {
        overflow: hidden;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address .address-form.mg-r {
        margin-right: 15px;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address .address-form {
        flex-basis: 48%;
        padding: 10px 15px;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address .shipping-address {
        position: relative;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address .address-form .form__control .control-top, .modal .modal-dialog .modal-content .modal-body .control-body .input__control {
        display: flex;
    }
</style>
<script>
    $('body').on('click', '.history_transport_detail', function () {
        let modal = $('#popup-add-sub-partner');

        modal.modal();
    });
</script>
