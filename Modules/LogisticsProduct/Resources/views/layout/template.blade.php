<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.themes.metronic1.partials.head_meta')
    @include('admin.themes.metronic1.partials.head_script')

    <link href="{{URL::asset('public/themelogistic/css/main_home8f26.css?v=55')}}" rel="stylesheet" type="text/css">
    <link href="{{URL::asset('public/themelogistic/css/style.css')}}" rel="stylesheet" type="text/css">

    <style>
        @media (min-width: 1200px) {
            .container {
                max-width: 1200px;
            }
            .kt-container.kt-container--fluid.kt-grid__item.kt-grid__item--fluid {
                margin-top: 50px;
                margin-bottom: 50px;
            }
            .table:not(.table-bordered) thead th, .table:not(.table-bordered) thead td {
                font-size: 12px;
            }
            .kt-portlet__head-wrapper {
                display: inline-block;
                width: 100%;
                position: relative;
            }

            .kt-portlet__head-wrapper .btn-success:not(:disabled):not(.disabled):active, .kt-portlet__head-wrapper .btn-success:not(:disabled):not(.disabled).active, .kt-portlet__head-wrapper .show > .btn-success.dropdown-toggle,
            .kt-portlet__head-wrapper .btn-success:hover {
                color: #000 !important;
            }
            .kt-portlet__head-wrapper .btn-brand:hover {
                color: #000;
            }
        }
    </style>
</head>
<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
<!-- begin:: Page -->

@include('themelogistics::partials.top_header')
@yield('main')
@include('themelogistics::partials.footer')


@include('admin.themes.metronic1.modal.blank_modal')
@include('admin.themes.metronic1.modal.delete_warning_modal')
@include('admin.themes.metronic1.modal.confirm_action_modal')
@include('admin.themes.metronic1.modal.something_went_wrong')

@include('admin.themes.metronic1.partials.footer_script')

<script>
    document.querySelector('.mini-photo-wrapper').addEventListener('click', function() {
        document.querySelector('.menu-container').classList.toggle('active');
    });
    $('#order-status-modal').on('show.bs.modal', function (e) {
        var button = $(e.relatedTarget);
        var name = button.data('name');
        console.log(name);
    })
</script>
</body>
<!-- end::Body -->
</html>
