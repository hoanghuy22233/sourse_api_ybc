<a class="history_transport_detail{{ $item->id }}" target="_blank">Xem</a>


<div class="modal fade" id="popup-add-history_transport{{ $item->id }}" role="dialog" style="z-index: 100000">
    <?php
    $status_text = [
        0 => 'Chưa thanh toán',
        1 => 'Chờ lấy hàng',
        2 => 'Đã lấy hàng',
        3 => 'Đã giao hàng',
        4 => 'Nợ COD',
        5 => 'Đã trả COD',
        6 => 'Không phát được',
        7 => 'Đã hủy',
        8 => 'Đang chuyển hoàn',
        9 => 'Đã chuyển hoàn',
        10 => 'Sự cố',
    ];
    $shipping_method = [
        1 => 'Chuyển phát hỏa tốc',
        2 => 'Chuyển phát nhanh',
        3 => 'Chuyển phát tiết kiệm',
        4 => 'Chuyển phát đường bộ',
        5 => 'Chuyển phát thu hộ(COD)',
        6 => 'Thuê xe nguyên chuyến',
    ];
    ?>
    <form id="form-history_transport" action="post" autocomplete="off">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content " style="height:auto">
                <div class="modal-header">
                    <h4 style="display:inline-block;" class="title-history_transport">Chi tiết vận đơn</h4>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <div class="box-info box-sd">
                        <h4 class="box-title">Thông tin hàng hóa</h4>
                        <div class="box-input">
                                <span>
                        <label for="">Mã đơn hàng</label>
                                    <p>{{ $item->code }}</p>

                    </span>
                            <span>
                        <label for="">Trạng thái</label>

                        <p>{{  @$status_text[$item->status] }}</p>
                    </span>
                            <span>
                        <label for="">Tên hàng hóa</label>
                        <p class="status-detail">{{ $item->name }}</p>
                    </span>


                        </div>
                    </div>


                    <div class="row" style="padding: 11px">
                        <div class="box-info box-sd col-md-6">
                            <div class="box-input">
                                 <span>
                        <label for="">Ghi chú</label>
                        <p>{{ $item->note }}</p>
                    </span>

                            </div>
                        </div>

                        <div class="box-info box-sd col-md-6">
                            <h4 class="box-title">Lịch sử vận đơn</h4>
                            <?php
                            $history = \Modules\LogisticsProduct\Models\TransportHistory::where('transport_id', $item->id)->get();
                            ?>
                            <div class="box-input area_history">

                                @foreach($history as $h)
                                    <span>
                                                <p>{{ $h->name }}</p>
                                            </span>
                                @endforeach


                            </div>
                        </div>



                    </div>


                    <div class="row" style="padding: 11px">
                        <div class="box-info box-sd col-md-6">
                            <h4 class="box-title">Thông tin nơi nhận hàng</h4>
                            <div class="box-input">
                                <span>
                        <label for="">Họ và tên</label>
                                    <p>{{ @$item->shipping_name }}</p>

                    </span>
                                <span>
                        <label for="">SĐT</label>

                        <p>{{ $item->shipping_tel }}</p>
                    </span>
                                <span>
                        <label for="">Địa chỉ </label>
                        <p>{{ $item->shipping_address }}</p>
                    </span>
                                <span>
                        <label for="">Tỉnh/thành phố </label>
                                <?php
                                    $province = \Modules\ThemeLogistics\Models\Province::where('id', @$item->shipping_province_id)->first();
                                    ?>
                        <p>{{@$province->name}}</p>
                    </span>
                                <span>
                        <label for="">Quận huyện</label>
                                <?php
                                    $district = \Modules\ThemeLogistics\Models\District::where('id', @$item->shipping_district_id)->first();
                                    ?>

                        <p>{{@$district->name}}</p>
                    </span>

                            </div>
                        </div>


                        <div class="box-info box-sd col-md-6">
                            <h4 class="box-title">Thông tin người nhận hàng</h4>
                            <div class="box-input">
                                <span>
                        <label for="">Họ và tên</label>
                                    <p>{{ $item->billing_name }}</p>

                    </span>

                                <span>
                        <label for="">Địa chỉ </label>
                        <p>{{ $item->billing_address }}</p>
                    </span>

                                <span>
                        <label for="">Tỉnh/thành phố </label>
                                <?php
                                    $province = \Modules\ThemeLogistics\Models\Province::where('id', @$item->shipping_province_id)->first();
                                    ?>
                        <p>{{@$province->name}}</p>
                    </span>
                                <span>
                        <label for="">Quận huyện</label>
                                <?php
                                    $district = \Modules\ThemeLogistics\Models\District::where('id', @$item->shipping_district_id)->first();
                                    ?>

                        <p>{{@$district->name}}</p>
                    </span>
                                <span>
                        <label for="">Email </label>
                        <p>{{ $item->billing_email }}</p>
                    </span>

                            </div>
                        </div>
                    </div>


                    <div class="row" style="padding: 11px">
                        <div class="box-info box-sd col-md-6">
                            <div class="box-input">
                                <span>
                        <label for="">Phương thức vận chuyển</label>
                                    <?php
                                    $shipping_method = \Modules\LogisticsProduct\Models\ShippingMethod::where('id', @$item->shipping_method_id)->first();
                                    ?>
                                    <p class="pr-1">{{  @$shipping_method->name }}</p>

                                     <p> {{@number_format(@$shipping_method->price , 0, '.', '.') }}đ </p>


                    </span>
                                {!! @$shipping_method->html_icon !!}
                            </div>
                        </div>



                        <div class="box-info box-sd col-md-6">
                            <h4 class="box-title">Thành tiền (chưa VAT)</h4>
                            <div class="box-input">
                                <span>
                        <label for="">Tổng tiền</label>
                                    <p> {{ number_format($item->discount_price , 0, '.', '.') }}đ </p>

                    </span>

                                <span>
                        <label for="">Mã giảm giá</label>
                        <p>{{ $item->discount_code }}</p>
                    </span>
                                <span>
                        <label for="">VAT</label>
                        <p>{{ $item->vat }}</p>
                    </span>

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        {{--<div class="modal-footer">--}}
        {{--<button class="btn btn-default" data-dismiss="modal">Đóng</button>--}}
        {{--<button class="btn btn-success" type="submit"><i class="icon-header fa fa-check-circle"></i> Hoàn--}}
        {{--thành--}}
        {{--</button>--}}
        {{--</div>--}}


    </form>
</div>
<style>
    .status-detail {
        padding: 3px 35px;
        background-color: #8064A2;
        color: #fff;
        font-size: 14px;
    }
    .area_history {
        height: 123px;
        overflow: auto;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address {
        display: flex;
        flex-wrap: wrap;
        border: 1px solid #ccc;
        border-radius: 5px;
        margin-bottom: 15px;
    }


    .modal .modal-dialog .modal-content .box-voucher, .modal .modal-dialog .modal-content .box-comment, .modal .modal-dialog .modal-content .box-paypal, .modal .modal-dialog .modal-content .box-total, .modal .modal-dialog .modal-content .box-info {
        padding: 10px 20px;
        border-radius: 5px;
        border: 1px solid #ccc;
        margin-bottom: 15px;
    }

    .box-sd {
        box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.4);
    }

    .box-title {
        font-size: 18px;
        font-weight: 600;
        color: #000000fa;
        margin-bottom: 15px;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span label {
        color: #000;
        font-size: 14px;
        margin-right: 10px;
        margin-bottom: 0;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span {
        display: flex;
        align-items: center;
        margin-bottom: 10px;
    }

    .modal .modal-dialog .modal-content .modal-body .form__control input, textarea {
        border: none;
        outline: none;
        resize: none;
    }

    .input__control {
        border-radius: 5px;
        width: 100% !important;
        border: 1px solid #c7c7c7 !important;
        margin-bottom: 8px;
        padding: 5px;
        font-size: 14px;
    }

    input {
        border: none;
        outline: none;
    }

    button, input {
        overflow: visible;
    }

    input, button, select, optgroup, textarea {
        margin: 0;
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
    }

    label {
        display: inline-block;
        margin-bottom: 0.5rem;
        color: #fff;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span .input__control {
        width: 80% !important;
        padding: 5px;
        margin-bottom: 0;
    }

    .input__control {
        border-radius: 5px;
        width: 100% !important;
        border: 1px solid #c7c7c7 !important;
        margin-bottom: 8px;
        padding: 5px;
        font-size: 14px;
    }

    input {
        border: none;
        outline: none;
    }

    p {
        margin: 0px !important;
    }

    .bootstrap-select .dropdown-toggle .filter-option-inner-inner {
        overflow: hidden;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address .address-form.mg-r {
        margin-right: 15px;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address .address-form {
        flex-basis: 48%;
        padding: 10px 15px;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address .shipping-address {
        position: relative;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address .address-form .form__control .control-top, .modal .modal-dialog .modal-content .modal-body .control-body .input__control {
        display: flex;
    }
</style>
<script>
    $('body').on('click', '.history_transport_detail{{ $item->id }}', function () {
        let modal = $('#popup-add-history_transport{{ $item->id }}');

        modal.modal();
    });
</script>
