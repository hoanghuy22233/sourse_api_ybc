

@if( (\Auth::guard('admin')->user()->id == $item->admin_id &&
        in_array($module['code'] . '_delete', $permissions)  &&
        @\Modules\LogisticsProduct\Models\Transport::find(@$_GET['transport_id'])->status != 0)
         || in_array('super_admin', $permissions)
         || (!isset($_GET['transport_id']) && @$item->status == 1) )
     <a href="{{ url('/admin/'.$module['code'].'/delete/' . $item->id) }}{{ isset($_GET['transport_id']) ? '?transport_id=' . $_GET['transport_id'] : '' }}"
       style="    font-size: 14px!important;" title="Xóa bản ghi"
       class="delete-warning {{ isset($field['tooltip_info']) ? 'a-tooltip_info' : '' }}">Xóa</a>
@endif
