<?php
$whereRaw = '1=1 ';
$role_name = \App\Http\Helpers\CommonHelper::getRoleName(\Auth::guard('admin')->user()->id, 'name');
if ($role_name == 'khach_hang') {
    $whereRaw .= 'AND admin_id = ' . \Auth::guard('admin')->user()->id;
}

if($role_name == 'khach_hang') {
    $extends = 'logisticsproduct::layout';
} else {
    $extends = config('core.admin_theme');
}
?>

@extends($extends.'.template')

@section('main')
    <style>
        .small-box {
            border-radius: 2px;
            position: relative;
            display: block;
            margin-bottom: 20px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1)
        }

        .small-box > .inner {
            padding: 10px
        }

        .small-box > .small-box-footer {
            position: relative;
            text-align: center;
            padding: 3px 0;
            color: #fff;
            color: rgba(255, 255, 255, 0.8);
            display: block;
            z-index: 10;
            background: rgba(0, 0, 0, 0.1);
            text-decoration: none
        }

        .small-box > .small-box-footer:hover {
            color: #fff;
            background: rgba(0, 0, 0, 0.15)
        }

        .small-box h3 {
            font-size: 38px;
            font-weight: bold;
            margin: 0 0 10px 0;
            white-space: nowrap;
            padding: 0
        }

        .small-box p {
            font-size: 15px
        }

        .small-box p > small {
            display: block;
            color: #f9f9f9;
            font-size: 13px;
            margin-top: 5px
        }

        .small-box h3,
        .small-box p {
            z-index: 5px
        }

        .small-box .icon {
            -webkit-transition: all .3s linear;
            -o-transition: all .3s linear;
            transition: all .3s linear;
            position: absolute;
            top: -10px;
            right: 10px;
            z-index: 0;
            font-size: 90px;
            color: rgba(0, 0, 0, 0.15)
        }

        .small-box:hover {
            text-decoration: none;
            color: #f9f9f9
        }

        .small-box:hover .icon {
            font-size: 95px
        }

        @media (max-width: 767px) {
            .small-box {
                text-align: center
            }

            .small-box .icon {
                display: none
            }

            .small-box p {
                font-size: 12px
            }
        }

        .bg-yellow,
        .bg-aqua,
        .bg-teal,
        .bg-purple,
        .bg-maroon {
            color: #fff !important
        }

        .bg-yellow {
            background-color: #f39c12 !important
        }

        .bg-aqua {
            background-color: #00c0ef !important
        }

        .bg-teal {
            background-color: #39cccc !important
        }

        .bg-purple {
            background-color: #605ca8 !important
        }

        .bg-maroon {
            background-color: #d81b60 !important
        }
    </style>
    @if($role_name == 'khach_hang')
        <div class="col-xs-12">
            <div class="row">
                @include('logisticsproduct::partials.aside_menu')
                <div class="col-md-10 kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                    <div class="row">
                        <div class="col-xs-12 col-lg-12 col-xl-12 col-lg-12 order-lg-1 order-xl-1">
                            <!--begin:: Widgets/Finance Summary-->
                            <div class="kt-portlet ">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title bold uppercase">
                                            Tổng quan
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="kt-widget12">
                                        <div class="kt-widget12__content">
                                            <div class="row">
                                                <div class="col-xs-4 col-sm-3">
                                                    <div class="small-box bg-yellow">
                                                        <div class="inner">
                                                            <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->sum('cod')) }}
                                                                đ</h3>
                                                            <p>Tổng tiền thu hộ</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-home"></i>
                                                        </div>
                                                        <a href="/admin/product" class="small-box-footer">Xem thêm <i
                                                                    class="fa fa-arrow-circle-right"></i></a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-3">
                                                    <div class="small-box bg-aqua">
                                                        <div class="inner">
                                                            <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->sum('tongtien')) }}
                                                                đ</h3>
                                                            <p>Phí vận chuyển</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-book"></i>
                                                        </div>
                                                        <a href="/admin/bill" class="small-box-footer">Xem thêm <i
                                                                    class="fa fa-arrow-circle-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-4 col-sm-3">
                                                    <div class="small-box bg-yellow">
                                                        <div class="inner">
                                                            <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->whereIn('status', [1,2,3,4,5,6])->count()) }}</h3>
                                                            <p>Chưa thành công</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-home"></i>
                                                        </div>
                                                        <a href="/admin/transport" class="small-box-footer">Xem thêm <i
                                                                    class="fa fa-arrow-circle-right"></i></a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-3">
                                                    <div class="small-box bg-aqua">
                                                        <div class="inner">
                                                            <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->whereIn('status', [1])->count()) }}</h3>
                                                            <p>Chờ lấy hàng</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-book"></i>
                                                        </div>
                                                        <a href="/admin/transport" class="small-box-footer">Xem thêm <i
                                                                    class="fa fa-arrow-circle-right"></i></a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-3">
                                                    <div class="small-box bg-purple">
                                                        <div class="inner">
                                                            <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->whereIn('status', [2])->count()) }}</h3>
                                                            <p>Đã lấy hàng</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                        <a href="/admin/transport" class="small-box-footer">Xem thêm <i
                                                                    class="fa fa-arrow-circle-right"></i></a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-3">
                                                    <div class="small-box bg-purple">
                                                        <div class="inner">
                                                            <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->whereIn('status', [3])->count()) }}</h3>
                                                            <p>Đã giao hàng</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                        <a href="/admin/transport" class="small-box-footer">Xem thêm <i
                                                                    class="fa fa-arrow-circle-right"></i></a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-3">
                                                    <div class="small-box bg-purple">
                                                        <div class="inner">
                                                            <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->whereIn('status', [5])->count()) }}</h3>
                                                            <p>Đã trả COD</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                        <a href="/admin/transport" class="small-box-footer">Xem thêm <i
                                                                    class="fa fa-arrow-circle-right"></i></a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4 col-sm-3">
                                                    <div class="small-box bg-purple">
                                                        <div class="inner">
                                                            <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->whereIn('status', [7])->count()) }}</h3>
                                                            <p>Đã hủy</p>
                                                        </div>
                                                        <div class="icon">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                        <a href="/admin/transport" class="small-box-footer">Xem thêm <i
                                                                    class="fa fa-arrow-circle-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end:: Widgets/Finance Summary-->

                            {{--<div class="kt-portlet ">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title bold uppercase">
                                            Thống kê
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <div class="kt-widget12">
                                        <div class="kt-widget12__content">
                                            <div class="row">
                                                <img src="http://logistics.webhobasoft.com/shop/images/thong_ke.png"
                                                     style="width: 100%;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--}}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-xs-12 col-lg-12 col-xl-12 col-lg-12 order-lg-1 order-xl-1">
                    <!--begin:: Widgets/Finance Summary-->
                    <div class="kt-portlet ">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title bold uppercase">
                                    Tổng quan
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="kt-widget12">
                                <div class="kt-widget12__content">
                                    <div class="row">
                                        <div class="col-xs-4 col-sm-3">
                                            <div class="small-box bg-yellow">
                                                <div class="inner">
                                                    <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->sum('cod')) }}
                                                        đ</h3>
                                                    <p>Tổng tiền thu hộ</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-home"></i>
                                                </div>
                                                <a href="/admin/product" class="small-box-footer">Xem thêm <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-3">
                                            <div class="small-box bg-aqua">
                                                <div class="inner">
                                                    <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->sum('tongtien')) }}
                                                        đ</h3>
                                                    <p>Phí vận chuyển</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-book"></i>
                                                </div>
                                                <a href="/admin/bill" class="small-box-footer">Xem thêm <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-4 col-sm-3">
                                            <div class="small-box bg-yellow">
                                                <div class="inner">
                                                    <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->whereIn('status', [1,2,3,4,5,6])->count()) }}</h3>
                                                    <p>Chưa thành công</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-home"></i>
                                                </div>
                                                <a href="/admin/transport" class="small-box-footer">Xem thêm <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-3">
                                            <div class="small-box bg-aqua">
                                                <div class="inner">
                                                    <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->whereIn('status', [1])->count()) }}</h3>
                                                    <p>Chờ lấy hàng</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-book"></i>
                                                </div>
                                                <a href="/admin/transport" class="small-box-footer">Xem thêm <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-3">
                                            <div class="small-box bg-purple">
                                                <div class="inner">
                                                    <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->whereIn('status', [2])->count()) }}</h3>
                                                    <p>Đã lấy hàng</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <a href="/admin/transport" class="small-box-footer">Xem thêm <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-3">
                                            <div class="small-box bg-purple">
                                                <div class="inner">
                                                    <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->whereIn('status', [3])->count()) }}</h3>
                                                    <p>Đã giao hàng</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <a href="/admin/transport" class="small-box-footer">Xem thêm <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-3">
                                            <div class="small-box bg-purple">
                                                <div class="inner">
                                                    <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->whereIn('status', [5])->count()) }}</h3>
                                                    <p>Đã trả COD</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <a href="/admin/transport" class="small-box-footer">Xem thêm <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-xs-4 col-sm-3">
                                            <div class="small-box bg-purple">
                                                <div class="inner">
                                                    <h3>{{ number_format(\Modules\LogisticsProduct\Models\Transport::whereRaw($whereRaw)->whereIn('status', [7])->count()) }}</h3>
                                                    <p>Đã hủy</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <a href="/admin/transport" class="small-box-footer">Xem thêm <i
                                                            class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end:: Widgets/Finance Summary-->

                    {{--<div class="kt-portlet ">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title bold uppercase">
                                    Thống kê
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="kt-widget12">
                                <div class="kt-widget12__content">
                                    <div class="row">
                                        <img src="http://logistics.webhobasoft.com/shop/images/thong_ke.png"
                                             style="width: 100%;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>--}}
                </div>

            </div>
        </div>
    @endif
@endsection
@section('custom_head')
    <style type="text/css">
        .kt-datatable__cell > span > a.cate {
            color: #5867dd;
            margin-bottom: 3px;
            background: rgba(88, 103, 221, 0.1);
            height: auto;
            display: inline-block;
            width: auto;
            padding: 0.15rem 0.75rem;
            border-radius: 2px;
        }

        .paginate > ul.pagination > li {
            padding: 5px 10px;
            border: 1px solid #ccc;
            margin: 0 5px;
            cursor: pointer;
        }

        .paginate > ul.pagination span {
            color: #000;
        }

        .paginate > ul.pagination > li.active {
            background: #0b57d5;
            color: #fff !important;
        }

        .paginate > ul.pagination > li.active span {
            color: #fff !important;
        }

        .kt-widget12__desc, .kt-widget12__value {
            text-align: center;
        }

        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99 list_user
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <style type="text/css">
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>

@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $('#active_service a').click(function (event) {
                event.preventDefault();
                var object = $(this);
                $.ajax({
                    url: '/admin/service_history/ajax-publish',
                    data: {
                        id: object.data('service_history_id')
                    },
                    success: function (result) {
                        if (result.status == true) {
                            toastr.success(result.msg);
                            object.parents('tr').remove();
                        } else {
                            toastr.error(result.msg);
                        }
                    },
                    error: function (e) {
                        console.log(e.message);
                    }
                })
            })
        })
    </script>
@endpush

