<?php
$area = !isset($_COOKIE['area']) ? 'transport' : $_COOKIE['area'];
?>
@if($area == 'shop')
    @if(in_array('bill_view', $permissions))
        <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                    href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon-folder-1"></i>
                    </span><span class="kt-menu__link-text">Quản Lý Đơn Hàng</span><i
                        class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                        class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/bill" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Tất cả</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="#" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Đơn hủy</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="#" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Trả hàng / Hoàn tiền</span></a></li>
                </ul>
            </div>
        </li>
    @endif

    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i class="kt-menu__link-icon flaticon-folder-1"></i>
                    </span><span class="kt-menu__link-text">Tổng quan</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="/admin/dashboard_bill_lading" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Thống kê</span></a></li>

            </ul>
        </div>
    </li>

    @if(in_array('product_view', $permissions))
        <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                    href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon-folder-1"></i>
                    </span><span class="kt-menu__link-text">Sản Phẩm</span><i
                        class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                        class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/product" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Tất cả sản phẩm</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/product/add" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Tạo sản phẩm mới</span></a></li>
                </ul>
            </div>
        </li>
    @endif


    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon-folder-1"></i>
                    </span><span class="kt-menu__link-text">Tài Chính</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/debt" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Doanh thu</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/withdrawal_request" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Yêu cầu rút tiền</span></a></li>
            </ul>
        </div>
    </li>
@endif


@if($area == 'transport')




    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i class="kt-menu__link-icon flaticon-folder-1"></i>
                    </span><span class="kt-menu__link-text">Vận đơn</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="/admin/transport" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Tất cả vận đơn</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="/transport/add"
                                                                   class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Tạo vận đơn</span></a></li>
            </ul>
        </div>
    </li>

    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i class="kt-menu__link-icon flaticon-folder-1"></i>
                    </span><span class="kt-menu__link-text">Sổ địa chỉ</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a href="/admin/pick_location" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Điểm lấy hàng</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a href="/admin/pay_location"
                                                                   class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Điểm trả hàng</span></a></li>
            </ul>
        </div>
    </li>
@endif