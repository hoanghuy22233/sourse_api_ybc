<?php
$area = !isset($_COOKIE['area']) ? 'transport' : $_COOKIE['area'];
?>
@if($area == 'shop')
    @if(in_array('user_view', $permissions))
        <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                    href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon-folder-1"></i>
                    </span><span class="kt-menu__link-text">Q Lý Khách Hàng</span><i
                        class="kt-menu__ver-arrow la la-angle-right"></i></a>
            <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                        class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/user" class="kt-menu__link "><span
                                    class="kt-menu__link-text">Tất cả</span></a></li>
                </ul>
            </div>
        </li>
    @endif
@endif