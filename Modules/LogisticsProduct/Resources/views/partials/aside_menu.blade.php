<style>
    .kt-portlet__head-wrapper a {
        /*margin: 0 !important;*/
        /*border-radius: 0 !important;*/
        /*background-color: unset !important;*/
        color: #000;
        /*border: 0;*/
        /*display: inline-block;*/
        /*float: left;*/
        position: relative;
    }

    .history_transport_detail {
        cursor: pointer;
    }

    .tracking-block {
        padding-left: 30px;
        margin-top: 50px;
        padding-right: 0;
    }

    .block-title {
        background: #fff;
        padding: 10px;
    }

    .tracking-block a {
        color: #212529;
    }

    .tracking-block a:hover {
        color: #5867dd;
    }
    .tracking-block a.active {
        color: #5867dd;
    }
</style>
<div class="col-md-2 tracking-block">
    <ul class="list-group">
        <li class="list-group-item active">Menu</li>
        <li class="list-group-item"><a href="/admin/dashboard" class="{{ strpos($_SERVER['REQUEST_URI'], '/dashboard') !== false ? 'active' : '' }}">Thống kê</a></li>
        <li class="list-group-item"><a href="/admin/transport" class="{{ strpos($_SERVER['REQUEST_URI'], '/transport') !== false ? 'active' : '' }}">Tất cả vận đơn</a></li>
        <li class="list-group-item"><a href="/admin/profile" class="{{ strpos($_SERVER['REQUEST_URI'], '/profile') !== false ? 'active' : '' }}">Sửa profile</a></li>
        <li class="list-group-item"><a href="/dang-xuat">Đăng xuất</a></li>
    </ul>

</div>