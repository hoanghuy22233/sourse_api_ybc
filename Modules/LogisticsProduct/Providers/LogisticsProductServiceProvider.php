<?php

namespace Modules\LogisticsProduct\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class LogisticsProductServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Cấu hình core
//            $this->registerTranslations();
//            $this->registerConfig();
            $this->registerViews();
//            $this->registerFactories();
//            $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');


            //  Custom setting
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['product_view', 'product_add', 'product_edit', 'product_delete', 'product_publish',
                'debt_view', 'debt_add', 'debt_edit', 'debt_delete', 'debt_publish',
                'transport_view', 'transport_add', 'transport_edit', 'transport_delete', 'transport_publish',
                'transport_history_view', 'transport_history_add', 'transport_history_edit', 'transport_history_delete', 'transport_history_publish',
                'withdrawal_request_view', 'withdrawal_request_add', 'withdrawal_request_edit', 'withdrawal_request_delete', 'withdrawal_request_publish'
                ]);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
                print view('logisticsproduct::partials.aside_menu.dashboard_after_product');
            }, 1, 1);

        //  Sửa menu khách hàng
        \Eventy::addFilter('aside_menu.user', function() {
            print view('logisticsproduct::partials.aside_menu.user');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('logisticsproduct.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'logisticsproduct'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/logisticsproduct');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/logisticsproduct';
        }, \Config::get('view.paths')), [$sourcePath]), 'logisticsproduct');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/logisticsproduct');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'logisticsproduct');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'logisticsproduct');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
