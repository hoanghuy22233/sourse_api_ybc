<?php

namespace Modules\LogisticsProduct\Models;

use Illuminate\Database\Eloquent\Model;

class TransportShippingMethods extends Model
{
    protected $table = 'transport_shipping_methods';

    public function transport_type() {
        return $this->belongsTo(TransportType::class, 'transport_type_id');
    }

    public function transport_product_type() {
        return $this->belongsTo(TransportProductType::class, 'transport_product_type_id');
    }
}
