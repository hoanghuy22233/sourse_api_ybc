<?php

namespace Modules\LogisticsProduct\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ward;
use App\Models\Province;
use App\Models\District;
use Modules\ThemeLogistics\Models\Admin;
use Modules\ThemeLogistics\Models\StatusTransport;
use Modules\ThemeLogisticsAdmin\Models\TransportProductTypes;
use Modules\ThemeLogisticsAdmin\Models\TransportShippingMethods;
use Modules\ThemeLogisticsAdmin\Models\TransportTypes;


class Transport extends Model
{

    protected $table = 'transports';
    public $timestamps = false;

    protected $fillable = [
        'code', 'admin_id', 'name', 'image', 'shipping_name', 'shipping_tel',
        'shipping_address', 'shipping_province_id', 'shipping_district_id', 'shipping_ward_id',
        'shipping_email', 'billing_name', 'billing_tel', 'billing_address',
        'billing_province_id', 'billing_district_id', 'billing_ward_id', 'billing_email',
        'discount_code', 'discount_price', 'note', 'payment_methods',
        'shipping_method_id', 'type', 'mass', 'owned',
        'vat', 'shipping_fee', 'type_commodities', 'status', 'type_quantity', 'cod', 'transport_shipping_method_id', 'transport_product_type_id', 'transport_type_id',
    ];

    public function shipping_method()
    {
        return $this->belongsTo(ShippingMethod::class, 'shipping_method_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function shipping_ward()
    {
        return $this->belongsTo(Ward::class, 'shipping_ward_id');
    }

    public function billing_ward()
    {
        return $this->belongsTo(Ward::class, 'billing_ward_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function admin_receiver()
    {
        return $this->belongsTo(Admin::class, 'receiver_admin_id');
    }

    public function transport_type()
    {
        return $this->belongsTo(TransportTypes::class, 'transport_type_id');
    }

    public function transport_product_type()
    {
        return $this->belongsTo(TransportProductTypes::class, 'transport_product_type_id');
    }

    public function transport_shipping_method()
    {
        return $this->belongsTo(TransportShippingMethods::class, 'transport_shipping_method_id');
    }

    public function statusObj(){
        return $this->belongsTo(StatusTransport::class, 'status_id', 'id');
    }
}
