<?php

namespace Modules\LogisticsProduct\Models;

use Illuminate\Database\Eloquent\Model;

class PropertieName extends Model
{
    protected $table = 'properties_name';
    protected $guarded = [];
    public $timestamps = false;

}
