<?php
namespace Modules\LogisticsProduct\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ward;
use App\Models\Province;
use App\Models\District;
use Modules\ThemeLogistics\Models\Admin;


class TransportHistory extends Model
{

    protected $table = 'transport_histories';

    protected $fillable = [
        'name','created_at','updated_at','transport_id','admin_id'
    ];

    public function transport()
    {
        return $this->belongsTo(Transport::class, 'transport_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }


}
