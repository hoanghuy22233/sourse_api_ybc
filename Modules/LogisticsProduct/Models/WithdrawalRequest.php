<?php

namespace Modules\LogisticsProduct\Models;

use Illuminate\Database\Eloquent\Model;

class WithdrawalRequest extends Model
{

    protected $table = 'withdrawal_requests';

    protected $fillable = [
        'id'
    ];
    public $timestamps =false;
}
