<?php

namespace Modules\LogisticsProduct\Models;

use Illuminate\Database\Eloquent\Model;

class TransportProductType extends Model
{
    protected $table = 'transport_product_types';

    public function transport_type() {
        return $this->belongsTo(TransportType::class, 'transport_type_id');
    }
}
