<?php


namespace Modules\LogisticsProduct\Models ;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment_logs';


    protected $fillable = [
        'booking_id', 'admin_id', 'reply','created_at','content','image','image_present'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    public function reply()
    {
        return $this->hasOne(Comment::class, 'id');
    }


    public function childs()
    {
        return $this->hasMany($this, 'reply', 'id')->orderBy('id', 'asc');
    }
}
