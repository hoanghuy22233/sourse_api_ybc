<?php

namespace Modules\LogisticsProduct\Models;

use Illuminate\Database\Eloquent\Model;

class Debt extends Model
{

    protected $table = 'debts';

    protected $fillable = [
        'id'
    ];
    public $timestamps =false;
}
