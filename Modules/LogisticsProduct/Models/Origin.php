<?php

namespace Modules\LogisticsProduct\Models;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
   protected $table = 'origins';
   public $timestamps = false;
}
