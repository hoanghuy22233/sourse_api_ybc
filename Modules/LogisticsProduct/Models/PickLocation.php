<?php
namespace Modules\LogisticsProduct\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ward;
use App\Models\Province;
use App\Models\District;


class PickLocation extends Model
{

    protected $table = 'pick_locations';
    public $timestamps = false;

    protected $fillable = [
        'bill_lading_code' , 'place_submission', 'recipient_name' , 'address' , 'province_id' , 'district_id' , 'ward_id' , 'date_creation', 'collect_household'
    ];

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }
    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }

}
