<style>
    .banner_slides .owl-stage {
        max-height: 500px;
        overflow: hidden;
    }

    .banner_slides .text-caro-meta {
        left: 27%;
        top: 35%;
    }
    .banner_slides .owl-carousel .item {
        width: 100%;
    }
    @media(max-width: 768px) {
        .banner_slides .owl-carousel .owl-item img {
            max-width: 320px;
        }
        .banner_slides .text-caro-meta h1 a {
            font-size: 12px;
        }
        .banner_slides .text-caro-meta {
            left: 50%;
        }
    }
    @media (max-width: 768px) {
        .banner_slides .owl-carousel .owl-item img {
            max-width: unset;
        }
    }
    @media (min-width: 1200px) {
    .desktop{
          height: 470px;
}
    }
    .banner_slides .unit-text-caro:before {
        background: none;
    }
    .banner_slides .owl-controls {
        display: none;
    }
    .banner_slides:hover .owl-controls {
        display: block;
    }
</style>
<?php
$data = CommonHelper::getFromCache('banners_' . json_encode($config), ['banners']);
if (!$data) {
    $data = \Modules\ThemeEduLDP\Models\Banner::where('location', @$config['location'])->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
    CommonHelper::putToCache('banners_' . json_encode($config), $data, ['banners']);
}
?>
@if(count($data) > 0)
    <div class="gap no-gap overlap22 banner_slides">
        <div class="text-caro owl-carousel owl-theme">
            @foreach($data as $k => $v)
                <div class="item {{ $k == 0 ? 'active' : '' }}">
                    <div class="unit-text-caro">
                        <img class="lazy desktop"  data-src="{{ asset('public/filemanager/userfiles/' . $v->image) }}"
                             alt="{{ $v->name }}">
                        <div class="text-caro-meta">
                            <h1><a href="{{ $v->link }}"
                                   title="{{ $v->name }}">{!! $v->intro !!}</a></h1>
                            @if ($v->intro != '' && $v->content != '')
                                <img class="lazy"
                                     data-src="{{ asset('public/frontend/themes/edu-ldp/images/icon-21.png') }}"
                                     alt="{{ $v->name }}">
                            @endif
                            <p>
                                {!! $v->content !!}
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif
<script>
    $(document).ready(function(){
        $(".banner_slides .owl-carousel").owlCarousel({
            items:1,
            loop:true,
            margin:0,
            autoplay:true,
            autoplayTimeout: 4000,
            autoplayHoverPause:true,
            nav:true,
            dots:false
        });
    });
</script>