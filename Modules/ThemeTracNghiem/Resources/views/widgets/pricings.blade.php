<?php
/**
Cấu hình trong widget:
phương án: 1 Môn
giá: 129.000đ
thời gian: /năm
mô tả: our price is free of cost for this package. you can not unlock all features.
cụ thể: 10.000+ câu hỏi/Môn để ôn luyện | 3 Hình thức ôn luyện (Môn - Chương - Chuyên Đề) | 3 Mức độ ôn luyện (5-7<sup>+</sup> - 8<sup>+ </sup>- 9<sup>+</sup>) | Hỗ trợ học viên 24/7 | Không giới hạn số lần ôn luyện
link: /dang-ky-mon
 */

?>

<style>
    .pricings > h1 span {
        right: -6px;
        top: 0;
    }
</style>

<div class="div">
    <div class="col-xs-12">
        <div class="sec-heading style9 text-center">
            <h2>{!! $widget->name !!}</h2>
        </div>
    </div>
    <?php
    $options = [];
    $arr = explode('phương án:', $widget->content);
    foreach ($arr as $item) {
        if ($item != '') {
            $option = [];
            $lines = preg_split('/\n|\r\n?/', $item);
            $option['name'] = trim(@$lines[0]);
            foreach ($lines as $line) {
                if (strpos($line, 'giá:') !== false) {
                    $option['price'] = trim(str_replace('giá:', '', $line));
                }
                if (strpos($line, 'thời gian:') !== false) {
                    $option['time'] = trim(str_replace('thời gian:', '', $line));
                }
                if (strpos($line, 'mô tả:') !== false) {
                    $option['intro'] = trim(str_replace('tả:', '', $line));
                }
                if (strpos($line, 'cụ thể:') !== false) {
                    $option['content'] = explode('|', str_replace('cụ thể:', '', $line));
                }
                if (strpos($line, 'link:') !== false) {
                    $option['link'] = trim(str_replace('link:', '', $line));
                }
            }
            $options[] = $option;
        }
    }
    ?>
    @foreach($options as $option)
        <div class="{{ @$config['col'] }}">
            <div class="price-box">
                <div class="pricings">
                    <h2>{{ $option['name'] }}</h2>
                    <h1>{{ @$option['price'] }} <span>{{ @$option['time'] }}</span></h1>
                    <p>
                        {!! @$option['intro'] !!}
                    </p>
                    <ul class="price-features">
                        @if(isset($option['content']))
                            @foreach(@$option['content'] as $li)
                                <li><i class="ti-check"></i> {!! $li !!}</li>
                            @endforeach
                        @endif
                    </ul>
                    <a class="main-btn" href="{{ @$option['link'] }}" title="Đăng ký" data-ripple="">Đăng ký</a>
                </div>
            </div>
        </div>
    @endforeach
</div>