<?php
/**
Cấu hình trong widget:
tiêu đề: MÔN TOÁN
mô tả: Tổng hợp kiến thức đáp ứng cho kỳ thi THPT Quốc Gia 2021 chia theo Chuyên Đề - Chương - Môn với các mức độ 5-7+ 8+ 9+ để các bạn dễ dàng nhất trong việc ôn luyện với mục tiêu mình đã đề ra.
link: /#
 */

?>
<style>
    .box_html .box-item {
        display: inline-block;
        float: left;
    }
</style>

<div class="div box_html">
    <div class="col-lg-10 offset-lg-1">
        <div class="sec-heading style9 text-center">
            <h2>{!! $widget->name !!}</h2>
        </div>
    </div>
    <?php
    $options = [];
    $arr = explode('tiêu đề:', $widget->content);
    foreach ($arr as $item) {
        if ($item != '') {
            $option = [];
            $lines = preg_split('/\n|\r\n?/', $item);
            $option['name'] = trim(@$lines[0]);
            foreach ($lines as $line) {
                if (strpos($line, 'mô tả:') !== false) {
                    $option['intro'] = trim(str_replace('mô tả:', '', $line));
                }
                if (strpos($line, 'link:') !== false) {
                    $option['link'] = trim(str_replace('link:', '', $line));
                }
            }
            $options[] = $option;
        }
    }
    ?>
    @foreach($options as $option)
        <div class="{{ @$config['col'] }} box-item" style="margin-bottom: 20px;">
            <div class="price-box">
                <div class="pricings">
                    <h2>{{ $option['name'] }}</h2>

                    <p>{!! @$option['intro'] !!}</p>
                    <a class="main-btn" href="{{ @$option['link'] }}" title="Đăng ký" data-ripple="">Xem chi
                        tiết</a>
                </div>
            </div>
        </div>
    @endforeach
</div>