<figure>
    <?php
    $banner = CommonHelper::getFromCache('banners_location_home_top', ['banners']);
    if (!$banner) {
        $banner = \Modules\ThemeTracNghiem\Models\Banner::where('location', 'home_top')->where('status', 1)->first();
        CommonHelper::putToCache('banners_location_home_top', $banner, ['banners']);
    }
    ?>
    <img class="lazy" data-src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb($banner->image,1110,300)}}"
         alt="{{@$banner->name}}">
</figure>
<style>
    * {
        margin: 0;
        padding: 0;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    /*sub-menu*/
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f5f5f5;
        z-index: 1;
        list-style: none;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .dropdown-item {
        display: block;
        width: 100%;
        padding: -0.25rem 0.5rem -1.25rem 0.75rem;
        clear: both;
        font-weight: 400;
        color: #212529;
        text-align: inherit;
        white-space: nowrap;
        background-color: transparent;
        border: 0;
    }
</style>
<div class="profile-section">
    <div class="row">
        <div class="col-lg-2">
            <div class="profile-author">
                <a class="profile-author-thumb" href="/">
                    <img alt="author" class="lazy" data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}">
                </a>
                <div class="author-content">
                    <a class="h4 author-name" href="/">{{ @$settings['name'] }}</a>
                </div>
            </div>
        </div>
        <div class="col-lg-10">
            <ul class="profile-menu">
                <?php
                $actives = array(
                    'khoahoc',
                    'tailieu',
                    'baikiemtra',
                );
                //                $menus = CommonHelper::getFromCache('menus_location_main_menu');
                //                if (!$menus) {
                //                    $menus = \Modules\EduCourse\Models\Category::where('parent_id', 0)->where('location', 'main_menu')->where('status', 1)->orderby('order_no', 'DESC')
                //                        ->orderby('id', 'asc')->get();
                //                    CommonHelper::putToCache('menus_location_main_menu', $menus);
                //                }
                $main_menus = CommonHelper::getFromCache('menus_location_main_menu', ['menus']);
                if (!$main_menus) {
                    $main_menus = \Modules\ThemeTracNghiem\Models\Menu::where('status', 1)->orderBy('order_no', 'desc')->orderBy('name', 'asc')->get();
                    CommonHelper::putToCache('menus_location_main_menu', $main_menus, ['menus']);
                }
                ?>

                {{--@foreach($main_menus as $k =>$main_menu)--}}
                {{--@if($main_menu->parent_id==0)--}}
                {{--<li>--}}
                {{--<a @yield(@$actives[$k]) href="/{{@$main_menu->slug}}">{{@$main_menu->name}}</a>--}}

                {{--<ul>--}}
                {{--@foreach($main_menus as $sub_menu)--}}
                {{--@if($sub_menu->parent_id==$main_menu->id)--}}
                {{--<li>--}}
                {{--<a @yield(@$actives[$k]) href="/{{@$sub_menu->slug}}">{{@$sub_menu->name}}</a>--}}
                {{--</li>--}}
                {{--@endif--}}
                {{--@endforeach--}}
                {{--</ul>--}}

                {{--</li>--}}

                {{--@endif--}}
                {{--@endforeach--}}

                @foreach($main_menus as $k =>$main_menu)
                    @if($main_menu->parent_id==0)
                        <li class="nav-item dropdown">
                            <a class="nav-link @yield(@$actives[$k])" id="navbarDropdown"
                               href="/{{@$main_menu->slug}}">{{@$main_menu->name}}</a>

                            @foreach($main_menus as $sub_menu)
                                @if($sub_menu->parent_id==$main_menu->id)
                                    <div class="dropdown-content">
                                        <a class="dropdown-item @yield(@$actives[$k])"
                                           href="/{{@$sub_menu->slug}}">{{@$sub_menu->name}}</a>
                                        <div class="dropdown-divider"></div>
                                    </div>

                                @endif
                            @endforeach


                        </li>

                    @endif
                @endforeach

                {{--<li class="nav-item dropdown">--}}
                {{--<a class="nav-link" href="#" id="navbarDropdown">--}}
                {{--Dropdown--}}
                {{--</a>--}}
                {{--<div class="dropdown-content">--}}
                {{--<a class="dropdown-item" href="#">Action</a>--}}
                {{--<div class="dropdown-divider"></div>--}}
                {{--<a class="dropdown-item" href="#">Another action</a>--}}
                {{--<div class="dropdown-divider"></div>--}}
                {{--<a class="dropdown-item" href="#">Something else here</a>--}}
                {{--</div>--}}

                {{--</li>--}}

            </ul>
        </div>
    </div>
</div>