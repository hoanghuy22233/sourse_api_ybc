<figure>
    <?php
    $banner = CommonHelper::getFromCache('banners_location_home_top', ['banners']);
    if (!$banner) {
        $banner = \Modules\ThemeTracNghiem\Models\Banner::where('location', 'home_top')->where('status', 1)->first();
        CommonHelper::putToCache('banners_location_home_top', $banner, ['banners']);
    }
    ?>
    <img class="lazy" data-src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb($banner->image,1110,300)}}"
         alt="{{@$banner->name}}">
</figure>
<style>
    * {
        margin: 0;
        padding: 0;
    }

    .dropdown {
        position: relative;
        display: inline-block;
    }

    /*sub-menu*/
    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f5f5f5;
        z-index: 1;
        list-style: none;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .dropdown-item {
        display: block;
        width: 100%;
        padding: -0.25rem 0.5rem -1.25rem 0.75rem;
        clear: both;
        font-weight: 400;
        color: #212529;
        text-align: inherit;
        white-space: nowrap;
        background-color: transparent;
        border: 0;
    }
</style>
<div class="profile-section">
    <div class="row">
        <div class="col-lg-2">
            <div class="profile-author">
                <a class="profile-author-thumb" href="/">
                    <img alt="author" class="lazy" data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}">
                </a>
                <div class="author-content">
                    <a class="h4 author-name" href="/">{{ @$settings['name'] }}</a>
                </div>
            </div>
        </div>
        <div class="col-lg-10">
            <ul class="profile-menu">
                <li>
                    <a {{!isset($_GET['type'])?'class=active' :''}} href="/tim-kiem?q={{$q}}">Lịch sử bài thi</a>
                </li>
            </ul>
        </div>
    </div>
</div>