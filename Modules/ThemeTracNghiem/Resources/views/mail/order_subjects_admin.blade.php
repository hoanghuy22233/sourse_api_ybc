<div style="max-width:700px;margin:10px auto 20px">
    <div>
        <div style="display:inline-block;margin:0px auto">

            <table style="margin:0 auto;border-collapse:collapse;background-color:#fff;border-radius:10px">
                <tbody>
                <tr>
                    <td style="padding:10px;border-bottom:1px solid #e5e5e5">
                        <table style="width:100%;border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td style="width:140px">
                                    <img class="lazy"
                                         data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo'], null, 150) }}"
                                         style="max-width: 150px; max-height: 150px;">

                                </td>
                                <td style="padding-left:10px;font:16px/1.6 Arial;color:#000;font-weight:bold;text-align:right">
                                    {{@$settings['header_mail']}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>


                <tr>
                    <td style="padding:0 20px">
                        <div style="margin:20px 0 0">
                            <table style="width:660px;border-collapse:collapse;word-break:break-all;box-sizing:border-box; border: 1px solid #ddd">
                                <tbody>
                                <tr>
                                    <td colspan="3"
                                        style="text-align:center;font-weight: bold;font-size: 16px;padding: 10px;">
                                        Thông tin đơn hàng
                                        <br>
                                        <a href="https://{{ env('DOMAIN') }}/admin/bill/{{ $bill->id }}">Xem chi tiết đơn hàng</a>
                                    </td>
                                </tr>
                                <tr style="text-align: center; font-weight: bold; font-size: 14px; border: 1px solid #ddd;">
                                    <td style="border-right:1px solid #ddd">STT</td>
                                    <td style="border-right:1px solid #ddd">Tên môn học</td>
                                </tr>

                                <?php
                                $subjects = \Modules\ThemeTracNghiem\Models\Category::whereIn('id', explode('|', $bill->subjects))->pluck('name');
                                ?>
                                @foreach($subjects as $k => $name)
                                    <tr style="border:1px solid #ddd">
                                        <td style="text-align: center; border-right: 1px solid #ddd ">{{ $k + 1 }}</td>
                                        <td style="text-align: center; border-right: 1px solid #ddd "><a
                                                    href="#">{{ $name }}</a></td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="1"
                                        style="font-weight: bold;font-size: 16px; border-right: 1px solid #ddd; text-align: center;">
                                        Tổng tiền
                                    </td>
                                    <td style="text-align: center; ">{{ number_format($bill->total_price, 0, '.', '.') }}
                                        <sup>đ</sup></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding:0 20px">
                        <div style="margin:20px 0 0">
                            <table style="width:660px;border-collapse:collapse;word-break:break-all;box-sizing:border-box; border: 1px solid #ddd;">
                                <tbody>
                                <tr>
                                    <td colspan="4"
                                        style="text-align:center;font-weight: bold;font-size: 16px;padding: 10px;">Thông
                                        tin khách hàng
                                    </td>
                                </tr>
                                <tr style="text-align: center; font-weight: bold; font-size: 14px; border: 1px solid #ddd;">
                                    <td style="border-right:1px solid #ddd">Tên</td>
                                    <td style="border-right:1px solid #ddd">Email</td>
                                    <td style="border-right:1px solid #ddd">Số điện thoại</td>
                                    <td style="border-right:1px solid #ddd">Ảnh chụp hóa đơn</td>
                                </tr>
                                <tr style="border:1px solid #ddd">
                                    <td style="text-align: center; border-right: 1px solid #ddd ">{{ @$bill->student->name }}</td>
                                    <td style="text-align: center; border-right: 1px solid #ddd ">{{ @$bill->student->email }}</td>
                                    <td style="text-align: center; border-right: 1px solid #ddd ">{{ @$bill->student->phone }}</td>
                                    <td style="text-align: center; border-right: 1px solid #ddd ">
                                        @if(@$bill->image != '')
                                            <img
                                                    src="{{ asset('public/filemanager/userfiles/').@$bill->image }}">
                                        @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="padding:10px 20px;background:#f8f8f8;border-top:1px solid #e5e5e5;border-radius:0 0 10px 10px"
                        id="m_-3744433325623262737m_5338770243025797393m_2723588182153450401m_5561831045043488677vi-VN">
                        <table style="width:100%;border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td style="width:160px;text-align:center">
                                    <img class="lazy"
                                         data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo'], null, 150) }}"
                                         style="max-width: 150px; max-height: 150px;">
                                    {{--<img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($settings['logo'],'false','20%')}}"--}}
                                    {{--class="{{$settings['name']}}">--}}
                                </td>
                                <td style="padding-left:30px;font:15px/1.6 Arial;color:#666">
                                    {{--<span style="font-weight:bold">{{@$settings['name']}}!</span><br>--}}
                                    {{@$settings['footer_mail']}}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>
