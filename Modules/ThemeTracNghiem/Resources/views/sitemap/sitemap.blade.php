<?php echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

    <url>
        <loc>{{ URL::to('/gioi-thieu') }}</loc>
        <lastmod>{{ date("Y-m-d", strtotime('2020-08-01')) }}T{{ date("H:i:s", strtotime('2020-08-01'))}}+07:00
        </lastmod>
        <changefreq>always</changefreq>
        <priority>0.4</priority>
    </url>
    <url>
        <loc>{{ URL::to('/khoa-hoc') }}</loc>
        <lastmod>{{ date("Y-m-d", strtotime('2020-08-01')) }}T{{ date("H:i:s", strtotime('2020-08-01'))}}+07:00
        </lastmod>
        <changefreq>always</changefreq>
        <priority>0.4</priority>
    </url>
    <url>
        <loc>{{ URL::to('/tao-bai-thi') }}</loc>
        <lastmod>{{ date("Y-m-d", strtotime('2020-08-01')) }}T{{ date("H:i:s", strtotime('2020-08-01'))}}+07:00
        </lastmod>
        <changefreq>always</changefreq>
        <priority>0.4</priority>
    </url>

</urlset>

