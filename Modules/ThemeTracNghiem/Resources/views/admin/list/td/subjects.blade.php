<?php $subjects = explode('|', $item->subjects);
$subjects = \Modules\ThemeTracNghiem\Models\Category::whereIn('id', $subjects)->pluck('name');
?>
@foreach($subjects as $v)
    <span class="kt-badge kt-badge--bolder kt-badge kt-badge--inline kt-badge--unified-primary">{!! $v !!}</span>
@endforeach