@extends('themetracnghiem::layouts.default')
@section('tailieu')
    active
@endsection
@section('main_content')
    @include('themetracnghiem::partials.banner_header')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="col-lg-3">
                                    <aside class="sidebar static">
                                        @include('themeedu::pages.document.partials.cate_document')
                                    </aside>
                                </div><!-- sidebar -->
                                <div class="col-lg-9">
                                    <div class="row merged20">
                                        @foreach($posts as $post)
                                            <div class="col-lg-6 col-md-6">
                                                <div class="central-meta">
                                                    <div class="blog-post">
                                                        <div class="friend-info">
                                                            <div class="friend-name">
                                                                <span><i class="fa fa-globe"></i> {{date('d-m-Y',strtotime($post->created_at))}} </span>
                                                            </div>
                                                            <div class="post-meta">
                                                                <figure>
                                                                    <a title="{{@$post->name}}"
                                                                       href="/{{$slug1}}/{{@$post->slug}}.html">
                                                                        <img alt="{{@$post->name}}"
                                                                             class="lazy" data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image,361,200) }}">
                                                                    </a>
                                                                </figure>
                                                                <div class="description">
                                                                    <h2><a href="/{{@$slug1}}/{{$post->slug}}.html"
                                                                           title="">{{@$post->name}}</a></h2>
                                                                    <p>
                                                                        {!!$post->intro!!}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-12 paginatee">
                                            {{ @$posts->appends(Request::all())->links() }}
                                        </div>
                                    </div>
                                </div><!-- centerl meta -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection