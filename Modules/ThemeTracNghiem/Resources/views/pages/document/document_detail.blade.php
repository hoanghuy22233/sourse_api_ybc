@extends('themetracnghiem::layouts.default')
@section('tailieu')
    active
@endsection
@section('main_content')
    @include('themetracnghiem::partials.banner_header')
    <div id="mm-0" class="mm-page mm-slideout">
        <div class="se-pre-con"></div>
        <div class="theme-layout">
            <section>
                <div class="gap2 gray-bg">
                    <div class="container">
                        <div class="post-content">
                            <div class="col-lg-12">
                                <div class="row merged20" id="page-contents">
                                    <div class="col-lg-9">
                                        <div class="post-title">
                                            <h6><i class="fa fa-link"></i> {{@$post->name}}</h6>
                                        </div>
                                        <div class="central-meta item">
                                            <div class="user-post">
                                                <div class="friend-info">
                                                    <div class="friend-name">
{{--                                                        <span>{{date('d-m-Y',strtotime($post->created_at))}}</span>--}}
                                                    </div>
                                                    <div class="post-meta">
                                                        <div class="description">
                                                            <p>
                                                                {!! @$post->content !!}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- centerl meta -->
                                    <div class="col-lg-3">
                                        <aside class="sidebar static right">
                                            <a class="btn btn-danger" title="Luyện thi ngay" href="/tao-bai-thi" data-ripple="" style="margin-bottom: 20px;">Luyện Thi Ngay</a>

                                            @include('themeedu::pages.document.partials.cate_document')
                                            <div class="widget">
                                            </div><!-- twitter feed-->
                                            <div class="widget">
                                                @include('themetracnghiem::partials.widget_right')
                                            </div>
                                        </aside>
                                    </div><!-- sidebar -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
@section('head_script')
    <style>
        .post-meta .description > p {
            width: unset;
            height: unset;
        }
        b,strong {
            line-height: 1;
        }
    </style>
@endsection