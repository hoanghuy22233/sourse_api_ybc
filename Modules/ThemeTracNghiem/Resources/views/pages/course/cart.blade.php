@extends('themetracnghiem::layouts.default')
@section('main_content')
    @include('themetracnghiem::partials.banner_header')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="col-lg-12">
                                    <form class="central-meta" action="" method="POST">
                                        {!! csrf_field() !!}
                                        <h4 class="create-post">Mua khóa học</h4>
                                        <div class="cart-sec">
                                            <span class="alert alert-success">Số dư tài khoản của bạn là : {{ number_format(\Auth::guard('student')->user()->balance, 0, '.', '.') }}đ</span>
                                            <?php
                                            $subjects = \Modules\ThemeTracNghiem\Models\Category::select(['name', 'id'])->where('type', 10)->where('status', 1)->get();
                                            ?>
                                            <table class="table table-responsive">
                                                <tbody>
                                                <tr>
                                                    <td style="width: 114px;">Chọn gói</td>
                                                    <td style="width: 114px;">Giá</td>
                                                    <td>Chọn môn</td>
                                                </tr>
                                                <tr>
                                                    <td>1 Môn</td>
                                                    <td>99 VNĐ</td>
                                                    <td>
                                                        <div>
                                                            @foreach($subjects as $subject)
                                                                <div class=" select-object">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input data-max="1" type="checkbox" value="{{ $subject->id }}" name="subjects[]" class="chose-subject"><i
                                                                                    class="check-box"></i>
                                                                            {{ $subject->name }}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3 Môn</td>
                                                    <td>249 VNĐ</td>
                                                    <td>
                                                        <div>
                                                            @foreach($subjects as $subject)
                                                                <div class=" select-object">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input data-max="3" type="checkbox" value="{{ $subject->id }}" name="subjects[]" class="chose-subject"><i
                                                                                    class="check-box"></i>
                                                                            {{ $subject->name }}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>5 349</td>
                                                    <td>249 VNĐ</td>
                                                    <td>
                                                        <div>
                                                            @foreach($subjects as $subject)
                                                                <div class=" select-object">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input data-max="5" type="checkbox" value="{{ $subject->id }}" name="subjects[]" class="chose-subject"><i
                                                                                    class="check-box"></i>
                                                                            {{ $subject->name }}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="proceed" style="margin-top: 30px;">
                                                    <button type="submit" title="" class="main-btn">Đăng ký ngay</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->

    </div>
    <script>
        $(document).ready(function () {
            $('.chose-subject').change(function () {
                if(this.checked) {
                    var val = $(this).val();
                    $(this).parents('tr').siblings().find('.chose-subject').prop('checked', false);

                    var count_cheked = $('.chose-subject:checked').length;
                    var max_cheked = $(this).data('max');
                    if (max_cheked < count_cheked) {
                        alert('Bạn đã chọn tối đa ' + max_cheked + ' môn');
                        $(this).prop('checked', false);
                    }
                }
            });
        });
    </script>

@endsection
@section('head_script')
    <style>
        .select-object {
            display: inline-block;
            margin-right: 15px;
        }
    </style>
@endsection