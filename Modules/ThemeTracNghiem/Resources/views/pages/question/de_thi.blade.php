@extends('themetracnghiem::layouts.default')
@section('tailieu')
    active
@endsection
@section('main_content')
    @include('themetracnghiem::partials.banner_header')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row merged20" id="page-contents">
                        <div class="col-lg-4 col-md-4">
                            <aside class="sidebar">
                                <div class="central-meta stick-widget bang-dieu-khien">
                                    <span class="create-post">Đề thi môn {{ @$exam->subject->name }}</span>
                                    <div class="personal-head">
                                        <span class="f-title"><i class="fa fa-user"></i> Phần thi đã chọn:</span>
                                        <p>
                                            Chương: {{ @$exam->chapter->name == '' ? 'Tất cả chương' : @$exam->chapter->name }}
                                        </p>
                                        <p>
                                            Chủ
                                            đề: {{ @$exam->thematic->name == '' ? 'Tất cả chủ đề' : $exam->thematic->name }}
                                        </p>
                                        <span class="f-title" style="color: red; font-size: 16px;"><i
                                                    class="fa fa-birthday-cake"></i> <span
                                                    id="deadline_time"></span></span>
                                        <hr>
                                        <div class="cart-sec">
                                            <?php $k = 0;?>
                                            @foreach($questions as $k => $question)
                                                    <?php $k++;?>
                                                    <div class="exam_item exam_item-{{ $k }}" data-key="{{ $k }}"
                                                         data-id="{{ $question->id }}">
                                                        {{ $k }} {{ strtoupper(@$exam_data[$question->id]) }}
                                                    </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </aside>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <form class="central-meta" action="" method="POST">
                                {!! csrf_field() !!}
                                <span class="create-post">Nội dung đề thi</span>
                                <div class="row">
                                    <div class="col-lg-12">
                                        @foreach($questions as $k => $question)
                                            <div class="gen-metabox" id="question-{{ $question->id }}">
                                                <span style="color: red;">Câu {{ $k + 1 }}:</span>
                                                <p>{!! $question->question !!}</p>
                                                <div class="col-md-12 select-radio">
                                                    <div class="form-radio">
                                                        <div class="col-xs-12 radio">
                                                            <label data-id="{{ $k + 1 }}">
                                                                <input type="radio" name="dapan[{{ $question->id }}]"
                                                                       value="a"><i
                                                                        class="check-box"></i><span
                                                                        style="color: blue;">A:</span> {!! $question->a !!}
                                                            </label>
                                                        </div>
                                                        <div class="col-xs-12 radio">
                                                            <label data-id="{{ $k + 1 }}">
                                                                <input type="radio" name="dapan[{{ $question->id }}]"
                                                                       value="b"><i
                                                                        class="check-box"></i><span
                                                                        style="color: blue;">B:</span> {!! $question->b !!}
                                                            </label>
                                                        </div>
                                                        <div class="col-xs-12 radio">
                                                            <label data-id="{{ $k + 1 }}">
                                                                <input type="radio" name="dapan[{{ $question->id }}]"
                                                                       value="c"><i
                                                                        class="check-box"></i><span
                                                                        style="color: blue;">C:</span> {!! $question->c !!}
                                                            </label>
                                                        </div>
                                                        <div class="col-xs-12 radio">
                                                            <label data-id="{{ $k + 1 }}">
                                                                <input type="radio" name="dapan[{{ $question->id }}]"
                                                                       value="d"><i
                                                                        class="check-box"></i><span
                                                                        style="color: blue;">D:</span> {!! $question->d !!}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                @if($can_edit)
                                    <div>
                                        <button class="main-btn" type="submit" data-ripple="">Nộp bài</button>
                                    </div>
                                @endif
                                <div id="expired" style="display: none;">
                                    <span class="alert alert-danger">Đã hết giờ làm bài. Vui lòng nộp bài để chấm điểm!</span>
                                    <div class="col-xs-12" style="text-align: center; width: 100%;">
                                        <button class="main-btn" type="submit" data-ripple="" style="float: none;">Nộp
                                            bài
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        window.onbeforeunload = function () {
            return "Tải lại trang web sẽ làm mất các câu bạn đã trả lời. Bạn có chắc chắn muốn tải lại?";
        };

        <?php
        $m = (isset($exam->subject->time) && $exam->subject->time != null) ? $exam->subject->time : 90;
        $deadline = strtotime($exam->created_at) + $m * 60 /*- 80*60*/
        ;
        ?>
        // Set the date we're counting down to
        var countDownDate = new Date("{{ date('M', $deadline) }} {{ date('d', $deadline) }}, {{ date('Y', $deadline) }} {{ date('H:i:s', $deadline) }}").getTime();

        // Update the count down every 1 second
        var x = setInterval(function () {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="deadline_time"
            document.getElementById("deadline_time").innerHTML = hours + ":"
                + minutes + ":" + seconds + "";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("deadline_time").innerHTML = "Hết giờ";
                $('#expired').css('display', 'block');
            }
        }, 1000);

        //  Click chọn đáp án nào bên trái sẽ tự điền vào bên phải
        $(document).ready(function () {
            $('.select-radio label').click(function () {
                var id = $(this).data('id');
                var key = $('.exam_item-' + id).data('key');
                $('.exam_item-' + id).html(key + '. ' + $(this).find('input').val().toUpperCase());
                $('.exam_item-' + id).addClass('active');
            });
        });
    </script>
@endsection
@section('head_script')
    @include('themetracnghiem::pages.question.partials.de_thi_css')
    <style>

        div#expired {
            position: fixed;
            width: 100%;
            left: 0;
            top: 0;
            height: 100%;
            padding-top: 40%;
            text-align: center;
            background: rgba(117, 128, 138, 0.7);
        }


    </style>
@endsection
@section('footer_script')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/latest.js?config=TeX-MML-AM_CHTML' async></script>
@endsection