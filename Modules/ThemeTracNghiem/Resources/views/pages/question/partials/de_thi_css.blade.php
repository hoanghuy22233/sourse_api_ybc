<style>
    .radio .check-box {
        top: 0;
    }
    .exam_item.active {
        font-weight: bold;
        color: #242c66;
        background: #d3e0ff;
    }

    .cart-sec table tr td {
        font-size: 12px;
        padding: 10px;
        font-weight: bold;
    }

    .exam_item {
        width: 25%;
        display: inline-block;
        float: left;
        border: 1px solid #ccc;
        padding: 5px;
    }

    .red {
        color: red !important;
        background: unset;
    }

    .ten-cau {
        font-size: 16px;
    }

    .form-radio .radio {
        width: 100%;
    }

    .form-radio .radio > label {
        color: #000;
    }

    .exam_item {
        text-align: center;
        cursor: pointer;
    }

    .exam_item.dung {
        background: #75f565;
        color: black;

    }

    .exam_item.sai {
        background: red;
        color: white;

    }

    .f-title {
        font-weight: bold !important;
    }

    .personal-head > p {
        color: #000;
    }
    .form-radio .radio label p {
        display: inline-block;
    }
    .gen-metabox p {
        color: #000;
        font-size: 16px;
    }
    .bang-dieu-khien {
        z-index: 9999;
    }
    .sidebar {
        display: inline-block;
        float: none;
        margin: 0 auto;
        width: 100%;
    }

    @media(max-width: 768px) {
        .personal-head .cart-sec {
            display: none;
        }
    }
    @media all and (min-width: 320px) {
        .bang-dieu-khien {
            z-index: 0;
        }
    }
    @media(min-width: 768px) {
        .topbar.transperent.stick {
            padding: 0;
            padding-left: 16px;
        }
        .cart-sec {
            max-height: 414px;
            overflow-y: scroll;
        }
    }
</style>
<script>
    $(document).ready(function () {
        $('.exam_item').click(function () {
            var question_id = $(this).data('id');
            $('html,body').animate({
                scrollTop: $('#question-' + question_id).offset().top - 100
            }, 'slow');
        });
    });
</script>