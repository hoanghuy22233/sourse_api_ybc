@extends('themetracnghiem::layouts.default')
@section('tailieu')
    active
@endsection
@section('main_content')
    @include('themetracnghiem::partials.banner_header')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row merged20" id="page-contents">
                        <div class="col-lg-4 col-md-4">
                            <aside class="sidebar">
                                <div class="central-meta stick-widget bang-dieu-khien">
                                    <span class="create-post">Đáp án đề thi môn {{ @$exam->subject->name }}</span>
                                    <div class="personal-head">
                                        <span class="f-title"><i class="fa fa-user"></i> Phần thi đã chọn:</span>
                                        <p>
                                            {{ @$exam->chapter->name }}
                                        </p>
                                        <p>
                                            {{ @$exam->thematic->name }}
                                        </p>
                                        <span class="f-title"><i class="fa fa-birthday-cake"></i> 90:00</span>
                                        <hr>
                                        <div class="cart-sec">
                                            <?php
                                            $count_question_true = 0;
                                            ?>
                                            @foreach($questions as $k => $question)
                                                <div class="exam_item @if(@$exam_data[$question->id] != null && $question->answer != strtolower(@$exam_data[$question->id])) sai @elseif($question->answer == strtolower(@$exam_data[$question->id])) <?php $count_question_true++;?> dung @endif"
                                                     data-id="{{ $question->id }}">
                                                    {{ $k + 1 }}. {{ strtoupper(@$exam_data[$question->id]) }}
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </aside>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <div class="central-meta" action="" method="POST">
                                {!! csrf_field() !!}
                                <span class="create-post">Nội dung đề thi
                                    <span style="float: right;">
                                        Đúng: {{ number_format($count_question_true, 0, '.', '.') }}/{{ number_format(count($questions), 0, '.', '.') }}
                                    </span>
                                </span>
                                <div class="row">
                                    <div class="col-lg-12">
                                        @foreach($questions as $k => $question)
                                            <div class="gen-metabox" data-q-id="{{ $question->id }}"
                                                 id="question-{{ $question->id }}">
                                                <span class="red ten-cau">Câu {{ $k + 1 }}:</span>
                                                <p>{!! $question->question !!}</p>
                                                <div class="col-md-12 select-radio">
                                                    <div class="form-radio">
                                                        <div class="col-xs-12 radio">
                                                            <label>
                                                                <input type="radio" name="dapan[{{ $question->id }}]"
                                                                       {{ $exam_data[$question->id] == 'a' ? 'checked' : '' }}
                                                                       value="a"><i
                                                                        class="check-box {{ @$exam_data[$question->id] != null && $exam_data[$question->id] != strtolower($question->answer) && $exam_data[$question->id] == 'a' ? 'sai' : '' }}"></i><span
                                                                        style="color: blue;">A:</span> {!! $question->a !!}
                                                            </label>
                                                        </div>
                                                        <div class="col-xs-12 radio">
                                                            <label>
                                                                <input type="radio" name="dapan[{{ $question->id }}]"
                                                                       {{ $exam_data[$question->id] == 'b' ? 'checked' : '' }}
                                                                       value="b"><i
                                                                        class="check-box {{ @$exam_data[$question->id] != null && $exam_data[$question->id] != strtolower($question->answer) && $exam_data[$question->id] == 'b' ? 'sai' : '' }}"></i><span
                                                                        style="color: blue;">B:</span> {!! $question->b !!}
                                                            </label>
                                                        </div>
                                                        <div class="col-xs-12 radio">
                                                            <label>
                                                                <input type="radio" name="dapan[{{ $question->id }}]"
                                                                       {{ $exam_data[$question->id] == 'c' ? 'checked' : '' }}
                                                                       value="c"><i
                                                                        class="check-box {{ @$exam_data[$question->id] != null && $exam_data[$question->id] != strtolower($question->answer) && $exam_data[$question->id] == 'c' ? 'sai' : '' }}"></i><span
                                                                        style="color: blue;">C:</span> {!! $question->c !!}
                                                            </label>
                                                        </div>
                                                        <div class="col-xs-12 radio">
                                                            <label>
                                                                <input type="radio" name="dapan[{{ $question->id }}]"
                                                                       {{ $exam_data[$question->id] == 'd' ? 'checked' : '' }}
                                                                       value="d"><i
                                                                        class="check-box {{ @$exam_data[$question->id] != null && $exam_data[$question->id] != strtolower($question->answer) && $exam_data[$question->id] == 'd' ? 'sai' : '' }}"></i><span
                                                                        style="color: blue;">D:</span> {!! $question->d !!}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="dap_an">
                                                    <strong>Đáp án chính
                                                        xác: {{ strtoupper($question->answer) }}</strong>
                                                    <p>{!! $question->answer_detail !!}</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-5 pr-0 "><p class="btn"
                                                                                   style="border: 1px solid grey">ID câu
                                                            hỏi: {{ $question->id }}</p></div>
                                                    <div class="col-md-7 load-error">
                                                        <button type="button" class="btn  view-data"
                                                                style="border: 1px solid grey" id="{{$question->id}}"
                                                                data-toggle="modal" data-target="#modalReport">
                                                            Báo cáo lỗi
                                                        </button>
                                                    </div>
                                                </div>
                                                <span></span>

                                            </div>
                                            <hr>
                                        @endforeach
                                    </div>
                                </div>
                                <div id="modalReport" class="modal fade g" tabindex="-1" role="dialog">
                                    <div class="modal-dialog modal-lg" style="max-height:50%;color: #0a001f"
                                         role="document">
                                        <form action="">
                                            <div class="modal-content">
                                                <div class="modal-header" style="display: initial">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close"><span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h4 class="modal-title " id="info"></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 ">
                                                            <label>Lý do</label>
                                                            <select class="form-control" id="reason">
                                                                <option value="0" selected>Chọn lý do</option>
                                                                <option value="Sai lỗi chính tả">Sai lỗi chính tả
                                                                </option>
                                                                <option value="Không có nội dung câu hỏi">Không có nội
                                                                    dung câu hỏi
                                                                </option>
                                                                <option value="Không có nội dung câu trả lời">Không có
                                                                    nội dung câu trả lời
                                                                </option>
                                                                <option value="Đề sai đáp án">Đề sai đáp án</option>
                                                                <option value="Hình ảnh hiển thị không đúng">Hình ảnh
                                                                    hiển thị không đúng
                                                                </option>
                                                                <option value="Không hiển thị nội dung">Không hiển thị
                                                                    nội dung
                                                                </option>
                                                                <option value="Lỗi khác">Lỗi khác</option>
                                                            </select>
                                                            <textarea name="textarea" style="resize: vertical;"
                                                                      id="error_content"
                                                                      rows="5" cols="30"
                                                                      placeholder="Mô tả chi tiết lỗi sai"></textarea>

                                                        </div>

                                                        <div class="col-lg-6 col-md-6">
                                                            <label>Với mong muốn không ngừng nâng cao chất lượng ngân
                                                                hàng đề thi hiện nay, chúng tôi hy vọng sẽ nhận được
                                                                nhiều phản hồi tích cực từ các em học sinh. Để cùng đóng
                                                                góp xây dựng 1 kho tài liệu chất lượng nhất để mọi người
                                                                cùng ôn luyện tốt nhất cho kì thi THPT sắp tới!
                                                                Xin trân trọng cảm ơn!
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer"
                                                     style=" display: flex;justify-content: center;">
                                                    <button type="button" id="submit_error" class="btn btn-success">
                                                        Gửi
                                                    </button>
                                                </div>
                                            </div>
                                        </form>

                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            <div>
                                <a href="/tao-bai-thi" class="main-btn" type="submit" data-ripple="">Luyện đề mới</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
    </div>
@endsection
@section('head_script')
    @include('themetracnghiem::pages.question.partials.de_thi_css')
    <style>
        .ten-cau {
            font-weight: 800 !important;
        }

        .dap_an strong {
            text-align: center;
            width: 100%;
            display: inline-block;
            margin-top: 10px;
            color: blue;
        }

        i.sai::before,
        i.sai::after {
            color: red !important;
        }

        .f-title {
            font-weight: bold !important;
        }

        .form-radio .radio label p {
            display: inline-block;
        }

        .modal-body {
            max-height: 500px;
            overflow-y: auto;
        }

        .chosen-container-single .chosen-single div b {
            background: url(../frontend/themes/edu-ldp/images/dropdown-512.png) no-repeat 0 2px !important;
        }

        .chosen-container-single .chosen-search input[type=text] {
            background: #fff;
        }

        .nav-list > li > a {
            color: white;
        }
    </style>
@endsection
@section('footer_script')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/latest.js?config=TeX-MML-AM_CHTML' async></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script>
        //quick view modal
        $(document).ready(function () {
            $('.view-data').click(function () {
                var prd_id = $(this).attr("id");
                $("#info").text("");
                $("#info").append("<i class=\"fas fa-eye\"></i> Báo lỗi đề thi câu hỏi số " + prd_id);
                $("#submit_error").attr("id_question", prd_id);
            });

            $(document).on('click', '#submit_error', function () {
                var prd_id = $("#submit_error").attr("id_question");
                var reason = $("#reason").val();
                var content = $("#error_content").val();
                // console.log(content);
                // $.ajaxSetup({
                //     headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
                // });
                $.ajax({
                    url: '{{route('bao-cao-loi')}}',
                    method: "post",
                    data: {
                        _token: '{{csrf_token()}}',
                        "prd_id": prd_id,
                        "reason": reason,
                        "content": content
                    },
                    success: function (data) {
                        if (data.status) {
                            $('#modalReport').modal('hide')
                            alert('Đã gửi báo cáo lỗi');

                        }
                    }, error: function (error) {
                        $('#modalReport').modal('hide')
                        console.log(error);
                    },

                });
            });
        });
    </script>
@endsection