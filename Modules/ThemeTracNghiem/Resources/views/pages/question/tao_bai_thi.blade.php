@extends('themetracnghiem::layouts.default')
@section('tailieu')
    active
@endsection
@section('main_content')
    @include('themetracnghiem::partials.banner_header')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="central-meta">
                            <h4 class="create-post">Ôn luyện theo chủ đề</h4>
                            <form method="post" class="c-form" id="tao-bai-thi">
                                <div class="col-md-3 select-item">
                                    <label>Chọn môn</label>
                                    <?php
                                    $subjects = \Modules\ThemeTracNghiem\Models\Category::where('type', 10)->pluck('name', 'id');

                                    //  Nếu đã đăng nhập thì kiểm tra các môn đã mua được phép thi, còn chưa đăng nhập thì được tạo bất kỳ môn
                                    if (\Auth::guard('student')->check()) {
                                        $subject_registered = explode('|', @\Auth::guard('student')->user()->subjects);
                                    } else {
                                        $subject_registered = true;
                                    }
                                    ?>
                                    <select class="form-control" name="subject_id" required>
                                        <option>Chọn môn</option>
                                        @foreach($subjects as $id => $name)
                                            <option value="{{ $id }}" {{ @$_GET['subject_id'] == $id ? 'selected' : '' }}  {{ $subject_registered === true || in_array($id, $subject_registered) ? 'data-buyed="true"' : 'data-buyed="false"' }}>{{ $name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 select-item">
                                    <label>Chọn chương</label>
                                    <select class="form-control" name="chapter_id">
                                        <option value="">Tất cả chương</option>
                                        <option value="">Vui lòng chọn môn trước</option>
                                    </select>
                                </div>
                                <div class="col-md-3 select-item">
                                    <label>Chọn chuyên đề</label>
                                    <select class="form-control" name="thematic_id">
                                        <option value="">Tất cả chủ đề</option>
                                        <option value="">Vui lòng chọn chương trước</option>
                                    </select>
                                </div>
                                <div class="col-md-12 select-radio">
                                    <label>Mức độ</label>
                                    <div class="form-radio">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="level" checked="checked" value="7"><i
                                                        class="check-box"></i>5 - 7+
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="level" value="8"><i class="check-box"></i>8+
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="level" value="9"><i class="check-box"></i>9+
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <button class="main-btn btn-tao-bai-thi" type="button" data-ripple="">Thi Ngay</button>
                                    <span id="error_buyed" style="display: none; " class="alert alert-danger">Môn này chưa được mua. Vui lòng <a href="/dang-ky-mon" style="    text-decoration: underline;color: #007bff;">mua khóa học tại đây</a></span>
                                </div>

                                <script>
                                    $('.btn-tao-bai-thi').click(function () {
                                        var buyed = $('select[name=subject_id] option:selected').data('buyed');

                                        if (buyed.indexOf("false") == 1) {
                                            $('#error_buyed').addClass('show');
                                        } else {
                                            $('form#tao-bai-thi').submit();
                                        }
                                    });

                                    $(document).ready(function () {
                                        changeChapter();

                                        $('body').on('change', 'select[name=subject_id]', function () {
                                            changeChapter();
                                        });

                                        $('body').on('change', 'select[name=chapter_id]', function () {
                                            changeThematic();
                                        });

                                        function changeChapter() {
                                            var subject_id = $('select[name=subject_id]').val();
                                            $.ajax({
                                                url: '/admin/question/chapter/get-data',
                                                type: 'GET',
                                                data: {
                                                    subject_id: subject_id,
                                                },
                                                success: function (resp) {
                                                    var data = resp.data;
                                                    var html = '<option value="">Tất cả chương</option>';
                                                    Object.keys(data).map(function (key) {
                                                        html += '<option value="' + key + '">' + data[key] + '</option>';
                                                    });

                                                    $('select[name=chapter_id]').html(html);
                                                    changeThematic();
                                                },
                                                error: function () {
                                                    alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại');
                                                }
                                            });
                                        }

                                        function changeThematic() {
                                            var chapter_id = $('select[name=chapter_id]').val();
                                            $.ajax({
                                                url: '/admin/question/thematic/get-data',
                                                type: 'GET',
                                                data: {
                                                    chapter_id: chapter_id,
                                                },
                                                success: function (resp) {
                                                    var data = resp.data;
                                                    var html = '<option value="">Tất cả chủ đề</option>';
                                                    Object.keys(data).map(function (key) {
                                                        html += '<option value="' + key + '">' + data[key] + '</option>';
                                                    });
                                                    $('select[name=thematic_id]').html(html);
                                                },
                                                error: function () {
                                                    alert('Có lỗi xảy ra! Vui lòng load lại trang và thử lại');
                                                }
                                            });
                                        }


                                    });
                                </script>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('head_script')
    <style>
        .select-item {
            display: inline-block;
        }

        .select-radio {
            margin-top: 15px;
        }

        .chosen-container {
            display: none !important;
        }

        select {
            display: block !important;
        }

        #error_buyed.show {
            display: inline-block !important;
        }
    </style>
@endsection