@extends('themetracnghiem::layouts.default')
@section('diem-thi')
    active
@endsection
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        @include('themetracnghiem::template.top_bar')
        <section style="min-height: 850px;background-color: #edf2f6;">
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themetracnghiem::partials.student_menu', ['user' => $user])
                                </div>
                                <div class="col-lg-12 single_lesson_content">
                                    <div class="central-meta">
                                        <div class="bg-white">
                                        </div>
                                        <div class="quiz_list">
                                            <table class="table table-striped" style="background-color: white">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tên</th>
                                                    <th>Lớp</th>
                                                    <th>Đường dẫn</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                        $classs=\Modules\EduCourse\Models\Classs::all();
                                                ?>
                                                @foreach($classs as $class)
                                                    <?php
                                                    $points = \Modules\EduCourse\Models\Point::where('class_id',$class->id)->orderBy('id','desc')->get();
                                                    ?>
                                                    @foreach($points as $point)
                                                    <tr>
                                                    <th scope="row">{{@$k+=1}}</th>
                                                    <td>{{@$point->class->name}}</td>
                                                    <td>{{@$point->name}}</td>
                                                    <td><a class=""
                                                    href="{{@$point->link}}">Xem</a>
                                                    </td>
                                                    </tr>
                                                @endforeach
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection
@section('head_script')
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/profile.css') }}?v={{ time() }}">
    <style>
        .quiz_list {
            overflow: scroll;
        }
    </style>
@endsection