@extends('themetracnghiem::layouts.default')
@section('diem-thi')
    active
@endsection
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        @include('themetracnghiem::template.top_bar')
        <section style="min-height: 850px;background-color: #edf2f6;">
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themetracnghiem::partials.student_menu', ['user' => $user])
                                </div>
                                <div class="col-lg-12 single_lesson_content">
                                    <div class="central-meta">
                                        <div class="bg-white">
                                        </div>
                                        <div class="quiz_list">
                                            <table class="table table-striped" style="background-color: white">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Môn</th>
                                                    <th>Chương</th>
                                                    <th>Chuyên đề</th>
                                                    <th>Độ khó</th>
                                                    <th>Ngày thi</th>
                                                    <th>Điểm</th>
                                                    <th>Lời giải</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $exams = \Modules\ThemeTracNghiem\Models\Exam::where('student_id', Auth::guard('student')->user()->id)->orderBy('id', 'desc')->paginate(30);
                                                $level = [
                                                    7 => '5 - 7+',
                                                    8 => 8,
                                                    9 => 9
                                                ];
                                                ?>
                                                @foreach($exams as $k => $exam)
                                                    <tr>
                                                        <th scope="row">{{@$k+=1}}</th>
                                                        <td>{{ @$exam->subject->name }}</td>
                                                        <td>{{ @$exam->chapter->name }}</td>
                                                        <td>{{ @$exam->thematic->name }}</td>
                                                        <td>{{ @$level[$exam->level] }}</td>
                                                        <td>{{ date('d/m/Y H:i', strtotime($exam->created_at)) }}</td>
                                                        <td>{{ number_format($exam->point) }}</td>
                                                        <td><a class=""
                                                               href="/dap-an/{{ @$exam->id }}">Xem</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                {!! $exams->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection
@section('head_script')
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/profile.css') }}?v={{ time() }}">
    <style>
        .quiz_list {
            overflow: scroll;
        }
    </style>
@endsection