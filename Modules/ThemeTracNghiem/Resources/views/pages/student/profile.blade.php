@extends('themetracnghiem::layouts.default')
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
    @include('themetracnghiem::template.top_bar')
    <!-- topbar -->
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themetracnghiem::partials.student_menu', ['user' => $user])
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="central-meta">

                                        <span class="create-post">Giới thiệu</span>
                                        <div class="personal-head">
                                            {{--<span class="f-title"><i class="fa fa-user"></i> Về tôi:</span>--}}
                                            {{--<p>--}}
                                            {{--Học viên gương mẫu, thấm nhuần tư tưởng của Đảng--}}
                                            {{--</p>--}}
                                            <span class="f-title"><i class="fa fa-birthday-cake"></i> Ngày sinh:</span>
                                            <p>
                                                @if(@$user->birthday != '' && @$user->birthday != null)
                                                    {{date('d-m-Y',strtotime(@$user->birthday))}}
                                                @endif
                                            </p>
                                            <span class="f-title"><i class="fa fa-phone"></i> Điện thoại:</span>
                                            <p>
                                                {{@$user->phone}}
                                            </p>
                                            <span class="f-title"><i class="fa fa-male"></i> Giới tính:</span>
                                            <p>
                                                @if(@$user->gender==1)
                                                    Nam
                                                @else
                                                    Nữ
                                                @endif
                                            </p>
                                            <span class="f-title"><i class="fa fa-globe"></i> Địa chỉ:</span>
                                            <p>
                                                {{@$user->address}}
                                            </p>
                                            <span class="f-title"><i class="fa fa-envelope"></i> Email:</span>
                                            <p>
                                                <a href="mailto:{{@$user->email}}"
                                                   class="__cf_email__"
                                                   data-cfemail="cb9ba2bfa5a2a08bb2a4beb9a6aaa2a7e5a8a4a6">
                                                    {{@$user->email}}
                                                </a>
                                            </p>
                                            <span class="f-title"><i class="fa fa-location-arrow"></i> Trường THPT:</span>
                                            <p>
                                                {{ @$user->school }}
                                            </p>
                                            @if($user->id == @\Auth::guard('student')->user()->id)
                                                <a href="/profile/edit"
                                                   style="    color: #007bff;text-decoration: underline; cursor: pointer;">Chỉnh
                                                    sửa profile</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8">
                                    <div class="central-meta">
                                        <span class="create-post">Số dư tài khoản: {{ number_format($user->balance, 0, '.', '.') }}đ
                                        </span>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <a class="btn btn-primary btn-nap-tien" href="/bai-viet/huong-dan-nap-tien.html">Nạp tiền</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="central-meta">
                                        <span class="create-post">Môn học đã mua <a href="/khoa-hoc" style="font-size: 12px;"
                                                                             title="Xem thông tin Môn học">Xem thông tin Môn học</a></span>
                                        <ul class="suggested-frnd-caro">
                                            <?php
                                            $data = CommonHelper::getFromCache('subject_of_student_' . $user->id, ['subjects', 'students']);
                                            if (!$data) {
                                                $data = \Modules\ThemeTracNghiem\Models\Category::whereIn('id', explode('|', $user->subjects))->get();
                                                CommonHelper::putToCache('subject_of_student_' . $user->id, $data, ['subjects', 'students']);
                                            }
                                            ?>
                                            @foreach($data as $v)
                                                <li >
                                                    <img class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image,210,129) }}"
                                                         alt="{{@$v->name}}">
                                                    <div class="sugtd-frnd-meta">
                                                        <a  href="/tao-bai-thi?subject_id={{ $v->id }}"
                                                           title="{{@$v->name}}">{{ @$v->name }}</a>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <div style="    margin-top: 10px;display: inline-block; width: 100%; text-align: center;">
                                            <a class="btn btn-primary" href="/dang-ky-mon">Mua khóa học</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection
@section('head_script')
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/profile.css') }}">
    <style>
        @media(min-width: 768px) {
            .btn-nap-tien {
                float: right;
            }
        }

    </style>
@endsection