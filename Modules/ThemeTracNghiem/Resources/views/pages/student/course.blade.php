@extends('themetracnghiem::layouts.default')
@section('khoahoc')
    class="active"
@endsection
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <style>
            .name_contact {
                margin-left: 5%;
                margin-top: 5%;
                width: 90%;
                height: 50%;
            }

            .mess_contact {
                margin-left: 5%;
                margin-top: 5%;
                width: 90%;
                height: 100%;
            }

            .input_contact {
                height: 45px;
            }

            .input_mess_contact {
                height: 130px;
            }
        </style>

{{--        @include('themeedu::template.top_bar')--}}
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themetracnghiem::partials.student_menu')
                                </div><!-- user profile banner  -->
                                <div class="col-lg-12">
                                    <div class="row merged20">
                                        <?php
                                        $data = CommonHelper::getFromCache('course_of_student_8_' . $user->id, ['courses']);
                                        if (!$data) {
                                            $data = \Modules\EduCourse\Models\Course::join('orders', 'orders.course_id', '=', 'courses.id')
                                                ->selectRaw('courses.*')
                                                ->where('orders.status', 1)->where('orders.student_id', $user->id)->orderBy('orders.created_at', 'desc')->paginate(8);
                                            CommonHelper::putToCache('course_of_student_8_' . $user->id, $data, ['courses']);
                                        }

                                        ?>
                                        @foreach($data as $v)
                                            <div class="col-lg-6 col-md-6">
                                                <div class="central-meta item">
                                                    <div class="classi-pst">
                                                        <div class="row merged10">
                                                            <div class="col-lg-6">
                                                                <div class="image-bunch single">
                                                                    <figure><img
                                                                                class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$v->image,244,122) }}"
                                                                                alt="{{@$course->name}}"></figure>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="classi-pst-meta">
                                                                    <h6><a href="/khoa-hoc/{{(@$v->slug)}}.html"
                                                                           title="{{@$v->name}}">{{@$v->name}}</a></h6>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <p class="classi-pst-des">
                                                                    {!!@$v->intro!!}
                                                                </p>
                                                                <a class="active btn btn-info"
                                                                   href="/khoa-hoc/{{(@$v->slug)}}.html"
                                                                   style="background-color: #fa6342;float: right;border-color: #bc482e;">Học
                                                                    ngay</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-12 paginatee">
                                            {{ $data->appends(Request::all())->links() }}
                                        </div>
                                    </div>
                                </div><!-- centerl meta -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection
@section('head_script')
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/profile.css') }}?v={{ time() }}">
@endsection