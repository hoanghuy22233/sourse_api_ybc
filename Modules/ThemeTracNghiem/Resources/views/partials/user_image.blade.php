<div class="user-img">
    <h5 style="margin-bottom: 5px;">{{@ \Auth::guard('student')->user()->name }}</h5>
    <img style="margin-bottom: 10px;"
         class="lazy" data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb(\Auth::guard('student')->user()->image,45,45) }}"
         alt="{{ @\Auth::guard('student')->user()->name }}">
    <span class="status f-online"></span>
    <div class="user-setting">
        <ul class="log-out">
            <li><a href="/profile" title=""><i class="ti-user"></i>Xem profile</a></li>
            <li><a href="/profile/edit" title=""><i class="ti-pencil-alt"></i>Sửa profile</a></li>
            <hr style="margin-bottom: 0px;margin-top: 10px;">
            <li><a href="/khoa-hoc" title=""><i class="fa fa-graduation-cap"></i>Thông tin khóa học</a>
            </li>
            <li><a href="/dang-ky-mon" title=""><i class="fa fa-book"></i>Mua khóa học</a>
            </li>
            <hr style="margin-bottom: 0px;margin-top: 10px;">
            <li><a href="/bai-viet/huong-dan-nap-tien.html" title=""><i class="fa fa-address-card-o"></i>Nạp tiền</a>
            </li>
            <li><a href="/student/{{ @\Auth::guard('student')->user()->id }}/bai-thi" title=""><i class="fa fa-address-card-o"></i>Lịch sử bài thi</a>
            </li>
            <hr style="margin-bottom: 0px;margin-top: 10px;">
            <li><a href="/dang-xuat" title=""><i class="ti-power-off"></i>Đăng xuất</a></li>
        </ul>
    </div>
</div>

<script>
    $('.user-img').hover(
        function () {
            $('.user-setting').addClass('active');
        },
        function () {
            $('.user-setting').removeClass('active');
        }
    );
</script>