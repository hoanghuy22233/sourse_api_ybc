<?php
$widgets = CommonHelper::getFromCache('widgets_location_home_sidebar_left', ['widgets']);
if (!$widgets) {
    $widgets = \Modules\ThemeTracNghiem\Models\Widget::select(['name', 'content'])
        ->where('location', 'home_sidebar_left')->where('status', 1)
        ->orderBy('order_no', 'desc')->orderBy('id', 'desc')->get();
    CommonHelper::putToCache('widgets_location_home_sidebar_left', $widgets, ['widgets']);
}
?>
@foreach($widgets as $widget)
    <div class="widget">
        <h4 class="widget-title">{!! $widget->name !!}</h4>
        {!! $widget->content !!}
    </div>
@endforeach