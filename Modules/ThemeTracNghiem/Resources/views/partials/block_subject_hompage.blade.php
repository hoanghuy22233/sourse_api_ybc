<style>
    .img-bunch-subject img {
        width: auto;
        display: inline-block;
    }
    .img-bunch figure {
        width: auto;
    }
</style>
<div class="post-meta">
    <figure>
        <div class="img-bunch img-bunch-subject">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?php
                    $subjects = \Modules\ThemeTracNghiem\Models\Category::select('id', 'name', 'image')->where('type', 10)->where('status', 1)->get();
                    ?>
                    @foreach($subjects as $subject)
                        <figure>
                            <a href="/tao-bai-thi?subject_id={{ $subject->id }}" title="{{ $subject->name }}" >
                                <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($subject->image, null, 300) }}" alt="{{ $subject->name }}" class="lazy">
                            </a>
                        </figure>
                    @endforeach
                </div>
            </div>
        </div>
    </figure>
</div>