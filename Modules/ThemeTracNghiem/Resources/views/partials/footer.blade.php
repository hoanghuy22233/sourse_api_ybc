<style>
    footer ul {
        padding: 0;
    }
    footer ul li {
        list-style: none;
    }
</style>
<?php
$data = CommonHelper::getFromCache('widget_footer', ['widgets']);
if (!$data) {
    $data = \Modules\ThemeTracNghiem\Models\Widget::select('name', 'content', 'location')->where('status', 1)->whereIn('location', ['copyright', 'footer1', 'footer2', 'footer3', 'footer4'])
        ->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->get();
    CommonHelper::putToCache('widget_footer', $data, ['widgets']);
}

$footers = [];
foreach ($data as $v) {
    $footers[$v->location][] = $v;
}
//dd($footers);
?>
<div>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="widget">
                        <div class="foot-logo">
                            <div class="logo">
                                <a href="/" title=""><img class="lazy"
                                                          data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($settings['logo'], null, 90) }}"
                                                          alt="{{ $settings['name'] }}"></a>
                            </div>
                            <div class="widget-title">
                                <h4>
                                    {!! @$settings['name'] !!}
                                </h4>
                            </div>
                        </div>
                        @if(isset($footers['footer1']))
                            @foreach($footers['footer1'] as $v)
                                {!! @$v->content !!}
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    @if(isset($footers['footer2']))
                        @foreach($footers['footer2'] as $v)
                            <div class="widget">
                                <div class="widget-title"><h4>{!! @$v->name !!}</h4></div>
                                {!! str_replace('<ul>', '<ul class="list-style">', @$v->content) !!}
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6">
                    @if(isset($footers['footer3']))
                        @foreach($footers['footer3'] as $v)
                            <div class="widget">
                                <div class="widget-title"><h4>{!! @$v->name !!}</h4></div>
                                {!! str_replace('<ul>', '<ul class="list-style">', @$v->content) !!}
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="col-lg-4 col-md-8 col-sm-6">
                    @if(isset($footers['footer4']))
                        @foreach($footers['footer4'] as $v)
                            <div class="widget">
                                <div class="widget-title"><h4>{!! @$v->name !!}</h4></div>
                                {!! str_replace('<ul>', '<ul class="list-style">', @$v->content) !!}
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </footer>

    <div class="bottombar p-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="copyright mt-4">{!! @$footers['copyright'][0]->content !!}</span>
                </div>
            </div>
        </div>
    </div><!-- bottom bar -->
</div>
