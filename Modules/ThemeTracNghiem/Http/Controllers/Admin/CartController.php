<?php

namespace Modules\ThemeTracNghiem\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Error;
use DB;
use Illuminate\Http\Request;
use Mail;
use Modules\ThemeEdu\Models\Bill;
use Modules\ThemeEdu\Models\Course;
use Modules\ThemeEdu\Models\Order;
use Modules\ThemeEdu\Models\Setting;
use Modules\ThemeEdu\Models\Student;
use Session;
use Validator;
use Cart;
use Auth;

//use Hash;

class CartController extends Controller
{

    public function getCart(request $request)
    {
//        dd(Cart::content());
//        dd(Hash::check('123456789',bcrypt('123456789')));
        $data['cart'] = Cart::content();
        $data['total'] = Cart::total(0, '', ',');
        return view('themeedu::pages.course.cart', $data);
    }

    public function postCart(request $request)
    {

        DB::beginTransaction();
        try {
            if (!\Auth::guard('student')->check()) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required|unique:students,email',
                    'phone' => 'required|unique:students,phone',
                    'password' => 'required',
                    're_password' => 'required|same:password'
                ], [
                    'name.required' => 'Bắt buộc phải nhập họ & tên!',
                    'email.required' => 'Bắt buộc phải nhập email!',
                    'email.unique' => 'Email đã được đăng ký!',
                    'phone.required' => 'Bắt buộc phải nhập số điện thoại!',
                    'phone.unique' => 'Số điện thoại này đã được đăng ký!',
                    'password.required' => 'Bắt buộc phải nhập mật khẩu!',
                    're_password.required' => 'Bắt buộc phải nhập lại mật khẩu',
                    're_password.same' => 'Nhập lại mật khẩu không đúng!',
                ]);
            } else {
                $validator = Validator::make($request->all(), [

                ], [

                ]);
            }

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                if (!\Auth::guard('student')->check()) {
//            Thêm thông tin khách hàng vào bảng student
                    $student = Student::create([
                        'name' => @$request->name,
                        'email' => $request->email,
                        'phone' => $request->phone,
                        'invoice_image' => $request->hasFile('invoice_image') ? CommonHelper::saveFile($request->invoice_image, 'hoa-don') : Null,
                        'password' => bcrypt($request->password)
                    ]);
                } else {

                    $path = 'hoa-don/' . date('Y/m/d');
                    $base_path = public_path() . '/filemanager/userfiles/';
                    $dir_name = $base_path . $path;
                    if (!is_dir($dir_name)) {
                        mkdir($dir_name, 0777, true);
                    }

                    $student = Auth::guard('student')->user();
                    $student->invoice_image = $request->hasFile('invoice_image') ? CommonHelper::saveFile($request->invoice_image, $path) : Null;
                    $student->save();

                }
                //            Thêm thông tin khóa học vào bảng Bill
                $bill = Bill::create([
                    'student_id' => Auth::guard('student')->check() ? Auth::guard('student')->user()->id : $student->id,
                    'total_price' => Cart::total(0, '', ''),
                ]);

//            thêm thông tin khách hàng vào bảng Orders
                foreach (Cart::content() as $v) {
                    Order::create([
                        'course_id' => $v->id,
                        'student_id' => $student->id,
                        'quantity' => $v->qty,
                        'price' => $v->qty * $v->price,
                        'bill_id' => $bill->id,
                        'status' => $v->price==0?1:0,
                    ]);
                }
                $bill = Bill::find($bill->id);
                $settings = Setting::whereIn('type', ['general_tab', 'mail'])->pluck('value', 'name')->toArray();
                $pass = $request->password;

                try {
                    //  Gửi mail cho admin
                    $admin_emails = explode(',', trim($settings['admin_emails'], ','));
                    foreach ($admin_emails as $k => $admin_email) {
                        $admin_emails[$k] = trim($admin_email);
                    }

                    Mail::send(['html' => 'themeedu::mail.order_mail_admin'], compact('bill'), function ($message) use ($settings, $admin_emails) {
                        $message->from($settings['mailgun_username'], $settings['mail_name']);
                        $message->to(trim($admin_emails[0]), 'Admin');
                        unset($admin_emails[0]);
                        if (!empty($admin_emails)) {
                            $message->cc($admin_emails);
                        }
                        $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] Đơn hàng mới');
                    });

                    //  Gửi mail cho khách hàng
                    Mail::send(['html' => 'themeedu::mail.order_mail_user'], compact('bill', 'pass'), function ($message) use ($settings, $request) {
                        $message->from($settings['smtp_username'], $settings['mail_name']);
                        $message->to(Auth::guard('student')->check() ? Auth::guard('student')->user()->email : $request->email, Auth::guard('student')->check() ? Auth::guard('student')->user()->name : $request->name);
                        $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] Đơn hàng');
                    });
                } catch (\Exception $ex) {
                    Error::create([
                        'code' => 'mail',
                        'message' => $ex->getMessage(),
                        'module' => 'gửi mail đơn hàng'
                    ]);
                }

                DB::commit();
                Cart::destroy();
                return redirect(route('cart.complete'));
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => false,
                'msg' => 'Có lỗi xảy ra! Vui lòng liên hệ với chúng tôi!'
            ]);
        }
    }

    public function addCart($id)
    {
        $course = Course::find($id);
        $carts = Cart::content();
        $cart_id = [];
        foreach ($carts as $v) {
            $cart_id[] = $v->id;
        }
        if (!in_array($id, $cart_id)) {

            Cart::add([
                'id' => $id,
                'name' => $course->name,
                'qty' => 1,
                'price' => !empty($course->final_price) ? $course->final_price : 0,
                'options' => [
                    'image' => $course->image
                ]
            ]);
            return redirect('/gio-hang');
        } else {
            CommonHelper::one_time_message('error', 'Bạn đã thêm khóa học này vào giỏ hàng rồi! Vui lòng chọn mua khóa học khác');
            return back();
        }


    }

    public function ajaxAddCart(Request $request)
    {
        $course = Course::find($request->id);
        $carts = Cart::content();
        $cart_id = [];
        foreach ($carts as $v) {
            $cart_id[] = $v->id;
        }
        if (!in_array($request->id, $cart_id)) {
            Cart::add([
                'id' => $request->id,
                'name' => $course->name,
                'qty' => 1,
                'price' => !empty($course->final_price) ? $course->final_price : 0,
                'options' => [
                    'image' => $course->image,
                    'base_price' => !empty($course->base_price) ? $course->base_price : 0,
                ]
            ]);
            return response()->json([
                'status' => true,
            ]);
        } else {
            return response()->json([
                'status' => false,
                'msg' => 'Bạn đã thêm khóa học này vào giỏ hàng rồi! Vui lòng chọn mua khóa học khác'
            ]);
        }
    }

    public function delItemCart($rowId)
    {
        Cart::remove($rowId);
        return redirect()->back();
    }
    public function delAllItemCart()
    {
        Cart::destroy();
        return redirect()->back();
    }

    public function updateItemCart($rowId, $qty)
    {
        Cart::update($rowId, $qty);
    }

    public function getComplete(request $request)
    {
        return view('themeedu::pages.course.complete');
    }
}
