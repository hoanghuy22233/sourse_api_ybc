<?php

namespace Modules\ThemeTracNghiem\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Modules\EntranceExam\Http\Controllers\Admin\QuestionController;
use Modules\ThemeTracNghiem\Models\Bill;
use Modules\ThemeTracNghiem\Models\Category;
use Modules\ThemeTracNghiem\Models\Exam;
use Modules\ThemeTracNghiem\Models\Question;
use Session;

class PaymentController extends Controller
{

    public function thanhToan(Request $r) {
        $data = [
            'price' => 6000000,
            'subjects' => '|222|233|217|'
        ];

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Thanh toán khóa học',
            'meta_description' => 'Thanh toán khóa học',
            'meta_keywords' => 'Thanh toán khóa học',
        ];
        view()->share('pageOption', $pageOption);
        return view('themetracnghiem::pages.payment.thanh_toan', $data);
    }

    public function napTien(Request $r) {
        if ($r->has('price')) {
            $price = $r->price;
        } elseif (Session::has('bill_wait_pay')) {
            $price = Session::get('bill_wait_pay')['price'];
        }

        //  Sang vnpay để nạp tiền

        return true;
    }

    public function nhanKetQuaNapTien() {


        //  Nếu đang có gói chờ thanh toán thì thanh toán gói luôn
        if (Session::has('bill_wait_pay')) {
            $price = Session::get('bill_wait_pay')['price'];
            $bill = Bill::find(Session::get('bill_wait_pay')['bill_id']);

            $questionContrller = new QuestionController();
            $questionContrller->deductMoneyRegisterSubject(\Auth::guard('student')->user(), $price, $bill);
            return redirect('/dang-ky-mon-thanh-cong?subjects=' . $bill->subjects);
        }

        return redirect('/nap-tien-thanh-cong?price=' . $price);
    }

    public function napTienThanhCong() {
        $data = [

        ];

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Thanh toán khóa học',
            'meta_description' => 'Thanh toán khóa học',
            'meta_keywords' => 'Thanh toán khóa học',
        ];
        view()->share('pageOption', $pageOption);
        return view('themetracnghiem::pages.payment.nap_tien_thanh_cong', $data);
    }
}
