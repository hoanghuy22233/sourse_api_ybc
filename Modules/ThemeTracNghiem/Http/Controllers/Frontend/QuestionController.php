<?php

namespace Modules\ThemeTracNghiem\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Modules\ThemeTracNghiem\Models\Bill;
use Modules\ThemeTracNghiem\Models\Category;
use Modules\ThemeTracNghiem\Models\Exam;
use Modules\ThemeTracNghiem\Models\Question;
use Modules\ThemeTracNghiem\Models\QuestionsError;
use Session;
use Mail;

class QuestionController extends Controller
{

    public function taoBaiThi(Request $r)
    {
        if (!$_POST) {
            $data = [];

            //  Thẻ meta cho seo
            $pageOption = [
                'meta_title' => 'Ôn luyện theo yêu cầu',
                'meta_description' => 'Ôn luyện theo yêu cầu',
                'meta_keywords' => 'Ôn luyện theo yêu cầu',
            ];
            view()->share('pageOption', $pageOption);
            return view('themetracnghiem::pages.question.tao_bai_thi', $data);
        } else {
            $subject_registered = explode('|', \Auth::guard('student')->user()->subjects);
            if (!in_array($r->subject_id, $subject_registered)) {
                return redirect('/dang-ky-mon');
            }
//            \Auth::guard('student')->user()->id;
            $exam = $this->createExam(\Auth::guard('student')->user()->id, $r->subject_id, $r->level, $r->chapter_id, $r->thematic_id);
            return redirect('de-thi/' . $exam->id);
        }
    }

    //  Tạo đề thi
    public function createExam($student_id, $subject_id = null, $level = null, $chapter_id = null, $thematic_id = null)
    {
        $exam = new Exam();
        $exam->student_id = $student_id;
        $exam->subject_id = $subject_id;
        $exam->level = $level;
        $exam->chapter_id = $chapter_id;
        $exam->thematic_id = $thematic_id;

        $questions = [];

        $subject = Category::find($subject_id);


        if ($thematic_id == null && $chapter_id == null) {  //  Nếu là môn
            $config = (array)json_decode($subject->config_subject);
            $config = (array)$config[$level];

            foreach ($config as $chapter_id => $chapter_config) {
                $chapter_config = (array)$chapter_config;
                foreach ($chapter_config as $do_kho => $quantity) {
                    if ($quantity != null) {
                        /*$q_arr = Question::where('status', 1)->where('subject_id', $subject_id)->where('chapter_id', $chapter_id)->where('level', $do_kho)
                            ->orderByRaw("RAND()")->limit($quantity)->pluck('id')->toArray();*/
                        $q_arr = $this->getQuestionsFromChapter($thematic_id, $chapter_id, $do_kho, $quantity);
                        $questions = array_merge($questions, $q_arr);
                    }
                }
            }
        } else {    //  là chương / chuyên đề
            $config = (array)json_decode($subject->config_chapter);
            $config = (array)$config[$level];

            if ($config['de'] != null) {
                /*$q_arr = Question::where('status', 1)->where('subject_id', $subject_id)->where('chapter_id', $chapter_id)->where('level', 'de')
                    ->orderByRaw("RAND()")->limit($config['de'])->pluck('id')->toArray();*/
                $q_arr = $this->getQuestionsFromChapter($thematic_id, $chapter_id, 'de', $config['de']);
                $questions = array_merge($questions, $q_arr);
            }
            if ($config['tb'] != null) {
                /*$q_arr = Question::where('status', 1)->where('subject_id', $subject_id)->where('chapter_id', $chapter_id)->where('level', 'tb')
                    ->orderByRaw("RAND()")->limit($config['tb'])->pluck('id')->toArray();*/
                $q_arr = $this->getQuestionsFromChapter($thematic_id, $chapter_id, 'tb', $config['tb']);
                $questions = array_merge($questions, $q_arr);
            }
            if ($config['kho'] != null) {
                /*$q_arr = Question::where('status', 1)->where('subject_id', $subject_id)->where('chapter_id', $chapter_id)->where('level', 'kho')
                    ->orderByRaw("RAND()")->limit($config['kho'])->pluck('id')->toArray();*/
                $q_arr = $this->getQuestionsFromChapter($thematic_id, $chapter_id, 'kho', $config['kho']);
                $questions = array_merge($questions, $q_arr);
            }
        }

        $data = [];
        foreach ($questions as $question) {
            $data[$question] = '';
        }
        $exam->data = json_encode($data);
        $exam->save();
        return $exam;
    }

    /**
     * Truy vấn các câu hỏi trong 1 chương, chia đều theo các chuyên đề
     */
    public function getQuestionsFromChapter($thematic_id, $chapter_id, $level, $limit)
    {
        if ($thematic_id == null) {
            $thematics = Category::where('parent_id', $chapter_id)->where('status', 1)->get();
        } else {
            $thematics = Category::where('id', $thematic_id)->where('status', 1)->get();
        }
        $tb_so_mon_moi_chuong = (int)$limit / count($thematics);   //   Trung bình số câu cần truy vấn mỗi chuyên đề

        $q_arr = [];
        $count = 0;
        if ($tb_so_mon_moi_chuong > 0 && count($thematics) > 0) {
            foreach ($thematics as $thematic) {
                $ids = Question::where('status', 1)->where('thematic_id', $thematic->id)->where('level', $level)
                    ->orderByRaw("RAND()")->limit($tb_so_mon_moi_chuong)->pluck('id')->toArray();
                $q_arr = array_merge($q_arr, $ids);
                $count += count($ids);
            }
        }

        //  Nếu truy vấn chưa đủ thì lấy thêm
        if ($limit > $count && $thematic_id == null) {
            $con_lai = $limit - $count;
            $ids = Question::where('status', 1)->where('chapter_id', $chapter_id)->where('level', $level)->whereNotIn('id', $q_arr)
                ->orderByRaw("RAND()")->limit($con_lai)->pluck('id')->toArray();
            $q_arr = array_merge($q_arr, $ids);
        }

        return $q_arr;
    }

    public function deThi($id, Request $r)
    {
        if (!$_POST) {
            $data['exam'] = Exam::find($id);
            $data['exam_data'] = (array)json_decode($data['exam']->data);
            $question_ids = array_keys($data['exam_data']);

            $data['can_edit'] = true;
            if ($data['exam']->student_id != \Auth::guard('student')->user()->id) { //   không phải bài của mình thì ko được sửa
                $data['can_edit'] = false;
            }
            if ($data['exam']->point != null) { //  đã có điểm số thì không được sửa
                $data['can_edit'] = false;
            }

            $data['questions'] = Question::whereIn('id', $question_ids)->get();

            //  Thẻ meta cho seo
            $pageOption = [
                'meta_title' => 'Luyện thi',
                'meta_description' => 'Luyện thi',
                'meta_keywords' => 'Luyện thi',
            ];
            view()->share('pageOption', $pageOption);
            return view('themetracnghiem::pages.question.de_thi', $data);
        } else {
            $exam = Exam::find($id);
            $data = (array)json_decode($exam->data);
            foreach ($data as $q_id => $dn) {
                $data[$q_id] = @$r->dapan[$q_id];
            }
            $exam->data = json_encode($data);
            $exam->save();
            return redirect('/dap-an/' . $id);
        }
    }

    public function dapAn($id)
    {
        $data['exam'] = Exam::find($id);
        $data['exam_data'] = (array)json_decode($data['exam']->data);
        $question_ids = array_keys($data['exam_data']);
        $data['questions'] = Question::whereIn('id', $question_ids)->get();

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Đáp án bài thi',
            'meta_description' => 'Đáp án bài thi',
            'meta_keywords' => 'Đáp án bài thi',
        ];
        view()->share('pageOption', $pageOption);
        return view('themetracnghiem::pages.question.dap_an', $data);
    }

    public function khoaHoc()
    {
        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Khóa học',
            'meta_description' => 'Khóa học',
            'meta_keywords' => 'Khóa học',
        ];
        view()->share('pageOption', $pageOption);
        $data = [];

        return view('themetracnghiem::pages.question.khoa_hoc', $data);
    }

    public function baoCaoLoi(Request $r)
    {
        QuestionsError::create([
            'question_id' => $r->prd_id,
            'reason' => $r->reason,
            'content' => $r->content,
        ]);
        try{
            $data['question_id'] = $r->prd_id;
            $data['reason'] = $r->reason;
            $data['content'] = $r->content;


//            $settings = Setting::where('type', 'mail')->pluck('value', 'name')->toArray();
//            $admins = explode(',', $settings['admin_emails']);
            \Illuminate\Support\Facades\Mail::send(['html' => 'themetracnghiem::mail.mail_report_answer'], $data, function ($message) {
                $message->from(env('MAIL_USERNAME'), '[ ' . $_SERVER['SERVER_NAME'] . ' ]');
                $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] Báo lỗi câu hỏi');

                $message->to('hieunc0398@gmail.com')->subject('Báo cáo lỗi câu hỏi!');
            });
            return response()->json([
                'status'=>true
            ]);
        }catch (\Exception $exception){
            return response()->json([
                'status'=>false

            ]);
        }

    }

    public function dangKyMon(Request $r)
    {
        if (!$_POST) {
            $data = [];

            //  Thẻ meta cho seo
            $pageOption = [
                'meta_title' => 'Mua khóa học',
                'meta_description' => 'Mua khóa học',
                'meta_keywords' => 'Mua khóa học',
            ];
            view()->share('pageOption', $pageOption);
            return view('themetracnghiem::pages.question.dang_ky_mon', $data);
        } else {
            $setting = \App\Models\Setting::where('type', 'service_tab')->where('name', count($r->subjects))->first();
            if (!is_object($setting)) {
                return back();
            }

            $price = $setting->value;

            $user = \Auth::guard('student')->user();

            // Tạo bill
            $bill = new Bill();
            $bill->user_id = $bill->student_id = $user->id;
            $bill->user_tel = $user->tel;
            $bill->user_name = $user->name;
            $bill->user_email = $user->email;
            $bill->user_address = $user->address;
            $bill->subjects = '|' . implode('|', $r->subjects) . '|';
            $bill->paid = 0;
            $bill->status = 0;
            $bill->total_price = $price;
            $bill->save();

            $this->sendMailCreateBill($bill);

            //  Nếu không đủ tiền thì chuyển đến trang nạp tiền
            if ($user->balance < $price) {
                $price_need = $price - $user->balance;
                \Session::flash('error', 'Tài khoản của bạn không đủ tiền. Vui lòng nạp thêm ' . number_format($price_need, 0, '.', '.') . 'đ. <a href="/bai-viet/huong-dan-nap-tien.html?price=' . $price_need . '" style="font-weight: bolder; color: blue;">CLICK vào đây để Nạp tiền</a>');
                return back()->withInput();

                Session::put('bill_wait_pay', [
                    'bill_id' => $bill->id,
                    'price' => $price_need,
                    'subjects' => $r->subjects,
                ]);
                return redirect('/thanh-toan');
            }

            $this->deductMoneyRegisterSubject($user, $price, $bill);
            return redirect('/dang-ky-mon-thanh-cong?subjects=' . $bill->subjects);
        }
    }

    public function sendMailCreateBill($bill)
    {
        $settings = Setting::whereIn('type', ['general_tab', 'mail'])->pluck('value', 'name')->toArray();
        if (@$settings['admin_receives_mail'] == 1) {
            //  Gửi mail cho admin
            $admin_emails = explode(',', trim($settings['admin_emails'], ','));
            foreach ($admin_emails as $k => $admin_email) {
                $admin_emails[$k] = trim($admin_email);
            }
            Mail::send(['html' => 'themetracnghiem::mail.order_subjects_admin'], compact('bill'), function ($message) use ($settings, $admin_emails) {
                $message->from($settings['mailgun_username'], $settings['mail_name']);
                $message->to(trim($admin_emails[0]), 'Admin');
                unset($admin_emails[0]);
                if (!empty($admin_emails)) {
                    $message->cc($admin_emails);
                }
                $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] Đơn hàng mới');
            });
        }
    }

    function configMail()
    {
        $settings = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();
        if (isset($settings['driver'])) {
            $username = $settings['driver'] == 'mailgun' ? @$settings['mailgun_username'] : @$settings['smtp_username'];
            $config =
                [
                    'mail.from' => [
                        'address' => $username,
                        'name' => @$settings['mail_name'],
                    ],
                    'mail.driver' => @$settings['driver'],
                ];

            if ($settings['driver'] == 'mailgun') {
                $config['services.mailgun'] =
                    [
                        'domain' => trim(@$settings['mailgun_domain']),
                        'secret' => trim(@$settings['mailgun_secret']),
                    ];
                $config['mail.port'] = @$settings['mailgun_port'];
                $config ['mail.username'] = @$settings['mailgun_username'];
            } else {
                $config['mail.port'] = @$settings['smtp_port'];
                $config['mail.password'] = @$settings['smtp_password'];
                $config['mail.encryption'] = @$settings['smtp_encryption'];
                $config['mail.host'] = @$settings['smtp_host'];
                $config['mail.username'] = @$settings['smtp_username'];
            }
//            $config['services.onesignal'] = [
//                'app_id' => '420af10d-5030-4f34-af19-68078fd6467c',
//                'rest_api_key' => 'MTY0MjA5NTktNjgwNS00NGM3LTg3YmYtNzcwMmRhZDUyZmE2'
//            ];
//            config onesignal
//            dd($config);
            config($config);
        }
        return $settings;
    }

    /**
     * Trừ tiên tài khoản & thêm môn vào danh sách đã mua
     *
     */
    public function deductMoneyRegisterSubject($user, $price, $bill)
    {
        //  Trừ tiền
        $user->balance = $user->balance - $price;

        //  Thêm môn vào danh sách môn đã mua
        $user->subjects .= '|' . $bill->subjects . '|';
        $user->subjects = preg_replace('/\|\|/', '|', $user->subjects);

        $user->save();

        $bill->status = 1;
        $bill->paid = 1;
        $bill->save();

        return true;
    }

    public function dangKyMonThanhCong(Request $r)
    {
        $data = [];

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Đăng ký môn học thành công',
            'meta_description' => 'Đăng ký môn học thành công',
            'meta_keywords' => 'Đăng ký môn học thành công',
        ];
        view()->share('pageOption', $pageOption);
        return view('themetracnghiem::pages.question.dang_ky_mon_thanh_cong', $data);
    }
}
