<?php

/**
 * Banners Model
 *
 * Banners Model manages Banners operation. 
 *
 * @category   Banners
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license    
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\ThemeTracNghiem\Models ;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
	protected $table = 'exams';

	public function subject() {
        return $this->belongsTo(Category::class,'subject_id');
    }

    public function chapter() {
        return $this->belongsTo(Category::class,'chapter_id');
    }

    public function thematic() {
        return $this->belongsTo(Category::class,'thematic_id');
    }
}
