<?php

namespace Modules\ThemeTracNghiem\Models ;
use Illuminate\Database\Eloquent\Model;
class Setting extends Model
{
	protected $table = 'settings';
    public $timestamps = false;
}
