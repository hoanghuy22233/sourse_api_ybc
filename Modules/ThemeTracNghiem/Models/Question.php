<?php

namespace Modules\ThemeTracNghiem\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    protected $table = 'questions';

    protected $guarded = [];



    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function thematic()
    {
        return $this->belongsTo(Category::class, 'thematic_id');
    }
    public function chapter()
    {

        return $this->belongsTo(Category::class, 'chapter_id');
    }
    public function subject()
    {
        return $this->belongsTo(Category::class, 'subject_id');
    }

}
