<?php

namespace Modules\A4iAuth\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\SmsController;
use App\Models\{RoleAdmin, Setting};
use Auth;
use Illuminate\Http\Request;
use Mail;
use Modules\A4iAdmin\Models\Admin;
use Modules\A4iAuth\Http\Requests\RegisterRequest;
use Session;
use Validator;

class AuthController extends Controller
{
    public function login()
    {
        $data['page_title'] = 'Đăng nhập';
        $data['page_type'] = 'list';
        return view('a4iauth::login');
    }

    public function getRegister()
    {
        $data['page_title'] = 'Đăng ký';
        $data['page_type'] = 'list';
        return view('a4iauth::register', $data);
    }

    public function postRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:admin,email',
            'password' => 'required|min:4',
            'password_confimation' => 'required|same:password',
            'tel' => 'required|unique:admin,tel',
        ], [
            'name.required' => 'Bắt buộc phải nhập tên!',
            'email.required' => 'Bắt buộc phải nhập email!',
            'email.unique' => 'Địa chỉ email đã tồn tại!',
            'password.required' => 'Bắt buộc phải nhập mật khẩu!',
            'password.min' => 'Mật khẩu phải trên 4 ký tự!',
            'password_confimation.required' => 'Bắt buộc nhập lại mật khẩu!',
            'password_confimation.same' => 'Nhập lại sai mật khẩu!',
            'tel.required' => 'Bắt buộc nhập số điện thoại!',
            'tel.unique' => 'Số điện thoại đã tồn tại!',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            if ($request->tel != null) {
                $smsController = new SmsController();
                @$respon = $smsController->verifyUserPhoneFirebase($request->tel, $request->get('tel_token', ''));
                if (@$respon['status'] !== true) {
                    \Session::flash('error', 'Số điện thoại chưa được xác nhận!');
                    return back();
                }
            }

            $data = $request->except('_token');
            $data['password'] = $data['password_md5'] = bcrypt($data['password']);
            $data['api_token'] = base64_encode(rand(1, 100) . time());
            if (isset($data['status'])) {
                unset($data['status']);
            }
            unset($data['password_confimation']);
            unset($data['agree']);
            unset($data['tel_code_country']);
            unset($data['tel_token']);
            unset($data['g-recaptcha-response']);

            $admin = new Admin;

            foreach ($data as $k => $v) {
                $admin->{$k} = $v;
            }

            $admin->save();

            //  Set quyền mặc định khi mới đăng ký tài khoản
            $role_default_id = @Setting::select(['value'])->where('name', 'role_default_id')->first()->value;
            if ($role_default_id != null) {
                RoleAdmin::insert([
                    'admin_id' => $admin->id,
                    'role_id' => $role_default_id,
                ]);
            }
            return redirect('/admin/login')->with('success', 'Chúc mừng bạn đã đăng ký thành công' . $admin->status == 0 ? '. Tài khoản của bạn đang chờ kick hoạt' : '');
        }
    }
}



