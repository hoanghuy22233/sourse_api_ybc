<style>
    .firebase-loader {
        border: 5px solid #f3f3f3;
        border-radius: 50%;
        border-top: 5px solid #3498db;
        width: 20px;
        height: 20px;
        -webkit-animation: spin 2s linear infinite; /* Safari */
        animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

    /*custom*/
    #msg-comfirm {
        width: 100%;
    }
</style>

<script src="https://www.gstatic.com/firebasejs/7.11.0/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
{{--<script src="https://www.gstatic.com/firebasejs/7.11.0/firebase-analytics.js"></script>--}}

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/7.11.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.11.0/firebase-firestore.js"></script>

<script>
    console.log('connect to firebase');
    var firebaseConfig = {
        apiKey: env('FIREBASE_API_KEY'),
        authDomain: env('FIREBASE_AUTH_DOMAIN'),
        databaseURL: env('FIREBASE_DATABASE_URL'),
        projectId: env('FIREBASE_PROJECT_ID'),
        storageBucket: env('FIREBASE_STORAGE_BUCKET'),
        messagingSenderId: "277589381178",
        appId: "1:277589381178:web:9530aa32f75c3da3762503",
        measurementId: "G-QHH89SNQ9Z"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    // firebase.getInstance().setLanguageCode("vi");
    setTimeout(function () {
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
            "recaptcha-container",
            {
                size: "invisible"
            }
        );
    }, 1000);

    function countdown(second) {
        $('#text-countdown').text('(' + second + ')');
        $('#send-opt').hide();
        $('#countdown').show();
        if (second >= 1) {
            setTimeout(function () {
                second--;
                countdown(second);
            }, 1000)
        } else {
            $('#send-opt').show();
            $('#countdown').hide();
            $('#text-send-opt').text('Gửi lại mã OTP');
        }
    }

    function getPhoneNumber() {
        var tel_code_country = $('select[name=tel_code_country]').val();
        var first_number = $('select[name=tel_code_country] option:selected').data('first_number').toString();

        var phone = $('#tel').val().slice(0, 15);

        if ($('#tel').val().slice(0, tel_code_country.length) == tel_code_country) {
            phone = $('#tel').val().slice(tel_code_country.length, 15);
        } else if ($('#tel').val().slice(0, first_number.length) == first_number) {
            phone = $('#tel').val().slice(first_number.length, 15);
        }

        phone = tel_code_country + phone;
        console.log('phone: ' + phone);
        return phone;
    }

    firebase.auth().onAuthStateChanged(function (user) {
        console.log('user>>>>', user);
        console.log('token>>>>', user.refreshToken);
        $('input[name=tel_token]').val(user.xa);
        // document.getElementById('loading').style.display = 'none';
        // document.getElementById('loaded').style.display = 'block';
        if (user) {
            $('#alert-respon').removeClass('text-danger').addClass('text-success').text('Xác thực thành công');
            // $('#tel').val(user.phoneNumber.replace('+84', '0'));
            $('#send-opt').hide();
            $('#send-back').show();

        } else {
            console.log('Xác thực thất bại');
        }
    });

    $(document).ready(function () {
        $('#send-opt').click(function () {
            var phonenumber = getPhoneNumber();
            //đếm ngược
            $('#loader-send').show();
            $('#text-send-opt').hide();
            firebase.auth().signInWithPhoneNumber(phonenumber, window.recaptchaVerifier)
                .then(function (confirmationResult) {
                    window.confirmationResult = confirmationResult;
                    countdown(10);
                    $('#loader-send').hide();
                    $('#text-send-opt').show();
                    $('#alert-respon').text('');
                    $('#box-confirm').show();
                })
                .catch(function (error) {
                    console.log(error);
                    $('#loader-send').hide();
                    $('#text-send-opt').show();
                    $('#alert-respon').removeClass('text-success').addClass('text-danger').text(error);
                });
        });

        $('#send-back').click(function () {
            firebase.auth().signOut();
            window.location.reload()
        });

        $('#btn-confirm').click(function () {
            var code = $('#input-confirm').val();
            $('#loader-confirm').show();
            $('#btn-confirm').hide();
            $('#loader-send').hide();
            confirmationResult.confirm(code).then(function (result) {
                // User signed in successfully.
                var user = result.user;
                $('#box-confirm').hide();

                $('#alert-respon').removeClass('text-danger').addClass('text-success').text('Xác thực thành công');
            }).catch(function (error) {
                // User couldn't sign in (bad verification code?)
                $('#loader-confirm').hide();
                $('#btn-confirm').show();
                $('#msg-comfirm').removeClass('text-success').addClass('text-danger').text(error.message)
            });
        });
    });
</script>


{{--<div class="input-group-append">--}}
{{--<span id="check-tel" class="send-opt btn btn-primary form-control"--}}
{{--style="border-radius: 0; padding-top: 12px;">--}}
{{--<span>Kiểm tra</span>--}}
{{--</span>--}}
{{--</div>--}}

<div id="tel_verify" class="input-group">
    <div class="input-group-append">
        <select style="border-radius: 17px;height: 70%;margin-top: 40%; border: none" name="tel_code_country">
            <option value="+84" data-first_number="0">+84</option>
            <option value="+61" data-first_number="0061">+61</option>
        </select>
    </div>
    <input style="color: #000; border-radius: 40px 0 0 40px; "
           class="form-control m-input text-color"
           name="tel" type="text" placeholder="Số điện thoại" id="tel"
           value="{{old('tel')}}">
    <div class="input-group-append" id="code_verify">
        <span id="send-opt" class="send-opt btn btn-success form-control"
              style="border-radius: 0 40px 40px 0; padding-top: 12px;">
            <span id="text-send-opt">Gửi mã OTP</span>
            <div id="loader-send" class="firebase-loader" style="display:none;"></div>
        </span>
        <span id="send-back" class="send-opt btn btn-success form-control"
              style="border-radius: 0 40px 40px 0; padding-top: 12px;display: none">
            <span id="text-send-back">Đổi số</span>
        </span>
        <span id="countdown" class="btn btn-success form-control"
              style="border-radius: 0 40px 40px 0; padding-top: 12px;display: none">
            <span id="text-countdown">Gửi mã OTP</span>
        </span>
    </div>
    <input name="tel_token" value="" type="hidden">
</div>
<div id="box-confirm" class="input-group" style="display: none;">
    <input style="color: #000; border-radius: 10px 0 0 10px; border: 1px solid #0000e6"
           class="form-control m-input text-color"
           id="input-confirm" type="text"
           placeholder="Nhập mã OTP">
    <div class="input-group-append">
        <span class="btn btn-success form-control"
              style="border-radius: 0 10px 10px 0; padding-top: 12px; border: 1px solid #0000e6;"><span
                    id="btn-confirm">Xác nhận</span><div id="loader-confirm"
                                                         class="loader"
                                                         style="display: none"></div></span>
    </div>
    <p id="msg-comfirm" class="text-danger" style=" padding-left: 10px;"></p>
    <div id="recaptcha-container"></div>
</div>
<p class="text-danger" id="alert-respon" style=" padding-left: 10px;"></p>