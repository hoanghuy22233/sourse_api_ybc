<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>{{ $settings['name'] }} | Lấy lại mật khẩu</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <link rel="stylesheet" href="{{asset('public/backend/bootstrap/css/bootstrap.min.css')}}">

    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!--end::Web font -->

    <link rel="stylesheet" href="{{asset('public/login/css/vendors.bundle.css')}}">
    <link rel="stylesheet" href="{{asset('public/login/css/vendors.bundle.rtl.css')}}">
    <link rel="stylesheet" href="{{asset('public/login/css/style.bundle.css')}}">


    <!--end::Global Theme Styles -->
    <link rel="shortcut icon" href="{{ @$favicon }}">
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url({{ asset('/public/login/images/bg-1.jpg') }}) !important;">
        <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <a href="#">
                        <img src="{{asset('/public/filemanager/userfiles/'.@$settings['logo'])}}"/>
                    </a>
                </div>
                @if (session('message'))
                    <div class="alert alert-success text-danger">
                        <p style="font-size: 15px">{{ session('message') }}</p>
                    </div>
                @endif
                <div class="forget-password">
                    <div class="m-login__head">
                        <h3 class="m-login__title">Bạn đã quên mật khẩu ?</h3>
                        <div class="m-login__desc">Vui lòng nhập email để lấy lại mật khẩu :</div>
                    </div>
                    <form class="m-login__form m-form" action="" id="register-form" role="form" method="post">
                        <div class="form-group m-form__group">
                            <input style="color: #000;!important;" class="form-control m-input" type="email" placeholder="Email" name="email" id="email" autocomplete="off">
                        </div>
                        <div class="m-login__form-action">
                            <button id="m_login_forget_password_submit" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary" name="recover-submit" type="submit">Gửi lại mật khẩu</button>&nbsp;&nbsp;
                            <a href="/admin/login" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">Hủy</a>
                        </div>
                        <input type="hidden" class="hide" name="token" id="token" value="">
                    </form>
                </div>
                <div class="m-login__account">
							<span class="m-login__account-msg">
								Bạn chưa có tài khoản ?
							</span>&nbsp;&nbsp;
                    <a href="/admin/register" id="m_login_signup" class="m-link m-link--light m-login__account-link">Đăng ký</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Page -->

<!--begin::Global Theme Bundle -->
<script src="../../../assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="../../../assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Scripts -->
<script src="../../../assets/snippets/custom/pages/user/login.js" type="text/javascript"></script>

<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>