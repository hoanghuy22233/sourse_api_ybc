<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>{{ $settings['name'] }} | Đăng ký</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <link rel="stylesheet" href="{{asset('public/login/css/vendors.bundle.css')}}">
    <link rel="stylesheet" href="{{asset('public/login/css/vendors.bundle.rtl.css')}}">
    <link rel="stylesheet" href="{{asset('public/login/css/style.bundle.css')}}">


    <!--end::Global Theme Styles -->
    <link rel="shortcut icon" href="{{ @$favicon }}">
    <style>
        .text-color {
            color: #0a001f !important;
        }
    </style>
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
<script src="{{asset('public/libs/jquery-3.4.0.min.js')}}"></script>


<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1"
         id="m_login"
         style="background-image: url({{ asset('public/filemanager/userfiles/' . @\App\Models\Setting::where('name', 'background_login')->first()->value) }}) !important;">
        <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
            <div class="m-login__container">
                <div class="m-login__logo">
                    <a href="#">
                        <img src="{{asset('/public/filemanager/userfiles/'.@$settings['logo'])}}"
                             title="{{ @$settings['name'] }}" alt="{{ @$settings['name'] }}"/>
                    </a>
                </div>

                <div class="signup">
                    <div class="m-login__head">
                        <h3 class="m-login__title">Đăng ký</h3>
                        <div class="m-login__desc">Nhập thông tin để tạo tài khoản của bạn:</div>
                    </div>
                    <form method="post" class="m-login__form m-form" action="">
                        <div class="form-group m-form__group">
                            <input style="color: #000;!important;" class="form-control m-input text-color" type="text"
                                   placeholder="Họ & Tên" name="name" id="firstname" value="{{old('name')}}">
                            @if ($errors->has('name'))
                                <p class="text-danger" style=" padding-left: 10px;">{{$errors->first('name')}}</p>
                            @endif
                        </div>
                        <div class="form-group m-form__group">
                            <input style="color: #000;!important;" class="form-control m-input  text-color" type="email"
                                   placeholder="Email" name="email" id="inputEmail" value="{{old('email')}}"
                                   autocomplete="off">
                            @if ($errors->has('email'))
                                <p class="text-danger" style=" padding-left: 10px;">{{$errors->first('email')}}</p>
                            @endif
                        </div>
                        <div class="form-group m-form__group">
                            <div class="input-group">
                                @include('a4iauth::form.fields.firebase_verify_phone')
                            </div>
                            {{--<p class="text-danger" style=" padding-left: 10px;">Bạn cần xác thực số điện thoại</p>--}}
                            @if ($errors->has('tel'))
                                <p class="text-danger" style=" padding-left: 10px;">{{$errors->first('tel')}}</p>
                            @endif

                        </div>
                        <div class="form-group m-form__group">
                            <input style="color: #000;!important;" class="form-control m-input text-color"
                                   type="password" name="password" id="inputPassword" placeholder="Mật khẩu">
                            @if ($errors->has('password'))
                                <p class="text-danger" style=" padding-left: 10px;">{{$errors->first('password')}}</p>
                            @endif
                        </div>
                        <div class="form-group m-form__group">
                            <input style="color:#000!important;"
                                   class="form-control m-input m-login__form-input--last text-color"
                                   name="password_confimation" type="password" id="inputPasswordConfirm"
                                   placeholder="Nhập lại mật khẩu">
                            @if ($errors->has('password_confimation'))
                                <p class="text-danger"
                                   style=" padding-left: 10px;">{{$errors->first('password_confimation')}}</p>
                            @endif
                        </div>
                        <div class="row form-group m-form__group m-login__form-sub">
                            <div class="col m--align-left">
                                <label class="m-checkbox m-checkbox--light">
                                    <input type="checkbox" name="agree">Tôi đồng ý <a href="#"
                                                                                      class="m-link m-link--focus">các
                                        điều khoản và điều kiện.</a>.
                                    <span></span>
                                </label>
                                <span class="m-form__help"></span>
                            </div>
                        </div>
                        <div class="m-login__form-action">
                            <button type="submit"
                                    class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                                Đăng ký
                            </button>&nbsp;&nbsp;
                            <a href="/admin/login" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">Bạn
                                đã có tài khoản!</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- end:: Page -->

<!--begin::Global Theme Bundle -->
<script src="{{asset('public/login/js/login.js')}}"></script>
{{--<script src="{{URL::to('public/backend/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>--}}
<!-- Bootstrap 3.3.6 -->
<script src="{{URL::to('public/libs/bootstrap/js/bootstrap.min.js')}}"></script>

<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>