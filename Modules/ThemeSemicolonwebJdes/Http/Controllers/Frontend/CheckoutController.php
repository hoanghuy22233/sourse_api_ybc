<?php

namespace Modules\ThemeSemicolonwebJdes\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CheckoutRequest;
use Cart;
use Illuminate\Http\Request;
use Modules\JdesSetting\Models\Editor;
use Modules\ThemeSemicolonwebJdes\Models\{Bill, Order, User};

class CheckoutController extends Controller
{
    public function getCkeckout()
    {
        $data['cart'] = Cart::content();
        $data['total'] = Cart::total(0, '', ',');
        return view('themesemicolonwebjdes::pages.product.ckeckout', $data);
    }

    public function postCkeckout(CheckoutRequest $request)
    {
        //  Tạo khách hàng
        $user = User::updateOrCreate(
            [
                'tel', $request->phone
            ],
            [
                'name' => $request->name,
                'address' => $request->address,
                'note' => $request->note,
                'date' => $request->date,
                'email' => $request->email,
            ]
        );

        $bill_group = time();

        //  Lấy thông tin chung của đơn hàng
        $bill_data_common = $request->thanhtoan == 1 ? [
            'user_name' => $request->shipping_form_name,
            'user_address' => $request->shipping_form_address,
            'user_tel' => $request->shipping_form_phone,
            'note' => $request->shipping_form_message,
            'receipt_method' => $request->receipt_method,
            'user_gender' => $request->shipping_form_gender,
            'user_email' => $request->shipping_form_email,
            'date' => $request->date,
            'user_id' => $user->id,
            'group_no' => $bill_group
        ] : [
            'user_name' => $request->name,
            'user_address' => $request->address,
            'user_tel' => $request->phone,
            'note' => $request->note,
            'date' => $request->date,
            'user_gender' => $request->gender,
            'user_email' => $request->email,
            'receipt_method' => $request->receipt_method,
            'user_id' => $user->id,
            'group_no' => $bill_group
        ];

        //  Phân tích data giỏ hàng chung ra thành từng giỏ hàng con của từng công ty
        $bills = [];
        $orders = [];
        foreach (Cart::content() as $product) {
            $bills[$product->options->company_id] = [
                'company_id' => $product->options->company_id,
                'total_price' => isset($bills[$product->options->company_id]['total_price']) ? $bills[$product->options->company_id]['total_price'] + ($product->qty * $product->price) : ($product->qty * $product->price),
            ];

            //  Lấy mã editor cho sản phẩm
            $key_editor_log = null;
            if ($product->options->key_editor_log != null) {
                $editor = Editor::where('product_ids', 'like', '%|' . $product->id . '|%')->first();
                $filename = base_path() . '/' . config('jdessetting.editor_log_path') . 'editor_id_' . $editor->id . $product->options->key_editor_log . '.txt';
                $key_editor_log = $product->options->key_editor_log . '_time_' . time();
                $filename_order = base_path() . '/' . config('jdessetting.editor_log_path') . 'editor_id_' . $editor->id . $key_editor_log . '.txt';
                $editor_log_data = @file_get_contents($filename);
                file_put_contents($filename_order, $editor_log_data);
            }

            $orders[$product->options->company_id][] = [
                'price' => $product->price * $product->qty,
                'product_price' => $product->price,
                'product_id' => $product->id,
                'quantity' => $product->qty,
                'product_name' => $product->name,
                'product_image' => $product->options->image,
                'company_id' => $product->options->company_id,
                'key_editor_log' => $key_editor_log,
                'editor_id' => $product->options->editor_id
            ];
        }

        //  Tạo đơn hàng - Tạo sản phẩm trong đơn
        foreach ($bills as $company_id => $bill_data) {
            $bill_data = array_merge($bill_data, $bill_data_common);
            $bill = Bill::create($bill_data);
            foreach ($orders[$company_id] as $order_data) {
                $order_data['bill_id'] = $bill->id;
                Order::create($order_data);
            }
        }

        Cart::destroy();
        \Session::put('bill_group', $bill_group);
        return redirect('/thanh-toan/hoan-thanh');
    }

    public function getComplete()
    {
        $bill_group = \Session::get('bill_group');
        $bills = Bill::where('group_no', $bill_group)->get();
        $bill = Bill::where('group_no', $bill_group)->first();
        $data['bills'] = $bills;
        $data['bill'] = $bill;
        return view('themesemicolonwebjdes::pages.product.complete', $data);
    }

}
