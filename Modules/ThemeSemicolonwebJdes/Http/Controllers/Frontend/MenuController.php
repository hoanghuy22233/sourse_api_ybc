<?php

namespace Modules\ThemeSemicolonwebJdes\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\ThemeSemicolonwebJdes\Models\{Order, Category, Product};


class MenuController extends Controller
{
    function GetViewCart()
    {

        return view('themesemicolonwebjdes::template.menu_other');
    }
}
