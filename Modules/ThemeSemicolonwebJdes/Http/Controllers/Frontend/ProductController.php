<?php

namespace Modules\ThemeSemicolonwebJdes\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeSemicolonwebJdes\Models\{Company, Post, Product};
use Modules\ThemeSemicolonwebJdes\Models\Category;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    //  Trang danh mục
    public function list($slug)
    {
//        dd($slug);
        $category = Category::where('slug', $slug)->first();
        if (!is_object($category)) {
            abort(404);
        }
        $data['category'] = $category;
        $data['slug1'] = $slug;

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $category->meta_title != '' ? $category->meta_title : $category->name,
            'meta_description' => $category->meta_description != '' ? $category->meta_description : $category->name,
            'meta_keywords' => $category->meta_keywords != '' ? $category->meta_keywords : $category->name,
        ];
        view()->share('pageOption', $pageOption);

        if (in_array($category->type, [0, 1])) {         //  Tin tức
            $posts = CommonHelper::getFromCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page']);
            if (!$posts) {
                $posts = Post::where('status', 1)->where(function ($query) use ($category) {
                    $cat_childs = $category->childs; //  Lấy các id của danh mục con
                    $query->orWhere('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các tin thuộc danh mục hiện tại
                    foreach ($cat_childs as $cat_child) {
                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
                    }
                })->orderBy('order_no', 'desc')->orderBy('updated_at', 'desc')->paginate(12);
                CommonHelper::putToCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page'], $posts);
            }
            $data['posts'] = $posts;

            return view('themesemicolonwebjdes::pages.post.list_post')->with($data);
        } elseif (in_array($category->type, [5, 6])) {
            ;     //  Sản phẩm
            $products = CommonHelper::getFromCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page']);
            if (!$products) {
                $products = Product::where('status', 1)->where('company_id',$category->company_id)->where(function ($query) use ($category) {
                    $cat_childs = $category->childs; //  Lấy các id của danh mục con
                    $query->orWhere('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các tin thuộc danh mục hiện tại
                    foreach ($cat_childs as $cat_child) {
                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
                    }
                })->orderBy('order_no', 'desc')->orderBy('updated_at', 'desc')->paginate(12);
                CommonHelper::putToCache('posts_by_multi_cat_like_category_id' . $category->id . @$_GET['page'], $products);
            }

            $data['products'] = $products;

            return view('themesemicolonwebjdes::pages.product.shop-3-left-sidebar')->with($data);
        }
    }

    public function oneParam($slug1)
    {
//        dd($slug1);
        //  Neu la vao cty thi chuyen den trang danh sach san pham cua cong ty
        $com = Company::where('slug', $slug1)->where('status', 1)->first();
        if (is_object($com)) {
            return redirect($slug1 . '/san-pham');
        }

        //  Chuyen den trang danh sanh tin tuc | Danh sach san pham
        return $this->list($slug1);
    }

    //  VD: hobasoft/card-visit  | danh-muc-cha/danh-muc-con | danh-muc/bai-viet.html | danh-muc/san-pham.html
    public function twoParam($slug1, $slug2)
    {

        if (strpos($slug2, '.html')) {  //  Nếu là trang chi tiết
            $slug2 = str_replace('.html', '', $slug2);
            if (Product::where('slug', $slug2)->where('status', 1)->count() > 0) {        //  Chi tiet san pham
                return $this->detail($slug2);
            } else {        //  Chi tiet tin tuc
                $postController = new PostController();
                return $postController->detail($slug2);
            }
            abort(404);
        }

        return $this->list($slug2);
    }

    //  VD: hobasoft/card-visit/card-truyen-thong
    public function threeParam($slug1, $slug2, $slug3)
    {

        if (strpos($slug3, '.html')) {  //  Nếu là trang chi tiết
            $slug3 = str_replace('.html', '', $slug3);
            if (Product::where('slug', $slug3)->where('status', 1)->count() > 0) {        //  Chi tiet san pham
                return $this->detail($slug3);
            } else {        //  Chi tiet tin tuc
                $postController = new PostController();
                return $postController->detail($slug3);
            }
        }

        return $this->list($slug3);
    }

    public function fourParam($slug1, $slug2, $slug3, $slug4)
    {
        if (strpos($slug4, '.html')) {  //  Nếu là trang chi tiết
            $slug4 = str_replace('.html', '', $slug4);
            if (Product::where('slug', $slug4)->where('status', 1)->count() > 0) {        //  Chi tiet san pham
                return $this->detail($slug4);
            } else {        //  Chi tiet tin tuc
                $postController = new PostController();
                return $postController->detail($slug4);
            }
        }

        abort(404);
    }

    //  Trang chi tiết sản phẩm
    public function detail($slug)
    {
        $slug = str_replace('.html', '', $slug);
        $data['product'] = Product::where('slug', $slug)->where('status', 1)->first();

        if (!is_object($data['product'])) {
            abort(404);
        }

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['product']->meta_title != '' ? $data['product']->meta_title : $data['product']->name,
            'meta_description' => $data['product']->meta_description != '' ? $data['product']->meta_description : $data['product']->name,
            'meta_keywords' => $data['product']->meta_keywords != '' ? $data['product']->meta_keywords : $data['product']->name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themesemicolonwebjdes::pages.product.shop-single', $data);
    }
    function ListCompany()
    {
        return view('themesemicolonwebjdes::pages.product.list-company');
    }

    function listProductsOfCompany($comSlug)
    {
//        dd($comSlug);
        $data['comSlug']=$comSlug;
        $data['company'] = Company::where('slug', $comSlug)->first();
        if (!is_object($data['company'])) {
            abort(404);
        }
        $data['products'] = Product::where('company_id', $data['company']->id)
            ->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->paginate(12);

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Danh sách sản phẩm của công  ty ' . $data['company']->name,
            'meta_description' => 'Danh sách sản phẩm của công  ty ' . $data['company']->name,
            'meta_keywords' => 'Danh sách sản phẩm của công  ty ' . $data['company']->name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themesemicolonwebjdes::pages.product.shop-3-left-sidebar', $data);
    }
    function GetSearch(request $r)
    {
        $data['products']=Product::where('name','like','%'.$r->q.'%')->where('status', 1)->whereNotNull('company_id')->orderBy('order_no', 'desc')->orderBy('id', 'desc')->paginate(12);
        $data['q']=$r->q;
        $pageOption = [
            'meta_title' => 'Tìm kiếm từ khóa: ' . @$r->q,
            'meta_description' => 'Tìm kiếm từ khóa: ' . @$r->q,
            'meta_keywords' => 'Tìm kiếm từ khóa: ' . @$r->q,
        ];
        view()->share('pageOption', $pageOption);

        return view('themesemicolonwebjdes::pages.product.search',$data);
    }

}
