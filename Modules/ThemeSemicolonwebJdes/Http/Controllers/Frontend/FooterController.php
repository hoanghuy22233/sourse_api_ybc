<?php

namespace Modules\ThemeSemicolonwebJdes\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\ThemeSemicolonwebJdes\Models\{Widget,Setting};

class FooterController extends Controller
{
    function GetWidget()
    {
        return view('themesemicolonwebjdes::partials.footer');
    }
}
