<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;

class ContactUser extends Model
{

    protected $table = 'contact_user';

    protected $guarded = [];

    public $timestamps = false;
}
