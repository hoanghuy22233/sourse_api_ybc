<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;

class ProjectType extends Model {

    protected $table = 'project_types';

    protected $fillable = [
        'name', 'intro'
    ];

    public $timestamps = false;

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'project_type_id', 'id');
    }
}