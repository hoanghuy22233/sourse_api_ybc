<?php

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;

class InternalNotifications extends Model
{
    protected $table = "internal_notifications";
    protected $fillable = ['id',
        'title','content','admin_id','company_id'
    ];
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }


    public function Admin_ids()
    {
        return $this->belongsTo(Admin::class,'admin_id');
    }
}
