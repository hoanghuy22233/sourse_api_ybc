<?php

namespace Modules\ThemeSemicolonwebJdes\Models ;

use Illuminate\Database\Eloquent\Model;

class MoneyHistory extends Model
{
    protected $table = 'money_history';
    protected $guarded = [];
}
