<?php
/*$multi_cat = \Modules\ThemeSemicolonwebJdes\Models\Product::where('company_id', $company->id)->where('status', 1)->pluck('multi_cat')->toArray();
$cat_ids = [];
foreach ($multi_cat as $cats) {
    $cats = explode('|', $cats);
    foreach ($cats as $v) {
        if ($v != '') {
            $cat_ids[] = $v;
        }
    }
}
$categories = \Modules\ThemeSemicolonwebJdes\Models\Category::select(['id', 'name', 'slug'])->whereIn('id', $cat_ids)->where('status', 1)->orderBy('order_no', 'desc')->orderBy('name', 'asc')->get();*/

$categories = \Modules\ThemeSemicolonwebJdes\Models\Category::select(['id', 'name', 'slug'])->where('company_id', $company->id)
    ->whereNull('parent_id')->orderBy('order_no', 'desc')->orderBy('name', 'asc')->get();
?>
<h4>Danh mục</h4>
<ul class="menu">
    @foreach($categories as $cat)
        <li class="">
            <a href="/{{ @$company->slug }}/{{ $cat->slug }}" title="{{ $cat->name }}">{{ $cat->name }}</a>
            @if(count($cat->childs) > 0)
                <ul>
                    @foreach($cat->childs as $child)
                        <li class="">
                            <a href="/{{ @$company->slug }}/{{ @$child->slug }}/{{ $child->slug }}"
                               title="{{ $child->name }}">{{ $child->name }}</a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </li>
    @endforeach
</ul>

