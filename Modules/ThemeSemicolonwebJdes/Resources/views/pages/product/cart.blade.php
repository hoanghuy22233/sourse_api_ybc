@extends('themesemicolonwebjdes::layouts.default')
@section('main_content')
    <div id="wrapper" class="clearfix">
        @include('themesemicolonwebjdes::template.menu_other')
        <?php
        $widget = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content'])->where('location', 'title2')->where('status', 1)->first();
        ?>
        <section id="page-title" class="no-print">
            <div class="container clearfix">
                <h1>{{@$widget->name}}</h1>
                @include('themesemicolonwebjdes::partials.breadcrumb')
            </div>
        </section>
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="table-responsive">
                        <table class="table cart">
                            <thead>
                            <tr>
                                <th class="cart-product-remove">&nbsp;</th>
                                <th class="cart-product-thumbnail">&nbsp;</th>
                                <th class="cart-product-name">Sản phẩm</th>
                                <th class="cart-product-shop">Shop</th>
                                <th class="cart-product-price">Đơn giá</th>
                                <th class="cart-product-quantity">Số lượng</th>
                                <th class="cart-product-quantity">Editor</th>
                                <th class="cart-product-subtotal">Thành tiền</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($cart as $item)
                                <?php
                                $product = \Modules\ThemeSemicolonwebJdes\Models\Product::where('id', $item->id)->first();
                                $company = \Modules\EworkingCompany\Models\Company::where('id', $product->company_id)->first();
                                ?>
                                <tr class="cart_item">
                                    <td class="cart-product-remove">
                                        <a href="/gio-hang/del_item/{{ $item->rowId }}" class="remove"
                                           title="Remove this item"><i class="icon-trash2"></i></a>
                                    </td>
                                    <td class="cart-product-thumbnail">
                                        <a href="{{ @$company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct(\Modules\ThemeSemicolonwebJdes\Models\Product::find($item->id)) }}"><img
                                                    width="64" height="64"
                                                    src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb( $item->options->image) }}"
                                                    alt="{{ $item->name }}"></a>
                                    </td>
                                    <td class="cart-product-name">
                                        <a href="{{ @$company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct(\Modules\ThemeSemicolonwebJdes\Models\Product::find($item->id)) }}">{{ $item->name }}</a>
                                    </td>
                                    <td class="cart-product-shop">
                                        <a style="color: #333" ;
                                           href="{{ @$company->slug }}/san-pham">{{ @$company->short_name }}</a>
                                    </td>
                                    <td class="cart-product-price">
                                        @if($item->options->key_editor_log == null)
                                            <span class="amount"> {{ number_format($item->price,0,'',',') }}đ</span>
                                        @endif
                                    </td>
                                    <td class="cart-product-quantity">
                                        @if($item->options->key_editor_log == null)
                                            <div class="quantity clearfix">
                                                <input onchange="edit_item('{{ $item->rowId }}',this.value)"
                                                       type="number"
                                                       id="quantity" name="qty"
                                                       class="form-control input-number text-center"
                                                       value={{ $item->qty }}>
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        @if($item->options->key_editor_log != null)
                                            <?php $editor = \Modules\JdesSetting\Models\Editor::find($item->options->editor_id);?>
                                            @include('jdessetting::editor.partials.product_show_editor', ['product_id' => $product->id, 'key_editor_log' => $item->options->key_editor_log, 'order' => $editor])
                                        @endif
                                    </td>
                                    <td class="cart-product-subtotal">
                                        <span class="amount">{{ number_format($item->price*$item->qty,0,'',',') }}
                                            đ</span>
                                    </td>
                                </tr>
                            @endforeach
                            <tr class="cart_item">
                                <td colspan="8">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-12 nopadding">
                                            {{--<a href="#" class="button button-3d nomargin fright">Cập nhật giỏ hàng</a>--}}
                                            <a href="/thanh-toan" class="button button-3d notopmargin fright">Tiếp
                                                tục thanh toán</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-6 clearfix">
                            <h4>Tổng Giỏ Hàng</h4>
                            <div class="table-responsive">
                                <table class="table cart">
                                    <tbody>
                                    <tr class="cart_item">
                                        <td class="cart-product-name">
                                            <strong>Tổng tiền hàng</strong>
                                        </td>
                                        <td class="cart-product-name">
                                            <span class="amount">{{ $total }}đ</span>
                                        </td>
                                    </tr>
                                    <tr class="cart_item">
                                        <td class="cart-product-name">
                                            <strong>Phí vận chuyển</strong>
                                        </td>
                                        <td class="cart-product-name">
                                            <span class="amount">Miễn phí</span>
                                        </td>
                                    </tr>
                                    <tr class="cart_item">
                                        <td class="cart-product-name">
                                            <strong>Tổng đơn hàng</strong>
                                        </td>
                                        <td class="cart-product-name">
                                            <span class="amount color lead"><strong>{{ $total }}đ</strong></span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('custom_script')
    <script>
        function edit_item(rowId, qty) {
            $.get('/gio-hang/update_item/' + rowId + '/' + qty,
                function () {
                    location.reload();
                });
        }
    </script>
@endsection