@extends('themesemicolonwebjdes::layouts.default')
@section('shop')
    class="color-menu"
@endsection
@section('main_content')
    <div id="wrapper" class="clearfix">
        @include('themesemicolonwebjdes::template.menu_other')
        <?php
        $widget = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content'])->where('location', 'title7')->where('status', 1)->first();
        ?>
        <section id="page-title">
            <div class="container clearfix">
                <h1>{{@$widget->name}}</h1>
                @include('themesemicolonwebjdes::partials.breadcrumb')
            </div>
        </section>
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div id="portfolio-shuffle" class="portfolio-shuffle" data-container="#portfolio">
                        <i class="icon-random"></i>
                    </div>
                    <div class="clear"></div>
                    <div id="portfolio" class="portfolio grid-container portfolio-6 portfolio-masonry clearfix">
                        <?php
                        $companys = \Modules\ThemeSemicolonwebJdes\Models\Company::where('status',1)->paginate(12);
                        ?>
                        @foreach(@$companys as $company)
                            <article class="portfolio-item pf-media pf-icons" style="margin-right: 80px;height: 220px;">
                                <div class="portfolio-image">
                                    <a href="/{{$company->slug}}/san-pham">
                                        <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($company->image,185,130)}}"
                                             alt="{{@$company->name}}">
                                    </a>
                                    <div>
                                        <a href="/{{$company->slug}}" class="right-icon"></a>
                                    </div>
                                </div>
                                <div class="portfolio-desc">
                                    <h3><a href="/{{$company->slug}}">{{@$company->name}}</a></h3>
                                </div>
                            </article>
                        @endforeach
                            <div class="col-md-9">
                                {{ @$companys->appends(Request::all())->links() }}
                            </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection