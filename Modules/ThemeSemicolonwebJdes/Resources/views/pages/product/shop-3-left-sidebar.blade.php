@extends('themesemicolonwebjdes::layouts.default')
@section('sanpham')
    class="color-menu"
@endsection
@section('main_content')
    <div id="wrapper" class="clearfix">
        <?php
        $widget = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content'])->where('location', 'title1')->where('status', 1)->first();
        ?>
        @include('themesemicolonwebjdes::template.menu_company')
        <section id="page-title">
            <div class="container clearfix">
                <h1>{{@$widget->name}} {{@$company->short_name}}</h1>
                @include('themesemicolonwebjdes::partials.breadcrumb')
            </div>
        </section>
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="postcontent nobottommargin col_last">
                        <div id="shop" class="shop product-3 grid-container clearfix" data-layout="fitRows">
                            @foreach($products as $prd)
                                <div class="product clearfix">
                                    <div class="product-image">
                                        <a href="/{{$company->slug}}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($prd) }}"><img
                                                    src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($prd->image,265,352)}}"
                                                    alt="{{$prd->name}}"></a>
                                        <a href="/{{$company->slug}}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($prd) }}"><img
                                                    src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($prd->image,265,352)}}"
                                                    alt="{{$prd->name}}"></a>
                                    </div>
                                    <div class="product-desc center">
                                        <div class="product-title"><h3><a
                                                        href="/{{$company->slug}}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($prd) }}">{{$prd->name}}</a>
                                            </h3></div>
                                        <div class="product-price">
                                            <del>{{ number_format($prd->base_price,0,'',',') }}đ</del>
                                            <ins>{{ number_format($prd->final_price,0,'',',') }}đ</ins>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="col-md-9 paginatee">
                                {{ $products->appends(Request::all())->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="sidebar nobottommargin">
                        <div class="sidebar-widgets-wrap">
                            <div class="widget widget_links clearfix">
                                @include('themesemicolonwebjdes::pages.product.danhmuc')
                            </div>
                            <div class="widget clearfix">
                                @include('themesemicolonwebjdes::pages.product.new_product')
                            </div>
                            <div class="widget clearfix">
                                @include('themesemicolonwebjdes::pages.product.popular_product')
                            </div>
                            <div class="widget clearfix">
                                <?php
                                $widgets = \Modules\ThemeSemicolonwebJdes\Models\Widget::select(['name', 'content', 'location'])->where('location', 'facebook0')->where('status', 1)->first();
                                ?>
                                    @if(isset($company->slug))
                                        {!! @$company->fanpage !!}
                                    @else
                                        {!! @$widgets->content !!}
                                    @endif
                                {{--<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fjdesvietnam%2F&tabs=272&width=270&height=196&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId"--}}
                                        {{--width="270" height="196" style="border:none;overflow:hidden" scrolling="no"--}}
                                        {{--frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>--}}
                                <div class="widget subscribe-widget clearfix">
                                    <h4>Đăng ký bản tin</h4>
                                    <h5>Đăng ký email để chập nhật thông tin thường xuyên:</h5>
                                    <div class="input-group divcenter">
                                        <div class="row">
                                            <input type="text" id="myInput" value="" class="form-control col-10" name="email"
                                                    placeholder="Nhập Email" required="">
                                            {{--<div class="input-group-append">--}}
                                            <button id="myBtn" style="cursor: pointer;"
                                                    class="btn btn-success add-mail2 input-group-append col-2"><i class="icon-email2"></i>
                                            </button>
                                            {{--</div>--}}
                                        </div>

                                    </div>
                                </div>
                                <div class="widget clearfix" style="margin-top: 25px;">
                                    <h4>Khách Hàng</h4>
                                </div>
                                <div id="oc-clients-full" class="owl-carousel image-carousel carousel-widget"
                                     data-items="1" data-margin="10" data-loop="true" data-nav="false"
                                     data-autoplay="5000" data-pagi="false">
                                    <?php
                                    $users = \Modules\ThemeSemicolonwebJdes\Models\User::where('company_id', $company->id)->where('status', 1)->get();
                                    ?>
                                    @foreach($users as $user)
                                        <div class="oc-item"><a><img
                                                        src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($user->image,251,188)}}"
                                                        alt="{{$user->name}}"></a></div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

