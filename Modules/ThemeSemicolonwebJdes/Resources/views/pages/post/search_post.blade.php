@extends('themesemicolonwebjdes::layouts.default')
@section('main_content')
    <div id="wrapper" class="clearfix">
        @include('themesemicolonwebjdes::template.menu_other')
        <section id="page-title">
            <div class="container clearfix">
                <div class="row" style="height: 30px;">
                    <div class="title_search col-md-8">
                        <h1>Kết quả tìm kiếm với từ khóa: {{ @$search}}</h1>
                    </div>
                    <div class="search_post col-md-4">
                        <form action="/search-bai-viet" class="form-inline">
                            <input style="width: 270px;" name="search" class="form-control boder" type="search"
                                   placeholder="Tìm kiếm bài viết" aria-label="Search">
                            <button class="btn btn-success " type="submit">Tìm kiếm</button>
                        </form>
                    </div>
                </div>
                @include('themesemicolonwebjdes::partials.breadcrumb')
                {{--<h1>Kết quả tìm kiếm với từ khóa: {{ @$search}}</h1>--}}
                {{--@include('themesemicolonwebjdes::partials.breadcrumb')--}}
                {{--<div>--}}
                    {{--<form action="/search-bai-viet" class="form-inline">--}}
                        {{--<input name="search" class="form-control mt-3 search-post" type="search" placeholder="Tìm kiếm bài viết" aria-label="Search">--}}
                        {{--<button class="btn btn-success mt-3" type="submit">Tìm kiếm</button>--}}
                    {{--</form>--}}
                {{--</div>--}}
            </div>
        </section>
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                    <div class="postcontent nobottommargin col_last clearfix">
                        <div id="posts" class="small-thumbs">
                            @foreach($posts as $post)
                                <div class="entry clearfix">
                                    <div class="entry-image">
                                        <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($post) }}"><img
                                                    class="image_fade"
                                                    src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image,300,225)}}"
                                                    alt="{{$post->name}}"></a>
                                    </div>
                                    <div class="entry-c">
                                        <div class="entry-title">
                                            <h2>
                                                <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($post) }}">{{$post->name}}</a>
                                            </h2>
                                        </div>
                                        <ul class="entry-meta clearfix">
                                            <li>
                                                <i class="icon-calendar3"></i> {{date('d-m-Y',strtotime($post->created_at))}}
                                            </li>
                                        </ul>
                                        <div class="entry-content">
                                            <p>{{$post->intro}}</p>
                                            <a href="{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($post) }}"
                                               class="more-link">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-12 paginatee">
                                {{ $posts->appends(Request::all())->links() }}
                            </div>
                        </div>
                    </div>
                    <div class="sidebar nobottommargin clearfix">
                        <div class="sidebar-widgets-wrap">
                            {{--<div class="widget clearfix">--}}
                                {{--<h4>Ảnh bìa</h4>--}}
                                {{--<div id="flickr-widget" class="flickr-feed masonry-thumbs" data-id="613394@N22"--}}
                                     {{--data-count="16" data-type="group" data-lightbox="gallery"></div>--}}
                            {{--</div>--}}
                            @include('themesemicolonwebjdes::pages.home.partials.popular_post')
                            @include('themesemicolonwebjdes::pages.home.partials.banner')
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
