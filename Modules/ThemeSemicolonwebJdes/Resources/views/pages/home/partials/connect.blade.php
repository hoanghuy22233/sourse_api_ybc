<?php
?>
<div class="widget clearfix">
    <div class="col_one_third nobottommargin mxh">
        <a href="{!! @$settings['fanpage'] !!}" target="_blank" class="social-icon si-dark si-colored si-facebook nobottommargin"
           style="margin-right: 10px;">
            <i class="icon-facebook"></i>
            <i class="icon-facebook"></i>
        </a>
        <small style="display: block; margin-top: 3px;"><strong>
                <div class="counter counter-inherit"><span>123</span>
                </div>
            </strong>Thích
        </small>
    </div>
    <div class="col_one_third nobottommargin mxh">
        <a href="{!! @$settings['skype'] !!}" target="_blank" class="social-icon si-dark si-colored si-twitter nobottommargin"
           style="margin-right: 10px;">
            <i class="icon-skype"></i>
            <i class="icon-skype"></i>
        </a>
        <small style="display: block; margin-top: 3px;"><strong>
                <div class="counter counter-inherit"><span>123</span>
                </div>
            </strong>Theo dõi
        </small>
    </div>
    <div class="col_one_third nobottommargin mxh col_last">
        <a href="{!! @$settings['instagram']  !!}" target="_blank" class="social-icon si-dark si-colored si-rss nobottommargin"
           style="margin-right: 10px;">
            <i class="icon-instagram"></i>
            <i class="icon-instagram"></i>
        </a>
        <small style="display: block; margin-top: 3px;"><strong>
                <div class="counter counter-inherit"><span >123</span>
                </div>
            </strong>Người đọc
        </small>
    </div>
</div>