<?php
$banner = \Modules\ThemeSemicolonwebJdes\Models\Banner::where('location', 'slide_home')->where("status", 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->get();
?>
<div class="col_full bottommargin-lg">
    <div class="fslider flex-thumb-grid grid-6" data-animation="fade" data-arrows="true"
         data-thumbs="true">
        <div class="flexslider">
            <div class="slider-wrap">
                @foreach($banner as $ban)
                    <div class="slide"
                         data-thumb="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($ban->image,123,93) }}">
                        <a href="{{$ban->link}}">
                            <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($ban->image,750,422)}}"
                                 alt="{{$ban->name}}">
                            <div class="overlay">
                                <div class="text-overlay">
                                    <div class="text-overlay-title">
                                        <h3>{{$ban->name}}</h3>
                                    </div>
                                    <div class="text-overlay-meta">
                                        <span>{{$ban->intro}}</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>