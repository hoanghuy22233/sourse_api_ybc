<?php
$populars = \Modules\ThemeSemicolonwebJdes\Models\Product::where('popular', 1)->where('status',1)->take(6)->orderBy('order_no', 'DESC')->orderBy('id','DESC')->get();
?>
<div class="fancy-title title-border">
    <h3>Sản phẩm được ưa chuộng</h3>
</div>
<div class="col_one_third" style="width: 100%;">
    @foreach($populars as $key => $popular)
        <div class="ipost clearfix" style="width: 33%;float: left; padding: 20px; @if($key%3==0) clear:both; @endif ">
            <div class="entry-image">
                <a href="{{ @$popular->company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($popular) }}"><img
                            class="image_fade"
                            src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($popular->image, 230, 172) }}"
                            alt="{{$popular->name}}"></a>
            </div>
            <div class="entry-title">
                <h3>
                    <a href="{{ @$popular->company->slug }}{{ Modules\ThemeSemicolonwebJdes\Http\Helpers\ThemeHelper::getUrlProduct($popular) }}">{{$popular->name}}</a>
                </h3>
            </div>
        </div>
    @endforeach
</div>