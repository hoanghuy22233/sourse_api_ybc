<?php
namespace Modules\EworkingUser\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Modules\EworkingCompany\Models\Company;

class User extends Model
{

    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password', 'phone', 'image', 'address', 'image', 'gender', 'birthday','balance','stk','change_password','api_token'
    ];

    public function admin(){
        return $this->belongsTo(Admin::class,'admin_id');
    }

    public function company(){
        return $this->belongsTo(Company::class,'company_id');
    }
}
