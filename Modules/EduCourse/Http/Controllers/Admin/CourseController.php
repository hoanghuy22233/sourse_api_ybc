<?php

namespace Modules\EduCourse\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\EduCourse\Models\Admin;
use Modules\EduCourse\Models\Category;
use Modules\EduCourse\Models\Lesson;
use Modules\ThemeEduAdmin\Models\Menu;
use Validator;

class CourseController extends CURDBaseController
{
    protected $orderByRaw = 'order_no desc, id desc';
    protected $module = [
        'code' => 'course',
        'table_name' => 'courses',
        'label' => 'Khóa học',
        'modal' => '\Modules\EduCourse\Models\Course',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh'],
            ['name' => 'name', 'type' => 'text', 'label' => 'Tên'],
            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'educourse::list.td.multi_cat', 'label' => 'Danh mục', 'object' => 'category'],
            ['name' => 'type', 'type' => 'select', 'label' => 'Thể loại', 'options' => [
                '' => 'Thể loại',
                'Text-Audio' => 'Text-Audio',
                'Video' => 'Video',
                'Học online' => 'Học online',
                'Học offline' => 'Học offline',
                'Học online/offline' => 'Học online/offline',
            ],],
//            ['name' => 'slug', 'type' => 'text', 'label' => 'Đường dẫn'],
//            ['name' => 'category_id', 'type' => 'text', 'label' => 'Danh mục'],
//            ['name' => 'category_id', 'type' => 'relation', 'label' => 'Danh mục', 'object' => 'category', 'display_field' => 'name'],
            ['name' => 'lecturer_id', 'type' => 'relation', 'label' => 'Giáo viên', 'object' => 'admin', 'display_field' => 'name'],
            ['name' => 'order_no', 'type' => 'text', 'label' => 'STT'],
//            ['name' => 'multi_cate', 'type' => 'custom', 'td' => 'educourse::list.td.multi_cat', 'label' => 'Danh mục', 'object' => 'category_product'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],
            ['name' => 'action', 'type' => 'custom', 'td' => 'educourse::list.td.action', 'class' => '', 'label' => '#'],
        ],
        'form' => [
            'general_tab' => [

                ['name' => 'name', 'type' => 'text', 'label' => 'Tên'],
                ['name' => 'multi_cat', 'type' => 'custom', 'field' => 'educourse::form.fields.multi_cat', 'label' => 'Danh mục', 'model' => Category::class,
                    'object' => 'category', 'display_field' => 'name', 'multiple' => true,'where' => 'type=5', 'des' => 'Danh mục đầu tiên chọn là danh mục chính', 'group_class' => 'col-md-6'],
//                ['name' => 'multi_cate', 'type' => 'custom', 'field' => 'educourse::form.fields.multi_cat', 'label' => 'Danh mục', 'model' => Category::class,'display_field' => 'name_vi', 'object' => 'category_course'],
                ['name' => 'type', 'type' => 'select', 'options' => [
                    '' => 'Thể loại',
                    'Text-Audio' => 'Text-Audio',
                    'Video' => 'Video',
                    'Học online' => 'Học online',
                    'Học offline' => 'Học offline',
                    'Học online/offline' => 'Học online/offline',
                ], 'class' => 'required', 'label' => 'Chọn thể loại', 'group_class' => 'col-md-6'],
                ['name' => 'intro', 'type' => 'textarea_editor', 'label' => 'Mô tả ngắn'],
                ['name' => 'content', 'type' => 'textarea_editor', 'label' => 'Nội dung'],
//                ['name' => 'category_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Danh mục', 'model' => Category::class, 'display_field' => 'name', 'group_class' => 'col-md-6'],
                ['name' => 'lecturer_id', 'type' => 'select2_ajax_model', 'class' => '', 'label' => 'Giáo viên', 'model' => Admin::class, 'display_field' => 'name', 'object' => 'admin'],
                ['name' => 'base_price', 'type' => 'text', 'class' => 'number_price', 'label' => 'Giá ban đầu',  'group_class' => 'col-md-6'],
                ['name' => 'final_price', 'type' => 'text', 'class' => 'number_price', 'label' => 'Giá khuyến mãi', 'group_class' => 'col-md-6'],
                                ['name' => 'time', 'type' => 'text', 'class' => '', 'label' => 'Thời gian khóa học', 'group_class' => 'col-md-6'],
                ['name' => 'date_of_possession', 'type' => 'text', 'label' => 'Thời hạn', 'group_class' => 'col-md-6'],
                ['name' => 'level', 'type' => 'text', 'label' => 'Trình độ', 'group_class' => 'col-md-6'],
                ['name' => 'certificate', 'type' => 'text', 'label' => 'Chứng chỉ', 'group_class' => 'col-md-6'],
                ['name' => 'utilities', 'type' => 'text', 'label' => 'Xem được trên'],

                ['name' => 'status', 'type' => 'checkbox', 'class' => '', 'label' => 'Kích hoạt', 'value' => 1, 'group_class' => 'col-md-3'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'Thứ tự ưu tiên', 'value' => 0, 'group_class' => 'col-md-3', 'des' => 'Số to ưu tiên hiển thị trước'],

            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh'],
            ],
            'info_tab' => [
                ['name' => 'document', 'type' => 'custom', 'fields' => 'educourse::form.fields.dynamic4','class' => 'required', 'label' => 'Tài liệu'],
            ],

            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'Slug', 'des' => 'Đường dẫn trên thanh địa chỉ'],
                ['name' => 'meta_title', 'type' => 'custom', 'field' => 'educourse::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta title', 'max_char' => 1000],
                ['name' => 'meta_description', 'type' => 'custom', 'field' => 'educourse::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta description', 'max_char' => 1000],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, tên',
        'fields' => 'id, name'
    ];

    protected $filter = [
        'category_id' => [
            'label' => 'Danh mục',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'category',
            'model' => \Modules\EduCourse\Models\Category::class,
            'where' => 'type=5',
            'query_type' => 'custom'
        ],
        'type' => [
            'label' => 'Thể loại',
            'type' => 'select',
            'options' => [
                '' => 'Thể loại',
                'Text-Audio' => 'Text-Audio',
                'Video' => 'Video',
                'Học online' => 'Học online',
                'Học offline' => 'Học offline',
                'Học online/offline' => 'Học online/offline',
            ],
            'query_type' => '='
        ],
        'lecturer_id' => [
            'label' => 'Giáo viên',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'admin',
            'model' => \Modules\EduCourse\Models\Admin::class,
            'query_type' => 'custom'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('educourse::list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        }

        //  Lọc theo danh mục
        if (!is_null($request->get('category_id'))) {
            $category = Category::find($request->category_id);
            if (is_object($category)) {
                $query = $query->where(function ($query) use ($category) {
                    $cat_childs = $category->childs; //  Lấy các id của danh mục con
                    $query->orWhere('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các tin thuộc danh mục hiện tại
                    foreach ($cat_childs as $cat_child) {
                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
                    }
                });
            }
        }
        if (!is_null($request->get('tags'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }
        return $query;
    }

    public function add(Request $request)
    {

        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('educourse::add')->with($data);
            } else if ($_POST) {
//                dd($request);
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    if ($request->has('names')) {
                        $data['names'] = implode('|', $request->names);
                    }

                    if ($request->has('links')) {
                        $data['links'] = implode('|', $request->links);
                    }

                    $data['document'] = [];
                    if ($request->has('document_names')) {
                        foreach ($request->document_names as $k => $v) {
                            $data['document'][] = [
                                'names' => $v,
                                'links' => $request->document_links[$k],
                            ];
                        }
                    }
//                    dd($data);

                    $data['document'] = json_encode($data['document']);
                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
//                    if ($request->has('tags')) {
//                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
//                    }
//                    if ($request->has('image_extra')) {
//                        $data['image_extra'] = implode('|', $request->image_extra);
//                    }
//                    if ($request->has('input_image_extra')) {
//                        $data['input_image_extra'] = implode('|', $request->input_image_extra);
//                    }
                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
//dd($data);
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache($this->module['table_name']);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code'].'/'.$this->model->id);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('educourse::edit')->with($data);
        } else if ($_POST) {
//            dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ], [
                'name.required' => 'Bắt buộc phải nhập tên',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                if ($request->has('names')) {
                    $data['names'] = implode('|', $request->names);
                }

                if ($request->has('links')) {
                    $data['links'] = implode('|', $request->links);
                }
//                    $data['parameters'] = json_decode($request->parameters);
//                    $data['product_name'] = json_decode($request->product_name);
//                    $data['result'] = json_decode($request->result);
//                    $data['standard'] = json_decode($request->standard);

                $data['document'] = [];
                if ($request->has('document_names')) {
                    foreach ($request->document_names as $k => $v) {
                        $data['document'][] = [
                            'names' => $v,
                            'links' => $request->document_links[$k],
                        ];
                    }
                }
                $data['document'] = json_encode($data['document']);

//                dd($data);
//                  Tùy chỉnh dữ liệu insert
                if ($request->has('multi_cat')) {
                    $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                    $data['category_id'] = $request->multi_cat[0];
                }
//                if ($request->has('tags')) {
//                    $data['tags'] = '|' . implode('|', $request->tags) . '|';
//                }
//                if ($request->has('image_extra')) {
//                    $data['image_extra'] = implode('|', $request->image_extra);
//                }
//                if ($request->has('input_image_extra')) {
//                    $data['input_image_extra'] = implode('|', $request->input_image_extra);
//                }
                #

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    CommonHelper::flushCache($this->module['table_name']);
                }

                else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function duplicate(Request $request, $id)
    {
        $poduct = Lesson::find($id);
        $poduct_new = $poduct->replicate();
        $poduct_new->company_id = \Auth::guard('admin')->user()->last_company_id;
        $poduct_new->admin_id = \Auth::guard('admin')->user()->id;
        $poduct_new->save();
        return $poduct_new;
    }
}
