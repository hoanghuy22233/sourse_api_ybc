@if(in_array('bill_view', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="/admin/bill" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i class="kt-menu__link-icon flaticon-folder-1"></i>
                    </span><span class="kt-menu__link-text">Đơn hàng</span></a>
    </li>
@endif