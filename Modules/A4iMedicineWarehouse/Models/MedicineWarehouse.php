<?php

namespace Modules\A4iMedicineWarehouse\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\A4iSeason\Models\PlantMedicine;
use Modules\EworkingAdmin\Models\Admin;
use Modules\A4iLand\Models\Land;
use Modules\A4iSeason\Models\Season;

class MedicineWarehouse extends Model
{

    protected $table = 'medicine_warehouses';
    public $timestamps = false;
    protected $fillable = [
        'name', 'image', 'quantity', 'execution_time', 'admin_id'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function land()
    {
        return $this->belongsTo(Land::class, 'land_id');
    }
    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }
    public function plant_medicine(){
        return $this->belongsTo(PlantMedicine::class, 'plant_medicine_id');

    }

}
