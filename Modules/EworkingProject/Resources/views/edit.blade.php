@extends(config('core.admin_theme').'.template')
@section('main')
    <form class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid {{ @$module['code'] }}"
          action="" method="POST"
          enctype="multipart/form-data">
        {{ csrf_field() }}
        <input name="return_direct" value="save_continue" type="hidden">
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="kt-portlet kt-portlet--last kt-portlet--head-lg kt-portlet--responsive-mobile"
                     id="kt_page_portlet">
                    <div class="kt-portlet__head kt-portlet__head--lg" style="">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Chỉnh sửa {{ $module['label'] }}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <a href="/admin/{{ $module['code'] }}" class="btn btn-clean kt-margin-r-10">
                                <i class="la la-arrow-left"></i>
                                <span class="kt-hidden-mobile">Quay lại</span>
                            </a>
                            {{--@if(in_array($module['code'].'_edit', $permissions))--}}
                                {{--<div class="btn-group pr-4 pb-4" style="float: right;">--}}
                                {{--<button type="submit" class="btn btn-brand">--}}
                                    {{--<i class="la la-check"></i>--}}
                                    {{--<span class="kt-hidden-mobile">Lưu</span>--}}
                                {{--</button>--}}
                                {{--</div>--}}
                            {{--@endif--}}
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-4" style="padding-right:0;padding-left:10px">
                <!--begin::Portlet-->
                {{--<div class="kt-portlet">--}}
                {{--<div class="kt-portlet__head">--}}
                {{--<div class="kt-portlet__head-label">--}}
                {{--<h3 class="kt-portlet__head-title">--}}
                {{--Thông tin chung <a class="btn-add edit-general"--}}
                {{--data-name="{{$result->name}}"--}}
                {{--data-common_total_vi="{{$result->common_total_vi}}"--}}
                {{--data-common_total_vi_sub="{{number_format($result->common_total_vi)}}"--}}
                {{--data-user_id="{{$result->user_id}}"--}}
                {{--data-intro="{{$result->intro}}"--}}
                {{--title="Sửa thông tin chung dự án"><i--}}
                {{--class="flaticon-edit"></i></a>--}}
                {{--</h3>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<!--begin::Form-->--}}
                {{--<div class="kt-form">--}}
                {{--<div class="kt-portlet__body" style="    padding-top: 10px;">--}}
                {{--<div class="kt-section kt-section--first">--}}
                {{--@foreach($module['form']['general_tab'] as $field)--}}
                {{--@php--}}
                {{--$field['value'] = @$result->{$field['name']};--}}
                {{--@endphp--}}
                {{--<div class="form-group-div form-group {{ @$field['group_class'] }}"--}}
                {{--id="form-group-{{ $field['name'] }}">--}}
                {{--<label for="{{ $field['name'] }}">{{ @$field['label'] }} @if(strpos(@$field['class'], 'require') !== false)--}}
                {{--<span class="color_btd">*</span>@endif</label>--}}
                {{--<div class="col-xs-12">--}}
                {{--@if($field['type'] == 'custom')--}}
                {{--@include($field['field'], ['field' => $field])--}}
                {{--@else--}}
                {{--@include(config('core.admin_theme').".form.fields.".$field['type'], ['field' => $field])--}}
                {{--@endif--}}

                {{--<span class="text-danger">{{ $errors->first($field['name']) }}</span>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--@endforeach--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--</div>--}}

                {{--<!--end::Form-->--}}
                {{--</div>--}}
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
					<span class="kt-portlet__head-icon">
						<i class="kt-font-brand flaticon-folder-1"></i>
					</span>
                            <h3 class="kt-portlet__head-title">
                                Thông tin chung
                            </h3>
                            <a class="btn-add edit-general-project"
                               data-name="{{$result->name}}"
                               data-common_total_vi="{{$result->common_total_vi}}"
                               data-common_total_vi_sub="{{number_format($result->common_total_vi)}}"
                               data-user_id="{{$result->user_id}}"
                               data-user_name="{{@$result->partner->short_name}}"
                               data-intro="{{$result->intro}}"
                               title="Sửa thông tin chung dự án"><i
                                        class="flaticon-edit"></i></a>
                        </div>

                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget5__content">
                            <div class="kt-widget5__section general-value">
                                <div style="display: flex;">
                                    <a class="kt-widget5__title" style="font-size: 2.0rem;">
                                        <span class="kt-font-info">{{@$result->name}}</span>
                                    </a>
                                </div>
                                <div class="kt-widget5__info">
                                    <p>
                                        <span>Tổng tiền:</span>
                                        <span class="kt-font-info">{{number_format($result->common_total_vi)}} <sup>đ</sup></span>
                                    </p>
                                    <p>
                                        <span>Đối tác:</span>
                                        <span class="kt-font-info">
                                {{@$result->partner->short_name}}
                            </span>
                                    </p>
                                </div>
                                <p class="kt-widget5__desc">
                                    Ghi chú : <span class="kt-font-info">
                              {{@$result->intro}}
                        </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->
            @include('eworkingproject::form.fields.thau_phu')
            <!--end::Portlet-->
            </div>

            <div class="col-xs-12 col-md-8" style="padding-right:0;padding-left:10px">
                <!--begin::Portlet-->
            @include('eworkingproject::form.fields.cong_viec')
            <!--end::Portlet-->
                <!--begin::Portlet-->
            @include('eworkingproject::form.fields.lich_su_tien')
            <!--end::Portlet-->
            </div>
        </div>
    </form>
    @include('eworkingproject::popup.popup_add_sub_partner')
    @include('eworkingproject::popup.popup_add_money_history')
    @include('eworkingproject::popup.edit_general')
    @include('eworkingjob::popup.job.edit_general',['project_id'=>$result->id,'project_name'=>$result->name])
@endsection
@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/form.css') }}">
{{--    <link type="text/css" rel="stylesheet" charset="UTF-8"--}}
{{--          href="{{ asset('/Modules\EworkingProject\Resources\assets\css\custom.css') }}">--}}
{{--    <script src="{{asset('Modules\EworkingProject\Resources\assets\js\custom.js')}}"></script>--}}
@endsection
@section('custom_footer')
    <script src="{{ asset(config('core.admin_asset').'/js/pages/crud/metronic-datatable/advanced/vertical.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/form.js') }}"></script>
    <script type="text/javascript" src="{{ asset(config('core.admin_asset').'/js/list.js') }}"></script>
@endsection
