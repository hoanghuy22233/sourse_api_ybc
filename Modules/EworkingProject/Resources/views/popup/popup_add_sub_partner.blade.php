<div class="modal fade" id="popup-add-sub-partner" role="dialog" style="z-index: 100000">
    <form id="form-sub-partner" action="post" autocomplete="off">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content " style="height:auto">
                <div class="modal-header">
                    <h4 style="display:inline-block;" class="title-sub-partner">Thêm thầu phụ</h4>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                <label for="usr">Tên đối tác</label>
                                <input type="text" name="name" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                <label for="usr">Số tiền</label>
                                <input type="text" name="price" class="form-control number_price">
                                <input type="text" name="project_id" value="{{$result->id}}" class="hidden">
                                <input type="text" name="id" class="hidden">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{--<button class="btn btn-default" data-dismiss="modal"></button>--}}
                    <button class="btn btn-success" type="submit"><i class="icon-header fa fa-check-circle"></i> Hoàn
                        thành
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $('body').on('click', '.edit-sub-partner', function () {
        let modal = $('#popup-add-sub-partner');

        modal.find(".title-sub-partner").text($(this).attr('title'));
        modal.find("input[name='id']").val($(this).data('id'));
        modal.find("input[name='name']").val($(this).data('name'));
        modal.find(".number_price").val($(this).data('price'));
        modal.find("input[name='price']").val($(this).data('price'));
        modal.modal();
    });

    $('body').on('click', '.btn-add-sub-partner', function () {
        let modal = $('#popup-add-sub-partner');
        modal.find(".title-sub-partner").text($(this).attr('title'));
        modal.modal();
    });
    $(document).ready(function () {
        // form popup
        $('#form-sub-partner').validate({
            onfocusout: false,
            onkeyup: false,
            onclick: false,
            rules: {
                name: {
                    required: true,
                },
                price: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: "<p style='color: red'>Tên thầu phụ không được để trống</p>",
                },
                price: {
                    required: "<p style='color: red'>Số tiền không được để trống</p>",
                },
            },
            submitHandler: function (form) {
                //code in here
                // console.log($('#form-sub-partner').serializeArray());

                var modal = $('#popup-add-sub-partner');
                let btn = modal.find('.btn');
                btn.children().hide();
                btn.attr("disabled", true);
                btn.addClass('kt-spinner').addClass('btn-outline').addClass('kt-spinner--sm').addClass('kt-spinner--light');
                setTimeout(function () {
                    var url = '{{route('sub-partner.ajax-update')}}';
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: $('#form-sub-partner').serializeArray(),
                        success: function (data) {
                            if (data.status) {
                                window.location.reload();
                                toastr.success("cập nhật thành công!");
                            } else {
                                modal.find('.close').click();
                                toastr.error("Thất bại!");
                            }
                        }
                    });
                    btn.children().show();
                    btn.removeClass('kt-spinner').removeClass('btn-outline').removeClass('kt-spinner--sm').removeClass('kt-spinner--light');
                }, 1000);

            }
        });
    });
</script>
