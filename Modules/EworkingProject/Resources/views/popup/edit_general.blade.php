<div class="modal fade" id="popup-edit-general-project" role="dialog">
    <form id="form-edit-general-project"  action="post" autocomplete="off">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content " style="height:auto">
                <div class="modal-header">
                    <h4 class="title-name" style="display:inline-block;">Thông tin chung</h4>
                    <button type="button" class="close" data-dismiss="modal"></button>
                </div>
                <div class="modal-content">

                    <div class="row" style="margin: 15px">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="usr">Tên dự án</label>
                                        <input type="text" name="name" id="name" class="form-control value">
                                        <input type="text" name="type" id="name" style="display: none">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="usr">Tổng tiền</label>
                                        <input type="text" name="common_total_vi" id="name"
                                               class="form-control number_price value">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="usr">Đối tác</label>
                                        <?php

                                        $field = ['name' => 'user_id', 'type' => 'eworkingjob::form.select2_ajax_model', 'class' => 'required', 'label' => 'Chọn đối tác',
                                            'where_this_company' => true, 'object' => 'user', 'model' => \Modules\EworkingUser\Models\User::class, 'display_field' => 'short_name'];
                                        if (isset($user_name) && isset($user_id)) {
                                            $field['disabled'] = '';
                                        }
                                        ?>
                                        @include($field['type'], ['field' => $field])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="usr">Ghi chú</label>
                                        <div class='input-group date' id='start_date'>
                                            <textarea name="intro" type="text" id="" class="value" cols="55"
                                                      rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit"><i class="icon-header fa fa-check-circle"></i> Hoàn
                        thành
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        //them thong tin chung
        $('body').on('click', '.add-project', function (event) {
            event.preventDefault();
            let modal = $('#popup-edit-general-project');
            let form = modal.find('form');
            form.validate().resetForm();
            form[0].reset();
            @if (isset($user_name) && isset($user_id))
            modal.find('select[name=user_id]').select2('trigger', 'select', {
                data: {
                    id: '{{$user_id}}',
                    text: '{{$user_name}}',
                }
            });
            @endif
            modal.find('input[name=type]').val('add');
            modal.find('.title-name').text($(this).attr('title'));
            modal.modal();
        });
        //sửa thong tin chung
        $('body').on('click', '.edit-general-project', function () {
            let modal = $('#popup-edit-general-project');
            loading();
            let find_data = $(this);
            modal.find('select[name=user_id]').select2('trigger', 'select', {
                data: {
                    id: find_data.data('user_id'),
                    text: find_data.data('user_name').trim(),
                }
            });
            modal.find('input[name=name]').val(find_data.data('name'));
            modal.find('input[name=common_total_vi]').val(find_data.data('common_total_vi'));
            modal.find('input.number_price').val(find_data.data('common_total_vi_sub'));
            modal.find('textarea[name=intro]').val(find_data.data('intro'));
            modal.find('input[name=type]').val('edit');
            modal.find('.title-name').text($(this).attr('title'));
            stopLoading();
            modal.modal();
        });

        $('#form-edit-general-project').validate({
            onfocusout: false,
            onkeyup: false,
            onclick: false,
            rules: {
                name: {
                    required: true,
                },
                common_total_vi: {
                    required: true,
                },
                user_id: {
                    required: true,
                },
            },
            messages: {
                name: {
                    required: "<p style='color: red'>Tên dự án không được để trống</p>",
                },
                common_total_vi: {
                    required: "<p style='color: red'>Tổng tiền không được để trống</p>",
                },
                user_id: {
                    required: "<p style='color: red'>Đối tác không được để trống</p>",
                },
            },
            submitHandler: function (form) {
                //code in her
                event.preventDefault();
                let modal = $('#popup-edit-general-project');
                if (modal.find('input[name=type]').val() == 'edit') {
                    $.ajax({
                        url: '{{URL::to('/admin/project/'.@$result->id)}}',
                        type: 'POST',
                        data: {
                            name: modal.find('input[name=name]').val(),
                            common_total_vi: modal.find('input[name=common_total_vi]').val(),
                            user_id: modal.find('select[name=user_id]').val(),
                            intro: modal.find('textarea[name=intro]').val(),
                            project_id: '{{@$result->id}}',
                        },
                        success: function (resp) {
                            window.location.reload();
                        },
                        error: function () {
                            alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                        }
                    });
                } else if (modal.find('input[name=type]').val() == 'add') {
                    $.ajax({
                        url: '{{route('project.add')}}',
                        type: 'POST',
                        data: {
                            name: modal.find('input[name=name]').val(),
                            common_total_vi: modal.find('input[name=common_total_vi]').val(),
                            user_id: modal.find('select[name=user_id]').val(),
                            intro: modal.find('textarea[name=intro]').val(),
                        },
                        success: function (resp) {
                            if (resp.status) {
                                // toastr.success('Tạo thành công !');
                                window.location = resp.link
                            }
                        },
                        error: function () {
                            alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');
                        }
                    });
                }

                modal.find('.close').click();
                return false;
            }
        });
    });
</script>


