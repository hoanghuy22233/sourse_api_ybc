{{--@if(!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'money_history_view'))--}}
<?php
$money_historys = Modules\EworkingProject\Models\MoneyHistory::where('project_id', $result->id)->get();
$subPartners = \Modules\EworkingProject\Models\SubPartner::pluck('name', 'id')->toArray();
?>
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Lịch sử tiền <a style="position: absolute;right: 10px;top: 10px;"
                                class="btn btn-icon btn btn-label btn-label-brand btn-bold btn-add-money-history"
                                title="Thêm lịch sử tiền"><i
                            class="flaticon2-add-1"></i></a>
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body" style="    padding-top: 10px;">
            <div class="kt-section kt-section--first">
                <div class="form-group-div form-group " id="form-group-progess">
                    <label for="progess" class="col-sm-6 control-label">
                        Số tiền đã nhận/ tổng tiền dự án</label>
                    <?php
                    $value = \Modules\EworkingProject\Models\MoneyHistory::where('project_id', $result->id)->where('type', 0)->sum('value');
                    if ($value <= 0) {
                        $chia = 100;
                        $valueNow = @$result->common_total_vi;
                    } elseif ($result->common_total_vi <= 0) {
                        $chia = 0;
                        $valueNow = 0;
                    } else {
                        $valueNow = $value / @$result->common_total_vi;
                        $chia = $valueNow * 100;
                    }
                    ?>
                    <div class="col-sm-12" style="padding-left: 40px;padding-right: 40px;">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: {{$chia}}%"
                                 aria-valuenow="{{$valueNow}}" aria-valuemin="0"
                                 aria-valuemax="{{$result->common_total_vi}}"></div>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        {{--<th>Giá trị--}}
                        {{--</th>--}}
                        <th>Ngày
                        </th>
                        <th>Giá trị
                        </th>
                        <th>Thầu phụ
                        </th>
                        <th>
                        </th>
                    </tr>
                    </thead>
                    <tbody class="rs-money_historys">
                    {{--@else--}}
                    {{----}}
                    @if(count($money_historys) > 0)
                        @foreach($money_historys as $money_history)
                            <tr>
                                <td class="item-date">
                                    {{date('d-m-Y',strtotime($money_history->date))}}
                                </td>
                                <td style="text-align: center;">
                                    {{($money_history->type == 0) ? '+' : '-'}}
                                    {{number_format($money_history->value)}}
                                </td>
                                <td style="text-align: center;">
                                    {{ @$subPartners[$money_history->sub_partner] }}
                                </td>
                                <td>
                                    <a class="edit-money-history btn-sub-partner" title="Sửa lịch sử tiền"
                                       data-id="{{$money_history->id}}"
                                       data-type="{{$money_history->type}}"
                                       data-value="{{$money_history->value}}"
                                       data-intro="{{$money_history->intro}}"
                                       data-sub_partner="{{$money_history->sub_partner}}"
                                       data-date="{{date('d-m-Y',strtotime($money_history->date))}}"
                                       style="padding: 0 10px;">
                                        <i class="flaticon-edit"></i></a>
                                    <a class="delete-warning btn-sub-partner" title="Xóa lịch sử tiền"
                                       style="padding: 0 0px;"
                                       href="{{route('money.del')}}?id={{$money_history->id}}">
                                        <i class="flaticon-delete"></i></a>

                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{{--@endif--}}

