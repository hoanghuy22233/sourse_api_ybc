{{--@if (isset($result))--}}
{{--<script>--}}
{{--$(document).ready(function () {--}}
{{--let rs_job = $('.rs-jobs');--}}
{{--KTApp.block(rs_job, {--}}
{{--overlayColor: "#ff19600",--}}
{{--type: "v2",--}}
{{--state: "success",--}}
{{--size: "lg"--}}
{{--});--}}

{{--//hien popup sua nhiem vu--}}
{{--$.ajax({--}}
{{--url: '/admin/job/get-by-id/' + '{{$result->id}}',--}}
{{--type: 'GET',--}}
{{--success: function (res) {--}}
{{--rs_job.html(res);--}}
{{--}, error: function () {--}}
{{--alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');--}}
{{--}--}}
{{--});--}}
{{--$('body').on('click', '.pagination-jobs ul li', function (event) {--}}
{{--event.preventDefault();--}}
{{--let url = $(this).children('a').attr('href');--}}
{{--if (url != undefined) {--}}
{{--$.ajax({--}}
{{--url: url,--}}
{{--type: 'GET',--}}
{{--beforeSend: function () {--}}
{{--// Handle the beforeSend event--}}
{{--KTApp.block(rs_job, {--}}
{{--overlayColor: "#000",--}}
{{--type: "v1",--}}
{{--state: "success",--}}
{{--size: "lg"--}}
{{--});--}}
{{--},--}}
{{--success: function (res) {--}}
{{--rs_job.html(res);--}}
{{--}, error: function () {--}}
{{--alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại.');--}}
{{--}--}}
{{--});--}}
{{--}--}}
{{--});--}}
{{--});--}}
{{--</script>--}}
<?php
$jobsInProject = \Modules\EworkingJob\Models\Job::where('project_id', $result->id)->whereNotNull('name')->where('name', '!=', '')->get();
$project = \Modules\EworkingProject\Models\Project::find($result->id);
?>
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Các công việc <a style="position: absolute;right: 10px;top: 10px;"
                                 data-project_id=""
                                 class="btn btn-icon btn btn-label btn-label-brand btn-bold add-job"
                                 title="Tạo công việc"><i
                            class="flaticon2-add-1"></i></a>
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body" style="    padding-top: 10px;">
            <div class="kt-section kt-section--first">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Tiêu đề
                        </th>
                        <th>Dự án
                        </th>
                        <th>Người làm
                        </th>
                        <th>Trạng thái
                        </th>
                    </tr>
                    </thead>
                    <tbody class="rs-jobs">
                    {{--@else--}}
                    {{--{{dd($jobsInProject)}}--}}
                    @if(count($jobsInProject) > 0)
                        @foreach($jobsInProject as $job)
                            <tr>
                                <td class="item-name">
                                    <a href="{{URL::to('/admin/job/'.$job->id)}}"
                                       style="    font-size: 14px!important; ">{{$job->name}}</a>
                                    <div class="row-actions" style="font-size: 13px;">
                                        <span class="edit" title="ID của bản ghi">ID:    {{$job->id}} | </span>
                                        <span class="edit"><a href="{{URL::to('/admin/job/'.$job->id)}}"
                                                              title="Sửa bản ghi này">Sửa</a> | </span>
                                        <span class="trash"><a class="delete-warning"
                                                               href="{{URL::to('/admin/job/delete/'.$job->id)}}"
                                                               title="Xóa bản ghi">Xóa</a> | </span>

                                    </div>
                                </td>
                                <td>{{$project->name}}</td>
                                <td class="item-employees_id">
                                    @include(config('core.admin_theme').'.list.td.admins', ['admin_ids' =>\Modules\EworkingJob\Http\Helpers\EworkingJobHelper::getAdminInJob( $job->id) ])
                                </td>
                                <td>
                                    @include(config('core.admin_theme').'.list.td.status', ['field' =>$field = ['name'=>'status','url'=>'admin/job'],'item'=>$job ])
                                </td>
                            </tr>
                            {{----}}
                        @endforeach
                        {{--<tr>--}}
                        {{--<td colspan="5">--}}
                        {{--{{$jobsInProject->render(config('core.admin_theme').'.partials.paginate',['class'=>'jobs'])}}--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                    @else
                        <tr>
                            <td colspan="4">Không tồn công việc!</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
{{--@endif--}}
{{--@if (isset($result))--}}

{{--@endif--}}
