<?php

/**
 * Widget Controller
 *
 * Widget Controller manages Widget by admin.
 *
 * @category   Widget
 * @package    hobasoft
 * @author     hobasoft.com
 * @copyright  2018 hobasoft.com
 * @license
 * @version    1.3
 * @link       http://hobasoft.com
 * @email      webhobasoft@gmail.com
 * @since      Version 1.0
 * @deprecated None
 */

namespace Modules\EworkingProject\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Http\Helpers\CommonHelper;
use \Modules\EworkingProject\Models\SubPartner;
use Auth;
use Illuminate\Http\Request;

class SubPartnerController extends CURDBaseController
{
    protected $_base;

    public function __construct()
    {
        parent::__construct();
        $this->_base = new BaseController();
    }

    protected $module = [
        'code' => 'job',
        'label' => 'Thầu phụ',
        'icon' => '<i class="kt-font-brand flaticon2-avatar"></i>',
        'modal' => '\Modules\EworkingJob\Models\Job',
    ];

    public function ajaxUpdate(Request $request)
    {
        $SubPartner = SubPartner::updateOrCreate(
            [
                'id' => $request->id,
                'project_id' => $request->project_id
            ],
            [
                'name' => $request->name,
                'price' => $request->price
            ]
        );
        return response()->json([
            'status' => true,
            'data' => $SubPartner
        ]);
    }

    public function del(Request $request)
    {
        SubPartner::destroy($request->id);
        CommonHelper::one_time_message('success', 'Xóa thầu thành công!');
        return back();
    }

    public function getByIdProject($projet_id)
    {
        $data['subpartners'] = \Modules\EworkingProject\Models\SubPartner::where('project_id', $projet_id)->paginate(5);
        return view('eworkingproject::form.fields.thau_phu')->with($data);
    }
}
