<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */

namespace Modules\EworkingProject\Models;

use Illuminate\Database\Eloquent\Model;

class SubPartner extends Model
{

    protected $table = 'project_sub_partners';

    protected $guarded = [];

    public $timestamps = false;
}
