<?php

namespace Modules\LogisticsBill\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\LogisticsBill\Models\Bill;
use Modules\ThemeLogistics\Models\District;
use Modules\ThemeLogistics\Models\Province;
use Modules\ThemeLogistics\Models\Ward;
use Validator;

class PayLocationController extends CURDBaseController
{

    protected $orderByRaw = 'id DESC';

    protected $module = [
        'code' => 'pay_location',
        'table_name' => 'pay_locations',
        'label' => 'Điểm trả hàng',
        'modal' => '\Modules\LogisticsBill\Models\PayLocation',
        'list' => [
            ['name' => 'id', 'type' => 'text', 'label' => 'ID'],
            ['name' => 'name_reminiscent', 'type' => 'text_edit', 'label' => 'Tên gợi nhớ'],
            ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ'],
            ['name' => 'province_id', 'type' => 'relation', 'label' => 'Tỉnh', 'object' => 'province', 'display_field' => 'name'],
            ['name' => 'district_id', 'type' => 'relation', 'label' => 'Huyện', 'object' => 'district', 'display_field' => 'name'],
            ['name' => 'ward_id', 'type' => 'relation', 'label' => 'Xẫ', 'object' => 'ward', 'display_field' => 'name'],
            ['name' => 'contact_name', 'type' => 'text', 'label' => 'Tên liên lạc'],
            ['name' => 'contact_phone', 'type' => 'text', 'label' => 'SĐT liên lạc'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name_reminiscent', 'type' => 'text', 'class' => '', 'label' => 'Tên gợi nhớ'],
                ['name' => 'status', 'type' => 'select', 'options' =>
                    [
                        0 => 'Ngưng hoạt động',
                        1 => 'Đang hoạt động',
                    ], 'class' => '', 'label' => 'Trạng thái', 'value' => 0, 'group_class' => ''],
            ],
            'info_tab' => [
                ['name' => 'contact_name', 'type' => 'text', 'label' => 'Tên người nhận'],
                ['name' => 'contact_phone', 'type' => 'number', 'label' => 'SĐT liên lạc'],
                ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ'],
                ['name' => 'province_id', 'type' => 'select2_ajax_model', 'label' => 'Tỉnh', 'model' => Province::class, 'object' => 'province', 'display_field' => 'name', 'group_class' => 'col-md-4'],
                ['name' => 'district_id', 'type' => 'select2_ajax_model', 'label' => 'Huyện', 'model' => District::class, 'object' => 'district', 'display_field' => 'name', 'group_class' => 'col-md-4'],
                ['name' => 'ward_id', 'type' => 'select2_ajax_model', 'label' => 'Xã', 'model' => Ward::class, 'object' => 'ward', 'display_field' => 'name', 'group_class' => 'col-md-4'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, tên gợi nhớ, địa chỉ, tên liên lạc, sđt liên lạc',
        'fields' => 'id, name_reminiscent, address, contact_name, contact_phone',
    ];

    protected $filter = [
        'name_reminiscent' => [
            'label' => 'Tên gợi nhớ',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'contact_name' => [
            'label' => 'Tên liên lạc',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'contact_phone' => [
            'label' => 'Sđt liên lạc',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'address' => [
            'label' => 'Địa chì',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'province_id' => [
            'label' => 'Tỉnh',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'province',
            'model' => Province::class,
            'query_type' => '='
        ],
        'district_id' => [
            'label' => 'Huyện',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'district',
            'model' => District::class,
            'query_type' => '='
        ],
        'ward_id' => [
            'label' => 'Xã',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'ward',
            'model' => Ward::class,
            'query_type' => 'custom'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('logisticsbill::pay_location.list')->with($data);
    }



//    public function appendWhere($query, $request)
//    {
//        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
//        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
//            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
//        }
//
//        return $query;
//    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('logisticsbill::pay_location.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name_reminiscent' => 'required'
                ], [
                    'name_reminiscent.required' => 'Bắt buộc phải nhập mã',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;
//                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);


            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('logisticsbill::pay_location.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'bill_lading_code' => 'required'
                ], [
                    'bill_lading_code.required' => 'Bắt buộc phải nhập mã',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;


                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

//            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
