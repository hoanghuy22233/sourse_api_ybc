@extends(config('core.admin_theme').'.template')
@section('main')
    <style>
        .small-box {
            border-radius: 2px;
            position: relative;
            display: block;
            margin-bottom: 20px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1)
        }

        .small-box > .inner {
            padding: 10px
        }

        .small-box > .small-box-footer {
            position: relative;
            text-align: center;
            padding: 3px 0;
            color: #fff;
            color: rgba(255, 255, 255, 0.8);
            display: block;
            z-index: 10;
            background: rgba(0, 0, 0, 0.1);
            text-decoration: none
        }

        .small-box > .small-box-footer:hover {
            color: #fff;
            background: rgba(0, 0, 0, 0.15)
        }

        .small-box h3 {
            font-size: 38px;
            font-weight: bold;
            margin: 0 0 10px 0;
            white-space: nowrap;
            padding: 0
        }

        .small-box p {
            font-size: 15px
        }

        .small-box p > small {
            display: block;
            color: #f9f9f9;
            font-size: 13px;
            margin-top: 5px
        }

        .small-box h3,
        .small-box p {
            z-index: 5px
        }

        .small-box .icon {
            -webkit-transition: all .3s linear;
            -o-transition: all .3s linear;
            transition: all .3s linear;
            position: absolute;
            top: -10px;
            right: 10px;
            z-index: 0;
            font-size: 90px;
            color: rgba(0, 0, 0, 0.15)
        }

        .small-box:hover {
            text-decoration: none;
            color: #f9f9f9
        }

        .small-box:hover .icon {
            font-size: 95px
        }

        @media (max-width: 767px) {
            .small-box {
                text-align: center
            }

            .small-box .icon {
                display: none
            }

            .small-box p {
                font-size: 12px
            }
        }

        .bg-yellow,
        .bg-aqua,
        .bg-teal,
        .bg-purple,
        .bg-maroon {
            color: #fff !important
        }

        .bg-yellow {
            background-color: #f39c12 !important
        }

        .bg-aqua {
            background-color: #00c0ef !important
        }

        .bg-teal {
            background-color: #39cccc !important
        }

        .bg-purple {
            background-color: #605ca8 !important
        }

        .bg-maroon {
            background-color: #d81b60 !important
        }
    </style>
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="col-md-12 order-lg-2 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title bold uppercase">
                            Tra cước & tạo đơn
                        </h3>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">

            <div class="col-md-4 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Thông tin người gửi
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            <form class="form">
                                <div class="card-body">


                                    <div class="form-group">
                                        <select class="form-control" id="exampleSelectd">
                                            <option>Hào</option>
                                            <option>Anh</option>
                                            <option>Quang</option>
                                            <option>Đồng</option>
                                            <option>Quảng</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Nhập tên gợi nhớ">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Người gửi">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Số điện thoại">
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" id="exampleSelectd">
                                            <option>Thành phố</option>
                                            <option>Anh</option>
                                            <option>Quang</option>
                                            <option>Đồng</option>
                                            <option>Quảng</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <select class="form-control" id="exampleSelectd">
                                            <option>Quận huyện</option>
                                            <option>Anh</option>
                                            <option>Quang</option>
                                            <option>Đồng</option>
                                            <option>Quảng</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control form-control-solid" rows="3"></textarea>
                                    </div>
                                    <!--begin: Code-->
                                    <!--end: Code-->
                                </div>
                                <div class="card-footer">
                                    <a href="/admin/pick_location/add" class="btn btn-success mr-2">Tạo điểm gửi</a>
                                </div>
                            </form>
                        </div>
                        <!--end: Datatable -->
                    </div>

                </div>
            </div>

            <div class="col-md-4 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Thông tin người nhận
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">

                            <form class="form">
                                <div class="card-body">


                                    <div class="form-group">
                                        <select class="form-control" id="exampleSelectd">
                                            <option>Hào</option>
                                            <option>Anh</option>
                                            <option>Quang</option>
                                            <option>Đồng</option>
                                            <option>Quảng</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Nhập tên gợi nhớ">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Người gửi">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Số điện thoại">
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" id="exampleSelectd">
                                            <option>Thành phố</option>
                                            <option>Anh</option>
                                            <option>Quang</option>
                                            <option>Đồng</option>
                                            <option>Quảng</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <select class="form-control" id="exampleSelectd">
                                            <option>Quận huyện</option>
                                            <option>Anh</option>
                                            <option>Quang</option>
                                            <option>Đồng</option>
                                            <option>Quảng</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control form-control-solid" rows="3"></textarea>
                                    </div>
                                    <!--begin: Code-->
                                    <!--end: Code-->
                                </div>
                                <div class="card-footer">
                                    <a href="/admin/pay_location/add" class="btn btn-success mr-2">Tạo điểm gửi</a>
                                </div>
                            </form>
                        </div>
                        <!--end: Datatable -->
                    </div>

                </div>
            </div>


            <div class="col-md-4 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Thông tin hàng hóa/ dịch vụ
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            <form class="form">
                                <div class="card-body row">

                                    <div class="form-group col-md-12">
                                        <input type="text" class="form-control" placeholder="Mã tham chiếu">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <select class="form-control" id="exampleSelectd">
                                            <option>Hào</option>
                                            <option>Anh</option>
                                            <option>Quang</option>
                                            <option>Đồng</option>
                                            <option>Quảng</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <select class="form-control" id="exampleSelectd">
                                            <option>Loại hàng</option>
                                            <option>Anh</option>
                                            <option>Quang</option>
                                            <option>Đồng</option>
                                            <option>Quảng</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" class="form-control" placeholder="Số kiện">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" class="form-control" placeholder="Khối lượng (kg)">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <input type="text" class="form-control" placeholder="Dài (cm)">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <input type="text" class="form-control" placeholder="Rộng (cm)">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <input type="text" class="form-control" placeholder="Cao (cm)">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" class="form-control" placeholder="Giá trị hàng hóa">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <input type="text" class="form-control" placeholder="Số tiền thu hộ">
                                    </div>
                                    <div class="checkbox-list col-md-12">
                                        <label class="checkbox">
                                            <input type="checkbox" name="Checkboxes1">
                                            <span></span>Chuyển hoàn chứng từ</label>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <select class="form-control" id="exampleSelectd">
                                            <option>Thời gian lấy</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <select class="form-control" id="exampleSelectd">
                                            <option>Giờ</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <select class="form-control" id="exampleSelectd">
                                            <option>Phút</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="exampleTextarea">Nội dung hàng hóa</label>
                                        <textarea class="form-control form-control-solid" rows="3"></textarea>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="exampleTextarea">Ghi chú</label>
                                        <textarea class="form-control form-control-solid" rows="3"></textarea>
                                    </div>
                                    <!--begin: Code-->
                                    <!--end: Code-->
                                </div>
                                <div class="card-footer">
                                    <a href="/admin/service_price" class="btn btn-success mr-2">Tính giá dich vụ</a>
                                </div>
                            </form>
                        </div>
                        <!--end: Datatable -->
                    </div>

                </div>
            </div>




            {{-- Danh sách các Product mới đăng --}}

        </div>
    </div>

@endsection
@section('custom_head')
    <style type="text/css">
        .kt-datatable__cell > span > a.cate {
            color: #5867dd;
            margin-bottom: 3px;
            background: rgba(88, 103, 221, 0.1);
            height: auto;
            display: inline-block;
            width: auto;
            padding: 0.15rem 0.75rem;
            border-radius: 2px;
        }

        .paginate > ul.pagination > li {
            padding: 5px 10px;
            border: 1px solid #ccc;
            margin: 0 5px;
            cursor: pointer;
        }

        .paginate > ul.pagination span {
            color: #000;
        }

        .paginate > ul.pagination > li.active {
            background: #0b57d5;
            color: #fff !important;
        }

        .paginate > ul.pagination > li.active span {
            color: #fff !important;
        }

        .kt-widget12__desc, .kt-widget12__value {
            text-align: center;
        }

        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99 list_user
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <style type="text/css">
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>

@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $('#active_service a').click(function (event) {
                event.preventDefault();
                var object = $(this);
                $.ajax({
                    url: '/admin/service_history/ajax-publish',
                    data: {
                        id: object.data('service_history_id')
                    },
                    success: function (result) {
                        if (result.status == true) {
                            toastr.success(result.msg);
                            object.parents('tr').remove();
                        } else {
                            toastr.error(result.msg);
                        }
                    },
                    error: function (e) {
                        console.log(e.message);
                    }
                })
            })
        })
    </script>
@endpush

