@php  $totalPrice = 0; @endphp
<ul style="padding: 0; margin: 0;">
    @foreach(\Modules\ThemeLogistics\Models\Product::whereIn('id', explode('|', $item->gift_list))->pluck('name') as $product_name)
        <li>
            {{ $product_name }}
        </li>
    @endforeach
</ul>
@foreach($item->orders as $order)
    {!! @$order->gift_text !!}
@endforeach