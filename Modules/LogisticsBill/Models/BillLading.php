<?php
namespace Modules\LogisticsBill\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\ThemeLogistics\Models\Ward;
use Modules\ThemeLogistics\Models\Province;
use Modules\ThemeLogistics\Models\District;


class BillLading extends Model
{

    protected $table = 'bill_ladings';
    public $timestamps = false;

    protected $fillable = [
        'bill_lading_code' , 'place_submission', 'recipient_name' , 'address' , 'province_id' , 'district_id' , 'ward_id' , 'date_creation', 'collect_household'
    ];

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }
    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }

}
