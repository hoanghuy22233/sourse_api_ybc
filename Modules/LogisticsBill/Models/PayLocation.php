<?php
namespace Modules\LogisticsBill\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\ThemeLogistics\Models\Ward;
use Modules\ThemeLogistics\Models\Province;
use Modules\ThemeLogistics\Models\District;


class PayLocation extends Model
{

    protected $table = 'pay_locations';
    public $timestamps = false;

    protected $fillable = [
        'name_reminiscent' , 'address' , 'province_id' , 'district_id' , 'ward_id' , 'contact_name', 'contact_phone', 'status'
    ];

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }
    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }

}
