<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/plugin', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {

    //  Admin
    Route::group(['prefix' => 'admin', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\AdminController@index')->middleware('api_permission:admin_view');
        Route::post('', 'Admin\AdminController@store')->middleware('api_permission:admin_add');
        Route::get('{id}', 'Admin\AdminController@show')->middleware('api_permission:admin_view');
        Route::post('{id}', 'Admin\AdminController@update')->middleware('api_permission:admin_edit');
        Route::delete('{id}', 'Admin\AdminController@delete')->middleware('api_permission:admin_delete');
    });
});

