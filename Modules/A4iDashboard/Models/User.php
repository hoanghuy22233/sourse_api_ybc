<?php

/**
 * BillPayment Model
 *
 * BillPayment Model manages BillPayment operation. 
 *
 * @category   BillPayment
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license    
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\A4iDashboard\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $table = 'users';

    protected $dates = ['deleted_at'];
    protected $softDelete = true;

    protected $fillable = [
        'name', 'email', 'password', 'phone', 'image', 'address', 'image', 'gender', 'birthday','balance','stk','change_password','api_token'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    public function product(){
        return $this->hasMany(Product::class,'user_id', 'id');
    }


}
