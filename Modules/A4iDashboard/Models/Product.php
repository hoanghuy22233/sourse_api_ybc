<?php
/**
 * Created by PhpStorm.
 * BillPayment: hoanghung
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace Modules\A4iDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = 'products';

    protected $appends = ['category', 'categories'];

    protected $fillable = [
        'image_extra', 'study_time', 'id', 'time', 'number_of_lessons', 'lecturer_id', 'intro_en', 'classroom_address_en', 'classroom_address_vi', 'info_en', 'name_vi', 'slug', 'category_id', 'course_prices', 'intro_vi', 'content_vi', 'content_en', 'image', 'status', 'seo_title', 'seo_description', 'seo_keywords', 'user_id', 'created_at', 'deleted_at', 'name_en', 'updated_at', 'multi_cat', 'order_no', 'day'

    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getCategoriesAttribute()
    {
        $cat_ids = explode('|', @$this->attributes['multi_cat']);
        return Category::whereIn('id', $cat_ids)->get();
    }

    public function getCategoryAttribute()
    {
        $cat_ids = explode('|', @$this->attributes['multi_cat']);
        return Category::whereIn('id', $cat_ids)->first();
    }

}