<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'Admin\DashboardController@getIndex')->name('dashboard');
       });
});
