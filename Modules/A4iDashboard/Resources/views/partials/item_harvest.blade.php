<div class="col-md-3 col-lg-3 bg_prd">
    <div class="content_prd">
        <a class="image_prd" href="/admin/season/{{ @$item->season->id }}">
            <img src="/public/filemanager/userfiles/{{$item->image}}"
                 class=" file_image_thumb"
                 alt="{{ @$item->season->tree_name }} của {{ @$item->admin->name }}" title="{{ @$item->season->tree_name }} của {{ @$item->admin->name }}">
        </a>
        <div class="title_prd">
            <a href="/admin/season/{{ @$item->season->id }}">
                <h3>{{ @$item->season->tree_name }}</h3>
            </a>
        </div>

        <div class="people_prd">
            <a href="/admin/profile/{{ @$item->admin_id }}">
                <img src="/public/filemanager/userfiles/{{@$item->admin->image}}"
                     style="    border-radius: 50%;    width: 30px; max-width: 30px; overflow: hidden; height: 30px;    max-height: 30px;"
                     alt="{{@$item->admin->name}}" title="{{@$item->admin->name}}">
                <span>{{@$item->admin->name}}</span>
            </a>
        </div>

        <div class="thu_hoach_prd">
            <span><i class="flaticon-calendar-3" style="margin: 10px;"></i>{{ date_format(date_create($item->time),'d-m-Y') }}</span>
        </div>

        <div class="address_prd">
                                    <span><i class="flaticon2-location" style="margin: 10px;"></i>
                                        @if(isset($item->season->land->ward->name))<a
                                                href="/admin/season?search=true&view=all&ward_id={{ @$item->season->land->ward->id }}">{{ @$item->season->land->ward->name }}</a>, @endif
                                        @if(isset($item->season->land->district->name))<a
                                                href="/admin/season?search=true&view=all&district_id={{ @$item->season->land->district->id }}">{{ @$item->season->land->district->name }}</a>, @endif
                                        @if(isset($item->season->land->province->name))<a
                                                href="/admin/season?search=true&view=all&province_id={{ @$item->season->land->province->id }}">{{ @$item->season->land->province->name }}</a>@endif
                                    </span>
        </div>

        <div class="price_prd">
            <span>{{ number_format($item->price,0,'',',') }}đ{{ $item->price_unit }}</span>
        </div>
        <div class="san_luong_prd">
            <span>Sản lượng: {{ $item->quantity }}</span>
        </div>
    </div>
</div>