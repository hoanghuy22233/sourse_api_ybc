<div class="col-md-3 col-lg-3 bg_prd">
    <div class="content_prd">
        <a class="image_prd" href="/admin/season/{{ @$item->id }}">
            <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, null, null) }}"
                 class=" file_image_thumb"
                 alt="{{ @$item->tree_name }} của {{ @$item->admin->name }}" title="{{ @$item->tree_name }} của {{ @$item->admin->name }}">
        </a>
        <div class="title_prd">
            <a href="/admin/season/{{ @$item->id }}">
                <h3>{{ @$item->tree_name }}</h3>
            </a>
        </div>

        <div class="people_prd">
            <a href="/admin/profile/{{ @$item->admin_id }}">
                <img src="/public/filemanager/userfiles/{{@$item->admin->image}}"
                     style="    border-radius: 50%;    width: 30px; max-width: 30px; overflow: hidden; height: 30px;    max-height: 30px;"
                     alt="{{@$item->admin->name}}" title="{{@$item->admin->name}}">
                <span>{{@$item->admin->name}}</span>
            </a>
        </div>

        <div class="thu_hoach_prd">
            <span><i class="flaticon-calendar-3" style="margin: 10px;"></i>{{ date_format(date_create($item->time),'d-m-Y') }}</span>
        </div>

        <div class="address_prd">
                                    <span><i class="flaticon2-location" style="margin: 10px;"></i>
                                        @if(isset($item->land->ward->name))<a
                                                href="/admin/season?search=true&view=all&ward_id={{ @$item->land->ward->id }}">{{ @$item->land->ward->name }}</a>, @endif
                                        @if(isset($item->land->district->name))<a
                                                href="/admin/season?search=true&view=all&district_id={{ @$item->land->district->id }}">{{ @$item->land->district->name }}</a>, @endif
                                        @if(isset($item->land->province->name))<a
                                                href="/admin/season?search=true&view=all&province_id={{ @$item->land->province->id }}">{{ @$item->land->province->name }}</a>@endif
                                    </span>
        </div>

        <div class="price_prd">
            <span></span>
        </div>
        <div class="san_luong_prd">
            <span>Sản lượng: {{ $item->quantity_expected }}</span>
        </div>
    </div>
</div>