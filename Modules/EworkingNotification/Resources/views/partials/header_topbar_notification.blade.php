<?php
//$toDayDeadlines = \Modules\EworkingJob\Models\Task::where('admin_ids', 'like', '%|' . Auth::guard('admin')->user()->id . '|%')->whereDate('end_date', date('Y-m-d'))->orderBy('end_date', 'desc')->get();
$company_ids_notification = \Auth::guard('admin')->user()->company_ids;
$company_ids_notification = trim($company_ids_notification, '|');
$company_ids_notification = explode('|', $company_ids_notification);
$query_notifications = \Modules\EworkingNotification\Models\Notifications::whereIn('company_id', $company_ids_notification)
    ->where('to_admin_id', \Auth::guard('admin')->user()->id)->orderBy('id', 'desc');
$notifications = $query_notifications->get();
$count_notifications = $query_notifications->where('readed', 0)->count();
?>
<style>
    @-webkit-keyframes my {
        0% {
            color: #dc3545;
        }
        50% {
            color: #fff;
        }
        100% {
            color: #dc3545;
        }
    }

    @-moz-keyframes my {
        0% {
            color: #dc3545;
        }
        50% {
            color: #fff;
        }
        100% {
            color: #dc3545;
        }
    }

    @-o-keyframes my {
        0% {
            color: #dc3545;
        }
        50% {
            color: #fff;
        }
        100% {
            color: #dc3545;
        }
    }

    @keyframes my {
        0% {
            color: #dc3545;
        }
        50% {
            color: #fff;
        }
        100% {
            color: #dc3545;
        }
    }

    .icon-bell {
        /*background:#3d3d3d;*/
        font-size: 24px !important;
        font-weight: bold;
        -webkit-animation: my 700ms infinite;
        -moz-animation: my 700ms infinite;
        -o-animation: my 700ms infinite;
        animation: my 700ms infinite;
    }

    .icon-bell:hover {
        -webkit-animation: my none;
        -moz-animation: my none;
        -o-animation: my none;
        animation: my none;
    }
</style>
<div class="kt-header__topbar-item dropdown">
    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px"
         aria-expanded="true">
        <span class="kt-header__topbar-icon"><i
                    class="@if($count_notifications > 0) flaticon-bell kt-font-info icon-bell @else flaticon2-bell @endif"></i></span>
        <span class="kt-hidden kt-badge kt-badge--dot kt-badge--notify kt-badge--sm"></span>
    </div>
    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-lg">
        <form>
            <!--begin: Head -->
            <div class="kt-head kt-head--skin-light kt-head--fit-x kt-head--fit-b">
                <h3 class="kt-head__title">
                    Thông báo
                    <span class="btn btn-label-primary btn-sm btn-bold btn-font-md">{{$count_notifications}} tin mới</span>
                </h3>
                <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand  kt-notification-item-padding-x"
                    role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active show" data-toggle="tab"
                           href="#topbar_notifications_notifications" role="tab"
                           aria-selected="true">Thông báo</a>
                    </li>
                </ul>
            </div>
            <!--end: Head -->

            <div class="tab-content">
                <div class="tab-pane active show" id="topbar_notifications_notifications"
                     role="tabpanel">
                    <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll"
                         data-scroll="true" data-height="300" data-mobile-height="200">
                        @if(count( $notifications) > 0)
                            @foreach($notifications as $notification)
                                <?php
                                $link = (!in_array($notification->type, [4, 5])) ? route('job.edit', ['id' => @$notification->job_id]) : '/admin/dashboard';
                                ?>
                                <a href="{{$link}}"
                                   target="_blank"
                                   data-id="{{$notification->id}}"
                                   class="kt-notification__item {{$notification->readed == 1 ? 'kt-notification__item--read': 'kt-notification__item--unread'}}">
                                    <div class="kt-notification__item-icon">
                                        {{--<i class="flaticon2-line-chart kt-font-success"></i>--}}
                                        <div class="kt-media kt-media--sm kt-media--circle">
                                            <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$notification->from_admin->image, false,null) }}"
                                                 alt="{{@$notification->from_admin->name}}">
                                        </div>
                                    </div>


                                    <div class="kt-notification__item-details" style="padding-left: 10px">
                                        <div class="kt-notification__item-title" style="color: #000000">
                                          {{$notification->content}}
                                        </div>
                                        <div class="kt-notification__item-time">
                                            {{\App\Http\Helpers\CommonHelper::formatTimePast($notification->created_at)}}
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>

    $('body').on('click', '.kt-notification__item--unread', function (event) {
        // event.preventDefault();
        // alert($(this).data('id'))
        let notification_item = $(this);
        $.ajax({
            url: "{{route('read')}}?id=" + notification_item.data('id'),
            type: 'GET',
            data: {},
            success: function (res) {
                if (res.status) {
                    notification_item.removeClass('kt-notification__item--unread').addClass('kt-notification__item--read')
                }
            }
        });
    })
</script>
