@extends(config('core.admin_theme').'.template')
@section('main')
    <a id="my-notification-button" style="display: none">Subscribe to Notifications</a>
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-xs-12 col-lg-12 col-xl-12 col-lg-12 order-lg-1 order-xl-1">
                <!--begin:: Widgets/Finance Summary-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Tổng quan
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget12">
                            <div class="kt-widget12__content">
                                <div class="kt-widget12__item">
                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng công ty</span>
                                        <span class="kt-widget12__value">{{number_format($company_all, 0, '.', '.')}}</span>
                                    </div>

                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Tài khoản đang sử dụng</span>
                                        <span class="kt-widget12__value">{{number_format($admin_all, 0, '.', '.')}}</span>
                                    </div>

                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng số gói dịch vụ</span>
                                        <span class="kt-widget12__value">{{number_format($service_all, 0, '.', '.')}}</span>
                                    </div>

                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng lượt đăng ký dịch vụ</span>
                                        <span class="kt-widget12__value">{{number_format($service_history_all, 0, '.', '.')}}</span>
                                    </div>

                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng tiền dịch vụ</span>
                                        <span class="kt-widget12__value">{{number_format($money_service_all, 0, '.', '.')}}<sup>đ</sup></span>
                                    </div>
                                    <div class="kt-widget12__info">
                                        <span class="kt-widget12__desc">Tổng kho bộ môn</span>
                                        <span class="kt-widget12__value">{{number_format($subject_null_all, 0, '.', '.')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Finance Summary-->
            </div>


            {{-- Các đăng ký dịch vụ chờ duyệt (bảng) --}}
            <div class="col-xl-12 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Đơn chờ duyệt
                            </h3>
                        </div>
                    </div>


                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            <table class="kt-datatable__table" style="display: block; ">
                                <thead class="kt-datatable__head">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Ngày đăng ký</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 252px;">Công ty</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 200px;">Tên gói</span></th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 80px;">Số tài khoản</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Số ngày sử dụng</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Giá</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Trạng thái</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="max-height: 500px; overflow: auto!important;">
                                @if($service_history_pending->count()>0)
                                    @foreach($service_history_pending as $v)
                                        <tr data-row="0" class="kt-datatable__row" style="left: 0px;">

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 100px;">
                                                        <span class="kt-font-bold">{{date('d/m/Y',strtotime(@$v->created_at))}}</span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 252px;">
                                                        <span class="kt-font-bold">{{@\Modules\EworkingCompany\Models\Company::find($v->company_id)->short_name}}</span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 200px;">
                                                        <span class="kt-font-bold">{{@\Modules\EworkingService\Models\Service::find($v->service_id)->name_vi}}</span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 80px;">
                                                        <span class="kt-font-bold">{{number_format(@\Modules\EworkingService\Models\Service::find($v->service_id)->account_max, 0, '.', '.')}}</span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 100px;">
                                                        <span class="kt-font-bold">{{number_format($v->use_date_max, 0, '.', '.')}}</span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 100px;">
                                                        <span class="kt-font-bold">{{number_format(@$v->payment, 0, '.', '.')}}<sup> đ</sup></span>
                                                    </span>
                                            </td>

                                            <td data-field="ShipDate" class="kt-datatable__cell">
                                                    <span style="width: 100px;">
                                                        <a href="{{URL::to('/admin/service_history/publish?id='.@$v->id)}}"
                                                           class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill confirm-action"
                                                           style="cursor:pointer;border-radius: 2rem!important;background: #fd397a;">Chờ kích hoạt</a>
                                                    </span>
                                            </td>

                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom_head')
    {{--    <link href="https://www.keenthemes.com/preview/metronic/theme/assets/global/css/components.min.css" rel="stylesheet"--}}
    {{--          type="text/css">--}}
    <style type="text/css">
        .kt-widget12__desc, .kt-widget12__value {
            text-align: center;
        }

        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <style type="text/css">
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>

@endsection
{{--@push('scripts')--}}
{{--<script>--}}
{{--$(document).ready(function () {--}}
{{--$('#active_service a').click(function (event) {--}}
{{--$('#confirm-action-yes').attr('href', '/admin/service_history/ajax-publish' + $(this).data('service_history_id'));--}}
{{--$('#confirm-action-modal').modal('show');--}}
{{--});--}}
{{--$('#confirm-action-yes').click(function (event) {--}}
{{--event.preventDefault();--}}
{{--var object = $(this);--}}
{{--$.ajax({--}}
{{--url: object.attr('url'),--}}
{{--data: {},--}}
{{--success: function (result) {--}}
{{--if (result.status == true) {--}}
{{--toastr.success(result.msg);--}}
{{--object.parents('tr').remove();--}}
{{--} else {--}}
{{--toastr.error(result.msg);--}}
{{--}--}}
{{--},--}}
{{--error: function (e) {--}}
{{--console.log(e.message);--}}
{{--}--}}
{{--})--}}
{{--})--}}
{{--})--}}
{{--</script>--}}
{{--@endpush--}}

