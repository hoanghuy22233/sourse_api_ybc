<div class="row">
    <div class="col-xl-12 order-lg-2 order-xl-1">
        <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <a class="btn btn-sm btn-label-brand btn-bold click-handle-change" title="Xem deadline tổng quan"><i
                                class="flaticon-refresh"></i></a>
                    <h3 class="kt-portlet__head-title bold uppercase">
                        Nhắc nhở deadline <span class="title-deadline">cá nhân</span>
                    </h3>

                </div>

            </div>
            <div id="ca-nhan" class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                     id="kt_datatable_latest_orders" style="">
                    <table class="kt-datatable__table" style="display: block; max-height: 500px;">
                        <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">

                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span
                                        style="width: 200px;">Dự án</span>
                            </th>
                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span
                                        style="width: 252px;">Công việc</span>
                            </th>
                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 100px;">Người làm</span>
                            </th>
                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 100px;">Ngày bắt đầu</span>
                            </th>
                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 100px;">Ngày hết hạn</span>
                            </th>
                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span
                                        style="width: 150px;">Công ty</span>
                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span
                                        style="width: 50px;">Chị tiết</span>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="kt-datatable__body ps ps--active-y"
                               style="max-height: 450px; overflow: auto!important;">
                        @if(isset($arrbeforeDeadlineUser) && count($arrbeforeDeadlineUser) > 0 ||isset($arrAfterDeadlinesUser) && count($arrAfterDeadlinesUser) > 0)
                            @include('eworkingdashboard::deadline',['afterDeadlines'=>$arrbeforeDeadlineUser,'beforeDeadlines'=>$arrAfterDeadlinesUser])
                        @else
                            <tr data-row="0" class="kt-datatable__row" style="left: 0px;"
                                style="border: 2px solid #ccc;">
                                <td class="kt-datatable__cell">
                                                <span style="width: 100%; text-align: center">
                                                    <span class="kt-font-bold">Bạn chưa có công việc nào sắp đến hạn, đã quá hạn deadline!</span>
                                                </span>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                </div>
                <!--end: Datatable -->
            </div>
            <div id="chung" class="hidden kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                     id="kt_datatable_latest_orders" style="">
                    <table class="kt-datatable__table" style="display: block; max-height: 500px;">
                        <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">

                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span
                                        style="width: 200px;">Dự án</span>
                            </th>
                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 252px;">Công việc</span>
                            </th>
                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 100px;">Người làm</span>
                            </th>
                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 100px;">Ngày bắt đầu</span>
                            </th>
                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span style="width: 100px;">Ngày hết hạn</span>
                            </th>
                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span
                                        style="width: 150px;">Công ty</span>
                            <th class="kt-datatable__cell kt-datatable__cell--sort"><span
                                        style="width: 50px;">Chị tiết</span>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="kt-datatable__body ps ps--active-y"
                               style="max-height: 450px; overflow: auto!important;">
                        @if(isset($afterDeadlines) && count($afterDeadlines) > 0 || isset($beforeDeadlines) && count($beforeDeadlines) > 0)
                            @include('eworkingdashboard::deadline',['afterDeadlines'=>$afterDeadlines,'beforeDeadlines'=>$beforeDeadlines])
                        @else
                            <tr data-row="0" class="kt-datatable__row" style="left: 0px;"
                                style="border: 2px solid #ccc;">
                                <td class="kt-datatable__cell">
                                                <span style="width: 100%; text-align: center">
                                                    <span class="kt-font-bold">Bạn chưa có công việc nào sắp đến hạn, đã quá hạn deadline!</span>
                                                </span>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".click-handle-change").click(function () {
            let text = $('#chung').is(":hidden") ? 'tổng quan' : 'cá nhân';
            let title = !$('#chung').is(":hidden") ? 'tổng quan' : 'cá nhân';
            $('.title-deadline').text(text)
            $(this).attr('title', 'Xem deadline ' + title)
            $('#ca-nhan').toggleClass("hidden");
            $('#chung').toggleClass("hidden");
        });
    });
</script>
