@extends(config('core.admin_theme').'.template')
@section('main')
    {{--{{dd($notification->count())}}--}}
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="row">
            <div class="col-xs-6 col-lg-6 col-xl-6 col-lg-6 order-lg-2 order-xl-1">
                @include('eworkingdashboard::partials.list_companies')
            </div>
            <div class="col-xs-6 col-lg-6 col-xl-6 col-lg-6 order-lg-2 order-xl-1">
                <!--begin:: Widgets/New Users-->
                <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Thông báo
                            </h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body" style="max-height: 318px; overflow: auto;">
                        <div class="tab-content">
                            <div class="tab-pane active" id="kt_widget4_tab1_content">
                                <div class="kt-widget4">
                                    @if($notification->count() > 0)
                                        {{--{{dd($notification)}}--}}
                                        @foreach($notification as $v)
                                            <div class="kt-widget4__item">
                                                <div class="kt-widget4__pic kt-widget4__pic--pic">
                                                    <a href="/admin/profile/{{ @$v->from_admin->id }}">
                                                        <img src="{{ CommonHelper::getUrlImageThumb(@$v->from_admin->image, 50, 50) }}"
                                                             alt="{{@$v->from_admin->name}}"
                                                             title="{{@$v->from_admin->name}}">
                                                    </a>
                                                </div>
                                                <div class="kt-widget4__info">
                                                    <a href="/admin/profile/{{ @$v->from_admin->id }}"
                                                       class="kt-widget4__username">
                                                        {{@$v->from_admin->name}}
                                                    </a>
                                                    <p class="kt-widget4__text">
                                                        {{$v->content}}
                                                    </p>
                                                </div>
                                                <?php
                                                $link = (!in_array($v->type, [4, 5])) ? route('job.edit', ['id' => @$v->job_id]) : '/admin/dashboard';
                                                ?>
                                                <a href="{{$link}}"
                                                   target="_blank"
                                                   data-id="{{$v->id}}"
                                                   class=" btn btn-sm btn-label-brand btn-bold {{$v->readed == 1 ? 'kt-notification__item--read': 'kt-notification__item--unread'}}">Xem</a>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="kt-widget4__item">
                                            <p>Bạn chưa có thông báo nào!!!</p>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/New Users-->
            </div>
        </div>
        @include('eworkingdashboard::promt')
    </div>

@endsection
@section('custom_head')

    <style type="text/css">
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
@endsection
