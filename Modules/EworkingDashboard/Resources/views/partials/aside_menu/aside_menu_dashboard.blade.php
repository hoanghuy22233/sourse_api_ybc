<li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
            href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon2-dashboard"></i>
                    </span><span class="kt-menu__link-text">Thống kê</span><i
                class="kt-menu__ver-arrow la la-angle-right"></i></a>
    <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            @if(in_array('super_admin', $permissions))
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/dashboard/software" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Phần mềm</span></a></li>
            @else
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/dashboard" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Chung</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/dashboard/company" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Theo công ty</span></a></li>
            @endif
        </ul>
    </div>
</li>