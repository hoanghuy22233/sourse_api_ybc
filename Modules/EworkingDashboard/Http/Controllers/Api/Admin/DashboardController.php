<?php

namespace Modules\EworkingDashboard\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\EworkingCompany\Models\Company;
use DB;
use Auth;
use Modules\EworkingJob\Models\Job;
use Modules\EworkingJob\Models\Task;

class DashboardController extends Controller
{

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'short_name' => [
            'query_type' => 'like'
        ],
        'msdn' => [
            'query_type' => 'like'
        ],
        'address' => [
            'query_type' => 'like',
        ],
    ];

    public function Index(Request $request)
    {
        try {
//              Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'admin_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Company::select('id','short_name')
                ->where('companies.admin_id',\Auth::guard('api')->id())
                ->whereRaw($where);

            //  Sort
            $listItem = $this->sort($request, $listItem);
            $listItem = $listItem->paginate(20)->appends($request->all());

            foreach ($listItem as $k => $item) {
                $jobs = Job::select('id','name','end_date')->where('company_id',$item->id)->get();
                $status = false;
                foreach ($jobs as $key=>$v){
                    $job_end_date = @Task::where('job_id',$v->id)->whereNotNull('end_date')->orderBy('end_date','desc')->first()->end_date;
                        $days = (strtotime($job_end_date) - strtotime(date('Y-m-d'))) / (60 * 60 * 24);
                        $bg_deadline='';
                        if ($days < 0) {
                            $bg_deadline = '#fff';
                        } elseif ($days == 0) {
                            $bg_deadline = 'Tomato';
                        } elseif ($days > 0 && $days <= 2) {
                            $bg_deadline = 'LightSalmon';
                        } elseif ($days > 2 && $days <= 7) {
                            $bg_deadline = 'MistyRose';
                        } elseif ($days > 0 && $days > 7) {
                            $bg_deadline = 'SeaShell';
                        }

                        $obj_jobs[] = (object)[
                            'id' => $v->id,
                            'name' => $v->name,
                            'job_color' => $bg_deadline,
                            'end_date' =>  $job_end_date
                        ];
                        $status = true;
                }
                if ($status){
                    $item->jobs = $obj_jobs;
                }
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function getDetail($id) {
        try {
            //  Check permission
            if (!CommonHelper::has_permission(\Auth::guard('api')->id(), 'company_view')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không đủ quyền'
                        ]
                    ],
                    'data' => null,
                    'code' => 403
                ]);
            }

            $item = Company::rightJoin('jobs', 'jobs.company_id', '=', 'companies.id')
                ->selectRaw('jobs.id as jobs_id', 'jobs.name as jobs_name', 'job.end_date as jobs_end_date')
                ->where('jobs.id', $id)->get();
            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $item->user = [
                'id' => $item->user_id,
                'name' => $item->user_name
            ];

            unset($item->user_id);
            unset($item->user_name);

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
