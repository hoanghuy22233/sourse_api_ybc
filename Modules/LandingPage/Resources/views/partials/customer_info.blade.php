<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Thông tin khách hàng
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body">
            <div class="row kt-section kt-section--first">
                <div class="col-md-6 kt-avatar kt-avatar--outline" id="kt_user_avatar_image">
                    <div class="kt-avatar__holder">
                        <img style="max-width:150px;padding-top:5px; cursor: pointer;" id="customer_image"
                             src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$result->customer->image, 100, 100) }}"
                             class="file_image_thumb" title="CLick để phóng to ảnh">
                    </div>
                </div>
                <div class="col-md-6 kt-widget__body">
                    <div class="kt-widget__content">
                        <div class="kt-widget__info">
                            <span class="kt-widget__label"><strong>Họ & tên:</strong></span>
                            <a id="customer_name" href="/admin/admin/{{ @$result->customer->id }}" class="kt-widget__data" target="_blank">{{ @$result->customer->name }}</a>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label"><strong>SĐT:</strong></span>
                            <span class="kt-widget__data" id="customer_tel">{{ @$result->customer->tel }}</span>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label"><strong>Email:</strong></span>
                            <span class="kt-widget__data" id="customer_email">{{ @$result->customer->email }}</span>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label"><strong>Quyền:</strong></span>
                            <span class="kt-widget__data" id="customer_role_name">{{ \App\Http\Helpers\CommonHelper::getRoleName(@$result->customer->id) }}</span>
                        </div>
                        {{--<div class="kt-widget__info">
                            <span class="kt-widget__label">Tỉnh / Thành:</span>
                            <span class="kt-widget__data" id="customer_province">{{ @$result->customer->province->name }}</span>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Quận / Huyện:</span>
                            <span class="kt-widget__data" id="customer_district">{{ @$result->customer->district->name }}</span>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Phường / Xã:</span>
                            <span class="kt-widget__data" id="customer_ward">{{ @$result->customer->ward->name }}</span>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Địa chỉ:</span>
                            <span class="kt-widget__data" id="customer_address">{{ @$result->customer->address }}</span>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Form-->
</div>

<script>
    $(document).ready(function () {
        getCustomerInfo();
        $('select[name=customer_id]').change(function () {
            getCustomerInfo();
        });
    });

    function getCustomerInfo() {
        var customer_id = $('select[name=customer_id]').val();
        $.ajax({
            url : '/admin/admin/ajax-get-info',
            data: {
                id : customer_id
            },
            success: function (resp) {
                if (resp.status) {
                    $('#customer_image').attr('src', resp.data.image);
                    $('#customer_name').html(resp.data.name);
                    $('#customer_email').html(resp.data.email);
                    $('#customer_tel').html(resp.data.tel);
                    $('#customer_role_name').html(resp.data.role_name);
                    // $('#customer_province').html(resp.data.province_name);
                    // $('#customer_district').html(resp.data.district_name);
                    // $('#customer_ward').html(resp.data.ward_name);
                    // $('#customer_address').html(resp.data.address);
                }
            },
            error: function () {
                alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại!');
            }
        });
    }
</script>