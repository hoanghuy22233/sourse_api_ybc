<style>
    .add_{{$item->id}} {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 500px;
        height: 171px;
        background: #fff;
        border: 1px solid #ccc;
        margin: auto;
        z-index: 99999999999999;
    }

    .che_man {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 999;
        background: #00000012;
    }
</style>
<div class="che_man" style="display: none"></div>

@if(!empty($item->card_id))
    <div class="pop_{{$item->id}}" style="cursor: pointer">
        <span class="btn btn-primary">Nạp tiền</span>

        <div class="form-group hidden_{{$item->id}} add_{{$item->id}}" style="display: none;">

            <input style="margin-top: 20px;width: 450px;margin-left: 20px;" type="number" class="form-control "
                   name="add-money" placeholder="Nhập số tiền">
            {{--<input style="margin-top: 20px;width: 450px;margin-left: 20px;" type="text" class="form-control number_price" name="add-money"  placeholder="Nhập số tiền">--}}
            <a style="margin-left: 160px;margin-top: 25px;" href="/admin/user"
               class="hiden_pop_{{$item->id}} btn btn-secondary">Cancel</a>
            <span style="margin-top: 20px;margin-left: 20px;" class="nap_{{$item->id}} btn btn-success">Nạp tiền</span>
        </div>
    </div>
    <script>
        $(document).ready(function () {

            $(".nap_{{$item->id}}").click(function () {
                var money_number = $(this).parent('.add_{{$item->id}}').find('input').val();
                var user_id = '{{$item->id}}';
                if (confirm('Bạn có chắc chắn muốn nạp không')) {
                    $.ajax({
                        url: '/nap-tien',
                        type: 'get',
                        data: {
                            'money_number': money_number,
                            'user_id': user_id,
                        },
                        success: function () {
                            location.reload();
                        }
                    });
                }
                location.reload();
            });

            $(".pop_{{$item->id}}").click(function () {
                $(this).children(".add_{{$item->id}}").show();
                $('.che_man').show();
            });
        });
    </script>
@endif

<a href="/admin/bill/add?user_id={{$item->id}}" class="btn btn-success"
   style="    font-size: 14px!important;"
>Tạo hóa đơn</a>