<style>
    .form-group-dynamic .fieldwrapper > div:nth-child(1) {
        padding-left: 0;
    }

    .fieldwrapper {
        padding: 5px;
        border: 1px solid #ccc;
        margin-bottom: 5px;
    }
</style>
<fieldset id="buildyourform-{{ $field['name'] }}" class="{{ @$field['class'] }}">
    <?php
    if (isset($result)) {
        $user_address = \Modules\CardUser2\Models\UserAddress::where('user_id', $result->id)->get();
    }
    ?>
    @if(isset($user_address))
        @foreach(@$user_address as $k1 => $v)
            <div class="fieldwrapper" id="field">
                <div class="col-xs-10 col-md-10">
                    <div class="row">
                        <div class="col-xs-6 col-md-6" style="margin-top: 4px;">
                            <input type="text" class="form-control" style="
    height: 0;
    width: 0;
    padding: 0;
"
                                   name="idd[]" value="{{ @$v->id }}">
                            <input type="text" class="form-control fieldname"
                                   name="ten[]" value="{{ @$v->name }}"
                                   placeholder="Tên" required>
                            {{--<input type="text" class="form-control fieldname mt-2"--}}
                                   {{--name="sdt[]" value="{{ @$v->tel }}"--}}
                                   {{--placeholder="SĐT" required>--}}
                        </div>
                        <div class="col-xs-6 col-md-6">
                            <input type="text" class="form-control fieldname mt-2"
                                   name="diachi[]" value="{{ @$v->address }}"
                                   placeholder="Địa chỉ" required>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 col-md-2" style="float: right; bottom: 40px; left: 20px;">
                    <i type="xóa hàng" style="cursor: pointer;"
                       class="btn remove btn btn-danger btn-icon la la-remove"></i>
                </div>
            </div>
        @endforeach
    @else
        <div class="fieldwrapper" id="field">
            <div class="col-xs-10 col-md-10">
                <div class="row">
                    <div class="col-xs-6 col-md-6" style="margin-top: 4px;">
                        <input type="text" class="form-control" style="
    height: 0;
    width: 0;
    padding: 0;
"
                               name="idd[]" value="">
                        <input type="text" class="form-control fieldname"
                               name="ten[]" value=""
                               placeholder="Tên" required>
                        {{--<input type="text" class="form-control fieldname mt-2"--}}
                               {{--name="sdt[]" value=""--}}
                               {{--placeholder="SĐT" required>--}}
                    </div>
                    <div class="col-xs-6 col-md-6">
                        <input type="text" class="form-control fieldname mt-2"
                               name="diachi[]" value=""
                               placeholder="Địa chỉ" required>
                    </div>
                </div>
            </div>
            <div class="col-xs-2 col-md-2 " style="float: right; bottom: 40px; left: 20px;">
                <i type="xóa hàng" style="cursor: pointer;"
                   class="btn remove btn btn-danger btn-icon la la-remove"></i>
            </div>
        </div>
    @endif
    {{--@endif--}}
</fieldset>
<a class="btn btn btn-primary btn-add-dynamic" style="color: white; margin-top: 20px; cursor: pointer;">
    <span>
        <i class="la la-plus"></i>
        <span>Thêm địa chỉ</span>
    </span>
</a>
<script>
    $(document).ready(function () {
        var i = '{{isset($v)?$v->id:1}}';
        $(".btn-add-dynamic").click(function (e) {

            i = parseInt(i);
            i++;
            console.log(i)
            var lastField = $("#buildyourform-{{ $field['name'] }} div:last");
            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $('<div class="fieldwrapper" style="margin-bottom: 5px;" id="field' + intId + '"/>');
            fieldWrapper.data("idx", intId);
            var fields = $('<div class="col-xs-10 col-md-10">\n' +
                '                            <div class="row">\n' +
                '                                <div class="col-xs-6 col-md-6" >\n' +
                '                                    <input type="text" class="form-control hidden" style="\n' +
                '    height: 0;\n' +
                '    width: 0;\n' +
                '    padding: 0;\n' +
                '"\n' +
                '                                                                           name="idd[]" value="' + i + '">\n' +
                '                                    <input type="text" class="form-control fieldname"\n' +
                '                                                                           name="ten[]" value=""\n' +
                '                                                                           placeholder="Tên" >\n' +
                // '                                    <input type="text" class="form-control fieldname mt-2"\n' +
                // '                                           name="sdt[]" value=""\n' +
                // '                                           placeholder="SĐT" required>\n' +
                '                                </div>\n' +
                '                                <div class="col-xs-6 col-md-6">\n' +
                '                                    <input type="text" class="form-control fieldname"\n' +
                '                                           name="diachi[]" value=""\n' +
                '                                           placeholder="Địa chỉ" required>\n' +
                '                                </div>\n' +
                '                            </div>\n' +
                '                        </div>');
            var removeButton = $('<div class="col-xs-2 col-md-2" style="float: right; bottom: 40px; left: 20px"><i type="xóa hàng" style="cursor: pointer;" class="btn remove btn btn-danger btn-icon la la-remove" ></i>');

            fieldWrapper.append(fields);
            fieldWrapper.append(removeButton);
            $("#buildyourform-{{ $field['name'] }}").append(fieldWrapper);
        });
        $('body').on('click', '.remove', function () {
            $(this).parents('.fieldwrapper').remove();
        });
    });
</script>