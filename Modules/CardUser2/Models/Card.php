<?php

namespace Modules\CardUser2\Models ;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $table = 'card';
    protected $guarded = [];

    public function user() {
        return $this->hasOne(User::class, 'card_id');
    }
}

