<?php

/**
 * BillPayment Model
 *
 * BillPayment Model manages BillPayment operation.
 *
 * @category   BillPayment
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\CardUser2\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CardBill\Models\Bill;

class UserAddress extends Model
{
    use Notifiable;

    protected $table = 'user_address';
    protected $guarded = [];
    public $timestamps = false;
    protected $fillable = [
        'name', 'address', 'tel', 'user_id', 'default'
    ];

//    protected $hidden = [
//        'password', 'remember_token',
//    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
