<?php

/**
 * BillPayment Model
 *
 * BillPayment Model manages BillPayment operation. 
 *
 * @category   BillPayment
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license    
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\ThemeCard\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use Notifiable;

    protected $table = 'users';

    protected $fillable = [
        'name', 'email', 'password', 'tel', 'image','card_id', 'address', 'image', 'gender', 'birthday','balance','stk','change_password','api_token','note','date'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    public function card() {
        return $this->belongsTo(Card::class, 'card_id');
    }
}
