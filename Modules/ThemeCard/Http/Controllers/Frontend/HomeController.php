<?php

namespace Modules\ThemeCard\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\CardUser\Models\UserAddress;
use Modules\ThemeCard\Models\Card;
use Session;
use Validator;


class HomeController extends Controller
{
    function getIndex(request $r)
    {
        if (\Auth::check()) {
            return redirect('/dang-xuat');
        }
        if ($r->has('code')) {
            $data['card'] = Card::where('code', $r->code)->first();
            if (is_object($data['card'])) {
                return redirect('enter-pass?code=' . $r->code);
            } else {
                \Session::flash('code', 'Mã thẻ không chính xác');
                return back();
            }
        }
        return view('themecard::pages.home.home');
    }

    function getEnterPass(request $r)
    {
        return view('themecard::pages.home.enter_pass');
    }

    function postEnterPass(request $r)
    {
        $card = Card::where('code', $r->code)->first();
        if (!is_object($card)) {
            \Session::flash('password', 'Mã thẻ không chính xác');
            return back();
        }

        $tel = @$card->user->tel;
        $fieldType = filter_var($tel, FILTER_VALIDATE_EMAIL) ? 'email' : 'tel';
        \Auth::logout();
        if (\Auth::attempt(array($fieldType => $tel, 'password' => $r->get('password')))) {
            return redirect('/profile');
        } else {
            \Session::flash('password', 'Mật khẩu không chính xác');
            return back();
        }
    }

    function getProfile(request $r)
    {
        $card = Card::find(\Auth::user()->card_id);
        $data['card'] = $card;

        if (!$card) {
            return redirect('/');
        }
        $pageOption = [
            'meta_title' => \Auth::user()->name,
            'meta_description' => \Auth::user()->name,
            'meta_keywords' => \Auth::user()->name,
        ];
        view()->share('pageOption', $pageOption);
        return view('themecard::pages.home.profile', $data);
    }

    function getEditProfile()
    {
        if (\Auth::check()) {
            return view('themecard::pages.home.edit_profile');
        } else {
            return redirect('/');
        }
    }

    function postEditProfile(Request $request)
    {
        \Auth::user()->name = $request->name;
        \Auth::user()->email = $request->email;
        \Auth::user()->gender = $request->gender;
        if ($request->password != null) {
            if ($request->password != $request->re_password) {
                \Session::flash('password-error', 'Mật khẩu xác nhận không chính xác');
                return back()->withInput();
            }
            \Auth::user()->password = bcrypt($request->password);
        }
        if ($request->hasFile('image')) {
            \Auth::user()->image = CommonHelper::saveFile($request->image, 'user');
        }
        \Auth::user()->save();
        \Session::flash('success', 'Cập nhật profile thành công!');
        return back();
    }

    function getSecurity()
    {
        if (\Auth::check()) {
            return view('themecard::pages.home.security');
        } else {
            return redirect('/');
        }
    }

    function postSecurity(Request $request)
    {
        $validator = Validator::make($request->all(), [
            're_password' => 'same:password',
        ], [
            're_password.same' => 'Không trùng với mật khẩu!',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            $security = \Auth::user();
            \Auth::user()->tel = $request->phone;
            if ($request->password == $request->re_password && auth()->attempt(array('email' => \Auth::user()->email, 'password' => $request->password)) && !empty($request->password)) {
                $security->save();

                $user_address_ids = [];
                if ($request->has('ten')) {
                    foreach ($request->ten as $k => $ten) {
                        $user_address = UserAddress::updateOrCreate(
                            [
                                'name' => $ten,
                                'address' => $request->diachi[$k],
                                'user_id' => \Auth::user()->id
                            ],
                            []);
                        $user_address_ids[] = $user_address->id;
                    }
                }
                UserAddress::whereNotIn('id', $user_address_ids)->delete();

                \Session::flash('success', 'Cập nhật profile thành công!');
                return back();
            } else {
                \Session::flash('error', 'Mật khẩu không đúng. Vui lòng nhập lại');
                return back();
            }
        }
    }
}
