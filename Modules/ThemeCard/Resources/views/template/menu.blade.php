<div class="row">
    <div class="col-12">
        <nav class="navbar navbar-expand-xl navbar-light bg-light">
            <a class="navbar-brand" href="/">
                <img alt="author" src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}">
                <h1 class="tm-site-title mb-0" style="margin: 0;">{!!@@$settings['name']!!}</h1>
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto">
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link d-flex" href="/dang-xuat">

                            <span>Đăng xuất</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>