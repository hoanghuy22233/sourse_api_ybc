<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600">
<!-- https://fonts.google.com/specimen/Open+Sans -->
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/card/css/fontawesome.min.css') }}">
<!-- https://fontawesome.com/ -->
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/card/css/bootstrap.min.css') }}">
<!-- https://getbootstrap.com/ -->
<link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/card/css/tooplate.css') }}">
<link href="{{ URL::asset('public/frontend/themes/card/css/main.css') }}" rel="stylesheet"/>