<!DOCTYPE html>
<html lang="vi">
<head>
    @include('themecard::partials.head_meta')
    @include('themecard::partials.head_script')
    @yield('head_script')
    <style>
        .tm-block {
            padding: 50px 15px;
        }
        .tm-list-group-alternate-color > th:nth-child(odd) {
            background-color: unset;
        }
        .table td, .table th {
            padding: 8px;
        }
        @media(max-width: 768px) {
            .table-container {
                overflow: scroll;
            }
        }
        h4.tm-block-title_item i {
            width: 20px !important;
        }
        a.navbar-brand h1 {
            display: none;
        }
        a.navbar-brand h1 {
            display: none;
        }
    </style>
</head>

<body class="bg03">

@yield('main_content')
<!-- #wrapper -->
@include('themecard::partials.footer')
@include('themecard::partials.footer_script')
@yield('footer_script')

</body>
</html>
