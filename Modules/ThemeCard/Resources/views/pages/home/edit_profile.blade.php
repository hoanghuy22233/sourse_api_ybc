@extends('themecard::layouts.default')
@section('main_content')
<div class="container">
@include('themecard::template.menu')
    <!-- row -->
    <div class="row tm-content-row tm-mt-big">
        {{--{{dd($user)}}--}}
        <div class="tm-col tm-col-big">
            <div class="bg-white tm-block">
                @if(Session::has('success'))
                    <p style="top: -15px;" class="alert alert-success">{!! Session::get('success') !!}</p>
                @endif
                <div class="row">
                    <div class="col-12">
                        <h2 class="tm-block-title">Thông tin</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <form action="" method="post" class="tm-signup-form" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="name">Họ & tên</label>
                                <input placeholder="Vulputate Eleifend Nulla" id="name" name="name" type="text"
                                       class="form-control validate" value="{{\Auth::user()->name}}">
                            </div>
                            <div class="form-group">
                                <label for="password">Mật khẩu</label>
                                <input placeholder="******" id="password" name="password" type="password"
                                       class="form-control validate" value="">
                            </div>
                            <div class="form-group">
                                <label for="password">Nhập lại mật khẩu</label>
                                <input placeholder="******" id="re-password" name="re_password" type="password"
                                       class="form-control validate" value="">
                                @if(Session::has('password-error'))
                                    <span class="alert alert-danger">{!! Session::get('password-error') !!}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input placeholder="vulputate@eleifend.co" id="email" name="email" type="email"
                                       class="form-control validate" value="{{\Auth::user()->email}}">
                            </div>
                            <label for="Gender">Giới tính
                                <div class="kt-radio-list mt-3">
                                    <label class="kt-radio" style="cursor: pointer;">
                                        <input type="radio" name="gender" @if(\Auth::user()->gender==0)checked
                                               @endif value="0">
                                        Nam
                                        <span></span>
                                    </label>
                                    <label class="kt-radio" style="margin-left: 30px;cursor: pointer;">
                                        <input type="radio" name="gender" @if(\Auth::user()->gender==1)checked
                                               @endif value="1">
                                        Nữ
                                        <span></span>
                                    </label>
                                </div>
                            </label>
                            <div class="form-group">
                                <label for="phone">Ảnh đại diện</label>
                                <input style="padding: 9px;" placeholder="Image" id="image" name="image" type="file"
                                       class="form-control validate" value="{{\Auth::user()->image}}">
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-sm-4">
                                    <a href="/profile"
                                       type="submit" class="btn btn-primary">Quay
                                        lại
                                    </a>
                                </div>
                                <div class="col-md-5 col-sm-4">
                                    <a style="text-align: left;margin-left: 50%; text-decoration: underline;" href="/profile/security" class="btn">Sửa
                                        thông tin
                                        bảo mật</a>
                                </div>
                                <div class="col-md-2 col-sm-4">
                                    <button style="margin-left: 6%;" type="submit" class="btn btn-success">Cập nhật
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection