<div>
    <div class="form-group form-group-last row" id="kt_repeater_1">
        <label class="col-lg-2 col-form-label">Địa chỉ:</label>
        <?php
        $user_address = \Modules\CardUser\Models\UserAddress::where('user_id', @\Auth::user()->id)->get();
        ?>
        @foreach($user_address as $address)
            <div data-repeater-list="" class="col-lg-12">

                <div data-repeater-item="" class="form-group row align-items-center" style="">
                    <input type="text" class="form-control" style="height: 0;width: 0;padding: 0;"
                           name="idd[]" value="{{ @$address->id }}">
                    <div class="col-md-3">
                        <div class="kt-form__group--inline">
                            <div class="kt-form__label">
                                <label>Tên:</label>
                            </div>
                            <div class="kt-form__control">
                                <input type="text" name="ten[]" class="form-control" placeholder="Nhập tên"
                                       value="{{@$address->name}}">
                            </div>
                        </div>
                        <div class="d-md-none kt-margin-b-10"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="kt-form__group--inline">
                            <div class="kt-form__label">
                                <label class="kt-label m-label--single">Địa chỉ:</label>
                            </div>
                            <div class="kt-form__control">
                                <input type="text" name="diachi[]" class="form-control" placeholder="Nhập địa chỉ"
                                       value="{{@$address->address}}">
                            </div>
                        </div>
                        <div class="d-md-none kt-margin-b-10"></div>
                    </div>
                    <div class="col-md-2">
                        <a href="javascript:;" data-repeater-delete=""
                           class="btn-sm btn btn-label-danger btn-bold remove btn-danger" style="margin-top: 19px;">
                            <i class="la la-trash-o"></i>
                            Xóa
                        </a>
                    </div>
                </div>
                <div><hr><br></div>
            </div>
        @endforeach
    </div>
    <div class="form-group form-group-last row">
        <div class="col-lg-4">
            <a href="javascript:;" data-repeater-create=""
               class="btn btn-bold btn-sm btn-label-brand btn-info btn-add-dynamic">
                <i class="la la-plus"></i> Thêm địa chỉ
            </a>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        var i = '{{isset($address)?$address->id:1}}';
        $('body').on("click", ".btn-add-dynamic", function (e) {
            i = parseInt(i);
            i++;
            console.log(i)
            var fields = '<div data-repeater-list="" class="col-lg-12">\n' +
                '\n' +
                '            <div data-repeater-item="" class="form-group row align-items-center" style="">\n' +
                ' <input type="text"  class="form-control" style=" height: 0;width: 0;padding: 0;"name="idd[]" value="' + i + '">\n' +
                '                <div class="col-md-3">\n' +
                '                    <div class="kt-form__group--inline">\n' +
                '                        <div class="kt-form__label">\n' +
                '                            <label>Tên:</label>\n' +
                '                        </div>\n' +
                '                        <div class="kt-form__control">\n' +
                '                            <input type="text" name="ten[]" class="form-control" placeholder="Nhập tên">\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="d-md-none kt-margin-b-10"></div>\n' +
                '                </div>\n' +
                '                <div class="col-md-6">\n' +
                '                    <div class="kt-form__group--inline">\n' +
                '                        <div class="kt-form__label">\n' +
                '                            <label class="kt-label m-label--single">Địa chỉ:</label>\n' +
                '                        </div>\n' +
                '                        <div class="kt-form__control">\n' +
                '                            <input type="text" name="diachi[]" class="form-control" placeholder="Nhập địa chỉ" >\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    <div class="d-md-none kt-margin-b-10"></div>\n' +
                '                </div>\n' +
                '                <div class="col-md-2">\n' +
                '                    <a href="javascript:;" data-repeater-delete="" class="btn-sm btn btn-label-danger btn-bold remove btn-danger" style="margin-top: 19px;">\n' +
                '                        <i class="la la-trash-o"></i>\n' +
                '                        Xóa\n' +
                '                    </a>\n' +
                '                </div>\n' +
                '            </div>\n' +
                '             <div><hr><br></div>\n' +
                '        </div>';
            $("#kt_repeater_1").append(fields);
        });
        $('body').on('click', '.remove', function () {
            $(this).parents('.col-lg-12').remove();
        });
    });
</script>
