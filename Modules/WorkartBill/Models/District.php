<?php
namespace Modules\WorkartBill\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class District extends Model
{

    protected $table = 'district';

    protected $guarded =[];
}
