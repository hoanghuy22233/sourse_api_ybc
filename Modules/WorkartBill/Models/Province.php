<?php
namespace Modules\WorkartBill\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Province extends Model
{

    protected $table = 'province';

    protected $guarded =[];
}
