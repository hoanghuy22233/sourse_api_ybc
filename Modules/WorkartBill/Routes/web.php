<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'bill'], function () {
        Route::get('', 'Admin\BillController@getIndex')->name('bill')->middleware('permission:bill_view');
        Route::get('publish', 'Admin\BillController@getPublish')->name('bill.publish')->middleware('permission:bill_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BillController@add')->middleware('permission:bill_add');
        Route::get('delete/{id}', 'Admin\BillController@delete')->middleware('permission:bill_delete');
        Route::post('multi-delete', 'Admin\BillController@multiDelete')->middleware('permission:bill_delete');
        Route::get('search-for-select2', 'Admin\BillController@searchForSelect2')->name('bill.search_for_select2')->middleware('permission:bill_view');
        Route::get('print/{id}', 'Admin\BillController@print')->middleware('permission:bill_view');
        Route::get('{id}', 'Admin\BillController@update')->middleware('permission:bill_view');
        Route::post('{id}', 'Admin\BillController@update')->middleware('permission:bill_edit');
    });

    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'Admin\DashboardController@dashboardSoftware');
    });
    Route::group(['prefix' => 'product_research'], function () {
        Route::get('', 'Admin\ProductResearchController@productResearch');
    });
    Route::group(['prefix' => 'analysis_chart'], function () {
        Route::get('', 'Admin\ProductResearchController@analysisChart');
    });


});
