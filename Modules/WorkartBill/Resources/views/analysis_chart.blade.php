@extends(config('core.admin_theme').'.template')
@section('main')
    <style>
        .small-box {
            border-radius: 2px;
            position: relative;
            display: block;
            margin-bottom: 20px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1)
        }
        .small-box>.inner {
            padding: 10px
        }
        .small-box>.small-box-footer {
            position: relative;
            text-align: center;
            padding: 3px 0;
            color: #fff;
            color: rgba(255, 255, 255, 0.8);
            display: block;
            z-index: 10;
            background: rgba(0, 0, 0, 0.1);
            text-decoration: none
        }
        .small-box>.small-box-footer:hover {
            color: #fff;
            background: rgba(0, 0, 0, 0.15)
        }
        .small-box h3 {
            font-size: 38px;
            font-weight: bold;
            margin: 0 0 10px 0;
            white-space: nowrap;
            padding: 0
        }
        .small-box p {
            font-size: 15px
        }
        .small-box p>small {
            display: block;
            color: #f9f9f9;
            font-size: 13px;
            margin-top: 5px
        }
        .small-box h3,
        .small-box p {
            z-index: 5px
        }
        .small-box .icon {
            -webkit-transition: all .3s linear;
            -o-transition: all .3s linear;
            transition: all .3s linear;
            position: absolute;
            top: -10px;
            right: 10px;
            z-index: 0;
            font-size: 90px;
            color: rgba(0, 0, 0, 0.15)
        }
        .small-box:hover {
            text-decoration: none;
            color: #f9f9f9
        }
        .small-box:hover .icon {
            font-size: 95px
        }
        @media(max-width:767px) {
            .small-box {
                text-align: center
            }
            .small-box .icon {
                display: none
            }
            .small-box p {
                font-size: 12px
            }
        }
        .bg-yellow,
        .bg-aqua,
        .bg-teal,
        .bg-purple,
        .bg-maroon
        {
            color: #fff !important
        }

        .bg-yellow{
            background-color: #f39c12 !important
        }
        .bg-aqua{
            background-color: #00c0ef !important
        }

        .bg-teal {
            background-color: #39cccc !important
        }
        .bg-purple {
            background-color: #605ca8 !important
        }
        .bg-maroon {
            background-color: #d81b60 !important
        }
    </style>
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="col-md-12 order-lg-2 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title bold uppercase">
                            Nghiên cứu sản phẩm
                        </h3>
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1"></label>
                        <select class="form-control" id="exampleSelect1">
                            <option>Tháng 1</option>
                            <option>Tháng 2</option>
                            <option>Tháng 3</option>
                            <option>Tháng 4</option>
                            <option>Tháng 5</option>
                            <option>Tháng 6</option>
                            <option>Tháng 7</option>

                        </select>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">

            <div class="col-md-6 order-lg-2 order-xl-1">
                    <!--begin::Card-->
                    <div class="card card-custom gutter-b">
                        <!--begin::Header-->
                        <div class="card-header h-auto">
                            <!--begin::Title-->
                            <div class="card-title py-5">
                                <h3 class="card-label">Line Chart</h3>
                            </div>
                            <!--end::Title-->
                        </div>
                        <!--end::Header-->
                        <div class="card-body" style="position: relative;">
                            <!--begin::Chart-->
                            <div id="chart_1" style="min-height: 365px;"><div id="apexchartsism6g6v4" class="apexcharts-canvas apexchartsism6g6v4 apexcharts-theme-light" style="width: 366px; height: 350px;"><svg id="SvgjsSvg3262" width="366" height="350" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)" style="background: transparent;"><g id="SvgjsG3264" class="apexcharts-inner apexcharts-graphical" transform="translate(44.4375, 30)"><defs id="SvgjsDefs3263"><clipPath id="gridRectMaskism6g6v4"><rect id="SvgjsRect3270" width="308.2578125" height="282.494" x="-4.5" y="-2.5" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath><clipPath id="gridRectMarkerMaskism6g6v4"><rect id="SvgjsRect3271" width="303.2578125" height="281.494" x="-2" y="-2" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fff"></rect></clipPath></defs><line id="SvgjsLine3269" x1="0" y1="0" x2="0" y2="277.494" stroke="#b6b6b6" stroke-dasharray="3" class="apexcharts-xcrosshairs" x="0" y="0" width="1" height="277.494" fill="#b1b9c4" filter="none" fill-opacity="0.9" stroke-width="1"></line><g id="SvgjsG3277" class="apexcharts-xaxis" transform="translate(0, 0)"><g id="SvgjsG3278" class="apexcharts-xaxis-texts-g" transform="translate(0, -4)"><text id="SvgjsText3280" font-family="Helvetica, Arial, sans-serif" x="0" y="306.494" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3281">Jan</tspan><title>Jan</title></text><text id="SvgjsText3283" font-family="Helvetica, Arial, sans-serif" x="37.4072265625" y="306.494" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3284">Feb</tspan><title>Feb</title></text><text id="SvgjsText3286" font-family="Helvetica, Arial, sans-serif" x="74.814453125" y="306.494" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3287">Mar</tspan><title>Mar</title></text><text id="SvgjsText3289" font-family="Helvetica, Arial, sans-serif" x="112.2216796875" y="306.494" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3290">Apr</tspan><title>Apr</title></text><text id="SvgjsText3292" font-family="Helvetica, Arial, sans-serif" x="149.62890625" y="306.494" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3293">May</tspan><title>May</title></text><text id="SvgjsText3295" font-family="Helvetica, Arial, sans-serif" x="187.0361328125" y="306.494" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3296">Jun</tspan><title>Jun</title></text><text id="SvgjsText3298" font-family="Helvetica, Arial, sans-serif" x="224.443359375" y="306.494" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3299">Jul</tspan><title>Jul</title></text><text id="SvgjsText3301" font-family="Helvetica, Arial, sans-serif" x="261.8505859375" y="306.494" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3302">Aug</tspan><title>Aug</title></text><text id="SvgjsText3304" font-family="Helvetica, Arial, sans-serif" x="299.2578125" y="306.494" text-anchor="middle" dominant-baseline="auto" font-size="12px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-xaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3305">Sep</tspan><title>Sep</title></text></g><line id="SvgjsLine3306" x1="0" y1="278.494" x2="299.2578125" y2="278.494" stroke="#e0e0e0" stroke-dasharray="0" stroke-width="1"></line></g><g id="SvgjsG3321" class="apexcharts-grid"><g id="SvgjsG3322" class="apexcharts-gridlines-horizontal"><line id="SvgjsLine3333" x1="0" y1="0" x2="299.2578125" y2="0" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine3334" x1="0" y1="55.4988" x2="299.2578125" y2="55.4988" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine3335" x1="0" y1="110.9976" x2="299.2578125" y2="110.9976" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine3336" x1="0" y1="166.4964" x2="299.2578125" y2="166.4964" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine3337" x1="0" y1="221.9952" x2="299.2578125" y2="221.9952" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line><line id="SvgjsLine3338" x1="0" y1="277.494" x2="299.2578125" y2="277.494" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-gridline"></line></g><g id="SvgjsG3323" class="apexcharts-gridlines-vertical"></g><line id="SvgjsLine3324" x1="0" y1="278.494" x2="0" y2="284.494" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-xaxis-tick"></line><line id="SvgjsLine3325" x1="37.4072265625" y1="278.494" x2="37.4072265625" y2="284.494" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-xaxis-tick"></line><line id="SvgjsLine3326" x1="74.814453125" y1="278.494" x2="74.814453125" y2="284.494" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-xaxis-tick"></line><line id="SvgjsLine3327" x1="112.2216796875" y1="278.494" x2="112.2216796875" y2="284.494" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-xaxis-tick"></line><line id="SvgjsLine3328" x1="149.62890625" y1="278.494" x2="149.62890625" y2="284.494" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-xaxis-tick"></line><line id="SvgjsLine3329" x1="187.0361328125" y1="278.494" x2="187.0361328125" y2="284.494" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-xaxis-tick"></line><line id="SvgjsLine3330" x1="224.443359375" y1="278.494" x2="224.443359375" y2="284.494" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-xaxis-tick"></line><line id="SvgjsLine3331" x1="261.8505859375" y1="278.494" x2="261.8505859375" y2="284.494" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-xaxis-tick"></line><line id="SvgjsLine3332" x1="299.2578125" y1="278.494" x2="299.2578125" y2="284.494" stroke="#e0e0e0" stroke-dasharray="0" class="apexcharts-xaxis-tick"></line><rect id="SvgjsRect3339" width="299.2578125" height="55.4988" x="0" y="0" rx="0" ry="0" opacity="0.5" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#f3f3f3" clip-path="url(#gridRectMaskism6g6v4)" class="apexcharts-grid-row"></rect><rect id="SvgjsRect3340" width="299.2578125" height="55.4988" x="0" y="55.4988" rx="0" ry="0" opacity="0.5" stroke-width="0" stroke="none" stroke-dasharray="0" fill="transparent" clip-path="url(#gridRectMaskism6g6v4)" class="apexcharts-grid-row"></rect><rect id="SvgjsRect3341" width="299.2578125" height="55.4988" x="0" y="110.9976" rx="0" ry="0" opacity="0.5" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#f3f3f3" clip-path="url(#gridRectMaskism6g6v4)" class="apexcharts-grid-row"></rect><rect id="SvgjsRect3342" width="299.2578125" height="55.4988" x="0" y="166.4964" rx="0" ry="0" opacity="0.5" stroke-width="0" stroke="none" stroke-dasharray="0" fill="transparent" clip-path="url(#gridRectMaskism6g6v4)" class="apexcharts-grid-row"></rect><rect id="SvgjsRect3343" width="299.2578125" height="55.4988" x="0" y="221.9952" rx="0" ry="0" opacity="0.5" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#f3f3f3" clip-path="url(#gridRectMaskism6g6v4)" class="apexcharts-grid-row"></rect><line id="SvgjsLine3345" x1="0" y1="277.494" x2="299.2578125" y2="277.494" stroke="transparent" stroke-dasharray="0"></line><line id="SvgjsLine3344" x1="0" y1="1" x2="0" y2="277.494" stroke="transparent" stroke-dasharray="0"></line></g><g id="SvgjsG3272" class="apexcharts-line-series apexcharts-plot-series"><g id="SvgjsG3273" class="apexcharts-series" seriesName="Desktops" data:longestSeries="true" rel="1" data:realIndex="0"><path id="SvgjsPath3276" d="M 0 258.99440000000004L 37.4072265625 201.64564000000001L 74.814453125 212.74540000000002L 112.2216796875 183.14604000000003L 149.62890625 186.84596000000005L 187.0361328125 162.79648000000003L 224.443359375 149.84676000000002L 261.8505859375 109.14764000000002L 299.2578125 3.69992000000002" fill="none" fill-opacity="1" stroke="rgba(105,147,255,0.85)" stroke-opacity="1" stroke-linecap="butt" stroke-width="5" stroke-dasharray="0" class="apexcharts-line" index="0" clip-path="url(#gridRectMaskism6g6v4)" pathTo="M 0 258.99440000000004L 37.4072265625 201.64564000000001L 74.814453125 212.74540000000002L 112.2216796875 183.14604000000003L 149.62890625 186.84596000000005L 187.0361328125 162.79648000000003L 224.443359375 149.84676000000002L 261.8505859375 109.14764000000002L 299.2578125 3.69992000000002" pathFrom="M -1 277.494L -1 277.494L 37.4072265625 277.494L 74.814453125 277.494L 112.2216796875 277.494L 149.62890625 277.494L 187.0361328125 277.494L 224.443359375 277.494L 261.8505859375 277.494L 299.2578125 277.494"></path><g id="SvgjsG3274" class="apexcharts-series-markers-wrap" data:realIndex="0"><g class="apexcharts-series-markers"><circle id="SvgjsCircle3351" r="0" cx="0" cy="0" class="apexcharts-marker w88ex6zcf no-pointer-events" stroke="#ffffff" fill="#6993ff" fill-opacity="1" stroke-width="2" stroke-opacity="0.9" default-marker-size="0"></circle></g></g></g><g id="SvgjsG3275" class="apexcharts-datalabels" data:realIndex="0"></g></g><line id="SvgjsLine3346" x1="0" y1="0" x2="299.2578125" y2="0" stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs"></line><line id="SvgjsLine3347" x1="0" y1="0" x2="299.2578125" y2="0" stroke-dasharray="0" stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line><g id="SvgjsG3348" class="apexcharts-yaxis-annotations"></g><g id="SvgjsG3349" class="apexcharts-xaxis-annotations"></g><g id="SvgjsG3350" class="apexcharts-point-annotations"></g></g><rect id="SvgjsRect3268" width="0" height="0" x="0" y="0" rx="0" ry="0" opacity="1" stroke-width="0" stroke="none" stroke-dasharray="0" fill="#fefefe"></rect><g id="SvgjsG3307" class="apexcharts-yaxis" rel="0" transform="translate(14.4375, 0)"><g id="SvgjsG3308" class="apexcharts-yaxis-texts-g"><text id="SvgjsText3309" font-family="Helvetica, Arial, sans-serif" x="20" y="31.5" text-anchor="end" dominant-baseline="auto" font-size="11px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3310">150</tspan></text><text id="SvgjsText3311" font-family="Helvetica, Arial, sans-serif" x="20" y="86.9988" text-anchor="end" dominant-baseline="auto" font-size="11px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3312">120</tspan></text><text id="SvgjsText3313" font-family="Helvetica, Arial, sans-serif" x="20" y="142.4976" text-anchor="end" dominant-baseline="auto" font-size="11px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3314">90</tspan></text><text id="SvgjsText3315" font-family="Helvetica, Arial, sans-serif" x="20" y="197.9964" text-anchor="end" dominant-baseline="auto" font-size="11px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3316">60</tspan></text><text id="SvgjsText3317" font-family="Helvetica, Arial, sans-serif" x="20" y="253.4952" text-anchor="end" dominant-baseline="auto" font-size="11px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3318">30</tspan></text><text id="SvgjsText3319" font-family="Helvetica, Arial, sans-serif" x="20" y="308.994" text-anchor="end" dominant-baseline="auto" font-size="11px" font-weight="400" fill="#373d3f" class="apexcharts-text apexcharts-yaxis-label " style="font-family: Helvetica, Arial, sans-serif;"><tspan id="SvgjsTspan3320">0</tspan></text></g></g><g id="SvgjsG3265" class="apexcharts-annotations"></g></svg><div class="apexcharts-legend" style="max-height: 175px;"></div><div class="apexcharts-tooltip apexcharts-theme-light"><div class="apexcharts-tooltip-title" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;"></div><div class="apexcharts-tooltip-series-group" style="order: 1;"><span class="apexcharts-tooltip-marker" style="background-color: rgb(105, 147, 255);"></span><div class="apexcharts-tooltip-text" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;"><div class="apexcharts-tooltip-y-group"><span class="apexcharts-tooltip-text-label"></span><span class="apexcharts-tooltip-text-value"></span></div><div class="apexcharts-tooltip-z-group"><span class="apexcharts-tooltip-text-z-label"></span><span class="apexcharts-tooltip-text-z-value"></span></div></div></div></div><div class="apexcharts-xaxistooltip apexcharts-xaxistooltip-bottom apexcharts-theme-light"><div class="apexcharts-xaxistooltip-text" style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;"></div></div><div class="apexcharts-yaxistooltip apexcharts-yaxistooltip-0 apexcharts-yaxistooltip-left apexcharts-theme-light"><div class="apexcharts-yaxistooltip-text"></div></div><div class="apexcharts-toolbar" style="top: 0px; right: 3px;"><div class="apexcharts-menu-icon" title="Menu"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"></path><path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path></svg></div><div class="apexcharts-menu"><div class="apexcharts-menu-item exportSVG" title="Download SVG">Download SVG</div><div class="apexcharts-menu-item exportPNG" title="Download PNG">Download PNG</div><div class="apexcharts-menu-item exportCSV" title="Download CSV">Download CSV</div></div></div></div></div>
                            <!--end::Chart-->
                            <div class="resize-triggers"><div class="expand-trigger"><div style="width: 426px; height: 418px;"></div></div><div class="contract-trigger"></div></div></div>
                    </div>
                    <!--end::Card-->
            </div>

            <div class="col-md-6 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Sản phẩm thịnh hành
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            @php
                                $data = \Modules\ThemeWorkart\Models\Product::orderBy('id', 'desc')->paginate(5);
                            @endphp
                            <table class="kt-datatable__table" style=" width: 100%;">
                                <thead class="kt-datatable__head" style="    overflow: unset;">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 25px;">STT</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Sản phẩm</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 200px;">Lượt xem</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Tỷ lệ mua</span>
                                    </th>
                                    {{--<th class="kt-datatable__cell kt-datatable__cell--sort">--}}
                                    {{--<span style="width: 50px;">Chi tiết</span>--}}
                                    {{--</th>--}}
                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="">
                                {{--@if($bill_news->count()>0)--}}
                                @foreach($data as $k => $item)
                                    <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 25px;">
                                                    <span class="kt-font-bold">{{$k+1}}</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{ $item->name }}</span></a>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 200px;">
                                                        <span class="kt-font-bold">123</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">134</span>
                                                </span>
                                        </td>
                                    </tr>
                                @endforeach
                                {{--@endif--}}
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                        <div class="paginate">{{$data->appends(Request::all())->links()}}</div>
                    </div>

                </div>
            </div>


            <div class="col-md-6 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Sản phẩm tiềm năng
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            @php
                                $data = \Modules\ThemeWorkart\Models\Product::orderBy('id', 'desc')->paginate(5);
                            @endphp
                            <table class="kt-datatable__table" style=" width: 100%;">
                                <thead class="kt-datatable__head" style="    overflow: unset;">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 25px;">STT</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Sản phẩm</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 200px;">Lượt xem</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Tỷ lệ mua</span>
                                    </th>
                                    {{--<th class="kt-datatable__cell kt-datatable__cell--sort">--}}
                                    {{--<span style="width: 50px;">Chi tiết</span>--}}
                                    {{--</th>--}}
                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="">
                                {{--@if($bill_news->count()>0)--}}
                                @foreach($data as $k => $item)
                                    <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 25px;">
                                                    <span class="kt-font-bold">{{$k+1}}</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{ $item->name }}</span></a>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 200px;">
                                                        <span class="kt-font-bold">123</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">123</span>
                                                </span>
                                        </td>
                                    </tr>
                                @endforeach
                                {{--@endif--}}
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                        <div class="paginate">{{$data->appends(Request::all())->links()}}</div>
                    </div>

                </div>
            </div>


            <div class="col-md-6 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Sản phẩm thịnh hành
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            @php
                                $data = \Modules\ThemeWorkart\Models\Product::orderBy('id', 'desc')->paginate(5);
                            @endphp
                            <table class="kt-datatable__table" style=" width: 100%;">
                                <thead class="kt-datatable__head" style="    overflow: unset;">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 25px;">STT</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Sản phẩm</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 200px;">Lượt xem</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Tỷ lệ mua</span>
                                    </th>
                                    {{--<th class="kt-datatable__cell kt-datatable__cell--sort">--}}
                                    {{--<span style="width: 50px;">Chi tiết</span>--}}
                                    {{--</th>--}}
                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="">
                                {{--@if($bill_news->count()>0)--}}
                                @foreach($data as $k => $item)
                                    <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 25px;">
                                                    <span class="kt-font-bold">{{$k+1}}</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{ $item->name }}</span></a>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 200px;">
                                                        <span class="kt-font-bold">123</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">134</span>
                                                </span>
                                        </td>
                                    </tr>
                                @endforeach
                                {{--@endif--}}
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                        <div class="paginate">{{$data->appends(Request::all())->links()}}</div>
                    </div>

                </div>
            </div>



            {{-- Danh sách các sản phẩm mới đăng --}}

        </div>
    </div>

@endsection
@section('custom_head')
    <style type="text/css">
        .kt-datatable__cell>span>a.cate {
            color: #5867dd;
            margin-bottom: 3px;
            background: rgba(88, 103, 221, 0.1);
            height: auto;
            display: inline-block;
            width: auto;
            padding: 0.15rem 0.75rem;
            border-radius: 2px;
        }
        .paginate>ul.pagination>li{
            padding: 5px 10px;
            border: 1px solid #ccc;
            margin: 0 5px;
            cursor: pointer;        }

        .paginate>ul.pagination span{
            color: #000;
        }
        .paginate>ul.pagination>li.active{
            background: #0b57d5;
            color: #fff!important;
        }
        .paginate>ul.pagination>li.active span{
            color: #fff!important;
        }
        .kt-widget12__desc, .kt-widget12__value {
            text-align: center;
        }

        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99 list_user
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <style type="text/css">
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>

@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $('#active_service a').click(function (event) {
                event.preventDefault();
                var object = $(this);
                $.ajax({
                    url: '/admin/service_history/ajax-publish',
                    data: {
                        id: object.data('service_history_id')
                    },
                    success: function (result) {
                        if (result.status == true) {
                            toastr.success(result.msg);
                            object.parents('tr').remove();
                        } else {
                            toastr.error(result.msg);
                        }
                    },
                    error: function (e) {
                        console.log(e.message);
                    }
                })
            })
        })
    </script>
@endpush

