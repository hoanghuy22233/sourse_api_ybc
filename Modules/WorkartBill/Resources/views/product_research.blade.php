@extends(config('core.admin_theme').'.template')
@section('main')
    <style>
        .small-box {
            border-radius: 2px;
            position: relative;
            display: block;
            margin-bottom: 20px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1)
        }
        .small-box>.inner {
            padding: 10px
        }
        .small-box>.small-box-footer {
            position: relative;
            text-align: center;
            padding: 3px 0;
            color: #fff;
            color: rgba(255, 255, 255, 0.8);
            display: block;
            z-index: 10;
            background: rgba(0, 0, 0, 0.1);
            text-decoration: none
        }
        .small-box>.small-box-footer:hover {
            color: #fff;
            background: rgba(0, 0, 0, 0.15)
        }
        .small-box h3 {
            font-size: 38px;
            font-weight: bold;
            margin: 0 0 10px 0;
            white-space: nowrap;
            padding: 0
        }
        .small-box p {
            font-size: 15px
        }
        .small-box p>small {
            display: block;
            color: #f9f9f9;
            font-size: 13px;
            margin-top: 5px
        }
        .small-box h3,
        .small-box p {
            z-index: 5px
        }
        .small-box .icon {
            -webkit-transition: all .3s linear;
            -o-transition: all .3s linear;
            transition: all .3s linear;
            position: absolute;
            top: -10px;
            right: 10px;
            z-index: 0;
            font-size: 90px;
            color: rgba(0, 0, 0, 0.15)
        }
        .small-box:hover {
            text-decoration: none;
            color: #f9f9f9
        }
        .small-box:hover .icon {
            font-size: 95px
        }
        @media(max-width:767px) {
            .small-box {
                text-align: center
            }
            .small-box .icon {
                display: none
            }
            .small-box p {
                font-size: 12px
            }
        }
        .bg-yellow,
        .bg-aqua,
        .bg-teal,
        .bg-purple,
        .bg-maroon
        {
            color: #fff !important
        }

        .bg-yellow{
            background-color: #f39c12 !important
        }
        .bg-aqua{
            background-color: #00c0ef !important
        }

        .bg-teal {
            background-color: #39cccc !important
        }
        .bg-purple {
            background-color: #605ca8 !important
        }
        .bg-maroon {
            background-color: #d81b60 !important
        }
    </style>
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="col-md-12 order-lg-2 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title bold uppercase">
                            Product research
                        </h3>
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1"></label>
                        <select class="form-control" id="exampleSelect1">
                            <option>Month 1</option>
                            <option>Month 2</option>
                            <option>Month 3</option>
                            <option>Month 4</option>
                            <option>Month 5</option>
                            <option>Month 6</option>
                            <option>Month 7</option>

                        </select>
                    </div>
                </div>

            </div>
        </div>
        <div class="row">

            <div class="col-md-6 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Potential products
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            @php
                                $data = \Modules\ThemeWorkart\Models\Product::orderBy('id', 'desc')->paginate(5);
                            @endphp
                            <table class="kt-datatable__table" style=" width: 100%;">
                                <thead class="kt-datatable__head" style="    overflow: unset;">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 25px;">#</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Product</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 200px;">View</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Purchase rate</span>
                                    </th>
                                    {{--<th class="kt-datatable__cell kt-datatable__cell--sort">--}}
                                    {{--<span style="width: 50px;">Chi tiết</span>--}}
                                    {{--</th>--}}
                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="">
                                {{--@if($bill_news->count()>0)--}}
                                @foreach($data as $k => $item)
                                    <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 25px;">
                                                    <span class="kt-font-bold">{{$k+1}}</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{ $item->name }}</span></a>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 200px;">
                                                        <span class="kt-font-bold">123</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">123 %</span>
                                                </span>
                                        </td>
                                    </tr>
                                @endforeach
                                {{--@endif--}}
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                        <div class="paginate">{{$data->appends(Request::all())->links()}}</div>
                    </div>

                </div>
            </div>

            <div class="col-md-6 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Product popular
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            @php
                                $data = \Modules\ThemeWorkart\Models\Product::orderBy('id', 'desc')->paginate(5);
                            @endphp
                            <table class="kt-datatable__table" style=" width: 100%;">
                                <thead class="kt-datatable__head" style="    overflow: unset;">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 25px;">#</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Product</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 200px;">Quantity sold</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Rate difference</span>
                                    </th>
                                    {{--<th class="kt-datatable__cell kt-datatable__cell--sort">--}}
                                    {{--<span style="width: 50px;">Chi tiết</span>--}}
                                    {{--</th>--}}
                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="">
                                {{--@if($bill_news->count()>0)--}}
                                @foreach($data as $k => $item)
                                    <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 25px;">
                                                    <span class="kt-font-bold">{{$k+1}}</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{ $item->name }}</span></a>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 200px;">
                                                        <span class="kt-font-bold">54 &uarr; 34</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">134 %</span>
                                                </span>
                                        </td>
                                    </tr>
                                @endforeach
                                {{--@endif--}}
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                        <div class="paginate">{{$data->appends(Request::all())->links()}}</div>
                    </div>

                </div>
            </div>


            <div class="col-md-6 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Product tiềm năng
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            @php
                                $data = \Modules\ThemeWorkart\Models\Product::orderBy('id', 'desc')->paginate(5);
                            @endphp
                            <table class="kt-datatable__table" style=" width: 100%;">
                                <thead class="kt-datatable__head" style="    overflow: unset;">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 25px;">#</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Product</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 200px;">View</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Purchase rate</span>
                                    </th>
                                    {{--<th class="kt-datatable__cell kt-datatable__cell--sort">--}}
                                    {{--<span style="width: 50px;">Chi tiết</span>--}}
                                    {{--</th>--}}
                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="">
                                {{--@if($bill_news->count()>0)--}}
                                @foreach($data as $k => $item)
                                    <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 25px;">
                                                    <span class="kt-font-bold">{{$k+1}}</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{ $item->name }}</span></a>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 200px;">
                                                        <span class="kt-font-bold">123</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">&uarr; 69 %</span>
                                                </span>
                                        </td>
                                    </tr>
                                @endforeach
                                {{--@endif--}}
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                        <div class="paginate">{{$data->appends(Request::all())->links()}}</div>
                    </div>

                </div>
            </div>


            <div class="col-md-6 order-lg-2 order-xl-1">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
                    <div class="kt-portlet__head kt-portlet__head--lg kt-portlet__head--noborder kt-portlet__head--break-sm">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title bold uppercase">
                                Product popular
                            </h3>
                        </div>
                    </div>

                    <div class="kt-portlet__body kt-portlet__body--fit">
                        <!--begin: Datatable -->
                        <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                             id="kt_datatable_latest_orders" style="">
                            @php
                                $data = \Modules\ThemeWorkart\Models\Product::orderBy('id', 'desc')->paginate(5);
                            @endphp
                            <table class="kt-datatable__table" style=" width: 100%;">
                                <thead class="kt-datatable__head" style="    overflow: unset;">
                                <tr class="kt-datatable__row" style="left: 0px;">
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 25px;">#</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Product</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 200px;">Quantity sold</span>
                                    </th>
                                    <th class="kt-datatable__cell kt-datatable__cell--sort">
                                        <span style="width: 100px;">Rate difference</span>
                                    </th>
                                    {{--<th class="kt-datatable__cell kt-datatable__cell--sort">--}}
                                    {{--<span style="width: 50px;">Chi tiết</span>--}}
                                    {{--</th>--}}
                                </tr>
                                </thead>
                                <tbody class="kt-datatable__body ps ps--active-y"
                                       style="">
                                {{--@if($bill_news->count()>0)--}}
                                @foreach($data as $k => $item)
                                    <tr data-row="0" class="kt-datatable__row" style="left: 0px;">
                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 25px;">
                                                    <span class="kt-font-bold">{{$k+1}}</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">{{ $item->name }}</span></a>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 200px;">
                                                        <span class="kt-font-bold">&uarr; 123 &uarr;</span>
                                                </span>
                                        </td>

                                        <td data-field="ShipDate" class="kt-datatable__cell">
                                                <span style="width: 100px;">
                                                    <span class="kt-font-bold">134</span>
                                                </span>
                                        </td>
                                    </tr>
                                @endforeach
                                {{--@endif--}}
                                </tbody>
                            </table>
                        </div>
                        <!--end: Datatable -->
                        <div class="paginate">{{$data->appends(Request::all())->links()}}</div>
                    </div>

                </div>
            </div>



            {{-- Danh sách các Product mới đăng --}}

        </div>
    </div>

@endsection
@section('custom_head')
    <style type="text/css">
        .kt-datatable__cell>span>a.cate {
            color: #5867dd;
            margin-bottom: 3px;
            background: rgba(88, 103, 221, 0.1);
            height: auto;
            display: inline-block;
            width: auto;
            padding: 0.15rem 0.75rem;
            border-radius: 2px;
        }
        .paginate>ul.pagination>li{
            padding: 5px 10px;
            border: 1px solid #ccc;
            margin: 0 5px;
            cursor: pointer;        }

        .paginate>ul.pagination span{
            color: #000;
        }
        .paginate>ul.pagination>li.active{
            background: #0b57d5;
            color: #fff!important;
        }
        .paginate>ul.pagination>li.active span{
            color: #fff!important;
        }
        .kt-widget12__desc, .kt-widget12__value {
            text-align: center;
        }

        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99 list_user
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <style type="text/css">
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }
            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }

        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>

@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $('#active_service a').click(function (event) {
                event.preventDefault();
                var object = $(this);
                $.ajax({
                    url: '/admin/service_history/ajax-publish',
                    data: {
                        id: object.data('service_history_id')
                    },
                    success: function (result) {
                        if (result.status == true) {
                            toastr.success(result.msg);
                            object.parents('tr').remove();
                        } else {
                            toastr.error(result.msg);
                        }
                    },
                    error: function (e) {
                        console.log(e.message);
                    }
                })
            })
        })
    </script>
@endpush

