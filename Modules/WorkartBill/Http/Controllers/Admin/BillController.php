<?php

namespace Modules\WorkartBill\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\ThemeWorkart\Models\Country;
use Modules\WorkartBill\Models\Bill;
use Modules\ThemeWorkart\Models\District;
use Modules\ThemeWorkart\Models\Province;
use Validator;

class BillController extends CURDBaseController
{

    protected $orderByRaw = 'id DESC';

    protected $module = [
        'code' => 'bill',
        'table_name' => 'bills',
        'label' => 'Bill',
        'modal' => '\Modules\WorkartBill\Models\Bill',
        'list' => [
            ['name' => 'user_id', 'type' => 'relation_edit', 'label' => 'Customer', 'object' => 'user', 'display_field' => 'name'],
            ['name' => 'id', 'type' => 'custom','td' => 'workartbill::list.td.code_prd', 'label' => 'Bill code'],
            ['name' => 'total_price', 'type' => 'price_en', 'label' => 'Total price'],
//            ['name' => 'id', 'type' => 'custom','td' => 'workartbill::list.td.qua_tang', 'label' => 'Quà tặng'],
            ['name' => 'updated_at', 'type' => 'text', 'label' => 'Updated at'],
//            ['name' => 'updated_at', 'type' => 'custom','td' => 'workartbill::list.td.hour', 'label' => 'Giờ'],
            ['name' => 'receipt_method', 'type' => 'text', 'label' => 'Receipt method'],
            ['name' => 'status', 'type' => 'select', 'options' => [
                'pending' => 'Pending',
                'complete' => 'Complete',
            ], 'label' => 'Status'],
<<<<<<< HEAD
            ['name' => 'quantity', 'type' => 'text', 'label' => 'Quantity'],
=======

>>>>>>> 4c3924285f481262847106e882d2f4591f5afb80
            ['name' => 'user_address', 'type' => 'text', 'label' => 'Address',],



        ],
        'form' => [
            'general_tab' => [
                ['name' => 'total_price', 'type' => 'text', 'class' => 'number_price', 'label' => 'Total price'],
                ['name' => 'coupon_code', 'type' => 'number', 'label' => 'Coupon code', 'group_class' => 'col-md-6'],
                ['name' => 'date', 'type' => 'datetime-local', 'label' => 'Date', 'group_class' => 'col-md-6'],
                ['name' => 'status', 'type' => 'select', 'options' => [
                    'pending' => 'Pending',
                    'complete' => 'Complete',
                ], 'label' => 'Status'],
                ['name' => 'receipt_method', 'type' => 'text', 'label' => 'Receipt method'],
                ['name' => 'quantity', 'type' => 'number', 'label' => 'quantity'],
                ['name' => 'note', 'type' => 'text', 'label' => 'Note'],
            ],
            'info_tab' => [
                ['name' => 'user_name', 'type' => 'text', 'class' => 'required', 'label' => 'User name'],
                ['name' => 'user_tel', 'type' => 'custom', 'field' => 'workartbill::form.field.text_input', 'class' => '', 'label' => 'Tel'],
                ['name' => 'user_email','type' => 'custom', 'field' => 'workartbill::form.field.text_input', 'class' => '', 'label' => 'Email'],

                ['name' => 'user_address', 'type' => 'text', 'class' => '', 'label' => 'Address'],
                ['name' => 'note', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Note'],

                ['name' => 'user_country_id', 'type' => 'select_country', 'class' => '', 'label' => 'Country',
                    'model' => Country::class, 'display_field' => 'name'],
                ['name' => 'user_city', 'type' => 'text', 'class' => '', 'label' => 'City'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, name, tel, email, price',
        'fields' => 'id, user_name, user_tel, user_email, total_price, updated_at, date',
    ];

    protected $filter = [
        'updated_at' => [
            'label' => 'Ngày đặt',
            'type' => 'date',
            'query_type' => 'custom'
        ],
        'date' => [
            'label' => 'Ngày nhận',
            'type' => 'date',
            'query_type' => 'custom'
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                'pending' => 'Pending',
                'complete' => 'Complete',
            ],
        ],

    ];

    public function getIndex(Request $request)
    {

        $data = $this->getDataList($request);

        return view('workartbill::list')->with($data);
    }

    public function print($id, Request $request)
    {
        $data['page_title'] = 'In báo giá';
        $bill = Bill::findOrFail($id);
        $data['result'] = $bill;
        $data['header_print'] = @$bill->company->header_print;
        $data['footer_print'] = @$bill->company->footer_print;
        return view('workartbill::print')->with($data);
    }

//    public function appendWhere($query, $request)
//    {
//        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
//        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
//            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
//        }
//
//        return $query;
//    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('workartbill::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'user_name' => 'required'
                ], [
                    'user_name.required' => 'Required to enter name',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;
//                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'New successfully created!');
                    } else {
                        CommonHelper::one_time_message('error', 'Im new. Please reload the page and try again!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);


            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('workartbill::edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'user_name' => 'required'
                ], [
                    'user_name.required' => 'Required to enter name',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;


                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

//            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
