<?php

namespace Modules\WorkartBill\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Auth;
use DB;
use Illuminate\Http\Request;
use Mail;
use Modules\JdesBill\Models\Bill;
use Modules\Theme\Models\Post;
use Modules\JdesProduct\Models\Product;
use Modules\ThemeSemicolonwebJdes\Models\Company;

class ProductResearchController extends Controller
{
    protected $module = [
    ];

    public function productResearch()
    {
        $data['page_title'] = 'Nghiên cứu sản phẩm';
        $data['page_type'] = 'list';
        return view('workartbill::product_research', $data);
    }


    public function tooltipInfo(Request $request)
    {
        $modal = new $request->modal;
        $data['item'] = $modal->find($request->id);
        $data['tooltip_info'] = $request->tooltip_info;

        return view('admin.common.modal.tooltip_info', $data);
    }

    public function analysisChart(Request $request)
    {
        $data['page_title'] = 'Nghiên cứu sản phẩm';
        $data['page_type'] = 'list';
        return view('workartbill::analysis_chart', $data);
    }

    public function ajax_up_file(Request $request)
    {
        if ($request->has('file')) {
            $file = CommonHelper::saveFile($request->file('file'));
        }
        return $file;
    }
}
