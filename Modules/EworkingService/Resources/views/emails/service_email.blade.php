<div style="max-width:700px;margin:10px auto 20px">
    <div>
        <div style="display:inline-block;margin:0px auto">

            <table style="margin:0 auto;border-collapse:collapse;background-color:#fff;border-radius:10px">

                <tbody>
                <tr>
                    <td style="padding:10px;border-bottom:1px solid #e5e5e5">
                        <table style="width:100%;border-collapse:collapse">
                            <tbody>
                                <tr>
                                    <td style="width:140px">
                                        <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($settings['logo'])}}" class="{{$settings['name']}}">

                                    </td>
                                    <td style="padding-left:10px;font:16px/1.6 Arial;color:#000;font-weight:bold;text-align:right">Thư Xác nhận và thanh toán mua gói dịch vụ {{@setting['general_tab_name']}}!</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>


                <tr>
                    <td style="padding:0 20px">
                        <div style="margin:20px 0 0">

                            <table style="width:660px;border-collapse:collapse;word-break:break-all;box-sizing:border-box">

                                <tbody>
                                <tr>

                                    <td style="padding:0 0 10px;font-family:Arial;font-size:15px;line-height:1.6;color:#000;">

                                        Kính chào quý khách {{@$data['user']->name}},<br>

                                        Cảm ơn quý khách đã đăng ký gói dịch vụ <b>{{@$data['user']->service_name}}</b> tại {{@setting['general_tab_name']}}.<br>
                                        Thông tin thanh toán và gói dịch vụ:<br>
                                        + Tên gói: <b>{{@$data['user']->service_name}}</b>.<br>
                                        + Số tài khoản tối đa: <b>{{@$data['user']->account_max}}</b>.<br>
                                        + Thời hạn sử dụng: <b>{{@$data['user']->use_date_max}} ngày</b>.<br>
                                        + Tổng tiền thanh toán: <b>{{@$data['user']->payment}}<sup>đ</sup></b>.<br>

                                        Quý khách vui lòng thanh toán trước ngày <b>{{@$data['user']->deadline}}</b> để được kích hoạt gói dịch vụ.


                                    </td>


                                </tr>

                                <tr>

                                    <td style="font:15px/1.8 Arial;color:#000">
                                        Bạn vui lòng thanh toán vào một trong các tài khoản dưới đây:(khi chuyển khoản vui lòng ghi rõ nội dung: "Thanh toan don hang {{$data['user']->service_history_id}}").<br>
                                        Sau đó chụp ảnh màn hình chuyển tiền và gửi vào Email: {{ @$settings['email'] }}<br><br>
                                        1. BIDV - Chi nhành Hà Thành.<br>
                                        Hồ Việt Hùng.<br>
                                        Số TK: 1121 000 1397 554.<br>
                                        <br>
                                        2. VIETCOMBANK - Chi nhánh Hà Nội<br>
                                        Hồ Việt Hùng.<br>
                                        Số TK: 00210 0235 7070.<br>
                                        <br>
                                        <br>
                                        3. Techcombank - Sở giao dịch<br>
                                        Hồ Việt Hùng.<br>
                                        Số TK: 1902 9950 136013.<br>
                                        <br>
                                        <br>
                                        4. SeaBank - Chi nhánh Cầu Giấy<br>
                                        Hồ Việt Hùng.<br>
                                        Số TK: 1902 9950 136013.<br>
                                        <br>

                                    </td>

                                </tr>

                                <tr>

                                    <td style="padding:10px 0px;font:15px/1.8 Arial;font-weight:bold;color:#d23737">
                                        <b>Thông tin hỗ trợ:</b><br>
                                        Điện thoại: {{ @$settings['hotline'] }}<br>
                                        Email: {{ @$settings['email'] }}<br>
                                        Fanpage : {{ @$settings['fanpage'] }}

                                    </td>

                                </tr>

                                </tbody></table>

                        </div>



                    </td>
                </tr>


                <tr>
                    <td style="padding:10px 20px;background:#f8f8f8;border-top:1px solid #e5e5e5;border-radius:0 0 10px 10px" id="m_-3744433325623262737m_5338770243025797393m_2723588182153450401m_5561831045043488677vi-VN">
                        <table style="width:100%;border-collapse:collapse">
                            <tbody>
                            <tr>
                                <td style="width:160px;text-align:center">
                                    <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($settings['logo'])}}" class="{{$settings['name']}}">
                                </td>
                                <td style="padding-left:30px;font:15px/1.6 Arial;color:#666">
                                    <span style="font-weight:bold">{{@setting['general_tab_name']}}!</span><br>
                                    Copyright ©{{@setting['general_tab_name']}}. All Rights Reserved.
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                </tbody>
            </table>

        </div>
    </div>
</div>
