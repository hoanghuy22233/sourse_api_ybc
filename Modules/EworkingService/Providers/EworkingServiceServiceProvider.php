<?php

namespace Modules\EworkingService\Providers;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\EworkingService\Http\Controllers\Admin\MailController;
use Modules\EworkingService\Http\Controllers\Admin\ServiceHistoryController;
use Modules\EworkingService\Models\Service;

class EworkingServiceServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Custom setting
            $this->registerPermission();

            //  Sửa user_bar
            $this->rendUserBarFooter();

            //  Cấu hình menu trái
            $this->rendAsideMenu();

            //  Cấu hình dịch vụ trong setting
            $this->addSettingService();

            //  Khóa màn hình khi hết hạn dịch vụ
            $this->lockScreenWhenServiceExpires();
        }

        //  Công ty mới đăng ký => Đăng ký gói dùng thử
        \Eventy::addAction('company.register', function ($company) {
            $serviceHistoryController = new ServiceHistoryController();
            $serviceHistoryController->registerTrialService($company);
        }, 1, 1);

        //  Gửi mail đăng ký gói dịch vụ
        \Eventy::addAction('service_history.add', function ($serviceHistory) {
            $mailController = new MailController();
            $mailController->registerServiceSendMail($serviceHistory);
            return true;
        }, 1, 1);


        //  Gửi lại mail khi full  slot trong cong ty
        \Eventy::addAction('service_history.full_Slot', function ($serviceHistory) {
            $mailController = new MailController();
            $mailController->SendMailFullSlot($serviceHistory);
            return true;
        }, 1, 1);
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['service_view', 'service_add', 'service_edit', 'service_delete', 'service_history_publish',
                'service_history_view', 'service_history_add', 'service_history_edit', 'service_history_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function lockScreenWhenServiceExpires()
    {
        \Eventy::addFilter('block.main_before', function () {
            print view('eworkingservice::partials.lock_screen_when_service_expires');
        }, 1, 1);
    }

    public function rendUserBarFooter()
    {
        \Eventy::addFilter('user_bar.footer', function () {
            print '<div class="kt-notification__custom kt-space-between">
                        <a href="/admin/logout"
                           class="btn btn-label btn-label-brand btn-sm btn-bold">'.trans('admin.logout').'</a>
                    
                        <a href="/admin/service/show"
                           class="btn btn-clean btn-sm btn-bold">Nâng cấp tài khoản</a>
                    </div>';
        }, 1, 1);
    }

    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('eworkingservice::partials.aside_menu.aside_menu_dashboard_after');
        }, 2, 1);
    }

    public function addSettingService()
    {
        \Eventy::addFilter('setting.custom_module', function ($module) {
            $module['tabs']['service_tab'] = [
                'label' => 'Cấu hình dịch vụ',
                'icon' => '<i class="flaticon-mail"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'service_trial_id', 'type' => 'select2_model', 'label' => 'Gói dùng thử',
                        'model' => Service::class, 'display_field' => 'name_vi',],
                    ['name' => 'payment_before_date', 'type' => 'number', 'label' => 'Yêu cầu thanh toán trước số ngày',],
                ]
            ];
            return $module;
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__ . '/../Config/config.php' => config_path('eworkingservice.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/config.php', 'eworkingservice'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/eworkingservice');

        $sourcePath = __DIR__ . '/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/eworkingservice';
        }, \Config::get('view.paths')), [$sourcePath]), 'eworkingservice');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/eworkingservice');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'eworkingservice');
        } else {
            $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'eworkingservice');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
