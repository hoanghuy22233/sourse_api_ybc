<?php

namespace Modules\HandymanServicesDashboard\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class HandymanServicesDashboardServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('HandymanServicesDashboard', 'Database/Migrations'));

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Custom setting
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {

            $per_check = array_merge($per_check, ['dashboard','view_all_data']);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {
            print view('handymanservicesdashboard::partials.aside_menu.aside_menu_dashboard');
        }, 1, 1);
    }
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('HandymanServicesDashboard', 'Config/config.php') => config_path('handymanservicesdashboard.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('HandymanServicesDashboard', 'Config/config.php'), 'handymanservicesdashboard'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/handymanservicesdashboard');

        $sourcePath = module_path('HandymanServicesDashboard', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/handymanservicesdashboard';
        }, \Config::get('view.paths')), [$sourcePath]), 'handymanservicesdashboard');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/handymanservicesdashboard');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'handymanservicesdashboard');
        } else {
            $this->loadTranslationsFrom(module_path('HandymanServicesDashboard', 'Resources/lang'), 'handymanservicesdashboard');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('HandymanServicesDashboard', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
