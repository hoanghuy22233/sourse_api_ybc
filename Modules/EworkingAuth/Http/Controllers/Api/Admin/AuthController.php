<?php

namespace Modules\EworkingAuth\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Http\Controllers\Api\CURDBaseController;
use App\Models\Admin;
use App\Models\Company;
use App\Models\RoleAdmin;
use App\Models\Roles;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Mail;

class AdminController extends CURDBaseController
{
    protected $limit_default = 10000;

    protected $module = [
        'code' => 'admin',
        'table_name' => 'admin',
        'label' => 'Thành viên',
        'modal' => '\Modules\EworkingAdmin\Models\Admin',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'sort' => true],
            ['name' => 'name', 'type' => 'custom', 'td' => 'eworkingadmin::td.name', 'label' => 'Tên', 'sort' => true],
            ['name' => 'role_id', 'type' => 'role_name', 'label' => 'Quyền'],
            ['name' => 'tel', 'type' => 'text', 'label' => 'SĐT', 'sort' => true],
            ['name' => 'email', 'type' => 'text', 'label' => 'Email', 'sort' => true],
            ['name' => 'status', 'type' => 'custom', 'td' => 'eworkingadmin::td.status_company', 'label' => 'Trạng thái', 'sort' => true],

        ],
        'list_all' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh'],
            ['name' => 'name', 'type' => 'custom', 'td' => 'eworkingadmin::td.name', 'label' => 'Tên'],
            ['name' => 'tel', 'type' => 'text', 'label' => 'SĐT'],
            ['name' => 'email', 'type' => 'text', 'label' => 'Email'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'image', 'type' => 'file_image', 'label' => 'Ảnh đại diện'],
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Họ & tên'],
                ['name' => 'tel', 'type' => 'text', 'label' => 'tel'],
                ['name' => 'email', 'type' => 'text', 'label' => 'email'],
            ],
        ]
    ];

    protected $filter = [
        'name_vi' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'tel' => [
            'label' => 'SĐT',
            'type' => 'number',
            'query_type' => '='
        ],
        'email' => [
            'label' => 'Email',
            'type' => 'number',
            'query_type' => '='
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                1 => 'Kich hoạt',
                0 => 'Khóa',
            ]
        ],
    ];

    public function appendWhere($query, $request)
    {
        if ($request->has('company_id')) {
            $query = $query->where('company_ids', 'like', '%|' . $request->company_id . '|%');
        }
        return $query;
    }

    public function appendData($request, $data, $item = false)
    {
        if (strpos(@$data['image'], 'filemanager')) {
            $data['image'] = @explode('filemanager/userfiles/', $data['image'])[1];
        }
        if (isset($data['password']) && $data['password'] != '') {
            if ($data['password'] != $data['re_password']) {
                return [
                    'error' => false,
                    'msg' => 'Mật khẩu không khớp'
                ];
            }
            $data['password'] = bcrypt($data['password']);
        } else {

            unset($data['password']);
        }
        $data['api_token'] = base64_encode(rand(1, 100) . time());
        unset($data['re_password']);
        $data['status'] = $request->has('status') ? 1 : 0;
        return $data;
    }

    public function afterAdd($request, $item)
    {
        RoleAdmin::insert(['admin_id' => $item->id, 'role_id' => $request->role_id]);
        return true;
    }

    public function afterUpdate($request, $item)
    {
        RoleAdmin::updateOrCreate(['admin_id' => $item->id], ['role_id' => $request->role_id]);
        return true;
    }

    public function postProfile(Request $request)
    {
        try {
            $admin = Admin::select(['status', 'email', 'name', 'password', 'api_token', 'name', 'phone', 'address', 'image', 'gender', 'birthday'])->find(Auth::guard('api')->id());
            if ($request->has('name')) {
                $admin->name = $request->name;
            }
            if ($request->has('email')) {
                $admin->email = $request->email;
            }
            if ($request->has('gender')) {
                $admin->gender = $request->gender;
            }
            if ($request->has('birthday')) {
                $admin->birthday = $request->birthday;
            }
            if ($request->has('address')) {
                $admin->address = $request->address;
            }
            if ($request->has('password')) {
                if ($request->has('re_password') && $request->password == $request->re_password) {
                    $admin->password = Hash::make($request->password);
                } else {
                    return response()->json([
                        'status' => true,
                        'msg' => 'Thất bại',
                        'errors' => 'Nhập lại mật khẩu không đúng',
                        'data' => null,
                        'code' => 201
                    ]);
                }

            }
            if ($request->has('phone')) {
                $admin->phone = $request->phone;
            }
            if ($request->has('image')) {
                $file = $request->file('image');
                $file_name = $file->getClientOriginalName();
                $file_name = str_replace(' ', '', $file_name);
                $file_name_insert = date('s_i_') . $file_name;
                $file->move(base_path() . '/public/filemanager/userfiles/user', $file_name_insert);
                $admin->image = 'user/' . $file_name_insert;
            }
            $admin->save();
            if ($admin['image'] !== null) {
                $admin['image'] = asset('public/filemanager/userfiles/' . $admin['image']);
            }
            return response()->json([
                'status' => true,
                'msg' => 'Thành công',
                'errors' => (object)[],
                'data' => $admin,
                'code' => 201
            ]);

        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lấy dữ liệu không thành công',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }

    }

    public function getProfile()
    {
        try {
            $admin = Admin::select(['status', 'email', 'name', 'api_token', 'name', 'tel', 'address', 'image', 'gender', 'birthday'])->where('id', Auth::guard('api')->id())->first();
            if ($admin['image'] !== null) {
                $admin['image'] = asset('public/filemanager/userfiles/' . $admin['image']);
            }
            return response()->json([
                'status' => true,
                'msg' => 'Thành công',
                'errors' => (object)[],
                'data' => $admin,
                'code' => 201
            ]);

        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => $ex->getMessage(),
                'errors' => [
                    'exception' => [
                        'Người dùng không tồn tại'
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }

    }

    public function login(Request $request)
    {
        try {
            $admin = Admin::where('email', $request['email'])->first();
            if (!is_object($admin)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn nhập sai email',
                    'errors' => [
                    ],
                    'data' => null,
                    'code' => 401
                ]);
            }
            if (@$admin->status != 0) {
                if (\Auth::guard('admin')->attempt(['email' => trim($request['email']), 'password' => trim($request['password'])])) {
                    if ($admin['image'] !== null) {
                        $admin['image'] = asset('public/filemanager/userfiles/' . $admin['image']);
                        return response()->json([
                            'status' => true,
                            'msg' => 'Thành công',
                            'errors' => (object)[],
                            'data' => $admin,
                            'code' => 201
                        ]);
                    }
                } else {
                    return response()->json([
                        'status' => false,
                        'msg' => 'Bạn nhập sai email hoặc mật khẩu',
                        'errors' => [
                        ],
                        'data' => null,
                        'code' => 401
                    ]);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'msg' => 'Tài khoản của bạn đã bị khóa',
                    'errors' => [
                    ],
                    'data' => $admin,
                    'code' => 401
                ]);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Thất bại',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    protected function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|unique:users,email',
                'password' => 'required|min:4',
                'password_confimation' => 'required|same:password',
                'tel' => 'required|unique:users,tel',
                'address' => 'required',
            ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                    'email.required' => 'Bắt buộc phải nhập email',
                    'email.unique' => 'Địa chỉ email đã tồn tại',
                    'password.required' => 'Bắt buộc phải nhập mật khẩu',
                    'password.min' => 'Mật khẩu phải trên 4 ký tự',
                    'password_confimation.required' => 'Bắt buộc nhập lại mật khẩu',
                    'password_confimation.same' => 'Nhập lại sai mật khẩu',
                    'tel.required' => 'Bắt buộc nhập số điện thoại',
                    'tel.unique' => 'Số điện thoại đã tồn tại',
                    'address.required' => 'Bắt buộc phải nhập address',
                ]
            );

            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Thất bại',
                    'errors' => $validator->errors(),
                    'data' => null,
                    'code' => 401
                ]);
            } else {
                $data = $request->all();
                $data['password'] = bcrypt($request->password);
                $data['api_token'] = base64_encode(time() . rand(1, 100));
                if ($request->has('image')) {
                    $file = $request->file('image');
                    $file_name = $file->getClientOriginalName();
                    $file_name = str_replace(' ', '', $file_name);
                    $file_name_insert = date('s_i_') . $file_name;
                    $file->move(base_path() . '/public/filemanager/userfiles/user', $file_name_insert);
                    $data['image'] = 'user/' . $file_name_insert;
                }
                $admin = Admin::create($data);
                $admin['image'] = asset('public/filemanager/userfiles/' . $admin->image);

                return response()->json([
                    'status' => true,
                    'msg' => 'Đăng ký thành công',
                    'errors' => (object)[],
                    'data' => $admin,
                    'code' => 201
                ]);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Thất bại',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }


    }
    public function dataListTransformation($request, $listItem)
    {
        $data = [];
        foreach ($listItem as $k => $item) {
            $item->role_id = @Roles::select(['id', 'name'])->where('id', $item->role_id)->first();
            $item->last_company_id = @Company::select(['id', 'name'])->where('id', $item->last_company_id)->first();
            $item->company_ids = @Company::select(['id', 'name'])->whereIn('id', explode('|', $item->company_ids))->get();
            $item->image = CommonHelper::getUrlImageThumb($item->image,  null, null);

            $data[] = $item;
        }
        $listItem->data = $data;
        return $listItem;
    }
}

