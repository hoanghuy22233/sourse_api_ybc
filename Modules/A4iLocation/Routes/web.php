<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'province'], function () {
        Route::get('', 'Admin\ProvinceController@getIndex')->name('province')->middleware('permission:location');
        Route::get('publish', 'Admin\ProvinceController@getPublish')->name('province.publish')->middleware('permission:location');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ProvinceController@add')->middleware('permission:location');
        Route::get('delete/{id}', 'Admin\ProvinceController@delete')->middleware('permission:location');
        Route::post('multi-delete', 'Admin\ProvinceController@multiDelete')->middleware('permission:location');
        Route::get('search-for-select2', 'Admin\ProvinceController@searchForSelect2')->name('province.search_for_select2');
        Route::get('{id}', 'Admin\ProvinceController@update')->middleware('permission:location');
        Route::post('{id}', 'Admin\ProvinceController@update')->middleware('permission:location');
    });


    Route::group(['prefix' => 'district'], function () {
        Route::get('', 'Admin\DistrictController@getIndex')->name('district')->middleware('permission:location');
        Route::get('publish', 'Admin\DistrictController@getPublish')->name('district.publish')->middleware('permission:location');
        Route::match(array('GET', 'POST'), 'add', 'Admin\DistrictController@add')->middleware('permission:location');
        Route::get('delete/{id}', 'Admin\DistrictController@delete')->middleware('permission:location');
        Route::post('multi-delete', 'Admin\DistrictController@multiDelete')->middleware('permission:location');
        Route::get('search-for-select2', 'Admin\DistrictController@searchForSelect2')->name('district.search_for_select2');
        Route::get('{id}', 'Admin\DistrictController@update')->middleware('permission:location');
        Route::post('{id}', 'Admin\DistrictController@update')->middleware('permission:location');
    });

    Route::group(['prefix' => 'ward'], function () {
        Route::get('', 'Admin\WardController@getIndex')->name('ward')->middleware('permission:location');
        Route::get('publish', 'Admin\WardController@getPublish')->name('ward.publish')->middleware('permission:location');
        Route::match(array('GET', 'POST'), 'add', 'Admin\WardController@add')->middleware('permission:location');
        Route::get('delete/{id}', 'Admin\WardController@delete')->middleware('permission:location');
        Route::post('multi-delete', 'Admin\WardController@multiDelete')->middleware('permission:location');
        Route::get('search-for-select2', 'Admin\WardController@searchForSelect2')->name('ward.search_for_select2');
        Route::get('{id}', 'Admin\WardController@update')->middleware('permission:location');
        Route::post('{id}', 'Admin\WardController@update')->middleware('permission:location');
    });
});