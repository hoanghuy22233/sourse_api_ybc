<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::group(['prefix' => 'wards'], function () {
        Route::get('', 'Admin\WardController@index');
    });
    Route::group(['prefix' => 'districts'], function () {
        Route::get('', 'Admin\DistrictController@index');
    });
    Route::group(['prefix' => 'provinces'], function () {
        Route::get('', 'Admin\ProvinceController@index');
    });
});