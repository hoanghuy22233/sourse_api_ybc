<?php

namespace Modules\A4iLocation\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
	protected $table = 'districts';
    public $timestamps = false;

    public function province() {
        return $this->belongsTo(Province::class, 'province_id');
    }
}
