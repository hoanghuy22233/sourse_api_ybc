<?php

namespace Modules\ThemeWorkart\Entities;

use Illuminate\Database\Eloquent\Model;

class UserReview extends Model
{
    protected $fillable = [ 'user_id' , 'product_id' , 'review' , 'star' , 'created_at' , 'updated_at' ];


}
