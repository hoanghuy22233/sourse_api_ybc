<?php
/**
 * Created by PhpStorm.
 * User: thanhphong
 * Date: 14/05/2016
 * Time: 22:13
 */
namespace Modules\ThemeWorkart\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model {

    protected $table = 'reviews';

    protected $fillable = [
        'product_id',	'description',	'name',	'email',	'star',	'picture'
    ];


    public function product() {
        return $this->belongsTo(Product::class, 'product_id');
    }


}