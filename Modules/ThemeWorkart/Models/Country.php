<?php

/**
 * Country Model
 *
 * Country Model manages Country operation.
 *
 * @category   Country
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\ThemeWorkart\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'country';
    protected $guarded=[];
    public function getImageUrlAttribute()
    {
        return url('/').'/public/filemanager/userfiles/slides/'.$this->attributes['image'];
    }
}
?>
