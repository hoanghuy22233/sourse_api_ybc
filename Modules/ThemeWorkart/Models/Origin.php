<?php

namespace Modules\ThemeWorkart\Models;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
   protected $table = 'origins';
}
