<?php

/**
 * Page Model
 *
 * Page Model manages page operation. 
 *
 * @category   Page
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license    
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\ThemeWorkart\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $table = 'authors';

    public function products() {
        return $this->hasMany(Product::class, 'author_id', 'id');
    }
}
