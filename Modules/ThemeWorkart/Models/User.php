<?php

/**
 * User Model
 *
 * User Model manages User operation. 
 *
 * @category   User
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license    
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\ThemeWorkart\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\Helpers\CommonHelper;
use \Modules\ThemeWorkart\Models\UserDetails;
//use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable;
//    use Billable;

    protected $fillable = [
        'name', 'email', 'password', 'tel', 'image', 'balance', 'status', 'stripe_id', 'card_brand', 'card_last_four', 'trial_ends_at', 'plan', 'address'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function childs()
    {
        return $this->hasMany($this, 'parent_id', 'id')->orderBy('order_no', 'asc');
    }
}
