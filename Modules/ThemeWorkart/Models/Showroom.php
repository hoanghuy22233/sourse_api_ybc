<?php

namespace Modules\ThemeWorkart\Models;

use Illuminate\Database\Eloquent\Model;

class Showroom extends Model
{
    protected $table = 'showrooms';
}
