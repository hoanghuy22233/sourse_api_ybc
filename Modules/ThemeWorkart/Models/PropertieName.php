<?php

namespace Modules\ThemeWorkart\Models;

use Illuminate\Database\Eloquent\Model;

class PropertieName extends Model
{
    protected $table = 'properties_name';
    protected $fillable = ['name', ''];

    public $timestamps = false;
}
