jQuery(document).ready(function ($) {
    var moveOnMenuOpen = $(".mobileNavWrapper, #page"), mobileSize, showSub = false;
    //slider home
    $('.owl-carousel.slider').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: true
            }
        }
    });

    //slider product
    $('.owl-carousel.slider-product').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 4,
                nav: true,
                loop: true
            }
        }
    });

    $('.owl-carousel .owl-nav button.owl-prev').html('<i class="fa fa-angle-left"></i>');
    $('.owl-carousel .owl-nav button.owl-next').html('<i class="fa fa-angle-right"></i>');

    //click menu
    //click meu
    function detectWindowSize() {
        if ($(window).width() <= 850) {
            mobileSize = true;
        } else {
            mobileSize = false;
            moveOnMenuOpen.removeClass("showNav");
        }

        if ($(window).width() < 985) {
            offsetVal = 122;
        } else {
            offsetVal = 115;
        }
    }

    // run this on page load
    detectWindowSize();
    // show the main menu when you click the menu button
    $(".menu-click i").on("click", function (e) {
        e.stopPropagation();
        moveOnMenuOpen.toggleClass("showNav");
    });

    // hide the menu on page click/tap if mobile menu is showing
    $("#page").on("click", function () {
        if (mobileSize) {
            moveOnMenuOpen.removeClass("showNav");
        }
    });

    // show sub menu
    $('.mobileNavWrapper ul > li > a').on('click', function (e) {
        e.stopPropagation();
        e.preventDefault();
        if ($(this).parent().hasClass('active')) {
            $(this).parent().removeClass('active');
        } else {
            $('.mobileNavWrapper ul > li > a').parent().removeClass('active');
            $(this).parent().addClass('active');
        }
    });
});

//slider jquery gallery product
jQuery(document).ready(function ($) {
    var sync1 = $("#sync1");
    var sync2 = $("#sync2");
    var slidesPerPage = 4; //globaly define number of elements per page
    var syncedSecondary = true;

    sync1.owlCarousel({
        items: 1,
        slideSpeed: 2000,
        nav: false,
        autoplay: false,
        dots: false,
        loop: true,
        responsiveRefreshRate: 200,
        navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
    }).on('changed.owl.carousel', syncPosition);

    sync2
        .on('initialized.owl.carousel', function () {
            sync2.find(".owl-item").eq(0).addClass("current");
        })
        .owlCarousel({
            items: slidesPerPage,
            dots: false,
            nav: true,
            smartSpeed: 200,
            slideSpeed: 500,
            slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
            responsiveRefreshRate: 100
        }).on('changed.owl.carousel', syncPosition2);

    function syncPosition(el) {
        //if you set loop to false, you have to restore this next line
        //var current = el.item.index;

        //if you disable loop you have to comment this block
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - (el.item.count / 2) - .5);

        if (current < 0) {
            current = count;
        }
        if (current > count) {
            current = 0;
        }

        //end block

        sync2
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
        var onscreen = sync2.find('.owl-item.active').length - 1;
        var start = sync2.find('.owl-item.active').first().index();
        var end = sync2.find('.owl-item.active').last().index();

        if (current > end) {
            sync2.data('owl.carousel').to(current, 100, true);
        }
        if (current < start) {
            sync2.data('owl.carousel').to(current - onscreen, 100, true);
        }
    }

    function syncPosition2(el) {
        if (syncedSecondary) {
            var number = el.item.index;
            sync1.data('owl.carousel').to(number, 100, true);
        }
    }

    sync2.on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = $(this).index();
        sync1.data('owl.carousel').to(number, 300, true);
    });

    //tab order product user
    function activeTab(obj)
    {
        $('.tab-product ul li').removeClass('active');
        $(obj).addClass('active');
        var id = $(obj).find('a').attr('href');
        $('.tab-item').hide();
        $(id) .show();
    }

    // Sự kiện click đổi tab
    $('.tab-product ul li').click(function(){
        activeTab(this);
        return false;
    });

    // Active tab đầu tiên khi trang web được chạy
    activeTab($('.tab-product ul li:first-child'));
});
