{{--<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/home.css')}}">--}}
{{--<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/footer.css')}}">--}}
{{--<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/slide.css')}}">--}}
{{--<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/detailproduct.css')}}">--}}
{{--<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/frontend/css/style-responsive.css')}}">--}}
{{--<script type="text/javascript" src="{{URL::asset('public/frontend/themes/stbd/front/js/jquery.js')}}"></script>--}}

<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/workart/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/workart/js/owl/dist/assets/owl.theme.default.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/workart/js/owl/dist/assets/owl.carousel.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/workart/fonts/awesome/css/font-awesome.css')}}">

<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/workart/css/main.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/workart/css/responsive.css')}}">
<script type="text/javascript" src="{{URL::asset('public/frontend/themes/workart/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/frontend/themes/workart/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/frontend/themes/workart/js/owl/dist/owl.carousel.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/frontend/themes/workart/js/main.js')}}"></script>

{!! @$settings['frontend_head_code'] !!}
