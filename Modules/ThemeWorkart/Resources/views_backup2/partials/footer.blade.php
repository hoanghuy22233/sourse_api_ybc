<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-2 col-lg-2 col-xl-2 mt-mobile">
                <div class="footer-top">
                    <div class="footer-title"><h3>Giới thiệu</h3></div>
                    <div class="footer-menu">
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li><a href="">Faqs</a></li>
                            <li><a href="">Location</a></li>
                            <li><a href="">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-2 col-lg-2 col-xl-2 mt-mobile">
                <div class="footer-top">
                    <div class="footer-title"><h3>Về chúng tôi</h3></div>
                    <div class="footer-menu">
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li><a href="">Faqs</a></li>
                            <li><a href="">Location</a></li>
                            <li><a href="">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 col-xl-4 mt-mobile">
                <div class="footer-top">
                    <div class="footer-title"><h3>Subcribe</h3></div>
                    <div class="form-subcribe">
                        <div class="form-group">
                            <input type="email" class="form-control input-email" placeholder="Email của bạn">
                            <button type="submit" class="btn-register">Đăng ký</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-4 col-xl-4 mt-mobile">
                <div class="footer-top">
                    <div class="footer-title"><h3>Địa chỉ liên hệ</h3></div>
                    <div class="footer-address">
                        <p>305 County St.Sugar Land, TX 77479, United State</p>
                        <p>Email: info@website.com</p>
                        <p>Số điện thoại: 1900 88 68 90</p>
                    </div>
                    <div class="footer-social">
                        <ul>
                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram"></i></a></li>
                            <li><a href=""><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-absolute">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12 col-md-12 col-xl-12">
                    &copy; Bản quyền thuộc về <b>Website</b>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<!--footer-->
