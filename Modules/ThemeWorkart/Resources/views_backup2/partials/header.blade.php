{{--header--}}
@php
    $brans = CommonHelper::getFromCache('get_brans');
    if (!$brans){
        $brans = \Modules\ThemeWorkart\Models\Manufacturer::where('status', 1)->orderBy('order_no', 'asc')->get();
        CommonHelper::putToCache('get_brans', $brans);
    }

    $show_bran_homepage = CommonHelper::getFromCache('settings_name_show_bran_homepage_type_homepage_tab');
    if (!$show_bran_homepage){
        $show_bran_homepage = @\Modules\ThemeWorkart\Models\Settings::where('name','show_bran_homepage')->where('type','homepage_tab')->first()->value;
        CommonHelper::putToCache('settings_name_show_bran_homepage_type_homepage_tab', $show_bran_homepage);
    }

    $menus = \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getMenusByLocation('main_menu', 10, false);
@endphp


<div class="mobileNavWrapper">
    <nav class="menu-main">
        <ul>
            @if(!empty($menus))
                @foreach($menus as $menu1)
                    @if($menu1['link'] == '#')
                        <li class="nav-item menu-{{ str_slug($menu1['name'], '_') }}">
                            <a class="nav-link" href="javascript:;"
                               @if($menu1['name'] != 'Sản phẩm') onclick="showmenudv();"
                               @endif title="{{$menu1['name']}}">{{$menu1['name']}}</a>
                            @if(!isMobile())
                                @if(@$settings['option_menu'] == 1 && str_slug($menu1['name'], '_')=='san_pham')
                                    @include('themeworkart::partials.menu_efect.menu_dropdown')
                                @endif
                            @endif
                        </li>
                    @else
                        <li class="nav-item menu-{{ str_slug($menu1['name'], '_') }} menu-item">
                            <a class="nav-link" href="{{ $menu1['link'] }}"
                               title="{{$menu1['name']}}">{{$menu1['name']}}</a>
                            @if(!empty($menu1['childs']))
                                <ul class="sub-menu">
                                    @foreach($menu1['childs'] as $c => $menu2)
                                        <li class="sub-menu-item">
                                            <a href="{{$menu2['link']}}">{{$menu2['name']}}</a>
                                            @if(!empty($menu2['childs']))
                                                <ul class="sub-menu3">
                                                    @foreach($menu2['childs'] as $c3 => $menu3)
                                                        <li class="sub-menu-item3">
                                                            <a href="{{$menu3['link']}}">{{$menu3['name']}}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>

                            @endif
                        </li>
                    @endif
                @endforeach
            @endif
        </ul>
    </nav>
</div>

<div id="page" class="hideMenu">
    <header id="header">
        <div class="header-main">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-2 col-lg-2 col-xl-2">
                        <a href="/"><img class="lazy" data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo_brand'])  }}" alt="{{ @$settings['logo_brand_intro'] }}" title="{{ @$settings['logo_brand_intro'] }}" ></a>
                    </div>
                    <div class="col-12 col-md-7 col-lg-7 col-xl-7">
                        <form action="search">
                            <div class="form-group" style="position: relative;" id="form-search">
                                <input type="search" name="key" placeholder="Nhập từ khoá tìm kiếm" class="form-control input-s">
                                <button type="submit" id="btn-s"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-12 col-md-3 col-lg-3 col-xl-3">
                        <div class="header-right">
                            <ul>
                                <li><a href="/my-account"><i class="fa fa-user-o"></i> Đăng nhập</a></li>
                                <li class="cart-icon">
                                    <a href="/cart"><i class="fa fa-shopping-cart"></i>
                                        <span class="cart-count">{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::cartGetTotalItem()  }}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-9 col-lg-9 col-xl-9">
                        <nav id="menu-main">
                            <ul>
                                <li>
                                    <div class="select-redirect select">
                                        <select class="header-select">
                                            <option value="">Shop</option>
                                            <option value="">Dog</option>
                                            <option value="">Cart</option>
                                        </select>
                                    </div>
                                </li>


                                @if(!empty($menus))
                                    @foreach($menus as $menu1)
                                        @if($menu1['link'] == '#')
                                            <li class="nav-item menu-{{ str_slug($menu1['name'], '_') }}">
                                                <a class="nav-link" href="javascript:;"
                                                   @if($menu1['name'] != 'Sản phẩm') onclick="showmenudv();"
                                                   @endif title="{{$menu1['name']}}">{{$menu1['name']}}</a>
                                                @if(!isMobile())
                                                    @if(@$settings['option_menu'] == 1 && str_slug($menu1['name'], '_')=='san_pham')
                                                        @include('themeworkart::partials.menu_efect.menu_dropdown')
                                                    @endif
                                                @endif
                                            </li>
                                        @else
                                            <li class="nav-item menu-{{ str_slug($menu1['name'], '_') }} menu-item">
                                                <a class="nav-link" href="{{ $menu1['link'] }}"
                                                   title="{{$menu1['name']}}">{{$menu1['name']}}</a>
                                                @if(!empty($menu1['childs']))
                                                    <ul class="sub-menu">
                                                        @foreach($menu1['childs'] as $c => $menu2)
                                                            <li class="sub-menu-item">
                                                                <a href="{{$menu2['link']}}">{{$menu2['name']}}</a>
                                                                @if(!empty($menu2['childs']))
                                                                    <ul class="sub-menu3">
                                                                        @foreach($menu2['childs'] as $c3 => $menu3)
                                                                            <li class="sub-menu-item3">
                                                                                <a href="{{$menu3['link']}}">{{$menu3['name']}}</a>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ul>

                                                @endif
                                            </li>
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                        </nav>
                    </div>
                    <div class="col-12 col-md-3 col-lg-3 col-xl-3">
                        <div class="header-action">
                            <ul>
                                <li><a href="/track-order">Check Order</a></li>
                                <li><a href="/faqs">FAQS</a></li>
                                <li>
                                    <div class="select-redirect select curency">
                                        <select class="header-select">
                                            <option value="">USD</option>
                                            <option value="">VND</option>
                                        </select>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-mobile">
            <a href="javascript:void(0)" class="menu-click"><i class="fa fa-bars"></i></a>
            <a href="/"><i class="fa fa-user-o"></i></a>
            <a href="/"><i class="fa fa-shopping-cart"></i></a>
        </div>
    </header>
{{--    <!--header-->--}}
