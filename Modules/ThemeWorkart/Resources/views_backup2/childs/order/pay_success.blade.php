@extends('themeworkart::layouts.master')
@section('main_content')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="path">
                        <ul class="clearfix">
                            <li>Thanh toán thành công</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="box-white mt30 pb30 clearfix">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="dnBox clearfix">
                                @if(session('success')) <span
                                        class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
                                @if(session('error')) <span
                                        class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('custom_header')
    <style>
        section.content {
            min-height: 500px;
        }
    </style>
@endsection

@section('custom_footer')

@endsection