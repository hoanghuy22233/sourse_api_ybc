@extends( 'themeworkart::layouts.master' )

@section( 'main_content' )
    <main id="main">
        <section id="category-page">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                        @include( 'themeworkart::partials.breadcrumb' )
                        <!--breadcrumb-->
                        <div class="page-order">
                            <div class="row">
                                <div class="col-12 col-md-6 col-lg-6 col-xl-6">
                                    <form class="form-order">
                                        <h2>Track order</h2>
                                        <div class="form-group">
                                            <label>Nhập email và ID đơn hàng để kiểm tra trạng thái đơn hàng</label>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" placeholder="Email" class="form-control input-text">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" placeholder="ID đơn hàng" class="form-control input-text">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success" id="btn-login">Kiểm tra</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-12 col-md-6 col-lg-6 col-xl-6">
                                    <div class="banner-order">
                                        <img src="https://via.placeholder.com/1200x1200?text=Banner" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!--main-->
@endsection
