@extends('themeworkart::layouts.master')
@section('main_content')
        <section id="category-page">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                            @include('themeworkart::partials.breadcrumb')
                        <!--breadcrumb-->
                        <div class="dtop">
                            <div class="ds">
                               <h3> List of <b>{{$count}} </b> items for: </h3>
                            </div>
                        </div>

                        <div class="category-search">
                            <form action="">
                                <input type="text" name="key" value="{{$keyword}}" class="form-control">
                                <button type="submit" class="btnSearchCat"><i class="fa fa-search"></i></button>
                            </form>
                        </div>

                        <div class="category-list">
                            <div class="row row-5">
                                    @if(!empty($products))
                                        @php
                                            $getId = '';
                                                foreach ($products as $product){
                                                $getId .= $product->proprerties_id;
                                              }
                                            $getId = array_unique(explode('|', $getId));
                                             if ($getId['0'] == ''){
                                                    array_shift($getId);
                                                }
                                        @endphp
                                        @foreach($products as $featured)
                                        <div class="col-12 col-lg-3 col-md-3 col-xl-3 pd-5">
                                             @include( 'themeworkart::childs.product.partials.product_loop' )
                                        </div>
                                    @endforeach
                                    @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pagination" style="clear: both">
                    <div style="margin: auto; text-align: center">{!! $products->appends(['key' => Request::get('key')])->links() !!}</div>
                </div>

            </div>
        </section>


    <style>

    </style>
@endsection
