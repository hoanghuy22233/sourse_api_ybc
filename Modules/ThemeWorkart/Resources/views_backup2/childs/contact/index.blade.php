@extends('themeworkart::layouts.master')

@section('main_content')

    <!-- ******CONTENT****** -->

    <div class="content container">
        @if(session('success')) <span
                class="alert alert-success col-xs-12">{{session('success')}}</span>@endif
        @if(session('error')) <span
                class="alert alert-danger col-xs-12">{{session('error')}}</span>@endif

        <div class="vc_row wpb_row vc_row-fluid vc_custom_1435659486461" style="display: inline-block; width: 100%;">
            <div class="stm_icon_box_responsive wpb_column vc_column_container col-sm-6">
                <div class="wpb_wrapper">
                    <div class="vc_custom_heading vc_custom_1435665468258"><h3
                                style="text-align: left;font-family:Montserrat;font-weight:700;font-style:normal">
                            LIÊN HỆ:</h3></div>

                    <div class="icon_box vc_custom_1435658011508 standart clearfix"
                         style="background:#ffffff; color:#555555">
                        <div class="icon_alignment_left">
                            <div class="icon vc_custom_1435658011507" style="width:35px">
                                <i style="font-size: 35px; color:#eab830" class="fa fa-icon-stm_icon_pin-o"></i>
                            </div>

                            <div class="icon_text">
                                <h5 style="color:#555555">Địa chỉ:</h5>
                                <p>{{ $setting->address }}</p>
                            </div>
                        </div> <!-- align icons -->
                    </div>

                    <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1435139343164">
                        <div class="stm_sm_gutter_back wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6 vc_custom_1436791397964">
                            <div class="wpb_wrapper">

                                <div class="icon_box vc_custom_1435665617764 dark clearfix"
                                     style="background:#ffffff; color:#555555">
                                    <div class="icon_alignment_left">
                                        <div class="icon vc_custom_1435665617761" style="width:35px">
                                            <i style="font-size: 33px; color:#eab830"
                                               class="fa fa-icon-stm_icon_phone-o"></i>
                                        </div>

                                        <div class="icon_text">
                                            <h5 style="color:#555555">Điện thoại:</h5>
                                            <p><a href="tel:+8 (800) 659-2684">{{ $setting->hotline }}</a></p>
                                        </div>
                                    </div> <!-- align icons -->
                                </div>

                            </div>
                        </div>
                        <div class="stm_sm_gutter_back wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6 vc_custom_1436790977027">
                            <div class="wpb_wrapper">

                                <div class="icon_box vc_custom_1435665538825 dark clearfix"
                                     style="background:#ffffff; color:#555555">
                                    <div class="icon_alignment_left">
                                        <div class="icon vc_custom_1435665538824" style="width:61px">
                                            <i style="font-size: 33px; color:#eab830"
                                               class="fa fa-icon-stm_icon_skype-o"></i>
                                        </div>
                                    </div> <!-- align icons -->
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1435139343164">
                        <div class="stm_sm_gutter_back stm_bottom_border wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6 vc_custom_1436791452718">
                            <div class="wpb_wrapper">

                                <div class="icon_box vc_custom_1435657717792 dark clearfix"
                                     style="background:#ffffff; color:#555555">
                                    <div class="icon_alignment_left">
                                        <div class="icon vc_custom_1435657717791" style="width:35px">
                                            <i style="font-size: 22px; color:#eab830"
                                               class="fa fa-icon-stm_icon_mail-o"></i>
                                        </div>

                                        <div class="icon_text">
                                            <h5 style="color:#555555">Email:</h5>
                                            <p><a href="{{ $setting->email }}">{{ $setting->email }}</a></p>
                                        </div>
                                    </div> <!-- align icons -->
                                </div>

                            </div>
                        </div>
                        <div class="stm_sm_gutter_back wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6 vc_custom_1436791272952">
                            <div class="wpb_wrapper">

                                <div class="icon_box vc_custom_1435657734648 dark clearfix"
                                     style="background:#ffffff; color:#555555">
                                    <div class="icon_alignment_left">
                                        <div class="icon vc_custom_1435657734647" style="width:61px">
                                            <i style="font-size: 33px; color:#eab830"
                                               class="fa fa-icon-stm_icon_earth"></i>
                                        </div>
                                    </div> <!-- align icons -->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container col-sm-6">
                <div class="wpb_wrapper">
                    <div class="vc_custom_heading vc_custom_1435665472476"><h3
                                style="text-align: left;font-family:Montserrat;font-weight:700;font-style:normal">
                            GOOGLE MAP:</h3></div>
                    <div class="wpb_gmaps_widget wpb_content_element">
                        <div class="wpb_wrapper">
                            <div class="wpb_map_wraper">
                                {!! $setting->google_map_link !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row wpb_row vc_row-fluid">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="wpb_wrapper">
                    <div class="multiseparator vc_custom_1435665826984"></div>
                </div>
            </div>
        </div>
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1435665045394">
            <div class="custom-border wpb_column vc_column_container vc_col-sm-8 vc_custom_1437478190523">
                <div class="wpb_wrapper">
                    <div class="vc_custom_heading vc_custom_1435665759680"><h3
                                style="text-align: left;font-family:Montserrat;font-weight:700;font-style:normal">
                            GỬI PHẢN HỒI</h3></div>
                    <div role="form" class="wpcf7" id="wpcf7-f817-p793-o1" lang="en-US" dir="ltr">
                        <div class="screen-reader-response"></div>
                        <form action="" method="post" class="wpcf7-form"
                              novalidate="novalidate">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <span class="wpcf7-form-control-wrap name"><input type="text" name="name"
                                                                                          value="" size="40"
                                                                                          class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control"
                                                                                          aria-required="true"
                                                                                          aria-invalid="false"
                                                                                          placeholder="Your Name"></span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <span class="wpcf7-form-control-wrap email"><input type="email" name="email"
                                                                                           value="" size="40"
                                                                                           class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control"
                                                                                           aria-required="true"
                                                                                           aria-invalid="false"
                                                                                           placeholder="Your Email"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <span class="wpcf7-form-control-wrap message"><textarea name="content" cols="40"
                                                                                                rows="9"
                                                                                                class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control"
                                                                                                aria-required="true"
                                                                                                aria-invalid="false"></textarea></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <input type="submit" value="Submit"
                                           class="wpcf7-form-control wpcf7-submit btn btn-default"><img
                                            class="lazy ajax-loader"
                                            data-src="https://old.englishcamp.edu.vn/wp-content/themes/masterstudy/assets/img/ajax-loader.gif"
                                            alt="Đang gửi ..." style="visibility: hidden;">
                                </div>
                            </div>
                            <div class="wpcf7-response-output wpcf7-display-none"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <div class="clearfix">
        </div>


    </div><!--//content-->

@stop