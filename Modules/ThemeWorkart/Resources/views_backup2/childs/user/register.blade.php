@extends('themeworkart::layouts.master')

@section('main_content')
    <main id="main">
        <section id="category-page">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                     @include('themeworkart::partials.breadcrumb')
                     <!--breadcrumb-->
                        <div class="page-login">
                            <div class="row">
                                <div class="col-12 col-md-8 offset-md-2">
                                    <div class="form-login">
                                        <h2>Tạo tài khoản mới</h2>
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Họ" class="form-control input-text">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Tên" class="form-control input-text">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <div class="form-group">
                                                    <input type="email" placeholder="Email" class="form-control input-text">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <div class="form-group">
                                                    <input type="password" placeholder="Mật khẩu" class="form-control input-text">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-12 col-md-3 col-lg-3">
                                                            <button type="submit" class="btn btn-success" id="btn-login">Đăng
                                                                ký
                                                            </button>
                                                        </div>
                                                        <div class="col-12 col-md-9 col-lg-9">
                                                            <div class="login-text">
                                                                Bạn có tài khoản <a href="/my-account">Đăng nhập</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
