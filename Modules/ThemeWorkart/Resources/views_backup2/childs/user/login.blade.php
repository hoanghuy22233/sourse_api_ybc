@extends('themeworkart::layouts.master')

@section('main_content')
    <main id="main">
        <section id="category-page">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                    @include('themeworkart::partials.breadcrumb')
                        <!--breadcrumb-->
                        <div class="page-login">
                            <div class="row">
                                <div class="col-12 col-md-6 offset-md-3">
                                    <div class="form-login">
                                        <h2>Đăng nhập</h2>
                                        <div class="form-group">
                                            <input type="email" placeholder="Email" class="form-control input-text">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" placeholder="Mật khẩu"
                                                   class="form-control input-text">
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-12 col-md-5 col-lg-5">
                                                    <button type="submit" class="btn btn-success" id="btn-login">Đăng
                                                        nhập
                                                    </button>
                                                </div>
                                                <div class="col-12 col-md-7 col-lg-7">
                                                    <div class="login-text">
                                                        Bạn chưa có tài khoản <a href="/my-account/register" >Tạo tài khoản?</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
