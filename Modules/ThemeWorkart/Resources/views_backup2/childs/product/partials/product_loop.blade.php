<div class="product-item {{ $item_class or ''   }}">
    <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($featured, 'array') }}"
       title="{{@$featured->meta_title}}">
        <div class="product-avatar">
            <img class="lazy img-responsive"
                 data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($featured->image, 150 ,null) }}"
                 alt="{{$featured->name}}"/>
        </div>

        <div class="product-content">
            <h3>
                <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($featured, 'array') }}"
                   title="{{@$featured->meta_title}}">{{$featured->name}}</a>
            </h3>

            <div class="product_price">
                @if($featured->final_price != 0)
                    <span class="price">
                            @if($featured->final_price < $featured->base_price) @else
                        @endif
                        {{number_format($featured->final_price, 0, '.', '.')}} USD
                    </span>
                    @else
                    </br> <p class="final_price" class="pr">not set</p>
                @endif

                @if($featured->final_price < $featured->base_price)
                    <del class="base_price">{{number_format($featured->base_price, 0, '.', '.')}}</del>
                @endif

                @if($featured->final_price < $featured->base_price)
                    <div class="on_sale"><span>{{  100 - ( round(($featured->final_price/$featured->base_price) * 100 )) }}%</span>
                        Off
                    </div>
                @else
                    <div style="height: 24px;border-top: none"></div>
                @endif
            </div>

            <div class="rating_wrap">
                <div class="rating">
                    <div class="product_rate" style="width:68%"></div>
                </div>
                <span class="rating_num">(15)</span>
            </div>


            <div class="pr_switch_wrap">
                <div class="product_color_switch">
                                                    <span class="active" data-color="#87554B"
                                                          style="background-color: rgb(135, 85, 75);"></span>
                    <span data-color="#333333"
                          style="background-color: rgb(51, 51, 51);"></span>
                    <span data-color="#5FB7D4"
                          style="background-color: rgb(95, 183, 212);"></span>
                </div>
            </div>
        </div>
    </a>
</div>
