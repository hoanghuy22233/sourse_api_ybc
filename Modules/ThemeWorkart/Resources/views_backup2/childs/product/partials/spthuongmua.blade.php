@if(@$settings['related_product'] == 1 && isset($relate_products) && count($relate_products) > 0)
    <div class="sp_lien_quan" style="display: inline-block;width: 100%;">
        <span class="dtit">Sản phẩm thường mua</span>
        <div class="ss" id="SPTT2">
            <span class="psback"></span>
            <div class="pspanel">
                <div class="pswrap">

                    @foreach( $relate_products as $relate_product)
                        @php
                            $cate_slug =\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getFromCache('get_slug_cate_by_sptt'.@$relate_product->id);
                            if (!$cate_slug){
                                $cate_slug = \Modules\ThemeWorkart\Models\Category::whereIn('id', explode('|', @$relate_product->multi_cat))->first();
                               \Modules\ThemeWorkart\Http\Helpers\CommonHelper::putToCache('get_slug_cate_by_sptt'.@$relate_product->id, @$cate_slug);
                            }
                        @endphp
                        <a class="psitem"
                           href="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($relate_product) }}"
                           title="{{$relate_product->name}}">

                            <div class="pi">
                                <img alt="{{@$relate_product->name}}"
                                     class="lazy"
                                     data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb(@$relate_product->image, 160, null) }}"
                                     data-src="{{@$relate_product->image}}"/>
                            </div>
                            <p class="pn">{{@$relate_product->name}}</p>
                            <span class="pr">{{number_format(@$relate_product->final_price, 0, '.', '.')}}<sup
                                        style="margin-left: 5px;">đ</sup></span>
                        </a>
                    @endforeach
                </div>
            </div>
            <span class="psnext"></span>
        </div>
    </div>
@endif

<aside class="widget" id="widget-product">
    <div class="widget-title">
        <h3>Sản phẩm thường mua</h3>
    </div>
    <div class="product-list">


        @if(@$settings['related_product'] == 1 && isset($relate_products) && count($relate_products) > 0)
            @foreach( $relate_products as $featured)
                <div class="product-wd-item">
                    <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($featured, 'array') }}"  title="{{@$featured->meta_title}}">
                        <div class="product-avatar">
                            <img class="lazy img-responsive"
                                 data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($featured->image, 150 ,null) }}"
                                 alt="{{$featured->name}}"/>
                        </div>

                        <div class="product-content">
                            <h3>
                                <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($featured, 'array') }}"
                                   title="{{@$featured->meta_title}}">{{$featured->name}}</a>
                            </h3>

                            <div class="product_price">
                                @if($featured->final_price != 0)
                                    <span class="price">
                                                        @if($featured->final_price < $featured->base_price) @else
                                        @endif
                                        {{number_format($featured->final_price, 0, '.', '.')}}
                                                    </span>
                                    @else
                                    </br> <p  class="final_price" class="pr">not set</p>
                                @endif

                                @if($featured->final_price < $featured->base_price)
                                    <del  class="base_price">{{number_format($featured->base_price, 0, '.', '.')}}</del>
                                @endif

                                @if($featured->final_price < $featured->base_price)
                                    <div class="on_sale"><span>{{  100 - ( round(($featured->final_price/$featured->base_price) * 100 )) }}%</span> Off</div>
                                @else
                                    <div style="height: 24px;border-top: none"></div>
                                @endif
                            </div>

                            <div class="rating_wrap">
                                <div class="rating">
                                    <div class="product_rate" style="width:68%"></div>
                                </div>
                                <span class="rating_num">(15)</span>
                            </div>


                            <div class="pr_switch_wrap">
                                <div class="product_color_switch">
                                                    <span class="active" data-color="#87554B"
                                                          style="background-color: rgb(135, 85, 75);"></span>
                                    <span data-color="#333333"
                                          style="background-color: rgb(51, 51, 51);"></span>
                                    <span data-color="#5FB7D4"
                                          style="background-color: rgb(95, 183, 212);"></span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
        @endif

    </div>
</aside>
