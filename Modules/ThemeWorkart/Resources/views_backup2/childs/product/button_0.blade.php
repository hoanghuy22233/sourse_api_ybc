
<style>
#urlCart{
    margin-bottom: 10px;
}
</style>
<div class="f isdt">
    <label class="label01">ĐĂNG KÝ MUA HÀNG</label>
    <label class="label02">NHẬN ƯU ĐÃI 5-10%</label>
    <small>Ưu đãi đặc quyền chỉ dành cho 100 khách hàng đầu tiên</small>
    <div class="form-group form-checksales-detail">
        <form method="post" class="sell" action="{{route('resign.contact')}}">
            <input type="hidden" name="ProductID" value="{{@$product->id}}">
            <input type="hidden" name="Product" value="{{@$product->name}}">
            <input type="hidden" name="State" value="Nhận ưu đãi ">
            <select name="province_uu_dai" id="province_uu_dai" required="required"  style="
    padding: 5px;
    margin-bottom: 10px;
    width: 100%;
">              <option value="" selected>Chọn tỉnh/thành</option>
                @foreach($province as $pp)
                    <option value="{{$pp->id}}">{{$pp->name}}</option>
                @endforeach
            </select>
            <input type="tel" pattern="(09|01[2|6|8|9])+([0-9]{8})\b"
                   title="Có 10 hoặc 11 số bắt đầu bằng 0" name="Phone" required="required"
                   class="form-controls Phone" placeholder="Nhập số điện thoại.">

            <input style="bottom: 3px; top:unset" class="btn btn-check-submit" type="submit" name="_w_action[AddPOST]"
                   value="ĐĂNG KÝ NGAY">
        </form>
    </div>
    <div class="clearfix"></div>
</div>
<div class="da">

    <p id="urlCart" data-text="{{route('order.view')}}"
       onclick="(addCart({{@$product->id}}))" style="color:white" class="btn3 bxanh">MUA
        NGAY<span>(Xem hàng, không mua không sao)</span>
    </p>
    @if(@$settings['show_khao_sat'] == 1)
        <a style="display:block;margin-bottom:10px;" class="btn3 bcam"
           href="{{(@$settings['link_khao_sat']!='')?@$settings['link_khao_sat']:'#'}}" title="Khảo sát tư vấn lắp đặt">KHẢO
            SÁT TƯ VẤN LẮP ĐẶT MIỄN PHÍ<span>(Dịch vụ khảo sát tư vấn miễn phí)</span></a>
    @endif
    @if(@$settings['show_tra_gop'] == 1)
        <a style="display:block; margin-bottom:10px;" class="btn3 bcam mua_tra_gop_btn"
           href="{{(@$settings['link_tra_gop']!='') ? @$settings['link_tra_gop']:'#'}}" title="Mua trả góp">MUA TRẢ GÓP</a>
    @endif
</div>
{{--@include('themeworkart::childs.product.mua_tra_gop')--}}
{{--<script>--}}
{{--    $(document).ready(function(){--}}
{{--        $('.mua_tra_gop_btn').click(function(){--}}
{{--            $('.mua-tra-gop-form').show();--}}
{{--        })--}}
{{--    })--}}
{{--</script>--}}