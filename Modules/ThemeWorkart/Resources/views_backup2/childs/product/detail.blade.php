<?php
$manufacturer = CommonHelper::getFromCache('manufacturer_id' . @$product->manufacture_id);
if (!$manufacturer) {
    $manufacturer = Modules\ThemeWorkart\Models\Manufacturer::find($product->manufacture_id);
    CommonHelper::putToCache('manufacturer_id' . @$product->manufacture_id, $manufacturer);
}

$province = CommonHelper::getFromCache('province_orderBy_name_asc_get');
if (!$province) {
    $province = \Modules\ThemeWorkart\Models\Province::orderBy('name', 'ASC')->get();
    CommonHelper::putToCache('province_orderBy_name_asc_get', $province);
}

$html_province = '<select name="province_home" id="province_home">';
foreach ($province as $p) {
    $html_province .= '<option value="' . $p->id . '">' . $p->name . '</option>';
}
$html_province .= '</select>';

$html_province1 = '<select name="province_tuvan" id="province_tuvan">';
foreach ($province as $p1) {
    $html_province1 .= '<option value="' . $p1->id . '">' . $p1->name . '</option>';
}
$html_province1 .= '</select>';

?>
@extends('themeworkart::layouts.master')

@section('main_content')
    <section id="category-page">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-9 col-lg-9 col-xl-9">
{{--                    <div class="breadcrumb">--}}
{{--                        <a href="">Trang chủ</a>--}}
{{--                        <span class="icon-angle"><i class="fa fa-angle-right"></i></span>--}}
{{--                        <a href="">Shop</a>--}}
{{--                    </div>--}}

                    @include('themeworkart::partials.breadcrumb')
                    <!--breadcrumb-->
                    <div class="row">

                        <div class="col-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="thumbnail-product">


                                <div id="sync1" class="owl-carousel owl-theme">
                                    <div class="item">
                                        <img class="lazy" alt="{{@$product->name}}"
                                             data-src="{{ asset('public/filemanager/userfiles/' . $product->image) }}"
                                             src="{{ asset('public/filemanager/userfiles/' . $product->image) }}"/>
                                    </div>

                                    @if(!empty($image_extras))
                                        @foreach($image_extras as $key => $image_extra)
                                            <div class="item" id="thum{{$key+1}}">
                                            <img class="lazy" alt="{{ $product->name .' '. $key }}"
                                                 data-src="{{ asset('public/filemanager/userfiles/' . $image_extra) }}"
                                            /></div>
                                        @endforeach
                                    @endif
                                </div>


                                <div id="sync2" class="owl-carousel owl-theme">

                                    <div class="item">
                                        <img class="lazy" alt="{{@$product->name}}"
                                             data-src="{{ asset('public/filemanager/userfiles/' . $product->image) }}"
                                             src="{{ asset('public/filemanager/userfiles/' . $product->image) }}"/>
                                    </div>

                                    @if(!empty($image_extras))
                                        @foreach($image_extras as $key => $image_extra)
                                            <div class="item" id="thum{{$key+1}}">
                                                <img class="lazy" alt="{{ $product->name .' '. $key }}"
                                                     data-src="{{ asset('public/filemanager/userfiles/' . $image_extra) }}"
                                                /></div>
                                        @endforeach
                                    @endif

                                </div>
                            </div>
                        </div>


                        <div class="col-12 col-md-6 col-lg-6 col-xl-6">
                            <div class="product-info">
                                <h2>{{$product->name}}</h2>

                                <div class="product_price">
                                    @if($product->final_price != 0)
                                        <span class="price">
                                            @if($product->final_price < $product->base_price) @else
                                            @endif
                                            {{number_format($product->final_price, 0, '.', '.')}}
                                        </span>
                                        @else
                                        </br> <p  class="final_price" class="pr">not set</p>
                                    @endif

                                    @if($product->final_price < $product->base_price)
                                        <del  class="base_price">{{number_format($product->base_price, 0, '.', '.')}}</del>
                                    @endif

                                    @if($product->final_price < $product->base_price)
                                        <div class="on_sale"><span>{{  100 - ( round(($product->final_price/$product->base_price) * 100 )) }}%</span> Off</div>
                                    @else
                                        <div style="height: 24px;border-top: none"></div>
                                    @endif
                                </div>


                                <div class="product-validate">
                                    <div class="product-type product-vali">
                                        <label>Loại sản phẩm</label>
                                        <div class="product-list-validation">
                                            <ul>
                                                <li class="active">Class T-Shift</li>
                                                <li>Hooded Sweatshirt</li>
                                                <li>Hooded Sweatshirt</li>
                                                <li>Hooded Sweatshirt</li>
                                                <li>Hooded Sweatshirt</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="product-color product-vali">
                                        <label>Màu</label>
                                        <div class="product-list-validation">
                                            <ul>
                                                <li class="active">Blue</li>
                                                <li>Red</li>
                                                <li>Yellow</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="product-size product-vali">
                                        <label>Size</label>
                                        <div class="product-list-validation">
                                            <ul>
                                                <li class="active">M</li>
                                                <li>L</li>
                                                <li>XL</li>
                                                <li>XXL</li>
                                                <li>XS</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div><!--validate-->

                                <div class="product-add-cart">
                                    <h2>Personalize design</h2>
                                    <div class="product-attr">
                                        <div class="form-group">
                                            <label>Choose number of Personalize</label>
                                            <div class="choose-item">
                                                <ul>
                                                    <li>1</li>
                                                    <li>2</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Pet Name:</label>
                                            <div class="choose-select">
                                                <select name="" class="form-control">
                                                    <option value="">Pet name 1</option>
                                                    <option value="">Pet name 1</option>
                                                    <option value="">Pet name 1</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Pet Breed:</label>
                                            <div class="choose-select">
                                                <select name="" class="form-control">
                                                    <option value="">Dog & cat</option>
                                                    <option value="">Dog & cat</option>
                                                    <option value="">Dog & cat</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="button" id="reviewClick" class="btn btn-primary" data-toggle="modal" data-target=".bd-review">Preview
                                                your Design
                                            </button>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-12 col-lg-4 col-md-4 col-xl-4">
                                                    <select name="" class="form-control">
                                                        <option value="">1</option>
                                                        <option value="">2</option>
                                                        <option value="">3</option>
                                                        <option value="">4</option>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-lg-8 col-md-8 col-xl-8">
                                                    <button type="button" id="addtocart" class="btn btn-success"  data-text="/cart" onclick="addCart({{$product->id}})">
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--product-cart-->

                                <div class="product-share">
                                    <label>Chia sẻ</label>
                                    <div class="product-share-icon">
                                        <ul>
                                            <li><a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="https://twitter.com/intent/tweet?&url={{url()->current()}}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="https://pinterest.com/pin/create/button/?url={{url()->current()}}&media=&description=" target="_blank"><i class="fa fa-pinterest-square"></i></a></li>
                                            <li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}&title=&summary=&source=" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div><!--product-share-->

                                <div class="product-description">
                                    <h4>Mô tả</h4>
                                    {!! @$product->content !!}
                                </div>
                            </div>
                        </div>
                    </div><!--gallery-->
                </div>
                <div class="col-12 col-md-3 col-lg-3 col-xl-3">
                    @include('themeworkart::childs.product.partials.spthuongmua')
                </div>


                <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="product-related">
                        <div class="widget-title">
                            <h3>Sản phẩm liên quan</h3>
                        </div>
                        <div class="owl-carousel slider-product">

                            @php
                                $cate_multi = explode('|', trim($product->multi_cat, '|'));

                                $productInMultiCats = CommonHelper::getFromCache('products_multi_cat_like_cate_multi' . $cate_multi[0]);
                                if (!$productInMultiCats) {
                                    $productInMultiCats = \Modules\ThemeWorkart\Models\Product::where('multi_cat', 'like', '%|' . $cate_multi[0] . '|%')->where('manufacture_id', $product->manufacture_id)->where('status', 1)->get();
                                    CommonHelper::putToCache('products_multi_cat_like_cate_multi' . $cate_multi[0], $productInMultiCats);
                                }
                            @endphp

                            @if($productInMultiCats->count() > 0)
                                @foreach( $productInMultiCats as $featured)
                                   @include( 'themeworkart::childs.product.partials.product_loop' )
                                @endforeach
                            @endif



                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-12">
                    <div class="review">
                        <div class="row">
                            <div class="col-md-6 col-6 btn-write-review" >
                                <div class="review-star">
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                            </div>


                            <div class="col-md-6 col-6 text-right">
                                <div id="reviewWrite" class="btn btn-primary btn-write-review">
                                    Write a Review
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

{{--    <!-- Modal Add to cart -->--}}
    <div class="modal fade" id="addToCartModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding: 10px;border-bottom: 0px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4 style="font-size: 20px;" class="text-center">Thêm thành công vào giỏ hàng</h4>
                    <div class="form-group mt-4 mb-4 text-center">
                        <a href="/cart" class="btn btn-success btn-sm">Xem giỏ hàng</a>
                        <a href="#" data-dismiss="modal" class="btn btn-primary btn-sm">Close</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('themeworkart::childs.product.modal_review')


@endsection
