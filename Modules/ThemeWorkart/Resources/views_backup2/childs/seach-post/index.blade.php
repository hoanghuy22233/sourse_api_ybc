@extends('themeworkart::layouts.master')
@section('main_content')
    @include('themeworkart::partials.breadcrumb')
    <div class="container">

        <div class="row">
            @include('themeworkart::partials.sidebar_posts')
            <section id="seach_post" class="main_container collection col-md-9 col-sm-8 col-xs-12">
                <div class="pottion">
                    <div class="category-products products">

                        <div class="sortPagiBar">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 text-xs-left text-sm-right">
                                    @if(isset($text_filter))
                                        <h3 style="    display: inline-block;margin: 0;">{{ $text_filter }}</h3>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <section class="products-view products-view-grid">
                            <div class="row">

                                @foreach($posts as $post)
                                    <div class="col-xs-6 col-sm-6 col-lg-4">


                                        <div class="product-box product-box-edits">
                                            <div class="product-thumbnail">
                                                <?php
                                                if ($post->final_price < $post->base_price) {
                                                    echo '<div class="sale-flash">SALE</div>';
                                                } else {
                                                    echo '<div class="sale-flash hide">SALE</div>';
                                                }
                                                ?>

                                                <a href="{{ $post->slug }}"
                                                   title="{{$post -> name}}">

                                                    <picture><img
                                                                data-src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($post->image, 263, 197) }}"
                                                                alt="{{$post -> name}}"
                                                                class="lazy img-responsive center-block"/>
                                                    </picture>

                                                </a>

                                                <div class="prod-icons">
                                                    <div class="variants form-nut-grid"
                                                         data-id="product-actions-8702198"
                                                         enctype="multipart/form-data">

                                                        @if($post->final_price == 0)
                                                            <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($post) }}"
                                                               data-toggle="tooltip" data-placement="top"
                                                               class="fa fa-link"
                                                               title="Chọn sản phẩm"></a>
                                                        @else
                                                            <a class="fa fa-shopping-basket add_to_cart"
                                                               href="javascript:;" data-product_id="{{ $post->id }}"
                                                               title="{{$post->name}}"
                                                               data-original-title="Mua ngay"></a>
                                                        @endif

                                                        <a
                                                                href="javascript:;" data-product_id="{{ $post->id }}"
                                                                title="Xem nhanh"
                                                                class="fa fa-search quick-view"></a>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-info">
                                                <h3 class="product-name">
                                                    <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($post) }}" title="{{$post -> name}}">{{$post -> name}}
                                                    </a>
                                                </h3>
                                                <div class="price-box clearfix">

                                                    {!! \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getPriceHtml($post) !!}
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div style="width: 100%; display: inline-block">
                                    {!! $posts->appends(isset($filter) ? $filter : '')->links() !!}
                                </div>
                            </div>
                        </section>

                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="bizweb-product-reviews-module"></div>
@endsection

@section('custom_header')
    <link href="{{ URL::asset('public/frontend/themes/stbd/css/bpr-products-module4826.css') }}" rel='stylesheet' type='text/css'/>
@endsection

@section('custom_footer')
    <script>
        //  Custom code
        //  Search product by ajax
        function search() {
            loading();
            console.log('search...');
            var manufactureres = [];
            $(".serach-mamufacturer:checked").each(function () {
                manufactureres.push($(this).val());
            });

            var min_price = $('#start input').val();
            var max_price = $('#stop input').val();

            var order = $('input[name=order]').val();

            $.ajax({
                url: "{{ route('product.ajax_get_product') }}",
                type: 'GET',
                data: {
                    manufactureres: manufactureres,
                    min_price: min_price,
                    max_price: max_price,
                    category_id: '{{ isset($category) ? $category->id : false }}',
                    keyword: '{{ isset($_GET['keyword']) ? $_GET['keyword'] : false }}',
                    order: order
                },
                success: function (result) {
                    stopLoading();
                    $('section.products-view.products-view-grid > .row').html(result);
                },
                error: function () {
                    stopLoading();
                    alert('Có lỗi xảy ra. Vui lòng load lại website và thử lại!');
                }
            });
        }

        $('.serach-mamufacturer').click(function () {
            search();
        });

        $('a#filter-value').click(function () {
            search();
        });

        $('#sort-by a').click(function () {
            console.log('sort by');
            $('input[name=order]').val($(this).data('value'));
            $('#order_label').html($(this).html());
            search();
        });
    </script>

    <script>





    </script>
@endsection
