@extends('themeworkart::layouts.master')
@section('main_content')
    <div class="b" style="margin:0px auto;">
        <div class="f flexJus headpage">
            <div class="flex">
                <ul class="breadcrumb">
                    <li itemscope="itemscope"><a itemprop="url" href="{{route('home')}}" title="{{ @$settings['name'] }}"><span itemprop="title">{{ @$settings['name'] }}</span></a></li>
                    <li itemscope="itemscope"><span itemprop="title">{{ucfirst(mb_strtolower($categoryId->name))}}</span></li>
                </ul>
            </div>

        </div>
    </div>

    <div class="f BGPrimary ColorPrimary">
        <img class="lazy" data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($categoryId->banner, 1350, null) }}"  alt="{{$categoryId->name}}" style="margin: auto;display:block;">
    </div>
    <style>
        .submenu2 ul li h3{
            cursor: pointer;
        }
        .tit {margin:3rem 0 0;padding:10px 0;font-size:30px;line-height:30px;font-weight: 700;text-align: center;float:left;width:100%;background:url('/public/frontend/themes/stbd/upload/files/Page/home/BG-top.jpg') repeat 0 0;color:#fff;}
        .submenu2{background:url('/public/frontend/themes/stbd/upload/files/Page/home/BG-top.jpg') repeat 0 0;padding:10px 0;}
        .submenu2 li h3{padding:6px 20px;text-transform:uppercase;font-size:15px;color:#fff;}
        @media (max-width: 768px){
                .f .b a.gri {
                    width: 48%;
                }
        }
    </style>
    @if(count($category) > 0)
    <div class="f submenu2">
        <div class="b">
            <ul class="flexCen" id="menu1">

            </ul>
        </div>
    </div>
    @endif
    <script>
        function movetobox(id) {
            var v = $('#' + id).offset().top;
            $('html, body').animate({ scrollTop: v }, 1000);
        }
        $(document).ready(function () {
            var cmtab = '';
            $('.khoisp').each(function () {
                if( $(this).attr('data-shorttitle') != undefined) {
                    cmtab += '<li onclick="movetobox(\'' + this.id + '\');"><h3>' + $(this).attr('data-shorttitle') + '</h3></li>';
                }
            });
            $('#menu1').html(cmtab);
        });
    </script>
    <div class="f" style="background:#f6f6f6">
        <!--san pham-->
            @foreach($category as $val)
                <div class="f">
                    <div class="b khoisp" id="sale{{$val->id}}" data-title="{{$val->name}}">
                        <h2 class="tit">{{ucfirst(mb_strtolower($val->name))}}</h2>
                        @if(!empty($products))
                            @foreach($category as $cate)
                                @foreach($products as $product)
                                   @if($cate->id == $val->id && in_array($cate->id, explode('|', $product->multi_cat)))
                                       @php
                                           $manufacture = \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getFromCache('manufacture_id'.$product->manufacture_id);
                                       if (!$manufacture){
                                            $manufacture = Modules\ThemeWorkart\Models\Manufacturer::find($product->manufacture_id);
                                            \Modules\ThemeWorkart\Http\Helpers\CommonHelper::putToCache('manufacture_id'.$product->manufacture_id, $manufacture);
                                       }
                                       @endphp
                                        <a class="gri" href="{{ Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product) }}" title="{{$product->name}}">
                                            <div class="gi">
                                                <div>
                                                    <img class="lazy" data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($product->image, 220, null) }}" alt="{{$product->name}}">
                                                </div>
                                                <div><img class="lazy" data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($manufacture->image, 120, null) }}" alt="{{$categoryId->name}}"></div>
                                                <h3>{{$product->name}}</h3>
                                                <p class="product_intro">{{$product->intro}}</p>
                                                <span><u>{{number_format($product->base_price, 0, '.', '.')}}&nbsp;<sup>đ</sup></u><b>@if($product->final_price == 0) Liên hệ @else {{number_format($product->final_price, 0, '.', '.')}}&nbsp;<sup>đ</sup> @endif</b></span>
                                            </div>
                                            <div>
                                                <span>XEM HÀNG</span>
                                                <span>MUA GIÁ XẢ</span>
                                            </div>
                                        </a>
                                    @endif
                                @endforeach
                            @endforeach
                        @endif
                    </div>
                </div>
            @endforeach
        <style>
            /*button*/
            .dangkingay {margin-top:50px;margin-bottom:50px;}
            .dangkingay label{margin-bottom:20px;display:block;font:bold 25px/25px arial;}
            .btn {display: inline-block;font-weight: 400;text-align: center;white-space: nowrap;vertical-align: middle;user-select: none;border: 1px solid transparent;box-shadow: 0 2px 4px 0 rgba(248, 90, 34, 0.5), 0 10px 20px 0 rgba(0, 0, 0, 0.25);padding: 1rem 4.5rem;font-size: 1.125rem;line-height: 1.5;border-radius: 50px;
                transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;-webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;-o-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;color: #fff;cursor: pointer;margin: 0 2px;text-transform: uppercase;}
            .btn.btn-primary {border: none;background: -webkit-gradient(linear, right top, left top, from(#F95B21), color-stop(36.83%, #E84352), to(#BC0A25));background: -webkit-linear-gradient(right, #F95B21 0%, #E84352 36.83%, #BC0A25 100%);background: -o-linear-gradient(right, #F95B21 0%, #E84352 36.83%, #BC0A25 100%);background: linear-gradient(270deg, #F95B21 0%, #E84352 36.83%, #BC0A25 100%);color: #fff;}
            .btn.btn-primary:hover, .btn.btn-primary:active {opacity: 0.8;box-shadow: none;box-shadow: none;}

            /*san pham luoi*/
            .gri {float: left;width: 23%;margin: 1%;position: relative;text-align: center;background: #fff;display: flex;flex-direction: column;color: #666;border-radius: 4px;}
            .gri:hover{color:#666;}
            .gri .gi {display: flex;flex-direction: column;padding: 1%;}
            .gi div:nth-child(1){height:250px;display: flex;align-items: center;justify-content:center;margin-bottom:10px;}
            .gi h3{color:#000;font-size:15px;text-transform:uppercase;}
            .gi p {font-size: 13px;padding:15px;height:85px;overflow:hidden;}
            .gri3 p {height:45px;padding:5px;}
            .gi label {display: flex;justify-content: center;align-items:center;color: #00a7e9;}
            .gi label i {margin: 0 10px 0 0;padding-right:80px;border-right:2px solid #00a7e9;}
            .gi span {padding:10px 0;}
            .gi span u{text-decoration:line-through;}
            .gi span b{color:red;display:block;font-size:26px;margin-top:12px;}
            .gri div:nth-last-child(1) {height: 50px;text-align: center;border-top: 1px solid #ddd;}
            .gri div:nth-last-child(1) span {width: 50%;float:left;line-height:50px;}
            .gri div:nth-last-child(1) span:hover {background: linear-gradient(to bottom,#ffa103,#fb7d0f);color: #fff;}
            .gri div:nth-last-child(1) span:nth-child(1) {border-right: 1px solid #ddd;border-radius: 0 0 0 4px;}
            .gri div:nth-last-child(1) span:nth-child(2) {border-radius: 0 0 4px 0;}
        </style>

    </div>
@endsection