<!DOCTYPE html>
<html lang="vi-vn" xml:lang="vi-vn">
<?php
    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

$banner_top = CommonHelper::getFromCache('banner_location_banner_header_top');
if (!$banner_top) {
    $banner_top = \Modules\ThemeWorkart\Models\Banner::where('status',1)->where('location','banner_header_top')->first();
    CommonHelper::putToCache('banner_location_banner_header_top', $banner_top);
}

$banner_right = CommonHelper::getFromCache('banner_location_banner_header_right');
if (!$banner_right) {
    $banner_right = \Modules\ThemeWorkart\Models\Banner::where('status',1)->where('location','banner_header_right')->first();
    CommonHelper::putToCache('banner_location_banner_header_right', $banner_right);
}

$banner_left = CommonHelper::getFromCache('banner_location_banner_header_left');
if (!$banner_left) {
    $banner_left = \Modules\ThemeWorkart\Models\Banner::where('status',1)->where('location','banner_header_left')->first();
    CommonHelper::putToCache('banner_location_banner_header_left', $banner_left);
}

?>
<head>

    @include('themeworkart::partials.header_detail_script')
    @include('themeworkart::partials.head_meta')
    @include('themeworkart::partials.header_cate_script')
    @include('themeworkart::partials.header_script')
    {!! @$settings['head_code'] !!}
    @yield('head_script')
</head>


<body>
    @include('themeworkart::partials.header')

    <main id="main">
        @yield('main_content')
    </main> <!-- end main -->

    @include('themeworkart::partials.footer')
</body>
</html>
