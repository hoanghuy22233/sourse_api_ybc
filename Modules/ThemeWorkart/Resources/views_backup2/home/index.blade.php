@extends('themeworkart::layouts.master')

@section('main_content')

    {{--        HOME PAGE --}}
    {{--home slider --}}

    <section id="home-slider">

       @php
        $banners = CommonHelper::getFromCache('get_banners_by_slide');
        if (!$banners) {
            $banners = \Modules\ThemeWorkart\Models\Banner::where('status', 1)->where('location', 'slide_home')->orderBy('order_no', 'ASC')->get();
            CommonHelper::putToCache('get_banners_by_slide', $banners);
        }
        @endphp

        <div class="owl-carousel slider">
            @if(!empty($banners) || isset($banners))
                @foreach($banners as $data)
                    <div class="slider-item">
                        <a href="{{$data->link}}" title="{{$data->name}}">
                            <img class="lazy" data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumbNoCut($data->image)}}"
                                 alt="{{$data->name}}"/>
                        </a>
                    </div>
                @endforeach
            @endif
        </div>
    </section><!--section-slider-->
    {{-- !home slider --}}

    {{--        product box --}}

    <section class="product-box">
        <div class="container">
            <div class="row row-5">
                <div class="col-12 pd-5">
                    <div class="home-title">
                        <h2>Sản phẩm mới</h2>
                    </div>
                </div>

                <div class="col-12 col-lg-4 col-xl-4 col-md-4  pd-5">
                    <div class="banner-ad-view">
                        <img src="https://via.placeholder.com/700x700?text=Banner" alt="">
                    </div>
                </div>


                <div class="col-12 col-lg-8 col-xl-8 col-md-8  pd-5">
                    <div class="product-grid">
                        <div class="row row-5">
                            @php
                                $features = CommonHelper::getFromCache('product_featured_1');
                                if (!$features){
                                    $features = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(6);
                                    CommonHelper::putToCache('product_featured_1', $features);
                                }
                            @endphp

                            @foreach($features as $k=>$featured)
                                <div class="col-12 col-lg-4 col-md-4 col-xl-4 pd-5">
                                    @include( 'themeworkart::childs.product.partials.product_loop' )
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--section-product-new-->
    {{--        !product box --}}

    <!-- product box category -->
    <section class="product-box">
        <div class="container">
            <div class="row row-5">
                <div class="col-12 pd-5">
                    <div class="home-title">
                        <h2>Sản phẩm theo chuyên mục</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-5">
                @php
                        $product_cat = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(8);
                @endphp

                @foreach($product_cat as $k=>$featured)
                    <div class="col-12 col-lg-3 col-md-3 col-xl-3 pd-5">
                        @php $item_class = 'small' @endphp
                        @include( 'themeworkart::childs.product.partials.product_loop' )
                    </div>
                @endforeach

                </div>
        </div>

    </section><!--section-product-category-->


    <!-- product box sale  -->
    <section class="product-box">
        <div class="container">
            <div class="row row-5">
                <div class="col-12 pd-5">
                    <div class="home-title">
                        <h2>Sản phẩm giảm giá</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row row-5">
                        @php
                            $sale_products = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(12);
                        @endphp

                <div class="owl-carousel slider-product">

                @foreach($sale_products as $k=>$featured)
                            <div class="col-12 col-lg-12 col-md-12 col-xl-12 pd-5">
                                        @php $item_class = 'small' @endphp
                                        @include( 'themeworkart::childs.product.partials.product_loop' )
                            </div>
                        @endforeach
                </div>

            </div>
                </div>
            </div>
        </div>
    </section><!--section-product-sale-->
@endsection
