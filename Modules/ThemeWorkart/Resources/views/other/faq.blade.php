@extends('themeworkart::layouts.master')

@section('main_content')
    <div class="container">
        <div class="row">
            <div class="breadcrumbs">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item fs14"><a href="#">Home</a></li>
                        <li class="breadcrumb-item fs14 active">FAQs</li>
                    </ol>
                </nav>
            </div>
            <div class="col-12">
                <p class="text-center fs32">FAQs</p>
            </div>
        </div>
    </div>
    <div class="py-5 container">
        <div class="row justify-content-center">
            <div class="col-3">
                <a href="#">
                    <div class="hover hover-1 text-white rounded">
                        <img src="css/img/Virtual-Payment-Terminal-1.png" alt="">
                        <div class="hover-overlay"></div>
                        <div class="hover-1-noidung px-5 py-4">
                            <h5 class="hover-1-tieude text-uppercase font-weight-bold mb-0"><span class="font-weight-light"> Manage Oder</span>
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-3">
                <a href="#">
                    <div class="hover hover-1 text-white rounded">
                        <img src="css/img/fob%20meaning%20in%20shipping.png" alt="Image">
                        <div class="hover-overlay"></div>
                        <div class="hover-1-noidung px-5 py-4">
                            <h5 class="hover-1-tieude text-uppercase font-weight-bold mb-0"><span class="font-weight-light"> Shipping &amp; Delivery</span>
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-3">
                <a href="#">
                    <div class="hover hover-1 text-white rounded">
                        <img src="css/img/Mask.png" alt="">
                        <div class="hover-overlay"></div>
                        <div class="hover-1-noidung px-5 py-4">
                            <h5 class="hover-1-tieude text-uppercase font-weight-bold mb-0"><span class="font-weight-light">Covid-19 Delay Notice</span>
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-3">
                <a href="#">
                    <div class="hover hover-1 text-white rounded">
                        <img src="css/img/paymentgateway.png" alt="">
                        <div class="hover-overlay"></div>
                        <div class="hover-1-noidung px-5 py-4">
                            <h5 class="hover-1-tieude text-uppercase font-weight-bold mb-0"><span class="font-weight-light"> Payment Methods</span>
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <br>
        <div class="row justify-content-center">
            <div class="col-3">
                <a href="#">
                    <div class="hover hover-1 text-white rounded">
                        <img src="css/img/Income-Tax-Refund-Reissue.png" alt="">
                        <div class="hover-overlay"></div>
                        <div class="hover-1-noidung px-5 py-4">
                            <h5 class="hover-1-tieude text-uppercase font-weight-bold mb-0"><span class="font-weight-light"> Repacement &amp; Refund</span>
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-3">
                <a href="#">
                    <div class="hover hover-1 text-white rounded">
                        <img src="css/img/asset-4.png" alt="">
                        <div class="hover-overlay"></div>
                        <div class="hover-1-noidung px-5 py-4">
                            <h5 class="hover-1-tieude text-uppercase font-weight-bold mb-0"><span class="font-weight-light"> Track my order</span>
                            </h5>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection