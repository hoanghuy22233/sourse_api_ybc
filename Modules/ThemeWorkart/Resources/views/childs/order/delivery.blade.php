@extends('themeworkart::layouts.master')
@section('main_content')
    <div class="d-none d-md-block pl-sm-0 pl-md-3">
        <div class="breadcrumbs container-custom">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item fs14"><a href="#">Cart</a></li>
                    <li class="breadcrumb-item fs14"><a href="#">Information</a></li>
                    <li class="breadcrumb-item fs14"><a href="#">Shipping</a></li>
                    <li class="breadcrumb-item fs14 active">Payment</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container-custom">
        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 contact-info-customer" style="margin-left: 39px;">
                <div class="row d-flex justify-content-between" style="max-width: 583px;">
                    <p class="color-text fs18">Contact information</p>
                    <p class="color-text fs14">Already have an account? <span style="color: #2F80ED;">Log in</span></p>
                </div>
                <div class="row form-info-checkout"   style="max-width: 583px;">
                    <form method="POST" action="{{ route('order.postDelivery') }}" style="width: 100%; margin-bottom: 73px;">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            @if($errors->has('bill.user_email'))
                                <div class="error">{{ ucfirst($errors->first('bill.user_email')) }}</div>
                            @endif
                            <input value="{{ isset(Session::get('cart_delivery')['bill']['user_email']) ? Session::get('cart_delivery')['bill']['user_email'] : "" }}" name="bill[user_email]" type="text" class="form-control rounded-0" placeholder="Email" style="height: 50px;">
                        </div>
                        <div class="custom-control custom-checkbox my-1 mr-sm-2 mt-3">
                            <input type="checkbox" class="custom-control-input" id="keepme">
                            <label class="custom-control-label fs14" style="color: #707070;" for="keepme">Keep me up to date on news and exclusive offerse</label>
                        </div>
                        <p class="color-text fs18" style="margin-top: 36px;">Shipping address</p>

                        @if($errors->has('bill.first_name'))
                            <div class="error">{{ ucfirst($errors->first('bill.first_name')) }}</div>
                        @endif
                        @if($errors->has('bill.last_name'))
                            <div class="error">{{ ucfirst($errors->first('bill.last_name')) }}</div>
                        @endif
                        <div class="form-group d-md-flex justify-content-md-between">

                            <input value="{{ isset(Session::get('cart_delivery')['bill']['first_name']) ? Session::get('cart_delivery')['bill']['first_name'] : "" }}" name="bill[first_name]" type="text" class="form-control rounded-0 d-inline mr-sm-1" placeholder="First name ( optional)" style="height: 50px; width: 49%;">
                            <input value="{{ isset(Session::get('cart_delivery')['bill']['last_name']) ? Session::get('cart_delivery')['bill']['last_name'] : "" }}" name="bill[last_name]" type="text" class="form-control rounded-0 d-inline" placeholder="Last name" style="height: 50px; width: 49%;">
                        </div>
                        <div class="form-group">
                            @if($errors->has('bill.user_address'))
                                <div class="error">{{ ucfirst($errors->first('bill.user_address')) }}</div>
                            @endif
                            <input value="{{ isset(Session::get('cart_delivery')['bill']['user_address']) ? Session::get('cart_delivery')['bill']['user_address'] : "" }}" name="bill[user_address]" type="text" class="form-control rounded-0" placeholder="Address" style="height: 50px;">
                        </div>
                        <div class="form-group">
                            @if($errors->has('bill.user_apartment'))
                                <div class="error">{{ ucfirst($errors->first('bill.user_apartment')) }}</div>
                            @endif
                            <input value="{{ isset(Session::get('cart_delivery')['bill']['user_apartment']) ? Session::get('cart_delivery')['bill']['user_apartment'] : "" }}" name="bill[user_apartment]" type="text" class="form-control rounded-0" placeholder="Apartment, suite, etc. (optional)" style="height: 50px;">
                        </div>
                        <div class="form-group">
                            @if($errors->has('bill.user_country_id'))
                                <div class="error">{{ ucfirst($errors->first('bill.user_country_id')) }}</div>
                            @endif
                            <div class="field__input-wrapper field__input-wrapper--select">
                                @php

                                    $data = \Modules\ThemeWorkart\Models\Country::pluck('name', 'id');
                                @endphp
                                <select class="field__input field__input--select form-control" name="bill[user_country_id]" id="billingProvince">
                                    <option value="">--- Select Country ---</option>
                                    @foreach($data as $id => $v)
                                        @php
                                            $id_country = isset(Session::get('cart_delivery')['bill']['user_country_id']) ? Session::get('cart_delivery')['bill']['user_country_id'] : "";
                                            $selected = $id_country == $id ? "selected" : "";
                                        @endphp
                                        <option {{ $selected  }} value="{{ $id }}">{{ $v }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @if($errors->has('bill.user_city'))
                            <div class="error">{{ ucfirst($errors->first('bill.user_city')) }}</div>
                        @endif
                        @if($errors->has('bill.user_postal'))
                            <div class="error">{{ ucfirst($errors->first('bill.user_postal')) }}</div>
                        @endif
                        <div class="form-group d-md-flex justify-content-md-between">

                            <input value="{{ isset(Session::get('cart_delivery')['bill']['user_district']) ? Session::get('cart_delivery')['bill']['user_city'] : "" }}" name="bill[user_city]" type="text" class="form-control rounded-0 d-inline  mr-sm-1" placeholder="City" style="height: 50px; width: 49%;">
                            <input value="{{ isset(Session::get('cart_delivery')['bill']['user_postal']) ? Session::get('cart_delivery')['bill']['user_postal'] : "" }}" name="bill[user_postal]" type="text" class="form-control rounded-0 d-inline" placeholder="Postal code" style="height: 50px; width: 49%;">
                        </div>
                        <div class="form-group">
                            @if($errors->has('bill.user_tel'))
                                <div class="error">{{ ucfirst($errors->first('bill.user_tel')) }}</div>
                            @endif
                            <input value="{{ isset(Session::get('cart_delivery')['bill']['user_tel']) ? Session::get('cart_delivery')['bill']['user_tel'] : "" }}" name="bill[user_tel]" type="text" class="form-control rounded-0" placeholder="Phone" style="height: 50px;">
                        </div>
                        <div class="custom-control custom-checkbox mr-sm-2 mt-3" style="margin-bottom: 34px;">
                            <input type="checkbox" class="custom-control-input" id="saveInfor">
                            <label class="custom-control-label fs14" style="color: #707070;" for="saveInfor">Save this information for next time</label>
                        </div>
                        <div class="submit-return d-flex justify-content-between">
                            <a href="{{ route('cart.index')  }}" class="">&lt; Return to cart</a>
                            <button class="btn btn-orange btn-checkout" type="submit">Continue to shipping</button>
                        </div>
                    </form>
                    <div class="info-more d-flex justify-content-between" style="border-top: 1px solid #ccc; width: 100%;">
                        <a href="" class="color-text fs12">Refund policy</a>
                        <a href="" class="color-text fs12">Shipping policy</a>
                        <a href="" class="color-text fs12">Privacy policy</a>
                        <a href="" class="color-text fs12">Term of service</a>
                    </div>
                </div>

            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 mt-sm-4 mt-md-0 ml-4">
                <p style="font-size: 18px; font-weight: 600; color: #282364;">Order Summary</p>
                @php
                    $checkPromote = false;
                    $promotion = Session::get('promotion');
                    $value_promote = 0;
                    if($promotion){
                         $checkPromote = true;
                         $value_promote = $promotion['value'];
                         $typePromote = "default";
                         if(preg_match('#%$#', $value_promote)){
                             $typePromote = "percent";
                         }
                    }


                    $carts = Cart::content();
                    $countItem = Cart::count();
                    $priceTotalBase = 0;
                    $shipping = 0;

                @endphp
                @foreach( $carts  as $cart)
                    @php
                        $product = \Modules\ThemeWorkart\Models\Product::find($cart->id);
                         $priceTotalBase += $cart->price * $cart->qty;
                    @endphp
                    <div class="wp-item-cart">
                        <div class="item-cart d-flex">
                            <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product) }}" class="d-block"><img src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($product->image, 50, 50) }}" class="" alt=""></a>
                            <div class="info-item-cart ml-3">
                                <p class="title-info-item-cart">{{ $cart->name  }}</p>
                                <div class="custom-item-cart d-md-flex justify-content-between d-none">
                                    <ul class="list-unstyled">
                                        <li>Available Products</li>
                                        <li>Portrait Canvas: 73 in</li>
                                        <li>Frame</li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li>Color white</li>
                                        <li>Size 12" x 18"</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="qty-price text-right">
                            <p>{{ $cart->qty  }} X {{ number_format($cart->price) }} vnđ</p>
                        </div>
                    </div>
                @endforeach
                @php
                    $total = $priceTotalBase + $shipping;
                    if($value_promote != 0){
                        if($typePromote == "default"){
                            $total -= $value_promote;
                        }else if($typePromote == "percent"){
                            $value_promote = preg_replace("#%#", "", $value_promote);
                            $total = $total - $total *  $value_promote / 100;
                        }
                    }
                @endphp
                <div class="pay-cart">
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Subtotal : ( {{ $countItem  }} item )</span><span>${{ number_format($priceTotalBase)  }}</span></p>
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Shipping:</span><span>$0</span></p>
                    <div id="promote_result">
                    @if($checkPromote)
                        <p class="d-flex justify-content-between title-info-item-cart">
                            <span>Promotion:</span>
                            <span>-
                                @if($typePromote == "default")
                                    ${{ number_format($value_promote) }}
                                @elseif($typePromote == "percent")
                                    {{ $value_promote }}%
                                @endif
                            </span></p>
                    @endif
                    </div>
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Total:</span><span id="total_price_result">${{ number_format($total) }}</span></p>
                    <form id="frmPromote" action="{{ route('order.addPromote')  }}" method="get">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <div class="error-promote"></div>
                            <input name="code" type="text" class="form-control rounded-0" placeholder="Enter promote code" style="height: 50px;">
                            <span onclick="ajaxPromote('{{ route('order.addPromote')  }}')" class="d-none d-lg-block">Apply</span>
                            <p onclick="ajaxPromote('{{ route('order.addPromote')  }}')" class="text-center mt-3 d-lg-none apply">Apply</p>

                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('custom_header')
    <link href="{{ URL::asset('public/frontend/themes/stbd/css/checkout.css') }}" rel='stylesheet' type='text/css'/>
    <link href="{{ URL::asset('public/frontend/themes/stbd/css/checkout_dev_2.css') }}" rel='stylesheet' type='text/css'/>
    <style>
        .form-group input {
            margin: 0;
        }
        .field__input-btn-wrapper .btn-checkout {
            padding: 0px 20px;
        }
        .summary-product-list table td {
            padding: 10px !important;
        }
        td.product-price.text-right {
            min-height: 81px;
            vertical-align: middle;
            text-align: center;
        }
        span.total-line-name {
            font-weight: bold;
        }
        span.total-line-price {
            font-weight: bold;
        }
    </style>
@endsection

@section('custom_footer')
    <script>
        $(document).ready(function () {
            $('select#billingProvince').change(function () {
                $('select#billingDistrict').attr('disabled', 'disabled');
                var city_id = $(this).val();
                $.ajax({
                    url: '{{ URL::to('ajax-get-district') }}',
                    type: 'GET',
                    data: {
                        city_id : city_id
                    },
                    success: function (result) {
                        $('select#billingDistrict').removeAttr('disabled');
                        $('select#billingDistrict').html(result);
                    },
                    error: function () {
                        alert('Có lỗi xảy ra. Vui lòng F5 lại website!');
                    }
                });
            });
        });

    </script>
@endsection

@section('script')
    <script>
        function ajaxPromote(url) {
            var code = $("input[name='code']").val();
            if (code == '') return;
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    code: code
                },
                success: function (result) {
                    console.log(result);
                    if(result.status == 'error'){
                        $('.error-promote').text(result.error);
                    }else{
                        var html = '<p class="d-flex justify-content-between title-info-item-cart"><span>Promotion:</span><span>'+result.success.promote+'</span></p>';
                        $('.error-promote').text('');
                        $("#promote_result").html(html);
                        $("#total_price_result").html(result.success.total);
                    }
                },
                error: function () {
                    alert('Có lỗi xảy ra. Vui lòng F5 lại website!');
                }
            });
        }
    </script>
@stop