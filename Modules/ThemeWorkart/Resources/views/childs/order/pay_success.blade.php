@extends('themeworkart::layouts.master')
@section('main_content')
    <div class="d-none d-md-block pl-sm-0 pl-md-3">
        <div class="breadcrumbs container-custom">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item fs14"><a href="#">Cart</a></li>
                    <li class="breadcrumb-item fs14"><a href="#">Information</a></li>
                    <li class="breadcrumb-item fs14"><a href="#">Shipping</a></li>
                    <li class="breadcrumb-item fs14 active">Payment</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-5 col-md-5 col-sm-12 text-center">
                <img src="css/img/checked.png" alt="">
                <p class="mt-3" style="font-size: 18px; font-weight: 600; color: #FF920C; margin-bottom: 32px;">Thank you
                    for your puchase</p>
                <p class="d-none d-md-block" style="color: #282364; margin-bottom: 40px;">We love our customer dearly and your feedback
                    is so helpful for us to hear</p>
                <p style="color: #2F80ED;" class="mb-3"><img src="css/img/printer%20(1).png" alt=""> Print recelt</p>
                <a href="{{ route('home')  }}" class="btn btn-success border-0">Return to store</a>
            </div>
            <div class="col-md-1 d-md-block d-none"></div>
            <div class="col-md-6 col-sm-12 mt-sm-5 mt-md-0">
                <p style="font-size: 18px; font-weight: 600; color: #282364;">Order Summary</p>
                @php
                    $checkPromote = false;
                        $promotion = Session::get('promotion');
                        $value_promote = 0;
                        if($promotion){
                             $checkPromote = true;
                             $value_promote = $promotion['value'];
                             $typePromote = "default";
                             if(preg_match('#%$#', $value_promote)){
                                 $typePromote = "percent";
                             }
                        }
                        $carts = Cart::content();
                        $countItem = Cart::count();
                        $priceTotalBase = 0;
                        $shipping = 0;
                @endphp
                @foreach( $carts  as $cart)
                    @php
                        $product = \Modules\ThemeWorkart\Models\Product::find($cart->id);
                         $priceTotalBase += $cart->price * $cart->qty;
                         $shipping = 0;
                         $total = $priceTotalBase + $shipping;
                    @endphp
                    <div class="wp-item-cart">
                        <div class="item-cart d-flex">
                            <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product) }}" class="d-block"><img src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($product->image, 50, 50) }}" class="" alt=""></a>
                            <div class="info-item-cart ml-3">
                                <p class="title-info-item-cart">{{ $cart->name  }}</p>
                                <div class="custom-item-cart d-md-flex justify-content-between d-none">
                                    <ul class="list-unstyled">
                                        <li>Available Products</li>
                                        <li>Portrait Canvas: 73 in</li>
                                        <li>Frame</li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li>Color white</li>
                                        <li>Size 12" x 18"</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="qty-price text-right">
                            <p>{{ $cart->qty  }} X {{ number_format($cart->price) }} vnđ</p>
                        </div>
                    </div>
                @endforeach
                @php
                    $total = $priceTotalBase + $shipping;
                    if($value_promote != 0){
                        if($typePromote == "default"){
                            $total -= $value_promote;
                        }else if($typePromote == "percent"){
                            $value_promote = preg_replace("#%#", "", $value_promote);
                            $total = $total - $total *  $value_promote / 100;
                        }
                    }
                @endphp
                <div class="pay-cart">
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Subtotal : ( {{ $countItem  }} item )</span><span>{{ number_format($priceTotalBase)  }} vnđ</span></p>
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Shipping:</span><span>0 VNĐ</span></p>
                    <div id="promote_result">
                        @if($checkPromote)
                            <p class="d-flex justify-content-between title-info-item-cart">
                                <span>Promotion:</span>
                                <span>-
                                @if($typePromote == "default")
                                        ${{ number_format($value_promote) }}
                                    @elseif($typePromote == "percent")
                                        {{ $value_promote }}%
                                    @endif
                            </span></p>
                        @endif
                    </div>
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Total:</span><span>{{ number_format($total) }} vnđ</span></p>

                </div>
            </div>
        </div>
    </div>
    @php
        // huy session
        Cart::destroy();
        Session::forget('promotion');
        Session::forget('cart_delivery');
    @endphp
@endsection

@section('custom_header')
    <style>
        section.content {
            min-height: 500px;
        }
    </style>
@endsection

@section('custom_footer')

@endsection