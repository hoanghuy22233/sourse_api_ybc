@extends('themeworkart::layouts.master')
@section('main_content')
    @php
        $province = Modules\ThemeWorkart\Models\Province::all();
    @endphp

    <section id="category-page">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="header-cart">
                        <div class="row">
                            <div class="col-6 col-md-6 col-lg-6 col-xl-6">
                                <h2>Your cart</h2>
                            </div>
                            <div class="col-6 col-md-6 col-lg-6 col-xl-6 text-right">
                                <div class="cart-total">
{{--                                    <label>Total:</label>--}}
{{--                                    <div data-value="{{@$total}}"  class="price-cart total_money">{{number_format(@$total, 0, '.', '.')}} USD</div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-12 col-md-9 col-lg-9 col-xl-9">
                    <div class="list-cart">
                    @if( !empty( $cart ) )
                        @foreach($cart as $i=>$data)
                             @php
                                $product = \Modules\ThemeWorkart\Models\Product::find(@$data['id']);
                             @endphp

                            <div class="cart-item">

                                    <div class="cart-avatar">
                                        <img class="lazy" data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($data['image'], 320, null) }}" alt="{{$product->name}}" />
                                    </div>

                                    <div class="cart-content">
                                        <h3>{{$product->name}}</h3>
                                        <div class="product_price">
                                            <span class="price">${{@$data['price']}}</span>
                                        </div>

                                        <div class="pr_switch_wrap">
                                            <div class="product_color_switch">
                                                    <span class="active" data-color="#87554B"
                                                          style="background-color: rgb(135, 85, 75);"></span>
                                                <span data-color="#333333"
                                                      style="background-color: rgb(51, 51, 51);"></span>
                                                <span data-color="#5FB7D4"
                                                      style="background-color: rgb(95, 183, 212);"></span>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="qty cart-quantity">

                                        <div class="ArrCount">

                                            <span style="cursor: pointer;"
                                                  onclick="addQuantity({{$data['id']}},  this.innerText, this); return false;"
                                                  class=" minus sub disblade" data-rel="cart_quantity{{$data['id']}}">-</span>

                                            <input data-value="{{$data['quantity']}}" class="cartQuantity count" type="text"
                                                   id="cart_quantity{{$data['id']}}" name="ArrCount"
                                                   value="{{$data['quantity']}}"/>

                                            <span style="cursor: pointer;"
                                                    onclick="addQuantity({{$data['id']}},  this.innerText, this); return false;"
                                                    class="plus cre disblade"
                                                    data-rel="cart_quantity{{$data['id']}}">+</span>
                                        </div>
                                    </div>

                                    <div class="cart-price">
                                        {{@$data['price']}}USD
                                    </div>

                                    <div class="cart-action">
                                        <a title="remove from cart" style="cursor:pointer" onclick='removeCart( {{$data['id']}} ); return false;'>
                                            <i  class="fa fa-trash-o"></i>
                                        </a>
                                    </div>
                                </div>

                        @php
                            $total +=$data['quantity']*$data['price'];
                        @endphp

                        @endforeach
                            <style>
                                .qty .count {
                                    color: #000;
                                    display: inline-block;
                                    vertical-align: top;
                                    font-size: 15px;
                                    font-weight: 700;
                                    line-height: 30px;
                                    padding: 0 2px;
                                    min-width: 35px;
                                    text-align: center;
                                }

                                .qty .minus,  .qty .plus {
                                    display: inline-block;
                                    vertical-align: top;
                                    color: black;
                                    width: 20px;
                                    height: 20px;
                                    font: 21px/1 Arial,sans-serif;
                                    text-align: center;
                                    border-radius: 50%;
                                    background-clip: padding-box;
                                    margin-top: 3px;
                                }
                                .minus:hover,.plus:hover{
                                    background-color: #e80000 !important;
                                }
                                /*Prevent text selection*/
                                .qty  span{
                                    -webkit-user-select: none;
                                    -moz-user-select: none;
                                    -ms-user-select: none;
                                }
                                .qty input{
                                    border: 1px solid #3333;
                                    width: 2%;
                                }
                                .qty input::-webkit-outer-spin-button,
                                .qty  input::-webkit-inner-spin-button {
                                    -webkit-appearance: none;
                                    margin: 0;
                                }
                                .qty  input:disabled{
                                    background-color:white;
                                }

                            </style>
                    @else
                        @if(empty($data['cate']) && ($data['msg'] == '' || ($data['msg'] == null)))
                            <p style="text-align: center; background-color: #f3d0db; padding: 10px 0px; color: #e05f5f;border: 1px solid transparent;">
                                Your shopping cart is currently empty</p>
                        @endif
                    @endif

                    </div><!-- list cart -->

                    @if( !empty( $cart ) )
                        <div class="cart-subtal">
                            <div class="row">
                                <div class="col-6 col-md-6 col-lg-6 col-xl-6">
                                    <h2>Tổng tiền thanh toán</h2>
                                </div>
                                <div class="col-6 col-md-6 col-lg-6 col-xl-6 text-right">
                                    <div class="cart-total">
                                        <div class="price-cart total_money">${{$total}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="checkout-page text-right">
                            <a href="" class="btn btn-danger btnCheckout"><i class="fa fa-shopping-cart btn-block"></i> Checkout</a>
                        </div>

                    @else
                        <div class="cart-subtal">
                            <div class="row">
                                <div class="col-6 col-md-6 col-lg-6 col-xl-6">
                                    <a href="/"><i class="fa fa-chevron-left"></i> Return to store</a>
                                </div>
                                <div class="col-6 col-md-6 col-lg-6 col-xl-6 text-right">
                                </div>
                            </div>
                        </div>
                    @endif



                </div>


                <div class="col-12 col-md-3 col-lg-3 col-xl-3">
                    <aside class="widget" id="widget-product">
                        <div class="widget-title">
                            <h3>Sản phẩm thường mua</h3>
                        </div>


                        @php
                            $muacung = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(4);
                        @endphp

                        <div class="product-list">
                            @foreach($muacung as $k=>$featured)
                                    @php  $item_class = 'product-wd-item';  @endphp
                                    @include( 'themeworkart::childs.product.partials.product_loop' )
                            @endforeach
                        </div>


{{--                            <div class="product-wd-item">--}}
{{--                                <a href="">--}}
{{--                                    <div class="product-avatar"><img--}}
{{--                                                src="https://via.placeholder.com/120x120?text=Product" alt=""></div>--}}
{{--                                    <div class="product-content">--}}
{{--                                        <h3>Lether Gray Tuxedo</h3>--}}
{{--                                        <div class="product_price">--}}
{{--                                            <span class="price">$55.00</span>--}}
{{--                                            <del>$95.00</del>--}}
{{--                                        </div>--}}
{{--                                        <div class="pr_switch_wrap">--}}
{{--                                            <div class="product_color_switch">--}}
{{--                                                    <span class="active" data-color="#87554B"--}}
{{--                                                          style="background-color: rgb(135, 85, 75);"></span>--}}
{{--                                                <span data-color="#333333"--}}
{{--                                                      style="background-color: rgb(51, 51, 51);"></span>--}}
{{--                                                <span data-color="#5FB7D4"--}}
{{--                                                      style="background-color: rgb(95, 183, 212);"></span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </div>--}}
                    </aside>
                </div>

                <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="product-related">
                        <div class="widget-title">
                            <h3>Sản phẩm liên quan</h3>
                        </div>


                        <div class="owl-carousel slider-product">

{{--                            <div class="product-item">--}}
{{--                                <a href="">--}}
{{--                                    <div class="product-avatar">--}}
{{--                                        <img src="https://via.placeholder.com/700x700?text=Product" alt="">--}}
{{--                                    </div>--}}
{{--                                    <div class="product-content">--}}
{{--                                        <h3>Lether Gray Tuxedo</h3>--}}
{{--                                        <div class="product_price">--}}
{{--                                            <span class="price">$55.00</span>--}}
{{--                                            <del>$95.00</del>--}}
{{--                                            <div class="on_sale">--}}
{{--                                                <span>25% Off</span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="rating_wrap">--}}
{{--                                            <div class="rating">--}}
{{--                                                <div class="product_rate" style="width:68%"></div>--}}
{{--                                            </div>--}}
{{--                                            <span class="rating_num">(15)</span>--}}
{{--                                        </div>--}}
{{--                                        <div class="pr_switch_wrap">--}}
{{--                                            <div class="product_color_switch">--}}
{{--                                                    <span class="active" data-color="#87554B"--}}
{{--                                                          style="background-color: rgb(135, 85, 75);"></span>--}}
{{--                                                <span data-color="#333333"--}}
{{--                                                      style="background-color: rgb(51, 51, 51);"></span>--}}
{{--                                                <span data-color="#5FB7D4"--}}
{{--                                                      style="background-color: rgb(95, 183, 212);"></span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </div>--}}

                            @php
                                $lienquan = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(8);
                            @endphp
                            @foreach($lienquan as $k=>$featured)
{{--                                <div class="col-12 col-lg-3 col-md-3 col-xl-3 pd-5">--}}
                                    @include( 'themeworkart::childs.product.partials.product_loop' )
{{--                                </div>--}}
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
