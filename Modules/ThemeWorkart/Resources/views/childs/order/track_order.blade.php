@extends( 'themeworkart::layouts.master' )

@section( 'main_content' )
    <div class="container">
        <div class="row">
            <div class="breadcrumbs">
                <nav aria-label="breadcrumb">
                    @include('themeworkart::partials.breadcrumb')
                </nav>
            </div>


            <div class="col-12">
                <p class="fs22 color-text">Track Oder</p>
                <p class="fs14 color-text">Enter your email and order ID to check the status of your order</p>
            </div>
            <div class="col-md-6 col-sm-12 mb-sm-5 mb-md-0 form-track-order">
                <form action="" method="post">
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" placeholder="Email ">
                    </div>
                    <div class="form-group">
                        <input type="text" name="order_id" class="form-control" placeholder="ID ">
                    </div>
                    <button type="submit" class="btn btn-orange text-dark">Subcribe</button>
                </form>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="hover">
                    <img src="css/img/Artboard%205.png" class="db" alt="image">
                </div>
            </div>
        </div>
    </div>
    <!--main-->
@endsection