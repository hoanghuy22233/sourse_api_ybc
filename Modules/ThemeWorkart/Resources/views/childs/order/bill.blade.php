@extends('themeworkart::layouts.master')

@section('main_content')
    <div class="d-none d-md-block pl-sm-0 pl-md-3">
        <div class="breadcrumbs container-custom">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item fs14"><a href="#">Cart</a></li>
                    <li class="breadcrumb-item fs14"><a href="#">Information</a></li>
                    <li class="breadcrumb-item fs14"><a href="#">Shipping</a></li>
                    <li class="breadcrumb-item fs14 active">Payment</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container-custom">
        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 info-payment" style="margin-left: 39px;">
                <div class="row" style="max-width: 583px;">
                    <p class="color-text fs18">Information</p>
                </div>
                <div class="row info-contact tbl-checkout" style="max-width: 583px;">
                    <div class="w-100 d-flex pb-3">
                        <span class="color-text fs14">Contact</span>
                        <span class="color-text fs14">{{ Session::get('cart_delivery')['bill']['user_email'] }}</span>
                        <span class="fs14" style="color: #2F80ED;">Change</span>
                    </div>
                    <div class="w-100 d-flex pt-3 pb-3">
                        <span class="color-text fs14">Ship to</span>
                        <span class="color-text fs14">{{ Session::get('cart_delivery')['bill']['user_address'] }}</span>
                        <span class="fs14" style="color: #2F80ED;">Change</span>
                    </div>
                    <div class="w-100 d-flex pt-3">
                        <span class="color-text fs14">Method</span>
                        <span class="color-text fs14">COD</span>
                        <span class="fs14" style="color: #2F80ED;">Change</span>
                    </div>
                </div>
                @if(Session::get('cart_delivery')['bill']['payment_method'] != "cod")
                <div class="row" style="max-width: 583px; margin-top: 22px; margin-bottom: 27px;">
                    <p class="color-text fs18 w-100 mb-0">Payment</p>
                    <p class="color-text fs14 mb-0">All transactions are secure and encrypted.</p>
                </div>

                <div class="row form-info-checkout" style="max-width: 583px;">
                    <form method="POST" action="{{ route('order.postBill') }}" style="width: 100%;">
                        {!! csrf_field() !!}
                        <div class="tbl-checkout shipping-method" style="padding-bottom: 0 !important;;">
                            <div class="method w-100 d-flex justify-content-between" style="padding-bottom: 12px; margin-bottom: 32px;">
                                <div class="custom-control custom-radio w-100">
                                    <input type="radio" id="creditCart" name="creditCart" checked="" class="custom-control-input">
                                    <label class="custom-control-label fs14 color-text" for="creditCart">Credit card</label>
                                </div>
                                <div class="d-flex">
                                    <img src="css/img/download%20(1).png" alt="">
                                    <img src="css/img/download%20(2).png" alt="">
                                    <img src="css/img/download%20(8).png" alt="">
                                </div>
                            </div>
                            <div class="w-100 mb-3" style="border-bottom: dashed 0.1px #B7B7B7;">
                                <div class="form-group">
                                    @if($errors->has('card.card_number'))
                                        <div class="error">{{ ucfirst($errors->first('card.card_number')) }}</div>
                                    @endif
                                    <input name="card[card_number]" type="text" class="form-control rounded-0" placeholder="Card number" style="height: 50px;">
                                </div>
                                <div class="form-group">
                                    @if($errors->has('card.card_name'))
                                        <div class="error">{{ ucfirst($errors->first('card.card_name')) }}</div>
                                    @endif
                                    <input name="card[card_name]" type="text" class="form-control rounded-0" placeholder="Name on card" style="height: 50px;">
                                </div>
                                @if($errors->has('card.card_expi'))
                                    <div class="error">{{ ucfirst($errors->first('card.card_expi')) }}</div>
                                @endif
                                @if($errors->has('card.card_code'))
                                    <div class="error">{{ ucfirst($errors->first('card.card_code')) }}</div>
                                @endif
                                <div class="form-group d-md-flex justify-content-md-between">
                                    <input name="card[card_expi]" type="text" class="form-control rounded-0 d-inline mr-sm-1" placeholder="Expiratation date (MM/YY)" style="height: 50px; width: 49%;">
                                    <input name="card[card_code]" type="text" class="form-control rounded-0 d-inline" placeholder="Security code" style="height: 50px; width: 49%;">
                                </div>
                            </div>
                            <div class="w-100">
                                <div class="custom-control custom-radio w-100">
                                    <input type="checkbox" id="customCheck1" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label fs14 color-text" for="customCheck1"><img src="css/img/download.png" alt=""></label>
                                </div>
                            </div>
                        </div>
                        <div class="row w-100 ml-1" style="margin-top: 33px; margin-bottom: 27px;">
                            <p class="color-text fs18 w-100 mb-0">Payment</p>
                            <p class="color-text fs14 mb-0">All transactions are secure and encrypted.</p>
                        </div>
                        <div class="tbl-checkout shipping-method">
                            <div class="method w-100 mb-3">
                                <div class="custom-control custom-radio w-100 pb-3">
                                    <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label fs14 color-text" for="customRadio1">Same as shipping address</label>
                                </div>
                            </div>
                            <div class="method w-100">
                                <div class="custom-control custom-radio w-100">
                                    <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label fs14 color-text" for="customRadio2">Use a different billing address</label>
                                </div>
                            </div>
                        </div>
                        <div class="submit-return d-flex justify-content-between mt-3">
                            <a href="{{ route('order.pay')  }}" class="">&lt; Return to information</a>
                            <button class="btn btn-orange btn-checkout" type="submit">Pay now</button>
                        </div>
                    </form>
                    <div class="info-more d-flex justify-content-between" style="border-top: 1px solid #ccc; width: 100%; margin-top: 24px;">
                        <a href="" class="color-text fs12">Refund policy</a>
                        <a href="" class="color-text fs12">Shipping policy</a>
                        <a href="" class="color-text fs12">Privacy policy</a>
                        <a href="" class="color-text fs12">Term of service</a>
                    </div>
                </div>
            @else
                    <form method="POST" action="{{ route('order.postBill') }}" style="width: 100%;">
                        <div class="submit-return d-flex justify-content-between mt-3">
                            <a href="{{ route('order.pay')  }}" class="">&lt; Return to information</a>
                            <button class="btn btn-orange btn-checkout" type="submit">Pay now</button>
                        </div>
                    </form>
                @endif
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 mt-sm-4 mt-md-0 ml-4">
                <p style="font-size: 18px; font-weight: 600; color: #282364;">Order Summary</p>
                @php
                    $checkPromote = false;
                    $promotion = Session::get('promotion');
                    $value_promote = 0;
                    if($promotion){
                         $checkPromote = true;
                         $value_promote = $promotion['value'];
                         $typePromote = "default";
                         if(preg_match('#%$#', $value_promote)){
                             $typePromote = "percent";
                         }
                    }


                    $carts = Cart::content();
                    $countItem = Cart::count();
                    $priceTotalBase = 0;
                    $shipping = 0;

                @endphp
                @foreach( $carts  as $cart)
                    @php
                        $product = \Modules\ThemeWorkart\Models\Product::find($cart->id);
                         $priceTotalBase += $cart->price * $cart->qty;
                    @endphp
                    <div class="wp-item-cart">
                        <div class="item-cart d-flex">
                            <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product) }}" class="d-block"><img src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($product->image, 50, 50) }}" class="" alt=""></a>
                            <div class="info-item-cart ml-3">
                                <p class="title-info-item-cart">{{ $cart->name  }}</p>
                                <div class="custom-item-cart d-md-flex justify-content-between d-none">
                                    <ul class="list-unstyled">
                                        <li>Available Products</li>
                                        <li>Portrait Canvas: 73 in</li>
                                        <li>Frame</li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li>Color white</li>
                                        <li>Size 12" x 18"</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="qty-price text-right">
                            <p>{{ $cart->qty  }} X {{ number_format($cart->price) }} vnđ</p>
                        </div>
                    </div>
                @endforeach
                @php
                    $total = $priceTotalBase + $shipping;
                    if($value_promote != 0){
                        if($typePromote == "default"){
                            $total -= $value_promote;
                        }else if($typePromote == "percent"){
                            $value_promote = preg_replace("#%#", "", $value_promote);
                            $total = $total - $total *  $value_promote / 100;
                        }
                    }
                @endphp
                <div class="pay-cart">
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Subtotal : ( {{ $countItem  }} item )</span><span>${{ number_format($priceTotalBase)  }}</span></p>
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Shipping:</span><span>$0</span></p>
                    @if($checkPromote)
                        <p class="d-flex justify-content-between title-info-item-cart">
                            <span>Promotion:</span>
                            <span>-
                                @if($typePromote == "default")
                                    ${{ number_format($value_promote) }}
                                @elseif($typePromote == "percent")
                                    {{ $value_promote }}%
                                @endif
                            </span></p>
                    @endif
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Total:</span><span>${{ number_format($total) }}</span></p>

                </div   >
            </div>


        </div>
    </div>

@endsection

@section('custom_footer')

@endsection