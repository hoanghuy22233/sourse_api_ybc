@extends('themeworkart::layouts.master')

@section('main_content')
    <div class="d-none d-md-block pl-sm-0 pl-md-3">
        <div class="breadcrumbs container-custom">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item fs14"><a href="#">Cart</a></li>
                    <li class="breadcrumb-item fs14"><a href="#">Information</a></li>
                    <li class="breadcrumb-item fs14"><a href="#">Shipping</a></li>
                    <li class="breadcrumb-item fs14 active">Payment</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container-custom">
        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 info-shipping" style="margin-left: 39px;">
                <div class="row" style="max-width: 583px;">
                    <p class="color-text fs18">Information</p>
                </div>
                <div class="row info-contact tbl-checkout" style="max-width: 583px;">
                    <div class="w-100 d-flex pb-3">
                        <span class="color-text fs14">Contact</span>
                        <span class="color-text fs14">{{ Session::get('cart_delivery')['bill']['user_email'] }}</span>
                        <span class="fs14" style="color: #2F80ED;">Change</span>
                    </div>
                    <div class="w-100 d-flex pt-3">
                        <span class="color-text fs14">Ship to</span>
                        <span class="color-text fs14">{{ Session::get('cart_delivery')['bill']['user_address'] }}</span>
                        <span class="fs14" style="color: #2F80ED;">Change</span>
                    </div>
                </div>
                <div class="row" style="max-width: 583px; margin-top: 22px;">
                    <p class="color-text fs18">Shipping method</p>
                </div>

                <div class="row" style="max-width: 583px;">
                    <form method="POST" action="{{ route('order.getBill') }}">
                        {!! csrf_field() !!}
                        <div class="tbl-checkout shipping-method">
                            <div class="method w-100 d-flex justify-content-between mb-3">
                                <div class="custom-control custom-radio w-100">
                                    <input name="pay_method" value="cod" checked type="radio" id="customRadio1"  class="custom-control-input">
                                    <label class="custom-control-label fs14 color-text" for="customRadio1">COD</label>
                                </div>

                            </div>
                            <div class="method w-100 d-flex justify-content-between mb-3">
                                <div class="custom-control custom-radio w-100">
                                    <input name="pay_method" value="other" type="radio" id="customRadio2"  class="custom-control-input">
                                    <label class="custom-control-label fs14 color-text" for="customRadio2">Free Ship Order
                                        Over
                                        100$</label>
                                </div>
                                <p class="fs14 color-text">Free</p>
                            </div>
                            <div class="method w-100 d-flex justify-content-between mb-3">
                                <div class="custom-control custom-radio w-100">
                                    <input name="pay_method" value="other" type="radio" id="customRadio2"  class="custom-control-input">
                                    <label class="custom-control-label fs14 color-text" for="customRadio2">Standard
                                        Shipping</label>
                                </div>
                                <p class="fs14 color-text">$18.98</p>
                            </div>
                            <div class="method w-100 d-flex justify-content-between mb-3">
                                <div class="custom-control custom-radio w-100">
                                    <input name="pay_method" value="other" type="radio" id="customRadio3"  class="custom-control-input">
                                    <label class="custom-control-label fs14 color-text pb-3" for="customRadio3">Priority
                                        Care
                                        Shipping - Guarantees that your package is insured
                                        against loss, damage or missing contents</label>
                                </div>
                                <p class="fs14 color-text">$18.98</p>
                            </div>
                            <div class="method w-100 d-flex justify-content-between">
                                <div class="custom-control custom-radio w-100">
                                    <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
                                    <label class="custom-control-label fs14 color-text" for="customRadio4">DHL
                                        Express</label>
                                </div>
                                <p class="fs14 color-text mb-0">$18.98</p>
                            </div>
                        </div>`
                        <div class="submit-return d-flex justify-content-between" style="margin-top: 14px;">
                            <a href="{{ route('order.postDelivery') }}" class="">&lt; Return to information</a>
                            <button class="btn btn-orange btn-checkout" type="submit">Continue to payment</button>
                        </div>
                    </form>
                    <div class="info-more d-flex justify-content-between" style="border-top: 1px solid #ccc; width: 100%; margin-top: 24px;">
                        <a href="" class="color-text fs12">Refund policy</a>
                        <a href="" class="color-text fs12">Shipping policy</a>
                        <a href="" class="color-text fs12">Privacy policy</a>
                        <a href="" class="color-text fs12">Term of service</a>
                    </div>
                </div>

            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 mt-sm-4 mt-md-0 ml-4">
                <p style="font-size: 18px; font-weight: 600; color: #282364;">Order Summary</p>
                @php
                    $checkPromote = false;
                    $promotion = Session::get('promotion');
                    $value_promote = 0;
                    if($promotion){
                         $checkPromote = true;
                         $value_promote = $promotion['value'];
                         $typePromote = "default";
                         if(preg_match('#%$#', $value_promote)){
                             $typePromote = "percent";
                         }
                    }


                    $carts = Cart::content();
                    $countItem = Cart::count();
                    $priceTotalBase = 0;
                    $shipping = 0;

                @endphp
                @foreach( $carts  as $cart)
                    @php
                        $product = \Modules\ThemeWorkart\Models\Product::find($cart->id);
                         $priceTotalBase += $cart->price * $cart->qty;
                    @endphp
                    <div class="wp-item-cart">
                        <div class="item-cart d-flex">
                            <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product) }}" class="d-block"><img src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($product->image, 50, 50) }}" class="" alt=""></a>
                            <div class="info-item-cart ml-3">
                                <p class="title-info-item-cart">{{ $cart->name  }}</p>
                                <div class="custom-item-cart d-md-flex justify-content-between d-none">
                                    <ul class="list-unstyled">
                                        <li>Available Products</li>
                                        <li>Portrait Canvas: 73 in</li>
                                        <li>Frame</li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li>Color white</li>
                                        <li>Size 12" x 18"</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="qty-price text-right">
                            <p>{{ $cart->qty  }} X {{ number_format($cart->price) }} vnđ</p>
                        </div>
                    </div>
                @endforeach
                @php
                    $total = $priceTotalBase + $shipping;
                    if($value_promote != 0){
                        if($typePromote == "default"){
                            $total -= $value_promote;
                        }else if($typePromote == "percent"){
                            $value_promote = preg_replace("#%#", "", $value_promote);
                            $total = $total - $total *  $value_promote / 100;
                        }
                    }
                @endphp
                <div class="pay-cart">
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Subtotal : ( {{ $countItem  }} item )</span><span>${{ number_format($priceTotalBase)  }}</span></p>
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Shipping:</span><span>$0</span></p>
                    <div id="promote_result">
                        @if($checkPromote)
                            <p class="d-flex justify-content-between title-info-item-cart">
                                <span>Promotion:</span>
                                <span>-
                                @if($typePromote == "default")
                                        ${{ number_format($value_promote) }}
                                    @elseif($typePromote == "percent")
                                        {{ $value_promote }}%
                                    @endif
                            </span></p>
                        @endif
                    </div>
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Total:</span><span id="total_price_result">${{ number_format($total) }}</span></p>
                    <form id="frmPromote" action="{{ route('order.addPromote')  }}" method="get">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <div class="error-promote"></div>
                            <input name="code" type="text" class="form-control rounded-0" placeholder="Enter promote code" style="height: 50px;">
                            <span onclick="ajaxPromote('{{ route('order.addPromote')  }}')" class="d-none d-lg-block">Apply</span>
                            <p onclick="ajaxPromote('{{ route('order.addPromote')  }}')" class="text-center mt-3 d-lg-none apply">Apply</p>

                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('custom_footer')

@endsection
@section('script')
    <script>
        function ajaxPromote(url) {
            var code = $("input[name='code']").val();
            if (code == '') return;
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    code: code
                },
                success: function (result) {
                    console.log(result);
                    if(result.status == 'error'){
                        $('.error-promote').text(result.error);
                    }else{
                        var html = '<p class="d-flex justify-content-between title-info-item-cart"><span>Promotion:</span><span>'+result.success.promote+'</span></p>';
                        $('.error-promote').text('');
                        $("#promote_result").html(html);
                        $("#total_price_result").html(result.success.total);
                    }
                },
                error: function () {
                    alert('Có lỗi xảy ra. Vui lòng F5 lại website!');
                }
            });
        }
    </script>
@stop