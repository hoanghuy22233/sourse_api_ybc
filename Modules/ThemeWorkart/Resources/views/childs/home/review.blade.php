<!-- Review Home-->
<div class="review-box mt-5" id="home-review">
    <div class="container">
        <h4 class="color-282364 font-weight-bold">Review</h4>
        <div>
            <div class="row no-gutters review-box-star mb-3">
                <div class="col-lg-4 col-md-12 col-12 ">
                    <div class="mr-1 review_star_title">
                        <p class="fs-26 mb-0">4.8/5</p>
                        <p class="mb-0 _icon-star">
                            <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                        </p>
                        <p class="fs-14">8475 Review</p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-3">
                    <div class="post_review mb-1 mr-1">
                        <a href="#">All <span class="pc-home">( 8734 )</span></a>
                    </div>
                    <div class="post_review mr-1 _bottom ">
                        <a href="#">2 Star  <span class="pc-home">( 283 )</span></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-3">
                    <div class="post_review mb-1 mr-1">
                        <a href="#">5 Star  <span class="pc-home">( 7883 )</span></a>
                    </div>
                    <div class="post_review  mr-1 _bottom">
                        <a href="#">1 Star  <span class="pc-home">( 183 )</span></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-3">
                    <div class="post_review mb-1 mr-1">
                        <a href="#">5 Star  <span class="pc-home">( 883 )</span></a>
                    </div>
                    <div class="post_review mr-1 _bottom _bottom_pc ">
                        <a href="#">Have comment  <span class="pc-home">( 8634 )</span></a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-3">
                    <div class="post_review mb-1 ">
                        <a href="#">5 Star  <span class="pc-home">( 783 )</span></a>
                    </div>
                    <div class="post_review _bottom _bottom_pc">
                        <a href="#">Have pictures  <span class="pc-home">(384)</span></a>
                    </div>
                </div>
            </div>
        </div>
        <!--        content review-->
        <div class="content_review">
            <div class="row no-gutters">
                <div class="col-lg-3 col-md-6 col-sm-12 col-md">
                    <!--  card comment review-->
                    <div class="card mr-2 mb-2 shadow-card-review"  onclick="document.getElementById('home01').style.display='block'">
                        <div class="row no-gutters">
                            <div class="col-lg-12 col-md-5 col-sm-5 col-5 ">
                                <div class="review_img">
                                    <img class="img-w100" src="css/img/41FRpSoOY.png" alt="Review_img">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-7 col-sm-7 col-7 ">
                                <div class="review_star pl-3">
                                    <p>Ashley Ortiz <span>check-circle</span></p>
                                    <p>
                                        <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                    </p>
                                </div>
                                <div class="review_content">
                                    <p>Omg sooo cute it looked perfect , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤</p>
                                </div>

                            </div>
                        </div>
                        <hr>
                        <div class="review_comment">
                            <div class="row no-gutters mb-2">
                                <div class="col-4">
                                    <img class="img-w60 ml-3" src="css/img/asset-2.png" alt="Avatar_img">
                                </div>
                                <div class="col-8">
                                    <p>Girl & Dogs/Cats on Color Van Tshirt, Name, Pet Breed, Hair Style can be customized TS04</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  card comment review end-->
                    <!-- Modal home-->
                    <div id="home01" class="review-modal" style="display: none;">
                        <div class="review-modal-content">
                            <div>
                                <span onclick="document.getElementById('home01').style.display='none'" class="_modal-closed">×</span>

                                <div class="content-modal">
                                    <div class="row no-gutters">
                                        <!--  img review modal-->
                                        <div class="col-lg-7 col-12 pr-2 mb-2">
                                            <img class="w-100 " src="css/img/41FRpSoOY.png" alt="Review_Product">
                                        </div>
                                        <!--  Content review modal-->
                                        <div class="col-lg-5 col-12 pr-2 _modal-right">
                                            <div class="modal-comment">
                                                <div>
                                                    <div class="review_star pl-3">
                                                        <p class="title-content-modal">Ashley Ortiz <span>check-circle</span></p>
                                                        <p class="modal-icon-app"> <span><img src="css/img/download (3).png" alt="icon">
                                                                <img src="css/img/download (4).png" alt="icon">
                                                                <img src="css/img/download (5).png" alt="icon">
                                                                <img src="css/img/download (6).png" alt="icon">
                                                            </span></p>
                                                        <p class="title-content-modal">
                                                            <svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com -->
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="review_content-modal">
                                                    <p class="pl-3">Love the idea. Will buy more , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤️</p>
                                                </div>
                                            </div>
                                            <div class="modal-comment w-100 mt-1">
                                                <div class="row no-gutters">
                                                    <div class="col-lg-3 col-4 text-center">
                                                        <img class="img_viewProduct" src="css/img/asset-2.png" alt="Avatar_img">
                                                    </div>
                                                    <div class="col-lg-9 col-8 viewProduct">
                                                        <p>LORD OF THE CATS PERSONALIZED SHIRT. <br> TS179</p>
                                                        <div class="text-center pt-2">
                                                            <a href="#">View Product</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal home end-->
                    <!--  card comment review-->
                    <div class="card mr-2 mb-2 shadow-card-review"  onclick="document.getElementById('home02').style.display='block'">
                        <div class="row no-gutters">
                            <div class="col-lg-12 col-md-5 col-sm-5 col-5 ">
                                <div class="review_img">
                                    <img class="img-w100" src="css/img/NyuklLidt.png" alt="Review_img">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-7 col-sm-7 col-7 ">
                                <div class="review_star pl-3">
                                    <p>Ashley Ortiz <span>check-circle</span></p>
                                    <p>
                                        <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                    </p>
                                </div>
                                <div class="review_content">
                                    <p>Omg sooo cute it looked perfect , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤</p>
                                </div>

                            </div>
                        </div>
                        <hr>
                        <div class="review_comment">
                            <div class="row no-gutters mb-2">
                                <div class="col-4">
                                    <img class="img-w60 ml-3" src="css/img/asset-2.png" alt="Avatar_img">
                                </div>
                                <div class="col-8">
                                    <p>Girl & Dogs/Cats on Color Van Tshirt, Name, Pet Breed, Hair Style can be customized TS04</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  card comment review end-->
                    <!-- Modal home-->
                    <div id="home02" class="review-modal" style="display: none;">
                        <div class="review-modal-content">
                            <div>
                                <span onclick="document.getElementById('home02').style.display='none'" class="_modal-closed">×</span>
                                <div class="content-modal">
                                    <div class="row no-gutters">
                                        <!--  img review modal-->
                                        <div class="col-lg-7 col-12 pr-2 mb-2">
                                            <img class="w-100 " src="css/img/NyuklLidt.png" alt="Review_Product">
                                        </div>
                                        <!--  Content review modal-->
                                        <div class="col-lg-5 col-12 pr-2 _modal-right">
                                            <div class="modal-comment">
                                                <div>
                                                    <div class="review_star pl-3">
                                                        <p class="title-content-modal">Ashley Ortiz <span>check-circle</span></p>
                                                        <p class="modal-icon-app"> <span><img src="css/img/download (3).png" alt="icon">
                                                                <img src="css/img/download (4).png" alt="icon">
                                                                <img src="css/img/download (5).png" alt="icon">
                                                                <img src="css/img/download (6).png" alt="icon">
                                                            </span></p>
                                                        <p class="title-content-modal">
                                                            <svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com -->
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="review_content-modal">
                                                    <p class="pl-3">Love the idea. Will buy more , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤️</p>
                                                </div>
                                            </div>
                                            <div class="modal-comment w-100 mt-1">
                                                <div class="row no-gutters">
                                                    <div class="col-lg-3 col-4 text-center">
                                                        <img class="img_viewProduct" src="css/img/asset-2.png" alt="Avatar_img">
                                                    </div>
                                                    <div class="col-lg-9 col-8 viewProduct">
                                                        <p>LORD OF THE CATS PERSONALIZED SHIRT. <br> TS179</p>
                                                        <div class="text-center pt-2">
                                                            <a href="#">View Product</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal home end-->


                </div>

                <!--                -->
                <div class="col-lg-3 col-md-6 col-sm-12 col-md">
                    <!--  card comment review-->
                    <div class="card mr-2 mb-2 shadow-card-review"  onclick="document.getElementById('home03').style.display='block'">
                        <div class="row no-gutters">
                            <div class="col-lg-12 col-md-5 col-sm-5 col-5 ">
                                <div class="review_img">
                                    <img class="img-w100" src="css/img/V1KJXIouK.png" alt="Review_img">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-7 col-sm-7 col-7 ">
                                <div class="review_star pl-3">
                                    <p>Ashley Ortiz <span>check-circle</span></p>
                                    <p>
                                        <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                    </p>
                                </div>
                                <div class="review_content">
                                    <p>Omg sooo cute it looked perfect , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤</p>
                                </div>

                            </div>
                        </div>
                        <hr>
                        <div class="review_comment">
                            <div class="row no-gutters mb-2">
                                <div class="col-4">
                                    <img class="img-w60 ml-3" src="css/img/asset-2.png" alt="Avatar_img">
                                </div>
                                <div class="col-8">
                                    <p>Girl & Dogs/Cats on Color Van Tshirt, Name, Pet Breed, Hair Style can be customized TS04</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  card comment review end-->
                    <!-- Modal home-->
                    <div id="home03" class="review-modal" style="display: none;">
                        <div class="review-modal-content">
                            <div>
                                <span onclick="document.getElementById('home03').style.display='none'" class="_modal-closed">×</span>

                                <div class="content-modal">
                                    <div class="row no-gutters">
                                        <!--  img review modal-->
                                        <div class="col-lg-7 col-12 pr-2 mb-2">
                                            <img class="w-100 " src="css/img/V1KJXIouK.png" alt="Review_Product">
                                        </div>
                                        <!--  Content review modal-->
                                        <div class="col-lg-5 col-12 pr-2 _modal-right">
                                            <div class="modal-comment">
                                                <div>
                                                    <div class="review_star pl-3">
                                                        <p class="title-content-modal">Ashley Ortiz <span>check-circle</span></p>
                                                        <p class="modal-icon-app"> <span><img src="css/img/download (3).png" alt="icon">
                                                                <img src="css/img/download (4).png" alt="icon">
                                                                <img src="css/img/download (5).png" alt="icon">
                                                                <img src="css/img/download (6).png" alt="icon">
                                                            </span></p>
                                                        <p class="title-content-modal">
                                                            <svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com -->
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="review_content-modal">
                                                    <p class="pl-3">Love the idea. Will buy more , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤️</p>
                                                </div>
                                            </div>
                                            <div class="modal-comment w-100 mt-1">
                                                <div class="row no-gutters">
                                                    <div class="col-lg-3 col-4 text-center">
                                                        <img class="img_viewProduct" src="css/img/asset-2.png" alt="Avatar_img">
                                                    </div>
                                                    <div class="col-lg-9 col-8 viewProduct">
                                                        <p>LORD OF THE CATS PERSONALIZED SHIRT. <br> TS179</p>
                                                        <div class="text-center pt-2">
                                                            <a href="#">View Product</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal home end-->
                    <!--  card comment review-->
                    <div class="card mr-2 mb-2 shadow-card-review"  onclick="document.getElementById('home04').style.display='block'">
                        <div class="row no-gutters">
                            <div class="col-lg-12 col-md-5 col-sm-5 col-5 ">
                                <div class="review_img">
                                    <img class="img-w100" src="css/img/Mask.png" alt="Review_img">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-7 col-sm-7 col-7 ">
                                <div class="review_star pl-3">
                                    <p>Ashley Ortiz <span>check-circle</span></p>
                                    <p>
                                        <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                    </p>
                                </div>
                                <div class="review_content">
                                    <p>Omg sooo cute it looked perfect , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤</p>
                                </div>

                            </div>
                        </div>
                        <hr>
                        <div class="review_comment">
                            <div class="row no-gutters mb-2">
                                <div class="col-4">
                                    <img class="img-w60 ml-3" src="css/img/asset-2.png" alt="Avatar_img">
                                </div>
                                <div class="col-8">
                                    <p>Girl & Dogs/Cats on Color Van Tshirt, Name, Pet Breed, Hair Style can be customized TS04</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  card comment review end-->
                    <!-- Modal home-->
                    <div id="home04" class="review-modal" style="display: none;">
                        <div class="review-modal-content">
                            <div>
                                <span onclick="document.getElementById('home04').style.display='none'" class="_modal-closed">×</span>
                                <div class="content-modal">
                                    <div class="row no-gutters">
                                        <!--  img review modal-->
                                        <div class="col-lg-7 col-12 pr-2 mb-2">
                                            <img class="w-100 " src="css/img/Mask.png" alt="Review_Product">
                                        </div>
                                        <!--  Content review modal-->
                                        <div class="col-lg-5 col-12 pr-2 _modal-right">
                                            <div class="modal-comment">
                                                <div>
                                                    <div class="review_star pl-3">
                                                        <p class="title-content-modal">Ashley Ortiz <span>check-circle</span></p>
                                                        <p class="modal-icon-app"> <span><img src="css/img/download (3).png" alt="icon">
                                                                <img src="css/img/download (4).png" alt="icon">
                                                                <img src="css/img/download (5).png" alt="icon">
                                                                <img src="css/img/download (6).png" alt="icon">
                                                            </span></p>
                                                        <p class="title-content-modal">
                                                            <svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com -->
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="review_content-modal">
                                                    <p class="pl-3">Love the idea. Will buy more , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤️</p>
                                                </div>
                                            </div>
                                            <div class="modal-comment w-100 mt-1">
                                                <div class="row no-gutters">
                                                    <div class="col-lg-3 col-4 text-center">
                                                        <img class="img_viewProduct" src="css/img/asset-2.png" alt="Avatar_img">
                                                    </div>
                                                    <div class="col-lg-9 col-8 viewProduct">
                                                        <p>LORD OF THE CATS PERSONALIZED SHIRT. <br> TS179</p>
                                                        <div class="text-center pt-2">
                                                            <a href="#">View Product</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal home end-->


                </div>
                <!--                -->
                <div class="col-lg-3 col-md-6 col-sm-12 col-md">
                    <!--  card comment review-->
                    <div class="card mr-2 mb-2 shadow-card-review"  onclick="document.getElementById('home05').style.display='block'">
                        <div class="row no-gutters">
                            <div class="col-lg-12 col-md-5 col-sm-5 col-5 ">
                                <div class="review_img">
                                    <img class="img-w100" src="css/img/EkerWnLBkF.png" alt="Review_img">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-7 col-sm-7 col-7 ">
                                <div class="review_star pl-3">
                                    <p>Ashley Ortiz <span>check-circle</span></p>
                                    <p>
                                        <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                    </p>
                                </div>
                                <div class="review_content">
                                    <p>Omg sooo cute it looked perfect , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤</p>
                                </div>

                            </div>
                        </div>
                        <hr>
                        <div class="review_comment">
                            <div class="row no-gutters mb-2">
                                <div class="col-4">
                                    <img class="img-w60 ml-3" src="css/img/asset-2.png" alt="Avatar_img">
                                </div>
                                <div class="col-8">
                                    <p>Girl & Dogs/Cats on Color Van Tshirt, Name, Pet Breed, Hair Style can be customized TS04</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  card comment review end-->
                    <!-- Modal home-->
                    <div id="home05" class="review-modal" style="display: none;">
                        <div class="review-modal-content">
                            <div>
                                <span onclick="document.getElementById('home05').style.display='none'" class="_modal-closed">×</span>

                                <div class="content-modal">
                                    <div class="row no-gutters">
                                        <!--  img review modal-->
                                        <div class="col-lg-7 col-12 pr-2 mb-2">
                                            <img class="w-100 " src="css/img/EkerWnLBkF.png" alt="Review_Product">
                                        </div>
                                        <!--  Content review modal-->
                                        <div class="col-lg-5 col-12 pr-2 _modal-right">
                                            <div class="modal-comment">
                                                <div>
                                                    <div class="review_star pl-3">
                                                        <p class="title-content-modal">Ashley Ortiz <span>check-circle</span></p>
                                                        <p class="modal-icon-app"> <span><img src="css/img/download (3).png" alt="icon">
                                                                <img src="css/img/download (4).png" alt="icon">
                                                                <img src="css/img/download (5).png" alt="icon">
                                                                <img src="css/img/download (6).png" alt="icon">
                                                            </span></p>
                                                        <p class="title-content-modal">
                                                            <svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com -->
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="review_content-modal">
                                                    <p class="pl-3">Love the idea. Will buy more , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤️</p>
                                                </div>
                                            </div>
                                            <div class="modal-comment w-100 mt-1">
                                                <div class="row no-gutters">
                                                    <div class="col-lg-3 col-4 text-center">
                                                        <img class="img_viewProduct" src="css/img/asset-2.png" alt="Avatar_img">
                                                    </div>
                                                    <div class="col-lg-9 col-8 viewProduct">
                                                        <p>LORD OF THE CATS PERSONALIZED SHIRT. <br> TS179</p>
                                                        <div class="text-center pt-2">
                                                            <a href="#">View Product</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal home end-->
                    <!--  card comment review-->
                    <div class="card mr-2 mb-2 shadow-card-review"  onclick="document.getElementById('home06').style.display='block'">
                        <div class="row no-gutters">
                            <div class="col-lg-12 col-md-5 col-sm-5 col-5 ">
                                <div class="review_img">
                                    <img class="img-w100" src="css/img/V1KJXIouK.png" alt="Review_img">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-7 col-sm-7 col-7 ">
                                <div class="review_star pl-3">
                                    <p>Ashley Ortiz <span>check-circle</span></p>
                                    <p>
                                        <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                    </p>
                                </div>
                                <div class="review_content">
                                    <p>Omg sooo cute it looked perfect , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤</p>
                                </div>

                            </div>
                        </div>
                        <hr>
                        <div class="review_comment">
                            <div class="row no-gutters mb-2">
                                <div class="col-4">
                                    <img class="img-w60 ml-3" src="css/img/asset-2.png" alt="Avatar_img">
                                </div>
                                <div class="col-8">
                                    <p>Girl & Dogs/Cats on Color Van Tshirt, Name, Pet Breed, Hair Style can be customized TS04</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  card comment review end-->
                    <!-- Modal home-->
                    <div id="home06" class="review-modal" style="display: none;">
                        <div class="review-modal-content">
                            <div>
                                <span onclick="document.getElementById('home06').style.display='none'" class="_modal-closed">×</span>
                                <div class="content-modal">
                                    <div class="row no-gutters">
                                        <!--  img review modal-->
                                        <div class="col-lg-7 col-12 pr-2 mb-2">
                                            <img class="w-100 " src="css/img/V1KJXIouK.png" alt="Review_Product">
                                        </div>
                                        <!--  Content review modal-->
                                        <div class="col-lg-5 col-12 pr-2 _modal-right">
                                            <div class="modal-comment">
                                                <div>
                                                    <div class="review_star pl-3">
                                                        <p class="title-content-modal">Ashley Ortiz <span>check-circle</span></p>
                                                        <p class="modal-icon-app"> <span><img src="css/img/download (3).png" alt="icon">
                                                                <img src="css/img/download (4).png" alt="icon">
                                                                <img src="css/img/download (5).png" alt="icon">
                                                                <img src="css/img/download (6).png" alt="icon">
                                                            </span></p>
                                                        <p class="title-content-modal">
                                                            <svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com -->
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="review_content-modal">
                                                    <p class="pl-3">Love the idea. Will buy more , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤️</p>
                                                </div>
                                            </div>
                                            <div class="modal-comment w-100 mt-1">
                                                <div class="row no-gutters">
                                                    <div class="col-lg-3 col-4 text-center">
                                                        <img class="img_viewProduct" src="css/img/asset-2.png" alt="Avatar_img">
                                                    </div>
                                                    <div class="col-lg-9 col-8 viewProduct">
                                                        <p>LORD OF THE CATS PERSONALIZED SHIRT. <br> TS179</p>
                                                        <div class="text-center pt-2">
                                                            <a href="#">View Product</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal home end-->


                </div>
                <!--                -->
                <div class="col-lg-3 col-md-6 col-sm-12 col-md">
                    <!--  card comment review-->
                    <div class="card mb-2 mr-2 shadow-card-review"  onclick="document.getElementById('home07').style.display='block'">
                        <div class="row no-gutters">
                            <div class="col-lg-12 col-md-5 col-sm-5 col-5 ">
                                <div class="review_img">
                                    <img class="img-w100" src="css/img/Ek2sjSsOK.png" alt="Review_img">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-7 col-sm-7 col-7 ">
                                <div class="review_star pl-3">
                                    <p>Ashley Ortiz <span>check-circle</span></p>
                                    <p>
                                        <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                    </p>
                                </div>
                                <div class="review_content">
                                    <p>Omg sooo cute it looked perfect , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤</p>
                                </div>

                            </div>
                        </div>
                        <hr>
                        <div class="review_comment">
                            <div class="row no-gutters mb-2">
                                <div class="col-4">
                                    <img class="img-w60 ml-3" src="css/img/asset-2.png" alt="Avatar_img">
                                </div>
                                <div class="col-8">
                                    <p>Girl & Dogs/Cats on Color Van Tshirt, Name, Pet Breed, Hair Style can be customized TS04</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  card comment review end-->
                    <!-- Modal home-->
                    <div id="home07" class="review-modal" style="display: none;">
                        <div class="review-modal-content">
                            <div>
                                <span onclick="document.getElementById('home07').style.display='none'" class="_modal-closed">×</span>

                                <div class="content-modal">
                                    <div class="row no-gutters">
                                        <!--  img review modal-->
                                        <div class="col-lg-7 col-12 pr-2 mb-2">
                                            <img class="w-100 " src="css/img/Ek2sjSsOK.png" alt="Review_Product">
                                        </div>
                                        <!--  Content review modal-->
                                        <div class="col-lg-5 col-12 pr-2 _modal-right">
                                            <div class="modal-comment">
                                                <div>
                                                    <div class="review_star pl-3">
                                                        <p class="title-content-modal">Ashley Ortiz <span>check-circle</span></p>
                                                        <p class="modal-icon-app"> <span><img src="css/img/download (3).png" alt="icon">
                                                                <img src="css/img/download (4).png" alt="icon">
                                                                <img src="css/img/download (5).png" alt="icon">
                                                                <img src="css/img/download (6).png" alt="icon">
                                                            </span></p>
                                                        <p class="title-content-modal">
                                                            <svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com -->
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="review_content-modal">
                                                    <p class="pl-3">Love the idea. Will buy more , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤️</p>
                                                </div>
                                            </div>
                                            <div class="modal-comment w-100 mt-1">
                                                <div class="row no-gutters">
                                                    <div class="col-lg-3 col-4 text-center">
                                                        <img class="img_viewProduct" src="css/img/asset-2.png" alt="Avatar_img">
                                                    </div>
                                                    <div class="col-lg-9 col-8 viewProduct">
                                                        <p>LORD OF THE CATS PERSONALIZED SHIRT. <br> TS179</p>
                                                        <div class="text-center pt-2">
                                                            <a href="#">View Product</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal home end-->
                    <!--  card comment review-->
                    <div class="card mb-2 mr-2 shadow-card-review"  onclick="document.getElementById('home08').style.display='block'">
                        <div class="row no-gutters">
                            <div class="col-lg-12 col-md-5 col-sm-5 col-5 ">
                                <div class="review_img">
                                    <img class="img-w100" src="css/img/V1KJXIouK.png" alt="Review_img">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-7 col-sm-7 col-7 ">
                                <div class="review_star pl-3">
                                    <p>Ashley Ortiz <span>check-circle</span></p>
                                    <p>
                                        <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                    </p>
                                </div>
                                <div class="review_content">
                                    <p>Omg sooo cute it looked perfect , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤</p>
                                </div>

                            </div>
                        </div>
                        <hr>
                        <div class="review_comment">
                            <div class="row no-gutters mb-2">
                                <div class="col-4">
                                    <img class="img-w60 ml-3" src="css/img/asset-2.png" alt="Avatar_img">
                                </div>
                                <div class="col-8">
                                    <p>Girl & Dogs/Cats on Color Van Tshirt, Name, Pet Breed, Hair Style can be customized TS04</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--  card comment review end-->
                    <!-- Modal home-->
                    <div id="home08" class="review-modal" style="display: none;">
                        <div class="review-modal-content">
                            <div>
                                <span onclick="document.getElementById('home08').style.display='none'" class="_modal-closed">×</span>
                                <div class="content-modal">
                                    <div class="row no-gutters">
                                        <!--  img review modal-->
                                        <div class="col-lg-7 col-12 pr-2 mb-2">
                                            <img class="w-100 " src="css/img/V1KJXIouK.png" alt="Review_Product">
                                        </div>
                                        <!--  Content review modal-->
                                        <div class="col-lg-5 col-12 pr-2 _modal-right">
                                            <div class="modal-comment">
                                                <div>
                                                    <div class="review_star pl-3">
                                                        <p class="title-content-modal">Ashley Ortiz <span>check-circle</span></p>
                                                        <p class="modal-icon-app"> <span><img src="css/img/download (3).png" alt="icon">
                                                                <img src="css/img/download (4).png" alt="icon">
                                                                <img src="css/img/download (5).png" alt="icon">
                                                                <img src="css/img/download (6).png" alt="icon">
                                                            </span></p>
                                                        <p class="title-content-modal">
                                                            <svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com --><svg class="svg-inline--fa fa-star fa-w-18" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com -->
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="review_content-modal">
                                                    <p class="pl-3">Love the idea. Will buy more , first time ordering from here so I’m sooo pleased with how it came out my best friend loved it sooo much ❤️</p>
                                                </div>
                                            </div>
                                            <div class="modal-comment w-100 mt-1">
                                                <div class="row no-gutters">
                                                    <div class="col-lg-3 col-4 text-center">
                                                        <img class="img_viewProduct" src="css/img/asset-2.png" alt="Avatar_img">
                                                    </div>
                                                    <div class="col-lg-9 col-8 viewProduct">
                                                        <p>LORD OF THE CATS PERSONALIZED SHIRT. <br> TS179</p>
                                                        <div class="text-center pt-2">
                                                            <a href="#">View Product</a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal home end-->


                </div>
            </div>
        </div>
        <div class="button_review">
            <div class="button_Show_review ">
                <a href="#">Show more reviews</a>
            </div>
        </div>
    </div>
</div>