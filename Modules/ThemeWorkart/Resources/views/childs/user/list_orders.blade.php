@extends('themeworkart::layouts.master')

@section('main_content');
<main id="main">
    <section id="category-page">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3 col-lg-3 col-xl-3">
                    <div class="profile-user">
                        <div class="profile-avatar"><img src="https://via.placeholder.com/120x120?text=Avatar"
                                                         alt=""></div>
                        <div class="profile-content">
                            <h5>Duong Dinh</h5>
                            <a href=""><i class="fa fa-pencil"></i> Sửa hồ sơ</a>
                        </div>
                    </div>
                    <div class="profile-list">
                        <a href="">Tài khoản của tôi</a>
                        <a href="page-21.html">Đơn mua</a>
                    </div>
                </div>
                <div class="col-12 col-md-9 col-lg-9 col-xl-9">
                    <div class="tab-product">
                        <ul>
                            <li><a href="#all">Tất cả</a></li>
                            <li><a href="#checkout">Chờ xác nhận</a></li>
                            <li><a href="#processed">Processed</a></li>
                            <li><a href="#shipped">Shipped</a></li>
                            <li><a href="#delivered">Delivered</a></li>
                            <li><a href="#cancel">Đã huỷ</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div id="all" class="tab-item">
                            <div class="list-cart cart-1">
                                <div class="cart-item">
                                    <a href="">
                                        <div class="cart-avatar"><img src="https://via.placeholder.com/320x320?text=Product" alt=""></div>
                                        <div class="cart-content">
                                            <h3>Lether Gray Tuxedo</h3>
                                            <div class="product_price">
                                                <span class="price">$55.00</span>
                                                <del>$95.00</del>
                                            </div>
                                            <div class="pr_switch_wrap">
                                                <div class="product_color_switch">
                                                    <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                                                    <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                                                    <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cart-price" style="text-align: right">
                                            $55.00 USD
                                        </div>
                                        <div class="order-action">
                                            <a href="page-21.html" class="btn btn-success btn-sm">Chi tiết đơn hàng</a>
                                            <a href="" class="btn btn-danger btn-sm">Huỷ đơn hàng</a>
                                        </div>
                                    </a>
                                </div>
                                <div class="cart-item">
                                    <a href="">
                                        <div class="cart-avatar"><img src="https://via.placeholder.com/320x320?text=Product" alt=""></div>
                                        <div class="cart-content">
                                            <h3>Lether Gray Tuxedo</h3>
                                            <div class="product_price">
                                                <span class="price">$55.00</span>
                                                <del>$95.00</del>
                                            </div>
                                            <div class="pr_switch_wrap">
                                                <div class="product_color_switch">
                                                    <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                                                    <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                                                    <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cart-price" style="text-align: right">
                                            $55.00 USD
                                        </div>
                                        <div class="order-action">
                                            <a href="page-21.html" class="btn btn-success btn-sm">Chi tiết đơn hàng</a>
                                            <a href="" class="btn btn-danger btn-sm">Huỷ đơn hàng</a>
                                        </div>
                                    </a>
                                </div>
                                <div class="cart-item">
                                    <a href="">
                                        <div class="cart-avatar"><img src="https://via.placeholder.com/320x320?text=Product" alt=""></div>
                                        <div class="cart-content">
                                            <h3>Lether Gray Tuxedo</h3>
                                            <div class="product_price">
                                                <span class="price">$55.00</span>
                                                <del>$95.00</del>
                                            </div>
                                            <div class="pr_switch_wrap">
                                                <div class="product_color_switch">
                                                    <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                                                    <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                                                    <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cart-price" style="text-align: right">
                                            $55.00 USD
                                        </div>
                                        <div class="order-action">
                                            <a href="page-21.html" class="btn btn-success btn-sm">Chi tiết đơn hàng</a>
                                            <a href="" class="btn btn-danger btn-sm">Huỷ đơn hàng</a>
                                        </div>
                                    </a>
                                </div>
                                <div class="cart-item">
                                    <a href="">
                                        <div class="cart-avatar"><img src="https://via.placeholder.com/320x320?text=Product" alt=""></div>
                                        <div class="cart-content">
                                            <h3>Lether Gray Tuxedo</h3>
                                            <div class="product_price">
                                                <span class="price">$55.00</span>
                                                <del>$95.00</del>
                                            </div>
                                            <div class="pr_switch_wrap">
                                                <div class="product_color_switch">
                                                    <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                                                    <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                                                    <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cart-price" style="text-align: right">
                                            $55.00 USD
                                        </div>
                                        <div class="order-action">
                                            <a href="page-21.html" class="btn btn-success btn-sm">Chi tiết đơn hàng</a>
                                            <a href="" class="btn btn-danger btn-sm">Huỷ đơn hàng</a>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div id="checkout" class="tab-item">
                            <div class="alert alert-danger">Đơn đã mua trống</div>
                        </div>
                        <div id="processed" class="tab-item">
                            <div class="alert alert-danger">Đơn đã mua trống</div>
                        </div>
                        <div id="shipped" class="tab-item">
                            <div class="alert alert-danger">Đơn đã mua trống</div>
                        </div>
                        <div id="delivered" class="tab-item">
                            <div class="alert alert-danger">Đơn đã mua trống</div>
                        </div>
                        <div id="cancel" class="tab-item">
                            <div class="alert alert-danger">Đơn đã mua trống</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!--main-->
@endsection
