@extends('themeworkart::layouts.master')

@section('main_content')
    <main id="main">
        <section id="category-page">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                     @include('themeworkart::partials.breadcrumb')
                     <!--breadcrumb-->
                        <div class="page-login">
                            <div class="row">
                                <div class="col-12 col-md-8 offset-md-2">
                                    <form class="form-login" method="post" action="/dang-ky">
                                        <h2>Tạo tài khoản mới</h2>
                                        <div class="row">
                                            <div class="col-12 col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="first_name" placeholder="Họ" class="form-control input-text">
                                                    @if($errors->has('first_name'))
                                                        <p style="color: red"><b>{{ $errors->first('first_name') }}</b></p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="name" placeholder="Tên" class="form-control input-text">
                                                    @if($errors->has('name'))
                                                        <p style="color: red"><b>{{ $errors->first('name') }}</b></p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <div class="form-group">
                                                    <input type="email" name="email" placeholder="Email" class="form-control input-text">
                                                    @if($errors->has('email'))
                                                        <p style="color: red"><b>{{ $errors->first('email') }}</b></p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6">
                                                <div class="form-group">
                                                    <input type="password" name="password" placeholder="Mật khẩu" class="form-control input-text">
                                                    @if($errors->has('password'))
                                                        <p style="color: red"><b>{{ $errors->first('password') }}</b></p>
                                                    @endif
                                                </div>
                                                {!! csrf_field() !!}

                                            </div>
                                            <div class="col-12 col-md-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-12 col-md-3 col-lg-3">
                                                            <button type="submit" class="btn btn-success" id="">Đăng
                                                                ký
                                                            </button>
                                                        </div>
                                                        <div class="col-12 col-md-9 col-lg-9">
                                                            <div class="login-text">
                                                                Bạn có tài khoản <a href="/my-account">Đăng nhập</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
