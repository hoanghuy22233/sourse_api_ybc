@extends('themeworkart::layouts.master')

@section('main_content')
    <main id="main">
        <section id="category-page">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-3 col-lg-3 col-xl-3">
                        <div class="profile-user">
                            <div class="profile-avatar"><img src="https://via.placeholder.com/120x120?text=Avatar" alt=""></div>
                            <div class="profile-content">
                                <h5>Duong Dinh</h5>
                                <a href=""><i class="fa fa-pencil"></i> Sửa hồ sơ</a>
                            </div>
                        </div>
                        <div class="profile-list">
                            <a href="">Tài khoản của tôi</a>
                            <a href="page-21.html">Đơn mua</a>
                        </div>
                    </div>
                    <div class="col-12 col-md-9 col-lg-9 col-xl-9">
                        <div class="order-title" style="text-align: left !important;">
                            <h2 style="text-align: left !important;">Trạng thái đơn hàng</h2>
                        </div>
                        <div class="order-proccess">
                            <div class="order-icon order-v active">
                                <i class="fa fa-indent"></i>
                                <label for="">Đã xác nhân<br>23/9</label>
                            </div>
                            <div class="order-arrow order-v active"></div>
                            <div class="order-icon order-v active">
                                <i class="fa fa-credit-card-alt"></i>
                                <label for="">Đóng gói hàng<br>24/9</label>
                            </div>
                            <div class="order-arrow order-v active"></div>
                            <div class="order-icon order-v active">
                                <i class="fa fa-car"></i>
                                <label for="">Đang vận chuyển<br>25/9</label>
                            </div>
                            <div class="order-arrow order-v active"></div>
                            <div class="order-icon order-v">
                                <i class="fa fa-ambulance"></i>
                                <label for="">Thành công</label>
                            </div>
                        </div>
                        <div class="order-body-address">
                            <div class="order-title" style="text-align: left !important;">
                                <h2 style="text-align: left !important;">Địa chỉ nhận hàng</h2>
                            </div>
                            <p>Nguyễn văn A</p>
                            <p>1234567890</p>
                            <p>Xóm 4 - thôn lương - tri phương - tiên du - bắc ninh</p>
                        </div>
                        <div class="order-body-information">
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <h5>J&amp;T Express <br> 8110063153130</h5>
                                </div>
                                <div class="col-12 col-md-8">
                                    <p>24/11/2020: Tiếp nhận đơn hàng</p>
                                    <p>24/11/2020: Tiếp nhận đơn hàng</p>
                                    <p>24/11/2020: Tiếp nhận đơn hàng</p>
                                    <p>24/11/2020: Tiếp nhận đơn hàng</p>
                                    <p>24/11/2020: Tiếp nhận đơn hàng</p>
                                    <p>24/11/2020: Tiếp nhận đơn hàng</p>
                                </div>
                            </div>
                        </div>
                        <div class="order-cart">
                            <div class="list-cart cart-1">
                                <div class="cart-item">
                                    <a href="">
                                        <div class="cart-avatar"><img src="https://via.placeholder.com/320x320?text=Product" alt=""></div>
                                        <div class="cart-content">
                                            <h3>Lether Gray Tuxedo</h3>
                                            <div class="product_price">
                                                <span class="price">$55.00</span>
                                                <del>$95.00</del>
                                            </div>
                                            <div class="pr_switch_wrap">
                                                <div class="product_color_switch">
                                                    <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                                                    <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                                                    <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cart-price" style="text-align: right">
                                            $55.00 USD
                                        </div>
                                    </a>
                                </div>
                                <div class="cart-item">
                                    <a href="">
                                        <div class="cart-avatar"><img src="https://via.placeholder.com/320x320?text=Product" alt=""></div>
                                        <div class="cart-content">
                                            <h3>Lether Gray Tuxedo</h3>
                                            <div class="product_price">
                                                <span class="price">$55.00</span>
                                                <del>$95.00</del>
                                            </div>
                                            <div class="pr_switch_wrap">
                                                <div class="product_color_switch">
                                                    <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>
                                                    <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>
                                                    <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cart-price" style="text-align: right">
                                            $55.00 USD
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="order-total-body">
                            <table>
                                <tbody><tr>
                                    <td>Tổng tiền: </td>
                                    <td>$110 USD</td>
                                </tr>
                                <tr>
                                    <td>Vận chuyển: </td>
                                    <td>$10 USD</td>
                                </tr>
                                <tr>
                                    <td>Tổng tiền thanh toán: </td>
                                    <td>$120 USD</td>
                                </tr>
                                </tbody></table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
