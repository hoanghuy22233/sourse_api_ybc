@extends('themeworkart::layouts.master')


@section('main_content')

    <div class="row">
        <div class="col-12">
            <p class="text-md-center text-sm-left fs32" style="color: #282364">Forgot password</p>
        </div>
        @if (session('success'))
            <div class="alert bg-success" role="alert">
                <p style="color: red"><b>{!!session('success')!!}</b></p>
            </div>
        @endif
        <form class="form-login col-12" method="post">
            <div class="row">
                <div class="col-3 d-none d-md-block"></div>
                <div class="col-9">
                    <input type="email" name="email" class="form-control rounded-0"
                           placeholder="Email address">
                    @if($errors->has('email'))
                        <p style="color: red"><b>{{ $errors->first('email') }}</b></p>
                    @endif
                </div>

            </div>
            <div class="row login_submit_forget_pass" style="margin-top: 21px;">
                <div class="col-3 d-none d-md-block"></div>
                <div class="col-2">
                    <button type="submit" class="form-control btn-orange rounded-0 d-inline" style="font-size: 15px;
                    font-weight: bold;
                    color: #282364;
                    width: 123px;
                    height: 50px;">Send</button>

                </div>
                <div class="col-md-7 col-sm-12">
                    <p class="mb-0">New Customer? <a href="/dang-ky" class="ml-1">Create account</a></p>
                </div>
            </div>
        </form>
    </div>


    <div class="row" style="margin-top: 46px; margin-bottom: 30px;">
        <div class="col-12 d-none d-md-block">
            <div class="row">
                <div class="col-3 d-block"></div>
                <div class="col-3 d-block mb-2 pl-0 ml-3 pr-0 mr-3 line-login"
                     style="max-width: 230px !important;"></div>
                <div class="d-inline-block mr-3" id="or_login_with">Or login with</div>
                <div class="col-3 d-block mb-2 line-login" style="max-width: 230px !important;"></div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-3 d-none d-md-block"></div>
        <div class="d-flex justify-content-around" style="width: 585px;">
            <div class="login-with">
                <a href=""><img src="css/img/facebook-logo-in-circular-shape.png" alt=""></a>
                <a href="" class="">Facebook</a>
            </div>
            <div class="login-with">
                <a href=""><img src="css/img/1200px-Google__G__Logo.svg.png" alt=""></a>
                <a href="">Google</a>
            </div>
        </div>
    </div>
    <div style="margin-bottom: 120px"></div>
@endsection
