<div style="text-align: center"><h3>Xin Chào: {{$user_name}}</h3></div>
<div style="text-align: center"><h3>Thông tin đơn hàng của bạn</h3></div>
<div class="container-table100">
    <div class="wrap-table100">
        <div class="table100 ver1">
            <div class="wrap-table100-nextcols js-pscroll ps ps--active-x" style="width: 80%; margin: auto;">
                <div class="table100-nextcols">
                    <table style="border-collapse: collapse; width: 100%;margin: auto; border: 1px solid gray">
                        <thead>
                        <tr style="border: 1px solid gray;" class="row100 head">
                            <th style="border: 1px solid gray;" class="cell100 column2">Tên sản phẩm</th>
                            <th style="border: 1px solid gray;" class="cell100 column3">Số lượng</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Giá</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($getCart as $cart)

                            <tr style="border: 1px solid red; text-align: center;" class="row100 body">
                                <td style="border: 1px solid gray;" class="cell100 column2">{{$cart->name }}</td>
                                <td style="border: 1px solid gray;" class="cell100 column3">{{$cart->qty }}</td>
                                <td style="border: 1px solid gray;"
                                    class="cell100 column4">{{number_format($cart->price, 0, '.', '.')}} <sup>đ</sup>
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <?php
                    $totalPrice=0;

                    ?>

                </div>
                <div class="ps__rail-x" style="width: 563px; left: 0px; bottom: 10px;">
                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 160px;"></div>
                </div>
                <div class="ps__rail-y" style="top: 0px; right: 0px;">
                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
