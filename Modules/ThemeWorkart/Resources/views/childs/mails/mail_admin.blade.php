<div style="text-align: center"><h3>Chào admin: Thông báo từ {{$user_name}}</h3></div>
<div style="text-align: center"><h3>Thông tin đơn hàng của {{$user_name}}</h3></div>
<div style="text-align: center"><h4><a href="{{$_SERVER['SERVER_NAME']}}/admin/bill">Xem chi tiết đơn hàng</a></h4>
</div>
<div class="container-table100">
    <div class="wrap-table100">
        <div class="table100 ver1">
            <div class="wrap-table100-nextcols js-pscroll ps ps--active-x" style="width: 80%; margin: auto;">
                <div class="table100-nextcols">
                    <table style="border-collapse: collapse; width: 100%;margin: auto; border: 1px solid gray">
                        <thead>
                        <tr style="border: 1px solid gray;" class="row100 head">
                            <th style="border: 1px solid gray;" class="cell100 column2">Tên sản phẩm</th>
                            <th style="border: 1px solid gray;" class="cell100 column3">Số lượng</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Giá</th>
                            <th>Quà tặng</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($getCart as $cart)
                            <tr style="border: 1px solid red; text-align: center;" class="row100 body">
                                <td style="border: 1px solid gray;" class="cell100 column2">{{$cart['name']}}</td>
                                <td style="border: 1px solid gray;" class="cell100 column3">{{$cart['quantity']}}</td>
                                <td style="border: 1px solid gray;"
                                    class="cell100 column4">{{number_format($cart['price'], 0, '.', '.')}} <sup>đ</sup>
                                </td>
                                <td style="border: 1px solid gray;" class="qua-tang">

                                    <?php
                                    $gifts = explode(',', $cart['gift']);
                                    $prd_gifts = \Modules\ThemeWorkart\Models\Product::whereIn('id', $gifts)->get();

                                    ?>
                                    @if(!empty($prd_gifts))
                                        @include('themeworkart::partials.qua_tang')

                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <?php
                    $totalPrice = 0;
                    foreach ($prd_gifts as $data) {
                        $totalPrice += $data['base_price'];
                    }
                    $prd_category_id = (int)@\Modules\ThemeWorkart\Models\Product::find($cart['id'])->category->gift_value_max;
                    if ($totalPrice > $prd_category_id) {
                        $totalPrice -= $prd_category_id;
                    }
                    ?>
                    <div style="text-align: right; border: 1px solid gray; border-top: none;"><p
                                style="padding: 10px; margin: 0">
                            Tổng: {{number_format($total_price+$totalPrice, 0, '.', '.')}}
                            <sup>đ</sup></p></div>
                </div>
                <div class="ps__rail-x" style="width: 563px; left: 0px; bottom: 10px;">
                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 160px;"></div>
                </div>
                <div class="ps__rail-y" style="top: 0px; right: 0px;">
                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="text-align: center"><h4>Thông tin khách hàng</h4></div>
<div class="container-table100">
    <div class="wrap-table100">
        <div class="table100 ver1">
            <div class="wrap-table100-nextcols js-pscroll ps ps--active-x" style="width: 80%; margin: auto;">
                <div class="table100-nextcols">
                    <table style="border-collapse: collapse; width: 100%;margin: auto; border: 1px solid gray">
                        <thead>
                        <tr style="border: 1px solid gray;" class="row100 head">
                            <th style="border: 1px solid gray;" class="cell100 column2">Họ & tên</th>
                            <th style="border: 1px solid gray;" class="cell100 column3">SĐT</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Email</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Địa chỉ</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Tỉnh / Thành phố</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Quận - Huyện</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Phương thức thanh toán</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Yêu cầu khác</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="border: 1px solid gray;" class="cell100 column4">{{ @$request->Name }}</td>
                            <td style="border: 1px solid gray;" class="cell100 column4">{{ @$user_tel }}</td>
                            <td style="border: 1px solid gray;" class="cell100 column4">{{ @$user_email }}</td>
                            <td style="border: 1px solid gray;" class="cell100 column4">{{ @$request->Address }}</td>
                            <td style="border: 1px solid gray;"
                                class="cell100 column4">{{ @\Modules\ThemeWorkart\Models\Province::find($request->get('user_city_id', 0))->name }}</td>
                            <td style="border: 1px solid gray;"
                                class="cell100 column4">{{ @\Modules\ThemeWorkart\Models\District::find($request->get('user_district_id', 0))->name }}</td>
                            <td style="border: 1px solid gray;" class="cell100 column4">{{ @$receipt_method }}</td>
                            <td style="border: 1px solid gray;" class="cell100 column4">{{ @$request->Note }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
