@extends('themeworkart::layouts.master')

@section('main_content')
    <main id="main">
        <section id="category-page">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12 col-xl-12">
                    @include('themeworkart::partials.breadcrumb')
                    <!--breadcrumb-->
                        <div class="page-content">
                            <h2>  {{@$post->name}} </h2>

                            <div class="page-entry">
                                {!! $content !!}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
