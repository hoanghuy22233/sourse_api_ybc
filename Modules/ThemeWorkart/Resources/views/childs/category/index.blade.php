@extends('themeworkart::layouts.master')
@section('main_content')
    <section id="category-page">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3 col-lg-3 col-xl-3" id="desktop">
                    <aside class="widget" id="widget-filter">
                        <div class="widget-title">
                            <h3>Bộ lọc</h3>
                        </div>


                        <form>
                            <p><input type="checkbox"> Bộ lọc 1</p>
                            <p><input type="checkbox"> Bộ lọc 2</p>
                            <p><input type="checkbox"> Bộ lọc 3</p>
                            <p><input type="checkbox"> Bộ lọc 4</p>
                            <p><input type="checkbox"> Bộ lọc 5</p>
                        </form>


                    </aside>


                    <aside class="widget" id="widget-product">
                        <div class="widget-title">
                            <h3>Sản phẩm thường mua</h3>
                        </div>
                        @php
                            $recommendProducts = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(4);
                        @endphp
                        <div class="product-list">
                            @foreach( $recommendProducts as $featured )
                                    @php
                                        $item_class = 'product-wd-item';
                                    @endphp
                                    @include( 'themeworkart::childs.product.partials.product_loop' )
                            @endforeach
                        </div>
                    </aside>
                </div>

                <div class="col-12 col-md-9 col-lg-9 col-xl-9">
                 @include('themeworkart::partials.breadcrumb')
                <!--breadcrumb-->
                    <div class="category-title">
                        <h3>{{ $pageOption['title'] }}</h3>
                    </div>

                    <!--category-title-->

                    <div class="category-filter">
                        <div class="row">
                            <div class="col-12 col-md-7 col-lg-7 col-xl-7 mt-mobile">
                                <div class="order-title">
                                    Sắp xếp
                                </div>
                                <div class="select-redirect select curency">

                                    <select class="header-select" onchange="location = this.value">
{{--                                        <option value="?sort_prd_alpha=0">Alphabetic ally: A-</option>--}}
{{--                                        <option value="?sort_prd_alpha=1">Alphabetic ally: Z-</option>--}}
                                        <option value="?sort_prd_price=1">Price: high to low</option>
                                        <option value="?sort_prd_price=0">Price: low to high</option>
                                        <option value="?sort_prd_date=0">Date: old to new </option>
                                        <option value="?sort_prd_date=1">Date: new to old</option>
                                    </select>
                                </div>

                            </div>

                            <div class="col-12 col-md-3 col-lg-3 col-xl-3 mt-mobile">
                                <div class="order-title">
                                    Hiển thị
                                </div>
                                <div class="select-redirect select curency">
                                    <form action="">
                                        <select class="header-select" name="showLimit" onchange="this.form.submit()">
                                            <option value="10" {{request()->showLimit == 10 ? 'selected' : '' }}>10</option>
                                            <option value="20"  {{request()->showLimit == 20 ? 'selected' : '' }}>20</option>
                                        </select>
                                    </form>

                                </div>
                            </div>
                            <div class="col-12 col-md-2 col-lg-2 col-xl-2 text-right mt-mobile">
                                <a href="?viewMode=grid" class="action-view {{request()->viewMode == 'grid' ? 'active' : '' }} "><i class="fa fa-th-large"></i></a>
                                <a href="?viewMode=list" class="action-view {{request()->viewMode == 'list' ? 'active' : '' }}"><i class="fa fa-list"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="category-list">
                        <div class="row row-5">
                            @if( request()->viewMode == 'list' )
                                @foreach( $products as $featured )
                                    <div class="col-12 col-lg-12 col-md-12 col-xl-12 pd-5">
                                        @php $item_class = ' list' @endphp
                                        @include( 'themeworkart::childs.product.partials.product_loop' )
                                    </div>
                                @endforeach
                            @else
                                @foreach( $products as $featured )
                                    <div class="col-12 col-lg-4 col-md-4 col-xl-4 pd-5">
                                        @include( 'themeworkart::childs.product.partials.product_loop' )
                                    </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                    <!--category-list-->

                     <div style="margin: auto; text-align: center">{!! $products->links() !!}</div>

                </div>
                <div class="col-12 col-md-3 col-lg-3 col-xl-3" id="mobile">
                    <aside class="widget" id="widget-filter">
                        <div class="widget-title">
                            <h3>Bộ lọc</h3>
                        </div>
                        <form>
                            <p><input type="checkbox"> Bộ lọc 1</p>
                            <p><input type="checkbox"> Bộ lọc 2</p>
                            <p><input type="checkbox"> Bộ lọc 3</p>
                            <p><input type="checkbox"> Bộ lọc 4</p>
                            <p><input type="checkbox"> Bộ lọc 5</p>
                        </form>
                    </aside>
                    <aside class="widget" id="widget-product">
                        <div class="widget-title">
                            <h3>Sản phẩm thường mua</h3>
                        </div>
                        <div class="product-list">

                            @foreach( $recommendProducts as $featured )
                                @php  $item_class = 'product-wd-item' @endphp
                                @include( 'themeworkart::childs.product.partials.product_loop' )
                            @endforeach

{{--                            <div class="$item_class">--}}
{{--                                <a href="">--}}
{{--                                    <div class="product-avatar"><img src="https://via.placeholder.com/120x120?text=Product" alt=""></div>--}}
{{--                                    <div class="product-content">--}}
{{--                                        <h3>Lether Gray Tuxedo</h3>--}}
{{--                                        <div class="product_price">--}}
{{--                                            <span class="price">$55.00</span>--}}
{{--                                            <del>$95.00</del>--}}
{{--                                        </div>--}}
{{--                                        <div class="pr_switch_wrap">--}}
{{--                                            <div class="product_color_switch">--}}
{{--                                                <span class="active" data-color="#87554B" style="background-color: rgb(135, 85, 75);"></span>--}}
{{--                                                <span data-color="#333333" style="background-color: rgb(51, 51, 51);"></span>--}}
{{--                                                <span data-color="#5FB7D4" style="background-color: rgb(95, 183, 212);"></span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </div>--}}
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>



@endsection


