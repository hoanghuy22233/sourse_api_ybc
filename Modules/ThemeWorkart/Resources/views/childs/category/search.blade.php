@extends('themeworkart::layouts.master')
@section('main_content')

    <div id="search-header">
        <div class="container">
            <div>
                <div class="_title_search">
                    <p class="Link"><a class="home_link" href="/home">Home</a>
                        <a class="review_link" href="search">Search</a></p>

                    <form method="get">
                        <div class="_input_search_product">
                            <input  type="search" placeholder="Type then hit enter to search ..." name="key">
                            <button type="submit"><svg class="svg-inline--fa fa-search fa-w-16" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path></svg><!-- <i class="fas fa-search"></i> Font Awesome fontawesome.com --> Search</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>


        <div class="container" id="search-content">
            <div class="dtop">
                <div class="ds">
                    <h3> List of <b>{{$count}} </b> items </h3>
                </div>
            </div>


            <div class="row _title-content ">
                    @if(!empty($products))
                        @php
                            $getId = '';
                                foreach ($products as $product){
                                $getId .= $product->proprerties_id;
                              }
                            $getId = array_unique(explode('|', $getId));
                             if ($getId['0'] == ''){
                                    array_shift($getId);
                                }
                        @endphp
                        @foreach($products as $featured)
                            <div class="col col-sm-4 col-md-3 col-6">
                                @php
                                    $item_class = ' selling_search';
                                    $img_class = 'width_i_search';
                                @endphp
                                    @include( 'themeworkart::childs.product.partials.product_loop' )
                            </div>


{{--                        <div class="col">--}}
{{--                            <div class="card selling_search">--}}
{{--                                <a href="#">--}}
{{--                                    <img class="width_i_search" src="css/img/copyoptimized-ifo9_295x295.png" alt="product"/>--}}
{{--                                    <div class="sale_price_search">--}}
{{--                                        <div class="price_sale">--}}
{{--                                            <!--                                    <img src="css/img/Subtraction 1.png" alt="sale %">-->--}}
{{--                                            <p>43%</p>--}}
{{--                                            <p>OFF</p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <p>3 Sisters - you are my person</p>--}}
{{--                                    <p> you will always...</p>--}}
{{--                                    <h5>Cloth Face Mask</h5>--}}
{{--                                    <p class="price">€21.05 EUR 44% <span>OFF€11.78 EUR</span></p>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    @endforeach
                    @endif
                </div>
            </div>

        <div class="_search_page">
            <nav class="_search_next_page">
{{--                <ul class="pagination">--}}
{{--                    <li class="page-item">--}}
{{--                        <a class="page-link" href="#">1</a>--}}
{{--                    </li>--}}
{{--                    <li class="page-item">--}}
{{--                        <a class="page-link" href="#">2</a>--}}
{{--                    </li>--}}
{{--                    <li class="page-item">--}}
{{--                        <a class="page-link" href="#">3</a>--}}
{{--                    </li>--}}
{{--                    <li class="page-item">--}}
{{--                        <a class="page-link" href="#">4</a>--}}
{{--                    </li>--}}
{{--                    <li class="page-item">--}}
{{--                        <a class="page-link" href="#">5</a>--}}
{{--                    </li>--}}
{{--                    <li class="page-item">--}}
{{--                        <a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>--}}
{{--                    </li>--}}
{{--                    <li class="page-item">--}}
{{--                        <a class="page-link" href="#"><i class="fas fa-angle-double-right"></i></a>--}}
{{--                    </li>--}}
{{--                </ul>--}}

                <div class="pagination" style="clear: both">
                    <div style="margin: auto; text-align: center">{!! $products->appends(['key' => Request::get('key')])->links() !!}</div>
                </div>

            </nav>
        </div>
@endsection