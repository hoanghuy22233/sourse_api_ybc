@extends('themeworkart::layouts.master')

@section('main_content')
<div class="body mt-1">
    <div class="container">
        <div class="main-cart row">
            <div class="col-12">
                <div class="breadcrumbs">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb pl-0">
                            <li class="breadcrumb-item fs14"><a href="#">Home</a></li>
                            <li class="breadcrumb-item fs14 active">Your cart</li>
                        </ol>
                    </nav>
                </div>
            </div>
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Thành công!</strong> {{ Session::get('success') }}

                </div>
            @endif
            <div class="col-12 col-sm-8">
                <h3 class="text-semibold">Your cart</h3>
            </div>
            <div class="pc col-sm-4">
                <h5 class="text-semibold">Upsale product</h5>
            </div>
            <div class="col-12 col-sm-8">
                <div class="row">
                    <div class="col-12 single-cart-item mt-5">
                        @if (Cart::Count()>0)
                            @foreach ($carts as $item)
                            @php
                                $product = \Modules\ThemeWorkart\Models\Product::find($item->id);
                            @endphp
                        <div class="row">
                            <div class="col-4 col-sm-2 pr-1">
                                <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product) }}"><img src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($item->options->image, 50, 50) }}" class="w-100" alt=""></a>
                            </div>
                            <div class="col-8 col-sm-6">
                                <p class="fs14">{{$item->name}}</p>
{{--                                <div class="row">--}}
{{--                                    <div class="col-6">--}}
{{--                                        <p class="fs12 mb-0">--}}
{{--                                            Available Products--}}
{{--                                        </p>--}}
{{--                                        <p class="fs12 mb-0">--}}
{{--                                            Portrait Canvas: 73 in--}}
{{--                                        </p>--}}
{{--                                        <p class="fs12 mb-0">--}}
{{--                                            Frame--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-6">--}}
{{--                                        <p class="fs12 mb-0">--}}
{{--                                            Color white--}}
{{--                                        </p>--}}
{{--                                        <p class="fs12 mb-0">--}}
{{--                                            Size 12" x 18"--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
{{--                            <div class="col-6 col-sm-2 pr-1">--}}
{{--                                <img src="./img/front-shirt.png" class="w-100 border border-dark" alt="">--}}
{{--                            </div>--}}
{{--                            <div class="col-6 col-sm-2 pl-1">--}}
{{--                                <img src="./img/front-shirt.png" class="w-100 border border-dark" alt="">--}}
{{--                            </div>--}}
                            <div class="col-12">
                                <span class="float-right text-light-blue fs14">
                                <svg class="svg-inline--fa fa-search-plus fa-w-16 fs14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search-plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M304 192v32c0 6.6-5.4 12-12 12h-56v56c0 6.6-5.4 12-12 12h-32c-6.6 0-12-5.4-12-12v-56h-56c-6.6 0-12-5.4-12-12v-32c0-6.6 5.4-12 12-12h56v-56c0-6.6 5.4-12 12-12h32c6.6 0 12 5.4 12 12v56h56c6.6 0 12 5.4 12 12zm201 284.7L476.7 505c-9.4 9.4-24.6 9.4-33.9 0L343 405.3c-4.5-4.5-7-10.6-7-17V372c-35.3 27.6-79.7 44-128 44C93.1 416 0 322.9 0 208S93.1 0 208 0s208 93.1 208 208c0 48.3-16.4 92.7-44 128h16.3c6.4 0 12.5 2.5 17 7l99.7 99.7c9.3 9.4 9.3 24.6 0 34zM344 208c0-75.2-60.8-136-136-136S72 132.8 72 208s60.8 136 136 136 136-60.8 136-136z"></path></svg><!-- <i class="fas fa-search-plus fs14"></i> Font Awesome fontawesome.com --> Preview design
                            </span>
                            </div>
                            <div class="col-12">
                                <div class="bg-grey">
                                    <form class="cart_quantity_button" method="POST" action="{{route('cart.update',['id'=>$item->rowId])}}">
                                        {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-4 col-sm-4 text-left">
                                            <button type="submit" class="btn btn-edit m-2">
                                                Update
                                            </button>
                                            <button class="btn btn-edit m-2">
                                                <a href="{{route('cart.delete',['id'=>$item->rowId])}}">delete</a>
                                            </button>
                                        </div>
                                        <div class="col-4 col-sm-4">
                                            <div class="input-group m-2">
                <span class="input-group-btn">
{{--              <button type="button" class="btn btn-minus btn-number" disabled="disabled" data-type="minus" data-field="quant[1]">--}}

{{--                  <span><svg class="svg-inline--fa fa-minus fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="minus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg><!-- <i class="fas fa-minus"></i> Font Awesome fontawesome.com --></span>--}}
{{--                </button>--}}
                </span>
                                                <input class="cart_quantity_input" type="hidden" name="id_hidden" value="{{$item->rowId}}">
                                                <input type="number" name="qty" class="form-control input-number" value="{{$item->qty}}" value="0" min="0" max="10000">
                                                <span class="input-group-btn">
{{--              <button type="button" class="btn btn-plus btn-number" data-type="plus" data-field="quant[1]">--}}
{{--                  <span><svg class="svg-inline--fa fa-plus fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg><!-- <i class="fas fa-plus"></i> Font Awesome fontawesome.com --></span>--}}
{{--                </button>--}}
                </span>
                                            </div>
                                        </div>
                                        <div class="col-4 col-sm-4">
                                            <span class="m-2 float-right fs18">
                                               <?php
                                                $subtotal = $item->price * $item->qty;
                                                echo number_format($subtotal);
                                                ?>
                                            </span>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                            @endforeach
                            @else
                            <p class="text-center">Bạn chưa có sản phẩm nào trong giỏ hàng, <a href="https://workart.webhobasoft.com/">Tiếp tục mua hàng</a></p>
                            @endif
                    </div>
                </div>
            </div>
            <div class="col-sm-4 pc">
                @php
                    $up_sale = \Modules\ThemeWorkart\Models\Product::whereRaw('final_price < base_price')->orderBy('id','desc')->limit(3)->get();

                @endphp
                @foreach($up_sale as $k => $product)
                    <form action="{{route('cart.add_up_sale')}}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $product->id  }}">
                <div class="single-upsale-product p-2 box-shadow mb-3">
                    <div class="row">
                        <div class="col-4">
                            <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product) }}"><img src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($product->image) }}" class="w-100" alt=""></a>
                        </div>
                        <div class="col-8">
                            <p class="fs14 mb-1"><a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product) }}">{{ $product->name  }}</a></p>
                            <div class="row">
                                <div class="col-7 pr-1">
                                    <div class="input-group">
                <span class="input-group-btn">
              <button type="button" class="btn btn-minus btn-number" disabled="disabled" data-type="minus" data-field="quant[{{ $k }}]">

                  <span><svg class="svg-inline--fa fa-minus fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="minus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg><!-- <i class="fas fa-minus"></i> Font Awesome fontawesome.com --></span>
                </button>
                </span>
                                        <input type="text" name="quant[{{ $k }}]" class="form-control input-number" value="0" min="0" max="10000">
                                        <span class="input-group-btn">
              <button type="button" class="btn btn-plus btn-number" data-type="plus" data-field="quant[{{ $k }}]">
                  <span><svg class="svg-inline--fa fa-plus fa-w-14" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg><!-- <i class="fas fa-plus"></i> Font Awesome fontawesome.com --></span>
                </button>
                </span>

                                    </div>
                                </div>
                                <div class="col-5 pl-1">
{{--                                    <div class="form-group">--}}
{{--                                        <select class="form-control input-lg">--}}
{{--                                            <option value="option-1">Option 1</option>--}}
{{--                                            <option value="option-2">Option 2</option>--}}
{{--                                            <option value="option-3">Option 3</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-5">
                                    @if($product->final_price < $product->base_price)
                                        <del class="text-grey fs16">{{number_format($product->base_price, 0, '.', '.')}}</del>
                                    @endif

                                    <span class="text-green fs16">{{number_format($product->final_price, 0, '.', '.')}}</span>
                                </div>
                                <div class="col-7">
                                    <button type="submit" class="btn btn-primary fs14 w-100 h-100">
                                        Yes, I want
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    </form>
                @endforeach
            </div>
        </div>
    </div>
    <div class="recent-product mt-5 mb-5">
        @if (Cart::Count()>0)
        <div class="">
            <div class="container">
                <div class="total-price row">
                    <div class="col-8">
                        <div class="d-flex align-items-center justify-content-between">
                            <p class=" fs18 ">Total: <span> ({{Cart::Count()}})</span></p>
                            <p class=" fs26">{{Cart::total()}} </p>
                        </div>
                    </div>
                    <div class="col-4">
                        <button class="btn btn-orange text-white w-100">
                            <span class="pc"><a href="{{ route('order.getDelivery')  }}">Secure checkout</a></span>
                            <span class="sp"><a href="{{ route('order.getDelivery')  }}"></a>Checkout</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @endif
            @php
                $id_recenty = Session::get('recenty');

                $dem = 0;
            @endphp
            @if($id_recenty && count($id_recenty) > 0)
        <div class="container pc">
            <p class="fs24">Recently viewed products</p>
            <div class="row no-gutters">

                @for($i = count($id_recenty) - 1; $i >= 0; $i-- )
                    @if(isset($id_recenty[$i]) )
                    @php
                        if($dem > 3){
                            break;
                        }
                        $product = \Modules\ThemeWorkart\Models\Product::find($id_recenty[$i]);
                        $dem++;
                    @endphp
                <div class="col-3 p-1">
                    <div class="card">
                        <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product) }}"><img class="card-img-top w-100" src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($product->image) }}" alt="image"></a>
                        <div class="discount-tag recent-view-item">
                            <img src="./img/discount_tag.png" alt="">
{{--                            <div class="discount-text">--}}
{{--                                <p class="text-white fs14 mb-0">43%</p>--}}
{{--                                <p class="text-white fs14">OFF</p>--}}
{{--                            </div>--}}
                        </div>
                        <div class="card-body p-2">
                            <h4 class="card-title text-center fs16">{{ $product->name  }}</h4>

                            <p class="text-center fs14">
                                @if($product->final_price < $product->base_price)
                                    <del class="text-grey fs16">{{number_format($product->base_price, 0, '.', '.')}}</del>
                                @endif

                                <span class="text-green fs16">{{number_format($product->final_price, 0, '.', '.')}}</span>
                            </p></div>
                    </div>
                </div>
                    @endif
                @endfor
            </div>
        </div>
            @endif
    </div>
</div>
    @endsection