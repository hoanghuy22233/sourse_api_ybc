@extends('themeworkart::layouts.master')
@section('main_content')
<div class="review-box">
    <div class="container-custom">
        <div class="container_Review">
            <p class="Link"><a class="home_link text-decoration-none" href="#">Home</a><a class="review_link text-decoration-none" href="#">Happy Customers</a></p>
            <div class="content_review">
                <div class="row">
                    @php

                        $length = count($reviews);

                    @endphp
                    @for($i = 0; $i < $length; $i+=2)

                        <div class="col-lg-3 col-md-6 col-sm-12 col-md">
                            <!--  card comment review-->
                            @for($j = $i; $j < $i + 2; $j++)

                                @if(isset($reviews[$j]))
                                    @php
                                        $item = $reviews[$j];
                                        $src = asset('public/public/img_reviews/' . $item->picture);
                                        $product = $item->product;

                                    @endphp
                                    <div class="card" onclick="document.getElementById('id{{$i.$j}}').style.display='block'">
                                        <div class="review_img">
                                            <img src="{{ $src  }}" alt="Review_img">
                                        </div>
                                        <div class="review_star">
                                            <p>{{ $item->name }}</p>
                                            <p>
                                                @php
                                                    $star = $item->star;
                                                @endphp
                                                @for($numberStart = 0; $numberStart < 5; $numberStart++)
                                                    @php
                                                        $color = $numberStart <= $star - 1 ? "yellow" : "black";
                                                    @endphp
                                                    <svg style="color:{{ $color  }}" class="svg-inline--fa fa-star fa-w-18" aria-hidden="true"
                                                         focusable="false"
                                                         data-prefix="fas" data-icon="star" role="img"
                                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"
                                                         data-fa-i2svg="">
                                                        <path fill="currentColor"
                                                              d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                    </svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com -->
                                                @endfor
                                            </p>
                                        </div>
                                        <div class="review_content">
                                            <p>{{ $item->description  }}</p>
                                        </div>
                                        <hr>
                                        <div class="review_comment">
                                            <div class="row">
                                                <div class="col-3">
                                                    <img src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($product->image) }}" alt="Avatar_img">
                                                </div>
                                                <div class="col-9">
                                                    <p>{{ $product->name }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal review-->
                                    <div id="id{{$i.$j}}" class="review-modal" style="display: none;">
                                        <div class="review-modal-content">
                                            <div>
                                        <span onclick="document.getElementById('id{{$i.$j}}').style.display='none'"
                                              class="_modal-closed">×</span>

                                                <div class="content-modal">
                                                    <div class="row">
                                                        <!--  img review modal-->
                                                        <div class="col-sm-7">
                                                            <img src="{{ $src  }}" alt="Review_Product">
                                                        </div>
                                                        <!--  Content review modal-->
                                                        <div class="col-sm-5 _modal-right">
                                                            <div class="modal-comment">
                                                                <div>
                                                                    <div class="review_star">
                                                                        <p class="title-content-modal">{{ $item->name  }}</span>
                                                                        </p>
                                                                        <p class="modal-icon-app"> <span></span></p>
                                                                        <p class="title-content-modal">
                                                                            @php
                                                                                $star = $item->star;
                                                                            @endphp
                                                                            @for($numberStart = 0; $numberStart < 5; $numberStart++)
                                                                                @php
                                                                                    $color = $numberStart <= $star - 1 ? "yellow" : "black";
                                                                                @endphp
                                                                                <svg style="color:{{ $color  }}" class="svg-inline--fa fa-star fa-w-18" aria-hidden="true"
                                                                                     focusable="false"
                                                                                     data-prefix="fas" data-icon="star" role="img"
                                                                                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"
                                                                                     data-fa-i2svg="">
                                                                                    <path fill="currentColor"
                                                                                          d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                                </svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com -->
                                                                            @endfor

                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="review_content-modal">
                                                                    <p>{{ $item->description }}</p>
                                                                </div>
                                                            </div>
                                                            <div class="modal-comment">
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <img src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($product->image) }}" alt="Avatar_img">
                                                                    </div>
                                                                    <div class="col-9">
                                                                        <p>{{ $product->name }}
                                                                        </p>
                                                                        <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product) }}">View Product</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal review-->
                                @endif
                            @endfor
                        </div>
                    @endfor

                </div>
            </div>

        </div>
    </div>
</div>


@endsection

@section('custom_footer')

@endsection
