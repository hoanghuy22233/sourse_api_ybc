{{--<div class="col ">--}}
    <div class="card product-item {{ $item_class or ''   }}">
        <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($featured, 'array') }}"
           title="{{@$featured->meta_title}}">

            <div class="product-avatar">
                <img class="lazy img-responsive {{$img_class or '' }}"
                     data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($featured->image, 300 ,null) }}"
                     alt="{{$featured->name}}"/>

                @if($featured->final_price < $featured->base_price)
                    <div class="sale_price">
                        <div class="price_sale">
                            <p>{{  100 - ( round(($featured->final_price/$featured->base_price) * 100 )) }}%</p>
                            <p>OFF</p>
                        </div>
                    </div>
                @endif
            </div>


            <div class="product-content">

                <p> {{$featured->name}}  </p>

                <div class="product_price">
                    @if($featured->final_price != 0)
                        <span class="price">
                                @if($featured->final_price < $featured->base_price) @else
                            @endif
                            {{number_format($featured->final_price, 0, '.', '.')}} USD
                        </span>
                        @else
                        </br> <p class="final_price" class="pr">not set</p>
                    @endif

                    @if($featured->final_price < $featured->base_price)
                        <del class="base_price">{{number_format($featured->base_price, 0, '.', '.')}}</del>
                    @endif

                    @if($featured->final_price < $featured->base_price)
                        <div class="on_sale"><span>{{  100 - ( round(($featured->final_price/$featured->base_price) * 100 )) }}%</span>
                            Off
                        </div>
                    @else
                        <div style="height: 24px;border-top: none"></div>
                    @endif
                </div>
            </div>
        </a>
    </div>
{{--</div>--}}