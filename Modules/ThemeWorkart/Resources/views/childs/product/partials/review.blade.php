<p class="fs24 text-semibold mt-3 color-text">
    Review
</p>
@php

    if(isset($product->id)){

        $length = 0;

        $reviews = \Modules\ThemeWorkart\Models\Review::where('product_id', $product->id)->orderBy('id','DESC')->limit(8)->get();

        if($reviews){
            $length = count($reviews);
        }

        $all_star = $length;
        $count_five_star = 0;
        $count_four_star = 0;
        $count_three_star = 0;
        $count_two_star = 0;
        $count_one_star = 0;
        $count_comment = 0;
        $count_five_star = \Modules\ThemeWorkart\Models\Review::where('product_id', $product->id)->where('star',5)->get()->count();
        $count_four_star = \Modules\ThemeWorkart\Models\Review::where('product_id', $product->id)->where('star',4)->get()->count();
        $count_three_star = \Modules\ThemeWorkart\Models\Review::where('product_id', $product->id)->where('star',3)->get()->count();
        $count_two_star = \Modules\ThemeWorkart\Models\Review::where('product_id', $product->id)->where('star',2)->get()->count();
        $count_one_star = \Modules\ThemeWorkart\Models\Review::where('product_id', $product->id)->where('star',1)->get()->count();
        $count_photo = \Modules\ThemeWorkart\Models\Review::where('product_id', $product->id)->where('picture','!=','')->get()->count();
        $count_comment = \Modules\ThemeWorkart\Models\Review::where('product_id', $product->id)->where('description','!=','')->get()->count();
    }else{
        $length = 0;
        $reviews = \Modules\ThemeWorkart\Models\Review::orderBy('id','DESC')->limit(8)->get();

        if($reviews){
            $length = count($reviews);
        }

        $all_star = $length;
        $count_five_star = 0;
        $count_four_star = 0;
        $count_three_star = 0;
        $count_two_star = 0;
        $count_one_star = 0;
        $count_comment = 0;
        $count_five_star = \Modules\ThemeWorkart\Models\Review::where('star',5)->get()->count();
        $count_four_star = \Modules\ThemeWorkart\Models\Review::where('star',4)->get()->count();
        $count_three_star = \Modules\ThemeWorkart\Models\Review::where('star',3)->get()->count();
        $count_two_star = \Modules\ThemeWorkart\Models\Review::where('star',2)->get()->count();
        $count_one_star = \Modules\ThemeWorkart\Models\Review::where('star',1)->get()->count();
        $count_photo = \Modules\ThemeWorkart\Models\Review::where('picture','!=','')->get()->count();
        $count_comment = \Modules\ThemeWorkart\Models\Review::where('description','!=','')->get()->count();
    }
     $trungbinh = 0;
    if(($count_five_star + $count_four_star + $count_three_star + $count_two_star + $count_one_star) > 0){
        $trungbinh = (5 * $count_five_star + 4 * $count_four_star + 3 *  $count_three_star + 2 * $count_two_star + $count_one_star) / ($count_five_star + $count_four_star + $count_three_star + $count_two_star + $count_one_star);
    }

@endphp
@if($length)
    <div class="review-area">
        <div class="row">
            <div class="col-12 col-sm-4 pr-sm-1">
                <div class="review-main">
                    <div class="review-content float-right mr-4">
                        <p class="fs24 mb-0 text-center color-text">{{ round($trungbinh,1)  }}/5</p>
                        <p class="fs20 mb-0 text-center"><i class="fas fa-star text-gold"></i><i
                                    class="fas fa-star text-gold"></i><i
                                    class="fas fa-star text-gold"></i><i
                                    class="fas fa-star text-gold"></i><i class="fas fa-star text-gold"></i>
                        </p>
                        <p class="fs14 mb-0 text-center">{{ $all_star }} </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-8 pl-sm-1 mt-1 mt-sm-0">
                <div class="row">
                    <div class="col pr-1">
                        <div class="review-option p-1">
                            <p class="text-center fs14 mt-2 mb-2"><a href="javascript:showStar('all')">All</a> <span class="pc">({{ $all_star }})</span></p>
                        </div>
                        <div class="review-option mt-1 p-1">
                            <p class="text-center fs14 mt-2 mb-2"><a href="javascript:showStar(2)">2 Star</a><span class="pc">({{ $count_two_star }})</span></p>
                        </div>
                    </div>
                    <div class="col pl-1 pr-1">
                        <div class="review-option p-1">
                            <p class="text-center fs14 mt-2 mb-2"><a href="javascript:showStar(5)">5 Star</a> <span class="pc">({{ $count_five_star }})</span></p>
                        </div>
                        <div class="review-option mt-1 p-1">
                            <p class="text-center fs14 mt-2 mb-2"><a href="javascript:showStar(1)">1 Star</a><span class="pc">({{ $count_one_star  }})</span></p>
                        </div>
                    </div>
                    <div class="col pl-1 pr-1">
                        <div class="review-option p-1">
                            <p class="text-center fs14 mt-2 mb-2"><a href="javascript:showStar(4)">4 Star</a> <span class="pc">({{ $count_four_star }})</span></p>
                        </div>
                        <div class="review-option mt-1 p-1">
                            <p class="text-center fs14 mt-2 mb-2"><span
                                        class="pc"><a href="javascript:showStar('have_comment')">Have comment ({{ $count_comment }})</a></span><span
                                        class="sp">Comments</span></p>
                        </div>
                    </div>
                    <div class="col pl-1">
                        <div class="review-option p-1">
                            <p class="text-center fs14 mt-2 mb-2"><a href="javascript:showStar(3)">3 Star</a> <span class="pc">({{ $count_three_star  }})</span></p>
                        </div>
                        <div class="review-option mt-1 p-1">
                            <p class="text-center fs14 mt-2 mb-2"><span
                                        class="pc"><a href="javascript:showStar('have_picture')">Have pictures ({{ $count_photo  }})</a></span><span
                                        class="sp">Pictures</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content_review">
            <div class="row">

                @for($i = 0; $i < $length; $i+=2)
                    @php
                        $class_p = " pl-1 pr-1";
                        if($i == 0){
                             $class_p = "pr-1";
                        }else if($i + 2 >= $length){
                            $class_p = "pl-1";
                        }

                    @endphp
                    <div class="col-lg-3 col-md-6 col-sm-12 col-md {{ $class_p }}">
                        <!--  card comment review-->
                        @for($j = $i; $j < $i + 2; $j++)

                            @if(isset($reviews[$j]))
                                @php
                                    $item = $reviews[$j];
                                    $src = asset('public/public/img_reviews/' . $item->picture);
                                    $product = $item->product;
                                    $pathSrc = public_path() . "/public/img_reviews/" .$item->picture;

                                    $have_comment = !empty($item->description) ? "have_comment" : "";
                                    $have_picture = !empty($item->picture) ? "have_picture" : "";
                                @endphp
                                <div  class="card star-{{  $item->star }} {{ $have_comment  }} {{  $have_picture }}" onclick="document.getElementById('id{{$i.$j}}').style.display='block'">
                                    @if(!empty($item->picture) && file_exists($pathSrc))
                                        <div class="review_img">

                                            <img src="{{ $src  }}" alt="Review_img">
                                        </div>
                                    @endif
                                    <div class="review_star">
                                        <p>{{ $item->name }}</p>
                                        <p>
                                            @php
                                                $star = $item->star;
                                            @endphp
                                            @for($numberStart = 0; $numberStart < 5; $numberStart++)
                                                @php
                                                    $color = $numberStart <= $star - 1 ? "yellow" : "black";
                                                @endphp
                                                <svg style="color:{{ $color  }}" class="svg-inline--fa fa-star fa-w-18" aria-hidden="true"
                                                     focusable="false"
                                                     data-prefix="fas" data-icon="star" role="img"
                                                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"
                                                     data-fa-i2svg="">
                                                    <path fill="currentColor"
                                                          d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                </svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com -->
                                            @endfor
                                        </p>
                                    </div>
                                    <div class="review_content">
                                        <p>{{ $item->description  }}</p>
                                    </div>
                                    <hr>
                                    <div class="review_comment">
                                        <div class="row">
                                            <div class="col-3">
                                                <img src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($product->image) }}" alt="Avatar_img">
                                            </div>
                                            <div class="col-9">
                                                <p>{{ $product->name }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal review-->
                                <div id="id{{$i.$j}}" class="review-modal" style="display: none;">
                                    <div class="review-modal-content">
                                        <div>
                                        <span onclick="document.getElementById('id{{$i.$j}}').style.display='none'"
                                              class="_modal-closed">×</span>

                                            <div class="content-modal">
                                                <div class="row">
                                                    <!--  img review modal-->
                                                    <div class="col-sm-7">
                                                        @if(!empty($item->picture) && file_exists($pathSrc))
                                                            <img src="{{ $src  }}" alt="Review_Product">
                                                        @endif
                                                    </div>
                                                    <!--  Content review modal-->
                                                    <div class="col-sm-5 _modal-right">
                                                        <div class="modal-comment">
                                                            <div>
                                                                <div class="review_star">
                                                                    <p class="title-content-modal">{{ $item->name  }}</span>
                                                                    </p>
                                                                    <p class="modal-icon-app"> <span></span></p>
                                                                    <p class="title-content-modal">
                                                                        @php
                                                                            $star = $item->star;
                                                                        @endphp
                                                                        @for($numberStart = 0; $numberStart < 5; $numberStart++)
                                                                            @php
                                                                                $color = $numberStart <= $star - 1 ? "yellow" : "black";
                                                                            @endphp
                                                                            <svg style="color:{{ $color  }}" class="svg-inline--fa fa-star fa-w-18" aria-hidden="true"
                                                                                 focusable="false"
                                                                                 data-prefix="fas" data-icon="star" role="img"
                                                                                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"
                                                                                 data-fa-i2svg="">
                                                                                <path fill="currentColor"
                                                                                      d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                                                                            </svg><!-- <i class="fas fa-star"></i> Font Awesome fontawesome.com -->
                                                                        @endfor

                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="review_content-modal">
                                                                <p>{{ $item->description }}</p>
                                                            </div>
                                                        </div>
                                                        <div class="modal-comment">
                                                            <div class="row">
                                                                <div class="col-3">
                                                                    <img src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($product->image) }}" alt="Avatar_img">
                                                                </div>
                                                                <div class="col-9">
                                                                    <p>{{ $product->name }}
                                                                    </p>
                                                                    <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product) }}">View Product</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal review-->
                            @endif
                        @endfor
                    </div>
                @endfor

            </div>
        </div>

        <div class="button_review">
            <div class="button_Show_review ">
                <a href="{{ route('allReview')  }}">Show more reviews</a>
            </div>
        </div>
    </div>
@endif