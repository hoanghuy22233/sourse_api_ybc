<?php
$manufacturer = CommonHelper::getFromCache('manufacturer_id' . @$product->manufacture_id);
if (!$manufacturer) {
    $manufacturer = Modules\ThemeWorkart\Models\Manufacturer::find($product->manufacture_id);
    CommonHelper::putToCache('manufacturer_id' . @$product->manufacture_id, $manufacturer);
}

$province = CommonHelper::getFromCache('province_orderBy_name_asc_get');
if (!$province) {
    $province = \Modules\ThemeWorkart\Models\Province::orderBy('name', 'ASC')->get();
    CommonHelper::putToCache('province_orderBy_name_asc_get', $province);
}

$html_province = '<select name="province_home" id="province_home">';
foreach ($province as $p) {
    $html_province .= '<option value="' . $p->id . '">' . $p->name . '</option>';
}
$html_province .= '</select>';

$html_province1 = '<select name="province_tuvan" id="province_tuvan">';
foreach ($province as $p1) {
    $html_province1 .= '<option value="' . $p1->id . '">' . $p1->name . '</option>';
}
$html_province1 .= '</select>';

?>
@extends('themeworkart::layouts.master')

@section('main_content')


    <div class="body mt-1">
        <div class="container-custom">
            <div class="breadcrumbs">
                <nav aria-label="breadcrumb">
                    @include('themeworkart::partials.breadcrumb')
                </nav>
            </div>

            <div class="main-content row">
                <div class="col-md-6 col-sm-12">
                    <div class="container">

                        <div class="main-image text-center pl-5 pr-5">

                            <img class="lazy w-100" alt="{{@$product->name}}"
                                 data-src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($product->image) }}"
                                 src=""/>


                            <div class="discount-tag">
                                <img id="main-display" src="/img/discount_tag.png" alt="">
                                <div class="discount-text">
                                    @if($product->final_price < $product->base_price)
                                        <p class="text-white fs14 mb-0">{{  100 - ( round(($product->final_price/$product->base_price) * 100 )) }}
                                            %</p>
                                        <p class="text-white fs14">OFF</p>
                                    @endif


                                </div>
                            </div>
                        </div>

                        <div class="more-image mt-3">
                            <div class="row">

                                @if(!empty($image_extras))
                                    @foreach($image_extras as $key => $image_extra)
                                        <div class="col-3 ">
                                            <a onclick="changeMainDisplayImage(this)">
                                                <img class="lazy w-100 h-100 single-image-item"
                                                     alt="{{ $product->name .' '. $key }}"
                                                     data-src="{{ asset('public/filemanager/userfiles/' . $image_extra) }}"
                                                />
                                            </a>
                                        </div>
                                    @endforeach
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 mt-sm-4 mt-md-0">
                    <div class="row">
                        <div class="col-12 col-sm-8 col-md-8 pr-1">
                            @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Success!</strong> {{ Session::get('success') }}
                                    <p>View cart here <a href="{{ route('cart.index')  }}">View Cart</a></p>
                                </div>
                            @endif
                                @if(Session::has('error'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>Error!</strong> {{ Session::get('error') }}

                                    </div>
                                @endif
                            <p class="fs20 color-text">{{$product->name}} </p>
                            <p>
                                @if($product->final_price < $product->base_price)
                                    <del class="text-grey fs16">{{number_format($product->base_price, 0, '.', '.')}}</del>
                                @endif

                                <span class="text-green fs16">{{number_format($product->final_price, 0, '.', '.')}}</span>
                            </p>

                            <img src="/img/notification.svg" alt="">
                            <span class="text-light-blue fs14">ORDER CUT OFF FOR CHRISTMAS: Dec 01.</span>
                            </p>
                            <div class="bg-grey item-options">
                                <form action="{{route('cart.add',['id'=>$product->id])}}" method="post">
                                    {{ csrf_field() }}
                                <div class="p-3">
{{--                                    <p class="fs16 text-semibold color-text">Choose a product type</p>--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-6 col-sm-3 pl-1 pr-1">--}}
{{--                                            <div class="card">--}}
{{--                                                <img class="card-img-top" src="/img/t-shirt.png" alt="Card image">--}}
{{--                                                <div class="card-body single-product p-1">--}}
{{--                                                    <p class="card-title text-center fs14">Classic T-Shirt</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-6 col-sm-3 pl-1 pr-1">--}}
{{--                                            <div class="card">--}}
{{--                                                <img class="card-img-top" src="/img/hoodi.png" alt="Card image">--}}
{{--                                                <div class="card-body single-product p-1">--}}
{{--                                                    <p class="card-title text-center fs14">Hooded Sweatshirt</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-6 col-sm-3 pl-1 pr-1">--}}
{{--                                            <div class="card">--}}
{{--                                                <img class="card-img-top" src="/img/long%20t%20shirt.png"--}}
{{--                                                     alt="Card image">--}}
{{--                                                <div class="card-body single-product p-1">--}}
{{--                                                    <p class="card-title text-center fs14">Long Sleeve T-Shirt </p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-6 col-sm-3 pl-1 pr-1">--}}
{{--                                            <div class="card">--}}
{{--                                                <img class="card-img-top" src="/img/girl%20t%20shirt.png"--}}
{{--                                                     alt="Card image">--}}
{{--                                                <div class="card-body single-product p-1">--}}
{{--                                                    <p class="card-title text-center fs14">Ladies T-Shirt</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <p class="mt-3 fs16 text-semibold color-text">Color</p>--}}
{{--                                    <div class="color-list">--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #F8F8F8"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #BCBCC6"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #9AB5D2"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #9C263A"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #4C3025"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #FEA821"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #259B5E"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #E2B5C9"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #FF3300"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #E6E7EC"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #5C4881"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #CD0102"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #1D4E9A"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #8CAC69"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #4B4D34"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-color">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="circle-color" style="background: #E6E7EC"></div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <p class="mt-3 fs16 text-semibold color-text">Size</p>--}}
{{--                                    <div class="size-list">--}}
{{--                                        <div class="single-size mt-1">--}}
{{--                                            <button type="button" class="btn btn-pick-size fs14 color-text">S</button>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-size mt-1">--}}
{{--                                            <button type="button" class="btn btn-pick-size fs14 color-text">M</button>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-size mt-1">--}}
{{--                                            <button type="button" class="btn btn-pick-size fs14 color-text">L</button>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-size mt-1">--}}
{{--                                            <button type="button" class="btn btn-pick-size fs14 color-text">XL</button>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-size mt-1">--}}
{{--                                            <button type="button" class="btn btn-pick-size fs14 color-text">2XL</button>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-size mt-1">--}}
{{--                                            <button type="button" class="btn btn-pick-size fs14 color-text">3XL</button>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-size mt-1">--}}
{{--                                            <button type="button" class="btn btn-pick-size fs14 color-text">4XL</button>--}}
{{--                                        </div>--}}
{{--                                        <div class="single-size mt-1">--}}
{{--                                            <button type="button" class="btn btn-pick-size fs14 color-text">5XL</button>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <p class="mt-3 fs16 text-semibold color-text">Personalized for Front</p>--}}
{{--                                    <form class="mt-3">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="fs14 color-text">Number Of Pets</label>--}}
{{--                                            <select class="form-control input-lg">--}}
{{--                                                <option value="option-1">Option 1</option>--}}
{{--                                                <option value="option-2">Option 2</option>--}}
{{--                                                <option value="option-3">Option 3</option>--}}
{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="color-text">Kid's</label>--}}
{{--                                            <input class="form-control">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="fs14 color-text">Number Of Chilren</label>--}}
{{--                                            <select class="form-control input-lg">--}}
{{--                                                <option value="option-1">Option 1</option>--}}
{{--                                                <option value="option-2">Option 2</option>--}}
{{--                                                <option value="option-3">Option 3</option>--}}
{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="fs14 color-text">Number Of Pets</label>--}}
{{--                                            <select class="form-control input-lg">--}}
{{--                                                <option value="option-1">Option 1</option>--}}
{{--                                                <option value="option-2">Option 2</option>--}}
{{--                                                <option value="option-3">Option 3</option>--}}
{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="color-text">Kid's</label>--}}
{{--                                            <input class="form-control">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label class="fs14 color-text">Number Pets Chilren</label>--}}
{{--                                            <select class="form-control input-lg">--}}
{{--                                                <option value="option-1" class="color-text">Option 1</option>--}}
{{--                                                <option value="option-2" class="color-text">Option 2</option>--}}
{{--                                                <option value="option-3" class="color-text">Option 3</option>--}}
{{--                                            </select>--}}
{{--                                        </div>--}}
{{--                                        <button type="button" class="btn btn-outline-primary bg-white w-100">Preview--}}
{{--                                        </button>--}}
{{--                                    </form>--}}
{{--                                    <p class="fs14 mt-3 color-text">Please make sure that all the options you have--}}
{{--                                        chosen--}}
{{--                                        are correct before clicking on the "Add To Cart" button.</p>--}}
{{--                                    <a class="text-primary fs14 detail-item" data-toggle="collapse"--}}
{{--                                       data-target="#collapseExample" aria-expanded="false"--}}
{{--                                       aria-controls="collapseExample"--}}
{{--                                       style="cursor: pointer;">--}}
{{--                                        Show Details <i class="fas fa-chevron-down"></i>--}}
{{--                                    </a>--}}
{{--                                    <div class="collapse" id="collapseExample">--}}
{{--                                        <div class="card card-body">--}}
{{--                                            <p class="fs14 text-semibold color-text">Product Information</p>--}}
{{--                                            <ul class="footer-list pl-3">--}}
{{--                                                <li class="fs14 mb-2 color-text">5.3-ounce, 100% cotton (99/1--}}
{{--                                                    cotton/poly--}}
{{--                                                    (Ash) & 90/10 cotton/poly (Sport Grey)--}}
{{--                                                </li>--}}
{{--                                                <li class="fs14 mb-2 color-text">Heavyweight classic unisex tee</li>--}}
{{--                                                <li class="fs14 mb-2 color-text">Taped neck and shoulders; Tearaway--}}
{{--                                                    label--}}
{{--                                                </li>--}}
{{--                                                <li class="fs14 mb-2 color-text">Decoration type: Digital Print</li>--}}
{{--                                            </ul>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <p class="mt-3 fs16 text-semibold">Quantity</p>
                                    <div class="row p-0">
                                        <div class="col-5">
                                            <div class="input-group">
                                    <span class="input-group-btn">
{{--                                      <button type="button" class="btn btn-minus btn-number" disabled="disabled" data-type="minus"--}}
{{--                                     data-field="quant[1]">--}}
{{--                                       <span><i class="fas fa-minus"></i></span>--}}
{{--                                   </button>--}}
                                    </span>
                                                <input type="number" name="qty" class="form-control input-number"
                                                       value="1" min="1" max="10000">
                                                <span class="input-group-btn">
{{--                    <button type="button" class="btn btn-plus btn-number" data-type="plus" data-field="quant[1]">--}}
{{--                      <span><i class="fas fa-plus"></i></span>--}}
{{--                    </button>--}}
                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-orange text-white mt-3 w-100">
                                    Add To Cart
                                </button>
                                </form>
                            </div>

{{--                            <p class="mt-3">--}}
{{--                                <img src="/img/notification.svg" alt="">--}}
{{--                                <span class="text-light-blue fs14">Please place orders no later than Nov 25 to have them arrived in time for Christmas</span>--}}
{{--                            </p>--}}
{{--                            <div class="customer-helper">--}}
{{--                                <ul class="footer-list mt-3 pl-0">--}}
{{--                                    <li class="fs14 mb-2 color-text">- Shipping calculated at checkout</li>--}}
{{--                                    <li class="fs14 mb-2 color-text">- Need Help? <span--}}
{{--                                                class="text-light-blue">support@gossby.com</span></li>--}}
{{--                                    <li class="fs14 mb-2 color-text">- Guaranted safe and secure checkout via:</li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                            <div class="list-payment">--}}
{{--                                <div class="single-payment">--}}
{{--                                    <img src="/img/paypal.png" alt="">--}}
{{--                                </div>--}}
{{--                                <div class="single-payment">--}}
{{--                                    <img src="/img/visa.png" alt="">--}}
{{--                                </div>--}}
{{--                                <div class="single-payment">--}}
{{--                                    <img src="/img/mastercard.png" alt="">--}}
{{--                                </div>--}}
{{--                                <div class="single-payment">--}}
{{--                                    <img src="/img/american_express.png" alt="">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="list-share">--}}
{{--                                <p class="fs14 mt-3 mb-1">Share this:</p>--}}

{{--                                <div class="single-payment">--}}
{{--                                    <a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}"--}}
{{--                                       target="_blank">--}}
{{--                                        <img src="/img/facebook.png" alt="">--}}
{{--                                    </a>--}}
{{--                                </div>--}}

{{--                                <div class="single-payment">--}}
{{--                                    <a href="https://twitter.com/intent/tweet?&url={{url()->current()}}"--}}
{{--                                       target="_blank">--}}
{{--                                        <img src="/img/twitter.png" alt="">--}}
{{--                                    </a>--}}
{{--                                </div>--}}

{{--                                <div class="single-payment">--}}
{{--                                    <a href="https://pinterest.com/pin/create/button/?url={{url()->current()}}&media=&description=">--}}
{{--                                        <img src="/img/in.png" alt="">--}}
{{--                                    </a>--}}
{{--                                </div>--}}

{{--                                <div class="single-payment">--}}
{{--                                    <a href="https://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}&title=&summary=&source="--}}
{{--                                       target="_blank">--}}
{{--                                        <img src="/img/pinterest.png" alt="">--}}
{{--                                    </a>--}}
{{--                                </div>--}}


{{--                            </div>--}}
                        </div>
                        <div class="col-sm-4 col-md-4 pl-1">
                            <p class="fs16 color-text">Special Discounts Unlocked! 🎁</p>
                            <div class="row">
                                <div class="col-12 mb-3">

                                    @php
                                        $cate_multi = explode('|', trim($product->multi_cat, '|'));

                                        $productInMultiCats = CommonHelper::getFromCache('products_multi_cat_like_cate_multi' . $cate_multi[0]);
                                        if (!$productInMultiCats) {
                                            $productInMultiCats = \Modules\ThemeWorkart\Models\Product::where('multi_cat', 'like', '%|' . $cate_multi[0] . '|%')->where('manufacture_id', $product->manufacture_id)->where('status', 1)->paginate(6);
                                            CommonHelper::putToCache('products_multi_cat_like_cate_multi' . $cate_multi[0], $productInMultiCats);
                                        }
                                    @endphp

                                    @if($productInMultiCats->count() > 0)
                                        @foreach( $productInMultiCats as $k => $featured)
                                            <form action="{{route('cart.add_up_sale')}}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="id" value="{{ $product->id  }}">
                                            <div class="card">
                                                <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product) }}"><img class="lazy img-responsive card-img-top w-100" data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($featured->image, 300 ,null) }}" alt="{{$featured->name}}"/></a>

                                                <div class="card-body p-2">
                                                    <h4 class="card-title fs14 color-text">{{$featured->name}}</h4>
                                                    <div class="row">
                                                        <div class="col-7 pr-1">
                                                            <div class="input-group">
                                                                            <span class="input-group-btn">
                                                                          <button type="button"
                                                                                  class="btn btn-minus btn-number"
                                                                                  disabled="disabled" data-type="minus"
                                                                                  data-field="quant[{{ $k }}]">

                                                                              <span><i class="fas fa-minus"></i></span>
                                                                            </button>
                                                                            </span>
                                                                <input type="text" name="quant[{{ $k }}]"
                                                                       class="form-control input-number"
                                                                       value="0" min="0" max="10000">
                                                                <span class="input-group-btn">
                                                              <button type="button" class="btn btn-plus btn-number"
                                                                      data-type="plus" data-field="quant[{{ $k }}]">
                                                                  <span><i class="fas fa-plus"></i></span>
                                                                </button>
                                                         </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-5 pr-1">

                                                            <p class="mb-0">
                                                                @if($product->final_price < $product->base_price)
                                                                    <del class="text-grey fs16">{{number_format($product->base_price, 0, '.', '.')}}</del>
                                                                @endif
                                                            </p>
                                                            <p class="text-green fs14">{{number_format($product->final_price, 0, '.', '.')}} </p>
                                                        </div>
                                                        <div class="col-7 pl-1">
                                                            <button type="submit" class="btn btn-primary fs14 w-100 h-75">
                                                                Yes, I want
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        @endforeach
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @php
                $id_recenty = Session::get('recenty');

                $dem = 0;
            @endphp
            @if($id_recenty && count($id_recenty) > 0)
            <div class="container pc">
                <p class="fs24">Recently viewed products</p>
                <div class="row no-gutters">

                    @for($i = count($id_recenty) - 1; $i >= 0; $i-- )
                        @if(isset($id_recenty[$i]) && $id_recenty[$i] != $product->id )
                        @php
                            if($dem > 3){
                                break;
                            }
                            $product_recent = \Modules\ThemeWorkart\Models\Product::find($id_recenty[$i]);
                            $dem++;
                        @endphp
                        <div class="col-3 p-1">
                            <div class="card">
                                <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product_recent) }}"><img class="card-img-top w-100" src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($product_recent->image) }}" alt="image"></a>
                                <div class="discount-tag recent-view-item">
                                    <img src="./img/discount_tag.png" alt="">
                                    {{--                            <div class="discount-text">--}}
                                    {{--                                <p class="text-white fs14 mb-0">43%</p>--}}
                                    {{--                                <p class="text-white fs14">OFF</p>--}}
                                    {{--                            </div>--}}
                                </div>
                                <div class="card-body p-2">
                                    <h4 class="card-title text-center fs16">{{ $product_recent->name  }}</h4>

                                    <p class="text-center fs14">
                                        @if($product_recent->final_price < $product_recent->base_price)
                                            <del class="text-grey fs16">{{number_format($product_recent->base_price, 0, '.', '.')}}</del>
                                        @endif

                                        <span class="text-green fs16">{{number_format($product_recent->final_price, 0, '.', '.')}}</span>
                                    </p></div>
                            </div>
                        </div>
                        @endif
                    @endfor
                </div>
            </div>
            @endif
        </div>
        <div class="bg-grey">
            <div class="preview container pt-3">
                <p class="fs24 text-semibold color-text">
                    Your Review
                </p>
                <div class="bg-white w-100">
                    <p class="text-center fs20 text-semibold p-3 color-text">
                        Have you purchased this product? Write a <a  class="text-light-blue btn-write-review"
                                                                    style="color: #2F80ED; cursor: pointer;">review!</a>
                    </p>
                </div>
                @php

                @endphp
                @include('themeworkart::childs.product.partials.review')
            </div>
        </div>
        <div class="container mt-5 mb-5">
            <p class="fs24 text-semibold color-text">Photos from review</p>
            <div class="list-photo-review">
                <div class="row">
                    <div class="col pr-1">
                        <img class="w-100" src="/img/review-img.png" alt="">
                    </div>
                    <div class="col pl-1 pr-1">
                        <img class="w-100" src="/img/review-img.png" alt="">
                    </div>
                    <div class="col pl-1 pr-1">
                        <img class="w-100" src="/img/review-img.png" alt="">
                    </div>
                    <div class="col pl-1">
                        <img class="w-100" src="/img/review-img.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        @include('themeworkart::childs.product.modal_review')

    </div>

    @if(Session::has('code'))
        <div class="modal fade" id="modal-thanks" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 900px;">
                <div class="modal-content" style="min-height: 500px;">
                    <div class="d-flex justify-content-center position-relative" style="border-bottom: none !important;">
                        <p class="modal-title fs26" style="color:#1B1D1F;">Thank you!</p>
                        <button type="button" class="close text-right position-absolute" data-dismiss="modal" aria-label="Close"
                                style="right: 12px;">
                            <span aria-hidden="true" style="font-size: 36px;color: #1B1D1F;">&times;</span>
                        </button>
                    </div>
                    <p class="text-center fs20 mb-0" style="color: #8A8A8A;">Your review was submitted</p>
                    <div class="modal-body" style="margin-top: 64px; padding-top: 0;">
                        <p class="fs15 text-center" style="color: #1B1D1F;">Use the following discount code for 15% off your next purchase!</p>
                        <div class="" style="max-width: 530px; margin: 0 auto; padding: 19px 0; border: 1px solid #707070;">
                            <p class="text-center pb-0 fs26" style="color: #1B1D1F; font-weight: 600;">{{ Session::get('code')  }}</p>
                        </div>
                        <p class="fs15 text-center mt-2" style="color: #1B1D1F; margin-bottom: 62px;">We'll also send it by email</p>

                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('script')
    @if(Session::has('code'))
        <script>
            $("#modal-thanks").modal('show');
        </script>

    @endif
    <script !src="">
        (function($){
            $(document).ready(function(){
                console.log( 'modal review ready! ' );

                $(".btn-write-review").on('click' , function(){
                    goToStep(1);
                });

                $("#modalReview1 .reviewItem").on('click' , function(){
                    var star =  $(this).find(".review-star").attr("data-star");
                    $("input[name='star']").val(star);
                    closeAllReviewModal();
                    //step 2
                    $("#modalReview2").modal('show');

                    //step 3
                    $("#reviewUploadImage").change(function() {
                        if( $(this).val() ){
                            closeAllReviewModal();
                            $("#modalReview3").modal('show');
                        }
                    });
                });
            });




        })(jQuery);//dom ready

        function closeAllReviewModal(){
           // $("#reviewModals .modal").hide();
            $("#reviewModals .modal").modal('hide');
        }
        function goToStep(nth){
            closeAllReviewModal();
            $("#modalReview" + nth).modal('show');
        }

        $("#reviewUplooadImage").change(function() {
            readURL(this, "#reviewUplooadImage");
        });
        function readURL(input, id) {
            $(id + "+ #preview_picture").remove();
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    var src = e.target.result;
                    var img = '<img style="max-width:50%" src="' + src + '" id="preview_picture">';
                    $(id).after(img);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script>
        function showStar(star){
            if(star == 'all'){
                $(".review-area .content_review .card").show();
            }else if(star == "have_comment"){

                $(".content_review .card").hide();
                $(".content_review .card.have_comment").show();
            }else if(star == "have_picture"){

                $(".content_review .card").hide();
                $(".content_review .have_picture").show();
            } else{
                $(".content_review .card").hide();
                $(".content_review .star-" + star).show();
            }

        }
    </script>
@stop