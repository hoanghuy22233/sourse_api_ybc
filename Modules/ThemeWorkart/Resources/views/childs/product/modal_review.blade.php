
{{--    modal customer review --}}
<!--modal-review-->
<form action="{{ route('addReview')  }}" method="POST"  enctype="multipart/form-data">

<div id="reviewModals">


<div id="modalReview1" class="modal fade bd-review" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">How would you rate this items ?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="reviewItem" >
                    <div class="review-star" data-star="5">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="review-name">5 stars</div>
                </div>

                <div class="reviewItem">
                    <div class="review-star" data-star="4">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <div class="review-name">4 stars</div>
                </div>
                <div class="reviewItem">
                    <div class="review-star" data-star="3">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <div class="review-name">3 stars </div>
                </div>
                <div class="reviewItem">
                    <div class="review-star" data-star="2">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <div class="review-name">2 stars </div>
                </div>
                <div class="reviewItem">
                    <div class="review-star" data-star="1">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                    </div>
                    <div class="review-name">1 star</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end-modal rating stars-->

{{--    modal step 2 --}}
<div id="modalReview2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" >
    <div class="modal-dialog modal-dialog-centered modal-custom" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0px !important;padding: 0px 5px">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-header-title"><h3>Show IT OFF</h3></div>
                <div class="modal-header-description">We'D Love to see it in action!</div>
                <div class="modal-border">
                    <h4>Choose photo</h4>
                    <input name="picture" type="file" class="btn" id="reviewUplooadImage" />
                    <div class="modal-sale">Get 15% off your next purchase!</div>
                </div>
            </div>
            <div class="modal-footer" style="border: 0px !important">
                <div class="container">
                    <div class="row">
                        <div class="col-6 col-md-6 col-lg-6 text-left"><a onclick="goToStep(1);">Back</a></div>
                        <div class="col-6 col-md-6 col-lg-6 text-right"><a href="#" data-dismiss="modalReview2" onclick="goToStep(3)">Skip</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--    end modal step 2: upload photo + write review --}}

{{--    modal step 3: enter user info --}}
<div id="modalReview3" class="modal fade" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" >
    <div class="modal-dialog modal-dialog-centered modal-custom modal-custom2" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0px !important;padding: 0px 5px">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-header-title"><h3>About you</h3></div>
                <div class="modal-edit">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <input required name="first_name" type="text" class="form-control" placeholder="First Name*">
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <input required name="last_name" type="text" class="form-control" placeholder="Last Name*">
                            </div>
                        </div>
                        <div class="col-12 col-md-12">
                            <div class="form-group">
                                <input  required name="email" type="email" class="form-control" placeholder="Email*">
                            </div>
                        </div>

                        <div class="col-12 col-md-12">
                            <div class="form-group">
                                <textarea   name="description" class="form-control" placeholder="Share your experience"></textarea>
                            </div>
                        </div>


                        <div class="col-12 col-md-12">
                            <div class="description">Buy submiting, I acknowledege the Pivacy Policy and that my review wall be publicly posted and shared online</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-custom" onclick="finishReview()">Finish</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border: 0px !important">
                <div class="container">
                    <div class="row">
                        <div class="col-6 col-md-12 text-left"><a href="#" onclick="goToStep(2)">Back</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--    modal step 3: enter user info --}}


{{--    modal step 4: get coupon code --}}
<div id="modalReview4" class="modal fade" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" >
    <div class="modal-dialog modal-dialog-centered modal-custom" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0px !important;padding: 0px 5px">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-header-title"><h3>Thank you!</h3></div>
                <div class="modal-header-description">You review was submited</div>
                <p>Use the following discount code for 15% off your next purchase!</p>
                <div class="modal-code">
                    LX-AJFSMG
                </div>
                <p>We'll also send it by email</p>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-custom">Continue</button>
                </div>
            </div>
        </div>
    </div>
</div>
{{--    modal step 4: get coupon code --}}

</div><!-- All review modals-->
    {!! csrf_field() !!}
    <input type="hidden" name="star">
    <input type="hidden" name="product_id" value="{{ $product->id }}">
</form>

