@extends('themeworkart::layouts.master')

@section('main_content')

    <div class="container" style="max-width: 1180px !important;">
        <div class="row mb-3">
            <div class="col-12 p-0">
                <nav class="navbar navbar-expand-sm p-0">
                    @include('themeworkart::partials.breadcrumb')
                </nav>
                <h2 class="pl20 fs22 mb-0" style="padding-left: 5px !important; margin-top: 22px;">{{@$post->name}}</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-8 pl-0" style="max-width: 705px;">
                {!! $post->content !!}
            </div>
            <div class="col-md-1 d-block"></div>

            <div class="col-md-3 d-none d-md-block">
                <img data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb(@$post->image, 500, null) }}" class="img-fluid" alt="{{$post->name}}">
            </div>

        </div>
    </div>
@endsection