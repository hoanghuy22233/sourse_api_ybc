<?php
$cates = Modules\ThemeWorkart\Models\Category::where('status', 1)
    ->where('type', 5)->where('show_menu', 1)->orderBy('order_no', 'asc')
    ->where(function ($query) {
        $query->where('parent_id', 0)->orwhere('parent_id', null);
    })->get();

$dataShowrooms = \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getFromCache('get_showroom_footer');
if (!$dataShowrooms) {
    $dataShowrooms = Modules\ThemeWorkart\Models\Showroom::all();
    \Modules\ThemeWorkart\Http\Helpers\CommonHelper::putToCache('get_showroom_footer', $dataShowrooms);
}
$p = [];
$c = ['values' => [], 'location' => []];
foreach ($dataShowrooms as $key => $t) {
    array_push($p, mb_strtoupper($t['location']));
    foreach ($p as $key => $l) {
        $p = array_unique($p);
    }
}
foreach ($p as $keyp => $l) {
    array_push($c['values'], [$l => []]);
    array_push($c['location'], $l);
    foreach ($dataShowrooms as $key => $t) {
        if (mb_strtoupper($l) == mb_strtoupper($t['location'])) {
            array_push($c['values'][$keyp][$l], $t);
        }
    }
}
$fg = [];
foreach ($c['values'] as $f) {
    foreach ($f as $g) {
        foreach ($g as $gj) {
            $fg[] = $gj;
        }
    }
}
?>
<style>
    .head {
        height: 77px;
    }
    @media (min-width: 992px){
        #menu-show {
            display: block!important
        }
    }
</style>
<div class="f nav" id="menu-show" style="">
    <div class="b flexJus">
        @foreach($cates as $cate)
        <a style="text-align: center!important;" href="{{route('cate.list', ['slug' => $cate->slug])}}" title="{{$cate->name}}">
            <img src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($cate->image, 100, 'auto')}}"
                 alt="{{$cate->name}}"/><span>{{$cate->name}}</span></a>
        @endforeach
    </div>
    <div class="f mcontact">
        <p><a class="fa-phone" href="tel:{{@$settings['phone']}}">Liên hệ mua hàng:
                <strong>{!! @$settings['hotline'] !!}</strong></a>
        </p>
        <p><a class="fa-phone" href="tel:{{@$settings['phone_bh']}}" rel="nofollow">Bảo hành:
                <strong>{{@$settings['phone_bh']}}</strong></a>
        </p>
        <p><a class="fa-phone" href="tel:{{@$settings['phone_kn']}}" rel="nofollow">Khiếu nại:
                <strong>{{@$settings['phone_kn']}}</strong></a>
        </p>
        @if(!empty($c))
        @foreach($c['location'] as $location)
        @php $shoroomLocation = Modules\ThemeWorkart\Models\Showroom::where('location', $location)->orWhere('location', mb_strtolower($location))->orWhere('location', ucfirst($location))->orWhere('location', ucwords($location))->get();   @endphp
        <p><a class="fa-showroom"
              href="{{route('showroom.list', ['slug' => \Modules\ThemeWorkart\Http\Helpers\CommonHelper::convertSlug($location)])}}"
              title="hệ thống showroom">Xem địa chỉ {{count($shoroomLocation)}} Showroom tại {{$location}}</a>
        </p>
        @endforeach
        @endif
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#touch-menu').click(function () {
            $('#menu-show').slideToggle();
            icon = $('header').find("#touch-menu");
            icon.toggleClass("touch-menu mclo")
        })
    })

</script>