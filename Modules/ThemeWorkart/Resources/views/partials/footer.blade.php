<div class="bg-grey footer">
    <div class="container-custom pt-5 pb-5">
        <div class="row">
            <div class="col-2">
                <a href="#" class="ml-3">
                    <img src="{{ asset('public/filemanager/userfiles/' . @$settings['logo_brand'])  }}" class="w-75" alt="">
                </a>
            </div>
            <div class="col-2">
                <p class="fs26">Information</p>
                <ul class="footer-list pl-0">
                    <li class="fs15">About Us</li>
                    <li class="fs15">Contact Us</li>
                    <li class="fs15">Terms Of Service</li>
                    <li class="fs15">DMCA</li>
                </ul>
            </div>
            <div class="col-2">
                <p class="fs26">Information</p>
                <ul class="footer-list pl-0">
                    <li class="fs15">About Us</li>
                    <li class="fs15">Contact Us</li>
                    <li class="fs15">Terms Of Service</li>
                    <li class="fs15">DMCA</li>
                </ul>
            </div>
            <div class="col-3">
                <p class="fs26">Subcribe</p>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Email Address">
                </div>
                <button type="button" class="btn btn-orange text-dark">Subcribe</button>
            </div>
            <div class="col-3">
                <p class="fs26">CustomA2Z - Make It Your!</p>
                <ul class="footer-list pl-0">
                    <li class="fs15">Addess: 812 Mission Street, Suite 500, San Francisco, CA, UNITED STATES</li>
                    <li class="fs15">Email: Support@customa2z.com</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-3 offset-2">
                <ul class="footer-list pl-0">
                    <li class="fs15">Search</li>
                    <li class="fs15">Copyright@2020CUSTOMA2Z - Make It Your!</li>
                    <li class="fs15">Powered by Helitech</li>
                </ul>
            </div>
        </div>
    </div>
</div>