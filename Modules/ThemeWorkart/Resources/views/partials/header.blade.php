{{--header--}}
@php
    $brans = CommonHelper::getFromCache('get_brans');
    if (!$brans){
        $brans = \Modules\ThemeWorkart\Models\Manufacturer::where('status', 1)->orderBy('order_no', 'asc')->get();
        CommonHelper::putToCache('get_brans', $brans);
    }

    $show_bran_homepage = CommonHelper::getFromCache('settings_name_show_bran_homepage_type_homepage_tab');
    if (!$show_bran_homepage){
        $show_bran_homepage = @\Modules\ThemeWorkart\Models\Settings::where('name','show_bran_homepage')->where('type','homepage_tab')->first()->value;
        CommonHelper::putToCache('settings_name_show_bran_homepage_type_homepage_tab', $show_bran_homepage);
    }
    $menus = \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getMenusByLocation('main_menu', 10, false);

    $menus_mobile = \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getMenusByLocation('menu_mobile', 10, false);
@endphp

{{--header start --}}
<div class="header pc">
    <nav class="navbar navbar-expand-lg container-custom row pb-0 pl-0">
        <a class="navbar-brand col-2" href="#">
            <img class="lazy w-75" data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo_brand'])  }}" alt="logo" title="{{ @$settings['logo_brand_intro'] }}" />
        </a>

        <div class="col-6">
            <form class="form-inline" action="search">
                <div class="input-group w-100">
                    <input type="search" name="key" class="form-control header-search-input border-right-0" placeholder="Search">
                    <button class="input-group-prepend btn-search header-search-icon" type="submit">
                        <img src="/img/serach.svg" class="input-group-text header-search border-left-0" alt="search button">
                    </button>
                </div>
            </form>
        </div>

        <div class="col-4">
            <div class="float-right">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link text-dark fs16" href="/track-order">Tracking Your Order</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark fs16" href="/guide">Guide</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark fs16" href="/sizing">
                            Sizing
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link text-dark fs16" href="#">Chart</a>
                    </li>

                    <li class="nav-item dropdown">
                        <button type="button" class="btn btn-outline-dark dropdown-toggle" data-toggle="dropdown">
                            USD
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">USD</a>
                            <a class="dropdown-item" href="#">EUR</a>
                            <a class="dropdown-item" href="#">VNĐ</a>
                        </div>
                    </li>
                    <li class="nav-item position-relative">
                        <a href="#" class="nav-link cart-icon">
                            <img src="./img/cart.svg" alt="">
                        </a>
{{--                        <span class="cart-text">{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::cartGetTotalItem()  }}</span>--}}
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-custom row navbar navbar-expand-lg pt-0 pl-0">
        <div class="col-8">
                <nav class="nav" id="menu-main">
                    @if(!empty($menus))
                        @foreach($menus as $menu1)
                            @if($menu1['link'] == '#')
                                <li class="nav-link text-dark fs16 nav-item menu-{{ str_slug($menu1['name'], '_') }}">
                                    <a class="nav-link" href="javascript:;"
                                       @if($menu1['name'] != 'Sản phẩm') onclick="showmenudv();"
                                       @endif title="{{$menu1['name']}}">{{$menu1['name']}}</a>
                                    @if(!isMobile())
                                        @if(@$settings['option_menu'] == 1 && str_slug($menu1['name'], '_')=='san_pham')
                                            @include('themeworkart::partials.menu_efect.menu_dropdown')
                                        @endif
                                    @endif
                                </li>
                            @else
                                <li class="nav-link text-dark fs16 nav-item menu-{{ str_slug($menu1['name'], '_') }} menu-item">
                                    <a class="nav-link" href="{{ $menu1['link'] }}"
                                       title="{{$menu1['name']}}">{{$menu1['name']}}</a>
                                    @if(!empty($menu1['childs']))
                                        <ul class="sub-menu">
                                            @foreach($menu1['childs'] as $c => $menu2)
                                                <li class="sub-menu-item">
                                                    <a href="{{$menu2['link']}}">{{$menu2['name']}}</a>
                                                    @if(!empty($menu2['childs']))
                                                        <ul class="sub-menu3">
                                                            @foreach($menu2['childs'] as $c3 => $menu3)
                                                                <li class="sub-menu-item3">
                                                                    <a href="{{$menu3['link']}}">{{$menu3['name']}}</a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>

                                    @endif
                                </li>
                            @endif
                        @endforeach
                    @endif
            </nav>
        </div>
        <div class="col-4 pr-0">
            <div class="float-right">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link text-dark" href="/my-account">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark" href="#">USD</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
{{--!header pc--}}

{{--header mobile--}}

<div class="header sp">
    <div class="row">
        <div class="col-2 left-menu-responsive">
            <span class="fs25">
                <a onclick="openNavMobile()"><i class="fas fa-bars"></i></a>
            </span>
        </div>
        <div class="col-8">
            <a class="" href="">
                <img src="./img/logo.png" class="w-100" alt="">
            </a>
        </div>
        <div class="col-2 right-menu-responsive">
            <a href="#" class="cart-icon-responsive">
                <img src="./img/cart.svg" alt="">
            </a>
        </div>
    </div>
    <div id="side-nav-mobile" class="d-none bg-white sidepanel">
        <div class="row">
            <div class="col-12 pl-5">
                <div class="row">
                    <div class="col-3">
                        <div class="d-flex align-items-center justify-content-start h-100">
                            <a class="text-dark">Login</a>
                        </div>
                    </div>
                    <div class="col-3">
                        <button type="button" class="btn btn-outline-dark dropdown-toggle" data-toggle="dropdown">
                            USD
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">USD</a>
                            <a class="dropdown-item" href="#">EUR</a>
                            <a class="dropdown-item" href="#">VNĐ</a>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="d-flex align-items-center justify-content-end h-100">
                            <a onclick="closeNavMobile()">
                                <i class="fas fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <ul class="navbar-nav">
                            @if(!empty($menus_mobile))
                                @foreach($menus_mobile as $menu1)
                                        <li class="nav-item mb-2">
                                            <a href="{{$menu1['link']}}" title="{{$menu1['link']}}">{{$menu1['name']}}</a>
                                        </li>
                                    @endforeach
                                @endif

{{--                            <li class="nav-item mb-2">--}}
{{--                                <a href="/guide">Guide</a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item mb-2">--}}
{{--                                <a href="/sizing">Sizing</a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item mb-2">--}}
{{--                                <a href="/chart">Chart</a>--}}
{{--                            </li>--}}

{{--                            <li class="nav-item mb-2">--}}
{{--                                Personalized Gift--}}
{{--                            </li>--}}
{{--                            <li class="nav-item mb-2">--}}
{{--                                Cloth Face Mask--}}
{{--                            </li>--}}
{{--                            <li class="nav-item mb-2">--}}
{{--                                Tshirt--}}
{{--                            </li>--}}
{{--                            <li class="nav-item mb-2">--}}
{{--                                Canvas & Poster--}}
{{--                            </li>--}}
{{--                            <li class="nav-item mb-2">--}}
{{--                                Blanket--}}
{{--                            </li>--}}
{{--                            <li class="nav-item mb-2">--}}
{{--                                Happy Customers--}}
{{--                            </li>--}}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-mobile-backdrop"></div>
</div>