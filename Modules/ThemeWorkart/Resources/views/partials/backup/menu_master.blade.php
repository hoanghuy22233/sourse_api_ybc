
<style>
    .nav-menu .nav-item .nav-link{
        padding: 0.5rem 5px!important;
    }
    .nav a {
        text-align: left!important;
        width: 100%;
        margin: 0!important;
    }
    .nav span {
        width: 200px;
        line-height: 30px;
        border-bottom: 1px solid #ccc!important;
    }
    .nav-menu .nav-item {
        padding: 20px 0;
    }
    .nav span:hover{
        background: #69a4f0;
        color: #fff;

        font-weight: bold;
    }
    .nav span{
        padding: 0 20px;
        display: block;
    }
    .hleft div a{
        padding: 0!important;
    }
    div#menu-show{
        display: none;
        background: #fff;
    }
    .simM{
        position: relative;
    }
    .simM>span.menu-san_pham:hover div#menu-show{
        position: absolute;
        display: block!important;
        left: 0;
        top: 17px;
        width: 250%!important;
        height: max-content;
        z-index: 1;
    }
</style>
@if(@$settings['option_menu'] == 2)
<div class="flexJus btop">
    <div class="simM">
        @php
            $menus = \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getMenusByLocation('main_menu', 10, false);
        @endphp
        @if(!empty($menus))
            <a href="/{{@$settings['thuong_hieu_slug']}}" title="Thương hiệu">Thương hiệu</a>
            @foreach($menus as $menu)
                @if($menu['link'] == '#')
                        <a class="nav-link" href="javascript:;" onclick="showmenudv();" title="{{$menu['name']}}">{{$menu['name']}}</a>
                @else
                    <a href="{{$menu['link']}}" title="{{$menu['name']}}">{{$menu['name']}}</a>
                @endif
            @endforeach
        @endif
    </div>
    <a class="bg bCart" href="{{route('order.view')}}" title="Xem giỏ hàng" rel="nofollow">
        <label>@php echo Modules\ThemeWorkart\Http\Controllers\Frontend\OrderController::totalCart() @endphp</label>Giỏ hàng</a>
</div>
@endif
<div>
    @include('themeworkart::partials.show_dv')
</div>
<div style="clear: both"></div>

