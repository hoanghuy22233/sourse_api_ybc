<a href="{{route('home')}}" title="{{@$settings['name']}}">
    <img class="lazy" height="30"
            data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo'], 210, null) }}"
            alt="{{@$settings['name']}}"/>
</a>

