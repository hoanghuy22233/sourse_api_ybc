<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/workart/libs/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/workart/libs/fontawesome/fontawesome.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous" />


<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/workart/css/common.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/workart/css/style.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/workart/css/cuongdc.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/workart/css/phongpro.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/workart/css/responsive.css')}}">


{{--<script type="text/javascript" src="{{URL::asset('public/frontend/themes/workart/js/jquery.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{URL::asset('public/frontend/themes/workart/js/bootstrap.min.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{URL::asset('public/frontend/themes/workart/js/owl/dist/owl.carousel.js')}}"></script>--}}
{{--<script type="text/javascript" src="{{URL::asset('public/frontend/themes/workart/js/main.js')}}"></script>--}}


{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>--}}
{{--<script src="{{URL::asset('public/frontend/themes/workart/libs/bootstrap/js/bootstrap.min.js')}}"></script>--}}
{{--<script src="{{URL::asset('public/frontend/themes/workart/libs/fontawesome/all.min.js')}}"></script>--}}
{{--<script src="{{URL::asset('public/frontend/themes/workart/libs/navJs/dist/navigatorJS.min.js')}}"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"></script>--}}
{{--<script src="{{URL::asset('public/frontend/themes/workart/js/common.js')}}"></script>--}}


{!! @$settings['frontend_head_code'] !!}