@extends( 'themeworkart::layouts.master' )

@section('main_content')
        <!--banner start -->
        <div>
                @php
                        $banners = CommonHelper::getFromCache('get_banners_by_slide');
                        if (!$banners) {
                            $banners = \Modules\ThemeWorkart\Models\Banner::where('status', 1)->where('location', 'slide_home')->orderBy('order_no', 'ASC')->paginate(1);
                            CommonHelper::putToCache('get_banners_by_slide', $banners);
                        }

                @endphp

                <div class="banner_home">
                        @if(!empty($banners) || isset($banners))
                                @foreach($banners as $data)
                                        <div class="slider-item img-banner">
                                                <a href="{{$data->link}}" title="{{$data->name}}">
                                                        <img class="lazy" data-img={{$data->image}} data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumbNoCut($data->image)}}"
                                                             alt="{{$data->name}}"/>
                                                </a>
                                        </div>
                                @endforeach
                        @endif
                </div>
        </div>
        <!--banner end-->


        <!--Product start-->

        <div>
                <div class="container-custom">
                        <div class="product-title">
                                <div class="mb-3">
                                        <h4>Collection list</h4>
                                </div>

                                <div class="row no-gutters ">

                                        @php
                                                $features = CommonHelper::getFromCache('collection_list');
                                                if (!$features){
                                                    $features = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(5);
                                                    CommonHelper::putToCache('collection_list', $features);
                                                }
                                        @endphp

                                        @foreach($features as $k=>$featured)
                                                <div class="col-md col-sm-4 col-6">
                                                        <div class="card custom-card-home text-center mr-2">
                                                                <a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($featured, 'array') }}" class="text-decoration-none color-282364">
                                                                        <img class="w-100 mb-3"
                                                                             src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($featured->image, 262 ,null) }}"
                                                                             alt="{{$featured->name}}">
                                                                        <p>{{$featured->name}}</p>
                                                                </a>
                                                        </div>
                                                </div>
                                        @endforeach


{{--                                        <div class="col-md col-sm-4 col-6">--}}
{{--                                                <div class="card custom-card-home text-center mr-2">--}}
{{--                                                        <a href="#" class="text-decoration-none color-282364">--}}
{{--                                                                <img class="w-100 mb-3" src="css/img/copyoptimized-ifo9_295x295.png" alt="product">--}}
{{--                                                                <p>Cloth Face Mask</p>--}}
{{--                                                        </a>--}}
{{--                                                </div>--}}
{{--                                        </div>--}}

                                </div>
                        </div>


                        <!--        product - sale -->
                        <div class="product-sale">
                                <div class="title_home_content d-flex justify-content-between">
                                        <h4 class="font-weight-bold">Best Selling</h4>
                                        <a class="mt-1" href="/best-selling">View more <span><i class="fas fa-angle-double-right"></i></span></a>
                                </div>
                                <div class="row _title-content slick-home no-gutters">
                                                @php
                                                        $product_cat = \App\Http\Helpers\CommonHelper::getFromCache('home_best_selling');
                                                        if(!$product_cat ){
                                                                $product_cat = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(6);
                                                                \Modules\ThemeWorkart\Http\Helpers\CommonHelper::putToCache('home_best_selling' , $product_cat);
                                                        }
                                                @endphp

                                                @foreach($product_cat as $k=>$featured)
                                                        <div class="col-sm">
                                                                @php
                                                                  $item_class = 'selling mr-1';
                                                                @endphp
                                                                @include( 'themeworkart::childs.product.partials.product_loop' )
                                                        </div>
                                                @endforeach
                                </div>
                        </div>

                        <!--    Product New-->
                        <div class="product-new">
                                <div class="title_home_content d-flex justify-content-between">
                                        <h4 class="font-weight-bold">New Arrivals</h4>
                                        <a class="mt-1" href="/new-arrival">View more <span><i class="fas fa-angle-double-right"></i></span></a>
                                </div>
                                <div class="row _title-content slick-home no-gutters">
                                        @php
                                                $product_cat = \App\Http\Helpers\CommonHelper::getFromCache('home_best_selling');
                                                if(!$product_cat ){
                                                        $product_cat = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(6);
                                                        \Modules\ThemeWorkart\Http\Helpers\CommonHelper::putToCache('home_best_selling' , $product_cat);
                                                }
                                        @endphp

                                        @foreach($product_cat as $k=>$featured)
                                                <div class="col-sm p-0">
                                                        @php
                                                                $item_class = ' mr-1';
                                                        @endphp
                                                        @include( 'themeworkart::childs.product.partials.product_loop' )
                                                </div>
                                        @endforeach

{{--                                        <div class="col-sm p-0">--}}
{{--                                                <div class="card mr-1">--}}
{{--                                                        <a href="#">--}}
{{--                                                                <img src="css/img/product-mockup-2.png" alt="product"/>--}}
{{--                                                                <p>3 Sisters - you are my person</p>--}}
{{--                                                                <p> you will always...</p>--}}
{{--                                                                <h5>Cloth Face Mask</h5>--}}
{{--                                                                <p class="price">€21.05 EUR 44% <span>OFF€11.78 EUR</span></p>--}}
{{--                                                        </a>--}}
{{--                                                </div>--}}
{{--                                        </div>--}}

                                </div>
                        </div>
                </div>
        </div>
        <!--Product end-->

        <!-- Home Mug-->

        <div>
                <div class="container-custom">

                        <div class="Mug_news">
                                <div class="title_home_content d-flex justify-content-between">
                                        <h4 class="font-weight-bold color-282364 f-26">Mug</h4>
                                        <a class="mt-1" href="/mugs">View more <span><i class="fas fa-angle-double-right"></i></span></a>
                                </div>
                                <div class="row _title-content home_mobie_none no-gutters">
                                        <div class="product_mug_home col-sm">

                                                @php
                                                        $mug = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(6);
                                                        @$mug1 = [ $mug[0] , $mug[1] ];
                                                @endphp

                                                @if( !empty($mug[0]) && !empty($mug[1]) )
                                                        @foreach($mug1 as $k=>$featured)
                                                                @include( 'themeworkart::childs.product.partials.product_loop' )
                                                        @endforeach
                                                @endif


{{--                                                <div class="card mb-2">--}}
{{--                                                        <a href="#">--}}
{{--                                                                <img src="css/img/778895867ec1f52b1a32241df91868a4.500x500.0.1605153805.png" alt="product">--}}
{{--                                                                <p>3 Sisters - you are my person</p>--}}
{{--                                                                <p> you will always...</p>--}}
{{--                                                                <h5>Personalized Mug</h5>--}}
{{--                                                                <p class="price">€21.05 EUR 44% <span>OFF€11.78 EUR</span></p>--}}
{{--                                                        </a>--}}
{{--                                                </div>--}}

{{--                                                <div class="card">--}}
{{--                                                        <a href="#">--}}
{{--                                                                <img src="css/img/778895867ec1f52b1a32241df91868a4.500x500.0.1605153805.png" alt="product">--}}
{{--                                                                <p>3 Sisters - you are my person</p>--}}
{{--                                                                <p> you will always...</p>--}}
{{--                                                                <h5>Personalized Mug</h5>--}}
{{--                                                                <p class="price">€21.05 EUR 44% <span>OFF€11.78 EUR</span></p>--}}
{{--                                                        </a>--}}
{{--                                                </div>--}}
                                        </div>

                                        <div class="col-sm-8 banner_mug_home ml-2 mr-2">
                                                <img src="css/img/f0309d9385dd58d40ff80c6e2347c66c.1072x1072.0.1605174431.png" alt="Banenr Mug">
                                        </div>

                                        <div class="product_mug_home col-sm ">
                                                @php
                                                        @$mug2 = [ $mug[1] , $mug[2] ];
                                                @endphp

                                                @if(  !empty( $mug[1] ) && !empty( $mug[2] ) )
                                                @foreach($mug2 as $k=>$featured)
                                                        @include( 'themeworkart::childs.product.partials.product_loop' )
                                                @endforeach
                                                @endif
                                        </div>
                                </div>


                                <div class="row _title-content-mobie  slick-home no-gutters">

                                        @foreach($mug as $k=>$featured)
                                                @php
                                                  $item_class = 'col mr-2';
                                                @endphp
                                                @include( 'themeworkart::childs.product.partials.product_loop' )
                                        @endforeach


                                </div>
                        </div>
                </div>
        </div>

        <!-- Home Mug end-->
        <!-- T-shirt Home-->
        <div>
                <div class="container-custom">
                        <div class="shirt_home">
                                <div class="title_home_content d-flex justify-content-between">
                                        <h4 class=" font-weight-bold color-282364 f-26">Tshirt</h4>
                                        <a class="mt-1" href="/tshirts">View more <span><i class="fas fa-angle-double-right"></i></span></a>
                                </div>


                                <div class="row _title-content home_mobie_none no-gutters">

                                        <div class="col-md-8 product-wrap-tshirt">
                                                @php
                                                        $tshirts = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(8);
                                                @endphp

                                                @if( !empty($tshirts) )
                                                        @foreach($tshirts as $k=>$featured)
                                                                <div class="product_t-shirt col-md-3  col-sm-2">
                                                                        @include( 'themeworkart::childs.product.partials.product_loop' )
                                                                </div>
                                                        @endforeach
                                                @endif
                                        </div>

                                        <div class="col-sm-4">
                                                <img src="css/img/9112df15a02cae7df32084178a338861_listingImg_OJ3xsBiBGI.png" alt="T-shirt" width="100%" height="auto">
                                                <a href="/tshirts" class="button_shop-home">
                                                        SHOP NOW
                                                </a>
                                        </div>
                                </div>

                                <div class="row _title-content-mobie  slick-home">
                                        @if( !empty($tshirts) )
                                                @foreach($tshirts as $k=>$featured)
                                                        @php
                                                                $item_class ='col';
                                                        @endphp
                                                        @include( 'themeworkart::childs.product.partials.product_loop' )
                                                @endforeach
                                        @endif
                                </div>
                        </div>
                </div>
        </div>

        <!---->
        <!--Blanket home-->

        <div>
                <div class="container-custom">
                        <div class="shirt_home">
                                <div class="title_home_content d-flex justify-content-between">
                                        <h4 class=" font-weight-bold color-282364 f-26">Blanket</h4>
                                        <a class="mt-1" href="/blankets">View more <span><i class="fas fa-angle-double-right"></i></span></a>
                                </div>
                                <div class="row _title-content home_mobie_none no-gutters">
                                        <div class="col-sm-4">
                                                <img src="css/img/product-mockup-13490-tz6p1xxr_3e226cd8-ef9c-4020-ac8e-15cc6df9aa08_869x869.png" alt="t-shirt" width="100%" height="auto">
                                                <a href="/blankets" class="button_shop-home">
                                                        SHOP NOW
                                                </a>
                                        </div>


                                        <div class="col-md-8 product-wrap-tshirt product-wrap-blanket">
                                                @php
                                                        $blankets = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(8);
                                                @endphp

                                                @if( !empty($blankets) )
                                                        @foreach($blankets as $k=>$featured)
                                                                <div class="product_t-shirt col-md-3  col-sm-2">
                                                                        @include( 'themeworkart::childs.product.partials.product_loop' )
                                                                </div>
                                                        @endforeach
                                                @endif
                                        </div>

                                        <div class="row _title-content-mobie  slick-home">
                                                @if( !empty($blankets) )
                                                        @foreach($blankets as $k=>$featured)
                                                                @php
                                                                        $item_class ='col';
                                                                @endphp
                                                                @include( 'themeworkart::childs.product.partials.product_loop' )
                                                        @endforeach
                                                @endif
                                        </div>

                                </div>
                        </div>
                </div>
        </div>


        <!---->
        <!--Canvas & Poster Home-->

        <div>
                <div class="container-custom">
                        <div class="shirt_home">
                                <div class="title_home_content d-flex justify-content-between">
                                        <h4 class=" font-weight-bold color-282364 f-26">Canvas & Poster</h4>
                                        <a class="mt-1" href="/canvas-posters">View more <span><i class="fas fa-angle-double-right"></i></span></a>
                                </div>


                                <div class="row _title-content home_mobie_none no-gutters">

                                        <div class="col-md-8 product-wrap-tshirt product-wrap-shirt">
                                                @php
                                                        $canvasposters = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(8);
                                                @endphp

                                                @if( !empty($canvasposters) )
                                                        @foreach($canvasposters as $k=>$featured)
                                                                <div class="product_t-shirt col-md-3  col-sm-2">
                                                                        @include( 'themeworkart::childs.product.partials.product_loop' )
                                                                </div>
                                                        @endforeach
                                                @endif
                                        </div>

                                        <div class="col-sm-4">
                                                <img src="css/img/product-mockup-13490-tz6p1xxr_3e226cd8-ef9c-4020-ac8e-15cc6df9aa08_869x869.png" alt="T-shirt" width="100%" height="auto">
                                                <a href="/canvas-posters" class="button_shop-home">
                                                        SHOP NOW
                                                </a>
                                        </div>
                                </div>

                                <div class="row _title-content-mobie  slick-home">
                                        @if( !empty($canvasposters) )
                                                @foreach($canvasposters as $k=>$featured)
                                                        @php
                                                                $item_class ='col';
                                                        @endphp
                                                        @include( 'themeworkart::childs.product.partials.product_loop' )
                                                @endforeach
                                        @endif
                                </div>
                        </div>
                </div>
        </div>
        <!---->

        <!--Cloth Face Mask Home-->

        <div>

                <div class="container-custom">
                        <div class="shirt_home">
                                <div class="title_home_content d-flex justify-content-between">
                                        <h4 class=" font-weight-bold color-282364 f-26">Cloth Face Mask</h4>
                                        <a class="mt-1" href="/cloth-face-masks">View more <span><i class="fas fa-angle-double-right"></i></span></a>
                                </div>

                                <div class="row _title-content home_mobie_none no-gutters">
                                        <div class="col-sm-4">
                                                <img src="css/img/khau trang.png" alt="facemask" width="100%" height="auto">
                                                <a href="/cloth-face-masks" class="button_shop-home">
                                                        SHOP NOW
                                                </a>
                                        </div>

                                        <div class="col-md-8 product-wrap-facemasks">
                                                @php
                                                        $facemasks = \Modules\ThemeWorkart\Models\Product::where('featured',1)->where('status',1)->paginate(8);
                                                @endphp

                                                @if( !empty($facemasks) )
                                                        @foreach($facemasks as $k=>$featured)
                                                                <div class="product_t-shirt col-md-3  col-sm-2">
                                                                        @include( 'themeworkart::childs.product.partials.product_loop' )
                                                                </div>
                                                        @endforeach
                                                @endif
                                        </div>

                                </div>


                                <div class="row _title-content-mobie  slick-home">
                                        @if( !empty($blankets) )
                                                @foreach($blankets as $k=>$featured)
                                                        @php
                                                                $item_class ='col';
                                                        @endphp
                                                        @include( 'themeworkart::childs.product.partials.product_loop' )
                                                @endforeach
                                        @endif
                                </div>

                        </div>
                </div>
        </div>


{{--        <!-- Review Home-->--}}
        <div class="bg-grey">
        @include('themeworkart::childs.product.partials.review')
        </div>
{{--        end reiew home --}}
@endsection

@section('script')
        <script>
                function showStar(star){
                        if(star == 'all'){
                                $(".review-area .content_review .card").show();
                        }else if(star == "have_comment"){

                                $(".content_review .card").hide();
                                $(".content_review .card.have_comment").show();
                        }else if(star == "have_picture"){

                                $(".content_review .card").hide();
                                $(".content_review .have_picture").show();
                        } else{
                                $(".content_review .card").hide();
                                $(".content_review .star-" + star).show();
                        }

                }
        </script>
@stop