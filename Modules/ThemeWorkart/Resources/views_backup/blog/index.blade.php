@extends('themelogistics::layouts.layout_homepage')
@section('main_content')
    <section class="bg_cont">
        <a class="logo" href="#" style="background: url({{URL::asset('public/themelogistic/images/bg_pa.png')}}) repeat center center;">
            <img src="{{URL::asset('public/themelogistic/images/logopng.png')}}" alt="Logo">
        </a>
        <img
                src="{{URL::asset('public/themelogistic/images/bg_news.jpg')}}"
                class="img_bg_cont"
        />
        <h1 class="t_cont">Tin tức và Bài Viết</h1>
        <ul class="ul_breacrum" xmlns:v="http://rdf.data-vocabulary.org/#">
            <li class="list__item item--af">
                <a href="#"> Trang chủ </a>
                <i class="fas fa-angle-double-right"></i>
            </li>
            <li class="list__item item--af">
                <a href="#"> Tin tức và Bài Viết </a>
                <i class="fas fa-angle-double-right"></i>
            </li>
            <li class="list__item">
                <a href="#"> Tin tức </a>
            </li>
        </ul>
        <!-- End .ul-breacrum -->
    </section>

    <section class="f_cont min_wrap">
        <ul class="list_news filter-button-group">
            <li class="active news-item border_lb" data-filter=".tt"><a href="javascript:void(0)">Tin tức</a></li>
            <li class="news-item" data-filter=".ttn"><a href="javascript:void(0)">Tin Trong Ngành</a></li>
            <li class="news-item border_rb"data-filter=".bl"><a href="javascript:void(0)">Blog</a></li>
        </ul>
        <!-- End .list_news -->
        <ul class="cont__list clearfix grid">
            <li class="list-item grid-item tt ttn">
                <a href="">
                    <figure class="item__img">
                        <img src="{{URL::asset('public/themelogistic/images/tintuc.jpg')}}" class="img_object_fit" alt="" />
                    </figure>
                    <div class="nd_news">
                        <h3>VANTAGE ĐẠT CHỨNG NHẬN AN NINH HÀNG HÓA C-TPAT</h3>
                        <ol>
                            <li>
                                <svg
                                        class="svg-inline--fa fa-calendar-alt fa-w-14"
                                        aria-hidden="true"
                                        data-fa-processed=""
                                        data-prefix="far"
                                        data-icon="calendar-alt"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 448 512"
                                >
                                    <path
                                            fill="currentColor"
                                            d="M148 288h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm108-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 96v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm192 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96-260v352c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h48V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h128V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h48c26.5 0 48 21.5 48 48zm-48 346V160H48v298c0 3.3 2.7 6 6 6h340c3.3 0 6-2.7 6-6z"
                                    ></path></svg
                                ><!-- <i class="far fa-calendar-alt"></i> -->
                                21/12/2020
                            </li>
                            <li>
                                <svg
                                        class="svg-inline--fa fa-eye fa-w-18"
                                        aria-hidden="true"
                                        data-fa-processed=""
                                        data-prefix="far"
                                        data-icon="eye"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512"
                                >
                                    <path
                                            fill="currentColor"
                                            d="M569.354 231.631C512.97 135.949 407.81 72 288 72 168.14 72 63.004 135.994 6.646 231.631a47.999 47.999 0 0 0 0 48.739C63.031 376.051 168.19 440 288 440c119.86 0 224.996-63.994 281.354-159.631a47.997 47.997 0 0 0 0-48.738zM288 392c-102.556 0-192.091-54.701-240-136 44.157-74.933 123.677-127.27 216.162-135.007C273.958 131.078 280 144.83 280 160c0 30.928-25.072 56-56 56s-56-25.072-56-56l.001-.042C157.794 179.043 152 200.844 152 224c0 75.111 60.889 136 136 136s136-60.889 136-136c0-31.031-10.4-59.629-27.895-82.515C451.704 164.638 498.009 205.106 528 256c-47.908 81.299-137.444 136-240 136z"
                                    ></path></svg
                                ><!-- <i class="far fa-eye"></i> -->
                                15
                            </li>
                        </ol>
                    </div>
                </a>
            </li>
            <li class="list-item grid-item tt bl ttn">
                <a href="">
                    <figure class="item__img">
                        <img src="{{URL::asset('public/themelogistic/images/tintuc.jpg')}}" class="img_object_fit" alt="" />
                    </figure>
                    <div class="nd_news">
                        <h3>VANTAGE ĐẠT CHỨNG NHẬN AN NINH HÀNG HÓA C-TPAT</h3>
                        <ol>
                            <li>
                                <svg
                                        class="svg-inline--fa fa-calendar-alt fa-w-14"
                                        aria-hidden="true"
                                        data-fa-processed=""
                                        data-prefix="far"
                                        data-icon="calendar-alt"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 448 512"
                                >
                                    <path
                                            fill="currentColor"
                                            d="M148 288h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm108-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 96v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm192 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96-260v352c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h48V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h128V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h48c26.5 0 48 21.5 48 48zm-48 346V160H48v298c0 3.3 2.7 6 6 6h340c3.3 0 6-2.7 6-6z"
                                    ></path></svg
                                ><!-- <i class="far fa-calendar-alt"></i> -->
                                21/12/2020
                            </li>
                            <li>
                                <svg
                                        class="svg-inline--fa fa-eye fa-w-18"
                                        aria-hidden="true"
                                        data-fa-processed=""
                                        data-prefix="far"
                                        data-icon="eye"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512"
                                >
                                    <path
                                            fill="currentColor"
                                            d="M569.354 231.631C512.97 135.949 407.81 72 288 72 168.14 72 63.004 135.994 6.646 231.631a47.999 47.999 0 0 0 0 48.739C63.031 376.051 168.19 440 288 440c119.86 0 224.996-63.994 281.354-159.631a47.997 47.997 0 0 0 0-48.738zM288 392c-102.556 0-192.091-54.701-240-136 44.157-74.933 123.677-127.27 216.162-135.007C273.958 131.078 280 144.83 280 160c0 30.928-25.072 56-56 56s-56-25.072-56-56l.001-.042C157.794 179.043 152 200.844 152 224c0 75.111 60.889 136 136 136s136-60.889 136-136c0-31.031-10.4-59.629-27.895-82.515C451.704 164.638 498.009 205.106 528 256c-47.908 81.299-137.444 136-240 136z"
                                    ></path></svg
                                ><!-- <i class="far fa-eye"></i> -->
                                15
                            </li>
                        </ol>
                    </div>
                </a>
            </li>
            <li class="list-item grid-item tt bl ttn">
                <a href="">
                    <figure class="item__img">
                        <img src="{{URL::asset('public/themelogistic/images/tintuc.jpg')}}" class="img_object_fit" alt="" />
                    </figure>
                    <div class="nd_news">
                        <h3>VANTAGE ĐẠT CHỨNG NHẬN AN NINH HÀNG HÓA C-TPAT</h3>
                        <ol>
                            <li>
                                <svg
                                        class="svg-inline--fa fa-calendar-alt fa-w-14"
                                        aria-hidden="true"
                                        data-fa-processed=""
                                        data-prefix="far"
                                        data-icon="calendar-alt"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 448 512"
                                >
                                    <path
                                            fill="currentColor"
                                            d="M148 288h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm108-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 96v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm192 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96-260v352c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h48V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h128V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h48c26.5 0 48 21.5 48 48zm-48 346V160H48v298c0 3.3 2.7 6 6 6h340c3.3 0 6-2.7 6-6z"
                                    ></path></svg
                                ><!-- <i class="far fa-calendar-alt"></i> -->
                                21/12/2020
                            </li>
                            <li>
                                <svg
                                        class="svg-inline--fa fa-eye fa-w-18"
                                        aria-hidden="true"
                                        data-fa-processed=""
                                        data-prefix="far"
                                        data-icon="eye"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512"
                                >
                                    <path
                                            fill="currentColor"
                                            d="M569.354 231.631C512.97 135.949 407.81 72 288 72 168.14 72 63.004 135.994 6.646 231.631a47.999 47.999 0 0 0 0 48.739C63.031 376.051 168.19 440 288 440c119.86 0 224.996-63.994 281.354-159.631a47.997 47.997 0 0 0 0-48.738zM288 392c-102.556 0-192.091-54.701-240-136 44.157-74.933 123.677-127.27 216.162-135.007C273.958 131.078 280 144.83 280 160c0 30.928-25.072 56-56 56s-56-25.072-56-56l.001-.042C157.794 179.043 152 200.844 152 224c0 75.111 60.889 136 136 136s136-60.889 136-136c0-31.031-10.4-59.629-27.895-82.515C451.704 164.638 498.009 205.106 528 256c-47.908 81.299-137.444 136-240 136z"
                                    ></path></svg
                                ><!-- <i class="far fa-eye"></i> -->
                                15
                            </li>
                        </ol>
                    </div>
                </a>
            </li>
            <li class="list-item grid-item tt ttn">
                <a href="">
                    <figure class="item__img">
                        <img src="{{URL::asset('public/themelogistic/images/tintuc.jpg')}}" class="img_object_fit" alt="" />
                    </figure>
                    <div class="nd_news">
                        <h3>VANTAGE ĐẠT CHỨNG NHẬN AN NINH HÀNG HÓA C-TPAT</h3>
                        <ol>
                            <li>
                                <svg
                                        class="svg-inline--fa fa-calendar-alt fa-w-14"
                                        aria-hidden="true"
                                        data-fa-processed=""
                                        data-prefix="far"
                                        data-icon="calendar-alt"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 448 512"
                                >
                                    <path
                                            fill="currentColor"
                                            d="M148 288h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm108-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 96v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm192 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96-260v352c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h48V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h128V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h48c26.5 0 48 21.5 48 48zm-48 346V160H48v298c0 3.3 2.7 6 6 6h340c3.3 0 6-2.7 6-6z"
                                    ></path></svg
                                ><!-- <i class="far fa-calendar-alt"></i> -->
                                21/12/2020
                            </li>
                            <li>
                                <svg
                                        class="svg-inline--fa fa-eye fa-w-18"
                                        aria-hidden="true"
                                        data-fa-processed=""
                                        data-prefix="far"
                                        data-icon="eye"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512"
                                >
                                    <path
                                            fill="currentColor"
                                            d="M569.354 231.631C512.97 135.949 407.81 72 288 72 168.14 72 63.004 135.994 6.646 231.631a47.999 47.999 0 0 0 0 48.739C63.031 376.051 168.19 440 288 440c119.86 0 224.996-63.994 281.354-159.631a47.997 47.997 0 0 0 0-48.738zM288 392c-102.556 0-192.091-54.701-240-136 44.157-74.933 123.677-127.27 216.162-135.007C273.958 131.078 280 144.83 280 160c0 30.928-25.072 56-56 56s-56-25.072-56-56l.001-.042C157.794 179.043 152 200.844 152 224c0 75.111 60.889 136 136 136s136-60.889 136-136c0-31.031-10.4-59.629-27.895-82.515C451.704 164.638 498.009 205.106 528 256c-47.908 81.299-137.444 136-240 136z"
                                    ></path></svg
                                ><!-- <i class="far fa-eye"></i> -->
                                15
                            </li>
                        </ol>
                    </div>
                </a>
            </li>
            <li class="list-item grid-item tt ttn">
                <a href="">
                    <figure class="item__img">
                        <img src="{{URL::asset('public/themelogistic/images/tintuc.jpg')}}" class="img_object_fit" alt="" />
                    </figure>
                    <div class="nd_news">
                        <h3>VANTAGE ĐẠT CHỨNG NHẬN AN NINH HÀNG HÓA C-TPAT</h3>
                        <ol>
                            <li>
                                <svg
                                        class="svg-inline--fa fa-calendar-alt fa-w-14"
                                        aria-hidden="true"
                                        data-fa-processed=""
                                        data-prefix="far"
                                        data-icon="calendar-alt"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 448 512"
                                >
                                    <path
                                            fill="currentColor"
                                            d="M148 288h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm108-12v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 96v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm-96 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm192 0v-40c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v40c0 6.6 5.4 12 12 12h40c6.6 0 12-5.4 12-12zm96-260v352c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h48V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h128V12c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v52h48c26.5 0 48 21.5 48 48zm-48 346V160H48v298c0 3.3 2.7 6 6 6h340c3.3 0 6-2.7 6-6z"
                                    ></path></svg
                                ><!-- <i class="far fa-calendar-alt"></i> -->
                                21/12/2020
                            </li>
                            <li>
                                <svg
                                        class="svg-inline--fa fa-eye fa-w-18"
                                        aria-hidden="true"
                                        data-fa-processed=""
                                        data-prefix="far"
                                        data-icon="eye"
                                        role="img"
                                        xmlns="http://www.w3.org/2000/svg"
                                        viewBox="0 0 576 512"
                                >
                                    <path
                                            fill="currentColor"
                                            d="M569.354 231.631C512.97 135.949 407.81 72 288 72 168.14 72 63.004 135.994 6.646 231.631a47.999 47.999 0 0 0 0 48.739C63.031 376.051 168.19 440 288 440c119.86 0 224.996-63.994 281.354-159.631a47.997 47.997 0 0 0 0-48.738zM288 392c-102.556 0-192.091-54.701-240-136 44.157-74.933 123.677-127.27 216.162-135.007C273.958 131.078 280 144.83 280 160c0 30.928-25.072 56-56 56s-56-25.072-56-56l.001-.042C157.794 179.043 152 200.844 152 224c0 75.111 60.889 136 136 136s136-60.889 136-136c0-31.031-10.4-59.629-27.895-82.515C451.704 164.638 498.009 205.106 528 256c-47.908 81.299-137.444 136-240 136z"
                                    ></path></svg
                                ><!-- <i class="far fa-eye"></i> -->
                                15
                            </li>
                        </ol>
                    </div>
                </a>
            </li>
        </ul>
        <div class="page">
            <div class="PageNum">
                <span>1</span><a rel="nofollow" href="">2</a><a rel="nofollow" href="">3</a><a rel="nofollow" href="">4</a><a rel="nofollow" href="">5</a><a rel="nofollow" href="1"> Trang kế </a><a rel="nofollow" href=""> Trang cuối </a>            </div>
            <div class="clear"></div>
        </div>
    </section>
    @endsection