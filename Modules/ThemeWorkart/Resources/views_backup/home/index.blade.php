@extends('themeworkart::layouts.layout_homepage')
@section('main_content')
    <style>
        @media (max-width: 768px) {
            .bnews li .bsum {
                display: none;
            }
        }

        @media (min-width: 768px) {
            .tim {
                width: 72% !important;
            }
        }

        @media (min-width: 1300px) {
            .topic img, .topic object {
                display: inline-block;
            }

            @for($i = 0; $i < count($posts); $i++)
            @if($i % 2 == 0)
                .dd-{{ $i }}  {
                display: inline-block;
                text-align: left;
            }

            .dd-{{ $i }}  {
                display: inline-block;
                text-align: right;
            }

            @else
                .dd-{{ $i }}  {
                display: inline-block;
                text-align: right;
            }

            .dd-{{ $i }}  {
                display: inline-block;
                text-align: left;
            }

            @endif
            @endfor

            .fix-box {
                min-height: 300px !important;
            }

        }

        .bnews a div {
            max-height: 150px;
            overflow: hidden;
            text-align: center;
            display: inline-block;
        }

        .bnews li {
            padding-bottom: 0;
            border: 1px #ccc solid;
            border-radius: 4px;
            box-shadow: 1px 1px 2px #666;
            background: #fff;
            padding: 10px !important;
            text-align: center;
        }
    </style>
    <div class="f f123" style="background:#fbfbfb;padding-bottom:30px;">
        @if(!empty($posts))
            @foreach($posts as $k => $post)
                <?php
                $cate_get_by_post = CommonHelper::getFromCache('get_category_by_post' . $post->multi_cat);
                if (!$cate_get_by_post) {
                    $cate_get_by_post = Modules\ThemeWorkart\Models\Category::whereIn('id', explode('|', $post->multi_cat))->first();
                    CommonHelper::putToCache('get_category_by_post' . $post->multi_cat, $cate_get_by_post);
                }
                ?>
                <div class="topic">
                    <div class="ddimg flexL dd-{{ $k }}" style="order:{{$post->oder}};" id="nb0">
                        <img class="lazy0 lazy"
                             data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($post->image, 675, null) }}"
                             alt="{{$post->name}}"/>
                    </div>
                    <div class="ddinfo padd1 dc-{{ $k }}" style="order:@if($post->oder == 1) 2 @else 1 @endif;">
                        <h1>{{$post->name}}</h1>
                        <p>
                            <strong>{{$post->intro}}</strong>
                            {!!  $post->content !!}
                        </p>
                    </div>
                </div>
            @endforeach
        @endif
    </div>

    <script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "Store",
        "image": [
            "https://bephoangcuong.com/public/filemanager/userfiles/1.logo/BepHC-logo-Web8.png"
        ],
        "@id": "https://bephoangcuong.com/",
        "name": "Bếp Hoàng Cương - Since 1995",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Số 348 Bạch Đằng",
            "addressLocality": "Quận Bình Thạnh",
            "addressRegion": "Thành phố Hồ Chí Minh",
            "postalCode": "100000",
            "addressCountry": "Vietnam"
        },
        "url": "https://bephoangcuong.com/",
        "priceRange": "VND",
        "telephone": "0974329191",
        "openingHoursSpecification": [
            {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": [
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday",
                    "Saturday"
                ],
                "opens": "08:00",
                "closes": "21:00"
            },
            {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": "Sunday",
                "opens": "08:00",
                "closes": "21:00"
            }
        ]
    }

    </script>
@endsection
