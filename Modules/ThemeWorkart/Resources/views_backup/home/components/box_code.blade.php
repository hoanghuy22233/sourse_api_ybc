@php

    function createSelect($data, $keyselect = null){
        $html = '';
        foreach($data as $k => $item){
            $selected = $keyselect == $k ? "selected" : "";
            $html .= '<option '.$selected.' value="'.$k.'">'.$item.'</option>';
        }

        return $html;
    }
@endphp
<section class="w-100 mt-2 mb-3">
    <div class="container">
        <div class="row align-items-center d-flex">
            <div class="box-cp-l box-cp" id="content">
                <h1 class="entry-cp font-IntelBold mb-2">Dịch vụ Chuyển phát thu hộ (COD)</h1>
                <div class="summary text-justify">Dịch vụ CPTH (COD) là dịch vụ chuyển phát hàng hóa kết hợp
                    thu tiền cho khách hàng. Phù hợp với những khách hàng kinh doanh thương mại điện tử
                    (E-commerce). Thời gian chuyển phát tiêu chuẩn theo lựa chọn của khách hàng từ 01 ngày
                    đến 07 ngày tùy theo khu vực giao hàng.
                </div>
                <a href="tin-tuc/dv-van-chuyen-thuong-mai-dien-tu-cod/index.html" class="d-inline-block mt-2 mb-2">Xem
                    thêm <i class="ntl-Thin-Arrow-4"></i></a>
            </div>
            <div class="box-cp-r">
                <div class="w-100 border pt-2 pl-md-3 pr-md-3">
                    <form class="row position-relative form-service pb-5 pb-md-0"
                          action="#" method="get" id="tra_cuoc">
                        <input type="hidden" name="package_no" value="1">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <label class="text-dark font-size-16">Nơi gửi</label>
                            <div class="row">
                                <div class="col-lg-6 col-sm-6 col-12 pr-sm-1">
                                    <div class="form-group">
                                        <!--start section -->
                                        <div class="dropdown bootstrap-select form-control change_city border-cam">
                                            <select name="send_city_id" class="selectpicker form-control change_city border-cam" id="send_city_id" data-id="send_district_id" data-live-search="true" data-size="10" tabindex="-98">
                                            {!! createSelect($province, 1) !!}
                                            </select>
                                        </div>
                                        <!-- end section -->
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6 col-12 pl-sm-1">
                                    <div class="form-group">
                                        <!--start section -->
                                        <div class="dropdown bootstrap-select form-control change_city border-cam">
                                            <select name="send_district_id" class="selectpicker form-control border-cam" id="send_district_id" data-id="send_district_id" data-live-search="true" data-size="10" tabindex="-98">
                                                {!! createSelect($district) !!}
                                            </select>
                                        </div>
                                        <!-- end section -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 pl-md-1">
                            <label class="text-dark font-size-16">Nơi nhận</label>
                            <div class="row">
                                <div class="col-lg-6 col-sm-6 col-12 pr-sm-1">
                                    <div class="form-group">
                                        <!--start section -->
                                        <div class="dropdown bootstrap-select form-control change_city border-cam">
                                            <select name="recieve_city_id" class="selectpicker form-control change_city border-cam" id="recieve_city_id" data-id="recieve_city_id" data-live-search="true" data-size="10" tabindex="-98">
                                                {!! createSelect($province, 1) !!}
                                            </select>
                                        </div>
                                        <!-- end section -->
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6 col-12 pl-sm-1">
                                    <div class="form-group">
                                        <!--start section -->
                                        <div class="dropdown bootstrap-select form-control change_city border-cam">
                                            <select name="recieve_district_id" class="selectpicker form-control border-cam" id="recieve_district_id" data-id="recieve_district_id" data-live-search="true" data-size="10" tabindex="-98">
                                                {!! createSelect($district) !!}
                                            </select>
                                        </div>
                                        <!-- end section -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 position-static">
                            <div class="form-group">
                                <label class="text-dark font-size-16">Trọng lượng (kg)</label>
                                <div class="w-100">
                                    <input type="number" class="form-control" name="weight"
                                           placeholder="Nhập trọng lượng">
                                </div>
                                <div class="w-100 font-size-14 text-dark mt-sm-3 mt-1">* Chọn đúng đặc tính sẽ giúp
                                    hàng của bạn đi đúng lịch trình, ước lượng và tìm được đúng giá cước
                                </div>
                                <div class="w-100 mt-sm-3 position-submit">
                                    <input type="hidden" id="feature" name="feature" value="1">
                                    <input type="hidden" id="cargo_content" name="cargo_content" value="Tài liệu">
                                    <input type="button" name="search" data-toggle="modal" data-target="#service-modal"  value="Tra cứu giá cước"
                                           class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12 pl-md-1">
                            <div class="form-group">
                                <label class="text-dark font-size-16">Đặc tính hàng hóa</label>
                                <ul class="row dactinh">
                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1 current"
                                        data-id="Tài liệu" data-value="1">
                                        <div class="w-100 text-dark text-center ">
                                                <span class="icon ntl-Letter d-block"><span class="path1"></span><span
                                                            class="path2"></span><span class="path3"></span><span
                                                            class="path4"></span><span class="path5"></span><span
                                                            class="path6"></span><span class="path7"></span><span
                                                            class="path8"></span><span class="path9"></span><span
                                                            class="path10"></span><span class="path11"></span><span
                                                            class="path12"></span><span class="path13"></span><span
                                                            class="path14"></span><span class="path15"></span><span
                                                            class="path16"></span></span>
                                            <div class="mt-2">Tài liệu</div>
                                        </div>
                                    </li>
                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1"
                                        data-id="Thời trang Mỹ phẩm" data-value="2">
                                        <div class="w-100 text-dark text-center">
                                                <span class="icon ntl-Coat-and-lipstick d-block"><span
                                                            class="path1"></span><span class="path2"></span><span
                                                            class="path3"></span><span class="path4"></span><span
                                                            class="path5"></span><span class="path6"></span><span
                                                            class="path7"></span><span class="path8"></span><span
                                                            class="path9"></span><span class="path10"></span><span
                                                            class="path11"></span><span class="path12"></span><span
                                                            class="path13"></span><span class="path14"></span><span
                                                            class="path15"></span><span class="path16"></span><span
                                                            class="path17"></span><span class="path18"></span><span
                                                            class="path19"></span><span class="path20"></span><span
                                                            class="path21"></span></span>
                                            <div class="mt-2">Thời trang Mỹ phẩm</div>
                                        </div>
                                    </li>
                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1" data-id="Đồ điện tử"
                                        data-value="2">
                                        <div class="w-100 text-dark text-center">
                                                <span class="icon ntl-Phone d-block"><span class="path1"></span><span
                                                            class="path2"></span><span class="path3"></span><span
                                                            class="path4"></span><span class="path5"></span><span
                                                            class="path6"></span><span class="path7"></span><span
                                                            class="path8"></span><span class="path9"></span><span
                                                            class="path10"></span><span class="path11"></span><span
                                                            class="path12"></span><span class="path13"></span><span
                                                            class="path14"></span><span class="path15"></span><span
                                                            class="path16"></span><span class="path17"></span><span
                                                            class="path18"></span><span class="path19"></span><span
                                                            class="path20"></span><span class="path21"></span><span
                                                            class="path22"></span><span class="path23"></span><span
                                                            class="path24"></span><span class="path25"></span><span
                                                            class="path26"></span><span class="path27"></span></span>
                                            <div class="mt-2">Đồ điện tử
                                                <!-- <br>tử --></div>
                                        </div>
                                    </li>
                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1"
                                        data-id="Dược phẩm TPCN" data-value="2">
                                        <div class="w-100 text-dark text-center">
                                                <span class="icon ntl-Medicine d-block"><span class="path1"></span><span
                                                            class="path2"></span><span class="path3"></span><span
                                                            class="path4"></span><span class="path5"></span><span
                                                            class="path6"></span><span class="path7"></span><span
                                                            class="path8"></span><span class="path9"></span><span
                                                            class="path10"></span><span class="path11"></span><span
                                                            class="path12"></span><span class="path13"></span><span
                                                            class="path14"></span></span>
                                            <div class="mt-2">Dược phẩm
                                                <!-- <br>TPCN --></div>
                                        </div>
                                    </li>
                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1" data-id="Thực phẩm"
                                        data-value="2">
                                        <div class="w-100 text-dark text-center">
                                                <span class="icon ntl-Bread d-block"><span class="path1"></span><span
                                                            class="path2"></span><span class="path3"></span><span
                                                            class="path4"></span><span class="path5"></span><span
                                                            class="path6"></span><span class="path7"></span><span
                                                            class="path8"></span><span class="path9"></span><span
                                                            class="path10"></span><span class="path11"></span><span
                                                            class="path12"></span></span>
                                            <div class="mt-2">Thực phẩm</div>
                                        </div>
                                    </li>
                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1"
                                        data-id="Đồ tươi sống" data-value="2">
                                        <div class="w-100 text-dark text-center">
                                                <span class="icon ntl-Meat d-block"><span class="path1"></span><span
                                                            class="path2"></span><span class="path3"></span><span
                                                            class="path4"></span><span class="path5"></span><span
                                                            class="path6"></span><span class="path7"></span><span
                                                            class="path8"></span><span class="path9"></span><span
                                                            class="path10"></span><span class="path11"></span><span
                                                            class="path12"></span></span>
                                            <div class="mt-2">Đồ tươi sống</div>
                                        </div>
                                    </li>
                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1" data-id="Đồ có pin"
                                        data-value="2">
                                        <div class="w-100 text-dark text-center">
                                                <span class="icon ntl-Battery d-block"><span class="path1"></span><span
                                                            class="path2"></span><span class="path3"></span><span
                                                            class="path4"></span></span>
                                            <div class="mt-2">Đồ có pin</div>
                                        </div>
                                    </li>
                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1"
                                        data-id="Chất bột nước" data-value="2">
                                        <div class="w-100 text-dark text-center">
                                                <span class="icon ntl-Water d-block"><span class="path1"></span><span
                                                            class="path2"></span><span class="path3"></span><span
                                                            class="path4"></span><span class="path5"></span><span
                                                            class="path6"></span></span>
                                            <div class="mt-2 mb-1">Chất bột nước</div>
                                        </div>
                                    </li>
                                    <li class="border bg-fff translate rounded pt-1 pb-1 mb-1"
                                        data-id="Chất dễ cháy" data-value="2">
                                        <div class="w-100 text-dark text-center">
                                                <span class="icon ntl-Fire d-block"><span class="path1"></span><span
                                                            class="path2"></span><span class="path3"></span><span
                                                            class="path4"></span></span>
                                            <div class="mt-2">Chất dễ cháy</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@section('script')
    <script src="{{ asset('public/themelogistic/js/select.js') }}"></script>
    <script>
        $("select[name='send_city_id']").change(function(){
            var id_pro = $(this).val();
            $.ajax({
               url: '{{ route('ajax_get_district') }}',
                data: { 'province_id' : id_pro},
                dataType:'html',
                success: function(data){
                   var current = $("select[name='send_district_id']");
                    current.empty();
                    current.html(data);
                    current.selectpicker('refresh');
                },
                error: function(){
                   alert('Có lỗi Server, vui lòng bấm f5 để load lại trang');
                }
            });
        });
        $("select[name='recieve_city_id']").change(function(){
            var id_pro = $(this).val();
            $.ajax({
                url: '{{ route('ajax_get_district') }}',
                data: { 'province_id' : id_pro},
                dataType:'html',
                success: function(data){
                    var current = $("select[name='recieve_district_id']");
                    current.empty();
                    current.html(data);
                    current.selectpicker('refresh');
                },
                error: function(){
                    alert('Có lỗi Server, vui lòng bấm f5 để load lại trang');
                }
            });
        })
    </script>
@stop