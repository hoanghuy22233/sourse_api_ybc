<div class="w-100 wp-hotline-footer text-center text-white pt-5 pb-5">
    <div class="container">

        <div class="row">
            <div class="col-lg-8 col-md-8 col-12 offset-md-2">
                <a class="hotline SanFranciscoDisplay-Bold" href="tel:0975 116 893">HOTLINE: 0975 116 893</a>
                <p class="text-nn">Liên hệ Etal Group Logistics để được tư vấn chi tiết và có giá dịch vụ tốt
                    nhất!</p>
                <p style="color: #FCD804; font-size: 20px;">Lấy hàng tận nơi. Giao hàng tận tay. Phục vụ tận
                    tình</p>
                <div class="col-lg-12 col-12">
                    <div class="row d-flex justify-content-center align-items-center font-size-16 pt-2 mt-4 mb-3 content-ft">
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12 mb-4 mb-lg-0">
                            <a href="demo/dist/shop.html"
                               class="d-inline-block p-2 rounded bg-white d-flex justify-content-center align-items-center"><span
                                        class="icon ntl-Location-1 mr-3"><span class="path1"></span><span
                                            class="path22"></span><span class="path3"></span><span
                                            class="path4"></span><span class="path5"></span></span> SHop bán hàng</a>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                            <a href="tra-cuoc.html"
                               class="d-inline-block p-2 rounded d-flex justify-content-center align-items-center"><span
                                        class="icon ntl-Box-search mr-3"></span> Tra cứu vận đơn</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>