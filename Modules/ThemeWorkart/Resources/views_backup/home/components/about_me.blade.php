<section class="network">
    <div class="container do-you-know">
        <div class="row">
        <?php
            $widgets = CommonHelper::getFromCache('widgets_home_sidebar_center', ['widgets']);
            if (!$widgets) {
                $widgets =  \Modules\ThemeLogistics\Models\Widget::where('location', 'home3')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
                CommonHelper::putToCache('widgets_home_sidebar_center', $widgets, ['widgets']);
            }
            $networks = CommonHelper::getFromCache('get_banners_by_slide');
            if (!$networks) {
                $networks = \Modules\ThemeLogistics\Models\Banner::where('status', 1)->where('location', 'banner_network')->orderBy('order_no', 'DESC')->get();
                CommonHelper::putToCache('get_banners_by_slide', $networks);
            }
            ?>

          @foreach ($widgets as $ws)
                <div class="col-12 col-lg-6 col-md-6 col-sm-12">
                    {!! $ws->content !!}
                    <p class="btn-register"><a class="btn btn-primary btn-100" href="#" data-toggle="modal" data-target="#login-modal">Đăng ký ngay</a></p>
                </div>
            @endforeach

            <div class="col-12 col-lg-6 col-md-6 col-sm-12">
                <ul>
                    @foreach($networks as $key =>  $nk)
                    <li>
                        <img  style="{{$key == 0 ? 'position: absolute; left: 0; top: 10px; height: 75px;left: 15px;' : '' }}"
                             src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($nk->image) }}"><br>
                        <strong><br>
                            {{$nk->name}}<br>
                        </strong><br>
                        <i>{!! $nk->intro !!}</i></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>
{{--<div class="show_dangky modal fade" id="exampleModalLong" tabindex="-1" role="dialog"--}}
{{--     aria-labelledby="exampleModalLongTitle" aria-hidden="true">--}}
{{--    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">--}}
{{--        <div class="modal-content p-3">--}}
{{--           <h4>Bạn cần đăng nhập để tạo vận đơn</h4>--}}
{{--            <button type="button" class="btn btn-warning" style="    background-color: #FCD804;--}}
{{--    border-color: #FCD804;">Đăng nhập ngay</button>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
@section('script')
    <script type="text/javascript">
        // $(document).ready(function () {
        //     $(".btn-register").click(function () {
        //         $('.show_dangky').modal('show');
        //         // var course_id = $(this).data('course_id');
        //         // console.log(course_id);
        //         // $('form.dang-ky-course input[name=course_id]').val(course_id);
        //     });
        // });
    </script>
@endsection
