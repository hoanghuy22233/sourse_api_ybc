<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
{{--<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>--}}
<style>
    @import url(https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
    .hpanelwrap, .f2-topic p{
        height: 328px;
    }

    .f2-topic {
        width: 69%!important;
    }
    .f2-h {
        width: 30%!important;
    }
    div#hpanel a.manuface{
        width: 23%;
        height: auto;
        padding: 5px;
        margin: 1%;
        border-radius: 10px;
        border: 1px solid #eee;
    }
    @media (max-width: 991px){
        .f2-h, .f2-topic{
            width: 100%!important;
        }
    }
    .banner-abc .f2-topic a:before{
        content: unset!important;
    }
    .banner-abc .flexJus>li>a{
        overflow: hidden;
        width: 100%;
        margin: 0;
    }
    .f2-topic li{
        height: 100%;
    }
    .banner-abc ul.flexJus{
        height: 328px;
        border: 1px solid #ddd;
    }
    .banner-abc .f2-topic img{
        /*width: 100%!important;*/
        max-height: 326px!important;
    }
    .banner-abc .f2-topic a{
        background: none!important;
        padding: 0!important;
        border-radius: unset!important;
    }

    @media only screen and (max-width: 768px) {
        .f2-hsx a img {
            height: auto;
        }
        .hpanelwrap{
            height: unset;
        }
        div#hpanel {
            position: relative !important;
        }
    }
</style>
<div class="f f2-head banner-abc">
    <div class="b">
        <div class="flexCol f2-h fl">
            <label>CÁC THƯƠNG HIỆU NỔI TIẾNG</label>
            <div class="f flexCol f2-hsx ">
                <div class="hpanelwrap" style="overflow-y:auto;">
                    <div id="hpanel" style="width:100%;position:absolute;left:0;top:0; bottom: 0">
                        @php
                            $brans = \Modules\ThemeWorkart\Models\Manufacturer::where('status', 1)->whereIn('id', $bran_ids)->orderBy('order_no', 'asc')->get();
                        @endphp
                        @if(!empty($brans))
                            @foreach($brans as $bran)
                                <a class="manuface" data-value="{{$bran->id}}" title="{{$bran->name}}"
                                   href="{{route('cate.list',['slug' => $category->slug.'/'.$bran->slug])}}">
                                    <img class="lazy" data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumbNoCut($bran->image)}}"
                                         alt="{{$bran->image}}"/>
                                </a>
                            @endforeach
                        @endif
                    </div>
                </div>

            </div>
            <script>
                $(document).ready(function () {
                    $('#viewfullsapo').click(function () {
                        $('#sapo').css('height', 'auto');
                        $(this).hide();
                    });
                });
            </script>
        </div>
        <div class="flexCol f2-topic fl">
            <label>BANNER</label>
            <ul class="flexJus">
                    <li>
                        <a  href="{{ $category->link_banner_1}}">
                        <img class="lazy" data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumbNoCut($category->banner_1)}}"/></a>
                    </li>

                    <li>
                            <a  href="{{ $category->link_banner_2}}">
                                <img class="lazy" data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumbNoCut($category->banner_2)}}"/></a>
                    </li>

                    <li>
                            <a  href="{{$category->link_banner_3}}">
                                <img class="lazy" data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumbNoCut($category->banner_3)}}"/></a>
                    </li>
            </ul>
        </div>
    </div>
</div>