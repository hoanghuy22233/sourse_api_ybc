@if(@$settings['related_product'] == 1 && isset($relate_products) && count($relate_products) > 0)
    <div class="sp_lien_quan" style="    display: inline-block;width: 100%;">
        <span class="dtit">Sản phẩm liên quan</span>
        <div class="ss" id="SPTT2">
            <span class="psback"></span>
            <div class="pspanel">
                <div class="pswrap">
                    @foreach( $relate_products as $relate_product)
                        @php
                            $cate_slug =\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getFromCache('get_slug_cate_by_sptt'.@$relate_product->id);
                            if (!$cate_slug){
                                $cate_slug = \Modules\ThemeWorkart\Models\Category::whereIn('id', explode('|', @$relate_product->multi_cat))->first();
                               \Modules\ThemeWorkart\Http\Helpers\CommonHelper::putToCache('get_slug_cate_by_sptt'.@$relate_product->id, @$cate_slug);
                            }
                        @endphp
                        <a class="psitem"
                           href="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($relate_product) }}"
                           title="{{$relate_product->name}}">

                            <div class="pi">
                                <img alt="{{@$relate_product->name}}"
                                     class="lazy"
                                     data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb(@$relate_product->image, 160, null) }}"
                                     data-src="{{@$relate_product->image}}"/>
                            </div>
                            <p class="pn">{{@$relate_product->name}}</p>
                            <span class="pr">{{number_format(@$relate_product->final_price, 0, '.', '.')}}<sup
                                        style="margin-left: 5px;">đ</sup></span>
                        </a>
                    @endforeach
                </div>
            </div>
            <span class="psnext"></span>
        </div>
    </div>
@endif