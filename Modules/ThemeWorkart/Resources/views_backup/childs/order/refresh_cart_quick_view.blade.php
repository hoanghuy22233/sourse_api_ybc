<div id="popup-cart" class="modal" role="dialog">
    <div id="popup-cart-desktop" class="clearfix">
<div class="title-quantity-popup">
    <i class="fa fa-cart-arrow-down" aria-hidden="true"></i> Giỏ hàng của bạn (<span
            class="cart-popup-count">{{ Session::has('cart') ? count(Session::get('cart')['items']) : 0 }}</span> sản
    phẩm) <i class="fa fa-caret-right" aria-hidden="true"></i>
</div>
<div class="content-popup-cart">
    <div class="thead-popup">
        <div style="width: 50%;" class="text-left">Sản phẩm</div>
        <div style="width: 15%;" class="text-right">Đơn giá</div>
        <div style="width: 15%;" class="text-center">Số lượng</div>
        <div style="width: 20%;" class="text-right">Thành tiền</div>
    </div>
    <div class="tbody-popup">
        @foreach($products as $item)
            <div class="item-popup productid-{{$item->id}}">
                <div style="width: 50%;" class="text-left">
                    <div class="item-image"><a class="product-image" href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($item) }}"
                                               title="{{$item->name}}"><img class="lazy"
                                    alt="{{$item->name}}"
                                    data-src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, 80, 61) }}"
                                    width="80"></a></div>
                    <div class="item-info"><p class="item-name"><a href="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($item) }}"
                                                                   title="{{$item->name}}">{{$item->name}}</a></p>
                        <p class="variant-title-popup" style="display: none;">Default Title</p>
                        <p class="item-remove"><a href="javascript:;" class="remove-item-cart-popup" title="Xóa"
                                                  data-product_id="{{ $item->id }}"><i class="fa fa-close"></i> Bỏ sản phẩm</a></p>
                        <p class="addpass" style="color:#fff;">{{$item->id}}</p></div>
                </div>
                <div style="width: 15%;" class="text-right">
                    <div class="item-price"><span class="price">{{ number_format($item->final_price, 0, '.', '.') }}₫</span></div>
                </div>
                <div style="width: 15%;" class="text-center"><input class="variantID" type="hidden" name="variantId"
                                                                    value="{{$item->id}}">
                    <button onclick="var result = document.getElementById('quantity-{{$item->id}}'); var qtyItem{{$item->id}} = result.value; if( !isNaN( qtyItem{{$item->id}} )) result.value++;return false;"
                            class=" btn-plus btn btn-default bootstrap-touchspin-up btn-update-cart"
                            type="button">+
                    </button>
                    <input type="text" maxlength="12"
                           min="1"
                           class="form-control quantity-r2 quantity js-quantity-product input-text number-sidebar input_pop input_pop quantity-{{$item->id}}"
                           id="quantity-{{$item->id}}" name="Lines"
                           size="4" value="{{ Session::get('cart')['items'][$item->id]['quantity'] }}">
                    <button onclick="var result = document.getElementById('quantity-{{$item->id}}'); var qtyItem{{$item->id}} = result.value; if( !isNaN( qtyItem{{$item->id}} ) &amp;&amp; qtyItem{{$item->id}} > 1 ) result.value--;return false;"
                            class=" btn-minus btn btn-default bootstrap-touchspin-down btn-update-cart"
                            type="button">–
                    </button>
                    <a href="javascript:;" class="cart-update" data-product_id="{{$item->id}}">Cập nhật</a>
                </div>
                <div style="width: 20%;" class="text-right"><span class="cart-price"> <span
                                class="price">{{ number_format(Session::get('cart')['items'][$item->id]['quantity'] * $item->final_price, 0, '.', '.') }}₫</span> </span>
                </div>
            </div>
        @endforeach
    </div>
    <div class="tfoot-popup">
        <div class="tfoot-popup-1 clearfix">
            <div class="pull-right popup-total">
                <p>Thành tiền: <span class="total-price">{{ number_format(Session::get('cart')['total_price'], 0, '.', '.') }}</span></p>
            </div>
        </div>
        <div class="tfoot-popup-2 clearfix">
            <a class="button btn-proceed-checkout" title="Tiến hành thanh toán"
               href="{{ route('order.getDelivery') }}"><span>Tiến hành thanh toán <i
                            class="fa fa-long-arrow-right" aria-hidden="true"></i></span></a>
            <a class="button btn-continue close-window2" href="/" title="Tiếp tục mua hàng"><span><span><i class="fa fa-caret-left"
                                                                        aria-hidden="true"></i> Tiếp tục mua hàng</span></span></a>
        </div>
    </div>
</div>
<a title="Close" class="quickview-close close-window" href="javascript:;" ><i
            class="fa  fa-close"></i></a>
    </div>
</div>

{{--<div id="myModal" class="modal fade in" role="dialog" style="display: block; padding-left: 0px;">--}}
    {{--<div class="modal-dialog">--}}
        {{--<div class="modal-content">--}}
            {{--<div class="modal-header">--}}
                {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"--}}
                        {{--style="position: relative; z-index: 9;"><span aria-hidden="true">×</span></button>--}}
                {{--<h4 class="modal-title"><span><i class="fa fa-check" aria-hidden="true"></i></span>Thêm vào giỏ hàng--}}
                    {{--thành công</h4></div>--}}
            {{--<div class="modal-body">--}}
                {{--@foreach($products as $item)--}}
                {{--<div class="media">--}}
                    {{--<div class="media-left">--}}
                        {{--<div class="thumb-1x1"><img width="70px"--}}
                                                    {{--data-src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, 70, 53) }}"--}}
                                                    {{--alt="{{$item->name}}" class="lazy"></div>--}}
                    {{--</div>--}}
                    {{--<div class="media-body">--}}
                        {{--<div class="product-title">{{$item->name}}</div>--}}
                        {{--<div class="product-new-price"><span>{{ number_format($item->final_price, 0, '.', '.') }} đ</span></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--@endforeach--}}
                {{--<button class="btn btn-block btn-outline-red" data-dismiss="modal"><a href="/">Tiếp tục mua hàng</a></button>--}}
                {{--<a href="{{ route('order.getDelivery') }}" class="btn btn-block btn-red">Tiến hành thanh toán »</a></div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<style>
    .js-quantity-product {
        display: inline-block;
        height: 28px;
        padding: 0 5px;
        text-align: center;
        border-radius: 0;
        width: 35px;
        float: right;
        min-height: 28px;
        border: 1px solid #e5e5e5;
    }
    .btn-update-cart {
        border-radius: 3px 0 0 3px;
        border: 1px solid #e5e5e5;
        border-color: #e5e5e5;
        color: #999;
        line-height: 20px;
        padding: 3px 9px;
        margin-top: 10px;
        width: 28px;
        background-color: #fff;
        float: right;
        height: 24px;
    }

    a.cart-update {
        width: 76px;
        display: inline-block;
        float: right;
        text-align: center;
    }
</style>