@extends('themeworkart::layouts.master')
@section('main_content')

<section>
    <div class="flexJus headpage">
        <div class="flex">
            <ul class="breadcrumb">
                <li itemscope="itemscope" itemtype=""><a itemprop="url" href="/" title="{{@$settings['name']}}"><span itemprop="title">{{@$settings['name']}}</span></a></li>
                <li itemscope="itemscope" itemtype=""><span itemprop="url" title="{{@$settings['name']}} Tại {{ucwords(mb_strtolower($name->location))}}"><span itemprop="title">{{@$settings['name']}} Tại {{ucwords(mb_strtolower($name->location))}}</span></span></li>
            </ul>
        </div>

    </div>

    <style>
        .blok-showroom-m{float:left;margin:1%;width:48%}
        .title-showroom-m{background:#f6f6f6 url(../images/icon-hsx.png) no-repeat 0 -34px;border:1px solid #ececec;display:block;padding:10px 5%;color:#00659f;font:bold 13px Arial}
        .name-showroom{color:#666;font:normal 12px/22px Arial;padding:10px 5% 5px}
        .name-showroom b{color:#333}
        .maps-m{margin:5px 5%;width:90%;text-align:center;overflow:hidden;}
        .maps-m img{max-width:100%}
        .address-m{padding:5px 5%;font:bold 12px/22px Arial;color:#333}
        .address-m span{color:#00659f;font-weight:400;font-style:italic}
        .address-m p{color:#c00;font:bold italic 12px/19px arial;}
        .phone-m{padding:5px 5%;font:bold 12px/24px Arial;color:#666}
        .phone-m b{width:45px;color:#333;float:left;font-weight:normal;}
    </style>
    <section id="page-showroom-m">
        @foreach($showrooms as $showroom)
            <div class="blok-showroom-m">
            <div class="title-showroom-m">{{$showroom->name}}</div>
            <div class="name-showroom">Mở cửa: 8:00 - 21:00 (Kể cả ngày lễ)</div>
            <div class="maps-m">
                <img class="lazy" alt="Click để xem đường đến {{$showroom->name}} trên google maps" title="Click để xem đường đến  {{$showroom->name}} trên google maps" onclick="openmap({{$showroom->id}});" data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($showroom->image_map, 557, null) }}">
            </div>
            <div class="address-m">
                {!!$showroom->id!!}
                {!!$showroom->address!!}
            </div>
            <div class="phone-m">
                {!!$showroom->contact!!}
            </div>
            <a href="/{{$showroom->slug}}" title="{{$showroom->name}}" class="btn btnvang">Giới thiệu Showroom <i class="fa fa-caret-right" aria-hidden="true"></i></a>
            <a href="javascript:openmap({{$showroom->id}});" class="btn btnxanh">Bản đồ <i class="fa fa-caret-right" aria-hidden="true"></i></a>
        </div>
        @endforeach
    </section>

</section>
@endsection