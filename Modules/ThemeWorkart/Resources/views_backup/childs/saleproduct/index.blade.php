@extends('themeworkart::layouts.master')
@section('main_content')
    <section class="f">

        <div class="flexJus headpage">
            @section('name', 'Khuyến mãi')
            @include('themeworkart::partials.breadcrumb')
        </div>
        <style type="text/css">
            .lkm{float:left;width:100%;padding:0;}
            .lkm li{float:left;width:32%;margin:0 2% 2% 0;background:#eee;min-height:410px;}
            .lkm li:nth-child(3n){margin-right:0;}
            .lkm div{padding:10px;}
            .lkm img{width:100%;}
            .lkm h3{margin:0;}
            .lkm h3 a{font:18px/20px tahoma;}
            .lkm span{display:block;padding-bottom:3px;margin-bottom:3px;/*border-bottom:1px solid #ddd;*/}
            .lkm span label{font:12px/20px arial;}
            .lkm span label b{color:red;}
            .lkm p{font:12px/20px arial;text-align:justify;margin:5px 0 0 0;color:#666;}
        </style>
        <ul class="lkm">
            <?php
//                dd($category->toArray());
            ?>
            @if(!empty($category))
                @foreach($category as $cate)
                    <li>
                        <a title="{{ucfirst(mb_strtolower($cate->name))}}" href="{{route('cate.list', ['slug' => $cate->slug])}}">
                            <img class="lazy" alt="{{ucfirst(mb_strtolower($cate->name))}}" data-src="{{\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($cate->image, 500, null) }}" style="display: inline;"></a>
                        <div>
                            <h3><a title="{{ucfirst(mb_strtolower($cate->name))}}" href="{{route('cate.list', ['slug' => $cate->slug])}}">{{ucfirst(mb_strtolower($cate->name))}}</a></h3>
                        </div>
                    </li>
                @endforeach
            @endif
        </ul>
    </section>
@endsection