{{--<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/home.css')}}">--}}
{{--<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/footer.css')}}">--}}
{{--<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/slide.css')}}">--}}
{{--<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/detailproduct.css')}}">--}}
{{--<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/frontend/css/style-responsive.css')}}">--}}
{{--<script type="text/javascript" src="{{URL::asset('public/frontend/themes/stbd/front/js/jquery.js')}}"></script>--}}

<link rel="canonical" href="index.html"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{URL::asset('public/themelogistic/css/bootstrap8f26.css?v=55')}}">
<link href="{{URL::asset('public/themelogistic/css/main_home8f26.css?v=55')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{URL::asset('public/themelogistic/vendor/select2.min8f26.css')}}" type="text/css">
<link rel="stylesheet" href="{{URL::asset('public/themelogistic/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/themelogistic/css/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
<link href="{{URL::asset('public/themelogistic/css/style.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('public/themelogistic/css/select.css')}}" rel="stylesheet" type="text/css">

<style type="text/css">
    .lag-mb a:first-child:after {
        border: 0px !important
    }
</style>
<style>
    .container-service .row.bx-sh {
        box-shadow: 0px 5px 6px rgba(0, 0, 0, 0.4);
        margin-bottom: 40px;
    }

    .container-service .service-box {
        display: block;
        background-color: #fff;
        transition: all 0.3s ease;
        text-align: center;
        position: relative;
        color: #2e3553;
        padding: 16px;
        margin-top: -60px;
    }

    .container-service .service-box::before {
        position: absolute;
        height: 70%;
        left: 0;
        top: 15%;
        border-left: 1px solid #c7c7c7;
        content: "";
    }

    .container-service .service-box:hover::before {
        border-left-color: #fcd804;
    }

    .container-service .service-box.active {
        background-color: #fcd804;
    }

    .container-service .service-box.active::before,
    .container-service .service-box:hover::before {
        position: absolute;
        height: 100%;
        left: auto;
        right: -1px;
        top: 0;
        border-right: 1px solid #fcd804;
        content: "";
        z-index: 1;
    }

    .container-service .service-box:hover,
    .container-service .service-box.avtive:hover {
        background: #fcd804;
    }

    .container-service .service-box .box-img {
        text-align: center;
        height: 50px;
    }

    .container-service .service-box .box-img img {
        width: 55px;
    }

    .container-service .service-box .box-img p {
        font-size: 44px;
    }

    .container-service .service-box .box-content {
        font-size: 18px;
        color: #2e3553;
        margin-top: 5px;
    }

    .container-service .row [class^="col-"] {
        padding: 0;
    }

    /* ======================================== Start Service Modal Popup ========================================*/
    .modal {
        padding: 0 !important;
    }

    .modal .modal-dialog {
        background-color: #fff;
        border-radius: 5px;
        position: relative;
    }

    .modal .modal-dialog .modal-content .modal-body {
        padding: 15px 20px;
    }

    .modal .modal-dialog .modal-content .modal-body .shipping-method {
        background-color: #fff;
        border: 1px solid #ccc;
        padding: 10px 0;
        margin-bottom: 15px;
        border-radius: 5px;
    }

    .modal .modal-dialog .modal-content .modal-body .shipping-method .box-title {
        padding-left: 15px;
    }

    .modal .modal-dialog .modal-content .modal-body .shipping-method .method-content {
        text-align: center;
        padding: 0 5px;
    }

    .modal .modal-dialog .modal-content .modal-body .shipping-method .service-text {
        font-size: 14px;
        margin-bottom: 0;
    }

    .box-title {
        font-size: 18px;
        font-weight: 600;
        color: #000000fa;
        margin-bottom: 15px;
    }

    .box-sd {
        box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.4);
    }
    .modal .modal-dialog .modal-content .modal-body .shipping-method .item-service::before,
    .modal .modal-dialog .modal-content .modal-body .shipping-method .item-service.active::before,
    .modal .modal-dialog .modal-content .modal-body .shipping-method .item-service:hover::before {
        display: none;
    }

    .modal .modal-dialog .modal-content .modal-body .shipping-method .item-service:hover {
        color: #fcd804;
        background: #fff;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address {
        display: flex;
        flex-wrap: wrap;
        border: 1px solid #ccc;
        border-radius: 5px;
        margin-bottom: 15px;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address .shipping-address {
        position: relative;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address .shipping-address:before {
        position: absolute;
        content: "";
        background: #c7c7c7;
        height: 100%;
        width: 1px;
        top: 0;
        right: -10px;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address .address-form {
        flex-basis: 48%;
        padding: 10px 15px;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address .address-form.mg-r {
        margin-right: 15px;
    }

    .modal .modal-dialog .modal-content .modal-body .box-address .address-form .form__control .control-top,
    .modal .modal-dialog .modal-content .modal-body .control-body .input__control {
        display: flex;
    }

    .modal .modal-dialog .modal-content .modal-body .form__control input,
    textarea {
        border: none;
        outline: none;
        resize: none;
    }

    .input__control {
        border-radius: 5px;
        width: 100% !important;
        border: 1px solid #c7c7c7 !important;
        margin-bottom: 8px;
        padding: 5px;
        font-size: 14px;
    }

    .modal .modal-dialog .modal-content .modal-body .control-top .input__control,
    .modal .modal-dialog .modal-content .modal-body .control-body .input__control {
        width: 50%;
    }

    .modal .modal-dialog .modal-content .modal-body .control-top .input__control:not(:last-child),
    .modal .modal-dialog .modal-content .modal-body .control-body .input__control:not(:last-child) {
        margin-right: 10px;
    }

    .modal .modal-dialog .modal-content .box-voucher,
    .modal .modal-dialog .modal-content .box-comment,
    .modal .modal-dialog .modal-content .box-paypal,
    .modal .modal-dialog .modal-content .box-total,
    .modal .modal-dialog .modal-content .box-info {
        padding: 10px 20px;
        border-radius: 5px;
        border: 1px solid #ccc;
        margin-bottom: 15px;
    }

    .modal .modal-dialog .modal-content .box-voucher .box-input {
        display: flex;
    }

    .modal .modal-dialog .modal-content .box-voucher .box-input .v-btn {
        display: inline-block;
        position: inherit;
        border-radius: 5px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        outline: none;
        border: 1px solid #ccc;
        font-size: 15px;
        cursor: pointer;
        transition: color 0.2s ease;
        padding: 5px 15px;
    }

    .modal .modal-dialog .modal-content .box-voucher .box-input .v-btn:hover a {
        color: #fff !important;
    }

    .modal .modal-dialog .modal-content .box-voucher .box-input .input__control {
        width: 50% !important;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        margin-bottom: 0;
    }

    .modal .modal-dialog .modal-content .box-paypal form {
        display: flex;
        flex-direction: column;
    }

    .modal .modal-dialog .modal-content .box-paypal label {
        color: #000;
        font-weight: normal;
    }

    .modal .modal-dialog .modal-content .box-paypal .paypal-img img {
        width: 200px;
        display: block;
    }

    .modal .modal-dialog .modal-content .box-total {
        width: 50%;
        margin-left: auto;
    }

    .modal .modal-dialog .modal-content .box-total .b-p {
        display: flex;
        justify-content: space-between;
        margin-bottom: -15px;
        font-size: 14px;
        opacity: 0.8;
    }

    .modal .modal-dialog .modal-content .box-total .done-price {
        margin-top: 10px;
        font-size: 17px;
        opacity: 1;
    }

    .modal .modal-dialog .modal-content .box-total .done-price span {
        color: #fcd804;
    }

    .modal .modal-dialog .modal-content .box-footer {
        display: block;
        position: inherit;
        margin-left: auto;
        overflow: hidden;
        border-radius: 5px;
        border: 1px solid #c7c7c7;
        padding: 5px 15px;
        font-size: 16px;
        cursor: pointer;
        transition: color 0.2s ease;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span {
        display: flex;
        align-items: center;
        margin-bottom: 10px;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span label {
        color: #000;
        font-size: 14px;
        margin-right: 10px;
        margin-bottom: 0;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span .input__control {
        width: 80% !important;
        padding: 5px;
        margin-bottom: 0;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span .file {
        position: relative;
        cursor: pointer;
        overflow: hidden;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span .file input {
        position: absolute;
        top: 0;
        left: 0;
        opacity: 0;
        cursor: pointer;
        height: 100%;
        width: 100%;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span .file .btn-m {
        color: #4d4d4d;
        border: 1px solid #ccc;
        font-size: 14px;
        border-radius: 5px;
        transition: all 0.2s;
        cursor: pointer;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span .file .file-btn {
        padding: 0 20px;
        background-color: #fff;
        cursor: pointer;
        outline: none;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .modal .modal-dialog .modal-content .box-info .box-input span .select{
        margin-left: 3px;
        padding: 7px 5px;
        outline: none;
        border: none;
    }

    .modal .modal-dialog .modal-content .box-footer:hover a {
        color: #fff;
    }

    @media screen and (min-width: 480px) {
        .item-service {
            font-size: 13px;
        }
    }

    @media screen and (min-width: 769px) {
        .method-content {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 16.666667%;
        }
    }

    @media (max-width: 768px) {
        .method-content {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 33.333333%;
            flex: 0 0 33.333333% !important;
            max-width: 33.333333% !important;
        }
        .box-title {
            font-size: 17px;
        }
        .modal .modal-dialog .modal-content .box-voucher,
        .modal .modal-dialog .modal-content .box-comment,
        .modal .modal-dialog .modal-content .box-paypal,
        .modal .modal-dialog .modal-content .box-total,
        .modal .modal-dialog .modal-content .box-info {
            padding: 10px;
        }
        .modal .modal-dialog .modal-content .box-voucher .box-input .v-btn {
            font-size: 14px;
        }
    }

    @media (max-width: 525px) {
        .method-content {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 100%;
            flex: 0 0 100% !important;
            max-width: 100% !important;
        }
        .item-service:last-child {
            border-bottom: none;
        }
        .icon-hoa-toc {
            width: 90px !important;
            margin-top: -70px !important;
        }
        .item-service p {
            margin-top: 15px;
            min-height: 0;
        }
        .item-service:last-child p {
            margin-bottom: -5px !important;
        }
        .modal .modal-dialog .modal-content .box-total {
            width: 100%;
        }
        .modal .modal-dialog .modal-content .modal-body .box-address .address-form {
            flex-basis: 100%;
        }
        .modal .modal-dialog .modal-content .modal-body .box-address .address-form:before {
            display: none;
        }
        .modal .modal-dialog .modal-content .modal-body .box-address .address-form.mg-r {
            margin-right: 0;
        }
    }

    .p-3 {
        padding: 2px !important;
    }

    @media (min-width: 992px) {
        .modal-lg,
        .modal-xl {
            max-width: 850px;
        }
    }

    input {
        border: none;
        outline: none;
    }

    /* ======================================== End Service Modal Popup ========================================*/

    /* ======================================== Start Login Modal Popup ========================================*/
    .login-request .modal {
        top: 15%;
    }

    .login-modal .modal-content .modal-header {
        background: #fff;
        padding: 10px 20px;
    }

    .login-modal .modal-content .modal-header .modal-title {
        color: #000;
        font-size: 18px;
        text-transform: uppercase;
        text-align: left;
    }

    .login-modal .modal-content .modal-body {
        display: flex;
        align-items: center;
    }

    .login-modal .modal-content .modal-body p {
        margin: 0;
    }

    .login-modal .modal-content .modal-footer .btn {
        background-color: #2e3553;
        color: #fff;
    }

    .login-modal .modal-content .modal-footer .btn-active {
        background: #fcd804;
        color: #000;
    }
    .login-modal .modal-content .modal-footer .btn-active a:hover {

        color: #000;
    }
    .modal .modal-dialog .modal-content .modal-body .shipping-method .method-content.active{background: #FCD804;}

    /* ======================================== Start Order Status Modal Popup ========================================*/
    .order-status .status-title{
        display: flex;
        align-items: center;
        margin-bottom: 15px;
    }

    .order-status .status-top{
        border-radius: 5px;
        box-shadow: 0 1px 2px 1px rgba(0, 0, 0, 0.4);
        padding: 15px 10px;
        margin-bottom: 15px;
    }

    .order-status h4{
        width: 170px;
        font-weight: 600;
    }

    .order-status h4, .order-status h3 , .order-status p{
        margin-bottom: 0;
    }

    .order-status .status-title .status-detail{
        padding: 3px 35px;
        background-color: #8064A2;
        color: #fff;
        font-size: 14px;
    }

    .order-status .name-order{
        display: flex;
        align-items: center;
    }

    .order-status .status-content{
        display: flex;
        flex-wrap: wrap;
    }

    .order-status .status-content .status-box:not(:last-child){
        margin-right: 30px;
    }

    .order-status .status-content .status-box{
        flex-basis: 48%;
        margin-bottom: 30px;
        border-radius: 5px;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.4);
        padding: 15px 10px;
    }

    .order-status .status-content .status-box h3{
        padding: 10px 0 20px 0;
        font-weight: 600;
        font-size: 17px;
    }

    .order-status .status-content .status-box .detail-m{
        display: flex;
        align-items: center;
        margin-bottom: 10px;
    }

    .order-status .status-content .status-box .detail-m .description{
        width: 150px;
        max-width: 100%;
        text-align: left;
    }

    .order-status .status-content .status-box .detail-m .email-t{
        text-decoration: revert;
        color: #E6A2B2;
    }

    .order-status .btn-detail{
        position: inherit;
        margin-top: 20px;
        border: 1px solid #fff;
        border-radius: 5px;
        outline: none;
        display: flex;
        justify-content: flex-end;
        width: 100%;
        background: none;
        margin-right: 50px;
    }

    .order-status .btn-detail .btn-click{
        color: #000;
        display: inline-block;
        width: 250px;
        background-color:#FFC000;
        padding: 8px 0;
        overflow: hidden;
        transition: all .3s ease;
        cursor: pointer;
    }

    .order-status .btn-detail .btn-click:hover{
        background-color: #fcd804;
    }

    @media (max-width: 768px){
        .order-status .status-content .status-box{
            margin-right: 0 !important;
            flex-basis: 100%;
        }
    }

    @media (max-width: 525px){
        .order-status .status-content .status-box .detail-m .description{
            font-size: 14px;
        }
        .order-status .status-content .status-box .detail-m {
            font-size: 13px;
        }
        .order-status .status-title .status-detail{
            padding: 3px 15px;
        }

        .order-status .status-content .status-box .detail-m .description{
            width: 120px;
        }
    }

    /* ======================================== End Order Status Modal Popup ========================================*/
</style>
<style>



    @media screen and (min-width: 240px) {
        .wp-slide {
            background: unset;
            padding-top: 0;
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
        }
    }


    .font-size-30 {
        font-size: 30px;
    }

    .box-cube {
        height: 120px;
        background-color: #fafafa;
        padding: 25px 15px;
        transition: .25s ease background-color, .25s ease height;
        text-align: center;
        margin: auto;
    }

    .box-cube:hover {
        background-color: #fdd800;
    }

    .box-cube > a {
        display: block;
        overflow: hidden;
    }

    .box-cube > a:hover {
        color: unset !important;
    }

    .swiper-slide-active .box-cube {
        background-color: #FCD804;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }


    .top-slide {
        width: 162px;
        height: 0 !important;
    }

    @media (max-width: 575px) and (min-width: 425px) {
        .item-service {
            max-width: 50%;
        }
    }

    @media screen and (min-width: 768px) {
        .item-service {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 16.666667%;
        }
    }

    @media (max-width: 768px) {
        .box-cube {
            width: 110px;
            padding: 20px 0px;
            height: 110px;
        }

        .top-slide {
            width: 100%;
        }

        .item-slide {
            width: 98%;
        }
    }

    .more-than-log {
        text-align: center;
        font-size: 40px;
        color: #fff;
        font-weight: bold;
    }

</style>
<style type="text/css">
    .select2-selection {
        height: 38px !important;
    }

    .select2 {
        width: 100%;
    }
</style>
<style>
    form a:hover {
        opacity: 1 !important;
    }
    .avatar {
        vertical-align: middle;
        width: 35px;
        height: 35px;
        border-radius: 50%;
    }
</style> <style>
    @media screen and (min-width: 240px) {
        .wp-slide {
            background: unset;
            padding-top: 0;
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
        }
    }


    .font-size-30 {
        font-size: 30px;
    }

    .box-cube {
        width: 158px;
        height: 120px;
        background-color: #fafafa;
        padding: 25px 15px;
        transition: .25s ease background-color, .25s ease height;
        text-align: center;
        margin: auto;
    }

    .box-cube:hover {
        background-color: #fdd800;
    }

    .box-cube > a {
        display: block;
        overflow: hidden;
    }

    .box-cube > a:hover {
        color: unset !important;
    }

    .active-box-cube {
        background-color: #FCD804;

        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    }

    .top-slide {
        width: 162px;
        height: 0 !important;
    }

    @media (max-width: 575px) and (min-width: 425px) {
        .item-service {
            max-width: 50%;
        }
    }

    @media screen and (min-width: 768px) {
        .item-service {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
            max-width: 16.666667%;
        }
    }

    @media (max-width: 768px) {
        .box-cube {
            width: 110px;
            padding: 20px 0px;
            height: 110px;
        }

        .top-slide {
            width: 100%;
        }

        .item-slide {
            width: 98%;
        }
    }

    .more-than-log {
        text-align: center;
        font-size: 40px;
        color: #fff;
        font-weight: bold;
    }

</style>
<style>
    /*.item-service {*/
    /*    max-width: 16.666667% ;*/
    /*}*/

    .icon-hoa-toc {
        width: 55px;
        margin-top: -30px;
    }


    @media screen and (max-width: 425px) {
        .item-service {
            max-width: 100% !important;
        }

        .icon-hoa-toc {
            width: 90px;
            margin-top: -70px;
        }
    }
</style> <style>
    @media screen and (min-width: 768px) {
        .left-about .summary {
            font-size: 15px;
        }

        .unset-color > a {
            color: unset !important;
        }

        .summary_bottom {
            height: 250px;
        }
    }
</style><style>
    .modal-body button {
        position: absolute;
        padding: 5px;
        right: 0;
        background-color: #fdd800;
        color: #ffffff;
    }
</style>

<style type="text/css">
    .lag-mb a:first-child:after {
        border: 0px !important
    }
</style>
<style type="text/css">
    .select2-selection {
        height: 38px !important;
    }

    .select2 {
        width: 100%;
    }
</style>

{!! @$settings['frontend_head_code'] !!}
