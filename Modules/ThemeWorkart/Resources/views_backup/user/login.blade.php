<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet"  type="text/css" href="{{URL::asset('public/themelogistic/css/account.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans|Roboto&amp;display=swap" rel="stylesheet">
    <title></title>
</head>
<body>
<div class="container-fluid hidden-scroll">
    <div class="row">
        <div class="left-content content">
            <div class="background-left" style="background-image: url(https://logistics.webhobasoft.com/static/media/4753605.jpg);">
                <div class="content-note"><a aria-current="page" class="active" href="/v2/ssoLogin?app=import&amp;returnUrl=http://khachhang.ghn.vn/sso-login?token=">
                        <div class="logo"><img alt="test" src="/images/logog-02.png">
                        </div>
                    </a>
                    <div class="left-row2">THIẾT KẾ CHO GIẢI PHÁP GIAO NHẬN HÀNG<br>TỐT NHẤT TỪ TRƯỚC ĐẾN NAY</div>
                    <i class="left-row3">Nhanh hơn, rẻ hơn và thông minh hơn</i></div>
                <div class="backdrop"></div>
            </div>
        </div>
        <div class="right-content">
            <div class="form-content">
                <div class="login-content">
                    <div class="title-login"><span class="login-row-1">Đăng nhập</span>
                        <p class="login-row-2">Buổi chiều của bạn thế nào rồi? Gửi hàng dần thôi.</p></div>
                    @if (Session('success'))
                        <div class="alert bg-success" role="alert">
                            <p style="color: red"><b>{!!session('success')!!}</b></p>
                        </div>
                    @endif
                    @if(Session::has('message') && !Auth::check())
                        <div class="alert text-center text-white " role="alert"
                             style=" margin: 0; font-size: 16px;">
                            <a href="#" style="float:right;" class="alert-close"
                               data-dismiss="alert">&times;</a>
                            <p style="color: red"><b>{{ Session::get('message') }}</b></p>
                        </div>
                    @endif
                    <form action="/dang-nhap" method="post">
                        {{ csrf_field() }}
                    <div class="login-form">
                        <div class="row">
                            <div class="col-3"></div>
                            <div class="col-6">
                                <div class="form-group"><label class="">Tài khoản</label><input tabindex="1" name="email" autocomplete="email" placeholder="Nhập email" type="text" class="form-control {{ $errors->first('email', 'is-invalid') }}">
                                    @if($errors->has('email'))
                                        <p style="color: red"><b>{{ $errors->first('email') }}</b></p>
                                    @endif
                                </div>
                                <div class="form-group"><label class="">Mật khẩu</label><a href="/forgot" style="float: right; color: rgb(255,220,25); text-decoration: none;">Quên
                                        mật khẩu?</a><input tabindex="2" name="password" autocomplete="current password" placeholder="Nhập mật khẩu" type="password" class="form-control {{ $errors->first('password', 'is-invalid') }}">
                                    @if($errors->has('password'))
                                        <p style="color: red"><b>{{ $errors->first('password') }}</b></p>
                                    @endif
                                </div>
                                <p style="color: rgb(228, 19, 44); font-size: 12px; margin-top: 2px; margin-bottom: 8px; height: 12px; font-weight: 500;"></p>
                                <button style="color: black !important;" type="submit" tabindex="4" class="btn-primay-page btn-login btn btn-secondary">Đăng nhập
                                </button>
                                <div class="login-row-3"><label class="">Bạn chưa có tài khoản</label><a tabindex="4" href="{{route('user.register')}}" style="color: rgb(255,220,25); font-weight: 700; text-decoration: none;">
                                        Đăng ký ngay</a></div>
                                <div class="note-ghn">Nhân sự Etal Group bấm <a title="Đăng nhập hệ thống nhân viên" href="#" style="color: rgb(255,220,25); font-weight: 700; text-decoration: none;"><b>vào
                                            đây</b></a> để đăng nhập
                                </div>
                            </div>
                            <div class="col-3"></div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div></div>
</div>
</body>
</html>


