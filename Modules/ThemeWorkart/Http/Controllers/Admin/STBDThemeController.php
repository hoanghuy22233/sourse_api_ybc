<?php

namespace Modules\ThemeWorkart\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Illuminate\Http\Request;
use Modules\ThemeWorkart\Models\Category;
use Illuminate\Support\Facades\Schema;

class STBDThemeController extends CURDBaseController
{
    protected $module = [
        'code' => 'setting',
        'label' => 'Setting theme',
        'modal' => '\App\Models\Setting',
        'tabs' => [
            /*
            'social_tab' => [
                'label' => 'Mạng xã hội',
                'icon' => '<i class="kt-menu__link-icon flaticon-settings-1"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'fanpage', 'type' => 'text', 'label' => 'Fanpage FB', 'des' => 'Fanpage Facebook'],
                    ['name' => 'google_map', 'type' => 'text', 'label' => 'google_map', 'des' => 'Fanpage Facebook'],
                    ['name' => 'skype', 'type' => 'text', 'label' => 'skype', 'des' => 'Fanpage Facebook'],
                    ['name' => 'instagram', 'type' => 'text', 'label' => 'instagram', 'des' => 'Fanpage Facebook'],
                ]
            ],*/
            'common_tab' => [
                'label' => 'General',
                'icon' => '<i class="kt-menu__link-icon flaticon2-layers-2"></i>',
                'intro' => '',
                'td' => [
//                    ['type' => 'text', 'class' => '', 'label' => 'SĐT bảo hành', 'name' => 'phone_bh'],
//                    ['type' => 'checkbox', 'class' => '', 'label' => 'Bật chế độ bảo trì', 'name' => 'maintan'],
//                    ['type' => 'select', 'options' =>
//                        [
//                            2 => 'Mặc định',
//                            1 => 'Dropdown',
//                            0 => 'Slide down',
//                        ], 'class' => '', 'label' => 'Hiệu ứng menu', 'name' => "option_menu"
//                    ],
//                    ['type' => 'text', 'class' => '', 'label' => 'Slug trang thương hiệu', 'name' => 'thuong_hieu_slug'],
//                    ['type' => 'textarea_editor', 'class' => '', 'label' => 'Top footer trang danh mục con', 'name' => 'policy_child', 'group_class' => 'col-xs-12 col-md-12 col-xl-12'],
                    ['type' => 'textarea_editor', 'class' => '', 'label' => 'Copyright footer', 'name' => 'footer'],
//                    ['type' => 'file_editor', 'class' => '', 'label' => 'Ảnh nền website', 'name' => 'background_image'],
//                    ['type' => 'file_editor', 'class' => '', 'label' => 'Logo thương hiệu', 'name' => "logo_brand"],
//                    ['type' => 'file_editor', 'class' => '', 'label' => 'Logo footer', 'name' => "tagline_footer"],
//                    ['type' => 'file_editor', 'class' => '', 'label' => 'Logo dịch vụ trang chi tiết sản phẩm', 'name' => "logo_service_detail"],
//                    ['type' => 'select', 'options' =>
//                        [
//                            'noindex, nofollow' => 'noindex, nofollow',
//                            'index, follow' => 'index, follow',
//                            'index, nofollow' => 'index, nofollow',
//                            'noindex, follow' => 'noindex, follow',
//                        ], 'class' => '', 'label' => 'Robots', 'name' => "robots"],
//                    ['type' => 'text', 'class' => '', 'label' => 'Header background', 'name' => 'header_background'],
//                    ['type' => 'text', 'class' => '', 'label' => 'Footer background', 'name' => 'footer_background'],
//                    ['type' => 'select', 'options' =>
//                        [
//                            1 => 'Trái',
//                            0 => 'Giữa',
//                        ], 'class' => '', 'label' => 'Thay đổi vị trí logo', 'name' => "logo_position"
//                    ],
//                    ['type' => 'checkbox', 'class' => '', 'label' => 'Hiện Nút "Xem Hàng"', 'name' => "show_detail"
//                    ],
//                    ['type' => 'text', 'class' => '', 'label' => 'Màu nền website', 'name' => 'background_color'],
//                    ['type' => 'select', 'options' =>
//                        [
//                            0 => 'Không nền',
//                            1 => 'Màu nền',
//                            2 => 'Ảnh nền',
//                        ], 'class' => '', 'label' => 'Chọn kiểu nền website', 'name' => "option_background"
//                    ],
//                    ['type' => 'select_font', 'class' => '', 'label' => 'Cài đặt font chữ', 'name' => "set_font"],
//                    ['type' => 'textarea', 'class' => '', 'label' => 'Danh sách font', 'name' => 'list_font'],
//                    ['type' => 'select', 'options' =>
//                        [
//                            0 => 'Mặc định',
//                            1 => 'Cột',
//                        ], 'class' => '', 'label' => 'Kiểu hiển thị footer', 'name' => "footer_content"
//                    ],
//                    ['type' => 'checkbox', 'class' => '', 'label' => 'Hiện footer trái', 'name' => "footer_left"
//                    ],
//                    ['type' => 'select', 'options' =>
//                        [
//                            0 => 'Mặc định',
//                            1 => 'Dạng 1',
//                        ], 'class' => '', 'label' => 'Kiểu nút chi tiết sản phẩm', 'name' => "custom_button_detail"
//                    ],
//                    ['type' => 'checkbox', 'class' => '', 'label' => 'Hiện Nút khảo sát', 'name' => "show_khao_sat"
//                    ],
//                    ['type' => 'text', 'class' => '', 'label' => 'Link khảo sát', 'name' => 'link_khao_sat'],
//                    ['type' => 'text', 'class' => '', 'label' => 'SĐT', 'name' => 'head_code'],
//                    ['type' => 'file_editor', 'class' => '', 'label' => 'Banner head top', 'name' => "banner_head_top"],
//                    ['type' => 'file_editor', 'class' => '', 'label' => 'Banner head right', 'name' => "banner_head_right"],
//                    ['type' => 'file_editor', 'class' => '', 'label' => 'Banner head left', 'name' => "banner_head_left"],
//                    ['name' => "menu_text_color", 'type' => 'text', 'class' => '', 'label' => 'Màu chữ menu chính', ],
//                    ['name' => "footer_text_color", 'type' => 'text', 'class' => '', 'label' => 'Màu chữ chân trang', ],
                    ['name' =>'frontend_head_code','type' =>'textarea','label' =>'admin.insert_code','inner' =>'rows=20'],
                    ['name' =>'frontend_footer_code','type' =>'textarea','label' =>'admin.insert_code_footer','inner' =>'rows=20'],
                ],
            ],
//            'seo_tab' => [
//                'label' => 'admin.configuration_seo',
//                'icon' => '<i class="flaticon-globe"></i>',
//                'intro' => 'admin.configuration_seo',
//                'td' => [
//                    ['name' => 'robots', 'type' => 'select', 'options' =>
//                        [
//                            'noindex, nofollow' => 'noindex, nofollow',
//                            'index, follow' => 'index, follow',
//                            'index, nofollow' => 'index, nofollow',
//                            'noindex, follow' => 'noindex, follow',
//                        ], 'label' => 'admin.status', 'value' => 'noindex, nofollow'],
//                    ['name' => 'default_meta_title', 'type' => 'text', 'label' => 'admin.meta_title'],
//                    ['name' => 'default_meta_description', 'type' => 'text', 'label' => 'admin.meta_description'],
//                    ['name' => 'default_meta_keywords', 'type' => 'text', 'label' => 'admin.meta_keywords'],
//                ]
//            ],
//            'homepage_tab' => [
//                'label' => 'Trang chủ',
//                'icon' => '<i class="kt-menu__link-icon flaticon2-open-box"></i>',
//                'intro' => '',
//                'td' => [
////                    ['type' => 'text', 'class' => '', 'label' => 'SĐT', 'name' => 'phone', 'value' => ['phone']],
//                    ['type' => 'text', 'class' => '', 'label' => 'SĐT khiếu nại', 'name' => 'phone_kn'],
////                    ['type' => 'text', 'class' => '', 'label' => 'SĐT trang chi tiết sản phẩm', 'name' => 'hotline', 'value' => ['hotline']],
//                    ['type' => 'text', 'class' => '', 'label' => 'Thời gian', 'name' => 'time'],
//                    ['type' => 'text', 'class' => '', 'label' => 'Thời gian hỗ trợ', 'name' => 'support_time'],
//                    ['type' => 'checkbox', 'class' => '', 'label' => 'Hiện Sản phẩm hot', 'name' => "custom_slide_product_hot"],
//                    ['type' => 'text', 'class' => '', 'label' => 'Mô tả thương hiệu', 'name' => 'logo_brand_intro'],
//                    ['type' => 'checkbox', 'class' => '', 'label' => 'Hiện khối Thương hiệu', 'name' => "show_bran_homepage"],
//                    ['type' => 'checkbox', 'class' => '', 'label' => 'Hiện khối Đăng ký Email', 'name' => "show_email_homepage"],
//                    ['type' => 'text', 'class' => '', 'label' => 'Màu nền khối đăng ký email', 'name' => 'background_color_email'],
////                    ['type' => 'textarea_editor', 'class' => '', 'label' => 'Lý do mua', 'name' => 'taisao_mua', 'value' => ['taisao_mua']],
////                    ['type' => 'textarea_editor', 'class' => '', 'label' => 'Nội dung trang chi tiết sản phẩm', 'name' => 'service_detail',],
////                    ['type' => 'textarea_editor', 'class' => '', 'label' => 'Hỗ trợ trang chi tiết sản phẩm', 'name' => 'support_view_detail', 'value' => ['support_view_detail']],
////                    ['type' => 'textarea_editor', 'class' => '', 'label' => 'Top footer trang chi tiết sản phẩm', 'name' => 'footer_detail_top', 'value' => ['footer_detail_top']],
////                    ['type' => 'file_editor', 'class' => '', 'label' => 'Ảnh quà tặng', 'name' => "photos[logo_qua_tang]", 'value' => ['logo_qua_tang']],
////                    ['type' => 'text', 'class' => '', 'label' => 'ID Bài viết khảo sát', 'name' => 'khao_sat_post', 'value' => ['khao_sat_post']],
//                ]
//            ],

            'enterprise_page_tab' => [
                'label' => 'Tên pháp lý doanh nghiệp',
                'icon' => '<i class="kt-menu__link-icon flaticon2-open-box"></i>',
                'intro' => '',
                'td' => [
                    ['type' => 'text', 'class' => '', 'label' => 'Tên', 'name' => 'name_enterprise'],
                    ['type' => 'text', 'class' => '', 'label' => 'SĐT', 'name' => 'tel_enterprise'],
                    ['type' => 'text', 'class' => '', 'label' => 'Địa chỉ', 'name' => 'address_enterprise'],
                    ['type' => 'text', 'class' => '', 'label' => 'Quận huyên', 'name' => 'district_enterprise'],
                    ['type' => 'text', 'class' => '', 'label' => 'Tỉnh thành', 'name' => 'province_enterprise'],
                    ['type' => 'text', 'class' => '', 'label' => 'Quốc gia', 'name' => 'nation_enterprise'],
                    ['name' => 'timezone_enterprise', 'type' => 'select', 'label' => 'Múi giờ', 'options' => [
                        'Cầu Giấy, Hà Nội (GMT+7)' => 'Cầu Giấy, Hà Nội (GMT+7)',
                        'Hà Nội UTC +7' => 'Hà Nội UTC +7',
                    ], 'class' => ''],
                    ['type' => 'text', 'class' => '', 'label' => 'Đơn vị hiện tại', 'name' => 'unit_enterprise'],

                ]
            ],
        ]
    ];

    public function setting(Request $request)
    {

        $data['page_type'] = 'list';

        $module = \Eventy::filter('theme.custom_module', $this->module);
        if (!$_POST) {
            $listItem = $this->model->get();
            $tabs = [];
            foreach ($listItem as $item) {
                $tabs[$item->type][$item->name] = $item->value;
            }
            #
            $data['tabs'] = $tabs;
            $data['page_title'] = $module['label'];
            $data['module'] = \Eventy::filter('theme.custom_module', $module);
            return view(config('core.admin_theme') . '.setting.view')->with($data);
        } else {
            foreach ($module['tabs'] as $type => $tab) {
                $data = $this->processingValueInFields($request, $tab['td'], $type . '_');

                //  Tùy chỉnh dữ liệu insert
                if (isset($data['category_post_ids'])) {
                    $data['category_post_ids'] = '|' . implode('|', $data['category_post_ids']) . '|';
                }
                if (isset($data['category_product_ids'])) {
                    $data['category_product_ids'] = '|' . implode('|', $data['category_product_ids']) . '|';
                }
                #

                foreach ($data as $key => $value) {
                    $item = Setting::where('name', $key)->where('type', $type)->first();
                    if (!is_object($item)) {
                        $item = new Setting();
                        $item->name = $key;
                        $item->type = $type;
                    }
                    $item->value = $value;
                    $item->save();
                }
            }

            if(Schema::hasTable('admin_logs')){
                $this->adminLog($request,$item=false,'settingTheme');
            }
            $robots = Setting::where('name', 'robots')->where('type', 'seo_tab')->first();
            if (!empty($robots->value)){
                @Category::whereIn('type',[1,5])->update([
                    'meta_robot' => @$robots->value
                ]);
                @\Modules\STBDProduct\Models\Manufacturer::whereRaw('1=1')->update([
                    'meta_robot' => @$robots->value
                ]);
            }

            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Cập nhật thành công!');

            if ($request->return_direct == 'save_exit') {
                return redirect('admin/dashboard');
            }

            return back();
        }
    }
}
