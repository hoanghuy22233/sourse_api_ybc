<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace Modules\ThemeWorkart\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeWorkart\Models\Category;
use Modules\ThemeWorkart\Models\Post;
use Modules\ThemeWorkart\Models\Product;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function loadSanPhamHtml(Request $r)
    {
        $data['products'] = Product::find($r->product_id);
        return view('themeworkart::childs.post.product_in_content_post',$data);
    }
    public function getDetail($slug,$slug_cate)
    {
        $data['cate_id'] = CommonHelper::getFromCache('category_slug'.@$slug_cate);
        if(!$data['cate_id']){
            $data['cate_id'] = Category::where('slug',$slug_cate)->first();
            CommonHelper::putToCache('category_slug'.@$slug_cate, $data['cate_id']);
        }

        $data['post'] = CommonHelper::getFromCache('post_slug'.$slug);
        if (!$data['post']) {
            $data['post'] = Post::where('status', 1)->where('slug', $slug)->first();
            $slugcanonica = \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getPostSlug($data['post']);

//            $url = str_replace('https://bephoangcuong.com','',url()->current());
//            if ($slugcanonica ==$url){
//                $data['canonical'] = Url($slug_cate.'/'.$slug.'.html');
                $data['canonical'] = \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getPostSlug($data['post']);
//            }

            CommonHelper::putToCache('post_slug'.$slug, $data['post']);
        }

        if(!is_object($data['post']) ){
            return redirect('/');
        }


        $data['post'] = $this->getHtmlProductInContentPost($data['post']);

        $pageOption = [
            'title'  => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
            'description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->name,
            'keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->name,
        ];
        view()->share('pageOption', $pageOption);
        return view('themeworkart::childs.post.index',$data);
    }


    public function getHtmlProductInContentPost($post) {
        $arr = explode('[Products:', $post->content);
        $content = '';
        foreach ($arr as $v){
            if(strpos($v,']')){
                $arr1 = explode(']', $v);

                $prd_ids = explode(',', $arr1[0]);
                $div_prods = '';
                foreach ($prd_ids as $k=>$prd_id) {
                    if ($k%6==0){
                        $div_prods .= '<div class="product-in-content">';
                    }
                    $div_prods .= '<div class="prd-in-content" data-id="'.$prd_id.'">';
                    $div_prods .= '</div>';
                    if (($k+1)%6==0 || ($k+1==count($prd_ids))){
                        $div_prods .= '</div>';
                    }
                }
//                $div_prods .= '</div>';
                $content .= $div_prods . $arr1[1];
            }else{
                $content .= $v;
            }
        }
        $post->content = $content;
        return $post;
    }

    public function getIndexTags($slug,Request $request)
    {

        $data['category'] = CommonHelper::getFromCache('get_category_post_by_slug_'.$slug);
        if (!$data['category']) {
            $data['category'] = Category::where('slug', $slug)->whereIn('type', [1,2])->where('status', 1)->first();
            CommonHelper::putToCache('get_category_post_by_slug_'.$slug, $data['category']);
        }
        if(!is_object($data['category']))
            return redirect('/');

        $data['posts'] = CommonHelper::getFromCache('get_posts_by_category_id_'.$data['category']->id);
        if (!$data['posts']) {
            $data['posts'] = Post::select(['name', 'image', 'slug', 'multi_cat'])->where('multi_cat', 'LIKE', '%|'.$data['category']->id.'|%')
                ->where('status', 1)->orderBy('updated_at', 'desc')->paginate(12);
            CommonHelper::putToCache('get_posts_by_category_id_'.$data['category']->id, $data['posts']);
        }

        $data['filter'] = $request->all();

        $pageOption = [
            'title'  => $data['category']->meta_title != '' ? $data['category']->meta_title : $data['category']->name,
            'description' => $data['category']->meta_description != '' ? $data['category']->meta_description : $data['category']->name,
            'keywords' => $data['category']->meta_keywords != '' ? $data['category']->meta_keywords : $data['category']->name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themeworkart::childs.post.index_tags')->with($data);
    }

    public function getSearch(Request $request)
    {
        $data['products'] = CommonHelper::getFromCache('search_products_by_code_'.base64_encode(json_encode($request->all())));
        if (!$data['products']) {
            $products = Product::select(['name', 'image', 'slug', 'multi_cat', 'base_price', 'final_price'])->where('status', 1);
            if($request->has('keyword')) {
                $products = $products->where('name', 'LIKE', "%".$request->get('keyword', '')."%");
                $data['text_filter'] = 'Tìm kiếm với từ khóa : ' . $request->get('keyword', '');
            }

            $data['products'] = $products->orderBy('updated_at', 'desc')->orderBy('order_no', 'desc')->paginate(9);
            CommonHelper::putToCache('search_products_by_code_'.base64_encode(json_encode($request->all())), $data['products']);
        }

        $data['filter'] = $request->all();

        if($request->has('keyword')) {
            $data['text_filter'] = 'Tìm kiếm với từ khóa : ' . $request->get('keyword', '');
        }

        $pageOption = [
            'title'  => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
            'pageName'  => 'Tìm kiếm với từ khóa : ' . $request->get('keyword'),
            'parentName' => '',
            
        ];
        view()->share('pageOption', $pageOption);

        return view('themeworkart::childs.category.index')->with($data);
    }

    public function getSearchPost(Request $request)
    {
        $data['posts'] = CommonHelper::getFromCache('search_posts_by_code_'.base64_encode(json_encode($request->all())));
        if (!$data['posts']) {
            $posts = Post::select(['name', 'image', 'slug', ])->where('status', 1);
            if($request->has('keyword_post')) {
                $posts = $posts->where('name', 'LIKE', "%".$request->get('keyword_post', '')."%");
                $data['text_filter'] = 'Tìm kiếm với từ khóa : ' . $request->get('keyword_post', '');
            }

            $data['posts'] = $posts->orderBy('updated_at', 'desc')->paginate(9);
        }

        $data['filter'] = $request->all();

        if($request->has('keyword_post')) {
            $data['text_filter'] = 'Tìm kiếm với từ khóa : ' . $request->get('keyword_post', '');
        }

        $pageOption = [
            'title'  => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
            'pageName'  => 'Tìm kiếm với từ khóa : ' . $request->get('keyword_post'),
            'parentName' => '',

        ];
        view()->share('pageOption', $pageOption);

        return view('themeworkart::childs.seach-post.index')->with($data);
    }
}