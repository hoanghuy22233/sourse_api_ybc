<?php

namespace Modules\ThemeWorkart\Http\Controllers\Frontend;

use App\Http\Helpers\CommonHelper;
use Modules\ThemeWorkart\Models\Showroom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShowRoomController extends Controller
{
   public function getShowRoom($slug, Request $request){
       $getLocation = Showroom::pluck('location', 'id');
       $id = [];
       foreach ($getLocation as $key => $val){
            if (\Modules\ThemeWorkart\Http\Helpers\CommonHelper::convertSlug($val) == $slug){
                array_push($id, $key);
            }
       }
       $showroom = Showroom::whereIn('id', $id);
       $data['showrooms'] = $showroom->orderBy('id', 'desc')->get();
       $data['name'] = $showroom->first();
       return view('themeworkart::childs.showroom.list')->with($data);
   }
}
