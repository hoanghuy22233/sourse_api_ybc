<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */

namespace Modules\ThemeWorkart\Http\Controllers\Frontend;
use App\Http\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use Modules\ThemeWorkart\Models\Category;
//use Modules\ThemeWorkart\Http\Helpers\CommonHelper;
use Modules\ThemeWorkart\Models\Contact;
use Modules\ThemeWorkart\Models\District;
use Modules\ThemeWorkart\Models\Manufacturer;
use Modules\ThemeWorkart\Models\Menu;
use Modules\ThemeWorkart\Models\NTPost;
use Modules\ThemeWorkart\Models\NTTerm;
use Modules\ThemeWorkart\Models\NTTermTaxonomy;
use Modules\ThemeWorkart\Models\Post;
use Modules\ThemeWorkart\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Modules\ThemeWorkart\Models\Banner;
use DB;

class FAQController extends Controller
{

    public function getIndex(){
        return view( 'themeworkart::other.faq' );
    }

}