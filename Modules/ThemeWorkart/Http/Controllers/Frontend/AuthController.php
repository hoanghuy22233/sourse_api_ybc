<?php

namespace Modules\ThemeWorkart\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Mail;
use Modules\ThemeWorkart\Http\Requests\CreateRequest;
use Modules\ThemeWorkart\Http\Requests\ForgotPasswordRequest;
use Modules\ThemeWorkart\Http\Requests\LoginRequest;
use Modules\ThemeWorkart\Models\User;
use Session;
use Validator;


class AuthController extends Controller
{

    public function getLogin()
    {
        $data['page_title'] = 'Đăng nhập';
        $data['page_type'] = 'list';
        return view('themeworkart::childs.user.login');
    }

    public function authenticate(LoginRequest $request)
    {
        $user = User::where('email', $request['email'])->orWhere('tel', $request['email'])->first();
        if (!is_object($user)) {
            CommonHelper::one_time_message('danger', 'Sai email hoặc số điện thoại');
            return redirect('/dang-nhap');
        }
        if (@$user->status == 0) {
            CommonHelper::one_time_message('danger', 'Tài khoản của bạn chưa được kích hoạt!');
            return redirect('/dang-nhap');
        }

        if (@$user->status == -1) {
            CommonHelper::one_time_message('danger', 'Tài khoản của bạn đã bị khóa!');
            return redirect('/dang-nhap');
        }

        if (\Auth::guard('user')->attempt(['email' => trim($request['email']), 'password' => trim($request['password'])], true)) {
            return redirect()->intended('/');
        } elseif (\Auth::guard('user')->attempt(['tel' => trim($request['email']), 'password' => trim($request['password'])], true))
            return redirect()->intended('/');
        else {
            CommonHelper::one_time_message('danger', 'Sai mật khẩu');
            return redirect('/dang-nhap');
        }
    }

    public function getRegister()
    {
        $data['page_title'] = 'Đăng ký';
        $data['page_type'] = 'list';
        return view('themeworkart::childs.user.register', $data);
    }

    public function postRegister(CreateRequest $request)
    {
        $data = $request->except('_token');
        $data['api_token'] = base64_encode(rand(1, 100) . time());
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);
        CommonHelper::one_time_message('success', 'Bạn đã đăng ký tài khoản thành công! Vui lòng đăng nhập!');
        \Auth::guard('user')->login($user);
        return redirect()->intended('/');
    }

    public function getForgotPassword(Request $request, $change_password)
    {
        if (!$_POST) {

            $query = User::where('change_password', $change_password);

            if (!$query->exists() || !isset($change_password)) {
                abort(404);
            }
            $data['page_title'] = 'Lấy lại mật khẩu';
            $data['page_type'] = 'list';
            return view('themeworkart::childs.user.change_password')->with($data);
        } else {

            if ($request->password == $request->re_password) {
                $user = User::where('change_password', $change_password)->first();
                $user->password = bcrypt($request->password);
                $user->change_password = $user->id . '_' . time();
                $user->save();
                CommonHelper::one_time_message('success', 'Đổi mật khẩu thành công! vui lòng đăng nhập!');
                return redirect('/dang-nhap');
            } else {
                return back()->with('alert_re_password', 'Nhập lại mật khâu không khớp!');
            }
        }
    }


    public function getEmailForgotPassword(Request $request)
    {
        if (!$_POST) {
            $data['page_title'] = 'Quên mật khẩu';

            return view('themeworkart::childs.user.forgot_password')->with($data);

        } else {
            $query = User::where('email', $request->email);

            if (!$query->exists()) {
                return back()->with('success', 'Email chưa được đăng ký');
            }
            $user = $query->first();
            $user->change_password = $user->id . '_' . time();
            $user->save();
            try {
                \Eventy::action('admin.restorePassword', [
                    'link' => \URL::to('forgot-password/' . @$user->change_password),
                    'name' => @$user->name,
                    'email' => $user->email
                ]);
                //CommonHelper::one_time_message('success', 'Email sẽ được gửi trong ít phút. Bạn vui lòng kiểm tra mail để lấy lại mật khẩu!');
                return back()->with('success', 'Email sẽ được gửi trong ít phút. Bạn vui lòng kiểm tra mail để lấy lại mật khẩu!');
            } catch (\Exception $ex) {
                CommonHelper::one_time_message('error', 'Xin vui lòng thử lại!');
            }
        }
    }


}




