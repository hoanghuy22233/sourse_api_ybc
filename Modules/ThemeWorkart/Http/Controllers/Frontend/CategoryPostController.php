<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace Modules\ThemeWorkart\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeWorkart\Models\Category;
use Modules\ThemeWorkart\Models\Post;
use Illuminate\Http\Request;

class CategoryPostController extends Controller
{

    public function getIndex($slug, Request $request)
    {
        $data['category'] = CommonHelper::getFromCache('get_category_post_by_slug_'.$slug);
        if (!$data['category']) {
            $data['category'] = Category::where('slug', $slug)->whereIn('type', [1,2])->where('status', 1)->first();
            CommonHelper::putToCache('get_category_post_by_slug_'.$slug, $data['category']);
        }
        if(!is_object($data['category']))
            return redirect('/');

        $data['posts'] = CommonHelper::getFromCache('get_posts_by_category_id_'.$data['category']->id);
        if (!$data['posts']) {
            $data['posts'] = Post::select(['name', 'image', 'slug', 'multi_cat'])->where('multi_cat', 'LIKE', '%|'.$data['category']->id.'|%')
                ->where('status', 1)->orderBy('updated_at', 'desc')->paginate(12);
            CommonHelper::putToCache('get_posts_by_category_id_'.$data['category']->id, $data['posts']);
        }

        $data['filter'] = $request->all();

        $pageOption = [
            'type'      => 'page',
            'pageName'  => $data['category']->name,
            'parentName' => 'Trang chủ',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('themeworkart::childs.category_post.index')->with($data);
    }

    public function getSearch(Request $request)
    {
        $data['posts'] = CommonHelper::getFromCache('search_posts_by_code_'.base64_encode(json_encode($request->all())));
        if (!$data['posts']) {
            $posts = Post::select(['name', 'image', 'slug', 'multi_cat', 'base_price', 'final_price'])->where('status', 1);
            if($request->has('keyword')) {
                $posts = $posts->where('name', 'LIKE', "%".$request->get('keyword', '')."%");
                $data['text_filter'] = 'Tìm kiếm với từ khóa : ' . $request->get('keyword', '');
            }

            $data['posts'] = $posts->orderBy('updated_at', 'desc')->paginate(9);
            CommonHelper::putToCache('search_posts_by_code_'.base64_encode(json_encode($request->all())), $data['posts']);
        }

        $data['filter'] = $request->all();

        if($request->has('keyword')) {
            $data['text_filter'] = 'Tìm kiếm với từ khóa : ' . $request->get('keyword', '');
        }

        $pageOption = [
            'type'      => 'page',
            'pageName'  => 'Tìm kiếm với từ khóa : ' . $request->get('keyword'),
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('themeworkart::childs.category.index')->with($data);
    }
}