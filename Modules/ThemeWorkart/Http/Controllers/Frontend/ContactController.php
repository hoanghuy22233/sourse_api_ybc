<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */
namespace Modules\ThemeWorkart\Http\Controllers\Frontend;
use App\Http\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Modules\ThemeWorkart\Models\Contact;
use Modules\ThemeWorkart\Models\District;
use Modules\ThemeWorkart\Models\Product;
use Modules\ThemeWorkart\Models\Settings;
use Illuminate\Http\Request;
use Mail;

class ContactController extends Controller
{

    function configMail()
    {
        $settings = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();
        if (isset($settings['driver'])) {
            $username = $settings['driver'] == 'mailgun' ? @$settings['mailgun_username'] : @$settings['smtp_username'];
            $config =
                [
                    'mail.from' => [
                        'address' => $username,
                        'name' => @$settings['mail_name'],
                    ],
                    'mail.driver' => @$settings['driver'],
                ];

            if ($settings['driver'] == 'mailgun') {
                $config['services.mailgun'] =
                    [
                        'domain' => trim(@$settings['mailgun_domain']),
                        'secret' => trim(@$settings['mailgun_secret']),
                    ];
                $config['mail.port'] = @$settings['mailgun_port'];
                $config ['mail.username'] = @$settings['mailgun_username'];
            } else {
                $config['mail.port'] = @$settings['smtp_port'];
                $config['mail.password'] = @$settings['smtp_password'];
                $config['mail.encryption'] = @$settings['smtp_encryption'];
                $config['mail.host'] = @$settings['smtp_host'];
                $config['mail.username'] = @$settings['smtp_username'];
            }
//            $config['services.onesignal'] = [
//                'app_id' => '420af10d-5030-4f34-af19-68078fd6467c',
//                'rest_api_key' => 'MTY0MjA5NTktNjgwNS00NGM3LTg3YmYtNzcwMmRhZDUyZmE2'
//            ];
//            config onesignal
//            dd($config);
            config($config);
        }
        return $settings;
    }
    public function contactsAjax(Request $request)
    {

        if ($request->ajax()){
            $contact = new Contact();
            $contact->name = $request->Name;
            $contact->tel = base64_encode($request->Phone);
            $contact->email = base64_encode($request->Email);
            $contact->province = $request->tinh_thanh;
            $contact->distric = $request->quan_huyen;
            $contact->address = $request->Address;
            $contact->url = $request->get('url', '');
            $contact->user_ip = @$_SERVER['REMOTE_ADDR'];
//            if ($request->has('product_code')) {
//                $product = Product::where('code', $request->product_code)->first();
//                if (is_object($product)) {
            if ($request->has('product_code')) {
                $contact->product_id = $request->product_code;
            } elseif ($request->has('pid')) {
                $contact->product_id = $request->pid;
            }
//                }
//            } else {
//                $contact->product_id = $request->pid;
//            }
            $contact->content = 'Form email '.$request->Content;
            $contact->save();

            $user_email = base64_decode($contact->email);
//            $settings = $this->configMail();

            $settings = $this->configMail();
            if (!empty($settings['smtp_username']) && !empty($settings['admin_emails'])){

                $admin_emails = explode(',',$settings['admin_emails']);

                foreach ($admin_emails as $admin_email){
                    Mail::send(['html' => 'themeworkart::childs.mails.contact'], compact('contact'), function ($message) use ($settings,$admin_email) {
                        $message->from($settings['smtp_username'], $settings['mail_name']);
                        $message->to($admin_email, 'Admin');
                        $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] liên hệ mới');
                    });
                }

            }

            return response()->json([
                'success' => true
            ]);



        }
    }

    public function provinceContactPost(Request $request){
        $html = '<select class="form-group contents" name="distric_contact_post" id="distric_contact_post" style="width: 100%;padding: 9px;border-radius: 3px;">';
        $disstrics = District::where('province_id',$request->province_contact_post)->orderBy('name','ASC')->get();
        foreach ($disstrics as $v){
            $html .= '<option value="'.$v->id.'">'.$v->name.'</option>';
        }
        $html .='</select>';
        return response()->json([
            'success' => true,
            'html' => $html
        ]);
    }
    public function addPhoneContact(Request $request){
        if ($request -> ajax()){

            $contact = new Contact();
            $contact->product_id = $request->ProductID;
            $contact->content = $request->State .' '. $request->Product;
            $contact->tel = base64_encode($request->Phone);
            $contact->province = $request->province_uu_dai;
            $contact->user_ip = @$_SERVER['REMOTE_ADDR'];
            $contact->save();

            try {
                $settings = $this->configMail();
                if (!empty($settings['smtp_username']) && !empty($settings['admin_emails'])){

                    $admin_emails = explode(',',$settings['admin_emails']);

                    foreach ($admin_emails as $admin_email){
                        Mail::send(['html' => 'themeworkart::childs.mails.contact'], compact('contact'), function ($message) use ($settings,$admin_email) {
                            $message->from($settings['smtp_username'], $settings['mail_name']);
                            $message->to($admin_email, 'Admin');
                            $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] liên hệ mới');
                        });
                    }

                }
            } catch (\Exception $ex) {
                return response()->json([
                    'success' => false,
                    'msg' => $ex->getMessage()
                ]);
            }

            return response()->json([
                'success' => true,
            ]);
        }
    }

    public function sendContact(Request $request){

        if ($request->ajax()){
            $contact = Contact::create([
                'name' => $request->name,
                'email' => base64_encode($request->email),
                'content' => 'Góc phải '.$request->message,
                'province' => $request->tinh_thanh,
                'tel' => base64_encode($request->tel),
                'url' => $request->get('url', ''),
                'user_ip' => @$_SERVER['REMOTE_ADDR']
            ]);
                //                $admin_email = Settings::where('name', 'email_notifi')->first();
//
//                Mail::send(['html' => 'themeworkart::childs.mails.contact'], compact('contact'), function ($message) use ($settings,$contact) {
//                    $message->from($settings['mailgun_username'], $settings['mail_name']);
//                    $message->to(base64_decode($contact->email), 'Admin');
//                    $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] liên hệ mới');
//                });
$settings = $this->configMail();
            if (!empty($settings['smtp_username']) && !empty($settings['admin_emails'])){

                $admin_emails = explode(',',$settings['admin_emails']);

                foreach ($admin_emails as $admin_email){
                    Mail::send(['html' => 'themeworkart::childs.mails.contact'], compact('contact'), function ($message) use ($settings,$admin_email) {
                        $message->from($settings['smtp_username'], $settings['mail_name']);
                        $message->to(trim($admin_email), 'Admin');
                        $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] liên hệ mới');
                    });
                }

            }

            return response()->json([
                'success' => true
            ]);
        }
    }



    public function getIndex()
    {
        $pageOption = [
            'type'      => 'page',
            'pageName'  => 'Liên hệ',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('themeworkart::childs.contact.index');
    }

    public function postIndex(Request $request) {
        $data = $request->except('_token');
        $data['user_ip'] = @$_SERVER['REMOTE_ADDR'];
        $contact = Contact::create($data);

        try {
            Mail::send('emails.new_contact', ['bill' => $contact], function($message)
            {
                $admin_config = Settings::where('type', 'admin_info')->pluck('value', 'name')->toArray();
                $message->from(env('MAIL_USERNAME'), '['.Settings::select(['value'])->where('name', 'name')->value.']');
                $message->to($admin_config['admin_email'], $admin_config['admin_name'])->subject('Liên hệ mới');
            });

        } catch (\Exception $ex) {

        }

        return response()->json([
            'status'    => true
        ]);
    }

    public function ajax_contact(Request $request) {
        $data = $request->except('_token');
        $data['user_ip'] = @$_SERVER['REMOTE_ADDR'];
        $contact = Contact::create($data);

        try {
            @Mail::send('emails.new_contact', ['bill' => $contact], function($message)
            {
                $admin_config = Settings::where('type', 'admin_info')->pluck('value', 'name')->toArray();
                $message->from(env('MAIL_USERNAME'), '['.Settings::select(['value'])->where('name', 'name')->value.']');
                $message->to($admin_config['admin_email'], $admin_config['admin_name'])->subject('Liên hệ mới');
            });
        } catch (\Exception $ex) {

        }

        return response()->json([
            'status'    => true
        ]);
    }
}