<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */

namespace Modules\ThemeWorkart\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeWorkart\Models\Category;
use Modules\ThemeWorkart\Models\Contact;
use Modules\ThemeWorkart\Models\Installment_purchases;
use Modules\ThemeWorkart\Models\Manufacturer;
use Modules\ThemeWorkart\Models\Menu;
use Modules\ThemeWorkart\Models\Post;
use Modules\ThemeWorkart\Models\Product;
use Modules\ThemeWorkart\Models\Promotion;
use Modules\ThemeWorkart\Models\Review;
use Modules\ThemeWorkart\Models\Settings;
use Modules\ThemeWorkart\Models\Showroom;
use Modules\ThemeWorkart\Models\Trademark;
use Modules\ThemeWorkart\Models\Widget;
use Illuminate\Http\Request;
use Session;
use DB;
use Cookie;

class ProductController extends Controller
{

    public function allReview(Request $request){
        $reviews = Review::orderBy('id','DESC')->get();
        $data['reviews'] = $reviews;
        return view('themeworkart::childs.product.allReview')->with($data);
    }
    public function addReview(Request $request){

        $validatedData = $request->validate([
            'product_id' => 'required',
            'description' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'star' => 'required',
        ]);

        $newName = "";
        if($request->picture != null){
            $image = $request->picture;
            $newName =  time().'.'.$image->getClientOriginalExtension();
            $path = public_path() . "/public/img_reviews/";
            $image->move($path, $newName);
        }
        Review::create([
            'product_id' => $request->product_id,
            'description' => $request->description,
            'name' => $request->first_name . " " . $request->last_name,
            'email' => $request->email,
            'star' => $request->star,
            'picture' => $newName
        ]);
        $promo = Promotion::where('status', 1)->first();
        Session::flash('code', $promo->code);
        return redirect()->back();
    }
    public function getDetail($cate_slug, $slug, Request $request, $product = false)
    {


        if (!Category::where('slug', $cate_slug)->exists() && !Category::where('slug', $slug)->exists() && !Product::where('slug', $slug)->exists() && !Post::where('slug', $slug)->exists()) {
            return redirect()->back();
        }
        if ($cate_slug == false && $product) {
            $data['category'] = $product->first_category;
        }
        if (!isset($data['category']) || $data['category'] == false) {
            $data['category'] = CommonHelper::getFromCache('category_slug' . $cate_slug);
            if (!$data['category']) {
                $data['category'] = Category::where('slug', $cate_slug)->first();
                CommonHelper::putToCache('category_slug' . $cate_slug, $data['category']);
            }
        }
        // get all Product
        $data_id = $this->getIdAllProduct($data['category']);
        $data['child_cate'] = CommonHelper::getFromCache('get_child_cate_by_cate' . @$data['category']->id);
        if (!$data['child_cate']) {
            $data['child_cate'] = CommonHelper::getFromCache('category_parent_id_type_5' . @$data['category']->id);
            if (!$data['child_cate']) {
                $data['child_cate'] = Category::where('status', 1)->where('type', 5)->where('parent_id', @$data['category']->id)->get();
                CommonHelper::putToCache('category_parent_id_type_5' . @$data['category']->id, $data['child_cate']);
            }

            CommonHelper::putToCache('get_child_cate_by_cate' . $cate_slug, $data['category']);
        }

        if (!$product) {
            $data['product'] = CommonHelper::getFromCache('product_slug' . $slug);
            if (!$data['product']) {
                $data['product'] = Product::where('slug', $slug)->where('status', 1)->first();
                CommonHelper::putToCache('product_slug' . $slug, $data['product']);
            }

        } else {
            $data['product'] = $product;
            $recenty = [];
            if(Session::has('recenty')){
                $recenty = Session::get('recenty');
                $check = array_search ($product->id, $recenty);
                if(!$check){
                    $recenty[] = $product->id;
                }else{
                    foreach($recenty as $k => $id){
                        if($id == $product->id){
                            unset($recenty[$k]);
                        }
                    }
                    $recenty[] = $product->id;
                }

                Session::put('recenty', $recenty);
            }else{
                $recenty[] = $product->id;
                Session::put('recenty', $recenty);
            }

        }

        if ($data['product'] != null || isset($data['product'])) {
            $data['id_child'] = CommonHelper::getFromCache('category_in_ids' . $data['product']->multi_cat);
            if (!$data['id_child']) {
                $data['id_child'] = Category::whereIn('id', explode('|', $data['product']->multi_cat))->first();
                CommonHelper::putToCache('category_in_ids' . $data['product']->multi_cat, $data['id_child']);
            }

            $data['productCptbs'] = CommonHelper::getFromCache('get_san_pham_tuong_thich' . $data['product']->id . $data['product']->final_price . implode('|', $data_id));
            if (!$data['productCptbs']) {
                $data['productCptbs'] = Product::whereIn('id', $data_id)->where('status', 1)
                    ->whereBetween('final_price', [$data['product']->final_price - 3000000, $data['product']->final_price + 3000000])
                    ->where('id', '<>', $data['product']->id)->orderBy('id', 'desc')->limit(10)->get();
                CommonHelper::putToCache('get_san_pham_tuong_thich' . $data['product']->id . $data['product']->final_price . implode('|', $data_id), $data['productCptbs']);
            }

            if ($data['product']->image_extra == '' || $data['product']->image_extra == null) {
                $image_extras = [];
            } else {
                $image_extras = explode('|', $data['product']->image_extra);
                if ($image_extras['0'] == '') {
                    array_shift($image_extras);
                }
                if (end($image_extras) == '') {
                    array_pop($image_extras);
                }
            }
            $data['image_extras'] = $image_extras;
            $data['showroom'] = CommonHelper::getFromCache('get_all_showroom_product');
            if (!$data['showroom']) {
                $data['showroom'] = Showroom::all();
                CommonHelper::putToCache('get_all_showroom_product', $data['showroom']);
            }
            $p = [];
            $c = ['values' => [], 'location' => []];
            foreach ($data['showroom'] as $key => $t) {
                array_push($p, mb_strtoupper($t['location']));
                foreach ($p as $key => $l) {
                    $p = array_unique($p);
                }
            }
            foreach ($p as $keyp => $l) {
                array_push($c['values'], [$l => []]);
                array_push($c['location'], $l);
                foreach ($data['showroom'] as $key => $t) {
                    if (mb_strtoupper($l) == mb_strtoupper($t['location'])) {
                        array_push($c['values'][$keyp][$l], $t);
                    }
                }
            }
            $fg = [];
            foreach ($c['values'] as $f) {
                foreach ($f as $g) {
                    foreach ($g as $gj) {
                        $fg[] = $gj;
                    }
                }
            }

            $pageOption = [
                'image' => \URL::to('filemanager/userfiles/' . $data['product']->image),
                'title' => $data['product']->meta_title != '' ? $data['product']->meta_title : $data['product']->name,
                'description' => $data['product']->meta_description != '' ? $data['product']->meta_description : $data['product']->name,
                'keywords' => $data['product']->meta_keywords != '' ? $data['product']->meta_keywords : $data['product']->name,
            ];
            view()->share('pageOption', $pageOption);

//            setcookie('products', 'SDFDSF', 600000, '/');
//            $product_ids_cookie = isset($_COOKIE['product_viewed']) ? $_COOKIE['product_viewed'] : [];
//            $product_ids_cookie[] = $data['product']->id;
//            setcookie('product_viewed', $product_ids_cookie, time() + (86400 * 365), "/");
//            Session::forget('product_ids');
//            Session::push('product_ids3', $data['product']->id);
//            print_r(Session::get('product_ids3'));
//            dd('w');
            session_start();
//            session_destroy();
            if (!isset($_SESSION['product_viewed'])) {
                $product_viewed = [$data['product']->id];
            } else {

                $product_viewed = $_SESSION['product_viewed'];
                if (count($product_viewed) > 9) {
                    array_shift($product_viewed);
                }
                $product_viewed[] = $data['product']->id;
            }
            $_SESSION['product_viewed'] = $product_viewed;
            $data['canonical'] = asset(\Modules\ThemeWorkart\Http\Helpers\CommonHelper::getProductSlug($product));
            return view('themeworkart::childs.product.detail', ['c' => $c, 'fg' => $fg])->with($data);
        } else {

            $data['slug'] = urldecode($request->path());
            $data['slug'] = explode('/', $data['slug']);
            $data['slug'] = $data['slug']['0'];
            if (Post::where('slug', $slug)->exists()) {
                $data['post'] = CommonHelper::getFromCache('post_slug' . $slug);
                if (!$data['post']) {
                    $data['post'] = Post::where('slug', $slug)->first();
                    CommonHelper::putToCache('post_slug' . $slug, $data['post']);
                }
                if ($data['post'] == null || !isset($data['post'])) {
                    return redirect()->back();
                }
                if ($data['post']->type_page == 'page_static') {
                    $pageOption = [
                        'image' => \URL::to('filemanager/userfiles/' . $data['post']->image),
                        'title' => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
                        'description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->name,
                        'keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->name,
                    ];
                    view()->share('pageOption', $pageOption);
                    return view('themeworkart::childs.page.index')->with($data);
                }
                $data['tags'] = explode('|', $data['post']->tags);
                if ($data['tags']['0'] == '' || $data['tags']['0'] == 0 || end($data['tags']) == '') {
                    array_shift($data['tags']);
                    array_pop($data['tags']);
                }
                $data['multi_cat'] = explode('|', $data['post']->multi_cat);
                if ($data['multi_cat']['0'] == '' || $data['multi_cat']['0'] == 0 || end($data['multi_cat']) == '') {
                    array_shift($data['multi_cat']);
                    array_pop($data['multi_cat']);
                }

                $data['cate_tags'] = CommonHelper::getFromCache('category_in_ids' . implode('|', $data['multi_cat']));
                if (!$data['cate_tags']) {
                    $data['cate_tags'] = Category::whereIn('id', $data['multi_cat'])->limit(5)->get();
                    CommonHelper::putToCache('category_in_ids' . implode('|', $data['multi_cat']), $data['cate_tags']);
                }

                $data['post_tags'] = CommonHelper::getFromCache('post_in_ids' . implode('|', $data['tags']));
                if (!$data['post_tags']) {
                    $data['post_tags'] = Post::whereIn('id', $data['tags'])->get();
                    CommonHelper::putToCache('post_in_ids' . implode('|', $data['tags']), $data['post_tags']);
                }

                $pageOption = [
                    'image' => \URL::to('filemanager/userfiles/' . $data['post']->image),
                    'title' => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
                    'description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->name,
                    'keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->name,
                ];

                view()->share('pageOption', $pageOption);
                return view('themeworkart::childs.post.post_detai')->with($data);
            } else {
                $data['categorysub'] = Category::where('slug', $slug)->first();
                if (!empty($data['categorysub'])) {

                    $data['posts'] = CommonHelper::getFromCache('get_posts_by_cate' . $data['categorysub']->id);
                    if (!$data['posts']) {
                        $data['posts'] = Post::where('category_id', $data['categorysub']->id)
                            ->where('status', 1)
                            ->orderBy('id', 'desc')->paginate(11);
                        CommonHelper::putToCache('get_posts_by_cate' . $data['categorysub']->id, $data['posts']);
                    }

                    $pageOption = [
                        'image' => \URL::to('filemanager/userfiles/' . $data['post']->image),
                        'title' => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
                        'description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->name,
                        'keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->name,
                    ];
                    view()->share('pageOption', $pageOption);
                    return view('themeworkart::childs.category.post_cate')->with($data);
                } else {
                    return redirect()->back();
                }
            }
        }

    }

    public function detailTwo($slug, $productSlug, Request $request)
    {

        $cate_post = Menu::where('url', '/' . $productSlug)->where('parent_id', '<>', 0)->orWhere('parent_id', '<>', Null)->first();
        $catego = Category::where('slug', $slug)->first();
        $manufacturersa = Manufacturer::where('slug', $productSlug)->first();

//        if (strpos($_SERVER['REQUEST_URI'], '/public/') !== false) {
//            $uri = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
//            $uri = str_replace('/public', '', $uri);
//            return redirect($uri);
//        }
        if (!empty($cate_post->toArray()) && @$catego->type == 1) {

            $cate = new CategoryController();
            return $cate->getIndex($productSlug, $request);
        }
        if (!empty($cate_post->toArray()) && @$catego->type == 5 && is_null($manufacturersa)) {
            $cate = new CategoryController();
            return $cate->getIndex($productSlug, $request);
        } elseif (strpos($productSlug, '.html')) {   //  là sản phẩm || bài viết
            $productSlug = str_replace('.html', '', $productSlug);

            // Chi tiết sản phẩm

            $product = CommonHelper::getFromCache('product_slug' . @$productSlug);
            if (!$product) {
                $product = Product::where('slug', $productSlug)->where('status', 1)->first();
                CommonHelper::putToCache('product_slug' . @$productSlug, $product);
            }


            if (is_object($product)) {

                $data['canonical'] = Url($slug . '/' . $productSlug . '.html');
                $product = $this->getHtmlProductInContentProduct($product);
                return $this->getDetail($slug, $productSlug, $request, $product);
            }

            //  Chi tiết bài viết
            $postController = new PostController();
            return $postController->getDetail($productSlug, $slug);
        } else {
            //  là vào danh-muc/thuong-hieu

            $urlCate = $slug;
            $url = $productSlug;
            if (!Manufacturer::where('slug', $url)->exists()) {
                return redirect()->back();
            } else {

                $data['manufacturer'] = CommonHelper::getFromCache('manufacturer_slug' . @$url);
                if (!$data['manufacturer']) {
                    $data['manufacturer'] = Manufacturer::where('slug', $url)->first();
                    CommonHelper::putToCache('manufacturer_slug' . @$url, $data['manufacturer']);
                }

                $data['category'] = CommonHelper::getFromCache('categorys_slug' . @$urlCate);
                if (!$data['category']) {
                    $data['category'] = Category::where('slug', $urlCate)->first();
                    CommonHelper::putToCache('manufacturer_slug' . @$urlCate, $data['category']);
                }
                if (empty($data['category'])) {
                    return redirect('/');
                }
                $id = $data['category']->id;
                $categoryController = new CategoryController();
                [$data_id, $arrayCate_id, $data['child_category']] = $categoryController->getAllPluckProduct($data['category']);
                if ($request->ajax()) {
                    return $categoryController->getDataAjaxCateAndManu(null, 36, $data['category'], $data_id, $data['manufacturer']);
                } else {
                    $data['child_category'] = CommonHelper::getFromCache('category_parent_id' . $id);
                    if (!$data['child_category']) {
                        $data['child_category'] = Category::where('parent_id', $id)->where('status', 1)->orderBy('order_no', 'asc')->limit(3)->get();
                        CommonHelper::putToCache('category_parent_id' . $id, $data['child_category']);
                    }

                    $data['child_category_show'] = CommonHelper::getFromCache('category_parent_id' . $id);
                    if (!$data['child_category_show']) {
                        $data['child_category_show'] = Category::where('parent_id', $id)->where('status', 1)->get();
                        CommonHelper::putToCache('category_parent_id' . $id, $data['child_category_show']);
                    }

                    foreach ($data['child_category_show'] as $val) {
                        array_push($arrayCate_id, $val->id);
                    }

                    $data['products'] = CommonHelper::getFromCache('get_product_by_' . $url);
                    if (!$data['products']) {
                        $data['products'] = Product::where('manufacture_id', $data['manufacturer']->id)->whereIn('id', $data_id)
                            ->where('status', 1)->where('type', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->orderBy('order_no', 'desc')->limit(20)->get();
                        CommonHelper::putToCache('get_products_by' . $url, $data['products']);
                    }

                    $data['productNolimit'] = CommonHelper::getFromCache('get_productsNolimit_by' . $url);
                    if (!$data['productNolimit']) {
                        $data['productNolimit'] = Product::whereIn('id', $data_id)->where('manufacture_id', $data['manufacturer']->id)
                            ->where('status', 1)->where('type', 1)->get();
                        CommonHelper::putToCache('get_productsNolimit_by' . $url, $data['productNolimit']);
                    }
                    $data['countProduct'] = count($data['productNolimit']);

                    $data['bran_ids'] = Product::where('manufacture_id', $data['manufacturer']->id)->whereIn('id', $data_id)
                        ->where('status', 1)->where('type', 1)->pluck('manufacture_id')->toArray();

                    $data['category_des'] = $data['category']->category_manufacturer_frontend_des_first . ' ' . $data['category']->name . ' ' . $data['manufacturer']->name . ' ' . $data['category']->category_manufacturer_frontend_des_last;
                    $data['category_des_bot'] = $data['category']->category_manufacturer_frontend_des_first_bot . ' ' . $data['category']->name . ' ' . $data['manufacturer']->name . ' ' . $data['category']->category_manufacturer_frontend_des_last_bot;
                    $pageOption = [
                        'image' => \URL::to('filemanager/userfiles/' . $data['category']->image),
                        'title' => $data['category']->category_manufacturer_first_title . ' ' . $data['category']->name . ' ' . $data['manufacturer']->name . ' ' . $data['category']->category_manufacturer_last_title,
                        'description' => $data['category']->category_manufacturer_first_description . ' ' . $data['category']->name . ' ' . $data['manufacturer']->name . ' ' . $data['category']->category_manufacturer_last_description,
                        'keywords' => $data['category']->category_manufacturer_first_title . ' ' . $data['category']->name . ' ' . $data['manufacturer']->name . ' ' . $data['category']->category_manufacturer_last_title,
                    ];
                    view()->share('pageOption', $pageOption);
                    return view('themeworkart::childs.category.index')->with($data);
                }
            }
        }
    }

    public function detailThree($slug, $slugTwo, $productSlug, Request $request)
    {
        $productSlug = str_replace('.html', '', $productSlug);
        //  Chi tiết sản phẩm
        $product = Product::where('slug', $productSlug)->where('status', 1)->first();
        if (is_object($product)) {
            $data['canonical'] = Url($slug . '/' . $slugTwo . '/' . $productSlug . '.html');
            $product = $this->getHtmlProductInContentProduct($product);
            return $this->getDetail($slug, $productSlug, $request, $product);
        }
//dd(1);

        //  Chi tiết bài viết
        $postController = new PostController();
        return $postController->getDetail($productSlug, $slugTwo);
    }

    public function detailFor($slug, $slugTwo, $slugThree, $productSlug, Request $request)
    {
        $productSlug = str_replace('.html', '', $productSlug);
        //  Chi tiết sản phẩm
        $product = Product::where('slug', $productSlug)->where('status', 1)->first();
        if (is_object($product)) {

            $data['canonical'] = Url($slug . '/' . $slugTwo . '/' . $slugThree . '/' . $productSlug . '.html');
            $product = $this->getHtmlProductInContentProduct($product);
            return $this->getDetail($slugTwo, $productSlug, $request, $product);
        }
        return back();
    }

    public function getIdAllProduct($category)
    {
        $getIdCate = CommonHelper::getFromCache('get_all_index');
        if (!$getIdCate) {
            $getIdCate = Product::where('status', 1)->pluck('multi_cat', 'id');
            CommonHelper::putToCache('get_all_index', $getIdCate);
        }
        $data_id = '|';
        if ($category) {
            if ($category->parent_id == 0 || $category == null) {
                $data['category_childs'] = CommonHelper::getFromCache('category_parent_id' . $category);
                if (!$data['category_childs']) {
                    $data['category_childs'] = Category::where('parent_id', $category->id)->get();
                    CommonHelper::putToCache('category_parent_id' . $category, $data['category_childs']);
                }

                $data['category_childs']->push($category);
                foreach ($data['category_childs'] as $val) {
                    foreach ($getIdCate as $key => $val1) {
                        $t = explode('|', $val1);
                        if (in_array($val->id, $t)) {
                            $data_id .= $key . '|';
                        }
                    }
                }
            } else {
                foreach ($getIdCate as $key => $val1) {
                    $t = explode('|', $val1);
                    if (in_array($category->id, $t)) {
                        $data_id .= $key . '|';
                    }
                }
            }
        }
        $data_id = explode('|', $data_id);
        $data_id = array_unique($data_id);
        return $data_id;
    }

    public function ajax_get_product(Request $request)
    {
        $keyword = $request->get('keyword', false);
        $manufactureres = $request->get('manufactureres', false);
        $min_price = $request->get('min_price', 0);
        $max_price = $request->get('max_price', false);
        $order = $request->get('order', false);
        $category_id = $request->get('category_id', false);
        $data['category'] = Category::select(['slug'])->where('id', $category_id)->first();

        $key = json_encode($manufactureres) . $min_price . $max_price . $order . $category_id;

        $products = CommonHelper::getFromCache('search_products_by_key_' . $key);
        if (!$products) {
            $products = Product::select(['id', 'name', 'image', 'slug', 'multi_cat', 'base_price', 'final_price'])->where('status', 1)
                ->where('final_price', '>=', $min_price);
            if ($category_id) {
                $cat_childs = Category::where('parent_id', $category_id)->where('status', 1)->pluck('id')->toArray();
                $cat_childs[] = $category_id;
                // $products = $products->where('multi_cat', 'LIKE', '%|' . $category_id . '|%');
                $products = $products->where(function ($query) use ($cat_childs) {
                    foreach ($cat_childs as $cat_id) {
                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_id . '|%');
                    }
                });
            }
            if ($keyword) {
                $products = $products->where('name', 'LIKE', '%' . $keyword . '%');
            }
            if ($max_price) {
                $products = $products->where('final_price', '<=', $max_price);
            }
            if ($manufactureres) {
                $products = $products->where(function ($query) use ($manufactureres) {
                    foreach ($manufactureres as $manufacturer_id) {
                        $query->orWhere('manufacture_id', $manufacturer_id);
                    }
                });
            }
            if ($order && $order != 0) {
                switch ($order):
                    case '1':
                        $products = $products->orderBy('name', 'asc');
                        break;
                    case '2':
                        $products = $products->orderBy('name', 'desc');
                        break;
                    case '3':
                        $products = $products->orderBy('final_price', 'asc');
                        break;
                    case '4':
                        $products = $products->orderBy('final_price', 'desc');
                        break;
                    case '5':
                        $products = $products->orderBy('updated_at', 'desc');
                        break;
                    default:
                        $products = $products->orderBy('updated_at', 'asc');
                        break;
                endswitch;
            } else {
                $products = $products->orderBy('updated_at', 'desc')->orderBy('order_no', 'desc');
            }
            $products = $products->paginate(12);
            CommonHelper::putToCache('search_products_by_key_' . $key, $products);
        }
        $data['setPath'] = $data['category'] != null ? $data['category']->slug : '/tim-kiem';
        $data['products'] = $products;
        $data['filter'] = $request->all();
        return view('themeworkart::childs.product.ajax_get_product')->with($data);
    }

    public function ajax_quick_view(Request $request)
    {
        $product_id = $request->get('product_id', 0);

        $product = CommonHelper::getFromCache('get_product_by_product_id_' . $product_id);
        if (!$product) {
            $product = Product::select(['id', 'name', 'image', 'slug', 'content', 'multi_cat', 'base_price', 'final_price'])->where('status', 1)->where('id', $product_id)->first();
            CommonHelper::putToCache('get_product_by_product_id_' . $product_id, $product);
        }
        if (!is_object($product))
            return redirect('/');
        $data['product'] = $product;

        if ($product->image_extra == null) $image_extras = [];
        else $image_extras = explode("|", $product->image_extra);
        $count = count($image_extras);
        if (count($image_extras) > 4) {
            for ($i = 4; $i <= $count; $i++) {
                unset($image_extras[$i]);
            }
        }
        $data['image_extras'] = $image_extras;
        return view('themeworkart::childs.product.ajax_quick_view')->with($data);
    }

    public function postMuaTraGop(Request $request)
    {
        $Installment_purchase = new Installment_purchases();
        $Installment_purchase->name = $request->name;
        $Installment_purchase->tel = $request->tel;
        $Installment_purchase->product_id = $request->product_id;
        $Installment_purchase->service = $request->service;
        $Installment_purchase->address = $request->address;
        $Installment_purchase->save();

    }

    public function getHtmlProductInContentProduct($product)
    {

        $arr = explode('[Products:', $product->content);

        $content = '';
        foreach ($arr as $v) {
            if (strpos($v, ']')) {
                $arr1 = explode(']', $v);

                $prd_ids = explode(',', $arr1[0]);
                $div_prods = '';
                foreach ($prd_ids as $k1 => $prd_id) {
                    if ($k1 % 3 == 0) {
                        $div_prods .= '<div class="product-in-content" style="display:flex;">';
                    }
                    $div_prods .= '<div class="prd-in-content" data-id="' . $prd_id . '">';
                    $div_prods .= '</div>';
                    if (($k1 + 1) % 3 == 0 || ($k1 + 1 == count($prd_ids))) {
                        $div_prods .= '</div>';
                    }
                }
//                $div_prods .= '</div>';
                $content .= $div_prods . $arr1[1];
            } else {
                $content .= $v;
            }
        }


        $arr_highlight = explode('[Products:', $product->highlight);

        $highlight = '';
        foreach ($arr_highlight as $highlights) {
            if (strpos($highlights, ']')) {
                $arr_highlight1 = explode(']', $highlights);

                $prd_ids_highlight = explode(',', $arr_highlight1[0]);
                $div_prods_highlight = '';
                foreach ($prd_ids_highlight as $k2 => $prd_ids_highlights) {
                    if ($k2 % 3 == 0) {
                        $div_prods_highlight .= '<div class="product-in-content" style="display:flex;">';
                    }
                    $div_prods_highlight .= '<div class="prd-in-content" data-id="' . $prd_ids_highlights . '">';
                    $div_prods_highlight .= '</div>';
                    if (($k2 + 1) % 3 == 0 || ($k2 + 1 == count($prd_ids_highlight))) {
                        $div_prods_highlight .= '</div>';
                    }
                }
//                $div_prods_highlight .= '</div>';
                $highlight .= $div_prods_highlight . $arr_highlight1[1];
            } else {
                $highlight .= $highlights;
            }
        }


        $arr_review_detail = explode('[Products:', $product->review_detail);

        $content_review_detail = '';
        foreach ($arr_review_detail as $review_detail) {
            if (strpos($review_detail, ']')) {
                $arr1_review_detail = explode(']', $review_detail);

                $prd_ids_review_detail = explode(',', $arr1_review_detail[0]);
                $div_prods_review_detail = '';
                foreach ($prd_ids_review_detail as $k3 => $prd_id_review_detail) {
                    if ($k3 % 3 == 0) {
                        $div_prods_review_detail .= '<div class="product-in-content" style="display:flex;">';
                    }
                    $div_prods_review_detail .= '<div class="prd-in-content" data-id="' . $prd_id_review_detail . '">';
                    $div_prods_review_detail .= '</div>';
                    if (($k3 + 1) % 3 == 0 || ($k3 + 1 == count($prd_ids_review_detail))) {
                        $div_prods_review_detail .= '</div>';
                    }
                }
//                $div_prods_review_detail .= '</div>';

                $content_review_detail .= $div_prods_review_detail . $arr1_review_detail[1];
            } else {
                $content_review_detail .= $review_detail;
            }
        }

        $product->content = $content;
        $product->review_detail = $content_review_detail;
        $product->highlight = $highlight;
        return $product;
    }

    public function loadSanPhamTrongSanPhamHtml(Request $r)
    {
        $data['products'] = Product::find($r->product_id);
        return view('themeworkart::childs.product.product_in_content_product', $data);
    }
}