<?php
/**
 * Created by PhpStorm.
 * User: hoanghung
 * Date: 16/05/2016
 * Time: 15:19
 */

namespace Modules\ThemeWorkart\Http\Controllers\Frontend;
use App\Http\Helpers\CommonHelper;
use App\Http\Controllers\Controller;
use App\Mail\MailServer;
use App\Models\Setting;
use Modules\ThemeWorkart\Models\Bill;
use Modules\ThemeWorkart\Models\Category;
use Modules\ThemeWorkart\Models\District;
use Modules\ThemeWorkart\Models\Order;
use Modules\ThemeWorkart\Models\Product;
use Modules\ThemeWorkart\Models\Country;
use Modules\ThemeWorkart\Models\Promotion;
use Modules\ThemeWorkart\Models\Settings;
use Modules\ThemeWorkart\Models\Showroom;
use Modules\ThemeWorkart\Models\User;
use Auth;
use Illuminate\Http\Request;
use Mail;
use Modules\WorkartProduct\Models\PromotionalCode;
use Session;
use Cart;

class OrderController extends Controller
{
    public  function getCart(){
        $carts = Cart::content();

        return view('themeworkart::childs.product.cart',compact('carts'));
    }
    public  function addCart(Request $request,$id){
        $product = Product::where('id',$id)->first();
        if($request->qty <= 0){
            Session::flash('error', 'You have not chosen quantity');
            return redirect()->back();
        }
        if ($id) {
            $qty = $request->qty;
            $data['id'] = $product->id;
            $data['qty'] = $qty;
            $data['name'] =$product->name;
            $data['price'] = !empty($product->final_price) ? $product->final_price : 0;
            $data['weight'] = '131';
            $data['options']['image'] = $product->image;
            Cart::add($data);
            Session::flash('success', 'Add shopping cart successfully!');
            return redirect()->back();
        }
        else{
            CommonHelper::one_time_message('error', 'Bạn đã thêm khóa học này vào giỏ hàng rồi! Vui lòng chọn mua khóa học khác');
            return back();
        }

    }
    public  function addCartUpSale(Request $request){

        $id = $request->id;
        $product = Product::where('id',$id)->first();
        $quantity = 0;
        foreach($request->quant as $item){
            $quantity = $item;
        }
        if($quantity <= 0){
            Session::flash('error', 'You have not chosen quantity');
            return redirect()->back();
        }
        if ($id) {
            $qty = $request->qty;
            $data['id'] = $product->id;
            $data['qty'] = $quantity;
            $data['name'] =$product->name;
            $data['price'] = !empty($product->final_price) ? $product->final_price : 0;
            $data['weight'] = '131';
            $data['options']['image'] = $product->image;
            Cart::add($data);
            Session::flash('success', 'Add shopping cart successfully!');
            return redirect()->back();
        }
        else{
            CommonHelper::one_time_message('error', 'Bạn đã thêm khóa học này vào giỏ hàng rồi! Vui lòng chọn mua khóa học khác');
            return back();
        }

    }
    public function deleteCart($id){
        Cart::remove($id);
        return redirect()->to('/gio-hang');
    }
    public function updateCart(Request $request){
        $id = $request->id_hidden;
        Cart::update($id,$request->qty);
        return redirect()->to('/gio-hang');
    }
    public function addPromote(Request $request){
        $code = $request->code;

        $item = Promotion::where('code','like', $code)->first();
        $data = [];
        if($item){
            if($item->status == 0 || $item->status == 3){
                $data['status'] = 'error';
                $data['error'] = 'Stop promotion';
            }else{
                $carts = Cart::content();
                $priceTotalBase = 0;
                foreach( $carts  as $cart){
                    $priceTotalBase += $cart->price * $cart->qty;
                }
                $itemArr = $item->toArray();
                $value_promote = 0;
                $value_promote = $itemArr['value'];


                if(preg_match('#%$#', $value_promote)){
                    $value_promote = preg_replace("#%#", "", $value_promote);
                    $data['success']['promote'] = '-' . $value_promote . "%";
                    $data['success']['total'] =  '$' . number_format($priceTotalBase - $priceTotalBase * $value_promote / 100);
                }else{
                    $data['success']['promote'] = '-$' . number_format($value_promote);
                    $data['success']['total'] =  '$' . number_format($priceTotalBase - $value_promote);
                }

                Session::put('promotion', $itemArr);
                $data['status'] = 'success';


            }
        }else{
            $data['status'] = 'error';
            $data['error'] = 'This code does not exist';
        }
        return response()->json($data);
    }
//    public function getIndex(Request $request)
//    {
//
//        if ($request->ajax()) {
//            if (!$request->p) {
//                $id = $request->data;
//                if (!empty($id)) {
//                    $ditric = District::where('province_id', $id)->get();
//                    $html = '';
//                    foreach ($ditric as $data) {
//                        $html .= '<option value=' . $data->id . ' id=' . $data->id . '>' . $data->type . ' ' . $data->name . '</option>';
//                    }
//                    return response()->json($html);
//                }
//            }
//        }
//        $total = 0;
//        $data['msg'] = Session()->get('msg');
//        $data['cate'] = null;
//        $getRoom = Showroom::all();
//        $cart = Session()->get('cart');
//        if ($cart === null) {
//            session()->put('cart', null);
//            Session::save();
//        }
//        if (!empty($cart)) {
//            $id = end($cart);
//            $data['product'] = Product::find($id['id']);
//            $data['cate'] = Category::whereIn('id', explode('|', $data['product']->multi_cat))->first();
////            dd($data['cate']);
//            if (@$data['cate']->parent_id > 0) {
//                $data['cate'] = Category::where('id', @$data['cate']->parent_id)->first();
//            }
//        }
//
//        if ($request->ajax()) {
//            $data['product'] = Product::find($request->p);
//            $type = $request->type;
//            $item = [
//                'id' => $data['product']->id,
//                'name' => $data['product']->name,
//                'price' => $data['product']->final_price,
//                'image' => $data['product']->image,
//                'multi_cat' => $data['product']->multi_cat,
//                'gift' => $request->gift,
////                'gift_price_value' => $request->gift_price_value,
//                'quantity' => 1
//            ];
//
//            $cart = Session::get('cart');
//            if ($cart === null) {
//                session()->push('cart', $item);
//                Session::save();
//            } else {
//                $flag = false;
//                for ($i = 0; $i < count($cart); $i++) {
//                    if ($cart[$i]['id'] == $data['product']->id) {
//                        if (empty($type)) {
//                            $cart[$i]['quantity']++;
//                            session()->put('cart', $cart);
//                            Session::save();
//                        } else {
//                            if ($type === '+') {
//                                $cart[$i]['quantity']++;
//                            } elseif ($type === '-') {
//                                if ($cart[$i]['quantity'] <= 1) {
//                                    $cart[$i]['quantity'] = 1;
//                                } else {
//                                    $cart[$i]['quantity']--;
//                                }
//                            } else {
//                                $cart[$i]['quantity'] = (int)$type;
//                            }
//                            session()->put('cart', $cart);
//                            Session::save();
//                        }
//                        $flag = true;
//                        break;
//                    }
//                }
//                if ($flag == false) {
//                    session()->push('cart', $item);
//                    Session::save();
//                }
//            }
//            return Response()->json([
//                'success' => true,
//                'count'=>count(session()->get('cart'))
//            ]);
//        }
//        return view('themeworkart::childs.order.view', ['cart' => $cart, 'total' => $total, 'data' => $data, 'getRoom' => $getRoom]);
//    }
//
//    public function getRemoveCart(Request $request)
//    {
//        if ($request->ajax()) {
//            $id = $request['id'];
//            $cart = session()->get('cart');
//            for ($i = 0; $i < count($cart); $i++) {
//                if ($id == $cart[$i]['id']) {
//                    unset($cart[$i]);
//                    session()->put('cart', array_values($cart));
//                    Session::save();
//                }
//            }
//            return Response()->json([
//                'success' => true
//            ]);
//        }
//    }
//
//    public static function totalCart()
//    {
//        $countItemCart = '';
//        $arrCart = session()->get('cart');
//        if (!empty($arrCart)) {
//            $countItemCart = count($arrCart);
//        }
//        return $countItemCart;
//    }

    public function getDelivery()
    {

        $pageOption = [
            'type' => 'page',
            'pageName' => 'Giao hàng',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('themeworkart::childs.order.delivery');
    }

    public function postDelivery(Request $request)
    {
        /*if (!Auth::check()) {
            return redirect()->route('user.login');
        }*/
        $data = $request->except('_token');

        Session::put('cart_delivery', $data);
        $validatedData = $request->validate([
            'bill.user_email' => 'required|email',
            'bill.first_name' => 'required|max:255',
            'bill.last_name' => 'required|max:255',
            'bill.user_address' => 'required|max:255',
            'bill.user_apartment' => 'max:255',
            'bill.user_postal' => 'required|integer',
            'bill.user_country_id' => 'required|integer',
            'bill.user_tel' => 'required|min:7|max:15',

        ],[
            'required' => 'The :attribute is required',
            'email' =>  'The :attribute must be a valid email address.',
            'max' => 'The :attribute may not be greater than :max.',
            'integer' => 'The :attribute must be an integer.'
        ],[
            'bill.user_email' => 'email',
            'bill.first_name' => 'First Name',
            'bill.last_name' => 'Last Name',
            'bill.user_address' => 'Address',
            'bill.user_apartment' => 'Apartment',
            'bill.user_postal' => 'Postal',
            'bill.user_tel' => 'Tel',
            'bill.user_country_id' => 'Country'
        ]);
        return redirect()->route('order.pay');
    }

    public function getPay()
    {


        $pageOption = [
            'type' => 'page',
            'pageName' => 'Thanh toán',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('themeworkart::childs.order.pay');
    }
    public function getBill(Request $request)
    {
        $pay_method = $request->pay_method;
        $delivery = Session::get('cart_delivery');
        $delivery['bill']['payment_method'] = $pay_method;
        Session::put('cart_delivery', $delivery);
        return redirect()->route('order.redirectBill');
    }
    public function redirectBill()
    {

        $delivery = Session::get('cart_delivery');

        $pageOption = [
            'type' => 'page',
            'pageName' => 'Thanh toán',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);

        return view('themeworkart::childs.order.bill');
    }
    public function postBill(Request $request)
    {
        $delivery = Session::get('cart_delivery')['bill'];
        $data = $delivery;

        unset($data['payment_method']);

        if($delivery['payment_method'] != "cod"){
            $validatedData = $request->validate([
                'card.card_number' => 'required',
                'card.card_name' => 'required',
                'card.card_expi' => 'required',
                'card.card_code' => 'required',
            ], [], [
                'card.card_number' => 'Cart number',
                'card.card_name' => 'Name on Card',
                'card.card_expi' => 'Expiratation date',
                'card.card_code' => 'Security code',
            ]);
            $card = $request->card;
        }

        $data['user_name'] = $data['first_name'] . " " . $data['last_name'];
        $data['receipt_method'] = $delivery['payment_method'];
        $value_promote = 0;
        if(Session::get('promotion')){
            $promotion = Session::get('promotion');
            $data['coupon_code'] = $promotion['code'];
            $value_promote = $promotion['value'];
            $typePromote = "default";
            if(preg_match('#%$#', $value_promote)){
                $typePromote = "percent";
            }
        }
        // xử lý total price, có tính promote
        $total_price = 0;
        $carts = Cart::content();
        foreach ($carts as $k => $item) {
            $product = Product::find($item->id);
            $total_price += $item->price * $item->qty;
        }
        if($value_promote != 0){
            if($typePromote == "default"){
                $total_price -= $value_promote;
            }else if($typePromote == "percent"){
                $value_promote = preg_replace("#%#", "", $value_promote);
                $total_price = $total_price - $total_price *  $value_promote / 100;
            }
        }
        $data['total_price'] = $total_price;
        $data['date'] = date('d-m-Y h:i');
        $bill = Bill::create($data);

        // insert Orthers
        foreach ($carts as $k => $item) {

            $data = [
                'bill_id' => $bill->id,
                'price' => $item->price,
                'product_id' => $item->id,
                'quantity' => $item->qty
            ];
            $order = Order::create($data);
        }


        $settings = $this->configMail();
        $user_email = $delivery['user_email'];
        $user_name = "";
        $url_mail = "";
        $getCart = $carts;

        $msg = "thanh cong";
        if (!empty($user_email)) {

            try {

                Mail::send(['html' => 'themeworkart::childs.mails.mail_oder'], compact('url_mail', 'getCart', 'user_name', 'user_email', 'total_price'), function ($message) use ($settings, $user_email, $user_name) {
                    $message->from($settings['mailgun_username'], $settings['mail_name']);
                    $message->to($user_email, $user_name);
                    $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] Xác nhận đơn hàng bạn đã đặt.');
                });
            } catch (\Exception $ex) {
                $msg .= '. Gửi mail hóa đơn cho khách hàng không thành công.' . $ex->getMessage();
            }

            return redirect()->route('order.pay_success');
        }
    }

    function configMail()
    {
        $settings = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();
        if (isset($settings['driver'])) {
            $username = $settings['driver'] == 'mailgun' ? @$settings['mailgun_username'] : @$settings['smtp_username'];
            $config =
                [
                    'mail.from' => [
                        'address' => $username,
                        'name' => @$settings['mail_name'],
                    ],
                    'mail.driver' => @$settings['driver'],
                ];

            if ($settings['driver'] == 'mailgun') {
                $config['services.mailgun'] =
                    [
                        'domain' => trim(@$settings['mailgun_domain']),
                        'secret' => trim(@$settings['mailgun_secret']),
                    ];
                $config['mail.port'] = @$settings['mailgun_port'];
                $config ['mail.username'] = @$settings['mailgun_username'];
            } else {
                $config['mail.port'] = @$settings['smtp_port'];
                $config['mail.password'] = @$settings['smtp_password'];
                $config['mail.encryption'] = @$settings['smtp_encryption'];
                $config['mail.host'] = @$settings['smtp_host'];
                $config['mail.username'] = @$settings['smtp_username'];
            }
//            $config['services.onesignal'] = [
//                'app_id' => '420af10d-5030-4f34-af19-68078fd6467c',
//                'rest_api_key' => 'MTY0MjA5NTktNjgwNS00NGM3LTg3YmYtNzcwMmRhZDUyZmE2'
//            ];
//            config onesignal
//            dd($config);
            config($config);
        }
        return $settings;
    }

//    public function createBill(Request $request)
//    {
//
//        $settings = $this->configMail();
//        dd($request->all());
//        $getCart = session()->get('cart');
//        $total_price = 0;
//        $msg = 'Đặt hàng thành công! Chúng tôi sẽ sớm liên hệ cho bạn để giao hàng';
//        if (!empty($getCart)) {
//            $tel = $request->Phone;
//            $user_email = $request->Email;
//            $user = User::where('tel', base64_encode($tel))->orWhere('email', base64_encode($user_email))->first();
//            if (empty($user)) {
//                $user = User::create([
//                    'name' => $request->Name,
//                    'tel' => base64_encode($request->Phone),
//                    'email' =>base64_encode($request->Email)
//                ]);
//            }
//
//            foreach ($getCart as $value) {
//                $total_price += $value['quantity'] * $value['price'];
//            }
//            $gift_list='';
//            foreach ($getCart as $v) {
//                $gift_list .= '|' . str_replace(',','|',$v['gift']);
//            }
//            $note='';
//            $price_giftsss=0;
//            if($request->price_giftsss){
//                $price_giftsss = $request->price_giftsss;
//                $note .= 'Cộng thêm tiền bù khi chọn quà: '.number_format($request->price_giftsss,0,'','.').'đ';
//            }
//            $bill = Bill::create([
//                'user_id' => $user->id,
//                'user_name' => $request->Name,
//                'user_email' => base64_encode($request->Email),
//                'user_tel' => base64_encode($request->Phone),
//                'note' => $request->Note .$note,
//                'total_price' => $total_price + $price_giftsss,
//                'user_address' => $request->Address,
//                'receipt_method' => $request->PayType,
//                'user_gender' => $request->Sex,
//                'user_city_id' => $request->user_city_id,
//                'gift_list' => $gift_list,
//                'user_district_id' => $request->user_district_id
//
//            ]);
//
//            $bill_id = $bill->id;
//
//            foreach ($getCart as $item) {
//                $order = Order::create([
//                    'bill_id' => $bill_id,
//                    'price' => $item['quantity'] * $item['price'],
//                    'product_id' => $item['id'],
//                    'quantity' => $item['quantity'],
//                    'product_name' => $item['name'],
//                    'product_price' => !empty($item['price'])?$item['price']:Null,
//                    'product_image' => $item['image'],
//                    'gift_list' => '|' . str_replace(',','|',$item['gift'])
//                ]);
//            }
//            $admin_email = Settings::where('name', 'email_notifi')->first();
//            $user_name = $request->Name;
//            $user_email = base64_decode($user->email);
//            $user_tel = base64_decode($user->tel);
//            $receipt_method = $bill->receipt_method;
//
//            $url_mail = route('send.mail');
//
//
//            if (!empty($settings['mailgun_username']) || filter_var($settings['mailgun_username'], FILTER_VALIDATE_EMAIL)) {
//
//                try {
//                    $admin_emails = explode(',',$settings['admin_emails']);
//                    foreach ($admin_emails as $admin_email) {
//                        Mail::send(['html' => 'themeworkart::childs.mails.mail_admin'], compact('url_mail', 'getCart', 'user_name', 'user_email','user_tel','receipt_method', 'total_price', 'request'), function ($message) use ($settings, $admin_email) {
//                            $message->from($settings['smtp_username'], @$settings['mail_name']);
//                            $message->to($admin_email, 'Admin');
//                            $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] Đơn hàng mới');
//                        });
//                    }
//                } catch (\Exception $ex) {
//                    $msg .= '. Gửi mail cho admin không thành công.' . $ex->getMessage();
//                }
//            }
//            if (!empty($user_email) || filter_var($url_mail, FILTER_VALIDATE_EMAIL)) {
//
//                try {
//
//                    Mail::send(['html' => 'themeworkart::childs.mails.mail_oder'], compact('url_mail', 'getCart', 'user_name', 'user_email', 'total_price'), function ($message) use ($settings,$user_email, $user_name) {
//                        $message->from($settings['mailgun_username'], $settings['mail_name']);
//                        $message->to($user_email, $user_name);
//                        $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] Xác nhận đơn hàng bạn đã đặt.');
//                    });
//                } catch (\Exception $ex) {
//                    $msg .= '. Gửi mail hóa đơn cho khách hàng không thành công.' . $ex->getMessage();
//                }
//            }
//            Session::forget('cart');
//        }
//        return redirect()->back()->with('msg', $msg);
//    }


    public function pay_success()
    {
        if (Session::get('cart') == null) {
            $data['products'] = [];
        } else {
            $carts = Cart::content();
            $product_id_arr = [];
            foreach($carts as $k => $cart){
                $product_id_arr[] = $cart->id;
            }

            $data['products'] = Product::select(['id', 'name', 'slug', 'multi_cat', 'image', 'base_price', 'final_price'])
                ->where('status', 1)->whereIn('id', $product_id_arr)->get();
        }
        $pageOption = [
            'type' => 'page',
            'pageName' => 'Thanh toán thành công',
            'parentName' => '',
            'parentUrl' => '/',
        ];
        view()->share('pageOption', $pageOption);



        return view('themeworkart::childs.order.pay_success')->with($data);
    }

    public function track( Request $request ){
        return view( 'themeworkart::childs.order.track_order' );
    }
}
