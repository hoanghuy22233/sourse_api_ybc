<?php

namespace Modules\ThemeWorkart\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'first_name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:4',
//            'phone' => 'required|unique:users,phone',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return [
            'name' => 'Tên',
            'first_name' => 'Họ',
            'email' => 'Email',
            'password' => 'Mật khẩu',
//            'phone' => 'Số điện thoại',
        ];
    }
}
