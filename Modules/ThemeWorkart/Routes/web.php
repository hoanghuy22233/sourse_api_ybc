<?php
Route::get('sitemap.xml', function () {
    return response()->view('themeworkart::sitemap.sitemap')->header('Content-Type', 'text/xml');
});
Route::get('post-sitemap.xml', function () {
    return response()->view('themeworkart::sitemap.post_sitemap')->header('Content-Type', 'text/xml');
});
Route::get('category-sitemap.xml', function () {
    return response()->view('themeworkart::sitemap.category_sitemap')->header('Content-Type', 'text/xml');
});
Route::get('product-sitemap.xml', function () {
    return response()->view('themeworkart::sitemap.product_sitemap')->header('Content-Type', 'text/xml');
});



Route::group(['prefix' => '', 'middleware' => 'no_auth:student'], function () {
    Route::get('dang-nhap', 'Frontend\AuthController@getLogin')->name('dang-nhap');
    Route::post('dang-nhap', 'Frontend\AuthController@authenticate');
    Route::get('dang-ky', 'Frontend\AuthController@getRegister')->name('dang-ky');
    Route::post('dang-ky', 'Frontend\AuthController@postRegister');
});

Route::match(array('GET', 'POST'), 'quen-mat-khau', 'Frontend\AuthController@getEmailForgotPassword');
Route::match(array('GET', 'POST'), 'forgot-password/{change_password}', 'Frontend\AuthController@getForgotPassword');
Route::get('dang-xuat', function () {
    \Auth::guard('student')->logout();
    return redirect('/');
});


Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions', 'locale']], function () {
    Route::group(['prefix' => 'theme'], function () {
        Route::match(['get', 'post'], 'setting', 'Admin\STBDThemeController@setting')->name('theme')->middleware('permission:theme');
    });

    //  Showroom
    Route::group(['prefix' => 'showroom'], function () {
        Route::get('', 'Admin\ShowroomController@getIndex')->name('showroom')->middleware('permission:setting');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ShowroomController@add')->middleware('permission:setting');
        Route::get('delete/{id}', 'Admin\ShowroomController@delete')->middleware('permission:setting');
        Route::post('multi-delete', 'Admin\ShowroomController@multiDelete')->middleware('permission:setting');
        Route::get('{id}', 'Admin\ShowroomController@update')->middleware('permission:setting');
        Route::post('{id}', 'Admin\ShowroomController@update')->middleware('permission:setting');
    });
});

//  Xóa cache

//user can view it anytime with or without logged in
Route::group(['middleware' => ['locale']], function () {
    Route::get('signup', 'LoginController@signup');

});
Route::get('admin/browser', 'CkFinderController@browser')->name('browser');
Route::post('set_session', 'HomeController@set_session');

Route::get('trung-sp', 'Frontend\HomeController@trungSP');


Route::post('contact', 'Frontend\HomeController@postContact');

Route::post('mua-tra-gop', 'Frontend\ProductController@postMuaTraGop');

Route::get('load-san-pham-html', 'Frontend\PostController@loadSanPhamHtml');
Route::get('load-san-pham-trong-san-pham-html', 'Frontend\ProductController@loadSanPhamTrongSanPhamHtml');
Route::get('/admin/register', function () {
    return redirect('/admin/login');
});

//  Ajax routing
Route::get('ajax-get-district', 'Frontend\HomeController@ajax_get_district');
Route::get('ajax-get-product', 'Frontend\ProductController@ajax_get_product')->name('product.ajax_get_product');
Route::get('ajax-quick-view', 'Frontend\ProductController@ajax_quick_view')->name('product.ajax_quick_view');
Route::post('ajax-contact', 'Frontend\ContactController@ajax_contact')->name('contact.ajax_contact');
Route::get('order.refresh-cart-quick-view', 'Frontend\OrderController@refresh_cart_quick_view')->name('order.refresh_cart_quick_view');

Route::group(['middleware' => ['no_auth:users', 'locale']], function () {
    Route::post('register', 'Frontend\UserController@postRegister')->name('user.postRegister');
    Route::post('login', 'Frontend\UserController@postLogin')->name('user.postLogin');
});
//Route::get('login', 'Frontend\UserController@getLogin')->name('user.login');


Route::get('logout', 'Frontend\UserController@logout')->name('user.logout');
Route::get('forgot', 'Frontend\UserController@postDangKy')->name('user.forgot_password');
#
//Route::get('add-to-cart', 'Frontend\OrderController@addToCart')->name('order.add_to_cart');
//Route::get('delete-from-cart', 'Frontend\OrderController@deleteFromCart')->name('order.delete_from_cart');
//Route::get('update-cart', 'Frontend\OrderController@updateCart')->name('order.update_cart');
#
Route::group(['prefix' => 'gio-hang'],function () {
    Route::get('', 'Frontend\OrderController@getCart')->name('cart.index');
    Route::post('', 'Frontend\CartController@postCart');
    Route::post('them-vao-gio-hang/{id}', 'Frontend\OrderController@addCart')->name('cart.add');
    Route::post('them-vao-gio-hang-up-sale', 'Frontend\OrderController@addCartUpSale')->name('cart.add_up_sale');
    Route::get('del_item/{rowId}','Frontend\OrderController@deleteCart')->name('cart.delete');
    Route::get('del_all_item','Frontend\CartController@delAllItemCart');
    Route::post('update_cart/{rowId}','Frontend\OrderController@updateCart')->name('cart.update');
    Route::get('thank-you', 'Frontend\CartController@getComplete')->name('cart.complete');
    Route::post('ajax-add-cart', 'Frontend\CartController@ajaxAddCart')->name('cart.ajaxAddCart');
});

#

Route::get('seach-post', 'Frontend\PostController@getSearchPost')->name('search_post');     // Tim kiem
Route::get('tag/{tag}', 'Frontend\CategoryController@getSearch');
Route::get('seed/{action}', 'Frontend\SeedController@getIndex');

Route::get( '/track-order' , 'Frontend\OrderController@track' );
Route::post( '/track-order' , 'Frontend\OrderController@track' );

Route::group( ['prefix' => 'my-account' ] , function(){

    Route::get('/' , 'Frontend\UserController@index');

    Route::get('/register' , 'Frontend\UserController@getRegister');
    Route::post('/register' , 'Frontend\UserController@postRegister');

    Route::get('profile'  , 'Frontend\UserController@getProfile' );
    Route::post('profile'  , 'Frontend\UserController@postProfile' );

    Route::get('/order/{orderId}' , function(){
       return view( 'themeworkart::childs.user.order_detail' );
    });
} );



// Tim kiem
//Route::get('tin-tuc/', 'Frontend\PostController@getIndex');

Route::get('tag_post/{slug}', 'Frontend\PostController@getIndexTags');
Route::get('/tags/{slug}', 'Frontend\CategoryController@getIndex');  //  Danh sach san pham
Route::get('/category/{slug}', 'Frontend\CategoryPostController@getIndex');  //  Danh sach tin tuc


Route::get('giao-hang', 'Frontend\OrderController@getDelivery')->name('order.getDelivery');
Route::post('giao-hang', 'Frontend\OrderController@postDelivery')->name('order.postDelivery');
Route::get('phuong-thuc-thanh-toan', 'Frontend\OrderController@getPay')->name('order.pay');
Route::post('phuong-thuc-thanh-toan', 'Frontend\OrderController@createBill')->name('order.createBill');
Route::post('thanh-toan', 'Frontend\OrderController@getBill')->name('order.getBill');
Route::get('thanh-toan', 'Frontend\OrderController@redirectBill')->name('order.redirectBill');
Route::post('tao-don-hang', 'Frontend\OrderController@postBill')->name('order.postBill');
Route::get('thanh-toan-thanh-cong', 'Frontend\OrderController@pay_success')->name('order.pay_success');
Route::get('add-promote-code', 'Frontend\OrderController@addPromote')->name('order.addPromote');
Route::post('add-review', 'Frontend\ProductController@addReview')->name('addReview');
Route::get('all-review', 'Frontend\ProductController@allReview')->name('allReview');

Route::group(['middleware' => Modules\ThemeWorkart\Http\Middleware\CheckMaintan::class], function () {
    Route::post('/create-bill', 'Frontend\OrderController@createBill')->name('order.create_bill'); // add đơn hàng vào database
    Route::get('/remove-to-cart', 'Frontend\OrderController@getRemoveCart')->name('remove.cart'); // xoa san pham gio hang
    Route::get('/cart', 'Frontend\OrderController@getIndex')->name('order.view'); // xem gio hang

    Route::post('/send/contact', 'Frontend\ContactController@sendContact')->name('send.contact'); // post phone contact chi tiet san pham
    Route::post('/action/phone', 'Frontend\ContactController@addPhoneContact')->name('resign.contact'); // post phone contact chi tiet san pham
    Route::post('/ajax/contacts', 'Frontend\ContactController@contactsAjax')->name('contacts.form'); // form trong chi tiết sp;
    Route::post('/ajax/province_contact_post', 'Frontend\ContactController@provinceContactPost'); // post form contact trang chu !;
    Route::get('/search', 'Frontend\CategoryController@getSearch')->name('search'); // search  ajax

    Route::post('/send-mail', 'Frontend\HomeController@getAllManufacturer')->name('send.mail');

    Route::get('/khuyen-mai', 'Frontend\HomeController@getSaleList')->name('sale.list');
    Route::get('/tu-van', 'Frontend\HomeController@advisory')->name('advisory.list');;
    Route::get('/show-room/{slug}', 'Frontend\ShowRoomController@getShowRoom')->name('showroom.list');


    //  Cấu hình đường dẫn tới trang thương hiệu
    $thuong_hieu_slug = @\Modules\ThemeWorkart\Models\Settings::where('name', 'thuong_hieu_slug')->first()->value;
    Route::get($thuong_hieu_slug, 'Frontend\HomeController@getAllManufacturer')->name('manufacturer');
    Route::get('/' . $thuong_hieu_slug . '/{slug}', 'Frontend\HomeController@getManufacturer')->name('manufacturer.detail');


    Route::get('/{slug}/{productSlug}', 'Frontend\ProductController@detailTwo')->name('two-slug');    // Chi tiet san pham hoac chi tiet bai viet / danh mục tin tức
    Route::get('/{slug}/{slugTwo}/{productSlug}', 'Frontend\ProductController@detailThree');    // Chi tiet san pham hoac chi tiet bai viet
    Route::get('/{slug}/{slugTwo}/{slugThree}/{productSlug}', 'Frontend\ProductController@detailFor');    // Chi tiet san pham hoac chi tiet bai viet
    Route::get('/{slug}', 'Frontend\CategoryController@getIndex')->name('cate.list');  //  Danh sach danh muc


    //workart
    //happy customer
    Route::post('/review/' , 'Frontend\CustomerReviewController@review');
    Route::get('/happy-customer' , 'Frontend\CustomerReviewController@happyCustomer');

});

//route frontend
Route::get('/', 'Frontend\HomeController@getIndex')->name('home');    // Trang chu !
Route::get( '/faqs' , 'Frontend\HomeController@FAQController@index' );