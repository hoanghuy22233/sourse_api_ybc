<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {

    //  booking
    Route::group(['prefix' => 'products'/*, 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class*/], function () {
        Route::get('', 'Admin\ProductController@index');
        Route::post('', 'Admin\ProductController@store');
        Route::get('{id}', 'Admin\ProductController@show');
        Route::post('{id}', 'Admin\ProductController@update');
        Route::delete('{id}', 'Admin\ProductController@delete');
    });
});
