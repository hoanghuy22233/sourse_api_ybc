<!DOCTYPE html>
<html lang="vi">
<head>
    @include('themecard2::partials.head_meta')
    @include('themecard2::partials.head_script_home')
    @yield('head_script')
</head>

<body>

@yield('main_content')
<!-- #wrapper -->
@include('themecard2::partials.footer_script_home')
@yield('footer_script')

</body>
</html>
