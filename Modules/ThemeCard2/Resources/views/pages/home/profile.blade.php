@extends('themecard2::layouts.default')
@section('main_content')

    <style>
        .paginate > ul.nav-pagination li a, .paginate > ul.nav-pagination li.active span {
            padding: 5px 10px;
            border: 1px solid #ccc;
            margin: 0 5px;
            cursor: pointer;
        }

        .tm-block {
            padding: 50px 15px;
        }
        .table-data {
            max-height: 500px;
        }
    </style>
    <div class="container">
    @include('themecard2::template.menu')
    <?php
        $bills = \Modules\ThemeCard2\Models\Bill::where('user_id', @\Auth::user()->id)->orderby('created_at', 'DESC')->orderBy('id', 'desc')->paginate(7, ['*'], 'bills');
        $history_moneys = \Modules\ThemeCard2\Models\RechargeHistorys::where('user_id', @\Auth::user()->id)->orderby('created_at', 'DESC')->orderBy('id', 'desc')->paginate(7, ['*'], 'history_moneys');
    ?>
    <!-- row -->
        @if(empty(\Auth::user()->password))
            <div class="p-3 mb-2 bg-danger text-white" style="margin-top: 20px;text-align: center;"><i>Bạn chưa đặt mật
                    khẩu. Vui lòng click <a href="/profile/edit">vào đây</a> để đặt mật khẩu để lần tới đăng nhập không
                    phải
                    gửi mã về điện thoại</i></div>
        @endif
        <div class="row tm-content-row tm-mt-big">
            {{--<div class="mt-8 tm-col tm-col-big info-left">--}}
                {{--<div class="bg-white tm-block table-data">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-12">--}}
                            {{--<h2 class="tm-block-title d-inline-block">Lịch sử mua hàng</h2>--}}
                            {{--<hr>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="table-container">--}}
                        {{--<table class="table table-striped">--}}
                            {{--<tr class=" tm-list-group-alternate-color tm-list-group-pad-big">--}}
                                {{--<th style="width: 10%;" class="">--}}
                                    {{--STT--}}
                                {{--</th>--}}
                                {{--<th style="width: 30%;" class="">--}}
                                    {{--Thời gian mua--}}
                                {{--</th>--}}
                                {{--<th style="width: 40%;" class="">--}}
                                    {{--Đơn hàng--}}
                                {{--</th>--}}
                                {{--<th style="width: 30%;" class="">--}}
                                    {{--Giá--}}
                                {{--</th>--}}
                            {{--</tr>--}}
                                {{--@foreach($bills as $k=> $bill)--}}
                                    {{--<tr class=" tm-list-group-alternate-color tm-list-group-pad-big">--}}
                                        {{--<th class="">--}}
                                            {{--{{$k+=1}}--}}
                                        {{--</th>--}}
                                        {{--<th class="">--}}
                                            {{--{{date('d/m/Y - H:i:s',strtotime(@$bill->created_at))}}--}}
                                        {{--</th>--}}
                                        {{--<th class="">--}}
                                            {{--{!! @$bill->note !!}--}}
                                        {{--</th>--}}
                                        {{--<th class="">--}}
                                            {{--{{ number_format(@$bill->total_price,0,'',',') }}đ--}}
                                        {{--</th>--}}
                                    {{--</tr>--}}
                                {{--@endforeach--}}
                        {{--</table>--}}
                    {{--</div>--}}
                    {{--{{ $bills->appends(Request::all())->links() }}--}}
                {{--</div>--}}
                {{--<div class="bg-white tm-block table-data">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-12">--}}
                            {{--<h2 class="tm-block-title">Lịch sử nạp tiền</h2>--}}
                            {{--<hr>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="table-container">--}}
                        {{--<table class="table table-striped">--}}
                            {{--<tr class=" tm-list-group-alternate-color tm-list-group-pad-big">--}}
                                {{--<th style="width: 10%;" class="">--}}
                                    {{--STT--}}
                                {{--</th>--}}
                                {{--<th style="width: 50%;" class="">--}}
                                    {{--Thời gian nạp--}}
                                {{--</th>--}}
                                {{--<th style="width: 50%;" class="">--}}
                                    {{--Số tiền nạp--}}
                                {{--</th>--}}
                            {{--</tr>--}}
                                {{--@foreach($history_moneys as $k=>$history_money)--}}
                                    {{--<tr class=" tm-list-group-alternate-color tm-list-group-pad-big">--}}
                                        {{--<th class="">--}}
                                            {{--{{$k+=1}}--}}
                                        {{--</th>--}}
                                        {{--<th class="">--}}
                                            {{--{{date('d/m/Y - H:i:s',strtotime(@$history_money->created_at))}}--}}
                                        {{--</th>--}}
                                        {{--<th class="">--}}
                                            {{--{{ number_format(@$history_money->money,0,'',',') }}đ--}}
                                        {{--</th>--}}
                                    {{--</tr>--}}
                                {{--@endforeach--}}
                        {{--</table>--}}
                    {{--</div>--}}
                    {{--{{ $history_moneys->appends(Request::all())->links() }}--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="mt-4 tm-col tm-col-small info-right">
                <div class="bg-white tm-block">
                    <h2 class="tm-block-title">Thông tin cá nhân</h2>
                    <img src="{{@\App\Http\Helpers\CommonHelper::getUrlImageThumb(@\Auth::user()->image,230,230)}}"
                         alt="Profile Image" class="img-fluid">
                    <h4 class="tm-block-title_item"><i class="fa fa-user" style="color: deepskyblue"></i>
                         {{@\Auth::user()->name}}</h4>
                    <hr>
                    <h4 class="tm-block-title_item"><i class="fa fa-mobile-alt" style="color: deepskyblue"></i>
                         {{@\Auth::user()->tel}}</h4>
                    <hr>
                    <h4 class="tm-block-title_item"><i class="fa fa-mail-bulk" style="color: deepskyblue"></i>
                         {{@\Auth::user()->email}}</h4>
                    <hr>
                    @if(@\Auth::user()->gender==0)
                        <h4 class="tm-block-title_item"><i class="fa fa-transgender" style="color: deepskyblue"></i>
                             Nam</h4>
                        <hr>
                    @else
                        <h4 class="tm-block-title_item"><i class="fa fa-transgender" style="color: deepskyblue"></i>
                             Nữ</h4>
                        <hr>
                    @endif
                    <?php
                    $address_users = \Modules\CardUser\Models\UserAddress::where('user_id', \Auth::user()->id)->get();
                    ?>
                    <i class="fa fa-location-arrow" style="color: deepskyblue"></i>
                    <ul>
                        @foreach($address_users as $k=>$address_user)
                            <li>{{@$k+=1}}: {{@$address_user->address}}</li>
                        @endforeach
                    </ul>
                    <h4 style="color: red;" class="tm-block-title_item"><b><i class="fa fa-money-bill"></i> {{ number_format(@\Auth::user()->total_money,0,'',',') }}£</b></h4>
                    <hr>
                    <div class="custom-file mt-3 mb-3">
                        <a class="btn btn-primary d-block mx-xl-auto" href="/profile/edit">Chỉnh sửa profile</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection