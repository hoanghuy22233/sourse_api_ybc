@extends('themecard2::layouts.default')
@section('main_content')
    <div class="container">
    @include('themecard2::template.menu')
    <!-- row -->
        <div class="row tm-content-row tm-mt-big">
            <div class="tm-col tm-col-big">
                <div class="bg-white tm-block">
                    @if(Session::has('success'))
                        <p style="top: -15px;" class="alert alert-success">{!! Session::get('success') !!}</p>
                    @endif
                    @if(Session::has('error'))
                        <p style="top: -15px;" class="alert alert-danger">{!! Session::get('error') !!}</p>
                    @endif
                    <div class="row">
                        <div class="col-12">
                            <h2 class="tm-block-title">Bảo mật</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <form action="" method="post" class="tm-signup-form" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="phone">Số điện thoại</label>
                                    <input placeholder="010-030-0440" id="phone" name="phone" type="tel"
                                           class="form-control validate" value="{{@\Auth::user()->tel}}">
                                </div>
                                <br>
                                <div class="form-group">
                                    @include('themecard2::pages.home.address')
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label for="password">Xác nhận mật khẩu</label>
                                    <input placeholder="******" id="password" name="password" type="password"
                                           class="form-control validate required" required>
                                    @if ($errors->has('password'))
                                        <p class="text-danger"
                                           style=" padding-left: 10px;">{{$errors->first('password')}}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password">Nhập lại password</label>
                                    <input placeholder="******" id="re-password" name="re_password" type="password"
                                           class="form-control validate required" required>
                                    @if ($errors->has('re_password'))
                                        <p class="text-danger"
                                           style=" padding-left: 10px;">{{$errors->first('re_password')}}</p>
                                    @endif
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6 col-sm-4">
                                        <a href="/profile" type="submit" class="btn btn-primary">Quay lại
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-sm-4">
                                        <button style="margin-left: 72%;" type="submit" class="btn btn-success">Cập nhật
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
