<?php

namespace Modules\ThemeCard2\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bill extends Model
{
    use SoftDeletes;

    protected $table = 'bills';

    protected $guarded = [];

    protected $dates = ['deleted_at'];
    protected $softDelete = true;
//    protected $fillable = [
//        'receipt_method' , 'user_gender', 'date' , 'coupon_code' , 'note' , 'status' , 'total_price' , 'user_id', 'user_tel', 'user_name', 'user_email', 'user_address', 'user_wards', 'user_city_id'
//    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
