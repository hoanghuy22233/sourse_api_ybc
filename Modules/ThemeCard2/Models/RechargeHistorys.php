<?php

namespace Modules\ThemeCard2\Models ;

use Illuminate\Database\Eloquent\Model;

class RechargeHistorys extends Model
{
    protected $table = 'recharge_historys';
    protected $guarded = [];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function card() {
        return $this->belongsTo(Card::class, 'card_id');
    }
}
