<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'Frontend\HomeController@getIndex');

Route::get('dang-xuat', function () {
    \Auth::logout();
    return redirect('/');
});

Route::get('enter-pass', 'Frontend\HomeController@getEnterPass');
Route::post('enter-pass', 'Frontend\HomeController@postEnterPass');

Route::group(['prefix' => 'profile', 'middleware' => ['guest:users']], function () {
    Route::get('', 'Frontend\HomeController@getProfile');

    Route::get('edit', 'Frontend\HomeController@getEditProfile');
    Route::post('edit', 'Frontend\HomeController@postEditProfile');

    Route::get('security', 'Frontend\HomeController@getSecurity');
    Route::post('security', 'Frontend\HomeController@postSecurity');
});

