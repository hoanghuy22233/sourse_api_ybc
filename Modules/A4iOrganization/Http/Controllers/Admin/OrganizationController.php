<?php

namespace Modules\A4iOrganization\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Validator;
use DB;


class OrganizationController extends CURDBaseController
{

    protected $orderByRaw = 'status ASC, updated_at desc';

    protected $module = [
        'code' => 'organization',
        'table_name' => 'organizations',
        'label' => 'Tổ chức',
        'modal' => '\Modules\A4iOrganization\Models\Organization',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Logo'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'tel', 'type' => 'text', 'label' => 'Điện thoại'],
            ['name' => 'address', 'type' => 'custom', 'td' => 'a4iorganization::td.address', 'label' => 'Đại chỉ'],
            ['name' => 'status', 'type' => 'custom', 'td' => 'a4iorganization::td.status', 'label' => 'Trạng thái'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'require', 'label' => 'Tên'],
                ['name' => 'email', 'type' => 'text', 'class' => 'require', 'label' => 'Email'],
                ['name' => 'tel', 'type' => 'text', 'class' => '', 'label' => 'Số điện thoại'],
                ['name' => 'image', 'type' => 'file_image', 'class' => '', 'label' => 'Logo'],
                ['name' => 'status', 'type' => 'checkbox', 'class' => '', 'label' => 'Trạng thái'],
            ],
            'info_tab' => [
                ['name' => 'address', 'type' => 'text', 'class' => '', 'label' => 'Địa chỉ'],
                ['name' => 'province_id', 'type' => 'select2_ajax_model', 'class' => '', 'label' => 'Tỉnh/Thành','object' => 'province', 'model' => \Modules\A4iLocation\Models\Province::class, 'display_field' => 'name',],
                ['name' => 'district_id', 'type' => 'select2_ajax_model', 'class' => '', 'label' => 'Quận/Huyện' ,'object' => 'district', 'model' => \Modules\A4iLocation\Models\District::class, 'display_field' => 'name',],
                ['name' => 'ward_id', 'type' => 'select2_ajax_model', 'class' => '', 'label' => 'Phường/Xã' ,'object' => 'ward', 'model' => \Modules\A4iLocation\Models\Ward::class, 'display_field' => 'name',],
            ],

        ],

    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'tel' => [
            'label' => 'Số điện thoại',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'province_id' => [
            'label' => 'Tỉnh/Thành',
            'type' => 'select2_ajax_model',
            'model' => \Modules\A4iLocation\Models\Province::class,
            'display_field' => 'name',
            'object' => 'province',
            'query_type' => '='
        ],
        'district_id' => [
            'label' => 'Quận/Huyện',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'district',
            'model' => \Modules\A4iLocation\Models\District::class,
            'query_type' => '='
        ],
        'ward_id' => [
            'label' => 'Phường/Xã',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'model' => \Modules\A4iLocation\Models\Ward::class,
            'object' => 'ward',
            'query_type' => '='
        ],

    ];
    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('a4iorganization::list')->with($data);
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('a4iorganization::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

//                    $data['admin_id'] = \Auth::guard('admin')->user()->id;

                    DB::beginTransaction();
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {


//                            luu anh vao he thong



                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            DB::rollback();
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('a4iorganization::edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên gắn gọn',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia


            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia


            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


//

}
