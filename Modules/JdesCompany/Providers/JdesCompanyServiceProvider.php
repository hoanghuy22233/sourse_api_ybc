<?php

namespace Modules\JdesCompany\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class JdesCompanyServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Custom setting
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();

            //  Hiển thị logo công ty
            $this->rendAsideLogo();

            //  Show danh sách các công ty đang tham gia ở popup profile
            $this->rendListCompany();

            \Eventy::addAction('inviteCompany.renew', function ($item) {
                $mailController = new MailController();
                $mailController->reInviteSendMail($item);
                return true;
            }, 1, 1);
            \Eventy::addAction('inviteCompany.add', function ($model) {
                $mailController = new MailController();
                $mailController->inviteCompanySendMail($model);
                return true;
            }, 1, 1);
            \Eventy::addAction('admin.deactiveFromCompany', function ($company_id) {
                $mailController = new MailController();
                $mailController->deActiveAdminFromCompanySendMail($company_id);
                return true;
            }, 1, 1);
        }
    }




    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['company_view', 'company_add', 'company_edit', 'company_delete',]);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('jdescompany::partials.aside_menu.aside_menu_dashboard_after');
        }, 3, 1);
    }

    public function rendAsideLogo() {
        \Eventy::addFilter('aside.logo', function() {
            if (!isset($company_choosing)) {
                $company_choosing = \Modules\JdesCompany\Models\Company::find(\Auth::guard('admin')->user()->last_company_id);
                \View::share('company_choosing', $company_choosing);
            }
            print '<img alt="'.@$company_choosing->name.'"
                 src="'.@\App\Http\Helpers\CommonHelper::getUrlImageThumb($company_choosing->image,100,null).'"/>';
        }, 1, 1);
    }

    public function rendListCompany()
    {
        \Eventy::addFilter('user_bar.profile_after', function () {
            print view('jdescompany::partials.user_bar');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('jdescompany.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'jdescompany'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/jdescompany');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/jdescompany';
        }, \Config::get('view.paths')), [$sourcePath]), 'jdescompany');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/jdescompany');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'jdescompany');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'jdescompany');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
