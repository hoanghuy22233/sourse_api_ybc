<?php

namespace Modules\ThemeRaoVatAdmin\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\STBDProduct\Models\Product;
use Modules\ThemeRaoVatAdmin\Models\Category;
use Modules\ThemeRaoVatAdmin\Models\Post;
use Validator;

class PostController extends CURDBaseController
{
    protected $whereRaw = 'post_type in (1, 2)';

    protected $module = [
        'code' => 'post',
        'table_name' => 'posts',
        'label' => 'Bài viết',
        'modal' => '\Modules\ThemeRaoVatAdmin\Models\Post',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên bài viết'],
            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'themeraovatadmin::list.td.multi_cat', 'label' => 'Danh mục', 'object' => 'category_post'],
//            ['name' => 'tags', 'type' => 'custom', 'td' => 'themeraovatadmin::list.td.multi_cat', 'label' => 'Từ khóa', 'object' => 'tag_post'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trang thái'],
            ['name' => 'view_total', 'type' => 'number', 'label' => 'Luợt xem'],
            ['name' => 'view', 'type' => 'custom', 'td' => 'themeraovatadmin::list.td.view_post_fontend', 'label' => 'Xem'],
            ['name' => 'updated_at', 'type' => 'text', 'label' => 'Cập nhật']
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tiêu đề hiển thị'],
                ['name' => 'post_type', 'type' => 'select', 'label' => 'Thể loại', 'class' => 'required', 'options' => [
                    1 =>'Tìm nhà phân phối',
                    2 =>'Mở đại lý phân phối',
                ], 'group_class' => 'col-md-4'],
                ['name' => 'multi_cat', 'class' => 'required', 'type' => 'select2_model', 'field' => 'themeraovatadmin::form.fields.multi_cat', 'label' => 'Danh mục', 'model' => Category::class,
                    'object' => 'category_post', 'display_field' => 'name', 'where' => 'type=10 AND parent_id is null', 'group_class' => 'col-md-4'],
                ['name' => 'category_child_id', 'type' => 'custom', 'field' => 'raovatproduct::form.fields.category_child_id', 'label' => 'Ngành hàng', 'model' => Category::class, 'object' => 'category', 'display_field' => 'name', 'where' => 'type=10', 'group_class' => 'col-md-4'],
                ['name' => 'tags', 'type' => 'text', 'label' => 'Từ khóa bài viết', 'des' => 'Viết cách nhau bởi dấu phẩy. VD: từ khóa 1, từ khóa 2, từ khóa 3'],
                ['name' => 'intro', 'type' => 'custom', 'field' => 'themeraovatadmin::form.fields.count_char_intro', 'class' => '', 'label' => 'Mô tả ngắn (Tối đa 120 ký tự)'],
                ['name' => 'content', 'type' => 'textarea', 'label' => 'Nội dung đăng tin', 'inner' => 'rows=20'],
                ['name' => 'link_youtube', 'type' => 'text', 'class' => '', 'label' => 'Đường dẫn video youtube'],
                ['name' => 'acreage', 'type' => 'select', 'class' => '', 'label' => 'Mặt bằng','group_class' => 'col-md-6', 'options' => [
                    1 => 'Chưa xác định',
                    2 => 'Dưới 30 m2',
                    3 => '30 m2 - 50 m2',
                    4 => '50 m2 - 80 m2',
                    5 => '80 m2 - 120 m2',
                    6 => '120 m2 - 200 m2',
                    7 => '200 m2 - 250 m2',
                    8 => '250 m2 - 300 m2',
                    9 => '300 m2 - 500 m2',
                    10 => 'Trên 500 m2',
                ]],
                ['name' => 'price_range', 'type' => 'select', 'class' => '', 'label' => 'Tài chính','group_class' => 'col-md-6', 'options' => [
                    1 => 'Không yêu cầu',
                    2 => 'Dưới 100 Triệu',
                    3 => '100 triệu - 200 triệu',
                    4 => '200 triệu - 300 triệu',
                    5 => '300 triệu - 500 triệu',
                    6 => '500 triệu - 800 triệu',
                    7 => '800 triệu - 1 Tỷ',
                    8 => '1 Tỷ - 2 Tỷ',
                    9 => '2 tỷ - 3 tỷ',
                    10 => '3 tỷ - 5 tỷ',
                    11 => '5 tỷ - 7 tỷ',
                    12 => '7 tỷ - 10 tỷ',
                    13 => '10 tỷ - 15 tỷ',
                    14 => '15 tỷ - 20 tỷ',
                    15 => '20 tỷ - 30 tỷ',
                    16 => '30 tỷ - 50 tỷ',
                    17 => 'Trên 50 tỷ',
                ]],
                ['name' => 'province_id', 'type' => 'select_location', 'label' => 'Chọn địa điểm'],
                ['name' => 'address', 'type' => 'text', 'label' => 'Địa điểm cụ thể'],
                ['name' => 'supporting', 'type' => 'checkbox_multiple', 'label' => 'Chính sách hỗ trợ', 'options' => [
                    1 =>'Được phân phối độc quyền',
                    2 =>'Được hưởng chính sách ưu đãi khuyến mãi',
                    3 =>'Được ưu đãi chính sách về giá',
                    4 =>'Hỗ trợ dư nợ',
                    5 =>'Hỗ trợ marketing, hình ảnh, đào tạo',
                    6 =>'Hỗ trợ catalogue, sản phẩm mẫu',
                    7 =>'Khuyến mãi các dịp lễ, tết, sinh nhật',
                    8 =>'Chiết khấu cao trên mỗi mặt hàng',
                ]],
                ['name' => 'deadline', 'type' => 'datetimepicker', 'class' => '', 'label' => 'Ngày hết hạn','group_class' => 'col-md-6'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt', 'value' => 1, 'group_class' => 'col-md-3'],
            ],
            'image_tab' => [
                ['name' => 'image', 'type' => 'file_image', 'label' => 'Ảnh đại diện'],
                ['name' => 'image_extra', 'type' => 'multiple_image_dropzone', 'count' => '6', 'label' => 'Thêm nhiều ảnh khác'],
            ],
            'contact_tab' => [
                ['name' => 'inner', 'type' => 'inner', 'label' => '', 'html' => 'Nếu bạn không điền gì thì mặc định lấy thông tin tài khoản của bạn'],
                ['name' => 'contact_name', 'type' => 'text', 'label' => 'Tên người liên hệ', ],
                ['name' => 'contact_tel', 'type' => 'text', 'label' => 'Điện thoại liên hệ', ],
                ['name' => 'contact_zalo', 'type' => 'text', 'label' => 'Zalo liên hệ', ],
                ['name' => 'contact_address', 'type' => 'text', 'label' => 'Địa chỉ liên hệ', ],
            ],
            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'Slug', 'des' => 'Đường dẫn bài viết trên thanh địa chỉ'],
                ['name' => 'meta_title', 'type' => 'custom', 'field' => 'themeraovatadmin::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta title', 'max_char' => 1000],
                ['name' => 'meta_description', 'type' => 'custom', 'field' => 'themeraovatadmin::form.fields.count_char_intro', 'class' => '', 'label' => 'Meta description', 'max_char' => 1000],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
            ],
        ],
    ];

    protected $mat_bat = [
        1 => 'Chưa xác định',
        2 => 'Dưới 30 m2',
        3 => '30 m2 - 50 m2',
        4 => '50 m2 - 80 m2',
        5 => '80 m2 - 120 m2',
        6 => '120 m2 - 200 m2',
        7 => '200 m2 - 250 m2',
        8 => '250 m2 - 300 m2',
        9 => '300 m2 - 500 m2',
        10 => 'Trên 500 m2',
    ];

    protected $tai_chinh = [
        1 => 'Không yêu cầu',
        2 => 'Dưới 100 Triệu',
        3 => '100 triệu - 200 triệu',
        4 => '200 triệu - 300 triệu',
        5 => '300 triệu - 500 triệu',
        6 => '500 triệu - 800 triệu',
        7 => '800 triệu - 1 Tỷ',
        8 => '1 Tỷ - 2 Tỷ',
        9 => '2 tỷ - 3 tỷ',
        10 => '3 tỷ - 5 tỷ',
        11 => '5 tỷ - 7 tỷ',
        12 => '7 tỷ - 10 tỷ',
        13 => '10 tỷ - 15 tỷ',
        14 => '15 tỷ - 20 tỷ',
        15 => '20 tỷ - 30 tỷ',
        16 => '30 tỷ - 50 tỷ',
        17 => 'Trên 50 tỷ',
    ];

    protected $quick_search = [
        'label' => 'ID, tên',
        'fields' => 'id, name'
    ];

    protected $filter = [
        'multi_cat' => [
            'label' => 'Danh mục',
            'type' => 'select2_model',
            'model' => Category::class,
            'where' => 'type = 1',
            'query_type' => 'like'
        ],
        'tags' => [
            'label' => 'Tags',
            'type' => 'select2_model',
            'model' => Category::class,
            'where' => 'type = 2',
            'query_type' => 'like'
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Ẩn',
                1 => 'Duyệt'
            ]
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('themeraovatadmin::post.list')->with($data);
    }

    public function appendWhere($query, $request)
    {

        //  Lọc theo danh mục
        if (!is_null($request->get('category_id'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }
        if (!is_null($request->get('tags'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
        }

        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu mình tạo
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('admin_id', \Auth::guard('admin')->user()->id);
        }

        return $query;
    }

    public function add(Request $request)
    {
//        try {
            if (!$_POST) {

                $data = $this->getDataAdd($request);

                return view('themeraovatadmin::post.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert

                    if ($request->has('image_extra')) {
                        $data['image_extra'] = implode('|', $request->image_extra);
                    }

                    if ($request->has('supporting')) {
                        $data['supporting'] = '|' . implode('|', $request->supporting) . '|';
                    }

                    if ($request->has('acreage')) {
                        $data['mat_bang'] = @$this->mat_bat[$request->acreage];
                    }

                    if ($request->has('price_range')) {
                        $data['yeu_cau_tai_chinh'] = @$this->tai_chinh[$request->price_range];
                    }

                    $data['admin_id']= \Auth::guard('admin')->user()->id;
                    /*if ($request->has('product_sidebar')) {
                        $data['product_sidebar'] = '|' . implode('|', $request->product_sidebar) . '|';

                    }*/

                    $data['district_id'] = @$request->district_id;
                    $data['ward_id'] = @$request->ward_id;
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        if (!empty($request->related_posts)) {
                            foreach ($request->related_posts as $related_post) {
                                $pos = Post::find($related_post);
                                if (empty($pos->related_posts)) {

                                    $pos->related_posts = '|' . $this->model->id . '|';
                                } else {
                                    $pos->related_posts .= $this->model->id . '|';
                                }
                                $pos->save();
                            }
                        }
                        $this->afterAddLog($request, $this->model);
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tạo mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
//        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', $ex->getMessage());
//            return redirect()->back()->withInput();
//        }
    }

    public function update(Request $request)
    {
//        try {
            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('themeraovatadmin::post.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());


                    //  Tùy chỉnh dữ liệu insert


                    /*if ($request->has('product_sidebar')) {
                        $data['product_sidebar'] = '|' . implode('|', $request->product_sidebar) . '|';

                    }*/

                    if ($request->has('related_products')) {
                        $data['related_products'] = '|' . implode('|', $request->related_products) . '|';
                    }
                    if ($request->has('image_extra')) {
                        $data['image_extra'] = implode('|', $request->image_extra);
                    }
                    if ($request->has('related_posts')) {
                        $data['related_posts'] = '|' . implode('|', $request->related_posts) . '|';
                        foreach ($request->related_posts as $related_post) {

                            $pos = Post::find($related_post);
                            if (empty($pos->related_posts)) {
                                $pos->related_posts = '|' . $item->id . '|';
                            } else {
                                $pos->related_posts .= $item->id . '|';
                            }
                            $pos->save();
                        }
                    }
                    if ($request->has('supporting')) {
                        $data['supporting'] = '|' . implode('|', $request->supporting) . '|';
                    }

                    if ($request->has('acreage')) {
                        $data['mat_bang'] = @$this->mat_bat[$request->acreage];
                    }

                    if ($request->has('price_range')) {
                        $data['yeu_cau_tai_chinh'] = @$this->tai_chinh[$request->price_range];
                    }

                    #
                    $data['district_id'] = @$request->district_id;
                    $data['ward_id'] = @$request->ward_id;

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
//        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
////            CommonHelper::one_time_message('error', $ex->getMessage());
//            return redirect()->back()->withInput();
//        }
    }

    public function getPublish(Request $request)
    {
        try {

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }

            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
    public function enabledStatus(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {

                foreach ($ids as $post){

                    $post = $this->model->find($post);
                    $post->status = 1;
                    $post->save();
                }
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Đổi trang thái sang kích hoạt thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }
    public function disabledStatus(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                foreach ($ids as $post){
                    $post = $this->model->find($post);
                    $post->status = 0;
                    $post->save();
                }
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Đổi trạng thái sang hủy kích hoạt thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
