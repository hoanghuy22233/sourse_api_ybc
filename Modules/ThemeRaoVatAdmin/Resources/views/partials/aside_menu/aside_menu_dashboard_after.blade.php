@if(in_array('post_view', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon-edit-1"></i>
                    </span><span class="kt-menu__link-text">{{ trans('themeraovatadmin::admin.post') }}</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/post" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">{{ trans('themeraovatadmin::admin.all_post') }}</span></a>
                </li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/post/add" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">{{ trans('themeraovatadmin::admin.create_post') }}</span></a>
                </li>
                @if(in_array('category_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/tag_post" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Thẻ</span></a></li>
                    <li class="kt-menu__item " aria-haspopup="true">
                        <a href="/admin/category_post" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">{{ trans('themeraovatadmin::admin.category') }}</span></a>
                    </li>
                @endif
                @if(in_array('post_admin_view', $permissions))
                    <li class="kt-menu__item " aria-haspopup="true"><a
                                href="/admin/post_admin" class="kt-menu__link "><i
                                    class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                    class="kt-menu__link-text">Bài viết hệ thống</span></a>
                    </li>
                @endif
            </ul>
        </div>
    </li>
@endif
@if(in_array('theme', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                        <i
                                class="kt-menu__link-icon flaticon-imac"></i>
                    </span><span class="kt-menu__link-text">{{ trans('themeraovatadmin::admin.theme') }}</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true" title="Quản lý chủ đề"><a
                            href="/admin/theme?status=1" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">{{ trans('themeraovatadmin::admin.theme') }}</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/rank" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Huân chương</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/banner" class="kt-menu__link " title="Quản lý hình ảnh"><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">{{ trans('themeraovatadmin::admin.banner') }}</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/widget" class="kt-menu__link " title="Quản lý các khối chức năng trên web"><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">{{ trans('themeraovatadmin::admin.widget') }}</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/menu" class="kt-menu__link " title="Quản lý các menu"><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">{{ trans('themeraovatadmin::admin.menu') }}</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/theme/setting" class="kt-menu__link " title="Cấu hình giao diện"><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Cấu hình theme</span></a></li>
                {!! Eventy::filter('aside_menu.theme_menu_childs', '') !!}
            </ul>
        </div>
    </li>
@endif
@if(in_array('misson_view', $permissions))
    <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
<i
        class="kt-menu__link-icon flaticon-list"></i>
</span><span class="kt-menu__link-text">Nhiệm vụ</span><i
                    class="kt-menu__ver-arrow la la-angle-right"></i></a>
        <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                    class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/misson" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Tât cả nhiệm vụ</span></a></li>
                <li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/history-misson" class="kt-menu__link "><i
                                class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                class="kt-menu__link-text">Lịch sử làm nhiệm vụ</span></a></li>

            </ul>
        </div>
    </li>
@endif
<li class="kt-menu__item" aria-haspopup="true"><a href="/admin/rut_gon_link"
                                                  class="kt-menu__link "><span
                class="kt-menu__link-icon"><i class="flaticon-book"></i>
            </span><span class="kt-menu__link-text">Rút gọn link</span></a></li>
@if(in_array('contact_view', $permissions))
    <li class="kt-menu__item" aria-haspopup="true"><a href="/admin/contact"
                                                      class="kt-menu__link "><span
                    class="kt-menu__link-icon"><i class="flaticon-book"></i>
            </span><span class="kt-menu__link-text">{{ trans('themeraovatadmin::admin.contact') }}</span></a></li>
@endif