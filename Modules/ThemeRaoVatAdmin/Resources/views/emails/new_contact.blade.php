{!! @$settings['header_mail'] !!}
<tr>
    <td><strong>Thông tin khách hàng đăng ký trên website:</strong></td>
</tr>
<tr>
    <td>Họ & tên: {!! @$customer->name !!}</td>
</tr>
<tr>
    <td>Email: {!! @$customer->email !!}</td>
</tr>
<tr>
    <td>Số điện thoại: {!! @$customer->tel !!}</td>
</tr>
<tr>
    <td>Địa chỉ: {!! @$customer->address !!}</td>
</tr>
<tr>
    <td>Lời nhắn: {!! @$customer->note !!}</td>
</tr>
{!! @$settings['footer_mail'] !!}