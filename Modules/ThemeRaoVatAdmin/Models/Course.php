<?php
namespace Modules\ThemeRaoVatAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\JdesCategory\Models\CategoryProduct;

class Course extends Model
{

    protected $table = 'courses';
    public $timestamps = false;



    protected $fillable = [
        'name', 'image', 'intro','content'
    ];

    public function category()
{
    return $this->belongsTo(Bill::class, 'category_id');
}



}
