<?php
namespace Modules\ThemeRaoVatAdmin\Models;

use App\Models\District;
use App\Models\Province;
use App\Models\Ward;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\JdesCategory\Models\CategoryProduct;

class Post extends Model
{

    protected $table = 'posts';
    public $timestamps = false;

    protected $fillable = [
        'name', 'image', 'intro','content'
    ];

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function province(){
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function district(){
        return $this->belongsTo(District::class, 'district_id');
    }

    public function ward(){
        return $this->belongsTo(Ward::class, 'ward_id');
    }

}
