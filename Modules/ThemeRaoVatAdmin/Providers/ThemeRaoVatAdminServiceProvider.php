<?php

namespace Modules\ThemeRaoVatAdmin\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class ThemeRaoVatAdminServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            // Cấu hình Core
//            $this->registerTranslations();
//            $this->registerConfig();
//            $this->registerFactories();
//            $this->loadMigrationsFrom(module_path('ThemeRaoVatAdmin', 'Database/Migrations'));


            //  Custom setting

            //  Cấu hình menu trái
            $this->rendAsideMenu();

            //  Đăng ký quyền
            $this->registerPermission();
        }

        $this->registerViews();
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['theme',
                'contact_view', 'contact_add', 'contact_edit', 'contact_delete', 'contact_edit',
                'post_view', 'post_add', 'post_edit', 'post_delete', 'post_publish', 'category_view',
                'post_admin_view', 'post_admin_add', 'post_admin_edit', 'post_admin_delete', 'post_admin_publish',
                ]);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('themeraovatadmin::partials.aside_menu.aside_menu_dashboard_after');
        }, 0, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('ThemeRaoVatAdmin', 'Config/config.php') => config_path('themeraovatadmin.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('ThemeRaoVatAdmin', 'Config/config.php'), 'themeraovatadmin'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/themeraovatadmin');

        $sourcePath = module_path('ThemeRaoVatAdmin', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/themeraovatadmin';
        }, \Config::get('view.paths')), [$sourcePath]), 'themeraovatadmin');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/themeraovatadmin');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'themeraovatadmin');
        } else {
            $this->loadTranslationsFrom(module_path('ThemeRaoVatAdmin', 'Resources/lang'), 'themeraovatadmin');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('ThemeRaoVatAdmin', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
