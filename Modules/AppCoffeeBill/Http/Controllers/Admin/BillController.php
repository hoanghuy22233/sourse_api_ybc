<?php

namespace Modules\AppCoffeeBill\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Modules\AppCoffeeBill\Models\Area;
use Modules\AppCoffeeBill\Models\Bill;
use Modules\AppCoffeeBill\Models\Choose_table;
use Validator;

class BillController extends CURDBaseController
{

    protected $orderByRaw = 'status ASC';

    protected $module = [
        'code' => 'bill',
        'table_name' => 'bills',
        'label' => 'Đơn hàng',
        'modal' => '\Modules\AppCoffeeBill\Models\Bill',
        'list' => [
//            ['name' => 'user_name', 'type' => 'relation_edit', 'label' => 'Khách hàng'],
            ['name' => 'user_name', 'type' => 'relation', 'label' => 'Khách hàng', 'object' => 'user', 'display_field' => 'name'],
            ['name' => 'choose_table_id', 'type' => 'custom', 'td' => 'appcoffeebill::list.td.relation_area_choose_table', 'object' => 'choose_table', 'display_field' => 'position', 'label' => 'Bàn'],
            ['name' => 'area_id', 'type' => 'custom', 'td' => 'appcoffeebill::list.td.relation_area_choose_table', 'object' => 'area', 'display_field' => 'name', 'label' => 'Khu'],
//            ['name' => 'count_product', 'type' => 'custom', 'td' => 'appcoffeebill::list.td.count_product', 'label' => 'Tổng SP'],
            ['name' => 'total_price', 'type' => 'price_vi', 'label' => 'Tổng tiền'],
            ['name' => 'status', 'type' => 'custom', 'td' => 'appcoffeebill::list.td.select_status', 'label' => 'Trạng thái'],
            ['name' => 'updated_at', 'type' => 'custom', 'td' => 'appcoffeebill::list.td.date', 'label' => 'Ngày đặt'],
            ['name' => 'date', 'type' => 'custom', 'td' => 'appcoffeebill::list.td.date', 'label' => 'Thời gian nhận'],
            ['name' => 'show', 'type' => 'custom', 'td' => 'appcoffeebill::list.td.show', 'label' => 'Xem'],
            ['name' => 'edit_bill', 'type' => 'custom', 'td' => 'appcoffeebill::list.td.edit_bill', 'label' => 'Chỉnh sửa'],
//            ['name' => 'company_id', 'type' => 'relation', 'object' => 'company', 'display_field' => 'name', 'label' => 'C.ty',],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'user_name', 'type' => 'text', 'label' => 'Khách hàng'],
                ['name' => 'area_id', 'type' => 'select2_model', 'label' => 'Chọn khu vực', 'model' => Area::class, 'display_field' => 'name'],
                ['name' => 'choose_table_id', 'type' => 'select2_model', 'label' => 'Chọn bàn', 'model' => Choose_table::class, 'display_field' => 'position'],
                ['name' => 'total_price', 'type' => 'number', 'label' => 'Tổng giá'],
                ['name' => 'updated_at', 'type' => 'date', 'label' => 'Thời gian đặt hàng'],
                ['name' => 'date', 'type' => 'date', 'label' => 'Thời gian nhận hàng'],
                ['name' => 'status', 'type' => 'select', 'options' =>
                    [
                        0 => 'Chờ duyệt',
                        1 => 'Đang giao',
                        2 => 'Hoàn thành',
                        3 => 'Hủy',
                    ], 'class' => '', 'label' => 'Trạng thái'],
            ],

        ],
    ];

    protected $filter = [
        'user_name' => [
            'label' => 'Tên khách hàng',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'user_tel' => [
            'label' => 'SĐT',
            'type' => 'number',
            'query_type' => 'like'
        ],
        'user_email' => [
            'label' => 'Email',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'total_price' => [
            'label' => 'Tổng tiền',
            'type' => 'number',
            'query_type' => 'like'
        ],
        'updated_at' => [
            'label' => 'Ngày đặt',
            'type' => 'date',
            'query_type' => 'custom'
        ],
        'date' => [
            'label' => 'Ngày nhận',
            'type' => 'date',
            'query_type' => 'custom'
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Vị trí',
                0 => 'Mới tạo',
                1 => 'Chờ xưởng duyệt',
                2 => 'Đang làm',
                3 => 'Hoàn thành',
                4 => 'Hủy',
            ],
        ],

    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('appcoffeebill::list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        }

        return $query;
    }

    public function detailBillUser(Request $r)
    {
        $data['bill'] = Bill::find($r->bill_id);
        return view('appcoffeebill::popup_bill_user', $data);
    }

    public function detailBillTable(Request $r)
    {
        $bill = Bill::where('choose_table_id', $r->table)->where('area_id', $r->area)->whereIn('status', [0, 1])->first();
        if (!is_object($bill)) {
            die('');
        }
        return view('appcoffeebill::popup_bill_user', compact('bill'));
    }

    public function getRemoveProBill(Request $r)
    {
//        $data['pro'] = Bill::find($r->bill_u);
//        return view('appcoffeebill::popup_bill_user', $data);
    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('appcoffeebill::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'user_name' => 'required'
                ], [
                    'user_name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;
                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {


            $item = $this->model->find($request->id);


            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('appcoffeebill::edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'user_name' => 'required'
                ], [
                    'user_name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;


                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return redirect()->back()->withInput();
        }
    }


    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

//            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


    public function getIndexProcess(Request $request)
    {
        //  Filter
        $where = $this->filterSimple($request);
        $listItem = $this->model->whereRaw($where);
        if ($this->whereRaw) {
            $listItem = $listItem->whereRaw($this->whereRaw);
        }
        $listItem = $this->appendWhere($listItem, $request);

        $listItem = $listItem->whereIn('status', [0, 1])->orderBy('status', 'asc');

        //  Export
        if ($request->has('export')) {
            $this->exportExcel($request, $listItem->get());
        }

        //  Sort
        $listItem = $this->sort($request, $listItem);
        if ($request->has('limit')) {
            $data['listItem'] = $listItem->paginate($request->limit);
            $data['limit'] = $request->limit;
        } else {
            $data['listItem'] = $listItem->paginate($this->limit_default);
            $data['limit'] = $this->limit_default;
        }
        $data['page'] = $request->get('page', 1);

        $data['param_url'] = $request->all();

        //  Get data default (param_url, filter, module) for return view
        $data['module'] = $this->module;
        $data['filter'] = $this->filter;
        if ($this->whereRaw) {
            $data['record_total'] = $this->model->whereRaw($this->whereRaw);
        } else {
            $data['record_total'] = $this->model;
        }

        $data['record_total'] = $data['record_total']->whereRaw($where)->count();

        //  Set data for seo
        $data['page_title'] = $this->module['label'];
        $data['page_type'] = 'list';

        return view('appcoffeebill::list_processing')->with($data);
    }

    public function loadTableStatus()
    {
        return view('appcoffeebill::load_table_status');
    }

    public function getTableInfo()
    {
        return view('appcoffeebill::get_table_info');
    }

    public function getDataBillItem(Request $request)
    {
        $bill = Bill::find($request->bill_id);

        $data = [
            'table_id' => $bill->choose_table_id,
            'bill_html' => view('appcoffeebill::get_data_bill_item', compact('bill'))->render()
        ];
        return response()->json($data);
    }

    public function getDataBillTable(Request $request)
    {
        $data['bill'] = Bill::find($request->bill_id);

        return view('appcoffeebill::get_table_action', $data);
    }

    public function getChangeStatusBill(Request $request)
    {
        $bill = Bill::updateOrCreate(
            ['id' => $request->bill_id],
            [
                'status' => $request->status,
            ]
        );
        $status_span = '';
        if ($bill->status == 0) {
            $status_span = 'Chờ duyệt';
        } elseif ($bill->status == 1) {
            $status_span = 'Đang giao';
        } elseif ($bill->status == 2) {
            $status_span = 'Hoàn thành';
        } elseif ($bill->status == 3) {
            $status_span = 'Hủy';
        }


        if ($request->ajax()) {
            return \response()->json([
                'status' => $status_span,
                'data_status' => $bill->status,
            ]);
        }
    }

    public function getChangeNewStatusBill(Request $request)
    {
        if ($request->status == 0) {
            $bill = Bill::updateOrCreate(
                ['id' => $request->bill_id],
                [
                    'status' => 1,
                ]
            );
            if ($request->ajax()) {
                return \response()->json([
                    'stt' => true,
                    'data_status' => $bill->status,
                ]);
            }
        }
        if ($request->status == 1) {
            $bill = Bill::updateOrCreate(
                ['id' => $request->bill_id],
                [
                    'status' => 2,
                ]
            );
            if ($request->ajax()) {
                return \response()->json([
                    'stt' => true,
                    'data_status' => $bill->status,
                ]);
            }
        }
    }
}
