<div class="cart_content">
    <div class="cart_popup_detail">
        <div class="cart_content_right">
            <div class="title_pro_cart">{{$v->product_name}}</div>
            <div class="info_cart">
                <div class="count_pro_cart">
                            <span class="down_pro_cart fa fa-minus"
                                  style="  cursor: pointer; font-size: 16px; color: red;"></span>
                    <input type="text" class="count_product_cart"
                           name="count_product_cart"
                           data-price_product="{{$v->product_price}}"
                           data-row_id="{{$v->id}}"
                           value="{{$v->quantity}}"
                           min=1>
                    <span class="up_pro_cart fa fa-plus"
                          style="  cursor: pointer; font-size: 16px; color: green;"></span>
                </div>
                <div class="price_pro_cart">
                    <input name="rowId" data-priceOfProduct="{{$v->product_price}}"
                           value="{{$v->id}}" type="hidden">
                    <span>{{number_format($v->price,0,'','.')}}</span><sup>đ</sup>
                    <p class="fa fa-times-circle-o remove_pro_cart" aria-hidden="true"
                       style="cursor: pointer;"></p>
                </div>
            </div>
        </div>
    </div>
</div>