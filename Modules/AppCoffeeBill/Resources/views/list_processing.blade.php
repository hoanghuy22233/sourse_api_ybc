@extends(config('core.admin_theme').'.template')
@section('main')
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-medical-records"></i>
			</span>
                    <h3 class="kt-portlet__head-title">
                        {!! @$module['label'] !!}
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <div class="kt-portlet__head-actions">
                            <audio controls>
                                <source src="{{ asset('public/filemanager/userfiles/audio/plucky.mp3') }}" type='audio/mp4'>
                            </audio>
                            <button type="button" class="btn btn-default btn-icon-sm btn-show-bill active"
                                    onclick="billTab('bill')">
                                <i class="la la-search"></i> Đơn hàng đang xử lý
                            </button>
                            <button type="button" class="btn btn-default btn-icon-sm btn-show-table"
                                    onclick="billTab('table-status')">
                                <i class="la la-search"></i> Trạng thái bàn
                            </button>
                            <a href="/admin/bill" class="btn btn-default btn-icon-sm ">
                                <i class="la la-search"></i> Tất cả đơn hàng
                            </a>
                            <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle btn-closed-search"
                                    onclick="$('.form-search').slideToggle(100); $('.kt-portlet-search').toggleClass('no-padding');">
                                <i class="la la-search"></i> Tìm kiếm
                            </button>
                            <div class="dropdown dropdown-inline">
                                <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="la la-download"></i> Hành động
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end"
                                     style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(114px, 38px, 0px);">
                                    <ul class="kt-nav">
                                        <li class="kt-nav__section kt-nav__section--first">
                                            <span class="kt-nav__section-text">Chọn hành động</span>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a class="kt-nav__link export-excel" title="Xuất các bản ghi đang lọc ra file excel"
                                               onclick="$('input[name=export]').click();">
                                                <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                                <span class="kt-nav__link-text">Xuất Excel</span>
                                            </a>
                                        </li>
                                        @if(in_array($module['code'] . '_delete', $permissions))
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link" onclick="multiDelete();" title="Xóa tất cả các dòng đang được tích chọn">
                                                    <i class="kt-nav__link-icon la la-copy"></i>
                                                    <span class="kt-nav__link-text">Xóa nhiều</span>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            &nbsp;
                            {{--<a href="{{ url('/admin/'.$module['code'].'/add/') }}"--}}
                            {{--class="btn btn-brand btn-elevate btn-icon-sm">--}}
                            {{--<i class="la la-plus"></i>--}}
                            {{--Tạo mới--}}
                            {{--</a>--}}
                        </div>
                    </div>
                </div>
            </div>


            <div id="popup-bill-users">

            </div>


            <div class="kt-portlet__body kt-portlet-search @if(!isset($_GET['search'])) no-padding @endif">
                <!--begin: Search Form -->
                <form class="kt-form kt-form--fit kt-margin-b-20 form-search" id="form-search" method="GET" action=""
                      @if(!isset($_GET['search'])) style="display: none;" @endif>
                    <input name="search" type="hidden" value="true">
                    <input name="limit" type="hidden" value="{{ $limit }}">
                    <div class="row">
                        <div class="col-sm-6 col-lg-1 kt-margin-b-10-tablet-and-mobile list-filter-item">
                            <label>ID:</label>
                            <input type="number" name="id"
                                   placeholder="ID"
                                   value="{{ @$_GET['id'] }}"
                                   class="form-control kt-input">
                        </div>
                        @foreach($filter as $filter_name => $field)
                            <div class="col-sm-6 col-lg-3 kt-margin-b-10-tablet-and-mobile list-filter-item">
                                <label>{{ @$field['label'] }}:</label>
                                @include(config('core.admin_theme').'.list.filter.' . $field['type'], ['name' => $filter_name, 'field'  => $field])
                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button class="btn btn-primary btn-brand--icon" id="kt_search" type="submit">
						<span>
							<i class="la la-search"></i>
							<span>Lọc</span>
						</span>
                            </button>
                            &nbsp;&nbsp;
                            <a class="btn btn-secondary btn-secondary--icon" id="kt_reset" title="Xóa bỏ bộ lọc"
                               href="/admin/{{ $module['code'] }}">
						<span>
							<i class="la la-close"></i>
							<span>Reset</span>
						</span>
                            </a>
                        </div>
                    </div>
                    <input name="export" type="submit" value="export" style="display: none;">
                    @foreach($module['list'] as $k => $field)
                        <input name="sorts[]" value="{{ @$_GET['sorts'][$k] }}"
                               class="sort sort-{{ $field['name'] }}" type="hidden">
                    @endforeach
                </form>
                <!--end: Search Form -->
            </div>
            <div class="kt-separator kt-separator--md kt-separator--dashed" style="margin: 0;"></div>
            <div class="kt-portlet__body kt-portlet__body--fit bill">
                <!--begin: Datatable -->
                <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded"
                     id="scrolling_vertical" style="">
                    <table class="table table-striped list-bill">
                        <thead class="kt-datatable__head">
                        <tr class="kt-datatable__row" style="left: 0px;">
                            <th style="display: none;"></th>
                            <th data-field="id"
                                class="kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check"><span
                                        style="width: 20px;"><label
                                            class="kt-checkbox kt-checkbox--single kt-checkbox--all kt-checkbox--solid"><input
                                                type="checkbox"
                                                class="checkbox-master">&nbsp;<span></span></label></span></th>
                            @php $count_sort = 0; @endphp
                            @foreach($module['list'] as $field)
                                <th data-field="{{ $field['name'] }}"
                                    class="kt-datatable__cell kt-datatable__cell--sort {{ @$_GET['sorts'][$count_sort] != '' ? 'kt-datatable__cell--sorted' : '' }}"
                                    onclick="sort('{{ $field['name'] }}')">
                                    {{ $field['label'] }}

                                    @if(@$_GET['sorts'][$count_sort] == $field['name'].'|asc')
                                        <i class="flaticon2-arrow-up"></i>
                                    @else
                                        <i class="flaticon2-arrow-down"></i>
                                    @endif

                                </th>
                                @php $count_sort++; @endphp
                            @endforeach
                        </tr>
                        </thead>
                        <tbody class="kt-datatable__body ps ps--active-y" style="max-height: 496px;">
                        @foreach($listItem as $item)
                            <tr data-row="0" class="kt-datatable__row" id="bill-id-{{ $item->id }}" style="left: 0px;">
                                <td style="display: none;"
                                    class="id id-{{ $item->id }}">{{ $item->id }}</td>
                                <td class="kt-datatable__cell--center kt-datatable__cell kt-datatable__cell--check"
                                    data-field="ID"><span style="width: 20px;"><label
                                                class="kt-checkbox kt-checkbox--single kt-checkbox--solid"><input
                                                    name="id[]"
                                                    type="checkbox" class="ids"
                                                    value="{{ $item->id }}">&nbsp;<span></span></label></span>
                                </td>
                                @foreach($module['list'] as $field)
                                    <td data-field="{{ @$field['name'] }}"
                                        class="kt-datatable__cell item-{{ @$field['name'] }}">
                                        @if($field['type'] == 'custom')
                                            @include($field['td'], ['field' => $field])
                                        @else
                                            @include(config('core.admin_theme').'.list.td.'.$field['type'])
                                        @endif
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                        </div>
                        <div class="ps__rail-y" style="top: 0px; height: 496px; right: 0px;">
                            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 207px;"></div>
                        </div>
                        </tbody>
                    </table>
                    <div class="kt-datatable__pager kt-datatable--paging-loaded">
                        {!! $listItem->appends(isset($param_url) ? $param_url : '')->links() != '' ? $listItem->appends(isset($param_url) ? $param_url : '')->links() : '<ul class="pagination page-numbers nav-pagination links text-center"></ul>' !!}
                        <div class="kt-datatable__pager-info">
                            <div class="dropdown bootstrap-select kt-datatable__pager-size"
                                 style="width: 60px;">
                                <select class="selectpicker kt-datatable__pager-size select-page-size"
                                        onchange="$('input[name=limit]').val($(this).val());$('#form-search').submit();"
                                        title="Chọn số bản ghi hiển thị" data-width="60px"
                                        data-selected="20" tabindex="-98">
                                    <option value="20" {{ $limit == 20 ? 'selected' : '' }}>20</option>
                                    <option value="30" {{ $limit == 30 ? 'selected' : '' }}>30</option>
                                    <option value="50" {{ $limit == 50 ? 'selected' : '' }}>50</option>
                                    <option value="100" {{ $limit == 100 ? 'selected' : '' }}>100</option>
                                </select>
                            </div>
                            <span class="kt-datatable__pager-detail">Hiển thị {{ (($page - 1) * $limit) + 1 }} - {{ ($page * $limit) < $record_total ? ($page * $limit) : $record_total }} của {{ @number_format($record_total) }}</span>
                        </div>
                    </div>
                </div>
                <!--end: Datatable -->
            </div>
            <div class="table-status" style="display:none;">
                <div class="content-table">

                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom_head')
    <link type="text/css" rel="stylesheet" charset="UTF-8"
          href="{{ asset(config('core.admin_asset').'/css/list.css') }}">


    {{--Custom style--}}
    <style>
        audio::-webkit-media-controls-panel {
            background: none;
            height: 23px;
            margin-top: 30px;
        }
        audio {
            width: 100px;
            height: 22px;
            background: unset;
        }
        audio::-webkit-media-controls-timeline {
            display: none;
        }
        audio::-webkit-media-controls-time-remaining-display {
            display: none;
        }
        audio::-webkit-media-controls-current-time-display {
            display: none;
        }
        audio::-webkit-media-controls-mute-button {
            display: none;
        }
        audio::-webkit-media-controls-pan
    </style>
@endsection
@section('custom_footer')
    <script src="{{ asset(config('core.admin_asset').'/js/pages/crud/metronic-datatable/advanced/vertical.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset(config('core.admin_asset').'/js/list.js') }}"></script>
    @include(config('core.admin_theme').'.partials.js_common')


    {{--Custom JS--}}
    <script src="{{asset('public/libs/pusher/js/pusher.min.js')}}"></script>
    <script>

        $(document).ready(function () {
            loadTableStatus();
            $('body').on('click', '.table_a', function () {
                var table = $(this).data('table');
                var area = $(this).data('area');
                $.ajax({
                    url: '{{route('detail-bill-table')}}',
                    data: {
                        'table': table,
                        'area': area,
                    },
                    type: 'get',
                    success: function (html) {
                        $('#popup-bill-users').html(html);
                        $('.thong-tin-khach-hang-dat-ban').show();
                        $('.thong-tin-khach-hang').hide();
                    }
                })
            });
        });

        function getTableInfo(table_id, position) {
            $('#table-info').modal();
            $.ajax({
                url: '/admin/bill/get-table-info',
                data: {},
                type: 'GET',
                success: function (html) {
                    $('#table-info .modal-title').html('Thông tin bàn ' + position);
                    $('#table-info .modal-body').html(html);
                }
            });
        }

        function loadTableStatus() {
            $.ajax({
                url: '/admin/bill/load-table-status',
                data: {},
                type: 'GET',
                success: function (html) {
                    $('.table-status .content-table').html(html);
                }
            });
        }

        function billTab(option) {
            if (option == 'table-status') {
                $('.table-status').show();
                $('.bill').hide();
                $('.btn-show-bill').removeClass('active');
                $('.btn-show-table').addClass('active');
            } else {
                $('.table-status').hide();
                $('.bill').show();
                $('.btn-show-bill').addClass('active');
                $('.btn-show-table').removeClass('active');
                loadTableStatus();
            }
        }


        <?php
            $settings = \App\Models\Setting::whereIn('type', ['pusher'])->pluck('value', 'name')->toArray();
            ?>
            Pusher.logToConsole = true;
        var pusher = new Pusher('{{$settings["PUSHER_APP_KEY"]}}', {
            cluster: '{{$settings["PUSHER_APP_CLUSTER"]}}',
            forceTLS: true
        });
        var channel = pusher.subscribe('bill');
        channel.bind('update', function (data) {
            $('audio')[0].play();

            console.log(data.bill_id);
            $.ajax({
                url: '/admin/bill/get-data-bill-item',
                data: {
                    bill_id: data.bill_id
                },
                type: 'GET',
                success: function (resp) {
                    //  Xóa dòng cũ nếu có
                    $('#bill-id-' + data.bill_id).remove();

                    //  Thêm dòng mới
                    $('.list-bill tbody').html(resp.bill_html + $('.list-bill tbody').html());
                    if (resp.table_id != false) {
                        $(".table_" + resp.table_id).find('img').attr('src', '/public/frontend/themes/appcoffee/image/icon_table_selected.png');
                    }
                }
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "1000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            $('body').on('change', '.select-status', function () {
                //    Cập nhật lại trong bảng bill cột status và Đổi text của thẻ span
                var status = $(this).val();
                var bill_id = $(this).data('bill_id');
                $.ajax({
                    url: '/admin/bill/get-change-status-bill',
                    data: {
                        'bill_id': bill_id,
                        'status': status,
                    },
                    type: 'get',
                    success: function (result) {
                        window.location.reload();
                        toastr.success('Đổi trạng thái thành công!');
                    }
                });

            });
            //Khi click vào text của status
            var status = $(this).data('status');
            $('body').on('click', '.change-new-status', function () {
                var status = $(this).data('status');
                var bill_id = $(this).data('bill_id');
                $.ajax({
                    url: '/admin/bill/get-change-new-status-bill',
                    data: {
                        'bill_id': bill_id,
                        'status': status,
                    },
                    type: 'get',
                    success: function (result) {
                        if (result.stt === true) {
                            window.location.reload();
                            toastr.success('Đổi trạng thái thành công!');
                        }
                    }
                });
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('body').on('click', 'audio', function(){
                $('audio').css('display', 'none');
            });

            $('body').on('click', '.show_info', function () {
                var that = $(this);
                $.ajax({
                    url:'{{route('detail-bill-user')}}',
                    data:{
                        'bill_id':that.data('bill_id'),
                    },
                    type:'get',
                    success: function (html) {
                        $('#popup-bill-users').html(html);
                        $('.thong-tin-khach-hang-dat-ban').show();
                        $('.thong-tin-khach-hang').hide();
                    }
                })
            });
            $('body').on('click', '.go_home', function () {
                var that = $(this);
                $.ajax({
                    url:'{{route('detail-bill-user')}}',
                    data:{
                        'bill_id':that.data('bill_id'),
                    },
                    type:'get',
                    success: function (html) {
                        $('#popup-bill-users').html(html);
                        $('.thong-tin-khach-hang-dat-ban').hide();
                        $('.thong-tin-khach-hang').show();
                    }
                })
            });
            $('body').on('click', '.close_popup', function () {
                $('.thong-tin-khach-hang-dat-ban').hide();
                $('.thong-tin-khach-hang').hide();
            });

        });
    </script>
@endsection
@push('scripts')
    @include(config('core.admin_theme').'.partials.js_common_list')
@endpush