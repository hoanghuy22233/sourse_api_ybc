<?php

namespace Modules\HandymanSetting\Http\Controllers;

use App\Http\Controllers\Admin\RoleController;
use App\Http\Helpers\CommonHelper;
use App\Models\{Admin, RoleAdmin, Setting, Roles};
use Auth;
use Illuminate\Http\Request;
use Mail;
use Modules\Theme\Http\Controllers\Admin\CURDBaseController;
use Session;
use Validator;

class HandymanSettingController extends CURDBaseController
{
    protected $_role;

    public function __construct()
    {
        parent::__construct();
        $this->_role = new RoleController();
    }

    protected $module = [
        'code' => 'admin',
        'label' => 'handymansetting::admin.admin',
        'modal' => '\App\Models\Admin',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'handymansetting::admin.image'],
            ['name' => 'name', 'type' => 'text_admin_edit', 'label' => 'handymansetting::admin.name'],
            ['name' => 'role_id', 'type' => 'role_name', 'label' => 'handymansetting::admin.permission'],
            ['name' => 'tel', 'type' => 'text', 'label' => 'handymansetting::admin.phone'],
            ['name' => 'email', 'type' => 'text', 'label' => 'handymansetting::admin.email'],
            ['name' => 'status', 'type' => 'status', 'label' => 'handymansetting::admin.status'],
            ['name' => 'updated_at', 'type' => 'text', 'label' => 'handymansetting::admin.update']

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'handymansetting::admin.full_name'],
                ['name' => 'email', 'type' => 'text', 'class' => 'required', 'label' => 'handymansetting::admin.email'],
                ['name' => 'tel', 'type' => 'text', 'label' => 'handymansetting::admin.phoneS'],
                ['name' => 'password', 'type' => 'password', 'class' => 'required', 'label' => 'handymansetting::admin.password'],
                ['name' => 'password_confimation', 'type' => 'password', 'class' => 'required', 'label' => 'handymansetting::admin.re_password'],
            ],
        ]
    ];

    protected $filter = [
        'name_vi' => [
            'label' => 'handymansetting::admin.name',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'tel' => [
            'label' => 'handymansetting::admin.phone',
            'type' => 'number',
            'query_type' => '='
        ],
        'email' => [
            'label' => 'handymansetting::admin.email',
            'type' => 'number',
            'query_type' => '='
        ],
        'status' => [
            'label' => 'handymansetting::admin.status',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'handymansetting::admin.status',
                0 => 'handymansetting::admin.hidden',
                1 => 'handymansetting::admin.active'
            ]
        ],
    ];

    /*protected $validate = [
        'request' => [
            'name' => 'required',
        ],
        'label' => [
            'name' => 'Tên',
            'email' => 'Email'
        ]
    ];*/

    /*protected $validate_add = [
        'request' => [
            'email' => 'required|email|max:255|unique:admin'
        ],
        'label' => [
            'email' => 'Email'
        ]
    ];*/

    public function profile(Request $request)
    {
        if (!$_POST) {
//            dd(1);
            $data['result'] = \Auth::guard('admin')->user();
            $data['module'] = $this->module;
            $data['page_title'] = 'Chỉnh sửa ' . trans($this->module['label']);
            $data['page_type'] = 'update';
            return view('handymansetting::profile', $data);
        } else if ($_POST) {
            try {


                $item = \Auth::guard('admin')->user();

                if ($item->id == \Auth::guard('admin')->user()->id) {
                    $validator = Validator::make($request->all(), [
                        'name' => 'required',
                        'email' => 'required|email',
                    ], [
                        'name.required' => 'Bắt buộc phải nhập tên',
                        'email.required' => 'Bắt buộc phải nhập email',
                        'email.email' => 'Vui lòng nhập email',
                    ]);

                    if ($validator->fails()) {
                        return back()->withErrors($validator)->withInput();
                    }
                }

                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert
                $day_of_week='|';
                if ($item->day_of_week!=Null) {
                    $item->day_of_week = Null;
                    $item->save();
                }
                if (is_array($request->day_of_week)){
                    foreach ($request->day_of_week as $v) {
                        $day_of_week .= $v . '|';
                    }
                }else{
                    $day_of_week=Null;
                }

//                dd($day_of_week);
                $data['area1_start']=$request->area1_start;
                $data['area1_end']=$request->area1_end;
                $data['area2_start']=$request->area2_start;
                $data['area2_end']=$request->area2_end;
                $data['area3_start']=$request->area3_start;
                $data['area3_end']=$request->area3_end;
                $data['day_of_week']=$day_of_week;


                unset($data['password']);
                unset($data['password_confimation']);
//                dd($request);

                if (Admin::where('id', '!=', $item->id)->where('email', $data['email'])->count() > 0) {
                    CommonHelper::one_time_message('error', 'Email này đã được sử dụng!');
                    return back();
                }
                #

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');

//                    redirect()->back()->withInput();
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }

                return back();
            } catch (\Exception $ex) {
                CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//                CommonHelper::one_time_message('error', $ex->getMessage());
                return redirect()->back()->withInput();
            }
        }
    }

    public function profileAdmin(Request $request, $id)
    {
        $admin = Admin::findOrFail($id);
        if (!$_POST) {
            $data['result'] = $admin;
            $data['module'] = $this->module;
            $data['page_title'] = 'Chỉnh sửa ' . $this->module['label'];
            $data['page_type'] = 'update';
            return view('handymansetting::profile', $data);
        } else {
            $msg = 'Cập nhật thành công!';
            if (CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'admin_edit')) {
                RoleAdmin::where('admin_id', $id)->update([
                    'role_id' => $request->role_id
                ]);
            }
            CommonHelper::one_time_message('success', $msg);
            return back();
        }
    }


    public function searchForSelect2(Request $request)
    {
        $data = $this->model->select([$request->col, 'id'])->where($request->col, 'like', '%' . $request->keyword . '%');
        if ($request->where != '') {
            $data = $data->whereRaw(urldecode(str_replace('&#039;', "'", $request->where)));
        }

        $data = $data->limit(5)->get();
        return response()->json([
            'status' => true,
            'items' => $data
        ]);
    }
}