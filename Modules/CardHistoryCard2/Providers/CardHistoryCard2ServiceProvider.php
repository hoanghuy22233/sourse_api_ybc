<?php

namespace Modules\CardHistoryCard2\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class CardHistoryCard2ServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('CardHistoryCard2', 'Database/Migrations'));

        $this->registerPermission();

        //  Cấu hình menu trái
        $this->rendAsideMenu();

        //  Đăng ký quyền
        $this->registerPermission();
    }


    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['history-card_view', 'history-card_add', 'history-card_edit', 'history-card_delete']);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('cardhistorycard2::partials.aside_menu.dashboard_after_history_money');
        }, 1, 1);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('CardHistoryCard2', 'Config/config.php') => config_path('cardhistorycard2.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('CardHistoryCard2', 'Config/config.php'), 'cardhistorycard2'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/cardhistorycard2');

        $sourcePath = module_path('CardHistoryCard2', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/cardhistorycard2';
        }, \Config::get('view.paths')), [$sourcePath]), 'cardhistorycard2');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/cardhistorycard2');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'cardhistorycard2');
        } else {
            $this->loadTranslationsFrom(module_path('CardHistoryCard2', 'Resources/lang'), 'cardhistorycard2');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('CardHistoryCard2', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
