<?php

namespace Modules\CardHistoryCard2\Http\Controllers\Admin;

use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\CardHistoryCard2\Models\Card;
use Modules\CardHistoryCard2\Models\HistoryCard;
use Modules\CardHistoryCard2\Models\User;
use Validator;

class HistoryCardController extends CURDBaseController
{
    protected $module = [
        'code' => 'history-card',
        'table_name' => 'recharge_historys',
        'label' => 'Lịch sử nạp tiền',
        'modal' => '\Modules\CardHistoryCard2\Models\HistoryCard',
        'list' => [
//            ['name' => 'user_id', 'type' => 'relation', 'label' => 'Khách hàng', 'object' => 'user', 'display_field' => 'name'],
            ['name' => 'user_name', 'type' => 'text', 'label' => 'Khách hàng'],
            ['name' => 'card_code', 'type' => 'text', 'label' => 'Mã thẻ'],
            ['name' => 'money', 'type' => 'custom', 'td' => 'cardhistorycard2::list.td.price_eur', 'label' => 'Số tiền nạp', 'sort' => true],
            ['name' => 'created_at', 'type' => 'datetime_vi', 'label' => 'Ngày nạp'],
            ['name' => 'admin_id', 'type' => 'relation', 'label' => 'Người tạo', 'object' => 'admin', 'display_field' => 'name'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'user_id', 'type' => 'select2_model', 'class' => '', 'label' => 'Khách hàng', 'model' => User::class, 'display_field' => 'name'],
                ['name' => 'card_id', 'object' => 'card_ids', 'type' => 'select2_ajax_model', 'class' => '', 'label' => 'Mã thẻ', 'model' => Card::class, 'display_field' => 'code'],
                ['name' => 'money', 'type' => 'price', 'class' => 'required', 'label' => 'Số tiền nạp'],

            ],
        ],
    ];

    protected $filter = [
        'user_name' => [
            'label' => 'Tên khách hàng',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'card_code' => [
            'label' => 'Mã thẻ',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'admin_id' => [
            'label' => 'Người tạo',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'model' => User::class,
            'object' => 'admin',
            'query_type' => '='
        ],
//
//        'card_id' => [
//            'label' => 'Mã thẻ',
//            'type' => 'select2_ajax_model',
//            'display_field' => 'code',
//            'model' => Card::class,
//            'object' => 'card',
//            'query_type' => '='
//        ],
//        'money' => [
//            'label' => 'Số tiền nạp',
//            'type' => 'number',
//            'query_type' => 'like'
//        ],
        'created_at' => [
            'label' => 'Ngày nạp',
            'type' => 'date',
            'query_type' => 'custom'
        ],
        'user_id' => [
            'label' => 'ID khách',
            'type' => 'hidden',
            'query_type' => '='
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('cardhistorycard2::list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        //  Nếu không phải super_admin thì nó chỉ được xem dữ liệu shop nó tạo
        if(\Auth::guard('admin')->user()->super_admin != 1) {
            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
        }
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('cardhistorycard2::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'card_id' => 'required'
                ], [
                    'user_id.required' => 'Bắt buộc phải chọn khách hàng',
                    'card_id.required' => 'Bắt buộc phải chọn mã thẻ',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
//                    $data['type'] = 1;
                    #
                    $data['admin_id'] = \Auth::guard('admin')->user()->id;
                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        CommonHelper::flushCache();
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('cardhistorycard2::edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
                    'user_id' => 'required',
                    'card_id' => 'required'
                ], [
                    'user_id.required' => 'Bắt buộc phải chọn khách hàng',
                    'card_id.required' => 'Bắt buộc phải chọn mã thẻ',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
//                    $data['status'] = $request->has('status') ? 1 : 0;

                    if ($request->has('contact_info_name')) {
                        $data['contact_info'] = json_encode($this->getContactInfo($request));
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache();
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            CommonHelper::flushCache();
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
