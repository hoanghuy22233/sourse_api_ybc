<?php

namespace Modules\CardHistoryCard2\Models ;

use Illuminate\Database\Eloquent\Model;
use Modules\CardBill\Models\Admin;

class HistoryCard extends Model
{

    protected $table = 'recharge_historys';
    protected $guarded = [];
    public function user()
    {
        return $this->belongsTo(\Modules\CardUser\Models\User::class, 'user_id');
    }

    public function card() {
        return $this->belongsTo(Card::class, 'card_id');
    }
    public function admin() {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
}

