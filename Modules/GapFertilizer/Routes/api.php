<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function () {

    //  booking
    Route::group(['prefix' => 'bookings'], function () {
        Route::get('', 'Admin\BookingController@index');
        Route::post('', 'Admin\BookingController@store');
        Route::get('{id}', 'Admin\BookingController@show');
        Route::post('{id}', 'Admin\BookingController@update');
        Route::delete('{id}', 'Admin\BookingController@delete');

    });
    //  warehouses
    Route::group(['prefix' => 'warehouses', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\WarehouseController@index');
        Route::post('', 'Admin\WarehouseController@store');
        Route::get('{id}', 'Admin\WarehouseController@show');
        Route::post('{id}', 'Admin\WarehouseController@update');
        Route::delete('{id}', 'Admin\WarehouseController@delete');

    });

    //  đại lý
    Route::group(['prefix' => 'agencys', 'middleware'], function () {
        Route::get('', 'Admin\AgencyController@index');
        Route::post('', 'Admin\AgencyController@store');
        Route::get('{id}', 'Admin\AgencyController@show');
        Route::post('{id}', 'Admin\AgencyController@update');
        Route::delete('{id}', 'Admin\AgencyController@delete');

    });
    //  category
    Route::group(['prefix' => 'categories', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\CategoryController@index');
    });

    //  product
    Route::group(['prefix' => 'products'], function () {
        Route::get('search-by-qr-code', 'Admin\ProductController@qrCode');
        Route::get('all', 'Admin\ProductController@All');
        Route::get('', 'Admin\ProductController@index');
        Route::post('', 'Admin\ProductController@store');
        Route::get('{id}', 'Admin\ProductController@show');
        Route::post('{id}', 'Admin\ProductController@update');
        Route::delete('{id}', 'Admin\ProductController@delete');
    });

    //  User
    Route::group(['prefix' => 'users'], function () {
        Route::get('', 'Admin\UserController@index');
        Route::post('', 'Admin\UserController@store');
        Route::get('{id}', 'Admin\UserController@show');
        Route::post('{id}', 'Admin\UserController@update');
        Route::delete('{id}', 'Admin\UserController@delete');
    });

    //  bill
    Route::group(['prefix' => 'bills', 'middleware' => \App\Http\Middleware\CheckApiTokenAdmin::class], function () {
        Route::get('', 'Admin\BillController@index');
        Route::post('', 'Admin\BillController@store');
        Route::get('{id}', 'Admin\BillController@show');
        Route::post('{id}', 'Admin\BillController@update');
        Route::delete('{id}', 'Admin\BillController@delete');
    });

    //  orrder
    Route::group(['prefix' => 'orders'], function () {
        Route::get('', 'Admin\OrderController@index');
        Route::post('', 'Admin\OrderController@store');
        Route::get('{id}', 'Admin\OrderController@show');
        Route::post('{id}', 'Admin\OrderController@update');
        Route::delete('{id}', 'Admin\OrderController@delete');
    });

    //  Post
    Route::group(['prefix' => 'posts'], function () {
        Route::get('/list', 'Admin\PostController@list');
    });
});
