<?php
Route::get('/', function () {
    return redirect('/admin');
});
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {
    Route::group(['prefix' => 'booking'], function () {
        Route::get('', 'Admin\BookingController@getIndex')->name('booking')->middleware('permission:booking_view');
        Route::get('publish', 'Admin\BookingController@getPublish')->name('booking.publish')->middleware('permission:booking_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BookingController@add')->middleware('permission:booking_add');
        Route::get('delete/{id}', 'Admin\BookingController@delete')->middleware('permission:booking_delete');
        Route::post('multi-delete', 'Admin\BookingController@multiDelete')->middleware('permission:booking_delete');
        Route::get('search-for-select2', 'Admin\BookingController@searchForSelect2')->name('booking.search_for_select2')->middleware('permission:booking_view');
        Route::get('{id}/print', 'Admin\BookingController@print')->middleware('permission:booking_view');
        Route::get('{id}', 'Admin\BookingController@update')->middleware('permission:booking_view');
        Route::post('{id}', 'Admin\BookingController@update')->middleware('permission:booking_edit');
    });


    Route::group(['prefix' => 'product'], function () {
        Route::get('', 'Admin\ProductController@getIndex')->name('product')->middleware('permission:product_view');
        Route::get('publish', 'Admin\ProductController@getPublish')->name('product.publish')->middleware('permission:product_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\ProductController@add')->middleware('permission:product_add');
        Route::get('delete/{id}', 'Admin\ProductController@delete')->middleware('permission:product_delete');
        Route::post('multi-delete', 'Admin\ProductController@multiDelete')->middleware('permission:product_delete');
        Route::get('search-for-select2', 'Admin\ProductController@searchForSelect2')->name('product.search_for_select2')->middleware('permission:product_view');
        Route::get('{id}', 'Admin\ProductController@update')->middleware('permission:product_view');
        Route::post('{id}', 'Admin\ProductController@update')->middleware('permission:product_edit');
    });

    Route::group(['prefix' => 'order'], function () {
        Route::get('', 'Admin\OrderController@getIndex')->name('order')->middleware('permission:order_view');
        Route::get('publish', 'Admin\OrderController@getPublish')->name('order.publish')->middleware('permission:order_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\OrderController@add')->middleware('permission:order_add');
        Route::get('delete/{id}', 'Admin\OrderController@delete')->middleware('permission:order_delete');
        Route::post('multi-delete', 'Admin\OrderController@multiDelete')->middleware('permission:order_delete');
        Route::get('search-for-select2', 'Admin\OrderController@searchForSelect2')->name('order.search_for_select2')->middleware('permission:order_view');
        Route::get('{id}', 'Admin\OrderController@update')->middleware('permission:order_view');
        Route::post('{id}', 'Admin\OrderController@update')->middleware('permission:order_edit');
    });

    Route::group(['prefix' => 'bill'], function () {
        Route::get('', 'Admin\BillController@getIndex')->name('bill')->middleware('permission:bill_view');
        Route::get('publish', 'Admin\BillController@getPublish')->name('bill.publish')->middleware('permission:bill_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\BillController@add')->middleware('permission:bill_add');
        Route::get('delete/{id}', 'Admin\BillController@delete')->middleware('permission:bill_delete');
        Route::post('multi-delete', 'Admin\BillController@multiDelete')->middleware('permission:bill_delete');
        Route::get('search-for-select2', 'Admin\BillController@searchForSelect2')->name('bill.search_for_select2')->middleware('permission:bill_view');
        Route::get('{id}', 'Admin\BillController@update')->middleware('permission:bill_view');
        Route::post('{id}', 'Admin\BillController@update')->middleware('permission:bill_edit');
    });

    Route::group(['prefix' => 'warehouse'], function () {
        Route::get('', 'Admin\WarehouseController@getIndex')->name('warehouse')->middleware('permission:warehouse_view');
        Route::get('publish', 'Admin\WarehouseController@getPublish')->name('warehouse.publish')->middleware('permission:warehouse_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\WarehouseController@add')->middleware('permission:warehouse_add');
        Route::get('delete/{id}', 'Admin\WarehouseController@delete')->middleware('permission:warehouse_delete');
        Route::post('multi-delete', 'Admin\WarehouseController@multiDelete')->middleware('permission:warehouse_delete');
        Route::get('search-for-select2', 'Admin\WarehouseController@searchForSelect2')->name('warehouse.search_for_select2')->middleware('permission:warehouse_view');
        Route::get('{id}', 'Admin\WarehouseController@update')->middleware('permission:warehouse_view');
        Route::post('{id}', 'Admin\WarehouseController@update')->middleware('permission:warehouse_edit');
    });


    Route::group(['prefix' => 'type_product'], function () {
        Route::get('', 'Admin\TypeProductController@getIndex')->name('type_product')->middleware('permission:type_product_view');
        Route::get('publish', 'Admin\TypeProductController@getPublish')->name('type_product.publish')->middleware('permission:type_product_publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\TypeProductController@add')->middleware('permission:type_product_add');
        Route::get('delete/{id}', 'Admin\TypeProductController@delete')->middleware('permission:type_product_delete');
        Route::post('multi-delete', 'Admin\TypeProductController@multiDelete')->middleware('permission:type_product_delete');
        Route::get('search-for-select2', 'Admin\TypeProductController@searchForSelect2')->name('type_product.search_for_select2')->middleware('permission:type_product_view');
        Route::get('{id}', 'Admin\TypeProductController@update')->middleware('permission:type_product_view');
        Route::post('{id}', 'Admin\TypeProductController@update')->middleware('permission:type_product_edit');
    });

    //  Đại lý
    Route::group(['prefix' => 'agency'], function () {
        Route::get('', 'Admin\AgencyController@getIndex')->name('admin')->middleware('permission:admin_view');
        Route::get('publish', 'Admin\AgencyController@getPublish')->name('admin_view.publish')->middleware('permission:admin_view');
        Route::match(array('GET', 'POST'), 'add', 'Admin\AgencyController@add')->middleware('permission:admin_view');
        Route::get('delete/{id}', 'Admin\AgencyController@delete')->middleware('permission:admin_view');
        Route::post('multi-delete', 'Admin\AgencyController@multiDelete')->middleware('permission:admin_view');
        Route::get('search-for-select2', 'Admin\AgencyController@searchForSelect2')->name('type_product.search_for_select2')->middleware('permission:admin_view');
        Route::get('{id}', 'Admin\AgencyController@update')->middleware('permission:admin_view');
        Route::post('{id}', 'Admin\AgencyController@update')->middleware('permission:admin_view');
    });


    //  Admin
    Route::group(['prefix' => 'admin'], function () {

        Route::get('{id}', 'Admin\AdminController@update')->middleware('permission:admin_view');
        Route::post('{id}', 'Admin\AdminController@update')->middleware('permission:admin_edit');
    });

    //  Khách
    Route::group(['prefix' => 'user'], function () {

        Route::get('', 'Admin\UserController@getIndex')->middleware('permission:user_view');
    });
});
