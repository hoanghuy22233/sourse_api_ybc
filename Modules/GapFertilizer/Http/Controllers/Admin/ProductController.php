<?php

namespace Modules\GapFertilizer\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;

use Modules\GapFertilizer\Models\Category;
use Validator;

class ProductController extends CURDBaseController
{
    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Danh sách sản phẩm',
        'modal' => '\Modules\GapFertilizer\Models\Product',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên sản phẩm'],
            ['name' => 'dosage', 'type' => 'text', 'label' => 'Liều dùng'],
            ['name' => 'form', 'type' => 'text', 'label' => 'Hình thức'],
            ['name' => 'production_address', 'type' => 'text', 'label' => 'Địa chỉ sản xuất'],
            ['name' => 'date_manufacture', 'type' => 'text', 'label' => 'Ngày sản xuất'],
            ['name' => 'created_at', 'type' => 'text', 'label' => 'Thời điểm sử dụng phân bón'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'image', 'type' => 'custom', 'field' => 'gapfertilizer::form.fields.qr_code', 'label' => 'QR code'],
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên sản phẩm'],
                ['name' => 'packing', 'type' => 'text', 'label' => 'Quy cách đóng gói', 'group_class' => 'col-md-4'],
                ['name' => 'dosage', 'type' => 'text', 'label' => 'Liều lượng', 'class' => 'number_price', 'group_class' => 'col-md-4'],
                ['name' => 'form', 'type' => 'text', 'label' => 'Hình thức', 'group_class' => 'col-md-4'],
                ['name' => 'code', 'type' => 'text', 'label' => 'Mã code', 'group_class' => 'col-md-4'],
                ['name' => 'featured', 'type' => 'text', 'label' => 'Hoạt chất', 'group_class' => 'col-md-4'],
                ['name' => 'plant_object', 'type' => 'text', 'label' => 'Đối tượng cây trồng', 'group_class' => 'col-md-4'],
                ['name' => 'date_manufacture', 'type' => 'date', 'label' => 'Ngày sản xuất', 'group_class' => 'col-md-4'],
                ['name' => 'expiration_date', 'type' => 'date', 'label' => 'Ngày hết hạn', 'group_class' => 'col-md-4'],
                ['name' => 'license', 'type' => 'text', 'label' => 'Giấy phép', 'group_class' => 'col-md-4'],
                ['name' => 'production_address', 'type' => 'text', 'label' => 'Địa chỉ sẩn xuất', 'group_class' => 'col-md-4'],
                ['name' => 'control_person', 'type' => 'text', 'label' => 'Người kiểm soát chất lượng', 'group_class' => 'col-md-4'],

            ],

            'info_tab' => [
                ['name' => 'ingredients', 'type' => 'custom', 'fields' => 'gapfertilizer::form.fields.dynamic4', 'object' => 'tours', 'class' => 'required', 'label' => 'Thành phần'],
            ],
            'image_tab' => [
                ['name' => 'image', 'type' => 'file_image', 'label' => 'Ảnh sản phẩm'],
                ['name' => 'image_present', 'type' => 'multiple_image_dropzone', 'count' => '6', 'label' => 'Ảnh bao bì'],//
                ['name' => 'image_present2', 'type' => 'multiple_image_dropzone', 'count' => '6', 'label' => 'Ảnh khi sử dụng'],//
            ],


            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'Slug', 'des' => 'Đường dẫn sản phẩm trên thanh địa chỉ'],
                ['name' => 'meta_title', 'type' => 'text', 'label' => 'Meta title'],
                ['name' => 'meta_description', 'type' => 'text', 'label' => 'Meta description'],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên sản phẩm',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'dosage' => [
            'label' => 'Liều dùng',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'form' => [
            'label' => 'Hình thức',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'production_address' => [
            'label' => 'Địa chỉ sẩn xuất',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'date_manufacture' => [
            'label' => 'Ngày sản xuất',
            'type' => 'date',
            'query_type' => 'like'
        ],


    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('gapfertilizer::product.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
//        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
//        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
//            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
//        }

        //  Lọc theo danh mục
        if (!is_null($request->get('category_id'))) {
            $category = Category::find($request->category_id);
            if (is_object($category)) {
                $query = $query->where(function ($query) use ($category) {
                    $cat_childs = $category->childs; //  Lấy các id của danh mục con
                    $query->orWhere('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các tin thuộc danh mục hiện tại
                    foreach ($cat_childs as $cat_child) {
                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
                    }
                });
            }
        }
        if (!is_null($request->get('tag_id'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->tag_id . '|%');
        }
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('gapfertilizer::product.add')->with($data);
            } else if ($_POST) {
//                dd($request->all());
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());


                    if ($request->has('names')) {
                        $ingredients = [];
                        foreach ($request->names as $k => $v) {
                            $ingredients[] = [
                                'names' => @$v,
                                'ratios' => @$request->ratios[$k],
                            ];
                        }
                        $data['ingredients'] = json_encode($ingredients);
                    }


                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('tags')) {
                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
                    }
                    if ($request->has('image_present')) {
                        $data['image_present'] = implode('|', $request->image_present);
                    }
                    if ($request->has('image_present2')) {
                        $data['image_present2'] = implode('|', $request->image_present2);
                    }

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('gapfertilizer::product.edit')->with($data);
        } else if ($_POST) {
//            dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ], [
                'name.required' => 'Bắt buộc phải nhập tên',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());


                if ($request->has('names')) {
                    $ingredients = [];
                    foreach ($request->names as $k => $v) {
                        $ingredients[] = [
                            'names' => @$v,
                            'ratios' => @$request->ratios[$k],
                        ];
                    }
                    $data['ingredients'] = json_encode($ingredients);
                }


                //  Tùy chỉnh dữ liệu insert
                if ($request->has('multi_cat')) {
                    $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                    $data['category_id'] = $request->multi_cat[0];
                }
                if ($request->has('tags')) {
                    $data['tags'] = '|' . implode('|', $request->tags) . '|';
                }
                if ($request->has('image_present')) {
                    $data['image_present'] = implode('|', $request->image_present);
                }
                if ($request->has('image_present2')) {
                    $data['image_present2'] = implode('|', $request->image_present2);
                }
                #

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


}
