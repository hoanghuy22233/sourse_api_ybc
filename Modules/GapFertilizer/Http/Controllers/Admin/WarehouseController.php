<?php

namespace Modules\GapFertilizer\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use App\Models\Admin;
use Auth;
use Illuminate\Http\Request;

use Modules\GapFertilizer\Models\Product;
use Validator;

class WarehouseController extends CURDBaseController
{
    protected $module = [
        'code' => 'warehouse',
        'table_name' => 'warehouses',
        'label' => 'Kho',
        'modal' => '\Modules\GapFertilizer\Models\Warehouse',
        'list' => [
//            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
            ['name' => 'product_id', 'type' => 'relation', 'label' => 'Sản phẩm', 'object' => 'product', 'display_field' => 'name'],
            ['name' => 'amount', 'type' => 'text_edit', 'label' => 'Số lượng'],
            ['name' => 'money', 'type' => 'text', 'label' => 'Thành tiền'],
            ['name' => 'time', 'type' => 'text', 'label' => 'Thời gian'],

        ],
        'form' => [
            'general_tab' => [
                ['name' => 'amount', 'type' => 'number', 'class' => '', 'label' => 'Số lượng'],
                ['name' => 'money', 'type' => 'text', 'class' => 'number_price', 'label' => 'Thành tiền'],
                ['name' => 'product_id', 'type' => 'select2_ajax_model', 'label' => 'Sản phẩm', 'model' => Product::class, 'object' => 'product', 'display_field' => 'name'],
                ['name' => 'time', 'type' => 'date', 'label' => 'Thời gian'],
            ],

//            'image_tab' =>   [
////                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh'],
//            ],
        ],
    ];

    protected $filter = [
        'product_id' => [
            'label' => 'Tên sản phẩm',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'model' => Product::class,
            'object' => 'product',
            'query_type' => '='
        ],
        'agency_id' => [
            'label' => 'Đại lý',
            'type' => 'select2_model',
            'display_field' => 'name',
            'model' => Admin::class,
            'object' => 'admin',
            'query_type' => '='
        ],

        'amount' => [
            'label' => 'Số lượng',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'money' => [
            'label' => 'Thành tiền',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'time' => [
            'label' => 'Thời gian',
            'type' => 'date',
            'query_type' => 'like'
        ],

    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('gapfertilizer::warehouse.list')->with($data);
    }

//    public function appendWhere($query, $request)
//    {
////        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
////        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
////            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
////        }
//
//        //  Lọc theo danh mục
//        if (!is_null($request->get('category_id'))) {
//            $category = Category::find($request->category_id);
//            if (is_object($category)) {
//                $query = $query->where(function ($query) use ($category) {
//                    $cat_childs = $category->childs; //  Lấy các id của danh mục con
//                    $query->orWhere('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các tin thuộc danh mục hiện tại
//                    foreach ($cat_childs as $cat_child) {
//                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
//                    }
//                });
//            }
//        }
//        if (!is_null($request->get('tags'))) {
//            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->category_id . '|%');
//        }
//        return $query;
//    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('gapfertilizer::warehouse.add')->with($data);
            } else if ($_POST) {
//                dd($request->all());
                $validator = Validator::make($request->all(), [
                    'product_id' => 'required'
                ], [
                    'product_id.required' => 'Bắt buộc phải nhập sản phẩm',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert


                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('gapfertilizer::warehouse.edit')->with($data);
        } else if ($_POST) {
//            dd($request->all());
            $validator = Validator::make($request->all(), [
                'product_id' => 'required'
            ], [
                'product_id.required' => 'Bắt buộc phải nhập sản phẩm',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert

                #

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


}
