<?php

namespace Modules\GapFertilizer\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\Models\Admin;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\CardBill\Models\Bill;
use Modules\GapFertilizer\Models\Order;
use Modules\GapFertilizer\Models\User;
use Validator;

class BillController extends CURDBaseController
{
    protected $module = [
        'code' => 'bill',
        'table_name' => 'bills',
        'label' => 'Đơn hàng',
        'modal' => '\Modules\GapFertilizer\Models\Bill',
        'list' => [
            ['name' => 'invoice_image', 'type' => 'image', 'label' => 'Ảnh hóa đơn'],
            ['name' => 'user_id', 'type' => 'relation_edit', 'label' => 'Khách hàng', 'object' => 'user', 'display_field' => 'name'],
            ['name' => 'user_id', 'type' => 'custom', 'td' => 'gapfertilizer::list.td.relation', 'label' => 'SĐT', 'object' => 'user', 'display_field' => 'tel'],
            ['name' => 'user_id', 'type' => 'custom', 'td' => 'gapfertilizer::list.td.relation', 'label' => 'Email', 'object' => 'user', 'display_field' => 'email'],
            ['name' => 'user_id', 'type' => 'custom', 'td' => 'gapfertilizer::list.td.relation', 'label' => 'Địa chỉ', 'object' => 'user', 'display_field' => 'address'],
            ['name' => 'total_price', 'type' => 'price_vi', 'label' => 'Tổng tiền'],
            ['name' => 'quantity', 'type' => 'text', 'label' => 'Số lượng'],
            ['name' => 'created_at', 'type' => 'text', 'label' => 'Ngày đặt'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'total_price', 'type' => 'text', 'class' => 'number_price', 'label' => 'Tổng tiền'],
                ['name' => 'status', 'type' => 'select', 'options' => [
                    0 => 'Chời kích hoạt',
                    1 => 'Kích hoạt',
                ], 'label' => 'Trạng thái'],
                ['name' => 'quantity', 'type' => 'text', 'class' => 'number', 'label' => 'Số lượng'],

            ],
            'info_tab' => [
                ['name' => 'user_id', 'type' => 'select2_model', 'label' => 'Khách hàng', 'model' => User::class, 'object' => 'user', 'display_field' => 'name'],

            ],
//            'type_product_tab' => [
//                ['name' => 'iframe', 'type' => 'iframe', 'class' => 'col-xs-12 col-md-8 padding-left', 'src' => '/admin/type_product?bill_id={id}', 'inner' => 'style="min-height: 785px;"'],
//            ],
        ],
    ];

    protected $filter = [
        'user_id' => [
            'label' => 'Khách hàng',
            'type' => 'select2_model',
            'display_field' => 'name',
            'model' => \Modules\GapFertilizer\Models\User::class,
            'object' => 'student',
            'query_type' => '='
        ],
        'admin_id' => [
            'label' => 'Đại lý',
            'type' => 'select2_model',
            'display_field' => 'name',
            'model' => Admin::class,
            'object' => 'admin',
            'query_type' => '='
        ],
        'total_price' => [
            'label' => 'Tổng tiền',
            'type' => 'number',
            'query_type' => 'like'
        ],
        'created_at' => [
            'label' => 'Ngày đặt',
            'type' => 'date',
            'query_type' => 'custom'
        ],
//        'status' => [
//            'label' => 'Trạng thái',
//            'type' => 'select',
//            'query_type' => '=',
//            'options' => [
//                '' => 'Trạng thái',
//                0 => 'Không kích hoạt',
//                1 => 'Kích hoạt',
//            ],
//        ],

    ];

    public function getIndex(Request $request)
    {


        $data = $this->getDataList($request);
        return view('gapfertilizer::bill.list')->with($data);
    }

    public function add(Request $request)
    {
        try {

            if (!$_POST) {
                $data1['user_id'] = $request->user_id;
                $data = $this->getDataAdd($request);
                return view('gapfertilizer::bill.add', $data1)->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
//                    'name' => 'required'
                ], [
//                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    unset($data['iframe']);
                    $data['admin_id'] = $request->admin_id;

                    #
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($request->total_price <= User::where('id', $request->user_id)->first()->total_money) {
                        if ($this->model->save()) {
                            CommonHelper::flushCache($this->module['table_name']);
                            $this->afterAddLog($request, $this->model);

                            CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                        } else {
                            CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                        }
                    } else {
                        CommonHelper::one_time_message('error', 'Tông tiền của bạn không đủ để thực hiện giao dịch này.Vui lòng nạp thêm tiền vào thẻ để tiếp tục!');
                        return back();
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
//        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('gapfertilizer::bill.edit')->with($data);
            } else if ($_POST) {


                $validator = Validator::make($request->all(), [
//                    'name' => 'required'
                ], [
//                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    unset($data['iframe']);
                    $data['admin_id'] = $request->admin_id;

                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
//        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
////            CommonHelper::one_time_message('error', $ex->getMessage());
//            return redirect()->back()->withInput();
//        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {
            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0) {
                $item->{$request->column} = 1;
                $this->updateStatusOrder($item->id, 1);
            }
            else {
                $item->{$request->column} = 0;
                $this->updateStatusOrder($item->id, 0);
            }

            $item->save();
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false,
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function updateStatusOrder($bill_id, $status) {
        Order::where('bill_id', $bill_id)->where('price', '!=', 0)->update(['status' => $status]);
        return true;
    }

    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function activeOrder(Request $request)
    {
//        $settings = Setting::whereIn('type', ['general_tab', 'mail'])->pluck('value', 'name')->toArray();
        try {
            Order::findOrFail($request->order_id)->update(['status' => ($request->status == 0) ? 1 : 0]);
//           gửi mail
//            Mail::send(['html' => 'themeedu::mail.order_mail_user'], compact('bill', 'pass'), function ($message) use ($settings, $request) {
//                $message->from($settings['smtp_username'], $settings['mail_name']);
//                $message->to(Auth::guard('student')->check() ? Auth::guard('student')->user()->email : $request->email, Auth::guard('student')->check() ? Auth::guard('student')->user()->name : $request->name);
//                $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] Đơn hàng');
//            });
            return response()->json([
                'status' => true,
                'msg' => 'Cập nhật thành công!'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Không thành công!'
            ]);
        }

    }

    public function activeOrderAll(Request $request)
    {
        try {
            Order::where('bill_id', $request->bill_id)->update(['status' => ($request->status == 0) ? 0 : 1]);
            return response()->json([
                'status' => true,
                'msg' => 'Cập nhật thành công!'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Không thành công!'
            ]);
        }

    }

}
