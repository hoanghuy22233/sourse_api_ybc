<?php

namespace Modules\GapFertilizer\Http\Controllers\Admin;

use App\Http\Controllers\Admin\ChangeDataHistoryController;
use App\Http\Helpers\CommonHelper;
use App\Models\RoleAdmin;
use App\Models\Roles;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

use Modules\GapFertilizer\Models\Agency;

use Modules\GapFertilizer\Models\District;
use Modules\GapFertilizer\Models\Province;
use Modules\GapFertilizer\Models\Ward;
use Validator;

class BookingController extends CURDBaseController
{
//    protected $orderByRaw = 'status asc, id asc';

    protected $module = [
        'code' => 'booking',
        'table_name' => 'bookings',
        'label' => 'Yêu cầu',
        'modal' => '\Modules\GapFertilizer\Models\Booking',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit',  'label' => 'Tên khách hàng'],
            ['name' => 'tel', 'type' => 'text', 'label' => 'SĐT'],
            ['name' => 'time', 'type' => 'date_vi', 'label' => 'Thời gian nhận'],
            ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ'],
            ['name' => 'province_id', 'type' => 'relation', 'label' => 'Tỉnh', 'object' => 'province', 'display_field' => 'name'],
            ['name' => 'district_id', 'type' => 'relation', 'label' => 'Huyện', 'object' => 'district', 'display_field' => 'name'],
            ['name' => 'ward_id', 'type' => 'relation', 'label' => 'Xẫ', 'object' => 'ward', 'display_field' => 'name'],
            ['name' => 'agency_id', 'type' => 'relation', 'label' => 'Đại lý', 'object' => 'agencys', 'display_field' => 'name'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'label' => 'Tên khách hàng', 'group_class' => 'col-md-4'],
                ['name' => 'province_id', 'type' => 'select2_ajax_model', 'label' => 'Tỉnh', 'model' => Province::class, 'object' => 'province', 'display_field' => 'name', 'group_class' => 'col-md-4'],
                ['name' => 'district_id', 'type' => 'select2_ajax_model', 'label' => 'Huyện', 'model' => District::class, 'object' => 'district', 'display_field' => 'name', 'group_class' => 'col-md-4'],
                ['name' => 'ward_id', 'type' => 'select2_ajax_model', 'label' => 'Xã', 'model' => Ward::class, 'object' => 'ward', 'display_field' => 'name', 'group_class' => 'col-md-4'],
                ['name' => 'agency_id', 'type' => 'select2_ajax_model', 'label' => 'Đại lý', 'model' => Agency::class, 'object' => 'agencys', 'display_field' => 'name', 'group_class' => 'col-md-4'],
                ['name' => 'tel', 'type' => 'text', 'label' => 'SĐT', 'group_class' => 'col-md-4'],
                ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ', 'group_class' => 'col-md-6'],
                ['name' => 'time', 'type' => 'datetime-local', 'label' => 'Ngày hẹn', 'group_class' => 'col-md-6'],
                ['name' => 'note', 'type' => 'textarea', 'label' => 'Lời nhắn', 'group_class' => 'col-md-6'],

            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh sản phẩm'],
                ['name' => 'image_present', 'type' => 'multiple_image_dropzone', 'count' => '6', 'label' => 'Ảnh khác'],
            ],
//            'customer_tab' => [
//                ['name' => 'admin_id', 'type' => 'custom', 'type_history' => 'relation_multiple', 'field' => 'gapfertilizer::form.fields.select_customer', 'label' => 'Khách hàng', 'model' => Admin::class, 'object' => 'admin', 'display_field' => 'name', 'display_field2' => 'tel'],
//            ],
//            'comment_tab' => [
//                ['name' => 'iframe', 'type' => 'iframe', 'class' => 'col-xs-12 col-md-8 padding-left', 'src' => '/admin/comment?booking_id={id}', 'inner' => 'style="min-height: 485px;"'],
//            ],
        ],
    ];

    protected $filter = [

        'name' => [
            'label' => 'Tên khách hàng',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'tel' => [
            'label' => 'Số điện thoại',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'address' => [
            'label' => 'Địa chì',
            'type' => 'text',
            'query_type' => 'like'
        ],
        'time' => [
            'label' => 'Thời gian nhận',
            'type' => 'date',
            'query_type' => 'like'
        ],
        'province_id' => [
            'label' => 'Tỉnh',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'province',
            'model' => Province::class,
            'query_type' => '='
        ],
        'district_id' => [
            'label' => 'Huyện',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'district',
            'model' => \Modules\GapFertilizer\Models\District::class,
            'query_type' => '='
        ],
        'ward_id' => [
            'label' => 'Xã',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'ward',
            'model' => Ward::class,
            'query_type' => 'custom'
        ],
        'agency_id' => [
            'label' => 'Đại lý',
            'type' => 'select2_model',
            'display_field' => 'name',
            'object' => 'agency',
            'model' => Agency::class,
            'query_type' => '='
        ],

    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('gapfertilizer::booking.list')->with($data);
    }


    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('gapfertilizer::booking.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert

                    if ($request->has('image_present')) {
                        $data['image_present'] = implode('|', $request->image_present);
                    }

                    $data['district_id'] = $request->district_id;

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
            } catch (\Exception $ex) {
                CommonHelper::one_time_message('error', $ex->getMessage());
                return redirect()->back()->withInput();
            }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('gapfertilizer::booking.edit')->with($data);
        } else if ($_POST) {
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ], [
                'name.required' => 'Bắt buộc phải nhập tên',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                \DB::beginTransaction();

                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert


                if ($request->has('image_present')) {
                    $data['image_present'] = implode('|', $request->image_present);
                }
                #


//                if ($data['ktv_ids'] != $item->ktv_ids) {
//                    $item->ktv_ids = $data['ktv_ids'];
//                    if (!empty($request->ktv_ids)) {
//                        $ktvs = Admin::whereIn('id', $request->ktv_ids)->pluck('name');
//                        $ktv_name = '';
//                        foreach ($ktvs as $v) {
//                            $ktv_name .= $v . ', ';
//                        }
//                        gapfertilizerHelper::pushNotiFication($item, 'Kỹ thuật viên ' . $ktv_name . ' đã phụ trách đơn', [\Auth::guard('admin')->id()]);
//                    } else {
//                        gapfertilizerHelper::pushNotiFication($item, 'Không có kỹ thuật viên nào phụ trách đơn', [\Auth::guard('admin')->id()]);
//                    }
//                }
                $data['district_id'] = $request->district_id;
                $data['time'] = date('Y-m-d H:i:s', strtotime($data['time']));


                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }

                if ($item->save()) {
                    \DB::commit();
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    \DB::rollBack();
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


}
