<?php

namespace Modules\GapFertilizer\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\RoleAdmin;
use Illuminate\Http\Request;
use Modules\GapFertilizer\Models\Post;
use Validator;

class PostController extends Controller
{

    protected $module = [
        'code' => 'post',
        'table_name' => 'posts',
        'label' => 'Đại lý',
        'modal' => 'Modules\GapFertilizer\Models\Post',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'tel' => [
            'query_type' => 'like'
        ],
        'email' => [
            'query_type' => 'like'
        ],
        'address' => [
            'query_type' => 'like'
        ],
        'province_id' => [
            'query_type' => '='
        ],
        'district_id' => [
            'query_type' => '='
        ],
        'ward_id' => [
            'query_type' => '='
        ],
    ];


    public function list(Request $request)
    {

        try {


            $news = Post::select('id', 'name', 'image', 'intro')->where('status', 1)->orderBy('id', 'desc')->limit(2)->get();

            $post_selected = [];
            foreach ($news as $new) {
                $new->image = asset('public/filemanager/userfiles/' . $new->image);
                $post_selected[] = $new->id;
            }

            $posts_propose = Post::select('id', 'name', 'image', 'intro')->where('status', 1)->whereNotIn('id', $post_selected)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->limit(8)->get();
            foreach ($posts_propose as $new) {
                $new->image = asset('public/filemanager/userfiles/' . $new->image);
                $post_selected[] = $new->id;
            }

            return response()->json([
                'status' => true,
                'msg' => 'Thành công',
                'errors' => (object)[],
                'data' => [
                    'news' => $news,
                    'posts_propose' => $posts_propose
                ],
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = Post::selectRaw('admin.id, admin.name, admin.tel, admin.email, 
            admin.address, admin.province_id, admin.district_id, admin.ward_id, admin.note')->where('admin.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

//            $item->image = asset('public/filemanager/userfiles/' . $item->image);
            $item->province_name = @$item->province->name;
            $item->district_name = @$item->district->name;
            $item->ward_name = @$item->ward->name;
            unset($item->product_id);
            unset($item->province);
            unset($item->district);
            unset($item->ward);
            unset($item->province_id);
            unset($item->district_id);
            unset($item->ward_id);

            return response()->json([
                'status' => true,
                'msg' => 'Xác thực thất bại',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'tel' => 'required|unique:admin,tel',
        ], [
            'name.required' => 'Bắt buộc phải nhập sản phẩm',
            'tel.required' => 'Bắt buộc phải nhập số điện thoại',
            'tel.unique' => 'Số điện thoại bị trùng!',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Xác thực thất bại',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('admin_api')->id();
            $data['password'] = bcrypt($request->password);
            $item = Post::create($data);

            RoleAdmin::create([
                'admin_id' => $item->id,
                'role_id' => 3,
            ]);

            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Xác thực thất bại',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {

            $item = Post::find($id);


            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Xác thực thất bại',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $data = $request->except('api_token');

            //  Tùy chỉnh dữ liệu insert
            $data['password'] = bcrypt($request->password);

//            if ($request->has('image')) {
//                if (is_array($request->file('image'))) {
//                    foreach ($request->file('image') as $image) {
//                        $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($image, 'season');
//                    }
//                } else {
//                    $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($request->file('image'), 'season');
//                }
//                $data['image_extra'] = implode('|', $data['image_extra']);
//            }

            foreach ($data as $k => $v) {
                $item->{$k} = $v;
            }
            $item->save();
            return $this->show($item->id);
        }
    }


    public function delete($id)
    {
        if (Post::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
