<?php

namespace Modules\GapFertilizer\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\GapFertilizer\Models\Bill;
use Modules\GapFertilizer\Models\Order;
use Modules\GapFertilizer\Models\Product;
use Modules\GapFertilizer\Models\User;
use Validator;

class BillController extends Controller
{

    protected $module = [
        'code' => 'bill',
        'table_name' => 'bills',
        'label' => 'Đơn hàng',
        'modal' => 'Modules\GapFertilizer\Models\Bill',
    ];

    protected $filter = [
        'user_id' => [
            'query_type' => '='
        ],
        'from_date' => [
            'query_type' => 'custom'
        ],
        'to_date' => [
            'query_type' => 'custom'
        ],
    ];


    public function index(Request $request)
    {

        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Bill::selectRaw('bills.id, bills.created_at, bills.receipt_method, bills.status, bills.total_price, bills.date, bills.note,
            bills.user_id, bills.user_name, bills.user_tel, bills.user_email, bills.user_address')
                ->whereRaw($where);

            //  Tìm theo người tạo
            //  Nếu truyền vào id người tạo thì tìm theo người đó, còn không thì tìm ra các bill của mình tạo
            if ($request->has('admin_id')) {
                $listItem = $listItem->where('bills.admin_id', $request->admin_id);
            } else {
                $listItem = $listItem->where('bills.admin_id', \Auth::guard('admin_api')->user()->id);
            }

            //   Tìm theo ngày tạo
            if (!is_null($request->get('from_date'))) {
                $listItem = $listItem->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime($request->get('from_date'))));
            }
            if (!is_null($request->get('to_date'))) {
                $listItem = $listItem->where('created_at', '<=', date('Y-m-d 23:59:59', strtotime($request->get('to_date'))));
            }

            $price_total = $listItem->sum('total_price');

            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());

            foreach ($listItem as $k => $bill) {
                $user_name = @$bill->user->name;
                $user_tel = @$bill->user->tel;
                $user_address = @$bill->user->address;
                unset($bill->user);
                $bill->buyer = [
                    'id' => @$bill->user_id,
                    'name' => $user_name,
                    'tel' => $user_tel,
                    'address' => $user_address,
                ];
                unset($bill->user_id);

                $bill->receiver = [
                    'name' => $bill->user_name,
                    'tel' => $bill->user_tel,
                    'address' => $bill->user_address,
                    'email' => $bill->user_email,
                ];
                unset($bill->user_name);
                unset($bill->user_tel);
                unset($bill->user_address);
                unset($bill->user_email);

                $bill->orders = Order::select('product_name', 'price', 'product_price', 'quantity', 'unit')->where('bill_id', $bill->id)->get();
                $bill->count_item = count($bill->orders);
            }
            return response()->json([
                'status' => true,
                'msg' => 'Thành công',
                'errors' => (object)[],
                'data' => $listItem,
                'price_total' => $price_total,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $bill = Bill::selectRaw('bills.id, bills.created_at, bills.receipt_method, bills.status, bills.total_price, bills.date, bills.note,
            bills.user_id, bills.user_name, bills.user_tel, bills.user_email, bills.user_address')->where('id', $id)->first();

            if (!is_object($bill)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $orders = Order::leftJoin('products', 'products.id', '=', 'orders.product_id')
                ->selectRaw('products.name, products.image, orders.product_id, orders.quantity, orders.price, orders.product_price, orders.unit')->where('orders.bill_id', $id)->get();
            foreach ($orders as $order) {
                $order->image = asset('public/filemanager/userfiles/' . $order->image);
            }

            $bill->orders = $orders;
            $bill->buyer = [
                'id' => @$bill->user_id,
                'name' => @$bill->user->name,
                'tel' => @$bill->user->tel,
                'address' => @$bill->user->address,
            ];
            unset($bill->user_id);

            $bill->receiver = [
                'name' => $bill->user_name,
                'tel' => $bill->user_tel,
                'address' => $bill->user_address,
                'email' => $bill->user_email,
            ];
            unset($bill->user);
            unset($bill->user_name);
            unset($bill->user_tel);
            unset($bill->user_address);
            unset($bill->user_email);

            $bill->count_item = Order::where('bill_id', $bill->id)->count();

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $bill,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'orders' => 'required',
        ], [
            'user_id.required' => 'Bắt buộc phải nhập khách hàng',
            'bills.required' => 'Bắt buộc phải nhập đơn hàng',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Xác thực thất bại',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {


            $user = \App\Models\User::find($request->user_id);

            $bill = new Bill();
            $bill->admin_id = \Auth::guard('admin_api')->user()->id;
            $bill->user_id = $user->id;
            $bill->user_name = $user->name;
            $bill->user_email = @$user->email;
            $bill->user_address = @$user->address;
            $bill->user_province_id = @$user->user_province_id;
            $bill->user_district_id = @$user->district_id;
            $bill->user_ward_id = @$user->ward_id;
            $bill->user_gender = @$user->gender;
            $bill->user_tel = $user->tel;
            $bill->note = @$request->note;
            $bill->date = @$request->date;
            $bill->receipt_method = @$request->receipt_method;
            $bill->total_price = 0;
            $bill->save();

            if ($request->has('orders')) {
                foreach ($request->orders as $order) {
                    $product = Product::find($order['product_id']);
                    if (is_object($product)) {
                        $order = Order::create([
                            'product_id' => $order['product_id'],
                            'product_name' => @$product->name,
                            'product_price' => @$order['price'],
                            'product_image' => @$product->image,
                            'quantity' => @$order['quantity'],
                            'unit' => @$order['unit'],
                            'price' => @$order['price'] * @$order['quantity'],
                            'bill_id' => $bill->id
                        ]);
                        $bill->total_price += $order->price;
                    }
                }
            }
            $bill->save();

            return $this->show($bill->id);
        }
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ], [
            'user_id.required' => 'Bắt buộc phải nhập khách hàng',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Xác thực thất bại',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {

            $user = User::find($request->user_id);
            if (!is_object($user)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy khách hàng này',
                    'errors' => (object)[],
                    'data' => null,
                    'code' => 422
                ]);
            }

            $bill = Bill::find($id);
            $bill->user_id = $user->id;
            $bill->user_name = $user->name;
            $bill->user_email = @$user->email;
            $bill->user_address = @$user->address;
            $bill->user_province_id = @$user->user_province_id;
            $bill->user_district_id = @$user->district_id;
            $bill->user_ward_id = @$user->ward_id;
            $bill->user_gender = @$user->gender;
            $bill->user_tel = $user->tel;
            $bill->note = @$request->note;
            $bill->date = @$request->date;
            $bill->receipt_method = @$request->receipt_method;
            $bill->total_price = 0;
            $bill->save();

            $order_id_updated = [];
            if ($request->has('orders')) {
                foreach ($request->orders as $order) {
                    $order = Order::updateOrCreate([
                        'product_id' => $order['product_id'],
                        'bill_id' => $bill->id
                    ], [
                        'quantity' => $order['quantity'],
                        'price' => $order['price'],
                        'unit' => $order['unit'],
                    ]);
                    $bill->total_price += $order['price'] * @$order['quantity'];
                    $order_id_updated[] = $order->id;
                }
            }
            Order::whereNotIn('id', $order_id_updated)->where('bill_id', $bill->id)->delete();

            $bill->save();

            return $this->show($bill->id);
        }
    }


    public function delete($id)
    {
        if (Bill::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
