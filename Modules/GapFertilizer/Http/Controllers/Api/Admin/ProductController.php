<?php

namespace Modules\GapFertilizer\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Admin;
use App\Models\Setting;
use Illuminate\Http\Request;
use Modules\GapFertilizer\Models\Agency;
use Modules\GapFertilizer\Models\Product;
use Modules\Theme\Models\Post;
use Validator;

class ProductController extends Controller
{

    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Sản phẩm',
        'modal' => 'Modules\GapFertilizer\Models\Product',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
    ];


    public function index(Request $request)
    {

        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Product::selectRaw('products.id, products.name, products.image, products.image_extra, products.final_price, products.active_element, products.image_present, products.image_present2')
                ->whereRaw($where);
            //  Sort
            $product_ids = explode('|', \Auth::guard('admin_api')->user()->product_ids);
            $listItem = $listItem->whereIn('id', $product_ids);

            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            foreach ($listItem as $k => $v) {
                $v->image = asset('public/filemanager/userfiles/' . $v->image);
                $v->image_extra = asset('public/filemanager/userfiles/' . $v->image_extra);
                foreach (explode('|', $v->image_present) as $img) {
                    if ($img != '') {
                        $image_present[] = asset('public/filemanager/userfiles/' . $img);
                    }
                }
                $v->image_present = @$image_present;

                foreach (explode('|', $v->image_present2) as $img) {
                    if ($img != '') {
                        $image_present2[] = asset('public/filemanager/userfiles/' . $img);
                    }
                }
                $v->image_present2 = @$image_present2;


            }


            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = Product::selectRaw('products.id, products.name, products.final_price, products.image, products.image_extra, products.image_present, products.image_present2, products.created_at, products.dosage, products.packing, products.active_element, products.plant_object, products.form')->where('products.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }


            if ($item->image != null) {
                $item->image = asset('public/filemanager/userfiles/' . $item->image);
            }
            foreach (explode('|', $item->image_present) as $img) {
                if ($img != '') {
                    $image_present[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->image_present = @$image_present;

            if ($item->image_extra != null) {
                $item->image_extra = asset('public/filemanager/userfiles/' . $item->image_extra);
            }
            foreach (explode('|', $item->image_present2) as $img) {
                if ($img != '') {
                    $image_present2[] = asset('public/filemanager/userfiles/' . $img);
                }
            }
            $item->image_present2 = @$image_present2;


            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Xác thực thất bại',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('admin_api')->id();
            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = $data['image_present'][] = CommonHelper::saveFile($image, 'product');
                    }
                } else {
                    $data['image'] = $data['image_present'][] = CommonHelper::saveFile($request->file('image'), 'product');
                }
                $data['image_present'] = implode('|', $data['image_present']);
            }


            if ($request->has('image_extra')) {
                if (is_array($request->file('image_extra'))) {
                    foreach ($request->file('image_extra') as $image) {
                        $data['image_extra'] = $data['image_present2'][] = CommonHelper::saveFile($image, 'product');
                    }
                } else {
                    $data['image_extra'] = $data['image_present2'][] = CommonHelper::saveFile($request->file('image'), 'product');
                }
                $data['image_present2'] = implode('|', $data['image_present2']);
            }

            $item = Product::create($data);

            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {

        $item = Product::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Xác thực thất bại',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = $data['image_present'][] = CommonHelper::saveFile($image, 'product');
                }
            } else {
                $data['image'] = $data['image_present'][] = CommonHelper::saveFile($request->file('image'), 'product');
            }
            $data['image_present'] = implode('|', $data['image_present']);
        }


        if ($request->has('image_extra')) {
            if (is_array($request->file('image_extra'))) {
                foreach ($request->file('image_extra') as $image) {
                    $data['image_extra'] = $data['image_present2'][] = CommonHelper::saveFile($image, 'product');
                }
            } else {
                $data['image_extra'] = $data['image_present2'][] = CommonHelper::saveFile($request->file('image'), 'product');
            }
            $data['image_present2'] = implode('|', $data['image_present2']);
        }


        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (Product::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }



    public function all(Request $request)
    {

        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Product::selectRaw('products.id, products.name, products.image, products.image_extra, products.final_price, products.active_element, products.image_present, products.image_present2')
                ->whereRaw($where);
            //  Sort

            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            foreach ($listItem as $k => $v) {
                $v->image = asset('public/filemanager/userfiles/' . $v->image);
                $v->image_extra = asset('public/filemanager/userfiles/' . $v->image_extra);
                foreach (explode('|', $v->image_present) as $img) {
                    if ($img != '') {
                        $image_present[] = asset('public/filemanager/userfiles/' . $img);
                    }
                }
                $v->image_present = @$image_present;

                foreach (explode('|', $v->image_present2) as $img) {
                    if ($img != '') {
                        $image_present2[] = asset('public/filemanager/userfiles/' . $img);
                    }
                }
                $v->image_present2 = @$image_present2;


            }


            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }



    public function qrCode(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [

                'code' => 'required',
            ], [
                'code.required' => 'Bắt buộc phải nhập mã sản phẩm!'
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Xác thực thất bại',
                    'errors' => $validator->errors(),
                    'data' => null,
                    'code' => 401
                ]);
            } else {

                $product = Product::selectRaw('products.id, products.name, products.code, products.image,
                 products.ingredients, products.packing,
                  products.date_manufacture, products.expiration_date,
                   products.circulate_number, products.utility, products.regulation_number')
                    ->where('code', $request->code)->first();

                if (!is_object($product)) {
                    return response()->json([
                        'status' => true,
                        'msg' => 'Không tìm thấy sản phẩm',
                        'errors' => (object)[],
                        'data' => null,
                        'code' => 401
                    ]);
                }

                $product->image = asset('public/filemanager/userfiles/' . @$product->image);
                $product->code = base64_encode($product->code);
                return response()->json([
                    'status' => true,
                    'msg' => 'lấy danh sách thành công',
                    'errors' => (object)[],
                    'data' => $product,
                    'company_info' => @Setting::where('name', 'company_info')->first()->value,
                    'code' => 201
                ]);
            }

        } catch (\Exception $ex) {

            return response()->json([
                'status' => false,
                'msg' => 'Xác thực thất bại',
                'errors' => $ex->getMessage(),
                'data' => null,
                'code' => 401

            ]);
        }
    }


}
