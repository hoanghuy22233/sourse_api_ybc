<?php

namespace Modules\GapFertilizer\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\GapFertilizer\Models\Bill;
use Modules\GapFertilizer\Models\User;
use Validator;

class UserController extends Controller
{

    protected $module = [
        'code' => 'user',
        'table_name' => 'users',
        'label' => 'Khách hàng',
        'modal' => 'Modules\GapFertilizer\Models\User',
    ];

    protected $filter = [
        'name' => [
            'query_type' => '='
        ],
        'tel' => [
            'query_type' => '='
        ],
        'province_id' => [
            'query_type' => '='
        ],
        'district_id' => [
            'query_type' => '='
        ],
        'ward_id' => [
            'query_type' => '='
        ],
    ];


    public function index(Request $request)
    {

        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = User::selectRaw('users.id, users.name, users.tel, 
            users.address, users.province_id, users.district_id, users.ward_id')
                ->whereRaw($where);
            $listItem = $listItem->where('users.agency_id', \Auth::guard('admin_api')->user()->id);
            if ($request->has('keyword')) {
                $listItem->where(function ($query) use ($request) {
                    $query->orWhere('name', 'LIKE', '%' . $request->keyword . '%');
                    $query->orWhere('tel', 'LIKE', '%' . $request->keyword . '%');
                    $query->orWhere('address', 'LIKE', '%' . $request->keyword . '%');
                    $query->orWhere('email', 'LIKE', '%' . $request->keyword . '%');
                });
            }

            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());
            foreach ($listItem as $k => $v) {
                $v->province_name = @$v->province->name;
                $v->district_name = @$v->district->name;
                $v->ward_name = @$v->ward->name;
                $v->price_total = @$v->bills->sum('total_price');
                unset($v->product_id);
                unset($v->province);
                unset($v->district);
                unset($v->ward);
                unset($v->province_id);
                unset($v->district_id);
                unset($v->ward_id);
                unset($v->bills);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = User::selectRaw('users.id, users.name, users.tel, 
            users.address, users.province_id, users.district_id, users.ward_id')->where('users.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }


//            $item->image = asset('public/filemanager/userfiles/' . $item->image);
            $item->province_name = @$item->province->name;
            $item->district_name = @$item->district->name;
            $item->ward_name = @$item->ward->name;
            unset($item->product_id);
            unset($item->province);
            unset($item->district);
            unset($item->ward);
            unset($item->province_id);
            unset($item->district_id);
            unset($item->ward_id);

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'tel' => 'required|unique:users|numeric',

        ], [
            'name.required' => 'Bắt buộc phải nhập sản phẩm',
            'tel.unique' => 'Sđt đã tồn tại',

        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => $validator->errors()->first('name') . ' ' . $validator->errors()->first('tel'),
//                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['agency_id'] = \Auth::guard('admin_api')->id();
            $data['password'] = bcrypt($request->password);
            $item = User::create($data);

            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'tel' => 'required|unique:users|numeric',

        ], [
            'name.required' => 'Bắt buộc phải nhập tên',
            'tel.unique' => 'Sđt đã tồn tại',

        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => $validator->errors()->first('name') . ' ' . $validator->errors()->first('tel'),
//                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {

            $item = User::find($id);

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Xác thực thất bại',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $data = $request->except('api_token');

            //  Tùy chỉnh dữ liệu insert
//            if ($request->has('image')) {
//                if (is_array($request->file('image'))) {
//                    foreach ($request->file('image') as $image) {
//                        $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($image, 'season');
//                    }
//                } else {
//                    $data['image'] = $data['image_extra'][] = CommonHelper::saveFile($request->file('image'), 'season');
//                }
//                $data['image_extra'] = implode('|', $data['image_extra']);
//            }

            foreach ($data as $k => $v) {
                $item->{$k} = $v;
            }
            $item->save();
            return $this->show($item->id);
        }
    }


    public function delete($id)
    {
        if (User::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
