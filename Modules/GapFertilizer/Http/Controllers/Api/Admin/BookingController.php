<?php

namespace Modules\GapFertilizer\Http\Controllers\Api\Admin;

use App\Mail\MailServer;
use Illuminate\Support\Facades\Mail;
use App\Models\Admin;
use App\Models\Setting;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;

use Modules\EduMarketing\Models\MarketingMail;
use Modules\GapFertilizer\Models\Agency;
use Modules\GapFertilizer\Models\Booking;
use Validator;
use URL;

class BookingController extends Controller
{
    protected $_mailSetting;

    public function __construct()
    {
        $this->_mailSetting = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();
    }

    protected $module = [
        'code' => 'booking',
        'table_name' => 'bookings',
        'label' => 'Yêu cầu',
        'modal' => 'Modules\GapFertilizer\Models\Booking',
    ];

    protected $filter = [
        'name' => [
            'query_type' => 'like'
        ],
        'admin_id' => [
            'query_type' => '='
        ],
        'product_id' => [
            'query_type' => '='
        ],
        'province_id' => [
            'query_type' => '='
        ],
        'district_id' => [
            'query_type' => '='
        ],
        'ward_id' => [
            'query_type' => '='
        ],
        'producer_id' => [
            'query_type' => '='
        ],
        'time' => [
            'query_type' => '='
        ],
        'from_date' => [
            'query_type' => 'from_to_date'
        ],
        'to_date' => [
            'query_type' => 'from_to_date'
        ],
        'status' => [
            'query_type' => '='
        ],


    ];



    public function index(Request $request)
    {

        try {

            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Booking::selectRaw('bookings.id, bookings.name, bookings.tel, bookings.time, bookings.province_id, bookings.district_id, 
            bookings.admin_id, bookings.ward_id, bookings.note, bookings.address, bookings.agency_id, bookings.created_at')->whereRaw($where);

            $listItem = $listItem->where('bookings.agency_id', \Auth::guard('admin_api')->user()->id);

            //  Sort
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 20;
            $listItem = $listItem->paginate($limit)->appends($request->all());

            foreach ($listItem as $k => $v) {

                $agency_name = @$v->agency->name;
                unset($v->agency);
                $v->agency = [
                    'id' => @$v->agency_id,
                    'name' => $agency_name,
                ];
                unset($v->agency_id);


                $v->buyer = [
                    'id' => @$v->admin_id,
                    'name' => @$v->admin->name,
                ];
                unset($v->admin);
                unset($v->admin_id);

                $v->province_name = @$v->province->name;
                $v->district_name = @$v->district->name;
                $v->ward_name = @$v->ward->name;


                unset($v->province_id);
                unset($v->district_id);
                unset($v->ward_id);
                unset($v->province);
                unset($v->district);
                unset($v->ward);
            }

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = Booking::selectRaw('bookings.id, bookings.name, bookings.tel, bookings.time, bookings.province_id, bookings.district_id, bookings.ward_id, bookings.note, bookings.address, bookings.agency_id, bookings.created_at')
                ->where('bookings.id', $id)->first();

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }


            $item->province_name = @$item->province->name;
            $item->district_name = @$item->district->name;
            $item->ward_name = @$item->ward->name;
            $agency_name = @$item->agency->name;
            unset($item->agency);
            $item->agency = [
                'id' => @$item->agency_id,
                'name' => $agency_name,
            ];

            unset($item->province_id);
            unset($item->district_id);
            unset($item->ward_id);
            unset($item->province);
            unset($item->district);
            unset($item->ward);
            unset($item->agency_id);

            return response()->json([
                'status' => true,
                'msg' => 'Thành công',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
//            'image' => 'required'
        ], [
            'name.required' => 'Bắt buộc phải nhập tên sản phẩm',
//            'image.required' => 'Bắt buộc phải nhập ảnh',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Xác thực thất bại',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {
            $data = $request->all();
            //  Tùy chỉnh dữ liệu insert
            $data['admin_id'] = \Auth::guard('admin_api')->id();
            $data['admin_name'] = str_slug(\Auth::guard('admin_api')->user()->name, '-');


            if ($request->has('image')) {
                if (is_array($request->file('image'))) {
                    foreach ($request->file('image') as $image) {
                        $data['image'] = $data['image_present'][] = CommonHelper::saveFile($image, 'booking/' . $data['admin_name'] . '/' . date('d-m-y'));
                    }
                } else {
                    $data['image'] = $data['image_present'][] = CommonHelper::saveFile($request->file('image'), 'booking/' . $data['admin_name'] . '/' . date('d-m-y'));
                }
                $data['image_present'] = implode('|', $data['image_present']);
            }
            $item = Booking::create($data);

            $this->sendMailToAdmin($item, 5);


            return $this->show($item->id);
        }
    }


    public function update(Request $request, $id)
    {

        $item = Booking::find($id);
        if (!is_object($item)) {
            return response()->json([
                'status' => false,
                'msg' => 'Xác thực thất bại',
                'errors' => [
                    'exception' => [
                        'Không tìm thấy bản ghi'
                    ]
                ],
                'data' => null,
                'code' => 404
            ]);
        }

        $data = $request->except('api_token');
        //  Tùy chỉnh dữ liệu insert

        if ($request->has('image')) {
            if (is_array($request->file('image'))) {
                foreach ($request->file('image') as $image) {
                    $data['image'] = $data['image_present'][] = CommonHelper::saveFile($image, 'booking/' . $data['admin_name'] . '/' . date('d-m-y'));
                }
            } else {
                $data['image'] = $data['image_present'][] = CommonHelper::saveFile($request->file('image'), 'booking/' . $data['admin_name'] . '/' . date('d-m-y'));
            }
            $data['image_present'] = implode('|', $data['image_present']);
        }

        if ($request->has('image_extra')) {
            if (is_array($request->file('image_extra'))) {
                foreach ($request->file('image_extra') as $image) {
                    $data['image_extra'] = $data['image_present2'][] = CommonHelper::saveFile($image, 'booking');
                }
            } else {
                $data['image_extra'] = $data['image_present2'][] = CommonHelper::saveFile($request->file('image'), 'booking');
            }
            $data['image_present2'] = implode('|', $data['image_present2']);
        }


        foreach ($data as $k => $v) {
            $item->{$k} = $v;
        }
        $item->save();

        return $this->show($item->id);
    }


    public function delete($id)
    {
        if (Booking::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {

                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . ".time >= '" . date('Y-m-d', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . ".time <= '" . date('Y-m-d', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }
        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }

    public function SendMailFullSlot($info)
    {

        $user = (object)[
            'email' => $info['email'],
            'name' => $info['name'],
            'booking_id' => $info['booking_id'],
        ];
        $data = [
            'view' => 'gapfertilizer::emails.booking_admin',
            'user' => $user,
            'name' => $this->_mailSetting['mail_name'],
            'subject' => 'Đơn hàng mới!'
        ];
        Mail::to($user)->send(new MailServer($data));
        return true;
    }

    public function sendMailToAdmin($booking, $camp_id = 5)
    {
        //  Gửi mail cho khách
        try {
            $camp = MarketingMail::find($camp_id);

            $this->_mailSetting = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();

            //  Lấy danh sách email của admin thành dạng mảng
            $admin_emails = $this->_mailSetting['admin_emails'];
            $admin_emails = explode(',', $admin_emails);
            //  Lấy thông tin người nhận là email đầu tiên trong mảng
            $user = (object)[
                'email' => $admin_emails[0],
                'name' => 'Admin',
                'id' => null
            ];

            //  Lấy những email khác cho vào cc
            $cc = $admin_emails;
            unset($cc[0]);

            $data = [
                'sender_account' => $camp->email_account,
                'user' => $user,
                'cc' => $cc,
                'subject' => $camp->subject,
                'content' => $this->processContentMail($camp->email_template->content, $booking)
            ];

            Mail::to($data['user'])->send(new MailServer($data));

            $agency_email = Agency::find($booking->agency_id)->email;
            //  Gửi mail cho đại lý
            $user = (object)[
                'email' => $agency_email,
                'name' => 'Admin',
                'id' => null
            ];

            $data = [
                'sender_account' => $camp->email_account,
                'user' => $user,
                'subject' => $camp->subject,
                'content' => $this->processContentMail($camp->email_template->content, $booking)
            ];

            Mail::to($data['user'])->send(new MailServer($data));
            return response()->json([
                'status' => true,
                'msg' => 'Thành công',
                'errors' => (object)[],
                'data' => $data,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function processContentMail($html, $booking)
    {
        $html = str_replace('{name}', $booking->name, $html);
        $html = str_replace('{tel}', $booking->tel, $html);
        $html = str_replace('{address}', $booking->address, $html);
        $html = str_replace('{note}', $booking->note, $html);

        return $html;
    }
}
