<?php

namespace Modules\GapFertilizer\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Modules\GapFertilizer\Models\Bill;
use Modules\GapFertilizer\Models\Order;
use Modules\GapFertilizer\Models\User;
use Validator;

class OrderController extends Controller
{

    protected $module = [
        'code' => 'order',
        'table_name' => 'orders',
        'label' => 'Đơn hàng',
        'modal' => 'Modules\GapFertilizer\Models\Order',
    ];

    protected $filter = [
        'bill_id' => [
            'query_type' => '='
        ],
        'price' => [
            'query_type' => 'like'
        ],
        'quantity' => [
            'query_type' => 'like'
        ],
        'user_id' => [
            'query_type' => '='
        ],
        'product_id' => [
            'query_type' => '='
        ],
        'from_date' => [
            'query_type' => 'custom'
        ],
        'to_date' => [
            'query_type' => 'custom'
        ],
    ];

    protected $type_unit = [
        '/kg' => '/kg',
        '/tấn' => '/tấn',
    ];

    public function index(Request $request)
    {

        try {
            //  Filter
            $where = $this->filterSimple($request);
            $listItem = Order::leftJoin('bills', 'bills.id', '=', 'orders.bill_id')
                ->leftJoin('products', 'products.id', '=', 'orders.product_id');

            if ($request->has('admin_id')) {
                $listItem = $listItem->where('bills.admin_id', $request->admin_id);
            } else {
                $listItem = $listItem->where('bills.admin_id', \Auth::guard('admin_api')->user()->id);
            }

            $listItem = $listItem->selectRaw('orders.id, orders.price, orders.quantity, orders.unit, orders.created_at, orders.product_id, orders.product_name, orders.product_price, bills.id as bill_id')
                ->whereRaw($where);

            //  Tìm theo ngày tạo
            if (!is_null($request->get('from_date'))) {
                $listItem = $listItem->where('orders.created_at', '>=', date('Y-m-d 00:00:00', strtotime($request->get('from_date'))));
            }
            if (!is_null($request->get('to_date'))) {
                $listItem = $listItem->where('orders.created_at', '<=', date('Y-m-d 23:59:59', strtotime($request->get('to_date'))));
            }

            $price_total = $listItem->sum('price');
            foreach ($listItem as $k => $v) {

                $v->method = array_key_exists($v->unit, $this->type_unit) ? $this->type_unit[$v->unit] : '';
            }
            $listItem = $this->sort($request, $listItem);

            $limit = $request->has('limit') ? $request->limit : 100;
            $listItem = $listItem->paginate($limit)->appends($request->all());

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $listItem,
                'price_total' => $price_total,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function show($id)
    {
        try {

            $item = Order::find($id);

            if (!is_object($item)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Lỗi',
                    'errors' => [
                        'exception' => [
                            'Không tìm thấy bản ghi'
                        ]
                    ],
                    'data' => null,
                    'code' => 404
                ]);
            }

            $orders = Order::leftJoin('products', 'products.id', '=', 'orders.product_id')
                ->selectRaw('products.name, products.image, orders.product_id, orders.quantity, orders.unit, orders.price')->where('orders.bill_id', $id)->get();
            foreach ($orders as $order) {
                $order->image = asset('public/filemanager/userfiles/' . $order->image);
            }

            $item->orders = $orders;

            return response()->json([
                'status' => true,
                'msg' => '',
                'errors' => (object)[],
                'data' => $item,
                'code' => 201
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi',
                'errors' => [
                    'exception' => [
                        $ex->getMessage()
                    ]
                ],
                'data' => null,
                'code' => 401
            ]);
        }
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'orders' => 'required',
        ], [
            'user_id.required' => 'Bắt buộc phải nhập khách hàng',
            'orders.required' => 'Bắt buộc phải nhập đơn hàng',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Xác thực thất bại',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {

            $user = User::find($request->user_id);
            if (!is_object($user)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy khách hàng này',
                    'errors' => (object)[],
                    'data' => null,
                    'code' => 422
                ]);
            }

            $bill = new Bill();
            $bill->user_id = $user->id;
            $bill->user_name = $user->name;
            $bill->user_email = @$user->email;
            $bill->user_address = @$user->address;
            $bill->user_province_id = @$user->user_province_id;
            $bill->user_district_id = @$user->district_id;
            $bill->user_ward_id = @$user->ward_id;
            $bill->user_gender = @$user->gender;
            $bill->user_tel = $user->tel;
            $bill->note = @$request->note;
            $bill->date = @$request->date;
            $bill->receipt_method = @$request->receipt_method;
            $bill->total_price = 0;
            $bill->save();

            if ($request->has('orders')) {
                foreach ($request->orders as $order) {
                    Order::create([
                        'product_id' => $order['product_id'],
                        'quantity' => $order['quantity'],
                        'price' => $order['price'],
                        'unit' => $order['unit'],
                        'bill_id' => $bill->id
                    ]);
                    $bill->total_price += $order['price']*@$order['quantity'];;
                }
            }
            $bill->save();

            return $this->show($bill->id);
        }
    }


    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ], [
            'user_id.required' => 'Bắt buộc phải nhập khách hàng',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'msg' => 'Xác thực thất bại',
                'errors' => $validator->errors(),
                'data' => null,
                'code' => 422
            ]);
        } else {

            $user = User::find($request->user_id);
            if (!is_object($user)) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy khách hàng này',
                    'errors' => (object)[],
                    'data' => null,
                    'code' => 422
                ]);
            }

            $bill = Bill::find($id);
            $bill->user_id = $user->id;
            $bill->user_name = $user->name;
            $bill->user_email = @$user->email;
            $bill->user_address = @$user->address;
            $bill->user_province_id = @$user->user_province_id;
            $bill->user_district_id = @$user->district_id;
            $bill->user_ward_id = @$user->ward_id;
            $bill->user_gender = @$user->gender;
            $bill->user_tel = $user->tel;
            $bill->note = @$request->note;
            $bill->date = @$request->date;
            $bill->receipt_method = @$request->receipt_method;
            $bill->total_price = 0;
            $bill->save();

            $order_id_updated = [];
            if ($request->has('orders')) {
                foreach ($request->orders as $order) {
                    $order = Order::updateOrCreate([
                        'product_id' => $order['product_id'],
                        'bill_id' => $bill->id
                    ],[
                        'quantity' => $order['quantity'],
                        'price' => $order['price'],
                        'unit' => $order['unit'],
                    ]);
                    $bill->total_price += $order['price'];
                    $order_id_updated[] = $order->id;
                }
            }
            Order::whereNotIn('id', $order_id_updated)->where('bill_id', $bill->id)->delete();

            $bill->save();

            return $this->show($bill->id);
        }
    }


    public function delete($id)
    {
        if (Bill::where('id', $id)->delete()) {
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
        } else
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại bản ghi',
                'errors' => (object)[],
                'data' => null,
                'code' => 404
            ], 200);
    }

    public function filterSimple($request)
    {
        $where = '1=1 ';
        if (!is_null($request->id)) {
            $where .= " AND " . 'id' . " = " . $request->id;
        }
        #
        foreach ($this->filter as $filter_name => $filter_option) {
            if (!is_null($request->get($filter_name))) {
                if ($filter_option['query_type'] == 'like') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " LIKE '%" . $request->get($filter_name) . "%'";
                } elseif ($filter_option['query_type'] == 'from_to_date') {
                    if (!is_null($request->get('from_date')) || $request->get('from_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " >= '" . date('Y-m-d 00:00:00', strtotime($request->get('from_date'))) . "'";
                    }
                    if (!is_null($request->get('to_date')) || $request->get('to_date') != '') {
                        $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " <= '" . date('Y-m-d 23:59:59', strtotime($request->get('to_date'))) . "'";
                    }
                } elseif ($filter_option['query_type'] == '=') {
                    $where .= " AND " . $this->module['table_name'] . "." . $filter_name . " = '" . $request->get($filter_name) . "'";
                }
            }
        }

        return $where;
    }

    public function sort($request, $model)
    {
        if ($request->sorts != null) {
            foreach ($request->sorts as $sort) {
                if ($sort != null) {
                    $sort_data = explode('|', $sort);
                    $model = $model->orderBy($sort_data[0], $sort_data[1]);
                }
            }
        } else {
            $model = $model->orderBy('id', 'desc');
        }
        return $model;
    }
}
