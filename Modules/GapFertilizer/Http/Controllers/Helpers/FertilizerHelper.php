<?php

//namespace App\Http\Helpers;
namespace Modules\GapFertilizer\Http\Helpers;

use App\Models\Category;
use App\Models\Meta;
use App\Models\RoleAdmin;
use Auth;
use Illuminate\Support\Facades\Cache;
use Modules\EworkingJob\Models\Task;
use Session;
use View;

class FertilizerHelper
{

    public static function getPriceHarvest($harvest)
    {
        $thanh_tien = 0;
        if ($harvest->quantity_unit == 'tấn') {
            $thanh_tien = (int)$harvest->quantity * 1000;
            if ($harvest->price_unit == '/tấn') {
                $thanh_tien *= ((int)$harvest->price / 1000);
            } else {
                $thanh_tien *= (int)$harvest->price;
            }
        } elseif ($harvest->quantity_unit == 'kg') {
            $thanh_tien = (int)$harvest->quantity;
            if ($harvest->price_unit == '/tấn') {
                $thanh_tien *= ((int)$harvest->price / 1000);
            } else {
                $thanh_tien *= (int)$harvest->price;
            }
        }
        return $thanh_tien;
    }
}
