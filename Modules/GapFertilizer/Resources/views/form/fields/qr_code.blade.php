@if(isset($result))
    <?php
    $qr_code = base64_encode(\SimpleSoftwareIO\QrCode\Facades\QrCode::format('png')->size(100)->generate(json_encode([
        'product_id' => $result->id,
        'name_product' => $result->name,
        'license' => @$result->license,
        'date_manufacture' => @$result->date_manufacture,
        'expiration_date' => @$result->expiration_date,
        'production_address' => @$result->production_address,
        'control_person' => @$result->control_person,
        'ingredients' => @$result->ingredients,
        'code' => @$result->code,
        'default_show' => 'product info',
    ])))
    ?>
    <img src="data:image/png;base64, {!! $qr_code!!} ">
    <a href="data:application/octet-stream;base64,{!! $qr_code !!} " download="{{$result->id}}_{{str_slug(@$result->name,'_')}}_{{str_slug(@$result->admin->name,'_')}}.png">Tải QR code</a>
    (Thông tin sản phẩm)
@endif
