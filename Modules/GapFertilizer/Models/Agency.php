<?php


namespace Modules\GapFertilizer\Models;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $table = 'admin';
    public $timestamps = false;


    protected $fillable = [
        'name', 'tel', 'email', 'password', 'address', 'province_id', 'district_id', 'ward_id', 'note', 'admin_id', 'api_token', 'product_ids'
    ];

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }
    public function product_ids()
    {
        return $this->hasMany(Product::class,'product_ids','id');
    }
    public static function create(array $attributes = [])
    {
        $attributes['api_token'] = base64_encode(rand(1, 100) . time());
        $attributes['password_md5'] = @md5($attributes['password']);
        $model = static::query()->create($attributes);

        return $model;
    }
}
