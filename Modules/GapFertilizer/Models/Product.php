<?php

namespace Modules\GapFertilizer\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    protected $table = 'products';

    protected $fillable = [
        'name', 'image', 'image_present','packing','form','dosage','type','bonus_services',
        'created_at','image_extra','image_present2','date_manufacture',
        'license','expiration_date','production_address','ingredients','active_element','plant_object'
    ];





}