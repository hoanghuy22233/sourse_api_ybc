<?php

namespace Modules\GapFertilizer\Models;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{

    protected $table = 'warehouses';
    public $timestamps = false;

    protected $fillable = [
        'product_id', 'amount', 'unit_price', 'money', 'time', 'agency_id', 'amount_unit'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function type_product()
    {
        return $this->belongsTo(TypeProduct::class, 'type_product_id');
    }


}