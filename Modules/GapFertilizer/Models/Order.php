<?php


namespace Modules\GapFertilizer\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';


    protected $fillable = [
        'user_id', 'product_id', 'quantity', 'price', 'bill_id', 'unit', 'product_name', 'product_price', 'product_image'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function bill()
    {
        return $this->belongsTo(Bill::class, 'bill_id');
    }
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
