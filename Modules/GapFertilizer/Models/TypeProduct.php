<?php

namespace Modules\GapFertilizer\Models;

use Illuminate\Database\Eloquent\Model;

class TypeProduct extends Model {

    protected $table = 'type_products';
    public $timestamps = false;

    protected $fillable = [
        'name', 'image', 'image_present','packing','dosage','type','bonus_services','created_at','image_extra','image_present2'
    ];


    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }


}