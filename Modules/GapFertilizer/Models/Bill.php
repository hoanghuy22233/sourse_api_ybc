<?php


namespace Modules\GapFertilizer\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $table = 'bills';


    protected $fillable = [
        'user_id', 'type_product_id', 'quantity', 'total_price', 'created_at', 'admin_id', 'product_id'
    ];



    public function orders()
    {
        return $this->hasMany(Order::class, 'bill_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
