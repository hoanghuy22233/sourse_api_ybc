<?php



namespace Modules\GapFertilizer\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
	protected $table = 'provinces';
}
