<?php


namespace Modules\KitCareBooking\Models ;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment_logs';


    protected $fillable = [
        'booking_id', 'admin_id', 'reply','created_at','content','image','image_present'
    ];
    public function admins()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
    public function reply()
    {
        return $this->hasOne(Comment::class, 'id');
    }
    public function booking()
    {
        return $this->belongsTo(Booking::class, 'booking_id');
    }

}
