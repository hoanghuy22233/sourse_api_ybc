<?php


namespace Modules\GapFertilizer\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    public $timestamps = false;


    protected $fillable = [
        'name', 'tel', 'address', 'province_id', 'district_id', 'ward_id', 'agency_id'
    ];

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function ward()
    {
        return $this->belongsTo(Ward::class, 'ward_id');
    }
    public function bills()
    {
        return $this->hasMany(Bill::class, 'user_id','id');
    }

}
