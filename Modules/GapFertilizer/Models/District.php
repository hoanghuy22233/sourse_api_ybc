<?php


namespace Modules\GapFertilizer\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
	protected $table = 'districts';
}
