<?php

namespace Modules\GapFertilizer\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class GapFertilizerServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
//        $this->registerTranslations();
//        $this->registerConfig();
//        $this->registerFactories();
//        $this->loadMigrationsFrom(module_path('GapFertilizer', 'Database/Migrations'));

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            $this->registerViews();

            //  Custom setting
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }

        //  Nếu là trang admin/setting thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/setting') !== false) {
            $this->addSetting();
        }
    }

    public function addSetting()
    {
        \Eventy::addFilter('setting.custom_module', function ($module) {
            $module['tabs']['company_info'] = [
                'label' => 'Thông tin công ty',
                'icon' => '<i class="flaticon-mail"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'company_info', 'type' => 'textarea', 'class' => '', 'label' => 'Thông tin công ty', 'inner' => 'rows=15'],
                ]
            ];
            return $module;
        }, 1, 1);
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['product_view', 'product_add', 'product_edit', 'product_delete', 'product_publish',
                'booking_view', 'booking_add', 'booking_edit', 'booking_delete', 'booking_publish',
                'admin_view', 'admin_add', 'admin_edit', 'admin_delete', 'admin_publish',
                'order_view', 'order_add', 'order_edit', 'order_delete', 'order_publish',
                'bill_view', 'bill_add', 'bill_edit', 'bill_delete', 'bill_publish',
                'type_product_view', 'type_product_add', 'type_product_edit', 'type_product_delete', 'type_product_publish',
                'agency_view', 'agency_add', 'agency_edit', 'agency_delete', 'agency_publish',
                'warehouse_view','warehouse_add', 'warehouse_edit', 'warehouse_delete', 'warehouse_publish',]);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('gapfertilizer::partials.aside_menu.dashboard_after_product');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('gapfertilizer::partials.aside_menu.dashboard_after_booking');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('gapfertilizer::partials.aside_menu.dashboard_after_user');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('gapfertilizer::partials.aside_menu.dashboard_after_agency');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('gapfertilizer::partials.aside_menu.dashboard_after_warehouse');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('gapfertilizer::partials.aside_menu.dashboard_after_bill');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.user', function() {
            print '';
        }, 1, 1);

    }
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('GapFertilizer', 'Config/config.php') => config_path('gapfertilizer.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('GapFertilizer', 'Config/config.php'), 'gapfertilizer'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/gapfertilizer');

        $sourcePath = module_path('GapFertilizer', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/gapfertilizer';
        }, \Config::get('view.paths')), [$sourcePath]), 'gapfertilizer');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/gapfertilizer');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'gapfertilizer');
        } else {
            $this->loadTranslationsFrom(module_path('GapFertilizer', 'Resources/lang'), 'gapfertilizer');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('GapFertilizer', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
