@extends('themelaptop88::layouts.layout_homepage')
@section('main_content')
    <style>
        @media(max-width: 768px) {
            .bnews li .bsum {
                display: none;
            }
        }

        @media(min-width: 1300px) {
            .topic img, .topic object {
                display: inline-block;
            }
            @for($i = 0; $i < count($posts); $i++)
            @if($i % 2 == 0)
                .dd-{{ $i }} {
                display: inline-block;
                text-align: left;
            }
            .dd-{{ $i }} {
                display: inline-block;
                text-align: right;
            }
            @else
                .dd-{{ $i }} {
                display: inline-block;
                text-align: right;
            }
            .dd-{{ $i }} {
                display: inline-block;
                text-align: left;
            }
            @endif
            @endfor

            .fix-box {
                min-height: 300px !important;
            }

        }
        .bnews a div {
            max-height: 150px;
            overflow: hidden;
            text-align: center;
            display: inline-block;
        }

        .bnews li {
            padding-bottom: 0;
            border: 1px #ccc solid;
            border-radius: 4px;
            box-shadow: 1px 1px 2px #666;
            background: #fff;
            padding: 10px !important;
            text-align: center;
        }
        .list-launcher img {
            max-width: 59px;
        }
        @media(max-width: 768px) {
            .new-banner-bottom a {
                width: 100%;
                max-width: unset;
                flex: unset;
            }
        }
    </style>
@endsection
