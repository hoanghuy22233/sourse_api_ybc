<?php $number = \Modules\ThemeLaptop88\Http\Controllers\Frontend\OrderController::totalCart()?>
<a href="{{route('order.view')}}" class="cart" rel="nofollow" title="giỏ hàng">
    <i class="fas fa-shopping-cart"></i>
    <div class="content-minicart">
        <p>Giỏ hàng của bạn</p>
        <p class="cart-total">Có <span id="count_shopping_cart_store"><?=($number == '') ? 0 : $number?></span> sản phẩm</p>
    </div>
</a>
