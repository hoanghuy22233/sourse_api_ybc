<div class="phone_now_detail">
    <?php
    $dataShowrooms = CommonHelper::getFromCache('get_showroom_footer',['showrooms']);
    if (!$dataShowrooms) {
        $dataShowrooms = \Modules\ThemeLaptop88\Models\Showroom::all();
        CommonHelper::putToCache('get_showroom_footer', $dataShowrooms,['showrooms']);
    }
    ?>
    @foreach($dataShowrooms as $k=>$dataShowroom)
        <div class="phone_now_detail1">
            <div class="contact_now-title">
                <span style="line-height: 14px;">{!! $dataShowroom->address !!}</span>
                <a href="tel:{{ preg_replace('/\s+/', '', $dataShowroom->hotline) }}">
                    <img data-src="https://i.imgur.com/fmhOHUr.gif" width="30px" class="lazy">
                </a>
            </div>

        </div>
    @endforeach
</div>