<?php
$relate_products = CommonHelper::getFromCache('get_relate_products',['products']);
if (!$relate_products) {
    $relate_product_id = explode('|', trim(@$post->related_products, '|'));
    $relate_products = \Modules\ThemeLaptop88\Models\Product::whereIn('id', $relate_product_id)->where('status', 1)->orderBy('order_no', 'asc')->get();
    CommonHelper::putToCache('get_relate_products', $relate_products,['products']);
}

$cat = CommonHelper::getFromCache('categorys_find' . $post->category_id, ['categories']);
if (!$cat) {
    $cat = \Modules\ThemeLaptop88\Models\Category::find($post->category_id);
    CommonHelper::putToCache('categorys_find' . $post->category_id, $cat, ['categories']);
}


$products_hots = CommonHelper::getFromCache('get_products_hot' . @$cat->category_product_id, ['products']);
if (!$products_hots) {
    $products_hots = \Modules\ThemeLaptop88\Models\Product::where('featured', 1)->where('multi_cat', 'like', '%|' . @$cat->category_product_id . '|%')->where('status', 1)->take(5)->orderBy('order_no', 'asc')->get();
    CommonHelper::putToCache('get_products_hot' . @$cat->category_product_id, $products_hots, ['products']);
}
?>
<style>
    .ytms>p{
        text-align: justify;
        padding: 10px;
    }
</style>
<div class="newsright bgghi">
    <h4 style="
    text-align: center;
    padding: 15px;
    font-size: 20px;
    text-transform: uppercase;
    background: #308741; font-weight: bold; color: #fff;
">Sản phẩm liên quan</h4>
    <ul class="news-right2" style="background: #fff; border: 1px solid #fff576">
        @if(!empty($relate_products))
            @foreach($relate_products as $relate_product)
                <li style="border-bottom: 1px solid #ccc; display: flex;padding: 5px!important;">
                    <a class="img-news" style="width: 30%"
                       href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($relate_product, 'array') }}"
                       title="{{$relate_product->name}}">
                        <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($relate_product->image, 300, null) }}"
                             alt="{{$relate_product->name}}">
                    </a>
                    <div class="title_price_pro" style="width: 70%">
                        <h3 style="margin: 0; padding: 0 5px;    line-height: 1;">
                            <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($relate_product, 'array') }}"
                               title="{{$relate_product->name}}">{{$relate_product->name}}</a>
                        </h3>
                        <strike>{{number_format($relate_product->base_price, 0,'','.')}}<sup>đ</sup></strike>
                        <p style="font-weight: bold;color: red">{{number_format($relate_product->final_price, 0,'','.')}}
                            <sup>đ</sup></p>
                    </div>


                </li>
            @endforeach
        @endif
    </ul>
</div>
<div class="newsright spbccc bgghi">
    <h4 style="
    text-align: center;
    padding: 15px;
    font-size: 20px;
    text-transform: uppercase;
    background: #0388fc;font-weight: bold; color: #fff;
">Sản phẩm bán chạy</h4>
    <ul class="news-right2" style="background: #fff;border: 1px solid #fff576;">
        @if(!empty($products_hots))
            @foreach($products_hots as $products_hot)
                <li style="border-bottom: 1px solid #ccc; display: flex;padding: 5px!important;">
                    <a class="img-news" style="width: 30%"
                       href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($products_hot, 'array') }}"
                       title="{{$products_hot->name}}">
                        <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($products_hot->image, 300, null) }}"
                             alt="{{$products_hot->name}}">
                    </a>
                    <div class="title_price_pro" style="width: 70%">
                        <h3 style="margin: 0; padding: 0 5px;    line-height: 1;">
                            <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($products_hot, 'array') }}"
                               title="{{$products_hot->name}}">{{$products_hot->name}}</a>
                        </h3>
                        <strike>{{number_format($products_hot->base_price, 0,'','.')}}<sup>đ</sup></strike>
                        <p style="font-weight: bold;color: red">{{number_format($products_hot->final_price, 0,'','.')}}
                            <sup>đ</sup></p>
                    </div>


                </li>
            @endforeach
        @endif
    </ul>
</div>
<?php
$v = CommonHelper::getFromCache('widget_post_detail_sidebar_right', ['widgets']);
if ($v === false) {
    $v = \Modules\ThemeLaptop88\Models\Widget::where('location', 'post_detail_sidebar_right')->where('status', 1)->first();
    CommonHelper::putToCache('widget_post_detail_sidebar_right', $v, ['widgets']);
}
?>
@if(is_object($v))
    <div class="newsright ytms bgghi">
        <h4 style="
    text-align: center;
    padding: 15px;
    font-size: 20px;
    text-transform: uppercase;
    background: #d13317;font-weight: bold; color: #fff;
">{!! $v->name !!}</h4>
        <p>{!! $v->content !!}</p>
    </div>
@endif