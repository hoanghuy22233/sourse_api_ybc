<?php
$category = CommonHelper::getFromCache('get_category_by_experi', ['categories']);
if (!$category) {
    $category = Modules\ThemeLaptop88\Models\Category::where('status', 1)->where('type', 1)->where('show_homepage', 1)->orderBy('order_no', 'asc')->get();
    CommonHelper::putToCache('get_category_by_experi', $category, ['categories']);
}
$getIdCate = CommonHelper::getFromCache('get_all_index', ['posts']);
if (!$getIdCate) {
    $getIdCate = \Modules\ThemeLaptop88\Models\Post::all()->pluck('multi_cat', 'id');
    CommonHelper::putToCache('get_all_index', $getIdCate, ['posts']);
}
$data_id = '|';

?>
<style>
    .newest .flexnew {
        margin: 20px 0 !important;
        border-left: 5px solid #ccc;
        border-left: 5px solid red;
        background: #eee;
        padding: 10px 0 10px 10px;
    }

    .newest {
        padding: 0 15px !important;
        max-width: unset !important;
    }

    .bnews .bsum {
        height: 75px;
        overflow-y: hidden;
    }

    .bnews .more {
        display: block;
        padding: 10px 0;
    }

    .bnews a h4 {
        font: 18px/24px arial;
        margin: 20px 0 10px;
        color: #666;
        display: -webkit-box;
        max-width: 100%;
        height: 48px;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .bnews li{
        padding-bottom: 0;
    }
    @media (max-width: 767px){
        .bnews .b ul li {
            width: 45%;
        }
        }
</style>
<div class="f bnews">
    <div class="b newest">
        @if(!empty($category))
            @foreach($category as $k1=>$cat)
                <?php
                    $data = CommonHelper::getFromCache('get_posts_by_experi', ['posts']);
                    if (!$data) {
                        $data = \Modules\ThemeLaptop88\Models\Post::where('status', 1)->where('multi_cat', 'like','%|'.$cat->id.'|%')->limit(4)->orderBy('order_no', 'decs')->orderBy('id', 'decs')->get();
                        CommonHelper::putToCache('get_posts_by_experi', $data, ['posts']);
                    }
//                }
                ?>
            @if(!empty($data->toArray()))
                <div class="f flexL flexnew"><label><a href="{{route('cate.list', ['slug' => $cat->slug])}}">{{ucfirst(mb_strtoupper($cat->name))}}</a></label></div>
                <ul class="f padb">
                    @foreach($data as $v)
                        <li>
                            <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getPostSlug($v) }}"
                               title="{{$v->name}}">
                                <div>
                                    <img class="lazy" data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image, 300, null) }}"
                                         alt="{{$v->name}}"/></div>
                                <h4>{{$v->name, 50}}</h4>
                                {{--                            <h4>{{str_limit($v->name, 50)}}</h4>--}}
                            </a>
                            <div class="bsum">{{str_limit($v->intro, 180)}}</div>
{{--                            <a class="more" title="{{$v->name}}"--}}
{{--                               href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getPostSlug($v) }}">Tìm hiểu--}}
{{--                                thêm</a>--}}
                        </li>
                    @endforeach
                </ul>
@endif
            @endforeach
        @endif

    </div>
</div>

