<div class="f mcontact">
    <p><a class="fa-phone" href="tel:{{@$settings['phone']}}" rel="nofollow">Liên hệ mua hàng:
            <strong>{!! @$settings['hotline'] !!}</strong></a>
    </p>
    <p><a class="fa-phone" href="tel:{{@$settings['phone_bh']}}" rel="nofollow">Bảo hành:
            <strong>{{@$settings['phone_bh']}}</strong></a>
    </p>
    <p><a class="fa-phone" href="tel:{{@$settings['phone_kn']}}" rel="nofollow">Khiếu nại:
            <strong>{{@$settings['phone_kn']}}</strong></a>
    </p>
    @if(!empty($c))
        @foreach($c['location'] as $location)
            <?php
            $count_showroom = CommonHelper::getFromCache('showroom_count_by_location' . $location, ['showrooms']);
            if ($count_showroom === false) {
                $count_showroom = Modules\ThemeLaptop88\Models\Showroom::where('location', $location)->orWhere('location', mb_strtolower($location))->orWhere('location', ucfirst($location))->orWhere('location', ucwords($location))->count();
                CommonHelper::putToCache('showroom_count_by_location' . $location, $count_showroom, ['showrooms']);
            }
            ?>
            <p><a class="fa-showroom"
                  href="{{route('showroom.list', ['slug' => \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::convertSlug($location)])}}"
                  title="hệ thống showroom">Xem địa chỉ {{ $count_showroom }} showroom tại {{$location}}</a>
            </p>
        @endforeach
    @endif
</div>