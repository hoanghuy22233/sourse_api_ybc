<?php
$dataShowrooms = CommonHelper::getFromCache('get_showroom_footer', ['showrooms']);
if (!$dataShowrooms) {
    $dataShowrooms = Modules\ThemeLaptop88\Models\Showroom::all();
    CommonHelper::putToCache('get_showroom_footer', $dataShowrooms, ['showrooms']);
}
$p = [];
$c = ['values' => [], 'location' => []];
foreach ($dataShowrooms as $key => $t) {
    array_push($p, mb_strtoupper($t['location']));
    foreach ($p as $key => $l) {
        $p = array_unique($p);
    }
}
foreach ($p as $keyp => $l) {
    array_push($c['values'], [$l => []]);
    array_push($c['location'], $l);
    foreach ($dataShowrooms as $key => $t) {
        if (mb_strtoupper($l) == mb_strtoupper($t['location'])) {
            array_push($c['values'][$keyp][$l], $t);
        }
    }
}
$fg = [];
foreach ($c['values'] as $f) {
    foreach ($f as $g) {
        foreach ($g as $gj) {
            $fg[] = $gj;
        }
    }
}
?>
<style>
    @media (min-width: 992px) {
        #menu-show {
            display: block !important
        }
    }
</style>
<div class="f nav" id="menu-show" style="">
    <div class="b flexJus">
        <?php
        $cates = CommonHelper::getFromCache('categories_show_menu_no_parent', ['categories']);
        if (!$cates) {
            $cates = Modules\ThemeSTBD\Models\Category::where('status', 1)
                ->where('type', 5)->where('show_menu', 1)->orderBy('order_no', 'asc')
                ->where(function ($query) {
                    $query->where('parent_id', 0)->orwhere('parent_id', null);
                })->get();
            CommonHelper::putToCache('categories_show_menu_no_parent', $cates, ['categories']);
        }
        ?>
        @foreach($cates as $cate)
            <a style="text-align: center!important;" href="{{route('cate.list', ['slug' => $cate->slug])}}"
               title="{{$cate->name}}">
                <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($cate->image, 100, null) }}"
                     alt="{{$cate->name}}"/><span>{{$cate->name}}</span></a>
        @endforeach
    </div>
{{--    @include('themelaptop88::partials.menu_efect.menu_showrooms')--}}
</div>
<script>
    $(document).ready(function () {
        $('#touch-menu').click(function () {
            $('#menu-show').slideToggle();
            icon = $('header').find("#touch-menu");
            icon.toggleClass("touch-menu mclo")
        })
    })

</script>