@if(@$settings['show_showrooms_footer'] == 1)
    <style>
        #list-showroom-footer {
            padding: 15px 0;
            overflow: hidden;
            display: inline-block;
            margin-top: 3px;
            border-top: 1px dashed #aaa;
            margin-top: 20px;
        }

        #list-showroom-footer li:first-child {
            padding-left: 0;
            border: none;
        }

        #list-showroom-footer li:nth-child(5n + 1) {
            border-left: none;
            padding-left: 0;
            clear: both;
        }

        #list-showroom-footer li {
            float: left;
            width: 20% !important;
            padding: 0px 10px;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            border-left: solid 1px #ddd;
            height: 52px;
            margin-bottom: 20px;
        }

        #list-showroom-footer li b {
            display: block;
            font-size: 15px;
        }

        #list-showroom-footer span {
            font-size: 12px;
        }
        @media(max-width: 768px) {
            #list-showroom-footer li {
                width: 100% !important;
            }
        }
    </style>
    <div id="list-showroom-footer">
        <div class="b">
            <div class="container">
                <ul class="ul">
                    @foreach($showrooms as $showroom)
                        <li>
                            <b>{{ $showroom->name }}</b>
                            <span>{{ $showroom->address }} - Hotline: {{ $showroom->hotline }} - Làm việc 7h - 21h</span>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif