<style>
    @media (min-width: 992px) {
        #footer .fsr {
            margin: 0;
            width: 100%;
        }

        .footer-item {
            width: 25%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-left: 0;
            padding-right: 15px;
            display: inline-block;
            float: left;
        }
    }
</style>
<div class="fsr">
    <?php
    $footer_column_0 = CommonHelper::getFromCache('footer_oze', ['widgets']);
    if (!$footer_column_0) {
        $footer_column_0 = \Modules\ThemeLaptop88\Models\Widget::where('status', 1)->where('location', 'footer_0')->get();
        CommonHelper::putToCache('footer_oze', $footer_column_0, ['widgets']);
    }
    $footer_column_1 = CommonHelper::getFromCache('footer_one', ['widgets']);
    if (!$footer_column_1) {
        $footer_column_1 = \Modules\ThemeLaptop88\Models\Widget::where('status', 1)->where('location', 'footer_1')->get();
        CommonHelper::putToCache('footer_one', $footer_column_1, ['widgets']);
    }
    $footer_column_2 = CommonHelper::getFromCache('footer_two', ['widgets']);
    if (!$footer_column_2) {
        $footer_column_2 = \Modules\ThemeLaptop88\Models\Widget::where('status', 1)->where('location', 'footer_2')->get();
        CommonHelper::putToCache('footer_two', $footer_column_2, ['widgets']);
    }
    $footer_column_3 = CommonHelper::getFromCache('footer_thre', ['widgets']);
    if (!$footer_column_3) {
        $footer_column_3 = \Modules\ThemeLaptop88\Models\Widget::where('status', 1)->where('location', 'footer_3')->get();
        CommonHelper::putToCache('footer_two', $footer_column_3, ['widgets']);
    }
    ?>
    <div id="company" class="flexCol right-info-2" style="display: block!important;">
        <div class="f">
            <div class="col-md-3 footer-item">
                @if(!empty($footer_column_0))
                    @foreach($footer_column_0 as $widget)
                        <label class="toptit cen">{{$widget->name}}</label>
                        <p>{!! $widget->content !!}</p>
                    @endforeach
                @endif
            </div>
            <div class="col-md-3 footer-item">
                @if(!empty($footer_column_1))
                    @foreach($footer_column_1 as $widget)
                        <label class="toptit cen">{{$widget->name}}</label>
                        {!! $widget->content !!}
                    @endforeach
                @endif
            </div>
            <div class="col-md-3 footer-item">
                @if(!empty($footer_column_2))
                    @foreach($footer_column_2 as $widget)
                        <label class="toptit cen">{{$widget->name}}</label>
                        {!! $widget->content !!}
                    @endforeach
                @endif
            </div>
            <div class="col-md-3 footer-item">
                @if(!empty($footer_column_3))
                    @foreach($footer_column_3 as $widget)
                        <label class="toptit cen">{{$widget->name}}</label>
                        {!! $widget->content !!}
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    @include('themelaptop88::partials.footer.showrooms_footer', ['showrooms' => @$showrooms])
    <div class="flexJus f fcop">
        <div id="copyright">{!! @$settings['footer'] !!}</div>
    </div>
</div>