<?php
$footer_column_0 = CommonHelper::getFromCache('footer_oze', ['widgets']);
if (!$footer_column_0) {
    $footer_column_0 = \Modules\ThemeLaptop88\Models\Widget::where('status', 1)->where('location', 'footer_0')->get();
    CommonHelper::putToCache('footer_oze', $footer_column_0, ['widgets']);
}
$footer_column_1 = CommonHelper::getFromCache('footer_one', ['widgets']);
if (!$footer_column_1) {
    $footer_column_1 = \Modules\ThemeLaptop88\Models\Widget::where('status', 1)->where('location', 'footer_1')->get();
    CommonHelper::putToCache('footer_one', $footer_column_1, ['widgets']);
}
$footer_column_2 = CommonHelper::getFromCache('footer_two', ['widgets']);
if (!$footer_column_2) {
    $footer_column_2 = \Modules\ThemeLaptop88\Models\Widget::where('status', 1)->where('location', 'footer_2')->get();
    CommonHelper::putToCache('footer_two', $footer_column_2, ['widgets']);
}
$footer_column_3 = CommonHelper::getFromCache('footer_thre', ['widgets']);
if (!$footer_column_3) {
    $footer_column_3 = \Modules\ThemeLaptop88\Models\Widget::where('status', 1)->where('location', 'footer_3')->get();
    CommonHelper::putToCache('footer_two', $footer_column_3, ['widgets']);
}
$showrooms = CommonHelper::getFromCache('showroom_footer', ['showrooms']);
if (!$showrooms) {
    $showrooms = \Modules\ThemeLaptop88\Models\Showroom::all();
    CommonHelper::putToCache('showroom_footer', $showrooms, ['showrooms']);
}

$mien = [];
$showrooms_arr = [
    'values' => [],
    'location' => []
];
foreach ($showrooms as $key => $showroom) {
    array_push($mien, mb_strtoupper($showroom['location']));
    foreach ($mien as $key => $l) {
        $mien = array_unique($mien);
    }
}

foreach ($mien as $keyp => $l) {
    array_push($showrooms_arr['values'], [$l => []]);
    array_push($showrooms_arr['location'], $l);
    foreach ($showrooms as $key => $showroom) {
        if (mb_strtoupper($l) == mb_strtoupper($showroom['location'])) {
            array_push($showrooms_arr['values'][$keyp][$l], $showroom);
        }
    }
}
/*$fg = [];
foreach ($showrooms_arr['values'] as $f) {
    foreach ($f as $g) {
        foreach ($g as $gj) {
            $fg[] = $gj;
        }
    }
}
$fg = $showrooms;*/
?>
<style>
    .buttons.b-close {
        border-radius: 7px 7px 7px 7px;
        box-shadow: none;
        font: bold 131% sans-serif;
        padding: 0 6px 2px;
        float: right;
    }

    .buttons {
        margin-left: 12px;
        margin-right: -17px;
        background-color: #2b91af;
        border-radius: 10px;
        box-shadow: 0 2px 3px rgba(0, 0, 0, 0.3);
        color: #fff;
        cursor: pointer;
        display: inline-block;
        padding: 10px 20px;
        text-align: center;
        text-decoration: none;
    }

    #popups {
        display: none;
        overflow: hidden;
        background-color: #fff;
        border-radius: 10px 10px 10px 10px;
        box-shadow: 0 0 25px 5px #999;
        text-align: left;
        color: #111;
        display: block;
        padding: 25px 25px;
        margin: 40px auto;
        max-width: 67%;
    }

    .mms {
        top: 0px;
        display: none;
        background-color: rgba(0, 0, 0, 0.6);
        width: 100%;
        min-height: 100vh;
        position: fixed;
        z-index: 999;
    }

    #Maps {
        padding-right: 20px;
        overflow: hidden;
    }

    #footer > .b {
        display: table;
    }

    .fix-box {
        min-height: 300px;
        display: flex;
    }

    .fsl li {
        background: {{@$settings['footer_background']}};
    }

    .fsl li span:before {
        content: "\f061" !important;
    }

    @if(@$settings['footer_left'] == 0)
        #footer .fsl {
        display: none;
    }

    #footer .fsr {
        margin: auto;
    }

    @endif

    @media (max-width: 768px) {
        .fsl .citys {
            border: none;
        }
    }

    .incom div {
        padding-right: 15px;
    }

    #company {
        display: block !important;
    }

    .fsl li {
        position: relative;
        display: inline-block;
        padding-right: 33px;
    }

    .fsl li span {
        position: absolute;
        right: -14px;
    }

    footer#footer li {
        width: 100%;
    }

    footer#footer .bst .ai {
        width: 100%;
    }
    label.toptit.cen {
        font-weight: bold;
        text-align: left;
        border-bottom: 1px dashed #aaa;
        padding-bottom: 10px;
    }
</style>
<div class="mms">
    <div id="popups">
        <span class="buttons b-close"><span>X</span></span>
        <div id="Maps"></div>
    </div>
</div>

<footer id="footer">
    <div class="b">
        <div class="tagline cen"><img class="lazy" height="80"
                                      data-src="{{ asset('public/filemanager/userfiles/' . @$settings['tagline_footer']) }}"
                                      alt="Logo footer"/>
        </div>
        <div class="fix-box">
            <div class="fsl">
                @if(!empty($showrooms_arr))
                    @foreach($showrooms_arr['location'] as $location)
                        <div class="citys">
                            <label>{{$location}}</label>
                            <ul>
                                @foreach($showrooms as $showroom)
                                    @if(mb_strtoupper($location) == mb_strtoupper($showroom['location']))
                                        <li class="address-{{$showroom['id']}} address-toggle"
                                            data-key="{{$showroom['id']}}"
                                            style="margin-bottom: 20px; padding-left: 15px;border: 1px solid #eee;">{{$showroom['name']}}
                                            <span></span></li>
                                        @if($showroom->location==$showroom['location'])
                                            <div class="togle-{{$showroom['id']}} togle-location" style="display: none">
                                                <div class="bst">
                                                    <div class="ai">
                                                        <div class="sn">
                                                            {!! $showroom->address !!}
                                                        </div>
                                                    </div>
                                                    <div class="anv">
                                                        <ul>
                                                            {!! $showroom->staff !!}
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="bsf"
                                                     style="margin-bottom: 20px; display: inline-block;">
                                                    <div class="atc">
                                                        <a rel="nofollow" title="{{$showroom->name}}"
                                                           href="/{{$showroom->slug}}">Giới thiệu Showroom</a>
                                                        <img class="lazy"
                                                             data-src="{{CommonHelper::getUrlImageThumb($showroom->showroom_image, 405, null) }}">
                                                    </div>
                                                    <div class="abd">
                                                        <a rel="nofollow" title="bản đồ đường đi">Bản đồ đường
                                                            đi </a>
                                                        <img class="lazy"
                                                             data-src="{{CommonHelper::getUrlImageThumb($showroom->image_map, 405, null) }}">
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                @endif
            </div>
            @if(!empty($showrooms))
                @foreach($showrooms as $showroom)
                    <div id="showroommobi{{$showroom->id}}" class="bs mobi address-mobi"></div>
                @endforeach
            @endif

            @include('themelaptop88::partials.footer.column')
        </div>
    </div>
</footer>

@include('themelaptop88::partials.footer.script', ['showrooms' => $showrooms])
