<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/home.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/footer.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/slide.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/css/detailproduct.css')}}">
<link rel="stylesheet" href="{{URL::asset('public/frontend/themes/laptop88/css/themelaptop88.css')}}">

@if (isMobile())
    <link rel="stylesheet" href="{{URL::asset('public/frontend/themes/stbd/frontend/css/style-responsive.css')}}">
    <link rel="stylesheet" href="{{URL::asset('public/frontend/themes/laptop88/css/themelaptop88_category_home_m.css')}}">
@else
    <link rel="stylesheet" href="{{URL::asset('public/frontend/themes/laptop88/css/themelaptop88_category_home.css')}}">
@endif

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css">
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<script type="text/javascript" src="{{URL::asset('public/frontend/themes/stbd/front/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('public/frontend/themes/laptop88/js/themelaptop88.js')}}"></script>
{!! @$settings['frontend_head_code'] !!}
{{--<script type="application/ld+json">--}}
{{--    {--}}
{{--      "@context": "https://schema.org",--}}
{{--      "@type": "Store",--}}
{{--      "image": [--}}
{{--        "https://bephoangcuong.com/timthumb.php?src=https://bephoangcuong.com/public/filemanager/userfiles/1.logo/BepHC-logo-Web8.png"--}}
{{--       ],--}}
{{--      "@id": "https://bephoangcuong.com/",--}}
{{--      "name": "{{$settings['name']}}",--}}
{{--      "address": {--}}
{{--        "@type": "PostalAddress",--}}
{{--        "streetAddress": "{{$settings['address']}}",--}}
{{--        "addressLocality": "Quận Đống Đa",--}}
{{--        "addressRegion": "Hà Nội",--}}
{{--        "postalCode": "100000",--}}
{{--        "addressCountry": "VN"--}}
{{--      },--}}
{{--      "url": "https://bephoangcuong.com/",--}}
{{--      "priceRange": "VND",--}}
{{--      "telephone": "{{$settings['hotline']}}",--}}
{{--      "openingHoursSpecification": [--}}
{{--        {--}}
{{--          "@type": "OpeningHoursSpecification",--}}
{{--          "dayOfWeek": [--}}
{{--            "Monday",--}}
{{--            "Tuesday",--}}
{{--            "Wednesday",--}}
{{--            "Thursday",--}}
{{--            "Friday",--}}
{{--            "Saturday"--}}
{{--          ],--}}
{{--          "opens": "08:00",--}}
{{--          "closes": "21:00"--}}
{{--        },--}}
{{--        {--}}
{{--          "@type": "OpeningHoursSpecification",--}}
{{--          "dayOfWeek": "Sunday",--}}
{{--          "opens": "08:00",--}}
{{--          "closes": "21:00"--}}
{{--        }--}}
{{--      ]--}}
{{--    }--}}
{{--    </script>--}}

<!-- Global site tag (gtag.js) - Google Analytics -->
{{--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-163337035-1"></script>--}}
{{--<script>--}}
    {{--window.dataLayer = window.dataLayer || [];--}}
    {{--function gtag(){dataLayer.push(arguments);}--}}
    {{--gtag('js', new Date());--}}

    {{--gtag('config', 'UA-163337035-1');--}}
{{--</script>--}}
