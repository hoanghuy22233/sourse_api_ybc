<?php
$setting = CommonHelper::getFromCache('settings_homepage_category_product',['settings']);
if (!$setting){
    $setting =\Modules\ThemeLaptop88\Models\Settings::where('name','homepage_category_product')->where('type','homepage_tab')->first();
    CommonHelper::putToCache('settings_homepage_category_product', $setting,['settings']);
}
$setting = explode(",",@$setting->value);
?>
@if(count($setting) > 0)
    @foreach( $setting as $items)
        <?php
        $category_id = $items;
        $category = CommonHelper::getFromCache('homepage_category_'.$category_id,['categories']);
        if (!$category){
            $category =\Modules\ThemeLaptop88\Models\Category::where('id',$category_id)->first();
            CommonHelper::putToCache('homepage_category_'.$category_id, $category,['categories']);
        }
        $product =  CommonHelper::getFromCache('category_home_block_'.$category_id,['products']);
        if (!$product){
            $product = @\Modules\ThemeLaptop88\Models\Product::where('category_id',$category_id)->orderBy('order_no', 'asc')->limit(3)->get();
            CommonHelper::putToCache('category_home_block_'.$category_id, $product,['products']);
        }
        $filter = CommonHelper::getFromCache('category_home_filter_'.$category_id,['products']);
        if (!$filter){
            $filter['max'] = @\Modules\ThemeLaptop88\Models\Product::where('category_id',$category_id)->max('final_price');
            $filter['min'] = @\Modules\ThemeLaptop88\Models\Product::where('category_id',$category_id)->min('final_price');
            CommonHelper::putToCache('category_home_filter_'.$category_id, $filter,['products']);
        }
        $last_key = count($product);
        $i = 0;
        ?>
        @if($last_key > 1)
        <section class="product-home">
            <div class="b">
                <div class="pro-head d-flex flex-wrap justify-content-between">
                    <div class="title">
                        <h3><a href="{{route('cate.list', ['slug' => $category['slug']])}}">{{$category['name']}}</a></h3>
                    </div>
                </div>
                <div class="filter-price d-flex align-items-center">
                    <span>Mức giá:</span>
                    @if($filter['min'] < 5000000)
                        <ul class="d-flex flex-wrap m-0">
                            <li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=5000000&amp;filter=%2C">Dưới 5 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=7000000&amp;min=5000000&amp;filter=%2C">5 Triệu - 7 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=10000000&amp;min=7000000&amp;filter=%2C">7 Triệu - 10 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=15000000&amp;min=10000000&amp;filter=%2C">10 Triệu - 15 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?min=15000000&amp;filter=%2C">Trên 15 Triệu</a></li>
                        </ul>
                    @elseif($filter['min'] > 15000000)
                        <ul class="d-flex flex-wrap m-0">
                            <li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?min=15000000&amp;filter=%2C">Trên 15 Triệu</a></li>
                        </ul>
                    @elseif($filter['min'] > 10000000)
                        <ul class="d-flex flex-wrap m-0">
                            <li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=15000000&amp;min=10000000&amp;filter=%2C">10 Triệu - 15 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?min=15000000&amp;filter=%2C">Trên 15 Triệu</a></li>
                        </ul>
                    @elseif($filter['min'] > 7000000)
                        <ul class="d-flex flex-wrap m-0">
                            <li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=10000000&amp;min=7000000&amp;filter=%2C">7 Triệu - 10 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=15000000&amp;min=10000000&amp;filter=%2C">10 Triệu - 15 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?min=15000000&amp;filter=%2C">Trên 15 Triệu</a></li>
                        </ul>
                    @elseif($filter['min'] > 5000000)
                        <ul class="d-flex flex-wrap m-0">
                            <li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=7000000&amp;min=5000000&amp;filter=%2C">5 Triệu - 7 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=10000000&amp;min=7000000&amp;filter=%2C">7 Triệu - 10 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=15000000&amp;min=10000000&amp;filter=%2C">10 Triệu - 15 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?min=15000000&amp;filter=%2C">Trên 15 Triệu</a></li>
                        </ul>
                    @endif
                </div>

                <div class="p-list-type-1 d-flex">
                    <div class="p-img">
                        <a
                           href="{{route('cate.list', ['slug' => $category['slug']])}}"
                           title="{{$category['name']}}">
                            <img alt="{{$category['name']}}"
                                 class="lazy"
                                 data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$category['image'], 410, null) }}"
                                 src="{{$category['image']}}"/>
                        </a>
                    </div>
                </div>
                <div class="p-list-type-1 d-flex category-home-slide">
                    @foreach($product as $items)
                        <div class="item">
                            <div class="p-item js-p-item <?= (++$i == $last_key)?'item_end':''?>">
                                <a href="#" class="p-img">
                                    <img  data-img="{{$items->image}}"class="lazy" data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($items->image, 220, null) }}" alt="{{$items->name}}">
                                </a>
                                <div class="p-emtry">
                                    <a href="{{ Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($items) }}" class="p-name">{{$items->name}}</a>
                                    <span class="p-price"><b>@if($items->final_price == 0) Liên hệ @else {{number_format($items->final_price, 0, '.', '.')}}đ @endif</b></span>
                                    <span class="p-summary">{{$items->intro}}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        @endif
    @endforeach
@endif
<script>
        $(".category-home-slide").owlCarousel({
            autoPlay: false,
            items: 1.5,
            itemsMobile: [479, 1.5],
            stagePadding: 100
        });
</script>