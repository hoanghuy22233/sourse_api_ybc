<?php
$slug = urldecode(last(request()->segments()));
$category = CommonHelper::getFromCache('categories_slug' . $slug,['categories']);
if ($category === false) {
    $category = Modules\ThemeLaptop88\Models\Category::where('slug', $slug)->where('status', 1)->first();
    CommonHelper::putToCache('categories_slug' . $slug, $category,['categories']);
}
$newPosts = CommonHelper::getFromCache('get_newpost_by_post',['posts']);
if (!$newPosts) {
    $newPosts = Modules\ThemeLaptop88\Models\Post::where('status', 1)->where('slug', '<>', '#')
        ->orderBy('id', 'desc')->where('type_page', '<>', 'page_static')->limit(5)->get();
    CommonHelper::putToCache('get_newpost_by_post', $newPosts,['posts']);
}
$postViews = CommonHelper::getFromCache('get_postviews_by_post',['posts']);
if (!$postViews) {
    $postViews = Modules\ThemeLaptop88\Models\Post::where('status', 1)->where('slug', '<>', '#')
        ->orderBy('view_total', 'desc')->where('type_page', '<>', 'page_static')->limit(5)->get();
    CommonHelper::putToCache('get_postviews_by_post', $postViews,['posts']);
}
?>
{{--<div class="newsright bgghi">--}}
{{--<ul class="news-right2">--}}
{{--@if(!empty($newPosts))--}}
{{--@foreach($newPosts as $newPost)--}}
{{--<li>--}}
{{--<a class="img-news" href="{{route('cate.list', ['slug' => $newPost->slug])}}.html" title="{{$newPost->name}}">--}}
{{--<img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($newPost->image, 300, null) }}" alt="{{$newPost->name}}"></a>--}}
{{--<h3><a href="{{route('cate.list', ['slug' => $newPost->slug])}}.html" title="{{$newPost->name}}">{{$newPost->name}}</a></h3>--}}
{{--</li>--}}
{{--@endforeach--}}
{{--@endif--}}
{{--</ul>--}}
{{--</div>--}}
{{--<!--chi danh cho tin thuoc trang tin hay-->--}}
{{--<div class="newsright bgghi">--}}
{{--<ul class="news-right2">--}}
{{--@if(!empty($newPosts))--}}
{{--@foreach($newPosts as $newPost)--}}
{{--<li>--}}
{{--<a class="img-news" href="{{route('cate.list', ['slug' => $newPost->slug])}}.html" title="{{$newPost->name}}">--}}
{{--                        <img src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($newPost->image, 300, null) }}" alt="{{$newPost->name}}"></a>--}}
{{--<h3><a href="{{route('cate.list', ['slug' => $newPost->slug])}}.html" title="{{$newPost->name}}">{{$newPost->name}}</a></h3>--}}
{{--</li>--}}
{{--@endforeach--}}
{{--@endif--}}
{{--</ul>--}}
{{--</div>--}}
<!--chi danh cho tin thuoc trang tin hay-->
<div class="newsright">
    <label style="background: #fff577">Tư vấn xem nhiều nhất</label>
    <ul class="news-list" style="border: 1px solid #fff576">
        <?php
        $cate_id = explode('|', trim(@$post->multi_cat, '|'));

        $possts = CommonHelper::getFromCache('category_id_cate_id_first' . @$cate_id[0],['categories']);
        if (!$possts) {
            $possts = \Modules\ThemeLaptop88\Models\Category::where('id', $cate_id[0])->where('status', 1)->first();
            CommonHelper::putToCache('category_id_cate_id_first' . @$cate_id[0], $possts,['categories']);
        }

        $xemnhieu = CommonHelper::getFromCache('category_multi_cat_like_cate_id_get',['posts']);
        if (!$xemnhieu) {
            $xemnhieu = \Modules\ThemeLaptop88\Models\Post::where('multi_cat', 'like', '%|' . $cate_id[0] . '|%')->where('status', 1)->orderBy('view_total', 'desc')->limit(5)->get();
            CommonHelper::putToCache('category_multi_cat_like_cate_id_get', $xemnhieu,['posts']);
        }
        ?>
        @if(!empty($xemnhieu))
            @foreach($xemnhieu as $key => $postView)
                <li style="    padding: 5px;    list-style: inside;">
                    <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getPostSlug($postView) }}"
                       title="Nên mua bếp từ hay bếp hồng ngoại">{{$postView->name}}</a>
                </li>
            @endforeach
        @endif
    </ul>

</div>
<div id="endright" style="float: left; width: 100%;"></div>

<script>
    $(document).ready(function () {
        $('#menu_catalog ul a').each(function () {
            if ($(this).hasClass('a2act')) {
                $(this).parents('ul').css('display', 'block');
                $(this).parents('ul').prev('a').addClass('a2act');
            }
        })
    })
</script>