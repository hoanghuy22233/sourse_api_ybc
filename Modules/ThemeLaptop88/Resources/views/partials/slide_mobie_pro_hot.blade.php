<?php
$cat_ids_show_homepage = $settings['homepage_slide_category_product'];
$categories = CommonHelper::getFromCache('homepage_slide_category', ['categories']);
if (!$categories) {
    $categories = \Modules\ThemeLaptop88\Models\Category::whereIn('id', explode(',', $cat_ids_show_homepage))->where('status', 1)->get();
    CommonHelper::putToCache('homepage_slide_category', $categories, ['categories']);
}
$child_category = CommonHelper::getFromCache('homepage_slide_category_child', ['categories']);
if (!$child_category) {
    $child_category = \Modules\ThemeLaptop88\Models\Category::whereIn('parent_id', explode(',', $cat_ids_show_homepage))->where('status', 1)->orderBy('order_no', 'asc')->limit(3)->get();
    CommonHelper::putToCache('homepage_slide_category_child', $child_category, ['categories']);
}
$features = CommonHelper::getFromCache('features_homepage_slide_category', ['products']);
if (!$features) {
    $features = \Modules\ThemeLaptop88\Models\Product::whereIn('category_id', explode(',', $cat_ids_show_homepage))->where('status', 1)->orderBy('order_no', 'asc')->limit(7)->get();
    CommonHelper::putToCache('features_homepage_slide_category', $features, ['products']);
}
?>
@foreach($categories as $category)
    <section class="product-home product-slider">
        <div class="b">
            <div class="pro-head d-flex justify-content-between">
                <div class="title">
                    <h3><a href="{{route('cate.list', ['slug' => $category->slug])}}">{{$category->name}}</a></h3>
                </div>
            </div>
            @if(count($child_category) >= 1)
                <div class="filter-price d-flex align-items-center">
                    <span>Xem thêm:</span>
                    <div class="filter-link">
                        @foreach($child_category as $items)
                            <a href="{{route('cate.list', ['slug' => $items->slug])}}">{{$items->name}}</a>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="p-list-type-1 d-flex">
                @foreach($features as $k=>$featured)
                    <div class="item">
                        <div class="p-item">
                            <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($featured, 'array') }}"
                               title="{{@$featured->meta_title}}" class="p-img">
                                <img data-img="{{$featured->image}}" class="lazy"
                                     data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($featured->image, 220, null) }}"
                                     alt="{{$featured->name}}">
                            </a>
                            <a href="{{ Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($featured) }}"
                               class="p-name">{{$featured->name}}</a>
                            <span class="p-price"><b>@if($featured->final_price == 0) Liên
                                    hệ @else {{number_format($featured->final_price, 0, '.', '.')}}đ @endif</b></span>
                            <p class="p-promotion">{{$featured->intro_quick_view}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endforeach
<script>

    $(".product-slider .p-list-type-1").owlCarousel({
        autoPlay: false,
        items: 1.5,
        itemsMobile: [479, 1.5],
        stagePadding: 100
    });
</script>