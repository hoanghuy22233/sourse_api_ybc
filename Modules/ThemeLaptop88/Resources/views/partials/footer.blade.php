<?php
$dataFooter_0 = CommonHelper::getFromCache('get_footer_oze', ['widgets']);
if (!$dataFooter_0) {
    $dataFooter_0 = \Modules\ThemeLaptop88\Models\Widget::where('status', 1)->where('location', 'footer_0')->get();
    CommonHelper::putToCache('get_footer_oze', $dataFooter_0, ['widgets']);
}
$dataFooter_1 = CommonHelper::getFromCache('get_footer_one', ['widgets']);
if (!$dataFooter_1) {
    $dataFooter_1 = \Modules\ThemeLaptop88\Models\Widget::where('status', 1)->where('location', 'footer_1')->get();
    CommonHelper::putToCache('get_footer_one', $dataFooter_1, ['widgets']);
}
$dataFooter_2 = CommonHelper::getFromCache('get_footer_two', ['widgets']);
if (!$dataFooter_2) {
    $dataFooter_2 = \Modules\ThemeLaptop88\Models\Widget::where('status', 1)->where('location', 'footer_2')->get();
    CommonHelper::putToCache('get_footer_two', $dataFooter_2, ['widgets']);
}
$showrooms = CommonHelper::getFromCache('get_showroom_footer', ['showrooms']);
if (!$showrooms) {
    $showrooms = \Modules\ThemeLaptop88\Models\Showroom::all();
    CommonHelper::putToCache('get_showroom_footer', $showrooms, ['showrooms']);
}

$p = [];
$c = ['values' => [], 'location' => []];
foreach ($showrooms as $key => $t) {
    array_push($p, mb_strtoupper($t['location']));
    foreach ($p as $key => $l) {
        $p = array_unique($p);
    }
}
foreach ($p as $keyp => $l) {
    array_push($c['values'], [$l => []]);
    array_push($c['location'], $l);
    foreach ($showrooms as $key => $t) {
        if (mb_strtoupper($l) == mb_strtoupper($t['location'])) {
            array_push($c['values'][$keyp][$l], $t);
        }
    }
}
$fg = [];
foreach ($c['values'] as $f) {
    foreach ($f as $g) {
        foreach ($g as $gj) {
            $fg[] = $gj;
        }
    }
}

?>
<style>
    .buttons.b-close {
        border-radius: 7px 7px 7px 7px;
        box-shadow: none;
        font: bold 131% sans-serif;
        padding: 0 6px 2px;
        float: right;
    }

    .buttons {
        margin-left: 12px;
        margin-right: -17px;
        background-color: #2b91af;
        border-radius: 10px;
        box-shadow: 0 2px 3px rgba(0, 0, 0, 0.3);
        color: #fff;
        cursor: pointer;
        display: inline-block;
        padding: 10px 20px;
        text-align: center;
        text-decoration: none;
    }

    #popups {
        display: none;
        overflow: hidden;
        background-color: #fff;
        border-radius: 10px 10px 10px 10px;
        box-shadow: 0 0 25px 5px #999;
        text-align: left;
        color: #111;
        display: block;
        padding: 25px 25px;
        margin: 40px auto;
        max-width: 67%;
    }

    .mms {
        top: 0px;
        display: none;
        background-color: rgba(0, 0, 0, 0.6);
        width: 100%;
        min-height: 100vh;
        position: fixed;
        z-index: 999;
    }

    #Maps {
        padding-right: 20px;
        overflow: hidden;
    }

    #footer > .b {
        display: table;
    }

    .fsl li {
        background: {{@$settings['footer_background']}};
    }

    .fix-box {
        min-height: 300px;
        display: flex;
    }

    @if(@$settings['footer_left'] == 0)
        #footer .fsl {
            display: none;
        }
        #footer .fsr {
            margin: auto;
            width: 100%
        }
    @endif

    @media (max-width: 768px){
        .fsl .citys {
            border: none;
        }
    }
    .incom div{
        padding-right: 15px;
    }
    div#Maps iframe {
        width: 100%!important;
    }
    .bs > .bsf img{
        max-width:unset!important
    }
</style>
<div class="mms">
    <div id="popups">
        <span class="buttons b-close"><span>X</span></span>
        <div id="Maps"></div>
    </div>
</div>
@if (!isMobile())
<div class="newleter">
    <div class="b d-flex align-item-center">
        <div class="newleter-text">
            <h4>Kênh thông tin</h4>
            <span>Mời bạn nhập email để nhận những thông tin khuyến mại mới nhất từ Bếp Hoàng Cương</span>
        </div>
        <form action="/send/contact" class="newleter-form">
            <input id="email_newsletter" type="text" name="email" placeholder="Nhập email của bạn " class="">
            <button type="submit" id="now_submit" class="newleter-btn"><i class="fas fa-long-arrow-alt-right"></i></button>
        </form>
    </div>
</div>
@endif
<footer id="footer">
    <div class="b">
        <div class="tagline cen"><img class="lazy" height="80"
                                      data-src="{{ asset('public/filemanager/userfiles/' . @$settings['tagline_footer']) }}"
                                      alt="Logo footer"/>
        </div>
        <div class="fix-box">
            {{--           Cột trái--}}
            <div class="fsl">
                @if(!empty($c))
                    @foreach($c['location'] as $location)
                        <div class="citys">
                            <label>{{$location}}</label>
                            <ul>
                                @foreach($fg as $gk)
                                    @if(mb_strtoupper($location) == mb_strtoupper($gk['location']))
                                        <li data-id="showroom{{$gk['id']}}">{{$gk['name']}}<span></span></li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                @endif
            </div>
            @if(!empty($showrooms))
                @foreach($showrooms as $data)
                    <div id="showroommobi{{$data->id}}" class="bs mobi address-mobi"></div>
                @endforeach
            @endif




            {{--           Cột phải--}}
            <div class="fsr">
                <div id="company" class="flexCol right-info-2">
                    <div class="f">
                        <div class="muahang">
                            @if(!empty($dataFooter_0))
                                @foreach($dataFooter_0 as $data)
                                    <label class="toptit cen">{{$data->name}}</label>
                                    <p>{!! $data->content !!}</p>
                                @endforeach
                            @endif
                        </div>
                        <div class="incom">
                            <div>
                                @if(!empty($dataFooter_0))
                                    @foreach($dataFooter_1 as $data)
                                        <label class="toptit cen">{{$data->name}}</label>
                                        {!! $data->content !!}
                                    @endforeach
                                @endif
                            </div>
                            <div>
                                @if(!empty($dataFooter_2))
                                    @foreach($dataFooter_2 as $data)
                                        <label class="toptit cen">{{$data->name}}</label>
                                        {!! $data->content !!}
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @if(!empty($showrooms))
                    @foreach($showrooms as $data)
                        <div id="showroom{{$data->id}}" class="bs"></div>
                    @endforeach
                @endif

                @include('themelaptop88::partials.footer.showrooms_footer', ['showrooms' => @$showrooms])


                <div class="flexJus f fcop">
                    <div id="copyright">{!! @$settings['footer'] !!}</div>
                </div>
            </div>
        </div>
    </div>
</footer>

@include('themelaptop88::partials.footer.script', ['showrooms' => @$showrooms])

<?php

$v = \App\Http\Helpers\CommonHelper::getFromCache('banners_banner_footer_page', ['banners']);
if ($v === false) {
    $v = \Modules\ThemeLaptop88\Models\Banner::select(['image', 'link'])->where('location', 'banner_footer_page')->where('status', 1)->first();
    \App\Http\Helpers\CommonHelper::putToCache('banners_banner_footer_page', $v, ['banners']);
}
?>
@if(is_object($v))
    <a href="{{ $v->link }}"><img class="lazy" style="    margin: auto;display: block;" data-src="{{ asset('public/filemanager/userfiles/' . $v->image) }}"></a>
@endif
