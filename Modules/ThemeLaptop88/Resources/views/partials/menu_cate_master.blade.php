@php
    $category = Modules\ThemeLaptop88\Models\Category::where('status', 1)->where('show_menu', 1)
        ->where('type', 5)->orderBy('order_no', 'asc')
        ->where(function ($query){
            $query->where('parent_id', 0)->orwhere('parent_id', null);
        })->get();
@endphp
<header class="">
    <div class="b f">
        <div class="flexJus">
            @if(@$settings['logo_position']==1)
                <div>
                    <style>
                        .tim {
                            width: 100% !important;
                        }
                    </style>
                    @include('themelaptop88::partials.logo')
                    @include('themelaptop88::partials.search')
                </div>

            @endif


            <div class="tc flexJus">
                @foreach($category as $data)
                    <a href="{{route('cate.list', ['slug' => $data->slug])}}" title="{{$data->name}}">
                        <img class="lazy"
                             data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($data->banner, '60', null) }}"/><span>{{$data->name}}</span></a>
                @endforeach
            </div>


            @if(@$settings['logo_position']==0)
                <style>
                    .tim {
                        width: 100% !important;
                    }
                </style>
                <div>
                    @include('themelaptop88::partials.logo')
                    @include('themelaptop88::partials.search')
                </div>
            @endif

        </div>
    </div>
</header>
