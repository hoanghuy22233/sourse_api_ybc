<header class="">
    <div class="head">
        <div class="b new-header-container">
            <div class="new-header-top b">
                <div class="header-showroom header-top-item" id="view-covid">
            <span class="top-header-span animated-linear-gradient show-hide" data-div="laptop88-showroom_online">
              <i class="fa fa-phone fa-flip-horizontal"></i>
              Liên hệ cửa hàng
            </span>
                    <div id="laptop88-showroom_online">
                        <div id="showroom_online_new">
                            <div class="he_thong_showroom-container">
                                <div class="he_thong_showroom-row">
                                    <?php
                                    $dataShowrooms = CommonHelper::getFromCache('get_showroom_footer', ['showrooms']);
                                    if (!$dataShowrooms) {
                                        $dataShowrooms = \Modules\ThemeLaptop88\Models\Showroom::all();
                                        CommonHelper::putToCache('get_showroom_footer', $dataShowrooms, ['showrooms']);
                                    }
                                    ?>
                                    @foreach($dataShowrooms as $k=>$dataShowroom)
                                        <div class="showroom-container">
                                            <p><span class="showroom-table-title">{!! $dataShowroom->name !!}</span></p>
                                            <p><span class="showroom-table-address"><i class="fa fa-map-marker"></i> {!! $dataShowroom->address !!}</span>
                                            </p>

                                            <div class="covid-19">
                                                <p>
                                                    <a href="https://zalo.me/{{ preg_replace('/\s+/', '', $dataShowroom->hotline) }}"
                                                       target="_blank" rel="nofollow"><span class="chat-zalo"><i
                                                                    class="fas fa-comment-dots"></i> Zalo</span></a>
                                                    <span class="phone-color">{{ preg_replace('/\s+/', '', $dataShowroom->hotline) }}</span>
                                                </p>

                                                <p><a href="tel:{{ preg_replace('/\s+/', '', $dataShowroom->hotline) }}"
                                                      target="_blank" rel="nofollow"><span class="chat-zalo"><i
                                                                    class="fas fa-comment-dots"></i> Hotline</span></a>
                                                    <span class="phone-color">{{ preg_replace('/\s+/', '', $dataShowroom->hotline) }}</span>
                                                </p>

                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-top-item" id="support_top">
            <span class="top-header-span animated-linear-gradient show-hide" data-div="laptop88-support_header">
              <i class="fa fa-phone fa-flip-horizontal"></i>
              Tư vấn mua hàng	</span>
                    <div id="laptop88-support_header">
                        <div id="h_menu_sub_on1" class="h_menu_sub hOnline">
                            <?php
                            $tu_van_mua_hang = CommonHelper::getFromCache('tu_van_mua_hang', ['widgets']);
                            if (!$tu_van_mua_hang) {
                                $tu_van_mua_hang = \Modules\ThemeLaptop88\Models\Widget::select('content')->where('location', 'tu_van_mua_hang')->where('status', 1)->first();
                                CommonHelper::putToCache('tu_van_mua_hang', $tu_van_mua_hang, ['widgets']);
                            }
                            ?>
                            @if(is_object($tu_van_mua_hang))
                                {!! $tu_van_mua_hang->content !!}
                            @endif
                        <!--hOnline-order-->
                        </div>
                        <!--h_menu_sub-->
                    </div>
                </div>
            </div>
            <script>
                $('.show-hide').click(function () {
                    $('#' + $(this).data('div')).slideToggle();
                });
            </script>
        </div>

        <div class="b flexJus mobie_padding">
            <div class="flexJus hleft">
                @if(@$settings['logo_position']==1)
                    @include('themelaptop88::partials.logo')
                @endif
                @include('themelaptop88::partials.search')
                @include('themelaptop88::partials.cart_total')
            </div>
            <div class="flexR hright">

                <nav class="a">
                    <ul id="menu" class="nav-menu">
                        @php
                            $menus = \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getMenusByLocation('main_menu', 8, false);
                        @endphp
                        @if(!empty($menus))
                            @foreach($menus as $menu1)
                                @if($menu1['link'] == '#showroom-menu')
                                    <li class="nav-item menu-item menu-showroom">
                                        <a class="nav-link hover1" href="#" rel="nofollow">
                                            <img data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($menu1['thumb'], 100, null) }}"
                                                 alt="{{$menu1['name']}}" class="lazy"/>
                                            <span class="menu-hover"
                                                  style="{{ 'color:'. @$cate->color }}">Showroom</span>
                                        </a>
                                        <ul class="sub-menu">
                                            <?php
                                            $dataShowrooms = CommonHelper::getFromCache('get_showroom_footer', ['showrooms']);
                                            if (!$dataShowrooms) {
                                                $dataShowrooms = Modules\ThemeLaptop88\Models\Showroom::all();
                                                CommonHelper::putToCache('get_showroom_footer', $dataShowrooms, ['showrooms']);
                                            }
                                            $p = [];
                                            $c = ['values' => [], 'location' => []];
                                            foreach ($dataShowrooms as $key => $t) {
                                                array_push($p, mb_strtoupper($t['location']));
                                                foreach ($p as $key => $l) {
                                                    $p = array_unique($p);
                                                }
                                            }
                                            foreach ($p as $keyp => $l) {
                                                array_push($c['values'], [$l => []]);
                                                array_push($c['location'], $l);
                                                foreach ($dataShowrooms as $key => $t) {
                                                    if (mb_strtoupper($l) == mb_strtoupper($t['location'])) {
                                                        array_push($c['values'][$keyp][$l], $t);
                                                    }
                                                }
                                            }
                                            $fg = [];
                                            foreach ($c['values'] as $f) {
                                                foreach ($f as $g) {
                                                    foreach ($g as $gj) {
                                                        $fg[] = $gj;
                                                    }
                                                }
                                            }
                                            ?>
                                            @if(!empty($c))
                                                @foreach($c['location'] as $location)
                                                    <?php
                                                    $shoroomLocation = CommonHelper::getFromCache('showroom_location' . $location, ['showrooms']);
                                                    if ($shoroomLocation === false) {
                                                        $shoroomLocation = Modules\ThemeLaptop88\Models\Showroom::where('location', $location)->orWhere('location', mb_strtolower($location))
                                                            ->orWhere('location', ucfirst($location))->orWhere('location', ucwords($location))->get();
                                                        CommonHelper::putToCache('showroom_location' . $location, $shoroomLocation, ['showrooms']);
                                                    }
                                                    ?>
                                                    <li class="sub-menu-item">
                                                        <a class="fa-showroom"
                                                           href="{{route('showroom.list', ['slug' => Modules\ThemeLaptop88\Http\Helpers\CommonHelper::convertSlug($location)])}}"
                                                           title="hệ thống showroom">Xem địa
                                                            chỉ {{count($shoroomLocation)}} Showroom
                                                            tại {{$location}}</a>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </li>
                                @else
                                    <li class="nav-item menu-{{ $slug = str_slug($menu1['name'], '_') }} menu-item">
                                        <a class="nav-link hover1" href="{{ $menu1['link'] }}" rel="nofollow"
                                           title="{{$menu1['name']}}">
                                            <img data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($menu1['thumb'], 100, null) }}"
                                                 alt="{{$menu1['name']}}" class="lazy"/>
                                            <span class="menu-hover"
                                                  style="{{ 'color:'. @$cate->color }}">{{$menu1['name']}}</span>
                                        </a>
                                        @if(!empty($menu1['childs']))
                                            <?php
                                            $menu2_col = !empty($settings['menu_lv2_col']) ? $settings['menu_lv2_col'] : 2;
                                            $col_width = 'calc(100% / ' . $menu2_col . ');';
                                            ?>
                                            <ul class="sub-menu" style="width: 500px">
                                                @foreach($menu1['childs'] as $c => $menu2)
                                                    <li class="sub-menu-item" style="width: {{$col_width}}">
                                                        <a href="{{$menu2['link']}}">{{$menu2['name']}}</a>
                                                        @if(!empty($menu2['childs']))
                                                            <ul class="sub-menu3">
                                                                @foreach($menu2['childs'] as $c3 => $menu3)
                                                                    <li class="sub-menu-item3">
                                                                        <a href="{{$menu3['link']}}">{{$menu3['name']}}</a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            </ul>

                                        @endif
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                </nav>
                @if(@$settings['logo_position']==0)
                    @include('themelaptop88::partials.logo')
                @endif
                <div style="clear: both; "></div>
                <?php
                $tongdai = CommonHelper::getFromCache('widgets_location_tong_dai', ['widgets']);
                if (!$tongdai) {
                    $tongdai = \Modules\ThemeLaptop88\Models\Widget::where('location', 'tongdai')->first();
                    CommonHelper::putToCache('widgets_location_tong_dai', $tongdai, ['widgets']);
                }
                ?>
                @if($tongdai->status==1)
                    <div class="tongdai">
                        {!! @$tongdai->content !!}
                    </div>
                @endif

            </div>
            <i id="touch-menu" class="touch-menu"></i>
        </div>
    </div>
    @include('themelaptop88::partials.show_dv')

    @if(isMobile())
        @if(@$settings['option_menu'] == 1)
            @include('themelaptop88::partials.menu_efect.menu_dropdown')
        @endif
    @endif
</header>
<style>

    li.sub-menu-item {
        position: relative;
    }

    ul.sub-menu3 {
        width: 100%;
    }

    li.sub-menu-item3 {
        background: #fff;
        width: 50%;
        margin-bottom: 2px;
    }

    li.sub-menu-item ul.sub-menu3 {
        display: flex;
        flex-wrap: wrap;
        background: #fff;
        width: 100%;
    }

    li.sub-menu-item3 > a {
        font-weight: normal !important;
    }
</style>
<script>
    $(document).ready(function () {
        $('#laptop88-support_header .h0nline-title-tab .hotline-title').hover(function () {
            var h0nline_id = $(this).attr('data-id');
            $('#laptop88-support_header .h0nline-title-tab .hotline-title').removeClass('hovered');
            $(this).addClass('hovered');

            $('#laptop88-support_header .h0nline-item .hotline-wrap').css('display', 'none');

            $('#' + h0nline_id).css('display', 'block');
        })
    })
</script>
