

<style>
    .search1{
        color: #000;
    }
</style>
<form style="" class="tim" method="get" action="{{route('search')}}">
    <input class="search1" type="text" value="" autocomplete="off" name="key" id="key" placeholder="Tìm kiếm...">
    <ul class="search-box"></ul>
    <button class="checks" type="submit"><i></i></button>
</form>
<script>
    $('.tim').on('submit', function (e) {
        if ($('#key').val() == ''){
            e.preventDefault();
            alert('Bạn chưa nhập dữ liệu !');
        }
    })
    $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
    $('#key').keyup(function (e) {
        var getData = $(this).val();
        ajaxSearch(getData);
    })
    function ajaxSearch(data) {
        let getUrl = $('.tim').attr('action');
        $.ajax({
            url: getUrl + '?keyword='+data,
            type: 'GET',
            success:function (data) {
                if ((data.success == true && data.result == '') || (data.success == false && data.result == '')){
                    $('ul.search-box').css('display', 'none');
                }
                else {
                    $('ul.search-box').css('display', 'block');
                    $('ul.search-box').html(data.result);
                }
            },
            error: function (data) { }
        })
    }
</script>