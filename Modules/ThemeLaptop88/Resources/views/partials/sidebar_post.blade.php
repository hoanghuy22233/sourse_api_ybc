<aside class="sidebar left left-content @if(isset($post) && count($product_sidebar) > 0) col-md-3 col-sm-4 col-xs-12 @endif" @if(!isset($post) || count($product_sidebar) == 0) style="display: none;" @endif>
    @if(isset($post))
        <div class="aside-filter" style="padding: 10px 0px 0px 0px;">
            <div class="aside-hidden-mobile">
                <aside class="aside-item banner hidden-xs">
                    <div class="aside-title" style="height: 30px; ">
                        <h2 class="title-head margin-top-0" style="border-bottom: 1px solid #e5e5e5;"><span style="color: #000000;">Sản phẩm liên quan</span></h2>
                    </div>
                    <div class="aside-content filter-group">
                        @foreach($product_sidebar as $item)
                            <div class="product-box product-box-edits">
                                <div class="product-thumbnail">
                                    <a href="{{ URL::to($item->slug) }}"
                                       title="{{$item -> name}}">
                                        <picture>
                                            <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, 263, 197) }}"
                                                 class="img-responsive center-block"/>
                                        </picture>
                                    </a>
                                </div>
                                <div class="product-info">
                                    <h3 class="product-name">
                                        <a href="{{ URL::to($item->slug) }}"
                                           title="{{$item -> name}}">{{$item -> name}}</a></h3>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </aside>
            </div>
        </div>
    @endif

    @if(isset($category) && $category->banner_sidebar != '')
        <aside class="aside-item banner hidden-xs">
            <div class="aside-content">
                <div class="image-effect">
                    <a href="#">
                        <picture>
                            <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($category->banner_sidebar, 263, 197) }}"
                                 class="img-responsive center-block" alt="{{$category->name}}"/>
                        </picture>
                    </a>
                </div>
            </div>
        </aside>
    @endif

    @if(isset($post) && $post->banner_sidebar != '' && $post->banner_sidebar_id != 0)
        @php $data = \Modules\ThemeLaptop88\Models\Product::find($post->banner_sidebar_id); @endphp
        @if(is_object($data))
            <aside class="aside-item banner hidden-xs">
                <div class="aside-content">
                    <div class="image-effect">
                        <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($data) }}">
                            <picture>
                                <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($post->banner_sidebar, 263, 197) }}"
                                     class="img-responsive center-block"/>
                            </picture>
                        </a>
                    </div>
                </div>
            </aside>
        @endif
    @endif


</aside>
<style>
    .aside-content {
        padding-bottom: 20px;
        padding-left: 10px;
        padding-right: 10px;
    }
</style>