<?php
    $brans = CommonHelper::getFromCache('get_brans');
    if (!$brans){
        $brans = \Modules\ThemeLaptop88\Models\Manufacturer::where('status', 1)->orderBy('order_no', 'asc')->get();
        CommonHelper::putToCache('get_brans', $brans);
    }

$show_bran_homepage = CommonHelper::getFromCache('settings_name_show_bran_homepage_type_homepage_tab',['settings']);
if (!$show_bran_homepage){
    $show_bran_homepage = @\Modules\ThemeLaptop88\Models\Settings::where('name','show_bran_homepage')->where('type','homepage_tab')->first()->value;
    CommonHelper::putToCache('settings_name_show_bran_homepage_type_homepage_tab', $show_bran_homepage,['settings']);
}
?>
<div class="b logo-container">
    <?php
    $home_top_address = CommonHelper::getFromCache('home_top_address',['widgets']);
    if (!$home_top_address) {
        $home_top_address = \Modules\ThemeLaptop88\Models\Widget::where('location', 'home_top_address')->where('status', 1)->first();
        CommonHelper::putToCache('home_top_address', $home_top_address,['widgets']);
    }
    ?>
        @if(is_object($home_top_address))
            <div class="new-banner-bottom">
                {!! $home_top_address->content !!}
            </div>
        @endif
    <div class="list-launcher">
        @if($show_bran_homepage == 1)
            <div >
                @if(!empty($brans))
                    @foreach($brans as $data)
                        <a href="{{route('manufacturer.detail', ['slug' => $data->slug])}}" title="{{$data->name}}">
                            <img class="lazy" data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($data->image, 153, null) }}"
                                 alt="{{$data->name}}">
                        </a>
                    @endforeach
                @endif
                @if(!empty($brans))
                    @foreach($brans as $data)
                        <a href="{{route('manufacturer.detail', ['slug' => $data->slug])}}" title="{{$data->name}}">
                            <img class="lazy" data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($data->image, 153, null) }}"
                                 alt="{{$data->name}}">
                        </a>
                    @endforeach
                @endif
            </div>
        @endif
    </div>
</div>


<script>
    $(".list-launcher > div").owlCarousel({
        autoPlay: !0,
        items: 6,
        navigation: true,
        pagination: false,
        itemsTablet: [768, 3],
        itemsMobile: [479, 3],
        responsiveRefreshRate: 100,
        navigationText: ["<i class='glyphicon glyphicon-menu-left'></i>", "<i class='glyphicon glyphicon-menu-right'></i>"]
    })
</script>