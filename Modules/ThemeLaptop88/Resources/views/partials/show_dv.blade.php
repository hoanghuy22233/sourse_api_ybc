<?php
$cate_dv = CommonHelper::getFromCache('category_find_id_121', ['categories']);
if ($cate_dv === false) {
    $cate_dv = \Modules\ThemeLaptop88\Models\Category::find(121);
    CommonHelper::putToCache('category_find_id_121', $cate_dv, ['categories']);
}

$post_dv = CommonHelper::getFromCache('post_multi_cat_like'.@$cate_dv->id, ['posts']);
if (!$post_dv) {
    $post_dv = \Modules\ThemeLaptop88\Models\Post::where('multi_cat', 'LIKE', '|%'.@$cate_dv->id.'%|')->where('show_home', 1)->orderBy('order_no', 'asc')->where('type_page', 'page_static')->limit(4)->get();
    CommonHelper::putToCache('post_multi_cat_like'.@$cate_dv->id, $post_dv, ['posts']);
}
?>


@if(!empty($post_dv))
    <div id="dichvu" class="f dv-menu flexCen" style="display:none;">
        <style>

            .dv-menu {
                background: #fff;
                padding: 20px 0;
            }

            .dv-menu figure {
                background: #fff;
                padding: 10px 0;
            }

            .dv-menu figure a {
                margin: 0 25px;
            }

            .dv-menu figure img {
                border: 1px solid #ddd;
            }

            .dv-menu figure a span {
                display: block;
                font: bold 18px/30px arial;
                color: #1063a5;
            }

            .dv-menu figure a span:after {
                content: "\f105";
                font: 18px/20px FontAwesome;
                padding-left: 10px;
            }

            .dv-menu p {
                text-align: center;
                margin: 40px 0 20px;
            }

            .dv-menu p span {
                display: inline-block;
                border: 1px solid #888;
                font: 16px/35px arial;
                padding: 0 25px;
                color: #888;
                border-radius: 5px;
                cursor: pointer;
            }
            @media(max-width: 768px){
                div#dichvu {
                    position: absolute;
                    top: 435px;
                    z-index: 999;
                }
            }
        </style>
        <figure class="flexCol">
            <div class="flexJus">
                @foreach($post_dv as $post)
                    <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getPostSlug($post) }}"
                       title="{{$post->name}}" rel="nofollow"><img class="lazy"
                                data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image, 280, null) }}"
                                alt="{{$post->name}}"/><span>{{$post->name}}</span></a>
                @endforeach
            </div>
            <p onclick="closemenudv();"><span>Đóng</span></p>
        </figure>
    </div>
@endif
<script>
    function showmenudv() {
        $('#dichvu').slideToggle('1000');
    }



    function closemenudv() {
        $('#dichvu').hide('1000');
    }
</script>
