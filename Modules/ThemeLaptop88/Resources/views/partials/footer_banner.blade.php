<?php
$banners_bottom = CommonHelper::getFromCache('get_banners_bottom',['banners']);
if (!$banners_bottom) {
    $banners_bottom = \Modules\ThemeLaptop88\Models\Banner::where('status', 1)->where('location', 'homepage_banner_footer')->orderBy('order_no', 'ASC')->get();
    CommonHelper::putToCache('get_banners_bottom', $banners_bottom,['banners']);
}
?>
<section class="banner-bottom-home my-2">
    <div class="b clearfix d-flex justify-content-between">
        @if(!empty($banners_bottom) || isset($banners_bottom))
            @foreach($banners_bottom as $data)
                <div class="col">
                    <a href="{{url($data->link)}}" title="{{$data->name}}">
                        <img class="lazy" data-src="{{\Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getUrlImageThumbNoCut($data->image)}}"
                             alt="{{$data->name}}"/>
                    </a>
                </div>
            @endforeach
        @endif
    </div>
</section>