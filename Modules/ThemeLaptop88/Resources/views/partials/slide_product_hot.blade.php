<?php
$cat_ids_show_homepage = $settings['homepage_slide_category_product'];
$categories = CommonHelper::getFromCache('homepage_slide_category', ['categories']);
if (!$categories) {
    $categories = \Modules\ThemeLaptop88\Models\Category::whereIn('id', explode(',', $cat_ids_show_homepage))->where('status', 1)->get();
    CommonHelper::putToCache('homepage_slide_category', $categories, ['categories']);
}

?>
@foreach($categories as $category)
    <?php
    $child_category = CommonHelper::getFromCache('homepage_slide_category_child', ['categories']);
    if (!$child_category) {
        $child_category = \Modules\ThemeLaptop88\Models\Category::where('parent_id', $category->id)->where('status', 1)->orderBy('order_no', 'asc')->limit(3)->get();
        CommonHelper::putToCache('homepage_slide_category_child', $child_category, ['categories']);
    }
    $features = CommonHelper::getFromCache('features_homepage_slide_category', ['products']);
    if (!$features) {
        $features = \Modules\ThemeLaptop88\Models\Product::where('category_id', $category->id)->where('status', 1)->orderBy('order_no', 'asc')->limit(7)->get();
        CommonHelper::putToCache('features_homepage_slide_category', $features, ['products']);
    }
    $category_id = $category->id;
    $filter = CommonHelper::getFromCache('category_home_filter_'.$category_id,['products']);
    if (!$filter){
        $filter['max'] = @\Modules\ThemeLaptop88\Models\Product::where('category_id',$category_id)->max('final_price');
        $filter['min'] = @\Modules\ThemeLaptop88\Models\Product::where('category_id',$category_id)->min('final_price');
        CommonHelper::putToCache('category_home_filter_'.$category_id, $filter,['products']);
    }
    ?>
    <section class="product-home product-slider">
        <div class="b">
            <div class="pro-head d-flex justify-content-between">
                <div class="title">
                    <h3><a href="{{route('cate.list', ['slug' => $category->slug])}}">{{$category->name}}</a></h3>
                </div>
                <div class="filter-price d-flex align-items-center">
                    <span>Mức giá:</span
                    @if($filter['min'] < 5000000)
                        <ul class="d-flex m-0">
                            <li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=5000000&amp;filter=%2C">Dưới 5 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=7000000&amp;min=5000000&amp;filter=%2C">5 Triệu - 7 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=10000000&amp;min=7000000&amp;filter=%2C">7 Triệu - 10 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=15000000&amp;min=10000000&amp;filter=%2C">10 Triệu - 15 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?min=15000000&amp;filter=%2C">Trên 15 Triệu</a></li>
                        </ul>
                    @elseif($filter['min'] > 15000000)
                        <ul class="d-flex m-0">
                            <li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?min=15000000&amp;filter=%2C">Trên 15 Triệu</a></li>
                        </ul>
                    @elseif($filter['min'] > 10000000)
                        <ul class="d-flex m-0">
                            <li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=15000000&amp;min=10000000&amp;filter=%2C">10 Triệu - 15 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?min=15000000&amp;filter=%2C">Trên 15 Triệu</a></li>
                        </ul>
                    @elseif($filter['min'] > 7000000)
                        <ul class="d-flex m-0">
                            <li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=10000000&amp;min=7000000&amp;filter=%2C">7 Triệu - 10 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=15000000&amp;min=10000000&amp;filter=%2C">10 Triệu - 15 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?min=15000000&amp;filter=%2C">Trên 15 Triệu</a></li>
                        </ul>
                    @elseif($filter['min'] > 5000000)
                        <ul class="d-flex m-0">
                            <li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=7000000&amp;min=5000000&amp;filter=%2C">5 Triệu - 7 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=10000000&amp;min=7000000&amp;filter=%2C">7 Triệu - 10 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?max=15000000&amp;min=10000000&amp;filter=%2C">10 Triệu - 15 Triệu</a></li><li><a href="{{route('cate.list', ['slug' => $category['slug']])}}?min=15000000&amp;filter=%2C">Trên 15 Triệu</a></li>
                        </ul>
                    @endif
                </div>
{{--                @if(count($child_category) >= 1)--}}
{{--                    <div class="filter-price d-flex align-items-center">--}}
{{--                        <span>Xem thêm:</span>--}}
{{--                        <div class="filter-link">--}}
{{--                            @foreach($child_category as $items)--}}
{{--                                <a href="{{route('cate.list', ['slug' => $items->slug])}}">{{$items->name}}</a>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endif--}}
            </div>
            <div class="p-list-type-1 d-flex">
                @foreach($features as $k=>$featured)
                    <div class="item">
                        <div class="p-item">
                            <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($featured, 'array') }}"
                               title="{{@$featured->meta_title}}" class="p-img">
                                <img data-img="{{$featured->image}}" class="lazy"
                                     data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($featured->image, 220, null) }}"
                                     alt="{{$featured->name}}">
                            </a>
                            <a href="{{ Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($featured) }}"
                               class="p-name">{{$featured->name}}</a>
                            <span class="p-price"><b>@if($featured->final_price == 0) Liên
                                    hệ @else {{number_format($featured->final_price, 0, '.', '.')}}đ @endif</b></span>
                            <p class="p-promotion">{{$featured->intro_quick_view}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endforeach
<script>

    $(".product-slider .p-list-type-1").owlCarousel({
        autoPlay: !1,
        items: 5,
        navigation: true,
        pagination: false,
        margin: 10,
        responsiveRefreshRate: 100,
        navigationText: ["<i class='glyphicon glyphicon-menu-left'></i>", "<i class='glyphicon glyphicon-menu-right'></i>"]
    });
</script>
