<div class="contact_now_parent">
    <div class="contact_now">
        <ul>
            <li class="fb_now">
                <div class="fb-customerchat"
                     attribution=setup_tool
                     page_id="169469587148351"
                     theme_color="#0084ff"
                     logged_in_greeting="Em chào anh / chị ạ. Mình đang cần tư vấn về sản phẩm gì ạ ?"
                     logged_out_greeting="Em chào anh / chị ạ. Mình đang cần tư vấn về sản phẩm gì ạ ?">
                </div>
                {{--                <div id="fb-root"></div>--}}
            </li>
            <li class="phone_now" style="bottom: 6px;">
                <a href="tel:{{ str_replace('-', '', str_replace('.', '', @$settings['hotline'])) }}">
                    <img data-src="{{ asset('public/frontend/themes/laptop88/image/hotline-call-mobile.png') }}"
                         class="lazy"
                         width="50px">
                </a>
            </li>
            <li class="zalo_now">
                <a href="https://zalo.me/0974329191" target="_blank">
                    <img class="lazy"
                         data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb('1.logo/zalo.png', '50px', null) }}"
                         alt="">

                </a>
                <span>Zalo: 0974 32 91 91</span>
            </li>
        </ul>
        @include('themelaptop88::partials.phone_now_detail')
    </div>
</div>