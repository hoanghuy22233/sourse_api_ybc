@extends('themelaptop88::layouts.master')
@section('main_content')
    <style>


        .content_category_last>div {
            margin: 0 15px;
            padding: 15px;
            background: #ccc!important;
        }
        label#sort_prd_price:after {
            content: "\f0d7";
            font: 15px FontAwesome;
            padding-left: 8px;
            cursor: pointer;
        }
        label#sort_prd_price {
            position: relative;
            background: #fff;
            cursor: pointer;
        }
        label#sort_prd_price>ul>li {
            border-bottom: 1px solid #ccc;
            text-align: center;
        }
        label#sort_prd_price>ul>li>a {
            display: block;
        }

        label#sort_prd_price>ul {
            position: absolute;
            top: 100%;
            left: 0;
            display: none;
            background: #fff;
            border: 1px solid #ccc;
            z-index: 999;
            width: 100%;
        }
        .full_k_che{
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: 999;
            background: #04040485;
        }
        .popup-add-cart {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 500px;
            height: 171px;
            background: #fff;
            border: 1px solid #ccc;
            margin: auto;
        }
        .popup-add-cart-content{
            /*width: 320px;*/
            /*margin: 0 auto;*/
            margin-top: auto;
        }
        .popup-add-cart p {
            text-align: center;
            margin: 15px 0;
        }

        a.btn-cart {
            background: #63cc50;
            text-transform: capitalize;
        }

        button.close-pop {
            background: #5467f1;
            border: none;
            text-transform: capitalize;
        }
        .btn-check {
            text-align: center;
            padding: 10px;
        }
        a.btn-cart:hover, button.close-pop:hover{
            color: #000;
        }
{{--        .f2-head > .b > .f2-h {--}}
{{--            width: 100% !important;--}}
{{--        }--}}

{{--        .thuonghieu a {--}}
{{--            width: 14.6%;--}}
{{--            padding: 3px;--}}
{{--            float: left;--}}
{{--            background: #fff;--}}
{{--            height: 80px;--}}
{{--            overflow: hidden;--}}
{{--            text-align: center;--}}
{{--            border-radius: 10px;--}}
{{--            margin: 1%;--}}
{{--            border: 1px solid #eee;--}}
{{--            display: flex;--}}
{{--            justify-content: center;--}}
{{--            align-items: center;--}}
{{--        }--}}

{{--        .cate-none {--}}
{{--            display: none;--}}
{{--        }--}}
    </style>
    <div class="b" style="margin:0px auto;">
        @include('themelaptop88::partials.menu_master')
        @include('themelaptop88::partials.breadcrumb')
    </div>
    <script>
        $(document).ready(function () {
            $('.showSapo').click(function () {
                $('#sapo').css('height', 'auto');
                $(this).hide()
                $('.hideSapo').show();
            });
            $('.hideSapo').click(function () {
                $('#sapo').css('height', '75px');
                $(this).hide()
                $('.showSapo').show();
            });
        });
    </script>
<?php

?>
@if(@$category->cate_show == 0)
    @include('themelaptop88::childs.category.customs_cate.cate_full_logo')

@elseif(@$category->cate_show == 1)
    @include('themelaptop88::childs.category.customs_cate.cate_child')
@elseif(@$category->cate_show == 2)
    @include('themelaptop88::childs.category.customs_cate.news')
@elseif(@$category->cate_show == 3)
    @include('themelaptop88::childs.category.customs_cate.banner')
@elseif(@$category->cate_show == 4)
        @include('themelaptop88::childs.category.customs_cate.product_hot')
@endif
<script>

        var isRemove = false;
        var page = 0;
        var order = 3;
        var view = '';
        var ismore = true;

        function regetdata() {
            page = 0;
            ismore = false;
        }

        function thtt(cla) {
            var s = '';
            $('.' + cla).each(function () {
                var vl = $(this).attr('data-value');
                if (vl > 0) {
                    s += (s == '' ? '' : ',') + vl;
                }
            });
            return s;
        }


        function get_data() {
            regetdata();
            var s = thtt('pp');
        }

        function thuoctinh(obj) {
            var parent = $(obj).parent().get(0);
            var cvl = $(parent).attr('data-value');
            var vl = $(obj).attr('data-value');
            if (vl == cvl) return;
            else {
                $(parent).attr('data-value', vl);

                //
                var current = $('#' + cvl);
                $(current).removeClass('select');
                $(current).addClass('unselect');
                //
                $(obj).removeClass('unselect');
                $(obj).addClass('select');
                //
                if (vl > 0) {
                    var idall = $(parent).attr('data-id');
                    var all = $('#' + idall);
                    $(all).removeClass('select');
                    $(all).addClass('unselect');
                }
                //
                var label = $(parent).prev();
                $(label).text($(obj).attr('data-title').slice(0, 20) + '...');
                var width = $(window).width()
                if (width < 767) {
                    $(label).text($(obj).attr('data-title').slice(0, 12) + '...');
                }
            }
            get_data();
        }

        $(document).ready(function () {
            $('.oli label').each(function () {
                var width = $(window).width()
                var te = $(this).text();
                if (width < 767) {
                    $(this).text(te.slice(0, 12) + '...');
                }
            })
        })
        order = 3;
        $(document).ready(function () {

            //sap xep
            $('.bs').click(function () {
                var neworder = $(this).attr('data-v');
                if (order == neworder) return;
                order = neworder;
                $('.bs').attr('class', '');
                $(this).attr('class', 'bs curent');
                $('#orderlabel').text($(this).text());
                get_data();
            });
            $('.othpro ul li').click(function () {
                thuoctinh(this);
                get_data();
            });
        });
    </script>
    <style>
        @media (max-width: 767px){
            .f>.inline-bl .flexJus {
                display: flex;
            }
            .f>.inline-bl .othpro{
                width: 40%;
                padding: 0;

            }
            .f>.inline-bl .othpro>.oli{
                width: 100%;
            }
            .f>.inline-bl label{
                padding-left: 0;
                font-size: 11px;
            }
            #sort_prd_price{
                width: 40%;
                font-size: 11px;
                padding: 4px!important;
                margin-top: 5px;
            }
            .content_category_last iframe{
                width: 100%;
                height: auto !important;
                min-height: 200px;
            }
            .f.content_category_last img {
                height: auto !important;
            }
            .content_category_last>div {
                margin: 0;
            }
            /*div#Product .gri>.gi>a {*/
            /*    height: unset !important;*/
            /*}*/
            .badge{
                background: red!important;
            }
        }
        .othpro label{
            margin: 0;
        }

    </style>
    <div class="b filter-category" >
        <div class="fl flexJus hp-pro">
            <label><span id="number">{{$countProduct}}</span> sản phẩm <span
                        id="propertyCurrent"></span></label>
            <ul class="othpro flexL sot_left">
                @php
                    $fiels = explode('|', @$category->properties_name_id);

                    if ($fiels['0'] == ''){
                        array_shift($fiels);
                    }
                    if (end($fiels) == ''){
                        array_pop($fiels);
                    }

                    $propretieName = CommonHelper::getFromCache('propretieName_id_order_no_desc' . implode('|', $fiels),['properties_name']);
                    if (!$propretieName) {
                        $propretieName = Modules\ThemeLaptop88\Models\PropertieName::whereIn('id', $fiels)->orderBy('order_no', 'desc')->get();
                        CommonHelper::putToCache('propretieName_id_order_no_desc' . implode('|', $fiels), $propretieName,['properties_name']);
                    }
                @endphp
                @if(!empty($propretieName))
                    @foreach($propretieName as $filel)
                        @php
                            $propretieValue = CommonHelper::getFromCache('propretieValue_properties_name_id_order_no_desc'.@$filel->id, ['properties_value']);
                        if (!$propretieValue) {
                             $propretieValue = Modules\ThemeLaptop88\Models\PropertieValue::where('properties_name_id', $filel->id)->orderBy('order_no', 'desc')->get();
                            CommonHelper::putToCache('propretieValue_properties_name_id_order_no_desc'.@$filel->id, $propretieValue, ['properties_value']);
                        }

                        @endphp
                        <li class="oli">
                            <label>{{ucfirst($filel->name)}}</label>
                            <ul class="pp" id="Classify{{$filel->id}}" data-value="" data-id="25{{$filel->id}}">
                                <li id="25{{$filel->id}}" data-title="{{ucfirst($filel->name)}}" data-value="0"
                                    class="radio select">Tất cả
                                </li>
                                @if(!empty($propretieValue))
                                    @foreach($propretieValue as $key => $value)
                                        <li id="{{$value->id}}" data-title="{{ucfirst($value->value)}}"
                                            data-value="{{$value->id}}"
                                            class="radio unselect">{!! ucfirst($value->value) !!}
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>


    @if(@$category->option_show_cate==0)
        @include('themelaptop88::childs.category.list_pro_cate')
    @else
        @include('themelaptop88::childs.category.colum_pro_cate')
    @endif
    <div class="b cate_bottom">
        <?php
        $category1 = CommonHelper::getFromCache('categories_id_' . $category->cate_new_1, ['categories']);
        if (!$category1) {
            $category1 = \Modules\ThemeLaptop88\Models\Category::find(@$category->cate_new_1);
            CommonHelper::putToCache('categories_id_' . $category->cate_new_1, $category1, ['categories']);
        }


        $posts1 = CommonHelper::getFromCache('posts_id_' . $category->cate_new_1, ['posts']);
        if (!$posts1) {
            $posts1 = \Modules\ThemeLaptop88\Models\Post::where('multi_cat', 'like', '%|' . @$category->cate_new_1 . '|%')->where('status', 1)->limit(5)->orderBy('id', 'decs')->get();
            CommonHelper::putToCache('posts_id_' . $category->cate_new_1, $posts1, ['posts']);
        }
        ?>
        <div class="cate_video">
            <iframe width="100%" height="100%"
                    src="https://www.youtube.com/embed/tgbNymZ7vqY">
            </iframe>
        </div>
        <div class="cate_newss" >
            @if(!empty($category1))
                <div class="cat-item">
                    @if($posts1->count()>0)
                        <div class="cate_new_content cate_new_1_content">
                            <div class="cate_new_1_header">
                                <h4>{{@$category1->name}}</h4>
                            </div>
                            <ul>
                                @foreach($posts1 as $k1=>$post1)
                                    <li>

                                        <img data-img="{{$post1->image}}"class="lazy" data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($post1->image, 250, null) }}" alt="">
                                        <div class="content-post">
                                            <h3>
                                                <a title="{{$post1->name}}"
                                                   href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getPostSlug($post1) }}">{{$post1->name}}</a>
                                            </h3>
                                            <p>{{$post1->created_at->format('d-m-Y g:i A')}}</p>
                                        </div>

                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            @endif
        </div>

    </div>
    <div class="full_k_che close-pop" style="display: none"></div>
    <div class="popup-add-cart" style="display: none">
        <div class="popup-add-cart-content">
            <p>Thêm thành công vào giỏ hàng!</p>
            <div class="btn-check">
                <a class="btn-cart btn" href="/gio-hang">Xem giỏ hàng</a>
                <button class="close-pop btn">Tiếp tục mua hàng</button>
            </div>
        </div>
    </div>
@endsection


