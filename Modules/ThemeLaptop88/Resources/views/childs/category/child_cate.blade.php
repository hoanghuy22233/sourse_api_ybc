
@extends('themelaptop88::layouts.master')
@section('main_content')
    <div class="b" style="margin:0px auto;">
        @include('themelaptop88::partials.breadcrumb')
    </div>

    <style>
        .headpage {
            border-bottom: none;
        }

        .title-section {
            margin-top: 4rem;
            font-size: 30px;
            line-height: 30px;
            font-weight: 700;
            text-align: center;
        }

        .bgwhite {
            background: #fff;
        }

        /*banner*/
        .ld2banner {
            position: relative;
        }

        .ld2banner figure {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .ld2banner .sapold { /*background:rgba(0, 0, 0, 0.2);*/
            padding: 20px 50px;
            margin: 0 auto;
            max-width: 700px;
            border-radius: 15px;
        }

        .ld2banner h1 {
            font: bold 35px/25px arial;
            margin-bottom: 15px;
            text-align: center;
            padding-bottom: 15px;
            border-bottom: 1px solid #aaa;
        }

        .ld2banner p {
            font-size: 14px;
            text-align: left;
        }

        .ld2banner h3 {
            text-align: center;
            display: block;
            font: bold 15px arial;
            margin: 10px 0;
        }

        .ld2banner img {
            display: block;
            margin: 0 auto;
        }

        /*gioi thieu*/
        .atn, .listadvan {
            height: 860px;
        }

        .atn {
            overflow: hidden;
            width: 48%;
        }

        .atn img {
            height: 100%;
        }

        .listadvan {
            width: 51%;
            margin-left: 1%;
            justify-content: space-between;
        }

        .intro {
            height: 46%;
            margin-bottom: 2%;
            padding: 30px 0 30px 50px;
            background: #fff;
            overflow: hidden;
        }

        .intro ul {
            color: #555;
            overflow-y: auto;
            height: 100%;
            padding-right: 30px;
        }

        @media only screen and (max-width: 1440px) {
            .atn, .listadvan {
                height: 772px;
            }
        }

        /*@media only screen and (max-width:1400px) {
            .atn, .listadvan {
                height: 750px;
            }
        }*/
        @media only screen and (max-width: 1360px) {
            .atn, .listadvan {
                height: 727px;
            }
        }

        @media only screen and (max-width: 1300px) {
            .atn, .listadvan {
                height: 696px;
            }
        }

        .intro ul::-webkit-scrollbar {
            width: 10px;
        }

        .intro ul::-webkit-scrollbar-track {
            box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            border-radius: 5px;
        }

        .intro ul::-webkit-scrollbar-thumb {
            border-radius: 5px;
            box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.5);
        }

        /*.intro div {display: flex;justify-content: space-between;}*/
        .intro li {
            margin-bottom: 30px;
            float: left;
            width: 100%;
        }

        .intro h3 {
            font: bold 18px/20px arial;
            margin-bottom: 15px;
        }

        .intro .indiv {
            float: left;
            width: 100%;
        }

        .intro div img {
            float: left;
            margin-right: 15px;
        }

        .intro div p {
            text-align: justify;
        }

        #introvideo {
            height: 52%;
            background: #000;
            overflow: hidden;
        }

        /*button*/
        .dangkingay {
            margin-top: 50px;
            margin-bottom: 50px;
        }

        .dangkingay label {
            margin-bottom: 20px;
            display: block;
            font: bold 25px/25px arial;
        }

        .btn {
            display: inline-block;
            font-weight: 400;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            user-select: none;
            border: 1px solid transparent;
            box-shadow: 0 2px 4px 0 rgba(248, 90, 34, 0.5), 0 10px 20px 0 rgba(0, 0, 0, 0.25);
            padding: 1rem 4.5rem;
            font-size: 1.125rem;
            line-height: 1.5;
            border-radius: 50px;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            -o-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            color: #fff;
            cursor: pointer;
            margin: 0 2px;
            text-transform: uppercase;
        }

        .btn.btn-primary {
            border: none;
            background: -webkit-gradient(linear, right top, left top, from(#F95B21), color-stop(36.83%, #E84352), to(#BC0A25));
            background: -webkit-linear-gradient(right, #F95B21 0%, #E84352 36.83%, #BC0A25 100%);
            background: -o-linear-gradient(right, #F95B21 0%, #E84352 36.83%, #BC0A25 100%);
            background: linear-gradient(270deg, #F95B21 0%, #E84352 36.83%, #BC0A25 100%);
            color: #fff;
        }

        .btn.btn-primary:hover, .btn.btn-primary:active {
            opacity: 0.8;
            box-shadow: none;
            box-shadow: none;
        }

        /*tieu de khoi san pham*/
        .hp-pro-hang {
            margin: 0 10px 0 20px;
            position: relative;
        }

        .hp-pro-hang label span {
            border: 1px solid #ddd;
            background: #fff;
            display: block;
            line-height: 30px;
            padding: 0 10px;
            color: #666;
            font-size: 12px;
        }

        .hp-pro-hang label span:after {
            content: "\f107";
            font: 18px FontAwesome;
            padding-left: 5px;
            color: #1c7eb6;
        }

        .hp-pro-panel {
            display: none;
            position: absolute;
            width: 270px;
            z-index: 2;
            background: #fff;
            border: 1px solid #eee;
            border-top: none;
            text-align: center;
            right: 0;
        }

        .hp-pro-panel a {
            display: block;
            font: 13px/35px arial;
            color: #444;
            padding: 0 10px;
            float: left;
            width: 100%;
        }

        .hp-pro-panel a:hover {
            color: #00639a;
        }

        .hp-pro-hang:hover > .hp-pro-panel {
            display: block;
        }

        /*san pham luoi*/
        .gri {
            float: left;
            width: 23%;
            margin: 1%;
            position: relative;
            text-align: center;
            background: #fff;
            display: flex;
            flex-direction: column;
            color: #666;
            border-radius: 4px;
        }

        .gri3 {
            width: 30%;
            margin: 1.66%;
        }

        .gri:hover {
            color: #666;
        }

        .gri .gi {
            display: flex;
            flex-direction: column;
            padding: 1%;
        }

        .gi div:nth-child(1) {
            height: 250px;
            display: flex;
            align-items: center;
            justify-content: center;
            margin-bottom: 10px;
        }

        .gi h3 {
            color: #000;
            font-size: 15px;
            text-transform: uppercase;
        }

        .gi p {
            font-size: 13px;
            padding: 15px;
            height: 85px;
            overflow: hidden;
        }

        .gri3 p {
            height: 45px;
            padding: 5px;
        }

        .gi label {
            display: flex;
            justify-content: center;
            align-items: center;
            color: #00a7e9;
        }

        .gi label i {
            margin: 0 10px 0 0;
            padding-right: 80px;
            border-right: 2px solid #00a7e9;
        }

        .gi span {
            padding: 10px 0;
        }

        .gi span b {
            color: red;
            display: block;
            font-size: 26px;
            margin-top: 12px;
        }

        .gift {
            height: 100px;
            position: relative;
            border-top: 1px solid #ddd;
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-top: 15px;
        }

        .nob {
            border: none;
        }

        .gift label {
            width: 100px;
            background: #fff;
            z-index: 1;
            position: absolute;
            top: -11px;
            left: calc(50% - 50px);
            font-size: 15px;
        }

        .gift img {
            margin: 0 auto;
        }

        .gift span {
            width: 50%;
        }

        .gift span:nth-last-child(1) {
            text-align: left;
            font-size: 12px;
            padding-right: 10px;
        }

        .gift b {
            color: red;
        }

        .gri div:nth-last-child(1) {
            height: 50px;
            text-align: center;
            border-top: 1px solid #ddd;
        }

        .gri div:nth-last-child(1) span {
            width: 50%;
            float: left;
            line-height: 50px;
        }

        .gri div:nth-last-child(1) span:hover {
            background: linear-gradient(to bottom, #ffa103, #fb7d0f);
            color: #fff;
        }

        .gri div:nth-last-child(1) span:nth-child(1) {
            border-right: 1px solid #ddd;
            border-radius: 0 0 0 4px;
        }

        .gri div:nth-last-child(1) span:nth-child(2) {
            border-radius: 0 0 4px 0;
        }

        /*sapo*/
        .ld2-chinhsach, .ld2-sapo {
            margin-top: 15px;
        }

        .ld2-sapo {
            padding: 55px 0;
        }

        .ld2-sapo h1 {
            font: bold 38px arial;
            color: #666;
            margin: 0 0 30px;
        }

        .ld2-sapo p {
            font-size: 15px;
            line-height: 25px;
            color: #666;
        }

        /*loi ich khi mua hang*/
        .list-profit {
            text-align: center;
            margin: 2rem 12rem;
        }

        .list-profit li {
            display: flex;
            margin: 1.51%;
            width: 30%;
            border: 1px solid #e9ecef;
            border-radius: 0.5rem;
            align-items: center;
            justify-content: center;
            float: left;
            padding: 2% 1%;
        }

        .list-profit li:hover {
            box-shadow: 0 12px 50px rgba(0, 0, 0, 0.15), 0 7px 24px rgba(0, 0, 0, 0.12);
            -webkit-transform: translate3d(0, -10px, 0);
            transform: translate3d(0, -10px, 0);
        }

        .list-profit img {
            width: 92px;
            height: 92px;
        }

        .list-profit div {
            width: 52%;
            color: #555555;
        }

        .BGPrimary {
            background: #3f3434;
        }

        .ColorPrimary {
            color: #fff;
        }


        .BGSecond {
            color: #eaeaea;
        }

        .ColorSecond {
            color: #1a1a1a;
        }
        .bd {
            border-left: 1px solid #ccc;
            border-right: 1px solid #ccc;
        }
    </style>
    <!--gioi thieu-->
    <div class="f BGPrimary ColorPrimary">
        <div class="ld2banner f">
            <img class="lazy" data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($category->banner_child, 1350, null) }}"
                 alt="{{$category->name}}"/>
            <figure class="flexCen">
                <div class="sapold">
                    <h1 style="line-height: 40px;" class="ColorSapo">{{$category->title_banner_child}}</h1>
                    <p style="text-align: center;">
                        {!! str_limit($category->intro, 570) !!}
                    </p>
                </div>
            </figure>
        </div>

        <div class="b flexCol">
            <label class="title-section ColorIntro" style="margin:2.5rem 0;">Đặc điểm nổi bật</label>
            <div class="flexJus">
                <div class="atn">
                    <img class="lazy" data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($category->banner_child_left, 633, null) }}"/>
                </div>
                <div class="listadvan flexCol">
                    <div class="intro">
                        <ul>
                            {!! $category->featured_description !!}
                        </ul>
                    </div>
                    <div id="introvideo" class="flexCen">
                        {!! $category->video  !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="f" style="background:#f6f6f6">
        <!--san pham-->

        <div class="f">
            <div class="f BGSecond ColorSecond">
                <div class="b flexJus" style="height:45px;">

                    <label><span id="number">{{count($productNolimit)}}</span> sản phẩm <span
                                id="propertyCurrent"></span></label>
                    <div class="hp-pro-hang">
                        <label><span>Xem thêm chủ đề khác</span></label>
                        <div class="hp-pro-panel">
                            @foreach($child_category_show as $child_cate)
                                <a href="{{route('cate.list', ['slug' => $child_cate->slug])}}"
                                   title="{{$child_cate->name}}">{{$child_cate->name}}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="b">
                @if(!empty($products))
                    @foreach($products as $product)
                        @php
                            $manufacture = CommonHelper::getFromCache('manufacture_id'.$product->manufacture_id,['manufactureres']);
                            if (!$manufacture){
                             $manufacture = \Modules\ThemeLaptop88\Models\Manufacturer::find($product->manufacture_id);
                             CommonHelper::putToCache('manufacture_id'.$product->manufacture_id, $manufacture,['manufactureres']);
                            }
                        @endphp
                        <a class="gri gri3"
                           href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($product) }}"
                           title="{{$product->name}}">
                            <div class="gi">
                                <div>
                                    <img class="lazy" data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($product->image, 250, null) }}"
                                         alt="{{$product->name}}"/>
                                </div>
                                <div>@if(!empty($manufacture)) <img class="lazy" height="50"
                                                                    data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($manufacture->image, '150', null) }}"
                                                                    alt="{{$manufacture->name}}"/> @else </br></br></br> @endif
                                </div>
                                <h3>{{$product->name}}</h3>
                                <p>{{$product->intro}}</p>
                                @if($product->final_price < $product->base_price)
                                    <span class="unse_price"><u>{{number_format($product->base_price, 0, '.', '.')}}</u><sup
                                                style="font-size: 18px;"> đ</sup></span>
                                @else
                                    <div style="height: 6px;"></div>
                                @endif
                                @if($product->final_price != 0)
                                    <span class="final_price">@if($product->final_price < $product->base_price) @else
                                            Giá bán @endif<b>{{number_format($product->final_price, 0, '.', '.')}}&nbsp;<sup>đ</sup></b></span>
                                    @else
                                    </br> <span class="pr">Liên hệ</span>
                                @endif
                                @if($product->final_price < $product->base_price)
                                    <span>Giảm: {{number_format($product->base_price - $product->final_price, 0, '.', '.')}} &nbsp;<sup>đ</sup></span>
                                @else
                                    <div style="height: 42px;border-top: none"></div>
                                @endif
                            </div>
                            <div class="gift" style="display: none">
                                <?php
                                $getDateToday = date_create(date('Y-m-d H:i:s'));
                                $product_all = Modules\ThemeLaptop88\Models\ProductSale::where(function ($query) use ($getDateToday) {
                                    $query->orWhere('time_start', '<=', date_format($getDateToday, 'Y-m-d H:i:s'));
                                    $query->orWhere('time_start', null);
                                })->where(function ($query) use ($getDateToday) {
                                    $query->orWhere('time_end', '>=', date_format($getDateToday, 'Y-m-d H:i:s'));
                                    $query->orWhere('time_end', null);
                                })->get();
                                foreach ($product_all as $sale_val) {
                                    $id_parent = Modules\ThemeLaptop88\Models\Category::whereIn('id', explode('|', $sale_val->category_ids))->get();
                                    if (!empty($manufacture) && (in_array($manufacture->id, explode('|', $sale_val->manufacturer_ids)) == true)) {
                                        $data['sales'] = Modules\ThemeLaptop88\Models\Product::whereIn('id', explode('|', $sale_val->id_product_sale))->get();
                                    } elseif (array_diff([$product['id']], explode('|', $sale_val->id_product)) == []) {
                                        $data['sales'] = Modules\ThemeLaptop88\Models\Product::whereIn('id', explode('|', $sale_val->id_product_sale))->get();
                                    }


                                    foreach ($id_parent as $rt) {
                                        if ($rt->parent_id == 0 && $category->parent_id == $rt->id && in_array($rt->id, explode('|', $sale_val->category_ids)) == true && $category->parent_id == $rt->id) {
                                            $data['sales'] = Modules\ThemeLaptop88\Models\Product::whereIn('id', explode('|', $sale_val->id_product_sale))->get();
                                        } elseif ($rt->parent_id > 0 && in_array($rt->id, explode('|', $sale_val->category_ids)) == true && in_array($rt->id, explode('|', $product->multi_cat)) == true) {
                                            $data['sales'] = Modules\ThemeLaptop88\Models\Product::whereIn('id', explode('|', $sale_val->id_product_sale))->get();
                                        } else {
                                            $data['sales'] = [];
                                        }
                                    }

                                }
                                ?>
                                @if(!empty($data['sales']))
                                    <label>Quà tặng</label>
                                    <span><img class="lazy" style="width: 60px; height: 60px;"
                                               data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo_qua_tang'], 60, null) }}"
                                               alt="Qùa tặng"/></span>
                                    <span>
                              @php
                                  $total = 0;
                                  $strName = '';
                              @endphp
                                        @foreach($data['sales'] as $sale)
                                            @php
                                                $total += $sale['base_price'];
                                                $strName .= $sale['name'] .' + ';
                                            @endphp
                                        @endforeach
                                        {{trim($strName, ' +')}}
                              <br>
                              @if($total > 0)
                                            Trị giá: <b>{{number_format($total, 0, '.', '.')}} <sup>đ</sup></b>
                                        @endif
                            </span>

                                @endif
                            </div>
                            <div class="bd">
                                <span>XEM HÀNG</span>
                                <span>Cho vào giỏ</span>
                            </div>
                        </a>
                    @endforeach
                    {{$products->links()}}
                @endif
            </div>
        </div>
        @include('themelaptop88::partials.policy_child_cate')
    </div>
@endsection