<?php
//if (isset($product_sort_price)){
//    $products=$product_sort_price;
//    dd($product_sort_price);
//}
?>
<style>
    @media (min-width: 990px) {
        .popup-add-cart-content {
            margin-top: 10%;
        }
    }

    .popup-add-cart {
        z-index: 999;
    }

    .gri-center > h3 {
        margin: 0 0 20px 0;
    }

    .gri-center .gi h3 {
        line-height: 30px;
    }

    div#Product .gri {
        width: 100% !important;
    }

    .gri-center > p {
        text-align: left;
        padding: 0 !important;
    }

    div.gi {
        display: block !important;
        height: auto !important;
    }

    .gri_left {
        width: 25% !important;
        max-width: 25% !important;
        float: left;
        position: relative;
        overflow: hidden;
        min-height: 400px;
    }

    .gri-center {
        width: 50% !important;
        max-width: 50% !important;
        float: left;
        text-align: left;
        padding: 0 20px;
    }

    .gri-right {
        width: 25% !important;
        max-width: 25% !important;
        float: left;
    }

    .gri > div:nth-last-child(1) span:hover {
        background: none !important;
        color: unset !important;
    }

    .gri > div:nth-last-child(1) {
        border: none !important;
    }

    .gri .gi .gri-right .gri-view-pro, .gri .gi .gri-right .gri-add-cart {
        background: @if(@$settings['header_background']!=''){{@$settings['header_background']}} @else {{'linear-gradient(to bottom,#ffa103,#fb7d0f)'}}@endif !important;
        border-radius: 5px;
        color: #fff !important;
    }

    .gri-right p b {
        color: red;
        display: block;
        font-size: 26px;
        margin-top: 0px;
    }

    .gift_content {
        text-align: left;
        margin-bottom: 20px;
    }

    @media (min-width: 1200px) {
        div#Product .gri {
            min-height: 420px !important;
        }
    }

    @media screen and (max-width: 991px) and (min-width: 768px) {
        .f > #Product > .gri > .gi p {
            height: auto !important;
        }

        .sale_text {
            left: -20px;
            bottom: 85px;
            margin: auto !important;
            height: 25px !important;
        }
    }
    .product_intro{
        height: 55px!important;
        text-align: justify;
    }
    @media (max-width: 767px) {
        .product_intro{
            height: 85px!important;
            text-align: justify;
        }
        .gri_left {
            width: 100% !important;
            max-width: 100% !important;
            /*float: left;*/
            position: relative;
            overflow: hidden;
            height: auto;
            min-height: auto;
        }

        .gri-center {
            width: 100% !important;
            max-width: 100% !important;

            height: auto !important;
            /*float: left;*/
        }

        .gri-right {
            width: 100% !important;
            max-width: 100% !important;
            height: auto !important;
            /*float: left;*/
        }


        .f > #Product > .gri > .gi p {
            height: auto !important;
        }

        .gift label {
            top: -15px;
        }
    }

    .gi div .manuface_g {
        height: unset !important;
    }

    .reset_content span {
        width: 100% !important;
        border: none !important;
        line-height: 1.5 !important;
    }

    .reset_content p {
        height: auto !important;
    }
</style>
<div class="f" style="background:#f6f6f6;min-height: 350px;">
    <div class="b" id="Product">
        <?php
        $price_max = app('request')->get('max') ? app('request')->get('max') : 999999999999;
        $price_min = app('request')->get('min') ? app('request')->get('min') : 0;
        $page_show = app('request')->get('perpage') ? app('request')->get('perpage') : 12;
        $filterdProducts = array_filter($products, function ($p) use ($price_max,$price_min) {
            return $p['final_price'] >= $price_min && $p['final_price'] <= $price_max;
        });
        sort($filterdProducts);
        $numOfProduct = count($filterdProducts);
        if ($numOfProduct < $page_show) {
            $isPagination = false;
        } else {
            $isPagination = true;
        }
        $du = $numOfProduct % $page_show;
        $numOfPage =  floor($numOfProduct / $page_show);
        if ($du > 0) {
            $numOfPage++;
        }
        $pindex = 0;
        ?>
        <?php for ($i = 1; $i <= $numOfPage; $i++): ?>
            <div id="page{{$i}}" class="page-product hide">
                <?php
                $count = 0;
                while ($count < $page_show && $pindex < $numOfProduct):
                ?>
                <?php
                    $product = $filterdProducts[$pindex];
                    $count++;
                    $pindex++;
                ?>
                @php
                    $manufacture = CommonHelper::getFromCache('manufacture_id'.$product['manufacture_id']);
                       if (!$manufacture){
                        $manufacture = \Modules\ThemeLaptop88\Models\Manufacturer::find($product['manufacture_id']);
                        CommonHelper::putToCache('manufacture_id'.$product['manufacture_id'], $manufacture);
                    }
                @endphp
                    <div class="gri">

                        <div class="gi">
                            @php
                                if (@$product['final_price'] != 0 && @$product['base_price'] != 0){
                                  $discount = 100 - ((@$product['final_price'])/ @$product['base_price'] * 100);
                                }
                                else{
                                    $discount = 0;
                                }
                            @endphp

                            {{--                        IMG--}}
                            <div class="gri_left">
                                {{--                        Giảm giá--}}

                                @if($discount > 0)
                                    <div class="badge @if(round($discount) >= 20) red_sale @else orange_sale @endif">
                                        <div class="sale_text">- {{round($discount)}} %</div>
                                    </div>
                                @endif
                                {{--                        img--}}
                                <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                                   title="{{@$product['meta_title']}}">
                                    <img class="lazy" data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($product['image'], 250, null) }}"
                                         alt="{{$product['name']}}"/>
                                </a>

                                <div class="manuface_g">
                                    @if(!empty($manufacture))
                                        <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                                           title="{{@$product['meta_title']}}">
                                            <img height="50" class="lazy"
                                                 data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($manufacture->image, 150 ,null) }}"
                                                 alt="{{$manufacture->name}}"/>
                                        </a>

                                        @else </br></br></br>
                                    @endif
                                </div>

                            </div>

                            {{--                        tiêu đề--}}
                            <div class="gri-center">
                                <h3>
                                    <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                                       title="{{@$product['meta_title']}}">{{$product['name']}}</a></h3>

                                {{--                        <p class="product_intro">--}}
                                <div class="reset_content">
                                    {!! $product['content'] !!}
                                </div>

                                {{--                        </p>--}}
                            </div>

                            <div class="gri-right">
                                {{--                        Giá--}}
                                @if($product['final_price'] < $product['base_price'])
                                    <p style="padding-top: 12px;"><u
                                                style="text-decoration: line-through;font-size: 18px">{{number_format($product['base_price'], 0, '.', '.')}}</u><sup
                                                style="font-size: 18px;"> đ</sup></p>
                                @else
                                    <div style="height: 32px;"></div>
                                @endif
                                @if($product['final_price'] != 0)
                                    <p style="padding: 0px 2px;">@if($product['final_price'] < $product['base_price']) @else
                                            Giá
                                            bán @endif<b style="margin: 0!important;">{{number_format($product['final_price'], 0, '.', '.')}}
                                            &nbsp;<sup>đ</sup></b></p>
                                    @else
                                    </br> <span style="font-size: 25px; width: 100%!important;" class="pr">Liên hệ</span>
                                @endif

                                @if($product['final_price'] < $product['base_price'])
                                    <p>Giảm: {{number_format($product['base_price'] - $product['final_price'], 0, '.', '.')}}
                                        &nbsp;<sup>đ</sup></p>
                                @else
                                    <div style="height: 24px;border-top: none"></div>
                                @endif

                                {{--                        <div class="gift">--}}
                                {{--                            {{$product->intro}}--}}
                                <?php
                                //                        $sales = \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductsSale($product['id'], @$category->id, @$manufacturer->id);

                                date_default_timezone_set('Asia/Ho_Chi_Minh');
                                $getDateToday = date_create(date('Y-m-d H:i:s'));
                                $sales = [];
                                $product_all = Modules\ThemeLaptop88\Models\ProductSale::where(function ($query) use ($getDateToday) {
                                    $query->orWhere('time_start', '<=', date_format($getDateToday, 'Y-m-d H:i:s'));
                                    $query->orWhere('time_start', null);
                                })->where(function ($query) use ($getDateToday) {
                                    $query->orWhere('time_end', '>=', date_format($getDateToday, 'Y-m-d H:i:s'));
                                    $query->orWhere('time_end', null);
                                })->get();
                                $sale = [];
                                foreach ($product_all as $key => $sale_val) {
                                    $manufacture = CommonHelper::getFromCache('manufacture_id_explode'.@$sale_val->manufacturer_ids,['manufactureres']);
                                    if (!$manufacture) {
                                        $manufacture = \Modules\ThemeLaptop88\Models\Manufacturer::whereIn('id', explode('|', @$sale_val->manufacturer_ids))->get();
                                        CommonHelper::putToCache('manufacture_id_explode'.@$sale_val->manufacturer_ids, $manufacture,['manufactureres']);
                                    }
                                    $cate_child = CommonHelper::getFromCache('category_id_explode'.@$sale_val->category_ids,['categories']);
                                    if (!$cate_child) {
                                        $cate_child = \Modules\ThemeLaptop88\Models\Category::whereIn('id', explode('|', @$sale_val->category_ids))->get();
                                        CommonHelper::putToCache('category_id_explode'.@$sale_val->category_ids, $cate_child,['categories']);
                                    }

                                    foreach ($cate_child as $rt) {
                                        if ($rt->parent_id == 0 && in_array($rt->id, explode('|', @$sale_val->category_ids)) == true && in_array($rt->id, explode('|', @$product->multi_cat)) == true) {
                                            $sale = Modules\ThemeLaptop88\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                                        } elseif (in_array($rt->id, explode('|', @$sale_val->category_ids)) == true && in_array($rt->id, explode('|', @$product->multi_cat)) == true) {
                                            $sale = Modules\ThemeLaptop88\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                                        } else {
                                            $sale = [];
                                        }
                                    }
//                                if ($sale == []) {
                                    foreach ($manufacture as $rts) {
                                        if (in_array($rts->id, explode('|', @$sale_val->manufacturer_ids)) == true && in_array($rts->id, explode('|', @$product->manufacture_id)) == true) {
                                            $sale = Modules\ThemeLaptop88\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                                        } else {
                                            $sale = [];
                                        }
                                    }
//                                }
                                    if (array_diff([@$product->id], explode('|', @$sale_val->id_product)) == []) {
                                        $sale = Modules\ThemeLaptop88\Models\Product::whereIn('id', explode('|', @$sale_val->id_product_sale))->get();
                                    }
                                    if (!empty($sale)) {
                                        $sales = array_merge($sales, $sale->toArray());
                                    }
                                }
                                $ids = array_column($sales, 'id');
                                $ids = array_unique($ids);
                                $sales = array_filter($sales, function ($key, $value) use ($ids) {
                                    return in_array($value, array_keys($ids));
                                }, ARRAY_FILTER_USE_BOTH);
                                ?>


                                {{--                            @if(count($sales) > 0)--}}
                                {{--                                <div class="qua-content">--}}
                                {{--                                    <label>Quà tặng</label>--}}
                                {{--                                    <div class="qua-anh"><img style="width: 60px; height: 60px;"--}}
                                {{--                                                              src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo_qua_tang'], 60, null) }}"--}}
                                {{--                                                              alt="Qùa tặng"/></div>--}}
                                {{--                                    <div class="gift_content">--}}
                                {{--                                        @php  $totalPrice = 0; @endphp--}}
                                {{--                                        <ul>--}}
                                {{--                                            @foreach($sales as $data)--}}
                                {{--                                                <li>--}}
                                {{--                                                    - {{@$data['name']}}--}}

                                {{--                                                </li>--}}
                                {{--                                                @php $totalPrice += $data['base_price'];  @endphp--}}
                                {{--                                            @endforeach--}}
                                {{--                                        </ul>--}}
                                {{--                                        Trị giá: <b>{{number_format($totalPrice, 0, '.', '.')}} <sup>đ</sup></b>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                                {{--                            @endif--}}
                                {{--                        </div>--}}
                                {{--                        <div class="gift">{{@$product['intro']}}</div>--}}
                                @if(@$settings['show_detail'] == 1 && @$settings['show_add_cart'] == 0)
                                    <div>
                                        <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                                           title="{{@$product['meta_title']}}">
                                            <span class="gri-view-pro" style="width: 100%!important;">XEM HÀNG</span>
                                        </a>
                                    </div>
                                @elseif(@$settings['show_detail'] == 0 && @$settings['show_add_cart'] == 1)
                                    <div>
                            <span data-text="{{route('order.view')}}" style="cursor: pointer;width: 100%!important;"
                                  onclick="(addCart1({{@$product['id']}}))"
                                  class="gri-add-cart urlCart">Cho vào giỏ</span>
                                    </div>

                                @elseif(@$settings['show_detail'] == 1 && @$settings['show_add_cart'] == 1)
                                    <div>
                                        <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}"
                                           title="{{@$product['meta_title']}}"><span class="gri-view-pro">XEM HÀNG</span></a>
                                        <span data-text="{{route('order.view')}}" style="cursor: pointer"
                                              onclick="(addCart1({{@$product['id']}}))"
                                              class="gri-add-cart urlCart">Cho vào giỏ</span>
                                    </div>
                                @elseif(@$settings['show_detail'] == 0 && @$settings['show_add_cart'] == 0)
                                    <div style="display: none;"></div>
                                @endif
                            </div>
                        </div>

                    </div>
                <?php endwhile; ?>
            </div>
        <?php endfor; ?>
    </div>
</div>
{{--@if(count($products) < $countProduct)--}}
{{--    <div data-value="20" class="f" id="pmore"><p id="showModel">Xem thêm ...</p></div>--}}
{{--@endif--}}

@if ($isPagination)
    <div class="pagination">
        <?php for($i = 1; $i <= $numOfPage; $i++): ?>
        <button class="btn btn-default" data-page="{{$i}}" onclick="switchpage({{$i}})">{{$i}}</button>
        <?php endfor; ?>
    </div>
@endif

<style>
    .page-product .gri {
        margin: 1% 0;
    }
    .page-product.show {
        display: block;
    }
    .page-product.hide {
        display: none;
    }
    .pagination :not(.active) {
        color: black;
    }
    .pagination .active {
        background-color: #f47b06;
    }
</style>
<script>
    $(document).ready(function(){
        $('#page1').removeClass('hide').addClass('show');
        $(`[data-page=${1}]`).addClass('active');
    })
    function switchpage(id) {
        $('.page-product.show').removeClass('show').addClass('hide');
        $('#page'+id).addClass('show');
        $('.pagination .active').removeClass('active');
        $(`[data-page=${id}]`).addClass('active');
    }
</script>


