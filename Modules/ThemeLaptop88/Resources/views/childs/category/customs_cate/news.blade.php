
<style>
    @import url(https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);

    .f2-h {
        width: 30% !important;
    }
    .f2-hsx a {
        height: 25%;
    }

    .new_post {
        width: 69% !important;
        margin-left: 1%;

    }

    .new_post label {
        display: block;
        font: 16px/40px arial;
        text-align: center;
        margin-bottom: 16px;
        background: #f6f6f6;
    }



    .f2-h label, .f2-topic label {
        font: 16px/40px arial;
    }


    .cate_newss {
        margin: 0;
        overflow: hidden;
        /*border: 1px solid #ddd;*/
    }
    .cate_new_content h3 {
        display: inline;
        font-size: 14px;
    }

    @media (max-width: 991px) {
        .f2-h, .new_post {
            width: 100% !important;
        }
        .cate_newss {
            height: unset;
        }

        .news_none_mobie{
            display: none;
        }

        .row.cate_newss .cat-item {
            padding: 0;
            margin-bottom: 15px;
        }
        .hpanelwrap {
            height: unset;
            padding: 10px 5px;
        }
        .hpanelwrap #hpanel {
            position: relative !important;
        }
        .f2-head>.b>.f2-h {
            margin-bottom: 20px;
        }
    }
    @media(min-width: 768px) {
        .cate_newss > .cat-item:nth-child(1) {
            padding-left: 0;
        }

        .cate_newss > .cat-item:nth-child(2) {
            padding: 0;
        }

        .cate_newss > .cat-item:nth-child(3) {
            padding-right: 0;
        }
    }

    @media only screen and (max-width: 768px) {
        .f2-hsx a img {
            height: auto;
        }

    }

</style>
<?php
$brans = CommonHelper::getFromCache('manufacturer_id_order_no_asc' . @implode('|', $bran_ids),['manufactureres']);
if (!$brans) {
    $brans = \Modules\ThemeLaptop88\Models\Manufacturer::where('status', 1)->whereIn('id', $bran_ids)->orderBy('order_no', 'asc')->get();
    CommonHelper::putToCache('manufacturer_id_order_no_asc' . @implode('|', $bran_ids), $brans,['manufactureres']);
}
?>
@include('themelaptop88::partials.slide')
    <div class="b">
        <div class="flexCol">
            <div class=" flexCol f2-hsx ">
                <div class="hpanelwrap logo-category clearfix" style="overflow-y:auto;">
                    <div class="list-launcher" id="hpanel" >
                        <?php
                        $banners =  CommonHelper::getFromCache('category_brands_block',['manufactureres']);
                        if (!$banners){
                            $banners = \Modules\ThemeLaptop88\Models\Manufacturer::where('status', 1)->whereIn('id', $bran_ids)->orderBy('order_no', 'asc')->get();
                            CommonHelper::putToCache('category_brands_block', $banners,['manufactureres']);
                        }
//                        $last_key = count($product);
                        $i = 0;
                        ?>
                        @if(!empty($banners))
                            @foreach($banners as $banner)

                                <a class="manuface" data-value="{{$banner->id}}" title="{{$banner->name}}"
                                   href="{{route('cate.list',['slug' => $category->slug.'/'.$banner->slug])}}">
                                    <img class="lazy" data-src="{{\Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getUrlImageThumbNoCut($banner->image)}}"
                                         alt="{{$banner->image}}"/>
                                </a>
                            @endforeach

                        @endif
                    </div>
                    <div class="list-sub-category">
                        @foreach($child_category_show as $child_cate)
                            <a href="{{route('cate.list', ['slug' => $child_cate->slug])}}"
                               title="{{$child_cate->name}}">{{$child_cate->name}}</a>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>

<script>
    if($(window).width() > 1200){
        $("#hpanel").owlCarousel({
            autoPlay: !0,
            items: 3,
            navigation: false,
            pagination: false,
            itemsTablet: [768, 3],
            itemsMobile: [479, 3],
            responsiveRefreshRate: 100,
            loop:true,
            navigationText: ["<i class='glyphicon glyphicon-menu-left'></i>", "<i class='glyphicon glyphicon-menu-right'></i>"]
        })
    }
</script>