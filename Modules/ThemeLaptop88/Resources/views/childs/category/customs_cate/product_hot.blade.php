
<style>
    @import url(https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
    .product_featured {
        width: 69%!important;
        margin-left: 1%;

    }
    .product_featured label {
        display: block;
        font: 16px/40px arial;
        text-align: center;
        margin-bottom: 16px;
        background: #f6f6f6;
    }
    .f2-h {
        width: 30%!important;
    }
    div#hpanel a.manuface{
        width: 23%;
        height: auto;
        padding: 5px;
        margin: 1%;
        border-radius: 10px;
        border: 1px solid #eee;
    }


    .col-item
    {
        /*border: 1px solid #E1E1E1;*/
        border-radius: 5px;
    }
    .price>h3>a:hover {
        text-decoration: none;
        color: #ffa103;
    }
    .price>h3{
        padding: 0 10px;
        font-size: 17px;
        margin: 0;
        text-decoration: none;
        text-transform: uppercase;
    }
    .price>h3>a{

        font-weight: 600;
        font-size: 12px;
        color: #1d537f;

    }
    .col-item .photo img
    {
        margin: 0 auto;
        width: 100%;
        transition: all .3s ease-in-out;
        object-fit: contain;
    }
    .photo {
        height: 120px;
        overflow: hidden;
        margin: 0;
    }
    .photo>a {
        display: block;
        height: 100%;
    }
    .col-item .info
    {
        padding: 0 0 10px 0;
        border-radius: 0 0 5px 5px;
        margin-top: 1px;
    }

    .col-item:hover .info {
        /*background-color: #F5F5DC;*/
    }
    .col-item .price
    {
        /*width: 50%;*/
        /*float: left;*/
        margin-top: 5px;
    }

    .col-item .price h5
    {
        line-height: 20px;
        margin: 0;
    }

    .price-text-color
    {
        color: #219FD1;
    }

    .col-item .info .rating
    {
        color: #777;
    }

    .col-item .rating
    {
        /*width: 50%;*/
        float: left;
        font-size: 17px;
        text-align: right;
        line-height: 52px;
        margin-bottom: 10px;
        height: 52px;
    }

    .col-item .separator
    {
        /*border-top: 1px solid #E1E1E1;*/
    }

    .clear-left
    {
        clear: left;
    }

    .col-item .separator p
    {
        line-height: 20px;
        margin-bottom: 0;
        margin-top: 10px;
        width: 100%;
        text-align: center;
    }

    .col-item .separator p i
    {
        margin-right: 5px;
    }
    .col-item .btn-add
    {
        width: 50%;
        float: left;
    }

    .col-item .btn-add
    {
        /*border-right: 1px solid #E1E1E1;*/
    }

    .col-item .btn-details
    {
        width: 50%;
        float: left;
        padding-left: 10px;
    }
    .controls
    {
        margin-top: 20px;
    }
    [data-slide="prev"]
    {
        margin-right: 10px;
    }
    .width20 {
        width: 33.333%;!important;
        padding: 0 10px;
    }
    .f2-h label, .f2-topic label {
        font: 16px/40px arial;
    }
    span.gri-view-pro {
        background: linear-gradient(to top,#ffa103,#fb7d0f);
        color: #fff;
        font: bold 12px/33px Arial;
        padding: 0 10px;
        border-radius: 5px;
        text-transform: uppercase;
    }
    span.gri-add-cart {
        background: linear-gradient(to top,#ffa103,#fb7d0f);
        color: #fff;
        font: bold 12px/33px Arial;
        padding: 0 10px;
        border-radius: 5px;
        text-transform: uppercase;
    }
    a.arow-left:before {

        background: #e4e4e4!important;
        position: absolute!important;
        left: 5px!important;
        top: 50%!important;
        border-radius: 3px!important;
        height: 42px!important;
        width: 40px!important;
        line-height: 42px!important;
        text-align: center!important;
        z-index: 1!important;
        font-size: 24px;
        opacity: 0.5;
        color: #999;
    }
    a.arow-right:before {
        background: #e4e4e4!important;
        position: absolute!important;
        right: 0!important;
        top: 47%!important;
        border-radius: 3px!important;
        opacity: 0.5;
        color: #999;
        height: 42px!important;
        width: 42px!important;
        line-height: 42px!important;
        font-size: 24px;
        text-align: center!important;
        z-index: 1!important;
    }
    .pro_hot>h3{
        border-left: 5px solid red;
        background: #eee;
        padding: 10px 0 10px 10px;
        margin: 20px 0 10px 0;
    }
    .pro_hot {
        margin-top: 20px;
        padding: 0;
    }
    @media (max-width: 991px){
        .f2-h, .product_featured{
            width: 100%!important;
        }
    }
    @media (max-width: 500px){
        .photo{
            height: unset;
        }
    }
    @media (max-width: 768px){
        #Product .gri{
            width: 100%!important;
        }
    }
    .slide_product{
        border: 1px solid #ddd;
        display: flex
    }
    /*.hpanelwrap{*/
    /*    height: 290px!important;*/
    /*}*/
    .slide_product_content {
        width:100%;
        border: 1px solid red!important
    }
    .list_product {
        width: 33.333%;
        padding-right: 2%;
    }
    .list_products {
        display: flex;
    }

    @media only screen and (max-width: 768px) {
        .f2-hsx a img {
            height: auto;
        }

        .list_product {
            width: 50%;
        }
    }
</style>
@include('themelaptop88::partials.slide')
<div class="b">
    <div class="flexCol">
        <div class=" flexCol f2-hsx ">
            <div class="hpanelwrap logo-category clearfix" style="overflow-y:auto;">
                <div class="list-launcher" id="hpanel" >
                    <?php
                    $banners =  CommonHelper::getFromCache('category_brands_block',['banners']);
                    if (!$banners){
                        $banners = \Modules\ThemeLaptop88\Models\Manufacturer::where('status', 1)->whereIn('id', $bran_ids)->orderBy('order_no', 'asc')->get();
                        CommonHelper::putToCache('category_brands_block', $banners,['banners']);
                    }
                    //                        $last_key = count($product);
                    $i = 0;
                    ?>
                    @if(!empty($banners))
                        @foreach($banners as $banner)

                            <a class="manuface" data-value="{{$banner->id}}" title="{{$banner->name}}"
                               href="{{route('cate.list',['slug' => $category->slug.'/'.$banner->slug])}}">
                                <img class="lazy" data-src="{{\Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getUrlImageThumbNoCut($banner->image)}}"
                                     alt="{{$banner->image}}"/>
                            </a>
                        @endforeach

                    @endif
                </div>
                <div class="list-sub-category">
                    @foreach($child_category_show as $child_cate)
                        <a href="{{route('cate.list', ['slug' => $child_cate->slug])}}"
                           title="{{$child_cate->name}}">{{$child_cate->name}}</a>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    if($(window).width() > 1200){
        $("#hpanel").owlCarousel({
            autoPlay: !0,
            items: 3,
            navigation: false,
            pagination: false,
            itemsTablet: [768, 3],
            itemsMobile: [479, 3],
            responsiveRefreshRate: 100,
            loop:true,
            navigationText: ["<i class='glyphicon glyphicon-menu-left'></i>", "<i class='glyphicon glyphicon-menu-right'></i>"]
        })
    }
</script>