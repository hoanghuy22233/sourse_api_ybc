
<style>
    .popup-add-cart{
        z-index: 999;
    }
    .gri-view-pro, .gri-add-cart{
        background: @if(@$settings['header_background']!=''){{@$settings['header_background']}} @else {{'linear-gradient(to bottom,#ffa103,#fb7d0f)'}}@endif!important;
        border-radius: 4px!important;
        border: none!important;
        color: #fff!important;
    }
    @media (min-width:990px) {
        .popup-add-cart-content{
            margin-top: 10%;
        }
    }
    .product_intro{
        height: 55px!important;
        text-align: justify;
    }
    @media (max-width:768px) {
        .gri{
            width: 46%;
            margin: 2%;
        }
        .banner-abc ul.flexJus{
            height: unset;
        }
        /*.f2-head>.b>.f2-topic ul li{*/
        /*    width: 100% !important;*/
        /*}*/
        .product_intro{
            height: 85px!important;
            text-align: justify;
        }
    }
    span.gri-add-cart.urlCart {
        float: right!important;
    }
    .gi h2{
        color: #000;
        font-size: 15px;
        text-transform: uppercase;
        height: 30px;
    }
    @media (max-width: 767px){
        .f>.inline-bl .flexJus {
            text-align: center;
        }
        #Product .gri .gi h2{
            height: 51px!important;
            line-height: 17px!important;
            font-size: 12px;
            -webkit-line-clamp: 3!important;
        }
    }
    .gri{
        height: 570px;
    }

</style>
<div class="b ">
    <div class="product-sort d-flex justify-content-between">
        <div class="">
            <span>Sắp xếp theo</span>
            <label id="sort_prd_price" data-sort_prd_price="1">
                @if(isset($_GET['sort_prd_price']))
                    @if($_GET['sort_prd_price']==1)
                        Giá Giảm
                    @else($_GET['sort_prd_price']==0)
                        Giá Tăng
                    @endif
                @else
                    Mặc định
                @endif
                <ul>
                    <li><a href="?sort_prd_price=1">Giá Giảm</a></li>
                    <li><a href="?sort_prd_price=0">Giá Tăng</a></li>
                </ul>
            </label>

        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#sort_prd_price').click(function(){
            $(this).find('ul').toggle();
        })
    })
</script>
<div class="b category-product" >
    <div class="b list-product p-list" id="Product">
        <?php
        $price_max = app('request')->get('max') ? app('request')->get('max') : 999999999999;
        $price_min = app('request')->get('min') ? app('request')->get('min') : 0;
        $page_show = app('request')->get('perpage') ? app('request')->get('perpage') : 12;
        $filterdProducts = array_filter($products, function ($p) use ($price_max,$price_min) {
            return $p['final_price'] >= $price_min && $p['final_price'] <= $price_max;
        });
        sort($filterdProducts);
        $numOfProduct = count($filterdProducts);
        if ($numOfProduct < $page_show) {
            $isPagination = false;
        } else {
            $isPagination = true;
        }
        $du = $numOfProduct % $page_show;
        $numOfPage =  floor($numOfProduct / $page_show);
        if ($du > 0) {
            $numOfPage++;
        }
        $pindex = 0;
        ?>
        <?php for ($i = 1; $i <= $numOfPage; $i++): ?>
        <div id="page{{$i}}" class="page-product hide">
            <?php
            $count = 0;
            while ($count < $page_show && $pindex < $numOfProduct):
            ?>
            <?php
            $product = $filterdProducts[$pindex];
            $count++;
            $pindex++;
            ?>
            <?php
            $manufacture = CommonHelper::getFromCache('manufacture_id'.@$product['manufacture_id']);
            if (!$manufacture){
                $manufacture = \Modules\ThemeLaptop88\Models\Manufacturer::find($product['manufacture_id']);
                CommonHelper::putToCache('manufacture_id'.@$product['manufacture_id'], $manufacture);
            }
            ?>
                <div class="p-item js-p-item">
                    <div class="p-img">
                        <a href="{{ Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}" class="p-img">
                            <img  data-img="{{$product['image']}}"class="lazy" data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($product['image'], 250, null) }}" alt="{{$product['name']}}">
                        </a>
                    </div>
                    <div class="p-emtry">
                        <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($product, 'array') }}" class="p-name">{{$product['name']}}</a>
                        <span class="p-summary">{!! $product['intro'] !!}</span>
                        <span class="p-price"><b>@if($product['final_price'] == 0) Liên hệ @else {{number_format($product['final_price'], 0, '.', '.')}}đ @endif</b></span>
                    </div>
                    <div class="p-hover">
                        <a href="{{ Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($product,'array') }}" class="p-name">{{$product['name']}}</a>
                        <span class="p-price"><b>@if($product['final_price'] == 0) Liên hệ @else {{number_format($product['final_price'], 0, '.', '.')}}đ @endif</b></span>
                        <p class="mb-0"><strong>Bảo hành: </strong></p>
                        <?php
                        $product_guarantees = CommonHelper::getFromCache('product_guarantees' . $product['id'],['guarantees']);
                        if (!$product_guarantees) {
                            $product_guarantees =  \Modules\ThemeLaptop88\Models\Guarantees::where('id',$product['guarantee'])->first();
                            CommonHelper::putToCache('product_guarantees' . $product['id'], $product_guarantees,['guarantees']);
                        }
                        ?>
                        <p class="p-guarantee">✅ {{@$product_guarantees->name}}</p>
                        <p class="p-stock"><strong>Kho hàng: </strong> <span>{{($product['status'] == 1)?'Còn hàng':'Hết hàng'}}</span></p>
                        <hr>
                        <div class="p-summary">
                            <strong>Mô tả tóm tắt</strong>
                            {{$product['intro']}}
                        </div>
                        <hr>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <?php endfor; ?>
    </div>
</div>

    @if ($isPagination)
        <div class="pagination">
            <?php for($i = 1; $i <= $numOfPage; $i++): ?>
            <button class="btn btn-default" data-page="{{$i}}" onclick="switchpage({{$i}})">{{$i}}</button>
            <?php endfor; ?>
        </div>
    @endif

    <style>
        .page-product .gri {
            margin: 1% 0;
        }
        .page-product.show {
            display: flex !important;
            flex-wrap: wrap;
        }
        .page-product.hide {
            display: none;
        }
        .pagination {
            align-self: center;
        }
        .pagination :not(.active) {
            color: black;
        }
        .pagination .active {
            background-color: #f47b06;
        }
    </style>
    <script>
        $(document).ready(function(){
            $('#page1').removeClass('hide').addClass('show');
            $(`[data-page=${1}]`).addClass('active');
        })
        function switchpage(id) {
            $('.page-product.show').removeClass('show').addClass('hide');
            $('#page'+id).addClass('show');
            $('.pagination .active').removeClass('active');
            $(`[data-page=${id}]`).addClass('active');
        }
    </script>
<style>

    .gri>.gi>a{
        height: 200px!important;
    }
</style>
    <div class="b cate_banner">
        @include('themelaptop88::partials.footer_banner')
    </div>

