@extends('themelaptop88::layouts.master')
@section('main_content')

    <style>
        header{padding:5px 0;}
        .logo {
            width: 14%;
            margin-right: 2%
        }

        .tc {
            width:58%;
            margin:0 0 0 2%;
        }

        .tc.flexJus a span {
            color: white
        }

        .tc a {
            font: bold 12px/20px arial;
            display: inline-block;
            text-transform: uppercase;
            text-align: center
        }

        .tc a {
            color: #eee;
            font: bold 11px/20px arial;
            display: inline-block;
            text-transform: uppercase;
            text-align: center
        }

        .tc a:nth-last-child(1) {
            padding-right: 0
        }

        .tc a img {
            display: block;
            margin: 0 auto
        }

        .tc span {
            display: block;
            height: 15px;
            overflow: hidden;
            margin-top: 2px;
            margin-bottom: 5px
        }

        /*.search {
            width: 14%
        }

        .search input {
            border-radius: 4px 0 0 4px;
            width: 600px;
            height: 38px;
            border: 1px solid #ccc;
            text-indent: 5px;
            max-width: 85%;
            border-right: none
        }

        .search button {
            background-color: #fff;
            color: #666;
            padding: 8px 0;
            border-radius: 0 4px 4px 0;
            height: 38px;
            border: 1px solid #ccc;
            border-left: none;
            width: 100px;
            max-width: 15%
        }

        .search button i:before {
            content: "\f002";
            font: 17px/1 FontAwesome
        }*/

        .tim {width:20%;position:relative;}
        .tim input {border-radius:15px;width: 100%;height:26px;border: 1px solid #ccc;text-indent: 5px;padding-right:105px;font-size:12px;}
        .tim button {color: #666;height: 26px;border: none;width: 100px;background:transparent;max-width: 15%;position:absolute;top:0;right:0;}
        .tim button i:before {content: "\f002";font: 17px/1 FontAwesome}
    </style>

    <section>

    @include('themelaptop88::partials.breadcrumb')

        <div class="content">
            <?php
            $khao_sat_post = \Modules\ThemeLaptop88\Models\Post::find($settings['khao_sat_post']);
            ?>
            {!! @$khao_sat_post->content !!}
            <style>
                .content img{display:block;}
                form.form-top {
                    padding: 40px 20px;background:#eee;position:relative;height:350px;
                }
                .form-top .tx02 {
                    font-size:18px;
                    margin-bottom: 15px;
                    background: linear-gradient(to right, #edcf89, #ddb968, #f7e1a8, #f0d697, #eccd85, #d6d014 );
                    -webkit-background-clip: text;
                    -webkit-text-fill-color: transparent;
                }

                .box-form-register {
                    background: #fff;
                    border-radius: 15px;
                    padding: 40px 30px;
                    margin-top: 25px;
                    box-shadow: 0 8px 20px -6px black;width:500px;margin:0 auto;position:absolute;left:calc(50% - 225px);top:-165px;
                }

                .form-group {
                    width: 100%;
                    margin-bottom: 10px;
                    text-align: left;
                }

                input.form-controls {
                    height: 38px;
                    width: 100%;
                    padding: 5px 10px;
                    border-radius: 5px;
                    border: 1px solid #efefef;
                    color: #5d5d5d;
                    background: #efefef;
                }

                select.form-controls {
                    width: 100%;
                    height: 38px;
                    padding: 8px 5px;
                    border-radius: 5px;
                    border: 1px solid #efefef;
                    color: #5d5d5d;
                    background: #efefef;
                }

                .formTopLeft {
                    width: 320px;
                    margin: 0 auto;
                    font-family: tahoma;
                }

                :focus {
                    outline: unset !important;
                }

                .btn-register-top {
                    background: #dc2013;
                    background: -webkit-linear-gradient(to right, #ff693a, #dc2013);
                    background: linear-gradient(to right, #ff693a, #dc2013);
                    border: none;
                    padding: 10px 20px;
                    color: #fff;
                    border-radius: 5px;
                    width: 100%;
                    font-weight: bold;
                    font-size: 16px;
                    cursor: pointer;
                    margin-top: 10px;
                    margin-bottom: 5px;
                }

                .btn-register-top:hover {
                    opacity: 0.9;
                }
            </style>
            <form class="form-top f" method="post">
                <div class="box-form-register">
                    <div class="formTopLeft">
                        <h3 class="txt tx02 cen">ĐĂNG KÝ NHẬN DỊCH VỤ</h3>
                        <div class="form-group">
                            <input type="hidden" name="Campaign" value="Khảo sát tư vấn lắp đặt miễn phí">
                            <input name="Name" type="text" required="required" class="form-controls clears name" placeholder="Nhập họ tên">
                        </div>
                        <div class="form-group">
                            <input type="Phone" pattern="(09|01[2|6|8|9])+([0-9]{8})\b" title="Có 10 hoặc 11 số bắt đầu bằng 0" name="Phone" required="required" class="form-controls clears phone" placeholder="Nhập số điện thoại.">
                        </div>
                        <div class="form-group">
                            <input type="text" name="Content" class="form-controls clears contents" required="required" placeholder="Nhập sản phẩm.">
                        </div>
                        <div class="form-group">
                            <input type="text" name="Address" class="form-controls clears address" required="required" placeholder="Nhập địa chỉ.">
                        </div>
                        <input type="hidden" name="State" value="4">
                        <input type="submit" name="" class="btn-register-top" value="ĐĂNG KÝ NGAY">
                        <small style="display:block;" class="cen">Cam kết mọi thông tin của bạn sẽ được bảo mật</small>
                    </div>
                </div>
            </form>

        </div>

    </section>
    <script>
        $(document).ready(function () {
            $('.form-top').on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    url: '/ajax/contacts?Name='+$('.name').val()+'&Phone='+$('.phone').val()+'&Content='+$('.contents').val()+'&Address='+$('.address').val(),
                    type: "POST",
                    success:function (data) {
                        if (data.success == true){
                            $('.clears').val('');
                            alert('Đăng ký thành công');
                        }
                    }
                })
            })
        })
    </script>
@endsection