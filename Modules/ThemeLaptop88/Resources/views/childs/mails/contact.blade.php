
<div style="text-align: center"><h3>Vừa có liên hệ mới gửi ở trang {{ $_SERVER['SERVER_NAME'] }}</h3></div>

<div style="text-align: center"><h4>Thông tin khách hàng</h4></div>
<div class="container-table100">
    <div class="wrap-table100">
        <div class="table100 ver1">
            <div class="wrap-table100-nextcols js-pscroll ps ps--active-x" style="width: 80%; margin: auto;">
                <div class="table100-nextcols">
                    <table style="border-collapse: collapse; width: 100%;margin: auto; border: 1px solid gray">
                        <thead>
                        <tr style="border: 1px solid gray;" class="row100 head">
                            <th style="border: 1px solid gray;" class="cell100 column2">Họ & tên</th>
                            <th style="border: 1px solid gray;" class="cell100 column3">SĐT</th>
{{--                            <th style="border: 1px solid gray;" class="cell100 column4">Email</th>--}}
{{--                            <th style="border: 1px solid gray;" class="cell100 column4">Địa chỉ</th>--}}
                            <th style="border: 1px solid gray;" class="cell100 column4">Lời nhắn</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Sản phẩm</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Tỉnh/Thành</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Quận/Huyện</th>
                            <th style="border: 1px solid gray;">URL</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="border: 1px solid gray;">{{ @$contact->name }}</td>
                            <td style="border: 1px solid gray;">{{ @base64_decode($contact->tel) }}</td>
{{--                            <td>{{ @base64_decode($contact->email) }}</td>--}}
{{--                            <td>{{ @$contact->address }}</td>--}}
                            <td style="border: 1px solid gray;">{!! @$contact->content !!}</td>
                            <td style="border: 1px solid gray;">
                                {{ @$contact->product_id }}
                            </td>
                            <td style="border: 1px solid gray;">{{ @$contact->provinces->name }}</td>
                            <td style="border: 1px solid gray;">{{ @$contact->district->name }}</td>
                            <td style="border: 1px solid gray;">{{ @$contact->url }}</td>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>