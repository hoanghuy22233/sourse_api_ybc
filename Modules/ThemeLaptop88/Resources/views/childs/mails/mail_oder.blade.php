<div style="text-align: center"><h3>Xin Chào: {{$user_name}}</h3></div>
<div style="text-align: center"><h3>Thông tin đơn hàng của bạn</h3></div>
<div class="container-table100">
    <div class="wrap-table100">
        <div class="table100 ver1">
            <div class="wrap-table100-nextcols js-pscroll ps ps--active-x" style="width: 80%; margin: auto;">
                <div class="table100-nextcols">
                    <table style="border-collapse: collapse; width: 100%;margin: auto; border: 1px solid gray">
                        <thead>
                        <tr style="border: 1px solid gray;" class="row100 head">
                            <th style="border: 1px solid gray;" class="cell100 column2">Tên sản phẩm</th>
                            <th>Thuộc tính</th>
                            <th style="border: 1px solid gray;" class="cell100 column3">Số lượng</th>
                            <th style="border: 1px solid gray;" class="cell100 column4">Giá</th>
                            <th>Quà tặng</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($getCart as $cart)
                            <tr style="border: 1px solid red; text-align: center;" class="row100 body">
                                <td style="border: 1px solid gray;" class="cell100 column2">{{$cart['name']}}</td>
                                <td style="border: 1px solid gray;" class="qua-tang">
                                    <?php
                                    $product = \Modules\ThemeLaptop88\Models\Product::find($cart['id']);
                                    ?>
                                    @include('themelaptop88::childs.order.partials.attributes', ['attr' => $cart['attr'], 'product' => $product])
                                </td>
                                <td style="border: 1px solid gray;" class="cell100 column3">{{$cart['quantity']}}</td>
                                <td style="border: 1px solid gray;"
                                    class="cell100 column4">{{number_format($cart['price'], 0, '.', '.')}} <sup>đ</sup>
                                </td>
                                <td style="border: 1px solid gray;" class="qua-tang">
                                    <?php
                                    $gifts =explode(',',$cart['gift']);
                                    $prd_gifts = \Modules\ThemeLaptop88\Models\Product::whereIn('id',$gifts)->get();

                                     ?>
                                    @if(!empty($prd_gifts))
                                        @include('themelaptop88::partials.qua_tang')
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <?php
                    $totalPrice=0;
                    foreach($prd_gifts as $data){
                        $totalPrice+=$data['base_price'];
                    }
                    $prd_category_id = @\Modules\ThemeLaptop88\Models\Product::find($cart['id'])->category->gift_value_max;
                    if ($totalPrice > $prd_category_id) {
                        $totalPrice -= $prd_category_id;
                    }
                    ?>
                    <div style="text-align: right; border: 1px solid gray; border-top: none;"><p
                                style="padding: 10px; margin: 0">Tổng: {{number_format($total_price+$totalPrice, 0, '.', '.')}}
                            <sup>đ</sup></p></div>
                </div>
                <div class="ps__rail-x" style="width: 563px; left: 0px; bottom: 10px;">
                    <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 160px;"></div>
                </div>
                <div class="ps__rail-y" style="top: 0px; right: 0px;">
                    <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
