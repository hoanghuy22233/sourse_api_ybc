@extends('themelaptop88::layouts.master')
@section('main_content')
    @php
        $province = Modules\ThemeLaptop88\Models\Province::all();
    @endphp
    <section class="f">

        <!--CSS-->
        <style>
            .qua_tang_label {
                height: 64px;
                line-height: 64px !important;
            }

            /*giohang*/
            .orderbox {
                display: table;
                width: 800px;
                margin: 0 auto;
            }

            .los {
                display: block;
                text-align: center;
                font: 11px/40px arial;
                color: #999;
            }

            .dieuhuong {
                margin: 0;
                float: left;
                width: 100%;
                background: #fafafa;
            }

            .dieuhuong a {
                text-align: right;
                font: bold 13px/40px arial;
                text-indent: 20px;
            }

            .dieuhuong a:before {
                font: normal 14px/1 FontAwesome;
                margin: 0 5px;
                content: "\f104";
            }

            .dieuhuong label {
                float: right;
                display: inline-block;
                font: 13px/40px arial;
                color: #666;
            }

            .disabled {
                pointer-events: none;
                cursor: wait !important;
            }

            .od {
                background: -webkit-linear-gradient(top, #ffa103, #fb7d0f);
                padding: 8px 0;
                font: bold 16px/27px Arial;
                border-radius: 5px;
                color: #fff;
                text-align: center;
            }

            .od span {
                display: block;
                font: 12px arial;
            }

            .fpanel {
                float: left;
                margin-top: 20px;
                width: 100%;
            }

            .fpanel label {
                float: left;
                display: block;
                font: 13px/25px arial;
            }

            .fpanel .od, .fpanel .odh {
                width: 300px;
            }

            .orderhome {
                float: left;
                width: 800px;
                border: 1px solid #ddd;
                background: #fff;
                box-shadow: 0 0 5px #ccc;
            }

            .giohang {
                padding: 10px 20px;
                display: table;
            }

            .total {
                float: left;
                width: 100%;
                margin: 12px 0;
                font-size: 13px;
            }

            .total * {
                display: inline-block;
            }

            .total span {
                float: left;
                font-weight: bold;
            }

            .total b {
                float: right;
                color: red;
            }

            #cart, #fod {
                float: left;
                width: 100%;
            }

            #cart {
                margin-bottom: 20px;
            }

            .odel {
                color: #666;
                display: block;
                margin-top: 10px;
            }

            .odel:before {
                content: "\f056";
                font: normal 11px/1 FontAwesome;
                margin-right: 3px;
            }

            .ohbanner img {
                max-width: 100%;
            }

            /*product col 1 style amazon*/

            .listcart {
                width: 100%;
                float: left;
                padding: 0;
            }

            .listcart .cartitem {
                float: left;
                width: 100%;
                padding: 10px 0;
                border-bottom: 1px solid #eee;
                position: relative;
            }

            .listcart .cartitem .oimg {
                width: 10%;
                height: 100%;
                position: relative;
                float: left;
                margin: 0 5% 0 0;
                text-align: center;
            }

            .listcart .cartitem .oimg img {
                max-width: 100%
            }

            .listcart .cartitem .oname {
                width: 85%;
                float: left;
                margin-bottom: 5px;
            }

            .listcart .cartitem h3 {
                float: left;
                width: 70%;
                margin: 0 3% 0 0;
                line-height: 15px;
            }

            .listcart .cartitem h3 a {
                font: bold 12px/15px arial;
                color: #444;
            }

            .listcart .cartitem label {
                color: red;
                font: bold 12px/15px arial;
                display: inline-block;
                width: 27%;
                text-align: right;
            }

            .oqt {
                width: 100%;
                float: left;
                margin-top: 5px;
                background: #eee;
                padding: 2%;
            }

            .oqt span {
                font: bold 12px/20px arial;
                display: block;
                color: #078ec0;
            }

            .oqt ul {
                padding: 0;
            }

            .oqt li {
                font: 12px/22px arial;
            }

            .oqt li:before {
                content: "\f111";
                font: normal 4px/1 FontAwesome;
                margin-right: 3px;
                vertical-align: middle;
            }

            .ArrCount {
                float: left;
                width: 100%;
                margin: 5px 0;
            }

            .ArrCount span, .ArrCount input {
                display: inline-block;
                width: 40px;
                text-align: center;
                border: 1px solid #ddd;
                height: 30px;
                line-height: 30px;
                border-right: none;
            }

            .ArrCount span:nth-last-child(1) {
                border-right: 1px solid #ddd;
            }

            .ArrCount span {
                font: bold 15px/30px arial;
            }

            .ArrCount input {
                vertical-align: top;
            }

            .group {
                width: 100%;
                float: left;
                margin-top: 10px;
            }

            .option {
                display: inline-block;
                margin-right: 25px;
            }

            .opanel {
                margin: 0 0 0 25px !important;
            }

            .option input {
                margin-right: 3px;
            }

            .group .fixinput {
                width: 100% !important;
            }

            button {
                cursor: pointer;
            }

            .group input[type=text], .group input[type=text]:nth-child(3) {
                border: 1px solid #ddd;
                padding: 1%;
                margin-bottom: 15px;
                height: 40px;
                border-radius: 10px;
            }

            .group input[type=text] {
                width: 49%;
            }

            .group input[type=text]:nth-child(1) {
                margin-right: 1%;
            }

            .group input[type=text]:nth-child(3), .group input[type=text]:nth-child(4) {
                width: 100%;
            }

            .paytype {
                display: none;
                background: #eee;
                width: 100%;
                padding: 5% 2%;
                float: left;
                margin-top: 10px;
                border-radius: 0 0 10px 10px;
            }

            .paytype input[type=text] {
                width: 100%;
                border: 1px solid #ddd;
                padding: 1%;
                margin-bottom: 10px;
                height: 40px;
                border-radius: 10px;
            }

            .agent {
                float: left;
                width: 100%;
                margin: 8px 0;
                color: #078ec0
            }

            .agent input {
                margin-right: 3px;
            }


            .group select {
                margin-right: 1%;
            }

            .group > select {
                width: 49%;
                float: left;
            }

            .group > select {
                border: 1px solid #ddd;
                padding: 1%;
                margin-bottom: 15px;
                height: 40px;
                border-radius: 10px;
            }

            select {
                color: #6b6868;
            }

            select option {
                color: black;
            }

            .gift {
                display: inline-block;
                width: 100%;
                height: auto;
                border: 0;
            }

            .qua-content label {
                text-align: left !important;
                margin-bottom: 20px !important;
                color: #000 !important;
            }
            @media(max-width: 768px) {
                .listcart .cartitem label {
                    width: 100%;
                    height: unset !important;
                    line-height: unset !important;
                    margin: 0 !important;
                }
            }
        </style>
        <div class="orderbox">
            <div class="dieuhuong">
                @if($data['cate'] != null)
                    <a id="backCart" href="{{route('cate.list', ['slug' => $data['cate']->slug])}}" class=""
                       title="Tiếp tục mua hàng">Tiếp tục mua hàng</a>
                @else
                    <a id="backCart" href="{{route('home')}}" class=""
                       title="Tiếp tục mua hàng">Tiếp tục mua hàng</a>
                @endif
                <label>Giỏ hàng của bạn </label>
                @if($data['msg'] != '')
                    <p style="text-align: center; background-color: #d4edda; padding: 10px 0px; color: #155724;border: 1px solid transparent;">{{$data['msg']}}</p>
                @endif
                @if(empty($data['cate']) && ($data['msg'] == '' || ($data['msg'] == null)))
                    <p style="text-align: center; background-color: #f3d0db; padding: 10px 0px; color: #e05f5f;border: 1px solid transparent;">
                        Giỏ hàng của bạn hiện không có sản phẩm</p>
                @endif

            </div>
            @if(!empty($cart) || $cart != null)
                <div class="giohang orderhome">
                    <form method="post" name="giohang" id="cart">
                        <ul class="listcart">
                            <?php
                            $price_gift = 0;
                            $gift_price1 = 0;
                            ?>
                            @foreach($cart as $i=>$data)

                                @php
                                    $product = \Modules\ThemeLaptop88\Models\Product::find(@$data['id']);
                                    $cate = \Modules\ThemeLaptop88\Models\Category::whereIn('id', explode('|', @$product->multi_cat))->get()->first();
                                    $manufacture = \Modules\ThemeLaptop88\Models\Manufacturer::find(@$product->manufacture_id);
                                $sales = \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getGifOfProduct($product, $qua_dang_hieu_luc = false);
                                @endphp
                                <li data-id="{{$data['id']}}" id="cart_list{{$data['id']}}" class="cartitem">
                                    <div class="oimg">
                                        <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($product) }}"
                                           title="{{ucfirst(mb_strtolower(@$cate->name))}}">
                                            <img class="lazy"
                                                 data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb($data['image'], 88, null) }}"
                                                 alt="{{ucfirst(mb_strtolower($data['name']))}}"></a>
                                        <a onclick="removeCart({{@$data['id'].', '}} this.href); return false;"
                                           href="{{route('remove.cart')}}" class="odel" rel="nofollow"
                                           title="Xóa sản phẩm khỏi đơn hàng">xóa</a>
                                    </div>
                                    <div class="oname">
                                        <h3>
                                            <a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($product) }}"
                                               title="{{$data['name']}}">{{$data['name']}}</a>
                                        </h3>
                                        <label id="cart_price{{@$data['id']}}" class="cartitemprice"
                                               data-value="{{@$data['price']}}">{{number_format($data['price'], 0, '.', '.')}}
                                            &nbsp;<sup>đ</sup></label>

                                        {{--                                        @include('themelaptop88::partials.qua_tang')--}}

                                        @include('themelaptop88::childs.order.partials.attributes', ['attr' => $data['attr'], 'product' => $product])
                                        <div class="qua-content">
                                                <span class="qua-anh"><img class="lazy"
                                                                           style="width: 60px; height: 60px;"
                                                                           data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo_qua_tang'], 60, null) }}"
                                                                           alt="Qùa tặng"/></span>
                                            <label class="qua_tang_label">Quà tặng</label>
                                            <span class="gift_content">
                                                  @php  $totalPrice = 0; @endphp
                                                    <ul>
                                                    <?php
                                                        $cate_id = explode('|', trim($data["multi_cat"], '|'));
                                                        $gift_value_max = @\Modules\ThemeLaptop88\Models\Category::find(@$cate_id[0])->gift_value_max;
                                                        ?>
                                                        @foreach(Session::get('cart') as $k1=>$data1)
                                                            <li>
                                                            @if($i==$k1)
                                                                    <?php
                                                                    $gifts = [];
                                                                    if (!empty($data1['gift'])) {
                                                                        $gifts = explode(',', trim($data1["gift"], ','));
                                                                    }
                                                                    $gift_price = 0;

                                                                    ?>
                                                                    @if(count($gifts)>0)
                                                                        @foreach($gifts as $g)
                                                                            <?php
                                                                            $gift_price += \Modules\ThemeLaptop88\Models\Product::find($g)->base_price;
                                                                            ?>
                                                                            -
                                                                            <span style="text-transform: capitalize">{{ @\Modules\ThemeLaptop88\Models\Product::find($g)->name }}</span>
                                                                            <br>
                                                                        @endforeach
                                                                        @if($gift_price > $gift_value_max)
                                                                            <?php
                                                                            $gift_price1 = $gift_price - $gift_value_max;
                                                                            ?>
                                                                            <span style="text-transform: uppercase">GHI CHÚ:</span>
                                                                            Số tiền bạn phải bù thêm:
                                                                            <span style="color: red">{{number_format($gift_price1,0,'','.')}}<sup>đ</sup></span>
                                                            @endif
                                                            @else
                                                                @if(count($sales) <= 4)
                                                                    <?php
                                                                    $price_gift_4 = 0;
                                                                    ?>

                                                                    @foreach($sales as $sale1)
                                                                        <?php $price_gift_4 += $sale1['base_price'];?>
                                                                        <li>
                                                                              - {{ $sale1['name'] }}<br>

                                                                        </li>
                                                                    @endforeach
                                                                    <?php
                                                                    $product_sales = \Modules\ThemeLaptop88\Models\ProductSale::where('id_product', 'like', '%|' . $product->id . '|%')->get()->count();

                                                                    ?>
                                                                    @if($product_sales==0)
                                                                        <span style="font-size: 14px; text-transform: capitalize;">- Giảm: {{number_format($product->base_price - $product->final_price , 0,'','.')}}đ</span>
                                                                        <br>
                                                                    @else
                                                                        <li>
                                                                      <b style="font-size: 14px; text-transform: capitalize;">Trị giá: {{number_format($price_gift_4, 0, '.', '.')}} <sup>đ</sup></b>
                                                                      </li>

                                                                    @endif
                                                                @elseif(count($sales) > 4&& count($gifts)>0)
                                                                    <?php
                                                                    $gifts = [];
                                                                    if (!empty($data1['gift'])) {
                                                                        $gifts = explode(',', trim($data1["gift"], ','));
                                                                    }
                                                                    $gift_price = 0;

                                                                    ?>
                                                                    @foreach($gifts as $g)
                                                                        <?php
                                                                        $gift_price += \Modules\ThemeLaptop88\Models\Product::find($g)->base_price;
                                                                        ?>
                                                                        -
                                                                        <span style="font-size: 14px; text-transform: capitalize;">{{ @\Modules\ThemeLaptop88\Models\Product::find($g)->name }}</span>
                                                                        <br>
                                                                    @endforeach
                                                                    @if($gift_price > $gift_value_max)
                                                                        <?php
                                                                        $gift_price1 = $gift_price - $gift_value_max;
                                                                        ?>
                                                                        <span style="text-transform: uppercase">GHI CHÚ:</span>
                                                                        Số tiền bạn phải bù thêm:
                                                                        <span style="color: red">{{number_format($gift_price1,0,'','.')}}<sup>đ</sup></span>
                                                                    @endif
                                                                @else
                                                                    <?php
                                                                    $product_sales = \Modules\ThemeLaptop88\Models\ProductSale::where('id_product', 'like', '%|' . $product->id . '|%')->get()->count();

                                                                    ?>
                                                                    @if($product_sales==0)
                                                                        <span style="font-size: 14px; text-transform: capitalize;">- Giảm: {{number_format($product->base_price - $product->final_price , 0,'','.')}}đ</span>
                                                                        <br>

                                                                    @endif
                                                                    <span style="font-size: 14px; text-transform: capitalize;">-Không chọn quà</span>

                                                                    @endif
                                                                    @endif
                                                                    @endif


                                                                    </li>

                                                                    @endforeach
                                                    </ul>

                                                {{--Khuyến mại dạng text--}}
                                                <?php
                                                $sale_text = \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductsSaleText($product->id, @$cate->id, $product->manufacture_id)
                                                ?>
                                                @foreach($sale_text as $v)
                                                    {!! $v !!}
                                                @endforeach
                                            </span>
                                        </div>


                                        <div class="ArrCount">
                                            <span style="cursor: pointer;"
                                                  onclick="addQuantity({{$data['id'].', ' }} this.innerText, this); return false;"
                                                  class="sub disblade" data-rel="cart_quantity{{$data['id']}}">-</span>
                                            <input data-value="{{$data['quantity']}}" class="cartQuantity" type="text"
                                                   id="cart_quantity{{$data['id']}}" name="ArrCount"
                                                   value="{{$data['quantity']}}"/><span
                                                    style="cursor: pointer;"
                                                    onclick="addQuantity({{$data['id'].', '}} this.innerText, this); return false;"
                                                    class="cre disblade"
                                                    data-rel="cart_quantity{{$data['id']}}">+</span>
                                        </div>
                                    </div>
                                </li>

                                @php
                                    $price_gift += $gift_price1;

                                        $total+=$data['quantity']*$data['price']+$price_gift;
                                @endphp
                            @endforeach
                        </ul>
                        <div class="total">
                            <span>Tổng tiền</span>
                            <b data-value="{{@$total}}" class="total_money">{{number_format(@$total, 0, '.', '.')}}
                                &nbsp;<sup>đ</sup></b>
                        </div>
                        <input type="submit" name="_w_action[UpdatePOST]" style="display: none;"/>
                        <input type="hidden" name="_w_action" value="UpdatePOST"/>
                    </form>
                    <!--.box_27-->
                    <form method="post" action="{{route('order.create_bill')}}" name="order" id="fod">
                        <div class="fpanel">
                            <div class="option">
                                <input name="Sex" type="radio" value="Nam"/>Anh
                            </div>
                            <div class="option">
                                <input name="Sex" checked type="radio" value="Nữ"/>Chị
                            </div>
                            <div class="group">
                                <input id="nameOder" type="text" name="Name" value="" placeholder="Họ và tên"/>
                                <input id="phoneOder" name="Phone" type="text" value="" placeholder="Số điện thoại"/>
                                <input id="emaiOder" name="Email" type="text" value="" placeholder="Email của bạn"/>
                                <select name="user_city_id" id="province">
                                    <option name="" value="" selected>Chọn tỉnh, Thành phố</option>
                                    @if(!empty($province))
                                        @foreach($province as $data)
                                            <option id="{{@$data->id}}" value="{{@$data->id}}">{{@$data->name}}</option>
                                        @endforeach
                                    @endif
                                </select>

                                <select name="user_district_id" id="ditric">
                                    <option value="" selected>Chọn quận, Huyện</option>
                                </select>

                                <input class="fixinput" name="Note" type="text"
                                       placeholder="Yêu cầu khác (không bắt buộc)" value=""/>
                            </div>
                        </div>

                        <div class="fpanel">
                            <label><b>Để được phục vụ nhanh hơn,</b> hãy chọn thêm:</label>
                            <div class="option opanel" data-rel="dcgh"><label style="font-size: 14px;" for="radioone">
                                    <input style="position: relative; top: 2px;" name="Panel" id="radioone" required
                                           type="radio"/> Địa chỉ giao hàng</label>
                            </div>
                            <div class="option opanel" data-rel="ntst"><label style="font-size: 14px;" for="radiotwo">
                                    <input style="position: relative; top: 2px;" name="Panel" id="radiotwo" required
                                           type="radio"/> Nhận tại siêu thị</label>
                            </div>
                            <div id="dcgh" class="paytype">
                                <input type="text" name="Address" value=""
                                       placeholder="Số nhà, tên đường, phường/xã/huyện/tỉnh"/>
                                <div class="agent">
                                    <label style="font-size: 14px;" for="add1">
                                        <input id="add1" style="position: relative; top: 1px;" type="radio" required
                                               name="PayType" value="Thanh toán tiền mặt"/> 1. Thanh toán tiền
                                        mặt</label>
                                </div>
                                <div class="agent">
                                    <label style="font-size: 14px;" for="add2">
                                        <input id="add2" style="position: relative; top: 1px;" type="radio" required
                                               name="PayType" value="Chuyển khoản/ quẹt thẻ"/> 2. Chuyển khoản/ quẹt thẻ</label>
                                </div>
                            </div>
                            <div id="ntst" class="paytype">
                                @if(!empty($getRoom))
                                    @foreach($getRoom as $data)
                                        <div class="agent">
                                            <label style="font-size: 14px;" for="room{{$data->id}}">
                                                <input style="position: relative; top: 1px;" id="room{{$data->id}}"
                                                       name="PayType" type="radio" required
                                                       value="{{@$data->name}}"/> {{ucwords(mb_strtolower($data->location))}}
                                                &nbsp;->> {{@$data->name}}</label>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>


                        <div class="fpanel">

                            <div class="da">
                                <input type="number" name="price_giftsss" value="{{$price_gift}}" style="display:none;">
                                <button style="border: none; float: left" class="od">GỬI ĐƠN HÀNG</span>
                                </button>
                            </div>
                            <input type="submit" name="_w_action[OrderPOST]" style="display: none;"/>
                            <input type="hidden" name="_w_action" value="OrderPOST"/>
                        </div>
                    </form>
                </div>
                <p class="los">Bằng cách đặt hàng, bạn đồng ý với <a>Điều khoản sử dụng</a> của {{@$settings['name']}}
                </p>
            @endif

        </div>
        <script>
            $(document).ready(function () {
                $('#province').on('change', function () {
                    let getData = $(this).val();
                    $.ajax({
                        type: 'GET',
                        url: location.href + '?data=' + getData,
                        success: function (data) {
                            $('#ditric').html(data);
                        }
                    })
                })


                //gio hang
                $('.opanel').click(function () {
                    var id = $(this).attr('data-rel');
                    $('.paytype').hide();
                    $('#' + id).show();
                });
            });


        </script>


    </section>

@endsection