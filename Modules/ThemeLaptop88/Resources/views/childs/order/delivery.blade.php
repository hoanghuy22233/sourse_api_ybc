@extends('themelaptop88::layouts.master')
@section('main_content')
    <form method="post" action="{{ route('order.createBill') }}" data-toggle="validator" class="content stateful-form formCheckout" novalidate="true">
        {!! csrf_field() !!}
        <div class="wrap">
            <div class="sidebar ">
                <div class="sidebar_header">
                    <h2>
                        <label class="control-label">Đơn hàng</label>
                        <label class="control-label">({{count(Session::get('cart')['items'])}} sản phẩm)</label>
                    </h2>
                    <hr class="full_width">
                </div>
                <div class="sidebar__content">
                    <div class="order-summary order-summary--product-list order-summary--is-collapsed">
                        <div class="summary-body summary-section summary-product">
                            <div class="summary-product-list">
                                <table class="product-table">
                                    <tbody>
                                    @foreach($products as $item)

                                    <tr class="product product-has-image clearfix">
                                        <td>
                                            <div class="product-thumbnail">
                                                <div class="product-thumbnail__wrapper">

                                                    <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, 50, 50) }}" class="lazy product-thumbnail__image">

                                                </div>
                                                <span class="product-thumbnail__quantity" aria-hidden="true">{{ Session::get('cart')['items'][$item->id]['quantity'] }}</span>
                                            </div>
                                        </td>
                                        <td class="product-info">
                                                    <span class="product-info-name"><a href="{{ \Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($item) }}" target="_blank">{{$item->name}}</a>
                                                    </span>
                                        </td>
                                        <td class="product-price text-right">
                                            {{ number_format(Session::get('cart')['items'][$item->id]['quantity'] * $item->final_price, 0, '.', '.') }}₫
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="order-summary__scroll-indicator">
                                    Cuộn chuột để xem thêm
                                    <i class="fa fa-long-arrow-down" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <hr class="m0">
                    </div>
                    <div class="order-summary order-summary--total-lines">
                        <div class="summary-section border-top-none--mobile">
                            <div class="total-line total-line-subtotal clearfix">
                                <span class="total-line-name pull-left">
                                    Tạm tính
                                </span>
                                <span  class="total-line-subprice pull-right">{{ number_format(Session::get('cart')['total_price'], 0, '.', '.') }}₫</span>
                            </div>
                            <div class="total-line total-line-shipping clearfix" bind-show="requiresShipping">
                                <span class="total-line-name pull-left">
                                    Phí vận chuyển
                                </span>
                                <span> Khu vực này không hỗ trợ vận chuyển</span>
                            </div>
                            <div class="total-line total-line-total clearfix">
                                <span class="total-line-name pull-left">
                                    Tổng cộng
                                </span>
                                <span class="total-line-price pull-right">{{ number_format(Session::get('cart')['total_price'], 0, '.', '.') }}₫</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group clearfix hidden-sm hidden-xs">
                        <div class="field__input-btn-wrapper mt10">
                            <a class="previous-link" href="{{ route('order.view') }}">
                                <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                                <span>Quay về giỏ hàng</span>
                            </a>
                            <input class="btn btn-primary btn-checkout" type="submit" value="ĐẶT HÀNG">
                        </div>
                    </div>
                    <div class="form-group has-error">
                        <div class="help-block ">
                            <ul class="list-unstyled">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main" role="main">
                <div class="main_content">
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="section" define="{billing_address: {}}">
                                <div class="section__header">
                                    <div class="layout-flex layout-flex--wrap">
                                        <h2 class="section__title layout-flex__item layout-flex__item--stretch">
                                            <i class="fa fa-id-card-o fa-lg section__title--icon hidden-md hidden-lg" aria-hidden="true"></i>
                                            <label class="control-label">Thông tin mua hàng</label>
                                        </h2>
                                    </div>
                                </div>
                                <div class="section__content">
                                    <div class="form-group" bind-class="{'has-error' : invalidEmail}">
                                        <div>
                                            <label class="field__input-wrapper">
                                                <input name="bill[user_email]" type="email" placeholder="Email" required style="    font-weight: 100;padding-left: 8px;">
                                            </label>
                                        </div>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="billing">
                                        <div>
                                            <div class="form-group">
                                                <div class="field__input-wrapper" bind-class="{ 'js-is-filled': billing_address.full_name }">
                                                    <input name="bill[user_name]" type="text" class="field__input form-control" id="_billing_address_last_name" placeholder="Họ và tên" required>
                                                </div>
                                                <div class="help-block with-errors"></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="field__input-wrapper" bind-class="{ 'js-is-filled': billing_address.phone }">
                                                    <input required name="bill[user_tel]" type="text" class="field__input form-control" id="_billing_address_phone" placeholder="Số điện thoại">
                                                </div>
                                                <div class="help-block with-errors"></div>
                                            </div>


                                            <div class="form-group">
                                                <div class="field__input-wrapper" bind-class="{ 'js-is-filled': billing_address.address1 }">
                                                    <input required name="bill[user_address]" placeholder="Địa chỉ" class="field__input form-control" id="_billing_address_address1">
                                                </div>
                                                <div class="help-block with-errors"></div>
                                            </div>



                                            <div class="form-group">
                                                <div class="field__input-wrapper field__input-wrapper--select">
                                                    @php
                                                        $data = CommonHelper::getFromCache('get_province_by_name_provinceid',['province']);
                                                            if (!$data) {
                                                                $data = \Modules\ThemeLaptop88\Models\Province::pluck('name', 'provinceid');
                                                                CommonHelper::putToCache('get_province_by_name_provinceid', $data,['province']);
                                                            }
                                                    @endphp
                                                    <select class="field__input field__input--select form-control" name="bill[user_city_id]" id="billingProvince">
                                                        <option value="">--- Chọn tỉnh thành ---</option>
                                                        @foreach($data as $id => $v)
                                                            <option value="{{ $id }}">{{ $v }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="help-block with-errors"></div>
                                            </div>

                                            <div bind-show="!otherAddress" class="form-group">
                                                <div class="field__input-wrapper field__input-wrapper--select">
                                                    <select class="field__input field__input--select form-control" name="bill[user_district_id]" id="billingDistrict" bind-event-change="billingDistrictChange('')" bind="BillingDistrictId">
                                                        <option value="">--- Chọn quận huyện ---</option>

                                                    </select>
                                                </div>
                                                <div class="help-block with-errors"></div>
                                            </div>


                                            <div bind-show="!otherAddress" class="form-group">
                                                <div class="error hide" bind-show="requiresShipping &amp;&amp; loadedShippingMethods &amp;&amp; shippingMethods.length == 0  &amp;&amp; BillingProvinceId  ">
                                                    <label>Khu vực này không hỗ trợ vận chuyển</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <div class="section shipping-method hide" bind-show="shippingMethods.length > 0">
                                <div class="section__header">
                                    <h2 class="section__title">
                                        <i class="fa fa-truck fa-lg section__title--icon hidden-md hidden-lg" aria-hidden="true"></i>
                                        <label class="control-label">Vận chuyển</label>
                                    </h2>
                                </div>
                                <div class="section__content">
                                    <div class="content-box">

                                    </div>
                                </div>
                            </div>
                            <div class="section payment-methods p0--desktop" bind-class="{'p0--desktop': shippingMethods.length == 0}">
                                <div class="section__header">
                                    <h2 class="section__title">
                                        <i class="fa fa-credit-card fa-lg section__title--icon hidden-md hidden-lg" aria-hidden="true"></i>
                                        <label class="control-label">Ghi chú</label>
                                    </h2>
                                </div>
                                <div class="section pt10">
                                    <div class="section__content">
                                        <div class="form-group m0">
                                            <textarea name="bill[note]" value="" class="field__input form-control m0" placeholder="Ghi chú"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="section hidden-md hidden-lg">
                                <div class="form-group clearfix m0">
                                    {{--<input class="btn btn-primary btn-checkout" data-loading-text="Đang xử lý" type="button" --}}
                                            {{--value="ĐẶT HÀNG">--}}
                                    <input class="btn btn-primary btn-checkout" type="submit" value="ĐẶT HÀNG">
                                </div>
                                <div class="text-center mt20">
                                    <a class="previous-link" href="{{ route('order.view') }}">
                                        <i class="fa fa-angle-left fa-lg" aria-hidden="true"></i>
                                        <span>Quay về giỏ hàng</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="main_footer footer unprint">



                    <div class="mt10"></div>
                </div>
                <div class="modal fade" id="refund-policy" data-width="" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                <h4 class="modal-title">Chính sách hoàn trả</h4>
                            </div>
                            <div class="modal-body">
                                <pre></pre>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="privacy-policy" data-width="" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                <h4 class="modal-title">Chính sách bảo mật</h4>
                            </div>
                            <div class="modal-body">
                                <pre></pre>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="terms-of-service" data-width="" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                <h4 class="modal-title">Điều khoản sử dụng</h4>
                            </div>
                            <div class="modal-body">
                                <pre></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('custom_header')
    <link href="{{ URL::asset('public/frontend/themes/stbd/css/checkout.css') }}" rel='stylesheet' type='text/css'/>
    <link href="{{ URL::asset('public/frontend/themes/stbd/css/checkout_dev_2.css') }}" rel='stylesheet' type='text/css'/>
    <style>
        .form-group input {
            margin: 0;
        }
        .field__input-btn-wrapper .btn-checkout {
            padding: 0px 20px;
        }
        .summary-product-list table td {
            padding: 10px !important;
        }
        td.product-price.text-right {
            min-height: 81px;
            vertical-align: middle;
            text-align: center;
        }
        span.total-line-name {
            font-weight: bold;
        }
        span.total-line-price {
            font-weight: bold;
        }
    </style>
@endsection

@section('custom_footer')
    <script>
        $(document).ready(function () {
            $('select#billingProvince').change(function () {
                $('select#billingDistrict').attr('disabled', 'disabled');
                var city_id = $(this).val();
                $.ajax({
                    url: '{{ URL::to('ajax-get-district') }}',
                    type: 'GET',
                    data: {
                        city_id : city_id
                    },
                    success: function (result) {
                        $('select#billingDistrict').removeAttr('disabled');
                        $('select#billingDistrict').html(result);
                    },
                    error: function () {
                        alert('Có lỗi xảy ra. Vui lòng F5 lại website!');
                    }
                });
            });
        });
    </script>
@endsection