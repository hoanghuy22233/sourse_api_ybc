<?php
if (strpos($attr, '|') !== false) {
    $attr = explode('|', $attr);
} else {
    $attr = explode(',', $attr);
}

$data = \Modules\ThemeLaptop88\Models\PropertieValue::whereIn('id', $attr)->get();
$attr_price_options = \Modules\ThemeLaptop88\Models\PropertieValue::join('properties_name', 'properties_value.properties_name_id', '=', 'properties_name.id')
    ->selectRaw('properties_name.name, properties_value.value')
    ->where('properties_name.price_option', 1)->whereIn('properties_value.id', $attr)
    ->where('properties_name.price_option', 1)->get();
?>
@foreach($data as $v)
    - {{ $v->property_name->name }}: {{ $v->value }}<br>
@endforeach
@foreach($attr_price_options as $v)
    - {{ $v->name }}: {{ $v->value }}<br>
@endforeach
