@if(@$settings['related_product'] == 1 && isset($relate_products) && count($relate_products) > 0)
    <?php
    $cate_multi = explode('|', trim($product->related_products, '|'));
    $relate_products = CommonHelper::getFromCache('product_id_cate_multi' . implode("|", $cate_multi), ['categories']);
    if (!$relate_products) {
        $relate_products = \Modules\ThemeLaptop88\Models\Product::whereIn('id', $cate_multi)->where('status', 1)->get();
        CommonHelper::putToCache('product_id_cate_multi' . implode("|", $cate_multi), $relate_products, ['categories']);
    }
    ?>

    <div id="relate-product">
        <div class="title-tab border-bottom">
            <h3 class="dtit">Sản phẩm liên quan</h3>
        </div>
        <div id="r-product-list">

            @foreach( $relate_products as $relate_product)
                @php
                    $cate_slug =CommonHelper::getFromCache('get_slug_cate_by_sptt'.@$relate_product->id,['categories']);
                    if (!$cate_slug){
                        $cate_slug = \Modules\ThemeLaptop88\Models\Category::whereIn('id', explode('|', @$relate_product->multi_cat))->first();
                       CommonHelper::putToCache('get_slug_cate_by_sptt'.@$relate_product->id, @$cate_slug,['categories']);
                    }
                @endphp
                <a class="psitem border-bottom"
                   href="{{\Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($relate_product) }}"
                   title="{{@$relate_product->name}}">

                    <div class="product-image">
                        <img alt="{{@$relate_product->name}}"
                             class="lazy"
                             data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$relate_product->image, 160, null) }}"
                             data-src="{{@$relate_product->image}}"/>
                    </div>
                    <div class="product-detail">
                        <p class="pn">{{@$relate_product->name}}</p>
                        <span class="pr">{{number_format(@$relate_product->final_price, 0, '.', '.')}}Đ</span>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
@endif