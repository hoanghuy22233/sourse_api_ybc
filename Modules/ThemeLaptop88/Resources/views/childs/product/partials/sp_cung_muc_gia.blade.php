@if(@$settings['sp_cung_muc_gia'] == 1)
    <div class="dbox">
        @if(!isset($productCptbs) || count($productCptbs) > 0)
            <span class="dtit">Sản phẩm cùng mức giá</span>
            <div class="ss" id="SPTT">
                <span class="psback"></span>
                <div class="pspanel">
                    <div class="pswrap">
                        @foreach( $productCptbs as $productCptb)
                            <?php
                            $cate_slug = CommonHelper::getFromCache('get_slug_cate_by_sptt_productCptb' . @$productCptb->multi_cat, ['categories']);
                            if (!$cate_slug) {
                                $cate_slug = \Modules\ThemeLaptop88\Models\Category::whereIn('id', explode('|', @$productCptb->multi_cat))->first();
                                CommonHelper::putToCache('get_slug_cate_by_sptt_productCptb' . @$productCptb->multi_cat, @$cate_slug, ['categories']);
                            }
                            ?>
                            <a class="psitem" target="_blank"
                               href="{{\Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($productCptb) }}"
                               title="{{@$productCptb->name}}">

                                <div class="pi">
                                    <img alt="{{@$productCptb->name}}"
                                         class="lazy"
                                         data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$productCptb->image, 160, null) }}"
                                    />
                                </div>
                                <p class="pn">{{@$productCptb->name}}</p>
                                <span class="pr">{{number_format(@$productCptb->final_price, 0, '.', '.')}}<sup
                                            style="margin-left: 5px;">đ</sup></span>
                            </a>
                        @endforeach

                    </div>
                </div>
                <span class="psnext"></span>
            </div>
        @endif
    </div>
@endif