@if(@$settings['sp_da_xem'] == 1)
    <div class="dbox">
        <?php
        $cate_multi = explode('|', trim($product->multi_cat, '|'));
        $product_vieweds = CommonHelper::getFromCache('products_id_session' . implode('|', $_SESSION['product_viewed']));
        if (!$product_vieweds) {
            $product_vieweds = \Modules\ThemeLaptop88\Models\Product::whereIn('id', $_SESSION['product_viewed'])->where('status', 1)->get();
            CommonHelper::putToCache('products_id_session' . implode('|', $_SESSION['product_viewed']), $product_vieweds);
        }
        ?>
        @if($product_vieweds->count() > 0)
            <span class="dtit">Sản phẩm đã xem</span>
            <div class="ss" id="SPTT3">
                <span class="psback"></span>
                <div class="pspanel">
                    <div class="pswrap">
                        @foreach( $product_vieweds as $product_viewed)
                            <?php
                            $cate_slug = CommonHelper::getFromCache('get_slug_cate_by_sptt' . @$product_viewed->id, ['categories']);
                            if (!$cate_slug) {
                                $cate_slug = \Modules\ThemeLaptop88\Models\Category::whereIn('id', explode('|', @$product_viewed->multi_cat))->first();
                                CommonHelper::putToCache('get_slug_cate_by_sptt' . @$product_viewed->id, @$cate_slug, ['categories']);
                            }
                            ?>
                            <a class="psitem"
                               href="{{\Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($product_viewed) }}"
                               title="{{@$product_viewed->name}}">

                                <div class="pi">
                                    <img alt="{{@$product_viewed->name}}"
                                         class="lazy"
                                         data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$product_viewed->image, 160, null) }}"
                                         src="{{@$product_viewed->image}}"/>
                                </div>
                                <p class="pn">{{@$product_viewed->name}}</p>
                                <span class="pr">{{number_format(@$product_viewed->final_price, 0, '.', '.')}}<sup
                                            style="margin-left: 5px;">đ</sup></span>
                            </a>
                        @endforeach

                    </div>
                </div>
                <span class="psnext"></span>
            </div>
        @endif
    </div>
@endif