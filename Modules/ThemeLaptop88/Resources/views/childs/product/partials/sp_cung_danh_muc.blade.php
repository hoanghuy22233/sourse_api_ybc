@if(@$settings['sp_cung_danh_muc'] == 1)
<style>
    .sp_cung_danh_muc a.psitem p.pn {
        height: 69px;
        overflow: hidden;
        align-items: inherit;
        justify-content: inherit;
    }
    .sp_cung_danh_muc .ss {
        height: 319px;
    }
</style>
<div class="dbox sp_cung_danh_muc">
    <?php
    $cate_multi = explode('|', trim($product->multi_cat, '|'));

    $productInMultiCats = CommonHelper::getFromCache('products_multi_cat_like_cate_multi' . $cate_multi[0], ['products']);
    if (!$productInMultiCats) {
        $productInMultiCats = \Modules\ThemeLaptop88\Models\Product::where('multi_cat', 'like', '%|' . $cate_multi[0] . '|%')->where('status', 1)->get();
        CommonHelper::putToCache('products_multi_cat_like_cate_multi' . $cate_multi[0], $productInMultiCats, ['products']);
    }
    ?>

    @if($productInMultiCats->count() > 0)
        <span class="dtit">Sản phẩm cùng danh mục</span>
        <div class="ss" id="SPTT1">
            <span class="psback"></span>
            <div class="pspanel">
                <div class="pswrap">
                    @foreach( $productInMultiCats as $productInMultiCat)
                        <?php
                        $cate_slug = CommonHelper::getFromCache('get_slug_cate_by_sptt' . @$productInMultiCat->id, ['categories']);
                        if (!$cate_slug) {
                            $cate_slug = \Modules\ThemeLaptop88\Models\Category::whereIn('id', explode('|', @$productInMultiCat->multi_cat))->first();
                            CommonHelper::putToCache('get_slug_cate_by_sptt' . @$productInMultiCat->id, @$cate_slug, ['categories']);
                        }
                        ?>
                        <a class="psitem"
                           href="{{\Modules\ThemeLaptop88\Http\Helpers\CommonHelper::getProductSlug($productInMultiCat) }}"
                           title="{{@$productInMultiCat->name}}">

                            <div class="pi">
                                <img alt="{{@$productInMultiCat->name}}"
                                     class="lazy"
                                     data-src="{{\App\Http\Helpers\CommonHelper::getUrlImageThumb(@$productInMultiCat->image, 160, null) }}"
                                />
                            </div>
                            <p class="pn">{{@$productInMultiCat->name}}</p>
                            <span class="pr">{{number_format(@$productInMultiCat->final_price, 0, '.', '.')}}<sup
                                        style="margin-left: 5px;">đ</sup></span>
                        </a>
                    @endforeach

                </div>
            </div>
            <span class="psnext"></span>
        </div>
    @endif
</div>
@endif