<style>
    @media (max-width: 991px) {
        form.mua-tra-gop-form {
            left: 2% !important;
            right: 2%;
        }
    }

    form.mua-tra-gop-form {
        position: fixed;
        background: #fff;
        z-index: 9999999;
        border: 1px solid red;
        padding: 20px;
        top: 10%;
        left: 40%;
        border-radius: 10px;
        box-shadow: 0px 0px 10px 5px red;

        display: inline-block;
    }

    .form-input {
        display: flex;
        margin-bottom: 10px;
    }

    .form-btn {
        text-align: center;
    }

    .form-input input, .form-input select {
        width: 65%;
        padding-left: 5px;
    }

    .form-input label {
        width: 35%;
    }

    .form-btn > p {
        margin-top: 10px;
    }

    .form-btn > span {
        padding: 10px 40px;
        text-align: center;
        font-weight: 600;
        background: red;
        border: none;
        color: #fff;
        border-radius: 5px;
        cursor: pointer;
    }

    .mua-tra-gop-form > h4 {
        color: red;
        text-transform: uppercase;
        text-align: center;
        margin-bottom: 20px;
        font-weight: 600;
    }
</style>
<?php
$province = \Modules\ThemeLaptop88\Models\Province::all();
?>
<form class="mua-tra-gop-form" method="post" style="display: none">
    <h4>đăng ký nhận dịch vụ</h4>
    <div class="form-input">
        <label>Họ tên: </label>
        <input type="text" id="name" name="name" required placeholder="Nhập họ tên">
    </div>
    <div class="form-input">
        <label>Số điện thoại: </label>
        <input type="text" id="tel" name="tel" required placeholder="Nhập số điện thoại">
    </div>
    <div class="form-input">
        <label>Tên sản phẩm: </label>
        <input type="text" id="product_id" name="product_id" value="{{$product->id}}" required
               placeholder="Nhập mã sản phẩm">
    </div>
    <div class="form-input">
        <label>Chọn dịch vụ: </label>
        <select name="service" id="service" class="option-mua-tra-gop">
            <option value="1">Dịch vụ trả góp</option>
        </select>
    </div>
    <div class="form-input">
        <label>Tỉnh/Thành: </label>
        <select name="province_uu_dai" id="province_uu_dai">
            <option value="" selected>Chọn tỉnh/thành</option>
            @foreach($province as $pp)
                <option value="{{$pp->id}}">{{$pp->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-input">
        <label>Địa chỉ: </label>
        <input type="text" id="address" name="address" placeholder="Nhập địa chỉ">
    </div>
    <div class="form-btn">
        <span id="register">ĐĂN KÝ NGAY</span>
        <p>Cam kết mọi thông tin của bạn sẽ được bảo mật</p>
    </div>
</form>
<script>
    $(document).on('click', '#register', function () {
        $.ajax({
            url: "/mua-tra-gop", // gửi ajax đến file result.php
            type: "post", // chọn phương thức gửi là post
            dataType: "text", // dữ liệu trả về dạng text
            data: { // Danh sách các thuộc tính sẽ gửi đi
                name: $('#name').val(),
                tel: $('#tel').val(),
                product_id: $('#product_id').val(),
                service: $('#service').val(),
                address: $('#address').val()
            }, success: function () {
                alert('Bạn đã đăng ký thành công');
                $('.mua-tra-gop-form').hide();
            }
        });
    });
</script>