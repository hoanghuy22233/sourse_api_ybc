<!DOCTYPE html>
<html lang="vi-vn" xml:lang="vi-vn">
<?php
function isMobile()
{
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
$banner_header_top = CommonHelper::getFromCache('setting_name_banner_head_top_type_common_tab', ['settings']);
if ($banner_header_top === false) {
    $banner_header_top = \App\Models\Setting::where('name', 'photos[banner_head_top]')->where('type', 'common_tab')->first();
    CommonHelper::putToCache('setting_name_banner_head_top_type_common_tab', $banner_header_top, ['settings']);
}

$banner_top = CommonHelper::getFromCache('banner_location_banner_header_top', ['banners']);
if ($banner_top === false) {
    $banner_top = \Modules\ThemeLaptop88\Models\Banner::where('status', 1)->where('location', 'banner_header_top')->first();
    CommonHelper::putToCache('banner_location_banner_header_top', $banner_top, ['banners']);
}

$custom_slide_product_hot = CommonHelper::getFromCache('setting_name_custom_slide_product_hot_type_homepage_tab_value', ['settings']);
if ($custom_slide_product_hot === false) {
    $custom_slide_product_hot = @\Modules\ThemeLaptop88\Models\Settings::where('name', 'custom_slide_product_hot')->where('type', 'homepage_tab')->first()->value;
    CommonHelper::putToCache('setting_name_custom_slide_product_hot_type_homepage_tab_value', $custom_slide_product_hot, ['settings']);
}



?>
<head>

    @include('themelaptop88::partials.head_meta')
    @include('themelaptop88::partials.header_script')
    <link rel="stylesheet" href="{{asset('public/frontend/themes/stbd/css/custom2.css')}}">
    <link rel="stylesheet" href="{{asset('public/frontend/themes/laptop88/css/custom.css')}}">
    {!! @$settings['head_code'] !!}

    <style>
        .nav-menu .nav-item {
            padding: 3px 0 !important;
        }

        {{--.f.regis {--}}
        {{--    @if(@$settings['show_email_homepage']==0)--}}
        {{--          display: none !important;--}}
        {{--@endif--}}




        }

        header {
            @if(@$settings['header_background'] != '')     background: {{@$settings['header_background']}}  !important;
        @endif




        }

        footer {
            @if(@$settings['footer_background'] != '')     background: {{@$settings['footer_background']}}  !important;
        @endif




        }

        @if(@$settings['option_background']==0)
            body > div.f {
            background: none !important;
        }

        header,
        .new-header-container {
            @if(@$settings['header_background'] != '')     background: {{@$settings['header_background']}}  !important;
        @endif

        }

        @elseif(@$settings['option_background']==1)
            @if(@$settings['background_color']!='')
                body > div.f {
            background-color: {{$settings['background_color']}}    !important;
        }

        @endif
        @elseif(@$settings['option_background']==2)
            @if(!empty($settings['background_image']))
                body > div.f {
            background-image: url('https://demo4.webhobasoft.com/public/filemanager/userfiles/{{$settings['background_image']}}') !important;
            background-attachment: fixed;
        }

        @endif
        @endif

        @if(@$settings['logo_background'] != '')
            body header .hleft {
            background: {{ $settings['logo_background'] }};
        }

        @endif
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            xfbml: true,
            version: 'v6.0'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

@include('themelaptop88::partials.icon_contact')

@if(!empty($banner_top))
    <a href="{{$banner_top->link}}">
        <img data-src=" /public/filemanager/userfiles/{{ $banner_top->image  }}" style="margin: auto;display: block;"
             alt="{{ $banner_top->name  }}" class="lazy">
    </a>
@endif

@include('themelaptop88::partials.menu_home')
{{--@include('themelaptop88::partials.menu_cate_home')--}}
@if(@$settings['option_menu'] == 0)
    @include('themelaptop88::partials.menu_efect.menu_slidedown')
@elseif(@$settings['option_menu'] == 2)
    @include('themelaptop88::partials.menu_efect.menu_default')
@endif

@include('themelaptop88::partials.slide')

@include('themelaptop88::partials.branlogo')
@yield('main_content')
@if (isMobile())
    @include('themelaptop88::partials.category_slide_mobie')
@else
    @include('themelaptop88::partials.category_slide')
@endif
{{--{{dd($settings['custom_slide_product_hot'])}}--}}

@if($custom_slide_product_hot == 1)
    @if (isMobile())
        @include('themelaptop88::partials.slide_mobie_pro_hot')
    @else
        @include('themelaptop88::partials.slide_product_hot')
    @endif

@endif
@if (!isMobile())
    @include('themelaptop88::partials.footer_banner')
@endif



@include('themelaptop88::partials.form_resign')
@include('themelaptop88::partials.set_font')
@if(@$settings['footer_content'] == 1)
    @include('themelaptop88::partials.footer_colum')
@elseif(@$settings['footer_content'] == 0)
    @include('themelaptop88::partials.footer')
@endif
@include('themelaptop88::partials.footer_script')
@include('themelaptop88::partials.contact_us')

{!! @$settings['footer_code'] !!}
<style>
    .fb_customer_chat_bubble_animated_no_badge {
        right: 0 !important;
        bottom: 125px !important;
    }

    .fb_customer_chat_bubble_animated_no_badge_280 {
        bottom: 125px !important;
    }

    .fb_customer_chat_bubble_animated_no_badge_281 {
        bottom: 300px !important;
    }
</style>
<script>
    $(window).scroll(function (event) {
        let scrollHeight = $(window).scrollTop();
        if (scrollHeight > 280) {
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').addClass('fb_customer_chat_bubble_animated_no_badge_281');
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').removeClass('fb_customer_chat_bubble_animated_no_badge_280');
        } else {
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').addClass('fb_customer_chat_bubble_animated_no_badge_280');
            $('#fb-root .fb_customer_chat_bubble_animated_no_badge').removeClass('fb_customer_chat_bubble_animated_no_badge_281');
        }
    });
</script>
</body>
</html>
