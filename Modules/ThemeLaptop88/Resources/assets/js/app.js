
$(document).ready(function () {
    $("blockquote").after('<i class="fa fa-quote-left" aria-hidden="true"></i>');
    $("blockquote").before('<i class="fa fa-quote-right" aria-hidden="true"></i>');


    $(".phone_now").click(function () {
        $('.contact_now .phone_now_detail').toggle();
    });
    $(".zalo_now").hover(function () {
        $('.zalo_now>span').toggle();
    });
    $(window).scroll(function(event){
        let sticky = $('body'),
            scroll = $(window).scrollTop();

        if (scroll > 375) sticky.addClass('header-fixed');
        else sticky.removeClass('header-fixed');

        let scrollHeight = $(window).scrollTop();
        if (scrollHeight > 280) {
            $('.contact_now_parent').css({
                'bottom': '40px'
            });
        } else {
            $('.contact_now_parent').css({
                'bottom': '10px'
            });
        }
    });

});
