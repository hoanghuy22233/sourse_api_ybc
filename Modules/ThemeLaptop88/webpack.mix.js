const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/assets/js/app.js', 'frontend/themes/laptop88/js/themelaptop88.js')
    .sass( __dirname + '/Resources/assets/sass/app.scss', 'frontend/themes/laptop88/css/themelaptop88.css')
    .sass( __dirname + '/Resources/assets/sass/category_home.scss', 'frontend/themes/laptop88/css/themelaptop88_category_home.css')
    .sass( __dirname + '/Resources/assets/sass/category_home_m.scss', 'frontend/themes/laptop88/css/themelaptop88_category_home_m.css')

if (mix.inProduction()) {
    mix.version();
}
