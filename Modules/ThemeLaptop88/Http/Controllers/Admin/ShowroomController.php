<?php

namespace Modules\ThemeLaptop88\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Auth;
use Illuminate\Http\Request;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeSTBDAdmin\Models\Category;
use Validator;

class ShowroomController extends CURDBaseController
{

    protected $module = [
        'code' => 'showroom',
        'table_name' => 'showrooms',
        'label' => 'Showroom',
        'modal' => '\Modules\ThemeLaptop88\Models\Showroom',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên'],
            ['name' => 'location', 'type' => 'text', 'label' => 'Vị trí'],
            ['name' => 'hotline', 'type' => 'text', 'label' => 'Hotline'],
            ['name' => 'staff', 'type' => 'text', 'label' => 'Người phụ trách'],
//            ['name' => 'status', 'type' => 'status', 'label' => 'Trạng thái', 'inner' => 'placeholder="0/1"'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'validate_field', 'label' => 'Tên', 'group_class' => 'col-md-6'],
                ['name' => 'slug', 'type' => 'text', 'class' => 'validate_field', 'label' => 'Slug', 'group_class' => 'col-md-6'],
                ['name' => 'category_id_field', 'type' => 'select2_ajax_model', 'class' => '', 'label' => 'Danh mục', 'model' => Category::class,
                    'object' => 'category_post', 'where' => 'type in (5)', 'display_field' => 'name', 'display_field2' => 'id', 'multiple' => true],
                ['name' => 'intro', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Giới thiệu'],
                ['name' => 'location', 'type' => 'text', 'class' => '', 'label' => 'Vị trí', 'group_class' => 'col-md-6'],
                ['name' => 'hotline', 'type' => 'text', 'class' => '', 'label' => 'Hotline', 'group_class' => 'col-md-6'],
                ['name' => 'address', 'type' => 'text', 'class' => '', 'label' => 'Địa chỉ'],
                ['name' => 'url_map', 'type' => 'text', 'class' => '', 'label' => 'Iframe map'],
                ['name' => 'staff', 'type' => 'text', 'class' => '', 'label' => 'Nhân viên', 'group_class' => 'col-md-6'],
                ['name' => 'contact', 'type' => 'text', 'class' => '', 'label' => 'Liên hệ', 'group_class' => 'col-md-6'],
            ],
            'image_tab' => [
                ['name' => 'showroom_image', 'type' => 'file_editor', 'class' => '', 'label' => 'Ảnh', 'group_class' => 'col-md-6'],
                ['name' => 'image_map', 'type' => 'file_editor', 'class' => '', 'label' => 'Ảnh map', 'group_class' => 'col-md-6'],
                ['name' => 'image_slide', 'type' => 'multiple_image_dropzone', 'class' => '', 'label' => 'Ảnh Slide', 'count' => 6],
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('themelaptop88::admin.showroom.list')->with($data);
    }

    public function add(Request $request)
    {
        try {


            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('themelaptop88::admin.showroom.add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'slug' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                    'slug.required' => 'Bắt buộc phải nhập slug',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('image_slide')) {
                        $data['image_slide'] = implode('|', $request->image_slide);
                    }
                    if ($request->has('category_id_field')) {
                        $data['category_id_field'] = implode('|', $request->category_id_field);
                    }
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->adminLog($request,$this->model,'add');
                        $this->afterAddLog($request, $this->model);
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {

            $item = $this->model->find($request->id);

            //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('themelaptop88::admin.showroom.edit')->with($data);
            } else if ($_POST) {



                $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'slug' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                    'slug.required' => 'Bắt buộc phải nhập slug',
                ]);

                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
//                    dd($data);
                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('image_slide')) {
                        $data['image_slide'] = implode('|', $request->image_slide);
                    }

                    if ($request->has('category_id_field')) {
                        $data['category_id_field'] = implode('|', $request->category_id_field);
                    }
                    foreach ($data as $k => $v) {
                        $item->$k = $v;
                    }
                    if ($item->save()) {
                        $this->adminLog($request,$item,'edit');
                        CommonHelper::flushCache($this->module['table_name']);
                        CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                    }
                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $item
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
//            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getContactInfo($request)
    {
        $contact_info = [];
        if ($request->has('contact_info_name')) {
            foreach ($request->contact_info_name as $k => $key) {
                if ($key != null) {
                    $contact_info[] = [
                        'name' => $key,
                        'tel' => $request->contact_info_tel[$k],
                        'email' => $request->contact_info_email[$k],
                        'note' => $request->contact_info_note[$k],
                    ];
                }
            }
        }
        return $contact_info;
    }

    public function getPublish(Request $request)
    {
        try {

                

            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();
            $this->adminLog($request,$item,'publish');
            CommonHelper::flushCache($this->module['table_name']);
            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {

                

            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();
            $this->adminLog($request,$item,'delete');
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {

                

            $ids = $request->ids;
            $this->adminLog($request,$ids,'multi_delete');
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

}
