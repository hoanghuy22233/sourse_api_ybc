<?php

/**
 * Page Model
 *
 * Page Model manages page operation.
 *
 * @category   Page
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\ThemeLaptop88\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';

    protected $fillable = [
        'name', 'email', 'tel', 'address', 'content', 'user_ip', 'product_id', 'province', 'url', 'distric'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
    public function provinces()
    {
        return $this->belongsTo(Province::class, 'province');
    }
    public function district()
    {
        return $this->belongsTo(District::class, 'distric');
    }
}
