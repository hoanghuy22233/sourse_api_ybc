<?php

namespace Modules\ThemeLaptop88\Models;

use Illuminate\Database\Eloquent\Model;

class AdminLog extends Model
{

    protected $table = 'admin_logs';

    protected $fillable = [
        'admin_id' , 'message' , 'type', 'item_id', 'model'
    ];

    protected $appends = ['item'];

    public function getItemAttribute()
    {
        $itemModel = new $this->model;
        return $itemModel->find($this->item_id);
    }

    public function admin() {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
}
