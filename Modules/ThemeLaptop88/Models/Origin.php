<?php

namespace Modules\ThemeLaptop88\Models;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
   protected $table = 'origins';
}
