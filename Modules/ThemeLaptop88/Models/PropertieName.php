<?php

namespace Modules\ThemeLaptop88\Models;

use Illuminate\Database\Eloquent\Model;

class PropertieName extends Model
{
    protected $table = 'properties_name';
    protected $fillable = ['name', 'price_option'];

    public $timestamps = false;
}
