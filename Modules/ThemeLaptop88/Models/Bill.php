<?php

/**
 * Page Model
 *
 * Page Model manages page operation. 
 *
 * @category   Page
 * @package    vRent
 * @author     Techvillage Dev Team
 * @copyright  2017 Techvillage
 * @license    
 * @version    1.3
 * @link       http://techvill.net
 * @since      Version 1.3
 * @deprecated None
 */

namespace Modules\ThemeLaptop88\Models;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $table = 'bills';

    protected $fillable = [
        'receipt_method' , 'user_gender', 'date' , 'coupon_code' , 'note' , 'status' , 'total_price' , 'user_id', 'user_tel', 'user_name', 'user_email', 'user_address', 'user_wards', 'user_city_id',
        'user_district_id', 'user_address_type','gift_list'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function orders() {
        return $this->hasMany(Order::class, 'bill_id', 'id');
    }
    public function province() {
        return $this->hasMany(Province::class, 'user_city_id');
    }
    public function distric() {
        return $this->hasMany(District::class, 'user_district_id');
    }
}
