<?php

namespace Modules\PaymentOnline\Http\Controllers;

use App\Http\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\PaymentOnline\Models\Invoice;
use Modules\ThemeHandymanServices\Models\Booking;
use Session;
use Srmklive\PayPal\Services\AdaptivePayments;
use Srmklive\PayPal\Services\ExpressCheckout;

class PaymentOnlineController extends Controller
{
//    public function getIndex(Request $request)
//    {
//        $response = [];
//        if (session()->has('code')) {
//            $response['code'] = session()->get('code');
//            session()->forget('code');
//        }
//
//        if (session()->has('message')) {
//            $response['message'] = session()->get('message');
//            session()->forget('message');
//        }
//
//        return view('welcome', compact('response'));
//    }


    public function getExpressCheckout(Request $request)
    {
        if (Session::has('booking_id')) {

            $data = $this->getCheckoutData();
            $provider = new ExpressCheckout();
            $response = $provider->setExpressCheckout($data);
            if (is_null($response['paypal_link'])) {
                echo "có lỗi xảy ra";
                Echo @$response['L_SHORTMESSAGE0'];
                Echo @$response['L_LONGMESSAGE0'];
            } else {
                return redirect($response['paypal_link']);
            }

        }
    }

    public function getExpressCheckoutSuccess(Request $request)
    {
        $recurring = ($request->get('mode') === 'recurring') ? true : false;
        $token = $request->get('token');
        $PayerID = $request->get('PayerID');

        $cart = $this->getCheckoutData($recurring);

        // Verify Express Checkout Token
        $provider = new ExpressCheckout();
        $response = $provider->getExpressCheckoutDetails($token);

        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            if ($recurring === true) {
                $response = $provider->createMonthlySubscription($response['TOKEN'], 9.99, $cart['subscription_desc']);
                if (!empty($response['PROFILESTATUS']) && in_array($response['PROFILESTATUS'], ['ActiveProfile', 'PendingProfile'])) {
                    $status = 'Processed';
                } else {
                    $status = 'Invalid';
                }
            } else {
                // Perform transaction on PayPal
                $payment_status = $provider->doExpressCheckoutPayment($cart, $token, $PayerID);
                $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];
            }

            $invoice = $this->createInvoice($cart, $status);
            if ($invoice->paid) {
                Session::forget('booking_id');
                $message = "Order $invoice->id has been paid successfully!";
                CommonHelper::one_time_message('success', $message);
            } else {
                $message = "Error processing PayPal payment for Order $invoice->id!";
                CommonHelper::one_time_message('danger', $message);
            }
            return redirect()->route('thank-you');
        }
    }

    public function getAdaptivePay()
    {
        $this->provider = new AdaptivePayments();

        $data = [
            'receivers' => [
                [
                    'email' => 'johndoe@example.com',
                    'amount' => 10,
                    'primary' => true,
                ],
                [
                    'email' => 'janedoe@example.com',
                    'amount' => 5,
                    'primary' => false,
                ],
            ],
            'payer' => 'EACHRECEIVER', // (Optional) Describes who pays PayPal fees. Allowed values are: 'SENDER', 'PRIMARYRECEIVER', 'EACHRECEIVER' (Default), 'SECONDARYONLY'
            'return_url' => url('payment/success'),
            'cancel_url' => url('payment/cancel'),
        ];

        $response = $this->provider->createPayRequest($data);
        dd($response);
    }


    public function notify(Request $request)
    {
        if (!($this->provider instanceof ExpressCheckout)) {
            $this->provider = new ExpressCheckout();
        }

        $post = [
            'cmd' => '_notify-validate',
        ];
        $data = $request->all();
        foreach ($data as $key => $value) {
            $post[$key] = $value;
        }

        $response = (string)$this->provider->verifyIPN($post);

        $ipn = new IPNStatus();
        $ipn->payload = json_encode($post);
        $ipn->status = $response;
        $ipn->save();
    }

    /**
     * Set cart data for processing payment on PayPal.
     *
     * @param bool $recurring
     *
     * @return array
     */
    protected function getCheckoutData($recurring = false)
    {
        $data = [];
        $booking_id = Session::get('booking_id');
        $booking = Booking::where('id', $booking_id)->first();
        if (is_object($booking)) {
            $service_fields = (array)json_decode($booking->service_fields);
            foreach ($service_fields as $service_field_id => $value) {
                $service = \Modules\HandymanServicesBooking\Models\ServiceFields::find($service_field_id);
                $field_name = $service->{'name_vi'};
                $service_controller = new \Modules\ThemeHandymanServices\Http\Controllers\Frontend\ServiceController();
                $price = $service_controller->getPriceService($service_field_id, $value);
                $data['items'][] = [
                    'name' => $field_name,
                    'price' => $price / $value,
                    'qty' => $value,
                ];
            }
            $total = 0;
            foreach ($data['items'] as $item) {
                $total += $item['price'] * $item['qty'];
            }

            $countInvoice = Invoice::count() + 1;

            $order_id = $countInvoice;

            $data['return_url'] = url('/paypal/checkout-success');

            $data['invoice_id'] = $order_id;
            $data['invoice_description'] = "Order #$order_id Invoice";
            $data['cancel_url'] = url('/cancle-paypal');

            $data['total'] = $total;

//            $data['shipping_discount'] = round((10 / 100) * $total, 2);


        }
        return $data;

    }

    /**
     * Create invoice.
     *
     * @param array $cart
     * @param string $status
     *
     * @return \App\Invoice
     */
    protected function createInvoice($cart, $status)
    {
        $booking_id = Session::get('booking_id');
        $invoice = new Invoice();
        $invoice->title = $cart['invoice_description'];
        $invoice->price = $cart['total'];
        $invoice->booking_id = $booking_id;
        if (!strcasecmp($status, 'Completed') || !strcasecmp($status, 'Processed')) {
            $invoice->paid = 1;
        } else {
            $invoice->paid = 0;
        }
        $invoice->save();
//
//        collect($cart['items'])->each(function ($product) use ($invoice) {
//            $item = new Item();
//            $item->invoice_id = $invoice->id;
//            $item->item_name = $product['name'];
//            $item->item_price = $product['price'];
//            $item->item_qty = $product['qty'];
//
//            $item->save();
//        });

        return $invoice;
    }

    public function thankYou(Request $request)
    {
        return view('paymentonline::paypal.thankyou');
    }
}
