<?php

namespace Modules\PaymentOnline\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Auth;
use Illuminate\Http\Request;
use Mail;
use Modules\EworkingCompany\Models\Company;
use Modules\PaymentOnline\Models\{Service, ServiceHistory};
use Validator;

class InvoiceController extends CURDBaseController
{

    protected $module = [
        'code' => 'invoice',
        'table_name' => 'invoice',
        'label' => 'paymentonline::admin.history',
        'modal' => '\Modules\PaymentOnline\Models\Invoice',
        'list' => [
            ['name' => 'title', 'type' => 'text', 'label' => 'paymentonline::admin.name_bill'],
            ['name' => 'booking_id', 'type' => 'custom', 'td'=>'paymentonline::layouts.td.booking', 'label' => 'paymentonline::admin.order',],
            ['name' => 'price', 'type' => 'number', 'label' => 'paymentonline::admin.money'],
//            ['name' => 'paid', 'type' => 'text', 'label' => 'Trạng thái'],
            ['name' => 'created_at', 'type' => 'text', 'label' => 'paymentonline::admin.time'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'service_id', 'type' => 'custom', 'field' => 'webservice::form.fields.select_service', 'class' => 'required', 'label' => 'paymentonline::admin.package', 'model' => \Modules\WebService\Models\Service::class, 'display_field' => 'name_vi', 'display_field2' => 'account_max'],
                ['name' => 'use_date_max', 'type' => 'custom', 'field' => 'webservice::form.fields.select_service_day', 'class' => 'required', 'label' => 'paymentonline::admin.used_time', 'model' => \Modules\WebService\Models\Service::class,
                    'display_field' => 'value'],
            ],
        ],
    ];

    protected $filter = [

        'service_id' => [
            'label' => 'paymentonline::admin.package_service',
            'type' => 'select2_model',
            'display_field' => 'name_vi',
            'model' => \Modules\WebService\Models\Service::class,
            'query_type' => '='
        ],
        'payment' => [
            'label' => 'paymentonline::admin.price',
            'type' => 'number',
            'query_type' => '='
        ],
        'start_date' => [
            'label' => 'paymentonline::admin.date_buy',
            'type' => 'date',
            'query_type' => '='
        ],
        'exp_date' => [
            'label' => 'paymentonline::admin.date_end',
            'type' => 'date',
            'query_type' => '='
        ],
    ];


    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('paymentonline::list')->with($data);
    }


    public function delete(Request $request)
    {
        try {


            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
                return back();
            }

            $item->delete();
            CommonHelper::one_time_message('success', 'Xóa thành công!');
//            dd(1);
            return back();
//            return $this->deleteReturn($request);
        } catch (\Exception $ex) {
//            dd(2);
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {


            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => 'Xóa thành công!'
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


}
