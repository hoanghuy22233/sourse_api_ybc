<?php

namespace Modules\PaymentOnline\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class PaymentOnlineServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->rendAsideMenu();
        $this->loadMigrationsFrom(module_path('PaymentOnline', 'Database/Migrations'));
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('PaymentOnline', 'Config/config.php') => config_path('paymentonline.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('PaymentOnline', 'Config/config.php'), 'paymentonline'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/paymentonline');

        $sourcePath = module_path('PaymentOnline', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/paymentonline';
        }, \Config::get('view.paths')), [$sourcePath]), 'paymentonline');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/paymentonline');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'paymentonline');
        } else {
            $this->loadTranslationsFrom(module_path('PaymentOnline', 'Resources/lang'), 'paymentonline');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('PaymentOnline', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
    public function rendAsideMenu()
    {
        \Eventy::addFilter('aside_menu.dashboard_after', function () {

            print view('paymentonline::partials.aside_menu.dashboard_after_booking');
        }, 2, 1);
    }
}
