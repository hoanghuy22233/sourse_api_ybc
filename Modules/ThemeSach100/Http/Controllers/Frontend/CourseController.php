<?php

namespace Modules\ThemeSach100\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Error;
use Illuminate\Http\Request;
use Mail;
use Modules\EduCourse\Models\LessonItem;
use Modules\EduCourse\Models\Quizzes;
use Modules\ThemeSach100\Models\Category;
use Modules\ThemeSach100\Models\Course;
use Modules\ThemeSach100\Models\Note;
use Modules\ThemeSach100\Models\Post;
use Session;
use Validator;
use \Modules\ThemeSach100\Models\Order;

class CourseController extends Controller
{
    public function list($slug, $r = false)
    {
        $user = \Auth::guard('student')->user();
        $category = Category::where('slug', $slug)->first();
        if (!is_object($category)) {
            if (Post::where('status', 1)->where('type_page', 'page')->where('slug', $slug)->count() > 0) {
                $postController = new PostController();
                return $postController->page($slug);
            }

            abort(404);
        }
        $data['category'] = $category;
        $data['slug1'] = $slug;

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $category->meta_title != '' ? $category->meta_title : $category->name,
            'meta_description' => $category->meta_description != '' ? $category->meta_description : $category->name,
            'meta_keywords' => $category->meta_keywords != '' ? $category->meta_keywords : $category->name,
        ];
        view()->share('pageOption', $pageOption);

        if (in_array($category->type, [1])) {         //  Tài liệu
            $data['posts'] = CommonHelper::getFromCache('posts_multi_category_id' . $category->id . ($r ? base64_encode(json_encode($r->all())) : ''), ['posts']);
            if (!$data['posts']) {
                $data['posts'] = @Post::where('multi_cat', 'like', '%|' . @$category->id . '|%')->where('slug', 'NOT LIKE', '%http%')
                    ->orderBy('order_no', 'desc')->orderBy('id', 'desc')->paginate(8);
                CommonHelper::putToCache('posts_multi_category_id' . $category->id . ($r ? base64_encode(json_encode($r->all())) : ''), $data['posts'], ['posts']);
            }


            return view('themesach100::pages.post.list')->with($data)->with('user', $user);
        } elseif (in_array($category->type, [5])) { //  Khóa học
            $data['courses'] = CommonHelper::getFromCache('course_by_category_id' . $category->id . ($r ? base64_encode(json_encode($r->all())) : ''), ['courses']);
            if (!$data['courses']) {
                $cat_childs = $category->childs;
                $data['courses'] = @Course::where(function ($query) use ($category, $cat_childs) {
                    $query->orWhere('multi_cat', 'LIKE', '%|' . $category->id . '|%');
                    foreach ($cat_childs as $cat) {
                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat->id . '|%');
                    }
                })->orderBy('order_no', 'desc')->orderBy('id', 'desc')->paginate(8);
                CommonHelper::putToCache('course_by_category_id' . $category->id . ($r ? base64_encode(json_encode($r->all())) : ''), $data['courses'], ['courses']);
            }

            if ($slug == 'mau-kinh-doanh') {
                return view('themesach100::pages.business_template.list')->with($data)->with('user', $user);
            }

            return view('themesach100::pages.course.course')->with($data)->with('user', $user);
        } elseif (in_array($category->type, [10])) {
            //  Bai kiem tra

            $questionController = new \Modules\ThemeSach100\Http\Controllers\Frontend\QuestionController();
            return $questionController->taoBaiThi($r, $category->id);
        }
//        elseif (in_array($category->type, [2])) {
//            //  Lich hoc
//
//            $data['posts'] = CommonHelper::getFromCache('calendars_multi_category_id' . $category->id);
//            if (!$data['posts']) {
//                $data['posts'] = @Post::where('multi_cat', 'like', '%|' . @$category->id . '|%')
//                    ->orderBy('order_no', 'desc')->orderBy('id', 'asc')->paginate(8);
//                CommonHelper::putToCache('calendars_multi_category_id' . $category->id, $data['posts']);
//            }
//            return view('themesach100::pages.calendar.calendar')->with($data)->with('user', $user);
//        }
        abort(404);
    }


    public function oneParam(request $r, $slug1)
    {
        //  Chuyen den trang danh sanh tin tuc | Danh sach san pham
        return $this->list($slug1, $r);
    }

    //  VD: hobasoft/card-visit  | danh-muc-cha/danh-muc-con | danh-muc/bai-viet.html | danh-muc/san-pham.html
    public function twoParam($slug1, $slug2)
    {

        if (strpos($slug2, '.html')) {  //  Nếu là trang chi tiết

            $slug2 = str_replace('.html', '', $slug2);
            if (Course::where('slug', $slug2)->where('status', 1)->count() > 0) {        //  Chi tiet khóa học
                return $this->detail($slug1, $slug2);
            } elseif (LessonItem::where('slug', $slug2)->count() > 0) {  //  Chi tiet
                return $this->lesson_detail($slug1, $slug2);
//            } elseif (Misson::where('slug', $slug2)->count() > 0) {  //  Nhiệm vụ
//                return $this->misson_detail($slug1, $slug2);
            } elseif (Quizzes::where('slug', $slug2)->count() > 0) {  //  Chi tiet bai kiem tra
                $quizController = new QuizController();
                return $quizController->detail($slug1, $slug2);
            }
//            elseif (Post::where('slug', $slug2)->where('type', 1)->count() > 0) {  //  Chi tiet lich hoc
//                $calendarController = new CalendarController();
//                return $calendarController->detail($slug1, $slug2);
//            }
            else {        //  Chi tiet tai lieu
                $postController = new PostController();
                return $postController->detail($slug1, $slug2);
            }
            abort(404);
        }
        return $this->list($slug2);
    }

    //  VD: hobasoft/card-visit/card-truyen-thong
    public function threeParam($slug1, $slug2, $slug3)
    {
        if (strpos($slug3, '.html')) {  //  Nếu là trang chi tiết
            $slug3 = str_replace('.html', '', $slug3);
            if (Course::where('slug', $slug3)->where('status', 1)->count() > 0) {        //  Chi tiet san pham
                return $this->detail($slug3);
            } elseif (\Modules\ThemeSach100\Models\LessonItem::where('slug', $slug3)->count() > 0) {//Chi tiet video
                return $this->lesson_item_detail($slug3);
            } else {        //  Chi tiet tin tuc
                $postController = new PostController();
                return $postController->detail($slug3);
            }
        }
        return $this->list($slug3);
    }

    public function fourParam($slug1, $slug2, $slug3, $slug4)
    {
        if (strpos($slug4, '.html')) {  //  Nếu là trang chi tiết
            $slug4 = str_replace('.html', '', $slug4);
            if (Course::where('slug', $slug4)->where('status', 1)->count() > 0) {        //  Chi tiet san pham
                return $this->detail($slug4);
            } else {        //  Chi tiet tin tuc
                $postController = new PostController();
                return $postController->detail($slug4);
            }
        }
        abort(404);
    }

    //  Trang chi tiết khóa học
    public function detail($slug1, $slug)
    {
        $data['user'] = \Auth::guard('student')->user();
        $slug = str_replace('.html', '', $slug);
        $data['course'] = CommonHelper::getFromCache('course_slug' . $slug, ['courses']);
        if (!$data['course']) {
            $data['course'] = Course::where('slug', $slug)->where('status', 1)->first();
            CommonHelper::putToCache('course_slug' . $slug, $data['course'], ['courses']);
        }

        if (!is_object($data['course'])) {
            abort(404);
        }
        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['course']->meta_title != '' ? $data['course']->meta_title : $data['course']->name,
            'meta_description' => $data['course']->meta_description != '' ? $data['course']->meta_description : $data['course']->name,
            'meta_keywords' => $data['course']->meta_keywords != '' ? $data['course']->meta_keywords : $data['course']->name,
        ];
        view()->share('pageOption', $pageOption);

        if ($slug1 == 'mau-kinh-doanh') {
            if ($data['course']->final_price == 0) {
                return view('themesach100::pages.business_template.free.detail', $data);
            } else {
                return view('themesach100::pages.business_template.pay.detail', $data);
            }
        }

        return view('themesach100::pages.course.list_lesson', $data);
    }

    //  Trang chi tiet video bai hoc
    public function lesson_item_detail($slug)
    {
        $data['lesson_item'] = CommonHelper::getFromCache('lesson_items_slug' . $slug, ['lesson_items']);
        if (!$data['lesson_item']) {
            $data['lesson_item'] = LessonItem::where('slug', $slug)->first();
            CommonHelper::putToCache('lesson_items_slug' . $slug, $data['lesson_item'], ['lesson_items']);
        }
        if (!is_object($data['lesson_item'])) {
            abort(404);
        }

        //  Kiểm tra nếu user chưa mua khóa này hoặc khóa này ko cho học thử sẽ bắn quay lại trang chủ
        $order = Order::where('student_id', @\Auth::guard('student')->user()->id)->where('course_id', $data['lesson_item']->course_id)->first();
        if ($data['lesson_item']->publish != 1 && @$order->status != 1) {
            CommonHelper::one_time_message('error', 'Bạn chưa mua bài học này!');
            return back();
        }

        $data['courses'] = Course::findOrFail($data['lesson_item']->course_id);
        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['lesson_item']->meta_title != '' ? $data['lesson_item']->meta_title : $data['lesson_item']->name,
            'meta_description' => $data['lesson_item']->meta_description != '' ? $data['lesson_item']->meta_description : $data['lesson_item']->name,
            'meta_keywords' => $data['lesson_item']->meta_keywords != '' ? $data['lesson_item']->meta_keywords : $data['lesson_item']->name,
        ];
        view()->share('pageOption', $pageOption);

        if ($data['courses']->type == 'Video') {
            return view('themesach100::pages.course.video.detail', $data);
        }
        return view('themesach100::pages.course.text.course_detail', $data);
    }

    public function lesson_detail($slug1, $slug)
    {
        $data['user'] = \Auth::guard('student')->user();
        $slug = str_replace('.html', '', $slug);
        $data['lessonitem'] = CommonHelper::getFromCache('lesson_item_slug' . $slug, ['lesson_items']);
        if (!$data['lessonitem']) {
            $data['lessonitem'] = LessonItem::where('slug', $slug)->first();

            CommonHelper::putToCache('lesson_item_slug' . $slug, $data['lessonitem'], ['lesson_items']);
        }

        $data['slug1'] = $slug1;
        if (!is_object($data['lessonitem'])) {
            abort(404);
        }

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['lessonitem']->meta_title != '' ? $data['lessonitem']->meta_title : $data['lessonitem']->name,
            'meta_description' => $data['lessonitem']->meta_description != '' ? $data['lessonitem']->meta_description : $data['lessonitem']->name,
            'meta_keywords' => $data['lessonitem']->meta_keywords != '' ? $data['lessonitem']->meta_keywords : $data['lessonitem']->name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themesach100::pages.course.text.course_detail', $data);
    }

    public function getSearch(request $r)
    {
        $data['user'] = \Auth::guard('student')->user();

//        if (!$data['courses']) {
//            $data['courses'] = Course::where('name', 'like', '%' . $r->q . '%')->where('status', 1)->orderBy('id', 'desc')->paginate(8);
//            CommonHelper::putToCache('courses_name_like' . $r->q, $data['courses']);
//        }

        $pageOption = [
            'meta_title' => 'Tìm kiếm từ khóa: ' . @$r->q,
            'meta_description' => 'Tìm kiếm từ khóa: ' . @$r->q,
            'meta_keywords' => 'Tìm kiếm từ khóa: ' . @$r->q,
        ];
        view()->share('pageOption', $pageOption);
        $data['q'] = $r->q;
        if (isset($r->type)) {
            if ($r->type == 2) {
                $data['posts'] = Post::where('name', 'like', '%' . $r->q . '%')->where('status', 1)->orderBy('id', 'desc')->paginate(8);
                return view('themesach100::pages.course.search_course', $data);
            } elseif ($r->type == 3) {
                $data['quizzes'] = \Modules\ThemeSach100\Models\Quizzes::where('name', 'like', '%' . $r->q . '%')->orderBy('id', 'desc')->paginate(8);
                return view('themesach100::pages.course.search_course', $data);
            } else {
                $data['courses'] = Course::where('name', 'like', '%' . $r->q . '%')->where('status', 1)->orderBy('id', 'desc')->paginate(8);
            }
        } else {
            $data['courses'] = Course::where('name', 'like', '%' . $r->q . '%')->where('status', 1)->orderBy('id', 'desc')->paginate(8);
        }
        return view('themesach100::pages.course.search_course', $data);
    }

    function getDetail(request $r)
    {
        return view('themesach100::pages.course.detail');
    }

    public function lichHoc()
    {
        $data['user'] = \Auth::guard('student')->user();
        $pageOption = [
            'meta_title' => 'Lịch học',
            'meta_description' => 'Lịch học, lịch dạy của trung tâm',
            'meta_keywords' => 'Lịch học, lịch dạy',
        ];
        view()->share('pageOption', $pageOption);
        return view('themesach100::pages.calendar.google_iframe', $data);
    }

//    function Error(request $r)
//    {
//        $error = new Error();
//        $error->module = 'ThemeSach100';
//        $error->message ='Tiết học:'. $r->lesson_item_id.'Học viên:'. @\Auth::guard('student')->user()->id;
//        $error->code = 'mã';
//        $error->file = 'thư mục';
//        $error->save();
//        \App\Http\Helpers\CommonHelper::flushCache($this->module['table_name']);
//        return response()->json([
//            'status' => true,
//            'msg' => 'Thông báo lỗi thành công'
//        ]);
//    }

    function Note(request $r)
    {
        $note = new Note();
        $note->student_id = @\Auth::guard('student')->user()->id;
        $note->lesson_item_id = @$r->lesson_item_id;
        $note->note = @$r->message;
        $note->save();
        \App\Http\Helpers\CommonHelper::flushCache($this->module['table_name']);
        return response()->json([
            'status' => true,
            'msg' => 'Lưu thành công'
        ]);
    }
}
