<?php

namespace Modules\ThemeSach100\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Modules\ThemeSach100\Models\Bill;
use Modules\ThemeSach100\Models\Category;
use Modules\ThemeSach100\Models\Exam;
use Modules\ThemeSach100\Models\Question;
use Modules\ThemeSach100\Models\QuestionsError;
use Session;
use Mail;

class QuestionController extends Controller
{

    public function taoBaiThi(Request $r, $subject_id = false)
    {
        if (!$_POST) {
            if (!$r->has('subject_id') && !$subject_id) {
                return redirect('/' . Category::where('type', 10)->where('status', 1)->first()->slug);
            }
            $subject_id = $subject_id ? $subject_id : $r->subject_id;
            $subject = Category::where('type', 10)->where('id', $subject_id)->first();

            $data = [
                'subject' => $subject
            ];

            //  Thẻ meta cho seo
            $pageOption = [
                'meta_title' => $subject->meta_title != '' ? $subject->meta_title : $subject->name,
                'meta_description' => $subject->meta_description != '' ? $subject->meta_description : $subject->name,
                'meta_keywords' => $subject->meta_keywords != '' ? $subject->meta_keywords : $subject->name,
            ];
            view()->share('pageOption', $pageOption);
            return view('themesach100::pages.question.tao_bai_thi', $data);
        } else {
            if ($r->has('chapter_name')) {
                $chapter_id = @Category::where('name', $r->chapter_name)->where('parent_id', $r->subject_id)->where('status', 1)->first()->id;
            } else {
                $chapter_id = null;
            }

            $level = null;
            if ($r->has('level')) {
                $level = [
                    'Cơ bản' => 'de',
                    'Nghiệp dư' => 'tb',
                    'Master' => 'kho',
                ];
                $level = $level[$r->level];
            }

            $exam = $this->createExam(\Auth::guard('student')->user()->id, $r->subject_id, $level, $chapter_id, null);
            return redirect('de-thi/' . $exam->id);
        }
    }

    public function vaoThiSubject(Request $r, $subjectSlug) {
        $subject_id = Category::where('slug', $subjectSlug)->where('type', 10)->first()->id;
        return $this->vaoThi($r, $subject_id);
    }

    public function vaoThiChapter(Request $r, $chapter_slug) {
        $chapter = Category::where('slug', $chapter_slug)->where('type', 11)->first();
        if (!is_object($chapter)) {
            abort(404);
        }
        return $this->vaoThi($r, $chapter->parent_id, $chapter->id);
    }

    public function vaoThi(Request $r, $subject_id = null, $chapter_id = null) {
        if ($chapter_id == null) {
            $chapter_id = $r->get('chapter_id', null);
        }

        if ($subject_id == null) {
            $subject_id = $r->get('subject_id', null);
        }
        if ($subject_id == null && $chapter_id != null) {
            $subject_id = @Category::find($chapter_id)->parent_id;
        }

        $level = $r->get('level', null);
        $exam = $this->createExam(\Auth::guard('student')->user()->id, $subject_id, $level, $chapter_id, null);
        return redirect('de-thi/' . $exam->id);
    }

    //  Tạo bài thi tương tự với bài $r->exam_id
    public function taoBaiTuongTu(Request $r) {
        $exam = Exam::find($r->exam_id);
        if(strpos($exam->chapter_id, '|') === false) {
            //  Đề tăng cường kỹ năng thì chỉ có 1 kỹ năng
            $chapter_id = $exam->chapter_id;
        } else {
            //  Đề chuẩn có nhiều kỹ năng
            $chapter_id = null;
        }
        $level = null;
        if ($r->has('level')) {
            $level = [
                'Cơ bản' => 'de',
                'Nghiệp dư' => 'tb',
                'Master' => 'kho',
            ];
            $level = $level[$exam->level];
        }
        $exam = $this->createExam(\Auth::guard('student')->user()->id, $exam->subject_id, $level, $chapter_id, null);
        return redirect('de-thi/' . $exam->id);
    }

    //  Tạo đề thi
    public function createExam($student_id, $subject_id = null, $level = null, $chapter_id = null, $thematic_id = null)
    {
//        dd($student_id, $subject_id, 8, $chapter_id, null);
        $exam = new Exam();
        $exam->student_id = $student_id;
        $exam->subject_id = $subject_id;
        $exam->created_at2 = date('Y-m-d H:i:s');
        $exam->level = $level;

        $questions = [];

        if ($level == null) {
            //  Nếu ko chọn đề test nhanh
            if ($chapter_id == null) {
                //  Nếu ko chọn kỹ năng thì lấy tất cả các kỹ năng
                $chapter_ids = Category::where('parent_id', $subject_id)->where('status', 1)->pluck('id')->toArray();

                $exam->type = 'Đề chuẩn JLPT';
            } else {
                //  Nếu chọn cụ thể kỹ năng thì chỉ lấy 1 kỹ năng đó
                $chapter_ids = [$chapter_id];

                $exam->type = 'Đề tăng cường kỹ năng';
            }

            $thematic_ids = [];
            foreach ($chapter_ids as $chapter_id) {
                $thematics = Category::where('parent_id', $chapter_id)->where('status', 1)->pluck('number_question', 'id');
                foreach ($thematics as $thematic_id => $number_question) {
                    if (is_numeric($number_question) && $number_question > 0) {
                        $thematic_ids[] = $thematic_id;
                        $q_arr = Question::where('status', 1)->where('thematic_id', $thematic_id)->where(function ($query) {
                            $query->orWhere('type', '!=', 'Đề Test nhanh');
                            $query->orWhereNull('type');
                        })->whereNull('parent_id')->orderByRaw("RAND()")->limit($number_question)->pluck('id')->toArray();
                        $questions = array_merge($questions, $q_arr);
                    }
                }
            }
        } else {
            //  Nếu chọn đề test nhanh

            //  Lấy tất cả câu hỏi trong trình độ đã chọn & độ khó đã chọn
            $questions = Question::where('status', 1)->where('subject_id', $subject_id)->whereNull('parent_id')->where('level', $level)->where('type', 'Đề Test nhanh')
                ->orderByRaw("RAND()")->pluck('id')->toArray();

            $chapter_ids = Question::whereIn('id', $questions)->groupBy('chapter_id')->pluck('chapter_id')->toArray();
            $thematic_ids = Question::whereIn('id', $questions)->groupBy('thematic_id')->pluck('thematic_id')->toArray();

            $exam->type = 'Đề Test nhanh';
        }

        //  Lấy các câu hỏi con
        $q_arr = Question::where('status', 1)->whereIn('parent_id', $questions)->pluck('id')->toArray();
        $questions = array_merge($questions, $q_arr);

        $exam->chapter_id = implode('|', $chapter_ids);
        $exam->thematic_id = implode('|', $thematic_ids);

        $data = [];
        foreach ($questions as $question) {
            $data[$question] = '';
        }
        $exam->data = json_encode($data);
        $exam->save();
        return $exam;
    }

    /**
     * Truy vấn các câu hỏi trong 1 chương, chia đều theo các chuyên đề
     */
    public function getQuestionsFromChapter($thematic_id, $chapter_id, $level, $limit)
    {
        if ($thematic_id == null) {
            $thematics = Category::where('parent_id', $chapter_id)->where('status', 1)->get();
        } else {
            $thematics = Category::where('id', $thematic_id)->where('status', 1)->get();
        }
        $tb_so_mon_moi_chuong = (int)$limit / count($thematics);   //   Trung bình số câu cần truy vấn mỗi chuyên đề

        $q_arr = [];
        $count = 0;
        if ($tb_so_mon_moi_chuong > 0 && count($thematics) > 0) {
            foreach ($thematics as $thematic) {
                $ids = Question::where('status', 1)->where('thematic_id', $thematic->id)->where('level', $level)
                    ->orderByRaw("RAND()")->limit($tb_so_mon_moi_chuong)->pluck('id')->toArray();
                $q_arr = array_merge($q_arr, $ids);
                $count += count($ids);
            }
        }

        //  Nếu truy vấn chưa đủ thì lấy thêm
        if ($limit > $count && $thematic_id == null) {
            $con_lai = $limit - $count;
            $ids = Question::where('status', 1)->where('chapter_id', $chapter_id)->where('level', $level)->whereNotIn('id', $q_arr)
                ->orderByRaw("RAND()")->limit($con_lai)->pluck('id')->toArray();
            $q_arr = array_merge($q_arr, $ids);
        }

        return $q_arr;
    }

    public function deThi($id, Request $r)
    {
        if (!$_POST) {
            $data['exam'] = Exam::find($id);
            $data['exam_data'] = (array)json_decode($data['exam']->data);
            foreach ($data['exam_data'] as $k => $v) {
                $data['exam_data'][ (int) $k] = $v;
            }
            $question_ids = array_keys($data['exam_data']);

            //  Check quyền sửa
            $data['can_edit'] = true;
            if ($data['exam']->student_id != \Auth::guard('student')->user()->id) { //   không phải bài của mình thì ko được sửa
                $data['can_edit'] = false;
            }
            if ($data['exam']->point != null) { //  đã có điểm số thì không được sửa
                $data['can_edit'] = false;
            }


            if ($data['exam']->type == 'Đề tăng cường kỹ năng') {
                $ch = Category::whereIn('id', explode('|', $data['exam']->chapter_id))->orderBy('order_no', 'ASC')->get();

                $order_no = 0;
                $chapters = [];
                foreach ($ch as $v) {
                    $chapters[0][] = $v;
                }
                $data['chapters'] = $chapters;
            } else {
                //
                $order_no = $r->get('order_no', 1);
                $ch = Category::where('type', '11')->where('parent_id', $data['exam']->subject_id)->orderBy('order_no', 'ASC')->get();

                $chapters = [];
                foreach ($ch as $v) {
                    $chapters[$v->order_no][] = $v;
                }
                $data['chapters'] = $chapters;
            }

//            dd($chapters[$order_no][0]);
            if(@$chapters[$order_no][0]->status == 0) {
                //  Nếu đang ở phần nghỉ
                return view('themesach100::pages.question.nghi', $data);
            }

            $chapter_ids_active = [];
            $monDai = [];
            foreach ($chapters[$order_no] as $chapter) {
                $chapter_ids_active[] = $chapter->id;
                $monDai = array_merge($monDai, Category::select('id', 'name')->where('type', '12')->where('parent_id', $chapter->id)->orderBy('order_no', 'ASC')->get()->toArray());
            }

            foreach ($monDai as $k => $md) {
                $monDai[$k]['questions'] = Question::whereNull('parent_id')->whereIn('id', $question_ids)->where('thematic_id', $md['id'])->get();
            }
            $data['monDai'] = $monDai;

            //  Thẻ meta cho seo
            $pageOption = [
                'meta_title' => 'Luyện thi',
                'meta_description' => 'Luyện thi',
                'meta_keywords' => 'Luyện thi',
            ];
            view()->share('pageOption', $pageOption);
            return view('themesach100::pages.question.de_thi', $data);
        } else {
            $exam = Exam::find($id);

            $data = (array)json_decode($exam->data);
            foreach ($data as $q_id => $dn) {
                if (@$r->dapan[$q_id] != null) {
                    $data[$q_id] = @$r->dapan[$q_id];
                }
            }
            $exam->data = json_encode($data);

            $exam->save();
            $order_no = $r->order_no;
            if (strpos($exam->chapter_id, '|') !== false && @$r->het_gio != '1') {
                //  Nếu không phải là thi theo kỹ năng thì next sang kỹ năng tiếp
                $chapter_next = Category::where('type', '11')->where('parent_id', $exam->subject_id)->where('order_no', '>', $order_no);
                if ($exam->type == 'Đề Test nhanh') {
                    $chapter_next = $chapter_next->where('show_in_quick_test', 1);
                }
                $chapter_next = $chapter_next->orderBy('order_no', 'ASC')->first();
                if (is_object($chapter_next)) {
                    //  Nếu còn bài thi sau thì chuyển đến bài sau

                    return redirect('/de-thi/' . $id . '?order_no=' . $chapter_next->order_no);
                }
            }
            $this->tinhDiemBaiThi($exam);

            CommonHelper::flushCache('exams');

            return redirect('/hoan-thanh-bai-thi?exam_id=' . $id);
        }
    }

    /*
     * Tính điểm bài thi
     * **/
    public function tinhDiemBaiThi($exam) {
        $exam_data = (array)json_decode($exam->data);
        foreach ($exam_data as $k => $v) {
            $exam_data[ (int) $k] = $v;
        }
        $questions = Question::whereIn('id', array_keys($exam_data))->get();
        $tong_diem = 0;
        $diem = 0;
        $ky_nang = [];
        foreach($questions as $question) {
            $tong_diem += $question->point;

            if (!isset($ky_nang[$question->chapter_id])) {
                $ky_nang[$question->chapter_id] = [
                    'diem' => 0,
                    'cau_dung' => 0,
                    'max_diem' => 0
                ];
            }

            if(@$exam_data[$question->id] == strtolower($question->answer)) {
                $diem += $question->point;

                $ky_nang[$question->chapter_id]['diem'] += $question->point;
                $ky_nang[$question->chapter_id]['cau_dung'] ++;
            }
            $ky_nang[$question->chapter_id]['max_diem'] += $question->point;
        }
        $exam->total_point = $diem;
        $exam->max_point = $tong_diem;
        $exam->data_diem = json_encode($ky_nang);
        $exam->save();
        return $exam;
    }

    public function hoanThanhBaiThi(Request $r)
    {
        $data['exam'] = Exam::find($r->exam_id);

        if ($data['exam']->type == 'Đề tăng cường kỹ năng') {
            //  Nếu là đề kỹ năng thì chuyển sang trang xem đáp án luôn
            return redirect('/dap-an/' . $r->exam_id);
        }

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Đáp án bài thi',
            'meta_description' => 'Đáp án bài thi',
            'meta_keywords' => 'Đáp án bài thi',
        ];
        view()->share('pageOption', $pageOption);
        return view('themesach100::pages.question.hoan_thanh_bai_thi', $data);
    }

    public function dapAn(Request $r, $id)
    {
        $data['exam'] = Exam::find($id);
        $data['exam_data'] = (array)json_decode($data['exam']->data);
        foreach ($data['exam_data'] as $k => $v) {
            $data['exam_data'][ (int) $k] = $v;
        }
        $question_ids = array_keys($data['exam_data']);

        //  Lấy kỹ năng đang thi
        $order_no = $r->get('order_no', 1);
        $ch = Category::where('type', '11')->where('parent_id', $data['exam']->subject_id)->where('status', '!=', 0)->orderBy('order_no', 'ASC')->get();
        $chapters = [];
        foreach ($ch as $v) {
//            $chapters[$v->order_no][] = $v;
            $chapters[] = $v;
        }

        $data['chapters'] = $chapters;

//            dd($chapters[$order_no][0]);
        if(@$chapters[$order_no][0]->status === 0) {
            //  Nếu đang ở phần nghỉ
            return view('themesach100::pages.question.nghi', $data);
        }

        $chapter_ids_active = [];
        $monDai = [];
//        foreach ($chapters[$order_no] as $chapter) {
        foreach ($chapters as $chapter) {
            $chapter_ids_active[] = $chapter->id;
            $monDai = array_merge($monDai, Category::select('id', 'name')->where('type', '12')->where('parent_id', $chapter->id)->orderBy('order_no', 'ASC')->get()->toArray());
        }

        foreach ($monDai as $k => $md) {
            $monDai[$k]['questions'] = Question::whereNull('parent_id')->whereIn('id', $question_ids)->where('thematic_id', $md['id'])->get();
        }
        $data['monDai'] = $monDai;

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Đáp án bài thi',
            'meta_description' => 'Đáp án bài thi',
            'meta_keywords' => 'Đáp án bài thi',
        ];
        view()->share('pageOption', $pageOption);
        return view('themesach100::pages.question.dap_an', $data);
    }

    public function khoaHoc()
    {
        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Khóa học',
            'meta_description' => 'Khóa học',
            'meta_keywords' => 'Khóa học',
        ];
        view()->share('pageOption', $pageOption);
        $data = [];

        return view('themesach100::pages.question.khoa_hoc', $data);
    }

    public function baoCaoLoi(Request $r)
    {

        QuestionsError::create([
            'code' => $r->prd_id,
            'reason' => $r->reason,
            'content' => $r->content,
        ]);
        try {
            $data['code'] = $r->prd_id;
            $data['reason'] = $r->reason;
            $data['content'] = $r->content;


//            $settings = Setting::where('type', 'mail')->pluck('value', 'name')->toArray();
//            $admins = explode(',', $settings['admin_emails']);
//            try{
//                \Illuminate\Support\Facades\Mail::send(['html' => 'themesach100::mail.mail_report_answer'], $data, function ($message) {
//                    $message->from(env('MAIL_USERNAME'), '[ ' . $_SERVER['SERVER_NAME'] . ' ]');
//                    $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] Báo lỗi câu hỏi');
//
//                    $message->to('hieunc0398@gmail.com')->subject('Báo cáo lỗi câu hỏi!');
//                });
//            } catch (\Exception $ex) {
//
//            }
            return response()->json([
                'status' => true
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false

            ]);
        }

    }

    public function dangKyMon(Request $r)
    {
        if (!$_POST) {
            $data = [];

            //  Thẻ meta cho seo
            $pageOption = [
                'meta_title' => 'Mua khóa học',
                'meta_description' => 'Mua khóa học',
                'meta_keywords' => 'Mua khóa học',
            ];
            view()->share('pageOption', $pageOption);
            return view('themesach100::pages.question.dang_ky_mon', $data);
        } else {
            $setting = Setting::where('type', 'service_tab')->where('name', count($r->subjects))->first();
            if (!is_object($setting)) {
                return back();
            }

            $price = $setting->value;

            $user = \Auth::guard('student')->user();

            // Tạo bill
            $bill = new Bill();
            $bill->user_id = $bill->student_id = $user->id;
            $bill->user_tel = $user->tel;
            $bill->user_name = $user->name;
            $bill->user_email = $user->email;
            $bill->user_address = $user->address;
            $bill->subjects = '|' . implode('|', $r->subjects) . '|';
            $bill->paid = 0;
            $bill->status = 0;
            $bill->total_price = $price;
            $bill->save();

            $this->sendMailCreateBill($bill);

            //  Nếu không đủ tiền thì chuyển đến trang nạp tiền
            if ($user->balance < $price) {
                $price_need = $price - $user->balance;
                \Session::flash('error', 'Tài khoản của bạn không đủ tiền. Vui lòng nạp thêm ' . number_format($price_need, 0, '.', '.') . 'đ. <a href="/bai-viet/huong-dan-nap-tien.html?price=' . $price_need . '" style="font-weight: bolder; color: blue;">CLICK vào đây để Nạp tiền</a>');
                return back()->withInput();

                Session::put('bill_wait_pay', [
                    'bill_id' => $bill->id,
                    'price' => $price_need,
                    'subjects' => $r->subjects,
                ]);
                return redirect('/thanh-toan');
            }

            $this->deductMoneyRegisterSubject($user, $price, $bill);
            return redirect('/dang-ky-mon-thanh-cong?subjects=' . $bill->subjects);
        }
    }

    public function sendMailCreateBill($bill)
    {
        $settings = Setting::whereIn('type', ['general_tab', 'mail'])->pluck('value', 'name')->toArray();
        if (@$settings['admin_receives_mail'] == 1) {
            //  Gửi mail cho admin
            $admin_emails = explode(',', trim($settings['admin_emails'], ','));
            foreach ($admin_emails as $k => $admin_email) {
                $admin_emails[$k] = trim($admin_email);
            }
            Mail::send(['html' => 'themesach100::mail.order_subjects_admin'], compact('bill'), function ($message) use ($settings, $admin_emails) {
                $message->from($settings['mailgun_username'], $settings['mail_name']);
                $message->to(trim($admin_emails[0]), 'Admin');
                unset($admin_emails[0]);
                if (!empty($admin_emails)) {
                    $message->cc($admin_emails);
                }
                $message->subject('[ ' . $_SERVER['SERVER_NAME'] . ' ] Đơn hàng mới');
            });
        }
    }

    function configMail()
    {
        $settings = Setting::whereIn('type', ['mail'])->pluck('value', 'name')->toArray();
        if (isset($settings['driver'])) {
            $username = $settings['driver'] == 'mailgun' ? @$settings['mailgun_username'] : @$settings['smtp_username'];
            $config =
                [
                    'mail.from' => [
                        'address' => $username,
                        'name' => @$settings['mail_name'],
                    ],
                    'mail.driver' => @$settings['driver'],
                ];

            if ($settings['driver'] == 'mailgun') {
                $config['services.mailgun'] =
                    [
                        'domain' => trim(@$settings['mailgun_domain']),
                        'secret' => trim(@$settings['mailgun_secret']),
                    ];
                $config['mail.port'] = @$settings['mailgun_port'];
                $config ['mail.username'] = @$settings['mailgun_username'];
            } else {
                $config['mail.port'] = @$settings['smtp_port'];
                $config['mail.password'] = @$settings['smtp_password'];
                $config['mail.encryption'] = @$settings['smtp_encryption'];
                $config['mail.host'] = @$settings['smtp_host'];
                $config['mail.username'] = @$settings['smtp_username'];
            }
//            $config['services.onesignal'] = [
//                'app_id' => '420af10d-5030-4f34-af19-68078fd6467c',
//                'rest_api_key' => 'MTY0MjA5NTktNjgwNS00NGM3LTg3YmYtNzcwMmRhZDUyZmE2'
//            ];
//            config onesignal
//            dd($config);
            config($config);
        }
        return $settings;
    }

    /**
     * Trừ tiên tài khoản & thêm môn vào danh sách đã mua
     *
     */
    public function deductMoneyRegisterSubject($user, $price, $bill)
    {
        //  Trừ tiền
        $user->balance = $user->balance - $price;

        //  Thêm môn vào danh sách môn đã mua
        $user->subjects .= '|' . $bill->subjects . '|';
        $user->subjects = preg_replace('/\|\|/', '|', $user->subjects);

        $user->save();

        $bill->status = 1;
        $bill->paid = 1;
        $bill->save();

        return true;
    }

    public function dangKyMonThanhCong(Request $r)
    {
        $data = [];

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Đăng ký môn học thành công',
            'meta_description' => 'Đăng ký môn học thành công',
            'meta_keywords' => 'Đăng ký môn học thành công',
        ];
        view()->share('pageOption', $pageOption);
        return view('themesach100::pages.question.dang_ky_mon_thanh_cong', $data);
    }

    public function bangXepHang() {
        $data = [];

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Bảng xếp hạng',
            'meta_description' => 'Bảng xếp hạng',
            'meta_keywords' => 'Bảng xếp hạng',
        ];
        view()->share('pageOption', $pageOption);
        return view('themesach100::pages.question.bang_xep_hang', $data);
    }

    public function bxhGetContent() {
        $data = [];
        return view('themesach100::partials.bang_xep_hang_content', $data);
    }
}
