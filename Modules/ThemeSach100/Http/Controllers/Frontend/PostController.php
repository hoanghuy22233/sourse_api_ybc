<?php

namespace Modules\ThemeSach100\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\ThemeSach100\Models\Menus;
use Modules\ThemeSach100\Models\Post;

class PostController extends Controller
{
//      Trang chi tiết bài viết
    public function detail($slug1, $slug)
    {
        $data['user'] = \Auth::guard('student')->user();
        $slug = str_replace('.html', '', $slug);

//        $data['post'] = CommonHelper::getFromCache('post_slug');
//        if (!$data['post']) {
            $data['post'] = Post::where('slug', $slug)->first();
//            CommonHelper::putToCache('post_slug', $data['post']);
//        }


        if (!is_object($data['post'])) {
            abort(404);
        }

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
            'meta_description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->name,
            'meta_keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->name,
            'image' => $data['post']->image != '' ? asset('public/filemanager/userfiles/' . $data['post']->image) : null,
        ];
        view()->share('pageOption', $pageOption);

        return view('themesach100::pages.post.detail', $data);
    }

    public function page($slug) {
        $data['post'] = Post::where('slug', $slug)->where('status', 1)->first();
        if (!is_object($data['post'])) {
            abort(404);
        }
        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['post']->meta_title != '' ? $data['post']->meta_title : $data['post']->name,
            'meta_description' => $data['post']->meta_description != '' ? $data['post']->meta_description : $data['post']->name,
            'meta_keywords' => $data['post']->meta_keywords != '' ? $data['post']->meta_keywords : $data['post']->name,
        ];
        view()->share('pageOption', $pageOption);
        return view('themesach100::pages.post.static_page', $data);
    }
}
