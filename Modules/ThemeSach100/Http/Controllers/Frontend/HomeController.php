<?php

namespace Modules\ThemeSach100\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Session;


class HomeController extends Controller
{
    function getHome()
    {

        $data['user'] = \Auth::guard('student')->user();
        return view('themesach100::pages.home.home',$data);
    }

    public function listMember($role_name) {
        $admins = Admin::leftJoin('role_admin', 'role_admin.admin_id', '=', 'admin.id')
            ->leftJoin('roles', 'roles.id', '=', 'role_admin.role_id')
            ->where('roles.name', $role_name)
            ->selectRaw('admin.id, admin.email, admin.name, admin.image, admin.tel, admin.intro, roles.display_name as role_name')->paginate(30);

        $data['admins'] = $admins;
//        dd($admins);

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Danh sách thành viên ' . @$admins[0]->role_name,
            'meta_description' => 'Danh sách thành viên' . @$admins[0]->role_name,
            'meta_keywords' => 'Danh sách thành viên' . @$admins[0]->role_name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themesach100::pages.admin.list_by_role')->with($data);
    }
    public function listallMember() {
        $admins = Admin::leftJoin('role_admin', 'role_admin.admin_id', '=', 'admin.id')
            ->leftJoin('roles', 'roles.id', '=', 'role_admin.role_id')
            ->selectRaw('admin.id, admin.email, admin.name, admin.image, admin.tel, admin.intro, roles.display_name as role_name')->paginate(30);
        $data['admins'] = $admins;
//        dd($admins);

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => 'Danh sách thành viên ' . @$admins[0]->role_name,
            'meta_description' => 'Danh sách thành viên' . @$admins[0]->role_name,
            'meta_keywords' => 'Danh sách thành viên' . @$admins[0]->role_name,
        ];
        view()->share('pageOption', $pageOption);

        return view('themesach100::pages.admin.list_by_role')->with($data);
    }
}
