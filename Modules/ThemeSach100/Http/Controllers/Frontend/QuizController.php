<?php

namespace Modules\ThemeSach100\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use Modules\EduCourse\Models\Quizzes;
use Modules\ThemeSach100\Models\Menus;
use Modules\ThemeSach100\Models\QuizLog;
use Modules\ThemeSach100\Models\Student;

class QuizController extends Controller
{
//      Trang chi tiết bài kiểm tra
    public function detail($slug1, $slug)
    {
        //  Dang nhap truoc khi lam bai KT
        $data['user'] = \Auth::guard('student')->user();
        if (!\Auth::guard('student')->check()) {
            return redirect('/dang-nhap');
        }
        $slug = str_replace('.html', '', $slug);

        $data['quiz'] = CommonHelper::getFromCache('quizzes_slug' . $slug, ['quizzes']);
        if (!$data['quiz']) {
            $data['quiz'] = Quizzes::where('slug', $slug)->first();
            CommonHelper::putToCache('quizzes_slug' . $slug, $data['quiz'], ['quizzes']);
        }

//        $data['slug1'] = $slug1;
        if (!is_object($data['quiz'])) {
            abort(404);
        }

        //  Kiểm tra HV đã thi bài này chưa - Nếu thi rồi thì update điểm
        $quiz_log = CommonHelper::getFromCache('quizz_log_quizz_id' . $data['user']->id . $data['quiz']->id, ['quiz_log']);
        if (!$quiz_log) {
            $quiz_log = QuizLog::where('student_id', $data['user']->id)->where('quizz_id', $data['quiz']->id)->first();
            CommonHelper::putToCache('quizz_log_quizz_id' . $data['user']->id . $data['quiz']->id, $quiz_log, ['quiz_log']);
        }

        if (is_object($quiz_log)) {
            $data['msg'] = 'Bạn đã làm bài kiểm tra này trước đó. Vì vậy sẽ không tính điểm tích lũy cho lần làm bài này.';
            if ($quiz_log->synch == 0) {    //  Gọi sang gg biểu mẫu để update điểm số
                $this->updateScores($quiz_log);
            }

            if ($quiz_log->scores != null) {
                $data['msg'] .= ' Điểm số đã ghi nhận ' . $quiz_log->scores;
            }
        } else {
            $data['msg'] = 'Điểm tích lũy sẽ chỉ được tính cho lần làm bài đầu tiên này. Vui lòng nhập đúng email tài khoản của bạn để tính điểm tích lũy!';
            //  Tạo lịch sử thi
            QuizLog::create([
                'student_id' => $data['user']->id,
                'quizz_id' => $data['quiz']->id
            ]);
        }

        //  Thẻ meta cho seo
        $pageOption = [
            'meta_title' => $data['quiz']->meta_title != '' ? $data['quiz']->meta_title : $data['quiz']->name,
            'meta_description' => $data['quiz']->meta_description != '' ? $data['quiz']->meta_description : $data['quiz']->name,
            'meta_keywords' => $data['quiz']->meta_keywords != '' ? $data['quiz']->meta_keywords : $data['quiz']->name,
        ];
        view()->share('pageOption', $pageOption);
        CommonHelper::flushCache(['quiz_log', 'quizzes']);
        return view('themesach100::pages.quiz.quiz_detail', $data);
    }

    /*
     * Update điểm số từ gg biểu mẫu về server & cộng điểm tích luy cho student
     * */
    public function updateScores($quiz_log)
    {
        try {
            $quizz = Quizzes::find($quiz_log->quizz_id);
            $student_mail = Student::find($quiz_log->student_id)->email;
            if ($quizz->google_form_answer_id != null && $quiz_log->synch == 0) {
                //  Gọi sang gg driver lấy data file bảng điểm
                $service = \Storage::cloud()->getAdapter()->getService();
                $mimeType = 'text/csv';
//            dd($quizz->google_form_answer_id);
                $export = $service->files->export($quizz->google_form_answer_id, $mimeType);
                $response = response($export->getBody(), 200, $export->getHeaders());
                $data = preg_split("/\\r\\n|\\r|\\n/", $response->getContent());
                $title = explode(',', $data[0]);
                $key_mail_position = array_keys($title, 'Địa chỉ email')[0];
                $key_scores_position = array_keys($title, 'Điểm số')[0];

                foreach ($data as $k => $row) {
                    if ($k != 0) {
                        $vals = explode(',', $row);
                        $email = $vals[$key_mail_position];
                        if ($email == $student_mail) {
                            $quiz_log->scores = $vals[$key_scores_position];
                            $quiz_log->accumulated_points = 0;
                            $quiz_log->data = json_encode($vals);

                            //  Tinh diem tich luy
                            $accumulated_points = (int)$quizz->accumulated_points;
                            $str = explode('/', $quiz_log->scores);
                            if (isset($str[0]) && isset($str[1])) {
                                $ps = (int)$str[0] / (int)$str[1];
                                $quiz_log->accumulated_points = (int)($ps * $accumulated_points);
                            }
                            $quiz_log->synch = 1;
                            $quiz_log->save();
                            return $quiz_log;
                        }
                    }
                }
            }

            $quiz_log->synch = 1;
            $quiz_log->save();
            CommonHelper::flushCache(['student', 'quizzes']);
            return $quiz_log;
        } catch (\Exception $ex) {
            return false;
        }
    }
}
