<?php

namespace Modules\ThemeSach100\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class EduThemeController extends CURDBaseController
{
    protected $module = [
        'code' => 'setting',
        'label' => 'Cấu hình theme',
        'table_name' => 'settings',
        'modal' => '\App\Models\Setting',
        'tabs' => [
            /*
            'social_tab' => [
                'label' => 'Mạng xã hội',
                'icon' => '<i class="kt-menu__link-icon flaticon-settings-1"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'fanpage', 'type' => 'text', 'label' => 'Fanpage FB', 'des' => 'Fanpage Facebook'],
                    ['name' => 'google_map', 'type' => 'text', 'label' => 'google_map', 'des' => 'Fanpage Facebook'],
                    ['name' => 'skype', 'type' => 'text', 'label' => 'skype', 'des' => 'Fanpage Facebook'],
                    ['name' => 'instagram', 'type' => 'text', 'label' => 'instagram', 'des' => 'Fanpage Facebook'],
                ]
            ],*/

            'common_tab' => [
                'label' => 'Chung',
                'icon' => '<i class="kt-menu__link-icon flaticon2-layers-2"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'logo-top', 'type' => 'file_editor', 'label' => 'Logo top'],
                    ['name' =>'frontend_head_code','type' =>'textarea','label' =>'admin.insert_code','inner' =>'rows=20'],
                    ['name' =>'frontend_body_code','type' =>'textarea','label' =>'Chèn code vào <body>','inner' =>'rows=20'],
                    ['name' => 'link_chat_fanpage_facebook', 'type' => 'text', 'label' => 'Link chat fanpage FB', 'des' => 'Đường link chat với fanpage facebook. VD: https://www.messenger.com/t/hobasoftcom'],
                    ['name' => 'break_time', 'type' => 'file_editor', 'label' => 'Ảnh thời gian nghỉ'],
                    ['name' => 'intermediaries', 'type' => 'file_editor', 'label' => 'Ảnh trung gian'],
                ],
            ],
            'homepage_tab' => [
                'label' => 'Trang chủ',
                'icon' => '<i class="kt-menu__link-icon flaticon2-open-box"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'banner_slides_type', 'type' => 'select', 'label' => 'Kiểu slide', 'options' => [
                        0 => '1 ảnh - không hiệu ứng',
                        1 => 'Hiệu ứng'
                    ]],
                ]
            ],

            'course_page_tab' => [
                'label' => 'Trang khóa học',
                'icon' => '<i class="kt-menu__link-icon flaticon2-open-box"></i>',
                'intro' => '',
                'td' => [

                ]
            ],
            'answer_page_tab' => [
                'label' => 'Trang đáp án',
                'icon' => '<i class="kt-menu__link-icon flaticon2-open-box"></i>',
                'intro' => '',
                'td' => [
                    ['name' => 'btn_1', 'type' => 'text', 'label' => 'Link từ vựng', 'des' => 'Đường link đáp án từ vựng. VD: https://jlpttest.vn/'],
                    ['name' => 'btn_2', 'type' => 'text', 'label' => 'Link nghe hiểu', 'des' => 'Đường link đáp án nghe hiểu. VD: https://jlpttest.vn/'],
                    ['name' => 'btn_3', 'type' => 'text', 'label' => 'Link đọc hiểu', 'des' => 'Đường link đọc hiểu. VD: https://jlpttest.vn/'],

                ]
            ],
        ]
    ];

    public function setting(Request $request)
    {

        $data['page_type'] = 'list';

        $module = \Eventy::filter('theme.custom_module', $this->module);
        if (!$_POST) {
            $listItem = $this->model->get();
            $tabs = [];
            foreach ($listItem as $item) {
                $tabs[$item->type][$item->name] = $item->value;
            }
            #
            $data['tabs'] = $tabs;
            $data['page_title'] = $module['label'];
            $data['module'] = \Eventy::filter('theme.custom_module', $module);
            return view(config('core.admin_theme') . '.setting.view')->with($data);
        } else {
            foreach ($module['tabs'] as $type => $tab) {
                $data = $this->processingValueInFields($request, $tab['td'], $type . '_');

                //  Tùy chỉnh dữ liệu insert

                foreach ($data as $key => $value) {
                    $item = Setting::where('name', $key)->where('type', $type)->first();
                    if (!is_object($item)) {
                        $item = new Setting();
                        $item->name = $key;
                        $item->type = $type;
                    }
                    $item->value = $value;
                    $item->save();
                }
            }

            if (Schema::hasTable('admin_logs')) {
                $this->adminLog($request, $item = false, 'settingTheme');
            }

            CommonHelper::flushCache($this->module['table_name']);
            CommonHelper::one_time_message('success', 'Cập nhật thành công!');

            if ($request->return_direct == 'save_exit') {
                return redirect('admin/dashboard');
            }

            return back();
        }
    }
}
