<?php echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

    <?php $data = \Modules\ThemeSach100\Models\Category::select(['slug', 'updated_at'])->whereIn('type', [1, 10])->where('status', 1)->where('slug', '!=', '')->get();?>
    @foreach($data as $item)
        <url>
            <loc>{{ URL::to($item->slug) }}</loc>
            <lastmod>{{ date("Y-m-d", strtotime($item->updated_at)) }}T{{ date("H:i:s", strtotime($item->updated_at))}}+07:00</lastmod>
            <changefreq>always</changefreq>
            <priority>0.4</priority>
        </url>
    @endforeach

</urlset>

