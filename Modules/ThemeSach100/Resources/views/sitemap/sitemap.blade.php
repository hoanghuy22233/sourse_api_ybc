<?php echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd">

    {{--Sitemap cho danh muc--}}
    <sitemap>
        <loc>{{ URL::to('/category-sitemap.xml') }}</loc>
        <lastmod>{{ date("Y-m-")}}01T08:00:00+07:00</lastmod>
    </sitemap>

    {{--Sitemap cho tin tuc--}}
    <sitemap>
        <loc>{{ URL::to('/post-sitemap.xml') }}</loc>
        <lastmod>{{ date("Y-m-")}}01T08:00:00+07:00</lastmod>
    </sitemap>

    {{--Sitemap cho san pham--}}
    {{--<sitemap>
        <loc>{{ URL::to('/test-sitemap.xml') }}</loc>
        <lastmod>{{ date("Y-m-")}}01T09:00:00+07:00</lastmod>
    </sitemap>--}}
</sitemapindex>