<div class="responsive-header">
    <div class="mh-head first mm-sticky mh-btns-left mh-btns-right mh-sticky" style="">
			<span class="mh-btns-left">
				<a class="" href="#menu"><i class="fa fa-align-justify"></i></a>
			</span>
        <span class="mh-text">
				<a href="/" title=""><img class="lazy"
                            data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo-top'], null, 95) }}"
                            alt="{{ @$settings['name'] }}"></a>
			</span>
        <span class="mh-btns-right">
				<a class="fa fa-sliders" href="#shoppingbag"></a>
			</span>
    </div>
    <div class="mh-head second">
        <form class="mh-form">
            <input placeholder="Tìm kiếm">
            <a href="#/" class="fa fa-search"></a>
        </form>
    </div>
</div>

{{--menu--}}

{{--end-menu--}}
<div class="topbar stick">

    <div class="logo">
        <a title="" href="/"><img class="lazy" data-src="{{ asset('public/filemanager/userfiles/' . @$settings['logo-top']) }}"
                                  style="    max-height: 45px;"
                                  alt="{{ @$settings['name'] }}"></a>
    </div>
    <div class="top-area">
        <div class="top-search">
            <form action="/tim-kiem" method="get" class="">
                <input type="search" name="q" class="form-control" value="{{@$_GET['q']}}"
                       placeholder="Tìm kiếm khóa học">
                <button data-ripple=""><i class="ti-search"></i></button>
            </form>
        </div>
        @if(\Auth::guard('student')->check())
            @include('themesach100::partials.user_image')
            <ul class="setting-area">

                <li>
                    <a href="#" title="Notification" data-ripple="">
                        <i class="fa fa-bell"></i><em class="bg-purple">0</em>
                        <span class="ripple"><span class="ink"
                                                   style="height: 58px; width: 58px; background-color: rgb(94, 126, 167); top: -8px; left: -10.0156px;"></span></span></a>
                    <div class="dropdowns ps-container ps-theme-default ps-active-y"
                         data-ps-id="52ace122-43d1-fd2f-1984-083535bd0b53">
                        <span>0 Có thông báo mới <a href="#" title="">Đánh dấu tất cả như đã đọc</a></span>
                        <ul class="drops-menu">
                            {{--<li>--}}
                            {{--<a href="notifications.html" title="">--}}
                            {{--<figure>--}}
                            {{--<img src="images/resources/thumb-1.jpg" alt="">--}}
                            {{--<span class="status f-online"></span>--}}
                            {{--</figure>--}}
                            {{--<div class="mesg-meta">--}}
                            {{--<h6>sarah Loren</h6>--}}
                            {{--<span>commented on your new profile status</span>--}}
                            {{--<i>2 min ago</i>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                            </li>
                            <li>
                                {{--<a href="notifications.html" title="">--}}
                                {{--<figure>--}}
                                {{--<img src="images/resources/thumb-2.jpg" alt="">--}}
                                {{--<span class="status f-online"></span>--}}
                                {{--</figure>--}}
                                {{--<div class="mesg-meta">--}}
                                {{--<h6>Jhon doe</h6>--}}
                                {{--<span>Nicholas Grissom just became friends. Write on his wall.</span>--}}
                                {{--<i>4 hours ago</i>--}}
                                {{--<figure>--}}
                                {{--<span>Today is Marina Valentine’s Birthday! wish for celebrating</span>--}}
                                {{--<img src="images/birthday.png" alt="">--}}
                                {{--</figure>--}}
                                {{--</div>--}}
                                {{--</a>--}}
                            </li>
                            <li>
                                {{--<a href="notifications.html" title="">--}}
                                {{--<figure>--}}
                                {{--<img src="images/resources/thumb-3.jpg" alt="">--}}
                                {{--<span class="status f-online"></span>--}}
                                {{--</figure>--}}
                                {{--<div class="mesg-meta">--}}
                                {{--<h6>Andrew</h6>--}}
                                {{--<span>commented on your photo.</span>--}}
                                {{--<i>Sunday</i>--}}
                                {{--<figure>--}}
                                {{--<span>"Celebrity looks Beautiful in that outfit! We should see each"</span>--}}
                                {{--<img src="images/resources/admin.jpg" alt="">--}}
                                {{--</figure>--}}
                                {{--</div>--}}
                                {{--</a>--}}
                            </li>
                            <li>
                                {{--<a href="notifications.html" title="">--}}
                                {{--<figure>--}}
                                {{--<img src="images/resources/thumb-4.jpg" alt="">--}}
                                {{--<span class="status f-online"></span>--}}
                                {{--</figure>--}}
                                {{--<div class="mesg-meta">--}}
                                {{--<h6>Tom cruse</h6>--}}
                                {{--<span>nvited you to attend to his event Goo in</span>--}}
                                {{--<i>May 19</i>--}}
                                {{--</div>--}}
                                {{--</a>--}}
                                {{--<span class="tag">New</span>--}}
                            </li>
                            <li>
                                {{--<a href="notifications.html" title="">--}}
                                {{--<figure>--}}
                                {{--<img src="images/resources/thumb-5.jpg" alt="">--}}
                                {{--<span class="status f-online"></span>--}}
                                {{--</figure>--}}
                                {{--<div class="mesg-meta">--}}
                                {{--<h6>Amy</h6>--}}
                                {{--<span>Andrew Changed his profile picture. </span>--}}
                                {{--<i>dec 18</i>--}}
                                {{--</div>--}}
                                {{--</a>--}}
                                {{--<span class="tag">New</span>--}}
                            </li>
                        </ul>
                        <a href="/" title="" class="more-mesg">Xem tất cả</a>
                        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                            <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                        </div>
                        <div class="ps-scrollbar-y-rail" style="top: 0px; height: 340px; right: 0px;">
                            <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 266px;"></div>
                        </div>
                    </div>
                </li>


            </ul>
        @else
            <div class="user-img">
                <h5><a href="/dang-nhap">Đăng nhập</a></h5>
            </div>
        @endif
        <div class="shopping_cart">
            <a href="/gio-hang">
                <i class="fas fa-shopping-cart"></i>
                <sup class="cart_total_item">{{Cart::count()}}</sup>
            </a>

        </div>

        <span class="" data-ripple=""></span>
    </div>
</div>