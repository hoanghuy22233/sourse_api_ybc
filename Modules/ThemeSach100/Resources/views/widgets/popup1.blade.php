@if(\Session::get('show_popup1') == null)
    <style>
        #popup1 img {
            max-height: 570px;
        }
        .modal-content{
            background: transparent;
            box-shadow: none;
            border: none;
        }

        button.close{
            width: 25px;
            height: 25px;
            position: absolute;
            top: -13px;
            right: -13px;
            z-index: 2;
             background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAMAAAAolt3jAAAAIVBMVEX///////////////////////////////////////////9/gMdvAAAACnRSTlMAERIThofMzc7PkNIUVAAAAE9JREFUeNpFz1kOwCAIBNBhsVTuf+BaHJEfeAlhAcwFFeK29GZI6clpGJlliVUMaPxW2Qn0Eduo7RZncl6LpiJoblDaS32GzbtvGmCu94UPiUQDv+BZIsEAAAAASUVORK5CYII=);
            background-repeat: no-repeat;
            background-position: center;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            background-color: #000;
            outline: none;
            border: 1px solid #000;
        }
        .modal-header{
            border-bottom:none;
        }
        .close{
            opacity: revert;
            font-size: 0px;

        }
        @media(max-width: 768px) {
            #popup1 img {
                width: unset !important;
                height: unset !important;
            }
        }

        #popup1 .modal-dialog {
            top: 10%;
        }

        #popup1 .modal-content,
        #popup1 .modal-header,
        #popup1 .modal-body {
            display: inline-block;
        }

        #popup1 .modal-header .close {
            right: 15px;
            top: 15px;
            opacity: .5;
        }
    </style>

    <div class="modal fade " id="popup1" style=" padding-right: 15px;" aria-modal="true">
        <div class="modal-dialog">
            <div class="modal-content">
                @if($widget->name != '')
                    <div class="modal-header">
                        {{--<h4 class="modal-title">{!! $widget->name !!}</h4>--}}
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        {!! $widget->content !!}
                    </div>

                    <!-- Modal footer -->
                    {{--<div class="modal-footer">--}}
                        {{--<button type="button" class="btn btn-danger close" data-dismiss="modal">Đóng</button>--}}
                    {{--</div>--}}
                @else
                    <button type="button" class="close" data-dismiss="modal"></button>
                    {!! $widget->content !!}
                @endif
            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function () {
            @if (isset($config['time']))
                myVar = setInterval(function () {
                jQuery('#popup1').removeClass('fade').show();
                clearTimeout(myVar);
            }, {{ $config['time'] }});
            @else
                myVar = setInterval(function () {
                jQuery('#popup1').removeClass('fade').show();
                clearTimeout(myVar);
            }, 5000);
            @endif

            jQuery('#popup1 .close').click(function () {
                jQuery('#popup1').addClass('fade').hide();
            });
        });

        jQuery(document).mouseup(function(e)
        {
            var container = jQuery("#popup1 .modal-content");

            // if the target of the click isn't the container nor a descendant of the container
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                jQuery('#popup1').addClass('fade').hide();
            }
        });
    </script>
    <?php
    \Session::put('show_popup1', true);
    ?>
@endif