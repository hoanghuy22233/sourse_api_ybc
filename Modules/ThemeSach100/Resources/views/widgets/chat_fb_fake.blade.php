<!--CHAT FACEBOOK-->
<style>
    /*Css Chat facebook */
    .fb-show-box {
        position: fixed;
        background: #fff;
        right: 50px;
        max-width: 360px;
        width: 360px;
        border-radius: 10px;
        bottom: 100px;
        overflow: hidden;
        box-shadow: 0 1pt 12pt rgba(0, 0, 0, .15);
        z-index: 9999;
        display: none;
        min-height: 210px;
        background: #fff;
    }

    .fb-show-box-body {
        float: left;
        width: 100%;
        background: rgba(0, 0, 0, .03);
    }

    .fb-show-box .close {
        position: absolute;
        right: 10px;
        top: 5px;
        font-size: 24px;
        text-align: center;
        font-weight: 500;
        align-items: center;
        background-color: rgba(0, 0, 0, .1);
        border-radius: 50%;
        cursor: pointer;
        height: 24px;
        line-height: 5px;
        padding-top: 7px;
        box-sizing: border-box;
        width: 24px;
    }

    .fb-show-box-row1 {
        display: inline-flex;
        flex-wrap: wrap;
        padding: 10px 10px;
        align-items: center;
        float: left;
        width: 100%;
    }

    .fb-row-1, .fb-row-2 {
        min-height: 50px;
    }

    .fb-gif {
        background: url(https://hobasoft.com/wp-content/themes/generatepress_child/images/loading.gif);
        background-repeat: no-repeat;
        background-position: center;
        background-size: 141px;
        height: 20px;
        width: 100%;
        float: left;
        clear: both;
    }

    .fb-show-box-img {
        float: left;
        width: 13%;
        text-align: left;
    }

    .fb-show-box-img img {
        border-radius: 50%;
        width: 35px;
    }

    .fb-show-box-detail {
        float: left;
        width: 87%;
    }

    .fb-show-box-body {
        color: rgba(0, 0, 0, .50);
        font-size: 11px;
        margin: 0;
        float: right;
        width: 100%;
        padding: 5px 0px;
        font-family: SF Pro Text, Helvetica Neue, Segoe UI, Helvetica, Arial, sans-serif;
        font-weight: 500;
        text-align: left;
    }

    .fb-show-box-detail h6, .fb-show-box-body h6 {
        color: rgba(0, 0, 0, .50);
        font-size: 11px;
        float: left;
        padding: 5px 0px;
        font-weight: 500;
        text-align: left;
        margin: 0;
        margin-left: 15px;
    }

    .fb-show-box-desc {
        line-height: 16px;
        max-width: 261px;
        background: #f1f0f0;
        padding: 8px 10px;
        border-radius: 10px;
        color: #000;
        font-size: 14px;
        text-align: left;
        word-wrap: break-word;
        font-weight: 400;
        float: left;
        width: 95%;
    }

    .fb-show-box-row2 {
        padding: 10px 20px;
        text-align: center;
        float: left;
        width: 100%;
        background: #fff;
    }

    .fb-show-box-row2 a {
        padding: 6px 30px;
        border-radius: 10px;
        border: 1px solid #0084ff;
        background: #0084ff;
        color: #fff;
        font-size: 14px;
        cursor: pointer;
        font-family: open sans;
        display: inline-block;
    }

    .fb-show-box-row2 a:hover {
        background: #0375e0;
    }

    .fb-show-box-row2 a::before {
        content: "";
        display: block;
        background-image: url("https://hobasoft.com/wp-content/themes/generatepress_child/images/icon_face.png");
        float: left;
        background-repeat: no-repeat;
        background-position: -25px -146px;
        margin-right: 5px;
        width: 16px;
        height: 16px;
        margin-top: 2px;
    }

    #fb-chat-app-1 {
        position: fixed;
        bottom: 40px;
        right: 15px;
        width: 53px;
        cursor: pointer;
        height: 53px;
        z-index: 999999;
    }

    .fb-row-1 img, .fb-row-1 .fb-show-box-desc, .fb-row-1 .fb-gif, .fb-row-2 .fb-gif, .fb-row-2 img, .fb-row-2 .fb-show-box-desc {
        display: none;
    }

    .fb-show-box-row2 {
        display: none;
    }

    @media only screen and (max-width: 480px) {
        .fb-show-box {
            width: 96%;
            right: 7px;
        }

        .fb-show-box-desc {
            font-size: 12px;
        }

        #fb-chat-app-1 {
            bottom: 15px;
        }

        .fb-show-box {
            bottom: 85px;
            width: 290px;
        }

        .fb-show-box-desc {
            font-size: 12px;
        }
    }

</style>

<?php
$msg = preg_split('/\n|\r\n?/', $widget->content);
foreach ($msg as $k => $v) {
    if ($v == '') {
        unset($msg[$k]);
    }
}
?>
<div class="fb-show-box">
    <span class="close">&times;</span>
    <div class="fb-show-box-body">
        <h6>Phản hồi trong vòng một phút</h6>
        @foreach($msg as $k => $v)
            <div class="fb-show-box-row1 fb-row-{{ $k }}">
                <div class="fb-show-box-img">
                    <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($settings['favicon'], null, 16) }}"
                         alt="logo">
                </div>
                <div class="fb-show-box-detail">
                    <div class="fb-show-box-desc">
                        {!! str_replace('</p>', '', $v) !!}
                    </div>
                </div>
            </div>
        @endforeach
        <div class="fb-gif"></div>
        <div class="fb-show-box-row2">
            <a href="{{ @$config['link_fb'] }}" onclick="bigDataCreateAction('openchatmobile')" target="blank">Click để
                chat!</a>
            <h6>Kết nối với {{ @$settings['name'] }} trong Messenger</h6>
        </div>
    </div>
</div>
<a id="fb-chat-app-1" target="blank">
    <svg width="60px" height="60px" viewBox="0 0 60 60">
        <svg x="0" y="0" width="60px" height="60px">
            <defs>
                <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="linearGradient-1">
                    <stop stop-color="#00B2FF" offset="0%"></stop>
                    <stop stop-color="#006AFF" offset="100%"></stop>
                </linearGradient>
            </defs>
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g>
                    <circle fill="#FFFFFF" cx="30" cy="30" r="30"></circle>
                    <svg x="10" y="10">
                        <g>
                            <rect id="container" x="0" y="0" width="40" height="40"></rect>
                            <g id="logo">
                                <path d="M20,0 C8.7334,0 0,8.2528 0,19.4 C0,25.2307 2.3896,30.2691 6.2811,33.7492 C6.6078,34.0414 6.805,34.4513 6.8184,34.8894 L6.9273,38.4474 C6.9621,39.5819 8.1343,40.3205 9.1727,39.8621 L13.1424,38.1098 C13.4789,37.9612 13.856,37.9335 14.2106,38.0311 C16.0348,38.5327 17.9763,38.8 20,38.8 C31.2666,38.8 40,30.5472 40,19.4 C40,8.2528 31.2666,0 20,0"
                                      id="bubble" fill="url(#linearGradient-1)"></path>
                                <path d="M7.99009,25.07344 L13.86509,15.75264 C14.79959,14.26984 16.80079,13.90064 18.20299,14.95224 L22.87569,18.45674 C23.30439,18.77834 23.89429,18.77664 24.32119,18.45264 L30.63189,13.66324 C31.47419,13.02404 32.57369,14.03204 32.00999,14.92654 L26.13499,24.24744 C25.20039,25.73014 23.19919,26.09944 21.79709,25.04774 L17.12429,21.54314 C16.69559,21.22164 16.10569,21.22334 15.67879,21.54734 L9.36809,26.33674 C8.52579,26.97594 7.42629,25.96794 7.99009,25.07344"
                                      id="bolt" fill="#FFFFFF"></path>
                            </g>
                        </g>
                    </svg>
                </g>
            </g>
        </svg>
    </svg>
</a>
<script>
    jQuery(document).ready(function () {
        var audio = new Audio("https://hobasoft.com/wp-content/themes/generatepress_child/audio/ting_ting.mp3");

        function showChatFacebook() {
            // Hiển thị box chat facebook sau 4s load trang
            setTimeout(function () {
                jQuery('.fb-show-box').fadeToggle('400');
                jQuery('.fb-show-box .fb-gif').fadeIn(); // chạy ảnh gif load chat text
            }, 500);

            <?php $k = 0;?>
            @foreach($msg as $v)
            <?php $k ++ ;?>
            // Hiển thị dòng chát 1 sau 5s từ khi load trang
            setTimeout(function () {
                jQuery('.fb-show-box .fb-gif').fadeOut();
                jQuery('.fb-row-{{ $k }} img').fadeIn('400');
                jQuery('.fb-row-{{ $k }} .fb-show-box-desc').fadeIn('500');
                @if($k < count($msg))
                jQuery('.fb-show-box .fb-gif').fadeIn();
                @endif
                // audio.play();
            }, 0 + (parseInt({{ $k }}) * 1000));
            @endforeach

            // Hiển thị nút click để chat
            setTimeout(function () {
                jQuery('.fb-show-box-row2').fadeIn('400');
            }, 0);
        }

        @if(@$config['auto_open'] != '0')
            showChatFacebook();
        @endif






        // Sự kiện click biểu tượng messenger facebook.
        jQuery('#fb-chat-app-1').on('click', function () {
            // jQuery('.fb-show-box').fadeToggle('400');

            showChatFacebook();
        });

        jQuery('.fb-show-box .close').on('click', function () {
            jQuery('.fb-show-box').fadeToggle('400');

            // showChatFacebook();
        });
    });
</script>
<!--END: CHAT FACEBOOK-->