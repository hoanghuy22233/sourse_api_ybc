<style>
    .back-to-top {
        background: url(/public/frontend/themes/sach100/themes/smartowl/images/mt-to-top-arrow.svg) no-repeat center center;
        opacity: .5;
        background-color: #393939;
        display: inline-block;
        height: 40px;
        width: 40px;
        position: fixed;
        bottom: 40px;
        right: 40px;
        overflow: hidden;
        text-indent: 100%;
        white-space: nowrap;
        visibility: visible;
    }
</style>
<a class="back-to-top" href="javascript:;" style="display:none;">
    <span></span>
</a>
<script>
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop()) {
            jQuery('.back-to-top').fadeIn();
        } else {
            jQuery('.back-to-top').fadeOut();
        }
    });
    jQuery(".back-to-top").click(function () {
        jQuery("html, body").animate({scrollTop: 0}, 1000);
    });
</script>