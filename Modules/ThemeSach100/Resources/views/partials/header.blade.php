<!-- TOP HEADER -->
<div class="top-header">
    <div class="vc_container">
        <div class="vc_row">
            <div class="vc_col-md-6 vc_col-sm-6 text-left account-urls first">
                {{--Hiển thị khối widget--}}
                <?php
                $widgets = CommonHelper::getFromCache('widgets_top_bar', ['widgets']);
                if (!$widgets) {
                    $widgets = \Modules\ThemeEdu\Models\Widget::where('location', 'top_bar')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
                    CommonHelper::putToCache('widgets_top_bar', $widgets, ['widgets']);
                }
                ?>
                @foreach($widgets as $widget)
                    @if($widget->type == 'html')
                        {!!$widget->content !!}

                    @else
                        @include(@json_decode($widget->config)->view, ['config' => (array) json_decode($widget->config)])
                    @endif
                @endforeach
                {{--END: Hiển thị khối widget--}}
            </div>
            <div class="vc_col-md-6 vc_col-sm-6 text-right account-urls first">
                @if(\Auth::guard('student')->check())
                    <a class="modeltheme-trigger" href="/profile" title="Xem profile"
                       data-modal="modal-log-in"><i style="font-family: 'simple-line-icons' !important;"
                                                    class="icon-user"></i> Xin
                        chào, {{ @Auth::guard('student')->user()->name }}</a>
                    <a class="modeltheme-trigger" href="/dang-xuat"
                       data-modal="modal-log-in"><i style="font-family: 'simple-line-icons' !important;"
                                                    class="icon-logout"></i> Đăng xuất</a>
                @else
                    <a href="/dang-ky" data-modal="modal-16"><i
                                class="fa fa-pencil"></i> Đăng ký</a>
                    <a class="modeltheme-trigger" href="/dang-nhap" data-modal="modal-log-in"><i
                                style="font-family: 'simple-line-icons' !important;"
                                class="icon-login"></i> Đăng nhập</a>
                @endif
            </div>
        </div>
    </div>
</div>


<!-- SMALL GRADIENT BAR -->
<div class="colored-fullwidth-gradient vc_row"></div>


<!-- BOTTOM BAR -->
<nav class="navbar navbar-default" id="modeltheme-main-head">
    <div class="vc_container">
        <div class="vc_row">
            <div class="navbar-header col-md-2">


                <h1 class="logo">
                    <a href="/">
                        <img class="theme-logo"
                             src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['logo'], null, 60) }}"
                             alt="{{ @$settings['name'] }}" width="60px"/> </a>
                </h1>
            </div>
            <div id="navbar" class="navbar-collapse collapse in col-md-10">
                <div class="menu nav navbar-nav pull-right nav-effect nav-menu">

                    <!-- begin "mega_main_menu" -->
                    <div id="mega_main_menu"
                         class="primary primary_style-flat icons-left first-lvl-align-left first-lvl-separator-none direction-horizontal fullwidth-enable pushing_content-enable mobile_minimized-enable dropdowns_trigger-hover dropdowns_animation-anim_4 no-logo no-search no-woo_cart no-buddypress responsive-enable coercive_styles-enable indefinite_location_mode-disable language_direction-ltr version-2-1-4 mega_main mega_main_menu">
                        <div class="menu_holder">
                            <div class="mmm_fullwidth_container"></div>
                            <!-- class="fullwidth_container" -->
                            <div class="menu_inner">
                                <span class="nav_logo">
                                    <a class="mobile_toggle">
                                        <span class="mobile_button">
                                            <span class="symbol_menu">&equiv;</span>
                                            <span class="symbol_cross">&#x2573;</span>
                                        </span>
                                    </a>
                                </span>
                                <style>
                                    #navbar .menu-item .sub-menu {
                                        position: absolute !important;
                                    }

                                    #mega_main_menu > .menu_holder > .menu_inner > ul > li .item_link {
                                        padding: 0px 12px !important;
                                    }
                                    @media(max-width: 768px) {
                                        ul#mega_main_menu_ul {
                                            display: block !important;
                                            min-width: 222px;
                                            right: 0px !important;
                                            position: fixed !important;
                                            background: #fff !important;
                                        }
                                        #mega_main_menu.responsive-enable>.menu_holder>.menu_inner>ul>li {
                                            padding: 0 !important;
                                        }
                                        .search_products {
                                            width: 100% !important;
                                            margin: 0 !important;
                                        }
                                        .search_products a {
                                            margin: 0 !important;
                                            width: 100% !important;
                                            border-radius: 0 !important;
                                        }
                                        #mega_main_menu.responsive-enable.mobile_minimized-enable>.menu_holder>.menu_inner>ul.menu-show {
                                            display: block !important;
                                            position: fixed !important;
                                            background: #fff !important;
                                            min-width: 250px;
                                            right: 0 !important;
                                        }
                                        #navbar .menu-item .sub-menu .sub-menu {
                                            left: 15px;
                                            top: 78px !important;
                                        }

                                        /*Menu toogle*/
                                        /*
                                        ul#mega_main_menu_ul li.menu-item-has-children a {
                                            width: 80%;
                                            display: inline-block;
                                        }
                                        ul#mega_main_menu_ul li.menu-item-has-children:after {
                                            content: '';
                                            border-top: 2px solid transparent;
                                            border-left: 2px solid transparent;
                                            border-bottom: 0;
                                            display: block;
                                            width: 8px;
                                            height: 8px;
                                            margin: auto;
                                            position: absolute;
                                            top: 0;
                                            bottom: 0;
                                            border-color: rgba(0,0,0,.3);
                                            transform: rotate(135deg);
                                            right: 23px;
                                            left: auto;
                                            z-index: 999999;
                                        }
                                        .mmm #mega_main_menu.primary .menu-item {
                                            padding: 0 !important;
                                        }
                                        #navbar .menu-item .sub-menu .sub-menu {
                                            left: 15px;
                                            top: 0;
                                        }
                                        #navbar ul.sub-menu li a {
                                            color: #000;
                                        }
                                        ul#mega_main_menu_ul > li.menu-item-has-children .sub-menu {
                                            position: relative !important;
                                            opacity: 1;
                                            visibility: visible;
                                            background: #fff;
                                        }*/
                                    }
                                </style>
                                <script>
                                    jQuery('a.mobile_toggle').click(function () {
                                        jQuery('#mega_main_menu_ul').toggleClass('menu-show');


                                        jQuery('ul#mega_main_menu_ul > li.menu-item-has-children').click(function () {
                                            jQuery(this).find('ul.sub-menu:first').slideToggle(100);
                                        });


                                        jQuery('ul#mega_main_menu_ul li.menu-item-has-children > a').click(function () {
                                            jQuery(this).find('ul.sub-menu:first').slideToggle(100);
                                            return false;
                                        });
                                    });
                                </script>
                                <ul id="mega_main_menu_ul" class="mega_main_menu_ul">
                                    <?php
                                    $menus = \App\Http\Helpers\CommonHelper::getFromCache('menuts_main_menu', ['widgets']);
                                    if (!$menus) {
                                        $menus = \Modules\ThemeSach100\Helpers\ThemeSach100Helper::getMenusByLocation('main_menu');
                                        \App\Http\Helpers\CommonHelper::putToCache('menuts_main_menu', $menus, ['widgets']);
                                    }
                                    ?>
                                    @foreach($menus as $k => $menu)
                                        <li id="menu-item-3282"
                                            class="menu-item menu-item-type-custom menu-item-object-custom
                                             @if(!empty($menu['childs'])) menu-item-has-children @endif
                                            {{ $k == (count($menus) - 1) ? 'last-child' : '' }}">
                                            <a href="{{ $menu['url'] }}" class="item_link  disable_icon"
                                               tabindex="1">
                                                <i class=""></i>
                                                <span class="link_content">
                                                    <span class="link_text">
                                                        {{ $menu['name'] }}
                                                    </span>
                                                </span>
                                            </a>

                                            @if(!empty($menu['childs']))
                                                <ul class="sub-menu">
                                                    @foreach($menu['childs'] as $child1)
                                                        <li id="menu-item-5819"
                                                            class="menu-item menu-item-type-post_type menu-item-object-page @if(!empty($child1['childs'])) menu-item-has-children @endif">
                                                            <a href="{{ $child1['url'] }}">{{ $child1['name'] }}</a>
                                                            @if(!empty($child1['childs']))
                                                                <ul class="sub-menu">
                                                                    @foreach($child1['childs'] as $child2)
                                                                        <li id="menu-item-5820"
                                                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5820">
                                                                            <a href="{{ $child2['url'] }}">{{ $child2['name'] }}</a>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            @endif
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                    <style>
                                        .search_products i.fa.fa-search {
                                            width: 173px !important;
                                        }

                                        .search_products i.fa.fa-search::after {
                                            content: "VÀO THI NGAY";
                                        }
                                    </style>
                                    <div class="search_products">
                                        <a href="/tao-bai-thi" style="background: #93beda;color: #fff; height: 37px; text-align: center;
                                            width: 131px; padding: 10px !important;margin-top: 29px; list-style-type:none; border-radius: 7px;
                                            ">VÀO THI NGAY</a>
                                    </div>
                                </ul>

                            </div>
                            <!-- /class="menu_inner" -->
                        </div>
                        <!-- /class="menu_holder" -->
                    </div>
                    <!-- /id="mega_main_menu" -->

                </div>

                <div class="header_mini_cart">
                    <div class="widget woocommerce widget_shopping_cart">
                        <h2 class="widgettitle">Cart</h2>
                        <div class="widget_shopping_cart_content"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
<script>
    jQuery(document).ready(function () {
        var obj = jQuery('#modeltheme-main-head');
        var top = obj.offset().top - parseFloat(obj.css('marginTop').replace(/auto/, 0));

        jQuery(window).scroll(function (event) {
            // what the y position of the scroll is
            var y = jQuery(this).scrollTop();

            // whether that's below the form
            if (y >= top) {
                // if so, ad the fixed class
                obj.addClass('fixed');
            } else {
                // otherwise remove it
                obj.removeClass('fixed');
            }
        });
    });

</script>