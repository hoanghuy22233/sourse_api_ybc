{{--<link rel='stylesheet' id='vc_animate-css-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/js_composer/assets/lib/bower/animate-css/animate.min.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='photoswipe-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/css/photoswipe/photoswipe.min.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
<link rel='stylesheet' id='photoswipe-default-skin-css'
      href="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/css/photoswipe/default-skin/default-skin.min.css')}}"
      type='text/css' media='all'/>
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js')}}"></script>--}}
{{--<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/wp-includes/js/comment-reply.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/modeltheme-framework/js/modeltheme-custom.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/modelthemes/js/modernizr.custom.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/modelthemes/js/classie.js')}}"></script>--}}
{{--<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/plugins/modelthemes/js/modelthemes-frontend-bar.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/js/frontend/woocommerce.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/yith-woocommerce-compare/assets/js/woocompare.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/yith-woocommerce-compare/assets/js/jquery.colorbox-min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/yith-woocommerce-quick-view/assets/js/frontend.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/wp-includes/js/jquery/jquery.form.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/jquery.ketchup.js')}}"></script>--}}
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/jquery.validation.js')}}"></script>
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/jquery.sticky.js')}}"></script>
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/uisearch.js')}}"></script>--}}
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/jquery.parallax.js')}}"></script>
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/jquery.appear.js')}}"></script>
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/jquery.countTo.js')}}"></script>
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/sidebarEffects.js')}}"></script>
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/owl.carousel.js')}}"></script>
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/modernizr.viewport.js')}}"></script>
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/animate.js')}}"></script>
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/jquery.countdown.js')}}"></script>
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/modalEffects.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/rippler.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/navigation.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/skip-link-focus-fix.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/loaders.css.js')}}"></script>--}}
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/google-maps-v3.js')}}"></script>
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/share-tooltip.js')}}"></script>
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/js/smartowl-custom.js')}}"></script>
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/mega_main_menu/src/js/frontend.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/wp-includes/js/wp-embed.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/js_composer/assets/js/dist/js_composer_front.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/js_composer/assets/lib/vc_waypoints/vc-waypoints.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/wp-includes/js/wp-util.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/js/zoom/jquery.zoom.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/js/photoswipe/photoswipe.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/js/photoswipe/photoswipe-ui-default.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/js/frontend/single-product.min.js')}}"></script>--}}

{{--Ladyload--}}
<script>
    setTimeout(function(){
        !function (e) {
            document.createElement("style").innerHTML = "img:not([src]) {visibility: hidden;}";

            function t(e, t) {
                var n = new Image, r = e.getAttribute("data-src");
                n.onload = function () {
                    e.parent ? e.parent.replaceChild(n, e) : e.src = r, e.style.opacity = "1", t && t()
                }, n.src = r
            }

            for (var n = new Array, r = function (e, t) {
                if (document.querySelectorAll) t = document.querySelectorAll(e); else {
                    var n = document, r = n.styleSheets[0] || n.createStyleSheet();
                    r.addRule(e, "f:b");
                    for (var i = n.all, l = 0, c = [], o = i.length; l < o; l++) i[l].currentStyle.f && c.push(i[l]);
                    r.removeRule(0), t = c
                }
                return t
            }("img.lazy"), i = function () {
                for (var r = 0; r < n.length; r++) i = n[r], l = void 0, (l = i.getBoundingClientRect()).top >= 0 && l.left >= 0 && l.top <= (e.innerHeight || document.documentElement.clientHeight) && t(n[r], function () {
                    n.splice(r, r)
                });
                var i, l
            }, l = 0; l < r.length; l++) n.push(r[l]);
            i(), function (t, n) {
                e.addEventListener ? this.addEventListener(t, n, !1) : e.attachEvent ? this.attachEvent("on" + t, n) : this["on" + t] = n
            }("scroll", i)
        }(this);
    }, 20);

</script>

