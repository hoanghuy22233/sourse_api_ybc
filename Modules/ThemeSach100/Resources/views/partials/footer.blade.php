<style>
    footer a,footer h4,
    footer p {
        color: #000 !important;
    }
</style>
<?php
$data = \App\Http\Helpers\CommonHelper::getFromCache('widget_footer', ['widgets']);
if (!$data) {
    $data = \Modules\ThemeSach100\Models\Widget::select('name', 'content', 'location')->where('status', 1)->whereIn('location', ['copyright', 'footer_icon', 'footer1', 'footer2', 'footer3', 'footer4'])
        ->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->get();
    \App\Http\Helpers\CommonHelper::putToCache('widget_footer', $data, ['widgets']);
}

$footers = [];
foreach ($data as $v) {
    $footers[@$v->location][] = $v;
}
//dd($footers);
?>
<footer  class="footer1">

    {{--<div class="colored-fullwidth-gradient vc_row"></div>--}}

    <div class="vc_container footer-top">
        <div class="vc_row">

            <div class="vc_col-md-12 footer-row-2">
                <div class="vc_row">
                    <div class="vc_col-md-4 sidebar-1">
                        <aside id="text-2" class="widget vc_column_vc_container widget_text">
                            <div class="textwidget"><img
                                        data-src="{{ @\App\Http\Helpers\CommonHelper::getUrlImageThumb($settings['logo'], null, 90) }}" class="lazy"
                                        alt="{{ $settings['name'] }}" width="200px"/>
                                <br><br>

                                <div class="contact-details">
                                    @if(isset($footers['footer1']))
                                        @foreach($footers['footer1'] as $v)
                                            {!! @$v->content !!}
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="vc_col-md-2 sidebar-2">
                        @if(isset($footers['footer2']))
                            @foreach($footers['footer2'] as $v)
                                <div class="widget">
                                    <div class="widget-title"><h4>{!! @$v->name !!}</h4></div>
                                    {!! str_replace('<ul>', '<ul class="list-style">', @$v->content) !!}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="vc_col-md-2 sidebar-3">
                        @if(isset($footers['footer3']))
                            @foreach($footers['footer3'] as $v)
                                <div class="widget">
                                    <div class="widget-title"><h4>{!! @$v->name !!}</h4></div>
                                    {!! str_replace('<ul>', '<ul class="list-style">', @$v->content) !!}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="vc_col-md-4 sidebar-4">
                        @if(isset($footers['footer4']))
                            @foreach($footers['footer4'] as $v)
                                <div class="widget">
                                    <div class="widget-title"><h4>{!! @$v->name !!}</h4></div>
                                    {!! str_replace('<ul>', '<ul class="list-style">', @$v->content) !!}
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer">
        <div class="vc_container">
            <div class="vc_row">
                <div class="vc_col-md-6">
                    <p class="copyright" style="color: #fff !important;">
                        {!! @$footers['copyright'][0]->content !!} </p>
                </div>
                <div class="vc_col-md-6">
                    {!! @$footers['footer_icon'][0]->content !!}
                </div>
            </div>
        </div>
    </div>
</footer>