<title>{{ @$pageOption['meta_title'] != '' ? $pageOption['meta_title'] : @$settings['default_meta_title'] }}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="{{ @$pageOption['meta_title'] != '' ? $pageOption['meta_title'] : @$settings['name'] }}"
      name="title">
<meta content="{{ @$pageOption['meta_keywords'] != '' ? $pageOption['meta_keywords'] : @$settings['default_meta_keywords'] }}"
      name="keywords">
<meta content="{{ @$pageOption['meta_description'] != '' ? $pageOption['meta_description'] : @$settings['default_meta_description'] }}"
      name="description">
<link rel="alternate" hreflang="vi" href="{{ URL::to('/') }}"/>
<link rel="canonical"
      href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"/>

<meta property="og:title"
      content="{{ @$pageOption['meta_title'] != '' ? $pageOption['meta_title'] : @$settings['default_meta_title'] }}"/>
<meta property="og:description"
      content="{{ @$pageOption['meta_description'] != '' ? $pageOption['meta_description'] : @$settings['default_meta_description'] }}"/>
<meta property="og:image"
      content="{{ @$pageOption['image'] != '' ? $pageOption['image'] : asset('public/filemanager/userfiles/' .@$settings['logo']) }}"/>
<meta property="og:url"
      content="{{ @$pageOption['link'] != '' ? $pageOption['link'] : (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"/>
<meta property="og:type" content="website"/>
<meta property="og:site_name" content="{{ @$settings['name'] }}"/>
<meta property="og:locale" content="vi_VN" />

<meta content="{{@$settings['robots'] }}" name="robots">
<link rel="shortcut icon" href="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$settings['favicon'], null, 16) }}"
      type="image/x-icon">

<meta http-equiv="Content-Language" content="vi" />
<meta name="Language" content="vi" />
<meta name="copyright" content="Copyright © {{ date('Y') }} by {{ @$settings['name'] }}" />
<meta name="abstract" content="{{ @$settings['name'] }}" />
<meta name="distribution" content="Global" />
<meta name="author" content="{{ @$settings['name'] }}" />

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
      integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">