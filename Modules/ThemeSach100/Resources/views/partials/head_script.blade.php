<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700|Roboto:300,400,500,600,700">

<style type="text/css">
    img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
    }
</style>
<link rel='stylesheet' id='js_composer_front-css'
      href="{{ URL::asset('public/frontend/themes/sach100/plugins/js_composer/assets/css/js_composer.min.css')}}"
      type='text/css' media='all'/>
{{--<link rel='stylesheet' id='tribe-common-skeleton-style-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/the-events-calendar/common/src/resources/css/common-skeleton.min.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='tribe-tooltip-css'
      href="{{ URL::asset('public/frontend/themes/sach100/plugins/the-events-calendar/common/src/resources/css/tooltip.min.css')}}"
      type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='wp-block-library-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/wp-includes/css/dist/block-library/style.min.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='bp-member-block-css'
      href="{{ URL::asset('public/frontend/themes/sach100/plugins/buddypress/bp-members/css/blocks/member.min.css')}}"
      type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='bp-group-block-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/buddypress/bp-groups/css/blocks/group.min.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='wc-block-style-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/packages/woocommerce-blocks/build/style.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='jquery-selectBox-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='yith-wcwl-font-awesome-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/yith-woocommerce-wishlist/assets/css/font-awesome.min.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='yith-wcwl-main-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/yith-woocommerce-wishlist/assets/css/style.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='bbp-default-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/bbpress/templates/default/css/bbpress.min.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='bp-legacy-css-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/buddypress/bp-templates/bp-legacy/css/buddypress.min.css')}}"--}}
      {{--type='text/css' media='screen'/>--}}
{{--<link rel='stylesheet' id='essential-grid-plugin-settings-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/essential-grid/public/assets/css/settings.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='tp-fontello-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/essential-grid/public/assets/font/fontello/css/fontello.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='modelteme-framework-style-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/modeltheme-framework/css/modelteme-framework-style.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
<link rel='stylesheet' id='modelthemes-frontend-bar-css'
      href='//modeltheme.com/json/modelthemes-frontend-bar.css' type='text/css' media='all'/>
<link rel='stylesheet' id='rs-plugin-settings-css'
      href="{{ URL::asset('public/frontend/themes/sach100/plugins/revslider/public/assets/css/rs6.css')}}"
      type='text/css' media='all'/>

{{--<link rel='stylesheet' id='woocommerce-layout-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/css/woocommerce-layout.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='woocommerce-smallscreen-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/css/woocommerce-smallscreen.css')}}"--}}
      {{--type='text/css' media='only screen and (max-width: 768px)'/>--}}
{{--<link rel='stylesheet' id='woocommerce-general-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/css/woocommerce.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<style id='woocommerce-inline-inline-css' type='text/css'>--}}
    {{--.woocommerce form .form-row .required {--}}
        {{--visibility: visible;--}}
    {{--}--}}
{{--</style>--}}
{{--<link rel='stylesheet' id='learn-press-buddypress-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/learnpress-buddypress/assets/css/site.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='jquery-colorbox-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/yith-woocommerce-compare/assets/css/colorbox.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
<link rel='stylesheet' id='yith-quick-view-css'
      href="{{ URL::asset('public/frontend/themes/sach100/plugins/yith-woocommerce-quick-view/assets/css/yith-quick-view.css')}}"
      type='text/css' media='all'/>
<style id='yith-quick-view-inline-css' type='text/css'>
    #yith-quick-view-modal .yith-wcqv-main {
        background: #ffffff;
    }

    #yith-quick-view-close {
        color: #cdcdcd;
    }

    #yith-quick-view-close:hover {
        color: #ff0000;
    }
</style>
{{--<link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/css/prettyPhoto.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='font-awesome-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/font-awesome.min.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<style id='font-awesome-inline-css' type='text/css'>--}}
    {{--[data-font="FontAwesome"]:before {--}}
        {{--font-family: 'FontAwesome' !important;--}}
        {{--content: attr(data-icon) !important;--}}
        {{--speak: none !important;--}}
        {{--font-weight: normal !important;--}}
        {{--font-variant: normal !important;--}}
        {{--text-transform: none !important;--}}
        {{--line-height: 1 !important;--}}
        {{--font-style: normal !important;--}}
        {{---webkit-font-smoothing: antialiased !important;--}}
        {{---moz-osx-font-smoothing: grayscale !important;--}}
    {{--}--}}
{{--</style>--}}
{{--<link rel='stylesheet' id='simple-line-icons-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/simple-line-icons.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
<link rel='stylesheet' id='smartowl-responsive-css'
      href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/responsive.css')}}" type='text/css'
      media='all'/>
{{--<link rel='stylesheet' id='smartowl-media-screens-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/media-screens.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
<link rel='stylesheet' id='owl-carousel-css'
      href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/owl.carousel.css')}}"
      type='text/css' media='all'/>
<link rel='stylesheet' id='owl-theme-css'
      href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/owl.theme.css')}}" type='text/css'
      media='all'/>
{{--<link rel='stylesheet' id='animate-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/animate.css')}}" type='text/css'--}}
      {{--media='all'/>--}}
{{--<link rel='stylesheet' id='loaders-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/loaders.css')}}" type='text/css'--}}
      {{--media='all'/>--}}
<link rel='stylesheet' id='smartowl-style-css-css'
      href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/styles.css')}}" type='text/css'
      media='all'/>
<link rel='stylesheet' id='smartowl-skin-color-css'
      href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/skin-colors/skin-turquoise.css')}}"
      type='text/css' media='all'/>
<link rel='stylesheet' id='smartowl-style-css'
      href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/style.css')}}" type='text/css'
      media='all'/>
{{--<link rel='stylesheet' id='sidebarEffects-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/sidebarEffects.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='rippler-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/rippler.min.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='smartowl-gutenberg-frontend-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/gutenberg-frontend.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='smartowl-custom-bbpress-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/custom-bbpress.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
<link rel='stylesheet' id='smartowl-custom-buddypress-css'
      href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/custom-buddypress.css')}}"
      type='text/css' media='all'/>
{{--<link rel='stylesheet' id='smartowl-custom-learnpress-v3-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/custom-learnpress-v3.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='smartowl-custom-style-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/themes/smartowl/css/custom-editor-style.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
<link rel='stylesheet' id='smartowl-custom-style-css'
      href="{{ URL::asset('public/frontend/themes/sach100/css/theme.css')}}"
      type='text/css' media='all'/>
{{--<link rel='stylesheet' id='smartowl-fonts-css'--}}
      {{--href='//fonts.googleapis.com/css?family=Libre+Baskerville%3Aregular%2Citalic%2C700%2Clatin-ext%2Clatin%7CRoboto%3A100%2C100italic%2C300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C700%2C700italic%2C900%2C900italic%2Cvietnamese%2Cgreek%2Clatin-ext%2Cgreek-ext%2Ccyrillic-ext%2Clatin%2Ccyrillic&#038;ver=1.0.0'--}}
      {{--type='text/css' media='all'/>--}}
{{--<link rel='stylesheet' id='mm_icomoon-css'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/plugins/mega_main_menu/framework/src/css/icomoon.css')}}"--}}
      {{--type='text/css' media='all'/>--}}
<link rel='stylesheet' id='mmm_mega_main_menu-css'
      href="{{ URL::asset('public/frontend/themes/sach100/plugins/mega_main_menu/src/css/cache.skin.css')}}"
      type='text/css' media='all'/>
{{--<link rel='stylesheet' id='redux-google-fonts-redux_demo-css'--}}
      {{--href='https://fonts.googleapis.com/css?family=Roboto%7CLibre+Baskerville&#038;ver=1592393494' type='text/css'--}}
      {{--media='all'/>--}}
<link rel='stylesheet' id='learn-press-bundle-css'
      href="{{ URL::asset('public/frontend/themes/sach100/plugins/learnpress1/assets/css/bundle.min.css')}}"
      type='text/css' media='all'/>
<link rel='stylesheet' id='learn-press-css'
      href="{{ URL::asset('public/frontend/themes/sach100/plugins/learnpress1/assets/css/learnpress.css')}}"
      type='text/css' media='all'/>
<script>
    window.LP_DEBUG = true;
</script>
{{--
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/wp-includes/js/jquery/jquery.js')}}"></script>--}}
<script src="{{ asset('/public/libs/jquery-3.4.0.min.js') }}"></script>
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/wp-includes/js/jquery/jquery-migrate.min.js')}}"></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var BP_Confirm = {
        "are_you_sure": "Are you sure?"
    };
    /* ]]> */
</script>
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/buddypress/bp-core/js/confirm.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/buddypress/bp-core/js/widget-members.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/buddypress/bp-core/js/jquery-query.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/buddypress/bp-core/js/vendor/jquery-cookie.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/buddypress/bp-core/js/vendor/jquery-scroll-to.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/buddypress/bp-templates/bp-legacy/js/buddypress.min.js')}}"></script>--}}
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/plugins/revslider/public/assets/js/rbtools.min.js')}}"></script>
<script type='text/javascript'
        src="{{ URL::asset('public/frontend/themes/sach100/plugins/revslider/public/assets/js/rs6.min.js')}}"></script>
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
{{--        src="{{ URL::asset('public/frontend/themes/sach100/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cart.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
{{--        src="{{ URL::asset('public/frontend/themes/sach100/plugins/learnpress1/assets/js/vendor/plugins.all.js')}}"></script>--}}
{{--<script src="{{ URL::asset('public/frontend/themes/sach100/plugins/the-events-calendar/common/src/resources/js/underscore-before.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
{{--        src="{{ URL::asset('public/frontend/themes/sach100/wp-includes/js/underscore.min.js')}}"></script>--}}
{{--<script src="{{ URL::asset('public/frontend/themes/sach100/plugins/the-events-calendar/common/src/resources/js/underscore-after.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/wp-includes/js/utils.min.js')}}"></script>--}}

{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/learnpress1/assets/js/global.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/learnpress1/assets/js/utils.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/learnpress1/assets/js/frontend/learnpress.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/learnpress1/assets/js/frontend/course.js')}}"></script>--}}
{{--<script type='text/javascript'--}}
        {{--src="{{ URL::asset('public/frontend/themes/sach100/plugins/learnpress1/assets/js/frontend/become-teacher.js')}}"></script>--}}
<!--[if IE 9]>
<script>var _gambitParallaxIE9 = true;</script> <![endif]-->
<script type="text/javascript" src="{{ URL::asset('public/frontend/themes/sach100/js/theme1.js')}}"></script>
{{--<link rel='stylesheet'--}}
      {{--href="{{ URL::asset('public/frontend/themes/sach100/css/vc_shortcodes.css') }}"--}}
      {{--type='text/css' media='all'/>--}}

@if($_SERVER['REQUEST_URI'] != '/')
<script src="{{ URL::asset('public/libs/jquery-3.4.0.min.js') }}"></script>
@endif

<link rel='stylesheet'
      href="{{ URL::asset('public/frontend/themes/sach100/css/custom.css') }}?v={{ time() }}"
      type='text/css' media='all'/>