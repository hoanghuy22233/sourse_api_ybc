<style>
    .alert.alert-default {
        border: 1px solid #ccc;
    }

    .wpb_wrapper.bxh-time {
        display: inline-block;
        width: 100%;
        text-align: center;
        margin-bottom: 50px;
    }

    .block-bxh .vc_row.wpb_row.vc_row-fluid {
        margin-bottom: 10px;
    }

    .alert-info {
        color: #FFFFFF !important;
    }

    .nut-khac .alert {
        display: inline-block;
        font-weight: bold;
        padding: 15px 30px;
        margin-right: 20px;
        width: 156px;
    }

    .nut-khac {
        display: inline-block;
        width: 100%;
        margin-top: 30px;
        text-align: center;
    }

    .wpb_row {
        background-size: cover;
    }

    .bxh-block .title-pricing {
        background: #5b7fb4 !important;
    }

    .bxh-block .price_circle {
        background: #4c6993 !important;
    }
    .bxh-block li {
        padding: 10px !important;
        width: 100%;
    }
    .bxh-block .pricing-table ul li .ten {
        float: left;
    }
    .bxh-block span.diem {
        max-width: 70px;
        float: right;
        position: absolute;
        right: 21px;
        left: unset;
        margin: 0;
        background: #f7f8fa;
    }
    .bxh-block .pricing-table ul li span {
        color: #000;
    }

    .circle-content img{
        border-radius: 50% !important;
    }

    @media (max-width: 768px) {
        .block-bxh .wpb_column {
            width: 100% !important;
        }

        .bxh-block .title-pricing {
            background: #5b7fb4 !important;
        }

        .bxh-block .price_circle {
            background: #4c6993 !important;
        }
        .bxh-block .pricing-table ul {
            max-height: 129px;
            overflow: hidden;
        }
        .circle-content img{
            border-radius: 50% !important;
        }
    }
</style>
<?php
$bxh = CommonHelper::getFromCache('bhx_content', ['exams']);
if (!$bxh) {
    $subjects = \Modules\Sach100Exam\Models\Category::select('name', 'id', 'image')->where('type', 10)->where('status', 1)->orderBy('id', 'asc')->get();

    $tuan = date('Y-m-d 00:00:00', strtotime('-'.date('w').' days'));
    $thang = date('Y-m-0 00:00:00');
    $nam = date('Y-01-01 00:00:00');

    $bxh_tuan = [];
    $bxh_thang = [];
    $bxh_nam = [];
    foreach ($subjects as $subject) {

        $bxh_tuan[$subject->id] = \Modules\ThemeSach100\Models\Exam::where('subject_id', $subject->id)->where('type', 'Đề chuẩn JLPT')->where('created_at', '>', $tuan)->groupBy('student_id')
            ->whereNotNull('total_point')->where('total_point', '!=', 0)->orderBy('total_point', 'desc')->limit(5)->get();

        $bxh_thang[$subject->id] = \Modules\ThemeSach100\Models\Exam::where('subject_id', $subject->id)->where('type', 'Đề chuẩn JLPT')->where('created_at', '>', $thang)->groupBy('student_id')
            ->whereNotNull('total_point')->where('total_point', '!=', 0)->orderBy('total_point', 'desc')->limit(5)->get();

        $bxh_nam[$subject->id] = \Modules\ThemeSach100\Models\Exam::where('subject_id', $subject->id)->where('type', 'Đề chuẩn JLPT')->where('created_at', '>', $nam)
            ->whereNotNull('total_point')->where('total_point', '!=', 0)->groupBy('student_id')
            ->orderBy('total_point', 'desc')->limit(5)->get();
    }

    $bxh = [
        'subjects' => $subjects,
        'bxh_tuan' => $bxh_tuan,
        'bxh_thang' => $bxh_thang,
        'bxh_nam' => $bxh_nam
    ];
    CommonHelper::putToCache('bhx_content', $bxh, ['exams']);
} else {
    $subjects = $bxh['subjects'];
    $bxh_tuan = $bxh['bxh_tuan'];
    $bxh_thang = $bxh['bxh_thang'];
    $bxh_nam = $bxh['bxh_nam'];
}
?>
<div class="wpb_row high-padding content-area block-bxh desktop">
    <div class="container">
        <div class="row">
            <main id="main" class="vc_col-md-12 site-main main-content">
                <article id="post-1825" class="post-1825 page type-page status-publish hentry">
                    <div class="entry-content">
                        <div class="vc_row wpb_row vc_row-fluid ">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="title-subtile-holder"><h1
                                                    class="section-title ">Bảng Xếp Hạng</h1>
                                            <div class="section-border "></div>
                                            <div class="section-subtitle "></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="vc_row wpb_row vc_row-fluid bxh-block" id="">
                            <div class="wpb_wrapper bxh-time">
                                <a class="alert  alert-info " data-box="bxh-box-1" data-animate="fadeInLeft"
                                   href="javascript:;"
                                   style=" border-top-right-radius: 0;border-bottom-right-radius: 0;">Tháng
                                </a>
                                <a class="alert  alert-default " data-box="bxh-box-2" data-animate="fadeInLeft"
                                   href="javascript:;" style="border-radius: 0;
    border: 1px solid #ccc;
    border-left: 0;
    border-right: 0;">Tuần
                                </a>
                                <a class="alert  alert-default " data-box="bxh-box-3" data-animate="fadeInLeft"
                                   href="javascript:;" style="border-top-left-radius: 0;
    border-bottom-left-radius: 0">Năm
                                </a>
                            </div>

                            <div>
                                <div class="bxh-box-1">
                                    @foreach ($subjects as $subject)
                                        <div id="bxh-1" style="width: 20%;"
                                             class="wpb_column vc_column_container vc_col-sm-2">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="pricing-table animateIn recommended"
                                                         data-animate="fadeInLeft">
                                                        <div class="table-content"><h4
                                                                    class="text-center title-pricing">
                                                                {{ $subject->name }}</h4>
                                                            <div class="circle-container">
                                                                <div class="block-circle">
                                                                    <div class="circle-content"><img
                                                                                data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($subject->image, 100, null) }}"
                                                                                class="lazy"
                                                                                alt="{{ $v->name }}"/></div>
                                                                </div>
                                                            </div>
                                                            <ul class="text-center" style="padding: 0;">
                                                                @foreach($bxh_thang[$subject->id] as $k => $exam)
                                                                    <li style="display: inline-block">
                                                                        <span class="ten">{{ $k + 1 }}. {{ @$exam->student->name }}
                                                                        </span>
                                                                        <span class="diem">
                                                                        - {{ $exam->total_point }}
                                                                        /180
                                                                        </span>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="bxh-box-2" style="display: none;">
                                    @foreach ($subjects as $subject)
                                        <div id="bxh-1" style="width: 20%;"
                                             class="wpb_column vc_column_container vc_col-sm-2">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="pricing-table animateIn recommended"
                                                         data-animate="fadeInLeft">
                                                        <div class="table-content"><h4
                                                                    class="text-center title-pricing">
                                                                {{ $subject->name }}</h4>
                                                            <div class="circle-container">
                                                                <div class="block-circle">
                                                                    <div class="circle-content">
                                                                        <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($subject->image, 100, null) }}"
                                                                             class="lazy"
                                                                             alt="{{ $v->name }}"/>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <ul class="" style="padding: 0;">
                                                                @foreach($bxh_tuan[$subject->id] as $k => $exam)
                                                                    <li style="display: inline-block">
                                                                        <span class="ten">{{ $k + 1 }}. {{ @$exam->student->name }}</span>

                                                                        <span class="diem">- {{ $exam->total_point }}
                                                                        /180</span>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="bxh-box-3" style="display: none;">
                                    @foreach ($subjects as $subject)
                                        <div id="bxh-1" style="width: 20%;"
                                             class="wpb_column vc_column_container vc_col-sm-2">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="pricing-table animateIn recommended"
                                                         data-animate="fadeInLeft">
                                                        <div class="table-content"><h4
                                                                    class="text-center title-pricing">
                                                                {{ $subject->name }}</h4>
                                                            <div class="circle-container">
                                                                <div class="block-circle">
                                                                    <div class="circle-content">
                                                                        {{--<p class="text-center price_circle">{{ $subject->name }}--}}
                                                                        {{--</p>--}}

                                                                        <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($subject->image, 100, null) }}"
                                                                             class="lazy"
                                                                             alt="{{ $v->name }}"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <ul class="text-center" style="padding: 0;">
                                                                @foreach($bxh_nam[$subject->id] as $k => $exam)
                                                                    <li style="display: inline-block">
                                                                        <span class="ten">{{ $k + 1 }}. {{ @$exam->student->name }}
                                                                        </span>
                                                                        <span class="diem">
                                                                        - {{ $exam->total_point }}
                                                                        /180
                                                                        </span>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        @if($_SERVER['REQUEST_URI'] != '/' && strpos($_SERVER['REQUEST_URI'], '/?') === false)
                            <div class="wpb_wrapper nut-khac" style="
    ">
                                <a class="alert  alert-warning " data-animate="fadeInLeft" href="/tao-bai-thi">Dạng đề
                                    khác
                                </a>
                                <a class="alert  alert-info " data-animate="fadeInLeft"
                                   href="/dap-an/{{ @$_GET['exam_id'] }}">Xem đáp án
                                </a>
                                <a class="alert  alert-info " data-animate="fadeInLeft" href="/tin-tuc">Tin tức
                                </a>
                            </div>
                        @endif
                    </div><!-- .entry-content -->
                </article><!-- #post-## -->
            </main>
        </div>
    </div>
</div>

<div class="vc_row wpb_row vc_row-fluid vc_custom_1461047794091 block-bxh mobile" style="/*background-image: url(/public/frontend/themes/sach100/images/danh_gia_cua_sv.png) !important;*/ margin: 0 !important;
    padding-top: 43px;
    padding-bottom: 68px;">
    <div class="container">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="title-subtile-holder"><h1
                                class="section-title ">Bảng Xếp Hạng</h1>
                        <div class="section-border "></div>
                        <div class="section-subtitle "></div>
                    </div>
                    <div class="vc_row bxh-block" id="">
                        <div class="wpb_wrapper bxh-time">
                            <a class="alert  alert-info " data-box="bxh-box-1" data-animate="fadeInLeft"
                               href="javascript:;"
                               style="border-top-right-radius: 0;border-bottom-right-radius: 0;">Tháng
                            </a>
                            <a class="alert  alert-default " data-box="bxh-box-2" data-animate="fadeInLeft"
                               href="javascript:;" style="border-radius: 0;
    border: 1px solid #ccc;
    border-left: 0;
    border-right: 0;">Tuần
                            </a>
                            <a class="alert  alert-default " data-box="bxh-box-3" data-animate="fadeInLeft"
                               href="javascript:;" style="border-top-left-radius: 0;
    border-bottom-left-radius: 0">Năm
                            </a>
                        </div>

                        <div>
                            <div class="bxh-box-1">
                                <div data-animate="fadeIn"
                                     class="testimonials-container-3 owl-carousel owl-theme animateIn">
                                    @foreach ($subjects as $subject)
                                        <div class="item vc_col-md-12 relative text-white" id="bxh-1">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="pricing-table animateIn recommended "
                                                         style="background-color: #5b7fb4 !important;"
                                                         data-animate="fadeInLeft">
                                                        <div class="table-content"><h4
                                                                    class="text-center title-pricing">
                                                                {{ $subject->name }}</h4>
                                                            <div class="circle-container">
                                                                <div class="block-circle">
                                                                    <div class="circle-content">
                                                                        {{--<p--}}
                                                                        {{--class="text-center price_circle">{{ $subject->name }}--}}
                                                                        {{--</p>--}}
                                                                        <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($subject->image, 100, null) }}"
                                                                             class="lazy"
                                                                             alt="{{ $v->name }}"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <ul class="text-center" style="padding: 0;">
                                                                @foreach($bxh_thang[$subject->id] as $k => $exam)
                                                                    <li>{{ $k + 1 }}. {{ @$exam->student->name }}
                                                                        - {{ $exam->total_point }}
                                                                        /180</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="bxh-box-2" style="display: none;">
                                <div data-animate="fadeIn"
                                     class="testimonials-container-3 owl-carousel owl-theme animateIn">
                                    @foreach ($subjects as $subject)
                                        <div class="item vc_col-md-12 relative text-white" id="bxh-1">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="pricing-table animateIn recommended"
                                                         data-animate="fadeInLeft">
                                                        <div class="table-content"><h4
                                                                    class="text-center title-pricing">
                                                                {{ $subject->name }}</h4>
                                                            <div class="circle-container">
                                                                <div class="block-circle">
                                                                    <div class="circle-content">
                                                                        {{--<p--}}
                                                                        {{--class="text-center price_circle">{{ $subject->name }}--}}
                                                                        {{--</p>--}}
                                                                        <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($subject->image, 100, null) }}"
                                                                             class="lazy"
                                                                             alt="{{ $v->name }}"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <ul class="text-center" style="padding: 0;">
                                                                @foreach($bxh_tuan[$subject->id] as $k => $exam)
                                                                    <li>{{ $k + 1 }}. {{ @$exam->student->name }}
                                                                        - {{ $exam->total_point }}
                                                                        /180</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="bxh-box-3" style="display: none;">
                                <div data-animate="fadeIn"
                                     class="testimonials-container-3 owl-carousel owl-theme animateIn">
                                    @foreach ($subjects as $subject)
                                        <div class="item vc_col-md-12 relative text-white" id="bxh-1">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="pricing-table animateIn recommended"
                                                         data-animate="fadeInLeft">
                                                        <div class="table-content"><h4
                                                                    class="text-center title-pricing">
                                                                {{ $subject->name }}</h4>
                                                            <div class="circle-container">
                                                                <div class="block-circle">
                                                                    <div class="circle-content">
                                                                        {{--<p--}}
                                                                                {{--class="text-center price_circle">{{ $subject->name }}--}}
                                                                        {{--</p>--}}
                                                                        <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($subject->image, 100, null) }}"
                                                                             class="lazy"
                                                                             alt="{{ $v->name }}"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <ul class="text-center" style="padding: 0;">
                                                                @foreach($bxh_nam[$subject->id] as $k => $exam)
                                                                    <li>{{ $k + 1 }}. {{ @$exam->student->name }}
                                                                        - {{ $exam->total_point }}
                                                                        /180</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($_SERVER['REQUEST_URI'] != '/' && strpos($_SERVER['REQUEST_URI'], '/?') === false)
                        <div class="wpb_wrapper nut-khac" style="
    ">
                            <a class="alert  alert-warning " data-animate="fadeInLeft" href="/tao-bai-thi">Dạng đề
                                khác
                            </a>
                            <a class="alert  alert-info " data-animate="fadeInLeft"
                               href="/dap-an/{{ @$_GET['exam_id'] }}">Xem đáp án
                            </a>
                            <a class="alert  alert-info " data-animate="fadeInLeft" href="/tin-tuc">Tin tức
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery('.bxh-block a').click(function () {
        var block = jQuery(this).data('box');
        console.log(block);
        jQuery(this).addClass('alert-info').removeClass('alert-default');
        jQuery(this).siblings().removeClass('alert-info').addClass('alert-default');
        jQuery('.' + block).show();
        jQuery('.' + block).siblings().hide();
    });
</script>