<!DOCTYPE html>
<html lang="vi">

<head>
    @include('themesach100::partials.head_meta')
    @include('themesach100::partials.head_script')
    {!! @$settings['frontend_head_code'] !!}
    @yield('head_script')
</head>

<body data-rsssl=1
      class="home-page bp-legacy home page-template-default page page-id-1125 theme-smartowl mmm mega_main_menu-2-1-4 woocommerce-no-js tribe-no-js       first_header  wpb-js-composer js-comp-ver-6.2.0 vc_responsive skin_1BBC9B no-js">

<div id="page" class="hfeed site">

    @include('themesach100::partials.header')


    @yield('main_content')


    @include('themesach100::partials.button.back_to_top')

    @include('themesach100::partials.footer')
</div>

<script type="text/javascript">
    var ajaxRevslider;

    jQuery(document).ready(function () {


        // CUSTOM AJAX CONTENT LOADING FUNCTION
        ajaxRevslider = function (obj) {

            // obj.type : Post Type
            // obj.id : ID of Content to Load
            // obj.aspectratio : The Aspect Ratio of the Container / Media
            // obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content

            var content = '';
            var data = {
                action: 'revslider_ajax_call_front',
                client_action: 'get_slider_html',
                token: 'f4900c10bd',
                type: obj.type,
                id: obj.id,
                aspectratio: obj.aspectratio
            };

            // SYNC AJAX REQUEST
            jQuery.ajax({
                type: 'post',
                url: './wp-admin/admin-ajax.php',
                dataType: 'json',
                data: data,
                async: false,
                success: function (ret, textStatus, XMLHttpRequest) {
                    if (ret.success == true)
                        content = ret.data;
                },
                error: function (e) {
                    console.log(e);
                }
            });

            // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
            return content;
        };

        // CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
        var ajaxRemoveRevslider = function (obj) {
            return jQuery(obj.selector + ' .rev_slider').revkill();
        };


        // EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION
        if (jQuery.fn.tpessential !== undefined)
            if (typeof (jQuery.fn.tpessential.defaults) !== 'undefined')
                jQuery.fn.tpessential.defaults.ajaxTypes.push({
                    type: 'revslider',
                    func: ajaxRevslider,
                    killfunc: ajaxRemoveRevslider,
                    openAnimationSpeed: 0.3
                });
        // type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
        // func: the Function Name which is Called once the Item with the Post Type has been clicked
        // killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
        // openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3)


    });
</script>
<script>
    (function (body) {
        'use strict';
        body.className = body.className.replace(/\btribe-no-js\b/, 'tribe-js');
    })(document.body);
</script>

<div id="yith-quick-view-modal">

    <div class="yith-quick-view-overlay"></div>

    <div class="yith-wcqv-wrapper">

        <div class="yith-wcqv-main">

            <div class="yith-wcqv-head">
                <a href="#" id="yith-quick-view-close" class="yith-wcqv-close">X</a>
            </div>

            <div id="yith-quick-view-content" class="woocommerce single-product"></div>

        </div>

    </div>

</div>

<link href="https://fonts.googleapis.com/css?family=Pacifico:400%7CRoboto:400%2C700%2C900" rel="stylesheet"
      property="stylesheet" media="all" type="text/css">
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" aria-label="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" aria-label="Share"></button>
                <button class="pswp__button pswp__button--fs" aria-label="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" aria-label="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" aria-label="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" aria-label="Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>




@include('themesach100::partials.footer_script')
@yield('footer_script')
{!! @$settings['frontend_body_code'] !!}
</body>

</html>