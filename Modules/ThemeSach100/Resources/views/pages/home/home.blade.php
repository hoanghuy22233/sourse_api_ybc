@extends('themesach100::layouts.default')
@section('main_content')

    @if(@$settings['banner_slides_type'] == 0)
        @include('themesach100::pages.home.partials.banner')
    @else
        @include('themesach100::partials.slides')
    @endif


    <style>
        .thong-ke p, .thong-ke span, .thong-ke h1 {
            color: #000 !important;
        }
    </style>
    <div class="vc_row wpb_row vc_row-fluid vc_custom_1461046961323 vc_row-has-fill thong-ke">
        {{--<img data-src="https://demo16.webhobasoft.com/public/frontend/themes/sach100/images/mon_hoc.png" class="lazy" style="
    position: absolute;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    max-width: unset;
">--}}
        <div class="container">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="title-subtile-holder">
                            <h1 class="section-title light_title">THỐNG KÊ</h1>
                            <div class="section-border dark_border"></div>
                            <div class="section-subtitle light_subtitle"></div>
                        </div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid">

                            <div class="wpb_column vc_column_container vc_col-sm-3 vc_col_dashboard">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stats-block statistics  animateIn"
                                             data-animate="fadeIn">
                                            <div class="stats-head">
                                                <p class="stat-number skill"><img class="skill_image lazy"
                                                                                  data-src="/public/filemanager/userfiles/thong_ke/det.png"
                                                                                  alt=""></p>
                                            </div>
                                            <div class="stats-content percentage" data-perc="1000"><span
                                                        class="skill-count">1000</span><span class="phan_tram">+</span>
                                                <p>Đề thi sát JLPT</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-3  vc_col_dashboard">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stats-block statistics  animateIn"
                                             data-animate="fadeIn">
                                            <div class="stats-head">
                                                <p class="stat-number skill"><img class="skill_image lazy"
                                                                                  data-src="/public/filemanager/userfiles/thong_ke/dt.png"
                                                                                  alt=""></p>
                                            </div>
                                            <div class="stats-content percentage" data-perc="5000"><span
                                                        class="skill-count">5000</span><span class="phan_tram">+</span>
                                                <p>Thí sinh đã tham gia</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-3  vc_col_dashboard">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stats-block statistics  animateIn"
                                             data-animate="fadeIn">
                                            <div class="stats-head">
                                                <p class="stat-number skill"><img class="skill_image lazy"
                                                                                  data-src="/public/filemanager/userfiles/thong_ke/dht.png"
                                                                                  alt=""></p>
                                            </div>
                                            <div class="stats-content percentage" data-perc="52">
                                                <span class="skill-count">52</span><span class="phan_tram">%</span>
                                                <p>Thí sinh đã đỗ JLPT</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-3  vc_col_dashboard">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="stats-block statistics  animateIn"
                                             data-animate="fadeIn">
                                            <div class="stats-head">
                                                <p class="stat-number skill"><img class="skill_image lazy"
                                                                                  data-src="/public/filemanager/userfiles/thong_ke/dtb.png"
                                                                                  alt=""></p>
                                            </div>
                                            <div class="stats-content percentage" data-perc="93"><span
                                                        class="skill-count">93</span>
                                                <p>Điểm số trung bình</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="vc_row wpb_row vc_row-fluid vc_custom_1472730538120 khoi-mon-hoc">
        {{--<img data-src="https://demo16.webhobasoft.com/public/frontend/themes/sach100/images/mon_hoc.png" class="lazy" style="
    position: absolute;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    max-width: unset;
">--}}
        <div class="container">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="courses-list row relative animateIn" data-animate="fadeIn">
                            <div class="vc_col-sm-4 courses">
                                <?php
                                $data = \App\Http\Helpers\CommonHelper::getFromCache('widget_home_subject', ['widgets']);
                                if (!$data) {
                                    $data = \Modules\ThemeSach100\Models\Widget::select('name', 'content', 'location')->where('status', 1)->where('order_no', 2)->whereIn('location', ['home_subject'])
                                        ->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->get();
                                    \App\Http\Helpers\CommonHelper::putToCache('widget_home_subject', $data, ['widgets']);
                                }

                                $subjects_new = [];
                                foreach ($data as $v) {
                                    $subjects_new[$v->location][] = $v;
                                }
                                ?>
                                @if(isset($subjects_new['home_subject']))
                                    @foreach($subjects_new['home_subject'] as $v)
                                        <div class="col-md-12 all_courses_box">

                                            <div class="all_courses_title">
                                                <h1 class="all_courses_box_title">{!! @$v->name !!}
                                                </h1>
                                            </div>
                                            <div class="all_courses_description">
                                                <p class="all_courses_box_desc"> {!! @$v->content !!}

                                                </p>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <?php
                            $subjects = CommonHelper::getFromCache('categories_new', ['categories']);
                            if (!$subjects) {
                                $subjects = \Modules\ThemeSach100\Models\Category::where('type', 10)->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->get();
                                CommonHelper::putToCache('categories_new', $subjects, ['categories']);
                            }
                            ?>
                            @foreach($subjects as $v)
                                <div class="vc_col-sm-4 vc_col-xs-6 courses">
                                    <div class="shortcode_course_content text-white"
                                         style="background-color:#bccbe2;">
                                        <a href="/{{ $v->slug }}">
                                            <div class="featured_image_courses">
                                                <div class="course_badge">
                                                </div>
                                                <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image, 700, 500) }}"
                                                     class="lazy"
                                                     alt="{{ $v->name }}"
                                                /></div>
                                        </a>
                                        <div class="col-md-12 course_text_content">
                                            <a href="/{{ $v->slug }}">
                                                <div class="col-md-11 course_text_container">
                                                    <h4 class="course_title"
                                                        style="text-align: center;">{{$v->name}}</h4>
                                                    <div class="clearfix"></div>
                                                    <h5 class="course_cost"></h5>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('themesach100::partials.bang_xep_hang_content')

    @include('themesach100::pages.home.partials.gioi_thieu')

    <div class="vc_row wpb_row vc_row-fluid vc_custom_1461047794091 danh_gia_cua_sv">
        {{--<img data-src="https://demo16.webhobasoft.com/public/frontend/themes/sach100/images/mon_hoc.png" class="lazy" style="
    position: absolute;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    max-width: unset;
">--}}
        <div class="container">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="title-subtile-holder">
                            <h1 class="section-title dark_title">ĐÁNH GIÁ CỦA HỌC VIÊN</h1>
                            <div class="section-border dark_border"></div>
                            <div class="section-subtitle dark_subtitle">
                            </div>
                        </div>
                        <div class="vc_row">
                            <div data-animate="fadeIn"
                                 class="testimonials-container-3 owl-carousel owl-theme animateIn">
                                <?php
                                $banner_student = CommonHelper::getFromCache('banners_home_between', ['banners']);
                                if (!$banner_student) {
                                    $banner_student = \Modules\ThemeSach100\Models\Banner::where('location', 'home_between')->where('status', 1)->orderBy('order_no', 'DESC')->orderBy('id', 'DESC')->get();
                                    CommonHelper::putToCache('banners_home_between', $banner_student, ['banners']);
                                }
                                ?>
                                @foreach($banner_student as $v)
                                    <div class="item vc_col-md-12 relative text-white">
                                        <div class="text-left">
                                            <div class="testimonial-img-holder pull-left">
                                                <div class="testimonial-img"
                                                     style="border:4px solid #374c75; width: 92px; height: 92px;    ">
                                                    <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image, 92, 92) }}"
                                                         class="lazy"
                                                         alt="{{$v->name}}"/></div>
                                            </div>
                                            <div class="testimonial-author-job">
                                                <h4><strong>{{$v->name}}</strong>, </h4>
                                                <h5>{!! $v->intro !!}</h5>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="testimonial-arrow"
                                             style="border-bottom: 20px solid #374c75;"></div>
                                        <div class="testimonail-content"
                                             style="background-color:#374c75; ">
                                            <p>{!! $v->content !!}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="vc_row wpb_row vc_row-fluid vc_custom_1461047932461 chuyen_de">
        {{--<img data-src="https://demo16.webhobasoft.com/public/frontend/themes/sach100/images/mon_hoc.png" class="lazy" style="
    position: absolute;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    max-width: unset;
">--}}
        <div class="container">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="title-subtile-holder">
                            <?php
                            $cats = CommonHelper::getFromCache('categories_featured_1', ['categories']);
                            if (!$cats) {
                                $cats = \Modules\Sach100Exam\Models\Category::select('id', 'name', 'slug', 'image')->where('featured', 1)->where('status', 1)->limit(4)->get();
                                CommonHelper::putToCache('categories_featured_1', $cats, ['categories']);
                            }
                            ?>
                            <h1 class="section-title dark_title">TIN TỨC</h1>
                            <div class="section-border dark_border"></div>
                            <div class="section-subtitle dark_subtitle">
                            </div>
                        </div>
                        <div class="smartowl_shortcode_blog vc_row sticky-posts animateIn"
                             data-animate="fadeIn">
                            <div class="row">
                                @foreach($cats as $cat)
                                    <div class="vc_col-sm-6 post">
                                        <div class="col-md-12 shortcode_post_content text-white"
                                             style="background-color:#4c6b94;">
                                            <div class="col-md-5 featured_image_content"><img class="lazy"
                                                                                              style="    width: 100%;"
                                                                                              data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($cat->image, 231, 262) }}"
                                                                                              alt="{{ $cat->name }}">
                                            </div>
                                            <div class="col-md-7 text_content"><h3 class="post-name post-name-color"><a
                                                            href="/{{ $cat->slug }}">{{ $cat->name }}</a>
                                                </h3>

                                                <div class="post-excerpt">
                                                    <?php
                                                    $posts = CommonHelper::getFromCache('posts_new' . $cat->id, ['posts']);
                                                    if (!$posts) {
                                                        $posts = \Modules\ThemeSach100\Models\Post::select('id', 'name', 'slug')->where('status', 1)
                                                            ->where('multi_cat', 'like', '%|' . $cat->id . '|%')->orderBy('order_no', 'desc')->orderBy('id', 'desc')->limit(3)->get();
                                                        CommonHelper::putToCache('posts_new' . $cat->id, $posts, ['posts']);
                                                    }
                                                    ?>
                                                    <ul style="padding-left: 14px;">
                                                        @foreach($posts as $post)
                                                            @if(strpos($post->slug, 'http') === false)
                                                                <li><a href="/{{ $cat->slug }}/{{ $post->slug }}.html"
                                                                       style="color: #fff;font-weight: 600;  width: 265px;
            overflow: hidden !important;
            white-space: nowrap;
            text-overflow: ellipsis;">{{ $post->name }}</a>
                                                                </li>
                                                            @else
                                                                <li><a href="{{ $post->slug }}" target="_blank"
                                                                       style="color: #fff;font-weight: 600;  width: 265px;
            overflow: hidden !important;
            white-space: nowrap;
            text-overflow: ellipsis;">{{ $post->name }}</a>
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                </div>
                                                <div class="post-more-download">
                                                    <div class="col-md-8 col-sm-9 col-xs-12 post-read-more"><a
                                                                class="rippler rippler-default hvr-float post-read-more-button"
                                                                href="/{{ $cat->slug }}">Xem
                                                            thêm</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="vc_row wpb_row vc_row-fluid vc_custom_1461046534176 chuong">
        {{--<img data-src="https://demo16.webhobasoft.com/public/frontend/themes/sach100/images/mon_hoc.png" class="lazy" style="
    position: absolute;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    max-width: unset;
">--}}
        <div class="container">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <?php
                    $widget = CommonHelper::getFromCache('widgets_home_content', ['widgets']);
                    if (!$widget) {
                        $widget = \Modules\ThemeEdu\Models\Widget::where('location', 'home_content')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->first();
                        CommonHelper::putToCache('widgets_home_content', $widget, ['widgets']);
                    }

                    $options = [];
                    $arr = explode('- Kênh: ', @$widget->content);
                    foreach ($arr as $item) {
                        if ($item != '') {
                            $option = [];
                            $lines = preg_split('/\n|\r\n?/', $item);
                            $option['name'] = trim(@$lines[0]);
                            foreach ($lines as $line) {
                                if (strpos($line, '     + ảnh:') !== false) {
                                    $option['image'] = trim(str_replace('     + ảnh:', '', $line));
                                }
                                if (strpos($line, '     + mô tả:') !== false) {
                                    $option['intro'] = trim(str_replace('     + mô tả:', '', $line));
                                }
                                if (strpos($line, '     + link:') !== false) {
                                    $option['link'] = trim(str_replace('     + link:', '', $line));
                                }
                            }
                            $options[] = $option;
                        }
                    }

                    ?>
                    <div class="wpb_wrapper">
                        <div class="title-subtile-holder"><h1
                                    class="section-title dark_title">{!! @$widget->name !!}</h1>
                            <div class="section-border dark_border"></div>
                            <div class="section-subtitle dark_subtitle">
                            </div>
                        </div>
                        <div class="teachers-list row relative animateIn animated fadeInUp"
                             data-animate="fadeInUp" style="opacity: 100;">
                            @foreach($options as $option)
                                <div class="vc_col-md-3 teachers">
                                    <div class="shortcode_course_content " style="">
                                        <a href="{{ $option['link'] }}">
                                            <div class="featured_image_courses"><img alt=""
                                                                                     data-src="{{ $option['image'] }}"
                                                                                     class="avatar avatar-400 photo lazy">
                                            </div>
                                        </a>
                                        <div class="vc_col-md-12 course_text_content">
                                            <div class="row teacher_icon_title_description">
                                                <div class="container_teacher_icon_title vc_col-md-12">
                                                    <i class=""></i><h4 class="teacher_title">
                                                        {{ $option['name'] }}</h4></div>
                                                <div class="container_teacher_description vc_col-md-12">
                                                    <p class="teacher_descriere"
                                                       style="color: #000;">{!! $option['intro'] !!}</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php
    $widget = CommonHelper::getFromCache('widgets_home_content_bottom', ['widgets']);
    if (!$widget) {
        $widget = \Modules\ThemeEdu\Models\Widget::where('location', 'home_content_bottom')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->first();
        CommonHelper::putToCache('widgets_home_content_bottom', $widget, ['widgets']);
    }
    ?>
    @if(is_object($widget))
        <div class="vc_row wpb_row vc_row-fluid vc_custom_1461048085915 vc_row-has-fill box-email khung_mail">
            <div class="container bgr-email">
                <div class="wpb_column vc_column_container vc_col-sm-8" style="z-index: 9999">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-3">
                                    <div class="vc_column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element wpb_left-to-right left-to-right">
                                                <div class="wpb_wrapper">
                                                    <p>
                                                        <a href="#"><img
                                                                    class="size-full wp-image-3729 aligncenter lazy"
                                                                    data-src="{{ asset('/public/frontend/themes/edu-ldp/images/envelop.png') }}"
                                                                    alt="poster_newsletter" width="120"
                                                                    height="135"/></a></p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-9" style="z-index: 9999">
                                    <div class="vc_column-inner vc_custom_1445429978052">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_text_column wpb_content_element wpb_top-to-bottom top-to-bottom">
                                                <div class="wpb_wrapper">
                                                    <h3 style="text-align: left;">
                                                        <strong>{!! @$widget->name !!}</strong>
                                                    </h3>
                                                    <p style="text-align: left; margin-top: 23px">{!! @$widget->content !!}</p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="mc_embed_signup animateIn" data-animate="fadeIn">
                                <h3 class="subscribe_title"><strong>ĐỊA CHỈ EMAIL CỦA BẠN</strong></h3>
                                <div class="email">
                                    <div class="subscribe subscribe_to_newsletter-form" method="POST"><input type="text"
                                                                                                             placeholder="Email"
                                                                                                             name="subscribe_to_newsletter_email"
                                                                                                             class="emaddress"
                                                                                                             data-validate="validate(required, email)"/>
                                        <button class="btn-warning hvr-float rippler rippler-default subscribe_to_newsletter-btn"
                                                name="submit_mailchimp"
                                                type="submit" style="background-color:#ffba41;">
                                            Gửi
                                        </button>
                                        <span class="result section-description"></span></div>
                                    <script>
                                        function isEmail(email) {
                                            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                                            return regex.test(email);
                                        }

                                        $(document).ready(function () {
                                            $('.subscribe_to_newsletter-btn').click(function () {
                                                var email = $('input[name=subscribe_to_newsletter_email]').val();
                                                if (email.length == 0) {
                                                    alert('Bạn chưa nhập vào email!');
                                                } else {
                                                    if (!isEmail(email)) {
                                                        alert('Email nhập sai!');
                                                    } else {
                                                        $('.subscribe_to_newsletter-btn').attr('disabled', 'disabled');
                                                        $('.subscribe_to_newsletter-btn').css('opacity', '0.4');
                                                        $.ajax({
                                                            url: '/admin/ajax/contact/add',
                                                            type: 'POST',
                                                            data: {
                                                                email: email,
                                                                link: '<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>',
                                                            },
                                                            success: function (resp) {
                                                                if (resp.status == true) {
                                                                    alert(resp.msg);
                                                                    $('input[name=subscribe_to_newsletter_email]').val('');
                                                                    $('.subscribe_to_newsletter-btn').removeAttr('disabled');
                                                                    $('.subscribe_to_newsletter-btn').css('opacity', '1');
                                                                } else {
                                                                    alert(resp.msg);
                                                                    location.reload();
                                                                }
                                                            },
                                                            error: function () {
                                                                alert('Có lỗi xảy ra! Vui lòng load lại website & thử lại.');
                                                            }
                                                        });
                                                    }
                                                }
                                            });

                                            $('.news-letter-bx input, .news-letter-bx textarea').keypress(function (e) {
                                                if (e.which == 13) {
                                                    $('.subscribe_to_newsletter-btn').click();
                                                }
                                            });
                                        });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    {{--Hiển thị khối widget--}}
    <?php
    $widgets = CommonHelper::getFromCache('widgets_home_sidebar_left', ['widgets']);
    if (!$widgets) {
        $widgets = \Modules\ThemeEdu\Models\Widget::where('location', 'home_sidebar_left')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
        CommonHelper::putToCache('widgets_home_sidebar_left', $widgets, ['widgets']);
    }
    ?>
    @foreach($widgets as $widget)

        @if($widget->type == 'html')
            {!!$widget->content !!}

        @else
            @include(@json_decode($widget->config)->view, ['config' => (array) json_decode($widget->config)])
        @endif
    @endforeach
    {{--END: Hiển thị khối widget--}}

@endsection
@section('head_script')

    <link rel='stylesheet'
          href="{{ URL::asset('/public/frontend/themes/sach100/css/home_bg.css')}}" type='text/css' media='all'/>
    <style>

        .thong-ke .wpb_row {
            margin-bottom: 50px;
        }

        .thong-ke .stats-content {
            text-align: center;
        }

        .thong-ke .phan_tram {
            font-size: 50px;
            line-height: 37px;
            font-weight: lighter;
        }

        .thong-ke span.skill-count {
            width: unset;
            min-width: unset;
            display: inline-block !important;
        }


        .khoi-mon-hoc .featured_image_courses {
            overflow: hidden;
        }

        .khoi-mon-hoc .featured_image_courses img,
        .chuyen_de .featured_image_content img,
        .chuong .featured_image_courses img {
            transition: transform .2s;
        }

        .khoi-mon-hoc .featured_image_courses:hover img,
        .chuyen_de .featured_image_content:hover img,
        .chuong .featured_image_courses:hover img {
            transform: scale(1.2);
        }

        .featured_image_content img {
            height: 194px;
        }

        .wpb_row {
            background-size: cover;
            margin: 0;
        }

        .smartowl_shortcode_blog .shortcode_post_content a:hover {
            text-decoration: underline;
        }

        .colored-fullwidth-gradient {
            margin: 0;
        }

        .btn-warning hvr-float rippler rippler-default subscribe_to_newsletter-btn {
            background-color: #ffba41;
            height: 47px !important;
            transition: none !IMPORTANT;
            transform: none !important;
            border: 0 !important;
        }

        .subscribe.subscribe_to_newsletter-form .rippler-effect.rippler-div {
            display: none !important;
        }

        .bgr-email {
            background-color: #ffffff9e;
            opacity: 0.9;
            padding-bottom: 15px;
            padding-top: 15px;

        }

        @media (min-width: 768px) {
            .vc_custom_1461048085915 {
                padding: 80px 0px !important;
            }

            .vc_col_dashboard {
                width: 25% !important;
                float: left;
            }
        }

        @media (max-width: 768px) {
            .post-more-download {
                display: inline-block;
            }

            .thong-ke .vc_column_container > .vc_column-inner {
                padding: 0;
                margin-bottom: -30px;
            }

            .thong-ke .stats-block.statistics .stat-number.skill {
                min-height: unset;
                margin-bottom: 0;
            }

            .thong-ke .stats-content {
                margin-top: 0;
            }

            .thong-ke p, .thong-ke span {
                color: #000 !important;
                font-size: 14px !important;
                line-height: 20px !important;
            }

            .thong-ke span.skill-count,
            .thong-ke .phan_tram {
                color: #000 !important;
                font-weight: bold;
                padding-top: 10px;
                padding-bottom: 5px;
            }

            .khoi-mon-hoc .courses {
                padding: 5px;
            }

            .khoi-mon-hoc .courses-list .shortcode_course_content {
                margin: 0;
            }

            .wpb_wrapper h1 {
                font-size: 28px !important;
            }

            .stats-block.statistics .skill_image {
                width: 40px !important;
            }

            .vc_col_dashboard {
                width: 25% !important;
                float: left;
            }

            .chuyen_de .post-excerpt a {
                width: unset !important;
            }

            .chuyen_de h3.post-name a {
                font-size: 20px;
            }
        }

        rs-layer-wrap.rs-parallax-wrap.tp-parallax-container,
        .dddwrapper-layer {
            transform: none !important;
        }

        .gioi-thieu rs-sbg-wrap rs-sbg {
            background-image: url(/public/frontend/themes/sach100/images/cuon_sach.png) !important;
        }

    </style>
@endsection
@section('footer_script')

@endsection
