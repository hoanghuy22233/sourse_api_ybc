<style>
    .gioi-thieu rs-layer iframe {
        visibility: visible;
        vertical-align: middle;
        margin: auto;
        float: none;
        margin-top: 30%;
    }

    .gioi-thieu rs-module .rs-layer {
        text-align: center;
    }

    .content_info {
        top: 50px;
    }

    .btn_info {
        top: 85px
    }

    .intro_info {
        bottom: 200px;
    }

    .namne_info {
        bottom: 100px;
    }

    @media (max-width: 768px) {
        .content_info {
            top: 0px;
        }

        .btn_info {
            top: 20px;
        }

        .intro_info {
            bottom: 160px;
        }

        .name_info {
            bottom: 60px;
        }

        .hr_blue_info {
            top: 222px !important;
        }
    }
</style>
<div class="vc_row wpb_row vc_row-fluid vc_custom_1472730365985 gioi-thieu">
    <div class="container">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="wpb_wrapper">
                    <div class="wpb_revslider_element wpb_content_element">
                        <!-- START Magazine Posts REVOLUTION SLIDER 6.2.12 -->
                        <p class="rs-p-wp-fix"></p>
                        <rs-module-wrap id="rev_slider_30_2_wrapper" data-source="gallery"
                                        style="background:transparent;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
                            <rs-module id="rev_slider_30_2" style="" data-version="6.2.12">

                                <rs-slides>
                                    <?php

                                    $data = CommonHelper::getFromCache('banners_homepage_introduce', ['banners']);
                                    if (!$data) {
                                        $data = \Modules\ThemeSach100\Models\Banner::where('location', 'homepage_introduce')->where('status', 1)->get();
                                        CommonHelper::putToCache('banners_homepage_introduce', $data, ['banners']);
                                    }
                                    ?>
                                    @foreach($data as $v)
                                        <rs-slide data-key="rs-{{ $v->id }}" data-title="Slide"
                                                  data-anim="ei:d;eo:d;s:d;r:0;t:slideremovevertical;sl:d;">
                                            <img data-src="{{ asset('public/filemanager/userfiles/' . $v->image) }}"
                                                 alt="Slide" title="Homepage 1"
                                                 data-bg="c:#eeeeee;"
                                                 data-parallax="off" class="lazy rev-slidebg" data-no-retina>
                                            <!--
            -->
                                            <rs-layer id="slider-30-slide-{{ $v->id }}-layer-1" class="rs-pxl-10"
                                                      data-type="image" data-rsp_ch="on"
                                                      data-xy="x:c;xo:-308px,-230px,-230px,0;y:m;"
                                                      data-text="l:22;"
                                                      data-dim="w:615px,461px,461px,538px;h:800px,600px,600px,700px;"
                                                      data-frame_0="y:top;o:1;"
                                                      data-frame_1="e:power4.out;st:500;sp:1500;"
                                                      data-frame_999="y:bottom;o:0;st:w;sp:1000;"
                                                      style="z-index:6;">

                                                @if(strpos($v->intro, 'youtube') === false)
                                                    <img
                                                            data-src="{{ asset('public/filemanager/userfiles/' . $v->image) }}"
                                                            width="615" height="800" class="lazy"
                                                            data-no-retina>
                                                @else
                                                    <iframe width="560" height="315"
                                                            src="https://www.youtube.com/embed/{{ @explode('&', @explode('?v=', $v->intro)[1])[0] }}"
                                                            frameborder="0"
                                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                            allowfullscreen></iframe>
                                                @endif
                                            </rs-layer>
                                            <!--

            -->
                                            <rs-layer id="slider-30-slide-{{ $v->id }}-layer-2"
                                                      class="tp-shape tp-shapewrapper rs-pxl-12"
                                                      data-type="shape" data-rsp_ch="on"
                                                      data-xy="x:c;xo:307px,231px,231px,0;y:m;"
                                                      data-dim="w:615px,461px,461px,420px;h:800px,600px,600px,650px;"
                                                      data-frame_0="y:bottom;o:1;"
                                                      data-frame_1="e:power4.out;st:500;sp:1500;"
                                                      data-frame_999="y:top;o:0;st:w;sp:1000;"
                                                      style="z-index:9;background-color:rgba(255,255,255,1);">
                                            </rs-layer>
                                            <!--

            -->
                                            <rs-layer id="slider-30-slide-{{ $v->id }}-layer-3" class="rs-pxl-13"
                                                      data-type="text"
                                                      data-color="rgba(72,168,167,1)||rgba(196,162,110,1)||rgba(196,162,110,1)||rgba(196,162,110,1)"
                                                      data-rsp_ch="on"
                                                      data-xy="x:c;xo:307px,231px,231px,0;y:m;yo:-315px,-246px,-246px,-270px;"
                                                      data-text="s:20,13,13,13;l:20,13,13,13;ls:3px;fw:900;a:center;"
                                                      data-dim="w:615px,461px,461px,420px;"
                                                      data-frame_0="y:50px;"
                                                      data-frame_1="e:power4.out;st:550;sp:1500;"
                                                      data-frame_999="y:-50px;o:0;e:power2.in;st:w;sp:600;"
                                                      style="z-index:10;font-family:Roboto;">
                                            </rs-layer>
                                            <!--

            -->
                                            <rs-layer id="slider-30-slide-{{ $v->id }}-layer-4"
                                                      class="rs-pxl-14 name_info"
                                                      data-type="text" data-color="rgba(0,0,0,1)"
                                                      data-rsp_ch="on"
                                                      data-xy="x:c;xo:307px,231px,231px,0;y:m;yo:-179px,-141px,-141px,-154px;"
                                                      data-text="s:40,25,25,30;l:60,40,40,50;ls:7px;fw:900,700,700,700;a:center;"
                                                      data-dim="w:615px,461px,461px,420px;"
                                                      data-frame_0="o:1;"
                                                      data-frame_0_chars="x:-50px;o:0;skX:5px;"
                                                      data-frame_1="e:power4.out;st:550;sp:2500;"
                                                      data-frame_1_chars="d:3;"
                                                      data-frame_999="y:-50px;o:0;e:power2.in;st:w;sp:500;"
                                                      style="z-index:11;font-family:Roboto;">
                                                {{ $v->name }}
                                            </rs-layer>
                                            <!--

            -->
                                            <rs-layer id="slider-30-slide-{{ $v->id }}-layer-5"
                                                      class="tp-shape tp-shapewrapper rs-pxl-13 hr_blue_info"
                                                      data-type="shape" data-rsp_ch="on"
                                                      data-xy="x:c;xo:307px,231px,231px,0;y:m;yo:-46px,-40px,-40px,-41px;"
                                                      data-text="fw:700;" data-dim="w:100px;h:6px;"
                                                      data-frame_0="sX:0;o:1;" data-frame_0_mask="u:t;"
                                                      data-frame_1="e:power4.out;st:550;sp:1500;"
                                                      data-frame_1_mask="u:t;"
                                                      data-frame_999="y:-100%;o:0;st:w;sp:400;"
                                                      data-frame_999_mask="u:t;"
                                                      style="z-index:12;background-color:rgba(72,168,167,1);">
                                            </rs-layer>
                                            <!--

            -->

                                            <rs-layer id="slider-30-slide-{{ $v->id }}-layer-6"
                                                      class="rs-pxl-13 intro_info"
                                                      data-type="text" data-color="rgba(119,119,119,1)"
                                                      data-rsp_ch="on"
                                                      data-xy="x:c;xo:307px,231px,231px,0;y:m;yo:79px,76px,76px,91px;"
                                                      data-text="w:normal;s:18;l:30;a:center;"
                                                      data-dim="w:460px,380px,380px,320px;"
                                                      data-frame_0="y:50px;"
                                                      data-frame_1="e:power4.out;st:550;sp:1500;"
                                                      data-frame_999="y:-50px;o:0;e:power2.in;st:w;sp:400;"
                                                      style="z-index:13;font-family:Roboto;">
                                                @if(strpos($v->intro, 'youtube') === false)
                                                    {!! $v->intro !!}
                                                @endif
                                            </rs-layer>

                                            <!--


                                                 -->
                                            <rs-layer id="slider-30-slide-{{ $v->id }}-layer-7"
                                                      class="rs-pxl-13 content_info"
                                                      data-type="text" data-color="rgba(119,119,119,1)"
                                                      data-rsp_ch="on"
                                                      data-xy="x:c;xo:307px,231px,231px,0;y:m;yo:79px,76px,76px,91px;"
                                                      data-text="w:normal;s:18;l:30;a:center;"
                                                      data-dim="w:460px,380px,380px,320px;"
                                                      data-frame_0="y:50px;"
                                                      data-frame_1="e:power4.out;st:550;sp:1500;"
                                                      data-frame_999="y:-50px;o:0;e:power2.in;st:w;sp:400;"
                                                      style="z-index:13;font-family:Roboto;">{!! $v->content !!}
                                            </rs-layer>
                                            <!--

            --><a id="slider-30-slide-{{ $v->id }}-layer-7" class="rs-layer rev-btn rs-pxl-13 btn_info"
                  href="{{ $v->link }}" target="_blank" rel="noopener"
                  data-type="button"
                  data-color="rgba(72,168,167,1)||rgba(196,162,110,1)||rgba(196,162,110,1)||rgba(196,162,110,1)"
                  data-rsp_ch="on" data-xy="x:c;xo:306px,231px,231px,0;y:m;yo:228px,224px,224px,253px;"
                  data-text="s:14;l:40;ls:3px;fw:900;" data-padding="r:40;l:40;"
                  data-border="bos:solid;boc:rgba(72,168,167,1);bow:2px,2px,2px,2px;"
                  data-frame_0="y:20px;" data-frame_1="e:power4.out;st:1250;sp:1500;"
                  data-frame_999="y:-50px;o:0;e:power2.in;st:w;"
                  data-frame_hover="c:#fff;bgc:#48a8a7;boc:#48a8a7;bor:0px,0px,0px,0px;bos:solid;bow:2px,2px,2px,2px;oX:50;oY:50;sp:150;e:power2.inOut;"
                  style="z-index:14;font-family:Roboto;cursor:pointer;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">Xem
                                                thêm
                                            </a>
                                            <!--

            -->
                                            <rs-layer id="slider-30-slide-{{ $v->id }}-layer-8"
                                                      class="tp-largeshadow tp-shape tp-shapewrapper"
                                                      data-type="shape" data-rsp_ch="on"
                                                      data-xy="x:c;xo:0,1px,1px,942px;y:m;yo:0,0,0,-63px;"
                                                      data-dim="w:1228px,920px,920px,798px;h:798px,598px,598px,518px;"
                                                      data-frame_0="sX:0.8;sY:0.8;"
                                                      data-frame_1="e:power4.out;st:1250;sp:2000;"
                                                      data-frame_999="sX:0.8;sY:0.8;o:0;st:w;sp:1000;"
                                                      style="z-index:5;background-color:rgba(72,168,167,1);">
                                            </rs-layer>
                                            <!--

            -->
                                            <rs-layer id="slider-30-slide-{{ $v->id }}-layer-10"
                                                      class="tp-shape tp-shapewrapper rs-pxl-11"
                                                      data-type="shape" data-rsp_ch="on"
                                                      data-xy="x:c;xo:307px,231px,231px,719px;y:m;yo:0,0,0,-24px;"
                                                      data-dim="w:615px,461px,461px,400px;h:800px,600px,600px,520px;"
                                                      data-vbility="t,t,t,f"
                                                      data-frame_0="y:bottom;o:1;"
                                                      data-frame_1="e:power4.out;st:600;sp:1500;"
                                                      data-frame_999="y:top;o:0;st:w;sp:1000;"
                                                      style="z-index:8;background-color:rgba(245,245,245,1);">
                                            </rs-layer>
                                            <!--

            -->
                                            <rs-layer id="slider-30-slide-{{ $v->id }}-layer-11"
                                                      class="tp-shape tp-shapewrapper rs-pxl-10"
                                                      data-type="shape" data-rsp_ch="on"
                                                      data-xy="x:c;xo:307px,231px,231px,698px;y:m;yo:0,0,0,-40px;"
                                                      data-dim="w:615px,461px,461px,400px;h:800px,600px,600px,520px;"
                                                      data-vbility="t,t,t,f"
                                                      data-frame_0="y:bottom;o:1;"
                                                      data-frame_1="e:power4.out;st:700;sp:1500;"
                                                      data-frame_999="y:top;o:0;st:w;sp:1000;"
                                                      style="z-index:7;background-color:rgba(229,229,229,1);">
                                            </rs-layer>
                                            <!--
    -->
                                        </rs-slide>
                                    @endforeach
                                </rs-slides>
                            </rs-module>
                            <script type="text/javascript">
                                setREVStartSize({
                                    c: 'rev_slider_30_2',
                                    rl: [1240, 1024, 778, 480],
                                    el: [1000, 900, 700, 700],
                                    gw: [1400, 1200, 1000, 480],
                                    gh: [1000, 900, 700, 700],
                                    type: 'standard',
                                    justify: '',
                                    layout: 'fullwidth',
                                    mh: "0"
                                });
                                var revapi30,
                                    tpj;
                                jQuery(function () {
                                    tpj = jQuery;
                                    revapi30 = tpj("#rev_slider_30_2")
                                    if (revapi30 == undefined || revapi30.revolution == undefined) {
                                        revslider_showDoubleJqueryError("rev_slider_30_2");
                                    } else {
                                        revapi30.revolution({
                                            sliderLayout: "fullwidth",
                                            visibilityLevels: "1240,1024,778,480",
                                            gridwidth: "1400,1200,1000,480",
                                            gridheight: "1000,900,700,700",
                                            lazyType: "single",
                                            perspective: 600,
                                            perspectiveType: "local",
                                            editorheight: "1000,900,700,700",
                                            responsiveLevels: "1240,1024,778,480",
                                            progressBar: {
                                                disableProgressBar: true
                                            },
                                            navigation: {
                                                mouseScrollNavigation: false,
                                                onHoverStop: false,
                                                touch: {
                                                    touchenabled: true
                                                },
                                                arrows: {
                                                    enable: true,
                                                    style: "metis",
                                                    hide_onmobile: true,
                                                    hide_under: 768,
                                                    left: {
                                                        h_offset: 0
                                                    },
                                                    right: {
                                                        h_offset: 0
                                                    }
                                                }
                                            },
                                            parallax: {
                                                levels: [5, 10, 15, 20, 25, 30, 35, 40, -5, -10, -15, -20, -25, -30, -35, 55],
                                                type: "3D",
                                                origo: "slidercenter",
                                                disable_onmobile: true,
                                                ddd_bgfreeze: true,
                                                ddd_z_correction: 65
                                            },
                                            fallbacks: {
                                                allowHTML5AutoPlayOnAndroid: true
                                            },
                                        });
                                    }

                                });
                            </script>
                            <script>
                                var htmlDivCss = unescape(".tp-largeshadow%20%7B%20-webkit-box-shadow%3A%200px%2030px%2050px%200px%20rgba%2850%2C%2050%2C%2050%2C%200.25%29%3B%0A-moz-box-shadow%3A%20%20%20%200px%2030px%2050px%200px%20rgba%2850%2C%2050%2C%2050%2C%200.25%29%3B%0Abox-shadow%3A%20%20%20%20%20%20%20%20%200px%2030px%2050px%200px%20rgba%2850%2C%2050%2C%2050%2C%200.25%29%3B%20%7D");
                                var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                } else {
                                    var htmlDiv = document.createElement('div');
                                    htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                    document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                            <script>
                                var htmlDivCss = unescape("%23rev_slider_30_2_wrapper%20.metis.tparrows%20%7B%0A%20%20background%3A%23ffffff%3B%0A%20%20padding%3A10px%3B%0A%20%20transition%3Aall%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20width%3A60px%3B%0A%20%20height%3A60px%3B%0A%20%20box-sizing%3Aborder-box%3B%0A%20%7D%0A%20%0A%20%23rev_slider_30_2_wrapper%20.metis.tparrows%3Ahover%20%7B%0A%20%20%20background%3Argba%28255%2C255%2C255%2C0.75%29%3B%0A%20%7D%0A%20%0A%20%23rev_slider_30_2_wrapper%20.metis.tparrows%3Abefore%20%7B%0A%20%20color%3A%23000000%3B%20%20%0A%20%20%20transition%3Aall%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%7D%0A%20%0A%20%23rev_slider_30_2_wrapper%20.metis.tparrows%3Ahover%3Abefore%20%7B%0A%20%20%20transform%3Ascale%281.5%29%3B%0A%20%20%7D%0A%20%0A");
                                var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                } else {
                                    var htmlDiv = document.createElement('div');
                                    htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                    document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                            <script>
                                var htmlDivCss = unescape("%0A%0A%0A");
                                var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                } else {
                                    var htmlDiv = document.createElement('div');
                                    htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                    document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                        </rs-module-wrap>
                        <!-- END REVOLUTION SLIDER -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>