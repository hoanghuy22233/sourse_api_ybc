<?php
$data = CommonHelper::getFromCache('banners_banner_slides', ['banners']);
if (!$data) {
    $data = @\Modules\ThemeSach100\Models\Banner::where('location', 'banner_slides')->where('status', 1)->get();
    CommonHelper::putToCache('banners_banner_slides', $data, ['banners']);
}
?>
@if(isset($data[0]))
    <div class="homepage_banner">
        <a href="{{ $data[0]->link }}">
            <img src="{{ asset('public/filemanager/userfiles/' . $data[0]->image) }}" style="width: 100%;"/>
        </a>
    </div>
@endif