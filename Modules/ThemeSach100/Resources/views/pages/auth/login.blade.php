@extends('themesach100::layouts.default')
@section('main_content')

    <link rel="stylesheet" href="{{ URL::asset('/public/frontend/themes/sach100/themes/smartowl/css_login/style.css') }}">

    <section>
        <div class="gap no-gap signin whitish medium-opacity">
            <div class="bg-image"
                 style="background-image:url(/public/frontend/themes/sach100/themes/smartowl/images/theme-bg.jpg)"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="big-ad">
                            <figure><a href="/"><img class="lazy"
                                            data-src="{{ URL::asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                                            alt="{{ @$settings['name'] }}" style="max-width: 150px;"></a></figure>
                            <h1>Chào mừng bạn đến với {{ @$settings['name'] }}</h1>
                            <p>
                                {!! @$settings['web_description'] !!}
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="we-login-register" style="background-color: #003300;">
                            <div class="form-title">
                                <i class="fa fa-key"></i>Đăng nhập
                                <span>Đừng quên đăng nhập sẽ giúp bạn dễ dàng lưu lại thành tích và quá trình làm bài của bản thân!</span>
                            </div>
                            @if (Session('success'))
                                <div class="alert bg-success" role="alert">
                                    <p style="color: red"><b>{!!session('success')!!}</b></p>
                                </div>
                            @endif
                            @if(Session::has('message') && !Auth::check())
                                <div class="alert text-center text-white " role="alert"
                                     style=" margin: 0; font-size: 16px;">
                                    <a href="#" style="float:right;" class="alert-close"
                                       data-dismiss="alert">&times;</a>
                                    <p style="color: red"><b>{{ Session::get('message') }}</b></p>
                                </div>
                            @endif
                            <form class="we-form" method="post" action="/dang-nhap">
                                <div class="we-form">
                                    <input class="text-black" type="text" name="email" placeholder="Email hoặc điện thoại">
                                    @if($errors->has('email'))
                                        <p style="color: red"><b>{{ $errors->first('email') }}</b></p>
                                    @endif
                                </div>
                                <div class="we-form">
                                    <input type="password" name="password" placeholder="Password">
                                    @if($errors->has('password'))
                                        <p style="color: red"><b>{{ $errors->first('password') }}</b></p>
                                    @endif
                                </div>
                                <label><input type="checkbox" checked name="remember_me"> Nhớ đăng nhập</label>
                                <button type="submit" data-ripple="">Đăng nhập</button>
                                <a class="forgot underline" href="/quen-mat-khau" title="">Quên mật khẩu?</a>
                            </form>
                            <a class="with-smedia facebook" href="/login/facebook/redirect/" title="" data-ripple="">Đăng
                                nhập bằng facebook</a>
                            <a class="with-smedia google" href="/login/google/redirect/" title="" data-ripple="">Đăng
                                nhập bằng google</a>
                            <span>Bạn chưa có tài khoản ? <a class="we-account underline" href="/dang-ky"
                                                             title="">Đăng ký</a></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
@section('head_script')
    @include('themesach100::pages.auth.partials.login_register_css')
    <style>
        .we-login-register input {
            color: #000;
        }
    </style>
@endsection