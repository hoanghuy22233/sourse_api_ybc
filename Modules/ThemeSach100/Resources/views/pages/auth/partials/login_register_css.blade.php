<style>
    @media(max-width: 768px) {
        footer {
            display: inline-block !important;
        }
        .vc_col-md-4 {
            display: inline-block !important;
        }
        .big-ad {
            display: none !important;
        }
        .we-login-register::before {
            display: none !important;
        }
        .we-login-register {
            padding: 40px 30px 50px;
        }
    }
</style>