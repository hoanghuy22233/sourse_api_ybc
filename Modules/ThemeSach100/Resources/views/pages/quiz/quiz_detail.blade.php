@extends('themesach100::layouts.default')
@section('baikiemtra')
    active
@endsection
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>


        @include('themesach100::template.top_bar')
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themesach100::template.menu')
                                </div><!-- user profile banner  -->
                                <div class="col-lg-3">
                                    <aside class="sidebar static left">
                                        <div class="widget stick-widget">
                                            <h4 class="widget-title">Top thành viên đạt điểm cao</h4>
                                            <ul class="followers ps-container ps-theme-default ps-active-y"
                                                data-ps-id="42e41a80-d050-998e-adde-c3912d0eac78">
                                                <?php
                                                $quiz_logs = CommonHelper::getFromCache('quizz_logs_scores_quizz_id' . $quiz->id, ['quiz_log']);
                                                if (!$quiz_logs) {
                                                    $quiz_logs = \Modules\ThemeSach100\Models\QuizLog::select('student_id', 'scores')->where('quizz_id', $quiz->id)->where('scores', '<>', 0)->where('scores', '<>', null)->orderBy('accumulated_points', 'desc')->limit(5)->get();
                                                    CommonHelper::putToCache('quizz_logs_scores_quizz_id' . $quiz->id, $quiz_logs, ['quiz_log']);
                                                }
                                                ?>
                                                @foreach($quiz_logs as $quiz_log)
                                                    <?php
                                                    $highScoreStudent = $quiz_log->student;
                                                    ?>
                                                    @if(is_object($highScoreStudent))
                                                        <li>
                                                            <figure><img class="lazy"
                                                                         data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$highScoreStudent->image, null, null) }}"
                                                                         alt="">
                                                            </figure>
                                                            <div class="friend-meta">
                                                                <h4><a href="/profile/{{ @$highScoreStudent->id }}"
                                                                       title="">{{ @$highScoreStudent->name }}</a></h4>
                                                                <a rel="nofollow" title="Điểm tích lũy đạt được"
                                                                   class="underline">{{ $quiz_log->scores }} điểm</a>
                                                            </div>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div><!-- Shortcuts -->
                                    </aside>
                                </div><!-- sidebar -->
                                <div class="col-lg-9">
                                    <div class="central-meta" style="height: 1000px;">
                                    <span style="line-height: 3.2; font-size: 12px;" class="alert alert-danger">
                                        {!! @$msg !!}
                                    </span>
                                        <iframe src="{{ $quiz->link_google_form }}"
                                                width="auto" height="100%"
                                                style="border:none;overflow:hidden;margin-top: 20px;" scrolling="true"
                                                frameborder="0" allowTransparency="true"
                                                allow="encrypted-media"></iframe>
                                    </div>
                                </div><!-- centerl meta -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>
@endsection