@extends('themesach100::layouts.default')
@section('khoahoc')
    active
@endsection
@section('main_content')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <style>
            .name_contact {
                margin-left: 5%;
                margin-top: 5%;
                width: 90%;
                height: 50%;
            }

            .mess_contact {
                margin-left: 5%;
                margin-top: 5%;
                width: 90%;
                height: 100%;
            }

            .input_contact {
                height: 45px;
            }

            .input_mess_contact {
                height: 130px;
            }
        </style>

        @include('themesach100::template.top_bar')

        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
                                    @include('themesach100::template.menu')
                                </div><!-- user profile banner  -->
                                @include('themesach100::pages.course.partials.sidebar_left')
                                <div class="col-lg-9">
                                    <div class="row merged20">
                                        <?php
                                        //   Lấy ra các khóa học đã mua
                                        $course_buyed = [];
                                        if (\Auth::guard('student')->check()) {
                                            $course_ids = [];
                                            foreach($courses as $course){
                                                $course_ids[] = $course->id;
                                            }

                                            $course_buyed = \Modules\ThemeSach100\Models\Order::where('student_id', @\Auth::guard('student')->user()->id)->whereNotNull('student_id')
                                                ->whereIn('course_id', $course_ids)->where('status', 1)->pluck('course_id')->toArray();
                                        }
                                        ?>
                                        @foreach($courses as $course)
                                            <div class="col-lg-6 col-md-6">
                                                @include('themesach100::partials.course_item')
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-12 paginatee">
                                            {{ $courses->appends(Request::all())->links() }}
                                        </div>
                                    </div>
                                </div><!-- centerl meta -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- content -->
    </div>
@endsection