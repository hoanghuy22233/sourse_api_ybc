@extends('themesach100::layouts.default')
@section('main_content')
    @include('themesach100::partials.banner_header')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>
        <section>
            <div class="gap2 bg-white">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="col-lg-12">
                                    <div id="middle">
                                        <div class="child-middle" style="padding: 100px 0; text-align: center; border-top: 1px solid #ddd;color: red;">
                                            <?php
//                                            $subject_ids = explode('|', @$_GET['subjects']);
//                                            $subjects = \Modules\ThemeSach100\Models\Category::select('id', 'name')->whereIn('id', $subject_ids)->get();
                                            ?>
                                                <h3 class="alert alert-success">
                                                    Bạn đã đăng kí thành công! Bạn vui lòng vào phần "Luyện Thi Ngay" để bắt đầu ôn luyện nha!</h3>
                                                {{--<a href="/tao-bai-thi" style="font-family: 'Titillium Web', Arial, Helvetica, 'Nimbus Sans L', sans-serif; font-style: normal;">
                                                    Vào thi ngay</a>--}}
                                        </div>
                                    </div>
                                </div><!-- nave list -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>

@endsection