@extends('themesach100::layouts.default')
@section('main_content')
    <div id="primary" class="no-padding content-area no-sidebar" style="background-image: url({{ asset('public/filemanager/userfiles/' . @$settings['intermediaries']) }});">
        <div class="container">
            <article id="post-1799" class="post-1799 page type-page status-publish hentry" style="margin-top: 50px;">
                <div class="entry-content">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="title-subtile-holder"><h1 class="section-title "
                                                                  style="cursor: pointer;">Chọn trình độ</h1>
                                <div class="section-border "></div>
                            </div>
                            <div class="wpb_wrapper">
                                <?php
                                $subjects = \Modules\ThemeSach100\Models\Category::select('id', 'name', 'slug')->where('status', 1)->where('type', 10)->get();
                                ?>
                                @foreach($subjects as $sj)
                                    <a class="alert @if(@$sj->id == $subject->id) alert-success @else alert-default @endif"
                                       data-animate="fadeInLeft"
                                       href="/{{ $sj->slug }}">{{ $sj->name }}
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <form action="/tao-bai-thi" method="POST">
                        {!! csrf_field() !!}

                        <input name="subject_id" value="{{ @$subject->id }}" type="hidden">
                        <input name="login_back_url" value="{{ @$_SERVER['REQUEST_URI'] }}" type="hidden">

                        <div class="title-subtile-holder"><h1 class="section-title "
                                                              style="cursor: pointer;">Chọn loại đề</h1>
                            <div class="section-border "></div>
                        </div>
                        <div class="wpb_wrapper">
                            <div class="wpb_tabs wpb_content_element" data-interval="0">
                                <div class="wpb_wrapper wpb_tour_tabs_wrapper ui-tabs vc_clearfix ui-widget ui-widget-content ui-corner-all">
                                    <ul class="wpb_tabs_nav ui-tabs-nav vc_clearfix ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all"
                                        role="tablist">
                                        <li class="ui-state-default ui-corner-top {{ !isset($_GET['tab_active']) ? 'ui-tabs-active ui-state-active' : '' }}"
                                            role="tab" tabindex="-1"
                                            aria-controls="tab-1434463308696-0-9" aria-labelledby="ui-id-3" data-tab="1"
                                            aria-selected="true" aria-expanded="true"><a href="#tab-1434463308696-0-9"
                                                                                         class="ui-tabs-anchor"
                                                                                         role="presentation"
                                                                                         tabindex="-1" id="ui-id-3">Đề
                                                chuẩn JLPT</a></li>
                                        <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1"
                                            aria-controls="tab-1434463308696-0-9" aria-labelledby="ui-id-3" data-tab="2"
                                            aria-selected="false" aria-expanded="false"><a href="#tab-1434463308696-0-9"
                                                                                           class="ui-tabs-anchor"
                                                                                           role="presentation"
                                                                                           tabindex="-1" id="ui-id-3">Đề
                                                Test nhanh</a></li>
                                        <li class="ui-state-default ui-corner-top {{ @$_GET['tab_active'] == 'tab3' ? 'ui-tabs-active ui-state-active' : '' }}" role="tab" tabindex="-1"
                                            aria-controls="tab-1434463308696-0-9" aria-labelledby="ui-id-3" data-tab="3"
                                            aria-selected="false" aria-expanded="false"><a href="#tab-1434463308696-0-9"
                                                                                           class="ui-tabs-anchor"
                                                                                           role="presentation"
                                                                                           tabindex="-1" id="ui-id-3">Đề
                                                tăng cường kỹ năng</a></li>

                                    </ul>
                                    <div id="tab-group">
                                        <div id="tab-1"
                                             class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix ui-widget-content ui-corner-bottom"
                                             aria-labelledby="ui-id-3" role="tabpanel" aria-hidden="true"
                                             style="{{ !isset($_GET['tab_active']) ? 'display: block;' : 'display: none;' }}">

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <button style="color: black; background-color: #b5d1dc;" type="submit" class="alert alert-success "
                                                            data-animate="fadeInLeft">
                                                        Vào Thi
                                                    </button>

                                                </div>
                                            </div>
                                        </div>
                                        <div id="tab-2"
                                             class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix ui-widget-content ui-corner-bottom"
                                             aria-labelledby="ui-id-3" role="tabpanel" aria-hidden="true"
                                             style="display: none;">

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <input style="background-color: #acccdd;" type="submit" class="alert alert-success " value="Cơ bản" name="level"
                                                           data-animate="fadeInLeft">
                                                    <input style="background-color: #6ea0cd;" type="submit" class="alert alert-info " value="Nghiệp dư" name="level"
                                                           data-animate="fadeInLeft">
                                                    <input style="background-color: #3868a0;" type="submit" class="alert alert-warning " value="Master" name="level"
                                                           data-animate="fadeInLeft">
                                                </div>
                                            </div>
                                        </div>
                                        <div id="tab-3"
                                             class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix ui-widget-content ui-corner-bottom"
                                             aria-labelledby="ui-id-3" role="tabpanel" aria-hidden="true"
                                             style="{{ @$_GET['tab_active'] == 'tab3' ? 'display: block;' : 'display: none;' }}">

                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <?php
                                                    $chapters = \Modules\Sach100Exam\Models\Category::select('id', 'name')->where('status', 1)->where('parent_id', @$subject->id)->orderBy('order_no', 'desc')->get();
                                                    ?>
                                                    @foreach($chapters as $chapter)
                                                        <input type="submit" class="alert alert-success " value="{{ $chapter->name }}" name="chapter_name"
                                                                data-animate="fadeInLeft">
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        {{--<div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="title-subtile-holder"><h1 class="section-title "
                                                                              style="cursor: pointer;">Bạn muốn vào
                                                phòng thi nào?</h1>
                                            <div class="section-border "></div>
                                            <div class="section-subtitle "></div>
                                        </div>
                                        <div class="group-option" style="display: none;">
                                            <button type="submit" class="alert alert-success "
                                                    data-animate="fadeInLeft">
                                                Đề N2 chuẩn JLPT
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="title-subtile-holder"><h1 class="section-title "
                                                                              style="cursor: pointer;">Vào phòng
                                                thi ngay</h1>
                                            <div class="section-border "></div>
                                            <div class="section-subtitle ">Đề test nhanh N2 30'
                                            </div>
                                        </div>
                                        <div class="group-option" style="display: none;">
                                            <button type="submit" class="alert alert-success "
                                                    data-animate="fadeInLeft">Cơ bản
                                            </button>
                                            <button type="submit" class="alert alert-info " data-animate="fadeInLeft">
                                                Nghiệp dư
                                            </button>
                                            <button type="submit" class="alert alert-warning "
                                                    data-animate="fadeInLeft">Master
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="title-subtile-holder"><h1 class="section-title "
                                                                              style="cursor: pointer;">Đề tăng cường
                                                kỹ năng</h1>
                                            <div class="section-border "></div>
                                        </div>
                                        <div class="group-option" style="display: none;">
                                            <button type="submit" class="alert alert-success "
                                                    data-animate="fadeInLeft">Từ vựng -
                                                Kanji
                                            </button>
                                            <button type="submit" class="alert alert-info " data-animate="fadeInLeft">
                                                Ngữ pháp
                                            </button>
                                            <button type="submit" class="alert alert-warning "
                                                    data-animate="fadeInLeft">Đọc hiểu
                                            </button>
                                            <button type="submit" class="alert alert-danger" data-animate="fadeInLeft">
                                                Nghe hiểu
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--}}
                    </form>
                </div><!-- .entry-content -->
            </article>
        </div>
    </div>

@endsection
@section('head_script')
    <style>
        .alert {
            display: inline-block;
            font-weight: bold;
            padding: 15px 30px;
            margin-right: 20px;
        }

        .wpb_wrapper {
            text-align: center;
        }

        .alert.alert-default {
            border: 2px solid #000000;
        }
        .entry-content form {
            max-width: 700px;
            margin: auto;
            margin-bottom: 50px;
        }

        .wpb_tabs .wpb_tour_tabs_wrapper .wpb_tabs_nav a {
            border-right: 1px solid #dfe5e9;
        }

        .wpb_wrapper a.alert-success {
            color: #fff;
        }
        .wpb_wrapper a{
            color: #4c6b94;
        }
        .alert-success {
            background-color: #4c6b94;
        }

        li.ui-state-default.ui-corner-top {
            float: none;
            display: inline-block;
        }

        .wpb_content_element .wpb_tabs_nav {
            border-right: 0;
        }

        .wpb_content_element .wpb_tabs_nav li:last-child a {
            border-right: 0;
            margin: 0;
        }

        .wpb_content_element .wpb_tabs_nav li {
            width: 33%;
            float: left;
        }

        .wpb_content_element .wpb_tabs_nav li a {
            font-weight: bolder;
        }
        @media(max-width: 768px) {
            .wpb_content_element .wpb_tabs_nav li {
                width: 100%;
            }
            article#post-1799 a.alert {
                width: 26%;
                margin: 5px !important;
            }
            .wpb_wrapper a{
                color: #4c6b94;
            }
            .alert-success {
                background-color: #4c6b94;
            }
        }
    </style>
@endsection
@section('footer_script')
    <script src="{{ URL::asset('public/libs/jquery-3.4.0.min.js') }}"></script>
    <script>
        $('.wpb_tabs_nav li').click(function () {
            var id = $(this).data('tab');

            $(this).addClass('ui-tabs-active ui-state-active');
            $(this).siblings().removeClass('ui-tabs-active ui-state-active');

            $('#tab-group').find('#tab-' + id).show();
            $('#tab-group').find('#tab-' + id).siblings().hide();
        });
    </script>
    {{--<script>
        $('.section-title').click(function () {
            $(this).parents('.wpb_wrapper').find('.group-option').slideToggle(200);
        });
    </script>--}}
@endsection