@extends('themesach100::layouts.default')
@section('tailieu')
    active
@endsection
@section('main_content')
    @include('themesach100::partials.banner_header')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row khoa-hoc-content">
                        <?php
                        $widget = CommonHelper::getFromCache('widgets_khoa_hoc', ['widgets']);
                        if (!$widget) {
                            $widget = \Modules\ThemeEdu\Models\Widget::where('location', 'khoa_hoc')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->first();
                            CommonHelper::putToCache('widgets_khoa_hoc', $widget, ['widgets']);
                        }
                        ?>
                        @include(@json_decode($widget->config)->view, ['config' => (array) json_decode($widget->config)])
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="central-meta">
                        <div class="color-palet">
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-3">
                                    <div class="color-box dark-gry-box">
                                        <a href="/blog/chi-tiet-khoa-on-luyen-mon-toan.html">Chi tiết khóa ôn luyện Toán</a>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-3">
                                    <div class="color-box orange-box">
                                        <a href="/blog/chi-tiet-khoa-on-luyen-mon-vat-ly.html">Chi tiết khóa ôn luyện Vật Lý</a>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-3">
                                    <div class="color-box blue-box">
                                        <a href="/blog/chi-tiet-khoa-on-luyen-mon-hoa-hoc.html">Chi tiết khóa ôn luyện Hóa Học</a>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-3">
                                    <div class="color-box purple-box">
                                        <a href="/blog/chi-tiet-khoa-on-luyen-mon-sinh-hoc.html">Chi tiết khóa ôn luyện Sinh Học</a>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-3">
                                    <div class="color-box red-box">
                                        <a href="/blog/chi-tiet-khoa-on-luyen-mon-tieng-anh.html">Chi tiết khóa ôn luyện Tiếng Anh</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('head_script')
    <style>
        .color-palet a {
            font-weight: bolder;
        }
        .col-lg-4 {
            display: inline-block;
            float: left;
        }

        .khoa-hoc-content ul {
            display: inline-block;
            list-style: outside none none;
            margin-bottom: 20px;
            padding-left: 0;
            text-align: left;
            width: 100%;
        }

        .khoa-hoc-content ul li {
            display: inline-block;
            font-size: 14px;
            margin-bottom: 15px;
            width: 100%;
        }

        .khoa-hoc-content ul li i {
            color: red;
            font-size: 10px;
            margin-right: 10px;
        }
    </style>
@endsection