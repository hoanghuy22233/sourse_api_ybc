@if($question->a == null)
@else
    <div class="exam_item exam_item-{{ $k }}{{ isset($is_child) ? '_' . ($j + 1) : '' }} @if(isset($exam_data[$question->id]) && @$exam_data[$question->id] != null) active @endif"
         data-key="{{ $k }}{{ isset($is_child) ? '_' . ($j + 1) : '' }}"
         data-id="{{ $question->id }}">
        {{ $k }}{{ isset($is_child) ? '.' . ($j + 1) : '' }}. {{ strtoupper(@$exam_data[$question->id]) }}
    </div>
@endif