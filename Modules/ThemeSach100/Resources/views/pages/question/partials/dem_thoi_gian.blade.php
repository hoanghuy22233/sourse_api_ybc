<script>
    <?php
    $deadline = strtotime($exam->created_at2) + $this_time * 60 /*- 80*60*/;
    ?>
    var this_time = '{{ date('d/m/Y H:i:s') }} | {{ $this_time }} | {{ $exam->created_at2 }} | {{ date('d/m/Y H:i:s', $deadline) }}';
    // Set the date we're counting down to
    var countDownDate = new Date("{{ date('M', $deadline) }} {{ date('d', $deadline) }}, {{ date('Y', $deadline) }} {{ date('H:i:s', $deadline) }}").getTime();

    {{--var countDownDate = new Date("{{ date('M', (time() + 5)) }} {{ date('d', (time() + 5)) }}, {{ date('Y', (time() + 5)) }} {{ date('H:i:s', (time() + 5)) }}").getTime();--}}

    // Update the count down every 1 second
    var x = setInterval(function () {

// Get today's date and time
        var now = new Date().getTime();

// Find the distance between now and the count down date
        var distance = countDownDate - now;

// Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

// Display the result in the element with id="deadline_time"
        document.getElementById("deadline_time").innerHTML = hours + ":"
            + minutes + ":" + seconds + "";

// If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            // document.getElementById("deadline_time").innerHTML = "Hết giờ";
            // $('#expired').css('display', 'block');
            $('input[name=het_gio]').val('1');
            $('button[type=submit]').click();
        }
    }, 1000);
</script>