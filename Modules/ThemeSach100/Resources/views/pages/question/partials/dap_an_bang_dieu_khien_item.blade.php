@if($question->a == null)
@else
    <div class="exam_item @if(@$exam_data[$question->id] != null && $question->answer != strtolower(@$exam_data[$question->id])) <?php $count_question_false++;?> sai @elseif($question->answer == strtolower(@$exam_data[$question->id])) <?php $count_question_true++;?> dung @endif"
         data-id="{{ $question->id }}">
        {{ $k }}{{ isset($is_child) ? '.' . ($j + 1) : '' }}. {{ @$dap_an[strtolower(@$exam_data[$question->id])] }}
    </div>
@endif