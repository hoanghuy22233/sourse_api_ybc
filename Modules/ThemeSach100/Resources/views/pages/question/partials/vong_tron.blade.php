<style>
    @import url(https://fonts.googleapis.com/css?family=Abel);
    /* ------- DO NOT EDIT BELOW --------- */
    .chart-one {
        width: 200px;
        height: 200px;
        margin: 0;
        position: relative;
    }

    .chart-one.animate svg .circle-foreground {
        animation: offset 3s ease-in-out forwards;
        animation-delay: 1s;
    }

    .chart-one.animate figcaption:after {
        animation: chart-one-label 3s steps(25) forwards;
        animation-delay: 1s;
    }

    .chart-one svg {
        width: 100%;
        height: 100%;
    }

    .chart-one svg .circle-background, .chart-one svg .circle-foreground, .chart-one svg .chart-two svg .circle-foreground, .chart-two svg .chart-one svg .circle-foreground, .chart-one svg .chart-three svg .circle-foreground, .chart-three svg .chart-one svg .circle-foreground {
        r: 99.5px;
        cx: 50%;
        cy: 50%;
        fill: none;
        stroke: #305556;
        stroke-width: 1px;
    }

    .chart-one svg .circle-foreground {
        stroke: #79be9b;
        stroke-dasharray: 156.215px 624.86px;
        stroke-dashoffset: 156.215px;
        stroke-linecap: round;
        transform-origin: 50% 50%;
        transform: rotate(-90deg);
    }

    .chart-one figcaption {
        display: inline-block;
        width: 100%;
        height: 2.5rem;
        overflow: hidden;
        text-align: center;
        color: #c6e8d7;
        position: absolute;
        top: calc(50% - 1.25rem);
        left: 0;
        font-size: 0;
    }

    .chart-one figcaption:after {
        display: inline-block;
        /*content: '0%\a1%\a2%\a3%\a4%\a5%\a6%\a7%\a8%\a9%\a10%\a11%\a12%\a13%\a14%\a15%\a16%\a17%\a18%\a19%\a20%\a21%\a22%\a23%\a24%\a25%\a26%\a27%\a28%\a29%\a30%\a31%\a32%\a33%\a34%\a35%\a36%\a37%\a38%\a39%\a40%\a41%\a42%\a43%\a44%\a45%\a46%\a47%\a48%\a49%\a50%\a51%\a52%\a53%\a54%\a55%\a56%\a57%\a58%\a59%\a60%\a61%\a62%\a63%\a64%\a65%\a66%\a67%\a68%\a69%\a70%\a71%\a72%\a73%\a74%\a75%\a76%\a77%\a78%\a79%\a80%\a81%\a82%\a83%\a84%\a85%\a86%\a87%\a88%\a89%\a90%\a91%\a92%\a93%\a94%\a95%\a96%\a97%\a98%\a99%\a100%\a';*/
        white-space: pre;
        font-size: 2.5rem;
        line-height: 2.5rem;
    }

    @keyframes chart-one-label {
        100% {
            transform: translateY(-62.5rem);
        }
    }

    .chart-two {
        width: 200px;
        height: 200px;
        margin: 0;
        position: relative;
    }

    .chart-two.animate svg .circle-foreground {
        animation: offset 3s ease-in-out forwards;
        animation-delay: 1s;
    }

    .chart-two.animate figcaption:after {
        animation: chart-two-label 3s steps(50) forwards;
        animation-delay: 1s;
    }

    .chart-two svg {
        width: 100%;
        height: 100%;
    }

    .chart-two svg .circle-background, .chart-two svg .chart-one svg .circle-foreground, .chart-one svg .chart-two svg .circle-foreground, .chart-two svg .circle-foreground, .chart-two svg .chart-three svg .circle-foreground, .chart-three svg .chart-two svg .circle-foreground {
        r: 52.5px;
        cx: 50%;
        cy: 50%;
        fill: none;
        stroke: #305556;
        stroke-width: 15px;
    }

    .chart-two svg .circle-foreground {
        stroke: #f36c62;
        stroke-dashoffset: 290.45px;
        stroke-linecap: round;
        transform-origin: 50% 50%;
        transform: rotate(-90deg);
    }

    .chart-two figcaption {
        display: inline-block;
        width: 100%;
        height: 2.5rem;
        overflow: hidden;
        text-align: center !important;
        color: #c6e8d7;
        position: absolute;
        top: calc(50% - 1.25rem);
        left: 0;
        font-size: 55px;
    }

    .chart-two figcaption:after {
        display: inline-block;
        /*content: '0%\a1%\a2%\a3%\a4%\a5%\a6%\a7%\a8%\a9%\a10%\a11%\a12%\a13%\a14%\a15%\a16%\a17%\a18%\a19%\a20%\a21%\a22%\a23%\a24%\a25%\a26%\a27%\a28%\a29%\a30%\a31%\a32%\a33%\a34%\a35%\a36%\a37%\a38%\a39%\a40%\a41%\a42%\a43%\a44%\a45%\a46%\a47%\a48%\a49%\a50%\a51%\a52%\a53%\a54%\a55%\a56%\a57%\a58%\a59%\a60%\a61%\a62%\a63%\a64%\a65%\a66%\a67%\a68%\a69%\a70%\a71%\a72%\a73%\a74%\a75%\a76%\a77%\a78%\a79%\a80%\a81%\a82%\a83%\a84%\a85%\a86%\a87%\a88%\a89%\a90%\a91%\a92%\a93%\a94%\a95%\a96%\a97%\a98%\a99%\a100%\a';*/
        white-space: pre;
        font-size: 2.5rem;
        line-height: 2.5rem;
    }

    @keyframes chart-two-label {
        100% {
            transform: translateY(-125rem);
        }
    }

    .chart-three {
        width: 200px;
        height: 200px;
        margin: 0;
        position: relative;
    }

    .chart-three.animate svg .circle-foreground {
        animation: offset 3s ease-in-out forwards;
        animation-delay: 1s;
    }

    .chart-three.animate figcaption:after {
        animation: chart-three-label 3s steps(75) forwards;
        animation-delay: 1s;
    }

    .chart-three svg {
        width: 100%;
        height: 100%;
    }

    .chart-three svg .circle-background, .chart-three svg .chart-one svg .circle-foreground, .chart-one svg .chart-three svg .circle-foreground, .chart-three svg .chart-two svg .circle-foreground, .chart-two svg .chart-three svg .circle-foreground, .chart-three svg .circle-foreground {
        r: 87.5px;
        cx: 50%;
        cy: 50%;
        fill: none;
        stroke: #305556;
        stroke-width: 25px;
    }

    .chart-three svg .circle-foreground {
        stroke: #389967;
        stroke-dasharray: 412.125px 549.5px;
        stroke-dashoffset: 412.125px;
        stroke-linecap: round;
        transform-origin: 50% 50%;
        transform: rotate(-90deg);
    }

    .chart-three figcaption {
        display: inline-block;
        width: 100%;
        height: 2.5rem;
        overflow: hidden;
        text-align: center;
        color: #c6e8d7;
        position: absolute;
        top: calc(50% - 1.25rem);
        left: 0;
        font-size: 0;
    }

    .chart-three figcaption:after {
        display: inline-block;
        /*content: '0%\a1%\a2%\a3%\a4%\a5%\a6%\a7%\a8%\a9%\a10%\a11%\a12%\a13%\a14%\a15%\a16%\a17%\a18%\a19%\a20%\a21%\a22%\a23%\a24%\a25%\a26%\a27%\a28%\a29%\a30%\a31%\a32%\a33%\a34%\a35%\a36%\a37%\a38%\a39%\a40%\a41%\a42%\a43%\a44%\a45%\a46%\a47%\a48%\a49%\a50%\a51%\a52%\a53%\a54%\a55%\a56%\a57%\a58%\a59%\a60%\a61%\a62%\a63%\a64%\a65%\a66%\a67%\a68%\a69%\a70%\a71%\a72%\a73%\a74%\a75%\a76%\a77%\a78%\a79%\a80%\a81%\a82%\a83%\a84%\a85%\a86%\a87%\a88%\a89%\a90%\a91%\a92%\a93%\a94%\a95%\a96%\a97%\a98%\a99%\a100%\a';*/
        white-space: pre;
        font-size: 2.5rem;
        line-height: 2.5rem;
    }

    @keyframes chart-three-label {
        100% {
            transform: translateY(-187.5rem);
        }
    }

    @keyframes offset {
        100% {
            stroke-dashoffset: 0;
        }
    }

    /* codepen styling only */

    figure {
        margin: 1rem !important;
    }

    figcaption {
        font-family: 'Abel', sans-serif;
    }

    .chart-two figcaption small {
        font-size: 15px;
        color: #000;
        font-weight: bold;
        top: -19px;
        position: relative;
    }

    .chart-two figcaption {
        display: inline-block;
        width: 100%;
        height: 137px;
        overflow: hidden;
        text-align: center;
        color: #000;
        position: absolute;
        top: 67px;
        left: 0;
        font-size: 29px;
        line-height: 42px;
    }

    .vt-item {
        width: calc(33% - 10px);
        float: left;
        text-align: center;
        background: #bee9ea;
        padding: 15px;
        margin-left: 10px;
    }

    figure.chart-two {
        display: inline-block;
    }

    .vt-item label {
        width: 100%;
    }

    .can_giua {
        float: none;
        margin: auto;
    }

    @media (max-width: 768px) {
        .vt-item {
            width: 100%;
            margin: 0;
            margin-bottom: 5px;
        }

        .chart-two svg .circle-background, .chart-two svg .chart-one svg .circle-foreground, .chart-one svg .chart-two svg .circle-foreground, .chart-two svg .circle-foreground, .chart-two svg .chart-three svg .circle-foreground, .chart-three svg .chart-two svg .circle-foreground {
            r: 59.5px;
        }

        .Result__container__1fq7Z .Result__detail__pCjU6 .Result__part__khkhs > div:nth-child(1) {
            width: 75%;
        }

        .Result__container__1fq7Z .Result__detail__pCjU6 .Result__part__khkhs > div {
            font-size: 15px;
        }
    }
</style>
<!-- remove/add 'animate' class to control when charts animate -->

<div class="row">
    <?php $btn_i = 1; ?>
    @if(strpos($exam->chapter_id, '|') === false)
        {{--Nếu là đề test nhanh--}}
        <?php
        $diem = $exam->total_point;
        $tong_diem = $exam->max_point != 0 ? $exam->max_point : 1;
        $ti_so = (float)$diem / (float)$tong_diem;

        if ($diem <= 30) {
            $danh_gia = 'Yếu';
            $nhan_xet = 'Ui thế này thì trượt JLPT mất rồi! Bạn phải dành nhiều thời gian luyện tập hơn nữa nhé!';
        } elseif ($diem <= 59) {
            $danh_gia = 'Trung bình';
            $nhan_xet = 'Buồn ghê! Kĩ năng của bạn đạt mức trung bình, bạn nên làm nhiều bài tập hơn để cải thiện điểm số nhé';
        } elseif ($diem <= 70) {
            $danh_gia = 'Khá';
            $nhan_xet = 'Tiếc quá,chỉ còn một chút nữa thôi là nắm chắc JLPT rồi. Hãy cố gắng đạt master nào';
        } else {
            $danh_gia = 'Tốt';
            $nhan_xet = 'Quá đỉnh! Master đây rồi ^^ Nhưng đừng chủ quan nhé, hãy chăm chỉ luyện tập để giữ vững phong độ nào!';
        }
        ?>

        @include('themesach100::pages.question.partials.vong_tron_item', ['key' => 'test-nhanh', 'ti_so' => $ti_so,
        'label' => 'Từ vựng-Ngữ pháp, Đọc hiểu, Nghe hiểu', 'danh_gia' => $danh_gia, 'nhan_xet' => $nhan_xet, 'class' => 'can_giua',
        'btn_i' => $btn_i])
        <?php $btn_i++;?>
    @else
        {{--nếu không phải đề test nhanh --}}

        {{--Hiển thị từ vựng, ngữ pháp--}}
        @if(isset($tu_vung_ngu_phap['diem']))
            <?php
            $diem = $tu_vung_ngu_phap['diem'];
            $tong_diem = $tu_vung_ngu_phap['tong_diem'] != 0 ? $tu_vung_ngu_phap['tong_diem'] : 1;
            $ti_so = (float)$diem / (float)$tong_diem;

            $ky_nang = (object)[];
            if ($diem <= 18) {
                $ky_nang->danh_gia = 'Yếu';
                $ky_nang->nhan_xet = 'Ui thế này thì trượt JLPT mất rồi! Bạn phải dành nhiều thời gian luyện tập hơn nữa nhé!';
            } elseif ($diem <= 30) {
                $ky_nang->danh_gia = 'Trung bình';
                $ky_nang->nhan_xet = 'Buồn ghê! Kĩ năng của bạn đạt mức trung bình, bạn nên làm nhiều bài tập hơn để cải thiện điểm số nhé';
            } elseif ($diem <= 45) {
                $ky_nang->danh_gia = 'Khá';
                $ky_nang->nhan_xet = 'Tiếc quá,chỉ còn một chút nữa thôi là nắm chắc JLPT rồi. Hãy cố gắng đạt master nào';
            } else {
                $ky_nang->danh_gia = 'Tốt';
                $ky_nang->nhan_xet = 'Quá đỉnh! Master đây rồi ^^ Nhưng đừng chủ quan nhé, hãy chăm chỉ luyện tập để giữ vững phong độ nào!';
            }
            ?>
            @include('themesach100::pages.question.partials.vong_tron_item', ['key' => 'tu_vung_ngu_phap', 'ti_so' => $ti_so,
            'label' => $tu_vung_ngu_phap['name'], 'danh_gia' => $ky_nang->danh_gia, 'nhan_xet' => $ky_nang->nhan_xet, 'class' => strpos($exam->chapter_id, '|') === false ? 'can_giua' : '',
            'btn_i' => $btn_i])
            <?php $btn_i++;?>
        @endif

        {{--Hiển thị các kỹ năng khác--}}
        @foreach($ky_nangs as $k => $ky_nang)
            <?php
            $diem = @$data_diem[$ky_nang->id]->diem;
            $tong_diem = @$data_diem[$ky_nang->id]->max_diem;
            $ti_so = (float)$diem / (float)($tong_diem != 0 ? $tong_diem : 1);

            if ($diem <= 18) {
                $ky_nang->danh_gia = 'Yếu';
                $ky_nang->nhan_xet = 'Ui thế này thì trượt JLPT mất rồi! Bạn phải dành nhiều thời gian luyện tập hơn nữa nhé!';
            } elseif ($diem <= 30) {
                $ky_nang->danh_gia = 'Trung bình';
                $ky_nang->nhan_xet = 'Buồn ghê! Kĩ năng của bạn đạt mức trung bình, bạn nên làm nhiều bài tập hơn để cải thiện điểm số nhé';
            } elseif ($diem <= 45) {
                $ky_nang->danh_gia = 'Khá';
                $ky_nang->nhan_xet = 'Tiếc quá,chỉ còn một chút nữa thôi là nắm chắc JLPT rồi. Hãy cố gắng đạt master nào';
            } else {
                $ky_nang->danh_gia = 'Tốt';
                $ky_nang->nhan_xet = 'Quá đỉnh! Master đây rồi ^^ Nhưng đừng chủ quan nhé, hãy chăm chỉ luyện tập để giữ vững phong độ nào!';
            }
            ?>

            @include('themesach100::pages.question.partials.vong_tron_item', ['key' => $k, 'ti_so' => $ti_so, 'label' => $ky_nang->name,
            'danh_gia' => $ky_nang->danh_gia, 'nhan_xet' => $ky_nang->nhan_xet, 'class' => strpos($exam->chapter_id, '|') === false ? 'can_giua' : '',
            'btn_i' => $btn_i])
            <?php $btn_i++;?>
        @endforeach
    @endif
</div>