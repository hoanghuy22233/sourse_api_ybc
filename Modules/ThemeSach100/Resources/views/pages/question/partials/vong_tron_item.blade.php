<style>
    .vt-item {
        background-size: cover;
        background-repeat: no-repeat;
        width: 257px;
        padding-top: 34px;
        display: inline-block;
        float: none;
    }
    .vt-{{ $key }} svg .circle-foreground {
        stroke-dasharray: {{ $ti_so * 581 }}px 580.9px;
    }
    @if($label == '文法, 文字・語彙')
        .vt-{{ $key }} {
            background-image: url(/public/frontend/themes/sach100/images/tu-vung.png);
        }
    @elseif($label == '聴解')
        .vt-{{ $key }} {
            background-image: url(/public/frontend/themes/sach100/images/nghe-hieu.png);
        }
    @elseif($label == '読解')
        .vt-{{ $key }} {
            background-image: url(/public/frontend/themes/sach100/images/doc-hieu.png);
        }
    @endif
</style>
<div class="vt-item {{ @$class }} vt-{{ $key }}" data-ti_so="{{ $ti_so }}">
    <label>{{--{{ $label }}--}}</label>
    <figure class="chart-two animate">
        <svg role="img" xmlns="http://www.w3.org/2000/svg">
            <title></title>
            <desc></desc>
            <circle class="circle-background"/>
            <circle class="circle-foreground"/>
        </svg>
        <figcaption>
            {{ (int) ($ti_so * 100) }}
            <br>
            <small>{{ $danh_gia }}</small>
        </figcaption>
    </figure>
    <p>{{ $nhan_xet }}</p>
    <a href="{{ @$settings['btn_' . @$btn_i] }}">Xem thêm</a>
</div>