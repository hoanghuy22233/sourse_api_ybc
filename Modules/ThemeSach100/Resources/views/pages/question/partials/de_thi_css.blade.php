<style>
    .radio .check-box {
        top: 0;
    }

    .exam_item.active {
        font-weight: bold;
        color: #242c66;
        background: #d3e0ff;
    }

    .cart-sec table tr td {
        font-size: 12px;
        padding: 10px;
        font-weight: bold;
    }

    .exam_item {
        width: 25%;
        display: inline-block;
        float: left;
        border: 1px solid #ccc;
        padding: 5px;
    }

    .red {
        color: red !important;
        background: unset;
    }

    .ten-cau {
        font-size: 16px;
    }

    .form-radio .radio {
        width: 100%;
    }

    .form-radio .radio > label {
        color: #000;
    }

    .exam_item {
        text-align: center;
        cursor: pointer;
    }

    .exam_item.dung {
        background: #75f565;
        color: black;

    }

    .exam_item.sai {
        background: red;
        color: white;

    }

    .f-title {
        font-weight: bold !important;
    }

    .personal-head > p {
        color: #000;
    }

    .form-radio .radio label p {
        display: inline-block;
    }

    .gen-metabox p {
        color: #000;
        font-size: 16px;
        font-weight: 500;
        word-spacing: 9px;
    }

    .bang-dieu-khien {
        z-index: 9999;
    }

    .sidebar {
        display: inline-block;
        float: none;
        margin: 0 auto;
        width: 100%;
    }

    .gen-metabox {
        display: inline-block;
        width: 100%;
    }

    @media (max-width: 768px) {
        .personal-head .cart-sec {
            display: none;
        }
    }

    @media all and (min-width: 320px) {
        .bang-dieu-khien {
            z-index: 0;
        }
    }

    @media (min-width: 768px) {
        .topbar.transperent.stick {
            padding: 0;
            padding-left: 16px;
        }

        .cart-sec {
            max-height: 414px;
            overflow-y: scroll;
        }
    }


    /*CSS layout*/
    .central-meta {
        background: #fff none repeat scroll 0 0;
        border: 1px solid #ede9e9;
        border-radius: 5px;
        display: inline-block;
        width: 100%;
        margin-bottom: 20px;
        padding: 20px;
        position: relative;
    }

    .create-post {
        border-bottom: 1px solid #e6ecf5;
        display: block;
        font-weight: 500;
        font-size: 15px;
        line-height: 15px;
        margin-bottom: 20px;
        padding-bottom: 12px;
        text-transform: capitalize;
        width: 100%;
        color: #515365;
        position: relative;
    }

    .personal-head {
        display: inline-block;
        width: 100%;
    }

    .f-title {
        font-weight: bold !important;
    }

    .f-title {
        color: #515365;
        display: inline-block;
        font-size: 13px;
        font-weight: 500;
        margin-bottom: 5px;
        width: 100%;
        text-transform: capitalize;
    }

    .gen-metabox p {
        display: inline-block;
    }

    .fixed {
        position: fixed !important;
    }

    .bang-dieu-khien.fixed {
        top: 105px;
        width: 357px;
        z-index: 99;
    }

    ul.timeline.fixed {
        top: 0;
        z-index: 999999;
        background: #fff;
        height: 105px;
    }

    .ky-nang {
        color: #fff;
        display: inline-block;
        width: 100%;
        font-size: 25px;
        background-color: #466ec4;
        padding: 1em;
        margin: 0;
    }

    .mondai {
        padding: 9px 18px;
        background: #eeeeee;
        display: inline-block;
        width: 100%;
        font-weight: bold;
        font-size: 15px;
        margin-bottom: 20px;
    }

    .AnswerList__title__3au8U.md {
        font-size: 18px;
        margin: .5em 0;
        width: 100%;
        display: inline-block;
        color: #4caf50;
    }

    .AnswerList__title__3au8U.chapter {
        display: inline-block;
        width: unset;
        padding: 10px;
        margin: 0;
        border: 1px #4caf50 solid;
        border-radius: 0;
        margin-top: 15px;
        background: #4caf50;
        color: #fff;
        border-radius: 10px;
    }
    button.solid-button {
        background-color: #4caf50 !important;
        border-color: #4caf50 !important;
    }
    .AnswerList__time__If1v5 {
        width: 70%;
        margin: 0 auto;
        color: #466ec4;
        padding: .5em;
        text-align: center;
        font-size: 20px;
        border: 2px solid #466ec4;
        border-radius: 10px;
    }

    .AnswerList__time__If1v5 {
        width: 70%;
        margin: 0 auto;
        color: #466ec4;
        padding: .5em;
        text-align: center;
        font-size: 20px;
        border: 2px solid #466ec4;
        border-radius: 10px;
    }
    button.solid-button {
        margin-top: 15px !important;
    }

    @media(max-width: 768px) {
        .timeline li {
            min-width: 102px;
        }
        .bang-dieu-khien,
        .nop_bai {
            text-align: center;
        }
        .fixed {
            position: relative !important;
        }
        .so_cau {
            width: 100%;
            display: inline-block;
        }
        .bang-dieu-khien.fixed {
            top: 0;
            width: 100%;
        }
        .AnswerList__title__3au8U.chapter {
            /*width: 100%;*/
        }
        .AnswerList__time__If1v5 {
            background-color: #b5d1dcbf;
            opacity: 0.9;
        }
    }
    .AnswerList__time__If1v5 {
        display: none;
    }
</style>
<script>
    $(document).ready(function () {
        $('.exam_item').click(function () {
            var question_id = $(this).data('id');
            $('html,body').animate({
                scrollTop: $('#question-' + question_id).offset().top - 100
            }, 'slow');
        });
    });
</script>