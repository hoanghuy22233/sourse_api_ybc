<style>
    ul.timeline {
        display: inline-block;
        width: 100%;
        padding: 0;
    }

    .timeline {
        counter-reset: year 2020;
        position: relative;
    }

    .timeline li {
        list-style: none;
        float: left;
        width: 16.3333%;
        position: relative;
        text-align: center;
        text-transform: uppercase;
    }

    ul:nth-child(1) {
        color: #4caf50;
    }

    .timeline li:before {
        counter-increment: year;
        content: counter(year);
        width: 50px;
        height: 50px;
        border: 3px solid #4caf50;
        border-radius: 50%;
        display: block;
        text-align: center;
        line-height: 50px;
        margin: 0 auto 10px auto;
        background: #F1F1F1;
        color: #000;
        transition: all ease-in-out .3s;
        cursor: pointer;
    }

    .timeline li:after {
        content: "";
        position: absolute;
        width: 100%;
        height: 1px;
        background-color: grey;
        top: 25px;
        left: -50%;
        z-index: -999;
        transition: all ease-in-out .3s;
    }

    .timeline li:first-child:after {
        content: none;
    }

    .timeline li.active {
        color: #555555;
    }

    .timeline li.active:before {
        background: #4caf50;
        color: #F1F1F1;
    }

    .timeline li.active + li:after {
        background: #4caf50;
    }

    .timeline:before {
        background: none;
    }

    .question-timeline {
        text-align: center;
        padding-top: 10px;
    }

    ul.timeline {
        width: unset;
    }

    .timeline li {
        width: unset;
        min-width: 130px;
    }

    .question-timeline.fixed {
        z-index: 999;
        width: 100%;
        background: #fff;
        top: 0;
    }

    #modeltheme-main-head.fixed {
        top: 0;
        position: relative !important;
    }

    /*li#l1::before {
        content: "50'";
    }

    li#l2::before {
        content: "5'";
    }

    li#l3::before {
        content: "50'";
    }

    li#l4::before {
        content: "10'";
    }

    li#l5::before {
        content: "50'";
    }

    li#l6::before {
        content: "End";
    }*/

    audio {
        max-width: 100%;
    }

    @media (max-width: 768px) {
        .timeline li {
            font-size: 10px;
        }

        .timeline li:before {
            width: 40px;
            height: 40px;
            line-height: 38px;
            margin-top: 4px;
        }
    }
</style>
<div class="question-timeline">
    <ul class="timeline">
        <?php $count = 0;?>
        @foreach($chapters as $orderNo => $chapter_arr)
            <?php
            $time = 0;
            ?>

            @foreach($chapter_arr as $k => $chapter)
                @if($exam->type == 'Đề Test nhanh' && $chapter->show_in_quick_test == 0)
                    {{--Nếu là đề test nhanh & ko được show trong đó thì ko hiển thị--}}
                @else
                    <?php $count++;?>
                    <li class="{{ $orderNo == $order_no ? 'active' : '' }} li_{{ $orderNo }}" id="l1"
                        data-url="/{{ isset($dap_an) ? 'dap-an' : 'de-thi' }}/{{ $exam->id }}?order_no={{ $orderNo }}">
                        <?php
                            if($exam->type == 'Đề Test nhanh') {
                                $time += $chapter->quick_test_time;
                            } else {
                                $time += $chapter->intro;
                            }
                        ?>
                        @if($k == 0)
                            {{ $chapter->name }}
                        @else
                            {{ $chapter->name }}
                        @endif
                    </li>
                @endif
            @endforeach
            <style>
                .timeline .li_{{ $orderNo }}::before {
                    content: "{{ $time }}'" !important;
                    @if($chapter->status == 0)
                       width: 30px;
                    height: 30px;
                    margin-top: 10px;
                    line-height: 25px;
                @endif



                }
            </style>
        @endforeach
    </ul>
</div>
<style>
    <?php
    $mau_so = $count != 0 ? $count : 1;
    ?>
    @media (max-width: 768px) {
        .timeline li {
            width: {{ 100 / $mau_so }}%;
            min-width: {{ 100 / $mau_so }}%;
        }
    }
</style>
{{--@if(isset($dap_an))--}}
<script>
    $(document).ready(function () {
        $('.timeline li').click(function () {
            var url = $(this).data('url');
            window.location.href = url;
        });
    });
</script>
{{--@endif--}}

<script>
    //  Time chạy theo
    var $obj_timeline = $('.question-timeline');
    var top_timeline = $obj_timeline.offset().top - parseFloat($obj_timeline.css('marginTop').replace(/auto/, 0));

    $(window).scroll(function (event) {
        // what the y position of the scroll is
        var y = $(this).scrollTop();

        // whether that's below the form
        if (y >= top_timeline) {
            // if so, ad the fixed class
            $obj_timeline.addClass('fixed');
        } else {
            // otherwise remove it
            $obj_timeline.removeClass('fixed');
        }
    });
</script>