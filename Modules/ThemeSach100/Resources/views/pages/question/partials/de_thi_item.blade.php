<div class="gen-metabox" id="question-{{ $question->id }}"
     style="display: inline-block; @if (isset($is_child)) margin-left: 30px; @endif">
    <span style="color: red;" class="so_cau">Câu {{ $k }}{{ isset($is_child) ? '.' . ($j + 1) : '' }}:</span>
    @if($question->file_audio != null)
        <div class="controlPlayer">
            @if(!empty($question->file_audio))
                <audio controls>
                    <source src="{{ asset('public/filemanager/userfiles/' . $question->file_audio) }}"
                            type="audio/mpeg">
                    Trình duyệt không hỗ trợ file audio này.
                </audio>
                <!--marquee-->
                <div class="titlePlayer">
                    <span class="titlePlayerSpan">{{ @$question->name }}</span>
                </div>
            @endif
        </div>
    @endif

    <?php
    $img = \Modules\ThemeSach100\Helpers\ThemeSach100Helper::fileQuestion($question, 'question_image');
    $audio = \Modules\ThemeSach100\Helpers\ThemeSach100Helper::fileQuestion($question, 'question_audio');
    ?>
    @if($img)
        <p style="display: inline-block; width: 100%;">
            <img data-src="{{ $img }}" class="lazy">
        </p>
    @endif
    @if($audio)
        <p style="display: inline-block; width: 100%;">
            <audio controls>
                <source src="{{ $audio }}"
                        type="audio/mpeg">
                Trình duyệt không hỗ trợ file audio này.
            </audio>
            <!--marquee-->
        <div class="titlePlayer">
            <span class="titlePlayerSpan">{{ @$question->name }}</span>
        </div>
        </p>
    @endif

    <p>{!! $question->question !!}</p>

    @if (count($childs) == 0 || isset($is_child))
        <div class="col-md-12 select-radio">
            <div class="form-radio">
                <div class="col-xs-12 radio">
                    <label data-id="{{ $k }}{{ isset($is_child) ? '_' . ($j + 1) : '' }}">
                        <input type="radio"
                               name="dapan[{{ $question->id }}]"
                               {{ @$exam_data[$question->id] == 'a' ? 'checked' : '' }}
                               value="a"><i
                                class="check-box"></i><span
                                style="color: blue;">1:</span> {!! $question->a !!}
                    </label>
                </div>
                <div class="col-xs-12 radio">
                    <label data-id="{{ $k }}{{ isset($is_child) ? '_' . ($j + 1) : '' }}">
                        <input type="radio"
                               name="dapan[{{ $question->id }}]"
                               {{ @$exam_data[$question->id] == 'b' ? 'checked' : '' }}
                               value="b"><i
                                class="check-box"></i><span
                                style="color: blue;">2:</span> {!! $question->b !!}
                    </label>
                </div>
                <div class="col-xs-12 radio">
                    <label data-id="{{ $k }}{{ isset($is_child) ? '_' . ($j + 1) : '' }}">
                        <input type="radio"
                               name="dapan[{{ $question->id }}]"
                               {{ @$exam_data[$question->id] == 'c' ? 'checked' : '' }}
                               value="c"><i
                                class="check-box"></i><span
                                style="color: blue;">3:</span> {!! $question->c !!}
                    </label>
                </div>
                <div class="col-xs-12 radio">
                    @if($question->d != null && $question->d != '')
                        <label data-id="{{ $k }}{{ isset($is_child) ? '_' . ($j + 1) : '' }}">
                            <input type="radio"
                                   name="dapan[{{ $question->id }}]"
                                   {{ @$exam_data[$question->id] == 'd' ? 'checked' : '' }}
                                   value="d"><i
                                    class="check-box"></i><span
                                    style="color: blue;">4:</span> {!! $question->d !!}
                        </label>
                    @endif
                </div>
            </div>
        </div>
    @endif
</div>