@extends('themesach100::layouts.default')
@section('main_content')
    <?php
    $order_no = isset($_GET['order_no']) ? $_GET['order_no'] : 1;

    $this_time = 0;
    foreach ($chapters as $chapter) {
        foreach ($chapter as $k => $chapter) {
            if($exam->type == 'Đề Test nhanh' && $chapter->show_in_quick_test == 0) {
                //  Nếu là đề test nhanh & ko được show trong đó thì ko hiển thị
            } else {
                if($exam->type == 'Đề Test nhanh') {
                    $this_time += $chapter->quick_test_time;
                } else {
                    $this_time += $chapter->intro;
                }
            }
        }
    }
    ?>
    <div class="theme-layout">
        @if(strpos($exam->chapter_id, '|') !== false)
            @include('themesach100::pages.question.partials.timeline', ['chapters' => $chapters, 'order_no' => $order_no])
        @endif
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row merged20" id="page-contents">
                        <form class="" action="" method="POST">
                            {!! csrf_field() !!}
                            <input type="hidden" name="het_gio" value="0">
                            <input type="hidden" name="order_no" value="{{ $order_no }}">
                            <div class="col-lg-4 col-md-4 bang-trai">
                                <aside class="sidebar">
                                    <div class="central-meta stick-widget bang-dieu-khien">
                                        <span class="create-post">Đề thi trình độ {{ @$exam->subject->name }}</span>
                                        <div class="personal-head dong-ho-mobile">
                                            <div class="AnswerList__time__If1v5"><span
                                                        id="deadline_time"
                                                        class="CountdownTimer__time__2tG6l"></span></div>

                                            <div class="cart-sec">
                                                <?php $k = 0;?>
                                                <?php
                                                $chapter = (object)[
                                                    'id' => false
                                                ];
                                                $thematic = (object)[
                                                    'id' => false
                                                ];
                                                $k = 0;
                                                ?>
                                                @foreach($monDai as $md)
                                                    <?php
                                                    $questions = $md['questions'];
                                                    ?>
                                                    @foreach($questions as $j => $question)
                                                        @if($question->chapter->id != $chapter->id)
                                                            <?php
                                                            $chapter = $question->chapter;
                                                            ?>
                                                            <div style="width: 100%; display: inline-block;">
                                                                <div class="AnswerList__title__3au8U chapter" style="background-color: #216295; border:#216295; ">Kỹ
                                                                    năng: {{ $chapter->name }}</div>
                                                            </div>
                                                        @endif
                                                        @if($question->thematic->id != $thematic->id)
                                                            <?php
                                                            $thematic = $question->thematic;
                                                            ?>
                                                            <div class="AnswerList__title__3au8U md">{{ $thematic->name }}</div>
                                                        @endif

                                                        <?php $k++;?>
                                                        @include('themesach100::pages.question.partials.de_thi_bang_dieu_khien_item', ['question' => $question])

                                                        <?php
                                                        $childs = $question->childs;
                                                        ?>

                                                        @if(count($childs) > 0)
                                                            @foreach($childs as $j => $child)
                                                                @include('themesach100::pages.question.partials.de_thi_bang_dieu_khien_item', ['question' => $child, 'j' => $j, 'is_child' => true])
                                                            @endforeach
                                                        @endif
                                                    @endforeach
                                                @endforeach

                                            </div>
                                        </div>
                                        <button class="solid-button button form-control" type="submit" data-ripple=""
                                                style="float: none; background-color: #93beda !important; border: #93beda !important;">Nộp
                                            bài
                                        </button>

                                    </div>
                                </aside>
                            </div>
                            <div class="col-lg-8 col-md-8">
                                <div class="central-meta" action="" method="POST">
                                    <span class="create-post">Nội dung đề thi</span>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <?php
                                            $chapter = (object)[
                                                'id' => false
                                            ];
                                            $thematic = (object)[
                                                'id' => false
                                            ];
                                            $k = 0;
                                            ?>
                                            @foreach($monDai as $md)
                                                <?php
                                                $questions = $md['questions'];
                                                ?>
                                                @foreach($questions as $question)
                                                    <?php
                                                    $k++;
                                                    $childs = $question->childs;
                                                    ?>
                                                    @if($question->chapter->id != $chapter->id)
                                                        <?php
                                                        $chapter = $question->chapter;
                                                        ?>
                                                        <div style="text-align: center;" class="ky-nang">
                                                            スキル: {{ $chapter->name }}</div>
                                                    @endif
                                                    @if($question->thematic->id != $thematic->id)
                                                        <?php
                                                        $thematic = $question->thematic;
                                                        ?>
                                                        <div style="" class="mondai">{{ $thematic->name }}: {{ $thematic->intro }}</div>
                                                    @endif
                                                    @include('themesach100::pages.question.partials.de_thi_item', ['question' => $question, 'k' => $k, 'childs' => $childs])
                                                    @if (count($childs) > 0)
                                                        @foreach($childs as $j => $child)
                                                            @include('themesach100::pages.question.partials.de_thi_item', ['question' => $child, 'k' => $k, 'j' => $j, 'is_child' => true])
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </div>
                                    </div>
                                    @if($can_edit)
                                        <div class="div-nop_bai">
                                            <button class="solid-button button form-control" type="submit"
                                                    data-ripple="">Nộp bài
                                            </button>
                                        </div>
                                    @endif
                                    <div id="expired" style="display: none;">
                                        <span class="alert alert-danger">Đã hết giờ làm bài. Vui lòng nộp bài để chấm điểm!</span>
                                        <div class="col-xs-12" style="text-align: center; width: 100%;">
                                            <button class="solid-button button form-control" type="submit"
                                                    data-ripple="" style="float: none;">
                                                Nộp
                                                bài
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('head_script')
    @include('themesach100::pages.question.partials.de_thi_css')
    <style>

        div#expired {
            position: fixed;
            width: 100%;
            left: 0;
            top: 0;
            height: 100%;
            padding-top: 40%;
            text-align: center;
            background: rgba(117, 128, 138, 0.7);
        }

        li#l2::before,
        li#l4::before {
            width: 30px;
            height: 30px;
            margin-top: 10px;
            line-height: 25px;
        }

        @media(max-width: 768px) {
            .bang-trai {
                z-index: 999999;
            }
            .dong-ho-fixed {
                position: fixed !important;
                top: 0;
                z-index: 9999999999999999;
                left: 0;
                font-weight: bold;
            }
            .bg-yellow {
                background: #b5d1dc;
            }
        }

    </style>
@endsection
@section('footer_script')
    @if($exam->type != 'Đề tăng cường kỹ năng')
        <style>
            .AnswerList__time__If1v5 {
                display: block !important;
            }
        </style>
        @include('themesach100::pages.question.partials.dem_thoi_gian')
    @endif

    <script>
        //  Bảng điều khiển chạy theo màn hình
        $(document).ready(function () {
            var $obj = $('.bang-dieu-khien');
            var top = $obj.offset().top - parseFloat($obj.css('marginTop').replace(/auto/, 0));

            $(window).scroll(function (event) {
                // what the y position of the scroll is
                var y = $(this).scrollTop();

                // whether that's below the form
                if (y >= top) {
                    // if so, ad the fixed class
                    $obj.addClass('fixed');
                } else {
                    // otherwise remove it
                    $obj.removeClass('fixed');
                }
            });


            if (window.innerWidth < 768) {
                var $obj = $('.dong-ho-mobile');
                var top = $obj.offset().top - parseFloat($obj.css('marginTop').replace(/auto/, 0));

                $(window).scroll(function (event) {
                    // what the y position of the scroll is
                    var y = $(this).scrollTop();

                    // whether that's below the form
                    if (y >= top) {
                        // if so, ad the fixed class
                        $obj.addClass('dong-ho-fixed');
                    } else {
                        // otherwise remove it
                        $obj.removeClass('dong-ho-fixed');
                    }
                });
            }
        });

        //  Click chọn đáp án nào bên phải sẽ tự điền vào bên trái
        $(document).ready(function () {
            $('.select-radio label').click(function () {
                var id = $(this).data('id');
                var key = $('.exam_item-' + id).data('key').toString().replace("_", ".");
                /*console.log(key);
                if (key.indexOf("_") != -1) {
                    key = key.replace("_", ".");
                }*/

                var dap_an = [];
                dap_an['a'] = 1;
                dap_an['b'] = 2;
                dap_an['c'] = 3;
                dap_an['d'] = 4;

                $('.exam_item-' + id).html(key + '. ' + dap_an[$(this).find('input').val()]);
                $('.exam_item-' + id).addClass('active');

                $(this).parents('.gen-metabox').addClass('bg-yellow');
            });
        });
    </script>
@endsection