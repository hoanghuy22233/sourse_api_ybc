@extends('themesach100::layouts.default')
@section('main_content')
    <?php
    $order_no = isset($_GET['order_no']) ? $_GET['order_no'] : 1;

    $this_time = 0;
    foreach ($chapters as $k => $chapter) {
        $this_time += $chapter->intro;
    }
    ?>
    <div class="theme-layout">
        {{--@if(strpos($exam->chapter_id, '|') !== false)
            @include('themesach100::pages.question.partials.timeline', ['chapters' => $chapters, 'order_no' => $order_no, 'dap_an' => true])
        @endif--}}
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row merged20" id="page-contents">
                        <form class="" action="" method="POST">
                            <div class="col-lg-4 col-md-4">
                                <aside class="sidebar">
                                    <div class="central-meta stick-widget bang-dieu-khien">
                                        <span class="create-post">Đáp án trình độ {{ @$exam->subject->name }}</span>
                                        <div class="personal-head">
                                            <span class="f-title" style="color: red; font-size: 16px;">
                                                <a href="/tao-bai-thi" class="AnswerList__title__3au8U chapter"
                                                   style="    background: #466ec4;font-weight: normal;">Dạng đề khác</a>
                                                @if ($exam->type != 'Đề Test nhanh')
                                                    <a href="/tao-bai-tuong-tu?exam_id={{ $exam->id }}"
                                                       class="AnswerList__title__3au8U chapter"
                                                       style="float: right;font-weight: normal;">Làm đề tiếp theo</a>
                                                @endif
                                            </span>
                                            <hr>
                                            <div class="cart-sec">
                                                <?php
                                                $count_question_true = 0;
                                                $count_question_false = 0;
                                                $count_question_total = 0;
                                                $k = 0;
                                                ?>
                                                <?php $k = 0;?>
                                                <?php
                                                $chapter = (object)[
                                                    'id' => false
                                                ];
                                                $thematic = (object)[
                                                    'id' => false
                                                ];
                                                $dap_an = [
                                                    'a' => 1,
                                                    'b' => 2,
                                                    'c' => 3,
                                                    'd' => 4
                                                ];
                                                ?>
                                                @foreach($monDai as $md)
                                                    <?php
                                                    $questions = $md['questions'];
                                                    ?>
                                                    @foreach($questions as $question)
                                                        @if($question->chapter->id != $chapter->id)
                                                            <?php
                                                            $chapter = $question->chapter;
                                                            ?>
                                                            <div style="width: 100%; display: inline-block;">
                                                                <div class="AnswerList__title__3au8U chapter">Kỹ
                                                                    năng: {{ $chapter->name }}</div>
                                                            </div>
                                                        @endif
                                                        @if($question->thematic->id != $thematic->id)
                                                            <?php
                                                            $thematic = $question->thematic;
                                                            ?>
                                                            <div class="AnswerList__title__3au8U md">{{ $thematic->name }}</div>
                                                        @endif

                                                        <?php $k++;?>
                                                        @include('themesach100::pages.question.partials.dap_an_bang_dieu_khien_item', ['question' => $question, 'dap_an' => $dap_an])
                                                        <?php
                                                        if ($question->a != null) {
                                                            if (@$exam_data[$question->id] != null && $question->answer != strtolower(@$exam_data[$question->id])) {
                                                                //  Câu này đúng
                                                                $count_question_false++;
                                                            } elseif ($question->answer == strtolower(@$exam_data[$question->id])) {
                                                                //  Câu này sai
                                                                $count_question_true++;
                                                            }
                                                            $count_question_total++;
                                                        }
                                                        ?>

                                                        <?php
                                                        $childs = $question->childs;
                                                        ?>

                                                        @if(count($childs) > 0)
                                                            @foreach($childs as $j => $child)

                                                                <?php
                                                                if ($child->a != null) {
                                                                    if (@$exam_data[$child->id] != null && $child->answer != strtolower(@$exam_data[$child->id])) {
                                                                        //  Câu này đúng
                                                                        $count_question_false++;
                                                                    } elseif ($child->answer == strtolower(@$exam_data[$child->id])) {
                                                                        //  Câu này sai
                                                                        $count_question_true++;
                                                                    }
                                                                    $count_question_total++;
                                                                }
                                                                ?>

                                                                @include('themesach100::pages.question.partials.dap_an_bang_dieu_khien_item', ['question' => $child, 'j' => $j, 'is_child' => true, 'dap_an' => $dap_an])
                                                            @endforeach
                                                        @endif

                                                    @endforeach
                                                @endforeach
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 text-center correct_answer" style="padding: 0px">
                                                    <div class="AnswerList__title__3au8U chapter"
                                                         style="padding-left: 15px; padding-right: 15px;"> {{ number_format($count_question_true, 0, '.', '.') }}</div>
                                                    <p>Câu đúng</p>
                                                </div>
                                                <div class="col-md-4 text-center correct_answer" style="padding: 0px">
                                                    <div class="AnswerList__title__3au8U chapter"
                                                         style="background-color: red; border: none;padding-left: 15px; padding-right: 15px;">{{ number_format($count_question_false, 0, '.', '.') }}</div>
                                                    <p>Câu sai</p>
                                                </div>
                                                <div class="col-md-4 text-center correct_answer" style="padding: 0px">
                                                    <div class="AnswerList__title__3au8U chapter"
                                                         style="background-color: #3c3c3c; border: none;padding-left: 15px; padding-right: 15px;">
                                                        {{ number_format($count_question_total, 0, '.', '.') }}</div>
                                                    <p>Tổng</p>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </aside>
                            </div>
                            <div class="col-lg-8 col-md-8">
                                <div class="central-meta" action="" method="POST">
                                    <span class="create-post">Nội dung đề thi
                                    <span style="float: right;">
                                        {{--Đúng: {{ number_format($count_question_true, 0, '.', '.') }}/{{ number_format($count_question_false, 0, '.', '.') }}/{{ number_format(count($questions), 0, '.', '.') }}--}}
                                    </span>
                                </span>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <?php $k = 0;?>
                                            @foreach($monDai as $md)
                                                <?php
                                                $questions = $md['questions'];
                                                ?>
                                                @foreach($questions as $question)
                                                    <?php
                                                    $k++;
                                                    $childs = $question->childs;
                                                    ?>
                                                    @if($question->chapter->id != $chapter->id)
                                                        <?php
                                                        $chapter = $question->chapter;
                                                        ?>
                                                        <div style="text-align: center;" class="ky-nang">
                                                            スキル: {{ $chapter->name }}</div>
                                                    @endif
                                                    @if($question->thematic->id != $thematic->id)
                                                        <?php
                                                        $thematic = $question->thematic;
                                                        ?>
                                                        <div style="" class="mondai">{{ $thematic->name }}
                                                            : {{ $thematic->intro }}</div>
                                                    @endif
                                                    @include('themesach100::pages.question.partials.dap_an_item', ['question' => $question, 'k' => $k, 'childs' => $childs])
                                                    @if (count($childs) > 0)
                                                        @foreach($childs as $j => $child)
                                                            @include('themesach100::pages.question.partials.dap_an_item', ['question' => $child, 'k' => $k, 'j' => $j, 'is_child' => true])
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <script>
        jQuery('.btn-modalReport').click(function () {
            jQuery('#modalReport').addClass('modeltheme-show');
        });

        jQuery('#modalReport .close').click(function () {
            console.log('fe');
            jQuery('#modalReport').removeClass('modeltheme-show');
        });
    </script>
    <div id="modalReport" style="-webkit-transition: opacity 0.15s linear;
    transition: opacity 0.15s linear;" class="modeltheme-modal modeltheme-effect-16" tabindex="-1"
         role="dialog">
        <div class="modal-dialog modal-lg"
             style="max-height:50%;color: #0a001f"
             role="document">
            <form action="">
                <div class="modal-content">
                    <div class="modal-header"
                         style="display: initial">
                        <button type="button" class="close" id="closeModal"
                                data-dismiss="modal"
                                aria-label="Close"><span
                                    aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title"
                            id="info"></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 ">
                                <label>Lý do</label>
                                <select class="select-form"
                                        id="reason">
                                    <option value="0"
                                            selected>Chọn lý
                                        do
                                    </option>
                                    <option value="Sai lỗi chính tả">
                                        Sai lỗi chính tả
                                    </option>
                                    <option value="Không có nội dung câu hỏi">
                                        Không có nội
                                        dung câu hỏi
                                    </option>
                                    <option value="Không có nội dung câu trả lời">
                                        Không có
                                        nội dung câu trả lời
                                    </option>
                                    <option value="Đề sai đáp án">
                                        Đề sai đáp án
                                    </option>
                                    <option value="Hình ảnh hiển thị không đúng">
                                        Hình ảnh
                                        hiển thị không đúng
                                    </option>
                                    <option value="Không hiển thị nội dung">
                                        Không hiển thị
                                        nội dung
                                    </option>
                                    <option value="Lỗi khác">
                                        Lỗi khác
                                    </option>
                                </select>
                                <textarea class="select-form textarea" name="textarea"
                                          style="resize: vertical;"
                                          id="error_content"
                                          rows="5" cols="30"
                                          placeholder="Mô tả chi tiết lỗi sai"></textarea>

                            </div>

                            <div class="col-lg-6 col-md-6">
                                <label class="text-alert">Với mong muốn không ngừng nâng cao chất lượng kho đề thi JLPT,
                                    chúng tôi rất hy vọng sẽ nhận được nhiều phản hồi tích cực từ các bạn học tiếng
                                    Nhật!
                                    Tuy nhiên, trong quá trình nhập liệu không tránh khỏi những sai sót không đáng có,
                                    bạn hãy đóng góp và mô tả chi tiết lỗi sai mà bạn gặp phải trong quá trình làm đề
                                    nhé! どうもありがとうございました ^^

                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"
                         style=" display: flex;justify-content: center;">
                        <button type="button"
                                id="submit_error"
                                class="btn btn-success">
                            Gửi
                        </button>
                    </div>
                </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
@endsection
@section('head_script')
    @include('themesach100::pages.question.partials.de_thi_css')
    <style>
        .ten-cau {
            font-weight: 800 !important;
        }

        .dap_an strong {
            text-align: center;
            width: 100%;
            display: inline-block;
            margin-top: 10px;
            color: blue;
        }

        i.sai::before,
        i.sai::after {
            color: red !important;
        }

        .f-title {
            font-weight: bold !important;
        }

        .form-radio .radio label p {
            display: inline-block;
        }

        .modal-body {
            max-height: 500px;
            overflow-y: auto;
        }

        .chosen-container-single .chosen-single div b {
            background: url(/public/frontend/themes/edu-ldp/images/dropdown-512.png) no-repeat 0 2px !important;
        }

        .chosen-container-single .chosen-search input[type=text] {
            background: #fff;
        }

        .nav-list > li > a {
            color: white;
        }

        label.dung p,
        label.dung span {
            color: green !important;
            font-weight: bold;
        }

        label.sai p,
        label.sai span {
            color: red !important;
            font-weight: bold;
        }


        .select-form {
            display: block;
            width: 100%;
            height: calc(1.5em + 1.3rem + 2px);
            padding: 0.65rem 1rem;
            font-size: 15px;
            font-weight: 400;
            line-height: 1.5;
            color: #3F4254;
            background-color: #ffffff;
            background-clip: padding-box;
            border: 1px solid #E4E6EF;
            border-radius: 0.42rem;
            -webkit-box-shadow: none;
            box-shadow: none;
            -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;

        }

        .textarea {
            overflow: auto;
            resize: vertical;
            height: auto;

        }

        .text-alert {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: stretch;
            -ms-flex-align: stretch;
            align-items: stretch;
            padding: 1.5rem 2rem;
            background-color: #F3F6F9;
            border-color: #F3F6F9;
        }

        .modal-content {
            position: relative;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            width: 100%;
            pointer-events: auto;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, .2);
            border-radius: .3rem;
            outline: 0;
        }


        @media (max-width: 768px) {
            .correct_answer {
                float: left !important;
            }

            .bang-dieu-khien .correct_answer {
                width: 33%;
            }

            .bang-dieu-khien .personal-head a.chapter {
                float: left;
            }

            .bang-dieu-khien .personal-head a {
                min-width: 150px;
            }
        }


    </style>
@endsection
@section('footer_script')
    <script>
        //quick view modal
        $(document).ready(function () {
            $('.view-data').click(function () {
                var prd_id = $(this).attr("id");
                $("#info").text("");
                $("#info").append("<i class=\"fa fa-exclamation-triangle\"></i> Báo lỗi đề thi câu hỏi mã số " + prd_id);
                $("#submit_error").attr("id_question", prd_id);
            });

            $(document).on('click', '#submit_error', function () {
                var prd_id = $("#submit_error").attr("id_question");
                var reason = $("#reason").val();
                var content = $("#error_content").val();
                // console.log(content);
                // $.ajaxSetup({
                //     headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}
                // });
                $('#modalReport').removeClass('modeltheme-show');

                $.ajax({
                    url: '{{route('bao-cao-loi')}}',
                    method: "post",
                    data: {
                        _token: '{{csrf_token()}}',
                        "prd_id": prd_id,
                        "reason": reason,
                        "content": content
                    },

                    success: function (data) {
                        if (data.status) {
                            alert('Đã gửi báo cáo lỗi');
                            $('#modalReport').modal('hide');
                            jQuery('#modalReport').removeClass('modeltheme-show');

                        }
                    }, error: function (error) {
                        $('#modalReport').modal('hide');
                        jQuery('#modalReport').removeClass('modeltheme-show');
                        console.log(error);
                    },

                });
            });

            $(document).on('click', '#closeModal', function () {
                $('#modalReport').removeClass('modeltheme-show');

            });
        });

        //  Bảng điều khiển chạy theo màn hình
        $(document).ready(function () {
            var $obj = $('.bang-dieu-khien');
            var top = $obj.offset().top - parseFloat($obj.css('marginTop').replace(/auto/, 0));

            $(window).scroll(function (event) {
                // what the y position of the scroll is
                var y = $(this).scrollTop();

                // whether that's below the form
                if (y >= top) {
                    // if so, ad the fixed class
                    $obj.addClass('fixed');
                } else {
                    // otherwise remove it
                    $obj.removeClass('fixed');
                }
            });
        });

    </script>
@endsection