@extends('themesach100::layouts.default')
@section('main_content')
    <?php
    $order_no = isset($_GET['order_no']) ? $_GET['order_no'] : 1;

    $this_time = 0;
    foreach ($chapters as $chapter) {
        foreach ($chapter as $k => $chapter) {
            $this_time += $chapter->intro;
        }
    }
    ?>
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        @include('themesach100::pages.question.partials.timeline', ['chapters' => $chapters, 'order_no' => $order_no])
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row merged20" id="page-contents">
                        <form class="" action="" method="POST">
                            {!! csrf_field() !!}
                            <input type="hidden" name="het_gio" value="0">
                            <input type="hidden" name="order_no" value="{{ $order_no }}">
                            <div class="col-lg-4 col-md-4">
                                <aside class="sidebar">
                                    <div class="central-meta stick-widget bang-dieu-khien">
                                        <span class="create-post">Đề thi trình độ {{ @$exam->subject->name }}</span>
                                        <div class="personal-head">
                                            <div class="AnswerList__time__If1v5"><span
                                                        id="deadline_time"
                                                        class="CountdownTimer__time__2tG6l"></span></div>
                                        </div>
                                        <button class="solid-button button form-control" type="submit" data-ripple="" style="float: none;">
                                            Thi tiếp
                                        </button>

                                    </div>
                                </aside>
                            </div>
                            <div class="col-lg-8 col-md-8">
                                <div class="central-meta" action="" method="POST">
                                    <span class="create-post">Thời gian nghỉ</span>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <img data-src="{{ asset('public/filemanager/userfiles/' . @$settings['break_time']) }}" class="lazy">
                                        </div>
                                    </div>
                                    @if($can_edit)
                                        <div>
                                            <button class="solid-button button form-control" type="submit" data-ripple="">Thi tiếp</button>
                                        </div>
                                    @endif
                                    <div id="expired" style="display: none;">
                                        <span class="alert alert-danger">Đã hết giờ nghỉ. Vui lòng vào thi tiếp!</span>
                                        <div class="col-xs-12 div-nop_bai" style="text-align: center; width: 100%;">
                                            <button class="solid-button button form-control" type="submit" data-ripple="" style="float: none;">
                                                Thi tiếp
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('head_script')
    <script src="{{ URL::asset('public/libs/jquery-3.4.0.min.js') }}"></script>
    @include('themesach100::pages.question.partials.de_thi_css')
    <style>

        div#expired {
            position: fixed;
            width: 100%;
            left: 0;
            top: 0;
            height: 100%;
            padding-top: 40%;
            text-align: center;
            background: rgba(117, 128, 138, 0.7);
            z-index: 9;
        }

        li#l2::before,
        li#l4::before {
            width: 30px;
            height: 30px;
            margin-top: 10px;
            line-height: 25px;
        }
    </style>
@endsection
@section('footer_script')
    <script>
        /*window.onbeforeunload = function () {
            return "Tải lại trang web sẽ làm mất các câu bạn đã trả lời. Bạn có chắc chắn muốn tải lại?";
        };*/

        <?php
        $deadline = strtotime($exam->created_at2) + $this_time * 60 /*- 80*60*/
        ;
        ?>
        // Set the date we're counting down to
        var countDownDate = new Date("{{ date('M', $deadline) }} {{ date('d', $deadline) }}, {{ date('Y', $deadline) }} {{ date('H:i:s', $deadline) }}").getTime();

        // Update the count down every 1 second
        var x = setInterval(function () {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="deadline_time"
            document.getElementById("deadline_time").innerHTML = hours + ":"
                + minutes + ":" + seconds + "";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("deadline_time").innerHTML = "Hết giờ";
                $('#expired').css('display', 'block');
            }
        }, 1000);

        //  Click chọn đáp án nào bên trái sẽ tự điền vào bên phải
        $(document).ready(function () {
            $('.select-radio label').click(function () {
                var id = $(this).data('id');
                var key = $('.exam_item-' + id).data('key');
                $('.exam_item-' + id).html(key + '. ' + $(this).find('input').val().toUpperCase());
                $('.exam_item-' + id).addClass('active');
            });
        });
    </script>
@endsection