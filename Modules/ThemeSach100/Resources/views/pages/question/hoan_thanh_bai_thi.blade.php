@extends('themesach100::layouts.default')
@section('main_content')
    <div class="theme-layout">
        <section>
            <div class="gap2 bg_gray">
                <div class="container">
                    <div class="row merged20" id="page-contents">
                        <div class="col-lg-12 col-md-12 box-cn" action="" method="POST">
                            <div class="Exam__exam__1RNFp">
                                <div class="Result__container__1fq7Z" id="box-chung-nhan">
                                    <div class="Result__title__3mVIt" style="opacity: 0">KẾT QUẢ BÀI THI</div>
                                    <div class="Result__exam__y54sV hide"
                                         style="opacity: 0">{{ @$exam->subject->name }}</div>
                                    <div class="Result__exam__y54sV ky-nang-thi"
                                    >日本語能力試験{{ @$exam->subject->name }}</div>
                                    <div class="bcn-content">
                                        <div class="bai-thi-tieu-de">
                                            <p>
                                                <label>Họ & tên thí sinh</label>
                                                <label>: {{ @$exam->student->name }}</label>
                                            </p>
                                            <p>
                                                <label>Ngày thi</label>
                                                <label>: {{ date('d/m/Y', strtotime($exam->created_at)) }}</label>
                                            </p>
                                        </div>
                                        <div class="Result__detail__pCjU6">
                                            <?php
                                            $do = $exam->total_point >= @$exam->subject->featured ? true : false;   //  Tính điểm đỗ / trượt
                                            $ky_nangs = \Modules\Sach100Exam\Models\Category::select('name', 'id')->where('type', 11)->whereIn('id', explode('|', $exam->chapter_id))->where('status', 1)->orderBy('order_no', 'desc')->get();
                                            $data_diem = (array)json_decode($exam->data_diem);

                                            foreach ($data_diem as $k => $v) {
                                                $data_diem[ (int) $k] = $v;
                                            }
                                            ?>
                                            <?php
                                            //  Nếu là đề chuẩn thì gom từ vựng, ngữ pháp lại làm 1
                                            if ($exam->type == 'Đề chuẩn JLPT') {
                                                $tu_vung_ngu_phap = [];
                                                foreach ($ky_nangs as $k => $ky_nang) {
                                                    if (strpos($ky_nang->name, '文字・語彙') !== false || strpos($ky_nang->name, '文法') !== false) {
                                                        if (empty($tu_vung_ngu_phap)) {
                                                            $tu_vung_ngu_phap = [
                                                                'name' => $ky_nang->name,
                                                                'diem' => @$data_diem[$ky_nang->id]->diem,
                                                                'tong_diem' => @$data_diem[$ky_nang->id]->max_diem,
                                                            ];
                                                        } else {
                                                            $tu_vung_ngu_phap = [
                                                                'name' => @$tu_vung_ngu_phap['name'] . ', ' . $ky_nang->name,
                                                                'diem' => @$tu_vung_ngu_phap['diem'] + @$data_diem[$ky_nang->id]->diem,
                                                                'tong_diem' => @$tu_vung_ngu_phap['tong_diem'] + @$data_diem[$ky_nang->id]->max_diem,
                                                            ];
                                                        }
                                                        unset($ky_nangs[$k]);
                                                    } else {
                                                        if (strpos($exam->chapter_id, '|') !== false) {
                                                            //  Là đề chuẩn thì < 19 điểm là trượt
                                                            if (@$data_diem[$ky_nang->id]->diem <= 19) {
                                                                $do = false;
                                                            }
                                                        } else {
                                                            //  Là đề test nhanh thì < 60 điểm là trượt
                                                            if (@$data_diem[$ky_nang->id]->diem <= 60) {
                                                                $do = false;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            ?>

                                            <div class="left">
                                                @if(isset($tu_vung_ngu_phap['diem']))
                                                    <?php
                                                    if (strpos($exam->chapter_id, '|') !== false) {
                                                        //  Là đề chuẩn thì < 19 điểm là trượt
                                                        if (@$tu_vung_ngu_phap['diem'] <= 19) {
                                                            $do = false;
                                                        }
                                                    } else {
                                                        //  Là đề test nhanh thì < 60 điểm là trượt
                                                        if (@$tu_vung_ngu_phap['diem'] <= 60) {
                                                            $do = false;
                                                        }
                                                    }
                                                    ?>
                                                    <div class="Result__part__khkhs">
                                                        <div>{{ @$tu_vung_ngu_phap['name'] }}</div>
                                                        <div>{{ @$tu_vung_ngu_phap['diem'] }}
                                                            /{{ $exam->type == 'Đề chuẩn JLPT' ? '60' : @$tu_vung_ngu_phap['tong_diem'] }}</div>
                                                    </div>
                                                @endif

                                                @foreach($ky_nangs as $ky_nang)
                                                    @if ($exam->type != 'Đề chuẩn JLPT' ||
                                                            ( $exam->type == 'Đề chuẩn JLPT' && (strpos($ky_nang->name, '文字・語彙') === false || strpos($ky_nang->name, '文法') === false)))
                                                        <div class="Result__part__khkhs">
                                                            <div>{{ $ky_nang->name }}</div>
                                                            <div>{{ @$data_diem[$ky_nang->id]->diem }}
                                                                /{{ $exam->type == 'Đề chuẩn JLPT' ? '60' : @$data_diem[$ky_nang->id]->max_diem }}</div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                            <div class="right">
                                                <div class="Result__part__khkhs" data-do="{{ $do ? 'do' : 'truot' }}"
                                                     style="    display: inline-block;width: 100%;text-align: center;">
                                                    <div>TỔNG ĐIỂM:</div>
                                                    <div>{{ $exam->total_point }}/{{ $exam->type == 'Đề chuẩn JLPT' ? '180' : @$exam->max_point }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="text-align: center;margin-bottom: 50px;">
                                    <a href="/dap-an/{{ @$_GET['exam_id'] }}"
                                       class="Result__review__1-D3R btn-xem-dap-an">Xem
                                        đáp án</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @if($exam->type != 'Đề Test nhanh')
            <section>
                <div class="gap2 gray-bg">
                    <div class="container" style="text-align: center;">
                        @include('themesach100::pages.question.partials.vong_tron', ['ky_nangs' => @$ky_nangs, 'data_diem' => @$data_diem, 'exam' => @$exam, 'tu_vung_ngu_phap' => @$tu_vung_ngu_phap])
                    </div>
                </div>
            </section>
        @endif

        @if($exam->type == 'Đề chuẩn JLPT')
            @include('themesach100::partials.bang_xep_hang')
        @endif
    </div>
@endsection
@section('head_script')
    <style>
        .Exam__container__2WUK1 .Exam__exam__1RNFp {
            width: 90%;
            max-width: 100em;
            margin: 0 auto;
        }

        .Exam__container__2WUK1 .Exam__exam__1RNFp {
            font-family: Shincomic;
            color: #464646;
        }

        .Result__container__1fq7Z {
            width: 60%;
            margin: 1em auto;
            padding: 1em;
            font-family: Open Sans, sans-serif;
        }

        .Result__container__1fq7Z .Result__title__3mVIt {
            font-size: 2.5em;
            text-align: center;
        }

        .Result__container__1fq7Z .Result__exam__y54sV {
            font-family: Shincomic;
            font-size: 2.5em;
            text-align: center;
            margin-bottom: 1.234%;
        }

        .Result__container__1fq7Z .Result__detail__pCjU6 .Result__part__khkhs {
            padding: 1em;
            display: flex;
            justify-content: space-between;
            font-size: 1.3em;
            font-weight: 700;
        }

        .btn-xem-dap-an {
            width: 10em;
            border-radius: 5px;
            background-color: #466ec4;
            margin: 1em auto;
            text-align: center;
            padding: .5em;
            font-size: 1.3em;
            color: #fff !important;
            cursor: pointer;
        }

        .Result__container__1fq7Z .Result__review__1-D3R {
            width: 10em;
            border-radius: 5px;
            background-color: #466ec4;
            margin: 1em auto;
            text-align: center;
            padding: .5em;
            font-size: 1.3em;
            color: #fff;
            cursor: pointer;
        }

        .Result__container__1fq7Z {
            @if($exam->type == 'Đề Test nhanh')
                 background-image: url(/public/frontend/themes/sach100/images/test_nhanh.png);
            @elseif(@$do)
              background-image: url(/public/frontend/themes/sach100/images/do.png);
            @else
              background-image: url(/public/frontend/themes/sach100/images/truot.png);
            @endif

              color: #000;
            font-weight: 400;
            background-size: contain;
            background-repeat: no-repeat;
        }

        .left,
        .right {
            width: 50%;
            float: left;
            padding-left: 9%;
        }

        .right::before {
            background-image: url(/public/frontend/themes/sach100/images/gk_dk.png);
            background-repeat: no-repeat;
            background-size: cover;
            content: " ";
            width: 6px;
            display: inline-block;
            position: absolute;
            height: 84px;
        }

        .ky-nang-thi {
            position: absolute;
            top: 19%;
            left: 39%;
            font-size: 29px !important;
        }

        .bai-thi-tieu-de {
            padding: 0px 10px;
            margin-left: 16%;
        }

        .Result__detail__pCjU6 {
            width: 100%;
            margin-top: 5%;
            display: inline-block;
        }

        .bai-thi-tieu-de p, .bai-thi-tieu-de label {
            margin: 0;
            font-size: 17px;
            color: #000;
            font-weight: 400;
        }

        .Result__container__1fq7Z .Result__detail__pCjU6 .Result__part__khkhs {
            font-weight: 400;
        }

        .bai-thi-tieu-de p label:nth-child(1) {
            width: 133px;
        }

        .Result__container__1fq7Z .Result__detail__pCjU6 .Result__part__khkhs {
            padding: 0px 9px 0px 10%;
            font-size: 18px;
            line-height: 28px;
        }

        .Result__container__1fq7Z .Result__title__3mVIt {
            font-size: 23px;
            text-align: center;
            font-weight: bold;
            margin-top: 7%;
        }

        @media (max-width: 768px) {
            .Result__container__1fq7Z {
                width: 100%;
                padding: 0;
            }

            .vc_row.wpb_row.vc_row-fluid {
                margin: 0;
            }

            .Result__container__1fq7Z {
                padding: 0;
                margin: 0;
                width: 100%;
            }

            .Result__container__1fq7Z .Result__title__3mVIt {
                font-size: 27px;
                margin-top: 20px;
            }

            .ky-nang-thi {
                position: absolute;
                top: 10%;
                left: 30%;
                font-size: 17px !important;
            }

            #box-chung-nhan {
                width: 306px;
                height: 197px;
                margin-left: 27px !important;
            }

            .bcn-content {
                text-align: center;
            }

            .box-cn {
                padding: 0px 5px;
            }

            .bai-thi-tieu-de {
                width: 211px;
                display: inline-block;
                margin-left: -24px;
                text-align: left;
            }

            .bai-thi-tieu-de p {
                line-height: 10px;
            }

            .bai-thi-tieu-de p label {
                font-size: 11px;
            }

            .Result__exam__y54sV.hide {
                height: 19px;
            }

            .bai-thi-tieu-de p label:nth-child(1) {
                width: 81px;
            }

            .Result__container__1fq7Z .Result__detail__pCjU6 .Result__part__khkhs > div {
                font-size: 12px !important;
                line-height: 16px;
                min-width: 42px;
            }

            .left, .right {
                padding-left: 0;
            }

            .right::before {
                background-size: contain;
                height: 39px;
            }

            .Result__container__1fq7Z .Result__detail__pCjU6 .Result__part__khkhs {
                padding: 0px 4px 0px 17px;
                font-size: 9px;
                line-height: 28px;
            }

            .Result__detail__pCjU6 .left .Result__part__khkhs > div:nth-child(2) {
                text-align: right;
            }

            #box-chung-nhan {
                margin: auto !important;
            }
        }

        .Result__exam__y54sV.hide {
            display: block !important;
        }

        @media (min-width: 768px) {
            #box-chung-nhan {
                width: 680px;
                height: 412px;
            }
        }
        @media only screen
        and (min-device-width: 414px)
        and (max-device-width: 736px)
        and (-webkit-min-device-pixel-ratio: 3) {
            #box-chung-nhan {
                margin-left: 51px !important;

            }
            .ky-nang-thi {
                left: 33%;
            }

        }
    </style>
@endsection
@section('footer_script')
    <script>

    </script>
@endsection