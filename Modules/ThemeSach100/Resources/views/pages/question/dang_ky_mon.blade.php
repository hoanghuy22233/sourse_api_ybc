@extends('themesach100::layouts.default')
@section('main_content')
    @include('themesach100::partials.banner_header')
    <div class="se-pre-con"></div>
    <div class="theme-layout">
        <div class="postoverlay"></div>
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="col-lg-12">
                                    <form class="central-meta" action="" method="POST">
                                        {!! csrf_field() !!}
                                        <h4 class="create-post">Mua khóa học</h4>
                                        <div class="cart-sec">
                                            <strong>Số dư tài khoản của bạn là : {{ number_format(\Auth::guard('student')->user()->balance, 0, '.', '.') }}đ</strong>
                                            @if (session('success'))
                                                <div class="alert bg-success" role="alert">
                                                    {!!session('success')!!}
                                                </div>
                                            @endif
                                            @if (session('error'))
                                                <div class="alert bg-danger" role="alert" style="    color: #fff;">
                                                    {!!session('error')!!}
                                                </div>
                                            @endif
                                            <?php
                                            $subject_registered = explode('|', \Auth::guard('student')->user()->subjects);
                                            $subjects = \Modules\ThemeSach100\Models\Category::select(['name', 'id'])->whereNotIn('id', $subject_registered)->where('type', 10)->where('status', 1)->get();
                                            $subjects_registered = \Modules\ThemeSach100\Models\Category::select(['name', 'id'])->whereIn('id', $subject_registered)->where('type', 10)->where('status', 1)->get();
                                            $settings_price = \App\Models\Setting::where('type', 'service_tab')->pluck('value', 'name')->toArray();

                                            ?>
                                            <table class="table table-responsive">
                                                <tbody>
                                                <tr>
                                                    <td style="width: 114px;">Chọn gói</td>
                                                    <td style="width: 114px;">Giá</td>
                                                    <td>Môn đã mua</td>
                                                </tr>
                                                <tr>
                                                    <td>1 Môn</td>
                                                    <td>{{ number_format(@$settings_price['1'], 0, '.', '.') }} VNĐ</td>
                                                    <td>
                                                        <div>
                                                            @foreach($subjects_registered as $subject)
                                                                <div class=" select-object buyed">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="checkbox" class="" checked disabled ><i
                                                                                    class="check-box"></i>
                                                                            {{ $subject->name }}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            @foreach($subjects as $subject)
                                                                <div class=" select-object">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input data-max="1" type="checkbox" value="{{ $subject->id }}" name="subjects[]" class="chose-subject"
                                                                            {{ in_array($subject->id, $subject_registered) ? 'checked disabled' : '' }} ><i
                                                                                    class="check-box"></i>
                                                                            {{ $subject->name }}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3 Môn</td>
                                                    <td>{{ number_format(@$settings_price['3'], 0, '.', '.') }} VNĐ</td>
                                                    <td>
                                                        <div>
                                                            @foreach($subjects_registered as $subject)
                                                                <div class=" select-object buyed">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="checkbox" class="" checked disabled ><i
                                                                                    class="check-box"></i>
                                                                            {{ $subject->name }}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            @foreach($subjects as $subject)
                                                                <div class=" select-object">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input data-max="3" type="checkbox" value="{{ $subject->id }}" name="subjects[]" class="chose-subject"
                                                                                    {{ in_array($subject->id, $subject_registered) ? 'checked disabled' : '' }}><i
                                                                                    class="check-box"></i>
                                                                            {{ $subject->name }}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>5 Môn</td>
                                                    <td>{{ number_format(@$settings_price['5'], 0, '.', '.') }} VNĐ</td>
                                                    <td>
                                                        <div>
                                                            @foreach($subjects_registered as $subject)
                                                                <div class=" select-object buyed">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="checkbox" class="" checked disabled ><i
                                                                                    class="check-box"></i>
                                                                            {{ $subject->name }}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            @foreach($subjects as $subject)
                                                                <div class=" select-object">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input data-max="5" type="checkbox" value="{{ $subject->id }}" name="subjects[]" class="chose-subject"
                                                                                    {{ in_array($subject->id, $subject_registered) ? 'checked disabled' : '' }}><i
                                                                                    class="check-box"></i>
                                                                            {{ $subject->name }}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="proceed" style="margin-top: 30px;">
                                                    <a href="/bai-viet/huong-dan-nap-tien.html" class="main-btn" style="background: none;
    color: #20117f;
    text-decoration: underline;">Nạp tiền</a>
                                                    <button type="submit" title="" class="main-btn">Đăng ký ngay</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->

    </div>
    <script>
        $(document).ready(function () {
            $('.chose-subject').change(function () {
                if(this.checked) {
                    var val = $(this).val();
                    $(this).parents('tr').siblings().find('.chose-subject').prop('checked', false);

                    var count_cheked = $('.chose-subject:checked').length;
                    var max_cheked = $(this).data('max');
                    if (max_cheked < count_cheked) {
                        alert('Bạn đã chọn tối đa ' + max_cheked + ' môn');
                        $(this).prop('checked', false);
                    }
                }
            });
        });
    </script>

@endsection
@section('head_script')
    <style>
        .select-object {
            display: inline-block;
            margin-right: 15px;
        }

        .buyed .checkbox .check-box {
            border-color: #9E9E9E;
        }

        .buyed .checkbox .check-box::before,
        .buyed .checkbox .check-box::after {
            background: #9E9E9E;
        }
        @media screen and (max-width: 990px){
            .cart-sec table tr td  {
                min-width: unset;
            }
        }
    </style>
@endsection