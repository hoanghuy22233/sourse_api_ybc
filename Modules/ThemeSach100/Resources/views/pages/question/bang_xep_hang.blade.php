@extends('themesach100::layouts.default')
@section('main_content')
    <div class="theme-layout">
        @include('themesach100::partials.bang_xep_hang')
    </div>
@endsection
@section('head_script')
    <style>
        .Exam__container__2WUK1 .Exam__exam__1RNFp {
            width: 90%;
            max-width: 100em;
            margin: 0 auto;
        }

        .Exam__container__2WUK1 .Exam__exam__1RNFp {
            font-family: Shincomic;
            color: #464646;
        }

        .Result__container__1fq7Z {
            width: 60%;
            margin: 1em auto;
            padding: 1em;
            font-family: Open Sans, sans-serif;
        }

        .Result__container__1fq7Z .Result__title__3mVIt {
            font-size: 2.5em;
            text-align: center;
        }

        .Result__container__1fq7Z .Result__exam__y54sV {
            font-family: Shincomic;
            font-size: 2.5em;
            text-align: center;
            margin-bottom: 1em;
        }

        .Result__container__1fq7Z .Result__detail__pCjU6 .Result__part__khkhs {
            padding: 1em;
            border-bottom: 1px solid #d1d1d1;
            display: flex;
            justify-content: space-between;
            font-size: 1.3em;
            font-weight: 700;
        }

        .Result__container__1fq7Z .Result__review__1-D3R {
            width: 10em;
            border-radius: 5px;
            background-color: #466ec4;
            margin: 1em auto;
            text-align: center;
            padding: .5em;
            font-size: 1.3em;
            color: #fff;
            cursor: pointer;
        }
    </style>
@endsection
@section('footer_script')
    <script>

    </script>
@endsection