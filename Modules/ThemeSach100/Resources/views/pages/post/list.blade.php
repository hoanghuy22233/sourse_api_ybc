@extends('themesach100::layouts.default')
@section('tailieu')
    active
@endsection
@section('main_content')
    <div class="high-padding">
        <!-- Sticky posts -->
        <?php
        $data = \Modules\ThemeSach100\Models\Post::select('id', 'name', 'slug', 'image')->where('status', 1)->where('multi_cat', 'like', '%|' . @$category->id . '|%')
            ->where('featured', 1)->orderBy('order_no', 'desc')->orderBy('id', 'desc')->limit(3)->get();
        ?>
        @if(count($data) > 0)
            <div class="container sticky-posts">
                <div class="vc_row">
                    @foreach($data as $v)
                        <div class="vc_col-md-4 post">
                            <div class="sticky_post_text_container" style="background-color:#9ab488; ">
                                <a href="/{{ $category->slug }}/{{ $v->slug }}.html" class="relative">
                                    <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($v->image, 360, null) }}" class="lazy"
                                         alt="{{ $v->name }}">
                                </a>
                                <div class="bottom">
                                    <h3 class="post-name" style="height: 104px; overflow: hidden;"><a
                                                href="/{{ $category->slug }}/{{ $v->slug }}.html">{{ $v->name }}</a>
                                    </h3>
                                    <div class="post-author"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
    @endif

    <!-- ///////////////////// Start Grid/List Layout ///////////////////// -->
        <!-- Blog content -->
        <div class="container blog-posts high-padding">
            <div class="vc_row">

                <div class="vc_col-md-9 main-content">
                    <div class="vc_row">
                        @foreach($posts as $post)
                            <article id="post-7558"
                                     class="single-post grid-view vc_col-md-12 list-view post-7558 post type-post status-publish format-standard has-post-thumbnail hentry category-smart-business tag-quiz">

                                <div class="blog_custom text-white" style="background-color:#a1d042;">
                                    <!-- POST THUMBNAIL -->
                                    <div class="col-md-4 post-thumbnail">
                                        <a href="/{{$slug1}}/{{@$post->slug}}.html" class="relative">
                                            <img class="blog_post_image lazy"
                                                 data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image,266, 266) }}"
                                                 alt="{{@$post->name}}"> </a>
                                    </div>

                                    <!-- POST DETAILS -->
                                    <div class="col-md-8  post-details">
                                        <h3 class="post-name row">
                                            <a title="{{@$post->name}}"
                                               href="/{{$slug1}}/{{@$post->slug}}.html">
                                                {{@$post->name}} </a>
                                        </h3>
                                        <div class="post-excerpt row">
                                            <p></p>
                                            <p>{!! $post->intro !!}</p>
                                            <p></p>
                                            <p><a href="/{{$slug1}}/{{@$post->slug}}.html"
                                                  class="more-link">Xem thêm <span class="meta-nav">→</span></a></p>

                                        </div>
                                    </div>
                                </div>
                            </article>
                        @endforeach

                        <div class="clearfix"></div>
                        {{ $posts->appends(Request::all())->links() }}
                    </div>
                </div>

                <div class="vc_col-md-3 sidebar-content">
                    @include('themesach100::pages.post.partials.sidebar_categories')
                </div>

            </div>
        </div>
        <!-- ///////////////////// Stop Grid/List Layout ///////////////////// -->
    </div>
@endsection
@section('head_script')
    <style>
        @media(max-width: 768px) {
            .main-content .post-thumbnail {
                width: 100%;
                padding: 0;
                max-height: 150px;
                overflow: hidden;
            }
            .main-content .post-thumbnail img {
                width: 100%;
            }
            #recent_entries_with_thumbnail-4 .post-thumbnail.relative {
                display: inline-block;
                float: left;
                margin-right: 15px;
            }
        }
    </style>
@endsection