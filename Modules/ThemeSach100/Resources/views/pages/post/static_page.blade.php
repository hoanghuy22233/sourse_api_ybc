@extends('themesach100::layouts.default')
@section('main_content')
    <div style="margin-bottom: 50px;">
        <!-- Sticky posts -->
        <!-- Blog content -->
        <div class="container blog-posts high-padding">
            <div class="vc_row">

                <div class="col-md-12 col-sm-12 status-panel-sidebar main-content">

                    <div class="article-function">

                        <script>
                            $("#comment").click(function () {
                                $('html, body').animate({
                                    scrollTop: $("#comments").offset().top
                                }, 1000);
                            });
                        </script>
                    </div>

                    <div class="article-content">

                        <p>{!! $post->content !!}</p>

                    </div>
                    <div id="comments" class="clearfix">
                        <h3 id="comments-title">Bình luận</h3>
                        <div id="fb-root"></div>
                        <script>(function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=1618627438366755&autoLogAppEvents=1';
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                        <div class="fb-comments"
                             data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"
                             data-numposts="5"></div>
                    </div>
                </div>

                <div class="vc_col-md-4 sidebar-content">
{{--                    @include('themesach100::pages.post.partials.sidebar_categories')--}}
                </div>

            </div>
        </div>
        <!-- ///////////////////// Stop Grid/List Layout ///////////////////// -->
    </div>
@endsection
@section('head_script')
    <style>
        .article-function {
            position: relative;
            float: left;
            width: 80px;
            padding-right: 10px;
            margin-top: 30px;
        }

        .article-content {
            float: left;
            width: calc(100% - 80px);
        }

        .article-function ul li.commnent {
            padding-bottom: 14px;
        }

        .article-function ul li {
            width: 50px;
            float: left;
            display: inline-block;
            position: relative;
            margin-bottom: 10px;
        }

        .article-function ul li.commnent a {
            font-size: 13px;
            line-height: 15px;
            padding: 5px;
            box-sizing: border-box;
            text-transform: uppercase;
            color: #fff;
            text-decoration: none;
            background: #94a2af;
            height: 40px;
            text-align: center;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            width: 100%;
            display: block;
        }
        span.date {
            margin-top: 10px;
        }
        @media(max-width: 768px) {
            .article-function {
                width: 100%;
                margin: 0;
            }
            .article-content {
                width: 100%;
            }
            .article-function img {
                width: 40px;
            }
            .container.high-padding {
                padding-top: 0;
            }
        }
    </style>
@endsection