<aside id="post_thumbnails_slider-2" class="widget widget_post_thumbnails_slider">
    {{--Hiển thị khối widget--}}
    <?php
    $widgets = CommonHelper::getFromCache('widgets_posts_sidebar_right', ['widgets']);
    if (!$widgets) {
        $widgets = \Modules\ThemeEdu\Models\Widget::where('location', 'posts_sidebar_right')->where('status', 1)->orderBy('order_no', 'desc')->orderBy('id', 'asc')->get();
        CommonHelper::putToCache('widgets_posts_sidebar_right', $widgets, ['widgets']);
    }
    ?>
    @foreach($widgets as $widget)

        @if($widget->type == 'html')
            {!!$widget->content !!}

        @else
            @include(@json_decode($widget->config)->view, ['config' => (array) json_decode($widget->config)])
        @endif
    @endforeach
    {{--END: Hiển thị khối widget--}}
</aside>

<aside id="recent_entries_with_thumbnail-4" class="widget widget_recent_entries_with_thumbnail">
    <?php
    $data = CommonHelper::getFromCache('posts_new', ['posts']);
    if (!$data) {
        $data = \Modules\ThemeSach100\Models\Post::where('status', 1)->where('slug', 'NOT LIKE', '%http%')->orderBy('order_no', 'desc')->orderBy('name', 'asc')->limit(5)->get();
        CommonHelper::putToCache('posts_new', $data, ['posts']);
    }
    ?>
    <h1 class="widget-title">Bài viết mới nhất</h1>
    <ul>
        @foreach($data as $post)
            <li class="row">
                <div class="vc_col-md-3 post-thumbnail relative"><a
                            href="{{ @$post->category->slug != '' ? '/' . @$post->category->slug : '/bai-viet' }}/{{@$post->slug}}.html"><img class=" lazy"
                                                                          data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($post->image, 66, 66) }}"
                                                                          alt="{{@$post->name}}">
                        <div class="thumbnail-overlay absolute"><i class="fa fa-plus absolute"></i>
                        </div>
                    </a></div>
                <div class="vc_col-md-9 post-details"><a
                            href="{{ @$post->category->slug != '' ? '/' . @$post->category->slug : '/bai-viet' }}/{{@$post->slug}}.html">{{@$post->name}}</a></div>
            </li>
        @endforeach
    </ul>
</aside>

