@extends('themesach100::layouts.default')
@section('main_content')
    <div style="margin-bottom: 50px;">
        <!-- Sticky posts -->
        <!-- Blog content -->
        <div class="container blog-posts high-padding">
            <div class="vc_row">

                <div class="col-md-8 col-sm-8 status-panel-sidebar main-content">
                    <div class="article-header">
                        <h3 class="post-name">{!! $post->name !!}</h3>
                        {{--<img data-src="{{ asset('public/filemanager/userfiles/' . $post->image) }}"
                             class="img-responsive single-post-featured-img lazy" alt="{{ $post->name }}">--}}
                        <div class="article-details">
                            <span class="date" style="display: block;">
					Ngày đăng: {{ date('d/m/Y', strtotime($post->created_at)) }} - Người đăng: {{ @$post->admin->name }}</span>

                            <div class="addthis_toolbox addthis_default_style ">


                                <div class="fb-like fb_iframe_widget" style="display: inline-block;
    float: left;"
                                     data-href="{{ "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"
                                     data-width="" data-layout="button_count" data-action="like" data-size="small"
                                     data-share="true" fb-xfbml-state="rendered"
                                     fb-iframe-plugin-query="action=like&amp;app_id=&amp;container_width=138&amp;href=https%3A%2F%2Fwww.sachtiengnhat100.com%2Fblogs%2Ftai-lieu-tu-vung%2Ftu-vung-tieng-nhat-chuyen-nganh-may&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=true&amp;size=small&amp;width=">
                                    <span style="vertical-align: bottom; width: 138px; height: 20px;"><iframe
                                                name="f3be01cace20f18" width="1000px" height="1000px"
                                                data-testid="fb:like Facebook Social Plugin"
                                                title="fb:like Facebook Social Plugin" frameborder="0"
                                                allowtransparency="true" allowfullscreen="true" scrolling="no"
                                                allow="encrypted-media"
                                                src="https://www.facebook.com/v8.0/plugins/like.php?action=like&amp;app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df14a4d1d80344c8%26domain%3Dwww.sachtiengnhat100.com%26origin%3Dhttps%253A%252F%252Fwww.sachtiengnhat100.com%252Ff190b3d590e2%26relation%3Dparent.parent&amp;container_width=138&amp;href=https%3A%2F%2Fwww.sachtiengnhat100.com%2Fblogs%2Ftai-lieu-tu-vung%2Ftu-vung-tieng-nhat-chuyen-nganh-may&amp;layout=button_count&amp;locale=vi_VN&amp;sdk=joey&amp;share=true&amp;size=small&amp;width="
                                                style="border: none; visibility: visible; width: 138px; height: 20px;"
                                                class=""></iframe></span></div>
                                <a class="addthis_button_tweet at300b" tw:count="none" style="display: inline-block;
    float: left;">
                                    <div class="tweet_iframe_widget" style="width: 61px; height: 25px;"><span><iframe
                                                    id="twitter-widget-0" scrolling="no" frameborder="0"
                                                    allowtransparency="true" allowfullscreen="true"
                                                    class="twitter-share-button twitter-share-button-rendered twitter-tweet-button"
                                                    style="position: static; visibility: visible; width: 60px; height: 20px;"
                                                    title="Twitter Tweet Button"
                                                    src="https://platform.twitter.com/widgets/tweet_button.2d7d9a6d04538bf11c7b23641e75738c.en.html#dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=https%3A%2F%2Fwww.sachtiengnhat100.com%2Fblogs%2Ftai-lieu-tu-vung%2Ftu-vung-tieng-nhat-chuyen-nganh-may&amp;size=m&amp;text=%22T%E1%BA%A4T%20T%E1%BA%A6N%20T%E1%BA%ACT%22%20t%E1%BB%AB%20v%E1%BB%B1ng%20chuy%C3%AAn%20ng%C3%A0nh%20may&amp;time=1601029442730&amp;type=share&amp;url=https%3A%2F%2Fwww.sachtiengnhat100.com%2Fblogs%2Ftai-lieu-tu-vung%2Ftu-vung-tieng-nhat-chuyen-nganh-may%23.X23FQU3WaaE.twitter"
                                                    data-url="{{ "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}#.X23FQU3WaaE.twitter"></iframe></span>
                                    </div>
                                </a>
                                <a class="addthis_button_google_plusone" g:plusone:size="medium" style="display: inline-block;
    float: left;"
                                   g:plusone:count="false"></a>
                                <div class="atclear"></div>
                            </div>
                        </div>
                    </div>
                    <div class="article-function">
                        <ul style="padding: 0">
                            <li class="commnent">
                                <a href="javascript:" id="comment">Bình<br>Luận</a>
                            </li>
                            <li class="home">
                                <a href="/" id="home"><img
                                            src="//theme.hstatic.net/1000302121/1000492939/14/icon-home-cam.png?v=964"></a>
                            </li>
                            <li class="share">
                                <a href="" id="share">
                                </a>
                                <div class="fb-share-button"
                                     data-href="{{ "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"
                                     data-layout="button" data-size="small"><a href="" id="share">
                                    </a><a target="_blank"
                                           href="https://www.facebook.com/sharer/sharer.php?u={{ "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"
                                           class="fb-xfbml-parse-ignore"><img
                                                src="//theme.hstatic.net/1000302121/1000492939/14/icon-facebook.png?v=964"></a>
                                </div>

                            </li>
                        </ul>
                        <script>
                            $("#comment").click(function () {
                                $('html, body').animate({
                                    scrollTop: $("#comments").offset().top
                                }, 1000);
                            });
                        </script>
                    </div>

                    <div class="article-content">

                        <?php
                        $arr = explode('[audio:', $post->content);
                        $str = '';
                        foreach ($arr as $k => $ar) {

                            if ($k == 0) {
                                $str .= $ar;
                            } else {
                                $ar_aray = explode(']', $ar);
                                $autio_src = strpos($ar_aray[0], 'http') === false ? asset('public/filemanager/userfiles/'. $ar_aray[0]) : $ar_aray[0];
                                $str .= '<audio controls>
                                            <source src="' . $autio_src . '" type="audio/mpeg">
                                        </audio>';
                                unset($ar_aray[0]);
                                $str .= implode("]", $ar_aray);
                            }
                        }
                        ?>

                        <p>{!! $str !!}</p>

                    </div>
                    <div id="comments" class="clearfix">
                        <h3 id="comments-title">Bình luận</h3>
                        <div id="fb-root"></div>
                        <script>(function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=1618627438366755&autoLogAppEvents=1';
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                        <div class="fb-comments"
                             data-href="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"
                             data-numposts="5"></div>
                    </div>
                </div>

                <div class="vc_col-md-4 sidebar-content">
                    @include('themesach100::pages.post.partials.sidebar_categories')
                </div>

            </div>
        </div>
        <!-- ///////////////////// Stop Grid/List Layout ///////////////////// -->
    </div>
    @include('themesach100::pages.post.partials.script_menu_post')
@endsection
@section('head_script')
    <style>
        ::marker {
            font-size: 18px;
            font-weight: bold;
        }
        .article-function {
            position: relative;
            float: left;
            width: 80px;
            padding-right: 10px;
            margin-top: 30px;
        }

        .article-content {
            float: left;
            width: calc(100% - 80px);
        }

        .article-function ul li.commnent {
            padding-bottom: 14px;
        }

        .article-function ul li {
            width: 50px;
            float: left;
            display: inline-block;
            position: relative;
            margin-bottom: 10px;
        }

        .article-function ul li.commnent a {
            font-size: 13px;
            line-height: 15px;
            padding: 5px;
            box-sizing: border-box;
            text-transform: uppercase;
            color: #fff;
            text-decoration: none;
            background: #94a2af;
            height: 40px;
            text-align: center;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            width: 100%;
            display: block;
        }
        .article-content table{
            max-width: 100%;
        }
        .article-content td {
            min-width: 40px;
        }
        span.date {
            margin-top: 10px;
        }
        @media(max-width: 768px) {
            .article-function {
                width: 100%;
                margin: 0;
            }
            .article-content {
                width: 100%;
            }
            .article-function img {
                width: 40px;
            }
            .container.high-padding {
                padding-top: 0;
            }
            .article-content {
                overflow: hidden;
            }
            #recent_entries_with_thumbnail-4 .post-thumbnail.relative {
                display: inline-block;
                float: left;
                margin-right: 15px;
            }
            .main-content img {
                width: unset !important;
                height: unset !important;
            }
        }
        .article-content img {
            height: unset !important;
        }
    </style>
@endsection