@extends('themesach100::layouts.default')
@section('main_content')
    <div class="theme-layout">
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="col-lg-4 col-md-4">
                        <div class="central-meta">

                            <span class="create-post">Giới thiệu</span>
                            <div class="personal-head">
                                <img src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($user->image) }}">
                                <span class="f-title"><i class="fa fa-birthday-cake"></i> Họ & tên:</span>
                                <p>
                                    {{@$user->name}}
                                </p>
                                <span class="f-title"><i class="fa fa-birthday-cake"></i> Ngày sinh:</span>
                                <p>
                                    @if(@$user->birthday != '' && @$user->birthday != null)
                                        {{date('d-m-Y',strtotime(@$user->birthday))}}
                                    @endif
                                </p>
                                <span class="f-title"><i class="fa fa-phone"></i> Điện thoại:</span>
                                <p>
                                    {{@$user->phone}}
                                </p>
                                <span class="f-title"><i class="fa fa-male"></i> Giới tính:</span>
                                <p>
                                    @if(@$user->gender === 1)
                                        Nam
                                    @elseif(@$user->gender === 0)
                                        Nữ
                                    @endif
                                </p>
                                <span class="f-title"><i class="fa fa-envelope"></i> Email:</span>
                                <p>
                                    <a href="mailto:{{@$user->email}}"
                                       class="__cf_email__"
                                       data-cfemail="cb9ba2bfa5a2a08bb2a4beb9a6aaa2a7e5a8a4a6">
                                        {{@$user->email}}
                                    </a>
                                </p>
                                @if($user->id == @\Auth::guard('student')->user()->id)
                                    <a href="/profile/edit"
                                       style="    color: #007bff;text-decoration: underline; cursor: pointer;">Chỉnh
                                        sửa profile</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8">
                        @include('themesach100::pages.student.partials.bai_thi_da_lam', ['student_id' => $user->id])
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('head_script')
    <style>
        .central-meta {
            background: #fff none repeat scroll 0 0;
            border: 1px solid #ede9e9;
            border-radius: 5px;
            display: inline-block;
            width: 100%;
            margin-bottom: 20px;
            padding: 20px;
            position: relative;
        }

        .create-post {
            border-bottom: 1px solid #e6ecf5;
            display: block;
            font-weight: 500;
            font-size: 15px;
            line-height: 15px;
            margin-bottom: 20px;
            padding-bottom: 12px;
            text-transform: capitalize;
            width: 100%;
            color: #515365;
            position: relative;
        }

        .personal-head {
            display: inline-block;
            width: 100%;
        }

        .f-title {
            color: #515365;
            display: inline-block;
            font-size: 13px;
            font-weight: 500;
            margin-bottom: 5px;
            width: 100%;
            text-transform: capitalize;
        }

        .personal-head > p {
            font-size: 13px;
            line-height: 20px;
            margin-bottom: 20px;
            padding-left: 20px;
        }
        @media(max-width: 768px) {
            .quiz_list {
                overflow-x: scroll;
            }
        }
    </style>
@endsection
@section('footer_script')
    <script>

    </script>
@endsection