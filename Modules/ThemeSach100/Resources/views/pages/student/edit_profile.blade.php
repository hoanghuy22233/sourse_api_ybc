@extends('themesach100::layouts.default')
@section('main_content')
    <link rel="stylesheet" href="{{ URL::asset('/public/frontend/themes/sach100/themes/smartowl/css_login/style.css') }}">

    <div class="se-pre-con"></div>
    <div class="theme-layout">

    <!-- topbar -->
        <section>
            <div class="gap2 gray-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row merged20" id="page-contents">
                                <div class="user-profile">
{{--                                    @include('themesach100::partials.student_menu')--}}
                                </div><!-- user profile banner  -->

                                <div class="col-lg-8 col-md-8">
                                    <div class="forum-form">
                                        @if(Session::has('success'))
                                            <p style="top: -15px;" class="alert alert-success">{!! Session::get('success') !!}</p>
                                        @endif
                                        <div class="central-meta">
                                            <span class="create-post">Thông tin cơ bản</span>
                                            <form method="post" class="c-form" action="/profile/edit"
                                                  enctype="multipart/form-data">
                                                {!! csrf_field() !!}
                                                <input type="hidden" name="student_id"
                                                       value="{{ Auth::guard('student')->user()->id }}">
                                                <div>
                                                    <label>Họ & tên</label>
                                                    <input type="text" name="name" placeholder="Name"
                                                           value="{{ Auth::guard('student')->user()->name }}">
                                                </div>
                                                <div>
                                                    <label>Số điện thoại</label>
                                                    <input type="text" name="phone" placeholder="Phone"
                                                           value="{{ Auth::guard('student')->user()->phone }}">
                                                </div>
                                                <div>
                                                    <label>Email</label>
                                                    <input type="text" name="email" placeholder="Email"
                                                           value="{{ Auth::guard('student')->user()->email }}">
                                                </div>
                                                <div>
                                                    <label>Ảnh đại diện</label>
                                                    <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@\Auth::guard('student')->user()->image, 150) }}" class="lazy">
                                                    <input type="file" name="image" placeholder="Image"
                                                           value="{{ Auth::guard('student')->user()->image }}">
                                                </div>
                                                {{--<div>
                                                    <label>Ảnh banner</label>
                                                    <img data-src="{{ asset('public/filemanager/userfiles/' . @Auth::guard('student')->user()->banner, 100, null) }}" class="lazy">
                                                    <input type="file" name="banner" placeholder="Banner"
                                                           value="{{ Auth::guard('student')->user()->banner }}">
                                                </div>--}}
                                                <div>
                                                    <label>Ngày sinh</label>
                                                    <input type="date" name="birthday" placeholder="Birthday"
                                                           value="{{ Auth::guard('student')->user()->birthday }}">
                                                </div>
                                                <div>
                                                    <label>Giới tính</label>
                                                    <div class="col-md-6" style="display: inline-block;">
                                                        <label><input type="radio" name="gender" placeholder="Gender"
                                                                      style="width: 50px;"
                                                                      value="1" {{ Auth::guard('student')->user()->gender === 1 ? 'checked' : '' }}>
                                                            Nam
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6" style="display: inline-block;">
                                                        <label>
                                                            <input type="radio" name="gender" placeholder="Gender"
                                                                   style="width: 50px;"
                                                                   value="0" {{ Auth::guard('student')->user()->gender === 0 ? 'checked' : '' }}>
                                                            Nữ
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 pd-none-left col-sm-3 col-3">
                                                        <a class="main-btn3 mr-4" href="/profile">Quay lại
                                                        </a>
                                                    </div>
                                                    <div class="col-md-6 dmk col-sm-6 col-6">
                                                        <a class="pt-2"  href="/student/doi-mat-khau">Đổi mật khẩu
                                                        </a>
                                                    </div>
                                                    <div class="col-md-3 pd-none-right col-sm-3 col-3">
                                                        <button class="main-btn ml-4" type="submit" data-ripple="">Cập nhật
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="central-meta stick-widget">
                                        <span class="create-post">Cấu hình khác</span>
                                        <div class="personal-head">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- content -->
    </div>

@endsection
@section('head_script')
    <link rel="stylesheet" href="{{ URL::asset('public/frontend/themes/edu/css/profile.css') }}?v={{ time() }}">
@endsection


<style>

    footer::before {
        background: rgba(0, 0, 0, 0) linear-gradient(to right, #ffffff 0%, #20117f 50%, #ffffff 100%) repeat scroll 0 0;
    }

    #nprogress .bar, .timeline-info > ul li a::before, .add-btn > a, .activitiez > li::before, form button, a.underline:before, .setting-row input:checked + label, .user-avatar:hover .edit-phto, .add-butn, .nav.nav-tabs.likes-btn > li a.active, a.dislike-btn, .drop > a:hover, .btn-view.btn-load-more:hover, .accordion .card h5 button[aria-expanded="true"], .f-page > figure em, .inbox-panel-head > ul > li > a, footer .widget-title h4::before, #topcontrol, .sidebar .widget-title::before, .g-post-classic > figure > i::after, .purify > a, .open-position::before, .info > a, a.main-btn, .section-heading::before, .more-branches > h4::before, .is-helpful > a, .cart-optionz > li > a:hover, .paginationz > li a:hover, .paginationz > li a.active, .shopping-cart, a.btn2:hover, .form-submit > input[type="submit"], button.submit-checkout, .delete-cart:hover, .proceed .button, .amount-area .update-cart, a.addnewforum, .attachments li.preview-btn button:hover, .new-postbox .post-btn:hover, .weather-date > span, a.learnmore, .banermeta > a:hover, .add-remove-frnd > li a:hover, .profile-controls > li > a:hover, .edit-seting:hover, .edit-phto:hover, .account-delete > div > button:hover, .radio .check-box::after, .eror::after, .eror::before, .big-font, .event-time .main-btn:hover, .group-box > button:hover, .dropcap-head > .dropcap, .checkbox .check-box::after, .checkbox .check-box::before, .main-btn2:hover, .main-btn3:hover, .jalendar .jalendar-container .jalendar-pages .add-event .close-button, .jalendar .jalendar-container .jalendar-pages .days .day.have-event span::before, .user-log > i:hover, .total > i, .login-frm .main-btn, .search-tab .nav-tabs .nav-item > a.active::after, .mh-head, .job-tgs > a:hover, .owl-prev:hover:before, .owl-next:hover:before, .help-list > a, .title2::before, .fun-box > i, .list-style > li a:hover:before, .postbox .we-video-info > button:hover, .postbox .we-video-info > button.main-btn.color, .copy-email > ul li a:hover, .post-status > ul li:hover, .tags_ > a:hover, .policy .nav-link.active::before, a.circle-btn:hover, .mega-menu > li:hover > a > span, .pit-tags > a:hover, .create-post::before, .amount-select > li:hover, .amount-select > li.active, .pay-methods > li:hover, .pay-methods > li.active, .msg-pepl-list .nav-item.unread::before, .menu .btn:hover, .menu-item-has-children ul.submenu > li a::before, .pagination > li a:hover, .pagination > li a.active, .slick-dots li button, .slick-prev:hover:before, .slick-next:hover:before, .sub-popup::before, .sub-popup::after, a.date, .welcome-area > h2::before, .page-header.theme-bg {
        background: #20117f;
    }

    .product-carousel .owl-nav .owl-prev::before, .product-carousel .owl-nav .owl-next::before, .product-caro .owl-prev:hover:before, .product-caro .owl-next:hover:before, .log-reg-area form .forgot-pwd, .log-reg-area form .already-have, .log-reg-area > p a, .timeline-info > ul li a.active, .timeline-info > ul li a:hover, .dropdowns > a.more-mesg, .activity-meta > h6 a, .activity-meta > span a:hover, .description > p a, .we-comment > p a, .sidebar .widget li:hover > a, .sidebar .widget li:hover > i, .friend-meta > a, .user-setting > a:hover, .we-comet li a.showmore, .twiter-feed > li p a, .tutor-links > li i, .tutor-links > li:hover, .pepl-info > span, .frnds .nav-tabs .nav-item a.active, #work > div a, .basics > li i, .education > li i, .groups > span i, a.forgot-pwd, .friend-meta > h4 a:hover, .x_title > h2, .post-meta .detail > span, .add-btn > a:hover, .top-area > ul.main-menu > li > ul li a:hover, .dropdowns.active > a i, .form-group input.form-file ~ .control-label, .form-group input.has-value ~ .control-label, .form-group input:focus ~ .control-label, .form-group input:valid ~ .control-label, .form-group select ~ .control-label, .form-group textarea.form-file ~ .control-label, .form-group textarea.has-value ~ .control-label, .form-group textarea:focus ~ .control-label, .form-group textarea:valid ~ .control-label, .flaged > h3, .invition .friend-meta a.invite:hover, .more-optns > ul li:hover, .post-title > h4 a:hover, .post-title .p-date a:hover, .l-post .l-post-meta > h4 a:hover, .read:hover, .tags > a:hover, .comment-titles > span, .help-list > ul li a:hover i, .carrer-title > span a, .open-position > h4 a:hover, .option-set.icon-style > li > a.selected, .category-box > i, .branches-box > ul li i, .help-topic-result > h2 a:hover, .product-name > h5 a:hover, .full-postmeta .shopnow, .prices.style2 ins span, .single-btn > li > a.active, .total-box > ul > li.final-total, .logout-meta > p a, .forum-list table tbody tr td i, .widget ul.recent-topics > li > i, .date-n-reply > a, .topic-data > span, .help-list > ul li a:hover, .employer-info h2, .job-detail > ul li i, .company-intro > a, .user-setting > ul li a:hover i, .your-page ul.page-publishes > li span:hover i, .drops-menu > li > a:hover .mesg-meta h6, .we-comment > h5:hover, .inline-itms > a:hover, .mesg-meta figure span, .like-dislike > li a:hover, .we-video-info > ul li .users-thumb-list > span strong, .we-video-info > ul li .users-thumb-list > span a, .add-del-friends > a:hover, .story-box:hover .story-thumb > i, .sugtd-frnd-meta > span > a, .sugtd-frnd-meta > a:hover, .create-post > a, .mesg-meta > h6 > a:hover, .profile-menu > li > a:hover, .profile-menu > li > a.active, .friend-name > ins > a, .more-post-optns > ul > li:hover, .more-post-optns > ul > li:hover i, .origin-name > a, .breadcrumb > .breadcrumb-item, .nav-tabs--left .nav-link.active, .nav-tabs--left .nav-link.active:hover, .set-title > span a, .onoff-options .setting-row > p > a, .checkbox > p a, .notifi-seting > p a, .page-likes .tab-content .tab-pane > a, .personal-head > p a, .f-title i, .more-opotnz > ul li a:hover, .frnd-name > a:hover, .option-list ul li a:hover, .option-list ul li i, .smal-box .fileContainer > i, .from-gallery > i, .over-photo > a:hover i, .featurepost > h5 > i, .widget .fav-community > li a, .radio input:checked ~ .check-box::before, .suggestd > li .sug-like:hover i, .gen-metabox > p > a, .widget .invitepage > li > a i, .see-all, .event-title > h4 a:hover, .event-date, .location-map > p, .event-title > span i, .typography > a, .main-btn2, a.main-btn2, blockquote p strong, .dob-meta > h6 a, .recent-jobs li > span a, .recent-jobs li h6 span, .position-meta > span, .invite-location > span, .invite-figure > h6 > a, .user-add > div > i, .logout-form > p > a, .logout-form > a, .login-frm > a, .c-form.search .radio > a, .frnd-meta > a, .notifi-meta > span > i, .card-body a, .search-meta > span i, .pit-frnz-meta > a:hover, .pit-groups-meta > a:hover, .pit-pages-meta > a:hover, .related-searches > li > a:hover, .wiki-box > h4 > a, .wiki-box > p > a, .p-info > a, .widget .reg-comp-meta > ul > li a, .re-links-meta > h6 > a:hover, .pitnik-video-help > i, h3.resutl-found > span, .related-links > li > a:hover, .attachments > ul .add-loc > i, .colla-apps > li a:hover, .add-location-post > span, footer .widget .colla-apps > li a:hover, .list-style > li a:hover, .page-meta > a:hover, .add-pitrest > a, .pitrest-pst-hding:hover, .fa.fa-heart.like, .log-out > li:last-child a, .log-out > li:last-child a i, .loc-cate > ul.loc > li i, .loc-cate > ul > li a, .loc-cate > ul > li::before, .job-price > ins, .users-thumb-list > span > a, .we-video-info > ul li span:hover, .we-video-info .heart:hover, .job-search-form > a, .user-figure > a, .user-info > li span, .main-color, .pit-points > i, .menu-list > li > a > i, .post-up-time > li a, .number > span.active i, .number > input.active, .pit-uzr > a:hover, .pit-post-deta > h4 > a:hover, .view-pst-style > li.active > a, .pit-opt > li.save, .Rpt-meta > span, .pitred-links > ul > li a:hover, .smilez > li > span, .sidebar .comnity-data > ul > li, .comnty-avatar > a:hover, .usr-fig > a:hover, .post-up-time > li .usr-fig > a:hover, .feature-title > h2 > a:hover, .feature-title > h4 > a:hover, .feature-title > h6 > a:hover, .nave-area > li > a > i, .nave-area > li > a:hover, .save-post.save, .tube-title > h6 > a:hover, .chanle-name > a, .channl-author > em, .pit-tags > span, .tube-pst-meta > h5 a:hover, .addnsend > a i, .follow-me:hover, .follow-me:hover i, .contribute:hover, .contribute:hover i, .links-tab li.nav-item > a.active, .post-meta > h6 > a:hover, .fixed-sidebar .left-menu-full > ul li a.closd-f-menu, .fixed-sidebar .left-menu-full > ul li a:hover, .help-box > span, .post-meta .detail > a:hover, .sugested-photos > h5 a, .our-moto > p > span, .sound-right .send-mesg, .title-block .align-left h5 > i, .audio-user-name > h6 a:hover, .add-send > ul > li a, .add-send .send-mesg, .audio-title:hover, .sound-post-box > h4, .singer-info > span, .playlist-box > ul > li:hover, .song-title > h6 > a:hover, .song-title > a:hover, .playlist-box > h4 i, .prise, .location-area > span > i, .classic-pst-meta > h4 a:hover, .total-area > ul li.order-total > i, .classi-pst-meta > span ins, .classi-pst-meta > h6 a:hover, .classi-pst .user-fig a, .msg-pepl-list .nav-item.unread > a > div h6, .chater-info > h6, .text-box > p a, .description > h2 a:hover, span.ttl, .filter-meta > input, .pagination.borderd > li a:hover, .pricings > h1 span, .count i, .testi-meta > span i, .sec-heading.style9 > h2 span, .sec-heading.style9 > span i, .blog-title > a:hover, .serv-box > i, .heading-2 span, .team > h5 span, .popup-closed:hover, .text-caro-meta > span, .text-caro-meta > h1 > a span, .sub-popup > h4 span, .testi-meta::before, .user > a {
        color: #20117f;
    }


</style>