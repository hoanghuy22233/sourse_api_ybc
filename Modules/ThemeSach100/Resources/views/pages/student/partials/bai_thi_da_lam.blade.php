<div class="central-meta" style="width: 100%;">
    <span class="create-post">Bài đã làm</span>
    <div class="quiz_list">
        <table class="table table-striped" style="background-color: white">
            <thead>
            <tr>
                <th>#</th>
                <th>Trình độ</th>
                <th>Loại đề</th>
                <th>Ngày thi</th>
                <th>Điểm</th>
                <th>Lời giải</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $data = \Modules\ThemeSach100\Models\Exam::where('student_id', $student_id)->orderBy('created_at', 'desc')->paginate(20);
            ?>
            @foreach($data as $k => $v)
                <tr>
                    <th scope="row">{{ $k + 1 }}</th>
                    <td>{{ @$v->subject->name }}</td>
                    <td>{{ $v->type }}</td>
                    <td>{{ date('d/m/Y H:i', strtotime($v->created_at)) }}</td>
                    <td>{{ $v->total_point }}/{{ $v->max_point }}</td>
                    <td><a class="" href="/dap-an/{{ $v->id }}" target="_blank">Xem</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $data->appends(Request::all())->links() !!}
    </div>
</div>