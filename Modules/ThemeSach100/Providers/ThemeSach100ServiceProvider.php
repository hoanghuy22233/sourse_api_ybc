<?php

namespace Modules\ThemeSach100\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\EduSettings\Providers\RepositoryServiceProvider;
use App\Models\Setting;
use App\Http\Helpers\CommonHelper;
use View;

class ThemeSach100ServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot(\Illuminate\Http\Request $r)
    {
        //  Cấu hình Core
//            $this->registerTranslations();
//            $this->registerConfig();
        $this->registerViews();
//            $this->registerFactories();
//            $this->loadMigrationsFrom(module_path('ThemeSach100', 'Database/Migrations'));
//            $this->app->register(RepositoryServiceProvider::class);

        //  Cầu hình frontend
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') === false) {
            $this->frontendSettings();
        } elseif (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/student/') !== false) {
            $this->rendBaiThiDaLam($r);
        }

        //  Cấu hình admin/setting
        /*if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/setting') !== false) {
            $this->customSetting();
        }*/

    }

    public function rendBaiThiDaLam(\Illuminate\Http\Request $r) {
        \Eventy::addFilter('edusettings.student_edit_footer', function() use ($r) {
            print view('themesach100::pages.student.partials.bai_thi_da_lam', ['student_id' => $r->id]);
        }, 0, 1);
    }

    public function frontendSettings()
    {
        $settings = CommonHelper::getFromCache('frontend_settings', ['settings']);
        if (!$settings) {
            $settings = Setting::whereIn('type', ['general_tab', 'seo_tab', 'common_tab', 'homepage_tab', 'answer_page_tab'])->pluck('value', 'name')->toArray();
            CommonHelper::putToCache('frontend_settings', $settings, ['settings']);
        }

        View::share('settings', $settings);
        return $settings;
    }

    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('ThemeSach100', 'Config/config.php') => config_path('themesach100.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('ThemeSach100', 'Config/config.php'), 'themesach100'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/themesach100');

        $sourcePath = module_path('ThemeSach100', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/themesach100';
        }, \Config::get('view.paths')), [$sourcePath]), 'themesach100');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/themesach100');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'themesach100');
        } else {
            $this->loadTranslationsFrom(module_path('ThemeSach100', 'Resources/lang'), 'themesach100');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('ThemeSach100', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
