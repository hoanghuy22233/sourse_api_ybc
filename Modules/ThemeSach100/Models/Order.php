<?php

namespace Modules\ThemeSach100\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';

    protected $guarded = [];

    public function course() {
        return $this->belongsTo(\Modules\ThemeSach100\Models\Course::class, 'course_id', 'id');
    }

}
