<?php

namespace Modules\ThemeSach100\Models ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionsError extends Model
{

    protected $table = 'questions_error';
    public $timestamps = false;

    protected $fillable = ['code','reason','content'];





}
