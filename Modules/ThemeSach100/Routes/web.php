<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions', 'locale']], function () {
    Route::group(['prefix' => 'theme'], function () {
        Route::match(['get', 'post'], 'setting', 'Admin\EduThemeController@setting')->name('themesach100')->middleware('permission:theme');
    });
});


Route::get('sitemap.xml', function () {
    return response()->view('themesach100::sitemap.sitemap')->header('Content-Type', 'text/xml');
});
Route::get('post-sitemap.xml', function () {
    return response()->view('themesach100::sitemap.post_sitemap')->header('Content-Type', 'text/xml');
});
Route::get('category-sitemap.xml', function () {
    return response()->view('themesach100::sitemap.category_sitemap')->header('Content-Type', 'text/xml');
});
Route::get('product-sitemap.xml', function () {
    return response()->view('themesach100::sitemap.product_sitemap')->header('Content-Type', 'text/xml');
});


//Route::get('register-course-when-login', 'Frontend\ContactController@registerCourseWhenLogin')->name('register-course-when-login');
Route::group(['prefix' => '', 'middleware' => 'no_auth:student'], function () {
    Route::get('dang-nhap', 'Frontend\AuthController@getLogin')->name('dang-nhap');
    Route::post('dang-nhap', 'Frontend\AuthController@authenticate');
    Route::get('dang-ky', 'Frontend\AuthController@getRegister')->name('dang-ky');
    Route::post('dang-ky', 'Frontend\AuthController@postRegister');
});

Route::match(array('GET', 'POST'), 'quen-mat-khau', 'Frontend\AuthController@getEmailForgotPassword');
Route::match(array('GET', 'POST'), 'forgot-password/{change_password}', 'Frontend\AuthController@getForgotPassword');
Route::get('dang-xuat', function () {
    \Auth::guard('student')->logout();
    return redirect('/');
});

//Route::match(array('GET', 'POST'), 'profile/{id}', 'Frontend\AuthController@profileAdmin')->name('profile');

Route::group(['prefix' => '', 'middleware' => ['guest:student']], function () {
    Route::get('profile', 'Frontend\StudentController@myProfile');
    Route::get('profile/edit', 'Frontend\StudentController@getEditProfile');
    Route::post('profile/edit', 'Frontend\StudentController@postEditProfile');

    Route::group(['prefix' => 'student'], function () {
        Route::get('doi-mat-khau', 'Frontend\StudentController@getChangePass');
        Route::post('doi-mat-khau', 'Frontend\StudentController@postChangePass');

        Route::get('{id}/khoa-hoc', 'Frontend\StudentController@course');
        Route::get('{id}/bai-kiem-tra', 'Frontend\StudentController@quiz');
        Route::get('{id}/diem-thi', 'Frontend\StudentController@getPoint');
        Route::get('{id}/{slug}', 'Frontend\StudentController@misson');
    });
});

Route::get('detail', 'Frontend\CourseController@getDetail');

Route::get('profile/{id}', 'Frontend\StudentController@getProfile');
//Route::get('error', 'Frontend\CourseController@Error')->name('error');
Route::get('note', 'Frontend\CourseController@Note')->name('note');
//Route::post('dang-ky-khoa-hoc', 'Frontend\ContactController@postContact')->name('contact.post');
Route::get('tim-kiem', 'Frontend\CourseController@getSearch');
//Route::get('tim-kiem-post', 'Frontend\CourseController@getSearchPost');
//Route::get('tim-kiem-quizz', 'Frontend\CourseController@getSearchQuizz');

Route::get('lich-hoc', 'Frontend\CourseController@lichHoc');

Route::group(['prefix' => '', 'middleware' => ['guest:student']], function () {
    Route::get('profile', 'Frontend\StudentController@myProfile');
    Route::get('profile/edit', 'Frontend\StudentController@getEditProfile');
    Route::post('profile/edit', 'Frontend\StudentController@postEditProfile');

    Route::group(['prefix' => 'student'], function () {
        Route::get('doi-mat-khau', 'Frontend\StudentController@getChangePass');
        Route::post('doi-mat-khau', 'Frontend\StudentController@postChangePass');

        Route::get('{id}/khoa-hoc', 'Frontend\StudentController@course');
        Route::get('{id}/bai-kiem-tra', 'Frontend\StudentController@quiz');
        Route::get('{id}/diem-thi', 'Frontend\StudentController@getPoint');
        Route::get('{id}/bai-thi', 'Frontend\StudentController@exam');
        Route::get('{id}/{slug}', 'Frontend\StudentController@misson');
    });

    Route::post('tao-bai-thi', 'Frontend\QuestionController@taoBaiThi');
    Route::get('tao-bai-tuong-tu', 'Frontend\QuestionController@taoBaiTuongTu');
    Route::get('vao-thi/{subject_slug}', 'Frontend\QuestionController@vaoThiSubject');
    Route::get('vao-thi/ky-nang/{chapter_slug}', 'Frontend\QuestionController@vaoThiChapter');
    Route::get('vao-thi', 'Frontend\QuestionController@vaoThi');

    Route::match(['GET', 'POST'], 'de-thi/{id}', 'Frontend\QuestionController@deThi');
    Route::get('dap-an/{id}', 'Frontend\QuestionController@dapAn');
    Route::post('bao-cao-loi', 'Frontend\QuestionController@baoCaoLoi')->name('bao-cao-loi');
    Route::match(['GET', 'POST'], 'dang-ky-mon', 'Frontend\QuestionController@dangKyMon');
});

Route::get('bxh-get-content', 'Frontend\QuestionController@bxhGetContent');
Route::get('tao-bai-thi', 'Frontend\QuestionController@taoBaiThi');
Route::get('hoan-thanh-bai-thi', 'Frontend\QuestionController@hoanThanhBaiThi');
Route::get('bang-xep-hang', 'Frontend\QuestionController@bangXepHang');

Route::group(['prefix' => 'gio-hang'],function () {
    Route::get('', 'Frontend\CartController@getCart');
    Route::post('', 'Frontend\CartController@postCart');
    Route::get('add/{id}', 'Frontend\CartController@addCart');
    Route::get('del_item/{rowId}','Frontend\CartController@delItemCart');
    Route::get('del_all_item','Frontend\CartController@delAllItemCart');
    Route::get('update_item/{rowId}/{qty}','Frontend\CartController@updateItemCart');
    Route::get('thank-you', 'Frontend\CartController@getComplete')->name('cart.complete');
    Route::post('ajax-add-cart', 'Frontend\CartController@ajaxAddCart')->name('cart.ajaxAddCart');
});

Route::group(['prefix' => 'thanh-vien'], function () {
    Route::get('', 'Frontend\HomeController@listallMember');
    Route::get('{role_name}', 'Frontend\HomeController@listMember');
});

Route::get('{slug1}', 'Frontend\CourseController@oneParam');
Route::get('{slug1}/{slug2}', 'Frontend\CourseController@twoParam');
Route::get('{slug1}/{slug2}/{slug3}', 'Frontend\CourseController@threeParam');
Route::get('{slug1}/{slug2}/{slug3}/{slug4}', 'Frontend\CourseController@fourParam');

Route::get('', 'Frontend\HomeController@getHome');

//  Dang nhap fb - gg
Route::get('/login/{param}/redirect/', 'Frontend\StudentController@redirect')->name('auth_redirect');//dang nhap
Route::get('/login/{param}/callback/', 'Frontend\StudentController@callback');