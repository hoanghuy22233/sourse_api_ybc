<?php

namespace Modules\EduNotification\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class EduNotificationServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('EduNotification', 'Database/Migrations'));
        //  Custom setting
        $this->registerPermission();

//        //  Cấu hình menu trái
//        $this->rendAsideMenu();
    }


public function registerPermission()
{
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['notification_view', 'notification_add', 'notification_edit', 'notification_delete',]);
            return $per_check;
        }, 1, 1);
}
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('EduNotification', 'Config/config.php') => config_path('edunotification.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('EduNotification', 'Config/config.php'), 'edunotification'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/edunotification');

        $sourcePath = module_path('EduNotification', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/edunotification';
        }, \Config::get('view.paths')), [$sourcePath]), 'edunotification');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/edunotification');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'edunotification');
        } else {
            $this->loadTranslationsFrom(module_path('EduNotification', 'Resources/lang'), 'edunotification');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('EduNotification', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
