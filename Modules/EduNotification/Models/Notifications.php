<?php

namespace Modules\EduNotification\Models;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table = "notifications";
    protected $guarded = [];

    protected $fillable = [
        'name', 'content', 'to_admin_id', 'item_id'
    ];

    public function from_admin()
    {
        return $this->belongsTo(Admin::class,'from_admin_id');
    }

    public function to_admin()
    {
        return $this->belongsTo(Admin::class,'to_admin_id');
    }

}
