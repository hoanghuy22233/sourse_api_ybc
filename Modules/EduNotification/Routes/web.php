<?php
Route::group(['prefix' => 'admin', 'middleware' => ['guest:admin', 'get_permissions']], function () {

    Route::group(['prefix' => 'notification'], function () {
        Route::get('', 'Admin\EduNotificationController@getIndex')->name('notification');
        Route::get('publish', 'Admin\EduNotificationController@getPublish')->name('notification.publish');
        Route::match(array('GET', 'POST'), 'add', 'Admin\EduNotificationController@add');
        Route::get('delete/{id}', 'Admin\EduNotificationController@delete');
        Route::post('multi-delete', 'Admin\EduNotificationController@multiDelete');
        Route::get('search-for-select2', 'Admin\EduNotificationController@searchForSelect2')->name('notification.search_for_select2');
        Route::post('active-order', 'Admin\EduNotificationController@activeOrder')->name('notification.active-order');
        Route::post('active-order-all', 'Admin\EduNotificationController@activeOrderAll')->name('notification.active-order-all');
        Route::get('{id}', 'Admin\EduNotificationController@update');
        Route::post('{id}', 'Admin\EduNotificationController@update');

    });

});
