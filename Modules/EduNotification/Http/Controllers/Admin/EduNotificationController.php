<?php

namespace Modules\EduNotification\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;
use Validator;

class EduNotificationController extends CURDBaseController
{
//    protected $orderByRaw = 'deadline desc';

    protected $module = [
        'code' => 'notification',
        'table_name' => 'notifications',
        'label' => 'Thông báo',
        'modal' => '\Modules\EduNotification\Models\Notifications',
        'list' => [
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên thông báo'],
            ['name' => 'content', 'type' => 'text', 'label' => 'Nội dung'],
            ['name' => 'link', 'type' => 'text', 'label' => 'Tiêu đề'],
            ['name' => 'type', 'type' => 'select', 'options' =>
                [
                    0 => 'Thông báo tới toàn bộ',
                    1 => 'Thông báo đến từng người',
                ], 'label' => 'Loại thông báo'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên thông báo', 'group_class' => 'col-md-6'],
                ['name' => 'link', 'type' => 'text', 'class' => 'required', 'label' => 'Đường dẫn', 'group_class' => 'col-md-6'],
                ['name' => 'content', 'type' => 'textarea_editor', 'label' => 'Nội dung'],
                ['name' => 'type', 'type' => 'select', 'options' =>
                    [
                        0 => 'Thông báo tới toàn bộ',
                        1 => 'Thông báo đến từng người',
                    ], 'label' => 'Loại thông báo', 'group_class' => 'col-md-4'],
            ],
        ],
    ];

    protected $filter = [
        'name' => [
            'label' => 'Tên',
            'type' => 'text',
            'query_type' => 'like'
        ],
        '   type' => [
            'label' => 'Loại thông báo',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Loại thông báo',
                0 => 'Thông báo tới toàn bộ',
                1 => 'Thông báo đến từng người',
            ]
        ],
    ];

    public function getIndex(Request $request)
    {
        //  Check permission
        $data = $this->getDataList($request);

        return view('edunotification::list')->with($data);
    }

    public function add(Request $request)
    {
        try {
            if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, $this->module['code'] . '_add')) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền sử dụng chức năng này!');
                return back();
            }

            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('edunotification::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'title' => 'required',
                    'content' => 'required'
                ], [
                    'title.required' => 'Bắt buộc phải nhập tiêu đề',
                    'content.required' => 'Bắt buộc phải nhập nội dung',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                //  Tùy chỉnh dữ liệu insert
                if ($request->notification_type == 1) {
                    $data['admin_ids'] = $request->admin_ids == null ? null : '|' . implode('|', $request->admin_ids) . '|';
                } elseif ($request->notification_type == 0) {
                    $data['admin_ids'] = null;
                }
//                $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                $data['admin_id'] = \Auth::guard('admin')->user()->id;

                foreach ($data as $k => $v) {
                    $this->model->$k = $v;
                }
                if ($this->model->save()) {
                    $this->afterAddLog($request, $this->model);

                    CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                }

                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $this->model
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        try {
            $item = $this->model->find($request->id);


            if (!is_object($item)) abort(404);
            if (!$_POST) {
                $data = $this->getDataUpdate($request, $item);
                return view('edunotification::edit')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'title' => 'required',
                    'content' => 'required'
                ], [
                    'title.required' => 'Bắt buộc phải nhập tiêu đề',
                    'content.required' => 'Bắt buộc phải nhập nội dung',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else
                    if (!CommonHelper::has_permission(Auth::guard('admin')->user()->id, $this->module['code'] . '_edit')) {
                        CommonHelper::one_time_message('error', 'Bạn không có quyền cập nhật!');
                        return back();
                    }

                $data = $this->processingValueInFields($request, $this->getAllFormFiled());
                //  Tùy chỉnh dữ liệu insert
                if ($request->notification_type == 1) {
                    $data['admin_ids'] = $request->admin_ids == null ? null : '|' . implode('|', $request->admin_ids) . '|';
                } elseif ($request->notification_type == 0) {
                    $data['admin_ids'] = null;
                }
                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        } catch (\Exception $ex) {
//            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function getPublish(Request $request)
    {
        try {

            if (!CommonHelper::has_permission(Auth::guard('admin')->user()->id, $this->module['code'] . '_edit')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xuất bản!'
                ]);
            }

            $id = $request->get('id', 0);
            $item = $this->model->find($id);



            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {

            if (!CommonHelper::has_permission(Auth::guard('admin')->user()->id, $this->module['code'] . '_delete')) {
                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
                return back();
            }

            $item = $this->model->find($request->id);



            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {

            if (!CommonHelper::has_permission(Auth::guard('admin')->user()->id, $this->module['code'] . '_delete')) {
                return response()->json([
                    'status' => false,
                    'msg' => 'Bạn không có quyền xóa!'
                ]);
            }

            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


}
