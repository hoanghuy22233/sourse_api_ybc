<?php

namespace Modules\KitCareBooking\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class KitCareBookingServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        //  Nếu là trang admin thì gọi các cấu hình
        if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') !== false) {
            //  Custom setting
            $this->registerPermission();

            //  Cấu hình menu trái
            $this->rendAsideMenu();
        }
    }

    public function registerPermission()
    {
        \Eventy::addFilter('permission.check', function ($per_check) {
            $per_check = array_merge($per_check, ['product_view', 'product_add', 'product_edit', 'product_delete', 'product_publish',
                'booking_view', 'booking_add', 'booking_edit', 'booking_delete', 'booking_publish',
                'producer_view', 'producer_add', 'producer_edit', 'producer_delete', 'producer_publish',
                'comment_view', 'comment_add', 'comment_edit', 'comment_delete', 'comment_publish',
                'admin_view', 'admin_add', 'admin_edit', 'admin_delete', 'admin_publish',
                'job_view', 'job_add', 'job_edit', 'job_delete', 'job_publish',
                'tag_booking_view', 'tag_booking_add', 'tag_booking_edit', 'tag_booking_delete', 'tag_booking_publish',
                'error_code_view', 'error_code_add', 'error_code_edit', 'error_code_delete', 'error_code_publish',]);
            return $per_check;
        }, 1, 1);
    }

    public function rendAsideMenu() {
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('kitcarebooking::partials.aside_menu.dashboard_after_product');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('kitcarebooking::partials.aside_menu.dashboard_after_producer');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('kitcarebooking::partials.aside_menu.dashboard_after_booking');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('kitcarebooking::partials.aside_menu.dashboard_after_error_code');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('kitcarebooking::partials.aside_menu.dashboard_after_job');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.dashboard_after', function() {
            print view('kitcarebooking::partials.aside_menu.dashboard_after_user');
        }, 1, 1);
        \Eventy::addFilter('aside_menu.user', function() {
            print '';
        }, 1, 1);

    }



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('KitCareBooking', 'Config/config.php') => config_path('kitcarebooking.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('KitCareBooking', 'Config/config.php'), 'kitcarebooking'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/kitcarebooking');

        $sourcePath = module_path('KitCareBooking', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/kitcarebooking';
        }, \Config::get('view.paths')), [$sourcePath]), 'kitcarebooking');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/kitcarebooking');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'kitcarebooking');
        } else {
            $this->loadTranslationsFrom(module_path('KitCareBooking', 'Resources/lang'), 'kitcarebooking');
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path('KitCareBooking', 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
