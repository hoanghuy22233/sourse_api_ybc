@extends(config('core.admin_theme').'.template_blank')
@section('main')
    <div class=" container ">
        <!-- begin::Card-->
        <?php
        $ktv_id = explode('|', trim($booking->ktv_ids, '|'));
        $ktvs = \App\Models\Admin::whereIn('id', $ktv_id)->get();
        ?>
        {{--<a href="/admin/booking/{{ $booking->id }}" class="btn btn-info">Quay lại</a>--}}
        <div class="card card-custom overflow-hidden">
            <div class="card-body p-0">
                <!-- begin: Invoice-->
                <!-- begin: Invoice header-->
                <div class="row justify-content-center py-8 px-8 py-md-27 px-md-0">
                    <div class="col-md-9">
                        <div class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">
                            <img src="{{ asset('public/filemanager/userfiles/' . @$settings['logo']) }}"
                                 style="width: 100px; height: 100px;">
                            <div class="d-flex flex-column align-items-md-end px-0">
                                <!--begin::Logo-->
                                <a href="#" class="mb-5">
                                    {{$settings['name']}}
                                </a>
                                <!--end::Logo-->
                                <span class=" d-flex flex-column align-items-md-end opacity-70">
                            <span> {{$settings['address']}}</span>
                            <span>Hotline: {{$settings['hotline']}}</span>
                        </span>
                            </div>
                        </div>

                        <?php
                        $error_codes = explode('|', trim($booking->error_code_id, '|'));
                        $error_code = \Modules\KitCareBooking\Models\ErrorCode::whereIn('id', $error_codes)->get();


                        $job_codes = explode('|', trim($booking->job_id, '|'));
                        $job_code = \Modules\KitCareBooking\Models\Job::whereIn('id', $job_codes)->get();
                        ?>
                        <p>THÔNG TIN SẢN PHẨM
                        </p>
                        <div class="border-bottom w-100"></div>
                        <div class="d-flex justify-content-between pt-6">
                            <div class="d-flex flex-column flex-root">
                                <span class="font-weight-bold mb-2">Mã SP</span>
                                <span class="opacity-70">{{@$booking->product_code}}</span>
                            </div>
                            <div class="d-flex flex-column flex-root">
                                <span class="font-weight-bold mb-2">Loại SP</span>
                                <span class="opacity-70">{{@$booking->product->name}}</span>
                            </div>
                            <div class="d-flex flex-column flex-root">
                                <span class="font-weight-bold mb-2">Hãng</span>
                                <span class="opacity-70">{{@$booking->producer->name}}<br></span>
                            </div>
                            <div class="d-flex flex-column flex-root">
                                <span class="font-weight-bold mb-2">Lời nhắn</span>
                                <span class="opacity-70">{{@$booking->note}}<br></span>
                            </div>
                            <div class="d-flex flex-column flex-root">
                                <span class="font-weight-bold mb-2">Công việc</span>
                                @foreach($job_code as $job)
                                    <span class="opacity-70" style="background-color: aqua;padding: 3px;}">{{@$job->name}}<br></span>
                                @endforeach

                            </div>
                            <div class="d-flex flex-column flex-root">
                                <span class="font-weight-bold mb-2">Lỗi</span>
                                @foreach($error_code as $error)
                                    <span class="opacity-70" style="background-color: aqua;padding: 3px;}">{{@$error->name}}<br></span>
                                @endforeach

                            </div>
                        </div>

                    </div>
                </div>
                <!-- end: Invoice header-->

                <!-- begin: Invoice body-->
                <div class="row justify-content-center py-8 px-8 py-md-27 px-md-0" style="
    margin: 0;
">
                    <div class="col-md-9" style="
    padding: 0;
">
                        <div class="col-md-6" style="
    display: inline-block;
    float: left;
    padding-left: 0;
">
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th class="pl-0 font-weight-bold text-muted  text-uppercase" style="
    /* width: 100%; */
" colspan="2">Thông tin khách
                                            </th>


                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="font-weight-boldest">
                                            <td class="pl-0 pt-7">Họ &amp; tên</td>
                                            <td class="text-right pt-7">{{@$booking->admin->name}}</td>


                                        </tr>
                                        <tr class="font-weight-boldest border-bottom-0">
                                            <td class="border-top-0 pl-0 py-4">SĐT</td>
                                            <td class="border-top-0 text-right py-4">{{@$booking->admin->tel}}</td>


                                        </tr>
                                        <tr class="font-weight-boldest border-bottom-0">
                                            <td class="border-top-0 pl-0 py-4">Địa chỉ</td>
                                            <td class="border-top-0 text-right py-4">{{@$booking->admin->address}} - {{@$booking->admin->ward->name}} - {{@$booking->admin->district->name}}
                                                - {{@$booking->admin->province->name}}
                                            </td>


                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="
    display: inline-block;
    float: left;
    padding-right: 0;
">
                            <div class="">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th class="pl-0 font-weight-bold text-muted  text-uppercase">KỸ THUẬT VIÊN
                                            </th>


                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($ktvs as $v)
                                            <tr class="font-weight-boldest">
                                                <td class="pl-0 pt-7">Họ &amp; tên</td>
                                                <td class="text-right pt-7">{{@$v->name}}</td>
                                            </tr>
                                            <tr class="font-weight-boldest border-bottom-0">
                                                <td class="border-top-0 pl-0 py-4">SĐT</td>
                                                <td class="border-top-0 text-right py-4">{{@$v->tel }}</td>


                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <p></p>
                    </div>
                </div>
                <!-- end: Invoice body-->

                <!-- begin: Invoice footer-->
                <div class="row justify-content-center bg-gray-100 py-8 px-8 py-md-10 px-md-0">
                    <div class="col-md-9">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="font-weight-bold text-muted  text-uppercase">GHI CHÚ CỦA ADMIN</th>
                                    <th class="font-weight-bold text-muted  text-uppercase">BẢO HÀNH</th>
                                    <th class="font-weight-bold text-muted  text-uppercase">Ngày</th>
                                    <th class="font-weight-bold text-muted  text-uppercase">CHI PHÍ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="font-weight-bolder">
                                    <td>{{@$booking->note_admin}}</td>

                                    @if(@$booking->care==0)
                                        <td>Không bảo hành</td>
                                    @elseif(@$booking->care==1)
                                        <td>1 tháng</td>
                                    @elseif(@$booking->care==2)
                                        <td>2 tháng</td>
                                    @elseif(@$booking->care==3)
                                        <td>3 tháng</td>
                                    @elseif(@$booking->care==4)
                                        <td>4 tháng</td>
                                    @elseif(@$booking->care==5)
                                        <td>5 tháng</td>
                                    @elseif(@$booking->care==6)
                                        <td>6 tháng</td>
                                    @elseif(@$booking->care==7)
                                        <td>7 tháng</td>
                                    @elseif(@$booking->care==8)
                                        <td>8 tháng</td>
                                    @elseif(@$booking->care==9)
                                        <td>9 tháng</td>
                                    @elseif(@$booking->care==10)
                                        <td>10 tháng</td>
                                    @elseif(@$booking->care==11)
                                        <td>11 tháng</td>
                                    @elseif(@$booking->care==12)
                                        <td>12 tháng</td>
                                    @elseif(@$booking->care==24)
                                        <td>24 tháng</td>
                                    @elseif(@$booking->care==36)
                                        <td>36 tháng</td>
                                    @elseif(@$booking->care==48)
                                        <td>48 tháng</td>
                                    @endif


                                    <td>{{date("d/m/Y H:i:s",strtotime(@$booking->time))}}</td>

                                    <td class="text-danger font-size-h3 font-weight-boldest ">{{number_format(@$booking->cost)}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row" style="text-align: center; margin-bottom: 300px;">
                    <div class="col-sm-4">
                        <strong>Khách hàng</strong>
                    </div>
                    <div class="col-sm-4">
                        <strong>Kỹ thuật viên</strong>
                    </div>
                    <div class="col-sm-4">
                        <strong>Quản lý kỹ thuật</strong>
                    </div>

                </div>
                <!-- end: Invoice footer-->

                <!-- begin: Invoice action-->
                <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0">
                    <div class="col-md-9">
                        <div class="d-flex justify-content-between">


                        </div>
                    </div>
                </div>
                <!-- end: Invoice action-->

                <!-- end: Invoice-->
            </div>
        </div>
        {{--<a href="/admin/booking/{{ $booking->id }}" class="btn btn-info">Quay lại</a>--}}
        <!-- end::Card-->
    </div>
@endsection
@section('custom_head')
    <link href="{{ asset('public/backend/themes/metronic1/css/style.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection
