<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Thông tin khách hàng
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <div class="kt-form">
        <div class="kt-portlet__body">
            <div class="row kt-section kt-section--first">
                <div class="col-md-6 kt-avatar kt-avatar--outline" id="kt_user_avatar_image">
                    <div class="kt-avatar__holder">
                        <img style="max-width:150px;padding-top:5px; cursor: pointer;" id="customer_image"
                             src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@$result->admin->image, 100, 100) }}"
                             class="file_image_thumb" title="CLick để phóng to ảnh">
                    </div>
                </div>
                <div class="col-md-6 kt-widget__body">
                    <div class="kt-widget__content">
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Họ & tên:</span>
                            <a id="customer_name" href="/admin/profile/{{ @$result->admin->id }}" class="kt-widget__data">{{ @$result->admin->name }}</a>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">SĐT:</span>
                            <span class="kt-widget__data" id="customer_tel">{{ @$result->admin->tel }}</span>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Email:</span>
                            <span class="kt-widget__data" id="customer_email">{{ @$result->admin->email }}</span>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Tỉnh / Thành:</span>
                            <span class="kt-widget__data" id="customer_province">{{ @$result->admin->province->name }}</span>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Quận / Huyện:</span>
                            <span class="kt-widget__data" id="customer_district">{{ @$result->admin->district->name }}</span>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Phường / Xã:</span>
                            <span class="kt-widget__data" id="customer_ward">{{ @$result->admin->ward->name }}</span>
                        </div>
                        <div class="kt-widget__info">
                            <span class="kt-widget__label">Địa chỉ:</span>
                            <span class="kt-widget__data" id="customer_address">{{ @$result->admin->address }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end::Form-->
</div>

<script>
    $(document).ready(function () {
        getCustomerInfo();
        $('select[name=admin_id]').change(function () {
            getCustomerInfo();
        });
    });

    function getCustomerInfo() {
        var admin_id = $('select[name=admin_id]').val();
        $.ajax({
            url : '/admin/admin/ajax/ajax-get-info',
            data: {
                id : admin_id
            },
            success: function (resp) {
                $('#customer_image').attr('src', resp.data.image);
                $('#customer_name').html(resp.data.name);
                $('#customer_email').html(resp.data.email);
                $('#customer_tel').html(resp.data.tel);
                $('#customer_province').html(resp.data.province_name);
                $('#customer_district').html(resp.data.district_name);
                $('#customer_ward').html(resp.data.ward_name);
                $('#customer_address').html(resp.data.address);
            },
            error: function () {
                alert('Có lỗi xảy ra! Vui lòng load lại website và thử lại!');
            }
        });
    }
</script>