<a href="/admin/{{ $module['code'] }}/{{ $item->id }}{{ isset($_GET['booking_id']) ? '?booking_id=' . $_GET['booking_id'] : '' }}"
   style="    font-size: 14px!important;"
   class="{{ isset($field['tooltip_info']) ? 'a-tooltip_info' : '' }}">Xem</a>

@if( (\Auth::guard('admin')->user()->id == $item->admin_id &&
        in_array($module['code'] . '_delete', $permissions)  &&
        @\Modules\KitCareBooking\Models\Booking::find(@$_GET['booking_id'])->status != 0)
         || in_array('super_admin', $permissions)
         || (!isset($_GET['booking_id']) && @$item->status == 1) )
    | <a href="{{ url('/admin/'.$module['code'].'/delete/' . $item->id) }}{{ isset($_GET['booking_id']) ? '?booking_id=' . $_GET['booking_id'] : '' }}"
       style="    font-size: 14px!important;" title="Xóa bản ghi"
       class="delete-warning {{ isset($field['tooltip_info']) ? 'a-tooltip_info' : '' }}">Xóa</a>
@endif
