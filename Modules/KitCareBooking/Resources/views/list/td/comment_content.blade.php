@if($item->image != null)
    <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, null, 80) }}"
         class="file_image_thumb lazy"><br>
@endif
@if($item->image_present != null)
    <?php
    $imgs = explode('|', $item->image_present);
    ?>
    @foreach($imgs as $img)
        <img data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb($img, null, 50) }}"
             class="file_image_thumb lazy"> |
    @endforeach
    <br>
@endif
{!! $item->content !!}