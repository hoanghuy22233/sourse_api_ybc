@if(in_array('comment_publish', $permissions))
    @if($item->{$field['name']} == 1)
        <a href="{{URL::to('/admin/comment/publish?id='.$item->id.'&column=status')}}"
           class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill"

           style="cursor:pointer;" data-column="{{ $field['name'] }}">Chưa tiếp nhận</a>
    @elseif($item->{$field['name']} == 2)
        <a href="{{URL::to('/admin/comment/publish?id='.$item->id.'&column=status')}}"
           class="kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill"

           style="cursor:pointer;" data-column="{{ $field['name'] }}">Đang yêu cầu</a>
    @elseif($item->{$field['name']} == 3)
        <a href="{{URL::to('/admin/comment/publish?id='.$item->id.'&column=status')}}"
           class="kt-badge  kt-badge--info kt-badge--inline kt-badge--pill"
           style="cursor:pointer;" data-column="{{ $field['name'] }}">Đã tiếp nhận</a>
    @elseif($item->{$field['name']} == 4)
        <a href="{{URL::to('/admin/comment/publish?id='.$item->id.'&column=status')}}"
           class="kt-badge  kt-badge--primary kt-badge--inline kt-badge--pill"
           style="cursor:pointer;" data-column="{{ $field['name'] }}">Đã sửa xong, chờ admin duyệt</a>
    @elseif($item->{$field['name']} == 5)
        <a href="{{URL::to('/admin/comment/publish?id='.$item->id.'&column=status')}}"
           class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill"
           style="cursor:pointer;" data-column="{{ $field['name'] }}">Đã hoàn thành</a>
    @elseif($item->{$field['name']} == 6)
        <a href="{{URL::to('/admin/comment/publish?id='.$item->id.'&column=status')}}"
           class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill"
           style="cursor:pointer;" data-column="{{ $field['name'] }}">Bảo hành sau khi sửa</a>
    @elseif($item->{$field['name']} == 7)
        <a href="{{URL::to('/admin/comment/publish?id='.$item->id.'&column=status')}}"
           class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill"
           style="cursor:pointer;" data-column="{{ $field['name'] }}">Hủy</a>
    @elseif($item->{$field['name']} == 8)
        <a href="{{URL::to('/admin/comment/publish?id='.$item->id.'&column=status')}}"
           class="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill"
           style="cursor:pointer;" data-column="{{ $field['name'] }}">Tư vấn bán mới</a>
    @endif
@else
    @if($item->{$field['name']} == 1)
        <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill"

              style="cursor:pointer;" data-column="{{ $field['name'] }}">Chưa tiếp nhận</span>
    @elseif($item->{$field['name']} == 2)
        <span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill"
              style="cursor:pointer;" data-column="{{ $field['name'] }}">Đang yêu cầu</span>
    @elseif($item->{$field['name']} == 3)
        <span class="kt-badge kt-badge--info kt-badge--inline kt-badge--pill"
              style="cursor:pointer;" data-column="{{ $field['name'] }}">Đã tiếp nhận</span>
    @elseif($item->{$field['name']} == 4)
        <span class="kt-badge kt-badge--primary kt-badge--inline kt-badge--pill"
              style="cursor:pointer;" data-column="{{ $field['name'] }}">Đã sửa xong, chờ admin duyệt</span>
    @elseif($item->{$field['name']} == 5)
        <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill"
              style="cursor:pointer;" data-column="{{ $field['name'] }}">Đã hoàn thành</span>
    @elseif($item->{$field['name']} == 6)
        <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill"
              style="cursor:pointer;" data-column="{{ $field['name'] }}">Bảo hành sau khi sửa</span>
    @elseif($item->{$field['name']} == 7)
        <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill"
              style="cursor:pointer;" data-column="{{ $field['name'] }}">Hủy</span>
    @elseif($item->{$field['name']} == 8)
        <span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill"
              style="cursor:pointer;" data-column="{{ $field['name'] }}">Tư vấn mới</span>
    @endif
@endif


