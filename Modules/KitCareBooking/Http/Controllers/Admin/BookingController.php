<?php

namespace Modules\KitCareBooking\Http\Controllers\Admin;

use App\Http\Controllers\Admin\ChangeDataHistoryController;
use App\Http\Controllers\Admin\ImportController;
use App\Http\Helpers\CommonHelper;
use App\Models\Admin;
use Auth;
use Illuminate\Http\Request;

use Modules\KitCareBooking\Helpers\KitCareBookingHelper;
use Modules\KitCareBooking\Models\Booking;
use Modules\KitCareBooking\Models\District;
use Modules\KitCareBooking\Models\ErrorCode;
use Modules\KitCareBooking\Models\Job;
use Modules\KitCareBooking\Models\Producer;
use Modules\KitCareBooking\Models\Product;
use Modules\KitCareBooking\Models\Province;
use Modules\KitCareBooking\Models\TagBooking;
use Validator;

class BookingController extends CURDBaseController
{
//    protected $orderByRaw = 'status asc, id asc';

    protected $module = [
        'code' => 'booking',
        'table_name' => 'bookings',
        'label' => 'Yêu cầu',
        'modal' => '\Modules\KitCareBooking\Models\Booking',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
            ['name' => 'product_code', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.text_edit_booking', 'label' => 'Mã'],
            ['name' => 'product_id', 'type' => 'relation', 'label' => 'Sản phẩm', 'object' => 'product', 'display_field' => 'name'],
            ['name' => 'producer_id', 'type' => 'relation', 'label' => 'Nhà sản xuẩt', 'object' => 'producer', 'display_field' => 'name'],
            ['name' => 'error_code_id', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.multi_error', 'label' => 'Tình trạng lỗi', 'object' => 'error_code'],
            ['name' => 'tag_id', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.tag_id', 'label' => 'Nhãn', 'object' => 'tag'],

            ['name' => 'bonus_services', 'type' => 'select', 'options' =>
                [
                    1 => 'Sửa DV',
                    2 => 'Bảo hành MEEG',
                ], 'label' => 'Phân loại'],
            ['name' => 'tel', 'type' => 'text', 'label' => 'SĐT'],
            ['name' => 'time', 'type' => 'date_vi', 'label' => 'Ngày hẹn'],
            ['name' => 'created_at', 'type' => 'date_vi', 'label' => 'Ngày tạo'],
            ['name' => 'cost', 'type' => 'price', 'label' => 'Chi phí'],
//            ['name' => 'custom', 'type' => 'role_name', 'label' => 'Quyền đăng'],
            ['name' => 'admin_id', 'type' => 'relation', 'object' => 'admin', 'display_field' => 'name', 'label' => 'Khách hàng',],
            ['name' => 'create_by', 'type' => 'relation', 'object' => 'createBy', 'display_field' => 'name', 'label' => 'Người đăng',],
            ['name' => 'payment_status', 'type' => 'select', 'label' => 'Thanh toán', 'options' => [
                0 => 'Chưa thanh toán',
                1 => 'Đã thanh toán',
            ],],
            ['name' => 'care', 'type' => 'select', 'label' => 'Bảo hành', 'options' => [
                0 => 'Không bảo hành',
                1 => '1 tháng',
                2 => '2 tháng',
                3 => '3 tháng',
                4 => '4 tháng',
                5 => '5 tháng',
                6 => '6 tháng',
                7 => '7 tháng',
                8 => '8 tháng',
                9 => '9 tháng',
                10 => '10 tháng',
                11 => '11 tháng',
                12 => '12 tháng',
                24 => '24 tháng',
                36 => '36 tháng',
                48 => '48 tháng',
            ],],
//            ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ'],
//            ['name' => 'province_id', 'type' => 'relation', 'label' => 'Tỉnh', 'object' => 'province', 'display_field' => 'name'],
//            ['name' => 'district_id', 'type' => 'relation', 'label' => 'Huyện', 'object' => 'district', 'display_field' => 'name'],
            ['name' => 'ktv_ids', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.multi_care', 'label' => 'KTV', 'object' => 'ktv_ids'],
            ['name' => 'status', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.status_booking', 'label' => 'Trang thái'],
            ['name' => 'deadline', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.deadline', 'label' => 'Deadline'],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'product_code', 'type' => 'text', 'class' => 'required', 'label' => 'Mã sản phẩm'],
                ['name' => 'product_id', 'type' => 'select2_ajax_model', 'label' => 'Danh mục sản phẩm', 'model' => Product::class, 'object' => 'product', 'display_field' => 'name', 'group_class' => 'col-md-4'],
                ['name' => 'error_code_id', 'type' => 'custom', 'type_history' => 'relation_multiple', 'field' => 'kitcarebooking::form.fields.multi_cat', 'label' => 'Tình trạng lỗi', 'model' => ErrorCode::class,
                    'object' => 'error_code', 'display_field' => 'name', 'multiple' => true, 'group_class' => 'col-md-4',],
                ['name' => 'producer_id', 'type' => 'select2_ajax_model', 'label' => 'Nhà sản xuất', 'model' => Producer::class, 'object' => 'producer', 'display_field' => 'name', 'group_class' => 'col-md-4'],
//                ['name' => 'error_code_id', 'type' => 'select2_ajax_model', 'label' => 'Tình trạng lỗi', 'model' => ErrorCode::class, 'object' => 'error_code', 'display_field' => 'name', 'group_class' => 'col-md-4'],
//                ['name' => 'province_id', 'type' => 'custom', 'type_history' => 'relation_multiple', 'field' => 'kitcarebooking::form.fields.select_location', 'label' => 'Tỉnh thành', 'model' => Province::class,
//                    'object' => 'province', 'display_field' => 'name', 'ward' => false],
//                ['name' => 'district_id', 'type' => 'select2_ajax_model', 'label' => 'Huyện', 'model' => District::class, 'object' => 'district', 'display_field' => 'name', 'group_class' => 'col-md-4'],

//                ['name' => 'tel', 'type' => 'text', 'label' => 'SĐT', 'group_class' => 'col-md-4'],
//                ['name' => 'address', 'type' => 'text', 'label' => 'Địa chỉ', 'group_class' => 'col-md-6'],
                ['name' => 'time', 'type' => 'datetime-local', 'label' => 'Ngày hẹn', 'group_class' => 'col-md-6'],
                ['name' => 'payment_status', 'type' => 'select', 'options' =>
                    [
                        0 => 'Chưa thanh toán',
                        1 => 'Đã thanh toán',
                    ], 'class' => '', 'label' => 'Trạng thái thanh toán', 'value' => 0, 'group_class' => 'col-md-4'],
                ['name' => 'care', 'type' => 'select', 'options' =>
                    [
                        0 => 'Không bảo hành',
                        1 => '1 tháng',
                        2 => '2 tháng',
                        3 => '3 tháng',
                        4 => '4 tháng',
                        5 => '5 tháng',
                        6 => '6 tháng',
                        7 => '7 tháng',
                        8 => '8 tháng',
                        9 => '9 tháng',
                        10 => '10 tháng',
                        11 => '11 tháng',
                        12 => '12 tháng',
                        24 => '24 tháng',
                        36 => '36 tháng',
                        48 => '48 tháng',
                    ], 'class' => '', 'label' => 'Bảo hành', 'value' => 0, 'group_class' => 'col-md-4'],
                ['name' => 'status', 'type' => 'select', 'options' =>
                    [
                        1 => 'Chưa tiếp nhận',
                        2 => 'Đang yêu cầu',
                        3 => 'Đã tiếp nhận',
                        4 => 'Đã sửa xong, chờ admin duyệt',
                        5 => 'Đã hoàn thành',
                        6 => 'Bảo hành sau khi sửa',
                        7 => 'Hủy',
                        8 => 'Tư vấn bán mới',
                    ], 'class' => '', 'label' => 'Trạng thái', 'value' => 1, 'group_class' => 'col-md-4'],
                ['name' => 'ktv_ids', 'type' => 'custom', 'type_history' => 'relation_multiple', 'field' => 'kitcarebooking::form.fields.select2_model', 'multiple' => true, 'label' => 'Kỹ thuật viên', 'model' => Admin::class, 'display_field' => 'name'],
                ['name' => 'job_id', 'type' => 'custom', 'type_history' => 'relation_multiple', 'field' => 'kitcarebooking::form.fields.multi_cat', 'label' => 'Công việc', 'model' => Job::class,
                    'object' => 'job', 'display_field' => 'name', 'multiple' => true],
                ['name' => 'tag_id', 'type' => 'custom', 'type_history' => 'relation_multiple', 'field' => 'kitcarebooking::form.fields.tags', 'label' => 'Tag', 'model' => TagBooking::class,
                    'object' => 'tag_booking', 'display_field' => 'name', 'multiple' => true,],
                ['name' => 'cost', 'type' => 'text', 'class' => 'number_price', 'label' => 'Chi phí', 'group_class' => 'col-md-6'],
                ['name' => 'bonus_services', 'type' => 'select', 'options' =>
                    [
                        1 => 'Sửa DV',
                        2 => 'Bảo hành MEEG',
                    ], 'class' => '', 'label' => 'Phân loại', 'value' => 1, 'group_class' => 'col-md-6'],
                ['name' => 'note', 'type' => 'textarea', 'label' => 'Lời nhắn', 'group_class' => 'col-md-6'],
                ['name' => 'note_user', 'type' => 'textarea', 'label' => 'Ghi chú của KTV', 'group_class' => 'col-md-6'],
                ['name' => 'note_admin', 'type' => 'textarea', 'label' => 'Ghi chú của Admin', 'group_class' => 'col-md-6'],
            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh đại diện'],
                ['name' => 'image_present', 'type' => 'multiple_image_dropzone', 'count' => '6', 'label' => 'Ảnh khác'],
                ['name' => 'image_extra', 'type' => 'multiple_image_dropzone', 'count' => '6', 'label' => 'Ảnh KTV chụp khi hoàn thành'],
            ],
            'customer_tab' => [
                ['name' => 'admin_id', 'class' => 'required', 'type' => 'custom', 'type_history' => 'relation_multiple', 'field' => 'kitcarebooking::form.fields.select_customer', 'label' => 'Khách hàng', 'model' => Admin::class, 'object' => 'admin', 'display_field' => 'name', 'display_field2' => 'tel'],
            ],
            'comment_tab' => [
                ['name' => 'iframe', 'type' => 'iframe', 'class' => 'col-xs-12 col-md-8 padding-left', 'src' => '/admin/comment?booking_id={id}', 'inner' => 'style="min-height: 485px;"'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, mã SP, sđt, ghi chú',
        'fields' => 'id, product_code, tel, note, note_user, note_admin, address, cost'
    ];

    protected $filter = [
        'created_at' => [
            'label' => 'Ngày tạo',
            'type' => 'date',
            'query_type' => 'like'
        ],
        'time' => [
            'label' => 'Ngày hẹn',
            'type' => 'date',
            'query_type' => 'like'
        ],
        'producer_id' => [
            'label' => 'Nhà sản xuất',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'producer',
            'model' => Producer::class,
            'query_type' => 'custom'
        ],
        'product_id' => [
            'label' => 'Sản phẩm',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'product',
            'model' => Product::class,
            'query_type' => 'custom'
        ],
        'user_id' => [
            'label' => 'Khách hàng',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'admin',
            'model' => Admin::class,
            'query_type' => 'custom',
        ],
        'admin_id' => [
            'label' => 'KTV',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'admin',
            'model' => Admin::class,
            'query_type' => 'custom',
        ],
        'tag_id' => [
            'label' => 'Từ khóa',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'tag_booking',
            'model' => TagBooking::class,
            'query_type' => 'custom'
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                1 => 'Chưa tiếp nhận',
                2 => 'Đang yêu cầu',
                3 => 'Đã tiếp nhận',
                4 => 'Đã sửa xong, chờ admin duyệt',
                5 => 'Đã hoàn thành',
                6 => 'Bảo hành sau khi sửa',
                7 => 'Hủy',
                8 => 'Tư vấn bán mới',
            ],
        ],
        'bonus_services' => [
            'label' => 'Phân loại',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                1 => 'Sửa DV',
                2 => 'Bảo hành MEEG',
            ],
        ],

    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('kitcarebooking::list')->with($data);
    }

    public function appendWhere($query, $request)
    {
        if (!is_null($request->get('admin_id'))) {
            $query = $query->where('ktv_ids', 'like', '%|' . $request->admin_id . '|%');
        }
        if (!is_null($request->get('user_id'))) {
            $query = $query->where('admin_id', $request->user_id);
        }

        if (!is_null($request->get('tag_id'))) {
            $query = $query->where('tag_id', 'LIKE', '%|' . $request->tag_id . '|%');
        }

        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu mình tạo
        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
            $query = $query->where('create_by', \Auth::guard('admin')->user()->id);
        }


        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('kitcarebooking::add')->with($data);
            } else if ($_POST) {
                $validator = Validator::make($request->all(), [
                    'product_code' => 'required'
                ], [
                    'product_code.required' => 'Bắt buộc phải nhập mã sản phẩm',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('ktv_ids')) {
                        $data['ktv_ids'] = '|' . implode('|', $request->ktv_ids) . '|';
                    }
                    if ($request->has('tag_id')) {
                        $data['tag_id'] = '|' . implode('|', $request->tag_id) . '|';
                    }
                    if ($request->has('job_id')) {
                        $data['job_id'] = '|' . implode('|', $request->job_id) . '|';
                    }
                    if ($request->has('error_code_id')) {
                        $data['error_code_id'] = '|' . implode('|', $request->error_code_id) . '|';
                    }
                    if ($request->has('image_present')) {
                        $data['image_present'] = implode('|', $request->image_present);
                    }
                    if ($request->has('image_extra')) {
                        $data['image_extra'] = implode('|', $request->image_extra);
                    }
                    unset($data['iframe']);
                    $data['province_id'] = \Auth::guard('admin')->user()->province_id;
                    $data['district_id'] = \Auth::guard('admin')->user()->district_id;
                    $data['address'] = \Auth::guard('admin')->user()->address;
                    $data['tel'] = @Admin::find($data['admin_id'])->tel;
                    $data['create_by'] = \Auth::guard('admin')->user()->id;

//                    $data['district_id'] = $request->district_id;

                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {

                        //  Gửi email
                        KitCareBookingHelper::sendMailToAdmin($this->model, 16);
                        KitCareBookingHelper::setDeadline($this->model);

                        //  Gửi thông báo cho kỹ thuật viên được nhận đơn
                        if (isset($data['ktv_ids'])) {
                            if (!empty($request->ktv_ids)) {
                                $ktvs = Admin::whereIn('id', $request->ktv_ids)->pluck('name');
                                $ktv_name = '';
                                foreach ($ktvs as $v) {
                                    $ktv_name .= $v . ', ';
                                }
                                KitCareBookingHelper::pushNotiFication($this->model, 'Mới tạo & Kỹ thuật viên ' . $ktv_name . ' đã phụ trách đơn', [\Auth::guard('admin')->id()]);
                            } else {
                                KitCareBookingHelper::pushNotiFication($this->model, 'Mới tạo & Không có kỹ thuật viên nào phụ trách đơn', [\Auth::guard('admin')->id()]);
                            }
                        }

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('kitcarebooking::edit')->with($data);
        } else if ($_POST) {
            $validator = Validator::make($request->all(), [
                'product_code' => 'required'
            ], [
                'product_code.required' => 'Bắt buộc phải nhập mã sản phẩm',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                \DB::beginTransaction();

                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert

                if ($request->has('ktv_ids')) {
                    $data['ktv_ids'] = '|' . implode('|', $request->ktv_ids) . '|';
                }
                if ($request->has('job_id')) {
                    $data['job_id'] = '|' . implode('|', $request->job_id) . '|';
                }
                if ($request->has('tag_id')) {
                    $data['tag_id'] = '|' . implode('|', $request->tag_id) . '|';
                }
                if ($request->has('error_code_id')) {
                    $data['error_code_id'] = '|' . implode('|', $request->error_code_id) . '|';
                }

                if ($request->has('image_present')) {
                    $data['image_present'] = implode('|', $request->image_present);
                }
                if ($request->has('image_extra')) {
                    $data['image_extra'] = implode('|', $request->image_extra);
                }
                #
                unset($data['iframe']);
                unset($data['admin_id']);


                if ($data['ktv_ids'] != $item->ktv_ids) {
                    $item->ktv_ids = $data['ktv_ids'];
                    if (!empty($request->ktv_ids)) {
                        $ktvs = Admin::whereIn('id', $request->ktv_ids)->pluck('name');
                        $ktv_name = '';
                        foreach ($ktvs as $v) {
                            $ktv_name .= $v . ', ';
                        }
                        KitCareBookingHelper::pushNotiFication($item, 'Kỹ thuật viên ' . $ktv_name . ' đã phụ trách đơn', [\Auth::guard('admin')->id()]);
                    } else {
                        KitCareBookingHelper::pushNotiFication($item, 'Không có kỹ thuật viên nào phụ trách đơn', [\Auth::guard('admin')->id()]);
                    }
                }
//                $data['district_id'] = $request->district_id;
                if (@$data['time'] != null) {
                    $data['time'] = date('Y-m-d H:i:s', strtotime($data['time']));
                }

                $changeDataHistoryController = new ChangeDataHistoryController();
                $changeDataHistoryController->addLog($request, $item, $this->module, $data, \Auth::guard('admin')->user()->id, $request->get('reason_for_editing', ''));

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }

                if ($item->save()) {
                    KitCareBookingHelper::setDeadline($item);
                    \DB::commit();
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    \DB::rollBack();
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }

    public function print($id)
    {
        $data['booking'] = Booking::find($id);
        return view('kitcarebooking::print')->with($data);
    }

    /**
     * Tối đa import được 999 dòng
     */
    public function importExcel(Request $r)
    {
        $table_import = $r->has('table') ? $r->table : $this->module['table_name'];
        $validator = Validator::make($r->all(), [
            'module' => 'required',
        ], [
            'module.required' => 'Bắt buộc phải nhập module!',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            $importController = new \App\Http\Controllers\Admin\ImportController();
            $data = $this->processingValueInFields($r, $importController->getAllFormFiled());
            //  Tùy chỉnh dữ liệu insert

            if ($r->has('file')) {
                $file_name = $r->file('file')->getClientOriginalName();
                $file_name = str_replace(' ', '', $file_name);
                $file_name_insert = date('s_i_') . $file_name;
                $r->file('file')->move(base_path() . '/public/filemanager/userfiles/imports/', $file_name_insert);
                $data['file'] = 'imports/' . $file_name_insert;
            }

            unset($data['field_options_key']);
            unset($data['field_options_value']);
            #
            $item = new \App\Models\Import();
            foreach ($data as $k => $v) {
                $item->$k = $v;
            }

            if ($item->save()) {
                //  Import dữ liệu vào
                $this->updateAttributes($r, $item);
                $this->processingImport($r, $item);

                CommonHelper::flushCache($table_import);
                CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                return redirect('/admin/import');
            } else {
                CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
            }

            if ($r->ajax()) {
                return response()->json([
                    'status' => true,
                    'msg' => '',
                    'data' => $item
                ]);
            }

            return redirect('/admin/import');
        }
    }

    public function updateAttributes($r, $item)
    {
        $table_import = $r->has('table') ? $r->table : $this->module['table_name'];
        if ($r->has('field_options_key')) {
            $key_update = [];
            foreach ($r->field_options_key as $k => $key) {
                if ($key != null && $r->field_options_value[$k] != null) {
                    $key_update[] = $key;
                    \App\Models\Attribute::updateOrCreate([
                        'key' => $key,
                        'table' => $table_import,
                        'type' => 'field_options',
                        'item_id' => $item->id
                    ], [
                        'value' => $r->field_options_value[$k]
                    ]);
                }
            }
            if (!empty($key_update)) {
                \App\Models\Attribute::where([
                    'table' => $table_import,
                    'type' => 'field_options',
                    'item_id' => $item->id
                ])->whereNotIn('key', $key_update)->delete();
            }
        } else {
            \App\Models\Attribute::where([
                'table' => $table_import,
                'type' => 'field_options',
                'item_id' => $item->id
            ])->delete();
        }
        return true;
    }

    public function processingImport($r, $item)
    {
        $table_import = $r->has('table') ? $r->table : $this->module['table_name'];
        $record_total = $record_success = 0;
        $dataInsertFix = \App\Models\Attribute::where('table', $table_import)->where('type', 'field_options')->where('item_id', @$item->id)->pluck('value', 'key')->toArray();

        \Excel::load('public/filemanager/userfiles/' . $item->file, function ($reader) use ($r, $dataInsertFix, &$model, &$record_total, &$record_success) {
            $reader->each(function ($sheet) use ($r, $reader, $dataInsertFix, &$model, &$record_total, &$record_success) {
                if ($reader->getSheetCount() == 1) {

                    $result = $this->importItem($sheet, $r, $dataInsertFix);
                    if ($result['status']) {
                        $record_total++;
                    }
                    if ($result['import']) {
                        $record_success++;
                    }
                } else {
                    $sheet->each(function ($row) use ($r, $dataInsertFix, &$model, &$record_total, &$record_success) {
                        $result = $this->importItem($row, $r, $dataInsertFix);
                        if ($result['status']) {
                            $record_total++;
                        }
                        if ($result['import']) {
                            $record_success++;
                        }
                    });
                }
            });
        });
        $item->record_total = $record_total;
        $item->record_success = $record_total;
        $item->save();
        return true;
    }

    //  Xử lý import 1 dòng excel
    public function importItem($row, $r, $dataInsertFix)
    {
        try {
            //  Kiểm tra trường dữ liêu bắt buộc có
            $fields_require = ['so_dien_thoai'];
            foreach ($fields_require as $field_require) {
                if (!isset($row->{$field_require}) || $row->{$field_require} == '' || $row->{$field_require} == null) {
                    return false;
                }
            }

            $row_empty = true;
            foreach ($row->all() as $key => $value) {
                if ($value != null) {
                    $row_empty = false;
                }
            }

            //  Các trường không được trùng
            /*$item_model = new $this->module['modal'];
            $item = $item_model->where('tel', $row->all()['tel'])->first();
            if (!is_object($item)) {
                $item = $item_model;
            }*/

            /*if ($this->import[$request->module]['unique']) {
                $field_name = $this->import[$request->module]['fields'][$this->import[$request->module]['unique']];
                $model_new = new $this->import[$request->module]['modal'];
                $model = $model_new->where($field_name, $row->{$this->import[$request->module]['unique']})->first();
            }*/

            if (!$row_empty) {
                $data = [];

                //  Gán các dữ liệu được fix cứng từ view
                foreach ($dataInsertFix as $k => $v) {
                    $data[$k] = $v;
                }

                //  Set mặc định import là bảo hành Meeg
                $data['bonus_services'] = 2;

                //  Chèn các dữ liệu lấy vào từ excel
                foreach ($row->all() as $key => $value) {
                    switch ($key) {
                        case 'so_dien_thoai':
                            {
                                $customer = Admin::where('tel', $value)->first();
                                if (!is_object($customer)) {
                                    $customer = new Admin();
                                    $customer->tel = $data['tel'] = $value;
                                    $customer->name = $row->all()['ten_khach_hang'];
                                    $customer->email = $row->all()['email'];
                                    $customer->address = $data['address'] = $row->all()['dia_chi_khach_hang'];
                                    $customer->save();
                                } else {
                                    $data['tel'] = @$customer->tel;
                                    $data['address'] = @$customer->address;
                                    $data['province_id'] = @$customer->province_id;
                                    $data['district_id'] = @$customer->district_id;
                                }
                                $data['admin_id'] = @$customer->id;
                                break;
                            }
                        case 'trang_thai_kieu_bao_hanh':
                            {
                                $data['note'] = $value;
                                break;
                            }
                        case 'thoi_diem':
                            {
                                $d = @explode('/', $value)[0];
                                $m = @explode('/', $value)[1];
                                $y = @explode('/', $value)[2];
                                $data['time'] = date('Y-m-d H:i:s', strtotime($y . '/' . $m . '/' . $d));
                                break;
                            }
                        case 'ten_san_pham':
                            {
                                $data['product_code'] = trim($value);
                                break;
                            }
                        case 'link_anh_san_pham':
                            {
//                            $data['note_admin'] = trim($value);
                                break;
                            }
                        default:
                            {
                                if (\Schema::hasColumn($r->table, $key)) {
                                    $data[$key] = $value;
                                }
                            }
                    }
                }
                if (\DB::table($r->table)->insert($data)) {
                    return [
                        'status' => true,
                        'import' => true
                    ];
                }
            }
        } catch (\Exception $ex) {
            return [
                'status' => true,
                'import' => false,
                'msg' => $ex->getMessage()
            ];
        }
    }
}
