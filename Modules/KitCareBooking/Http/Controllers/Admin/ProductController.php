<?php

namespace Modules\KitCareBooking\Http\Controllers\Admin;

use App\Http\Helpers\CommonHelper;
use Auth;
use Illuminate\Http\Request;

use Validator;

class ProductController extends CURDBaseController
{
    protected $module = [
        'code' => 'product',
        'table_name' => 'products',
        'label' => 'Danh mục sản phẩm',
        'modal' => '\Modules\KitCareBooking\Models\Product',
        'list' => [
            ['name' => 'image', 'type' => 'image', 'label' => 'Ảnh', 'width' => '60px'],
            ['name' => 'name', 'type' => 'text_edit', 'label' => 'Tên sản phẩm'],
//            ['name' => 'code', 'type' => 'text', 'label' => 'Mã'],
//            ['name' => 'final_price', 'type' => 'price_vi', 'label' => 'Giá bán'],
//            ['name' => 'multi_cat', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.multi_cat', 'label' => 'Danh mục', 'object' => 'category_product'],
//            ['name' => 'tags', 'type' => 'custom', 'td' => 'kitcarebooking::list.td.multi_cat', 'label' => 'Từ khóa', 'object' => 'tag_product'],
//            ['name' => 'popular', 'type' => 'status', 'label' => 'Nổi bật'],
            ['name' => 'status', 'type' => 'status', 'label' => 'Trang thái'],
            ['name' => 'admin_id', 'type' => 'relation', 'object' => 'admin', 'display_field' => 'name', 'label' => 'Người tạo',],
        ],
        'form' => [
            'general_tab' => [
                ['name' => 'name', 'type' => 'text', 'class' => 'required', 'label' => 'Tên sản phẩm'],
//                ['name' => 'code', 'type' => 'text', 'label' => 'Mã sản phẩm', 'group_class' => 'col-md-4'],
//                ['name' => 'base_price', 'type' => 'text', 'label' => 'Giá ban đầu', 'class' => 'number_price', 'group_class' => 'col-md-4'],
//                ['name' => 'final_price', 'type' => 'text', 'label' => 'Giá bán', 'class' => 'number_price', 'group_class' => 'col-md-4'],
//                ['name' => 'multi_cat', 'type' => 'custom', 'field' => 'kitcarebooking::form.fields.multi_cat', 'label' => 'Danh mục sản phẩm', 'model' => \Modules\KitCareBooking\Models\Category::class,
//                    'object' => 'category_product', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=5', 'des' => 'Danh mục đầu tiên chọn là danh mục chính'],
//                ['name' => 'tags', 'type' => 'custom', 'field' => 'kitcarebooking::form.fields.tags', 'label' => 'Từ khóa sản phẩm', 'model' => \Modules\KitCareBooking\Models\Category::class,
//                    'object' => 'tag_product', 'display_field' => 'name', 'multiple' => true, 'where' => 'type=6'],
                ['name' => 'intro', 'type' => 'textarea', 'label' => 'Mô tả ngắn'],
                ['name' => 'content', 'type' => 'textarea_editor', 'class' => '', 'label' => 'Nội dung'],
                ['name' => 'status', 'type' => 'checkbox', 'label' => 'Kích hoạt', 'value' => 1, 'group_class' => 'col-md-4'],
                ['name' => 'order_no', 'type' => 'number', 'label' => 'Thứ tự ưu tiên', 'value' => 0, 'group_class' => 'col-md-4', 'des' => 'Số to ưu tiên hiển thị trước'],
//                ['name' => 'popular', 'type' => 'checkbox', 'label' => 'Nổi bật', 'group_class' => 'col-md-4'],
            ],

            'image_tab' => [
                ['name' => 'image', 'type' => 'file_editor', 'label' => 'Ảnh sản phẩm'],
//                ['name' => 'image_template', 'type' => 'custom', 'field' => 'kitcarebooking::form.fields.text', 'label' => 'Template cho ảnh đại diện'],//
//                ['name' => 'image_template', 'type' => 'custom', 'field' => 'kitcarebooking::form.fields.text', 'label' => 'Template cho ảnh đại diện'],//
//                ['name' => 'image_extra', 'type' => 'custom', 'field' => 'kitcarebooking::form.fields.multiple_image_link', 'count' => '6', 'label' => 'Ảnh khác'],//
                ['name' => 'image_extra', 'type' => 'multiple_image_dropzone', 'count' => '6', 'label' => 'Ảnh khác'],//
//                ['name' => 'image_extra2', 'type' => 'multiple_image_dropzone2', 'count' => '6', 'label' => 'Ảnh khác'],//
            ],

            'seo_tab' => [
                ['name' => 'slug', 'type' => 'slug', 'class' => 'required', 'label' => 'Slug', 'des' => 'Đường dẫn sản phẩm trên thanh địa chỉ'],
                ['name' => 'meta_title', 'type' => 'text', 'label' => 'Meta title'],
                ['name' => 'meta_description', 'type' => 'text', 'label' => 'Meta description'],
                ['name' => 'meta_keywords', 'type' => 'text', 'label' => 'Meta keywords'],
            ],
        ],
    ];

    protected $quick_search = [
        'label' => 'ID, tên, mã, giá',
        'fields' => 'id, name, code, final_price'
    ];

    protected $filter = [
        'category_id' => [
            'label' => 'Danh mục',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'category_product',
            'model' => \Modules\JdesProduct\Models\Category::class,
            'query_type' => 'custom'
        ],
        'tag_id' => [
            'label' => 'Từ khóa',
            'type' => 'select2_ajax_model',
            'display_field' => 'name',
            'object' => 'tag_product',
            'model' => \Modules\JdesProduct\Models\Category::class,
            'query_type' => 'custom'
        ],
        'popular' => [
            'label' => 'Nổi bật',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
        ],
        'status' => [
            'label' => 'Trạng thái',
            'type' => 'select',
            'query_type' => '=',
            'options' => [
                '' => 'Trạng thái',
                0 => 'Không kích hoạt',
                1 => 'Kích hoạt',
            ],
        ],
    ];

    public function getIndex(Request $request)
    {
        $data = $this->getDataList($request);

        return view('kitcarebooking::product.list')->with($data);
    }

    public function appendWhere($query, $request)
    {
//        //  Nếu không có quyền xem toàn bộ dữ liệu thì chỉ được xem các dữ liệu của công ty mình
//        if (!CommonHelper::has_permission(\Auth::guard('admin')->user()->id, 'view_all_data')) {
//            $query = $query->where('company_id', \Auth::guard('admin')->user()->last_company_id);
//        }

        //  Lọc theo danh mục
        if (!is_null($request->get('category_id'))) {
            $category = Category::find($request->category_id);
            if (is_object($category)) {
                $query = $query->where(function ($query) use ($category) {
                    $cat_childs = $category->childs; //  Lấy các id của danh mục con
                    $query->orWhere('multi_cat', 'LIKE', '%|' . $category->id . '|%');  // truy vấn các tin thuộc danh mục hiện tại
                    foreach ($cat_childs as $cat_child) {
                        $query->orWhere('multi_cat', 'LIKE', '%|' . $cat_child->id . '|%');    //  truy vấn các tin thuộc các danh mục con của danh mục hiện tại
                    }
                });
            }
        }
        if (!is_null($request->get('tag_id'))) {
            $query = $query->where('multi_cat', 'LIKE', '%|' . $request->tag_id . '|%');
        }
        return $query;
    }

    public function add(Request $request)
    {
        try {
            if (!$_POST) {
                $data = $this->getDataAdd($request);
                return view('kitcarebooking::product.add')->with($data);
            } else if ($_POST) {
//                dd($request->all());
                $validator = Validator::make($request->all(), [
                    'name' => 'required'
                ], [
                    'name.required' => 'Bắt buộc phải nhập tên',
                ]);
                if ($validator->fails()) {
                    return back()->withErrors($validator)->withInput();
                } else {
                    $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                    //  Tùy chỉnh dữ liệu insert
                    if ($request->has('multi_cat')) {
                        $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                        $data['category_id'] = $request->multi_cat[0];
                    }
                    if ($request->has('tags')) {
                        $data['tags'] = '|' . implode('|', $request->tags) . '|';
                    }
                    if ($request->has('image_extra')) {
                        $data['image_extra'] = implode('|', $request->image_extra);
                    }
                    if ($request->has('input_image_extra')) {
                        $data['input_image_extra'] = implode('|', $request->input_image_extra);
                    }
                    $data['admin_id'] = \Auth::guard('admin')->user()->id;
//                    $data['company_id'] = \Auth::guard('admin')->user()->last_company_id;
                    #
//dd($data);
                    foreach ($data as $k => $v) {
                        $this->model->$k = $v;
                    }

                    if ($this->model->save()) {
                        $this->afterAddLog($request, $this->model);

                        CommonHelper::one_time_message('success', 'Tạo mới thành công!');
                    } else {
                        CommonHelper::one_time_message('error', 'Lỗi tao mới. Vui lòng load lại trang và thử lại!');
                    }

                    if ($request->ajax()) {
                        return response()->json([
                            'status' => true,
                            'msg' => '',
                            'data' => $this->model
                        ]);
                    }

                    if ($request->return_direct == 'save_continue') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id);
                    } elseif ($request->return_direct == 'save_create') {
                        return redirect('admin/' . $this->module['code'] . '/add');
                    } elseif ($request->return_direct == 'save_editor') {
                        return redirect('admin/' . $this->module['code'] . '/' . $this->model->id . '/editor');
                    }

                    return redirect('admin/' . $this->module['code']);
                }
            }
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', $ex->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $request)
    {
        $item = $this->model->find($request->id);

        //  Chỉ sửa được liệu công ty mình đang vào
//            if (strpos(\Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền!');
//                return back();
//            }

        if (!is_object($item)) abort(404);
        if (!$_POST) {
            $data = $this->getDataUpdate($request, $item);
            return view('kitcarebooking::product.edit')->with($data);
        } else if ($_POST) {
//            dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ], [
                'name.required' => 'Bắt buộc phải nhập tên',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $data = $this->processingValueInFields($request, $this->getAllFormFiled());

                //  Tùy chỉnh dữ liệu insert
                if ($request->has('multi_cat')) {
                    $data['multi_cat'] = '|' . implode('|', $request->multi_cat) . '|';
                    $data['category_id'] = $request->multi_cat[0];
                }
                if ($request->has('tags')) {
                    $data['tags'] = '|' . implode('|', $request->tags) . '|';
                }
                if ($request->has('image_extra')) {
                    $data['image_extra'] = implode('|', $request->image_extra);
                }
                if ($request->has('input_image_extra')) {
                    $data['input_image_extra'] = implode('|', $request->input_image_extra);
                }
                #

                foreach ($data as $k => $v) {
                    $item->$k = $v;
                }
                if ($item->save()) {
                    CommonHelper::one_time_message('success', 'Cập nhật thành công!');
                } else {
                    CommonHelper::one_time_message('error', 'Lỗi cập nhật. Vui lòng load lại trang và thử lại!');
                }
                if ($request->ajax()) {
                    return response()->json([
                        'status' => true,
                        'msg' => '',
                        'data' => $item
                    ]);
                }

                if ($request->return_direct == 'save_continue') {
                    return redirect('admin/' . $this->module['code'] . '/' . $item->id);
                } elseif ($request->return_direct == 'save_create') {
                    return redirect('admin/' . $this->module['code'] . '/add');
                }

                return redirect('admin/' . $this->module['code']);
            }
        }
    }

    public function getPublish(Request $request)
    {
        try {


            $id = $request->get('id', 0);
            $item = $this->model->find($id);

            // Không được sửa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                return response()->json([
//                    'status' => false,
//                    'msg' => 'Bạn không có quyền xuất bản!'
//                ]);
//            }

            if (!is_object($item))
                return response()->json([
                    'status' => false,
                    'msg' => 'Không tìm thấy bản ghi'
                ]);

            if ($item->{$request->column} == 0)
                $item->{$request->column} = 1;
            else
                $item->{$request->column} = 0;

            $item->save();

            return response()->json([
                'status' => true,
                'published' => $item->{$request->column} == 1 ? true : false
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'published' => null,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.'
            ]);
        }
    }

    public function delete(Request $request)
    {
        try {
            $item = $this->model->find($request->id);

            //  Không được xóa dữ liệu của cty khác, cty mình ko tham gia
//            if (strpos(Auth::guard('admin')->user()->company_ids, '|' . $item->company_id . '|') === false) {
//                CommonHelper::one_time_message('error', 'Bạn không có quyền xóa!');
//                return back();
//            }

            $item->delete();

            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return redirect('admin/' . $this->module['code']);
        } catch (\Exception $ex) {
            CommonHelper::one_time_message('error', 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên.');
            return back();
        }
    }

    public function multiDelete(Request $request)
    {
        try {
            $ids = $request->ids;
            if (is_array($ids)) {
                $this->model->whereIn('id', $ids)->delete();
            }
            CommonHelper::one_time_message('success', 'Xóa thành công!');
            return response()->json([
                'status' => true,
                'msg' => ''
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => false,
                'msg' => 'Lỗi hệ thống! Vui lòng liên hệ kỹ thuật viên'
            ]);
        }
    }


}
