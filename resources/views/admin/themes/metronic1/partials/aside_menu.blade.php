<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    {{--{{dd(in_array('setting', $permissions))}}--}}
    <div id="kt_aside_menu" class="kt-aside-menu kt-scroll ps ps--active-y" data-ktmenu-vertical="1"
         data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500" style="height: 191px; overflow: hidden;">
        <ul class="kt-menu__nav ">
            {!! Eventy::filter('aside_menu.dashboard_after', '') !!}

            @if(in_array('user_view', $permissions))
                {!! Eventy::filter('aside_menu.user', '<li class="kt-menu__item " aria-haspopup="true"><a
                            href="/admin/user" class="kt-menu__link "><span
                                class="kt-menu__link-icon"><i
                                class="kt-menu__link-icon flaticon2-avatar"></i></span><span class="kt-menu__link-text">'.trans('admin.customer').'</span></a></li>') !!}
            @endif

            @if(in_array('setting', $permissions))

            <li class="kt-menu__section ">
                <h4 class="kt-menu__section-text">{{trans('admin.settings')}}</h4>
                <i class="kt-menu__section-icon flaticon-more-v2"></i>
            </li>
            @endif

            @if(in_array('admin_view', $permissions))
                {!! Eventy::filter('aside_menu.admin', '<li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                            href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                            <i
                                    class="kt-menu__link-icon flaticon-profile-1"></i>
                        </span><span class="kt-menu__link-text">'.trans('admin.admin').'</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                                class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a
                                        href="/admin/admin" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">'.trans('admin.all_admin').'</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a
                                        href="/admin/admin/add" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">'.trans('admin.admin_add_new').'</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a
                                        href="/admin/profile" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">'.trans('admin.your_profile').'</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true"><a
                                        href="/admin/role" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">'.trans('admin.role').'</span></a></li>
                        </ul>
                    </div>
                </li>') !!}
            @endif

            {{--@if(in_array('plugin', $permissions))
                {!! Eventy::filter('aside_menu.plugin', '<li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                                        href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                                        <i
                                                class="kt-menu__link-icon flaticon-download-1"></i>
                                    </span><span class="kt-menu__link-text">'.trans('admin.plugin').'</span><i
                                            class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                                            class="kt-menu__arrow"></span>
                                    <ul class="kt-menu__subnav">
                                        <li class="kt-menu__item " aria-haspopup="true"><a
                                                    href="/admin/plugin?status=1" class="kt-menu__link "><i
                                                        class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                        class="kt-menu__link-text">'.trans('admin.installed').'</span></a></li>
                                        <li class="kt-menu__item " aria-haspopup="true"><a
                                                    href="#" class="kt-menu__link "><i
                                                        class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                                        class="kt-menu__link-text">'.trans('admin.new_install').'</span></a></li>
                                    </ul>
                                </div>
                            </li>') !!}
            @endif--}}

            @if(in_array('setting', $permissions))
                {!! Eventy::filter('aside_menu.setting', '<li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                            href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                            <i
                                    class="kt-menu__link-icon flaticon2-gear"></i>
                        </span><span class="kt-menu__link-text">'.trans('admin.setting').'</span><i
                                class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                                class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true"><a
                                        href="/admin/setting" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">'.trans('admin.general_setting').'</span></a></li>
                        </ul>
                    </div>
                </li>') !!}

                <li class="kt-menu__item kt-menu__item--submenu" aria-haspopup="true"
                    data-ktmenu-submenu-toggle="hover"><a
                            href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-icon">
                            <i
                                    class="kt-menu__link-icon flaticon2-user-1"></i>
                        </span><span class="kt-menu__link-text">{{trans('admin.system_management')}}</span><i
                                class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    <div class="kt-menu__submenu " kt-hidden-height="80" style="display: none; overflow: hidden;"><span
                                class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item " aria-haspopup="true" title="Quản lý bộ nhớ đệm"><a
                                        href="/admin/cache" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">{{trans('admin.cache')}}</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true" title="Xem lịch sử ghi nhận lỗi của hệ thống"><a
                                        href="/admin/error" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">{{trans('admin.history_error')}}</span></a></li>
                            {{--<li class="kt-menu__item " aria-haspopup="true"><a
                                        href="/admin/queue" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">{{trans('admin.the_process_run_hidden')}}</span></a></li>--}}
                            <li class="kt-menu__item " aria-haspopup="true" title="Sao lưu dữ liệu hệ thống"><a
                                        href="/admin/backup" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">{{trans('admin.data_backup')}}</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true" title="Nhập dữ liệu vào hệ thống nhanh bằng file excel"><a
                                        href="/admin/import" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Import</span></a></li>
                            <li class="kt-menu__item " aria-haspopup="true" title="Lịch sử hoạt động của các tài khoản"><a
                                        href="/admin/admin_logs" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">{{trans('admin.history_admin')}}</span></a></li>
                        </ul>
                    </div>
                </li>
            @endif

            <li class="kt-menu__item " aria-haspopup="true"><a
                        href="/admin/logout" class="kt-menu__link "><span
                            class="kt-menu__link-icon"><i
                                class="kt-menu__link-icon flaticon2-logout"></i></span><span class="kt-menu__link-text">{{ trans('admin.logout') }}</span></a></li>
        </ul>
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; height: 191px; right: 3px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 40px;"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        <?php $uri = "$_SERVER[REQUEST_URI]";?>
        $("#kt_aside_menu a[href='{{ $uri }}']").parents('li').addClass('kt-menu__item--active');
        $("#kt_aside_menu a[href='{{ $uri }}']").parents('li').parents('li').addClass('kt-menu__item--here kt-menu__item--open').removeClass('kt-menu__item--active');
        $("#kt_aside_menu a[href='{{ $uri }}']").parents('li').parents('li').find('.kt-menu__submenu').attr('style', '');
    });
</script>