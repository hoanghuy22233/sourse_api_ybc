<style>
    .item-header {
        position: relative;
    }
    .item-header:hover .item-header-popup {
        display: block;
    }
</style>

{!! Eventy::filter('block.header_topbar', '') !!}

<div class="topbar-item">
    <div class="btn btn-icon btn-clean btn-lg mr-1" id="kt_quick_panel_toggle" style="    width: unset;">
        <a href="/" target="_blank" title="Đi đến trang chủ của website">
            <span class="svg-icon svg-icon-xl svg-icon-primary">{{ trans('admin.go_to_home_page') }}</span></a></div>
</div>


<div class="interface-1 kt-header__topbar-item kt-header__topbar-item--langs item-header">
    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px" aria-expanded="true">
        <span class="kt-header__topbar-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1"
                 class="kt-svg-icon">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"></rect>
        <path d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z"
              fill="#000000" opacity="0.3"></path>
        <path d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z"
              fill="#000000"></path>
    </g>
</svg>
        </span>
    </div>
    <div class="interface dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround item-header-popup"
         style="position: absolute; will-change: transform; top: 53px; right: 0;
    left: unset;"
         x-placement="bottom-end">
        <div class="kt-quick-panel__nav" kt-hidden-height="66" style="">
            <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand  kt-notification-item-padding-x"
                role="tablist" style="padding: 10px; width: 100%;text-align: center;font-weight: bold;margin: 0px">
                <li class="nav-item active">
                    {{trans('admin.change_interface')}}
                </li>
            </ul>
        </div>
        <ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
            <?php
            $admin_theme_style = Cookie::get('admin_theme_style', 'dark');
            ?>
            <li class="kt-nav__item {{ $admin_theme_style == 'dark' ? 'kt-nav__item--active' : '' }}">
                <a href="/admin/theme/change?style=dark" class="kt-nav__link">
                    <span class="kt-nav__link-text">{{trans('admin.dark')}}</span>
                </a>
            </li>
            <li class="kt-nav__item {{ $admin_theme_style == 'light' ? 'kt-nav__item--active' : '' }}">
                <a href="/admin/theme/change?style=light" class="kt-nav__link">
                    <span class="kt-nav__link-text">{{trans('admin.light')}}</span>
                </a>
            </li>
        </ul>
    </div>
</div>


<div class="kt-header__topbar-item kt-header__topbar-item--user item-header">
    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
        <div class="kt-header__topbar-user">

            @if(@\Auth::guard('admin')->user()->image != null)
                <img alt="{{ @\Auth::guard('admin')->user()->name }}" class="lazy"
                     data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@\Auth::guard('admin')->user()->image,100,100) }}"/>
            @else
            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                <span class=" kt-header__topbar-welcome kt-hidden-mobile">{{trans('admin.hello')}}</span>
                <span class=" kt-header__topbar-username kt-hidden-mobile"
                      style="color: #636177">{{ @\Auth::guard('admin')->user()->name }}</span>
            @endif
        </div>
    </div>
    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl item-header-popup"
    style="position: absolute;     top: 53px; right: 0;
    left: unset; will-change: transform;"
    >
        <!--begin: Head -->
        <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
             style="background-image: url({{ asset('/public/backend/themes/metronic1/media/misc/bg-1.jpg') }})">
            <div class="kt-user-card__avatar">
                @if(@\Auth::guard('admin')->user()->image != null)
                    <img alt="Pic" class="lazy"
                         data-src="{{ \App\Http\Helpers\CommonHelper::getUrlImageThumb(@\Auth::guard('admin')->user()->image,100,100) }}"/>
                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                @else
                    <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{ mb_substr(@\Auth::guard('admin')->user()->name, 0, 1) }}</span>
                @endif
            </div>
            <div class="kt-user-card__name">
                {{ @\Auth::guard('admin')->user()->name }}
            </div>
        </div>
        <!--end: Head -->

        <!--begin: Navigation -->
        <div class="kt-notification">
            <a href="/admin/profile"
               class="kt-notification__item" style="margin: 10px">
                <div class="kt-notification__item-icon">
                    <i class="flaticon2-calendar-3 kt-font-success"></i>
                </div>
                <div class="kt-notification__item-details">
                    <div class="kt-notification__item-title kt-font-bold">
                        {{trans('admin.my_info')}}
                    </div>
                    <div class="kt-notification__item-time">
                        {{trans('admin.setting_account')}}
                    </div>
                </div>
            </a>

            {!! Eventy::filter('user_bar.profile_after', '') !!}

            <?php
            $btn_setting = in_array('setting', $permissions) ? '<a href="/admin/setting"
                   class="btn btn-clean btn-sm btn-bold">'.trans('admin.settings').'</a>' : '';
            ?>
            {!! Eventy::filter('user_bar.footer', '<div class="kt-notification__custom kt-space-between">
                <a href="/admin/logout"
                   class="btn btn-label btn-label-brand btn-sm btn-bold">'.trans('admin.logout').'</a>

                '.$btn_setting.'
            </div>') !!}
        </div>
        <!--end: Navigation -->
    </div>
</div>
