<?php
$post = \App\Models\HelpArticle::where('number', $_GET['code'])->first();
?>
<style>
    .box {
        display: inline-block;
        margin-top: 20px;
        border-bottom: 1px solid #ccc;
        width: 100%;
    }
    .box > label {
        font-weight: bold;
        margin-top: 20px;
        width: 100%;
        display: inline-block;
        font-size: 18px;
    }

</style>
<div class="box">
    <label>
        Tiêu đề
    </label>
    <div class="content">
        {!! $post->title !!}
    </div>y

</div>
<div class="box">
    <label>
        Mã
    </label>
    <div class="content">
        {!! $post->number !!}
    </div>
</div>
<div class="box">
    <label>
        Cơ quan ban hành
    </label>
    <div class="content">
        {!! @$post->organization->name !!}
    </div>
</div>
<div class="box">
    <label>
        Kết quả thực hiện
    </label>
    <div class="content">
        {!! $post->result !!}
    </div>
</div>
<div class="box">
    <label>
        Thành phần hồ sơ
    </label>
    <div class="content">
        {!! $post->profile_composition !!}
    </div>
</div>
<div class="box">
    <label>
        Trình tự thực hiện
    </label>
    <div class="content">
        {!! $post->the_order_execution !!}
    </div>
</div>
<div class="box">
    <label>
        Yêu cầu, đk thực hiện
    </label>
    <div class="content">
        {!! $post->conditions_required !!}
    </div>
</div>
<div class="box">
    <label>
        Cách thức thực hiện
    </label>
    <div class="content">
        {!! $post->settlement_term !!}
    </div>
</div>
<div class="box">
    <label>
        Lệ phí
    </label>
    <div class="content">
        {!! $post->price !!}
    </div>
</div>
<div class="box">
    <label>
        Tiêu đề
    </label>
    <div class="content">
        {!! $post->title !!}
    </div>
</div>
<div class="box">
    <label>
        Địa chỉ tiếp nhận HS
    </label>
    <div class="content">
        {!! $post->destination_address !!}
    </div>
</div>
<div class="box">
    <label>
        Liên hệ
    </label>
    <div class="content">
        {!! $post->contact !!}
    </div>
</div>
<div class="box">
    <label>
        Nội dung
    </label>
    <div class="content">
        {!! $post->content !!}
    </div>
</div>
<div class="box">
    <label>
        địa chỉ
    </label>
    <div class="content">
        {!! $post->address !!}
    </div>
</div>
<div class="box">
    <label>
        Cơ quan ban hành
    </label>
    <div class="content">
        {!! @$post->published_agency->name !!}
    </div>
</div>
<div class="box">
    <label>
        Tiêu đề
    </label>
    <div class="content">
        {!! $post->title !!}
    </div>
</div>
<div class="box">
    <label>
        Link nguồn
    </label>
    <div class="content">
        {!! $post->crawl_link !!}
    </div>
</div>
<div class="box">
    <label>
        Lĩnh vực thực hiện
    </label>
    <div class="content">
        {!! @$post->area->name !!}
    </div>
</div>
<div class="box">
    <label>
        Đối tượng thực hiện
    </label>
    <div class="content">
        {!! $post->object_implementation !!}
    </div>
</div>
<div class="box">
    <label>
        Căn cứ pháp lý
    </label>
    <div class="content">
        {!! $post->legal_grounds !!}
    </div>
</div>
<div class="box">
    <label>
        File hồ sơ
    </label>
    <div class="content">
        {!! $post->profile_composition_files !!}
    </div>
</div>
<div class="box">
    <label>
        Bộ / Tình
    </label>
    <div class="content">
        <?php
        $organ = @\App\Models\Organ::where('crawl_id', $post->impl_agency_id)->first()->name;
        ?>
        {!! $organ !!}
    </div>
</div>
<div class="box">
    <label>
        Số quyết định
    </label>
    <div class="content">
        {!! $post->so_quyet_dinh !!}
    </div>
</div>
<div class="box">
    <label>
        Cấp thực hiện
    </label>
    <div class="content">
        {!! $post->cap_thuc_hien !!}
    </div>
</div>
<div class="box">
    <label>
        Loại thủ tục
    </label>
    <div class="content">
        {!! $post->loai_thu_tuc !!}
    </div>
</div>
<div class="box">
    <label>
        Cơ quan thực hiện
    </label>
    <div class="content">
        {!! $post->co_quan_thuc_hien !!}
    </div>
</div>
<div class="box">
    <label>
        Cơ quan được ủy quyền
    </label>
    <div class="content">
        {!! $post->co_quan_duoc_uy_quyen !!}
    </div>
</div>
<div class="box">
    <label>
        Cơ quan có thẩm quyền
    </label>
    <div class="content">
        {!! $post->co_quan_co_tham_quyen !!}
    </div>
</div>
<div class="box">
    <label>
        Cơ quan phối hợp
    </label>
    <div class="content">
        {!! $post->co_quan_phoi_hop !!}
    </div>
</div>
<div class="box">
    <label>
        Từ khóa
    </label>
    <div class="content">
        {!! $post->tu_khoa !!}
    </div>
</div>
<div class="box">
    <label>
        Mô tả
    </label>
    <div class="content">
        {!! $post->mo_ta !!}
    </div>
</div>
