<?php
return [
    'transports' => [
        'status' => [
            'chuathanhtoan' => 'Chưa thanh toán',
            'cholayhang' => 'Chờ lấy hàng',
            'dalayhang' => 'Đã lấy hàng',
            'dagiaohang' => 'Đã giao hàng',
            'nocode' => 'Nợ COD',
            'datracode' => 'Đã trả COD',
            'khongphatduoc' => 'Không phát được',
            'dahuy' => 'Đã hủy',
        ]
    ]
];


