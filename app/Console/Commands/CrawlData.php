<?php

namespace App\Console\Commands;

use App\Console\Commands\Website\CrawlWebsiteBase;
use App\Models\Area;
use App\Models\CrawlError;
use App\Models\HelpArticle;
use App\Models\Organ;
use App\Models\OrganItem;
use App\Models\Organization;
use App\Models\Product;
use App\Models\ProfileCompositionFiles;
use App\Models\Website;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use function GuzzleHttp\Psr7\str;

class CrawlData extends Command
{

    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl du lieu products';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        require_once base_path('app/Console/Commands/simple_html_dom.php');
        ini_set("user_agent", "Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0");

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $this->convertToProfileCompositionFiles();dd('xong!');
        $curl = curl_init();
//        $this->getBo($curl); /*dd('xong!');*/
//        $this->getTinh($curl); dd('xong!');

//
//        $url = 'https://dichvucong.gov.vn/p/home/dvc-tthc-thu-tuc-hanh-chinh-chi-tiet.html?ma_thu_tuc=2';
//        $html = file_get_html($url);
//
//        dd($html);

        $this->updateOrganItems($curl);
//        $this->xuLyDuLieu($curl, 'crawl_help_articles');
//        $this->xuLyDuLieu($curl, 'update_help_articles');


        print "Hoàn Tất!\n";

        curl_close($curl);
    }

    public function updateOrganItems($curl) {
        $organ_items = OrganItem::all();
        foreach ($organ_items as $item) {
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://dichvucong.gov.vn/jsp/rest.jsp",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => 'params={"service":"thong_tin_co_quan_v2","provider":"dvcquocgia","type":"ref","pId":"'.$item->crawl_id.'"}',
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded",
                    "postman-token: a92d7e0c-2f04-b93e-277a-9cd9cdb3da91"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            if ($err) {
                print "Không lấy được dữ liệu. Lỗi : " . $err . "\n";

            } else {
                $data = (array)json_decode($response);
                $data = $data[0];
                $item->address = @$data->ADDRESS;
                $item->diemthuchien = @$data->DIEMTHUCHIEN;
                $item->location = @$data->LOCATION;
                $item->tinh = @$data->TINH;
                $item->huyen = @$data->HUYEN;
                $item->xa = @$data->XA;
                $item->save();

                print "Cập nhật thành công " . $item->id . "\n";
            }
        }
    }

    public function xuLyDuLieu($curl, $action, $impl_agency_id = -1, $agency_type = 0) {
        $i = 1;
        $per_page = 1000;
        $stop = false;
        while (!$stop) {
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://dichvucong.gov.vn/jsp/rest.jsp",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => 'params={"service":"procedure_advanced_search_service_v2","provider":"dvcquocgia","type":"ref","recordPerPage":' . $per_page . ',"pageIndex":' . $i . ',"is_connected":0,"keyword":"","agency_type":"'.$agency_type.'","impl_agency_id":"' . $impl_agency_id . '","object_id":"-1","field_id":"-1","impl_level_id":"-1"}',
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded",
                    "postman-token: a92d7e0c-2f04-b93e-277a-9cd9cdb3da91"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            if ($err) {
                print "Không lấy được dữ liệu. Lỗi : " . $err . "\n";

            } else {
                $data = (array)json_decode($response);
                if (count($data) == 0) {
                    $stop = true;
                } else {
                    if ($action == 'crawl_help_articles') {
                        $this->crawlHelpArticles($curl, $data);
                    } elseif ($action == 'update_help_articles') {
                        $this->updateHelpArticles($curl, $data, $impl_agency_id);
                    } elseif ($action == 'update_bo') {
                        $this->updateBo($curl, $data, $impl_agency_id);
                    }
                }
                $i++;
            }
        }
        return true;
    }

    public function crawlHelpArticles($curl, $data) {
        foreach ($data as $item) {

            $help_articles = HelpArticle::where('crawl_id', $item->ID)->first();
            if (is_object($help_articles)) {
                $this->updateItem($help_articles);
            } else {
                $url = 'https://dichvucong.gov.vn/p/home/dvc-tthc-thu-tuc-hanh-chinh-chi-tiet.html?ma_thu_tuc=' . $item->ID;

                //  Cơ quan ban hành
                $organization_published = Organization::updateOrCreate([
                    'name' => $item->PUBLISHED_AGENCY
                ], [

                ]);

                //  cơ quan thực hiện
                $organization = Organization::updateOrCreate([
                    'name' => $item->IMPLEMENTATION_AGENCY
                ], [

                ]);

                //  Lĩnh vực thực hiện
                $area = Area::updateOrCreate([
                    'name' => $item->FIELD_NAME
                ], [

                ]);

                //  Dữ liệu insert db
                $data = [
                    'crawl_id' => $item->ID,
                    'crawl_link' => $url,
                    'number' => $item->PROCEDURE_CODE,  //  Mã thủ tục
                    'title' => $item->PROCEDURE_NAME,  //  Tên thủ tục:
                    'published_agency_id' => $organization_published->id,  //  Cơ quan ban hành
                    'organization_id' => $organization->id,  //  Cơ quan thực hiện
                    'area_id' => $area->id,  //  Lĩnh vực thực hiện
                ];

                $html = file_get_html($url);
                foreach ($html->find('#popupChitietTTHC .info-row') as $row) {
                    $data = $this->getDataFromRow($data, $row);
                }

                $data = $this->getDataFromAjax($curl, $item->ID, $data);

                $help_articles = new HelpArticle();
                foreach ($data as $k => $v) {
                    $help_articles->{$k} = $v;
                }

                $help_articles->save();

                print "Lấy dữ liệu thành công : " . $item->ID . "\n";
            }

        }
        return true;
    }

    public function updateHelpArticles($curl, $data) {
        $arr_ids = [];
        foreach ($data as $item) {
            $arr_ids[] = $item->ID;
        }
        $help_articles = HelpArticle::whereIn('crawl_id', $arr_ids)->whereNull('version')->get();
        foreach ($help_articles as $help_article) {
            $url = 'https://dichvucong.gov.vn/p/home/dvc-tthc-thu-tuc-hanh-chinh-chi-tiet.html?ma_thu_tuc=' . $help_article->crawl_id;

            //  Dữ liệu update db
            $data = [
            ];

            $html = file_get_html($url);
            foreach ($html->find('#popupChitietTTHC .info-row') as $row) {
                $data = $this->getDataFromRowNew($data, $row);
            }

            foreach ($data as $k => $v) {
                $help_article->{$k} = $v;
            }

            $help_article->version = 1;
            $help_article->save();

            print "Cập nhật thành công : " . $help_article->crawl_id . "\n";
        }
        return true;
    }

    public function updateBo($curl, $data, $impl_agency_id) {
        $help_articles_id_update = [];
        foreach ($data as $v) {
            $help_articles_id_update[] = $v->ID;
        }
        HelpArticle::whereNull('impl_agency_id')->whereIn('crawl_id', $help_articles_id_update)->update([
            'impl_agency_id' => $impl_agency_id
        ]);
        print "Cập nhật: Gán ".count($help_articles_id_update)." bản ghi thành công cho bộ : " . $impl_agency_id . "\n";
        return true;
    }

    public function getBo($curl) {

        //  Lấy thông tin bộ & cơ quan ngang bộ
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dichvucong.gov.vn/jsp/rest.jsp",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => 'params=%7B%22service%22%3A%22get_cat_agency_tinh_bo_v2%22%2C%22provider%22%3A%22dvcquocgia%22%2C%22type%22%3A%22ref%22%2C%22pTypeCode%22%3A%22000.00.00.G%25%22%7D',
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "postman-token: a92d7e0c-2f04-b93e-277a-9cd9cdb3da91"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        if ($err) {
            print "Không lấy được dữ liệu. Lỗi : " . $err . "\n";

        } else {
            $data = (array)json_decode($response);
            foreach ($data as $item) {
                $oran = Organ::where('crawl_id', $item->ID)->first();

                if (!is_object($oran)) {
                    $oran_info = $this->getCoQuanInfo($curl, $item->ID);

                    $oran = new Organ();
                    $oran->crawl_id = $item->ID;
                    $oran->name = $item->NAME;
                    $oran->agency_amount = $item->AGENCY_AMOUNT;

                    $oran->address = @$oran_info->ADDRESS;
                    $oran->email = @$oran_info->EMAIL;
                    $oran->phone_number = @$oran_info->PHONE_NUMBER;
                    $oran->location = @$oran_info->LOCATION;
                    $oran->tinh = @$oran_info->TINH;
                    $oran->huyen = @$oran_info->HUYEN;
                    $oran->xa = @$oran_info->XA;
                    $oran->type = 'bo';
                    $oran->save();

                    print "Đã lấy dữ liệu cơ quan : " . $oran->crawl_id . ": " . $oran->name . "\n";

                    //  Cập nhật bộ cho các thủ tục hành chính
                    $this->xuLyDuLieu($curl, 'update_bo', $oran->crawl_id);
                } else {
                    $this->getCoCauToChuc($curl, $item->ID, $oran->id);
                }
            }
        }
    }

    public function getTinh($curl) {

        //  Lấy thông tin tinh, thh pho
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dichvucong.gov.vn/jsp/rest.jsp",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => 'params=%7B%22service%22%3A%22get_cat_agency_tinh_bo_v2%22%2C%22provider%22%3A%22dvcquocgia%22%2C%22type%22%3A%22ref%22%2C%22pTypeCode%22%3A%22000.00.00.H%25%22%7D',
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "postman-token: a92d7e0c-2f04-b93e-277a-9cd9cdb3da91"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        if ($err) {
            print "Không lấy được dữ liệu. Lỗi : " . $err . "\n";

        } else {
            $data = (array)json_decode($response);
            foreach ($data as $item) {
                $oran = Organ::where('crawl_id', $item->ID)->first();
                if (!is_object($oran)) {
                    $oran_info = $this->getCoQuanInfo($curl, $item->ID);

                    $oran = new Organ();
                    $oran->crawl_id = $item->ID;
                    $oran->name = $item->NAME;
                    $oran->agency_amount = $item->AGENCY_AMOUNT;

                    $oran->address = @$oran_info->ADDRESS;
                    $oran->email = @$oran_info->EMAIL;
                    $oran->phone_number = @$oran_info->PHONE_NUMBER;
                    $oran->location = @$oran_info->LOCATION;
                    $oran->tinh = @$oran_info->TINH;
                    $oran->huyen = @$oran_info->HUYEN;
                    $oran->xa = @$oran_info->XA;
                    $oran->type = 'tinh';
                    $oran->save();

                    print "Đã lấy dữ liệu cơ quan : " . $oran->crawl_id . ": " . $oran->name . "\n";

                    if ($oran->version != 2) {
                        //  Cập nhật bộ cho các thủ tục hành chính
                        $this->xuLyDuLieu($curl, 'update_bo', $oran->crawl_id, 1);
                        $oran->version = 2;
                        $oran->save();
                    }
                } else {
                    $this->getCoCauToChucTinh($curl, $item->ID, $oran->id);
                }
            }
        }
    }

    public function getCoCauToChuc($curl, $id, $parent_id) {
        //  Lấy thông tin bộ & cơ quan ngang bộ
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dichvucong.gov.vn/jsp/rest.jsp",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => 'params={"service":"get_co_cau_to_chuc_v2","provider":"dvcquocgia","type":"ref","pId":"'.$id.'","pParam1":5,"pParam2":6,"pParam3":7,"pParam4":8,"pPage_index":1,"pRecordPerPage":1000}',
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "postman-token: a92d7e0c-2f04-b93e-277a-9cd9cdb3da91"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        if ($err) {
            print "Không lấy được dữ liệu. Lỗi : " . $err . "\n";
            return false;
        } else {
            $data = (array)json_decode($response);
            foreach ($data as $v) {
                $organ_item = new OrganItem();
                $organ_item->name = $v->AGENCY_NAME;
                $organ_item->crawl_id = $v->ID;
                $organ_item->row_stt = $v->ROW_STT;
                $organ_item->organ_id = $parent_id;
                $organ_item->save();
                print "Đã tạo : " . $organ_item->id . "\n";
            }
            return  true;
        }
    }

    public function getCoCauToChucTinh($curl, $id, $parent_id) {
        //  Lấy thông tin bộ & cơ quan ngang bộ
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dichvucong.gov.vn/jsp/rest.jsp",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => 'params={"service":"get_co_cau_to_chuc_v2","provider":"dvcquocgia","type":"ref","pId":"'.$id.'","pParam1":1,"pParam2":2,"pParam3":3,"pParam4":4,"pPage_index":1,"pRecordPerPage":1000}',
            CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "postman-token: a92d7e0c-2f04-b93e-277a-9cd9cdb3da91"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        if ($err) {
            print "Không lấy được dữ liệu. Lỗi : " . $err . "\n";
            return false;
        } else {
            $data = (array)json_decode($response);
            foreach ($data as $v) {
                $organ_item = new OrganItem();
                $organ_item->name = $v->AGENCY_NAME;
                $organ_item->crawl_id = $v->ID;
                $organ_item->row_stt = $v->ROW_STT;
                $organ_item->organ_id = $parent_id;
                $organ_item->save();
                print "Đã tạo : " . $organ_item->id . "\n";
            }
            return  true;
        }
    }

    public function getCoQuanInfo($curl, $id) {
        //  Lấy thông tin bộ & cơ quan ngang bộ
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dichvucong.gov.vn/jsp/rest.jsp",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => 'params={"service":"thong_tin_co_quan_v2","provider":"dvcquocgia","type":"ref","pId":"'.$id.'"}',
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "postman-token: a92d7e0c-2f04-b93e-277a-9cd9cdb3da91"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        if ($err) {
            print "Không lấy được dữ liệu. Lỗi : " . $err . "\n";
            return (object)[];
        } else {
            $data = (array)json_decode($response);
            return $data[0];
        }
    }

    public function updateItem($help_articles)
    {
        print "Cập nhật bỏ qua : " . $help_articles->crawl_id . "\n";
        return true;
    }

    public function getDataFromRow($data, $row)
    {
        try {
            if ($row->find('.key', 0) != null && $row->find('.col-sm-9', 0) != null) {
                $key = trim($row->find('.key', 0)->innertext);
                $value = $row->find('.col-sm-9', 0)->innertext;
                $value = trim($value);
                if ($value != '') {
                    switch ($key) {
                        case 'Căn cứ ph&aacute;p l&yacute;:' :
                            $data['legal_grounds'] = $value;
                            break;
                        case 'C&aacute;ch thức thực hiện:' :
                            $data['settlement_term'] = $value;

                            if ($row->find('.col-sm-9 td', 2) != null) {
                                $data['price'] = $row->find('.col-sm-9 td', 2)->innertext;        //  Lệ phí
                            }
                            break;
                        case 'Đối tượng thực hiện:' :
                            $data['object_implementation'] = $value;
                            break;
                        case 'Kết quả thực hiện:' :
                            $data['result'] = $value;
                            break;
                        case 'Th&agrave;nh phần hồ sơ:' :
                            $data['profile_composition'] = $value;
                            $data['profile_composition_files'] = $this->downloadFile($value);
                            break;
                        case 'Tr&igrave;nh tự thực hiện:' :
//                    $data['the_order_execution'] = $value;

//                    $data['content'] = $value;  //  Nội dung
                            break;
                        case 'Y&ecirc;u cầu, điều kiện thực hiện:' :
//                    $data['conditions_required'] = $value;
//                    break;
                        case 'Địa chỉ tiếp nhận HS:' :
                            $data['destination_address'] = $value;

                            $data['contact'] = $value;      //  Liên hệ
                            $data['address'] = $value;      //  địa chỉ
                            break;
                        case 'Số quyết định:' :
                            $data['so_quyet_dinh'] = $value;
                            break;
                        case 'Cấp thực hiện:' :
                            $data['cap_thuc_hien'] = $value;
                            break;
                        case 'Loại thủ tục:' :
                            $data['loai_thu_tuc'] = $value;
                            break;
                        case 'Cơ quan thực hiện:' :
                            $data['co_quan_thuc_hien'] = $value;
                            break;
                        case 'Cơ quan c&oacute; thẩm quyền:' :
                            $data['co_quan_co_tham_quyen'] = $value;
                            break;
                        case 'Cơ quan được ủy quyền:' :
                            $data['co_quan_duoc_uy_quyen'] = $value;
                            break;
                        case 'Cơ quan phối hợp:' :
                            $data['co_quan_phoi_hop'] = $value;
                            break;
                        case 'Từ kh&oacute;a:' :
                            $data['tu_khoa'] = $value;
                            break;
                        case 'M&ocirc; tả:' :
                            $data['mo_ta'] = $value;
                            break;
                        default:
//                    $data[$key] = $value;
                            break;
                    }
                }
            }
        } catch (\Exception $ex) {
            CrawlError::create([
                'msg' => $ex->getMessage(),
                'line' => $ex->getLine(),
                'path' => $ex->getFile(),
                'item_id' => @$data['crawl_id'],
            ]);
        }
        return $data;
    }

    public function getDataFromRowNew($data, $row)
    {
        try {
            if ($row->find('.key', 0) != null && $row->find('.col-sm-9', 0) != null) {
                $key = trim($row->find('.key', 0)->innertext);
                $value = $row->find('.col-sm-9', 0)->innertext;
                $value = trim($value);
                if ($value != '') {
                    switch ($key) {
                        case 'Số quyết định:' :
                            $data['so_quyet_dinh'] = $value;
                            break;
                        case 'Cấp thực hiện:' :
                            $data['cap_thuc_hien'] = $value;
                            break;
                        case 'Loại thủ tục:' :
                            $data['loai_thu_tuc'] = $value;
                            break;
                        case 'Cơ quan thực hiện:' :
                            $data['co_quan_thuc_hien'] = $value;
                            break;
                        case 'Cơ quan c&oacute; thẩm quyền:' :
                            $data['co_quan_co_tham_quyen'] = $value;
                            break;
                        case 'Cơ quan được ủy quyền:' :
                            $data['co_quan_duoc_uy_quyen'] = $value;
                            break;
                        case 'Cơ quan phối hợp:' :
                            $data['co_quan_phoi_hop'] = $value;
                            break;
                        case 'Từ kh&oacute;a:' :
                            $data['tu_khoa'] = $value;
                            break;
                        case 'M&ocirc; tả:' :
                            $data['mo_ta'] = $value;
                            break;

                        default:
//                    $data[$key] = $value;
                            break;
                    }
                }
            }
        } catch (\Exception $ex) {
            CrawlError::create([
                'msg' => $ex->getMessage(),
                'line' => $ex->getLine(),
                'path' => $ex->getFile(),
                'item_id' => @$data['crawl_id'],
            ]);
        }
        return $data;
    }

    public function getDataFromAjax($curl, $id, $data)
    {
        try {
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://dichvucong.gov.vn/jsp/rest.jsp",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => 'params={"service":"procedure_get_impl_orders_by_proc_id_service_v2","provider":"dvcquocgia","type":"ref","id":"' . $id . '","parent_id":""}',
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded",
                    "postman-token: a92d7e0c-2f04-b93e-277a-9cd9cdb3da91"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            if ($err) {

            } else {
                $resp = (array)json_decode($response);

                $str = '';
                foreach ($resp as $rep) {
                    $str .= $rep->CONTENT;
                }

                $data['the_order_execution'] = $str;   //  Tr&igrave;nh tự thực hiện

                $data['content'] = $str;  //  Nội dung
            }


            //  'Y&ecirc;u cầu, điều kiện thực hiện:'
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://dichvucong.gov.vn/jsp/rest.jsp",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => 'params={"service":"procedure_get_requires_by_procedure_id_service_v2","provider":"dvcquocgia","type":"ref","id":"' . $id . '","parent_id":""}',
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "content-type: application/x-www-form-urlencoded",
                    "postman-token: a92d7e0c-2f04-b93e-277a-9cd9cdb3da91"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            if ($err) {

            } else {
                $resp = (array)json_decode($response);

                $data['conditions_required'] = @$resp[0]->REQUIRE_NAME;   // 'Y&ecirc;u cầu, điều kiện thực hiện:'
            }
        } catch (\Exception $ex) {
            CrawlError::create([
                'msg' => $ex->getMessage(),
                'line' => $ex->getLine(),
                'path' => $ex->getFile(),
                'item_id' => @$data['crawl_id'],
            ]);
        }
        return $data;
    }

    public function downloadFile($html)
    {
        if (strpos($html, 'downloadMaudon(') !== false) {
            $files = [];
            try {
                foreach (explode("downloadMaudon('", $html) as $k => $v) {
                    if ($k != 0) {
                        $id = @explode("')", $v)[0];

                        $file_name = @explode("')", $v)[1];
                        $file_name = @explode('">', $v)[1];
                        $file_name = @explode('</', $v)[0];
                        $x = explode('.', $file_name);
                        $duoi = end($x);
                        $file_name = $id . '_' . str_slug($file_name, '-', $file_name) . '.' . $duoi;


                        $arrContextOptions = array(
                            "ssl" => array(
                                "verify_peer" => false,
                                "verify_peer_name" => false,
                            ),
                        );
                        $link = 'https://csdl.dichvucong.gov.vn/web/jsp/download_file.jsp?ma=' . $id;
                        $v = file_get_contents($link, false, stream_context_create($arrContextOptions));

                        $path = 'files_ho_so/' . date('Y/m/d/H/i');
                        $base_path = public_path() . '/filemanager/userfiles/';
                        $dir_name = $base_path . $path;
                        if (!is_dir($dir_name)) {
                            // Tạo thư mục của chúng tôi nếu nó không tồn tại
                            mkdir($dir_name, 0755, true);
                        }

                        file_put_contents(base_path() . '/public/filemanager/userfiles/' . $path . '/' . $file_name, $v);

                        $files[] = $path . '/' . $file_name;
                    }
                }

            } catch (\Exception $ex) {
                CrawlError::create([
                    'msg' => $ex->getMessage(),
                    'line' => $ex->getLine(),
                    'path' => $ex->getFile(),
//                    'item_id' => @$data['crawl_id'],
                ]);
            }
            return implode('|', $files);
        }
    }

    public function convertToProfileCompositionFiles() {
        $articles = HelpArticle::where('profile_composition', 'like', '%downloadMaudon(%')->get();
        foreach ($articles as $i => $article) {
            $files = explode('|', $article->profile_composition_files);
            foreach ($files as $file) {
                print "update xong cho  : #" .$i . ": id:" . $article->id . "\n";
                try {
                    $code = explode('/', $file)[count(explode('/', $file)) - 1];
                    $code = explode('_', $code)[0];

                    if (ProfileCompositionFiles::where('code', $code)->where('help_article_id', $article->id)->count() == 0) {
                        $profileCompositionFiles = new ProfileCompositionFiles();
                        $profileCompositionFiles->help_article_id = $article->id;

                        $profileCompositionFiles->file_name = explode($code. "');", $article->profile_composition)[1];
                        $profileCompositionFiles->file_name = explode("</span>", $profileCompositionFiles->file_name)[0];
                        $profileCompositionFiles->file_name = explode('">', $profileCompositionFiles->file_name)[1];


                        $profileCompositionFiles->file_path = $file;
                        $profileCompositionFiles->code = $code;
                        $profileCompositionFiles->save();
                    }
                } catch (\Exception $ex) {
                    print "Lỗi  : #" . $ex->getMessage() . "\n";
                }
            }
        }
        return true;
    }

}
