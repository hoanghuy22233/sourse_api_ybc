<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\CommonHelper;
use App\Models\District;
use App\Models\Ward;
use Auth;
use DB;
use Illuminate\Http\Request;
use Mail;

class DashboardController extends Controller
{
    public function getIndex()
    {
        $data['module'] = [
            'code' => 'dashboard',
        ];
        $data['page_title'] = 'Trang thống kê theo công ty';
        $data['page_type'] = 'list';
        return view(config('core.admin_theme') . '.dashboard', $data);
    }

    public function changeTheme(Request $request)
    {
        \Cookie::queue('admin_theme_style', $request->style, 129600);
        CommonHelper::one_time_message('success', 'Đã đổi giao diện!');
        return back();
    }

    public function tooltipInfo(Request $request)
    {
        $modal = new $request->modal;
        $data['item'] = $modal->find($request->id);
        $data['tooltip_info'] = $request->tooltip_info;

        return view(config('core.admin_theme') . '.partials.modal.tooltip_info', $data);
    }

    public function ajax_up_file(Request $request)
    {
        if ($request->has('file')) {
            $fileRequest = $request->file;
            if (in_array($fileRequest->getClientOriginalExtension(), ['jpg', 'png', 'gif', 'jpeg'])) {
                $path = 'upload/' . date('Y/m/d');
                $base_path = public_path() . '/filemanager/userfiles/';
                $dir_name = $base_path . $path;
                if (!is_dir($dir_name)) {
                    // Tạo thư mục của chúng tôi nếu nó không tồn tại
                    mkdir($dir_name, 0777, true);
                }
                if (is_dir($dir_name)) {
                    $file = CommonHelper::saveFile($request->file('file'), $path);
                    return response()->json([
                        'status' => true,
                        'file' => '/public/filemanager/userfiles/' . $file,
                        'value' => $file,
                    ]);
                }
            } else {
                return response()->json([
                    'status' => false,
                    'msg' => 'Sai định dạng ảnh, vui lòng chọn lại',
                ]);
            }
        } else {
            return response()->json([
                'status' => false,
                'msg' => 'Không tồn tại file. xin vui  lòng thử lại',
            ]);
        }
    }

    public function ajax_up_file2(Request $request)
    {
        if ($request->has('file')) {
            if (is_array($request->file)) {
                foreach ($request->file as $key => $fileRequest) {
                    if (in_array($fileRequest->getClientOriginalExtension(), ['jpg', 'png', 'gif', 'jpeg'])) {
                        $path = 'upload/' . date('Y/m/d');
                        $base_path = public_path() . '/filemanager/userfiles/';
                        $dir_name = $base_path . $path;
                        if (!is_dir($dir_name)) {
                            // Tạo thư mục của chúng tôi nếu nó không tồn tại
                            mkdir($dir_name, 0777, true);
                        }
                        if (is_dir($dir_name)) {
                            $file = CommonHelper::saveFile($fileRequest, $path);
                            $img[] = [
                                'status' => true,
                                'file' => '/public/filemanager/userfiles/' . $file,
                                'value' => $file,
                                'msg' => 'Thành công !',
                            ];
                        }
                    } else {
                        $img[] = [
                            'status' => false,
                            'msg' => 'Không đúng định dạng !',
                        ];
                    }
                }
            } else {
                $fileRequest = $request->file;
                if (in_array($fileRequest->getClientOriginalExtension(), ['jpg', 'png', 'gif', 'jpeg'])) {
                    $path = 'upload/' . date('Y/m/d');
                    $base_path = public_path() . '/filemanager/userfiles/';
                    $dir_name = $base_path . $path;
                    if (!is_dir($dir_name)) {
                        // Tạo thư mục của chúng tôi nếu nó không tồn tại
                        mkdir($dir_name, 0777, true);
                    }
                    if (is_dir($dir_name)) {
                        $file = CommonHelper::saveFile($fileRequest, $path);
                        $img[] = [
                            'status' => true,
                            'file' => '/public/filemanager/userfiles/' . $file,
                            'value' => $file,
                            'msg' => 'Thành công !',
                        ];
                    }
                } else {
                    $img[] = [
                        'status' => false,
                        'msg' => 'Không đúng định dạng !',
                    ];
                }
            }

            return response()->json([
                'data' => $img
            ]);
        }
    }

    public function getDataLocation(Request $r, $table) {
        if ($table == 'districts') {
            $items = District::where('province_id', $r->province_id)->pluck('name', 'id');
        } elseif ($table == 'wards') {
            $items = Ward::where('district_id', $r->district_id)->pluck('name', 'id');
        }
        return response()->json([
            'status' => true,
            'msg' => '',
            'data' => $items
        ]);
    }

    public function logIp(Request $r) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "".@$r->url_action."",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"entry.52451068\"\r\n\r\n".@$r->ip."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"entry.601939792\"\r\n\r\n".@$r->url."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"entry.266140612\"\r\n\r\n".@$r->from."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "postman-token: f8f5ec09-3502-9be7-a72b-419a2410d2a6"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return response()->json([
                'status' => true,
                'msg'=> $err
            ]);
        } else {
            return response()->json([
                'status' => true,
                'msg'=> '',
                'data' => $response
            ]);
        }
    }
}
