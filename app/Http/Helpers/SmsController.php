<?php

namespace App\Http\Helpers;

use App\Http\Controllers\Controller;

class SmsController extends Controller
{

    public function verifyUserPhoneFirebase($tel, $token)
    {
//        dd($token);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=" . config('services.firebase.api_key') . "&idToken=" . $token,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Content-Length : 0'
            ),
        ));
        $response = curl_exec($curl);

        curl_close($curl);
        $response = (array)json_decode($response);
//        dd($response['users'][0]->phoneNumber, $tel);
        if (array_key_exists('error', $response)) {

            return response()->json([
                'status' => false,
                'msg' => 'Thất bại',
                'errors' => $response['error']->message,
                'data' => null,
                'code' => 401
            ]);
        }

        if (str_replace('0', '+84', $tel) != $response['users'][0]->phoneNumber) {

            return response()->json([
                'status' => false,
                'msg' => 'Thất bại',
                'errors' => 'Sai số điện thoại',
                'data' => null,
                'code' => 401
            ]);
        }

        return [
            'status' => true,
            'msg' => 'Xác thực số điện thoại với Firebase thành công!',
            'errors' => (object)[],
            'data' => $response,
            'code' => 201
        ];

        //curl --request POST \
        //--url https://us.sms.api.sinch.com/xms/v1/jd63jf88477ll/batches \
        //  --header 'accept: application/json' \
        //--header 'authorization: Bearer {token}' \
        //--header 'content-type: application/json' \
        //--data '{"to":[null],"type":"mt_text","expire_at":"3 days after send_at","flash_message":false}'

    }
}
