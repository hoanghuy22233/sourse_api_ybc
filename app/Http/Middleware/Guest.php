<?php

namespace App\Http\Middleware;

use App\Http\Helpers\CommonHelper;
use App\Models\User;
use Closure;
use Auth;

class Guest
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->check()) {
            if ($guard == 'admin') {
                return redirect()->route('admin.login');
            } else {
                if ($request->has('login_back_url')) {
                    \Session::put('url.intended', $request->login_back_url);
                } else {
                    \Session::put('url.intended', $_SERVER['REQUEST_URI']);
                }
                return redirect()->route('dang-nhap');
            }
        }

        if (@Auth::guard($guard)->user()->status == -1) {
            \Session::flash('error', 'Tài khoản của bạn bị khóa');
            return back();
        } else if (@Auth::guard($guard)->user()->status == 0) {
            \Session::flash('error', 'Tài khoản của bạn chưa được kích hoạt!');
            return back();
        }

        return $next($request);
    }
}
