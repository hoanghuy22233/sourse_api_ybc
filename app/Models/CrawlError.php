<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrawlError extends Model
{
    protected $table = 'crawl_errors';

    protected $fillable = ['msg', 'line', 'path', 'item_id'];
}
