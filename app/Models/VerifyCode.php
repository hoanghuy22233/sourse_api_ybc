<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VerifyCode extends Model
{

    protected $table = 'verify_code';

    protected $fillable = [
        'tel', 'email', 'code', 'object_id', 'type'
    ];

}
