<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HelpArticle extends Model
{
    protected $table = 'help_articles';

    public function organization() {
        return $this->belongsTo(Organization::class, 'organization_id');
    }

    public function published_agency() {

        return $this->belongsTo(Organization::class, 'published_agency_id');
    }
    public function area() {

        return $this->belongsTo(Area::class, 'area_id');
    }

    public function impl_agency() {

        return $this->belongsTo(Area::class, 'impl_agency_id');
    }

}
