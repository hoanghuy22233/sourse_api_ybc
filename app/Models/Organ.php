<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organ extends Model
{
    protected $table = 'organ';

    protected $fillable = ['name', 'crawl_id', 'agency_amount', 'location', 'tinh', 'huyen', 'xa', 'address', 'phone_number', 'email'];
}
