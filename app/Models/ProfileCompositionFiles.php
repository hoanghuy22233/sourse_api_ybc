<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileCompositionFiles extends Model
{
    protected $table = 'profile_composition_files';

    public $timestamps = false;

}
