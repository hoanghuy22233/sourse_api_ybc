<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChangeDataHistory extends Model
{
	protected $table = 'change_data_history';

	protected $fillable = [
	    'table_name', 'item_id', 'data', 'admin_id', 'note'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }
}
