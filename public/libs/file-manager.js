// $(document).on('click', '.deleteimage', function () {          // Delete
//     $(this).click(function () {
//         $('.div0-' + $(this).attr('name')).find('.wrap-thumbnail').html('');
//         $('.thumb .div1-' + $(this).attr('name')).css('display', 'none');
//         $('.thumb .div2-' + $(this).attr('name')).css('display', 'block');
//         $('#' + $(this).attr('name')).val('');
//     });
// });

function BrowseFile(obj) {
    CKFinder.modal({
        language: 'vi',
        chooseFiles: true,
        width: 800,
        height: 600,
        onInit: function (finder) {
            finder.on('files:choose', function (evt) {
                var file = evt.data.files.first();
                var url_respon = file.getUrl();
                changeFile(obj, url_respon);
            });
            finder.on('file:choose:resizedImage', function (evt) {
                var url_respon = evt.data.resizedUrl;
                changeFile(obj, url_respon);
            });
        }
    });
}

function changeFile(obj, url_respon) {
    var current_image = $("#form-group-" + obj).find('#' + obj).val();
    if (url_respon != current_image) {
        $("#form-group-" + obj).find('#' + obj).val(url_respon);
        console.log(url_respon);
        if (url_respon.indexOf('.mp3') != -1) {
            //  Neu la file .mp3
            $("#form-group-" + obj).find('.div0-' + obj).find('.wrap-thumbnail .kt-avatar__holder').html('<a target="_blank" href="'+ url_respon +'">'+url_respon+'</a>');
        } else {
            //  Neu la file ảnh
            $("#form-group-" + obj).find('.div0-' + obj).find('.wrap-thumbnail .kt-avatar__holder').attr('style', 'background-image: url(\'' + url_respon + '\');');
        }
        $("#form-group-" + obj).find('.div1-' + obj).find('.kt-avatar').addClass('kt-avatar--changed');
    }
}

//Multi image

function BrowseMultiFile(obj) {
    CKFinder.modal({
        language: 'vi',
        chooseFiles: true,
        width: 800,
        height: 600,
        onInit: function (finder) {
            finder.on('files:choose', function (evt) {
                var file = evt.data.files.first();
                var url_respon = file.getUrl();
                changeMultiFile(obj, url_respon);
            });
            finder.on('file:choose:resizedImage', function (evt) {
                var url_respon = evt.data.resizedUrl;
                changeMultiFile(obj, url_respon);
            });
        }
    });
}

function changeMultiFile(obj, url_respon) {
    var current_image = $("#" + obj).val();
    if (url_respon != current_image) {
        $('#' + obj).val(url_respon);
        if (url_respon.indexOf('.mp3') != -1) {
            //  Neu la file .mp3
            $(".item-" + obj).find('.wrap-thumbnail .kt-avatar__holder').html('<a target="_blank" href="'+ url_respon +'">'+url_respon+'</a>');
        } else {
            //  Neu la file anh
            $(".item-" + obj).find('.wrap-thumbnail .kt-avatar__holder').attr('style', 'background-image: url(\'' + url_respon + '\');');
        }

        $(".item-" + obj).find('.kt-avatar').addClass('kt-avatar--changed');
    }
}
