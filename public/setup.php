<?php

header('Content-Type: text/html; charset=utf-8');
$infoDatabase = file_get_contents('../.env');
$servername = "localhost";
$database = "congnguyen_card";
$username = "congnguyen_card";
$password = "Admin!23";
// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$conn->set_charset('utf8');
if (isset($_GET['user_name']) && isset($_GET['password'])) {
    $admin = "INSERT INTO admin(name, email, password, status) VALUES ('admin','" . $_GET['user_name'] . "','" . password_hash($_GET['password'], PASSWORD_BCRYPT) . "',1)";
    $role = "INSERT INTO roles(name, display_name, description) VALUES ('supperadmin','Sở hữu phần mềm','Tất cả mọi quyền')";

    if ($conn->query($admin) === TRUE) {
        $admin_id = $conn->insert_id;

        if ($conn->query($role) === TRUE) {
            $role_id = $conn->insert_id;
            $modules = [
                'admin' => 'người dùng',
                'role' => 'nhóm quyền',
                'user' => 'khách hàng-đối tác',
                'contact' => 'liên hệ',
                'bill' => 'hóa đơn',
                'role_software' => 'nhóm quyền hệ thống',
                'widget' => 'widget',
                'unit_price' => 'Đơn giá',
                'error' => 'lỗi',
                'group_label' => 'Nhóm nhãn',
                'plugin_repository' => 'Plugin',
                'check_error_link' => 'lỗi các đường dẫn',
                'menus' => 'menu',
                'category' => 'danh mục',
                'marketing-mail' => 'gửi mail',
                'marketing-mail-log' => 'lịch sử gửi mail',
                'card' => 'thông tin thẻ',
                'history-card' => 'lịch sử nạp tiền'
            ];
            $permission_arr = [];
            foreach ($modules as $code => $name) {
                $permission = "INSERT INTO permissions(name, display_name, description) VALUES ('" . $code . "_view','Xem " . $name . "','Xem " . $name . "')";
                $conn->query($permission);
                $permission_id = $conn->insert_id;
                $permission_arr[] = $permission_id;
                $permission = "INSERT INTO permissions(name, display_name, description) VALUES ('" . $code . "_add','Thêm " . $name . "','Thêm " . $name . "')";
                $conn->query($permission);
                $permission_id = $conn->insert_id;
                $permission_arr[] = $permission_id;
                $permission = "INSERT INTO permissions(name, display_name, description) VALUES ('" . $code . "_edit','Sửa " . $name . "','Sửa " . $name . "')";
                $conn->query($permission);
                $permission_id = $conn->insert_id;
                $permission_arr[] = $permission_id;
                $permission = "INSERT INTO permissions(name, display_name, description) VALUES ('" . $code . "_delete','Xóa " . $name . "','Xóa " . $name . "')";
                $conn->query($permission);
                $permission_id = $conn->insert_id;
                $permission_arr[] = $permission_id;
            }
            $module_manager = [
                'role' => 'Phân quyền',
                'import' => 'Quản lý import',
                'export' => 'Quản lý export',
                'setting' => 'Cấu hình website',
                'dashboard' => 'Thống kê'
            ];
            foreach ($module_manager as $module => $module_label) {
                $permission = "INSERT INTO permissions(name, display_name, description) VALUES ('" . $module . "_manager','" . $module_label . "','" . $module_label . "')";
                $conn->query($permission);
                $permission_id = $conn->insert_id;
                $permission_arr[] = $permission_id;
            }
            foreach ($permission_arr as $permission_id) {
                $permissionRole = "INSERT INTO permission_role(permission_id, role_id) VALUES (" . $permission_id . ",1)";
                $conn->query($permissionRole);
            }
            $role_admin = "INSERT INTO role_admin(admin_id, role_id, status) VALUES (" . $admin_id . "," . $role_id . ",1)";
            $conn->query($role_admin);
        }

        unlink('setup.php');
        echo 'Tạo dữ liệu thành công! Tài khoản quản trị của bạn là: ' . $_GET['user_name'] . ' - Mật khẩu là: ' . $_GET['password'] . '<br> File setup.php đã được xóa! Click vào <a href="/admin/login">đây</a> để đăng nhập!';
    } else {
        echo "Error: " . $admin . "<br>" . $conn->error;
    }

} else {
    $admin = "INSERT INTO admin(name, email, password, status) VALUES ('admin','admin@gmail.com','" . password_hash('admin123', PASSWORD_BCRYPT) . "',1)";
    $role = "INSERT INTO roles(name, display_name, description) VALUES ('supperadmin','Sở hữu phần mềm','Tất cả mọi quyền')";
    if ($conn->query($admin) === TRUE) {
        $admin_id = $conn->insert_id;

        if ($conn->query($role) === TRUE) {
            $role_id = $conn->insert_id;
            $modules = [
                'admin' => 'người dùng',
                'role' => 'nhóm quyền',
                'user' => 'khách hàng-đối tác',
                'bill' => 'hóa đơn',
                'error' => 'lỗi',
                'card' => 'thông tin thẻ',
                'history-card' => 'lịch sử nạp tiền'
            ];
            $permission_arr = [];
            foreach ($modules as $code => $name) {
                $permission = "INSERT INTO permissions(name, display_name, description) VALUES ('" . $code . "_view','Xem " . $name . "','Xem " . $name . "')";
                $conn->query($permission);
                $permission_id = $conn->insert_id;
                $permission_arr[] = $permission_id;
                $permission = "INSERT INTO permissions(name, display_name, description) VALUES ('" . $code . "_add','Thêm " . $name . "','Thêm " . $name . "')";
                $conn->query($permission);
                $permission_id = $conn->insert_id;
                $permission_arr[] = $permission_id;
                $permission = "INSERT INTO permissions(name, display_name, description) VALUES ('" . $code . "_edit','Sửa " . $name . "','Sửa " . $name . "')";
                $conn->query($permission);
                $permission_id = $conn->insert_id;
                $permission_arr[] = $permission_id;
                $permission = "INSERT INTO permissions(name, display_name, description) VALUES ('" . $code . "_delete','Xóa " . $name . "','Xóa " . $name . "')";
                $conn->query($permission);
                $permission_id = $conn->insert_id;
                $permission_arr[] = $permission_id;
            }
            $module_manager = [
                'import' => 'Quản lý import',
                'export' => 'Quản lý export',
                'setting' => 'Cấu hình website',
                'dashboard' => 'Thống kê'
            ];
            foreach ($module_manager as $module => $module_label) {
                $permission = "INSERT INTO permissions(name, display_name, description) VALUES ('" . $module . "','" . $module_label . "','" . $module_label . "')";
                $conn->query($permission);
                $permission_id = $conn->insert_id;
                $permission_arr[] = $permission_id;
            }
            foreach ($permission_arr as $permission_id) {
                $permissionRole = "INSERT INTO permission_role(permission_id, role_id) VALUES (" . $permission_id . ",1)";
                $conn->query($permissionRole);
            }
            $role_admin = "INSERT INTO role_admin(admin_id, role_id, status) VALUES (" . $admin_id . "," . $role_id . ",1)";
            $conn->query($role_admin);
        }
        unlink('setup.php');
        echo 'Tạo dữ liệu thành công! Tài khoản quản trị của bạn là: admin@gmail.com - Mật khẩu là: admin123 <br> File setup.php đã được xóa! Click vào <a href="/admin/login">đây</a> để đăng nhập!';
    } else {
        echo "Error: " . $admin . "<br>" . $conn->error;
    }
}
mysqli_close($conn);