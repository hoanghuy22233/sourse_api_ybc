var width = 700;
var height = 700;
const artwork_layers = [];
const stage = new Konva.Stage({
    container: 'workart-container',
    width: width,
    height: height,
});
stage.on('click', (e) => {
    // do nothing if we mousedown on eny shape
    let activeTemplateId = getActiveTemplateId();
    if (e.target === stage) {
        let trans = artwork_layers[activeTemplateId].find('Transformer');
        $.each(trans, function (index, item) {
            item.hide();
        });
        stage.draw();
        return;
    }
    let layerName = e.target.getAttr('name');
    let trans = artwork_layers[activeTemplateId].find('Transformer');
    $.each(trans, function (index, item) {
        if (item.hasName(layerName)) {
            item.show();
        } else {
            item.hide();
        }

    });
    artwork_layers[activeTemplateId].draw();
    stage.draw();

    $(".layer-item").each(function (index, item) {
        if ($(item).data('layer-name') == layerName) {
            $(item).addClass('active');
        } else {
            $(item).removeClass('active');
        }
    });
});

// const layer = new Konva.Layer();
// stage.add(layer);

$("#artwork-templates .template-item").each(function (index, item) {
    let templateId = $(item).data('id');
    artwork_layers[templateId] = new Konva.Layer();
    stage.add(artwork_layers[templateId]);
});

function centreRectShape(shape) {
    shape.x((stage.getWidth() - shape.getWidth()) / 2);
    shape.y((stage.getHeight() - shape.getHeight()) / 2);
}

function clickedToBodyArtwork (e) {
    if (!$("#right-sidebar").hasClass('expand')) {
        $("#button-toggle").addClass('d-none');
    }
    $(".layer-item").removeClass('active');
    if (e.target.nodeName != 'CANVAS') {
        stage.find('Transformer').each(function (item, index) {
            item.hide();
        });
    }
    stage.draw();
}


function artworkDrawImage(name, path, width, height) {
    var imageObj = new Image();
    let activeTemplateId = getActiveTemplateId();
    let layer = artwork_layers[activeTemplateId];

    imageObj.onload = function () {
        var image = new Konva.Image({
            x: stage.getWidth() / 2,
            y: stage.getHeight() / 2,
            name: name,
            image: imageObj,
            width: width,
            height: height,
            draggable: true
        });

        var w = imageObj.width;
        var h = imageObj.height;

        // get the aperture we need to fit by taking padding of the stage size.
        var targetW = stage.getWidth();
        var targetH = stage.getHeight();

        // compute the ratios of image dimensions to aperture dimensions
        var widthFit = targetW / w;
        var heightFit = targetH / h;

        // compute a scale for best fit and apply it
        var scale = (widthFit > heightFit) ? heightFit : widthFit;
        scale = 0.5;

        w = parseInt(w * scale, 10);
        h = parseInt(h * scale, 10);
        image.size({
            width: w,
            height: h
        });

        // Finally position the canvas image object centered.
        centreRectShape(image);

        layer.add(image);
        layer.draw();

        var transformer = new Konva.Transformer({
            keepRatio: true,
            name: name,
            nodes: [image],
        });
        layer.add(transformer);
        layer.draw();

        // image.on('click', function () {
        //     transformer.show();
        //     layer.draw();
        // });

    };
    imageObj.src = path;
    stage.draw();
}

function getActiveTemplateId() {
    let tempalteId = '';
    $("#artwork-templates .template-item").each(function (index, item) {
        if ($(item).hasClass('active')) {
            tempalteId = $(item).data('id');
        }
    });
    return tempalteId;
}

function addLayerItemList(name, type) {
    var templateActive = null;
    $("#artwork-templates .template-item").each(function (index, item) {
        if ($(item).hasClass('active')) {
            templateActive = $(item);
        }
    });
    if (!templateActive) {
        templateActive = $("#artwork-templates .template-item")[0];
    }
    var templateId = templateActive.data('id');
    $.ajax({
        type: "GET",
        url: route('artwork.addLayer'),
        data: {
            name: name,
            type: type
        },
        dataType: 'html',
        success: function (res) {
            var clone = $($.parseHTML($.trim(res)));
            var cls = clone.attr('class');
            var selector = "#" + templateId + " .artwork-layers";
            $(selector).prepend(clone);
            updateIndexInput("#template-content", 'template-content-item');
            update2ndIndexInput(selector, cls);
            $('.layer-name-editable').editable({
                event: 'dblclick',
                closeOnEnter: true,
                callback: function (data) {
                    if (data.content) {
                        $(data.$el).parents('.layer-item').attr('data-layer-name', data.content);
                    }
                }
            });
        },
        failure: function () {

        }
    });

}

function showHideLayer(e) {
    var parent = $(e).parents('.layer-item');
    var grand_parent = parent.parents('.template-content-item');
    var templateId = grand_parent.attr('id');
    var layer = artwork_layers[templateId];
    var layerName = parent.data('layer-name');
    var type = parent.data('layer-type');
    var layerObj;
    if (type == 1) {
        layerObj = layer.find('Image').hasName(layerName)[0];
    } else {

    }
    var tr = layer.find('Transformer').hasName(layerName)[0];
    var btnVisible = $(e).find('button');
    if (btnVisible.hasClass('d-none')) {
        btnVisible.removeClass('d-none');
        layerObj.show();
        tr.show();
    } else {
        btnVisible.addClass('d-none');
        layerObj.hide();
        tr.hide();
    }
    stage.draw();
}

function removeLayerItem(e) {
    var parent = $(e).parents('.layer-item');
    var grand_parent = parent.parents('.template-content-item');
    var templateId = grand_parent.attr('id');
    var layer = artwork_layers[templateId];
    var layerName = parent.data('layer-name');
    var imgs = layer.find('Image').toArray();
    var img = layer.find('Image').hasName(layerName)[0];
    img.destroy();
    var tr = layer.find('Transformer').hasName(layerName)[0];
    tr.destroy();
    layer.draw();
    // imgs.forEach(function (item) {
    //     if (item.getName() == layerName) {
    //         item.destroy();
    //     }
    // });
    // var trans = layer.find('Transformer').toArray();
    // trans.forEach(function (item) {
    //     if (item.getName() == layerName) {
    //         item.destroy();
    //     }
    // });
    // layer.draw();
    parent.remove();
    stage.draw();
}

function duplicateLayerItem(e) {
    var parent = $(e).parents('.layer-item');
    var layerName = parent.data('layer-name');
    var type = parent.data('layer-type');
    var newLayerName = layerName + ' copy';

    var grand_parent = parent.parents('.template-content-item');
    var templateId = grand_parent.attr('id');
    var layer = artwork_layers[templateId];
    var oldlayer = layer.find('Image').hasName(layerName)[0];
    addLayerItemList(newLayerName, type);
    if (type == 1) {
        artworkDrawImage(newLayerName, oldlayer.getAttr('image').src, 100, 100);
    }
}

function clickedLayerItem(e) {
    $("#button-toggle").removeClass('d-none');
    $(".layer-item").removeClass('active');
    $(e).addClass('active');
    let layerName = $(e).data('layer-name');
    let templateActiveId = getActiveTemplateId();
    let trs = artwork_layers[templateActiveId].find('Transformer');
    $.each(trs, function (index, transformer) {
        if (transformer.getAttr('name') == layerName) {
            transformer.show();
        } else {
            transformer.hide();
        }
    });
    stage.draw();
}

function saveArtworkSize() {
    var width = $("#form-artwork-size input[name=width]").val();
    var height = $("#form-artwork-size input[name=height]").val();
    if (!width) {
        width = 0;
    }
    if (!height) {
        height = 0;
    }
    stage.width(width);
    stage.height(height);
    $("#artwork-form input[name=width]").val(width);
    $("#artwork-form input[name=height]").val(height);
    var strSize = "" + width + "x" + height;
    $("#display-current-artwork-size").text(strSize);
    $("#changeSizeArtwork").modal('hide');
    stage.draw();
}

function addTemplate(name, templateId) {
    var numberTemplate = $("#artwork-templates .template-item").length;
    var newTemplateId = "artwork-template-" + (numberTemplate + 1) + "";
    $("#artwork-templates .template-item").removeClass('active');
    $("#template-content .template-content-item").removeClass('active');
    var template_header_item = '<div class="template-item active" data-id="' + newTemplateId + '">';
    template_header_item += '<input name="templates[0][name]" type="hidden" value="' + name + '">';
    template_header_item += '<a href="#"><i class="fas fa-arrows-alt"></i> <span class="header-template-name">' + name + '</span></a></div>';

    $("#artwork-templates").append(template_header_item);
    if (!templateId) {
        let template_content_item = '<div class="template-content-item active" id="' + newTemplateId + '"><div class="bg-light-black">';
        template_content_item += '<div class="text-center edit-template-zone">';
        template_content_item += '<input name="templates[0][name]" type="hidden" value="' + name + '" class="template-inp-name">';
        template_content_item += '<input name="templates[0][thumbnail]" type="hidden" value="" class="template-inp-thumbnail">';
        template_content_item += '<small class="text-white template-name">Editing ' + name + '</small>';
        template_content_item += '<div class="d-inline-flex ml-3">';
        template_content_item += '<a href="#" class="p-1 btn-edit-template"><i class="flaticon2-edit text-white"></i></a>';
        template_content_item += '<a href="#" class="p-1 btn-duplicate-template"><i class="flaticon2-copy text-white"></i></a>';
        template_content_item += '<a href="#" class="p-1 btn-remove-template"><i class="flaticon2-delete text-white"></i></a>';
        template_content_item += '</div></div></div>';
        template_content_item += '<div class="list-layer"><ul class="list-group artwork-layers"></ul></div></div>';

        $("#template-content").append(template_content_item);
    } else {
        // duplicate layer
        let clone = $("#template-content #" + templateId).clone();
        clone.attr('id', newTemplateId);
        clone.addClass('active');
        $("#template-content").append(clone);
    }

    updateIndexInput("#artwork-templates", "template-item");

    updateIndexInput("#template-content", "template-content-item");
    update2ndIndexInput("#" + newTemplateId + " .artwork-layers", "layer-item");


    // create new canvas
    if (!templateId) {
        artwork_layers[newTemplateId] = new Konva.Layer();
        stage.add(artwork_layers[newTemplateId]);
    } else {
        artwork_layers[newTemplateId] = artwork_layers[templateId].clone();
        let layer_images = artwork_layers[newTemplateId].find('Image');
        let layer_texts = artwork_layers[newTemplateId].find('Text');
        let trs = artwork_layers[newTemplateId].find('Transformer');

        $.each(layer_images, function (index, image) {
            let image_name = image.getAttr('name');
            $.each(trs, function (i, transform) {
                if (transform.getAttr('name') == image_name) {
                    transform.nodes([image]);
                }
            });
        });

        $.each(layer_texts, function (index, text) {
            let text_name = text.getAttr('name');
            $.each(trs, function (i, transform) {
                if (transform.getAttr('name') == text_name) {
                    transform.nodes([text]);
                }
            });
        });
        stage.add(artwork_layers[newTemplateId]);
    }

    stage.draw();
    showHideTemplateCanvas();
}

function editInfoTemplate(e) {
    let parent = $(e).parents('.template-content-item');
    let name = parent.find("input.template-inp-name").val();
    let thumbnail = parent.find("input.template-inp-thumbnail").val();
    $("#editInfoTemplate .modal-header h5").text("Edit - " + name);
    $("#editInfoTemplate .modal-body input[name=template_id]").val(parent.attr('id'));
    $("#editInfoTemplate .modal-body input[name=name]").val(name);
    $("#editInfoTemplate .modal-body input[name=thumbnail]").val(thumbnail);

    $("#templateThumbnailPreview").empty();

    if (thumbnail) {
        let img = document.createElement('img');
        img.src = thumbnail;

        $("#templateThumbnailPreview").append(img);
    }

    $("#editInfoTemplate").modal('show');
}

function duplicateTemplate(e) {
    let parent = $(e).parents('.template-content-item');
    let templateId = parent.attr('id');
    let newName = '';
    var numberTemplate = $("#artwork-templates .template-item").length;
    var newTemplateId = "artwork-template-" + (numberTemplate + 1) + "";
    $("#artwork-templates .template-item").each(function (index, item) {
        let copyTemplateId = $(item).data('id');
        if (copyTemplateId == templateId) {
            newName = $(item).find('input').val() + ' copy';
        }
    });

    addTemplate(newName, templateId);

    stage.draw();
}

function removeTemplate(e) {
    let parent = $(e).parents('.template-content-item');
    let templateId = parent.attr('id');
    $("#artwork-templates .template-item").each(function (index, item) {
        let curTemplateId = $(item).data('id');
        if (curTemplateId == templateId) {
            $(item).remove();
        }
    });
    parent.remove();
    artwork_layers[templateId].destroy();
    artwork_layers.splice(templateId, 1);

    let newActiveTemplate = $("#artwork-templates .template-item")[0];
    $(newActiveTemplate).addClass('active');
    let newActiveTemplateId = $(newActiveTemplate).data('id');
    $("#template-content #" + newActiveTemplateId).addClass('active');

    showHideTemplateCanvas();
}

function chooseTemplate(e) {
    $("#artwork-templates .template-item").removeClass('active');
    $("#template-content .template-content-item").removeClass('active');

    var templateId = $(e).data('id');
    $(e).addClass('active');
    $("#template-content #" + templateId).addClass('active');

    showHideTemplateCanvas();
}

function saveTemplateInfo(e) {
    let templateId = $("#editInfoTemplate .modal-body input[name=template_id]").val();
    let name = $("#editInfoTemplate .modal-body input[name=name]").val();
    let thumbnail = $("#editInfoTemplate .modal-body input[name=thumbnail]").val();
    $("#" + templateId).find("input.template-inp-name").val(name);
    $("#" + templateId).find("input.template-inp-thumbnail").val(thumbnail);
    $("#" + templateId).find("small.template-name").text("Editing " + name);

    $("#artwork-templates .template-item").each(function (index, item) {
        if ($(item).data('id') == $("#" + templateId).attr('id')) {
            $(item).find("span.header-template-name").text(name);
        }
    });

    $("#editInfoTemplate").modal('hide');

}

function showHideTemplateCanvas() {
    $("#artwork-templates .template-item").each(function (index, item) {
        var templateId = $(item).data('id');
        artwork_layers[templateId].hide();
        // $("#" + templateId + " .artwork-layers .layer-item").each(function (idx, layter_item) {
        //     var layerName = $(layter_item).data('layer-name');
        //     var type = $(layter_item).data('layer-type');
        //     var layerObj;
        //     if (type == 1) {
        //         layerObj = layer.find('Image').hasName(layerName)[0];
        //     } else {
        //
        //     }
        //     var tr = layer.find('Transformer').hasName(layerName)[0];
        //     layerObj.hide();
        //     tr.hide();
        // });
        if ($(item).hasClass('active')) {
            artwork_layers[templateId].show()
            // var activeTemplateId = $(item).data('id');
            // $("#" + activeTemplateId + " .artwork-layers .layer-item").each(function (idx, layter_item) {
            //     var layerName = $(layter_item).data('layer-name');
            //     var type = $(layter_item).data('layer-type');
            //     var layerObj;
            //     if (type == 1) {
            //         layerObj = layer.find('Image').hasName(layerName)[0];
            //     } else {
            //
            //     }
            //     var tr = layer.find('Transformer').hasName(layerName)[0];
            //     layerObj.show();
            //     tr.show();
            // });
        }
    });
    stage.draw();
}

// Function about Data

function changeOption(option) {
    switch (option) {
        case 0:
            $('.detail-option').addClass('d-none');
            $('#no-personalization').removeClass('d-none');
            break;
        case 1:
            $('.detail-option').addClass('d-none');
            $('#clipart-category').removeClass('d-none');
            break;
        case 2:
            $('.detail-option').addClass('d-none');
            $('#group-clipart-categories').removeClass('d-none');
            break;
        case 3:
            $('.detail-option').addClass('d-none');
            $('#upload-photo').removeClass('d-none');
            break;
    }
    openAdvancedOption(false);
}

function update2ndIndexInput(selector, cls) {
    $(selector).find('.' + cls).each(function (index, value) {
        $(value).find('input, select, textarea').each(function () {
            var $this = $(this);
            $this.attr('name', $this.attr('name').replace(/([^\[\]]+)(?=\]\[[^\]]+\]$)/, function ($0, $1) {
                var order = index + "";
                return order;
            }));
        });
    });
}

function updateIndexInput(selector, cls) {
    $(selector).find('.' + cls).each(function (index, value) {
        $(value).find('input, select, textarea').each(function () {
            var $this = $(this);
            if ($this[0].hasAttribute('name')) {
                $this.attr('name', $this.attr('name').replace(/\[(\d+)\]/, function ($0, $1) {
                    var order = '[' + index + ']';
                    return order;
                }));
            }
            // var newName = $this.attr('name');
            // var newId = newName.replace("][", "_");
            // newId = newId.replace("[", "_");
            // newId = newId.replace("]", "");
            // // console.log(newId);
            // // $this.attr('id', newId);
            // if (newId.includes('caption')) {
            //     $this.attr('id', newId);
            // }
            // var next = $this.next();
            // var parent = $this.parent();
            // if (parent.hasClass('input-group')) {
            //     if (parent.next().is("p")) {
            //         parent.next().attr('id', 'error_' + newId);
            //     }
            // } else {
            //     if (next.is("p")) {
            //         next.attr('id', 'error_' + newId);
            //     }
            // }
        });

    });
}

function saveArtwork(e) {
    // var data = $("#artwork-form").serialize();
    // $.ajax({
    //     type: "GET",
    //     url: route('artwork.addLayer'),
    //     data: data,
    //     dataType: 'json',
    //     success: function (res) {
    //
    //     },
    //     failure: function () {
    //
    //     }
    // });
    $("#artwork-form").submit();
}
