

function openAdvancedOption(open) {
    if ($('#advanced-option-button').hasClass('advanced-open')) {
        if (open) {
            $('#advanced-option-button').removeClass('advanced-open');
            $('.advanced-option').addClass('d-none');
            $('.advanced-icon').removeClass('fa-minus');
            $('.advanced-icon').addClass('fa-plus');
        } else {
            openDetailAdvancedOption();
        }
    } else {
        if (open) {
            openDetailAdvancedOption();
        }
    }
}

function openDetailAdvancedOption() {
    var option;
    if ($('input[name=radios1][value="0"]').is(':checked')) {
        option = 0;
    }
    if ($('input[name=radios1][value="1"]').is(':checked')) {
        option = 1;
    }
    if ($('input[name=radios1][value="2"]').is(':checked')) {
        option = 2;
    }
    if ($('input[name=radios1][value="3"]').is(':checked')) {
        option = 3;
    }
    switch (option) {
        case 0:
            $('.advanced-option').addClass('d-none');
            $('#advanced-personalization').removeClass('d-none');
            break;
        case 1:
            $('.advanced-option').addClass('d-none');
            $('#advanced-clipart-category').removeClass('d-none');
            break;
        case 2:
            $('.advanced-option').addClass('d-none');
            $('#advanced-group-clipart-categories').removeClass('d-none');
            break;
        case 3:
            $('.advanced-option').addClass('d-none');
            $('#advanced-upload-photo').removeClass('d-none');
            break;
    }
    $('#advanced-option-button').addClass('advanced-open');
    $('.advanced-icon').removeClass('fa-plus');
    $('.advanced-icon').addClass('fa-minus');
}

function toogleLayerDefault() {
    if ($('input[name=toogle_hide_show]').prop('checked')) {
        $('#set_layer_default').removeClass('d-none');
    } else {
        $('#set_layer_default').addClass('d-none');
    }
}

function toggleNav() {
    if ($("#right-sidebar").hasClass("expand")) {
        $("#right-sidebar").removeClass("expand");
    } else {
        $("#right-sidebar").addClass("expand");
    }
    if ($('#toogle-chevron').hasClass('fa-chevron-left')) {
        $('#toogle-chevron').removeClass('fa-chevron-left');
        $('#toogle-chevron').addClass('fa-chevron-right');
    } else if ($('#toogle-chevron').hasClass('fa-chevron-right')) {
        $('#toogle-chevron').removeClass('fa-chevron-right');
        $('#toogle-chevron').addClass('fa-chevron-left');
    }
}

function closeNav() {
    $('#right-sidebar').attr('right', -250);
}

function hightLightSelected(e) {
    $('.layer-item').removeClass('selected-layer');
    $(e).addClass('selected-layer');
    $('#button-toggle').removeClass('d-none');
}

function hightlightPrintareaOption(e) {
    $('.layer-item').removeClass('selected-layer');
    $(e).addClass('selected-layer');
    $('.detail-option').removeClass('d-none');
}

function changeEnableMasked() {
    if ($('input[name=enable_masked]').is(':checked')) {
        $('#masked-detail-option').removeClass('d-none');
    } else {
        $('#masked-detail-option').addClass('d-none');
    }
}

function editMask(is_edited) {
    if (is_edited) {
        $('#printarea-option').addClass('d-none');
        $('#edit-mask-zone').removeClass('d-none');
    } else {
        $('#edit-mask-zone').addClass('d-none');
        $('#printarea-option').removeClass('d-none');
    }
}

function editPerspective(is_edited) {
    if (is_edited) {
        $('#printarea-option').addClass('d-none');
        $('#edit-perspective-zone').removeClass('d-none');
    } else {
        $('#edit-perspective-zone').addClass('d-none');
        $('#printarea-option').removeClass('d-none');
    }
}
