$(document).ready(function() {
        $("#box-slide-1").mouseover(function() {
            $(".box-slide").removeClass("active"), $("#ntl-Document-2").attr("class", "path22"), $("#ntl-Box-1").attr("class", "path11")
        }), $("#box-slide-2").mouseover(function() {
            $(".box-slide").removeClass("active"), $("#ntl-Box-1").attr("class", "path1")
        }), $("#box-slide-3").mouseover(function() {
            $(".box-slide").removeClass("active"), $("#ntl-Location-2").attr("class", "path11"), $("#ntl-Location-22").attr("class", "path22"), $("#ntl-Box-1").attr("class", "path11")
        }), $("#box-slide-1").mouseout(function() {
            $("#ntl-Document-2").attr("class", "path2"), $("#ntl-Box-1").attr("class", "path1"), $("#ntl-Location-2").attr("class", "path1"), $("#box-slide-2").addClass("active")
        }), $("#box-slide-3").mouseout(function() {
            $("#ntl-Document-2").attr("class", "path2"), $("#ntl-Box-1").attr("class", "path1"), $("#ntl-Location-2").attr("class", "path1"), $("#ntl-Location-22").attr("class", "path2"), $("#box-slide-2").addClass("active")
        }), $("#box-slide-2").mouseout(function() {
            $("#box-slide-2").addClass("active")
        }), $("#partners-logo").slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: !1,
            autoplaySpeed: 3e3,
            arrows: !0,
            dots: !1,
            pauseOnHover: !1,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: !0,
                    dots: !1
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    dots: !1
                }
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: !1
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    dots: !1
                }
            }]
        }), $("#owlcarousel").slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: !1,
            autoplaySpeed: 3e3,
            arrows: !1,
            dots: !1,
            pauseOnHover: !1,
            responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }]
        }), $(".change_city").on("change", function() {
            let t = this.value,
                e = $(this).data("id");
            var i = $("#" + e);
            $.ajax({
                type: "GET",
                url: "get_district",
                data: {
                    city_id: t,
                    _token: $('meta[name="csrf-token"]').attr("content")
                },
                dataType: "json",
                beforeSend: function() {},
                success: function(t) {
                    if (0 == t.status) {
                        var o = "";
                        $.each(t.data, function(t, e) {
                            o += '<option value="' + e.id + '">' + e.district_name + "</option>"
                        }), $("#" + e).html(o), i.selectpicker("refresh")
                    }
                },
                complete: function() {}
            })
        }),$(".change_city1").on("change", function() {
            let t = this.value,
                e = $(this).data("id");
            var i = $("#" + e);
            $.ajax({
                type: "GET",
                url: "get_district",
                data: {
                    city_id: t,
                    _token: $('meta[name="csrf-token"]').attr("content")
                },
                dataType: "json",
                beforeSend: function() {},
                success: function(t) {
                    if (0 == t.status) {
                        var o = "";
                        $.each(t.data, function(t, e) {
                            o += '<option value="' + e.id + '">' + e.district_name + "</option>"
                        }), $("#" + e).html(o)
                    }
                },
                complete: function() {}
            })
        }), $("#register_email").submit(function(t) {
            t.preventDefault();
            let e = $("#email_register").val();
            $.ajax({
                type: "POST",
                url: "register_email",
                data: {
                    email: e,
                    _token: $('meta[name="csrf-token"]').attr("content")
                },
                success: function(t) {
                    t.status, alert(t.data)
                }
            })
        }), $("#tracking_top_backup").submit(function(t) {
            t.preventDefault();
            let e = $("#bill").val();
            $.ajax({
                type: "POST",
                url: "tracking_bill",
                data: {
                    bill: e,
                    _token: $('meta[name="csrf-token"]').attr("content")
                },
                success: function(t) {
                    0 == t.status ? "" == t.url ? ($("#error_bill").show()) : window.location.href = t.url : ($("#error_bill").show())
                }
            })
        })
    }), $('.lang_choice_1').on('change', function(){
        var url = this.value;
        window.location.href=url;
    }),
    function(t) {
        var e = [],
            i = !1,
            o = !1,
            n = {
                interval: 250,
                force_process: !1
            },
            a = t(window),
            s = [];

        function r() {
            o = !1;
            for (var i = 0, n = e.length; i < n; i++) {
                var a = t(e[i]).filter(function() {
                    return t(this).is(":appeared")
                });
                if (a.trigger("appear", [a]), s[i]) {
                    var r = s[i].not(a);
                    r.trigger("disappear", [r])
                }
                s[i] = a
            }
        }
        t.expr[":"].appeared = function(e) {
            var i = t(e);
            if (!i.is(":visible")) return !1;
            var o = a.scrollLeft(),
                n = a.scrollTop(),
                s = i.offset(),
                r = s.left,
                l = s.top;
            return l + i.height() >= n && l - (i.data("appear-top-offset") || 0) <= n + a.height() && r + i.width() >= o && r - (i.data("appear-left-offset") || 0) <= o + a.width()
        }, t.fn.extend({
            appear: function(a) {
                var l = t.extend({}, n, a || {}),
                    c = this.selector || this;
                if (!i) {
                    var d = function() {
                        o || (o = !0, setTimeout(r, l.interval))
                    };
                    t(window).scroll(d).resize(d), i = !0
                }
                return l.force_process && setTimeout(r, l.interval),
                    function(t) {
                        e.push(t), s.push()
                    }(c), t(c)
            }
        }), t.extend({
            force_appear: function() {
                return !!i && (r(), !0)
            }
        })
    }("undefined" != typeof module ? require("jquery") : jQuery),
    function(t) {
        var e = function(i, o) {
            this.$element = t(i), this.options = t.extend({}, e.DEFAULTS, this.dataOptions(), o), this.init()
        };
        e.DEFAULTS = {
            from: 0,
            to: 0,
            speed: 1e3,
            refreshInterval: 100,
            decimals: 0,
            formatter: function(t, e) {
                return value = t.toFixed(e.decimals), value = value.replace(/\B(?=(\d{3})+(?!\d))/g, "."), value
            },
            onUpdate: null,
            onComplete: null
        }, e.prototype.init = function() {
            this.value = this.options.from, this.loops = Math.ceil(this.options.speed / this.options.refreshInterval), this.loopCount = 0, this.increment = (this.options.to - this.options.from) / this.loops
        }, e.prototype.dataOptions = function() {
            var t = {
                    from: this.$element.data("from"),
                    to: this.$element.data("to"),
                    speed: this.$element.data("speed"),
                    refreshInterval: this.$element.data("refresh-interval"),
                    decimals: this.$element.data("decimals")
                },
                e = Object.keys(t);
            for (var i in e) {
                var o = e[i];
                void 0 === t[o] && delete t[o]
            }
            return t
        }, e.prototype.update = function() {
            this.value += this.increment, this.loopCount++, this.render(), "function" == typeof this.options.onUpdate && this.options.onUpdate.call(this.$element, this.value), this.loopCount >= this.loops && (clearInterval(this.interval), this.value = this.options.to, "function" == typeof this.options.onComplete && this.options.onComplete.call(this.$element, this.value))
        }, e.prototype.render = function() {
            var t = this.options.formatter.call(this.$element, this.value, this.options);
            this.$element.text(t)
        }, e.prototype.restart = function() {
            this.stop(), this.init(), this.start()
        }, e.prototype.start = function() {
            this.stop(), this.render(), this.interval = setInterval(this.update.bind(this), this.options.refreshInterval)
        }, e.prototype.stop = function() {
            this.interval && clearInterval(this.interval)
        }, e.prototype.toggle = function() {
            this.interval ? this.stop() : this.start()
        }, t.fn.countTo = function(i) {
            return this.each(function() {
                var o = t(this),
                    n = o.data("countTo"),
                    a = "object" == typeof i ? i : {},
                    s = "string" == typeof i ? i : "start";
                (!n || "object" == typeof i) && (n && n.stop(), o.data("countTo", n = new e(this, a))), n[s].call(n)
            })
        }
    }(jQuery);
var lazyImage = "vendor/loader.gif";

function animatecounters() {
    $(".timer").each(function(t) {
        var e = $(this);
        t = $.extend({}, t || {}, e.data("countToOptions") || {}), e.countTo(t)
    })
}

function placeOrder() {
    $(".spinner-border-main").show();
    let t = $("#cod_amt").val();
    t = t.replace(/,/g, "");
    let e = $("#feature").val(),
        i = $("#cargo_content").val(),
        o = $("#send_city_id").val();
    0 == o ? ($("#err_send_city_id").show(), $(".spinner-border-main").hide()) : $("#err_send_city_id").hide();
    let n = $("#send_district_id").val();
    0 == n ? ($("#err_send_district_id").show(), $(".spinner-border-main").hide()) : $("#err_send_district_id").hide();
    let a = $("#recieve_city_id").val();
    0 == a ? ($("#err_recieve_city_id").show(), $(".spinner-border-main").hide()) : $("#err_recieve_city_id").hide();
    let s = $("#recieve_district_id").val();
    0 == s ? ($("#err_recieve_district_id").show(), $(".spinner-border-main").hide()) : $("#err_recieve_district_id").hide();
    let r = $("#package_no").val();
    0 == r ? ($("#err_package_no").show(), $(".spinner-border-main").hide()) : $("#err_package_no").hide();
    let l = $("#weight").val();
    0 == l ? ($("#err_weight").show(), $(".spinner-border-main").hide()) : $("#err_weight").hide();
    let c = "tra-cuoc.html?send_city_id=" + o + "&send_district_id=" + n + "&recieve_city_id=" + a + "&recieve_district_id=" + s + "&package_no=" + r + "&weight=" + l + "&cod_amt=" + t + "&feature=" + e + "&cargo_content=" + i + "&hoantat=Hoàn+tất";
    o > 0 && n > 0 && a > 0 && s > 0 && r > 0 && l > 0 && (window.location.href = c)
}

function FormatNumber(obj) {
    var strvalue, num;
    strvalue = eval(obj) ? eval(obj).value : obj, num = strvalue.toString().replace(/\$|\,/g, ""), isNaN(num) && (num = ""), sign = num == (num = Math.abs(num)), num = Math.floor(100 * num + .50000000001), num = Math.floor(num / 100).toString();
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) num = num.substring(0, num.length - (4 * i + 3)) + "," + num.substring(num.length - (4 * i + 3));
    eval(obj).value = (sign ? "" : "-") + num
}
$(document).ready(function() {
    $(".timer").removeClass("appear"), $(".timer").appear(), $(document.body).on("appear", ".timer", function(t) {
        $(this).hasClass("appear") || (animatecounters(), $(this).addClass("appear"))
    })
}), jQuery(document).ready(function(t) {
    t("img.lazyload").each(function() {
        t(this).attr("data-original", t(this).attr("src")), t(this).attr("src", lazyImage), t(this).lazyload({
            effect: "fadeIn"
        })
    })
}), $(".dactinh li").on("click", function() {
    let t = $(this).attr("data-value"),
        e = $(this).attr("data-id");
    $("#feature").val(t), $("#cargo_content").val(e), $(".dactinh li.current").removeClass("current"), $(this).addClass("current")
});
