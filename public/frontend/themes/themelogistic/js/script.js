$("#testimonials-review").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: 0,
    pauseOnHover: 0,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToScroll:1,
                slidesToShow: 3,
                autoplaySpeed:3000,
                infinite:true,
            }
        },
        {
        breakpoint: 1024,
        settings: {
            slidesToScroll:1,
            slidesToShow: 2,
            autoplaySpeed:3000,
            infinite:true,
        }
    }, {
        breakpoint: 768,
        settings: {
            slidesToScroll:1,
            slidesToShow: 1,
            infinite:true,
            autoplaySpeed:3000,
        }
    },
        {
            breakpoint: 320,
            settings: {
                autoplaySpeed:3000,
                slidesToScroll:1,
                slidesToShow: 1,
                infinite:true,
            }
        }]
})

    var partner = {
        init: function () {
            this.owlCarousel();
        },
        owlCarousel: function () {
            $(document).ready(function () {
                var softAllSlider = $('#owl-partner');
                softAllSlider.owlCarousel({
                    loop: true,
                    margin: 30,
                    nav: true,
                    dots:true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    smartSpeed:800,
                    responsive: {
                        0: {
                            items: 1
                        },
                        768:{
                            items: 2
                        },
                        1024:{
                            items: 1
                        }
                    }
                })

            });
        },
    }
    partner.init();
const optionsblog = {
    init: function() {
        this.setupBlog('.list_news', '.news-item')
    },
    setupBlog: function(main, options) {
        const wrap = document.querySelector(main)
        if (wrap) {
            const optionsBtn = wrap.querySelectorAll(options)
            optionsBtn.forEach((item, index) => item.addEventListener('click', () => {
                optionsBtn.forEach(btn => btn.classList.remove('active'))
                item.classList.add('active')
            }))
        }
    }
}
optionsblog.init();

var isotope ={
    init:function(){
        this.isotopeFilter();
    } ,
    isotopeFilter:function(){
        var $grid = $('.grid').isotope({
            itemSelector: '.grid-item',
            layoutMode: 'fitRows'
        });

        $('.filter-button-group li').on("click",function(){
            var value = $(this).attr('data-filter');
            $grid.isotope({
                filter: value

            });
        });
    }
};
isotope.init();