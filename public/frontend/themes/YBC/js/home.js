//animation
AOS.init();
// btn-slide
$('#btn-slide-1').click( function () {
    $('#slider-2').css('display', 'none');
    $('#slider-3').css('display', 'none');
    $('#slider-1').css('display', 'block');
    $(this).addClass('active')
    $('#btn-slide-2').removeClass('active');
    $('#btn-slide-3').removeClass('active');
    $('.slider-1').get(0).slick.setPosition();
});
$('#btn-slide-2').click( function () {
    $('#slider-1').css('display', 'none');
    $('#slider-3').css('display', 'none');
    $('#slider-2').css('display', 'block');
    $(this).addClass('active')
    $('#btn-slide-1').removeClass('active');
    $('#btn-slide-3').removeClass('active');
    $('.slider-2').get(0).slick.setPosition();
});
$('#btn-slide-3').click( function () {
    $('#slider-1').css('display', 'none');
    $('#slider-2').css('display', 'none');
    $('#slider-3').css('display', 'block');
    $(this).addClass('active')
    $('#btn-slide-2').removeClass('active');
    $('#btn-slide-1').removeClass('active');
    $('.slider-3').get(0).slick.setPosition();
})
// select date
$('input[name="choose-date"]').daterangepicker({
    singleDatePicker: true,
});
// slide
$('.slider-1').slick({
    centerMode: true,
    arrows: false,
    slidesToShow:3,
    dots: true,
    autoplay:true,
    autoplaySpeed: 800,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
            slidesToShow: 3,
            slidesToScroll: 1
            }
        },
        {
            breakpoint: 1023,
            settings: {
            slidesToShow: 3,
            slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
            slidesToShow: 1,
            slidesToScroll: 1
            }
        }
    ]
});
$('.slider-2').slick({
    centerMode: true,
    arrows: false,
    slidesToShow:3,
    dots: true,
    autoplay:true,
    autoplaySpeed: 800,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
            slidesToShow: 3,
            slidesToScroll: 1
            }
        },
        {
            breakpoint: 1023,
            settings: {
            slidesToShow: 3,
            slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
            slidesToShow: 1,
            slidesToScroll: 1
            }
        }
    ]
});
$('.slider-3').slick({
    centerMode: true,
    arrows: false,
    slidesToShow:3,
    dots: true,
    autoplay:true,
    autoplaySpeed: 800,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
            slidesToShow: 3,
            slidesToScroll: 1
            }
        },
        {
            breakpoint: 1023,
            settings: {
            slidesToShow: 3,
            slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
            slidesToShow: 1,
            slidesToScroll: 1
            }
        }
    ]
});


// menu-mobile
$('.header-top-menu__icon-list').click( function () {
    $('.header-top-menu__list').addClass('active');
})
$('.header-top-menu__con-close').click( function () {
    $('.header-top-menu__list').removeClass('active');
})
//button link form
$(document).ready(function(){
    $( "a.home-event-left__button" ).click(function( event ) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 500);
    });
});