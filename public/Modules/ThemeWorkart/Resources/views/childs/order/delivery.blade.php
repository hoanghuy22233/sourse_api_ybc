@extends('themeworkart::layouts.master')
@section('main_content')
    <div class="d-none d-md-block pl-sm-0 pl-md-3">
        <div class="breadcrumbs container-custom">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item fs14"><a href="#">Cart</a></li>
                    <li class="breadcrumb-item fs14"><a href="#">Information</a></li>
                    <li class="breadcrumb-item fs14"><a href="#">Shipping</a></li>
                    <li class="breadcrumb-item fs14 active">Payment</li>
                </ol>
            </nav>
        </div>
    </div>
    <div class="container-custom">
        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 contact-info-customer" style="margin-left: 39px;">
                <div class="row d-flex justify-content-between" style="max-width: 583px;">
                    <p class="color-text fs18">Contact information</p>
                    <p class="color-text fs14">Already have an account? <span style="color: #2F80ED;">Log in</span></p>
                </div>
                <div class="row form-info-checkout"   style="max-width: 583px;">
                    <form method="POST" action="{{ route('order.createBill') }}" style="width: 100%; margin-bottom: 73px;">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <input name="bill[user_email]" type="text" class="form-control rounded-0" placeholder="Email" style="height: 50px;">
                        </div>
                        <div class="custom-control custom-checkbox my-1 mr-sm-2 mt-3">
                            <input type="checkbox" class="custom-control-input" id="keepme">
                            <label class="custom-control-label fs14" style="color: #707070;" for="keepme">Keep me up to date on news and exclusive offerse</label>
                        </div>
                        <p class="color-text fs18" style="margin-top: 36px;">Shipping address</p>
                        <div class="form-group d-md-flex justify-content-md-between">
                            <input name="bill[first_name]" type="text" class="form-control rounded-0 d-inline mr-sm-1" placeholder="First name ( optional)" style="height: 50px; width: 49%;">
                            <input name="bill[last_name]" type="text" class="form-control rounded-0 d-inline" placeholder="Last name" style="height: 50px; width: 49%;">
                        </div>
                        <div class="form-group">
                            <input name="bill[user_address]" type="text" class="form-control rounded-0" placeholder="Address" style="height: 50px;">
                        </div>
                        <div class="form-group">
                            <input name="bill[user_apartment]" type="text" class="form-control rounded-0" placeholder="Apartment, suite, etc. (optional)" style="height: 50px;">
                        </div>
                        <div class="form-group">
                            <div class="field__input-wrapper field__input-wrapper--select">
                                @php
                                    $data = \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getFromCache('get_province_by_name_provinceid');
                                        if (!$data) {
                                            $data = \Modules\ThemeWorkart\Models\Province::pluck('name', 'id');
                                            \Modules\ThemeWorkart\Http\Helpers\CommonHelper::putToCache('get_province_by_name_provinceid', $data);
                                        }
                                @endphp
                                <select class="field__input field__input--select form-control" name="bill[user_city_id]" id="billingProvince">
                                    <option value="">--- Chọn tỉnh thành ---</option>
                                    @foreach($data as $id => $v)
                                        <option value="{{ $id }}">{{ $v }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group d-md-flex justify-content-md-between">
                            <input name="bill[user_district]" type="text" class="form-control rounded-0 d-inline  mr-sm-1" placeholder="Country/Region" style="height: 50px; width: 49%;">
                            <input name="bill[user_postal]" type="text" class="form-control rounded-0 d-inline" placeholder="Postal code" style="height: 50px; width: 49%;">
                        </div>
                        <div class="form-group">
                            <input name="bill[user_tel]" type="text" class="form-control rounded-0" placeholder="Phone" style="height: 50px;">
                        </div>
                        <div class="custom-control custom-checkbox mr-sm-2 mt-3" style="margin-bottom: 34px;">
                            <input type="checkbox" class="custom-control-input" id="saveInfor">
                            <label class="custom-control-label fs14" style="color: #707070;" for="saveInfor">Save this information for next time</label>
                        </div>
                        <div class="submit-return d-flex justify-content-between">
                            <a href="" class="">&lt; Return to cart</a>
                            <button class="btn btn-orange btn-checkout" type="submit">Continue to shipping</button>
                        </div>
                    </form>
                    <div class="info-more d-flex justify-content-between" style="border-top: 1px solid #ccc; width: 100%;">
                        <a href="" class="color-text fs12">Refund policy</a>
                        <a href="" class="color-text fs12">Shipping policy</a>
                        <a href="" class="color-text fs12">Privacy policy</a>
                        <a href="" class="color-text fs12">Term of service</a>
                    </div>
                </div>

            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 mt-sm-4 mt-md-0 ml-4">
                <p style="font-size: 18px; font-weight: 600; color: #282364;">Order Summary</p>
                @foreach($products as $item)

                <div class="wp-item-cart">
                    <div class="item-cart d-flex">
                        <a href="" class="d-block"><img src="{{ \Modules\ThemeWorkart\Http\Helpers\CommonHelper::getUrlImageThumb($item->image, 50, 50) }}" class="" alt=""></a>
                        <div class="info-item-cart ml-3">
                            <p class="title-info-item-cart">{{ $item->name  }}</p>
                            <div class="custom-item-cart d-md-flex justify-content-between d-none">
                                <ul class="list-unstyled">
                                    <li>Available Products</li>
                                    <li>Portrait Canvas: 73 in</li>
                                    <li>Frame</li>
                                </ul>
                                <ul class="list-unstyled">
                                    <li>Color white</li>
                                    <li>Size 12" x 18"</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="qty-price text-right">
                        <p>1 X $18.75 SGD</p>
                    </div>
                </div>
                @endforeach

                <div class="pay-cart">
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Subtotal : ( 2 item )</span><span>$37.51 SGD</span></p>
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Shipping:</span><span>$17.51 SGD</span></p>
                    <p class="d-flex justify-content-between title-info-item-cart"><span>Total:</span><span>$57.51 SGD</span></p>
                    <form action="">
                        <div class="form-group">
                            <input type="text" class="form-control rounded-0" placeholder="Enter promote code" style="height: 50px;">
                            <span class="d-none d-lg-block">Apply</span>
                            <p class="text-center mt-3 d-lg-none apply">Apply</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('custom_header')
    <link href="{{ URL::asset('public/frontend/themes/stbd/css/checkout.css') }}" rel='stylesheet' type='text/css'/>
    <link href="{{ URL::asset('public/frontend/themes/stbd/css/checkout_dev_2.css') }}" rel='stylesheet' type='text/css'/>
    <style>
        .form-group input {
            margin: 0;
        }
        .field__input-btn-wrapper .btn-checkout {
            padding: 0px 20px;
        }
        .summary-product-list table td {
            padding: 10px !important;
        }
        td.product-price.text-right {
            min-height: 81px;
            vertical-align: middle;
            text-align: center;
        }
        span.total-line-name {
            font-weight: bold;
        }
        span.total-line-price {
            font-weight: bold;
        }
    </style>
@endsection

@section('custom_footer')
    <script>
        $(document).ready(function () {
            $('select#billingProvince').change(function () {
                $('select#billingDistrict').attr('disabled', 'disabled');
                var city_id = $(this).val();
                $.ajax({
                    url: '{{ URL::to('ajax-get-district') }}',
                    type: 'GET',
                    data: {
                        city_id : city_id
                    },
                    success: function (result) {
                        $('select#billingDistrict').removeAttr('disabled');
                        $('select#billingDistrict').html(result);
                    },
                    error: function () {
                        alert('Có lỗi xảy ra. Vui lòng F5 lại website!');
                    }
                });
            });
        });
    </script>
@endsection