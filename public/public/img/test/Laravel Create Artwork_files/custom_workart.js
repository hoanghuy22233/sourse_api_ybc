$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {
    if ($("#dropzone_file_attach").length) {
        var id = '#dropzone_file_attach';

        var uploadLayerItemImage = new Dropzone(id, { // Make the whole body a dropzone
            url: route('media.artwork.layer'), // Set the url for your upload script location
            parallelUploads: 20,
            previewsContainer: '',
            maxFilesize: 20, // Max filesize in MB
            clickable: id + " .dropzone-select", // Define the element that should be used as click trigger to select files.
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Hide the total progress bar when nothing's uploading anymore
        uploadLayerItemImage.on("complete", function (progress) {
            var obj = jQuery.parseJSON(progress.xhr.response);

            addLayerItemList(obj.name, 1);
            artworkDrawImage(obj.name, obj.image, 100,100);

        });
    }
    if ($("#dropzone_upload_template_thumbnail").length) {
        var id = '#dropzone_upload_template_thumbnail';

        var uploadTemplateThumbnail = new Dropzone(id, { // Make the whole body a dropzone
            url: route('media.artwork.layer'), // Set the url for your upload script location
            parallelUploads: 20,
            previewsContainer: '',
            maxFilesize: 20, // Max filesize in MB
            clickable: id + " .dropzone-select", // Define the element that should be used as click trigger to select files.
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Hide the total progress bar when nothing's uploading anymore
        uploadTemplateThumbnail.on("complete", function (progress) {
            let obj = jQuery.parseJSON(progress.xhr.response);
            $("#editInfoTemplate .modal-body input[name=thumbnail]").val(obj.image)

            let img = document.createElement('img');
            img.src = obj.image;
            $("#templateThumbnailPreview").empty();
            $("#templateThumbnailPreview").append(img);
        });
    }

    $('body').on('click', '.layer-item-delete', function () {
        removeLayerItem(this);
    });
    $('body').on('click', '.layer-item-copy', function () {
        duplicateLayerItem(this);
    });
    $('body').on('click', '.layer-item-copy', function () {
        duplicateLayerItem(this);
    });
    $('body').on('click', '.layer-item-visible', function () {
        showHideLayer(this);
    });
    $(document).click(function(event) {
        clickedToBodyArtwork(event);
    });

    $("body").on('click', '.layer-item', function(e){
        clickedLayerItem(this);

        e.stopPropagation();
    });
    $("#btn-save-artwork-size").click(function () {
        saveArtworkSize();
    });
    $("#btnSaveArtwork").click(function () {
        saveArtwork(this);
    });
    $("#btnAddTemplate").click(function (e) {
        e.preventDefault();
        addTemplate('New Template', '');
    });
    $('body').on('click', '.btn-edit-template', function (e) {
        e.preventDefault();
        editInfoTemplate(this);
    });
    $('body').on('click', '.btn-duplicate-template', function (e) {
        e.preventDefault();
        duplicateTemplate(this);
    });
    $("body").on('click', '.btn-remove-template', function (e) {
        e.preventDefault();
        removeTemplate(this);
    });
    $("body").on("click", ".template-item", function () {
        chooseTemplate(this);
    });
    $("#btn-save-template-info").click(function (e) {
        saveTemplateInfo(this);
    });
});
